#!/usr/bin/php
<?php
require_once '/var/www/html/site/phpmodule/include/config.ini.php';
require_once '/var/www/html/site/phpmodule/class/site.ini.php';
require_once '/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php';

// require_once '\phpmodule\include\config.ini.php';
// require_once '\phpmodule\class\site.ini.php';
// require_once '\oauth\WeixinAPI\WeixinChat.class.php';

class CronJobScript
{
	function __construct()
	{
		$this->countryid = 2;
		$this->config = new config();
		require_once "saja/mysql.ini.php";
		//require_once "..\..\lib\saja\mysql.ini.php";
		$this->model = new mysql($this->config->db[0]);
		$this->model->connect();
	}

	function changeProductStatus($productid, $status)
	{
		if ($status=='Ext'){
			//將商品資料更改為自動延長三天
			$query = "
			UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
			SET
			offtime=DATE_ADD(offtime, INTERVAL 3 DAY)
			WHERE
			prefixid = '".$this->config->default_prefix_id."'
			AND productid = '{$productid}'
			AND switch = 'Y'
			";
			$this->log("Change Product Status : ".$query);
		}else{
			//將商品資料更改為已結標
			$query = "
			UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
			SET
			closed = '{$status}'
			WHERE
			prefixid = '".$this->config->default_prefix_id."'
			AND productid = '{$productid}'
			AND switch = 'Y'
			";
			$this->log("Change Product Status : ".$query);
		}

		$res = $this->model->query($query);
	}

/*
	//流標
	//退殺幣是By userid, 針對當時所有下標但流標的商品, 下標金額加總後一起退 (不分商品productid, 也不分同一檔商品下了幾次標)
	function nobodyBid($productid)
	{
		$this->changeProductStatus($productid, 'NB');

		//取得使用者殺幣下標記錄
		$query = "
		SELECT
		distinct s.spointid
		,s.*
		FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
		LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s ON
			h.prefixid = s.prefixid
			and h.spointid = s.spointid
			and s.switch = 'Y'
		WHERE
		h.prefixid = '{$this->config->default_prefix_id}'
		AND h.productid = '{$productid}'
		AND type = 'bid'
		AND h.switch = 'Y'
		AND s.spointid IS NOT NULL
		";
		$table = $this->model->getQueryRecord($query);

		//統計使用者下標費用
		$spoint = array();
		if (!empty($table['table']['record'])) {
			foreach ($table['table']['record'] as $rk => $rv) {
				$userid = $rv['userid'];
				if (empty($spoint[$userid])) {
					$spoint[$userid]['countryid'] = $rv['countryid'];
					$spoint[$userid]['amount'] = 0.0;
				}

				$spoint[$userid]['amount'] += (float)$rv['amount'];
			}
		}

		//取得使用殺房卷下標記錄
		$query=
		"SELECT count(*) as ticket_using,userid FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h where h.productid = '{$productid}' and oscodeid>0 group by userid  order by shid desc";
		$table = $this->model->getQueryRecord($query);

		//統計使用者下標費用
		$spoint = array();
		if (!empty($table['table']['record'])) {
			foreach ($table['table']['record'] as $rk => $rv) {
				$userid = $rv['userid'];
				$spoint[$userid]['amount'] += (float)$rv['ticket_using']*100;
			}
		}

		//將手續費退還給使用者
		foreach ($spoint as $userid => $data)
		{
			$query = "
			INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint`
			(
				prefixid,
				userid,
				countryid,
				behav,
				amount,
				insertt
			)
			VALUE
			(
				'".$this->config->default_prefix_id."',
				'{$userid}',
				'{$data['countryid']}',
				'bid_refund',
				'".($data['amount'] * -1)."',
				NOW()
			)
			";
			$res = $this->model->query($query);
			$spointid = $this->model->_con->insert_id;

			$query = "
			INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history`
			(
				prefixid,
				productid,
				type,
				userid,
				spointid,
				insertt
			)
			VALUE
			(
				'".$this->config->default_prefix_id."',
				'".$this->product['productid']."',
				'refund',
				'{$userid}',
				'{$spointid}',
				NOW()
			)
			";
			$res = $this->model->query($query);

			//======== 活動免費幣使用紀錄 start ========

			//取下標時的免費幣金額
			$query2="SELECT sum(free_amount) as free_amount
					FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}spoint_free_history`
					WHERE userid = '{$userid}'
					AND behav = 'user_saja'
					AND amount_type ='1'
					AND productid = '{$this->product['productid']}'
					AND switch =  'Y' ";

			$table2 = $this->model->getQueryRecord($query2);

			if($table2['table']['record'][0]['free_amount'] < 0){

				$saja_free_amount = abs($table2['table']['record'][0]['free_amount']);
				$this->log("[s]cript/nobodyBid/spoint_free_history] productid : ".$this->product['productid'] .",free_amount : ".$saja_free_amount);

				//新增活動免費幣使用紀錄
				$query = "
				INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint_free_history`
				(
					userid,
					behav,
					amount_type,
					free_amount,
					total_amount,
					productid,
					spointid,
					insertt
				)
				VALUE
				(
					'{$userid}',
					'bid_refund',
					'1',
					'{$saja_free_amount}',
					'".($data['amount'] * -1)."',
					'{$this->product['productid']}',
					'{$spointid}',
					NOW()
				)
				";
				$this->model->query($query);
			}
			//======== 活動免費幣使用紀錄 end ========
		}
	}
*/
	//回饋使用者紅利
	function add_user_bonus($userid, $bonus, $behav)
	{
		$query = "
		INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus`
		(
			prefixid,
			userid,
			countryid,
			behav,
			amount,
			insertt
		)
		VALUE
		(
			'{$this->config->default_prefix_id}',
			'{$userid}',
			'{$this->countryid}',
			'{$behav}',
			'{$bonus}',
			NOW()
		)
		";
		$res = $this->model->query($query);
		if (!$res) {
			return false;
		}
		return $this->model->_con->insert_id;
	}

	//回饋使用者購物金
	function add_user_rebate($userid, $bonus, $behav,$productid)
	{
		$query = "
		INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."rebate`
		(
			prefixid,
			userid,
			behav,
			amount,
			insertt
		)
		VALUE
		(
			'{$this->config->default_prefix_id}',
			'{$userid}',
			'{$behav}',
			'{$bonus}',
			NOW()
		)
		";
		$res = $this->model->query($query);
		if (!$res) {
			return false;
		}
		return $this->model->_con->insert_id;
	}
	//回饋使用者店點
	// function add_user_gift($userid, $gift, $behav)
	// {
		// $query = "
		// INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."gift`
		// (
			// prefixid,
			// userid,
			// countryid,
			// behav,
			// amount,
			// insertt
		// )
		// VALUE
		// (
			// '{$this->config->default_prefix_id}',
			// '{$userid}',
			// '{$this->countryid}',
			// '{$behav}',
			// '{$gift}',
			// NOW()
		// )
		// ";
		// $res = $this->model->query($query);
		// if (!$res) {
			// return false;
		// }
		// return $this->model->_con->insert_id;
	// }

	//分潤
	function profitSharing($table) //---新增的部份
	{
		$total_bonus = 0.0;//總回饋紅利
		$total_count = 0;//總標數
		if (!empty($table['table']['record'])) {
			//紅利及店點回饋

			foreach($table['table']['record'] as $rk => $user) {

				//免費下標或是使用殺幣
				//if ((float)$this->product['saja_fee'] == 0 || empty($user['sgiftid'])) {
				//	$total_count += (int)$user['count'];
				//}

				if ($user['userid'] == $this->winner['userid']) {
					continue;//得標者不回饋
				}

				$bonusid = 0;
				if ($this->product['bonus_type'] != 'none') {
					$bonus = 0;
                    /*if($this->product['is_flash']=='Y') {
					    // 閃殺商品
						// 計算回饋商家紅利
						// if ($this->product['bonus_type'] == 'ratio') {
						// 	$bonus = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['bonus'] / 100;
						//     // 總和(每次下標的出價)
						// 	$bonus+=$user['bid_total'];
						// }
						// else if ($this->product['bonus_type'] == 'value') {
						// 	// 每一標回饋固定金額
						// 	$bonus = (int)$user['count'] * (float)$this->product['bonus'];
						// }
						// $total_bonus += $bonus;

						// 計算回饋使用者紅利(殺幣部份)---新增的部份
						if ($this->product['bonus_type'] == 'ratio') {
							$saja_bonus = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['bonus'] / 100;
						    // 總和(每次下標的出價)
							$saja_bonus+=$user['bid_total'];
						}
						else if ($this->product['bonus_type'] == 'value') {
							// 每一標回饋固定金額
							$saja_bonus = (int)$user['count'] * (float)$this->product['bonus'];
						}
						error_log("[script] ".$user['userid']." saja_bonus : ".$saja_bonus);
						if ($saja_bonus > 0) {
							//回饋使用者紅利
							$bonusid = $this->add_user_bonus($user['userid'], $saja_bonus, 'product_close');
						}
					} else {*/
						// 非閃殺商品
						// 計算回饋商家紅利
						// if ($this->product['bonus_type'] == 'ratio') {
						// 	$bonus = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['bonus'] / 100;
						// }
						// else if ($this->product['bonus_type'] == 'value') {
						// 	$bonus = (int)$user['count'] * (float)$this->product['bonus'];
						// }

						// $total_bonus += $bonus;

						//計算回饋使用者紅利額度(殺幣部份)---新增的部份
						if ($this->product['bonus_type'] == 'ratio') {

							// 根據totalfee_type退還鯊魚點 20190625 justice_lee
							switch ($this->product['totalfee_type']) {
								case 'A':
									$saja_total_bonus = ((int)$user['count'] * (float)$this->product['saja_fee']) + $user['bid_total'];
									break;
								case 'B':
									$saja_total_bonus = $user['bid_total'];
									break;
								case 'F':
									$saja_total_bonus = (int)$user['count'] * (float)$this->product['saja_fee'];
									break;
								case 'O':
									$saja_total_bonus = 0;
									break;

								default:
									$saja_total_bonus = 0;
									break;
							}

							// if ($this->product['totalfee_type'] == 'F'){
							// 	$saja_total_bonus = (int)$user['count'] * (float)$this->product['saja_fee'];
							// } else {
							// 	$saja_total_bonus = ((int)$user['count'] * (float)$this->product['saja_fee']) + $user['bid_total'];
							// }
							$saja_bonus = $saja_total_bonus * (float)$this->product['bonus'] / 100;
						}
						else if ($this->product['bonus_type'] == 'value') {

							// 根據totalfee_type退還鯊魚點 20190625 justice_lee
							if ($this->product['totalfee_type']  == 'A' || $this->product['totalfee_type']  == 'B' || $this->product['totalfee_type']  == 'F') {
								$saja_bonus = (int)$user['count'] * (float)$this->product['bonus'];
							}else{
								$saja_bonus = 0;
							}

						}
						// add coupon and bonus used records
						//$saja_bonus+=$this->get_coupons_bonus($this->$product['productid'],$user['userid']);
						if ($saja_bonus > 0) {
							//回饋使用者紅利
							//$bonusid = $this->add_user_bonus($user['userid'], $saja_bonus, 'product_close');

							//退購物金
							$bonusid = $this->add_user_rebate($user['userid'], $saja_bonus, 'bid_close',$this->$product['productid']);
							//======== 活動免費幣使用紀錄 start ========

							//取下標時的免費幣金額
							$query2="SELECT sum(free_amount) as free_amount
									FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}spoint_free_history`
									WHERE userid = '{$user['userid']}'
									AND behav = 'user_saja'
									AND amount_type ='1'
									AND productid = '{$this->product['productid']}'
									AND switch =  'Y' ";

							$table2 = $this->model->getQueryRecord($query2);

							if($table2['table']['record'][0]['free_amount'] < 0){

								$saja_free_amount = abs($table2['table']['record'][0]['free_amount']);
								if($saja_free_amount >= $saja_bonus){
									$saja_free_amount = $saja_bonus;
								}
								$this->log("[s]cript/profitSharing/spoint_free_history] productid : ".$this->product['productid'] .",free_amount : ".$saja_free_amount);

								//新增活動免費幣使用紀錄
								$query = "
								INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint_free_history`
								(
									userid,
									behav,
									amount_type,
									free_amount,
									total_amount,
									productid,
									bonusid,
									insertt
								)
								VALUE
								(
									'{$user['userid']}',
									'product_close',
									'2',
									'{$saja_free_amount}',
									'{$saja_bonus}',
									'{$this->product['productid']}',
									'{$bonusid}',
									NOW()
								)
								";
								$this->model->query($query);
							}
							//======== 活動免費幣使用紀錄 end ========
						}
					//}
				}
/*				noneed
				$giftid = 0;
				 // 未使用
				if ($this->product['gift_type'] != 'none') {
					$gift = 0;

					//計算回饋店點額度
					if ($this->product['gift_type'] == 'ratio') {
						$gift = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['gift'] / 100;
					}
					else if ($this->product['gift_type'] == 'value') {
						$gift = (int)$user['count'] * (float)$this->product['gift'];
					}

					if ($gift > 0) {
						//回饋使用者店點
						$giftid = $this->add_user_gift($user['userid'], $gift, 'product_close');
					}
				}

				$query = "
				INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."settlement_history`
				(
					prefixid,
					userid,
					productid,
					bonusid,
					giftid,
					insertt
				)
				VALUE
				(
					'{$this->config->default_prefix_id}',
					'{$user['userid']}',
					'{$this->product['productid']}',
					'{$bonusid}',
					'{$giftid}',
					NOW()
				)
				";
				$res = $this->model->query($query);*/
			}
		}

		// if ((float)$this->product['saja_fee'] != 0) {
		// 	$fee = (float)$this->product['saja_fee'];
		// }
		// else if ((float)$this->product['ads_fee'] != 0) {
		// 	$fee = (float)$this->product['ads_fee'];
		// } else {
		// 	$fee = 0;
		// }

		//廠商分潤資料
/* 		$profit_base = $total_count * $fee
				+ $this->product['retail_price'] * (float)$this->product['process_fee'] * $this->config->sjb_rate
				+ $this->winner['price'] * $this->config->sjb_rate
				- $total_bonus
				- $this->product['base_value'];

		$profit_sharing = round($profit_base * $this->product['split_rate'], 2);//給供應商的分潤

		$profit = $profit_base - $profit_sharing;//經銷商的分潤

		//2018-08-10 判斷分潤是否為負
		if ($profit < 0){
			$profit = 0;
		} */

/* 		$query = "
		INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."profit_sharing`
		(
			prefixid,
			pscid,
			productid,
			profit,
			insertt
		)
		VALUES
		(
			'{$this->config->default_prefix_id}',
			'2',
			'{$this->product['productid']}',
			'{$profit}',
			NOW()
		),
		(
			'{$this->config->default_prefix_id}',
			'1',
			'{$this->product['productid']}',
			'{$profit_sharing}',
			NOW()
		)
		";
		$res = $this->model->query($query); */
	}

	function getCloseProducts($productid)
	{

		if(!empty($productid) && $productid !=""){
			$rule = "productid ='{$productid}'";
		}else{
		  $rule = "((unix_timestamp(offtime) > 0 AND unix_timestamp() >= unix_timestamp(offtime)-10)
								OR (unix_timestamp(offtime) = 0 AND locked = 'Y'))";
		}

		$query = "
		SELECT
		*,
		unix_timestamp(offtime) as offtime
		FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
		WHERE
					prefixid = '".$this->config->default_prefix_id."'
					AND ".$rule."
					AND ptype = '1'
					AND closed = 'N'
					AND display = 'Y'
					AND switch = 'Y'
					AND is_flash= 'N'
		";
    //$this->log($query);
		return $this->model->getQueryRecord($query);
	}

	//取得得標者資料
	function getWinner($productid)
	{
		$query = "
	    SELECT
	    *
	    FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history`
	    WHERE
		prefixid = '{$this->config->default_prefix_id}'
		AND productid = '{$productid}'
		AND type = 'bid'
		AND switch = 'Y'
		GROUP BY price
		HAVING COUNT(*) = 1
		ORDER BY price ASC
		LIMIT 1
		";
		return $this->model->getQueryRecord($query);
	}

	//取得openid資料
	function getWechatopenid($userid)
	{
		$query = "SELECT * FROM `saja_user`.`v_user_profile_sso` WHERE userid='{$userid}' and sso_name='weixin' ORDER BY userid ASC LIMIT 1 ";
		error_log($query);
		return $this->model->getQueryRecord($query);
	}

	//取得coupons used
	function get_coupons_bonus($productid,$userid)
	{
		$query = "SELECT ifnull(SUM(h.bit_total),0) as bid_total_amount
			FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
			WHERE h.switch = 'Y' and (bonusid>0 or oscodeid>0)
			AND h.productid = '{$productid}'
			AND h.userid = '{$userid}'";

		//取得資料
		$table = $this->model->getQueryRecord($query);
		$bid_total_amount = $table['table']['record'][0]['bid_total_amount'];
		return $bid_total_amount;
	}

	function getSajaHistory($productid)
	{
		//統計分潤下標記錄(一般殺價, 只算殺价幣的標數)

	    // LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s ON

		$query = " SELECT h.userid, count(*) as count , h.sgiftid, sum(bid_total) as bid_total
		             FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h

		            WHERE h.prefixid = '{$this->config->default_prefix_id}'
		              AND h.productid = '{$productid}'
		              AND h.type = 'bid'
					  AND h.switch = 'Y'  GROUP BY h.userid, h.sgiftid ";

		return $this->model->getQueryRecord($query);
	}

	function getSajaHistory_flash($productid)
	{
		//統計分潤下標記錄(閃殺, 只要有下標就算)
	   	$query = "
		  SELECT h.userid, count(*) as count, h.sgiftid
		    FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
	    LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
		      ON h.prefixid = s.prefixid
	    	 AND h.spointid = s.spointid
	    	 AND s.switch = 'Y'
		   WHERE h.prefixid = '{$this->config->default_prefix_id}'
		     AND h.productid = '{$productid}'
		     AND h.type = 'bid'
		     AND h.switch = 'Y'
		GROUP BY h.userid, h.sgiftid ";

		return $this->model->getQueryRecord($query);
	}

	//---新增的部份
	function getSajaDollarsHistory($productid)
	{
        $query = " SELECT h.userid, count(*) as count, h.sgiftid, sum(price) as bid_total
                FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
            LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s ON
                h.prefixid = s.prefixid
                AND h.spointid = s.spointid
                AND s.switch = 'Y'
                WHERE
                h.prefixid = '{$this->config->default_prefix_id}'
                AND h.productid = '{$productid}'
                and h.spointid != '0'
                AND h.type = 'bid'
                AND h.switch = 'Y'
                GROUP BY h.userid, h.sgiftid ";
                error_log("[getSajaDollarsHistory] : ".$query);
		return $this->model->getQueryRecord($query);
	}


	function closeProduct()
	{

		//商家id
		$auth_id	= empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);

		//商品編號
		$productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);

		//手動結標密碼
		$sec_key	= empty($_POST['sec_key']) ? htmlspecialchars($_GET['sec_key']) : htmlspecialchars($_POST['sec_key']);

		//手動結標密碼正確才可使用單一商品結標功能
		if($sec_key != md5("qazwsxedc")){
		   $productid = "";
		}
		//echo $sec_key;

		//取得需結標之商品
		$table = $this->getCloseProducts($productid);

		die();
		if (empty($table['table']['record'])) {

			$this->log("No Product to Close !!");
			//die();
			$data['retCode'] = -2;
			$data['retMsg'] = "查無可結標商品";
			$data['retType'] = "MSG";

			echo json_encode($data,JSON_UNESCAPED_UNICODE);

			return;
		}

		//檢查用單一商品結標功能結標者與提供者是否相同
		if (!empty($productid) && $productid !="") {

			if($table['table']['record'][0]['vendorid'] != $auth_id){
                $this->log("Your ID: ".$auth_id." is Diffrent From Vendor ID !!");
				//die();
				$data['retCode'] = -3;
				$data['retMsg'] = "商家與商品提供者不同無法結標";
				$data['retType'] = "MSG";

				echo json_encode($data,JSON_UNESCAPED_UNICODE);

				return;
			}

		}

		//成功結標商品計算 初始值
		$success_closed_pro_count = 0;

		//需結標商品處理
		foreach ($table['table']['record'] as $rk => $product) {

			//將商品資料寫入暫存
			$this->product = $product;

			$this->log("Checking product id : ".$product['productid']);
			$this->log("bid type : ".$product['bid_type']." is_flash : ".$product['is_flash']);

			//取得商品所有殺價記錄
			if($product['is_flash']=='Y') {
			   $history = $this->getSajaHistory_flash($product['productid']);
			} else {
			   $history = $this->getSajaHistory($product['productid']);
			}
			/*
			$saja_history = $this->getSajaDollarsHistory($product['productid']);  //---新增的部份

			//計算總下標數
			$total_count = 0;
			if (!empty($history['table']['record'])) {
				foreach ($history['table']['record'] as $rk => $rv) {
					$total_count += (int)$rv['count'];
				}
			}
			$this->log("Total count : ".$total_count." <--> Limit : ".$product['saja_limit']);

no limit
			//檢查總下標數是否已達結標標準
			if ((int)$product['saja_limit'] > 0 && $total_count < (int)$product['saja_limit']) {

				//未達到總標數限制
				$this->log("Bids under limit !!");
				$this->changeProductStatus($product['productid'], 'Ext');
				die();

				$this->nobodyBid($product['productid'],);

				$redis = new Redis();
				$redis->connect('sajacache01',6379);
				$redis->del('getProductById:'.$product['productid'].'|Y|Y');

				$data['retCode'] = -4;
				$data['retMsg'] = "未達到總標數限制 流標";
				$data['retType'] = "MSG";
				continue;


			}
			*/

			//取得得標者資料
			$recArr = $this->getWinner($product['productid']);

            //檢查是否有得標者
			if (empty($recArr['table']['record'])) {

                //流標（無人最低且唯一）
				$this->log("No unique bid !!");
                //$this->nobodyBid($product['productid']);
				$this->changeProductStatus($product['productid'], 'Ext');
				die();
				$redis = new Redis();
				$redis->connect('sajacache01',6379);
				$redis->del('getProductById:'.$product['productid'].'|Y|Y');

				$data['retCode'] = -5;
				$data['retMsg'] = "無人最低且唯一 流標";
				$data['retType'] = "MSG";
				continue;

			}

            //將得標者寫入得標紀錄
			$this->winner = $recArr['table']['record'][0];

            //計算得標者下標金額合計
            $bid_price_total = $this->getBidPriceTotal($this->winner['productid'],$this->winner['userid']);

			$query = "
			INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product`
			(
				prefixid,
				productid,
				userid,
				nickname,
				price,
				bid_price_total,
				insertt
			)
			VALUE
			(
				'".$this->config->default_prefix_id."',
				'{$this->winner['productid']}',
				'{$this->winner['userid']}',
				'{$this->winner['nickname']}',
				'{$this->winner['price']}',
				'{$bid_price_total}',
				NOW()
			)
			";

			$res = $this->model->query($query);
			$this->profitSharing($history);
			$this->changeProductStatus($product['productid'], 'Y');

			$redis = new Redis();
			$redis->connect('sajacache01',6379);
			$redis->del('getProductById:'.$product['productid'].'|Y|Y');

			// Wechat通知中標者
			// 20150821 By Thomas
/* 			try {
				$options['appid'] = 'wxbd3bb123afa75e56';
			    $options['appsecret'] = '367c4259039bf38a65af9b93d92740ae';
				$wx=new WeixinChat($options);
				$sso=$this->getWechatopenid($this->winner['userid']);
				if (!empty($sso['table']['record'])) {
					if(!empty($sso['table']['record'][0]['uid'])) {
						$msg='【最终中标通知】 恭喜你！干掉数以百位的杀友。成为最终<a href="http://www.saja.com.tw/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$product['productid'].'">《'.$product['name'].'》</a>的中标者。请记得在七天内，至「会员」「交易纪录」「中标纪录」进行「前往结账」完成最终领取商品的程序。';
						//$msg='【最终中标通知】 恭喜你！干掉数以百位的杀友。成为最终<a href="http://test.shajiawang.com/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$product['productid'].'">《'.$product['name'].'》</a>的中标者。请记得在七天内，至「会员」「交易纪录」「中标纪录」进行「前往结账」完成最终领取商品的程序。';
                        //$msg='【最终中标通知】 恭喜你！干掉数以百位的杀友。成为最终<a href="http://qc.saja.tw/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$product['productid'].'">《'.$product['name'].'》</a>的中标者。请记得在七天内，至「会员」「交易纪录」「中标纪录」进行「前往结账」完成最终领取商品的程序。';
						error_log("[script]notify userid: ".$sso['table']['record'][0]['userid']." wechat : ".$sso['table']['record'][0]['uid']);
						$wx->sendCustomMessage('oNmoZtxOu0pKnlUjZjO0beFCSfEY',$msg,'text');
						$wx->sendCustomMessage($sso['table']['record'][0]['uid'],$msg,'text');
					}
				}
			} catch (Exception $e) {
			  error_log($e->getMessage());
			} */

			// 呼叫建立bid_info靜態網頁的function
			// 20150714 By Thomas
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, "http://www.saja.com.tw/site/bid/genBidDetailHtml/?productid=".$product['productid']."&type=deal&bg=Y");
			//curl_setopt($ch, CURLOPT_URL, "http://test.shajiawang.com/site/bid/genBidDetailHtml/?productid=".$product['productid']."&type=deal&bg=Y");
			//curl_setopt($ch, CURLOPT_URL, "http://qc.saja.tw/site/bid/genBidDetailHtml/?productid=".$product['productid']."&type=deal&bg=Y");
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			$ret=curl_exec($ch);
			curl_close($ch);
			//echo ("Gen bid info HTML of prod ".$product['productid']." :".$ret);
			$this->log("Gen bid info HTML of prod ".$product['productid']." :".$ret);
			// 拿掉得標者redis cache
			// $redis = new Redis();
			// $redis->connect('127.0.0.1');
			// $redis->del('PROD:'.$product['productid'].':BIDDER');

			// 得標者推播 20190815
			$post_array = array("productid"=>$product['productid']);
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_URL, $this->config->base_url.$this->config->default_main."/push/bid_winner");
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_array));
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			curl_close($ch);

			error_log("push/bid_winner product: ".$product['productid']);
			error_log("push/bid_winner url: ".$this->config->base_url.$this->config->default_main."/push/bid_winner");

			//成功結標商品計算
			$success_closed_pro_count = $success_closed_pro_count + 1;

			$data['retCode'] = 1;
			$data['retMsg'] = "結標完成";
			$data['retType'] = "MSG";

		}

		//有成功結標商品刷新成功統計相關資料
		if($success_closed_pro_count >= 1){
			$this->refreshBidAllTotalCount(); //刷新成功統計相關資料
		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

		return;

	}

  function log($msg) {
	  //echo(date("Y-m-d H:i:s")."--".$msg."\n");
	  // $file = fopen("script_log.txt","w");
	  // echo fwrite($file,date("Y-m-d H:i:s")."--".$msg."\n");
	  // fclose($file);
	  //file_put_contents("script_log.txt", date("Y-m-d H:i:s")."--".$msg."\n", FILE_APPEND | LOCK_EX);
	  file_put_contents("/var/www/html/site/script.log", date("Y-m-d H:i:s")."--".$msg."\n", FILE_APPEND | LOCK_EX);
	}

  /*
	* 刷新成功統計相關資料(成功人數與成功節省金額)
	*/
	function refreshBidAllTotalCount() {

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, "https://www.saja.com.tw/site/bid/getBidAllTotalCount?refresh=Y");
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		$ret=curl_exec($ch);
		curl_close($ch);

		//echo "</br>"."refreshBidAllTotalCount result:".$ret;
		$this->log("refreshBidAllTotalCount result:".$ret);

    return;

	}

	/*
	* 取得總下標金額合計
	*/
	function getBidPriceTotal($productid,$userid) {

		$query = "SELECT ifnull(SUM(h.price),0) as bid_price_total
								FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
								WHERE h.switch = 'Y'
								AND h.productid = '{$productid}'
								AND h.userid = '{$userid}'
							";

		//取得資料
		$table = $this->model->getQueryRecord($query);

    //$this->log("[getBidPriceTotal] query : ".$query);
	  //$this->log("[getBidPriceTotal] query_result : ".json_encode($table));

    $bid_price_total = $table['table']['record'][0]['bid_price_total'];

		$this->log("[getBidPriceTotal] productid :".$productid." userid :".$userid." bid_price_total : ".$bid_price_total);

		return $bid_price_total;

	}

}

// 更新cache 資料 By Thomas 2019/02/06
// 為避免影響結標 , 20190320 移到updCache.php 執行
// $redis = new Redis();
// $redis->connect('sajacache01',6379);
// $redis->del("inx_product_list");

$script = new CronJobScript();
$script->closeProduct();
//$script->refreshBidAllTotalCount(); //刷新成功統計相關資料
//$script->getBidPriceTotal(8843,1705); //計算得標者下標金額合計
// $productid = 19;
// $query = "
// SELECT
// *,
// unix_timestamp(offtime) as offtime
// FROM `".$script->config->db[4]["dbname"]."`.`".$script->config->default_prefix."product`
// WHERE
// prefixid = '".$script->config->default_prefix_id."'
// AND productid = '{$productid}'
// AND switch = 'Y'
// ";
// $recArr = $script->model->getQueryRecord($query);
// $script->product = $recArr['table']['record'][0];

// $history = $script->getSajaHistory($productid);

// $recArr = $script->getWinner($productid);
// $script->winner = $recArr['table']['record'][0];

// $script->profitSharing($history);
