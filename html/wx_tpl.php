<?php

$textTpl = "<xml>
                <ToUserName><![CDATA[%s]]></ToUserName>
                <FromUserName><![CDATA[%s]]></FromUserName>
                <CreateTime>%s</CreateTime>
                <MsgType><![CDATA[%s]]></MsgType>
                <Content><![CDATA[%s]]></Content>
                <FuncFlag>0</FuncFlag>
            </xml>";   
$newsTpl = "<xml>
               <ToUserName><![CDATA[%s]]></ToUserName>
               <FromUserName><![CDATA[%s]]></FromUserName>
               <CreateTime>%s</CreateTime>
               <MsgType><![CDATA[%s]]></MsgType>
               <ArticleCount>%s</ArticleCount>
               <Articles>";
$newsTplItem = "   <item>
                       <Title><![CDATA[%s]]></Title> 
                       <Description><![CDATA[%s]]></Description>
                       <PicUrl><![CDATA[%s]]></PicUrl>
                       <Url><![CDATA[%s]]></Url>
                   </item>";
$newsTplLast = "
               </Articles>
               <FuncFlag>1</FuncFlag>
           </xml> ";
$musicTpl = "<xml>
             <ToUserName><![CDATA[%s]]></ToUserName>
             <FromUserName><![CDATA[%s]]></FromUserName>
             <CreateTime>%s</CreateTime>
             <MsgType><![CDATA[%s]]></MsgType>
             <Music>
                 <Title><![CDATA[%s]]></Title>
                 <Description><![CDATA[%s]]></Description>
                 <MusicUrl><![CDATA[%s]]></MusicUrl>
                 <HQMusicUrl><![CDATA[%s]]></HQMusicUrl>
             </Music>
             <FuncFlag>0</FuncFlag>
             </xml>";
$voiceTpl = "<xml>
             <ToUserName><![CDATA[%s]]></ToUserName>
             <FromUserName><![CDATA[%s]]></FromUserName>
             <CreateTime>%s</CreateTime>
             <MsgType><![CDATA[%s]]></MsgType>
             <Voice>
                 <MediaId><![CDATA[%s]]></MediaId>
             </Voice>
             <FuncFlag>0</FuncFlag>
             </xml>";
?>