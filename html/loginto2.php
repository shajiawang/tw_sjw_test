<?php
require_once '/var/www/html/management/phpmodule/include/config.ini.php';
// require_once '/var/www/html/management/libs/admin.inc.php';
require_once '/var/www/lib/saja/mysql.ini.php';
require_once '/var/www/lib/saja/convertString.ini.php';

session_start();

setcookie('enterpriseid','',time() - (86400),"/",COOKIE_DOMAIN); //delete cookie
setcookie('loginname','',time() - (86400),"/",COOKIE_DOMAIN); //delete cookie
// unset($_COOKIE['enterpriseid']);

$login=$_REQUEST['login'];
$tx_key=$_REQUEST['tx_key'];

$arr=array();
		  
$enterpriseid=$_COOKIE['enterpriseid'];
$loginname=$_COOKIE['loginname'];

// error_log("[loginto2] tx_key : ".$tx_key);
error_log("[loginto2] enterpriseid : ".$enterpriseid);
error_log("[loginto2] loginname : ".$loginname);
error_log("[loginto2] form login : ".$login);

function login($loginname, $passwd0) {
    
	 global $config;
	 
     $ret=array();
	 $ret['code']=1;
     if($loginname=='' || $passwd0=='') {
	    $ret['code']=-1;
		$ret['msg']='登入失败 : 账号/密码缺误 !!';
	 }
     $query = "SELECT * FROM `saja_user`.`saja_enterprise` 
			WHERE 
			prefixid = 'saja' 
			AND loginname = '".$loginname."' 
			AND switch = 'Y' 
			";
	 // error_log($query);
	 $model = new mysql($config["db"][0]);
	 $model->connect();
	 $table = $model->getQueryRecord($query);
	 if (empty($table['table']['record'])) {
	     $ret['code']=-2;
		 $ret['msg']='登入失败 : 账号不存在!!';
     }
	 $record = $table['table']['record'][0];
	 $str = new convertString();
	 $passwd = $str->strEncode($passwd0, $config['encode_key']);
	 // error_log("[loginto login] user : ".$passwd."-".$record['passwd']);
	 if ($record['passwd']==$passwd ) {
		$ret['code']=1;
	    $ret['msg']=$record;
	 } else {
        $ret['code']=-3;
		$ret['msg']='登入失败 : 密码错误!!';
	 }
	 return $ret;
}

function getEnterprise($enterpriseid) {
     
	 global $config;
	 
	 $ret=array();
	 $ret['code']=1;
     if($enterpriseid=='') {
	    $ret['code']=-1;
		$ret['msg']='登入失败 : id缺误 !!';
	 }
     $query = "SELECT * FROM `saja_user`.`saja_enterprise` 
			WHERE 
			prefixid = 'saja' 
			AND enterpriseid = '".$enterpriseid."' 
			AND switch = 'Y' 
			";
	 error_log($query);
	 $model = new mysql($config["db"][0]);
	 $model->connect();
	 $table = $model->getQueryRecord($query);
	 if (empty($table['table']['record'])) {
	     $ret['code']=-2;
		 $ret['msg']='登入失败 : 账戶不存在!!';
     }
	 $ret['code']=1;
	 $ret['msg']=$table['table']['record'][0];
	 return $ret;
}

function redirto($action,$arrInputs='') {
          echo '<form name="f" action="'.$action.'" method="post">';
		  if($arrInputs!='') {
			  foreach($arrInputs as $name=>$value) {
				  echo '<input type="hidden" name="'.$name.'" value="'.$value.'" >';
			  }
		  }
		  echo '</form>';
	      echo '<script>document.f.submit();</script>';
		  return;
}

//    ===========================================================
if(empty($enterpriseid) && $login=='Y') {
	$loginname=$_POST['loginname'];
	$passwd0=$_POST['passwd'];
	$ret=login($loginname,$passwd0);
	if($ret['code']!=1) {
	  jsAlertMsg($ret['msg']);
	} else  {
	  $enterprise=$ret['msg'];
	  $_SESSION['sajamanagement']['enterprise'] = $enterprise;
	  setcookie('enterpriseid',$enterprise['enterpriseid'],time() + (86400),"/",COOKIE_DOMAIN); //set cookie for 1 day
	  setcookie('loginname',$enterprise['loginname'],time() + (86400),"/",COOKIE_DOMAIN); //set cookie for 1 day
	  if(!empty($tx_key)) {
	     $arr=array();
	     $arr['tx_key']=$tx_key;
		 redirto('/management/webtx/vendorConfirm2',$arr);
	  } else {
	     redirto('/management/','');
	  }
	} 
    exit;		
} 
if(!empty($enterpriseid) && $login=="") {
	$model = new mysql($config["db"][0]);
    $model->connect();
	$ret=getEnterprise($enterpriseid);
	if($ret['code']!=1) {
		jsAlertMsg($ret['msg']);
		return;
	} 
	$enterprise=$ret['msg'];
	$_SESSION['sajamanagement']['enterprise'] = $enterprise;
	setcookie('enterpriseid',$enterprise['enterpriseid'],time() + (86400),"/",COOKIE_DOMAIN); //set cookie for 1 day
	setcookie('loginname',$enterprise['loginname'],time() + (86400),"/",COOKIE_DOMAIN); //set cookie for 1 day
	if(!empty($tx_key)) {
	   $arr=array();
	   $arr['tx_key']=$tx_key;
	   redirto('/management/webtx/vendorConfirm2',$arr);
	} else {
	   redirto('/management/','');
	}
	exit;
} 	   
?>
<!DOCTYPE html>
<html>
	<head>
	<title>杀价王</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" src="/management/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="/management/js/jquery.timers-1.2.js"></script> 
		<script type="text/javascript" src="/management/js/jquery.mobile-1.4.3.min.js"></script>
		<script type="text/javascript" src="/management/js/jquery.gracefulWebSocket.js"></script>
		<script type="text/javascript" src="/management/js/jquery-ui.custom.min.js"></script>
		<script type="text/javascript" src="/management/js/messages_tw.js"></script>
	    <link rel="stylesheet"  href="/management/css/jquery-ui.css"  />
		<link rel="stylesheet"  href="/management/css/jquery.mobile-1.4.5.min.css" />
	</head>>
	<body>
<script>
   function go() {
          var f = document.loginto;
		  if(f.loginname.value=='') {
		     alert("请填写登入账号 !!");
			 return false;
		  }
		  if(f.passwd.value=='') {
		     alert("请填写登入密碼 !!");
			 return false;
		  }
		  f.submit(); 
   }
   
   function cancel() {
         var f = document.loginto;
		 f.loginname.value='';
		 f.passwd.value='';
		 return false;
   }
</script>
</head>
<body>
<div data-role="page" data-title="杀价王" >
   <div data-role="header" id="header">
      <h1>商家登入</h1>
   </div>
   <div data-role="content" id="content">
   <div class="article">
		<form name="loginto" method="POST" action="loginto2.php" >
		<?php
		    $arrKeys = array_keys($_REQUEST);
			foreach($arrKeys as $k) {
			   echo '<input type="hidden" name="'.$k.'" value="'.$_REQUEST[$k].'" >'."\n"; 
			}
		?>
		<input type="hidden" name="login" value="Y" >
		<ul data-role="listview" data-inset="true" data-icon="false">
			<li>
			   账号 : <input type="text" id="loginname" name="loginname" value="<?php echo $loginname; ?>">
			</li>
			<li>
			   密码 : <input type="password" id="passwd" name="passwd" >
			</li>
			<li>
			<div data-role="controlgroup" data-type="horizontal">
				<a href="#" data-role="button" data-icon="delete" data-iconpos="left" onclick="cancel()">清除</a>
				<a href="#" data-role="button" data-icon="check" data-iconpos="right" onclick="go()">登入</a>
			<div>
			</li>
		</ul>
		</form>
	</div>
  </div>
<div data-role="footer" id="footer">
  <h4>全球首创杀价式拍卖导购平台</h4>
</div>
</body>
</html>
