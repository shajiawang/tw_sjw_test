	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- area 內容區塊 ******************************-->
		<div class="about_area about_style clearboth clearfix">
		<div class="main_layout main_style">
				
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">登入</h2>
						<h3 class="main_sub font_avant_garde">Login</h3>
					</span>
					<hr class="main_hr">
				</div>
				
				<div class="container">
				<div class="row" style="margin:0 0 20px 0px;">
					<div class="col-md-4 col-md-offset-4">
						
						<form method="POST" role="form" id="loginform" class="form-signin" action="<?=base_url('login/login_check');?>">
						<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
							
							<fieldset>
								<h3 class="sign-up-title" style="color:dimgray; margin-bottom:20px;">請登入會員專區</h3>
								<?php /*if ($error) { ?>
								<div id="alert_error" class="alert bs-callout bs-callout-danger alert-dismissible" role="alert">
									<button class="close" aria-label="Close" data-dismiss="alert" type="button" style="color:red">
										<span aria-hidden="true">× <?=$error;?></span>
									</button>
								</div>
								<?php }*/ ?>
								
								<div class="form_group">
									<label class="form_label" for="username">登入帳號</label>
									<input type="text" name="username" id="user_name" value="<?=$username;?>" placeholder="會員帳號,或手機號碼" class="form_input form_control text ui-widget-content ui-corner-all">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
									<div id="username_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>登入帳號，不可空白。</span></div>
								</div>
								
								<div class="form_group">
									<label class="form_label" for="password">登入密碼</label>
									<input type="password" name="password" id="user_password" value="" class="form_input form_control text ui-widget-content ui-corner-all" >
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
									<div id="password_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>登入密碼，不可空白。</span></div>
								</div>
								<?php /*
								<a class="pull&#45;right" href="#forgot">Forgot password?</a>
								<div class="checkbox" style="width:140px;"><label><input name="remember" type="checkbox" checked>Remember Me</label></div>
								<input class="btn btn-lg btn-success " type="submit" value="Login" disabled="disabled">
								*/?>
								
								<div style="float:left; padding:5px;">
									<input class="btn btn-lg btn-success " id="js_submit" type="button" value="登入" >
								</div>
								<?php /*
								<div style="float:left; padding:5px;">
									<input class="btn btn-lg btn-secondary " type="button" onclick="location.href='<?=base_url('login/forgot');?>'" value="忘記密碼?" >
								</div>
								*/?>
								
							</fieldset>
						</form>
					</div>
			  </div>
				</div>
		</div>
		</div>
		<!--/ area 內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

<script>
$(document).ready(function(){
	var form_stat = true;
	
	$('#js_submit').click(function(){
		if($('#user_name').val()==''){
			$('#username_error').addClass('help_tips');
			form_stat = false;
		}
		if($('#user_password').val()==''){
			$('#password_error').addClass('help_tips');
			form_stat = false;
		}
		
		if(form_stat == true){
			$('#loginform').submit();
		}
	});
});
</script>
