<div id="js_push-right" class="shoppingcart_area shoppingcart_style menu_same c-menu--push-right " role="cart">
<button type="button" class="js_menu_close menu_close"><i class="icon-close"></i></button>
<h4 class="cart_title">我的購物車</h4>
    <div class="clearfix hide" id="cart_onload_img">
        <div class="process_area">
        <div class="cart_list">
            
			<div class="process_content">
                <div class="process_info">
                    <div class="ic-Spin-cycle--classic-salmon-yellow">
                        <svg xmlns="//www.w3.org/2000/svg" xmlns:xlink="//www.w3.org/1999/xlink" version="1.1" x="0" y="0" viewBox="156 -189 512 512" enable-background="new 156 -189 512 512" xml:space="preserve">
                            <path d="M636 99h-64c-17.7 0-32-14.3-32-32s14.3-32 32-32h64c17.7 0 32 14.3 32 32S653.7 99 636 99z"></path>
                            <path d="M547.8-23.5C535.2-11 515-11 502.5-23.5s-12.5-32.8 0-45.2l45.2-45.2c12.5-12.5 32.8-12.5 45.2 0s12.5 32.8 0 45.2L547.8-23.5z"></path>
                            <path d="M412-61c-17.7 0-32-14.3-32-32v-64c0-17.7 14.3-32 32-32s32 14.3 32 32v64C444-75.3 429.7-61 412-61z"></path>
                            <path d="M276.2-23.5L231-68.8c-12.5-12.5-12.5-32.8 0-45.2s32.8-12.5 45.2 0l45.2 45.2c12.5 12.5 12.5 32.8 0 45.2S288.8-11 276.2-23.5z"></path>
                            <path d="M284 67c0 17.7-14.3 32-32 32h-64c-17.7 0-32-14.3-32-32s14.3-32 32-32h64C269.7 35 284 49.3 284 67z"></path>
                            <path d="M276.2 248c-12.5 12.5-32.8 12.5-45.2 0 -12.5-12.5-12.5-32.8 0-45.2l45.2-45.2c12.5-12.5 32.8-12.5 45.2 0s12.5 32.8 0 45.2L276.2 248z"></path>
                            <path d="M412 323c-17.7 0-32-14.3-32-32v-64c0-17.7 14.3-32 32-32s32 14.3 32 32v64C444 308.7 429.7 323 412 323z"></path>
                            <path d="M547.8 157.5l45.2 45.2c12.5 12.5 12.5 32.8 0 45.2 -12.5 12.5-32.8 12.5-45.2 0l-45.2-45.2c-12.5-12.5-12.5-32.8 0-45.2S535.2 145 547.8 157.5z"></path>
                        </svg>
                    </div>
                    <h5 class="process_txt">讀取中...請稍後</h5>
                </div>
            </div>
        </div>
        </div>
    </div>
    <div class="cart_content" id="cart_content">
		<ul class="list-unstyled">
        <li class="cart_list cart_empty cart_check">
            <i class="icon-shopping-basket"></i>
            <span>購物車內無任何物品！</span>
        </li>
		</ul>
		<?php /*
		
		<button id="go_to_payment" type="button" class="btn_gopay icon-emoji-happy btn btn_block btn_lg btn_animated  four_word"><span>前往結帳</span></button>
		<script>
		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN':  '9aDsWPIFprHIsxNh2vODsziIY0d2OAzmUOEBeN3v' }
		});
		var payment_url = 'https://customerexperience.waca.ec/cart';
		</script>
		<script src="<?=$static_js;?>ec/cart.js" type="text/javascript" ></script>
		*/?>
	</div>
</div>