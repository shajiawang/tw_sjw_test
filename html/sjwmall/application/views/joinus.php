	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- area 內容區塊 ******************************-->
		<div class="about_area about_style clearboth clearfix">
		<div class="main_layout main_style">
				
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">註冊</h2>
						<h3 class="main_sub font_avant_garde">join us</h3>
					</span>
					<hr class="main_hr">
				</div>
				
				<div class="container">
				<div class="row" style="margin:0 0 20px 0px;">
					<div class="col-md-4 col-md-offset-4">
						
						<form method="POST" role="form" id="joinform" class="form-signin" action="<?=base_url('login/join_check');?>">
						<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
							
							<fieldset>
								<h3 class="sign-up-title" style="color:dimgray; margin-bottom:20px;"><span style="color:blue">註冊新用戶</span></h3>
								<?php /*if ($error) { ?>
								<div id="alert_error" class="alert bs-callout bs-callout-danger alert-dismissible" role="alert">
									<button class="close" aria-label="Close" data-dismiss="alert" type="button" style="color:red">
										<span aria-hidden="true">× <?=$error;?></span>
									</button>
								</div>
								<?php }*/ ?>
								
								<div class="form_group">
									<label class="form_label" for="phone">手機號碼</label>
									<input name="phone" id="phone" value="<?=$phone;?>" maxlength="10" placeholder="請輸入手機號碼" type="tel" class="form_input form_control text ui-widget-content ui-corner-all">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
									<div id="phone_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>手機號碼，不可空白。</span></div>
									<div id="phone_help" class="help_block help_tips"><i class="icon-triangle-right"></i><span>請輸入10碼的手機號碼, 以便接收簡訊提醒。</span></div>
								</div>
								
								<div class="form_group">
									<label class="form_label" for="email">E-mail</label>
									<input name="email" id="email" value="<?=$email;?>" maxlength="60" placeholder="E-Mail, EX: example@aaa.com" type="email"class="form_input form_control text ui-widget-content ui-corner-all">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
									<div id="email_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>E-mail，不可空白。</span></div>
									<div id="email_help" class="help_block help_tips"><i class="icon-triangle-right"></i><span>請輸入E-mail, 以便接收確認訊息。</span></div>
								</div>
								
								<div class="form_group">
									<label class="form_label" for="password">設定密碼</label>
									<input type="password" name="password" id="password" value="" maxlength="20" class="form_input form_control text ui-widget-content ui-corner-all" >
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
									<div id="password_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>密碼，不可空白。</span></div>
									<div id="password_help" class="help_block help_tips"><i class="icon-triangle-right"></i><span>請輸入密碼, 英數字符 6 ~ 20碼。</span></div>
								</div>
								
								<div class="form_group">
									<label class="form_label" for="passconf">再次確認</label>
									<input type="password" name="passconf" id="passconf" value="" maxlength="20" class="form_input form_control text ui-widget-content ui-corner-all" >
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
									<div id="passconf_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>請輸入再次確認密碼。</span></div>
								</div>
								
								<input class="btn btn-lg btn-success btn-block" id="js_submit" type="button" value="確定提交" >
								
							</fieldset>
						</form>
					</div>
			  </div>
				</div>
		</div>
		</div>
		<!--/ area 內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->


<script>
$(document).ready(function(){
	var form_stat = true;
	
	$('#js_submit').click(function(){
		if($('#phone').val()==''){
			$('#phone_error').addClass('help_tips');
			$('#phone_help').removeClass('help_tips');
			form_stat = false;
		}
		if($('#email').val()==''){
			$('#email_error').addClass('help_tips');
			$('#email_help').removeClass('help_tips');
			form_stat = false;
		}
		if($('#password').val()==''){
			$('#password_error').addClass('help_tips');
			$('#password_help').removeClass('help_tips');
			form_stat = false;
		}
		if($('#passconf').val()=='' || ($('#passconf').val() !== $('#password').val()) ){
			$('#passconf_error').addClass('help_tips');
			form_stat = false;
		}
		
		if(form_stat == true){
			$('#joinform').submit();
		}
	});
});
</script>

