<script>
function add_Cart()
{
	var u = '<?=$cart_action_save;?>';
	var $form = $('#list-form');
	$('#addcart').val('y');
	//console.log();
	$form.attr('action', u).submit();
}
function add_Cart2(){
	add_Cart();
}
</script>

<div class="category_area">

	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
    
	<aside class="accordion_area nav_area clearfix" role="left_category">
    <div class="js_navfixed_area">
	
	<ul class="nav_content clearfix js_nav_content nav_14px">
        <li class="nav_category clearfix">
        
        <ul class="nav js_nav_color clearfix">
			<li class="nav_allproducts dropdown_toggle  ">
				<input id="nav_title_LqnCp9S3_all" name="accordion_LqnCp9S3" class="hide" checked="checked" type="checkbox">
				<label class="nav_title " for="nav_title_LqnCp9S3_all" style="color: #000">
					<a href="<?=$category_url;?>" style="color:#000" >全部商品</a>
				</label>
				
				<?php if(!empty($category_menu)){
				foreach($category_menu as $k=>$cat_menu)
				{
					$class_act = 'style="color:#000"';
					$icon_triangle = '';
					if(intval($detail['vendor_category'])==$k){ 
						$class_act = 'class="active "';
						$icon_triangle = '<i class="icon-triangle-right"></i>';
					}
				?>
				<label class="nav_title " style="color: #000">
					<a href="<?=base_url($cat_menu['link']);?>" <?=$class_act;?> ><?=$icon_triangle;?><?=$cat_menu['title'];?></a>
				</label>
				<?php } } ?>
			</li>
		</ul>
    </li>
	</ul>
	</div>
	</aside>

	<main class="main clearfix" role="main">
	<div class="product_area">
	
        <?php /*
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		*/?>
		
		<!-- content_area 內容區塊 ******************************-->
		<div class="header_a">
		<div class="product_content clearboth product_style">
		
		<form id="list-form" method="post"><input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
		<input type="hidden" name="addcart" id="addcart" value="" />
			
			<div class="main_layout main_style">
			<div class="product_info clearfix">
			<div class="row">
					
					<div class="col-md-7 "><!--col-sm-6 col-xs-12-->
						<div class="product_gallery">
							<div class="swiper-container swiper-container-horizontal" style="cursor: grab;">
								<div class="swiper-wrapper my-gallery" data-pswp-uid="1">
								<div class="swiper-slide swiper-slide-active" data-size="600x443" >
									<img class="swiper-lazy swiper-lazy-loaded" data-small="<?=$detail['image_thumb_url'];?>" src="<?=$detail['image_url'];?>">
								</div>
								</div>
								<?php /* //小縮圖
								<div class="clearfix product_thumbs swiper-pagination-clickable swiper-pagination-bullets">
								<span class="swiper-pagination-bullet swiper-pagination-bullet-active" style="width: 75px;height: 75px;background: #fff;opacity: 1;position: relative;display: block;border-radius: 0;float: left;"><img src="<?=$detail['image_thumb_url'];?>"></span>
								</div>
								*/?>
							</div>
						</div>
					</div>

					<!-- Lightbox 燈箱 ******************************-->
					<div class="pswp" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="pswp__bg"></div>
						<div class="pswp__scroll-wrap">
							<div class="pswp__container">
								<div class="pswp__item"></div>
								<div class="pswp__item"></div>
								<div class="pswp__item"></div>
							</div>
							<div class="pswp__ui pswp__ui--hidden">
								<div class="pswp__top-bar">
									<div class="pswp__counter"></div>
									<button class="pswp__button pswp__button--close" title="Close (Esc)"></button>
									<button class="pswp__button pswp__button--fs" title="Toggle fullscreen"></button>
									<button class="pswp__button pswp__button--zoom" title="Zoom in/out"></button>
									<div class="pswp__preloader">
										<div class="pswp__preloader__icn">
											<div class="pswp__preloader__cut">
												<div class="pswp__preloader__donut"></div>
											</div>
										</div>
									</div>
								</div>
								<div class="pswp__share-modal pswp__share-modal--hidden pswp__single-tap">
									<div class="pswp__share-tooltip"></div>
								</div>
								<button class="pswp__button pswp__button--arrow--left" title="Previous (arrow left)"></button>
								<button class="pswp__button pswp__button--arrow--right" title="Next (arrow right)"></button>
								<div class="pswp__caption">
									<div class="pswp__caption__center"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- Lightbox 燈箱 ******************************-->
					
					<div class="col-md-5 product_description_area"><!--col-sm-6 col-xs-12-->
					
					<ul class="product_share list-unstyled clearfix">
						<?php /*
						<li><a class="btn link_share btn_link_twitter btn_block" href="https://twitter.com/" target="_new" rel="nofollow"><i class="icon-twitter2"></i></a></li>
						*/?>
						<li><a class="btn link_share btn_link_facebook btn_block" href="#" rel="nofollow"><i class="icon-facebook2"></i></a></li>
						<li><a class="btn link_share btn_link_line btn_block" href="http://line.me/R/msg/text/?<?=$line_share;?>"><i class="icon-line"></i></a></li>
					</ul>
					
					<div class="product_description clearfix">
					
					<div class="mobile_product_tags">
						<div class="product_tags_area clearfix"></div>
						<h4 class="product_num">商品編號：<span class="font_montserrat"><?=$detail['no'];?></span></h4>
					</div>
					
					<!-- 商品名稱 -->
					<h1 class="product_name">
						<span class="js_product_name"><?=$detail['name'];?></span>
					</h1>
					<!--/ 商品名稱 -->
					
					<div class="product_price" >
						<div class="product_sale">
							<sub class="txt_origin">訂價：<span class="font_montserrat"><span class="line_through">$<?=$detail['price'];?></span></span></sub>
							<h2 class="txt_sale">
								<?php if($detail['sale_price']){ ?>
								<span class="currency_sign">最殺價：</span><span class="font_montserrat">$<?=$detail['sale_price'];?></span>
								<?php } else { ?>
								<span class="currency_sign">優惠價</span>
								<?php } ?>
							</h2>
						</div>
					</div>
					
					<?php /*
					<input id="tags_shipping_title" class="tabs_title_ipt hide" name="tabs" checked="checked" type="radio">
					<label class="tabs_title_lab" for="tags_shipping_title"><i class="icon-local-shipping"></i><span>運送方式</span></label>
					<section id="tags_shipping_content" class="tabs_content">
						<ul class="tags_article_block clearfix">
								<li class="tags_preorder_info">實體門市取貨</li>
								<li class="tags_preorder_info">一般宅配</li>
								<li class="tags_preorder_info">7-11取貨不付款</li>
								<li class="tags_preorder_info">超商取貨</li>
								<li class="tags_preorder_info">貨到付款</li>
								<li class="tags_preorder_info">國際快遞</li>
								<li class="tags_preorder_info">低溫運送</li>
								<li class="tags_preorder_info">機車配送</li>
						</ul>
					</section>
					*/?>

					<div class="product_collection">
					<div class="row">
						<?php /*
						<div class="col-md-6 ">
							<div class="form_group">
								<label class="form_label" for="">選擇款式</label>
								<div class="form_data">
									<div class="form_select_block">
										<select id="js_desktop_product_standard" class="form_select form_control js_product_standard js_select_change">
											<option value="5392" data-product-stock="1_50" data-stock="ok" data-name="" selected="selected"></option>
										</select>
										<span class="form_select_bind"></span>
									</div>
								</div>
							</div>
						</div>
						*/?>
						
						<div class="col-md-6 js_addcart_origin ">
							<div class="form_group">
							<label class="form_label" for="">數量</label>
							<div class="form_data">
									<div class="visible-md visible-lg">
										<div class="form_select_block">
											<select class="form_select form_control js_product_qty js_select_change">
													<option value="1" selected="selected">1</option>
													<?php /*
													<option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>
													*/?>
												</select>
											<span class="form_select_bind">1</span>
										</div>
									</div>
									
									<?php /*
									<div class="visible-xs visible-sm">
										<div class="js_quantity form_quantity qty_md">
											<button type="button" class="js_qty_minus qty_minus" disabled="disabled"></button>
											<input name="quantity" value="1" class="js_product_qty js_qty_value qty_value" data-max="50" data-min="1" type="text">
											<button type="button" class="js_qty_plus qty_plus"></button>
										</div>
									</div>
									*/?>
								</div>
							</div>
						</div>
					</div>
					</div>
				
					<div class="product_btn" style="margin-top:15px;">
					<div class="js_addcart_origin ">
						<div class="row">
								<?php /*
								<div class="col-md-6 col-sm-6">
									<div class="form_group">
										<button type="button" class="btn btn_addcart btn_block btn_lg btn_animated icon-heart five_word js_add_cart">
											<span>加入購物車</span>
										</button>
									</div>
								</div>*/?>
							
								<div class="form_group form_lastmargin">
									<?php if($detail['start_stat'] && $detail['end_stat']){ ?>
										<?php if($detail['quantity']){ ?>
										<button type="button" id="js_go_cart" onclick="add_Cart()" class="btn btn_gopay btn_block btn_lg btn_animated four_word icon-emoji-happy">
											<span>我要搶購</span>
										</button>
										<?php } else { ?>
										<button type="button" class="btn btn_soldout disabled btn_block btn_lg" style="border-color:#c3c2c2;color:#2d2b2b;"><span>已售完</span></button>
										<?php } ?>
									
									<?php } else { ?>
										<button type="button" class="btn btn_soldout disabled btn_block btn_lg" style="border-color:#c3c2c2;color:#2d2b2b;"><span>
										<?php echo ($detail['start_msg']) ? $detail['start_msg'] : $detail['end_msg'];?>
										</span></button>
									<?php } ?>
								</div>
						</div>
					</div>
					
					<?php /*
					<div class="js_addcart_soldout hide">
						<div class="row">
							<div class="col-md-7 col-md-offset-5">
								<div class="form_group form_lastmargin">
									<!-- sold out -->
									<button type="button" class="btn btn_soldout disabled btn_block btn_lg"><span>已售完</span></button>
									
									<!-- out of stock -->
									<button type="button" class="btn btn_soldout disabled btn_block btn_lg"><span>庫存不足</span></button>
								</div>
							</div>
						</div>
					</div>
					*/?>
					</div>
					
				</div>
				</div>
				
				</div>
				</div>
				</div>
				
				<div class="main_layout main_style">
					<article class="product_details clearfix">
						<header class="title_area clearfix">
							<hgroup class="title_content clearfix">
								<h4 class="main_title">商品詳情</h4>
								<h5 class="main_sub font_avant_garde">product description</h5>
							</hgroup>
							<hr class="main_hr">
						</header>
						
						<section class="product_feature inner_layout clearfix">
							<?=$detail['description']?>
							<p>&nbsp;</p>
						</section>
					</article>
				</div>
				
				<div class="btn_mobile_gopay">
				<?php if($detail['start_stat'] && $detail['end_stat']){ ?>
						<?php if($detail['quantity']){ ?>
							<button type="button" id="js_go_payment_toggle" onclick="add_Cart2()" class="js_slide_button btn btn_gopay btn_block btn_lg btn_animated icon-emoji-happy four_word ">
							<span>我要搶購</span>
							</button>
						<?php } else { ?>
							<button type="button" class="btn btn_soldout disabled btn_block btn_lg" style="border-color:#c3c2c2;color:#2d2b2b;"><span>已售完</span></button>
						<?php } ?>
									
				<?php } else { ?>
					<button type="button" class="btn btn_soldout disabled btn_block btn_lg" style="border-color:#c3c2c2;color:#2d2b2b;"><span>
					<?php echo ($detail['start_msg']) ? $detail['start_msg'] : $detail['end_msg'];?>
					</span></button>
				<?php } ?>
				</div>
		
		</form>
		</div>
		</div>
		<!--/ content_area 內容區塊 ******************************-->

	<?php /*
	<script>
	var shops = '{"urls":{"productlist":"https:\/\/customerexperience.waca.ec\/productlist"},"sliders":[],"message":"","type":"product","value":"all","products_total":24,"is_backend":false,"is_fb":false,"shops_name":"WACA\u5546\u5e97\u9ad4\u9a57 X \u9280 Ying \u6212 Chieh","shops_popup":{"status":0,"type":"","img_height":500,"img_url":"","edm_content":""}}';
	</script>
	*/?>
	</div>
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

</div>

<script src="<?=$static_js;?>product.js" type="text/javascript" ></script>
<script src="<?=$static_js;?>products_recommend.js" type="text/javascript" ></script>
