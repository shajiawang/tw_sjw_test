<style type="text/css">
.full-screen {
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
}
.item>a{
	display: block;
	height: 100%;
}
.welcome_layout {
    background-color: #fff;
    border-color: #d9d9d9;
	margin-bottom: 20px;
    padding: 30px;
}
</style>

	<!-- main_area 主內容區塊 ******************************-->
	<div class=" clearfix">
	<main class="main clearfix" role="main">
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<?php /*<div class="breadcrumb_area visible-lg breadcrumb_index  list-unstyled clearfix" style="margin-bottom: 0;">
			<div class="breadcrumb_home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a href="<?=$home_url;?>" style="color: #000" itemprop="item"><span class="icon_home" itemprop="name"><i class="icon-store_mall_directory"></i></span><meta itemprop="position" content="1"></a></div>
			<ol class="" itemscope="" itemtype="http://schema.org/BreadcrumbList">
				<li style="color: #000" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<a class="current" href="#" style="color: #000" itemprop="item">
						<span itemprop="name">首頁</span>
						<meta itemprop="position" content="2">
					</a>
				</li>
			</ol>
		</div>*/?>
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		
		<!-- index_area 首頁內容區塊 ******************************-->
		<div class="index_area index_style js_index_area js_themes_layout layout_h">
		<div class="container">
			
			<!-- Banner_Slider_Carousel 首頁橫幅輪播 ******************************-->
			<div id="mycarousel" class=" carousel slide" data-ride="carousel" style="margin-bottom: 30px;">
			<ol class="carousel-indicators"></ol>
				  
				  <!-- 廣告輪播列表 -->
				  <div class="carousel-inner" role="listbox">
					
					<?php if(! empty($_banner)){ 
					foreach ($_banner as $k=>$_b) 
					{
						$_a = ($k==0) ? 'active' : '';
					?>
					<div class="item <?=$_a;?>">
						<a href="<?=$_b['url'];?>" target="<?=$_b['url_target'];?>">
							<img src="<?=$_b['image'];?>" data-color="#eee">
							<div class="carousel-caption">
								<h3><?=$_b['slogan'];?></h3>
							</div>
						</a>
					</div>
					
					<?php } } else { ?>
					
					<div class="item active">
						<a href="">
							<img src="<?=$static_img;?>def_banner0.jpg" data-color="#eee">
							<div class="carousel-caption">
								<h3>廣告輪播1</h3>
							</div>
						</a>
					</div>
					<div class="item">
						<img src="<?=$static_img;?>def_banner1.jpg" data-color="#eee">
						<div class="carousel-caption">
							<h3>廣告輪播2</h3>
						</div>
					</div>
					<div class="item ">
						<img src="<?=$static_img;?>def_banner2.jpg" data-color="#eee">
						<div class="carousel-caption">
							<h3>廣告輪播3</h3>
						</div>
					</div>
					<?php } ?>
				  </div>

				  <!-- Controls -->
				  <a class="left carousel-control" href="#mycarousel" role="button" data-slide="prev">
					<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">上一則</span>
				  </a>
				  <a class="right carousel-control" href="#mycarousel" role="button" data-slide="next">
					<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">下一則</span>
				  </a>
			</div>
			<!--/ Banner_Slider_Carousel 首頁橫幅輪播 ******************************-->
		
			<div class="item_area welcome_layout">
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">新品首賣</h2>
						<h3 class="main_sub font_avant_garde">new product</h3>
					</span>
					<hr class="main_hr">
				</div>
				
				<ul class="list-unstyled item_content clearfix" id="product-list">
				
				<?php foreach ($list as $row) 
				{ ?>
				<li class="item_block js_is_photo_style img_polaroid ">
					
					<a class="clearfix" href="<?=$row['link_edit'];?>" >
					<span class="item_photo js_product_image " <?php /*data-src="<?=$row['image_thumb_url'];?>"*/?> style="background-image: url('<?=$row['image_thumb_url'];?>')"></span>
					</a>
					<div class="item_caption js_themes_products_bg_color" style="">
					<span class="item_vertical">
						<div class="item_info js_matchheight js_themes_products_color" style="height: 84px;">
							<div class="item_text" style="">
								<a class="clearfix" href="<?=$row['link_edit'];?>" >
								<h4 class="item_name text_overflow js_products_name pt_fontsize_12px" style="color: #858585;">
								<?=$row['name'];?> 
								</h4>
								</a> 
								
								<div class="col-md-7 text-left">
									<a class="clearfix" href="<?=$row['link_vendor'];?>" ><?=$row['comp_name'];?></a>
								</div>
								
								<div class="col-md-5 text-right">
								<ul class="list-unstyled item_price">
									<?php /*<li class="item_origin item_actual" itemprop="price"><span class="font_montserrat line_through" style="color: #858585;">$<?=$row['price'];?></span></li>*/?>
									<li class="item_sale" itemprop="price"><span class="font_montserrat">
									搶購價 
									<?php if($row['sale_price']){ ?>
									$<?=$row['sale_price'];?>
									<?php } ?>
									</span></li>
								</ul>
								</div>
							</div>
						</div>
					</span>
					</div>
					
				</li>
				<?php } ?>

				</ul>
			<div id="js_noproductdata" style="display: none;"></div>
			</div>
			
			<?php
			/*
			<div class="row">
				<div class="col-md-5">
					Showing <?=$filter['offset']+1;?>
					to <?=(($filter['offset']+$filter['rows'])>$filter['total'])?$filter['total']:$filter['offset']+$filter['rows'];?>
					of <?=$filter['total'];?> entries
				</div>
				<div class="col-md-7 text-right">
					<?=$this->pagination->create_links();?>
				</div>
			</div>
			*/?>
			
		</div>
		
		</div>
		<!--/ index_area 首頁內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

<script>
/* Banner_Slider_Carousel 首頁橫幅輪播 */
$(function(){
	var $item = $('.carousel .item'); 
	var $wHeight = 350;//$(window).height();
	//$('.carousel').css('max-width',$('.carousel img').width());
	
	$item.height($wHeight); 
	$item.addClass('full-screen');

	$('.carousel img').each(function() {
		var $src = $(this).attr('src');
		var $color = $(this).attr('data-color');
		$(this).parent().css({
			'background-image' : 'url(' + $src + ')',
			'background-color' : $color
		});
		$(this).remove();
	});

	//下方自動加入控制圓鈕
	var total = $('.carousel .carousel-inner div.item').size();
	append_li();
	function append_li(){
		var li = "";
		var get_ac = $( ".carousel .active" );
		var ac =  $( ".carousel .carousel-inner div" ).index( get_ac );

		for (var i=0; i <= total-1; i++){
			if(i == (ac)/2){
				li += "<li data-target='#mycarousel' data-slide-to='"+i+"' class='active'></li>";					
			}else{
				li += "<li data-target='#mycarousel' data-slide-to='"+i+"' class=''></li>";						
				}
			}
			$(".carousel-indicators").append(li);
	}

	//單則隱藏控制鈕
	if ($('.carousel .carousel-inner div.item').length < 2 ) { 
		$('.carousel-indicators, .carousel-control').hide();
	}

	//縮放視窗調整視窗高度
	/*$(window).on('resize', function (){
		$wHeight = $(window).height();
		$item.height($wHeight);
	});*/

	//輪播秒數與滑入停止
	$('.carousel').carousel({
		interval: 5000,
		pause: "hover"
	});
});
</script>