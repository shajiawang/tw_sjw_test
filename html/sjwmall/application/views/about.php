	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		
		<!-- about_area 內容區塊 ******************************-->
		<div class="about_area about_style clearboth clearfix">
		<div class="main_layout main_style">
				
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">關於本站</h2>
						<h3 class="main_sub font_avant_garde">about us</h3>
					</span>
					<hr class="main_hr">
				</div>
		
				<div class="about_media clearfix">
					<div class="about_thumb">
						<img src="<?=$_SETTING['web_logo_image'];?>" alt="">
					</div>
					<div class="about_heading">
						<h1 class="about_company"><?=$_SETTING['web_site_name'];?></h1>
					</div>
				</div>
				
				<article class="about_article inner_layout clearfix">
					<?=$site_info['info'];?>
				</article>
		</div>
		</div>
		<!--/ index_area 首頁內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->
	
	
