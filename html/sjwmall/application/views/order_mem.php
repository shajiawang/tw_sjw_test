<div class="category_area">

	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
    
	<aside class="accordion_area nav_area clearfix" role="left_category">
    <div class="js_navfixed_area">
	
	<ul class="nav_content clearfix js_nav_content nav_14px">
        <li class="nav_category clearfix">
        
        <ul class="nav js_nav_color clearfix">
			<li class="nav_home "><a href="<?=base_url('member');?>" style="color:#000">會員專區</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/paycheck');?>" style="color: <?php echo ($this->router->method=='paycheck')?'#ff0000':'#000';?>">結帳中訂單</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/freight');?>" style="color: <?php echo ($this->router->method=='freight')?'#ff0000':'#000';?>">備貨中訂單</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/finish');?>" style="color: <?php echo ($this->router->method=='finish')?'#ff0000':'#000';?>">過往訂單</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/cancel');?>" style="color: <?php echo ($this->router->method=='cancel')?'#ff0000':'#000';?>">作廢訂單</a></li>
			
			<li class="nav_allproducts dropdown_toggle  ">
				<?php if(!empty($category_menu)){
				foreach($category_menu as $cat_menu){ ?>
				<label class="nav_title " style="color: #000">
					<a href="<?=base_url($cat_menu['link']);?>" style="color: #000" ><?=$cat_menu['title'];?></a>
				</label>
				<?php } } ?>
			</li>
		</ul>
    </li>
	</ul>
	</div>
	</aside>
					
	<main class="main clearfix" role="main">
	
		<!-- list_area 內容區塊 ******************************-->
		<div class="header_a">
		
		<div class="index_area index_style js_index_area js_themes_layout layout_h">
		<div class="container">
			
			<div class="item_area ">
				
				<form id="list-form" method="post">
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                    
					<table class="main_style table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th class="">訂單編號</th>
                                <th class="">總金額</th>
								<th class="">運費</th>
                                <th class="">品項數</th>
                                <th class="">新增時間</th>
                                <th class="">訂單狀態</th>
                                <th></th>
                            </tr>
                        </thead>
                        
						<tbody>
                        <?php if(! empty($list)) 
						foreach ($list as $row) { ?>
                            <tr class="<?=($row['order_status_id']==4)?'text-danger':'';?> ">
                                <td><?=$row['order_no'];?></td>
                                <td>
                                    <b>$ <span style="color:red"><?=number_format($row['total']);?></span></b><br>
                                    <?php if(! empty($row['payment_method_id'])) { ?>
									<span style="font-size: 80%;"><?=$payment_method[$row['payment_method_id']];?></span>
									<?php } ?>
                                </td>
								<td>$ <?=number_format($row['shipping_fee']);?></td>
								<td><?=$row['product_item'];?></td>
								<td><?=$row['create_at'];?></td>
                                <td>
									<?php if(! empty($row['order_status_id'])) { ?>
									<?=$order_status[$row['order_status_id']];?>
									<?php } ?>
								</td>

                                <td class="text-center" id="btn_group">
                                    <a type="button" class="btn btn-outline btn-success btn-xs btn-toggle" title="View" href="<?=$row['link_edit'];?>">
                                        <i class="fa fa-eye fa-lg"></i>
                                    </a>
									
									<a type="button" class="btn btn-outline btn-success btn-xs btn-toggle" title="View" href="<?=$row['link_contact'];?>">
                                        <i class="icon-email"></i>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>

			</div>
			
			<div class="row">
				<div class="col-md-5">
					Showing <?=$filter['offset']+1;?>
					to <?=(($filter['offset']+$filter['rows'])>$filter['total'])?$filter['total']:$filter['offset']+$filter['rows'];?>
					of <?=$filter['total'];?> entries
				</div>
				<div class="col-md-7 text-right">
					<?=$this->pagination->create_links();?>
				</div>
			</div>
			
		</div>
		</div>
	</div>
	<!--/ list_area 首頁內容區塊 ******************************-->

	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

</div>