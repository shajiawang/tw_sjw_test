<SCRIPT language="JavaScript" type="text/javascript">
function check_Name(){
    var contactName = $.trim($('#js_contact_name').val());
    return '' == contactName ? ($('#js_contact_name').parents('.form_group').addClass('form_valid'), !1)  : ($('#js_contact_name').parents('.form_group').removeClass('form_valid'), !0);
}
function check_Email(){
    var contactEMail = $.trim($('#js_contact_email').val()),
    re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return '' == contactEMail ? ($('#js_contact_email').parents('.form_group').addClass('form_valid'), $('#js_contact_email').parent().parent().find('.help_block').html('<i class="icon-exclamation-circle"></i><span>' + Lang.get('frontend.errormsg.isRequired') + '</span>'), !1)  : re.test(contactEMail) ? ($('#js_contact_email').parents('.form_group').removeClass('form_valid'), !0)  : ($('#js_contact_email').parents('.form_group').addClass('form_valid'), $('#js_contact_email').parent().parent().find('.help_block').html('<i class="icon-exclamation-circle"></i><span>' + Lang.get('frontend.errormsg.cart.email.format') + '</span>'), !1);
}
function check_Phone(){
    var contactPhone = $.trim($('#js_contact_phone').val()),
    re = /^[0-9]+$/;
	
	if('' == contactPhone){
		return ($('#js_contact_phone').parents('.form_group').addClass('form_valid'), $('#js_contact_phone').parent().parent().find('.help_block').html('<i class="icon-exclamation-circle"></i><span>' + Lang.get('frontend.errormsg.isRequired') + '</span>'), !1);
	} else {
		return re.test(contactPhone) ? ($('#js_contact_phone').parents('.form_group').removeClass('form_valid'), !0) : ($('#js_contact_phone').parents('.form_group').addClass('form_valid'), !1);
	}
}
function contact_submit()
{
	var checkNameStatus = check_Name();
	var checkEmailStatus = true;
	if($('#js_contact_email').val() !==''){ checkEmailStatus = check_Email(); }
	var checkPhoneStatus = check_Phone();
	
	if(checkNameStatus && checkEmailStatus && checkPhoneStatus){
		$('#form_contact').submit();
	} else {
		<?php //$this->setAlert(4, '欄位格式有誤 !');?>
		alert('欄位格式有誤 !');
	}
}
</SCRIPT>
	
	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- contactus_area 內容區塊 ******************************-->
		<div class="contactus_area clearfix">
			<div class="orderform_area clearboth">
				
				<form id="form_contact" action="<?php echo base_url($this->ctrl_dir);?>" method="post" >
				<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				<input type="hidden" name="user_id" value="<?=$user_id;?>" />
					
					<div class="form_layout form_style shadow_effect_a">
						<header class="title_area clearfix">
							<hgroup class="title_content clearfix">
								<h4 class="main_title">聯絡我們</h4>
								<h5 class="main_sub font_avant_garde">contact us</h5>
							</hgroup>
							<hr class="main_hr">
						</header>
						<div class="row">
							<div class="col-md-2">
								<div class="about_style clearfix">
									<div class="about_media">
										<div class="about_heading"></div>
									</div>
								</div>
								<ul class="contact_info_block list-unstyled hidden-sm hidden-xs clearfix"></ul>
							</div>
							
							<div class="col-md-10">
								<div class="row">
									<div class="col-md-6">
										<div class="form_group">
											<div class="row">
												<div class="col-sm-12">
													<label class="form_label" for="">姓名</label>
												</div>
												<div class="col-sm-12">
													<div class="form_data">
														<input id="js_contact_name" name="buyer_name" value="<?=$buyer_name;?>" class="form_input form_control" placeholder="請輸入姓名" type="text">
														<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
													</div>
													<div class="help_block"><i class="icon-exclamation-circle"></i><span>必填欄位，不得為空白。</span></div>
												</div>
											</div>
										</div>
										<div class="form_group">
											<div class="row">
												<div class="col-sm-12">
													<label class="form_label" for="">電子信箱</label>
												</div>
												<div class="col-sm-12">
													<div class="form_data">
														<input id="js_contact_email" name="buyer_email" value="<?=$buyer_email;?>" class="form_input form_control" placeholder="請輸入電子信箱" type="text">
														<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
													</div>
													<div class="help_block"><i class="icon-exclamation-circle"></i><span>必填欄位，交易成功通知信不得為空白。</span></div>
												</div>
											</div>
										</div>
										<div class="form_group">
											<div class="row">
												<div class="col-sm-12">
													<label class="form_label" for="">聯絡電話</label>
												</div>
												<div class="col-sm-12">
													<div class="form_data">
														<input id="js_contact_phone" name="buyer_phone" value="<?=$buyer_phone;?>" class="form_input form_control" placeholder="請輸入聯絡電話" type="text">
														<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
													</div>
													<div class="help_block"><i class="icon-exclamation-circle"></i><span>聯絡電話只能為數字。</span></div>
												</div>
											</div>
										</div>
										<div class="form_group">
											<div class="row">
												<div class="col-sm-12">
													<label class="form_label" for="">訂單編號</label>
												</div>
												<div class="col-sm-12">
													<div class="form_data">
														<input id="js_order_no" name="order_no" value="<?=$order_no;?>" class="form_input form_control" placeholder="請輸入訂單編號(選填)" type="text">
														<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form_group">
											<div class="row">
												<div class="col-sm-12">
													<label class="form_label" for="">詢問內容</label>
												</div>
												<div class="col-sm-12">
													<div class="form_data">
														<textarea id="js_contact_memo" name="comment" class="form_textarea form_control" placeholder="請輸入您想對店家說的話..." cols="30" rows="5" required=""></textarea>
													</div>
													<div class="help_block"><i class="icon-exclamation-circle"></i><span>必填欄位，不得為空白。</span></div>
												</div>
											</div>
										</div>
										<div class="form_group form_lastmargin">
											<div class="row">
												<div class="col-md-6 col-md-offset-6 col-sm-3 col-sm-offset-9 col-xs-12">
													<div class="form_group form_lastmargin">
														<button type="button" onclick="contact_submit();" id="js_contact" class="btn btn_submit btn_block btn_md"><span>確定送出</span></button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</form>
			</div>
		</div>
		<!--/ contactus_area 內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->
	
	
