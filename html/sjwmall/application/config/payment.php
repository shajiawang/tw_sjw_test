<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * https://cwww.spgateway.com/
 * 1. 測試環境僅接受以下的測試卡號。
 *    4000-2211-1111-1111（（一次付清與分期付款）
 * 2. 測試卡號有效月年及卡片背面末三碼，請任意填寫。
 * 3. 系統在執行測試刷卡後，以測試授權碼回應模擬付款完成。
 * 4. 以測試卡號之外的卡號資料進行交易都會失敗。
 * */
// $config['spgateway'] = array(
    // 'MerchantID' => 'MS1495433',
    // 'HashKey' => '9iJPWeRogIRBFLinusR2Xep68B8DRyR0',
    // 'HashIV' => 'RAQOPIc6r4H6P0uj',
    // 'CheckoutURL' => 'https://ccore.spgateway.com/MPG/mpg_gateway',
// );

$config['spgateway'] = array(
    'MerchantID' => 'MS3523351',
    'HashKey' => 'WvIAK5cwlFuLWvaqBnI5w6lyZwaD3ES0',
    'HashIV' => 'aAAVGMr2sWNpsYP3',
    'CheckoutURL' => 'https://ccore.spgateway.com/MPG/mpg_gateway',
);


/*
 *
 * 智付寶
 * 目前只用在電子發票，不做金流付款
後台
https://cweb.pay2go.com/
1. 測試環境僅接受以下的測試卡號。
4000-2222-2222-2222（一次付清與分期付款）
4000-1322-2222-2222（一次付清與分期付款）
4003-5522-2222-2222（紅利折抵）
2. 測試卡號有效月年及卡片背面末三碼，請任意填寫。
3. 系統在執行測試刷卡後，以測試授權碼回應模擬付款完成。
4. 以測試卡號之外的卡號資料進行交易都會失敗。
5. 銀聯卡交易目前不開放測試。

發票後台
https://cinv.pay2go.com/
 * */
$config['pay2go'] = array(
    'MerchantID' => '32589798',
    'HashKey' => 'w2eqxeR0D4fprxkzWcoQS0tZo7xbGHIc',
    'HashIV' => 'JWChauhOyxeZ5yz1',
    'InvoiceURL' => 'https://cinv.pay2go.com/API/invoice_issue',
    // 'CheckoutURL' => 'https://cinv.pay2go.com/API/invoice_issue',
);
