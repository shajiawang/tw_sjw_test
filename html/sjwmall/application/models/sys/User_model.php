<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model
{
	public $table = 'user';
	public $pk = 'id';
	public $key = 'ud181c679rhb24ce053027ks280a2ocz';  // 加密使用key
	public $user_kind = array();

	public function __construct()
	{
		parent::__construct();

		$this->init($this->table, $this->pk);
		
		$this->user_kind = array(
			'user' => '一般用戶',
			'is_vendor' => '供應廠商',
			'back' => '後台管理'
		);

	}

	//表單欄位預設
	public function getFormDefault($user=array())
	{
		$data = array_merge(array(
			'user_group_id' => 1,
			'name' => '',
			'username' => '',
			'pssword' => '',
			'passconf' => '',
			'email' => '',
			'telephone' => '',
			'enable' => 0,
		), $user);

		return $data;
	}

	//表單欄位驗證
	public function getVerifyConfig()
	{
		$config = array(
			'user_group_id' => array(
				'field' => 'user_group_id',
				'label' => '群組',
				'rules' => 'required',
			),
			'name' => array(
				'field' => 'name',
				'label' => '名稱',
				'rules' => 'trim|required',
			),
			'username' => array(
				'field' => 'username',
				'label' => '帳號',
				'rules' => 'trim|required|min_length[4]|max_length[20]|is_unique[user.username]',
			),
			'password' => array(
				'field' => 'password',
				'label' => '密碼',
				'rules' => 'required|min_length[4]|max_length[20]',
			),
			'passconf' => array(
				'field' => 'passconf',
				'label' => '確認密碼',
				'rules' => 'required|matches[password]',
			),
			'email' => array(
				'field' => 'email',
				'label' => 'E-Mail',
				'rules' => 'trim|valid_email|is_unique[user.email]',
			),
			'telephone' => array(
				'field' => 'telephone',
				'label' => 'Telephone',
				'rules' => 'trim|integer',
			),
			'enable' => array(
				'field' => 'enable',
				'label' => '啟用',
				'rules' => 'required|in_list[0,1]',
				/*
				'messagesd' => array(
					'required'=> 'XXXXX',
					'in_list' => 'sdfsdf'
				),
				*/
			),
		);

		return $config;
	}

	public function getList($attrs=array())
	{
		$groups = $this->user_group_model->getAll();
		$params = array(
			'select' => 'id, kind, user_group_id, name, username, email, telephone, enable, create_at',
			'order_by' => 'create_at',
		);
		if (isset($attrs['conditions'])) {
			$params['conditions'] = $attrs['conditions'];
		}
		if (isset($attrs['rows'])) {
			$params['rows'] = $attrs['rows'];
		}
		if (isset($attrs['offset'])) {
			$params['offset'] = $attrs['offset'];
		}
		if (isset($attrs['sort'])) {
			$params['order_by'] = $attrs['sort'];
		}
		if (isset($attrs['q'])) {
			$params['or_like'] = array(
				'many' => TRUE,
				'data' => array(
					array('field' => 'name', 'value'=>$attrs['q'], 'position'=>'both'),
					array('field' => 'username', 'value'=>$attrs['q'], 'position'=>'both'),
					array('field' => 'email', 'value'=>$attrs['q'], 'position'=>'both'),
					array('field' => 'telephone', 'value'=>$attrs['q'], 'position'=>'both'),
				),
			);
			// unset
		}

		$data = $this->getData($params);
		
		$groups_array = array();
		foreach ($groups as $v){
			$groups_array[$v['id']] = $v['name'];
		}
		
		foreach ($data as & $v_user){
			$v_user['group'] = $this->GroupName($groups_array, $v_user['user_group_id']);
		}
		
		return $data;
	}
	
	public function GroupName($groups=array(), $user_group_id=1 )
	{
		$user_group_array = explode(",", $user_group_id);
		
		foreach ($user_group_array as $gid){
			$_group_name[$gid] = $groups[$gid];
		}
		
		$_user_group = implode(",", $_group_name);
		
		return $_user_group;
	}

	public function getListCount($attrs=array())
	{
		$params = array(
			'conditions' => $attrs['conditions'],
		);

		if (isset($attrs['q'])) {
			$params['q'] = $attrs['q'];
		}
		$data = $this->getList($params);
		return count($data);
	}

	public function getChoices($conditions=array())
	{
		$data = array();
		$users = $this->getList();
		foreach ($users as $user) {
			$data[$user['id']] = $user['name'];
		}

		return $data;
	}
	
	public function getUserName($kind='is_vendor')
	{
		$data = array('null'=>'---');
		$conditions = array(
			'kind' => $kind,
			'enable'=>1,
			'is_admin'=>0
		);
		$params = array(
			'select' => 'username, name',
			'order_by' => 'username',
			'conditions' => $conditions,
		);
		
		$users = $this->getData($params,'array');
		foreach ($users as $user) {
			$data[$user['username']] = $user['name'];
		}

		return $data;
	}

	public function getUser($conditions)
	{
		return $this->get($conditions);
	}
	public function getUserById($id)
	{
		$conditions = array(
			'id' => $id,
		);

		return $this->get($conditions);
	}
	public function getUserByAccount($username)
	{
		$conditions = array(
			'username' => $username,
		);

		return $this->get($conditions);
	}

	public function getUserByEmail($email)
	{
		$conditions = array(
			'email' => $email,
		);

		return $this->get($conditions);
	}

	public function insert($fields=array(), $now_field=NULL)
	{
		if ($fields['password'] != '' && $fields['passconf'] != '' ) {
			$fields['password'] = $this->encrypt($fields['password']);
		} else {
			unset($fields['password']);
		}
		unset($fields['passconf']);

		return parent::insert($fields, 'create_at');
	}

	public function update($conditions=NULL, $fields=array())
	{
		if ($conditions && is_array($conditions)) {
			$this->db->where($conditions);
		} else {
			$this->db->where($this->pk, $conditions);
		}

		if (isset($fields['password']) && $fields['password'] != '') {
			$fields['password'] = $this->encrypt($fields['password']);
		} else {
			unset($fields['password']);
		}

		if (isset($fields['passconf'])) {
			unset($fields['passconf']);
		}

		return parent::update($conditions, $fields);
	}

	public function login($username=NULL, $password=NULL)
	{
		$result = array(
			'status' => FALSE,
			'message' => '請確認帳號、密碼',
			'user' => NULL,
		);
		if ($username && $password)
		{
			$conditions = array(
				'username' => $username,
				'password' => $this->encrypt($password),
			);
			$user = $this->get($conditions);
			if ($user)
			{
				if ($user['enable'] == 1) {
					$user['login_num'] = $user['login_num']+1;
					$user['last_login_at'] = date('Y-m-d H:i:s');
					$user['last_login_ip'] = $this->input->ip_address();
					parent::update($user['id'], $user);
					$result['status'] = TRUE;
					$result['user'] = $user;
					$result['message'] = '帳號巳確認';
				} else {
					$result['message'] = '帳號已停用';
				}
			}
		}

		return $result;
	}

	public function setToken($id)
	{
		$rand = uniqid(rand(), TRUE);
		$token = hash('sha1', md5($rand) . microtime(TRUE));
		$this->update($id, array('token'=>$token));

		return $token;
	}

	// 加密函數
	public function encrypt($str)
	{
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return base64_encode(trim(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key, $str, MCRYPT_MODE_ECB, $iv)));
	}

	// 解密函數
	public function decrypt($str)
	{
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->key, base64_decode($str), MCRYPT_MODE_ECB, $iv));
	}


}

