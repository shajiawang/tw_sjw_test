<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Shipping_method_model extends MY_Model
{
    public $table = 'ec_shipping_method';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'name' => '',
            'fee' => 0,
            'free_shipping' => 0,
            'enable' => 0,
            'sort_order' => 0,
        );

        return array_merge($fields, $data);
    }

    public function getVerifyConfig()
    {
        $config = array(
            'name' => array(
                'field' => "name",
                'label' => "名稱",
                'rules' => 'required',
            ),
            'fee' => array(
                'field' => "fee",
                'label' => "運費",
                'rules' => 'required|is_numeric',
            ),
            'free_shipping' => array(
                'field' => "free_shipping",
                'label' => "運費",
                'rules' => 'required|is_numeric',
            ),
            'sort_order' => array(
                'field' => 'sort_order',
                'label' => '排序',
                'rules' => 'required|integer',
            ),
            'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|in_list[0,1]',
            ),
        );


        return $config;
    }

    public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }

		$data = $this->getData($params);
		
		return $data;
    }

    public function getRowByID($id)
    {
        $default = array(
            'select' => "*",
            'conditions' => array(
				"id" => $id,
            ),
        );

        $data = $this->getData($default);

        return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
        );

        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
        $data = $this->getList($params);
        return count($data);
    }

    public function getChoices()
    {
        $choices = array();
        $data = $this->getList();
        foreach ($data as $row) {
            $choices[$row['id']] = $row['name'];
        }
        return $choices;
    }


}

