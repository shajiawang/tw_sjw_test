<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ticket_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('api');
        $this->api->setUrl(API_URL);
    }

    public function getChoices($conditions=array())
    {
        $data = array();

        $params = array_merge(array(
            'type' => 'choices',
            'show_kid' => 1,
            'online' => 0,
        ), $conditions);
        $response = $this->api->request('ticket/query', $params, 'get');
        if ($response->status == TRUE){
            $data = $response->data;
        }

        return $data;
    }

}

