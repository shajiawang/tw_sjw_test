<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_category extends MY_Controller
{
	public $category_kind = 'goods';
	public function __construct()
	{
		parent::__construct();

		$this->ctrl_dir = 'vendor_category';
		$this->view_dir = 'vendor_category';
		$this->data['allproduct_active'] = 'active';
		
		$this->vid = isset($_SESSION['vid']) ? $_SESSION['vid'] : $this->uri->segments[3];
		$this->_vendor = $this->vendor_model->getUser($this->vid);
		
		if(empty($this->_vendor)){
			redirect('welcome');
		}
		
		if (!isset($this->data['filter']['page'])) {
			$this->data['filter']['page'] = '1';
		}
		if (!isset($this->data['filter']['q'])) { 
			$this->data['filter']['q'] = ''; 
		}
	}
	
	public function index($cid=null)
	{
		$_SESSION['vid'] = $this->vid;
		$this->initCompany($this->vid);
		
		//廠商商品分類
		$this->data['category_menu'] = $this->setVendorCategory($this->_vendor);
		$this->data['cid'] = $cid;
		
		$get_category = $this->goods_category_model->getRowById($cid);
		$this->data['get_category'] = $get_category;
		$this->data['category_image'] = ($get_category['category_photo']) ? $this->data['media_url'] . $get_category['category_photo'] : '';
		
		$this->get_list($this->_vendor['company_id'], $cid);
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view("{$this->view_dir}", $this->data);
	}

	public function get_list($company_id=0, $category=0)
	{
		$this->data['page_name'] = 'list';
	
		$page = $this->data['filter']['page'];
        $rows = $this->data['filter']['rows'];
			
		$table0 = $this->goods_model->table;
		$table1 = $this->company_info_model->table;

		$conditions = array(
			"{$table0}.status !=1" => null,
			'enable' => 1,
			'company_id' => $company_id,
		);
		if(! empty($category)){
			$conditions['vendor_category'] = $category;
		}
		
		$attrs = array('select' => "{$table0}.*",
			'conditions' => $conditions,
			'order_by' => "{$table0}.sort_order",
		);
		
		$on_0 = "{$table0}.company_id={$table1}.id";
		$attrs['join'][0] = array('table'=>$table1, 'on'=>$on_0, 'type'=>'left');
		
		if ($this->data['filter']['q'] !== '' ) { $qv = "%{$this->data['filter']['q']}%";
			$attrs['q'] = array('many' => TRUE
				, 'data' => array(
					array('field' => "{$table0}.name", 'value'=>$qv, 'position'=>'both'),
					array('field' => "{$table0}.no", 'value'=>$qv, 'position'=>'both'),
				)
			);
		}
		
		$this->data['filter']['total'] = $total = $this->goods_model->getListCount($attrs);
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;

		$attrs['rows'] = $rows;
		$attrs['offset'] = $offset;
		
		$this->data['list'] = array();
		$list = $this->goods_model->getList($attrs); //echo $this->goods_model->get_query();
		
		foreach ($list as $k=>$row)
		{
			$this->data['list'][$k] = $row;
			$this->data['list'][$k]['image_url'] = ($row['image']) ? $this->data['media_url'] . $row['image'] : '';
			$this->data['list'][$k]['image_thumb_url'] = ($row['image']) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
			$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
			if ($extension == 'gif') {
				$this->data['list'][$k]['image_thumb_url'] = $row['image_url'];
			}
			
			$this->data['list'][$k]['link_edit'] = base_url("goods/{$row['id']}/{$this->vid}");
		}
		
		$this->load->library('pagination');
		$config['base_url'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $rows;
		$this->pagination->initialize($config);
	}
	
}
