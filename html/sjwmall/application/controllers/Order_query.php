<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_query extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->ctrl_dir = 'order_query';
		$this->view_dir = 'order_query';
		$this->set_layout = 'common/layout_welcome';
		
		$this->data['order_status'] = $this->order_status_model->getChoices(); //echo $this->order_model->get_query();
	}

	public function index()
	{
		$get = $this->input->get();
		if(isset($get['toajax']) && $get['toajax']=='act_cancel')
		{
			if(!empty($get['order_no']) ){
				echo $this->ajax_order_cancel($get['order_no']);
			} else {
				echo '處理中';
			}
		}
		else
		{
			$this->layout->setLayout($this->set_layout);
			$this->layout->view($this->view_dir, $this->data);
		}
	}
	public function ajax_order_cancel($order_no)
	{
		//訂單
		$order_info = $this->order_model->getByOrderNo($order_no);
			
		if(empty($order_info)){
			$_order_status = '無訂單資料 !';
		} 
		else
		{
			//更新訂單記錄
			$_fields['order_status_id'] = 2;
			$this->order_model->update(array('order_no' => $order_no), $_fields);
			
			$_order_status = $this->data['order_status'][6];
		}
		
		return $_order_status;
	}
	
	public function search()
	{
		$post = $this->input->post();
		
		if(empty($post['order_no']) ){
			//導入
			$this->setAlert(4, '無訂單資料 !');
			redirect(base_url($this->ctrl_dir));
		} 
		else
		{
			$this->data['page_name'] = 'list';
		
			//訂單是否存在
			$order_info = $this->order_model->getByOrderNo($post['order_no']);
			
			if($order_info['pin'] !== $post['order_validate_code']){
				//導入
				$this->setAlert(4, '無訂單資料 !');
				redirect(base_url($this->ctrl_dir));
			}
			else
			{
				$this->data['order'] = $order_info;
				$order_status_id = $order_info['order_status_id'];
				$_order_status = isset($this->data['order_status'][$order_status_id]) ? $this->data['order_status'][$order_status_id] : '處理中';
				$this->data['order']['status'] = $_order_status;
			
				//訂單細節
				$this->order_detail($order_info); 
				
				$this->layout->setLayout($this->set_layout);
				$this->layout->view('order_search', $this->data);
			}
		}
	}
	
	private function order_detail($order_info=null)
	{
		$this->data['history'] = $this->data['items'] = array();
		
		if(!empty($order_info) )
		{
			$OrderID = $order_info['id'];
			
			//運送 
			$freight = $this->shipping_method_model->getRowByID($order_info['shipping_method']);
			$this->data['order']['shipping'] = $freight[0]['name'] .', 運費: '. $order_info['shipping_fee'];
			
			//付款
			$payment = $this->payment_method_model->getRowByID($order_info['payment_method_id']);
			$this->data['order']['payment'] = $payment[0]['name'];
			
			$conditions = array('order_id'=>$OrderID);
			$attrs = array(
				'conditions' => $conditions,
				'order_by' => 'id',
			);
			
			//訂單商品
			$list = $this->order_item_model->getList($attrs); //echo $this->order_item_model->get_query();
			
			if(!empty($list))
			foreach($list as $k=>$row)
			{
				$image = $row['image'];
				$image_url = ($image) ? $this->data['media_url'] . $image : '';
				$image_thumb_url = ($image) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $image) : '';
				$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $image);
				if ($extension == 'gif') {
					$image_thumb_url = $image_url;
				}
				$list[$k]['image_url'] = $image_url;
				$list[$k]['image_thumb_url'] = $image_thumb_url;
			}
			$this->data['items'] = $list;
			
		}
	}
	
}
