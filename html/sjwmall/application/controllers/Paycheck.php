<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Paycheck extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->ctrl_dir = 'paycheck';
		$this->view_dir = 'paycheck';
	}

	public function index()
	{
		$this->data['page_name'] = 'paycheck';
		
		//訂單是否存在
		$order_id = $this->data['_cart']['id'];
		$this->order = $this->order_model->getByOrderId($order_id);
		
		if(empty($this->order)){
			$this->setAlert(4, '無訂單資料 !');
			redirect($this->data['home_url']);
		} else {
			$this->data['order'] = $this->order;
			
			//訂單細節
			$this->order_detail($this->order); 
		}
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view('cart_finish', $this->data);
	}
	
	private function order_detail($order_info=null)
	{
		$this->data['history'] = $this->data['items'] = array();
		
		if(!empty($order_info) )
		{
			$orderId = $order_info['id'];
			
			//運送 
			$freight = $this->shipping_method_model->getRowByID($order_info['shipping_method']);
			$this->data['order']['shipping'] = $freight[0]['name'] .', 運費: '. $order_info['shipping_fee'];
			
			//付款
			$payment = $this->payment_method_model->getRowByID($order_info['payment_method_id']);
			$this->data['order']['payment'] = $payment[0]['name'];
			
			$conditions = array('order_id'=>$orderId);
			$attrs = array(
				'conditions' => $conditions,
				'order_by' => 'id',
			);
			
			$message = "您好，恭喜您已經成功訂購商品!<br><br>";
			$message .= "訂單編號：{$order_info['order_no']}<br>訂購日期：{$order_info['create_at']}<br>";
			
			//訂單商品
			$list = $this->order_item_model->getList($attrs); //echo $this->order_item_model->get_query();
			
			if(!empty($list))
			foreach($list as $k=>$row)
			{
				$image = $row['image'];
				$image_url = ($image) ? $this->data['media_url'] . $image : '';
				$image_thumb_url = ($image) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $image) : '';
				$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $image);
				if ($extension == 'gif') {
					$image_thumb_url = $image_url;
				}
				$list[$k]['image_url'] = $image_url;
				$list[$k]['image_thumb_url'] = $image_thumb_url;
				
				$message .= "訂購品項：{$row['name']}<br>";
			}
			$this->data['items'] = $list;
			
			//更新訂單記錄
			$rand = uniqid(rand(), TRUE);
			$hash = hash('sha1', md5($rand) . microtime(TRUE));
			$_fields['pin'] = substr($hash, 0, 8);
			$_fields['sess_id'] = '';
			$_fields['order_status_id'] = 3; //付款確認中
			$this->data['order']['pin'] = $_fields['pin'];
			$this->order_model->update(array('id' => $orderId), $_fields);
			
			$message .= "總計(TWD)：{$order_info['total']}<br><br><hr>";
			$message .= "訂購人：{$order_info['buyer_name']}<br>";
			$message .= "電子信箱：{$order_info['buyer_email']}<br>";
			$message .= "聯絡電話：{$order_info['buyer_phone']}<br>";
			$message .= "備註事項：{$order_info['remark']}<br><br>";
			$message .= "<a href='". base_url('order_query') ."'>「訂單查詢」→輸入</a>「訂單編號：{$order_info['order_no']}」、「認證號碼：{$_fields['pin']}」，即可查詢。";
			
			//$this->send_email($order_info['buyer_email'], "訂購通知 ( {$order_info['order_no']} )", $message);
		}
	}
	
	private function send_email($to, $subject, $message, $attachment = NULL)
	{
		$this->email->from('service@sjmall.com', 'SJMall 訂單系統');
		$this->email->to($to);
		$this->email->subject($subject);
		$this->email->message($message);
		if( !empty($attachment) ){
			$this->email->attach($attachment);
		}

		$result = $this->email->send();
		
		if(!$result){
			error_log($this->email->print_debugger());
		}
		
		return $result;
	}
	
}
