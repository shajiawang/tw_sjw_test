<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor extends MY_Controller
{
	public $category_kind = 'goods';
	public function __construct()
	{
		parent::__construct();

		$this->ctrl_dir = 'vendor';
		$this->view_dir = 'vendor';
		$this->data['home_active'] = 'active';
		
		if (!isset($this->data['filter']['page'])) {
			$this->data['filter']['page'] = '1';
		}
		if (!isset($this->data['filter']['q'])) { 
			$this->data['filter']['q'] = ''; 
		}
		
	}

	public function index($id=null)
	{
		if(!empty($id)){
			$_SESSION['vid'] = $id;
			$this->data['_cart'] = $this->setCart($id);
		}
		$this->initCompany($id);
		
		$this->_vendor = $this->vendor_model->getUser($id); //echo $this->user_model->get_query();

		if(!empty($this->_vendor))
		{
			$this->data['page_name'] = 'list';
			$this->data['list'] = array();
			
			$page = $this->data['filter']['page'];
			$rows = 12;
			
			$this->data = $this->_get_data($page, $rows, $this->_vendor);
			
			$this->layout->setLayout('common/layout_base');
			$this->layout->view('vendor_index', $this->data);
		}
		else
		{
			redirect('welcome');
		}
	}
	
	private function _get_data($page, $rows, $_vendor)
	{
		$table0 = $this->goods_model->table;
		$table1 = $this->company_info_model->table;

		$conditions = array("{$table0}.status !=1" => null
			, 'enable' => 1
			, 'company_id' => $_vendor['company_id']
			, 'show_index' => 1
		);
		$attrs = array('select' => "{$table0}.*",
			'conditions' => $conditions,
			'order_by' => "{$table0}.sort_order",
		);
		
		$on_0 = "{$table0}.company_id={$table1}.id";
		$attrs['join'][0] = array('table'=>$table1, 'on'=>$on_0, 'type'=>'left');
		
		if ($this->data['filter']['q'] !== '' ) { $qv = "%{$this->data['filter']['q']}%";
			$attrs['q'] = array('many' => TRUE
				, 'data' => array(
					array('field' => "{$table0}.name", 'value'=>$qv, 'position'=>'both'),
					array('field' => "{$table0}.no", 'value'=>$qv, 'position'=>'both'),
				)
			);
		}
		
		$this->data['filter']['total'] = $total = $this->goods_model->getListCount($attrs);
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;

		$attrs['rows'] = $rows;
		$attrs['offset'] = $offset;
		
		$list = $this->goods_model->getList($attrs); //echo $this->goods_model->get_query();
		foreach ($list as $k=>$row)
		{
			$this->data['list'][$k] = $row;
			$this->data['list'][$k]['image_url'] = ($row['image']) ? $this->data['media_url'] . $row['image'] : '';
			$this->data['list'][$k]['image_thumb_url'] = ($row['image']) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
			$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
			if ($extension == 'gif') {
				$this->data['list'][$k]['image_thumb_url'] = $row['image_url'];
			}
			
			$this->data['list'][$k]['link_edit'] = base_url("goods/{$row['id']}/{$_SESSION['vid']}");
		}
		
		$this->load->library('pagination');
		$config['base_url'] = base_url("{$this->ctrl_dir}/{$_SESSION['vid']}?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $rows;
		$this->pagination->initialize($config);
		
		return $this->data;
	}
	
}
