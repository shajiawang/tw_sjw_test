<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->ctrl_dir = 'login';
		$this->view_dir = 'login';
		$this->data['home_active'] = 'active';
		
		$this->load->model('sys/login_model');
		$this->load->model('sys/user_blacklist_model');
		
		$this->_vendor = $this->vendor_model->getUser($_SESSION['vid']);
		
		if(empty($this->_vendor)){
			redirect('welcome');
		}
	}
	
	public function index()
	{
		unset($_SESSION['join_error']);
		unset($_SESSION['join_account']);
		unset($_SESSION['token']);
		
		$this->data['username'] = ($this->input->cookie('username')) ? $this->input->cookie('username') : '';
		$this->data['error'] = '';
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view($this->view_dir, $this->data);
	}
	public function login_check()
	{
		$error = '';
		$post = $this->input->post();
		
		if(!empty($post['username']) && !empty($post['password']) )
		{
			setcookie('username', $post['username'], time()+86400*7);  // exist by 7 days
			$this->data['username'] = $post['username'];

			$ip = $this->input->ip_address();
			
			$user_Account = $this->member_model->getUserByAccount($post['username']); 
			$user_Phone = $this->member_model->getUserByPhone($post['username']); 
			$user_data = empty($user_Account) ? $user_Phone : $user_Account;
			
			$error = $this->check_user_black($user_data['account'] ,$ip);

			if(empty($error))
			{
				if ($user_data) {
					$error = $this->check_user_data($user_data, $post['password']);
					
					if(empty($error)) {
						//導入首頁
						redirect(base_url('vendor/'. $_SESSION['vid']) );
					}
				
				} else {
					$this->check_ip_logs($ip);
					$error = '請確認帳號、密碼';
				}
			}
				
		} else {
			$error = '請輸入帳號、密碼';
		}

		$this->setAlert(4, str_replace("\n",'<br>', $error));
		redirect(base_url('login'));
	}
	
	private function check_user_data($user_data, $post_password)
	{
		$vars['username'] = $user_data['account'];
		$vars['password'] = $post_password;
		$vars['user'] = $user_data;
		
		$rs = $this->login_model->chk_login($vars);
		
		if($rs['status']=== TRUE)
		{
			//驗證 token
			//$_token = isset($_SESSION['token']) ? $this->myaes->mcrypt_de($_SESSION['token']) : '';
			//if(empty($_token) || ($user_data['token'] !== $_token) ){ $this->setFlags($rs['user']); }
			//$user_Account = $this->member_model->getUserByAccount($user_data['account']); 
			//$de = $this->myaes->mcrypt_de($_SESSION['token']);
			//var_dump($user_Account['token']);var_dump($de ); exit;
			
			//設定 token
			$this->setFlags($rs['user']);
			$this->flags->web_login = TRUE;
			$this->flags->member = $rs['user'];
				
			unset($_SESSION['login_error']);
			$error = '';
		}
		else
		{
			$this->check_account_logs($vars['username']);
			$error = $rs['message'];
		}
		
		return $error;
	}
	
	private function check_account_logs($user_name)
	{
		$admin_account_logs = array();
		if (isset($_SESSION['admin_account_logs'])) {
			$admin_account_logs = $_SESSION['admin_account_logs'];
		}

		if (isset($admin_account_logs[$user_name]))
		{
			//check login error numbers for account
			if($admin_account_logs[$user_name] >= 5)
			{
				$unlock_time = date('Y-m-d H:i:s', 60*30 + time());
				$this->user_blacklist_model->insertAccount($user_name, $unlock_time);
				unset($admin_account_logs[$user_name]);
			
			} else {
				$admin_account_logs[$user_name] += 1;
			}
			
		} else {
			$admin_account_logs[$user_name] = 1;
		}

		$_SESSION['admin_account_logs'] = $admin_account_logs;
	}
	private function check_ip_logs($ip)
	{
		$admin_ip_logs = array();
		if (isset($_SESSION['admin_ip_logs'])) {
			$admin_ip_logs = $_SESSION['admin_ip_logs'];
		}

		if (isset($admin_ip_logs[$ip]))
		{
			//check login error numbers for IP
			if ($admin_ip_logs[$ip] >= 5)
			{
				$unlock_time = date('Y-m-d H:i:s', 60*30 + time());
				$this->user_blacklist_model->insertIP($ip, $unlock_time);
				unset($admin_ip_logs[$ip]);
			
			} else {
				$admin_ip_logs[$ip] += 1;
			}
		
		} else {
			$admin_ip_logs[$ip] = 1;
		}

		$_SESSION['admin_ip_logs'] = $admin_ip_logs;
	}
	private function check_user_black($username=0, $ip)
	{
		$error = '';
		
		$lock_time = $this->user_blacklist_model->getLockTimeByIP($ip);
		if ($lock_time) {
			$error = "IP:{$ip} 已鎖定！";//<br>將於「{$lock_time}」解鎖
		}

		$lock_time = $this->user_blacklist_model->getLockTimeByAccount($username);
		if ($lock_time) {
			$error = "帳戶:{$username} 已鎖定！";//<br>將於「{$lock_time}」解鎖
		}	
		
		return $error;
	}
	
	
	///// 註冊新用戶 ////////////////////
	public function joinus()
	{
		unset($_SESSION['is_forgot']);
		unset($_SESSION['login_error']);
		unset($_SESSION['join_account']);
		
		//用戶類別 $this->data['user_kind'] = strtoupper($this->_vendor['kind']);
		
		$this->data['phone'] = ($this->input->cookie('phone')) ? $this->input->cookie('phone') : '';
		$this->data['email'] = ($this->input->cookie('email')) ? $this->input->cookie('email') : '';
		$this->data['error'] = isset($_SESSION['join_error']) ? $_SESSION['join_error'] : '';
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view('joinus', $this->data);
	}
	public function join_check()
	{
		$error = '';
		$post = $this->input->post();
		
		if (!empty($post['phone']) && !empty($post['password']) && !empty($post['email']) )
		{
			setcookie('phone', $post['phone'], time()+3600*1);
			setcookie('email', $post['email'], time()+3600*1);
			$this->data['phone'] = $post['phone'];
			$this->data['email'] = $post['email'];
			
			//會員一個手機, 限一個帳號
			$ChoicesPhone = array();
			$conditions = array(//'kind' => 'mall',
				'enable' => 2,
				'telephone' => $post['phone'],
			);
			$ChoicesPhone = $this->member_model->getChoicesRows($conditions); //echo $this->member_model->get_query(); exit;
			
			$verify = $this->_isVerify();
			
			if (! empty($ChoicesPhone)) {
				$error = '資料錯誤, 手機已存在 !';
			} 
			elseif ($verify['status'] === TRUE) 
			{
					//建立用戶帳號
					$rs = $this->login_model->chk_join($post); 
					
					if (! $rs['status']) {
						$error = $rs['message'];
					} else {
						unset($_SESSION['token']);
						unset($_SESSION['join_error']);
						//導入完成頁 redirect(base_url('login/join_sign'));
						
						redirect(base_url('login/join_ok'));
					}
			} else {
				$error = $verify['error'];
            }
			
		} else {
			$error = '輸入資料錯誤!';
		}
		
		$this->setAlert(4, str_replace("\n",'<br>', $error));
		redirect(base_url('login/joinus'));
	}
	public function join_ok()
	{
		//會員帳號
		$this->data['member'] = $this->member_model->getMemberInfo($_SESSION['join_account']);
		//$this->data['member']['url'] = base_url("{$_SESSION['site_kind']}/{$this->data['member']['account']}");
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view('join_ok', $this->data);
	}
	public function join_sign()
	{
		//數位簽章
		$this->data['sign_key'] = $this->myaes->sign_key();
		$this->data['error'] = isset($_SESSION['join_error']) ? $_SESSION['join_error'] : '';
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view('join_sign', $this->data);
	}
	public function join_sign_check()
	{
		$error = '';
		$post = $this->input->post(); 
		
		if (!empty($post['password']) && empty($error) )
		{
			$rs = $this->login_model->chk_sign($post['password']);
			
			if (! $rs['status']) {
				$error = $rs['message'];
			} else {
				unset($_SESSION['join_error']);
				
				//導入完成頁 
				if(isset($_SESSION['is_forgot'])){
					$_url = 'login';
				} else {
					$_url = 'login/join_ok';
				}
				
				redirect(base_url($_url));
			}
		
		} else {
			$error = '輸入資料錯誤!';
		}

		$_SESSION['join_error'] = $error;
		redirect(base_url('login/join_sign'));
	}
	
	///// 用戶登出 ////////////////////
	public function logout()
	{
		$vendor_id = $_SESSION['vid'];
		
		session_unset();
		
		redirect(base_url('vendor/'. $vendor_id) );
	}
	
	
	///// 忘記密碼 ////////////////////
	public function forgot()
	{
		unset($_SESSION['login_error']);
		unset($_SESSION['join_account']);
		$this->data['error'] = isset($_SESSION['forgot_error']) ? $_SESSION['forgot_error'] : '';
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view('login_forgot', $this->data);
	}
	public function forgot_check()
	{
		$error = '';
		$post = $this->input->post(); 
		
		if (!empty($post['phone']) && !empty($post['private_key']) && empty($error) )
		{
			$ip = $this->input->ip_address();
			$error = $this->check_user_black($this->site_id ,$ip);
			
			//會員帳號
			$user_data = $this->member_model->getUserByAccount($this->site_id);
			if(empty($user_data))
			{
				$error = '用戶帳戶資料錯誤 !';
			}
			else
			{
				$len = strlen($post['private_key']);
				if($len<6 || $len>20){
					$error = '私鑰字符規格錯誤 !';
				}
				else
				{
					$rs = $this->login_model->login_tel_private($post, $user_data);
					if(! $rs['status']){
						$error = $rs['message'];
					}
					
					if(empty($error))
					{
						unset($_SESSION['token']);
						unset($_SESSION['forgot_error']);
						$_SESSION['join_account'] = $this->site_id;
						$_SESSION['is_forgot'] = TRUE;
						
						//導入完成頁 
						redirect(base_url('login/join_sign'));
						
					} else {
						$this->check_account_logs($this->site_id);
						$this->check_ip_logs($ip);
					}
				}
			}
			
		} else {
			$error = '輸入資料錯誤!';
		}

		$_SESSION['forgot_error'] = $error;
		redirect(base_url('login/forgot'));
	}
	
	
	private function _isVerify()
    {
		$result = array(
            'status' => FALSE,
            'error' => '',
        );

        $this->load->library('form_validation');
		
        $this->form_validation->set_rules('phone', '手機號碼', 'trim|required|numeric');
		$this->form_validation->set_rules('email', 'E-Mail', "trim|required|valid_email");
		$this->form_validation->set_rules('password', '密碼', "min_length[6]|max_length[20]");
		$this->form_validation->set_rules('passconf', '確認密碼', "matches[password]");
		
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
		$result['status'] = $this->form_validation->run();
        if ($result['status'] == FALSE) {
            $result['error'] = validation_errors();
        }

        return $result;
        
    }
	
}
