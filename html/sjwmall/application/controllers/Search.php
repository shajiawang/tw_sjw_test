<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Search extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->data['allproduct_active'] = '';
		$this->data['searched'] = 0;
	}

	public function index()
	{
		$_keyword = $this->input->get('keyword');
		if($_keyword=='蛋糕'){
			$this->data['searched'] = 1;
		}
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view('search', $this->data);
	}
	
}
