<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_vendor extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->ctrl_dir = 'contact_vendor';
		$this->view_dir = 'contact';
		$this->data['member_active'] = 'active';
		
		$this->load->model('sales/contact_model');
		
		$this->_vendor = $this->vendor_model->getUser($_SESSION['vid']);
		
		if(empty($this->_vendor)){
			redirect('welcome');
		}
		
		if(! $this->flags->web_login){
			redirect($this->data['home_url']);
		}
		
		if (empty($this->data['filter']['odno'])) {
            $this->data['filter']['odno'] = '';
        }
		if (!isset($this->data['filter']['page'])) {
			$this->data['filter']['page'] = '1';
		}
		if (!isset($this->data['filter']['q'])) { 
			$this->data['filter']['q'] = ''; 
		}
	}

	public function index()
	{
		$post = $this->input->post();
		if (isset($_SESSION['vid']) && $post)
		{
			if ($this->_isVerify('add') == TRUE)
			{
				$post['create_at'] = date('Y-m-d H:i:s', time());
				$post['vendor_id'] = $_SESSION['vid'];
				
				$saved_id = $this->contact_model->insert($post); //echo $this->contact_model->get_query();
				if ($saved_id){
					$this->setAlert(1, '資料新增成功');
				}
				redirect(base_url($this->ctrl_dir));
			}
		}
		
		$this->get_member();
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view($this->view_dir, $this->data);
	}
	
	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->contact_model->getVerifyConfig();

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}
	
	public function get_member()
	{
		$this->data['user_id'] = $this->flags->member['id'];
		$this->data['buyer_name'] = $this->flags->member['name'];
		$this->data['buyer_email'] = $this->flags->member['email'];
		$this->data['buyer_phone'] = $this->flags->member['telephone'];
		$this->data['order_no'] = $this->data['filter']['odno'];
	}
	
	public function get_list()
	{
		/*
		$this->data['page_name'] = 'list';
	
		$page = $this->data['filter']['page'];
        $rows = $this->data['filter']['rows'];
			
		$uid = $this->flags->member['id'];
		
		$conditions = array('user_id' => $uid);
		
		$attrs = array('conditions' => $conditions,
			'order_by' => "create_at desc",
		);
		
		$this->data['filter']['total'] = $total = $this->contact_model->getListCount($attrs); 
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;

		$attrs['rows'] = $rows;
		$attrs['offset'] = $offset;
		
		if ($this->data['filter']['sort'] !== '' ) {
            $attrs['sort'] = $this->data['filter']['sort'];
        }
		
		$this->data['list'] = array();
		$list = $this->contact_model->getList($attrs); //echo $this->contact_model->get_query();
		*/
		
		/*
		 ["id"]=>
    string(1) "1"
    ["vendor_id"]=>
    string(3) "917"
    ["order_no"]=>
    string(0) ""
    ["buyer_name"]=>
    string(0) ""
    ["buyer_email"]=>
    string(0) ""
    ["buyer_phone"]=>
    string(0) ""
    ["comment"]=>
    string(4) "test"
		*/
		/*
		foreach ($list as $k=>$row)
		{
			$this->data['list'][$k] = $row;
			$order_no = $row['order_no'];//var_dump($order_no);
			$order_url = empty($order_no) ? '' : base_url("{$this->ctrl_dir}/search?id={$row['id']}&back={$this->router->method}");
			//$this->data['list'][$k]['link_edit'] = base_url("{$this->ctrl_dir}/search?id={$row['id']}&back={$this->router->method}");
		}
		
		$this->load->library('pagination');
		$config['base_url'] = base_url("{$this->ctrl_dir}?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $rows;
		$this->pagination->initialize($config);
		*/
	}
	
	
	
}

