<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_mem extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->ctrl_dir = 'order_mem';
		$this->view_dir = 'order_mem';
		$this->data['member_active'] = 'active';
		$this->set_layout = 'common/layout_welcome';
		
		if(! $this->flags->web_login){
			redirect('welcome');
		}
		
		$this->data['order_status'] = $this->order_status_model->getChoices();
        $this->data['payment_method'] = $this->payment_method_model->getChoices(); 
		
		if (empty($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = 'create_at desc';
        }
		if (!isset($this->data['filter']['page'])) {
			$this->data['filter']['page'] = '1';
		}
		if (!isset($this->data['filter']['q'])) { 
			$this->data['filter']['q'] = ''; 
		}
	}
	
	public function paycheck()
	{
		$conditions = array("order_status_id" => 3);
		$this->get_list($conditions, 'paycheck');
		
		$this->layout->setLayout($this->set_layout);
		$this->layout->view("{$this->view_dir}", $this->data);
	}
	public function freight()
	{
		$conditions = array("order_status_id in(4,5,7,8,9)" => null);
		$this->get_list($conditions, 'freight');
		
		$this->layout->setLayout($this->set_layout);
		$this->layout->view("{$this->view_dir}", $this->data);
	}
	public function finish()
	{
		$conditions = array("order_status_id" => 10);
		$this->get_list($conditions, 'finish');
		
		$this->layout->setLayout($this->set_layout);
		$this->layout->view("{$this->view_dir}", $this->data);
	}
	public function cancel()
	{
		$conditions = array("order_status_id in(2,6)" => null);
		$this->get_list($conditions, 'cancel');
		
		$this->layout->setLayout($this->set_layout);
		$this->layout->view("{$this->view_dir}", $this->data);
	}
	
	public function get_list($conditions, $act)
	{
		$this->data['page_name'] = 'list';
	
		$page = $this->data['filter']['page'];
        $rows = $this->data['filter']['rows'];
		
		$conditions['user_id'] = $this->flags->member['id'];
		$attrs = array('conditions' => $conditions,
			'order_by' => "create_at desc",
		);
		
		$this->data['filter']['total'] = $total = $this->order_model->getListCount($attrs);
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;

		$attrs['rows'] = $rows;
		$attrs['offset'] = $offset;
		
		if ($this->data['filter']['sort'] !== '' ) {
            $attrs['sort'] = $this->data['filter']['sort'];
        }
		
		$this->data['list'] = array();
		$list = $this->order_model->getList($attrs); //echo $this->order_model->get_query();
		foreach ($list as $k=>$row)
		{
			$this->data['list'][$k] = $row;
			$this->data['list'][$k]['product_item'] = $this->get_product_item($row['id']);
			$this->data['list'][$k]['link_edit'] = base_url("{$this->ctrl_dir}/search?id={$row['id']}&back={$this->router->method}");
			$this->data['list'][$k]['link_contact'] = base_url("contact_vendor?odno=". $row['order_no']);
		}
		
		$this->load->library('pagination');
		$config['base_url'] = base_url("{$this->ctrl_dir}/{$act}?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $rows;
		$this->pagination->initialize($config);
	}
	
	private function get_product_item($order_id)
	{
		$conditions = array('order_id'=>$order_id);
        $products = $this->order_item_model->getCount($conditions);
		$product_item = empty($products) ? 0 : $products;
		
		return $product_item;
	}
	
	
	public function do_ajax()
	{
		$get = $this->input->get();
		if(isset($get['toajax']) && $get['toajax']=='act_cancel')
		{
			if(!empty($get['order_no']) ){
				echo $this->ajax_order_cancel($get['order_no']);
			} else {
				echo '處理中';
			}
		}
		
	}
	private function ajax_order_cancel($order_no)
	{
		//訂單
		$order_info = $this->order_model->getByOrderNo($order_no);
			
		if(empty($order_info)){
			$_order_status = '無訂單資料 !';
		} 
		else
		{
			//更新訂單記錄
			$_fields['order_status_id'] = 2;
			$this->order_model->update(array('order_no' => $order_no), $_fields);
			
			$_order_status = $this->data['order_status'][6];
		}
		
		return $_order_status;
	}
	
	public function search()
	{
		$get = $this->input->get();
		
		//訂單是否存在
		$order_info = $this->order_model->getByOrderId($get['id']);
		$this->data['back_url'] = $this->ctrl_dir .'/'. $get['back'];
		
		if(empty($order_info))
		{
			//導入
			$this->setAlert(4, '無訂單資料 !');
			redirect(base_url($this->ctrl_dir) .'/'. $get['back']);
		}
		else
		{
			$this->data['order'] = $order_info;
			$order_status_id = $order_info['order_status_id'];
			
			$this->data['js_order_cancel'] = false;
			if($order_status_id=='1' || $order_status_id=='3'){
				$this->data['js_order_cancel'] = true;
			}
			
			$_order_status = isset($this->data['order_status'][$order_status_id]) ? $this->data['order_status'][$order_status_id] : '處理中';
			$this->data['order']['status'] = $_order_status;
		
			//訂單細節
			$this->order_detail($order_info); 
			
			$this->layout->setLayout($this->set_layout);
			$this->layout->view('order_mem_search', $this->data);
		}
	}
	
	private function order_detail($order_info=null)
	{
		$this->data['history'] = $this->data['items'] = array();
		
		if(!empty($order_info) )
		{
			$OrderID = $order_info['id'];
			
			//運送 
			$freight = $this->shipping_method_model->getRowByID($order_info['shipping_method']);
			$this->data['order']['shipping'] = empty($freight) ? '' : $freight[0]['name'] .', 運費: '. $order_info['shipping_fee'];
			
			//付款
			$payment = $this->payment_method_model->getRowByID($order_info['payment_method_id']);
			$this->data['order']['payment'] = empty($payment) ? '' : $payment[0]['name'];
			
			$conditions = array('order_id'=>$OrderID);
			$attrs = array(
				'conditions' => $conditions,
				'order_by' => 'id',
			);
			
			//訂單商品
			$list = $this->order_item_model->getList($attrs); //echo $this->order_model->get_query();
			
			if(!empty($list))
			foreach($list as $k=>$row)
			{
				$image = $row['image'];
				$image_url = ($image) ? $this->data['media_url'] . $image : '';
				$image_thumb_url = ($image) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $image) : '';
				$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $image);
				if ($extension == 'gif') {
					$image_thumb_url = $image_url;
				}
				$list[$k]['image_url'] = $image_url;
				$list[$k]['image_thumb_url'] = $image_thumb_url;
			}
			$this->data['items'] = $list;
			
		}
	}
	
}
