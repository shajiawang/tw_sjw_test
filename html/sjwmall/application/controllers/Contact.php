<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->ctrl_dir = 'contact';
		$this->view_dir = 'contact';
		
		$this->load->model('sales/contact_model');
		
		$this->_vendor = $this->vendor_model->getUser($_SESSION['vid']);
		
		if(empty($this->_vendor)){
			redirect('welcome');
		}
	}

	public function index()
	{
		$post = $this->input->post();
		if (isset($_SESSION['vid']) && $post)
		{
			if ($this->_isVerify('add') == TRUE)
			{
				$post['create_at'] = date('Y-m-d H:i:s', time());
				$post['vendor_id'] = $_SESSION['vid'];
				
				$saved_id = $this->contact_model->insert($post); //echo $this->contact_model->get_query();
				if ($saved_id){
					$this->setAlert(1, '資料新增成功');
				}
				redirect(base_url($this->ctrl_dir));
			}
		}
		
		$this->layout->setLayout('common/layout_welcome');
		$this->layout->view($this->view_dir, $this->data);
	}
	
	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->contact_model->getVerifyConfig();

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}
	
}

