<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Info extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->web_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->data['choices']['group'] = $this->user_group_model->getUserGroup();
		$this->data['choices']['user_id'] = $this->user_model->getUserName();

		if (empty($this->data['filter']['user_group_id'])) {
			$this->data['filter']['user_group_id'] = 'all';
		}

        if (!isset($this->data['filter']['page'])) {
            $this->data['filter']['page'] = '1';
        }
        if (!isset($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = '';
        }
        if (!isset($this->data['filter']['q'])) {
            $this->data['filter']['q'] = '';
        }
		
		//允許改密碼
		$this->data['chg_pwd'] = FALSE;
		
	}
	
	public function info_form_rule($tablist, $form_field)
	{
		foreach ($tablist as $k => $v) 
		{
			foreach ($form_field[$k] as $_field=>$row)
			{
				$_rows[$k][$_field] = $row;
				
				//依權限調整欄位
				if($_field=='sales_id' || $_field=='maintain_name'){
					$_rows[$k][$_field]['status'] = ($this->flags->member['is_admin']==1) ? 1 : 2;
				}
				
				if($_field=='memo'){
					$_rows[$k][$_field]['status'] = ($this->flags->member['kind']=='tlback') ? 1 : 2;
					$_rows[$k][$_field]['show'] = ($this->flags->member['kind']=='tlback') ? 1 : 0;
				}
			}
			
			//只顯示 ['show']=1 欄位
			foreach ($_rows[$k] as $_k=>$_row)
			{
				if($_row['show']==1){
					$rows[$k][$_k] = $_row;
					$rows[$k][$_k]['disabled'] = ($_row['status']==2) ? 'disabled' : '';
				}
			}
		}
		
		return $rows;
	}
	
	public function index($id=NULL)
	{
		$this->data['page_name'] = 'info';
		
		$_get = $this->input->get();
		$id = $_get['id'];
		
		$get_user = $this->user_model->get($id);
		
		$this->data['company_info'] = $this->company_info_model->getRowByAccount($get_user['username']);

		//表單頁籤
		$this->data['tablist'] = $this->company_info_model->getTabList();
		
		//表單欄位陣列
		$form_field = $this->company_info_model->getFormConfig();
		$this->data['form_field'] = $this->info_form_rule($this->data['tablist'], $form_field);

		//表單欄位選項
		$option_vars['exp_category'] = $this->schedule_category_model->getChoices();
		$this->data['form_options'] = $this->company_info_model->getOptions($option_vars);
		$this->data['form_options']['sales_id'] = $this->data['choices']['user_id'];
		$this->data['form_options']['maintain_name'] = $this->data['choices']['user_id'];

		if ($post = $this->input->post())
		{
			$old_data = $this->data['company_info'];
			
			if ($this->_isVerify('info', $old_data) == TRUE)
			{
				$company_id = $this->data['company_info']['id'];
				$post['update_at'] = date('Y-m-d H:i:s', time() );
				$post['user_id'] = $this->flags->member['username'];
				
				$rs = $this->company_info_model->_update($company_id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				
				redirect(base_url("info/?". $this->getQueryString()));
			}
		}
		
		$this->data['form'] = $this->user_model->getFormDefault($this->user_model->get($id));

		$this->data['link_save'] = base_url("info/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("info/?". $this->getQueryString());
		$this->layout->view('sys/user/info', $this->data);
	}

	private function _isVerify($action='info', $old_data=array())
	{
		$config = $this->user_model->getVerifyConfig();
		$config = $this->company_info_model->getVerifyConfig();
		
		if ($old_data['contractor_email'] == $this->input->post('contractor_email')) {
			$config['contractor_email']['rules'] = 'valid_email';
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}

	public function ajax_toggle($field)
	{
		$result = array(
			'status' => FALSE,
			'message' => '',
		);

		if ($post = $this->input->post()) {
			$rs = $this->user_model->update($post['pk'], array($field=>$post['value']));
			if ($rs) {
				$result['status'] = TRUE;
			}
		}

		echo json_encode($result);
	}

}
