<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->flags->web_login === FALSE) {
            redirect(base_url('welcome'));
        }

        $this->load->library('form_validation');
    }

    public function index()
    {
        $this->data['page_name'] = 'profile';
        $this->data['form'] = $this->user_model->get($this->flags->member['id']);

        $this->data['choices']['group'] = $this->user_group_model->getChoices();

        if ($post = $this->input->post()) {
            $old_data = $this->user_model->get($this->flags->member['id']);
            if ($this->_isVerify($old_data) == TRUE) {
                $rs = $this->user_model->update($this->flags->member['id'], $post);
                if ($rs) {
                    $this->setAlert(1, '資料編輯成功');
                }
                redirect(base_url('profile'));
            }
        }

        $this->data['choices_group'] = $this->user_group_model->getChoices();

        $this->data['link_save'] = base_url('profile/');
        $this->data['link_cancel'] = base_url('sys/user/');
        $this->layout->view('profile', $this->data);
    }

    private function _isVerify($old_data)
    {
        $config = $this->user_model->getVerifyConfig();
        $config['user_group_id']['rules'] = '';
        $config['username']['rules'] = '';
        $config['password']['rules'] = 'min_length[4]|max_length[20]';
        $config['passconf']['rules'] = 'matches[password]';

        if ($old_data['email'] == $this->input->post('email')) {
            $config['email']['rules'] = 'trim|valid_email';
        }

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        // $this->form_validation->set_message('required', '請勿空白');

        return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
    }
}
