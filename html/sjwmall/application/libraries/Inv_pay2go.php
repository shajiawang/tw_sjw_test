<?php
class Inv_pay2go
{
	private $CI;
	private $MerchantID;
	private $HashKey;
	private $HashIV;
	private $InvoiceURL;

	// public function __construct()
	// {
	// }

	public function setConfig($params=array())
	{
        if (isset($params['MerchantID'])) {
            $this->MerchantID = $params['MerchantID'];
        } else {
            die('MerchantID 未定義');
        }
        if (isset($params['HashKey'])) {
            $this->HashKey = $params['HashKey'];
        } else {
            die('HashKey 未定義');
        }
        if (isset($params['HashIV'])) {
            $this->HashIV = $params['HashIV'];
        } else {
            die('HashIV 未定義');
        }
        if (isset($params['InvoiceURL'])) {
            $this->InvoiceURL = $params['InvoiceURL'];
        } else {
            die('InvoiceURL 未定義');
        }
		return $this;
	}

    public function create($params=array())
    {
        $tax_rate = 5;
        $tax_amt = round($params['amount'] * ($tax_rate / 100));
        $total = $params['amount'];
        $amount = round($total / (($tax_rate / 100) + 1));
        while ($amount+$tax_amt != $total) {
            $tax_amt--;
        }

        $post_data_arr = array(//post_data 欄位資料
            "RespondType" => "JSON",
            "Version" => "1.3",
            "TimeStamp" => time(),
            "TransNum" => isset($params['trans_no'])? $params['trans_no'] : '',
            "MerchantOrderNo" => $params['order_no'],
            "BuyerName" => isset($params['buyer_name'])? $params['buyer_name'] : '',
            "BuyerUBN" => isset($params['buyer_ubn'])? $params['buyer_ubn'] : '',
            "BuyerAddress" => isset($params['buyer_addr'])? $params['buyer_addr'] : '',
            "BuyerPhone" => isset($params['buyer_phone'])? $params['buyer_phone'] : '',
            "BuyerEmail" => isset($params['buyer_email'])? $params['buyer_email'] : '',
            "Category" => "B2C",  // B2B, B2C
            "TaxType" => "1",  // 稅別 1=應稅 2=零稅率 3=免稅 9=混合稅率(混合應稅與免稅或零稅率)
            "TaxRate" => $tax_rate,  // 稅率
            "Amt" => $amount,
            "TaxAmt" => $tax_amt,
            "TotalAmt" => $total,
            // "CarrierType" => "",
            // "CarrierNum" => rawurlencode(""),
            // "LoveCode" => "",
            "PrintFlag" => "Y",
            "ItemName" => $params['item_name'], // 多項商品時，以「 | 」分開
            "ItemCount" => $params['item_count'], // 多項商品時，以「 | 」分開
            "ItemUnit" => $params['item_unit'], // 多項商品時，以「 | 」分開
            "ItemPrice" => $params['item_price'], // 多項商品時，以「 | 」分開
            "ItemAmt" => $params['item_subtotal'], // 多項商品時，以「 | 」分開
            "Comment" => $params['comment'],
            "Status" => "1", //1= 立即開立， 0= 待開立， 3= 延遲開立
            "NotifyEmail" => "1", //1= 通知， 0= 不通知
        );
        $post_data_str = http_build_query($post_data_arr);  // 轉成字串排列
        $post_data = trim(bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->HashKey, $this->addpadding($post_data_str), MCRYPT_MODE_CBC, $this->HashIV)));
        $data = array(
            'MerchantID_' => $this->MerchantID,
            'PostData_' => $post_data,
        );
        $transaction_data_str = http_build_query($data);
        $response = $this->curl($this->InvoiceURL, $transaction_data_str);  // 背景送出
        $result = $this->result($response);
        if ($result) {
            $result['tax_amt'] = $tax_amt;
            $result['amt'] = $amount;
        }

        return $result;
	}

    public function search($order_no, $amount)
    {

        $post_data_arr = array(//post_data 欄位資料
            "RespondType" => "JSON",
            "Version" => "1.1",
            "TimeStamp" => time(),
            "SearchType" => '1',
            "MerchantOrderNo" => $order_no,
            'TotalAmt' => $amount,
            'InvoiceNumber' => '',
            'RandomNum' => '',
        );

        $post_data_str = http_build_query($post_data_arr);  // 轉成字串排列
        $post_data = trim(bin2hex(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->HashKey, $this->addpadding($post_data_str), MCRYPT_MODE_CBC, $this->HashIV)));
        $data = array(
            'MerchantID_' => $this->MerchantID,
            'PostData_' => $post_data,
        );
        $transaction_data_str = http_build_query($data);
        $response = $this->curl($this->InvoiceURL, $transaction_data_str);  // 背景送出
        $result = $this->result($response);
        jd($result1);
        // if ($result) {
            // $result['tax_amt'] = $tax_amt;
            // $result['amt'] = $amount;
        // }
    }

	private function result($response)
	{
		$result = array(
			'status' => FALSE,
            'message' => '',
            'response' => $response,
        );

        $data = json_decode($response);
        $detail = json_decode($data->Result);
        if ($data->Status == 'SUCCESS') {
            if (!$this->verifyCheckCode($detail->CheckCode, $detail->InvoiceTransNo, $detail->MerchantOrderNo, $detail->RandomNum, $detail->TotalAmt)) {
                $result['message'] = 'verify check code error';
            } else {
                $result['status'] = TRUE;
                $result['invoice_no'] = $detail->InvoiceNumber;
                $result['random_no'] = $detail->RandomNum;
            }
        } else {
            $result['message'] = "{$data->Status} {$data->Message}";
        }

        return $result;
	}

	private function verifyCheckCode($code, $invoice_no, $order_no, $rand_num, $amt)
	{
        $check_code = array(
            'MerchantID' => $this->MerchantID,
            "MerchantOrderNo" => $order_no,
            'InvoiceTransNo' => $invoice_no,
            "TotalAmt" => $amt,
            "RandomNum" => $rand_num,
        );
        ksort($check_code);
        $check_str = http_build_query($check_code);
        $CheckCode = "HashIV={$this->HashIV}&{$check_str}&HashKey={$this->HashKey}";
        $CheckCode = strtoupper(hash("sha256", $CheckCode));
		return ($code == $CheckCode);
	}

    private function addpadding($string, $blocksize = 32) {
            $len = strlen($string);
            $pad = $blocksize - ($len % $blocksize);
            $string .= str_repeat(chr($pad), $pad);
            return $string;
    }

    private function curl($url = "", $parameter = "")
    {
        $curl_options = array(
                CURLOPT_URL => $url,
                CURLOPT_HEADER => false,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERAGENT => "Google Bot",
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYPEER => FALSE,
                CURLOPT_SSL_VERIFYHOST => FALSE,
                CURLOPT_POST => "1",
                CURLOPT_POSTFIELDS => $parameter
        );
        $ch = curl_init();
        curl_setopt_array($ch, $curl_options);
        $result = curl_exec($ch);
        $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $curl_error = curl_errno($ch);
        curl_close($ch);
        $return_info = array(
                "url" => $url,
                "sent_parameter" => $parameter,
                "http_status" => $retcode,
                "curl_error_no" => $curl_error,
                "response" => $result,
        );
        // jd($return_info,1);
        // return $return_info;
        return $result;
    }
}

