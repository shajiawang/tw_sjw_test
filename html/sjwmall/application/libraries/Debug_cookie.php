<?php
class Debug_cookie {
	private $CI;
	private $COOKIES;

	public function __construct($api_url = NULL) {
		$this->CI = & get_instance();
		$this->CI->load->helper('cookie');
		$this->COOKIES = array();
	}
	
	public function params_to_cookie() {
		$CI = & $this->CI;
		
		// debug_cookie=fake_ip:10.1.1.1, force_ab:xxxxx_adfa
		$i_debug_cookie = $CI->input->get('debug_cookie');
		
		$cookies = & $this->COOKIES;
		$params = explode(',', $i_debug_cookie);
		foreach ($params as & $_arg) {
			if (! preg_match('/(.+):(.*)/', $_arg, $matches)) {
				continue;
			}
			$key = $matches[1];
			$value = isset($matches[2]) ? $matches[2] : '';
			
			if (strtolower($value) == 'null') {
				$value = NULL;
				$this->delete_cookie($key);
			}
			
			$cookies[$key] = $value;
		}
		
		$this->set_cookie($cookies);
		
		return;
	}
	
	public function set_cookie(& $cookies) {
		if (is_array($cookies)) {
			foreach ($cookies as $key => $value) {
				if (is_null($value)) {
					continue;
				}
				// $this->input->set_cookie($name, $value, $expire, $domain, $path, $prefix, $secure);
				$this->CI->input->set_cookie($key, $value, time() + 86400, '', '/');
				setcookie($key, $value, time() + 86400, '/');
			}
		}
		
		return;
	}
	
	public function get_cookie($key) {
		$cookies = & $this->COOKIES;
		$return = (array_key_exists($key, $cookies)) ? $cookies[$key] : get_cookie($key);
		return (is_null($return)) ? FALSE : $return;
	}
	
	public function delete_cookie($key) {
		// $this->CI->input->set_cookie($key, '', time() - 86400, '', '/');
		// set_cookie($key, $value, time() - 86400, '', '/');
		// setcookie($key, $value, time() - 86400, '/');
		return delete_cookie($key);
	}
	
}
