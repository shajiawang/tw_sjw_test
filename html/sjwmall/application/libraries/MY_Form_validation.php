<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation {

    public function __construct($config=array())
    {
        parent::__construct($config);

        $this->set_message('valid_date', '%s 日期格式错误');
        $this->set_message('valid_datetime', '%s 日期时间格式错误');
    }

    /**
     * Validate Date
     *
     * @param   string
     * @return  bool
     */
    public function valid_date($date)
    {
        if (preg_match("/^\d{4}-\d{2}-\d{2}$/s", $date)) {
            $y = substr($date, 0, 4);
            $m = substr($date, 5, 2);
            $d = substr($date, 8, 2);

            if(checkdate($m, $d, $y)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    /**
     * Validate Date
     *
     * @param   string
     * @return  bool
     */
    public function valid_datetime($date)
    {
        if (preg_match("/^\d{4}-\d{2}-\d{2}\ \d{2}:\d{2}$/s", $date)) {
            $y = substr($date, 0, 4);
            $m = substr($date, 5, 2);
            $d = substr($date, 8, 2);

            if(checkdate($m, $d, $y)) {
                return TRUE;
            }
        }

        return FALSE;
    }


}
