<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {

	protected $table;
	protected $pk;
	public	$aa ='aa';

	public function __construct() {
		parent::__construct();
	}

	protected function init($table, $pk)
	{
		$this->table = $table;
		$this->pk = $pk;
	}

	public function insert($fields=array(), $now_field=NULL)
	{
		if ($now_field) {
			$this->db->set($now_field, 'NOW()', false);
		}
		$this->db->insert($this->table, $fields);
		return $this->db->insert_id();
	}

	public function update($conditions=NULL, $fields=array())
	{
		if (!$conditions) {
			return FALSE;
		}

		if (is_array($conditions)) {
			$this->db->where($conditions);
		} else {
			$this->db->where($this->pk, $conditions);
		}
		return $this->db->update($this->table, $fields);
		// return $this->db->affected_rows();
	}

	public function update_or_create($conditions=array(), $fields=array())
	{
		$result = array(
			'status' => FALSE,
			'created' => FALSE,
		);

		$data = $this->get($conditions);
		if ($data) {
			$rs = $this->update($conditions, $fields);
			if ($rs) {
				$result['status']= TRUE;
				$result['created'] = FALSE;
			}
		} else {
			if (!is_array($conditions)) {
				$conditions = array($this->pk=>$conditions);
			}
			$fields = array_merge($conditions, $fields);
			$saved_id = $this->insert($fields);
			if ($saved_id !== FALSE) {
				$result['created'] = TRUE;
				$result['saved_id'] = $saved_id;
				$result['status'] = TRUE;
			}
		}

		return $result;
	}

	public function delete($conditions=NULL)
	{
		if (!$conditions) {
			return FALSE;
		}
		if ($conditions && is_array($conditions)) {
			$this->db->where($conditions);
		} else {
			$this->db->where($this->pk, $conditions);
		}
		
		$this->db->delete($this->table);
		$num = $this->db->affected_rows();

		return ($num > 0);
	}
	
	public function hidden($conditions=NULL, $fields=array())
	{
		if (!$conditions) {
			return FALSE;
		}

		if (is_array($conditions)) {
			$this->db->where($conditions);
		} else {
			$this->db->where($this->pk, $conditions);
		}
		return $this->db->update($this->table, $fields);
	}

	public function exists($conditions=array(), $onlyOne=FALSE)
	{
		$count = $this->getCount($conditions);
		if ($onlyOne) {
			return ($count == 1);
		}

		return ($count > 0);
	}

	public function getCount($conditions=array())
	{
		if (is_array($conditions)) {
			$this->db->where($conditions);
		} else {
			$this->db->where($this->pk, $conditions);
		}
		$this->db->from($this->table);

		return $this->db->count_all_results();
	}

	public function getSum($field=NULL, $conditions=array())
	{
		$this->db->select_sum($field, 'sum');
		$query = $this->db->get_where($this->table, $conditions);
		
		$rs = $query->row();
		return ($rs->sum)? $rs->sum : 0;
	}
	
	public function get_query()
	{
		return $this->db->last_query();
	}

	public function get($conditions=NULL, $type='array', $get_query=NULL)
	{
		if ($conditions) {
			$this->db->from($this->table);
			if ($conditions) {
				if (is_array($conditions)) {
					$this->db->where($conditions);
				} else {
					$this->db->where($this->pk, $conditions);
				}
			}
			$query = $this->db->get();
			
			if($get_query){ 
				echo $this->get_query(); 
			} else {
				return ($type=='object')? $query->row() : $query->row_array();
			}
		}

		return FALSE;
	}


	public function getAll($order_by=NULL, $type='array', $get_query=NULL)
	{
		if ($order_by) {
			$this->db->order_by($order_by);
		}
		$query = $this->db->get($this->table);
		
		if($get_query){ 
			echo $this->get_query(); 
		} else {
			return ($type=='object')? $query->result() : $query->result_array();
		}
	}

	public function getData($params=array(), $type='array', $get_query=NULL)
	{
		$params = array_merge(array(
			'select' => '*',
			'from' => $this->table,
			'join' => array(),
			'conditions' => array(),
			'rows' => NULL,
			'offset' => NULL,
			'order_by' => NULL,
			'group_by' => NULL,
			'or_where' => NULL,
			'like' => NULL,
			'or_like' => NULL,
			'where_in' => NULL,
			'or_where_in' => NULL,
			'where_not_in' => NULL,
			'or_where_not_in' => NULL,
			'count' => FALSE,
			)
		, $params);

		$this->db->select($params['select']);
		$this->db->from($params['from']);

		foreach ($params['join'] as $row) {
			$this->db->join($row['table'], $row['on'], $row['type']);
		}

		$this->db->where($params['conditions']);

		if ($params['or_where']) {
			if (isset($params['or_where']['many']) && $params['or_where']['many'] == TRUE)	{
				foreach ($params['or_where']['data'] as $k =>$row) {
					$this->db->or_where($row['field'], $row['value']);
				}
			} else {
				$this->db->or_where($params['or_where']['field'], $params['or_where']['value']);
			}
		}

		if ($params['like']) {
			if (isset($params['like']['many']) && $params['like']['many'] == TRUE)	{
				foreach ($params['like']['data'] as $k =>$row) {
					$this->db->like($row['field'], $row['value'], $row['position']);
				}
			} else {
				$this->db->like($params['like']['field'], $params['like']['value'], $params['like']['position']);
			}
		}

		if ($params['or_like']) {
			if (isset($params['or_like']['many']) && $params['or_like']['many'] == TRUE)  {
				$sql = ' (';
				foreach ($params['or_like']['data'] as $k => $row) {
					if ($k == 0) {
						$sql .= "{$row['field']} like '{$row['value']}'";
					} else {
						$sql .= " or {$row['field']} like '{$row['value']}'";
					}
				}
				$sql .= ') ';
				$this->db->where($sql);
			} else {
				$this->db->or_like($params['or_like']['field'], $params['or_like']['value'], $params['or_like']['position']);
			}
		}

		if (isset($params['where_in'])){
			if (isset($params['where_in']['many']) && $params['where_in']['many'] == TRUE)	{
				foreach ($params['where_in']['data'] as $row) {
					$this->db->where_in($row['field'], $row['value']);
				}
			} else {
				$this->db->where_in($params['where_in']['field'], $params['where_in']['value']);
			}
		}

		if (isset($params['or_where_in']['field']) && isset($params['or_where_in']['value'])){
			$this->db->or_where_in($params['or_where_in']['field'], $params['or_where_in']['value']);
		}

		if (isset($params['where_not_in']['field']) && isset($params['where_not_in']['value'])){
			$this->db->where_not_in($params['where_not_in']['field'], $params['where_not_in']['value']);
		}

		if (isset($params['or_where_not_in'])){
			$this->db->or_where_not_in($params['or_where_not_in']['field'], $params['or_where_not_in']['value']);
		}

		if (isset($params['group_by'])) {
			$this->db->group_by($params['group_by']);
		}

		if ($params['count'] == TRUE) {
			return $this->db->count_all_results();
		}

		if (isset($params['order_by'])){
			$this->db->order_by($params['order_by']);
		}

		if ($params['rows'] && $params['offset']) {
			$this->db->limit($params['rows'], $params['offset']);
		} elseif ($params['rows']) {
			$this->db->limit($params['rows']);
		}

		$query = $this->db->get();
		
		if($get_query){ 
			echo $this->get_query(); 
		} else {
			if ($query) {
				return ($type=='object')? $query->result() : $query->result_array();
			} else {
				return ($type=='object')? new stdclass : array();
			}
		}
	}

	public function getDataCount($params)
	{
		$params['count'] = TRUE;
		return $this->getData($params);
	}

}
