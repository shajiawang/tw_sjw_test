!function (root, factory) {
  'function' == typeof define && define.amd ? define(factory)  : 'object' == typeof exports ? module.exports = factory()  : root.PhotoSwipe = factory()
}(this, function () {
  'use strict';
  var PhotoSwipe = function (template, UiClass, items, options) {
    var framework = {
      features: null,
      bind: function (target, type, listener, unbind) {
        var methodName = (unbind ? 'remove' : 'add') + 'EventListener';
        type = type.split(' ');
        for (var i = 0; i < type.length; i++) type[i] && target[methodName](type[i], listener, !1)
      },
      isArray: function (obj) {
        return obj instanceof Array
      },
      createEl: function (classes, tag) {
        var el = document.createElement(tag || 'div');
        return classes && (el.className = classes),
        el
      },
      getScrollY: function () {
        var yOffset = window.pageYOffset;
        return void 0 !== yOffset ? yOffset : document.documentElement.scrollTop
      },
      unbind: function (target, type, listener) {
        framework.bind(target, type, listener, !0)
      },
      removeClass: function (el, className) {
        var reg = new RegExp('(\\s|^)' + className + '(\\s|$)');
        el.className = el.className.replace(reg, ' ').replace(/^\s\s*/, '').replace(/\s\s*$/, '')
      },
      addClass: function (el, className) {
        framework.hasClass(el, className) || (el.className += (el.className ? ' ' : '') + className)
      },
      hasClass: function (el, className) {
        return el.className && new RegExp('(^|\\s)' + className + '(\\s|$)').test(el.className)
      },
      getChildByClass: function (parentEl, childClassName) {
        for (var node = parentEl.firstChild; node; ) {
          if (framework.hasClass(node, childClassName)) return node;
          node = node.nextSibling
        }
      },
      arraySearch: function (array, value, key) {
        for (var i = array.length; i--; ) if (array[i][key] === value) return i;
        return - 1
      },
      extend: function (o1, o2, preventOverwrite) {
        for (var prop in o2) if (o2.hasOwnProperty(prop)) {
          if (preventOverwrite && o1.hasOwnProperty(prop)) continue;
          o1[prop] = o2[prop]
        }
      },
      easing: {
        sine: {
          out: function (k) {
            return Math.sin(k * (Math.PI / 2))
          },
          inOut: function (k) {
            return - (Math.cos(Math.PI * k) - 1) / 2
          }
        },
        cubic: {
          out: function (k) {
            return --k * k * k + 1
          }
        }
      },
      detectFeatures: function () {
        if (framework.features) return framework.features;
        var helperEl = framework.createEl(),
        helperStyle = helperEl.style,
        vendor = '',
        features = {
        };
        if (features.oldIE = document.all && !document.addEventListener, features.touch = 'ontouchstart' in window, window.requestAnimationFrame && (features.raf = window.requestAnimationFrame, features.caf = window.cancelAnimationFrame), features.pointerEvent = navigator.pointerEnabled || navigator.msPointerEnabled, !features.pointerEvent) {
          var ua = navigator.userAgent;
          if (/iP(hone|od)/.test(navigator.platform)) {
            var v = navigator.appVersion.match(/OS (\d+)_(\d+)_?(\d+)?/);
            v && v.length > 0 && (v = parseInt(v[1], 10), v >= 1 && 8 > v && (features.isOldIOSPhone = !0))
          }
          var match = ua.match(/Android\s([0-9\.]*)/),
          androidversion = match ? match[1] : 0;
          androidversion = parseFloat(androidversion),
          androidversion >= 1 && (4.4 > androidversion && (features.isOldAndroid = !0), features.androidVersion = androidversion),
          features.isMobileOpera = /opera mini|opera mobi/i.test(ua)
        }
        for (var styleCheckItem, styleName, styleChecks = [
          'transform',
          'perspective',
          'animationName'
        ], vendors = [
          '',
          'webkit',
          'Moz',
          'ms',
          'O'
        ], i = 0; 4 > i; i++) {
          vendor = vendors[i];
          for (var a = 0; 3 > a; a++) styleCheckItem = styleChecks[a],
          styleName = vendor + (vendor ? styleCheckItem.charAt(0).toUpperCase() + styleCheckItem.slice(1)  : styleCheckItem),
          !features[styleCheckItem] && styleName in helperStyle && (features[styleCheckItem] = styleName);
          vendor && !features.raf && (vendor = vendor.toLowerCase(), features.raf = window[vendor + 'RequestAnimationFrame'], features.raf && (features.caf = window[vendor + 'CancelAnimationFrame'] || window[vendor + 'CancelRequestAnimationFrame']))
        }
        if (!features.raf) {
          var lastTime = 0;
          features.raf = function (fn) {
            var currTime = (new Date).getTime(),
            timeToCall = Math.max(0, 16 - (currTime - lastTime)),
            id = window.setTimeout(function () {
              fn(currTime + timeToCall)
            }, timeToCall);
            return lastTime = currTime + timeToCall,
            id
          },
          features.caf = function (id) {
            clearTimeout(id)
          }
        }
        return features.svg = !!document.createElementNS && !!document.createElementNS('http://www.w3.org/2000/svg', 'svg').createSVGRect,
        framework.features = features,
        features
      }
    };
    framework.detectFeatures(),
    framework.features.oldIE && (framework.bind = function (target, type, listener, unbind) {
      type = type.split(' ');
      for (var evName, methodName = (unbind ? 'detach' : 'attach') + 'Event', _handleEv = function () {
        listener.handleEvent.call(listener)
      }, i = 0; i < type.length; i++) if (evName = type[i]) if ('object' == typeof listener && listener.handleEvent) {
        if (unbind) {
          if (!listener['oldIE' + evName]) return !1
        } else listener['oldIE' + evName] = _handleEv;
        target[methodName]('on' + evName, listener['oldIE' + evName])
      } else target[methodName]('on' + evName, listener)
    });
    var self = this,
    DOUBLE_TAP_RADIUS = 25,
    NUM_HOLDERS = 3,
    _options = {
      allowPanToNext: !0,
      spacing: 0.12,
      bgOpacity: 1,
      mouseUsed: !1,
      loop: !0,
      pinchToClose: !0,
      closeOnScroll: !0,
      closeOnVerticalDrag: !0,
      verticalDragRange: 0.75,
      hideAnimationDuration: 333,
      showAnimationDuration: 333,
      showHideOpacity: !1,
      focus: !0,
      escKey: !0,
      arrowKeys: !0,
      mainScrollEndFriction: 0.35,
      panEndFriction: 0.35,
      isClickableElement: function (el) {
        return 'A' === el.tagName
      },
      getDoubleTapZoom: function (isMouseClick, item) {
        return isMouseClick ? 1 : item.initialZoomLevel < 0.7 ? 1 : 1.33
      },
      maxSpreadZoom: 1.33,
      modal: !0,
      scaleMode: 'fit'
    };
    framework.extend(_options, options);
    var _isOpen,
    _isDestroying,
    _closedByScroll,
    _currentItemIndex,
    _containerStyle,
    _containerShiftIndex,
    _upMoveEvents,
    _downEvents,
    _globalEventHandlers,
    _currZoomLevel,
    _startZoomLevel,
    _translatePrefix,
    _translateSufix,
    _updateSizeInterval,
    _itemsNeedUpdate,
    _itemHolders,
    _prevItemIndex,
    _dragStartEvent,
    _dragMoveEvent,
    _dragEndEvent,
    _dragCancelEvent,
    _transformKey,
    _pointerEventEnabled,
    _likelyTouchDevice,
    _requestAF,
    _cancelAF,
    _initalClassName,
    _initalWindowScrollY,
    _oldIE,
    _currentWindowScrollY,
    _features,
    _gestureStartTime,
    _gestureCheckSpeedTime,
    _releaseAnimData,
    _isZoomingIn,
    _verticalDragInitiated,
    _oldAndroidTouchEndTimeout,
    _isDragging,
    _isMultitouch,
    _zoomStarted,
    _moved,
    _dragAnimFrame,
    _mainScrollShifted,
    _currentPoints,
    _isZooming,
    _currPointsDistance,
    _startPointsDistance,
    _currPanBounds,
    _currZoomElementStyle,
    _mainScrollAnimating,
    _direction,
    _isFirstMove,
    _opacityChanged,
    _bgOpacity,
    _wasOverInitialZoom,
    _tempCounter,
    _getEmptyPoint = function () {
      return {
        x: 0,
        y: 0
      }
    },
    _currPanDist = _getEmptyPoint(),
    _startPanOffset = _getEmptyPoint(),
    _panOffset = _getEmptyPoint(),
    _viewportSize = {
    },
    _currPositionIndex = 0,
    _offset = {
    },
    _slideSize = _getEmptyPoint(),
    _indexDiff = 0,
    _isFixedPosition = !0,
    _modules = [
    ],
    _windowVisibleSize = {
    },
    _renderMaxResolution = !1,
    _registerModule = function (name, module) {
      framework.extend(self, module.publicMethods),
      _modules.push(name)
    },
    _getLoopedId = function (index) {
      var numSlides = _getNumItems();
      return index > numSlides - 1 ? index - numSlides : 0 > index ? numSlides + index : index
    },
    _listeners = {
    },
    _listen = function (name, fn) {
      return _listeners[name] || (_listeners[name] = [
      ]),
      _listeners[name].push(fn)
    },
    _shout = function (name) {
      var listeners = _listeners[name];
      if (listeners) {
        var args = Array.prototype.slice.call(arguments);
        args.shift();
        for (var i = 0; i < listeners.length; i++) listeners[i].apply(self, args)
      }
    },
    _getCurrentTime = function () {
      return (new Date).getTime()
    },
    _applyBgOpacity = function (opacity) {
      _bgOpacity = opacity,
      self.bg.style.opacity = opacity * _options.bgOpacity
    },
    _applyZoomTransform = function (styleObj, x, y, zoom, item) {
      (!_renderMaxResolution || item && item !== self.currItem) && (zoom /= item ? item.fitRatio : self.currItem.fitRatio),
      styleObj[_transformKey] = _translatePrefix + x + 'px, ' + y + 'px' + _translateSufix + ' scale(' + zoom + ')'
    },
    _applyCurrentZoomPan = function (allowRenderResolution) {
      _currZoomElementStyle && (allowRenderResolution && (_currZoomLevel > self.currItem.fitRatio ? _renderMaxResolution || (_setImageSize(self.currItem, !1, !0), _renderMaxResolution = !0)  : _renderMaxResolution && (_setImageSize(self.currItem), _renderMaxResolution = !1)), _applyZoomTransform(_currZoomElementStyle, _panOffset.x, _panOffset.y, _currZoomLevel))
    },
    _applyZoomPanToItem = function (item) {
      item.container && _applyZoomTransform(item.container.style, item.initialPosition.x, item.initialPosition.y, item.initialZoomLevel, item)
    },
    _setTranslateX = function (x, elStyle) {
      elStyle[_transformKey] = _translatePrefix + x + 'px, 0px' + _translateSufix
    },
    _moveMainScroll = function (x, dragging) {
      if (!_options.loop && dragging) {
        var newSlideIndexOffset = _currentItemIndex + (_slideSize.x * _currPositionIndex - x) / _slideSize.x,
        delta = Math.round(x - _mainScrollPos.x);
        (0 > newSlideIndexOffset && delta > 0 || newSlideIndexOffset >= _getNumItems() - 1 && 0 > delta) && (x = _mainScrollPos.x + delta * _options.mainScrollEndFriction)
      }
      _mainScrollPos.x = x,
      _setTranslateX(x, _containerStyle)
    },
    _calculatePanOffset = function (axis, zoomLevel) {
      var m = _midZoomPoint[axis] - _offset[axis];
      return _startPanOffset[axis] + _currPanDist[axis] + m - m * (zoomLevel / _startZoomLevel)
    },
    _equalizePoints = function (p1, p2) {
      p1.x = p2.x,
      p1.y = p2.y,
      p2.id && (p1.id = p2.id)
    },
    _roundPoint = function (p) {
      p.x = Math.round(p.x),
      p.y = Math.round(p.y)
    },
    _mouseMoveTimeout = null,
    _onFirstMouseMove = function () {
      _mouseMoveTimeout && (framework.unbind(document, 'mousemove', _onFirstMouseMove), framework.addClass(template, 'pswp--has_mouse'), _options.mouseUsed = !0, _shout('mouseUsed')),
      _mouseMoveTimeout = setTimeout(function () {
        _mouseMoveTimeout = null
      }, 100)
    },
    _bindEvents = function () {
      framework.bind(document, 'keydown', self),
      _features.transform && framework.bind(self.scrollWrap, 'click', self),
      _options.mouseUsed || framework.bind(document, 'mousemove', _onFirstMouseMove),
      framework.bind(window, 'resize scroll', self),
      _shout('bindEvents')
    },
    _unbindEvents = function () {
      framework.unbind(window, 'resize', self),
      framework.unbind(window, 'scroll', _globalEventHandlers.scroll),
      framework.unbind(document, 'keydown', self),
      framework.unbind(document, 'mousemove', _onFirstMouseMove),
      _features.transform && framework.unbind(self.scrollWrap, 'click', self),
      _isDragging && framework.unbind(window, _upMoveEvents, self),
      _shout('unbindEvents')
    },
    _calculatePanBounds = function (zoomLevel, update) {
      var bounds = _calculateItemSize(self.currItem, _viewportSize, zoomLevel);
      return update && (_currPanBounds = bounds),
      bounds
    },
    _getMinZoomLevel = function (item) {
      return item || (item = self.currItem),
      item.initialZoomLevel
    },
    _getMaxZoomLevel = function (item) {
      return item || (item = self.currItem),
      item.w > 0 ? _options.maxSpreadZoom : 1
    },
    _modifyDestPanOffset = function (axis, destPanBounds, destPanOffset, destZoomLevel) {
      return destZoomLevel === self.currItem.initialZoomLevel ? (destPanOffset[axis] = self.currItem.initialPosition[axis], !0)  : (destPanOffset[axis] = _calculatePanOffset(axis, destZoomLevel), destPanOffset[axis] > destPanBounds.min[axis] ? (destPanOffset[axis] = destPanBounds.min[axis], !0)  : destPanOffset[axis] < destPanBounds.max[axis] ? (destPanOffset[axis] = destPanBounds.max[axis], !0)  : !1)
    },
    _setupTransforms = function () {
      if (_transformKey) {
        var allow3dTransform = _features.perspective && !_likelyTouchDevice;
        return _translatePrefix = 'translate' + (allow3dTransform ? '3d(' : '('),
        void (_translateSufix = _features.perspective ? ', 0px)' : ')')
      }
      _transformKey = 'left',
      framework.addClass(template, 'pswp--ie'),
      _setTranslateX = function (x, elStyle) {
        elStyle.left = x + 'px'
      },
      _applyZoomPanToItem = function (item) {
        var zoomRatio = item.fitRatio > 1 ? 1 : item.fitRatio,
        s = item.container.style,
        w = zoomRatio * item.w,
        h = zoomRatio * item.h;
        s.width = w + 'px',
        s.height = h + 'px',
        s.left = item.initialPosition.x + 'px',
        s.top = item.initialPosition.y + 'px'
      },
      _applyCurrentZoomPan = function () {
        if (_currZoomElementStyle) {
          var s = _currZoomElementStyle,
          item = self.currItem,
          zoomRatio = item.fitRatio > 1 ? 1 : item.fitRatio,
          w = zoomRatio * item.w,
          h = zoomRatio * item.h;
          s.width = w + 'px',
          s.height = h + 'px',
          s.left = _panOffset.x + 'px',
          s.top = _panOffset.y + 'px'
        }
      }
    },
    _onKeyDown = function (e) {
      var keydownAction = '';
      _options.escKey && 27 === e.keyCode ? keydownAction = 'close' : _options.arrowKeys && (37 === e.keyCode ? keydownAction = 'prev' : 39 === e.keyCode && (keydownAction = 'next')),
      keydownAction && (e.ctrlKey || e.altKey || e.shiftKey || e.metaKey || (e.preventDefault ? e.preventDefault()  : e.returnValue = !1, self[keydownAction]()))
    },
    _onGlobalClick = function (e) {
      e && (_moved || _zoomStarted || _mainScrollAnimating || _verticalDragInitiated) && (e.preventDefault(), e.stopPropagation())
    },
    _updatePageScrollOffset = function () {
      self.setScrollOffset(0, framework.getScrollY())
    },
    _animations = {
    },
    _numAnimations = 0,
    _stopAnimation = function (name) {
      _animations[name] && (_animations[name].raf && _cancelAF(_animations[name].raf), _numAnimations--, delete _animations[name])
    },
    _registerStartAnimation = function (name) {
      _animations[name] && _stopAnimation(name),
      _animations[name] || (_numAnimations++, _animations[name] = {
      })
    },
    _stopAllAnimations = function () {
      for (var prop in _animations) _animations.hasOwnProperty(prop) && _stopAnimation(prop)
    },
    _animateProp = function (name, b, endProp, d, easingFn, onUpdate, onComplete) {
      var t,
      startAnimTime = _getCurrentTime();
      _registerStartAnimation(name);
      var animloop = function () {
        if (_animations[name]) {
          if (t = _getCurrentTime() - startAnimTime, t >= d) return _stopAnimation(name),
          onUpdate(endProp),
          void (onComplete && onComplete());
          onUpdate((endProp - b) * easingFn(t / d) + b),
          _animations[name].raf = _requestAF(animloop)
        }
      };
      animloop()
    },
    publicMethods = {
      shout: _shout,
      listen: _listen,
      viewportSize: _viewportSize,
      options: _options,
      isMainScrollAnimating: function () {
        return _mainScrollAnimating
      },
      getZoomLevel: function () {
        return _currZoomLevel
      },
      getCurrentIndex: function () {
        return _currentItemIndex
      },
      isDragging: function () {
        return _isDragging
      },
      isZooming: function () {
        return _isZooming
      },
      setScrollOffset: function (x, y) {
        _offset.x = x,
        _currentWindowScrollY = _offset.y = y,
        _shout('updateScrollOffset', _offset)
      },
      applyZoomPan: function (zoomLevel, panX, panY, allowRenderResolution) {
        _panOffset.x = panX,
        _panOffset.y = panY,
        _currZoomLevel = zoomLevel,
        _applyCurrentZoomPan(allowRenderResolution)
      },
      init: function () {
        if (!_isOpen && !_isDestroying) {
          var i;
          self.framework = framework,
          self.template = template,
          self.bg = framework.getChildByClass(template, 'pswp__bg'),
          _initalClassName = template.className,
          _isOpen = !0,
          _features = framework.detectFeatures(),
          _requestAF = _features.raf,
          _cancelAF = _features.caf,
          _transformKey = _features.transform,
          _oldIE = _features.oldIE,
          self.scrollWrap = framework.getChildByClass(template, 'pswp__scroll-wrap'),
          self.container = framework.getChildByClass(self.scrollWrap, 'pswp__container'),
          _containerStyle = self.container.style,
          self.itemHolders = _itemHolders = [
            {
              el: self.container.children[0],
              wrap: 0,
              index: - 1
            },
            {
              el: self.container.children[1],
              wrap: 0,
              index: - 1
            },
            {
              el: self.container.children[2],
              wrap: 0,
              index: - 1
            }
          ],
          _itemHolders[0].el.style.display = _itemHolders[2].el.style.display = 'none',
          _setupTransforms(),
          _globalEventHandlers = {
            resize: self.updateSize,
            scroll: _updatePageScrollOffset,
            keydown: _onKeyDown,
            click: _onGlobalClick
          };
          var oldPhone = _features.isOldIOSPhone || _features.isOldAndroid || _features.isMobileOpera;
          for (_features.animationName && _features.transform && !oldPhone || (_options.showAnimationDuration = _options.hideAnimationDuration = 0), i = 0; i < _modules.length; i++) self['init' + _modules[i]]();
          if (UiClass) {
            var ui = self.ui = new UiClass(self, framework);
            ui.init()
          }
          _shout('firstUpdate'),
          _currentItemIndex = _currentItemIndex || _options.index || 0,
          (isNaN(_currentItemIndex) || 0 > _currentItemIndex || _currentItemIndex >= _getNumItems()) && (_currentItemIndex = 0),
          self.currItem = _getItemAt(_currentItemIndex),
          (_features.isOldIOSPhone || _features.isOldAndroid) && (_isFixedPosition = !1),
          template.setAttribute('aria-hidden', 'false'),
          _options.modal && (_isFixedPosition ? template.style.position = 'fixed' : (template.style.position = 'absolute', template.style.top = framework.getScrollY() + 'px')),
          void 0 === _currentWindowScrollY && (_shout('initialLayout'), _currentWindowScrollY = _initalWindowScrollY = framework.getScrollY());
          var rootClasses = 'pswp--open ';
          for (_options.mainClass && (rootClasses += _options.mainClass + ' '), _options.showHideOpacity && (rootClasses += 'pswp--animate_opacity '), rootClasses += _likelyTouchDevice ? 'pswp--touch' : 'pswp--notouch', rootClasses += _features.animationName ? ' pswp--css_animation' : '', rootClasses += _features.svg ? ' pswp--svg' : '', $('.fbstore_area #js_nav_wrapper').css({
            overflow: 'auto'
          }), framework.addClass(template, rootClasses), self.updateSize(), _containerShiftIndex = - 1, _indexDiff = null, i = 0; NUM_HOLDERS > i; i++) _setTranslateX((i + _containerShiftIndex) * _slideSize.x, _itemHolders[i].el.style);
          _oldIE || framework.bind(self.scrollWrap, _downEvents, self),
          _listen('initialZoomInEnd', function () {
            self.setContent(_itemHolders[0], _currentItemIndex - 1),
            self.setContent(_itemHolders[2], _currentItemIndex + 1),
            _itemHolders[0].el.style.display = _itemHolders[2].el.style.display = 'block',
            _options.focus && template.focus(),
            _bindEvents()
          }),
          self.setContent(_itemHolders[1], _currentItemIndex),
          self.updateCurrItem(),
          _shout('afterInit'),
          _isFixedPosition || (_updateSizeInterval = setInterval(function () {
            _numAnimations || _isDragging || _isZooming || _currZoomLevel !== self.currItem.initialZoomLevel || self.updateSize()
          }, 1000)),
          framework.addClass(template, 'pswp--visible')
        }
      },
      close: function () {
        _isOpen && ($('.fbstore_area #js_nav_wrapper').css('overflow', ''), _isOpen = !1, _isDestroying = !0, _shout('close'), _unbindEvents(), _showOrHide(self.currItem, null, !0, self.destroy))
      },
      destroy: function () {
        _shout('destroy'),
        _showOrHideTimeout && clearTimeout(_showOrHideTimeout),
        template.setAttribute('aria-hidden', 'true'),
        template.className = _initalClassName,
        _updateSizeInterval && clearInterval(_updateSizeInterval),
        framework.unbind(self.scrollWrap, _downEvents, self),
        framework.unbind(window, 'scroll', self),
        _stopDragUpdateLoop(),
        _stopAllAnimations(),
        _listeners = null
      },
      panTo: function (x, y, force) {
        force || (x > _currPanBounds.min.x ? x = _currPanBounds.min.x : x < _currPanBounds.max.x && (x = _currPanBounds.max.x), y > _currPanBounds.min.y ? y = _currPanBounds.min.y : y < _currPanBounds.max.y && (y = _currPanBounds.max.y)),
        _panOffset.x = x,
        _panOffset.y = y,
        _applyCurrentZoomPan()
      },
      handleEvent: function (e) {
        e = e || window.event,
        _globalEventHandlers[e.type] && _globalEventHandlers[e.type](e)
      },
      goTo: function (index) {
        index = _getLoopedId(index);
        var diff = index - _currentItemIndex;
        _indexDiff = diff,
        _currentItemIndex = index,
        self.currItem = _getItemAt(_currentItemIndex),
        _currPositionIndex -= diff,
        _moveMainScroll(_slideSize.x * _currPositionIndex),
        _stopAllAnimations(),
        _mainScrollAnimating = !1,
        self.updateCurrItem()
      },
      next: function () {
        self.goTo(_currentItemIndex + 1)
      },
      prev: function () {
        self.goTo(_currentItemIndex - 1)
      },
      updateCurrZoomItem: function (emulateSetContent) {
        if (emulateSetContent && _shout('beforeChange', 0), _itemHolders[1].el.children.length) {
          var zoomElement = _itemHolders[1].el.children[0];
          _currZoomElementStyle = framework.hasClass(zoomElement, 'pswp__zoom-wrap') ? zoomElement.style : null
        } else _currZoomElementStyle = null;
        _currPanBounds = self.currItem.bounds,
        _startZoomLevel = _currZoomLevel = self.currItem.initialZoomLevel,
        _panOffset.x = _currPanBounds.center.x,
        _panOffset.y = _currPanBounds.center.y,
        emulateSetContent && _shout('afterChange')
      },
      invalidateCurrItems: function () {
        _itemsNeedUpdate = !0;
        for (var i = 0; NUM_HOLDERS > i; i++) _itemHolders[i].item && (_itemHolders[i].item.needsUpdate = !0)
      },
      updateCurrItem: function (beforeAnimation) {
        if (0 !== _indexDiff) {
          var tempHolder,
          diffAbs = Math.abs(_indexDiff);
          if (!(beforeAnimation && 2 > diffAbs)) {
            self.currItem = _getItemAt(_currentItemIndex),
            _renderMaxResolution = !1,
            _shout('beforeChange', _indexDiff),
            diffAbs >= NUM_HOLDERS && (_containerShiftIndex += _indexDiff + (_indexDiff > 0 ? - NUM_HOLDERS : NUM_HOLDERS), diffAbs = NUM_HOLDERS);
            for (var i = 0; diffAbs > i; i++) _indexDiff > 0 ? (tempHolder = _itemHolders.shift(), _itemHolders[NUM_HOLDERS - 1] = tempHolder, _containerShiftIndex++, _setTranslateX((_containerShiftIndex + 2) * _slideSize.x, tempHolder.el.style), self.setContent(tempHolder, _currentItemIndex - diffAbs + i + 1 + 1))  : (tempHolder = _itemHolders.pop(), _itemHolders.unshift(tempHolder), _containerShiftIndex--, _setTranslateX(_containerShiftIndex * _slideSize.x, tempHolder.el.style), self.setContent(tempHolder, _currentItemIndex + diffAbs - i - 1 - 1));
            if (_currZoomElementStyle && 1 === Math.abs(_indexDiff)) {
              var prevItem = _getItemAt(_prevItemIndex);
              prevItem.initialZoomLevel !== _currZoomLevel && (_calculateItemSize(prevItem, _viewportSize), _setImageSize(prevItem), _applyZoomPanToItem(prevItem))
            }
            _indexDiff = 0,
            self.updateCurrZoomItem(),
            _prevItemIndex = _currentItemIndex,
            _shout('afterChange')
          }
        }
      },
      updateSize: function (force) {
        if (!_isFixedPosition && _options.modal) {
          var windowScrollY = framework.getScrollY();
          if (_currentWindowScrollY !== windowScrollY && (template.style.top = windowScrollY + 'px', _currentWindowScrollY = windowScrollY), !force && _windowVisibleSize.x === window.innerWidth && _windowVisibleSize.y === window.innerHeight) return;
          _windowVisibleSize.x = window.innerWidth,
          _windowVisibleSize.y = window.innerHeight,
          template.style.height = _windowVisibleSize.y + 'px'
        }
        if (_viewportSize.x = self.scrollWrap.clientWidth, _viewportSize.y = self.scrollWrap.clientHeight, _updatePageScrollOffset(), _slideSize.x = _viewportSize.x + Math.round(_viewportSize.x * _options.spacing), _slideSize.y = _viewportSize.y, _moveMainScroll(_slideSize.x * _currPositionIndex), _shout('beforeResize'), void 0 !== _containerShiftIndex) {
          for (var holder, item, hIndex, i = 0; NUM_HOLDERS > i; i++) holder = _itemHolders[i],
          _setTranslateX((i + _containerShiftIndex) * _slideSize.x, holder.el.style),
          hIndex = _currentItemIndex + i - 1,
          _options.loop && _getNumItems() > 2 && (hIndex = _getLoopedId(hIndex)),
          item = _getItemAt(hIndex),
          item && (_itemsNeedUpdate || item.needsUpdate || !item.bounds) ? (self.cleanSlide(item), self.setContent(holder, hIndex), 1 === i && (self.currItem = item, self.updateCurrZoomItem(!0)), item.needsUpdate = !1)  : - 1 === holder.index && hIndex >= 0 && self.setContent(holder, hIndex),
          item && item.container && (_calculateItemSize(item, _viewportSize), _setImageSize(item), _applyZoomPanToItem(item));
          _itemsNeedUpdate = !1
        }
        _startZoomLevel = _currZoomLevel = self.currItem.initialZoomLevel,
        _currPanBounds = self.currItem.bounds,
        _currPanBounds && (_panOffset.x = _currPanBounds.center.x, _panOffset.y = _currPanBounds.center.y, _applyCurrentZoomPan(!0)),
        _shout('resize')
      },
      zoomTo: function (destZoomLevel, centerPoint, speed, easingFn, updateFn) {
        centerPoint && (_startZoomLevel = _currZoomLevel, _midZoomPoint.x = Math.abs(centerPoint.x) - _panOffset.x, _midZoomPoint.y = Math.abs(centerPoint.y) - _panOffset.y, _equalizePoints(_startPanOffset, _panOffset));
        var destPanBounds = _calculatePanBounds(destZoomLevel, !1),
        destPanOffset = {
        };
        _modifyDestPanOffset('x', destPanBounds, destPanOffset, destZoomLevel),
        _modifyDestPanOffset('y', destPanBounds, destPanOffset, destZoomLevel);
        var initialZoomLevel = _currZoomLevel,
        initialPanOffset = {
          x: _panOffset.x,
          y: _panOffset.y
        };
        _roundPoint(destPanOffset);
        var onUpdate = function (now) {
          1 === now ? (_currZoomLevel = destZoomLevel, _panOffset.x = destPanOffset.x, _panOffset.y = destPanOffset.y)  : (_currZoomLevel = (destZoomLevel - initialZoomLevel) * now + initialZoomLevel, _panOffset.x = (destPanOffset.x - initialPanOffset.x) * now + initialPanOffset.x, _panOffset.y = (destPanOffset.y - initialPanOffset.y) * now + initialPanOffset.y),
          updateFn && updateFn(now),
          _applyCurrentZoomPan(1 === now)
        };
        speed ? _animateProp('customZoomTo', 0, 1, speed, easingFn || framework.easing.sine.inOut, onUpdate)  : onUpdate(1)
      }
    },
    MIN_SWIPE_DISTANCE = 30,
    DIRECTION_CHECK_OFFSET = 10,
    p = {
    },
    p2 = {
    },
    delta = {
    },
    _currPoint = {
    },
    _startPoint = {
    },
    _currPointers = [
    ],
    _startMainScrollPos = {
    },
    _posPoints = [
    ],
    _tempPoint = {
    },
    _currZoomedItemIndex = 0,
    _centerPoint = _getEmptyPoint(),
    _lastReleaseTime = 0,
    _mainScrollPos = _getEmptyPoint(),
    _midZoomPoint = _getEmptyPoint(),
    _currCenterPoint = _getEmptyPoint(),
    _isEqualPoints = function (p1, p2) {
      return p1.x === p2.x && p1.y === p2.y
    },
    _isNearbyPoints = function (touch0, touch1) {
      return Math.abs(touch0.x - touch1.x) < DOUBLE_TAP_RADIUS && Math.abs(touch0.y - touch1.y) < DOUBLE_TAP_RADIUS
    },
    _calculatePointsDistance = function (p1, p2) {
      return _tempPoint.x = Math.abs(p1.x - p2.x),
      _tempPoint.y = Math.abs(p1.y - p2.y),
      Math.sqrt(_tempPoint.x * _tempPoint.x + _tempPoint.y * _tempPoint.y)
    },
    _stopDragUpdateLoop = function () {
      _dragAnimFrame && (_cancelAF(_dragAnimFrame), _dragAnimFrame = null)
    },
    _dragUpdateLoop = function () {
      _isDragging && (_dragAnimFrame = _requestAF(_dragUpdateLoop), _renderMovement())
    },
    _canPan = function () {
      return !('fit' === _options.scaleMode && _currZoomLevel === self.currItem.initialZoomLevel)
    },
    _closestElement = function (el, fn) {
      return el && el !== document ? el.getAttribute('class') && el.getAttribute('class').indexOf('pswp__scroll-wrap') > - 1 ? !1 : fn(el) ? el : _closestElement(el.parentNode, fn)  : !1
    },
    _preventObj = {
    },
    _preventDefaultEventBehaviour = function (e, isDown) {
      return _preventObj.prevent = !_closestElement(e.target, _options.isClickableElement),
      _shout('preventDragEvent', e, isDown, _preventObj),
      _preventObj.prevent
    },
    _convertTouchToPoint = function (touch, p) {
      return p.x = touch.pageX,
      p.y = touch.pageY,
      p.id = touch.identifier,
      p
    },
    _findCenterOfPoints = function (p1, p2, pCenter) {
      pCenter.x = 0.5 * (p1.x + p2.x),
      pCenter.y = 0.5 * (p1.y + p2.y)
    },
    _pushPosPoint = function (time, x, y) {
      if (time - _gestureCheckSpeedTime > 50) {
        var o = _posPoints.length > 2 ? _posPoints.shift()  : {
        };
        o.x = x,
        o.y = y,
        _posPoints.push(o),
        _gestureCheckSpeedTime = time
      }
    },
    _calculateVerticalDragOpacityRatio = function () {
      var yOffset = _panOffset.y - self.currItem.initialPosition.y;
      return 1 - Math.abs(yOffset / (_viewportSize.y / 2))
    },
    _ePoint1 = {
    },
    _ePoint2 = {
    },
    _tempPointsArr = [
    ],
    _getTouchPoints = function (e) {
      for (; _tempPointsArr.length > 0; ) _tempPointsArr.pop();
      return _pointerEventEnabled ? (_tempCounter = 0, _currPointers.forEach(function (p) {
        0 === _tempCounter ? _tempPointsArr[0] = p : 1 === _tempCounter && (_tempPointsArr[1] = p),
        _tempCounter++
      }))  : e.type.indexOf('touch') > - 1 ? e.touches && e.touches.length > 0 && (_tempPointsArr[0] = _convertTouchToPoint(e.touches[0], _ePoint1), e.touches.length > 1 && (_tempPointsArr[1] = _convertTouchToPoint(e.touches[1], _ePoint2)))  : (_ePoint1.x = e.pageX, _ePoint1.y = e.pageY, _ePoint1.id = '', _tempPointsArr[0] = _ePoint1),
      _tempPointsArr
    },
    _panOrMoveMainScroll = function (axis, delta) {
      var panFriction,
      startOverDiff,
      newPanPos,
      newMainScrollPos,
      overDiff = 0,
      newOffset = _panOffset[axis] + delta[axis],
      dir = delta[axis] > 0,
      newMainScrollPosition = _mainScrollPos.x + delta.x,
      mainScrollDiff = _mainScrollPos.x - _startMainScrollPos.x;
      return panFriction = newOffset > _currPanBounds.min[axis] || newOffset < _currPanBounds.max[axis] ? _options.panEndFriction : 1,
      newOffset = _panOffset[axis] + delta[axis] * panFriction,
      !_options.allowPanToNext && _currZoomLevel !== self.currItem.initialZoomLevel || (_currZoomElementStyle ? 'h' !== _direction || 'x' !== axis || _zoomStarted || (dir ? (newOffset > _currPanBounds.min[axis] && (panFriction = _options.panEndFriction, overDiff = _currPanBounds.min[axis] - newOffset, startOverDiff = _currPanBounds.min[axis] - _startPanOffset[axis]), (0 >= startOverDiff || 0 > mainScrollDiff) && _getNumItems() > 1 ? (newMainScrollPos = newMainScrollPosition, 0 > mainScrollDiff && newMainScrollPosition > _startMainScrollPos.x && (newMainScrollPos = _startMainScrollPos.x))  : _currPanBounds.min.x !== _currPanBounds.max.x && (newPanPos = newOffset))  : (newOffset < _currPanBounds.max[axis] && (panFriction = _options.panEndFriction, overDiff = newOffset - _currPanBounds.max[axis], startOverDiff = _startPanOffset[axis] - _currPanBounds.max[axis]), (0 >= startOverDiff || mainScrollDiff > 0) && _getNumItems() > 1 ? (newMainScrollPos = newMainScrollPosition, mainScrollDiff > 0 && newMainScrollPosition < _startMainScrollPos.x && (newMainScrollPos = _startMainScrollPos.x))  : _currPanBounds.min.x !== _currPanBounds.max.x && (newPanPos = newOffset)))  : newMainScrollPos = newMainScrollPosition, 'x' !== axis) ? void (_mainScrollAnimating || _mainScrollShifted || _currZoomLevel > self.currItem.fitRatio && (_panOffset[axis] += delta[axis] * panFriction))  : (void 0 !== newMainScrollPos && (_moveMainScroll(newMainScrollPos, !0), _mainScrollShifted = newMainScrollPos !== _startMainScrollPos.x), _currPanBounds.min.x !== _currPanBounds.max.x && (void 0 !== newPanPos ? _panOffset.x = newPanPos : _mainScrollShifted || (_panOffset.x += delta.x * panFriction)), void 0 !== newMainScrollPos)
    },
    _onDragStart = function (e) {
      if (!('mousedown' === e.type && e.button > 0)) {
        if (_initialZoomRunning) return void e.preventDefault();
        if (!_oldAndroidTouchEndTimeout || 'mousedown' !== e.type) {
          if (_preventDefaultEventBehaviour(e, !0) && e.preventDefault(), _shout('pointerDown'), _pointerEventEnabled) {
            var pointerIndex = framework.arraySearch(_currPointers, e.pointerId, 'id');
            0 > pointerIndex && (pointerIndex = _currPointers.length),
            _currPointers[pointerIndex] = {
              x: e.pageX,
              y: e.pageY,
              id: e.pointerId
            }
          }
          var startPointsList = _getTouchPoints(e),
          numPoints = startPointsList.length;
          _currentPoints = null,
          _stopAllAnimations(),
          _isDragging && 1 !== numPoints || (_isDragging = _isFirstMove = !0, framework.bind(window, _upMoveEvents, self), _isZoomingIn = _wasOverInitialZoom = _opacityChanged = _verticalDragInitiated = _mainScrollShifted = _moved = _isMultitouch = _zoomStarted = !1, _direction = null, _shout('firstTouchStart', startPointsList), _equalizePoints(_startPanOffset, _panOffset), _currPanDist.x = _currPanDist.y = 0, _equalizePoints(_currPoint, startPointsList[0]), _equalizePoints(_startPoint, _currPoint), _startMainScrollPos.x = _slideSize.x * _currPositionIndex, _posPoints = [
            {
              x: _currPoint.x,
              y: _currPoint.y
            }
          ], _gestureCheckSpeedTime = _gestureStartTime = _getCurrentTime(), _calculatePanBounds(_currZoomLevel, !0), _stopDragUpdateLoop(), _dragUpdateLoop()),
          !_isZooming && numPoints > 1 && !_mainScrollAnimating && !_mainScrollShifted && (_startZoomLevel = _currZoomLevel, _zoomStarted = !1, _isZooming = _isMultitouch = !0, _currPanDist.y = _currPanDist.x = 0, _equalizePoints(_startPanOffset, _panOffset), _equalizePoints(p, startPointsList[0]), _equalizePoints(p2, startPointsList[1]), _findCenterOfPoints(p, p2, _currCenterPoint), _midZoomPoint.x = Math.abs(_currCenterPoint.x) - _panOffset.x, _midZoomPoint.y = Math.abs(_currCenterPoint.y) - _panOffset.y, _currPointsDistance = _startPointsDistance = _calculatePointsDistance(p, p2))
        }
      }
    },
    _onDragMove = function (e) {
      if (e.preventDefault(), _pointerEventEnabled) {
        var pointerIndex = framework.arraySearch(_currPointers, e.pointerId, 'id');
        if (pointerIndex > - 1) {
          var p = _currPointers[pointerIndex];
          p.x = e.pageX,
          p.y = e.pageY
        }
      }
      if (_isDragging) {
        var touchesList = _getTouchPoints(e);
        if (_direction || _moved || _isZooming) _currentPoints = touchesList;
         else if (_mainScrollPos.x !== _slideSize.x * _currPositionIndex) _direction = 'h';
         else {
          var diff = Math.abs(touchesList[0].x - _currPoint.x) - Math.abs(touchesList[0].y - _currPoint.y);
          Math.abs(diff) >= DIRECTION_CHECK_OFFSET && (_direction = diff > 0 ? 'h' : 'v', _currentPoints = touchesList)
        }
      }
    },
    _renderMovement = function () {
      if (_currentPoints) {
        var numPoints = _currentPoints.length;
        if (0 !== numPoints) if (_equalizePoints(p, _currentPoints[0]), delta.x = p.x - _currPoint.x, delta.y = p.y - _currPoint.y, _isZooming && numPoints > 1) {
          if (_currPoint.x = p.x, _currPoint.y = p.y, !delta.x && !delta.y && _isEqualPoints(_currentPoints[1], p2)) return;
          _equalizePoints(p2, _currentPoints[1]),
          _zoomStarted || (_zoomStarted = !0, _shout('zoomGestureStarted'));
          var pointsDistance = _calculatePointsDistance(p, p2),
          zoomLevel = _calculateZoomLevel(pointsDistance);
          zoomLevel > self.currItem.initialZoomLevel + self.currItem.initialZoomLevel / 15 && (_wasOverInitialZoom = !0);
          var zoomFriction = 1,
          minZoomLevel = _getMinZoomLevel(),
          maxZoomLevel = _getMaxZoomLevel();
          if (minZoomLevel > zoomLevel) if (_options.pinchToClose && !_wasOverInitialZoom && _startZoomLevel <= self.currItem.initialZoomLevel) {
            var minusDiff = minZoomLevel - zoomLevel,
            percent = 1 - minusDiff / (minZoomLevel / 1.2);
            _applyBgOpacity(percent),
            _shout('onPinchClose', percent),
            _opacityChanged = !0
          } else zoomFriction = (minZoomLevel - zoomLevel) / minZoomLevel,
          zoomFriction > 1 && (zoomFriction = 1),
          zoomLevel = minZoomLevel - zoomFriction * (minZoomLevel / 3);
           else zoomLevel > maxZoomLevel && (zoomFriction = (zoomLevel - maxZoomLevel) / (6 * minZoomLevel), zoomFriction > 1 && (zoomFriction = 1), zoomLevel = maxZoomLevel + zoomFriction * minZoomLevel);
          0 > zoomFriction && (zoomFriction = 0),
          _currPointsDistance = pointsDistance,
          _findCenterOfPoints(p, p2, _centerPoint),
          _currPanDist.x += _centerPoint.x - _currCenterPoint.x,
          _currPanDist.y += _centerPoint.y - _currCenterPoint.y,
          _equalizePoints(_currCenterPoint, _centerPoint),
          _panOffset.x = _calculatePanOffset('x', zoomLevel),
          _panOffset.y = _calculatePanOffset('y', zoomLevel),
          _isZoomingIn = zoomLevel > _currZoomLevel,
          _currZoomLevel = zoomLevel,
          _applyCurrentZoomPan()
        } else {
          if (!_direction) return;
          if (_isFirstMove && (_isFirstMove = !1, Math.abs(delta.x) >= DIRECTION_CHECK_OFFSET && (delta.x -= _currentPoints[0].x - _startPoint.x), Math.abs(delta.y) >= DIRECTION_CHECK_OFFSET && (delta.y -= _currentPoints[0].y - _startPoint.y)), _currPoint.x = p.x, _currPoint.y = p.y, 0 === delta.x && 0 === delta.y) return;
          if ('v' === _direction && _options.closeOnVerticalDrag && !_canPan()) {
            _currPanDist.y += delta.y,
            _panOffset.y += delta.y;
            var opacityRatio = _calculateVerticalDragOpacityRatio();
            return _verticalDragInitiated = !0,
            _shout('onVerticalDrag', opacityRatio),
            _applyBgOpacity(opacityRatio),
            void _applyCurrentZoomPan()
          }
          _pushPosPoint(_getCurrentTime(), p.x, p.y),
          _moved = !0,
          _currPanBounds = self.currItem.bounds;
          var mainScrollChanged = _panOrMoveMainScroll('x', delta);
          mainScrollChanged || (_panOrMoveMainScroll('y', delta), _roundPoint(_panOffset), _applyCurrentZoomPan())
        }
      }
    },
    _onDragRelease = function (e) {
      if (_features.isOldAndroid) {
        if (_oldAndroidTouchEndTimeout && 'mouseup' === e.type) return;
        e.type.indexOf('touch') > - 1 && (clearTimeout(_oldAndroidTouchEndTimeout), _oldAndroidTouchEndTimeout = setTimeout(function () {
          _oldAndroidTouchEndTimeout = 0
        }, 600))
      }
      _shout('pointerUp'),
      _preventDefaultEventBehaviour(e, !1) && e.preventDefault();
      var releasePoint;
      if (_pointerEventEnabled) {
        var pointerIndex = framework.arraySearch(_currPointers, e.pointerId, 'id');
        if (pointerIndex > - 1) if (releasePoint = _currPointers.splice(pointerIndex, 1) [0], navigator.pointerEnabled) releasePoint.type = e.pointerType || 'mouse';
         else {
          var MSPOINTER_TYPES = {
            4: 'mouse',
            2: 'touch',
            3: 'pen'
          };
          releasePoint.type = MSPOINTER_TYPES[e.pointerType],
          releasePoint.type || (releasePoint.type = e.pointerType || 'mouse')
        }
      }
      var gestureType,
      touchList = _getTouchPoints(e),
      numPoints = touchList.length;
      if ('mouseup' === e.type && (numPoints = 0), 2 === numPoints) return _currentPoints = null,
      !0;
      1 === numPoints && _equalizePoints(_startPoint, touchList[0]),
      0 !== numPoints || _direction || _mainScrollAnimating || (releasePoint || ('mouseup' === e.type ? releasePoint = {
        x: e.pageX,
        y: e.pageY,
        type: 'mouse'
      }
       : e.changedTouches && e.changedTouches[0] && (releasePoint = {
        x: e.changedTouches[0].pageX,
        y: e.changedTouches[0].pageY,
        type: 'touch'
      })), _shout('touchRelease', e, releasePoint));
      var releaseTimeDiff = - 1;
      if (0 === numPoints && (_isDragging = !1, framework.unbind(window, _upMoveEvents, self), _stopDragUpdateLoop(), _isZooming ? releaseTimeDiff = 0 : - 1 !== _lastReleaseTime && (releaseTimeDiff = _getCurrentTime() - _lastReleaseTime)), _lastReleaseTime = 1 === numPoints ? _getCurrentTime()  : - 1, gestureType = - 1 !== releaseTimeDiff && 150 > releaseTimeDiff ? 'zoom' : 'swipe', _isZooming && 2 > numPoints && (_isZooming = !1, 1 === numPoints && (gestureType = 'zoomPointerUp'), _shout('zoomGestureEnded')), _currentPoints = null, _moved || _zoomStarted || _mainScrollAnimating || _verticalDragInitiated) if (_stopAllAnimations(), _releaseAnimData || (_releaseAnimData = _initDragReleaseAnimationData()), _releaseAnimData.calculateSwipeSpeed('x'), _verticalDragInitiated) {
        var opacityRatio = _calculateVerticalDragOpacityRatio();
        if (opacityRatio < _options.verticalDragRange) self.close();
         else {
          var initalPanY = _panOffset.y,
          initialBgOpacity = _bgOpacity;
          _animateProp('verticalDrag', 0, 1, 300, framework.easing.cubic.out, function (now) {
            _panOffset.y = (self.currItem.initialPosition.y - initalPanY) * now + initalPanY,
            _applyBgOpacity((1 - initialBgOpacity) * now + initialBgOpacity),
            _applyCurrentZoomPan()
          }),
          _shout('onVerticalDrag', 1)
        }
      } else {
        if ((_mainScrollShifted || _mainScrollAnimating) && 0 === numPoints) {
          var itemChanged = _finishSwipeMainScrollGesture(gestureType, _releaseAnimData);
          if (itemChanged) return;
          gestureType = 'zoomPointerUp'
        }
        if (!_mainScrollAnimating) return 'swipe' !== gestureType ? void _completeZoomGesture()  : void (!_mainScrollShifted && _currZoomLevel > self.currItem.fitRatio && _completePanGesture(_releaseAnimData))
      }
    },
    _initDragReleaseAnimationData = function () {
      var lastFlickDuration,
      tempReleasePos,
      s = {
        lastFlickOffset: {
        },
        lastFlickDist: {
        },
        lastFlickSpeed: {
        },
        slowDownRatio: {
        },
        slowDownRatioReverse: {
        },
        speedDecelerationRatio: {
        },
        speedDecelerationRatioAbs: {
        },
        distanceOffset: {
        },
        backAnimDestination: {
        },
        backAnimStarted: {
        },
        calculateSwipeSpeed: function (axis) {
          _posPoints.length > 1 ? (lastFlickDuration = _getCurrentTime() - _gestureCheckSpeedTime + 50, tempReleasePos = _posPoints[_posPoints.length - 2][axis])  : (lastFlickDuration = _getCurrentTime() - _gestureStartTime, tempReleasePos = _startPoint[axis]),
          s.lastFlickOffset[axis] = _currPoint[axis] - tempReleasePos,
          s.lastFlickDist[axis] = Math.abs(s.lastFlickOffset[axis]),
          s.lastFlickDist[axis] > 20 ? s.lastFlickSpeed[axis] = s.lastFlickOffset[axis] / lastFlickDuration : s.lastFlickSpeed[axis] = 0,
          Math.abs(s.lastFlickSpeed[axis]) < 0.1 && (s.lastFlickSpeed[axis] = 0),
          s.slowDownRatio[axis] = 0.95,
          s.slowDownRatioReverse[axis] = 1 - s.slowDownRatio[axis],
          s.speedDecelerationRatio[axis] = 1
        },
        calculateOverBoundsAnimOffset: function (axis, speed) {
          s.backAnimStarted[axis] || (_panOffset[axis] > _currPanBounds.min[axis] ? s.backAnimDestination[axis] = _currPanBounds.min[axis] : _panOffset[axis] < _currPanBounds.max[axis] && (s.backAnimDestination[axis] = _currPanBounds.max[axis]), void 0 !== s.backAnimDestination[axis] && (s.slowDownRatio[axis] = 0.7, s.slowDownRatioReverse[axis] = 1 - s.slowDownRatio[axis], s.speedDecelerationRatioAbs[axis] < 0.05 && (s.lastFlickSpeed[axis] = 0, s.backAnimStarted[axis] = !0, _animateProp('bounceZoomPan' + axis, _panOffset[axis], s.backAnimDestination[axis], speed || 300, framework.easing.sine.out, function (pos) {
            _panOffset[axis] = pos,
            _applyCurrentZoomPan()
          }))))
        },
        calculateAnimOffset: function (axis) {
          s.backAnimStarted[axis] || (s.speedDecelerationRatio[axis] = s.speedDecelerationRatio[axis] * (s.slowDownRatio[axis] + s.slowDownRatioReverse[axis] - s.slowDownRatioReverse[axis] * s.timeDiff / 10), s.speedDecelerationRatioAbs[axis] = Math.abs(s.lastFlickSpeed[axis] * s.speedDecelerationRatio[axis]), s.distanceOffset[axis] = s.lastFlickSpeed[axis] * s.speedDecelerationRatio[axis] * s.timeDiff, _panOffset[axis] += s.distanceOffset[axis])
        },
        panAnimLoop: function () {
          return _animations.zoomPan && (_animations.zoomPan.raf = _requestAF(s.panAnimLoop), s.now = _getCurrentTime(), s.timeDiff = s.now - s.lastNow, s.lastNow = s.now, s.calculateAnimOffset('x'), s.calculateAnimOffset('y'), _applyCurrentZoomPan(), s.calculateOverBoundsAnimOffset('x'), s.calculateOverBoundsAnimOffset('y'), s.speedDecelerationRatioAbs.x < 0.05 && s.speedDecelerationRatioAbs.y < 0.05) ? (_panOffset.x = Math.round(_panOffset.x), _panOffset.y = Math.round(_panOffset.y), _applyCurrentZoomPan(), void _stopAnimation('zoomPan'))  : void 0
        }
      };
      return s
    },
    _completePanGesture = function (animData) {
      return animData.calculateSwipeSpeed('y'),
      _currPanBounds = self.currItem.bounds,
      animData.backAnimDestination = {
      },
      animData.backAnimStarted = {
      },
      Math.abs(animData.lastFlickSpeed.x) <= 0.05 && Math.abs(animData.lastFlickSpeed.y) <= 0.05 ? (animData.speedDecelerationRatioAbs.x = animData.speedDecelerationRatioAbs.y = 0, animData.calculateOverBoundsAnimOffset('x'), animData.calculateOverBoundsAnimOffset('y'), !0)  : (_registerStartAnimation('zoomPan'), animData.lastNow = _getCurrentTime(), void animData.panAnimLoop())
    },
    _finishSwipeMainScrollGesture = function (gestureType, _releaseAnimData) {
      var itemChanged;
      _mainScrollAnimating || (_currZoomedItemIndex = _currentItemIndex);
      var itemsDiff;
      if ('swipe' === gestureType) {
        var totalShiftDist = _currPoint.x - _startPoint.x,
        isFastLastFlick = _releaseAnimData.lastFlickDist.x < 10;
        totalShiftDist > MIN_SWIPE_DISTANCE && (isFastLastFlick || _releaseAnimData.lastFlickOffset.x > 20) ? itemsDiff = - 1 : - MIN_SWIPE_DISTANCE > totalShiftDist && (isFastLastFlick || _releaseAnimData.lastFlickOffset.x < - 20) && (itemsDiff = 1)
      }
      var nextCircle;
      itemsDiff && (_currentItemIndex += itemsDiff, 0 > _currentItemIndex ? (_currentItemIndex = _options.loop ? _getNumItems() - 1 : 0, nextCircle = !0)  : _currentItemIndex >= _getNumItems() && (_currentItemIndex = _options.loop ? 0 : _getNumItems() - 1, nextCircle = !0), nextCircle && !_options.loop || (_indexDiff += itemsDiff, _currPositionIndex -= itemsDiff, itemChanged = !0));
      var finishAnimDuration,
      animateToX = _slideSize.x * _currPositionIndex,
      animateToDist = Math.abs(animateToX - _mainScrollPos.x);
      return itemChanged || animateToX > _mainScrollPos.x == _releaseAnimData.lastFlickSpeed.x > 0 ? (finishAnimDuration = Math.abs(_releaseAnimData.lastFlickSpeed.x) > 0 ? animateToDist / Math.abs(_releaseAnimData.lastFlickSpeed.x)  : 333, finishAnimDuration = Math.min(finishAnimDuration, 400), finishAnimDuration = Math.max(finishAnimDuration, 250))  : finishAnimDuration = 333,
      _currZoomedItemIndex === _currentItemIndex && (itemChanged = !1),
      _mainScrollAnimating = !0,
      _shout('mainScrollAnimStart'),
      _animateProp('mainScroll', _mainScrollPos.x, animateToX, finishAnimDuration, framework.easing.cubic.out, _moveMainScroll, function () {
        _stopAllAnimations(),
        _mainScrollAnimating = !1,
        _currZoomedItemIndex = - 1,
        (itemChanged || _currZoomedItemIndex !== _currentItemIndex) && self.updateCurrItem(),
        _shout('mainScrollAnimComplete')
      }),
      itemChanged && self.updateCurrItem(!0),
      itemChanged
    },
    _calculateZoomLevel = function (touchesDistance) {
      return 1 / _startPointsDistance * touchesDistance * _startZoomLevel
    },
    _completeZoomGesture = function () {
      var destZoomLevel = _currZoomLevel,
      minZoomLevel = _getMinZoomLevel(),
      maxZoomLevel = _getMaxZoomLevel();
      minZoomLevel > _currZoomLevel ? destZoomLevel = minZoomLevel : _currZoomLevel > maxZoomLevel && (destZoomLevel = maxZoomLevel);
      var onUpdate,
      destOpacity = 1,
      initialOpacity = _bgOpacity;
      return _opacityChanged && !_isZoomingIn && !_wasOverInitialZoom && minZoomLevel > _currZoomLevel ? (self.close(), !0)  : (_opacityChanged && (onUpdate = function (now) {
        _applyBgOpacity((destOpacity - initialOpacity) * now + initialOpacity)
      }), self.zoomTo(destZoomLevel, 0, 200, framework.easing.cubic.out, onUpdate), !0)
    };
    _registerModule('Gestures', {
      publicMethods: {
        initGestures: function () {
          var addEventNames = function (pref, down, move, up, cancel) {
            _dragStartEvent = pref + down,
            _dragMoveEvent = pref + move,
            _dragEndEvent = pref + up,
            _dragCancelEvent = cancel ? pref + cancel : ''
          };
          _pointerEventEnabled = _features.pointerEvent,
          _pointerEventEnabled && _features.touch && (_features.touch = !1),
          _pointerEventEnabled ? navigator.pointerEnabled ? addEventNames('pointer', 'down', 'move', 'up', 'cancel')  : addEventNames('MSPointer', 'Down', 'Move', 'Up', 'Cancel')  : _features.touch ? (addEventNames('touch', 'start', 'move', 'end', 'cancel'), _likelyTouchDevice = !0)  : addEventNames('mouse', 'down', 'move', 'up'),
          _upMoveEvents = _dragMoveEvent + ' ' + _dragEndEvent + ' ' + _dragCancelEvent,
          _downEvents = _dragStartEvent,
          _pointerEventEnabled && !_likelyTouchDevice && (_likelyTouchDevice = navigator.maxTouchPoints > 1 || navigator.msMaxTouchPoints > 1),
          self.likelyTouchDevice = _likelyTouchDevice,
          _globalEventHandlers[_dragStartEvent] = _onDragStart,
          _globalEventHandlers[_dragMoveEvent] = _onDragMove,
          _globalEventHandlers[_dragEndEvent] = _onDragRelease,
          _dragCancelEvent && (_globalEventHandlers[_dragCancelEvent] = _globalEventHandlers[_dragEndEvent]),
          _features.touch && (_downEvents += ' mousedown', _upMoveEvents += ' mousemove mouseup', _globalEventHandlers.mousedown = _globalEventHandlers[_dragStartEvent], _globalEventHandlers.mousemove = _globalEventHandlers[_dragMoveEvent], _globalEventHandlers.mouseup = _globalEventHandlers[_dragEndEvent]),
          _likelyTouchDevice || (_options.allowPanToNext = !1)
        }
      }
    });
    var _showOrHideTimeout,
    _items,
    _initialContentSet,
    _initialZoomRunning,
    _getItemAt,
    _getNumItems,
    _initialIsLoop,
    _showOrHide = function (item, img, out, completeFn) {
      _showOrHideTimeout && clearTimeout(_showOrHideTimeout),
      _initialZoomRunning = !0,
      _initialContentSet = !0;
      var thumbBounds,
      duration = out ? _options.hideAnimationDuration : _options.showAnimationDuration,
      onComplete = function () {
        _stopAnimation('initialZoom'),
        out ? (self.template.removeAttribute('style'), self.bg.removeAttribute('style'))  : (_applyBgOpacity(1), img && (img.style.display = 'block'), framework.addClass(template, 'pswp--animated-in'), _shout('initialZoom' + (out ? 'OutEnd' : 'InEnd'))),
        completeFn && completeFn(),
        _initialZoomRunning = !1
      };
      if (!duration || !thumbBounds || void 0 === thumbBounds.x) return _shout('initialZoom' + (out ? 'Out' : 'In')),
      _currZoomLevel = item.initialZoomLevel,
      _equalizePoints(_panOffset, item.initialPosition),
      _applyCurrentZoomPan(),
      template.style.opacity = out ? 0 : 1,
      _applyBgOpacity(1),
      void (duration ? setTimeout(function () {
        onComplete()
      }, duration)  : onComplete());
      var startAnimation = function () {
        var closeWithRaf = _closedByScroll,
        fadeEverything = !self.currItem.src || self.currItem.loadError || _options.showHideOpacity;
        item.miniImg && (item.miniImg.style.webkitBackfaceVisibility = 'hidden'),
        out || (_currZoomLevel = thumbBounds.w / item.w, _panOffset.x = thumbBounds.x, _panOffset.y = thumbBounds.y - _initalWindowScrollY, self[fadeEverything ? 'template' : 'bg'].style.opacity = 0.001, _applyCurrentZoomPan()),
        _registerStartAnimation('initialZoom'),
        out && !closeWithRaf && framework.removeClass(template, 'pswp--animated-in'),
        fadeEverything && (out ? framework[(closeWithRaf ? 'remove' : 'add') + 'Class'](template, 'pswp--animate_opacity')  : setTimeout(function () {
          framework.addClass(template, 'pswp--animate_opacity')
        }, 30)),
        _showOrHideTimeout = setTimeout(function () {
          if (_shout('initialZoom' + (out ? 'Out' : 'In')), out) {
            var destZoomLevel = thumbBounds.w / item.w,
            initialPanOffset = {
              x: _panOffset.x,
              y: _panOffset.y
            },
            initialZoomLevel = _currZoomLevel,
            initalBgOpacity = _bgOpacity,
            onUpdate = function (now) {
              1 === now ? (_currZoomLevel = destZoomLevel, _panOffset.x = thumbBounds.x, _panOffset.y = thumbBounds.y - _currentWindowScrollY)  : (_currZoomLevel = (destZoomLevel - initialZoomLevel) * now + initialZoomLevel, _panOffset.x = (thumbBounds.x - initialPanOffset.x) * now + initialPanOffset.x, _panOffset.y = (thumbBounds.y - _currentWindowScrollY - initialPanOffset.y) * now + initialPanOffset.y),
              _applyCurrentZoomPan(),
              fadeEverything ? template.style.opacity = 1 - now : _applyBgOpacity(initalBgOpacity - now * initalBgOpacity)
            };
            closeWithRaf ? _animateProp('initialZoom', 0, 1, duration, framework.easing.cubic.out, onUpdate, onComplete)  : (onUpdate(1), _showOrHideTimeout = setTimeout(onComplete, duration + 20))
          } else _currZoomLevel = item.initialZoomLevel,
          _equalizePoints(_panOffset, item.initialPosition),
          _applyCurrentZoomPan(),
          _applyBgOpacity(1),
          fadeEverything ? template.style.opacity = 1 : _applyBgOpacity(1),
          _showOrHideTimeout = setTimeout(onComplete, duration + 20)
        }, out ? 25 : 90)
      };
      startAnimation()
    },
    _tempPanAreaSize = {
    },
    _imagesToAppendPool = [
    ],
    _controllerDefaultOptions = {
      index: 0,
      errorMsg: '<div class="pswp__error-msg"><a href="%url%" target="_blank">The image</a> could not be loaded.</div>',
      forceProgressiveLoading: !1,
      preload: [
        1,
        1
      ],
      getNumItemsFn: function () {
        return _items.length
      }
    },
    _getZeroBounds = function () {
      return {
        center: {
          x: 0,
          y: 0
        },
        max: {
          x: 0,
          y: 0
        },
        min: {
          x: 0,
          y: 0
        }
      }
    },
    _calculateSingleItemPanBounds = function (item, realPanElementW, realPanElementH) {
      var bounds = item.bounds;
      bounds.center.x = Math.round((_tempPanAreaSize.x - realPanElementW) / 2),
      bounds.center.y = Math.round((_tempPanAreaSize.y - realPanElementH) / 2) + item.vGap.top,
      bounds.max.x = realPanElementW > _tempPanAreaSize.x ? Math.round(_tempPanAreaSize.x - realPanElementW)  : bounds.center.x,
      bounds.max.y = realPanElementH > _tempPanAreaSize.y ? Math.round(_tempPanAreaSize.y - realPanElementH) + item.vGap.top : bounds.center.y,
      bounds.min.x = realPanElementW > _tempPanAreaSize.x ? 0 : bounds.center.x,
      bounds.min.y = realPanElementH > _tempPanAreaSize.y ? item.vGap.top : bounds.center.y
    },
    _calculateItemSize = function (item, viewportSize, zoomLevel) {
      if (item.src && !item.loadError) {
        var isInitial = !zoomLevel;
        if (isInitial && (item.vGap || (item.vGap = {
          top: 0,
          bottom: 0
        }), _shout('parseVerticalMargin', item)), _tempPanAreaSize.x = viewportSize.x, _tempPanAreaSize.y = viewportSize.y - item.vGap.top - item.vGap.bottom, isInitial) {
          var hRatio = _tempPanAreaSize.x / item.w,
          vRatio = _tempPanAreaSize.y / item.h;
          item.fitRatio = vRatio > hRatio ? hRatio : vRatio;
          var scaleMode = _options.scaleMode;
          'orig' === scaleMode ? zoomLevel = 1 : 'fit' === scaleMode && (zoomLevel = item.fitRatio),
          zoomLevel > 1 && (zoomLevel = 1),
          item.initialZoomLevel = zoomLevel,
          item.bounds || (item.bounds = _getZeroBounds())
        }
        if (!zoomLevel) return;
        return _calculateSingleItemPanBounds(item, item.w * zoomLevel, item.h * zoomLevel),
        isInitial && zoomLevel === item.initialZoomLevel && (item.initialPosition = item.bounds.center),
        item.bounds
      }
      return item.w = item.h = 0,
      item.initialZoomLevel = item.fitRatio = 1,
      item.bounds = _getZeroBounds(),
      item.initialPosition = item.bounds.center,
      item.bounds
    },
    _appendImage = function (index, item, baseDiv, img, preventAnimation, keepPlaceholder) {
      item.loadError || img && (item.imageAppended = !0, _setImageSize(item, img, item === self.currItem && _renderMaxResolution), baseDiv.appendChild(img), keepPlaceholder && setTimeout(function () {
        item && item.loaded && item.placeholder && (item.placeholder.style.display = 'none', item.placeholder = null)
      }, 500))
    },
    _preloadImage = function (item) {
      item.loading = !0,
      item.loaded = !1;
      var img = item.img = framework.createEl('pswp__img', 'img'),
      onComplete = function () {
        item.loading = !1,
        item.loaded = !0,
        item.loadComplete ? item.loadComplete(item)  : item.img = null,
        img.onload = img.onerror = null,
        img = null
      };
      return img.onload = onComplete,
      img.onerror = function () {
        item.loadError = !0,
        onComplete()
      },
      img.src = item.src,
      img
    },
    _checkForError = function (item, cleanUp) {
      return item.src && item.loadError && item.container ? (cleanUp && (item.container.innerHTML = ''), item.container.innerHTML = _options.errorMsg.replace('%url%', item.src), !0)  : void 0
    },
    _setImageSize = function (item, img, maxRes) {
      if (item.src) {
        img || (img = item.container.lastChild);
        var w = maxRes ? item.w : Math.round(item.w * item.fitRatio),
        h = maxRes ? item.h : Math.round(item.h * item.fitRatio);
        item.placeholder && !item.loaded && (item.placeholder.style.width = w + 'px', item.placeholder.style.height = h + 'px'),
        img.style.width = w + 'px',
        img.style.height = h + 'px'
      }
    },
    _appendImagesPool = function () {
      if (_imagesToAppendPool.length) {
        for (var poolItem, i = 0; i < _imagesToAppendPool.length; i++) poolItem = _imagesToAppendPool[i],
        poolItem.holder.index === poolItem.index && _appendImage(poolItem.index, poolItem.item, poolItem.baseDiv, poolItem.img, !1, poolItem.clearPlaceholder);
        _imagesToAppendPool = [
        ]
      }
    };
    _registerModule('Controller', {
      publicMethods: {
        lazyLoadItem: function (index) {
          index = _getLoopedId(index);
          var item = _getItemAt(index);
          item && (!item.loaded && !item.loading || _itemsNeedUpdate) && (_shout('gettingData', index, item), item.src && _preloadImage(item))
        },
        initController: function () {
          framework.extend(_options, _controllerDefaultOptions, !0),
          self.items = _items = items,
          _getItemAt = self.getItemAt,
          _getNumItems = _options.getNumItemsFn,
          _initialIsLoop = _options.loop,
          _getNumItems() < 3 && (_options.loop = !1),
          _listen('beforeChange', function (diff) {
            var i,
            p = _options.preload,
            isNext = null === diff ? !0 : diff >= 0,
            preloadBefore = Math.min(p[0], _getNumItems()),
            preloadAfter = Math.min(p[1], _getNumItems());
            for (i = 1; (isNext ? preloadAfter : preloadBefore) >= i; i++) self.lazyLoadItem(_currentItemIndex + i);
            for (i = 1; (isNext ? preloadBefore : preloadAfter) >= i; i++) self.lazyLoadItem(_currentItemIndex - i)
          }),
          _listen('initialLayout', function () {
            self.currItem.initialLayout = _options.getThumbBoundsFn && _options.getThumbBoundsFn(_currentItemIndex)
          }),
          _listen('mainScrollAnimComplete', _appendImagesPool),
          _listen('initialZoomInEnd', _appendImagesPool),
          _listen('destroy', function () {
            for (var item, i = 0; i < _items.length; i++) item = _items[i],
            item.container && (item.container = null),
            item.placeholder && (item.placeholder = null),
            item.img && (item.img = null),
            item.preloader && (item.preloader = null),
            item.loadError && (item.loaded = item.loadError = !1);
            _imagesToAppendPool = null
          })
        },
        getItemAt: function (index) {
          return index >= 0 && void 0 !== _items[index] ? _items[index] : !1
        },
        allowProgressiveImg: function () {
          return _options.forceProgressiveLoading || !_likelyTouchDevice || _options.mouseUsed || screen.width > 1200
        },
        setContent: function (holder, index) {
          _options.loop && (index = _getLoopedId(index));
          var prevItem = self.getItemAt(holder.index);
          prevItem && (prevItem.container = null);
          var img,
          item = self.getItemAt(index);
          if (!item) return void (holder.el.innerHTML = '');
          _shout('gettingData', index, item),
          holder.index = index,
          holder.item = item;
          var baseDiv = item.container = framework.createEl('pswp__zoom-wrap');
          if (!item.src && item.html && (item.html.tagName ? baseDiv.appendChild(item.html)  : baseDiv.innerHTML = item.html), _checkForError(item), _calculateItemSize(item, _viewportSize), !item.src || item.loadError || item.loaded) item.src && !item.loadError && (img = framework.createEl('pswp__img', 'img'), img.style.opacity = 1, img.src = item.src, _setImageSize(item, img), _appendImage(index, item, baseDiv, img, !0));
           else {
            if (item.loadComplete = function (item) {
              if (_isOpen) {
                if (holder && holder.index === index) {
                  if (_checkForError(item, !0)) return item.loadComplete = item.img = null,
                  _calculateItemSize(item, _viewportSize),
                  _applyZoomPanToItem(item),
                  void (holder.index === _currentItemIndex && self.updateCurrZoomItem());
                  item.imageAppended ? !_initialZoomRunning && item.placeholder && (item.placeholder.style.display = 'none', item.placeholder = null)  : _features.transform && (_mainScrollAnimating || _initialZoomRunning) ? _imagesToAppendPool.push({
                    item: item,
                    baseDiv: baseDiv,
                    img: item.img,
                    index: index,
                    holder: holder,
                    clearPlaceholder: !0
                  })  : _appendImage(index, item, baseDiv, item.img, _mainScrollAnimating || _initialZoomRunning, !0)
                }
                item.loadComplete = null,
                item.img = null,
                _shout('imageLoadComplete', index, item)
              }
            }, framework.features.transform) {
              var placeholderClassName = 'pswp__img pswp__img--placeholder';
              placeholderClassName += item.msrc ? '' : ' pswp__img--placeholder--blank';
              var placeholder = framework.createEl(placeholderClassName, item.msrc ? 'img' : '');
              item.msrc && (placeholder.src = item.msrc),
              _setImageSize(item, placeholder),
              baseDiv.appendChild(placeholder),
              item.placeholder = placeholder
            }
            item.loading || _preloadImage(item),
            self.allowProgressiveImg() && (!_initialContentSet && _features.transform ? _imagesToAppendPool.push({
              item: item,
              baseDiv: baseDiv,
              img: item.img,
              index: index,
              holder: holder
            })  : _appendImage(index, item, baseDiv, item.img, !0, !0))
          }
          _initialContentSet || index !== _currentItemIndex ? _applyZoomPanToItem(item)  : (_currZoomElementStyle = baseDiv.style, _showOrHide(item, img || item.img)),
          holder.el.innerHTML = '',
          holder.el.appendChild(baseDiv)
        },
        cleanSlide: function (item) {
          item.img && (item.img.onload = item.img.onerror = null),
          item.loaded = item.loading = item.img = item.imageAppended = !1
        }
      }
    });
    var tapTimer,
    tapReleasePoint = {
    },
    _dispatchTapEvent = function (origEvent, releasePoint, pointerType) {
      var e = document.createEvent('CustomEvent'),
      eDetail = {
        origEvent: origEvent,
        target: origEvent.target,
        releasePoint: releasePoint,
        pointerType: pointerType || 'touch'
      };
      e.initCustomEvent('pswpTap', !0, !0, eDetail),
      origEvent.target.dispatchEvent(e)
    };
    _registerModule('Tap', {
      publicMethods: {
        initTap: function () {
          _listen('firstTouchStart', self.onTapStart),
          _listen('touchRelease', self.onTapRelease),
          _listen('destroy', function () {
            tapReleasePoint = {
            },
            tapTimer = null
          })
        },
        onTapStart: function (touchList) {
          touchList.length > 1 && (clearTimeout(tapTimer), tapTimer = null)
        },
        onTapRelease: function (e, releasePoint) {
          if (releasePoint && !_moved && !_isMultitouch && !_numAnimations) {
            var p0 = releasePoint;
            if (tapTimer && (clearTimeout(tapTimer), tapTimer = null, _isNearbyPoints(p0, tapReleasePoint))) return void _shout('doubleTap', p0);
            if ('mouse' === releasePoint.type) return void _dispatchTapEvent(e, releasePoint, 'mouse');
            var clickedTagName = e.target.tagName.toUpperCase();
            if ('BUTTON' === clickedTagName || framework.hasClass(e.target, 'pswp__single-tap')) return void _dispatchTapEvent(e, releasePoint);
            _equalizePoints(tapReleasePoint, p0),
            tapTimer = setTimeout(function () {
              _dispatchTapEvent(e, releasePoint),
              tapTimer = null
            }, 300)
          }
        }
      }
    });
    var _wheelDelta;
    _registerModule('DesktopZoom', {
      publicMethods: {
        initDesktopZoom: function () {
          _oldIE || (_likelyTouchDevice ? _listen('mouseUsed', function () {
            self.setupDesktopZoom()
          })  : self.setupDesktopZoom(!0))
        },
        setupDesktopZoom: function (onInit) {
          _wheelDelta = {
          };
          var events = 'wheel mousewheel DOMMouseScroll';
          _listen('bindEvents', function () {
            framework.bind(template, events, self.handleMouseWheel)
          }),
          _listen('unbindEvents', function () {
            _wheelDelta && framework.unbind(template, events, self.handleMouseWheel)
          }),
          self.mouseZoomedIn = !1;
          var hasDraggingClass,
          updateZoomable = function () {
            self.mouseZoomedIn && (framework.removeClass(template, 'pswp--zoomed-in'), self.mouseZoomedIn = !1),
            1 > _currZoomLevel ? framework.addClass(template, 'pswp--zoom-allowed')  : framework.removeClass(template, 'pswp--zoom-allowed'),
            removeDraggingClass()
          },
          removeDraggingClass = function () {
            hasDraggingClass && (framework.removeClass(template, 'pswp--dragging'), hasDraggingClass = !1)
          };
          _listen('resize', updateZoomable),
          _listen('afterChange', updateZoomable),
          _listen('pointerDown', function () {
            self.mouseZoomedIn && (hasDraggingClass = !0, framework.addClass(template, 'pswp--dragging'))
          }),
          _listen('pointerUp', removeDraggingClass),
          onInit || updateZoomable()
        },
        handleMouseWheel: function (e) {
          if (_currZoomLevel <= self.currItem.fitRatio) return _options.modal && (!_options.closeOnScroll || _numAnimations || _isDragging ? e.preventDefault()  : _transformKey && Math.abs(e.deltaY) > 2 && (_closedByScroll = !0, self.close())),
          !0;
          if (e.stopPropagation(), _wheelDelta.x = 0, 'deltaX' in e) 1 === e.deltaMode ? (_wheelDelta.x = 18 * e.deltaX, _wheelDelta.y = 18 * e.deltaY)  : (_wheelDelta.x = e.deltaX, _wheelDelta.y = e.deltaY);
           else if ('wheelDelta' in e) e.wheelDeltaX && (_wheelDelta.x = - 0.16 * e.wheelDeltaX),
          e.wheelDeltaY ? _wheelDelta.y = - 0.16 * e.wheelDeltaY : _wheelDelta.y = - 0.16 * e.wheelDelta;
           else {
            if (!('detail' in e)) return;
            _wheelDelta.y = e.detail
          }
          _calculatePanBounds(_currZoomLevel, !0);
          var newPanX = _panOffset.x - _wheelDelta.x,
          newPanY = _panOffset.y - _wheelDelta.y;
          (_options.modal || newPanX <= _currPanBounds.min.x && newPanX >= _currPanBounds.max.x && newPanY <= _currPanBounds.min.y && newPanY >= _currPanBounds.max.y) && e.preventDefault(),
          self.panTo(newPanX, newPanY)
        },
        toggleDesktopZoom: function (centerPoint) {
          centerPoint = centerPoint || {
            x: _viewportSize.x / 2 + _offset.x,
            y: _viewportSize.y / 2 + _offset.y
          };
          var doubleTapZoomLevel = _options.getDoubleTapZoom(!0, self.currItem),
          zoomOut = _currZoomLevel === doubleTapZoomLevel;
          self.mouseZoomedIn = !zoomOut,
          self.zoomTo(zoomOut ? self.currItem.initialZoomLevel : doubleTapZoomLevel, centerPoint, 333),
          framework[(zoomOut ? 'remove' : 'add') + 'Class'](template, 'pswp--zoomed-in')
        }
      }
    });
    var _historyUpdateTimeout,
    _hashAnimCheckTimeout,
    _hashChangedByScript,
    _hashChangedByHistory,
    _hashReseted,
    _initialHash,
    _historyChanged,
    _closedFromURL,
    _urlChangedOnce,
    _windowLoc,
    _supportsPushState,
    _historyDefaultOptions = {
      history: !0,
      galleryUID: 1
    },
    _getHash = function () {
      return _windowLoc.hash.substring(1)
    },
    _cleanHistoryTimeouts = function () {
      _historyUpdateTimeout && clearTimeout(_historyUpdateTimeout),
      _hashAnimCheckTimeout && clearTimeout(_hashAnimCheckTimeout)
    },
    _parseItemIndexFromURL = function () {
      var hash = _getHash(),
      params = {
      };
      if (hash.length < 5) return params;
      var i,
      vars = hash.split('&');
      for (i = 0; i < vars.length; i++) if (vars[i]) {
        var pair = vars[i].split('=');
        pair.length < 2 || (params[pair[0]] = pair[1])
      }
      if (_options.galleryPIDs) {
        var searchfor = params.pid;
        for (params.pid = 0, i = 0; i < _items.length; i++) if (_items[i].pid === searchfor) {
          params.pid = i;
          break
        }
      } else params.pid = parseInt(params.pid, 10) - 1;
      return params.pid < 0 && (params.pid = 0),
      params
    },
    _updateHash = function () {
      return !0
    };
    _registerModule('History', {
      publicMethods: {
        initHistory: function () {
          if (framework.extend(_options, _historyDefaultOptions, !0), _options.history) {
            _windowLoc = window.location,
            _urlChangedOnce = !1,
            _closedFromURL = !1,
            _historyChanged = !1,
            _initialHash = _getHash(),
            _supportsPushState = 'pushState' in history,
            _initialHash.indexOf('gid=') > - 1 && (_initialHash = _initialHash.split('&gid=') [0], _initialHash = _initialHash.split('?gid=') [0]),
            _listen('afterChange', self.updateURL),
            _listen('unbindEvents', function () {
              framework.unbind(window, 'hashchange', self.onHashChange)
            });
            var returnToOriginal = function () {
              _hashReseted = !0,
              _closedFromURL || (_urlChangedOnce ? history.back()  : _initialHash ? _windowLoc.hash = _initialHash : _supportsPushState ? history.pushState('', document.title, _windowLoc.pathname + _windowLoc.search)  : _windowLoc.hash = ''),
              _cleanHistoryTimeouts()
            };
            _listen('unbindEvents', function () {
              _closedByScroll && returnToOriginal()
            }),
            _listen('destroy', function () {
              _hashReseted || returnToOriginal()
            }),
            _listen('firstUpdate', function () {
              _currentItemIndex = _parseItemIndexFromURL().pid
            });
            var index = _initialHash.indexOf('pid=');
            index > - 1 && (_initialHash = _initialHash.substring(0, index), '&' === _initialHash.slice( - 1) && (_initialHash = _initialHash.slice(0, - 1))),
            setTimeout(function () {
              _isOpen && framework.bind(window, 'hashchange', self.onHashChange)
            }, 40)
          }
        },
        onHashChange: function () {
          return _getHash() === _initialHash ? (_closedFromURL = !0, void self.close())  : void (_hashChangedByScript || (_hashChangedByHistory = !0, self.goTo(_parseItemIndexFromURL().pid), _hashChangedByHistory = !1))
        },
        updateURL: function () {
          _cleanHistoryTimeouts(),
          _hashChangedByHistory || (_historyChanged ? _historyUpdateTimeout = setTimeout(_updateHash, 800)  : _updateHash())
        }
      }
    }),
    framework.extend(self, publicMethods)
  };
  return PhotoSwipe
});
