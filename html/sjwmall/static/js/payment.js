function formatNumber(str, glue) {
  if (isNaN(str)) return NaN;
  for (var glue = 'string' == typeof glue ? glue : ',', digits = str.toString().split('.'), integerDigits = digits[0].split(''), threeDigits = [
  ]; integerDigits.length > 3; ) threeDigits.unshift(integerDigits.splice(integerDigits.length - 3, 3).join(''));
  return threeDigits.unshift(integerDigits.join('')),
  digits[0] = threeDigits.join(glue),
  digits.join('.')
}
function sortObject(shipping_option) {
  var data,
  arr = [
  ];
  for (data in shipping_option) arr.push({
    shipping_id: data,
    alias_name: shipping_option[data].alias_name,
    name: shipping_option[data].name,
    value: shipping_option[data].sort
  });
  return arr.sort(function (a, b) {
    return b.value - a.value
  }),
  arr
}
var cart_detail_area = $('#js_cart_detail_area'),
paymentEnable = function (payment_id) {
  var check_cart_total = parseInt(cart_total);
  switch ('undefined' != typeof shops_additional[payment_id] && shops_additional[payment_id].number > 0 && (check_cart_total += 'percent' == shops_additional[payment_id].type ? Math.round(check_cart_total * shops_additional[payment_id].number / 100)  : parseInt(shops_additional[payment_id].number)), payment_id) {
    case 331:
    case 332:
      if ('undefined' == typeof shops_payment_setting[33]) return !1;
      var min = '' == shops_payment_setting[33].min_price || 'undefined' == typeof shops_payment_setting[33].min_price ? 10 : shops_payment_setting[33].min_price,
      max = '' == shops_payment_setting[33].max_price || 'undefined' == typeof shops_payment_setting[33].min_price ? 2000 : shops_payment_setting[33].max_price;
      if (min > check_cart_total || check_cart_total > max) return !1;
      break;
    case 356:
    case 357:
    case 358:
    case 359:
      if ('undefined' == typeof shops_payment_setting[35]) return !1;
      var min = '' == shops_payment_setting[35].min_price || 'undefined' == typeof shops_payment_setting[35].min_price ? 10 : shops_payment_setting[35].min_price,
      max = '' == shops_payment_setting[35].max_price || 'undefined' == typeof shops_payment_setting[35].min_price ? 19999 : shops_payment_setting[35].max_price;
      if (min > check_cart_total || check_cart_total > max) return !1;
      break;
    case 383:
      if (30 > check_cart_total || check_cart_total > 6000) return !1;
      break;
    case 386:
    case 387:
    case 388:
    case 389:
    case 390:
    case 391:
      if ('undefined' == typeof shops_payment_setting[38]) return !1;
      var min = '' == shops_payment_setting[38].min_price || 'undefined' == typeof shops_payment_setting[38].min_price ? 10 : shops_payment_setting[38].min_price,
      max = '' == shops_payment_setting[38].max_price || 'undefined' == typeof shops_payment_setting[38].min_price ? 19999 : shops_payment_setting[38].max_price;
      if (min > check_cart_total || check_cart_total > max) return !1;
      break;
    case 374:
    case 375:
      if (check_cart_total >= 20000 || 30 >= check_cart_total) return !1;
      break;
    case 341:
    case 342:
    case 343:
    case 344:
    case 346:
      if (30 > check_cart_total) return !1;
      break;
    case 345:
      if (30 > check_cart_total || check_cart_total > 30000) return !1;
      break;
    case 347:
      if (30 > check_cart_total || check_cart_total > 20000) return !1
  }
  return !($.inArray(payment_id, [
    354,
    351,
    341,
    384,
    381
  ]) >= 0 && '1' == is_mobile)
},
isCartEmpty = function (cart_number) {
  return 0 == cart_number
},
redirectToIdex = function () {
  $.dialog({
    animation: 'opacity',
    closeAnimation: 'none',
    animationSpeed: 1000,
    title: ' ',
    content: '<span class="content_dialog_inner">' + Lang.get('frontend.errormsg.cart.nodata') + '</span>',
    icon: 'icon-notice',
    theme: 'waca_confirm',
    keyboardEnabled: !0,
    onOpen: function () {
      setTimeout(function () {
        window.location = index_url
      }, 2000)
    }
  })
},
recipientBox = function (is_local) {
  $.confirm({
    animation: 'opacity',
    closeAnimation: 'none',
    animationSpeed: 1000,
    title: ' ',
    content: function () {
      var self = this;
      return $.ajax({
        url: '/cart/member-recipient-ajax',
        data: 'is_local=' + is_local,
        dataType: 'json',
        method: 'post'
      }).done(function (response) {
        self.recipients = response.data,
        self.setContent(response.view),
        self.recipientsElem = self.$content.find('.js_row_recipient'),
        self.recipientsElem.click(function () {
          $(this).addClass('active').siblings().removeClass('active')
        })
      })
    },
    theme: 'waca_confirm',
    keyboardEnabled: !0,
    columnClass: 'col-lg-6 col-lg-offset-3 col-md-6 col-md-offset-3 member_recipient_box',
    confirmButton: '送出',
    cancelButton: '取消',
    confirm: function () {
      var id = '0',
      elem = this.$content.find('.js_row_recipient.active');
      elem.length && (id = elem.data('id').toString()),
      '0' !== id && $.each(this.recipients, function (key, item) {
        if (item.id.toString() === id) {
          recipient_selected_data.set(item);
          var sameInfoElem = $('#same_info');
          $('#receiver_address_zip').val(item.zip).change(),
          $('.js_receiver_city').val(item.city).change(),
          $('.js_receiver_district').val(item.district).change(),
          $('#receiver_address').val(item.address),
          $('#receiver_name').val(item.name),
          $('#receiver_phone').val(item.phone),
          'tw' !== item.country && ($('#int_receiver_country').val(item.country).change(), $('#int_receiver_address').val(item.address), $('#int_receiver_district').val(item.district), $('#int_receiver_city').val(item.city), $('#int_receiver_zip').val(item.zip)),
          $('#purchaser_name').val() === item.name && $('#purchaser_phone').val() === item.phone || sameInfoElem.prop('checked') && sameInfoElem.click()
        }
      })
    },
    confirmButtonClass: 'btn_confirm btn_md',
    cancelButtonClass: 'btn_cancel btn_md'
  })
},
recipient_selected_data = {
  recipient: null,
  set: function (recipent) {
    this.recipient = recipent
  },
  get: function () {
    return this.recipient
  }
};
$('.js_member_recipient').on('click', function () {
  var is_local = $(this).parent().hasClass('js_member_recipient_area') ? 1 : 0;
  recipientBox(is_local)
});
var setPhonePlaceholder = function () {
  var shipping = $('#shipping').val(),
  payment = $('#payment').val(),
  is_same_info = $('#same_info').is(':checked');
  - 1 != $.inArray(shipping, [
    '221',
    '222',
    '241',
    '242',
    '243',
    '244',
    '245',
    '246',
    '247',
    '131'
  ]) || '-1' != shipping.indexOf('custom') && '1' == shipping_options[shipping].setting.in_taiwan ? $('#receiver_phone').attr('placeholder', Lang.get('frontend.cart.placeholder.phone1'))  : $('#receiver_phone').attr('placeholder', Lang.get('frontend.cart.receiver.placeholder.phone')),
  is_same_info && ( - 1 != $.inArray(shipping, [
    '221',
    '222',
    '241',
    '242',
    '243',
    '244',
    '245',
    '246',
    '247',
    '131'
  ]) || '-1' != shipping.indexOf('custom') && '1' == shipping_options[shipping].setting.in_taiwan) ? $('#purchaser_phone').attr('placeholder', Lang.get('frontend.cart.placeholder.phone1'))  : '34' == payment.substr(0, 2) ? $('#purchaser_phone').attr('placeholder', Lang.get('frontend.cart.placeholder.phone2'))  : $('#purchaser_phone').attr('placeholder', Lang.get('frontend.cart.purchaser.placeholder.phone'))
},
setEmailPlaceholder = function () {
  var shipping = $('#shipping').val(),
  payment = $('#payment').val();
  1 == order_email || - 1 != $.inArray(shipping, [
    '221',
    '222'
  ]) || - 1 != $.inArray(payment, [
    '401'
  ]) || - 1 != $.inArray(payment, [
    '601'
  ]) || 'Pay2Go' == invoice_type ? $('#purchaser_email').attr('placeholder', Lang.get('frontend.cart.purchaser.placeholder.email'))  : $('#purchaser_email').attr('placeholder', Lang.get('frontend.cart.purchaser.placeholder.email2'))
},
refreshOrderFields = function () {
  $('#shipping').val();
  has_invoice ? $('#js_invoice_area').removeClass('hide')  : '',
  $('.js_area_7-11, .js_store_area, .js_ezship_area, .js_allpay_area, #receiver_int_address_area, #receiver_int_city_area, #receiver_int_district_area, #receiver_int_country_area, #receiver_address_country_area, #receiver_int_zip_area, #local_address_area, #receiver_address_outer_area, #js_shipping_description_area, #js_payment_description_area, #installment_area, #js_shipping_area, #js_shipping_date_area, .js_member_recipient_area, .js_country_member_recipient_area, .js_recipient_book, .js_is_holiday_send, .js_credit_able_area, .js_paynow_credit_able_area').addClass('hide'),
  $('#receiver_phone_area').find('.help_tips').addClass('hide'),
  $('#int_receiver_country').val('none'),
  $('#is_holiday_send').prop('checked', !1),
  $('#is_holiday_send_val').val('-1'),
  $('.js_paynow_credit_able').data('status', 'off').find('i').attr('class', 'icon-chevron-small-down'),
  $('#js_logistics').attr('name', '').val(''),
  has_invoice && ($('#js_invoice_area, .js_invoice_block').removeClass('hide'), $('.js_information_block').removeClass('col-md-12').addClass('js_information_block col-md-6'))
},
refreshPaymentFields = function () {
  var payment_id = $('#payment').val(),
  is_allpay_collection_shipping = !1,
  shipping_id = $('#shipping').val();
  if ('111' == payment_id || '141' == payment_id ? ($('#js_invoice_area, .js_invoice_block').addClass('hide'), $('.js_information_block').removeClass('col-md-6').addClass('js_information_block col-md-12'))  : has_invoice && ($('#js_invoice_area, .js_invoice_block').removeClass('hide'), $('.js_information_block').removeClass('col-md-12').addClass('js_information_block col-md-6')), $.inArray(payment_id, [
    '355',
    '371',
    '385'
  ]) >= 0) switch ($('#installment_area').removeClass('hide'), $('#js-allpay-installment-select, #js-pay2go-installment-select, #js-ecpay-installment-select').addClass('hide'), payment_id) {
    case '355':
      $('#js-allpay-installment-select').removeClass('hide');
      break;
    case '385':
      $('#js-ecpay-installment-select').removeClass('hide');
      break;
    case '371':
      $('#js-pay2go-installment-select').removeClass('hide')
  } else $('#installment_area').addClass('hide');
  - 1 == $.inArray(payment_id, Object.keys(allpay_collection_map)) && - 1 == $.inArray(payment_id, Object.keys(ecpay_collection_map)) ? $('input[name=IsCollection]').val('N')  : $('input[name=IsCollection]').val('Y');
  for (key in allpay_collection_map) allpay_collection_map[key] == shipping_id && (is_allpay_collection_shipping = !0);
  for (key in ecpay_collection_map) ecpay_collection_map[key] == shipping_id && (is_allpay_collection_shipping = !0);
  if (is_allpay_collection_shipping && 'none' != payment_id) {
    $('.js_allpay_area').removeClass('hide');
    var extra_data = shipping_id + ',' + payment_id;
    $('input[name=ExtraData]').val(extra_data)
} else $('.js_allpay_area').addClass('hide');
'231' != shipping_id && - 1 == shipping_id.indexOf('custom') || - 1 == $.inArray(payment_id, [
  '341',
  '342',
  '351',
  '352',
  '372',
  '373',
  '381',
  '382'
]) ? '231' != shipping_id && - 1 == shipping_id.indexOf('custom') || - 1 == $.inArray(payment_id, [
  '345',
  '347',
  '353',
  '354',
  '374',
  '375',
  '383',
  '384'
]) ? $('#js_payment_description_area').addClass('hide')  : ($('#js_payment_description').html(Lang.get('frontend.cart.payment.description.convenient_store')), $('#js_payment_description_area').removeClass('hide'))  : ($('#js_payment_description').html(Lang.get('frontend.cart.payment.description.atm')), $('#js_payment_description_area').removeClass('hide'))
},
count_main_product_qty = function (standard_id) {
return parseInt($('.js_cart_list_quantity[data-standard-id=' + standard_id + ']').val())
},
count_add_price_qty = function (standard_id) {
var add_price_qty = 0;
return $('.js_cart_list_add_price_quantity[data-standard-id=' + standard_id + ']').each(function () {
  add_price_qty += parseInt($(this).val())
}),
add_price_qty
},
checkShippingForTwIslands = function () {
var shipping = $('#shipping').val(),
country = get_shipping_country();
return 'tw_islands' != country ? !0 : 222 == shipping ? ($.dialog({
  animation: 'opacity',
  closeAnimation: 'none',
  animationSpeed: 1000,
  title: ' ',
  icon: 'icon-notice',
  theme: 'waca_confirm',
  keyboardEnabled: !0,
  content: '<span class="content_dialog_inner">該運送方式不可寄送到外島</span>',
  onOpen: function () {
    var that = this;
    setTimeout(function () {
      that.close()
    }, 2000)
  }
}), $('#js_twzipcode').twzipcode('reset'), $('#receiver_address').val(''), !1)  : !0
},
get_shipping_country = function () {
var tw_islands_area = [
  '澎湖縣',
  '連江縣',
  '金門縣',
  '琉球鄉',
  '綠島鄉',
  '蘭嶼鄉',
  '釣魚臺列嶼',
  '東沙群島',
  '南沙群島'
],
shipping_id = $('#shipping').val(),
country = 'tw';
switch (shipping_id) {
  case '111':
  case '121':
  case '241':
  case '265':
  case '268':
  case '222':
    var city = $('.js_receiver_city').val(),
    district = $('.js_receiver_district').val();
    ($.inArray(city, tw_islands_area) >= 0 || $.inArray(district, tw_islands_area) >= 0) && (country = 'tw_islands');
    break;
  case '221':
    var address = $('input.js_ezship_address').val();
    '' != address && $.each(tw_islands_area, function (key, val) {
      address.match(val) && (country = 'tw_islands')
    });
    break;
  case '242':
  case '243':
  case '244':
  case '245':
  case '246':
  case '247':
    var address = $('#CVSAddress').val();
    '' != address && $.each(tw_islands_area, function (key, val) {
      address.match(val) && (country = 'tw_islands')
    });
    break;
  case '231':
    var receiver_country = $('#int_receiver_country').val();
    'none' != receiver_country && (country = receiver_country);
    break;
  default:
    if ( - 1 != shipping_id.indexOf('custom') && '1' == shipping_options[shipping_id].setting.in_taiwan && '1' == shipping_options[shipping_id].setting.has_address) {
      var city = $('.js_receiver_city').val(),
      district = $('.js_receiver_district').val();
      ($.inArray(city, tw_islands_area) >= 0 || $.inArray(district, tw_islands_area) >= 0) && (country = 'tw_islands')
    }
}
return country
},
shippingMessage = function (message) {
$.dialog({
  animation: 'opacity',
  closeAnimation: 'none',
  animationSpeed: 1000,
  title: ' ',
  content: '<span class="content_dialog_inner">' + message + '</span>',
  icon: 'icon-notice',
  theme: 'waca_confirm',
  keyboardEnabled: !0,
  onOpen: function () {
    var that = this;
    setTimeout(function () {
      that.close()
    }, 2000)
  }
})
},
refreshCartList = function (data) {
$('#shipping_area').removeClass('form_valid'),
'undefined' == typeof data.status || data.status ? ($('.js_total_list_checkbox').prop('checked', !1), $('#js_cart_detail_area').html(data.view), $('.js_total').html(currency_icon + formatNumber(cart_total)), $('.js_item_total_currency').html(item_total_currency_html), refreshSelectorSyncSpan(), isCartEmpty(data.cart_number) && redirectToIdex())  : $('#shipping_area').addClass('form_valid')
},
getParams = function () {
var shipping_id = $('#shipping').val(),
params = {
  coupon: $.trim($('.js_coupon').val()),
  redeem_discount: $('.js_redeem_discount').prop('checked') ? '1' : '-1',
  shipping_id: 'none' == shipping_id ? '' : shipping_id,
  shipping_country: get_shipping_country(),
  payment_id: '',
  payment_installment: '',
  view: 'cart_list'
};
return params
},
shippingAndPaymentRelation = {
none: {
  payments: [
  ],
  action: function (setting) {
  }
},
111: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    601
  ],
  action: function (setting) {
    $('#receiver_address_outer_area, #local_address_area, .js_member_recipient_area, .js_recipient_book').removeClass('hide'),
    'undefined' != typeof shipping_options[111].setting.is_holiday_send && '1' == shipping_options[111].setting.is_holiday_send && ($('.js_is_holiday_send').removeClass('hide'), $('#is_holiday_send_val').val(0)),
    'undefined' != typeof shipping_options[111].setting.time && '1' == shipping_options[111].setting.time && ($('#js_shipping_area').removeClass('hide'), $('#shipping_time').attr('name', 'shipping_info[111][time]')),
    'undefined' != typeof shipping_options[111].setting.date && '1' == shipping_options[111].setting.date && ($('#js_shipping_date_area').removeClass('hide'), $('#shipping_date').attr('name', 'shipping_info[111][date]'), 'undefined' != typeof shipping_options[111].setting.is_holiday_send && '1' == shipping_options[111].setting.is_holiday_send ? $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0])  : $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0,
    6]));
    var twland_not_send = 'undefined' != typeof shipping_options[111].setting.twland_not_send && '1' == shipping_options[111].setting.twland_not_send;
    twzipcode(twland_not_send, !1),
    $('#js_logistics').attr('name', 'shipping_info[111][logistics]').val('undefined' != typeof shipping_options[111].setting.logistics ? shipping_options[111].setting.logistics : '')
  }
},
121: {
  payments: [
    131
  ],
  action: function (setting) {
    $('#receiver_address_outer_area, #local_address_area, .js_member_recipient_area, .js_recipient_book').removeClass('hide'),
    'undefined' != typeof shipping_options[121].setting.is_holiday_send && '1' == shipping_options[121].setting.is_holiday_send && ($('.js_is_holiday_send').removeClass('hide'), $('#is_holiday_send_val').val(0)),
    'undefined' != typeof shipping_options[121].setting.time && '1' == shipping_options[121].setting.time && ($('#js_shipping_area').removeClass('hide'), $('#shipping_time').attr('name', 'shipping_info[121][time]')),
    'undefined' != typeof shipping_options[121].setting.date && '1' == shipping_options[121].setting.date && ($('#js_shipping_date_area').removeClass('hide'), $('#shipping_date').attr('name', 'shipping_info[121][date]'), 'undefined' != typeof shipping_options[121].setting.is_holiday_send && '1' == shipping_options[121].setting.is_holiday_send ? $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0])  : $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0,
    6]));
    var twland_not_send = 'undefined' != typeof shipping_options[121].setting.twland_not_send && '1' == shipping_options[121].setting.twland_not_send;
    twzipcode(twland_not_send, !1),
    $('#js_logistics').attr('name', 'shipping_info[121][logistics]').val('undefined' != typeof shipping_options[121].setting.logistics ? shipping_options[121].setting.logistics : '')
  }
},
131: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    601
  ],
  action: function (setting) {
    $('.js_area_7-11').removeClass('hide')
  }
},
211: {
  payments: [
    111,
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    601
  ],
  action: function (setting) {
    $('.js_store_area').removeClass('hide'),
    'undefined' != typeof shipping_options[211].setting[0].description && '' != shipping_options[211].setting[0].description && ($('#js_shipping_description_area').removeClass('hide'), $('#js_shipping_description').text(shipping_options[211].setting[0].description))
  }
},
221: {
  payments: [
    311,
    321,
    331,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    601
  ],
  action: function (setting) {
    $('.js_ezship_area').removeClass('hide')
  }
},
222: {
  payments: [
    311,
    321,
    332,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    601
  ],
  action: function (setting) {
    $('#receiver_address_outer_area, #local_address_area, .js_member_recipient_area, .js_recipient_book').removeClass('hide'),
    $('#js_logistics').attr('name', 'shipping_info[222][logistics]').val('undefined' != typeof shipping_options[222].setting.logistics ? shipping_options[222].setting.logistics : '')
  }
},
231: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    346,
    351,
    352,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    385,
    401,
    601
  ],
  action: function (setting) {
    $('#receiver_int_address_area, #receiver_int_city_area, #receiver_int_district_area, #receiver_int_country_area, #receiver_int_zip_area, .js_country_member_recipient_area, .js_recipient_book').removeClass('hide')
  }
},
242: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    356,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    386,
    401,
    601
  ],
  action: function (setting) {
    allpay_shipping.changeCVSInput(242)
  }
},
243: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    357,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    387,
    401,
    601
  ],
  action: function (setting) {
    allpay_shipping.changeCVSInput(243)
  }
},
244: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    358,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    388,
    401,
    601
  ],
  action: function (setting) {
    allpay_shipping.changeCVSInput(244)
  }
},
245: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    359,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    389,
    401,
    601
  ],
  action: function (setting) {
    allpay_shipping.changeCVSInput(245),
    $('#receiver_phone_area').find('.help_tips').removeClass('hide')
  }
},
246: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    359,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    390,
    401,
    601
  ],
  action: function (setting) {
    allpay_shipping.changeCVSInput(246),
    $('#receiver_phone_area').find('.help_tips').removeClass('hide')
  }
},
247: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    359,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    391,
    401,
    601
  ],
  action: function (setting) {
    allpay_shipping.changeCVSInput(247),
    $('#receiver_phone_area').find('.help_tips').removeClass('hide')
  }
},
265: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    505,
    601
  ],
  action: function (setting) {
    $('#receiver_address_outer_area, #local_address_area').removeClass('hide'),
    'undefined' != typeof shipping_options[265].setting.time && '1' == shipping_options[265].setting.time && ($('#js_shipping_area').removeClass('hide'), $('#shipping_time').attr('name', 'shipping_info[265][time]')),
    $('#js_logistics').attr('name', 'shipping_info[265][logistics]').val('undefined' != typeof shipping_options[265].setting.logistics ? shipping_options[265].setting.logistics : '')
  }
},
268: {
  payments: [
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    508,
    601
  ],
  action: function (setting) {
    $('#receiver_address_outer_area, #local_address_area').removeClass('hide'),
    'undefined' != typeof shipping_options[268].setting.time && '1' == shipping_options[268].setting.time && ($('#js_shipping_area').removeClass('hide'), $('#shipping_time').attr('name', 'shipping_info[268][time]')),
    $('#js_logistics').attr('name', 'shipping_info[268][logistics]').val('undefined' != typeof shipping_options[268].setting.logistics ? shipping_options[268].setting.logistics : '')
  }
},
311: {
  payments: [
    111,
    311,
    321,
    121,
    341,
    342,
    343,
    344,
    345,
    346,
    347,
    351,
    352,
    353,
    354,
    355,
    361,
    371,
    372,
    373,
    374,
    375,
    376,
    377,
    381,
    382,
    383,
    384,
    385,
    401,
    601
  ],
  action: function (setting) {
    if ('1' == setting.has_address) if ('1' == setting.is_holiday_send && ($('.js_is_holiday_send').removeClass('hide'), $('#is_holiday_send_val').val(0)), '1' == setting.in_taiwan) {
      $('#receiver_address_outer_area, .js_member_recipient_area, .js_recipient_book').removeClass('hide'),
      $('#local_address_area').removeClass('hide');
      var twland_not_send = 'undefined' != typeof setting.twland_not_send && '1' == setting.twland_not_send;
      twzipcode(twland_not_send, !1)
    } else $('#receiver_int_address_area, #receiver_int_city_area, #receiver_int_district_area, #receiver_address_country_area, #receiver_int_zip_area, .js_country_member_recipient_area, .js_recipient_book').removeClass('hide');
    '1' == setting.time && ($('#js_shipping_area').removeClass('hide'), $('#shipping_time').attr('name', 'shipping_info[311][time]')),
    '1' == setting.date && ($('#js_shipping_date_area').removeClass('hide'), $('#shipping_date').attr('name', 'shipping_info[311][date]'), '1' == setting.is_holiday_send ? $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0])  : $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0,
    6])),
    $('#js-custom-name').val($('#shipping option:selected').text()),
    $('#js_logistics').attr('name', 'shipping_info[311][logistics]').val('undefined' != typeof setting.logistics ? setting.logistics : '')
  }
}
},
paymentDisplay = function () {
var shipping = $('#shipping').val(),
html = '<option value="none">&dash; ' + Lang.get('frontend.product.option.select') + ' ' + Lang.get('frontend.order.title.payment') + ' &dash;</option>',
shipping_id = - 1 != shipping.indexOf('custom') ? '311' : shipping,
enabled_payment_id = shippingAndPaymentRelation[shipping_id].payments.slice(),
valid_payment_method = {
};
if (cart_total <= 0 || is_inquiry) html = '<option value="141">' + Lang.get('frontend.payment.none') + '</option>',
$('#js_invoice_area, .js_invoice_block').addClass('hide'),
$('.js_information_block').removeClass('col-md-6').addClass('js_information_block col-md-12'),
$('#payment').html(html),
'none' != shipping_id && $('#payment').change();
 else {
  '311' == shipping_id && '1' === shipping_options[shipping].setting.has_cash && (enabled_payment_id = [
    131
  ]);
  for (var key in payment_options) {
    var payment_id = parseInt(payment_options[key].id);
    if ( - 1 != enabled_payment_id.indexOf(payment_id)) {
      if (!paymentEnable(payment_id)) continue;
      if ('0' == payment_options[key].status) continue;
      if ('0' == payment_options[key][shops_currencies]) continue;
      var additional = '';
      $('.js_ticket_item_list').length <= 0 && 'undefined' != typeof shops_additional[payment_id] && shops_additional[payment_id].number > 0 && (additional = '(' + Lang.get('frontend.cart.payment.additional', {
        number: shops_additional[payment_id].number
      }) + ('percent' == shops_additional[payment_id].type ? '%' : Lang.get('frontend.other.money')) + ')');
      var payment_method = (null === payment_options[key].alias_name ? payment_options[key].name : payment_options[key].alias_name) + additional;
      html += '<option value=\'' + payment_id + '\'>' + payment_method + '</option>',
      valid_payment_method[key] = payment_method
    }
  }
  $('#payment').html(html),
  onlyOnePaymentMethodDisplay(valid_payment_method)
}
'none' != shipping_id && shippingAndPaymentRelation[shipping_id].action(shipping_options[shipping].setting),
'undefined' != typeof shipping_options[shipping] && 'undefined' != typeof shipping_options[shipping].setting.description && '' != shipping_options[shipping].setting.description && ($('#js_shipping_description_area').removeClass('hide'), $('#js_shipping_description').text(shipping_options[shipping].setting.description)),
refreshSelectorSyncSpan(),
refreshPaymentFields(),
$('#payment').prop('disabled', !1)
},
onlyOnePaymentMethodDisplay = function (valid_payment_method) {
var i,
payment_method_counter = 0;
for (i in valid_payment_method) payment_method_counter++;
if ($('#payment_area .form_select_block').removeClass('hide'), $('.one_payment_method').remove(), 1 === payment_method_counter) {
  var payment_method = valid_payment_method[i];
  $('#payment') [0].selectedIndex = 1,
  $('#payment').change(),
  $('#payment_area .form_select_block').addClass('hide'),
  $('#payment_area .form_data').append('<h5 class="form_info one_payment_method">' + payment_method + '</h5>')
}
},
paymentAdditionalDisplay = function (data) {
var shipping_id = $('#shipping').val(),
payment_id = $('#payment').val();
'undefined' != typeof data.shipping_option && (shipping_options = data.shipping_option, 'none' != shipping_id && 'undefined' == typeof data.shipping_option[shipping_id] ? ($('#shipping_area').addClass('form_valid'), shippingMessage(Lang.get('frontend.errormsg.cart.noShipping')))  : $('#shipping_area').removeClass('form_valid')),
refreshCartList(data),
($.inArray(payment_id, Object.keys(allpay_collection_map)) >= 0 || $.inArray(payment_id, Object.keys(ecpay_collection_map)) >= 0) && cart_total >= 20000 && (shippingMessage(Lang.get('frontend.errormsg.cart.allpay.money.limit')), $('#payment').val('none').change(), $('.js_allpay_area').addClass('hide'))
},
shippingCostDisplay = function (data) {
if ('undefined' == typeof data.status || data.status) {
  var shipping_id = $('#shipping').val();
  if ($('#shipping_area').removeClass('form_valid'), 'undefined' != typeof data.shipping_option) {
    var shipping_option_sort = sortObject(data.shipping_option),
    shipping_option = '<option value="none">&dash; ' + Lang.get('frontend.product.option.select') + ' ' + Lang.get('frontend.order.title.shipping') + ' &dash;</option>';
    $.each(shipping_option_sort, function (key, val) {
      shipping_option += '<option value="' + val.shipping_id + '" ' + (shipping_id == val.shipping_id ? 'selected' : '') + '>' + (null === val.alias_name ? val.name : val.alias_name) + '</option>'
    }),
    $('#shipping').html(shipping_option),
    shipping_options = data.shipping_option
  }
  data.freight_amount > 0 && 'none' != shipping_id ? ($('#freight_amount_area').removeClass('hide'), $('#freight_amount').text(currency_icon + data.freight_amount + Lang.get('frontend.other.money')))  : $('#freight_amount_area').addClass('hide'),
  refreshCartList(data),
  paymentDisplay()
} else shippingMessage(data.message),
shipping_options = '',
refreshCartList(data)
},
isEzship = function () {
return 'undefined' != typeof ezship_info
},
allpay_shipping = {
changeShippingTimes: 0,
isAllpayShipping: function () {
  return 'undefined' != typeof allpay_info
},
getLogisticsSubType: function (shipping_id) {
  var logistics_sub_type = '';
  switch (shipping_id) {
    case 242:
      logistics_sub_type = 'FAMI';
      break;
    case 243:
      logistics_sub_type = 'UNIMART';
      break;
    case 244:
      logistics_sub_type = 'FAMIC2C';
      break;
    case 245:
      logistics_sub_type = 'UNIMARTC2C';
      break;
    case 246:
      logistics_sub_type = 'HILIFE';
      break;
    case 247:
      logistics_sub_type = 'HILIFEC2C'
  }
  return logistics_sub_type
},
changeCVSInput: function (shipping_id) {
  if ($('#CVSAddress').attr('name', 'shipping_info[' + shipping_id + '][address]'), $('#CVSStoreName').attr('name', 'shipping_info[' + shipping_id + '][name]'), $('#CVSStoreID').attr('name', 'shipping_info[' + shipping_id + '][id]'), $('input[name=LogisticsSubType]').val(allpay_shipping.getLogisticsSubType(shipping_id)), allpay_shipping.changeShippingTimes += 1, allpay_shipping.clearSelected(), allpay_shipping.isAllpayShipping()) {
    var extra_data = allpay_info.ExtraData.split(',');
    $('#payment option[value="' + extra_data[1] + '"]').length > 0 && $('#payment').val(extra_data[1]).change()
  }
},
clearSelected: function () {
  if (allpay_shipping.isAllpayShipping() && allpay_shipping.changeShippingTimes > 1) {
    var is_allpay_collection_shipping = !1;
    for (key in allpay_collection_map) allpay_collection_map[key] == $('#shipping').val() && (is_allpay_collection_shipping = !0);
    if (!is_allpay_collection_shipping) return !1;
    $('.js_allpay_code, .js_allpay_name, .js_allpay_address').text(Lang.get('frontend.cart.shipping.allpay.none')),
    $('#CVSStoreID, #CVSStoreName, #js_allpay_address').val('')
  }
}
},
invoiceRefresh = function () {
$('#carrier_type_area, #company_title_outer_area, #lovecode_area, #invoice_address_area, .carrier_no_area, #electronic_return_area, #hard_copy_area').addClass('hide')
},
invoiceAction = function (type_id) {
switch (invoiceRefresh(), type_id) {
  case '1':
    $('#carrier_type_area').removeClass('hide'),
    $('#carrier_type').change(),
    $('#hard_copy').change();
    break;
  case '2':
    $('#company_title_outer_area, #invoice_address_area').removeClass('hide');
    break;
  case '3':
    $('#lovecode_area, #electronic_return_area').removeClass('hide')
}
},
carrierRefresh = function () {
$('.carrier_no_area, #electronic_return_area, #invoice_address_area, #hard_copy_area').addClass('hide')
},
carrierAction = function (type_id) {
switch (carrierRefresh(), type_id) {
  case '0':
    $('#carrier_no_mobile_outer_area, #electronic_return_area').removeClass('hide');
    break;
  case '1':
    $('#carrier_no_nature_outer_area, #electronic_return_area').removeClass('hide');
    break;
  case '2':
    $('#electronic_return_area, #hard_copy_area').removeClass('hide'),
    $('#hard_copy').change();
    break;
  case '3':
    $('#carrier_no_easy_card_area, #electronic_return_area').removeClass('hide');
    break;
  case '4':
    $('#electronic_return_area').removeClass('hide'),
    'Pay2Go' == invoice_type && ($('#invoice_address_area').removeClass('hide'), $('#hard_copy').prop('checked', !0), $('#hard_copy').change())
}
},
twzipcode = function (twland_not_send, init) {
var params = {
  detect: !1,
  zipcodeIntoDistrict: !0,
  countySel: receiver_default.receiver_city,
  districtSel: receiver_default.receiver_district,
  zipcodeSel: receiver_default.receiver_zip,
  onCountySelect: function () {
    $('.js_receiver_district').change()
  }
};
if (twland_not_send ? (params.hideCounty = [
  '澎湖縣',
  '連江縣',
  '金門縣'
], params.hideDistrict = [
  '琉球鄉',
  '綠島鄉',
  '蘭嶼鄉',
  '釣魚臺列嶼',
  '東沙群島',
  '南沙群島'
])  : (params.hideCounty = [
], params.hideDistrict = [
]), init) return $('#js_twzipcode').twzipcode(params),
!0;
var ops = $('#js_twzipcode').twzipcode('get_options');
return 'undefined' != typeof ops.hideCounty && ops.hideCounty.length == params.hideCounty.length ? !0 : ($('#js_twzipcode').twzipcode('set_options', params), void $('#js_twzipcode').twzipcode('reset'))
},
initSelector = function () {
if (isEzship() && $('#shipping option[value="221"]').length > 0) $('#shipping').val('221'),
setCartItem('set_shipping', getParams(), shippingCostDisplay);
 else if (allpay_shipping.isAllpayShipping()) {
  var extra_data = allpay_info.ExtraData.split(',');
  $('#shipping option[value="' + extra_data[0] + '"]').length > 0 && ($('#shipping').val(extra_data[0]), setCartItem('set_shipping', getParams(), shippingCostDisplay))
} else 'none' != $('#shipping').val() && (setCartItem('set_shipping', getParams(), shippingCostDisplay), setPhonePlaceholder(), setEmailPlaceholder());
refreshOrderFields(),
twzipcode(!1, !0)
};
$(function () {
function slideDownStyle(element, target, status, reverse) {
  'undefined' == typeof reverse && (reverse = !0);
  var slideUp = function () {
    target.animate({
      height: 0
    }, 300, function () {
      target.css('height', '').hide()
    })
  };
  element.on(status, function () {
    'change' == status ? $(this).prop('checked') == reverse ? target.fadeIn(800)  : slideUp()  : 'click' == status && (target.is(':visible') ? slideUp()  : target.fadeIn())
  })
}
initSelector(),
cart_detail_area.on('change', '.js_cart_list_quantity', function () {
  var qty = $(this).val(),
  standard_id = $(this).attr('data-standard-id'),
  main_qty = count_main_product_qty(standard_id),
  add_price_qty = count_add_price_qty(standard_id);
  if (!check_add_price_qty_limit(main_qty, add_price_qty)) return $(this).val($(this).attr('data-origin-qty')),
  !1;
  var params = getParams();
  params.qty = qty,
  params.standard_id = standard_id,
  setCartItem('update_cart_item', params, shippingCostDisplay)
}),
cart_detail_area.on('click', '.js_cart_list_del_item', function (event) {
  event.preventDefault();
  var params = getParams();
  params.standard_id = $(this).attr('data-standard-id'),
  setCartItem('remove_cart_item', params, shippingCostDisplay)
}),
cart_detail_area.on('change', '.js_cart_list_add_price_quantity', function () {
  var qty = $(this).val(),
  standard_id = $(this).attr('data-standard-id'),
  add_price_id = $(this).attr('data-add-price-id'),
  add_price_standard_id = $(this).attr('data-add-price-standard-id'),
  main_qty = count_main_product_qty(standard_id),
  add_price_qty = count_add_price_qty(standard_id);
  if (!check_add_price_qty_limit(main_qty, add_price_qty)) return $(this).val($(this).attr('data-origin-qty')),
  !1;
  var params = getParams();
  params.qty = qty,
  params.standard_id = standard_id,
  params.add_price_id = add_price_id,
  params.add_price_standard_id = add_price_standard_id,
  setCartItem('add_add_price_item', params, shippingCostDisplay)
}),
cart_detail_area.on('click', '.js_cart_list_del_add_price', function (event) {
  event.preventDefault();
  var params = getParams();
  params.standard_id = $(this).attr('data-standard-id'),
  params.add_price_id = $(this).attr('data-add-price-id'),
  params.add_price_standard_id = $(this).attr('data-add-price-standard-id'),
  setCartItem('remove_add_price_item', params, shippingCostDisplay)
}),
cart_detail_area.on('change', '.js_coupon', function () {
  var params = getParams();
  setCartItem('set_coupon', params, shippingCostDisplay)
}),
cart_detail_area.on('change', '.js_redeem_discount', function () {
  var params = getParams();
  setCartItem('set_redeem_discount', params, shippingCostDisplay)
}),
$('#order_fileds').is(':hidden') && 'none' !== $('#shipping').val() && ($('.js_customer_nologin').addClass('active'), $('#order_fileds').removeClass('hide'));
var showOrderfields = function () {
  var el_order_fields = $('#order_fileds'),
  el_body_html = $('body, html');
  el_order_fields.is(':hidden') && (el_order_fields.addClass('active'), $('#order_fileds').removeClass('hide').hide().fadeIn(), el_body_html.animate({
    scrollTop: el_order_fields.offset().top - 10
  }, 400))
};
$('.js_customer_nologin').on('click', function () {
  '1' === cart_redeem ? $.confirm({
    content: '<span class="content_confirm_inner"><div class="form_group popup_tips_inner form_lastmargin" id=""><label class="form_label" for="">' + Lang.get('frontend.popup.checkout_tips_inner') + '</label></span>',
    confirmButtonClass: 'btn_confirm btn_sm',
    cancelButtonClass: 'btn_cancel btn_sm',
    confirmButton: Lang.get('frontend.popup.checkout_notnow'),
    confirm: function () {
      showOrderfields()
    },
    closeIcon: !1,
    icon: 'icon-notice',
    title: ' ',
    theme: 'waca_popup_tips',
    cancelButton: Lang.get('frontend.popup.checkout_login'),
    keyboardEnabled: !0
  })  : showOrderfields()
}),
$('#shipping').change(function () {
  checkShippingForTwIslands(),
  $('#payment').prop('disabled', !0),
  $('#payment option[value="none"]').attr('selected', !0),
  $('#shipping_time').attr('name', ''),
  setCartItem('set_shipping', getParams(), shippingCostDisplay),
  refreshOrderFields(),
  setPhonePlaceholder(),
  setEmailPlaceholder()
}),
$('#payment').change(function () {
  var payment_id = $(this).val(),
  payment_installment = '';
  refreshPaymentFields(),
  355 == payment_id ? payment_installment = $('#allpay_installment').val()  : 385 == payment_id ? payment_installment = $('#ecpay_installment').val()  : 371 == payment_id && (payment_installment = $('#pay2go_installment').val());
  var params = getParams();
  params.payment_id = payment_id,
  params.payment_installment = payment_installment,
  setCartItem('set_shipping', params, paymentAdditionalDisplay),
  setPhonePlaceholder(),
  setEmailPlaceholder(),
  $('.js_card_payment_note').addClass('hide');
  var card_payment = [
    '311',
    '321',
    '343',
    '344',
    '346',
    '355',
    '361',
    '371',
    '376',
    '385'
  ];
  card_payment.indexOf(payment_id) >= 0 && $('.js_card_payment_note').removeClass('hide'),
  $('.js_credit_able_area, .js_paynow_credit_able_area').addClass('hide'),
  $('.js_paynow_credit_able').data('status', 'off').find('i').attr('class', 'icon-chevron-small-down'),
  '344' != payment_id && '346' != payment_id || $('.js_credit_able_area').removeClass('hide')
}),
$('#allpay_installment, #pay2go_installment, #ecpay_installment').change(function () {
  var params = getParams();
  params.payment_id = $('#payment').val(),
  params.payment_installment = $(this).val(),
  setCartItem('set_shipping', params, paymentAdditionalDisplay)
}),
$('#local_address_area').on('change', '.js_receiver_district', function () {
  $('#local_address_area').hasClass('hide') || ($('#payment option[value="none"]').attr('selected', !0), setCartItem('set_shipping', getParams(), shippingCostDisplay), checkShippingForTwIslands())
}),
$('#int_receiver_country').change(function () {
  setCartItem('set_shipping', getParams(), shippingCostDisplay);
  var country = $(this).val();
  $('#js_shipping_description_area, #js_shipping_date_area, #js_shipping_area, .js_is_holiday_send').addClass('hide'),
  $('#shipping_date').attr('name', ''),
  $('#shipping_time').attr('name', ''),
  $('#is_holiday_send').prop('checked', !1),
  $('#is_holiday_send_val').val('-1'),
  $.each(shipping_options[231].setting, function (key, val) {
    val.country == country && ('undefined' != typeof val.description && '' != val.description && ($('#js_shipping_description_area').removeClass('hide'), $('#js_shipping_description').text(val.description)), 'undefined' != typeof val.date && ($('#js_shipping_date_area').removeClass('hide'), $('#shipping_date').attr('name', 'shipping_info[231][date]'), 'undefined' != typeof val.is_holiday_send ? $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0])  : $('#shipping_date').data('DateTimePicker').daysOfWeekDisabled([0,
    6])), 'undefined' != typeof val.time && ($('#js_shipping_area').removeClass('hide'), $('#shipping_time').attr('name', 'shipping_info[231][time]')), 'undefined' != typeof val.is_holiday_send && $('.js_is_holiday_send').removeClass('hide'), $('#js_logistics').attr('name', 'shipping_info[231][logistics]').val('undefined' != typeof val.logistics ? val.logistics : ''))
  })
}),
$('#same_info').change(function () {
  setPhonePlaceholder()
}),
$('#store_name').change(function () {
  var store_address = $(this).children('option:selected').attr('data-store-address');
  $('.js_store_address').text(store_address).val(store_address),
  $('#js_shipping_description_area').addClass('hide');
  var store_name = $(this).val(),
  name_key = 'name_' + lang;
  $.each(shipping_options[211].setting, function (key, val) {
    return val[name_key] == store_name && 'undefined' != typeof val.description && '' != val.description ? ($('#js_shipping_description_area').removeClass('hide'), void $('#js_shipping_description').text(val.description))  : void 0
  })
}),
$('#btn_ezship_store').click(function () {
  window.location = 'https://map.ezship.com.tw/ezship_map_web.jsp?rtURL=' + cart_url + '%23shipping'
}),
$('#btn_7-11_store').click(function (event) {
  event.preventDefault(),
  window.open('//emap.pcsc.com.tw/emap.aspx', '_blank')
}),
$('.js_total_list_checkbox').click(function () {
  $(this).is(':checked') && $.ajax({
    type: 'POST',
    dataType: 'json',
    url: '/totalListAjax',
    success: function (result) {
      $('.js_total_list').html(result)
    }
  })
}),
$('#btn_allpay_store').click(function () {
  'undefined' == typeof $('#allpayForm').val() ? $.dialog({
    animation: 'opacity',
    closeAnimation: 'none',
    animationSpeed: 1000,
    title: ' ',
    icon: 'icon-notice',
    theme: 'waca_confirm',
    keyboardEnabled: !0,
    content: '<span class="content_dialog_inner">' + Lang.get('frontend.errormsg.cart.allpay.shipping.map') + '</span>',
    onOpen: function () {
      var that = this;
      setTimeout(function () {
        that.close(),
        slideBottom.close()
      }, 4000)
    }
  })  : $('#allpayForm').submit()
}),
slideDownStyle($('#same_info'), $('.js_addressee_block'), 'change', !1),
slideDownStyle($('.js_invoice'), $('.js_invoice_info'), 'click'),
$('#orders_invoice').change(function () {
  var invoice_type_id = $(this).val();
  invoiceAction(invoice_type_id)
}),
$('#carrier_type').change(function () {
  var carrier_type = $(this).val();
  carrierAction(carrier_type)
}),
$('#hard_copy').change(function () {
  $(this).is(':checked') ? $('#invoice_address_area').removeClass('hide')  : $('#invoice_address_area').addClass('hide')
}),
$('#copy_receiver_address').change(function () {
  if ($(this).prop('checked')) {
    var shipping = $('#shipping').val();
    if (231 == shipping) var copy_address = $('#int_receiver_address').val() + ' ' + $('#int_receiver_district').val() + ' ' + $('#int_receiver_city').val() + ' ' + $('#int_receiver_country option:selected').text() + ' ' + $('#int_receiver_zip').val();
     else if ('-1' != shipping.indexOf('custom') && '1' == shipping_options[shipping].setting.has_address && '0' == shipping_options[shipping].setting.in_taiwan) var copy_address = $('#int_receiver_address').val() + ' ' + $('#int_receiver_city').val() + ' ' + $('#int_receiver_district').val() + ' ' + $('#receiver_country_sel option:selected').text() + ' ' + $('#int_receiver_zip').val();
     else var copy_address = $('.js_receiver_city').val() + $('.js_receiver_district').val() + $('#receiver_address').val();
    $('#invoice_address').val(copy_address)
  } else $('#invoice_address').val('')
}),
$('.js_twzipcode_wrapper').append('<span class="form_select_bind js_select_bind"></span>'),
$(document).width() < 480 && $('#customer_text').attr('rows', 2),
$('.js_after_sales').click(function () {
  $(this).is(':checked') && $.ajax({
    type: 'POST',
    dataType: 'json',
    url: after_sales_url,
    success: function (result) {
      $('.after_sales_info').html(result)
    }
  })
}),
$('#receiver_country_sel').combobox({
  placeholder: Lang.get('frontend.cart.title.placeholder.receiver_address_country')
});
var locale = 'en';
'tw' == getLocale ? locale = 'zh-tw' : 'cn' == getLocale && (locale = 'zh-cn'),
Date.prototype.yyyymmdd = function () {
  var mm = this.getMonth() + 1,
  dd = this.getDate();
  return [this.getFullYear(),
  (mm > 9 ? '' : '0') + mm,
  (dd > 9 ? '' : '0') + dd].join('-')
};
var tomorrow = new Date((new Date).getTime() + 86400000),
tomorrow_date = tomorrow.yyyymmdd(),
year = new Date((new Date).getTime() + 31536000000),
year_date = year.yyyymmdd();
$('#shipping_date').datetimepicker({
  sideBySide: !0,
  useCurrent: !1,
  format: 'YYYY-MM-DD',
  ignoreReadonly: !0,
  minDate: tomorrow_date,
  maxDate: year_date,
  locale: locale,
  daysOfWeekDisabled: [
    0,
    6
  ]
}),
$('#is_holiday_send').click(function () {
  var is_holiday_send = $(this).is(':checked') ? 1 : 0;
  $('#is_holiday_send_val').val(is_holiday_send)
}),
$('.js_paynow_credit_able').click(function () {
  var status = $(this).data('status');
  'on' == status ? ($(this).data('status', 'off'), $('.js_paynow_credit_able_area').addClass('hide'), $(this).find('i').attr('class', 'icon-chevron-small-down'))  : ($(this).data('status', 'on'), $('.js_paynow_credit_able_area').removeClass('hide'), $(this).find('i').attr('class', 'icon-chevron-small-up'))
})
});
