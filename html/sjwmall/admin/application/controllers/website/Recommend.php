<?php
defined('BASEPATH') OR exit('No direct script access allowed');
//人氣展覽推薦
class Recommend extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->load->model('website/recommend_model');


		$this->data['filter']['page'] = $this->input->get('page')? $this->input->get('page') : 1;
		$this->data['filter']['sort'] = $this->input->get('sort')? $this->input->get('sort') : 'sort_order asc, create_at desc';
		$this->data['filter']['q'] = $this->input->get('q')? $this->input->get('q'): '';
		// $this->_import_history_image();
	}

	public function _import_history_image()
	{
		$this->load->model('alpha_user/exhibit_model');

		$exhibit_image = array();
		$fp = fopen(ROOT_PATH."exhibit_image.csv", "r");
		while (($row = fgetcsv($fp, 1000, ",")) !== FALSE) {
			if ($row[1] != 'NULL') {
				$exhibit_image[$row[0]] = $row[1];
			}
		}

		$exhibits = $this->exhibit_model->getAll();
		foreach ($exhibits as $row) {
			if ($row['image'] == '' && isset($exhibit_image[$row['id']])) {
				$image = '';
				if (isset($exhibit_image[$row['id']])) {
					$ext = strtolower(preg_replace('/^.*\.([^.]+)$/D', '$1', $exhibit_image[$row['id']]));
					$filename = uniqid() . ".{$ext}";

					$img_url = 'https://dykt84bvm7etr.cloudfront.net/uploadfiles/exhibition/pic/'. $exhibit_image[$row['id']];
					$img = file_get_contents($img_url);
					$path = MEDIA_PATH. "{$row['id']}/exhibit/";
					if (!is_dir($path)) {
						mkdir($path, 0777, true);
					}
					file_put_contents($path.$filename,$img);

					$image = 'exhibit/'.$filename;
				}
				$this->exhibit_model->update($row['id'], array(
					'image' => $image,
				));
			}
		}
	
	}

	public function _import_history_exhibit()
	{
		$this->load->model('alpha_user/exhibit_model');
		$this->load->model('alpha_user/exhibit_website_model');

		$this->load->library('excel');
		$file = ROOT_PATH . 'exhibit_list.xlsx';
		try {
			$objPHPExcel = PHPExcel_IOFactory::load($file);
		} catch(Exception $e) {
			die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
		}

		$categories = array (
			'３Ｃ家電' => 1,
			'寵物用品' => 2,
			'婦幼兒童' => 3,
			'婚紗精品' => 4,
			'家具設計' => 5,
			'觀光旅遊' => 6,
			'佛茶素展' => 7,
			'專業特展' => 8,
			'創業加盟' => 9,
			'酒展' => 10,
			'休閒健康' => 11,
			'其它' => 12,
		);
		$data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		if ($data) {
			$fields = array_shift($data);
			$list = array();
			foreach ($data as $row) {
				preg_replace('/\s(?=\s)/', '', $row[2]);
				$category = 12;
				if (in_array($row[2], $categories)) {
					$category = $categories[$row[2]];
				}

				$this->exhibit_model->update($row[0], array(
					'exhibit_category_id' => $category,
				));

				$this->exhibit_website_model->update($row[0], array(
					'url' => $row[3],
				));
			}
		}
	}

	public function index()
	{
		$page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows']?$this->data['filter']['rows'] : $this->db_rows;
		$offset = ($page -1) * $rows;

		$params = array(
			'q' => $this->data['filter']['q'],
			'conditions' => array(
			),
			'rows' => $rows,
			'offset' => $offset,
		);
		if ($this->data['filter']['q'] !== '' ) {
			$params['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort'] !== '' ) {
			$params['order_by'] = $this->data['filter']['sort'];
		}
		$total = $this->recommend_model->getDataCount($params);
		$list = $this->recommend_model->getData($params);
		foreach ($list as & $row) {
			$row['image_url'] = ($row['image'])? $this->data['media_url'].$row['image'] : '';
			$row['image_thumb_url'] = ($row['image'])? $this->data['media_url'].preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
			$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
			if ($extension == 'gif') {
				$row['image_thumb_url'] = $row['image_url'];
			}

			// youtube
			if (trim($row['youtube_url']) != '') {
				$uri = parse_url($row['youtube_url']);
				preg_match('/v=(\w*)/', $uri['query'], $matchs);
				$row['youtube_embed_url'] = 'https://www.youtube.com/embed/'. $matchs[1];
			}

			$row['start_date'] = strtotime($row['start_date'])? $row['start_date'] : '';
			$row['end_date'] = strtotime($row['end_date'])? $row['end_date'] : '';
			// $row['link_view'] = base_url("website/recommend/view/{$row['id']}/?". $this->getQueryString());
			$row['link_edit'] = base_url("website/recommend/edit/{$row['id']}/?". $this->getQueryString());
		}

		$this->load->library('pagination');
		$config['base_url'] = site_url("website/recommend?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $params['rows'];
		$this->pagination->initialize($config);

		$this->data['list'] = $list;
		$this->data['pagination'] = array(
			'page' => $page,
			'total' => $total,
			'rows' => $rows,
			'offset' => $offset,
		);

		$this->data['link_add'] = base_url("website/recommend/add/?". $this->getQueryString());
		$this->data['link_delete'] = base_url("website/recommend/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("website/recommend/");

		$this->layout->view("website/recommend/list", $this->data);
	}

	public function add()
	{
		$form = $this->recommend_model->getFormDefault();

		if ($post = $this->input->post()) {
			if ($this->_isVerify('add') == TRUE) {
				// upload image
				$saved_id = $this->recommend_model->insert($post, 'create_at');
				if ($saved_id) {
					if (isset($_FILES['upload']) && $_FILES['upload']['tmp_name'] != '') {
						$image = $this->upload_image($_FILES['upload'], "{$this->router->class}/{$saved_id}/");
						$this->recommend_model->update($saved_id, array('image'=>$image['relative_dir'] . $image['new_name']));
					}
					$this->setAlert(1, '資料新增成功');
				}

				redirect(base_url('website/recommend/'));
			}
		}

		$this->data['form'] =  $form;

		$this->data['link_save'] = base_url("website/recommend/add/");
		$this->data['link_cancel'] = base_url('website/recommend/');
		$this->layout->view('website/recommend/add', $this->data);
	}

	public function edit($id=NULL)
	{
		$form = $this->recommend_model->getFormDefault($this->recommend_model->get($id));
		if (!$form) {
			$this->setAlert(4, '錯誤連結請重新操作');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if ($post = $this->input->post()) {
			if ($this->_isVerify('edit', $form) == TRUE) {
				// upload image
				if (isset($_FILES['upload']) && $_FILES['upload']['name'] != '') {
					$image = $this->upload_image($_FILES['upload'], "{$this->router->class}/{$id}/");
					$post['image'] = $image['relative_dir'] . $image['new_name'];

					// delete old image
					if ($form['image'] != '') {
						remove_file($this->media_path . $form['image']);
						remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
					}
				}
				// delete image
				if ($post['image'] == '') {
					if ($form['image'] != '') {
						remove_file($this->media_path . $form['image']);
						remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
					}
				}
				$rs = $this->recommend_model->update($id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				redirect(base_url("website/recommend/?". $this->getQueryString()));
			}
		}

		$form['image_url'] = ($form['image'])? $this->data['media_url'].$form['image'] : '';
		$form['image_thumb_url'] = ($form['image'])? $this->data['media_url'].preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']) : '';
		$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $form['image']);
		if ($extension == 'gif') {
			$form['image_thumb_url'] = $form['image_url'];
		}

		if ($form['youtube_url']) {
			$uri = parse_url($form['youtube_url']);
			preg_match('/v=(\w*)/', $uri['query'], $matchs);
			$form['youtube_embed_url'] = 'https://www.youtube.com/embed/'. $matchs[1];
		}

		$form['start_date'] = strtotime($form['start_date'])? $form['start_date'] : '';
		$form['end_date'] = strtotime($form['end_date'])? $form['end_date'] : '';
		$this->data['form'] = $form;

		$this->data['link_save'] = base_url("website/recommend/edit/{$id}/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("website/recommend/?". $this->getQueryString());
		$this->layout->view('website/recommend/edit', $this->data);
	}

	public function delete()
	{
		if ($post = $this->input->post()) {
			$del_num = 0;
			foreach ($post['rowid'] as $id) {
				remove_file($this->media_path . "recommend/{$id}/");
				$rs = $this->recommend_model->delete($id);
				if ($rs) {
					$del_num ++;
				}
			}
			$error_num = count($post['rowid']) - $del_num;
			if ($error_num == 0) {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料");
			} else {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除");
			}
		}

		redirect(base_url("website/recommend/?". $this->getQueryString()));
	}

	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->recommend_model->getVerifyConfig();
		if ($action == 'edit') {
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}
}

