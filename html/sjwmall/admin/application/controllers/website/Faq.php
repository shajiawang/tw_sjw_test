<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Faq extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}
		
		$this->ctrl_dir = 'website/faq';
		$this->view_dir = 'website/faq/';

		$this->load->model('website/faq_model');

		if (!isset($this->data['filter']['page'])) {
            $this->data['filter']['page'] = '1';
        }
        if (!isset($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = '';
        }
        if (!isset($this->data['filter']['q'])) {
            $this->data['filter']['q'] = '';
        }
	}

	public function index()
	{
		$this->data['page_name'] = 'list';

        $page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows']?$this->data['filter']['rows'] : $this->db_rows;
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;
		
		$attrs = array(
			'q' => $this->data['filter']['q'],
			'conditions' => array("status !=1"=> null),
			'rows' => $rows,
			'offset' => $offset,
		);
		
		if ($this->data['filter']['q'] !== '' ) {
			$params['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort'] !== '' ) {
			$params['order_by'] = $this->data['filter']['sort'];
		}
		
		$this->data['filter']['total'] = $total = $this->faq_model->getListCount($attrs);
		$list = $this->faq_model->getList($attrs); //echo $this->faq_model->get_query();
		
		foreach ($list as & $row) {
			/*
			$row['image_url'] = ($row['image']) ? $this->data['media_url'] . $row['image'] : '';
			$row['image_thumb_url'] = ($row['image']) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
			$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
			if ($extension == 'gif') {
				$row['image_thumb_url'] = $row['image_url'];
			}

			$row['start_date'] = strtotime($row['start_date']) ? $row['start_date'] : '';
			$row['end_date'] = strtotime($row['end_date']) ? $row['end_date'] : '';
			*/
			
			// $row['link_view'] = base_url("{$this->ctrl_dir}/view/{$row['id']}/?". $this->getQueryString());
			$row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
		}
		
		$this->data['list'] = $list;
		$this->load->library('pagination');
        $config['base_url'] = base_url("{$this->ctrl_dir}?". $this->getQueryString(array(), array('page')));
        $config['total_rows'] = $total;
        $config['per_page'] = $rows;
        $this->pagination->initialize($config);

		$this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
		//$this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");

		$this->layout->view("{$this->view_dir}list", $this->data);
	}

	public function add()
	{
		$this->data['page_name'] = 'add';
		
		$form = $this->faq_model->getFormDefault();

		if ($post = $this->input->post()) {
			if ($this->_isVerify('add') == TRUE) {
				// upload image
				$saved_id = $this->faq_model->insert($post, 'create_at');
				if ($saved_id) {
					/*
					if (isset($_FILES['upload']) && $_FILES['upload']['tmp_name'] != '') {
						$image = $this->upload_image($_FILES['upload'], "banner/{$post['company_id']}/");
						$this->faq_model->update($saved_id, array('image'=>$image['relative_dir'] . $image['new_name']));
					}*/
					$this->setAlert(1, '資料新增成功');
				}

				redirect(base_url("{$this->ctrl_dir}?". $this->getQueryString()));
			}
		}

		$this->data['form'] =  $form;

		$this->data['link_save'] = base_url("{$this->ctrl_dir}/add/");
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/");
		$this->layout->view("{$this->view_dir}add", $this->data);
	}

	public function edit($id=NULL)
	{
		$this->data['page_name'] = 'edit';
		$get_data = $this->faq_model->get($id);
		
		$form = $this->faq_model->getFormDefault($get_data);
		if (!$form) {
			$this->setAlert(4, '錯誤連結請重新操作');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
		$post = $this->input->post();

		if ($post) 
		{
			if ($this->_isVerify('edit', $form) == TRUE) {
				
				/*
				// upload image
				if (isset($_FILES['upload']) && $_FILES['upload']['name'] != '') {
					$image = $this->upload_image($_FILES['upload'], "banner/{$post['company_id']}/");
					$post['image'] = $image['relative_dir'] . $image['new_name'];

					// delete old image
					if ($form['image'] != '') {
						remove_file($this->media_path . $form['image']);
						remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
					}
				}
				
				// delete image
				if ($post['image'] == '') {
					if ($form['image'] != '') {
						remove_file($this->media_path . $form['image']);
						remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
					}
				}*/
				
				$rs = $this->faq_model->update($id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
			}
		}

		/*
		$form['image_url'] = ($form['image'])? $this->data['media_url'].$form['image'] : '';
		$form['image_thumb_url'] = ($form['image'])? $this->data['media_url'].preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']) : '';
		$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $form['image']);
		if ($extension == 'gif') {
			$form['image_thumb_url'] = $form['image_url'];
		}
		$form['start_date'] = strtotime($form['start_date'])? $form['start_date'] : '';
		$form['end_date'] = strtotime($form['end_date'])? $form['end_date'] : '';
		*/
		$this->data['form'] = $form;

		$this->data['link_save'] = base_url("{$this->ctrl_dir}/edit/{$id}/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
		$this->layout->view("{$this->view_dir}edit", $this->data);
	}

	public function delete()
	{
		if ($post = $this->input->post()) {
			$del_num = 0;
			foreach ($post['rowid'] as $id) {
				remove_file($this->media_path . "{$this->router->class}/{$id}/");
				$rs = $this->faq_model->delete($id);
				if ($rs) {
					$del_num ++;
				}
			}
			$error_num = count($post['rowid']) - $del_num;
			if ($error_num == 0) {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料");
			} else {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除");
			}
		}

		redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
	}

	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->faq_model->getVerifyConfig();
		if ($action == 'edit') {
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}
}

