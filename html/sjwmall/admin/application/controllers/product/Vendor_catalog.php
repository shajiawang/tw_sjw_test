<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vendor_catalog extends MY_Controller
{
    public $kind = 'goods';
    public $level_max = 3;
    public function __construct()
    {
        parent::__construct();

        if ($this->flags->is_login === FALSE) {
            redirect(base_url('welcome'));
        }

        $this->load->model('product/category_model');
    }

    public function index()
    {
        $list = $this->category_model->getListByKind($this->kind, $this->level_max);
        $total = count($list);
        foreach ($list as & $row) {
            $row['image_url'] = ($row['image'])? $this->data['media_url'] . $row['image'] : '';
            $row['image_thumb_url'] = ($row['image'])? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
            $extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
            if ($extension == 'gif') {
                $row['image_thumb_url'] = $row['image_url'];
            }
            $row['description'] = cutContent($row['description'], 100);

            // $row['link_view'] = base_url("catalog/category/view/{$row['id']}/?". $this->getQueryString());
            $row['link_edit'] = base_url("catalog/goods_category/edit/{$row['id']}/?". $this->getQueryString());
        }

        $this->data['list'] = $list;
        $this->data['total'] = $total;

        $this->data['link_add'] = base_url("catalog/goods_category/add/?". $this->getQueryString());
        $this->data['link_delete'] = base_url("catalog/goods_category/delete/?". $this->getQueryString());
        $this->data['link_refresh'] = base_url("catalog/goods_category/");

        $this->layout->view('category/list', $this->data);
    }

    public function add()
    {
        $form = $this->category_model->getFormDefault(array('kind'=>$this->kind));

        if ($post = $this->input->post()) {
            if ($this->_isVerify('add') == TRUE) {
                $post['level'] = $this->category_model->getLevel($post['parent_id']);
                $saved_id = $this->category_model->insert($post);
                if ($saved_id) {
                    // upload image
                    if (isset($_FILES['upload']) && $_FILES['upload']['tmp_name'] != '') {
                        $image = $this->upload_image($_FILES['upload'], "category/{$saved_id}/");
                        $this->category_model->update($saved_id, array('image'=>$image['pelative'] . $image['new_name']));
                    }
                    $this->setAlert(1, '資料新增成功');
                }

                redirect(base_url('catalog/goods_category/'));
            }
        }

        $this->data['form'] =  $form;

        $this->data['level_max'] = $this->level_max;

        if ($this->level_max > 1) {
            $this->data['choices']['parent_list'] = $this->category_model->getParentChoices($this->kind, $this->level_max-1);
        }

        $this->data['link_save'] = base_url("catalog/goods_category/add/");
        $this->data['link_cancel'] = base_url('catalog/goods_category/');
        $this->layout->view('category/add', $this->data);
    }

    public function edit($id=NULL)
    {
        $form = $this->category_model->getFormDefault($this->category_model->get($id));
        if (!$form) {
            $this->setAlert(4, '錯誤連結請重新操作');
            redirect($_SERVER['HTTP_REFERER']);
        }

        if ($post = $this->input->post()) {
            if ($this->_isVerify('edit', $form) == TRUE) {
                // upload image
                if (isset($_FILES['upload']) && $_FILES['upload']['name'] != '') {
                    $image = $this->upload_image($_FILES['upload'], "category/{$id}/");
                    $post['image'] = $image['pelative'] . $image['new_name'];

                    // delete old image
                    if ($form['image'] != '') {
                        remove_file($this->media_path . $form['image']);
                        remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
                    }
                }
                // delete image
                if ($post['image'] == '') {
                    if ($form['image'] != '') {
                        remove_file($this->media_path . $form['image']);
                        remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
                    }
                }

                $post['level'] = $this->category_model->getLevel($post['parent_id']);
                $rs = $this->category_model->update($id, $post);
                if ($rs) {
                    $this->setAlert(2, '資料編輯成功');
                }
                redirect(base_url("catalog/goods_category/?". $this->getQueryString()));
            }
        }

        $form['image_url'] = ($form['image'])? $this->data['media_url'] . $form['image'] : '';
        $form['image_thumb_url'] = ($form['image'])? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']) : '';
        $extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $form['image']);
        if ($extension == 'gif') {
            $form['image_thumb_url'] = $form['image_url'];
        }

        $this->data['form'] = $form;
        $this->data['level_max'] = $this->level_max;

        if ($this->level_max > 1) {
            $this->data['choices']['parent_list'] = $this->category_model->getParentChoices($this->kind, $this->level_max-1);
            foreach ($this->data['choices']['parent_list'] as $k => $v) {
                if ($k == $id) {
                    unset($this->data['choices']['parent_list'][$k]);
                }
            }
        }

        $this->data['link_save'] = base_url("catalog/goods_category/edit/{$id}/?". $this->getQueryString());
        $this->data['link_cancel'] = base_url("catalog/goods_category/?". $this->getQueryString());
        $this->layout->view('category/edit', $this->data);
    }

    public function view($id=NULL)
    {
        $this->data['form'] = $this->category_model->getFormDefault($this->category_model->getInfo($id));

        $this->data['link_add'] = base_url("catalog/goods_category/add/?". $this->getQueryString());
        $this->data['link_edit'] = base_url("catalog/goods_category/edit/{$id}?". $this->getQueryString());
        $this->data['link_cancel'] = base_url("catalog/goods_category/?". $this->getQueryString());

        $this->layout->view('category/view', $this->data);
    }

    public function delete()
    {
        if ($post = $this->input->post()) {
            $del_num = 0;
            foreach ($post['rowid'] as $id) {
                remove_dir($this->media_path . "category/{$id}/");
                $rs = $this->category_model->delete($id);
                if ($rs) {
                    $del_num ++;
                }
            }
            $error_num = count($post['rowid']) - $del_num;
            if ($error_num == 0) {
                $this->setAlert(2, "共刪除 {$del_num} 筆資料");
            } else {
                $this->setAlert(2, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除");
            }
        }

        redirect(base_url("catalog/goods_category/?". $this->getQueryString()));
    }

    private function _isVerify($action='add', $old_data=array())
    {
        $config = $this->category_model->getVerifyConfig();
        if ($action == 'edit') {
        }

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

        return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
    }
}

