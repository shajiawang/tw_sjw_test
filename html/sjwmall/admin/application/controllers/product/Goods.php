<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Goods extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->flags->is_login === FALSE) {
            redirect(base_url('welcome'));
        }
		
		$this->ctrl_dir = 'product/goods';
		$this->view_dir = 'product/goods/';

		$this->load->model('product/category_model');
		$this->load->model('product/goods_model');
		$this->load->model('product/goods_image_model');

        $this->data['choices']['category'] = $this->category_model->getChoicesByKind('goods', 3);
    }

    public function index()
    {
        if (empty($this->data['filter']['category_id'])) {
            $this->data['filter']['category_id'] = 'all';
        }
        if (empty($this->data['filter']['page'])) {
            $this->data['filter']['page'] = 1;
        }
        if (empty($this->data['filter']['q'])) {
            $this->data['filter']['q'] = '';
        }
        if (empty($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = '';
        }

        $page = $this->data['filter']['page'];
        $rows = 10;
        $offset = ($page -1) * $rows;

        $params = array(
            'q' => $this->data['filter']['q'],
            'conditions' => array(
            ),
            'order_by' => 'create_at desc',
            'rows' => $rows,
            'offset' => $offset,
        );
        if ($this->data['filter']['category_id'] != 'all' ) {
            $params['conditions']['category_id'] = $this->data['filter']['category_id'];
        }
        if ($this->data['filter']['q'] !== '' ) {
            $params['q'] = $this->data['filter']['q'];
        }
        if ($this->data['filter']['sort'] !== '' ) {
            $params['order_by'] = $this->data['filter']['sort'];
        }
        $total = $this->goods_model->getListCount($params);
        $list = $this->goods_model->getList($params);
        foreach ($list as & $row) {
            $row['image_url'] = ($row['image'])? $this->data['media_url'] . $row['image'] : '';
            $row['image_thumb_url'] = ($row['image'])? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
            $extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
            if ($extension == 'gif') {
                $row['image_thumb_url'] = $row['image_url'];
            }

            $row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
        }

        $this->load->library('pagination');
        $config['base_url'] = site_url("{$this->ctrl_dir}?". $this->getQueryString(array(), array('page')));
        $config['total_rows'] = $total;
        $config['per_page'] = $params['rows'];
        $this->pagination->initialize($config);

        $this->data['list'] = $list;
        $this->data['pagination'] = array(
            'page' => $page,
            'total' => $total,
            'rows' => $rows,
            'offset' => $offset,
        );

        $this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
        $this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
        $this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");

        $this->layout->view($this->view_dir ."list", $this->data);
    }

    public function add()
    {
        $form =  $this->goods_model->getFormDefault();

        if ($post = $this->input->post()) {
            $category_id = $this->input->post('category_id');
            $subimage = $this->input->post('subimage');
            if ($this->_isVerify() == TRUE) {
                unset($post['category_id']);
                unset($post['subimage']);

                $saved_id = $this->goods_model->insert($post, 'create_at');
                if ($saved_id) {
                    // write category
                    if ($category_id) {
                        foreach ($category_id as $cid) {
                            $fields = array(
                                'goods_id' => $saved_id,
                                'category_id' => $cid,
                            );
                            $this->goods_to_category_model->insert($fields);
                        }
                    }

                    // upload image
                    if (isset($_FILES['upload']) && $_FILES['upload']['tmp_name'] != '') {
                        $image = $this->upload_image($_FILES['upload'], "{$this->router->class}/{$saved_id}/");
                        $this->goods_model->update($saved_id, array('image'=>$image['pelative'] . $image['new_name']));
                    }

                    if ($subimage) {
                        foreach ($subimage as $num => $row) {
                            if ($row['id'] == 0) {  // add
                                if (isset($_FILES['subimage']['name'][$num]) && $_FILES['subimage']['name'][$num] != '') {
                                    $file = array(
                                        'name' => $_FILES['subimage']['name'][$num],
                                        'tmp_name' => $_FILES['subimage']['tmp_name'][$num],
                                        'type' => $_FILES['subimage']['type'][$num],
                                        'size' => $_FILES['subimage']['size'][$num],
                                    );
                                    $image = $this->upload_image($file, "{$this->router->class}/{$saved_id}/");
                                    $fields = array(
                                        'goods_id' => $saved_id,
                                        'name' => $row['name'],
                                        'image' => $image['pelative'] . $image['new_name'],
                                        'sort_order' => $row['sort_order'],
                                    );
                                    $this->goods_image_model->insert($fields);
                                }
                            }
                        }
                    }

                    $this->setAlert(1, '資料新增成功');
                }

                redirect(base_url($this->ctrl_dir .'/'));
            } else {
                $this->data['form']['subimage'] = $subimage;
            }
        }

        $this->data['form'] = $form;

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/add/");
        $this->data['link_cancel'] = base_url($this->ctrl_dir .'/');
        $this->layout->view($this->view_dir .'add', $this->data);
    }

    public function copy($id=NULL)
    {
        $data = $this->goods_model->get($id);

        $image = $this->_copy_image($data['image']);
        $new_image = $image['pelative'] . $image['new_name'];

        $fields = array(
            'sort_order' => $data['sort_order'],
            'image' => $new_image,
            'name' => $data['name'],
            'model' => generatorRandom(8),
            'description' => $data['description'],
            'meta_title' => $data['meta_title'],
            'meta_description' => $data['meta_description'],
            'meta_keyword' => $data['meta_keyword'],
        );

        $saved_id = $this->goods_model->_insert($fields);
        if ($saved_id) {
            // 寫回類別
            $category = $data['cateogry'];
            foreach ($category as $category_id) {
               $fields = array(
                   'goods_id' => $saved_id,
                   'category_id' => $category_id,
               );
               $this->goods_category_model->insert($fields);
            }

            // 寫回圖片集
            $subimage = $data['subimage'];
            foreach ($subimage as $row) {
                $image = $this->_copy_image($row['image']);
                $new_image = $image['pelative'] . $image['new_name'];
                $fields = array(
                    'goods_id' => $saved_id,
                    'image' => $new_image,
                    'sort_order' => $row['sort_order'],
                );
                $this->goods_image_model->insert($fields);
            }

            $this->setAlert(1, '複製已完成');
            redirect(base_url("{$this->ctrl_dir}?{$_SERVER['QUERY_STRING']}"));
        }

    }

    public function edit($id=NULL)
    {
        $form = $this->goods_model->getFormDefault($this->goods_model->get($id));
        $form['subimage'] = $this->goods_image_model->getByGoodsId($id);

        if ($post = $this->input->post()) {
            $category_id = $this->input->post('category_id');
            $subimage = $this->input->post('subimage');

            if ($this->_isVerify($form) == TRUE) {
                unset($post['category_id']);
                unset($post['subimage']);

                // upload image
                if (isset($_FILES['upload']) && $_FILES['upload']['tmp_name'] != '') {
                    $image = $this->upload_image($_FILES['upload'], "{$this->router->class}/{$id}/");
                    $post['image'] = $image['pelative'] . $image['new_name'];

                    // delete old image
                    if ($form['image'] != '') {
                        remove_file($this->media_path . $form['image']);
                        remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
                    }
                }
                // delete image
                if ($post['image'] == '') {
                    if ($form['image'] != '') {
                        remove_file($this->media_path . $form['image']);
                        remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
                    }
                }

                $rs = $this->goods_model->update($id, $post);
                if ($rs) {
                    // write category_id
                    $this->goods_to_category_model->delete(array('goods_id'=>$id));
                    foreach ($category_id as $cid) {
                        $fields = array(
                            'goods_id' => $id,
                            'category_id' => $cid,
                        );
                        $this->goods_to_category_model->insert($fields);
                    }

                    // update subimage
                    if ($subimage) {
                        $subimage_ids = array();
                        foreach ($subimage as $row) {
                            $subimage_ids[] = $row['id'];
                        }

                        foreach ($form['subimage'] as $row) {
                            if (!in_array($row['id'], $subimage_ids)) {
                                remove_file($this->media_path . $row['image']);
                                remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']));
                                $this->goods_image_model->delete($row['id']);
                            }
                        }
                        foreach ($subimage as $num => $row) {
                            if ($row['id'] == 0) {  // add
                                if (isset($_FILES['subimage']['name'][$num]) && $_FILES['subimage']['name'][$num] != '') {
                                    $file = array(
                                        'name' => $_FILES['subimage']['name'][$num],
                                        'tmp_name' => $_FILES['subimage']['tmp_name'][$num],
                                        'type' => $_FILES['subimage']['type'][$num],
                                        'size' => $_FILES['subimage']['size'][$num],
                                    );
                                    $image = $this->upload_image($file, "{$this->router->class}/{$id}/");
                                    $fields = array(
                                        'goods_id' => $id,
                                        'name' => $row['name'],
                                        'image' => $image['pelative'] . $image['new_name'],
                                        'sort_order' => $row['sort_order'],
                                    );
                                    $this->goods_image_model->insert($fields);
                                }
                            } else {
                                $fields = array(
                                    'goods_id' => $id,
                                    'name' => $row['name'],
                                    'sort_order' => $row['sort_order'],
                                );

                                foreach ($form['subimage'] as $old_img) {
                                    if ($row['id'] == $old_img['id']) {
                                        if ($row['image'] != $old_img['image']) {
                                            $fields['image'] = '';
                                            if (isset($_FILES['subimage']['name'][$num]) && $_FILES['subimage']['name'][$num] != '') {
                                                $file = array(
                                                    'name' => $_FILES['subimage']['name'][$num],
                                                    'tmp_name' => $_FILES['subimage']['tmp_name'][$num],
                                                    'type' => $_FILES['subimage']['type'][$num],
                                                    'size' => $_FILES['subimage']['size'][$num],
                                                );
                                                $image = $this->upload_image($file, "{$this->router->class}/{$id}/");
                                                $fields['image'] = $image['pelative'] . $image['new_name'];
                                            }
                                            if ($old_img['image'] != '') {
                                                remove_file($this->media_path . $old_img['image']);
                                                remove_file($this->media_path.preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $old_img['image']));
                                            }
                                        }
                                        break;
                                    }
                                }

                                $this->goods_image_model->update($row['id'], $fields);
                            }
                        }

                    }

                    $this->setAlert(2, '資料編輯成功');
                }

                redirect(base_url("{$this->ctrl_dir}/?{$_SERVER['QUERY_STRING']}"));
            } else {
                $this->data['form']['subimage'] = $subimage;
            }
        }

        $form['sale_start_date'] = strtotime($form['sale_start_date'])>0? $form['sale_start_date'] : '';
        $form['sale_end_date'] = strtotime($form['sale_end_date'])>0? $form['sale_end_date'] : '';
        $form['category_id'] = $this->goods_to_category_model->getCategoryIds($id);
        $form['image_url'] = ($form['image'])? $this->data['media_url'] . $form['image'] : '';
        $form['image_thumb_url'] = ($form['image'])? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']) : '';
        $extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $form['image']);
        if ($extension == 'gif') {
            $form['image_thumb_url'] = $form['image_url'];
        }
        $this->data['form'] = $form;

        $this->setJson('choices', $this->data['choices']);

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/edit/{$id}/?{$_SERVER['QUERY_STRING']}");
        $this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?{$_SERVER['QUERY_STRING']}");
        $this->layout->view($this->view_dir .'edit', $this->data);
    }

    public function delete()
    {
        if ($post = $this->input->post()) {
            $del_num = 0;
            foreach ($post['rowid'] as $id) {
                remove_dir($this->media_path . "goods/{$id}/");
                $rs = $this->goods_model->delete($id);
                if ($rs) {
                    $del_num ++;
                }
            }
            $error_num = count($post['rowid']) - $del_num;
            if ($error_num == 0) {
                $this->setAlert(2, "共刪除 {$del_num} 筆資料");
            } else {
                $this->setAlert(2, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除");
            }
        }

        redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
    }

    private function _isVerify($old_data=array())
    {
        $config = $this->goods_model->getVerifyConfig();
        if ($this->router->method == 'edit') {
            if ($old_data['no'] == $this->input->post('no')) {
                $config['no']['rules'] = 'trim|required';
            }
        }

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

        return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
    }
}

