<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bonus_rule extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->load->model(array(
			// 'catalog/bonus_model',
			'catalog/bonus_rule_model',
		));
		$this->data['choices']['kind'] = $this->bonus_rule_model->kind_choices;
		$this->data['choices']['method'] = $this->bonus_rule_model->method_choices;

		$this->data['filter']['page'] = $this->input->get('page')? $this->input->get('page') : 1;
		$this->data['filter']['sort'] = $this->input->get('sort')? $this->input->get('sort') : NULL;
		$this->data['filter']['q'] = $this->input->get('q')? $this->input->get('q'): '';
		$this->data['filter']['kind'] = $this->input->get('kind')? $this->input->get('kind'): 1;
	}

	public function index()
	{
		$page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows']?$this->data['filter']['rows'] : $this->db_rows;
		$offset = ($page -1) * $rows;

		$params = array(
			'conditions' => array(
			),
			'order_by' => 'method asc, range_begin asc, note asc',
			'rows' => $rows,
			'offset' => $offset,
		);
		if ($this->data['filter']['q'] != '') {
			$params['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort']) {
			$params['order_by'] = $this->data['filter']['sort'];
		}
		if ($this->data['filter']['kind'] != 'all') {
			$params['conditions']['kind'] = $this->data['filter']['kind'];
		}

		$total = $this->bonus_rule_model->getDataCount($params);
		$this->data['list'] = $this->bonus_rule_model->getData($params);
		foreach ($this->data['list'] as & $row) {
			$row['link_edit'] = base_url("catalog/{$this->router->class}/edit/{$row['id']}?". $this->getQueryString());
		}

		$this->load->library('pagination');
		$config['base_url'] = site_url("catalog/{$this->router->class}?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $params['rows'];
		$this->pagination->initialize($config);

		$this->data['pagination'] = array(
			'page' => $page,
			'total' => $total,
			'rows' => $rows,
			'offset' => $offset,
		);

		$this->data['link_add'] = base_url("catalog/{$this->router->class}/add?". $this->getQueryString());
		$this->data['link_delete'] = base_url("catalog/{$this->router->class}/delete?". $this->getQueryString());

		$this->layout->view("catalog/{$this->router->class}/list", $this->data);
	}

	public function add()
	{
		$this->data['page_name'] = 'add';

		if ($post = $this->input->post()) {
			if ($this->_isVerify('add') == TRUE) {
				$post['note'] = trim($post['note']);
				$saved_id = $this->bonus_rule_model->insert($post, 'create_at');
				if ($saved_id) {
					$this->setAlert(1, '資料新增成功');
				}

				redirect(base_url("catalog/{$this->router->class}?". $this->getQueryString()));
			}
		}

		$this->data['form'] =  $this->bonus_rule_model->getFormDefault(array('kind'=>$this->data['filter']['kind']));

		$this->data['link_save'] = base_url("catalog/{$this->router->class}/add?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("catalog/{$this->router->class}?". $this->getQueryString());
		$this->layout->view("catalog/{$this->router->class}/add", $this->data);
	}

	public function edit($id=NULL)
	{
		$this->data['page_name'] = 'edit';

		$form = $this->bonus_rule_model->get($id);
		if ($post = $this->input->post()) {
			if ($this->_isVerify('edit', $form) == TRUE) {
				$post['note'] = trim($post['note']);
				$rs = $this->bonus_rule_model->update($id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				redirect(base_url("catalog/{$this->router->class}?". $this->getQueryString()));
			}
		}

		$this->data['form'] = $this->bonus_rule_model->getFormDefault($this->bonus_rule_model->get($id));

		$this->data['link_save'] = base_url("catalog/{$this->router->class}/edit/{$id}?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("catalog/{$this->router->class}?". $this->getQueryString());
		$this->layout->view("catalog/{$this->router->class}/edit", $this->data);
	}

	public function delete()
	{
		if ($post = $this->input->post()) {
			$del_num = 0;
			foreach ($post['rowid'] as $id) {
				$rs = $this->bonus_rule_model->delete($id);
				if ($rs['status']) {
					$del_num ++;
				}
			}

			$error_num = count($post['rowid']) - $del_num;
			if ($error_num == 0) {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料");
			} else {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除");
			}
		}

		redirect(base_url("catalog/{$this->router->class}?". $this->getQueryString()));
	}

	private function _isVerify($action='add', $old_data=array())
	{
		$post = $this->input->post();
		$config = $this->bonus_rule_model->getVerifyConfig();
		$config['range_end']['rules'] .= "|greater_than[{$post['range_begin']}]";
		if ($post['kind'] <= 2) {
			unset($config['method']);
			unset($config['rate_new']);
			unset($config['rate_old']);
		} else {
			unset($config['bonus']);
		}

		if ($action == 'edit') {
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}


}
