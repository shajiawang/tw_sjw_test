<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Product extends MY_Controller
{
	public $category_kind = 'goods';
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}
		
		$this->ctrl_dir = 'catalog/product';
		$this->view_dir = 'catalog/product/';

		$this->load->model('product/category_model');
		$this->load->model('product/goods_model');
		$this->load->model('product/goods_image_model');
		$this->load->model('company/company_info_model');
		$this->load->model('company/company_exhibitor_model');
		
		
		//廠商
		if($this->data['user_info']['edit'] == TRUE){
			$this->data['cname_disabled'] = 'disabled';
			$this->data['filter']['cname'] = '';
			$this->data['company_id'] = $this->flags->user['company_id'];
			$_SESSION['search_cname'] = $this->flags->user['name'];
		}
		else 
		{
			$this->data['cname_disabled'] = '';
			if (!isset($this->data['filter']['cname'])) {
				$get_cname = isset($_SESSION['search_cname']) ? $_SESSION['search_cname'] : null;
				$this->data['filter']['cname'] = empty($get_cname) ? $get_cname : '';
			} else {
				$_SESSION['search_cname'] = $this->data['filter']['cname'];
			}
			
			if (!empty($_SESSION['search_cname']) || !empty($this->data['filter']['cname']) ) {
				$params['q'] = empty($_SESSION['search_cname']) ? $this->data['filter']['cname'] : $_SESSION['search_cname'];
				$company = $this->user_model->getData($params); //echo $this->user_model->get_query();
				$this->data['company_id'] = empty($company) ? '' : $company[0]['company_id'];
			} else {
				$this->data['company_id'] = '';
			}
		}
		
		if (!isset($this->data['filter']['page'])) {
			$this->data['filter']['page'] = '1';
		}
		if (!isset($this->data['filter']['sort'])) {
			$this->data['filter']['sort'] = '';
		}
		if (!isset($this->data['filter']['q'])) {
			$this->data['filter']['q'] = '';
		}
	}

	public function index()
	{
		$this->data['page_name'] = 'list';
		
		$page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows'];
		
		$table0 = $this->goods_model->table;
		$table1 = $this->company_info_model->table;

		$conditions = array("{$table0}.status !=1" => null);
		if (! empty($this->data['company_id']) && empty($this->data['filter']['cname']) ) {
			$conditions["{$table0}.company_id"] = $this->data['company_id'];
		}
		 
		$attrs = array('select' => "{$table0}.*",
			'conditions' => $conditions,
			'order_by' => "{$table0}.name",
		);
		
		$on_0 = "{$table0}.company_id={$table1}.id";
		$attrs['join'][0] = array('table'=>$table1, 'on'=>$on_0, 'type'=>'left');
		
        if ($this->data['filter']['q'] !== '' ) { $qv = "%{$this->data['filter']['q']}%";
			$attrs['q'] = array('many' => TRUE
				, 'data' => array(
                    array('field' => "{$table0}.name", 'value'=>$qv, 'position'=>'both'),
					array('field' => "{$table0}.no", 'value'=>$qv, 'position'=>'both'),
                )
            );
        }
		if(!empty($this->data['filter']['cname'])){
			$attrs['like'] = array(
				'field' => "{$table1}.comp_name", 'value'=>$this->data['filter']['cname'], 'position'=>'both'
			);
		}
		
		$this->data['filter']['total'] = $total = $this->goods_model->getListCount($attrs);
        $this->data['filter']['offset'] = $offset = ($page -1) * $rows;

		$attrs['rows'] = $rows;
		$attrs['offset'] = $offset;
		if ($this->data['filter']['sort'] !== '' ) {
			$attrs['sort'] = $this->data['filter']['sort'];
		}

		$this->data['list'] = array();
		$list = $this->goods_model->getList($attrs); 
		
		foreach ($list as $k=>$row)
		{
			$this->data['list'][$k] = $row;
			$this->data['list'][$k]['image_url'] = ($row['image']) ? $this->data['media_url'] . $row['image'] : '';
			$this->data['list'][$k]['image_thumb_url'] = ($row['image']) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
			$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
			if ($extension == 'gif') {
				$this->data['list'][$k]['image_thumb_url'] = $row['image_url'];
			}
			
			$company = $this->company_info_model->getRowById($row['company_id']);
			$this->data['list'][$k]['company_id'] = '#'. $row['company_id'] .' '. $company['comp_name'];
			
			
			$this->data['list'][$k]['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
		}

		$this->load->library('pagination');
		$config['base_url'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $rows;
		$this->pagination->initialize($config);
		
		$this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
		$this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");

		$this->data['today'] = date('Y-m-d');

		$this->data['link_add'] = base_url("catalog/product/add/?". $this->getQueryString());
		$this->data['link_delete'] = base_url("catalog/product/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("catalog/product/");

		$this->layout->view("{$this->view_dir}list", $this->data);
	}
	
	public function add()
	{
		$this->data['page_name'] = 'add';
		$this->data['form'] = $form = $this->goods_model->getFormDefault();
		
		//創建新商品
		$com_goods = isset($this->data['filter']['com_goods']) ? $this->data['filter']['com_goods'] : '';
		$aid = isset($this->data['filter']['aid']) ? $this->data['filter']['aid'] : '';
		$back_goods_url = ($com_goods=='y' && !empty($aid)) ? base_url("comexpo/com_store/add_goods_list/?aid={$aid}") : '';
		$save_att = empty($back_goods_url) ? '' : '?com_goods='. $com_goods .'&aid='. $aid;
		
		if(empty($form['no']))
		{
			$MaxId = $this->goods_model->getMaxId();
			$this->data['form']['no'] = $MaxId + 1;
		}
		
        $post = $this->input->post();
		
		if ($post && $post['company_id'])
		{
            if ($this->_isVerify($form) == TRUE)
			{
				$img_id = '';
				$post['create_at'] = date('Y-m-d H:i:s', time());
				$post['years'] = date('Y', time());
				$img_id = $this->goods_model->insert($post);
				
				if(!empty($img_id))
				{
					$post['image'] = $this->_upload_image($img_id, $form, $post);
					$rs = $this->goods_model->update($img_id, $post);
				}
                
				$this->setAlert(1, '資料新增成功');
                
				if(empty($back_goods_url))
				{ 
					redirect(base_url("{$this->ctrl_dir}/edit/{$img_id}/?". $this->getQueryString()));
				}
				else
				{ 
					redirect($back_goods_url);
				}
            }
		} 

        $this->setJson('choices', $this->data['choices']);

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/add/". $save_att);
        $this->data['link_cancel'] = base_url($this->ctrl_dir .'/');
        $this->layout->view("{$this->view_dir}add", $this->data);
    }

	public function edit($id=NULL)
	{
		$this->data['page_name'] = 'edit';
		
		$form = $this->goods_model->getFormDefault($this->goods_model->get($id));
		$form['subimage'] = $this->goods_image_model->getByGoodsId($id);
		$img_id = $form['id'];
		
		$this->data['company_id'] = $form['company_id'];

        if ($post = $this->input->post())
		{
            $subimage = $this->input->post('subimage');
			unset($post['subimage']);

            if ($this->_isVerify($form) == TRUE)
			{
                $post['image'] = $this->_upload_image($img_id, $form, $post);
				
				$rs = $this->goods_model->update($id, $post);
                
				if ($rs) {
                    // update subimage
                    if ($subimage) {
                        $this->_upload_subimage($subimage, $img_id, $form);
                    }
					
					$this->setAlert(2, '資料編輯成功');
                }

                redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
            } else {
                $this->data['form']['subimage'] = $subimage;
            }
        }

        $form['sale_start_date'] = strtotime($form['sale_start_date'])>0 ? $form['sale_start_date'] : '';
        $form['sale_end_date'] = strtotime($form['sale_end_date'])>0 ? $form['sale_end_date'] : '';
        $form['image_url'] = ($form['image']) ? $this->data['media_url'] . $form['image'] : '';
        $form['image_thumb_url'] = ($form['image']) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']) : '';
		
		$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $form['image']);
        if ($extension == 'gif') {
            $form['image_thumb_url'] = $form['image_url'];
        }
        $this->data['form'] = $form;

        $this->setJson('choices', $this->data['choices']);

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/edit/{$id}/?". $this->getQueryString());
        $this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
        
		$this->layout->view("{$this->view_dir}edit", $this->data);
    }
	
	private function _upload_subimage($subimage, $img_id, $form)
	{
		$subimage_ids = array();
		if($subimage)
		foreach ($subimage as $row) {
			$subimage_ids[] = $row['id'];
		}
		
		if($form['subimage'])
		foreach ($form['subimage'] as $row) {
			if (!in_array($row['id'], $subimage_ids)) {
				remove_file($this->media_path . $row['image']);
				remove_file($this->media_path . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']));
				$this->goods_image_model->delete($row['id']);
			}
		}
		
		foreach ($subimage as $num => $row)
		{
			if ($row['id'] == 0)
			{  // add
				if (isset($_FILES['subimage']['name'][$num]) && $_FILES['subimage']['name'][$num] != '') {
					$file = array(
						'name' => $_FILES['subimage']['name'][$num],
						'tmp_name' => $_FILES['subimage']['tmp_name'][$num],
						'type' => $_FILES['subimage']['type'][$num],
						'size' => $_FILES['subimage']['size'][$num],
					);
					
					$image = $this->upload_image($file, "{$this->router->class}/{$img_id}/");
					$fields = array(
						'goods_id' => $img_id,
						'name' => isset($row['name'])?$row['name']:null,
						'image' => $image['relative_dir'] . $image['new_name'],
						'sort_order' => isset($row['sort_order'])?$row['sort_order']:0,
					);
					$this->goods_image_model->insert($fields);
				}
			}
			else
			{
				$fields = array(
					'goods_id' => $img_id,
					'name' => isset($row['name'])?$row['name']:null,
					'sort_order' => isset($row['sort_order'])?$row['sort_order']:0,
				);

				foreach ($form['subimage'] as $old_img)
				{
					if ($row['id'] == $old_img['id'])
					{
						if ($row['image'] != $old_img['image'])
						{
							$fields['image'] = '';
							if (isset($_FILES['subimage']['name'][$num]) && $_FILES['subimage']['name'][$num] != '') {
								$file = array(
									'name' => $_FILES['subimage']['name'][$num],
									'tmp_name' => $_FILES['subimage']['tmp_name'][$num],
									'type' => $_FILES['subimage']['type'][$num],
									'size' => $_FILES['subimage']['size'][$num],
								);
								$image = $this->upload_image($file, "{$this->router->class}/{$img_id}/");
								$fields['image'] = $image['relative_dir'] . $image['new_name'];
							}
							if ($old_img['image'] != '') {
								remove_file($this->media_path . $old_img['image']);
								remove_file($this->media_path . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $old_img['image']));
							}
						}
						break;
					}
				}

				$this->goods_image_model->update($row['id'], $fields);
			}
		}
	}
	private function _upload_image($img_id, $form, $post)
	{
		// 上傳圖檔 upload image
		if (isset($_FILES['upload']) && $_FILES['upload']['tmp_name'] != '') {
			$image = $this->upload_image($_FILES['upload'], "{$this->router->class}/{$img_id}/");
			$post['image'] = $image['relative_dir'] . $image['new_name'];

			// delete old image
			if ($form['image'] != '') {
				remove_file($this->media_path . $form['image']);
				remove_file($this->media_path . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
			}
		}
		
		// delete image
		if ($post['image'] == '') {
			if ($form['image'] != '') {
				remove_file($this->media_path . $form['image']);
				remove_file($this->media_path . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $form['image']));
			}
		}
		
		return $post['image'];
	}

	public function delete()
    {
		if ($post = $this->input->post())
		{
			$fields['status'] = 1;
			$fields['image'] = '';
			
			foreach ($post['rowid'] as $id)
			{
				$_old = $this->goods_model->get($id);
				remove_file($this->media_path . $_old['image']);
                remove_file($this->media_path . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $_old['image']));
				
				$_old_sub = $this->goods_image_model->getByGoodsId($id);
				if($_old_sub)
				foreach ($_old_sub as $row){
					remove_file($this->media_path . $row['image']);
					remove_file($this->media_path . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']));
					$this->goods_image_model->delete($row['id']);
				}
				
				$rs = $this->goods_model->hidden($id, $fields);
            }
			$this->setAlert(2, '資料刪除成功');
        }

        redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
    }

    public function ajax($action=NULL)
    {
        $result = array(
            'status' => FALSE,
            'data' => array(),
        );

        switch ($action)
        {
            case 'tickets':
                $aid = $this->input->post('aid');
                $conditions = array(
                    'exhibit_id' => $aid,
                );
                $result['data'] = $this->ticket_model->getChoices($conditions);
                $result['status'] = TRUE;
                break;
        }

        echo json_encode($result);
    }

    private function _isVerify($action='add', $old_data=array())
    {
        $config = $this->goods_model->getVerifyConfig();
        if ($action == 'edit') {
        }

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

        return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
    }

    private function _remove_image($path)
    {
        $path_thumb = str_replace('product/', 'product/thumb/', $path);

        if (file_exists($path)) {
            unlink($path);
        }

        if (file_exists($path_thumb)) {
            unlink($path_thumb);
        }
    }

}
