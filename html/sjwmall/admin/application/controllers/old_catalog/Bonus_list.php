<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bonus_list extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->load->model(array(
			'catalog/bonus_model',
			'catalog/bonus_detail_model',
			'catalog/bonus_rule_model',
		));
		$this->data['choices']['rule_kind'] = $this->bonus_rule_model->kind_choices;
		$this->data['choices']['formula_pair'] = $this->bonus_rule_model->formula_pair;
		$this->data['choices']['custom_method'] = $this->bonus_detail_model->custom_method_choices;

		$this->data['filter']['page'] = $this->input->get('page')? $this->input->get('page') : 1;
		$this->data['filter']['sort'] = $this->input->get('sort')? $this->input->get('sort') : NULL;
		$this->data['filter']['q'] = $this->input->get('q')? $this->input->get('q'): '';
	}

	public function index()
	{
		$page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows']?$this->data['filter']['rows'] : $this->db_rows;
		$offset = ($page -1) * $rows;

		$params = array(
			'conditions' => array(
			),
			'order_by' => 'balance_date desc',
			'rows' => $rows,
			'offset' => $offset,
		);
		if ($this->data['filter']['q'] != '') {
			$params['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort']) {
			$params['order_by'] = $this->data['filter']['sort'];
		}

		$total = $this->bonus_model->getDataCount($params);
		$this->data['list'] = $this->bonus_model->getData($params);
		foreach ($this->data['list'] as & $row) {
			$row['link_edit'] = base_url("catalog/{$this->router->class}/edit/{$row['id']}?". $this->getQueryString());
			$row['link_detail'] = base_url("catalog/{$this->router->class}/detail/{$row['id']}?". $this->getQueryString());
		}

		$this->load->library('pagination');
		$config['base_url'] = site_url("{$this->router->class}/index?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $params['rows'];
		$this->pagination->initialize($config);

		$this->data['pagination'] = array(
			'page' => $page,
			'total' => $total,
			'rows' => $rows,
			'offset' => $offset,
		);

		$this->data['link_add'] = base_url("catalog/{$this->router->class}/add?". $this->getQueryString());
		$this->data['link_delete'] = base_url("catalog/{$this->router->class}/delete?". $this->getQueryString());

		$this->layout->view("catalog/{$this->router->class}/list", $this->data);
	}

	public function add()
	{
		if ($post = $this->input->post()) {
			if ($this->_isVerify('add') == TRUE) {
				$saved_id = $this->bonus_model->insert($post, 'create_at');
				if ($saved_id) {
					$this->setAlert(1, '資料新增成功');
				}

				redirect(base_url("catalog/{$this->router->class}"));
			}
		}

		$this->data['form'] =  $this->bonus_model->getFormDefault();

		$this->data['link_save'] = base_url("catalog/{$this->router->class}/add/");
		$this->data['link_cancel'] = base_url("catalog/{$this->router->class}/");
		$this->layout->view("catalog/{$this->router->class}/add", $this->data);
	}

	public function edit($id=NULL)
	{
		$this->data['page_name'] = 'edit';

		$form = $this->bonus_model->get($id);
		if ($post = $this->input->post()) {
			if ($this->_isVerify('edit', $form) == TRUE) {
				$rs = $this->bonus_model->update($id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
			}
			redirect(base_url("catalog/{$this->router->class}?". $this->getQueryString()));
		}

		$this->data['form'] = $this->bonus_model->getFormDefault($this->bonus_model->get($id));

		$this->data['link_save'] = base_url("catalog/{$this->router->class}/edit/{$id}?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("catalog/{$this->router->class}?". $this->getQueryString());
		$this->layout->view("catalog/{$this->router->class}/edit", $this->data);
	}

	public function delete()
	{
		if ($post = $this->input->post()) {
			$del_num = 0;
			foreach ($post['rowid'] as $id) {
				$rs = $this->bonus_model->delete($id);
				if ($rs['status']) {
					$del_num ++;
				}
			}

			$error_num = count($post['rowid']) - $del_num;
			if ($error_num == 0) {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料");
			} else {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除");
			}
		}

		redirect(base_url("catalog/{$this->router->class}?". $this->getQueryString()));
	}

	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->bonus_model->getVerifyConfig();
		if ($action == 'edit') {
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}

	public function detail($id)
	{
		$this->data['bonus'] = $this->bonus_model->get($id);

		$params = array(
			'conditions' => array(
				'bonus_id' => $id,
			),
			'order_by' => 'id asc',
		);
		$this->data['details'] = $this->bonus_detail_model->getData($params);
		if ($this->data['details']) {

			$this->data['link_cancel'] = base_url("catalog/{$this->router->class}/");
			$this->layout->view("catalog/{$this->router->class}/detail", $this->data);

		} else {

			$rules = array();
			$params = array(
				'order_by' => 'kind asc, method asc, range_begin asc',
			);
			$queryset = $this->bonus_rule_model->getData($params);
			foreach ($queryset as $row) {
				$key = $row['note'];
				if (trim($row['note']) == '') {
					$key = 'default';
				}
				$rules[$row['kind']][$row['method']][$key][] = $row;
			}
			unset($queryset);
			$this->data['choices']['rules'] = $rules;

			$this->setJson('choices', $this->data['choices']);

			$this->data['link_detail_import'] = base_url("catalog/{$this->router->class}/detail_import/");
			$this->data['link_save'] = base_url("catalog/{$this->router->class}/detail_save/");
			$this->data['link_cancel'] = base_url("catalog/{$this->router->class}/");
			$this->layout->view("catalog/{$this->router->class}/detail_import", $this->data);
		}
	}

	public function detail_import()
	{
		$result = array(
			'status' => FALSE,
			'message' => '匯入失敗',
			'data' => array(
			),
		);
		if (isset($_FILES['file'])) {
			$this->load->library('excel');
			$file = $_FILES['file']['tmp_name'];
			try {
				$objPHPExcel = PHPExcel_IOFactory::load($file);
			} catch(Exception $e) {
				die('Error loading file "'.pathinfo($file,PATHINFO_BASENAME).'": '.$e->getMessage());
			}

			$data = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
			if ($data) {
				$result['data']['fields'] = array_shift($data);
				$result['data']['sum'] = array(
					'quantity' => 0,
					'total' => 0,
					'tax' => 0,
				);
				$result['data']['details'] = array();
				$result['data']['products'] = array();
				foreach ($data as $row) {
					if ($row[0] != '') {
						$result['data']['sum']['quantity'] += (int) $row[5];
						$result['data']['sum']['total'] += (int) $row[8];
						$result['data']['sum']['tax'] += (int) $row[9];

						$result['data']['details'][] = array(
							'product_no' => trim($row[0]),
							'product_name' => trim($row[1]),
							'customer_no' => trim($row[2]),
							'customer_name' => trim($row[3]),
							'order_no' => trim($row[4]),
							'quantity' => (int) $row[5],
							'unit' => trim($row[6]),
							'price' => (int) $row[7],
							'total' => (int) $row[8],
							'tax' => (int) $row[9],
							'sales' => trim($row[10]),
							'note' => trim($row[11]),
							'new' => substr_count($row[11], '新客戶')>0? 1 : 0,
						);

						// 依照品名切規則類別
						if (!in_array(trim($row[1]), $result['data']['products'])) {
							$rule_kind = 4;  // 其它類別
							if (substr_count($row[1], '家具') > 0) {
								$rule_kind = 1;
							} elseif (substr_count($row[1], '珠寶') > 0) {
								$rule_kind = 3;
							}

							$result['data']['products'][$row[0]] = array(
								'no' => trim($row[0]),
								'name' => trim($row[1]),
								'rule_kind' => $rule_kind,
							);
						}
					}
				}

				$result['status'] = TRUE;
				$result['message'] = 'ok';
			}
		}

		echo json_encode($result);
	}

	
	public function detail_save()
	{
		if ($post = $this->input->post()) {
			// jd($post,1);
			foreach ($post['amount'] as $k => $v) {
				$fields = array(
					'bonus_id' => $post['bonus_id'],
					'sales' => $post['sales'][$k],
					'order_no' => $post['order_no'][$k],
					'customer_no' => $post['customer_no'][$k],
					'customer_name' => $post['customer_name'][$k],
					'product_no' => $post['product_no'][$k],
					'product_name' => $post['product_name'][$k],
					'quantity' => $post['quantity'][$k],
					'price' => $post['price'][$k],
					'total' => $post['total'][$k],
					'note' => $post['note'][$k],
					'bonus_rule_kind' => $post['bonus_rule_kind'][$k],
					'bonus_rate' => $post['bonus_rate'][$k],
					'bonus_single' => $post['bonus_single'][$k],
					'bonus_total' => $post['bonus_total'][$k],
					'custom_method' => NULL,
					'custom_value' => NULL,
					'custom_reason' => NULL,
					'amount' => $v,
				);
				if ((isset($post['custom_method']) && $post['custom_method'] != '')
					&& (isset($post['custom_value']) && $post['custom_value'] != '')
					&& (isset($post['custom_reason']) && $post['custom_reason'] != ''))
				{
					$fields['custom_method'] = $post['custom_method'][$k];
					$fields['custom_value'] = $post['custom_value'][$k];
					$fields['custom_reason'] = $post['custom_reason'][$k];
				}

				$saved_id = $this->bonus_detail_model->insert($fields);
			}

			$this->setAlert(1, '資料新增成功');
			redirect(base_url("catalog/{$this->router->class}"));
		}
	}



}
