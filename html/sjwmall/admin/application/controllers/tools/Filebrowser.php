<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Filebrowser extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}
	}

	public function index()
	{
		$this->data['iframe_url'] = $this->data['static_plugin'] .'ckfinder/ckfinder.html?langCode=zh-tw&CKEditorFuncNum=1';
		$this->layout->view('tools/filebrower', $this->data);
	}

}
