<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_status extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if ($this->flags->is_login === FALSE) {
            redirect(base_url('welcome'));
        }
		
		$this->ctrl_dir = 'sales/order_status';
		$this->view_dir = 'sales/order_status';

        $this->load->model(array(
            'sales/order_status_model',
        ));
		
        if (!isset($this->data['filter']['page'])) {
            $this->data['filter']['page'] = '1';
        }
        if (!isset($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = '';
        }
        if (!isset($this->data['filter']['q'])) {
            $this->data['filter']['q'] = '';
        }
    }

    public function index()
    {
        $this->data['page_name'] = 'list';

        $conditions = array();
        $page = $this->data['filter']['page'];
        $rows = $this->data['filter']['rows'];

        $attrs = array(
            'conditions' => $conditions,
        );
        if ($this->data['filter']['q'] !== '' ) {
            $attrs['q'] = $this->data['filter']['q'];
        }

        $this->data['filter']['total'] = $total = $this->order_status_model->getListCount($attrs);
        $this->data['filter']['offset'] = $offset = ($page -1) * $rows;

        $attrs = array(
            'conditions' => $conditions,
            'rows' => $rows,
            'offset' => $offset,
        );
        if ($this->data['filter']['q'] !== '' ) {
            $attrs['q'] = $this->data['filter']['q'];
        }
        if ($this->data['filter']['sort'] !== '' ) {
            $attrs['sort'] = $this->data['filter']['sort'];
        }
		
        $this->data['list'] = $this->order_status_model->getList($attrs); //echo $this->order_status_model->get_query();
        foreach ($this->data['list'] as & $row) {
            $row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
        }
        $this->load->library('pagination');
        $config['base_url'] = base_url("{$this->ctrl_dir}?". $this->getQueryString(array(), array('page')));
        $config['total_rows'] = $total;
        $config['per_page'] = $rows;
        $this->pagination->initialize($config);
		
        $this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
        //$this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
        $this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");

        $this->layout->view($this->view_dir .'/list', $this->data);
    }

    public function add()
    {
        $this->data['page_name'] = 'add';
        $this->data['form'] =  $this->order_status_model->getFormDefault(); //echo $this->order_status_model->get_query();

        if ($post = $this->input->post()) {
            if ($this->_isVerify('add') == TRUE) {
                $saved_id = $this->order_status_model->insert($post);
                if ($saved_id) {
                    $this->setAlert(1, '資料新增成功');
                }

                redirect(base_url($this->ctrl_dir .'/'));
            }
        }

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/add/");
        $this->data['link_cancel'] = base_url($this->ctrl_dir .'/');
        $this->layout->view($this->view_dir .'/add', $this->data);
    }

    public function edit($id=NULL)
    {
        $this->data['page_name'] = 'edit';
        $this->data['form'] = $this->order_status_model->getFormDefault($this->order_status_model->get($id));

        if ($post = $this->input->post()) {
            $old_data = $this->order_status_model->get($id);
            if ($this->_isVerify('edit', $old_data) == TRUE) {
                $rs = $this->order_status_model->update($id, $post);
                if ($rs) {
                    $this->setAlert(2, '資料編輯成功');
                }
                redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
            }
        }

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/edit/{$id}/?". $this->getQueryString());
        $this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
        $this->layout->view($this->view_dir .'/edit', $this->data);
    }

    public function delete()
    {
        if ($post = $this->input->post()) {
            $del_num = 0;
            foreach ($post['rowid'] as $id) {
                if ($this->order_model->getCount(array('order_status'=>$id)) == 0) {
                    $rs = $this->order_status_model->delete($id);
                    if ($rs['status']) {
                        $del_num ++;
                    }
                }
            }

            $error_num = count($post['rowid']) - $del_num;
            if ($error_num == 0) {
                $this->setAlert(2, "共刪除 {$del_num} 筆資料");
            } else {
                $this->setAlert(3, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除<br>未刪除原因可能是因為該訂單狀態巳被訂單使用。");
            }
        }

        redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
    }

    private function _isVerify($action='add', $old_data=array())
    {
        $config = $this->order_status_model->getVerifyConfig();
        if ($action == 'edit') {
        }

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

        return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
    }

}

