<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->ctrl_dir = 'welcome';
		$this->view_dir = 'welcome';
		
		$this->load->model('sys/login_model');
	}

	public function logout()
	{
		session_unset();
		redirect(base_url('welcome'));
	}

	public function index()
	{
		//var_dump($this->config);
		if ($this->flags->is_login === TRUE) {
			redirect(base_url('dashboard'));
		}

		$error = '';
		$this->data['username'] = $this->input->cookie('username');
		$post = $this->input->post();
	
		if (! empty($post))
		{
			if (! empty($post['username'])) {
				$error = $this->login_check($post);
			} else {
				$error = '帳號請勿空白';
			}
		}
		
		$this->data['error'] = $error;
		
		//if(! empty($error)){
			//$this->setAlert(4, str_replace("\n",'<br>', $error));
		//}

		$this->layout->setLayout('common/layout_base');
		$this->layout->view('login', $this->data);
	}
	
	private function login_check($post)
	{
		$error = '';
		$ip = $this->input->ip_address();
		
		$user_data = $this->user_model->getuserByAccount($post['username']); //echo $this->user_model->get_query(); 
		
		$error = $this->check_user_black($user_data['username'] ,$ip);

		if(empty($error))
		{
			if ($user_data) {
				$error = $this->check_user_data($user_data, $post['password']);
				
				if(empty($error)) {
					if ($post['remember']) {
						setcookie('username', $post['username'], time()+86400*7);  // exist by 7 days
						$this->data['username'] = $post['username'];
					}
					
					//導入首頁
					redirect(base_url('dashboard'));
					exit;
				}
			
			} else {
				$this->check_ip_logs($ip);
				$error = '請確認帳號、密碼';
			}
		}
		
		return $error;		
	}
	
	private function check_user_data($user_data, $post_password)
	{
		$vars['username'] = $user_data['username'];
		$vars['password'] = $post_password;
		$vars['user'] = $user_data;
		
		$rs = $this->login_model->chk_admin_login($vars);

		if($rs['status']=== TRUE)
		{
			//設定 token
			$_token = isset($_SESSION['token']) ? $this->myaes->mcrypt_de($_SESSION['token']) : '';
			if(empty($_token) || ($user_data['token'] !== $_token) ) { 
				$this->setFlags($rs['user']); 
			}
			
			/*//驗證 token
			$user_Account = $this->user_model->getUserByAccount($user_data['username']); 
			$de = $this->myaes->mcrypt_de($_SESSION['token']);
			var_dump($user_Account['token']);var_dump($de ); exit;
			*/
			
			$this->flags->is_login = TRUE;
			$this->flags->user = $rs['user'];
				
			unset($_SESSION['login_error']);
			$error = '';
		}
		else
		{
			$this->check_account_logs($vars['username']);
			$error = $rs['message'];
		}
		
		return $error;
	}
	
	private function check_account_logs($user_name)
	{
		$admin_account_logs = array();
		if (isset($_SESSION['admin_account_logs'])) {
			$admin_account_logs = $_SESSION['admin_account_logs'];
		}

		if (isset($admin_account_logs[$user_name]))
		{
			//check login error numbers for account
			if($admin_account_logs[$user_name] >= 5)
			{
				$unlock_time = date('Y-m-d H:i:s', 60*30 + time());
				$this->user_blacklist_model->insertAccount($user_name, $unlock_time);
				unset($admin_account_logs[$user_name]);
			
			} else {
				$admin_account_logs[$user_name] += 1;
			}
			
		} else {
			$admin_account_logs[$user_name] = 1;
		}

		$_SESSION['admin_account_logs'] = $admin_account_logs;
	}
	private function check_ip_logs($ip)
	{
		$admin_ip_logs = array();
		if (isset($_SESSION['admin_ip_logs'])) {
			$admin_ip_logs = $_SESSION['admin_ip_logs'];
		}

		if (isset($admin_ip_logs[$ip]))
		{
			//check login error numbers for IP
			if ($admin_ip_logs[$ip] >= 5)
			{
				$unlock_time = date('Y-m-d H:i:s', 60*30 + time());
				$this->user_blacklist_model->insertIP($ip, $unlock_time);
				unset($admin_ip_logs[$ip]);
			
			} else {
				$admin_ip_logs[$ip] += 1;
			}
		
		} else {
			$admin_ip_logs[$ip] = 1;
		}

		$_SESSION['admin_ip_logs'] = $admin_ip_logs;
	}
	private function check_user_black($username=0, $ip)
	{
		$error = '';
		
		$lock_time = $this->user_blacklist_model->getLockTimeByIP($ip);
		if ($lock_time) {
			$error = "IP:{$ip} 已鎖定！";//<br>將於「{$lock_time}」解鎖
		}

		$lock_time = $this->user_blacklist_model->getLockTimeByAccount($username);
		if ($lock_time) {
			$error = "帳戶:{$username} 已鎖定！";//<br>將於「{$lock_time}」解鎖
		}	
		
		return $error;
	}
	
}
