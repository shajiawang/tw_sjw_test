<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Schedule extends MY_Controller
{
	public function __construct()
    {
        parent::__construct();

        if ($this->flags->is_login === FALSE) {
            redirect(base_url('welcome'));
        }

        $this->load->model('sys/schedule_list_model');
		$this->ctrl_dir = 'sys/schedule';
		$this->view_dir = 'sys/schedule/';
		

        if (!isset($this->data['filter']['page'])) {
            $this->data['filter']['page'] = '1';
        }
        if (!isset($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = '';
        }
        if (!isset($this->data['filter']['q'])) {
            $this->data['filter']['q'] = '';
        }
    }

    public function index()
    {
        $this->data['page_name'] = 'list';

        $conditions = array();
        $page = $this->data['filter']['page'];
        $rows = $this->data['filter']['rows'];

        $attrs = array(
            'conditions' => $conditions,
        );
        if ($this->data['filter']['q'] !== '' ) {
            $attrs['q'] = $this->data['filter']['q'];
        }

		$this->data['filter']['total'] = $total = $this->schedule_list_model->getListCount($attrs);
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;

        $attrs = array(
            'conditions' => $conditions,
            'rows' => $rows,
            'offset' => $offset,
        );
        if ($this->data['filter']['q'] !== '' ) {
            $attrs['q'] = $this->data['filter']['q'];
        }
        if ($this->data['filter']['sort'] !== '' ) {
            $attrs['sort'] = $this->data['filter']['sort'];
        }
		
        $_rows = $this->schedule_list_model->getList($attrs);
		foreach ($_rows as $k => $v)
		{
			$this->data['list'][$k] = $v;
			$this->data['list'][$k]['venues'] = $v['venues'] .'<br>'. $v['areas'];
			$this->data['list'][$k]['schedule'] = date('m/d', strtotime($v['begin_at'])) .'-'. date('m/d', strtotime($v['end_at']));
			$this->data['list'][$k]['people'] = '統籌指導: '. $v['overall'] .'<br>'.
				'專案協理: '. $v['director'] .'<br>'.
				'專案秘書: '. $v['secretary'] .'<br>'.
				'專案業務: '. $v['manager'] .'<br>'.
				'企劃部: '. $v['marketing'] .'<br>'.
				'策盟部: '. $v['alliances'] .'<br>'.
				'視覺設計: '. $v['design'] .'<br>'.
				'管理部: '. $v['decorate'] .'<br>';
        }
		
		foreach ($this->data['list'] as & $row) {
            //$row['link_view'] = base_url("{$this->ctrl_dir}/view/{$row['id']}/?". $this->getQueryString());
            $row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
        }
		
		$this->load->library('pagination');
        $config['base_url'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString(array(), array('page')));
        $config['total_rows'] = $total;
        $config['per_page'] = $rows;
        $this->pagination->initialize($config);

        $this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
        $this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
        $this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");

	$this->layout->view("{$this->view_dir}list", $this->data);
    }

    public function add()
    {
        $this->data['page_name'] = 'add';

        if ($post = $this->input->post()) {
            if ($this->_isVerify('add') == TRUE) {
                $auth = $post['auth'];
                unset($post['auth']);

                $saved_id = $this->schedule_list_model->insert($post);
                if ($saved_id) {
                    // write group auth
                    foreach ($auth as $menu_id) {
                        $fields = array(
                            'user_group_id' => $saved_id,
                            'menu_id' => $menu_id,
                        );
                        $this->user_group_auth_model->insert($fields);
                    }

                    $this->setAlert(1, '資料新增成功');
                }

                redirect(base_url($this->ctrl_dir .'/'));
            }
        }

        $this->data['form'] = $this->schedule_list_model->getFormDefault();

        $this->data['choices']['menu'] = $this->menu_model->getChoices($this->site);

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/add/");
        $this->data['link_cancel'] = base_url("{$this->ctrl_dir}/");
        $this->data['link_refresh'] = base_url("{$this->ctrl_dir}/add/");

        $this->layout->view("{$this->view_dir}add", $this->data);
    }

    public function edit($id=NULL)
    {
        $this->data['page_name'] = 'edit';
        if ($post = $this->input->post())
		{
            $old_data = $this->schedule_list_model->get($id);
            if ($this->_isVerify('edit', $old_data) == TRUE)
			{
                $auth = isset($post['auth']) ? $post['auth'] : array();
                unset($post['auth']);

                $rs = $this->schedule_list_model->update($id, $post);
                if ($rs)
				{
                    // write group auth
                    $this->user_group_auth_model->delete(array('user_group_id'=>$id));
                    foreach ($auth as $menu_id) {
                        $fields = array(
                            'user_group_id' => $id,
                            'menu_id' => $menu_id,
                        );
                        $this->user_group_auth_model->insert($fields);
                    }

                    $this->setAlert(2, '資料編輯成功');
                }
				
                redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
            }
        }

        $this->data['form'] = $this->schedule_list_model->getFormDefault($this->schedule_list_model->get($id));
        $this->data['form']['auth'] = $this->user_group_auth_model->getByGroupID($id);
		
		$use_site = $this->data['form']['menu_port'];
        $this->data['choices']['menu'] = $this->menu_model->getChoices($use_site);

        $this->data['link_save'] = base_url("{$this->ctrl_dir}/edit/{$id}/?". $this->getQueryString());
        $this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());

        $this->layout->view("{$this->view_dir}edit", $this->data);
    }

    public function delete()
    {
        if ($post = $this->input->post()) {
            foreach ($post['rowid'] as $id) {
                $rs = $this->schedule_list_model->delete($id);
            }
            $this->setAlert(2, '資料刪除成功');
        }

        redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
    }

    private function _isVerify($action='add', $old_data=array())
    {
        $config = $this->schedule_list_model->getVerifyConfig();
        if ($action == 'edit') {
        }

        $this->form_validation->set_rules($config);
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        // $this->form_validation->set_message('required', '請勿空白');

        return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
    }

    public function ajax_toggle($field)
    {
        $result = array(
            'status' => FALSE,
            'message' => '',
        );

        if ($post = $this->input->post()) {
            $rs = $this->schedule_list_model->update($post['pk'], array($field=>$post['value']));
            if ($rs) {
                $result['status'] = TRUE;
            }
        }

        echo json_encode($result);
    }

}
