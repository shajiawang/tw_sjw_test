<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Setting extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->load->model('sys/setting_model');
		$this->data['choices']['setting_kind'] = $this->setting_model->set_kind;
	}

	public function index()
	{
		if ($post = $this->input->post()) {
            if (isset($_FILES['web_logo']) && $_FILES['web_logo']['tmp_name'] != '') {
                $web_logo = $this->setting_model->getByField('web_logo');
                if ($web_logo && $web_logo['value'] != '') {
                    $old_path = $this->media_path.$web_logo['value'];
                    if (file_exists($old_path)) {
                        unlink($old_path);
                    }
                }

                $config['upload_path'] = $this->media_path;
                $config['allowed_types'] = 'gif|jpeg|jpg|png|bmp';
                $config['overwrite'] = TRUE;
                $config['file_ext_tolower'] = TRUE;
                $config['encrypt_name'] = TRUE;
                // $config['max_size'] = '0';
                // $config['max_width']  = '1024';
                // $config['max_height']  = '768';

                $this->load->library('upload', $config);
                if ($this->upload->do_upload('web_logo')){
                    $data = $this->upload->data();
                    // jdd($data);
                    $post['web_logo'] = $data['file_name'];
                } else {
                    // upload error
                }
            }

            $this->setting_model->_update($post);
            $this->setAlert(2, '資料修改成功！');
            redirect(base_url('sys/setting/'));
		}

		$this->data['settings'] = $this->setting_model->getList();
		
        foreach ($this->data['settings'] as & $kind) {
            foreach ($kind as & $row) {
                if ($row['type'] == 'image') {
                    $row['url'] = $this->data['media_url'] . $row['value'];
                } elseif ($row['type'] == 'file') {
                    $row['url'] = $this->data['media_url'] . $row['value'];
                }
            }
        }

		$this->data['link_save'] = base_url('sys/setting');
		$this->data['link_cancel'] = base_url('sys/setting');

		$this->layout->view('sys/setting', $this->data);
	}

}
