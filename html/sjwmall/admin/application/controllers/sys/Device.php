<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Device extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->load->model(array(
			'alpha_user/ticket_device_model',
			'alpha_user/exhibit_model',
			'alpha_user/ticket_model',
		));

	}

	public function index()
	{
		if (empty($this->data['filter']['page'])) {
			$this->data['filter']['page'] = 1;
		}
		if (empty($this->data['filter']['q'])) {
			$this->data['filter']['q'] = '';
		}
		if (empty($this->data['filter']['sort'])) {
			$this->data['filter']['sort'] = '';
		}

		$page = $this->data['filter']['page'];
		$rows = 10;
		$offset = ($page -1) * $rows;

		$params = array(
			'q' => $this->data['filter']['q'],
			'conditions' => array(
			),
			'order_by' => 'create_at desc',
			'rows' => $rows,
			'offset' => $offset,
		);
		if ($this->data['filter']['q'] !== '' ) {
			$params['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort'] !== '' ) {
			$params['order_by'] = $this->data['filter']['sort'];
		}
		$total = $this->ticket_device_model->getListCount($params);
		$list = $this->ticket_device_model->getList($params);
		foreach ($list as & $row) {
			$row['checkin_url'] = "https://checkin.top-link.com.tw/admission/config?device={$row['key']}&gate_no={$row['name']}";
			$row['link_edit'] = base_url("sys/device/edit/{$row['id']}/?". $this->getQueryString());
		}

		$this->load->library('pagination');
		$config['base_url'] = site_url("sys/device/?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $params['rows'];
		$this->pagination->initialize($config);

		$this->data['list'] = $list;
		$this->data['pagination'] = array(
			'page' => $page,
			'total' => $total,
			'rows' => $rows,
			'offset' => $offset,
		);

		$this->data['link_add'] = base_url("sys/device/add/?". $this->getQueryString());
		$this->data['link_delete'] = base_url("sys/device/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("sys/device/");	
		$this->layout->view("sys/device/list", $this->data);
	}

	public function add()
	{
		$form = $this->ticket_device_model->getFormDefault();
		$form['ticket_id'] = array();

		if ($post = $this->input->post()) {
			if ($this->_isVerify('add') == TRUE) {
				$post['key'] = uniqid('', TRUE);
				if (isset($post['ticket_id'])) {
					$post['ticket_id'] = implode(',', $post['ticket_id']);
				}
				$saved_id = $this->ticket_device_model->insert($post, 'create_at');
				if ($saved_id) {
					$this->setAlert(1, '資料新增成功');
				}

				redirect(base_url('sys/device/'));
			}
		}

		$this->data['form'] =  $form;

		$this->data['choices']['exhibits'] = $this->exhibit_model->getChoices(array('enable'=>1));
		$this->data['choices']['tickets'] = array();

		$this->data['link_ajax_get_ticket'] = base_url("sys/device/ajax_get_ticket");
		$this->data['link_save'] = base_url("sys/device/add/");
		$this->data['link_cancel'] = base_url('sys/device/');
		$this->layout->view('sys/device/add', $this->data);
	}

	public function edit($id=NULL)
	{
		$form = $this->ticket_device_model->getFormDefault($this->ticket_device_model->get($id));
		if (!$form) {
			$this->setAlert(4, '錯誤連結請重新操作');
			redirect($_SERVER['HTTP_REFERER']);
		}

		if ($post = $this->input->post()) {
			if ($this->_isVerify('edit', $form) == TRUE) {
				if (isset($post['ticket_id'])) {
					$post['ticket_id'] = implode(',', $post['ticket_id']);
				}
				$rs = $this->ticket_device_model->update($id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				redirect(base_url("sys/device/?". $this->getQueryString()));
			}
		}

		$this->data['form'] = $form;

		$this->data['choices']['exhibits'] = $this->exhibit_model->getChoices(array('enable'=>1));

		$ticket_ids_arr = explode(',', $form['ticket_id']);
		$this->data['choices']['tickets'] = array();
		foreach ($ticket_ids_arr as $tid) {
			$ticket = $this->ticket_model->get($tid);
			$this->data['choices']['tickets'][$tid] = "({$ticket['exhibit_id']}){$this->data['choices']['exhibits'][$ticket['exhibit_id']]} - kid:{$ticket['old_kid']} {$ticket['name']}";
		}

		$this->data['link_ajax_get_ticket'] = base_url("sys/device/ajax_get_ticket");
		$this->data['link_save'] = base_url("sys/device/edit/{$id}/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("sys/device/?". $this->getQueryString());
		$this->layout->view('sys/device/edit', $this->data);
	}

	public function delete()
	{
		if ($post = $this->input->post()) {
			$del_num = 0;
			foreach ($post['rowid'] as $id) {
				$rs = $this->ticket_device_model->delete($id);
				if ($rs) {
					$del_num ++;
				}
			}
			$error_num = count($post['rowid']) - $del_num;
			if ($error_num == 0) {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料");
			} else {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除");
			}
		}

		redirect(base_url("sys/device/?". $this->getQueryString()));
	}

	public function ajax_get_ticket()
	{
		$result = array(
			'status' => FALSE,
			'data' => array(),
		);
		if ($exhibit_id = $this->input->post('exhibit_id')) {
			$params = array(
				'select' => 'id, name, old_kid',
				'conditions' => array(
					'exhibit_id' => $exhibit_id,
				),
			);
			$queryset = $this->ticket_model->getData($params);
			foreach ($queryset as $row) {
				$result['data'][$row['id']] = "kid:{$row['old_kid']} {$row['name']}";
			}
			$result['status'] = TRUE;
		}

		echo json_encode($result);
	}

	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->ticket_device_model->getVerifyConfig();
		if ($action == 'edit') {
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}


}

