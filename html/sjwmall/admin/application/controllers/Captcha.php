<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Captcha extends MI_Controller
{
    public function __construct()
    {
        parent::__construct();

    }

    public function ajax_refresh()
    {
        $cap['image'] = $this->_get_captcha_image();
        echo json_encode($cap);
    }

    private function _get_image($len = 4)
    {
        $this->load->helper('captcha');

        // remove O,0,1,I
        $pool = '23456789ABCDEFGHJKLMNPQRSTUVWXY';

        $word = '';
        for ($i = 0; $i < $len; $i++){
            $word .= substr($pool, mt_rand(0, strlen($pool) -1), 1);
        }

        $vals = array(
            'word'  => $word,
            'img_path'  => DIR_UPLOAD . 'captcha/',
            'img_url'  => HTTP_UPLOAD . 'captcha/',
            'expiration' => 300
            );
        $cap = create_captcha($vals);

        //write session
        $this->session->set_userdata('captcha_'.$this->data['site'], $cap);
        return $cap['image'];
    }

    private function _chk_captcha($captcha)
    {
        $session_captcha = $this->session->userdata('captcha_'.$this->data['site']);
        $word = $session_captcha['word'];
        if ($word == $captcha || strtolower($word) == $captcha) {
            return true;
        } else {
            return false;
        }
    }
}

