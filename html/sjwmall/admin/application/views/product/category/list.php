<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list fa-lg"></i> <?=$_LOCATION['name'];?>
            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <form id="filter-form" role="form" class="form-inline">
                    <input type="hidden" name="sort" value="" />
                    <div class="row">
                        <div class="col-xs-6" >
                            <!--
                            <div class="form-group">
                                <label class="control-label">顯示筆數</label>
                                <?php
                                    echo form_dropdown('rows', $choices['rows'], $filter['rows'], 'class="form-control"');
                                ?>
                            </div>
                            -->
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="form-group">

                            </div>
                        </div>
                    </div>
                </form>

                <form id="list-form" method="post">
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 35px;"><input type="checkbox" id="chkall"></th>
                                <th>名稱</th>
                                <th>圖片</th>
                                <th>描述</th>
                                <th>排序</th>
                                <th>啟用</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr class="<?=($row['enable']==0)?'text-danger':'';?>">
                                <td class="text-center"><input type="checkbox" name="rowid[]" value="<?=$row['id'];?>"></td>
                                <td><?=$row['full_name'];?></td>
                                <td>
                                    <?php
                                        if ($row['image']) {
                                            echo '<a href="'.$row['image_url'].'" rel="fancybox_group" data-title="'.$row['name'].'">';
                                            echo img(array('src'=>$row['image_thumb_url'], 'class'=>'img-rounded', 'style'=>'max-height: 80px;'));
                                            echo '</a>';
                                        }
                                    ?>
                                </td>
                                <td><?=$row['description'];?></td>
                                <td><?=$row['sort_order'];?></td>
                                <td><?=($row['enable'] == 1)?'<i class="fa fa-check text-success"></i>':'<i class="fa fa-ban text-danger"></i>';?></td>
                                <td class="text-center" id="btn_group">
                                    <?php if (isset($row['link_view'])) { ?>
                                    <a type="button" class="btn btn-outline btn-success btn-xs btn-toggle" title="View" href="<?=$row['link_view'];?>">
                                        <i class="fa fa-eye fa-lg"></i>
                                    </a>
                                    <?php } ?>
                                    <?php if (isset($row['link_edit'])) { ?>
                                    <a type="button" class="btn btn-outline btn-warning btn-xs btn-toggle" href="<?=$row['link_edit'];?>">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                    <?php } ?>
                                    <?php if (isset($row['link_delete'])) { ?>
                                    <button type="button" class="btn btn-outline btn-danger btn-xs" onclick="ajaxDelete(this, '確認要刪除選單「<?=$row['name'];?>」?', '<?=$row['link_delete'];?>')">
                                        <i class="fa fa-trash fa-lg"></i>
                                    </button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>

                <div class="row">
                    <div class="col-md-5">
                        共 <?=$total;?> 筆
                    </div>
                    <div class="col-md-7 text-right">
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<!-- highlight JS -->
<script src="<?=$static_plugin;?>jquery.highlight-3.js"></script>
<script>
$(document).ready(function() {
    $('#filter-form select').change(function(){
        $('#filter-form').submit();
    });

    <?php if (isset($filter['q']) && $filter['q'] != ''){ ?>
    $('#list-form').highlight('<?=$filter['q'];?>');
    <?php } ?>
});
</script>
