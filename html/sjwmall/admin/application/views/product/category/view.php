<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-eye fa-fw"></i> <?=$_LOCATION['name'];?>
            </div>
            <div class="panel-body">
                <table class="table table-hover table-bordered table-condensed">
                <tr>
                        <th>上層分類</th>
                        <td>
                        <?php $choices['category'][0] = "無上層分類" ;?>
                        <?=$choices['category'][$form['parent_id']];?>

                        </td>
                    </tr>
                    <tr>
                        <th>圖片</th>
                        <td>
                                <?php
                                if (strlen($form['image']) > 0) {
                                    echo '<a rel="fancybox_group" href="'. $form['image_src'] .'">';
                                    echo '  <img src="'. $form['image_thumb_src'] .'" class="img-rounded" style="width: 60px; height: 50px;" />';
                                    echo '</a>';
                                   } else{
                                        echo '<i class="fa fa-plus fa-2x">noimge</i>' ;
                                    }
                                    if(isset($form['subimage'])){
                                   foreach ($form['subimage'] as $num => $row) {
                                    if (strlen($row['image']) > 0) {
                                        echo '<a rel="fancybox_group" href="'. $row['image_src'] .'">';
                                        echo '  <img src="'. $row['image_thumb_src'] .'" class="img-rounded" style="width: 60px; height: 50px;" />';
                                        echo '</a>';
                                    }else{
                                        echo '<i class="fa fa-plus fa-2x">noimge</i>' ;
                                    }
                                   }
                               }
                                   //jdd($form['subimage']) ;

                                ?>
                        </td>
                    </tr>
                    <tr>
                        <th width="20%">名稱</th>
                        <td style="background-color: #fff;">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <li>
                                        <a href="#title_<?=$lang['code'];?>" role="tab" data-toggle="tab">
                                            <?=img(HTTP_IMG ."flags/24/{$lang['image']}");?>
                                            <?=$lang['name'];?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" style="padding-top: 8px;">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <?php
                                        $title = isset($form['name'][$lang['id']])? $form['name'][$lang['id']] : '';
                                    ?>
                                    <div class="tab-pane" id="title_<?=$lang['code'];?>"><?=$title;?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>描述</th>
                        <td style="background-color: #fff;">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <li>
                                        <a href="#content_<?=$lang['code'];?>" role="tab" data-toggle="tab">
                                            <?=img(HTTP_IMG ."flags/24/{$lang['image']}");?>
                                            <?=$lang['name'];?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" style="padding-top: 8px;">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <?php
                                        $content = isset($form['description'][$lang['id']])? $form['description'][$lang['id']] : '';
                                    ?>
                                    <div class="tab-pane" id="content_<?=$lang['code'];?>"><?=$content;?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>排序</th>
                        <td><?=$form['sort_order'];?></td>
                    </tr>
                    <tr>
                        <th>啟用</th>
                        <td><?=($form['enable']=='1')?'<span style="color: green;">是</span>':'<span style="color: red;">否</span>';?></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
$(function(){
    $('ul.nav-tabs').each(function(){
        $(this).find('li:first').addClass('active');
    });
    $('div.tab-content').each(function(){
        $(this).find('.tab-pane:first').addClass('active');
    });
});
</script>
