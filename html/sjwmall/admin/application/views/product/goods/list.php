<script src="<?=$static_plugin;?>jquery.highlight-3.js"></script>
<script>
$(document).ready(function() {
    $('#filter-form select').change(function(){
        $('#filter-form').submit();
    });

    <?php if (isset($filter['q']) && $filter['q'] != ''){ ?>
    $('#list-form').highlight('<?=$filter['q'];?>');
    <?php } ?>
});
</script>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-list fa-lg"></i> <?=$_LOCATION['name'];?>
            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <form id="filter-form" role="form" class="form-inline">
                    <input type="hidden" name="sort" value="" />
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label">類別</label>
                                <?php
                                    $choices_category = array('all'=>'全部') + $choices['category'];
                                    echo form_dropdown('category_id', $choices_category, $filter['category_id'], 'class="form-control"');
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-6" >
                            <div class="form-group">
                                <label class="control-label">顯示筆數</label>
                                <?php
                                    echo form_dropdown('rows', $choices['rows'], $filter['rows'], 'class="form-control"');
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="form-group">
                                <label class="control-label"><i class="fa fa-search"></i></label>
                                <input type="text" class="form-control" name="q" value="<?=$filter['q'];?>">
                            </div>
                        </div>
                    </div>
                </form>

                <form id="list-form" method="post">
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 35px;"><input type="checkbox" id="chkall"></th>
                                <th class="sorting<?=($filter['sort']=='name asc')?'_asc':'';?><?=($filter['sort']=='name desc')?'_desc':'';?>" data-field="name" >產品名稱</th>
                                <th class="sorting<?=($filter['sort']=='no asc')?'_asc':'';?><?=($filter['sort']=='no desc')?'_desc':'';?>" data-field="no" >產品編號</th>
                                <th class="sorting<?=($filter['sort']=='image asc')?'_asc':'';?><?=($filter['sort']=='image desc')?'_desc':'';?>" data-field="image" >圖片</th>
                                <th class="sorting<?=($filter['sort']=='price asc')?'_asc':'';?><?=($filter['sort']=='price desc')?'_desc':'';?>" data-field="price" >價格</th>
                                <th class="sorting<?=($filter['sort']=='sort_order asc')?'_asc':'';?><?=($filter['sort']=='sort_order desc')?'_desc':'';?>" data-field="sort_order" >排序</th>
                                <th class="sorting<?=($filter['sort']=='recommend asc')?'_asc':'';?><?=($filter['sort']=='recommend desc')?'_desc':'';?>" data-field="recommend" >推薦</th>
                                <th class="sorting<?=($filter['sort']=='enable asc')?'_asc':'';?><?=($filter['sort']=='enable desc')?'_desc':'';?>" data-field="enable" >啟用</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr class="<?=($row['enable']==0)?'text-danger':'';?>">
                                <td class="text-center"><input type="checkbox" name="rowid[]" value="<?=$row['id'];?>"></td>
                                <td><?=$row['name'];?></td>
                                <td><?=$row['no'];?></td>
                                <td>
                                    <?php
                                        if ($row['image']) {
                                            echo '<a href="'.$row['image_url'].'" rel="fancybox_group" data-title="'.$row['name'].'">';
                                            echo img(array('src'=>$row['image_thumb_url'], 'class'=>'img-rounded', 'style'=>'max-width: 300px;max-height: 100px;'));
                                            echo '</a>';
                                        }
                                    ?>
                                </td>
                                <td>
                                <?php
                                    if ($row['sale_price']) {
                                        echo '<b>$'. number_format($row['price_sale'] ,2) .'</b>';
                                        echo '<br><span style="text-decoration: line-through; color: #999; font-size: 85%;">$'. number_format($row['price'] ,2) .'</span>';
                                    } else {
                                        echo '<b>$'. number_format($row['price'] ,2) .'</b>';
                                    }
                                ?>
                                </td>
                                <td><?=$row['sort_order'];?></td>
                                <td><?=($row['recommend'] == 1)?'<i class="fa fa-check text-success"></i>':'';?></td>
                                <td><?=($row['enable'] == 1)?'<i class="fa fa-check text-success"></i>':'<i class="fa fa-ban text-danger"></i>';?></td>
                                <td class="text-center" id="btn_group">
                                    <?php if (isset($row['link_view'])) { ?>
                                    <a type="button" class="btn btn-outline btn-success btn-xs btn-toggle" title="View" href="<?=$row['link_view'];?>">
                                        <i class="fa fa-eye fa-lg"></i>
                                    </a>
                                    <?php } ?>
                                    <?php if (isset($row['link_edit'])) { ?>
                                    <a type="button" class="btn btn-outline btn-warning btn-xs btn-toggle" title="Edit" href="<?=$row['link_edit'];?>">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                    <?php } ?>
                                    <?php if (isset($row['link_copy'])) { ?>
                                    <a type="button" class="btn btn-outline btn-warning btn-xs btn-toggle" title="Copy" href="<?=$row['link_copy'];?>">
                                        <i class="fa fa-files-o fa-lg"></i>
                                    </a>
                                    <?php } ?>
                                    <?php if (isset($row['link_delete'])) { ?>
                                    <button type="button" class="btn btn-outline btn-danger btn-xs" onclick="ajaxDelete(this, '確認要刪除選單「<?=$row['name'];?>」?', '<?=$row['link_delete'];?>')">
                                        <i class="fa fa-trash fa-lg"></i>
                                    </button>
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>

                <div class="row">
                    <div class="col-md-5">
                        顯示 <?=$pagination['offset']+1;?>
                        ~ <?=(($pagination['offset']+$pagination['rows'])>$pagination['total'])?$pagination['total']:$pagination['offset']+$pagination['rows'];?>
                        共 <?=$pagination['total'];?> 筆
                    </div>
                    <div class="col-md-7 text-right">
                        <?=$this->pagination->create_links();?>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

