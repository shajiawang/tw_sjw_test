<?php if (validation_errors()) { ?>
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <?php echo validation_errors();?>
</div>
<?php } ?>
<form id="data-form" role="form" method="post" action="<?php echo $link_save;?>">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />

    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" id="tab_data">
        <li><a href="#general" role="tab" data-toggle="tab">一般資料</a></li>
        <!-- <li><a href="#locale" role="tab" data-toggle="tab">語系資料</a></li> -->
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" style="padding: 15px;">

        <div class="tab-pane" id="general">
            <div class="form-group required <?=form_error('name')?'has-error':'';?>">
                <label class="control-label">名稱</label>
                <input class="form-control" name="name" value="<?=set_value('name', $form['name']);?>">
                <?=form_error('name'); ?>
            </div>
            <div class="form-group <?=form_error('fee')?'has-error':'';?>">
                <label class="control-label">運費</label>
                <input class="form-control" name="fee" placeholder="" value="<?=set_value('fee', $form['fee']); ?>">
                <?=form_error('fee');?>
                <p class="help-block">若不需要運費，請輸入0</p>
            </div>
            <div class="form-group <?php echo form_error('sort_order')?'has-error':'';?>">
                <label class="control-label">排序</label>
                <input class="form-control" name="sort_order" placeholder="" value="<?php echo set_value('sort_order', $form['sort_order']); ?>">
                <?php echo form_error('sort_order'); ?>
            </div>

            <div class="form-group <?php echo form_error('enable')?'has-error':'';?>">
                <label class="control-label">是否啟用</label>
                <div>
                    <div class="radio-inline">
                        <label>
                            <input id="enable_1" type="radio" value="1" name="enable" <?=set_radio('enable', '1', $form['enable']==1);?>>
                            <span style="color: green;">是　</span>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input id="enable_0" type="radio" value="0" name="enable" <?=set_radio('enable', '0', $form['enable']==0);?>>
                            <span style="color: red;">否　</span>
                        </label>
                    </div>
                    <?php echo form_error("enable");?>
                </div>
            </div>
        </div>


    </div>

</form>

<script>
$(function() {
    $('#tab_data a:first').tab('show');
    $('#tab_language a:first').tab('show');
});
</script>
