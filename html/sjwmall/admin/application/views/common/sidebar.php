            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">

                        <?php if($_MENU)
						foreach ($_MENU as $menu) {
						if (in_array($menu['id'], $this->flags->permission) || $menu['auth'] == 0) 
						{ ?>
                        <li class="">
                            <?php if (count($menu['sub']) == 0) { ?>
                                <?php if (in_array($menu['id'], $this->flags->permission) || $menu['auth'] == 0) { ?>
                                <a href="<?=base_url($menu['link']);?>">
                                    <?php if($menu['icon']!='') echo "<i class=\"fa {$menu['icon']} fa-fw\"></i>";?>
                                    <?=" {$menu['name']}";?>
                                </a>
                                <?php } ?>
                            <?php } else { ?>
                            <a href="<?=base_url($menu['link']);?>">
                                <?php if($menu['icon']!='') echo "<i class=\"fa {$menu['icon']} fa-fw\"></i>";?>
                                <?=" {$menu['name']}";?> <span class="fa arrow"></span>
                            </a>
                            <ul class="nav nav-second-level">
                                <?php foreach ($menu['sub'] as $item) { ?>
                                    <?php if (in_array($item['id'], $this->flags->permission) || $menu['auth'] == 0) { ?>
                                    <li>
                                        <a href="<?=base_url($item['link']);?>">
                                            &emsp;
                                            <?php if($item['icon']!='') echo "<i class=\"fa {$item['icon']} fa-fw\"></i>";?>
                                            <?="<i class=\"fa fa-angle-double-right\"></i> {$item['name']}";?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                <?php } ?>
                            </ul>
                            <?php } ?>
                        </li>
                        <?php } } ?>
                    
                        <?php if($user_info['edit']){ ?>
						<li>
							<a href="<?=base_url("info/?id={$user_info['user_id']}");?>"><i class="fa fa-edit fa-fw"></i> 設定廠商資訊</a>
                        </li>
						<?php } ?>
						
						<li>
							<a href="<?=base_url('profile');?>"><i class="fa fa-user fa-fw"></i> 用戶資料</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?=base_url('logout');?>"><i class="fa fa-sign-out fa-fw"></i> 登出</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
