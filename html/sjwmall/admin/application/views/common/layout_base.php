<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset=utf-8"utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=$_SETTING['admin_title'];?></title>

    <!-- Bootstrap Core CSS -->
    <link href="<?=$static_plugin;?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=$static_css;?>sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?=$static_plugin;?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery -->
    <script src="<?=$static_plugin;?>jquery-1.11.2.min.js"></script>

	<!-- Bootstrap Core JavaScript -->
    <script src="<?=$static_plugin;?>bootstrap/dist/js/bootstrap.min.js"></script>
	
	<!-- Noty jquery notification plugin -->
    <script src="<?=$static_plugin;?>noty/packaged/jquery.noty.packaged.min.js"></script>

	<?php if (isset($_JSON)): ?>
		<script type="text/javascript">
			var MY = MY || <?=json_encode($_JSON);?> || {};
		</script>
	<?php endif; ?>

</head>
<body>
	<?php
		// Loading inner content page
		echo $__content;
	?>

	<!-- Custom Theme JavaScript -->
    <!-- <script src="<?=$static_js;?>sb-admin-2.js"></script> -->
	<script src="<?=$static_plugin;?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?=$static_plugin;?>bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js"></script>
	<script src="<?=$static_plugin;?>bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?=$static_plugin;?>bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-TW.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?=$static_plugin;?>datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?=$static_plugin;?>datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <script src="<?=$static_plugin;?>msdropdown/js/jquery.dd.min.js" type="text/javascript"></script>
    <script src="<?=$static_plugin;?>moment-with-locales.js"></script>
    <script src="<?=$static_plugin;?>jStarbox/jstarbox.js"></script>
	
	<script src="<?=$static_js;?>my.js"></script>
	
</body>
</html>
