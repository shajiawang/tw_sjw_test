<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-pencil fa-lg"></i> 設定廠商資訊
			</div>
			<div class="panel-body">
				
				<link href="<?=$static_plugin;?>lou-multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
				<style>
				.ms-container {
					width: 100%;
				}
				.ms-container .ms-list  {
					height: 300px;
				}
				</style>

				<?php if (validation_errors()) { ?>
				<div class="alert alert-danger">
					<button class="close" data-dismiss="alert" type="button">×</button>
					<?=validation_errors();?>
				</div>
				<?php } ?>
				
				<!-- .data-form -->
				<form id="data-form" role="form" method="post" action="<?=$link_save;?>">
					<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="tab_data">
                    <?php foreach ($tablist as $k => $v) { ?>
                    <?php if (isset($form_field[$k])) { ?>
                    <li><a href="#<?php echo $k;?>" role="tab" data-toggle="tab"><?php echo $v;?></a></li>
                    <?php } ?>
                    <?php } ?>
                    </ul>
					
					<!-- Tab panes -->
                    <div class="tab-content" style="padding: 15px;">
					<?php foreach ($tablist as $k => $v) 
					{ ?>
                            <div class="tab-pane" id="<?=$k;?>">
                                <?php if (isset($form_field[$k])) { ?>
                                <?php foreach ($form_field[$k] as $_field=>$row)
								{ ?>
								
								<div class="form-group">
                                    <label><?=$row['label'];?> </label>
                                    
									<?php if ($row['type'] == 'select') {
										echo form_dropdown($_field, $form_options[$_field], $company_info[$_field], 'class="form-control" '. $row['disabled']);
									} elseif ($row['type'] == 'radio') { ?>

                                    <div>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" value="1" name="<?=$_field;?>" <?php //=set_radio($_field, '1', $row['value']==1);?> <?=$row['disabled'];?> >
                                                <span class="label label-success">啟用</span>
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" value="0" name="<?=$_field;?>" <?php //=set_radio($_field, '0', $row['value']==0);?> <?=$row['disabled'];?> >
                                                <span class="label label-danger">停用</span>
                                            </label>
                                        </div>
                                        <?=form_error("enable");?>
                                    </div>

                                    <?php } elseif ($row['type'] == 'file') { ?>

                                    <?php /*if ($row['image_src'] != '') { ?>
                                    <div class="help-block">
                                        <img src="<?=$row['image_src'];?>">
                                    </div>
                                    <?php }*/ ?>
                                    <input  type="file" name="<?=$_field;?>">

                                    <?php } elseif ($row['type'] == 'textarea') { ?>

                                    <textarea class="form-control" <?=$row['disabled'];?> name="<?=$_field;?>" ><?=$company_info[$_field];?></textarea>

                                    <?php } else { ?>

                                    <input class="form-control" <?=$row['disabled'];?> name="<?=$_field;?>" value="<?=$company_info[$_field];?>">

                                    <?php } ?>

                                    <?php if (strlen($row['remark']) > 0) { ?>
                                    <p class="help-block"><?=$row['remark'];?></p>
                                    <?php } ?>
                                </div>
								
                                <?php } ?>
                                <?php } ?>
                            </div>
                    <?php } ?>
							
                    </div>

				</form>
				<!-- /.data-form -->
				
			</div>
		</div>
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<script>
$(function() {
    $('#tab_data a:first').tab('show');
    $('#tab_locale a:first').tab('show');
});
</script>

