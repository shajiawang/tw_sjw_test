<?php if (validation_errors()) { ?>
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <?=validation_errors();?>
</div>
<?php } ?>
<form id="data-form" role="form" method="post" action="<?=$link_save;?>">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
	<input type="hidden" name="user_group_id" value="<?=$form['user_group_id']?>" />

    <div class="form-group required <?=form_error('name')?'has-error':'';?>">
        <label class="control-label">會員名稱</label>
        <input class="form-control" name="name" placeholder="" value="<?=set_value('name', $form['name']);?>">
        <?=form_error('name');?>
    </div>
	
	<div class="form-group required <?=form_error('telephone')?'has-error':'';?>">
        <label class="control-label">手機號碼</label>
        <input class="form-control" name="telephone" placeholder="0999000000" value="<?=set_value('telephone', $form['telephone']);?>">
        <?=form_error('telephone');?>
    </div>
	
    <?php /*
	<div class="form-group required <?=form_error('account')?'has-error':'';?>">
        <label class="control-label">會員帳號</label>
        <?php if ($page_name == 'add') { ?>
        <input class="form-control" name="account" placeholder="" value="<?=set_value('account', $form['account']);?>">
        <?php } else { ?>
        <input class="form-control" name="account" placeholder="" value="<?=$form['account'];?>" disabled>
        <?php }?>
        <?=form_error('account');?>
    </div>*/?>
	
	<div class="form-group required <?=form_error('password')?'has-error':'';?>">
        <label class="control-label">密碼</label>
        <input class="form-control" type="password" name="password" placeholder="" value="<?=set_value('password');?>">
        <?=form_error('password');?>
    </div>
	
    <div class="form-group <?=form_error('email')?'has-error':'';?>">
        <label class="control-label">E-Mail</label>
        <input class="form-control" name="email" placeholder="email@example.com" value="<?=set_value('email', $form['email']);?>">
        <?=form_error('email');?>
    </div>
	
	<?php 
	if($this->flags->user['is_admin']=='1'){
	?>
    <div class="form-group required <?=form_error('')?'has-error':'';?>">
        <label class="control-label">是否啟用</label>
        <div>
            <?php
                $enable_0 = $form['enable']==0? TRUE : FALSE;
				$enable_1 = $form['enable']==1? TRUE : FALSE;
				$enable_2 = $form['enable']==2? TRUE : FALSE;
            ?>
            <div class="radio-inline">
                <label>
                    <input id="enable_2" type="radio" value="2" name="enable" <?=set_radio('enable', '2', $enable_2);?>>
                    <span style="color: green;">啟用　</span>
                </label>
            </div>
			<div class="radio-inline">
                <label>
                    <input id="enable_1" type="radio" value="1" name="enable" <?=set_radio('enable', '1', $enable_1);?>>
                    <span style="color: green;">刪除　</span>
                </label>
            </div>
            <div class="radio-inline">
                <label>
                    <input id="enable_0" type="radio" value="0" name="enable" <?=set_radio('enable', '0', $enable_0);?>>
                    <span style="color: red;">停用　</span>
                </label>
            </div>
            <?=form_error("enable");?>
        </div>
    </div>
	<?php } else { ?>
		<input type="hidden" name="enable" value="<?=$form['enable']?>" />
	<?php } ?>

</form>
