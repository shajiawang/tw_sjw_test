<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-pencil fa-lg"></i> <?php echo $_LOCATION['name'];?>
            </div>
            <div class="panel-body">
                <form id="form-data" role="form" method="post" action="<?php echo $link_save;?>">
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist" id="tab_data">
                    <?php foreach ($choices['setting_kind'] as $k => $v) { ?>
                    <?php if (isset($settings[$k])) { ?>
                    <li><a href="#<?php echo $k;?>" role="tab" data-toggle="tab"><?php echo $v;?></a></li>
                    <?php } ?>
                    <?php } ?>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content" style="padding: 15px;">
                            <?php foreach ($choices['setting_kind'] as $k => $v) { ?>
                            <div class="tab-pane" id="<?=$k;?>">
                                <?php if (isset($settings[$k])) { ?>
                                <?php foreach ($settings[$k] as $row) { ?>
                                <div class="form-group">
                                    <label><?=$row['name'];?> </label>
                                    <?php if ($row['type'] == 'radio') { ?>

                                    <div>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" value="1" name="<?=$row['field'];?>" <?=set_radio($row['field'], '1', $row['value']==1);?>>
                                                <span class="label label-success">啟用</span>
                                            </label>
                                        </div>
                                        <div class="radio-inline">
                                            <label>
                                                <input type="radio" value="0" name="<?=$row['field'];?>" <?=set_radio($row['field'], '0', $row['value']==0);?>>
                                                <span class="label label-danger">停用</span>
                                            </label>
                                        </div>
                                        <?=form_error("enable");?>
                                    </div>

                                    <?php } elseif ($row['type'] == 'file') { ?>

                                    <?php if ($row['image_src'] != '') { ?>
                                    <div class="help-block">
                                        <img src="<?=$row['image_src'];?>">
                                    </div>
                                    <?php } ?>
                                    <input  type="file" name="<?=$row['field'];?>">

                                    <?php } elseif ($row['type'] == 'textarea') { ?>

                                    <textarea class="form-control" name="<?=$row['field'];?>"><?=$row['value'];?></textarea>

                                    <?php } else { ?>

                                    <input class="form-control" name="<?=$row['field'];?>" value="<?=$row['value'];?>">

                                    <?php } ?>

                                    <?php if (strlen($row['remark']) > 0) { ?>
                                    <p class="help-block"><?=$row['remark'];?></p>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                                <?php } ?>
                            </div>
                            <?php } ?>
                            <div class="form-group text-center">
                                <button class="btn btn-primary" title="Save"><i class="fa fa-save"></i> 送出</button>
                                <a class="btn btn-default" href="<?=$link_cancel;?>" title="Cancel"><i class="fa fa-reply"></i> 取消</a>
                            </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
$(function() {
    $('#tab_data a:first').tab('show');
    $('#tab_locale a:first').tab('show');
});
</script>
