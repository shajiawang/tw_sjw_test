<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-pencil fa-lg"></i> <?php echo $_LOCATION['name'];?>
            </div>
            <div class="panel-body">
                    <iframe src="<?php echo $iframe_url;?>" width="100%" height="500" frameborder="0" scrolling="no"></iframe>
            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->


