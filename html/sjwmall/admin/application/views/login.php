<!-- MetisMenu CSS -->
<link href="<?=$static_css;?>bootsnipp.min.css" rel="stylesheet">

<link href="<?=$static_css;?>login.css" rel="stylesheet">

<nav class="navbar navbar-fixed-top navbar-bootsnipp animate" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <div class="animbrand">
	  <a class="navbar-brand animate" href="<?=ROOT_URL;?>admin/dashboard"><?=$_SETTING['admin_title'];?></a>
      </div>
    </div>
  </div>
</nav>
<div class="container">
    <div class="row" style="margin-top:60px;height: 100%;">
        <div class="col-md-4 col-md-offset-4">
            <form method="POST" role="form" id="loginform" class="form-signin">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <fieldset>
                    <h3 class="sign-up-title" style="color:dimgray; text-align: center">Please sign in</h3>

                    <?php if ($error) { ?>
                    <div id="alert_error" class="alert bs-callout bs-callout-danger alert-dismissible" role="alert">
                        <button class="close" aria-label="Close" data-dismiss="alert" type="button">
                            <span aria-hidden="true">×</span>
                        </button>
                        <h5><?=$error;?></h5>
                    </div>
                    <?php } ?>

                    <hr class="colorgraph">
                    <input class="form-control email-title" placeholder="Username" name="username" type="text" value="<?=$username;?>">
                    <input class="form-control" placeholder="Password" name="password" type="password">
                    
                    <div class="checkbox" style="width:140px;">
                    <label><input name="remember" type="checkbox" checked>Remember Me</label>
                    </div>
                    <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                    <br>
                    
                </fieldset>
            </form>
        </div>
  </div>
</div>
<footer class="footer" role="contentinfo">
  <div class="container">
	Copyright 2016 SJ Mall
  </div>
</footer>
<script>
$(document).ready(function(){
    if ($('[name=username]').val().length > 0) {
        $('[name=password]').focus();
    } else {
        $('[name=username]').focus();
    }
});
</script>

