<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Acl  // Access Control List
{
    private $CI;

    public function __construct($config = array())
    {
        $this->CI = get_instance();

        $this->CI->load->model('sys/user_model');
        $this->CI->load->model('sys/user_group_model');
        $this->CI->load->model('sys/user_group_auth_model');
        $this->CI->load->model('sys/menu_model');
		
		//例外路徑
		$this->exception_list = array(
                '',
                'welcome',
                'logout',
                'profile',
				'info',
		);
		
		$this->uri_string = $this->CI->uri->uri_string();
    }
	
	public function auth_exception()
    {
		$current_url = current_url();
		$uri_array = $this->CI->uri->segment_array();

		$uri_last = array_pop($uri_array);
		if (is_numeric($uri_last)) {
			$this->uri_string = implode('/', $uri_array);
		}
		
		// not checking uri
		if (in_array($this->uri_string, $this->exception_list)) {
			return TRUE;
		}
	}

    public function permission()
    {
		if(!empty($this->CI->flags->user['id']) ){
			$user = $this->CI->user_model->get($this->CI->flags->user['id']); 
		}
		
		if (!empty($user))
		{
			// ajax
            if ($this->CI->input->is_ajax_request()) {
                return TRUE;
            }
			
			//判斷例外路徑
			if($this->auth_exception() ){
				return TRUE;
			}
			
			$group_arr = explode(",", $user['user_group_id']);
			$permission = $this->CI->user_group_auth_model->getByGroupStr($group_arr);
		
			$conditions = array(
                'port' => $this->CI->kind,
				'link' => $this->uri_string,
                'enable' => 1,
            );
			
            $menu = $this->CI->menu_model->get($conditions);//echo $this->CI->menu_model->get_query(); 
			if ($menu) {
				if (in_array($menu['id'], $permission) || $menu['auth'] == 0) {
                    return TRUE;
                }
            }
			
			$return_url = (isset($_SERVER['HTTP_REFERER']))? $_SERVER['HTTP_REFERER'] : base_url();
			show_error('你沒有權限訪問該頁面，請點擊<a href="'. $return_url .'">返回</a>');
        }
		
    }
	
}

