<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_history_model extends MY_Model
{
	public $table = 'ec_order_history';
	public $pk = 'id';

	public function __construct()
	{
		parent::__construct();

		$this->init($this->table, $this->pk);
	}

	public function getRowByOrderID($oid)
	{
		$this->db->from($this->table);
		$this->db->where("order_id", $oid);
		$query = $this->db->get(); //echo $this->db->last_query();
		$data = $query->row_array();
		
		return $data;
	}
	
	public function getListByOrderID($oid)
	{
		$this->db->from($this->table);
		$this->db->join('member', "{$this->table}.user_id=member.id", 'left');
		$this->db->select("{$this->table}.*, member.name as user_name");
		$this->db->where("{$this->table}.order_id", $oid);
		$this->db->order_by('id');
		$query = $this->db->get();
		$data = $query->result_array();
		
		foreach ($data as & $row) {
			if ($row['user_name'] == '') {
				$row['user_name'] = '系統新增';
			}
		}

		return $data;
	}

	public function _get($id)
	{
		$this->db->from($this->table);
		$this->db->join('user', "{$this->table}.user_id=user.id");
		$this->db->select("{$this->table}.*, user.name as user_name");
		$this->db->where("{$this->table}.{$this->pk}", $id);
		$query = $this->db->get();
		return $query->row_array();
	}

}


