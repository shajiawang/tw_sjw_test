<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login_model extends MY_Model
{
	public function __construct()
	{
		parent::__construct();

		$this->init($this->table, $this->pk);
		
		$this->load->model('sys/user_blacklist_model');
	}

	///// 註冊新用戶 ////////////////////
	
	public function chk_join( $vars=array() )
	{
		$phone = isset($vars['phone']) ? $vars['phone'] : NULL;
		$password = isset($vars['password']) ? $vars['password'] : NULL;
		
		/* 帳號、密碼 */
		$result = array(
			'status' => FALSE,
			'message' => '資料錯誤, 請重新輸入!',
			'user' => NULL,
		);
		
		if ($phone && $password)
		{
			//建立用戶帳號 
			$set_account = 'TW'. substr($phone, 1); //$this->setAccount();
			//$_Choices = $this->member_model->getChoicesRows( array('account' => $set_account ) );
			$user_account = $set_account; //empty($_Choices) ? $set_account : $this->setAccount();
			$_SESSION['join_account'] = $user_account;
			
			//推薦者 $get_parent = $this->member_model->getMemberInfo($_SESSION['site_id']);
			
			//寫入會員主檔
			$field['parent_uid'] = 1; //$get_parent['id']; 
			$field['user_group_id'] = 14; //$get_parent['user_group_id'];
			$field['company_id'] = 0; //$get_parent['company_id'];
			$field['account'] = $user_account;
			$field['password'] = $this->myaes->mcrypt_en($password);
			$field['name'] = $phone;
			$field['email'] = $vars['email']; 
			$field['telephone'] = $phone;  
			$field['enable'] = 2; 
			$saved_id = $this->member_model->insert($field); echo $this->member_model->get_query();
			
			//寫入帳戶資訊
			$mi_field['member_id'] = $saved_id;
			$mi_field['info'] = $user_account;
			$this->member_info_model->insert($mi_field);
			
			$result['status'] = TRUE;
			$result['message'] = '用戶帳號巳建立';
		}

		return $result;
	}
	public function chk_sign( $pass=null )
	{
		$password = isset($pass) ? $pass : NULL;
		
		/* 帳號、密碼 */
		$result = array(
			'status' => FALSE,
			'message' => '資料錯誤, 請重新輸入!',
			'user' => NULL,
		);
		
		if ($password)
		{
			//建立數位簽章
			$key = substr($password, 0, -1);
			$pin = substr($key, -1, 1);
			
			$get_arr = explode(",", $key);
			$c = count($get_arr);
			if($c<3 || $c>12)
			{
				$result['message'] = '簽章錯誤, 應介於 3~12 位!';
			}
			else
			{
				//用戶帳號 
				$get_account = $this->member_model->getMemberInfo($_SESSION['join_account']);
				
				//簽章編碼
				$sign_code = $this->myaes->sign_parser($key, $pin);
				
				//寫入會員主檔 
				$field['password'] = $sign_code;
				$field['pin'] = $pin;
				$this->member_model->update_password(array('id' => $get_account['id']), $field);
				
				$result['status'] = TRUE;
				$result['message'] = '數位簽章巳建立';
			}
		}

		return $result;
	}
	
	
	///// 用戶登入 ////////////////////
	
	public function chk_admin_login( $vars=array() )
	{
		$username = isset($vars['username']) ? $vars['username'] : NULL;
		$password = isset($vars['password']) ? $vars['password'] : NULL;
		$user = isset($vars['user']) ? $vars['user'] : NULL;
		
		/* 帳號、密碼 */
		$result = array(
			'status' => FALSE,
			'message' => '請確認帳號、密碼',
			'user' => NULL,
		);
		
		if ($username && $password)
		{
			if(intval($user['enable']) == 1 && intval($user['status']) == 0 )
			{
				//驗證密碼
				if($user['password'] !== $this->myaes->mcrypt_en($password))
				{
					$result['message'] = '密碼錯誤';
				}
				else
				{
					unset($user['password']);
					unset($user['token']);
					
					$ip = $this->input->ip_address();
					$update_var['token'] = null;
					$update_var['login_num'] = $user['login_num']+1;
					$update_var['last_login_at'] = date('Y-m-d H:i:s');
					$update_var['last_login_ip'] = $ip;
					
					$this->user_model->update($user['id'], $update_var);
					
					//cancel logs for login error
					$this->user_blacklist_model->cancelAccount($username);
					$this->user_blacklist_model->cancelIP($ip);
					
					$result['status'] = TRUE;
					$result['user'] = $user;
					$result['message'] = '確認登入';
				}
			}
			else
			{	
				$result['message'] = '帳戶已停用';
			}
		}

		return $result;
	}
	
	public function chk_login( $vars=array() )
	{
		$username = isset($vars['username']) ? $vars['username'] : NULL;
		$password = isset($vars['password']) ? $vars['password'] : NULL;
		$user = isset($vars['user']) ? $vars['user'] : NULL;
		
		/* 帳號、密碼 */
		$result = array(
			'status' => FALSE,
			'message' => '請確認帳號、密碼',
			'user' => NULL,
		);
		
		if ($username && $password)
		{
			if($user['enable'] !=='2')
			{
				$result['message'] = '帳戶已停用';
			}
			else
			{	
				//驗證密碼
				if($user['password'] !== $this->myaes->mcrypt_en($password))
				{
					$result['message'] = '密碼錯誤';
				}
				else
				{
					unset($user['password']);
					unset($user['token']);
					
					$ip = $this->input->ip_address();
					$update_var['token'] = null;
					$update_var['login_num'] = $user['login_num']+1;
					$update_var['last_login_at'] = date('Y-m-d H:i:s');
					$update_var['last_login_ip'] = $ip;
					
					$this->member_model->update($user['id'], $update_var);
					
					//cancel logs for login error
					$this->user_blacklist_model->cancelAccount($username);
					$this->user_blacklist_model->cancelIP($ip);
					
					$result['status'] = TRUE;
					$result['user'] = $user;
					$result['message'] = '確認登入';
				}
			}
		}

		return $result;
	}
	
	public function login_tel_private( $vars=array(), $member=array() )
	{
		$phone = isset($vars['phone']) ? $vars['phone'] : NULL;
		$private_key = isset($vars['private_key']) ? $vars['private_key'] : NULL;
		
		$result = array(
			'status' => FALSE,
			'message' => '請確認手機、私鑰',
		);
		
		if ($phone && $private_key)
		{
			//數位簽章 驗證編碼
			$crypt_en = $this->myaes->mcrypt_en($private_key);
			
			if($phone !== $member['telephone'])
			{
				$result['message'] = '用戶手機錯誤';
			}
			elseif($crypt_en !== $member['private_key'])
			{
				$result['message'] = '用戶私鑰錯誤';
			}
			else
			{
				$result['status'] = TRUE;
				$result['message'] = '帳戶巳確認';
			}
		}

		return $result;
	}
	
	public function login_private( $vars=array() )
	{
		$username = isset($vars['username']) ? $vars['username'] : NULL;
		$private_key = isset($vars['private_key']) ? $vars['private_key'] : NULL;
		
		/* 帳號、密碼 */
		$result = array(
			'status' => FALSE,
			'message' => '請確認用戶碼、私鑰',
			'user' => NULL,
		);
		
		if ($username && $private_key)
		{
			$user_name = substr($username, 1);
			$user_name = substr($user_name, 0, -1);
			$user = $this->user_model->getUserByAccount($user_name); //echo $this->user_model->get_query();
			
			if($user['enable'] !=='2')
			{
				$result['message'] = '帳戶已停用';
			}
			else
			{	
				//動態數位簽章
		        $this->myaes->public_key = $this->myaes->_Static_key;
		        $this->myaes->set_hash_config();
		        
		        //驗證編碼
				$crypt_en = $this->myaes->mcrypt_en($private_key);
				
				if($crypt_en !== $user['private_key'] )
				{
					$result['message'] = '用戶私鑰錯誤';
				}
				else
				{
					unset($user['password']);
					unset($user['private_key']);
					unset($user['token']);
					
					$update_var['token'] = null;
					$update_var['login_num'] = $user['login_num']+1;
					$update_var['last_login_at'] = date('Y-m-d H:i:s');
					$update_var['last_login_ip'] = $this->input->ip_address();
					parent::update($user['id'], $update_var);
					echo parent::get_query();
					
					$result['status'] = TRUE;
					$result['user'] = $user;
					$result['message'] = '帳戶巳確認';
				}
			}
		}

		return $result;
	}
	
	public function chk_private( $vars=array() )
	{
		$receiver_id = isset($vars['orders']['receiver_id']) ? $vars['orders']['receiver_id'] : NULL;
		$private_key = isset($vars['orders']['private_key']) ? $vars['orders']['private_key'] : NULL;
		
		/* 帳號、密碼 */
		$result = array(
			'status' => FALSE,
			'message' => '請確認用戶碼、私鑰',
		);
		
		if ($receiver_id && $private_key)
		{
			$user_name = substr($receiver_id, 1);
			$user_name = substr($user_name, 0, -1);
			$user = $this->user_model->getUserByAccount($user_name); //echo $this->user_model->get_query();
			
			if($user['enable'] !=='2')
			{
				$result['message'] = '帳戶已停用';
			}
			else
			{	
				//驗證編碼
				$crypt_en = $this->myaes->mcrypt_en($vars['orders']['private_key']);

				if($crypt_en !== $user['private_key'] )
				{
					$result['message'] = '用戶私鑰錯誤';
				}
				else
				{
					$result['status'] = TRUE;
					$result['message'] = '受方用戶巳確認';
				}
			}
		}

		return $result;
	}
	
	public function setAdminToken($id)
	{
		$rand = uniqid(rand(), TRUE);
		$_hash = hash('sha1', md5($rand) . microtime(TRUE));
		$token = $this->myaes->token_hash() . $_hash;

		$this->user_model->update($id, array('token'=>$token));

		return $token;
	}
	
	public function setToken($id)
	{
		$rand = uniqid(rand(), TRUE);
		$_hash = hash('sha1', md5($rand) . microtime(TRUE));
		$token = $this->myaes->token_hash() . $_hash;

		$this->member_model->update($id, array('token'=>$token));

		return $token;
	}
	
	//會員帳號
	public function setAccount($area='TW')
	{
		$m_array = array(1=>'A',2=>'B',3=>'C',4=>'D',5=>'E',6=>'F',7=>'G',8=>'H',9=>'J',10=>'K',11=>'L',12=>'M');
		
		$yy = date('y');
		$mm = (int)date('m');
		$_uniqid = strval( microtime(TRUE) + (int)rand(1, 100000) );
		
		//$data = $area . $yy . $m_array[$mm] . substr($_uniqid, 8) . random_int(1,9);
		$data = $yy . $m_array[$mm] . substr($_uniqid, 8) . random_int(1,9);
		$data = str_replace(".", '', $data);

		return $data;
	}

}

