<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_model extends MY_Model
{
	public $table = 'user';
	public $pk = 'id';
	public $key = 'ac181c517bdf24ce053556bb280a2dcb';  // 加密使用key

	public function __construct()
	{
		parent::__construct();

		$this->init($this->table, $this->pk);

	}

	//表單欄位預設
	public function getFormDefault($user=array())
	{
		$data = array_merge(array(
			'user_group_id' => 1,
			'name' => '',
			'username' => '',
			'pssword' => '',
			'passconf' => '',
			'email' => '',
			'telephone' => '',
			'enable' => 0,
		), $user);

		return $data;
	}

	//表單欄位驗證
	public function getVerifyConfig()
	{
		$config = array(
			'user_group_id' => array(
				'field' => 'user_group_id',
				'label' => '群組',
				'rules' => 'required',
			),
			'name' => array(
				'field' => 'name',
				'label' => '名稱',
				'rules' => 'trim|required',
			),
			'username' => array(
				'field' => 'username',
				'label' => '帳號',
				'rules' => 'trim|required|min_length[4]|max_length[20]|is_unique[user.username]',
			),
			'password' => array(
				'field' => 'password',
				'label' => '密碼',
				'rules' => 'required|min_length[4]|max_length[20]',
			),
			'passconf' => array(
				'field' => 'passconf',
				'label' => '確認密碼',
				'rules' => 'required|matches[password]',
			),
			'email' => array(
				'field' => 'email',
				'label' => 'E-Mail',
				'rules' => 'trim|valid_email|is_unique[user.email]',
			),
			'telephone' => array(
				'field' => 'telephone',
				'label' => 'Telephone',
				'rules' => 'trim|integer',
			),
			'enable' => array(
				'field' => 'enable',
				'label' => '啟用',
				'rules' => 'required|in_list[0,1]',
			),
		);

		return $config;
	}

	/*
	public function getData($params=array(), $type='array', $get_query=NULL)
	{
		$default = array(
			'select' => '*',
			'order_by' => 'create_at desc',
		);
		if (isset($params['conditions']) && is_array($params['conditions'])) {
			foreach ($params['conditions'] as $k => $v) {
				switch ($k) {
					case 'user_group_id':
						$default['conditions']["FIND_IN_SET({$v}, {$this->table}.{$k}) !="] = 0;
						break;

					default:
						$default['conditions']["{$this->table}.{$k}"] = $v;
						break;
				}
			}
		}

		if (isset($params['q']) && $params['q'] != '') {
			$default['or_like'] = array(
				'many' => TRUE,
				'data' => array(
					array('field' => "{$this->table}.name", 'value'=>$params['q'], 'position'=>'both'),
					array('field' => "{$this->table}.email", 'value'=>$params['q'], 'position'=>'both'),
					array('field' => "{$this->table}.username", 'value'=>$params['q'], 'position'=>'both'),
				),
			);
		}

		if (isset($params['count']) && $params['count'] == TRUE) {
			// return parent::getDataCount($default);
			$default['count'] = TRUE;
			return parent::getData($default);
		}

		if (isset($params['rows']) && $params['rows'] != '') $default['rows'] = $params['rows'];
		if (isset($params['offset']) && $params['offset'] != '') $default['offset'] = $params['offset'];
		if (isset($params['order_by']) && $params['order_by'] != '') $default['order_by'] = $params['order_by'];

		$data = parent::getData($default);

		return $data;
	}

	public function getDataCount($params=array())
	{
		$params['count'] = TRUE;
		return $this->getData($params);
	}*/
	
	public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }

		$data = $this->getData($params,'array');
		
		return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );
		
		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }
		
        $data = $this->getList($params);
        return count($data);
    }
	
	public function GroupName($groups=array(), $user_group_id=1 )
	{
		$user_group_array = explode(",", $user_group_id);
		
		foreach ($user_group_array as $gid){
			$_group_name[$gid] = $groups[$gid];
		}
		
		$_user_group = implode(",", $_group_name);
		
		return $_user_group;
	}

	public function getChoices($conditions=array())
	{
		$data = array();
		$users = $this->getList();
		foreach ($users as $user) {
			$data[$user['id']] = $user['name'];
		}

		return $data;
	}
	
	public function getUserName($kind='tlback')
	{
		$data = array('null'=>'---');
		$conditions = array(
			'kind' => $kind,
			'enable'=>1,
			'is_admin'=>0
		);
		$params = array(
			'select' => 'username, name',
			'order_by' => 'username',
			'conditions' => $conditions,
		);
		
		$users = $this->getData($params,'array');
		foreach ($users as $user) {
			$data[$user['username']] = $user['name'];
		}

		return $data;
	}

	public function getUserByAccount($username)
	{
		$conditions = array(
			'username' => $username,
		);

		return $this->get($conditions);
	}

	public function getUserByEmail($email)
	{
		$conditions = array(
			'email' => $email,
		);

		return $this->get($conditions);
	}

	public function insert($fields=array(), $now_field=NULL)
	{
		if ($fields['password'] != '' && $fields['passconf'] != '' ) {
			$fields['password'] = $this->encrypt($fields['password']);
		} else {
			unset($fields['password']);
		}
		unset($fields['passconf']);

		return parent::insert($fields, 'create_at');
	}

	public function update($conditions=NULL, $fields=array())
	{
		if (isset($fields['password']) && $fields['password'] != '') {
			$fields['password'] = $this->encrypt($fields['password']);
		} else {
			unset($fields['password']);
		}

		if (isset($fields['passconf'])) {
			unset($fields['passconf']);
		}

		return parent::update($conditions, $fields);
	}

	public function login($username=NULL, $password=NULL)
	{
		$result = array(
			'status' => FALSE,
			'message' => '請確認帳號、密碼',
			'user' => NULL,
		);
		if ($username && $password)
		{
			$conditions = array(
				'username' => $username,
				'password' => $this->encrypt($password),
			);
			$user = $this->get($conditions);
			if ($user)
			{
				if ($user['enable'] == 1) {
					$user['login_num'] = $user['login_num']+1;
					$user['last_login_at'] = date('Y-m-d H:i:s');
					$user['last_login_ip'] = $this->input->ip_address();
					parent::update($user['id'], $user);
					$result['status'] = TRUE;
					$result['user'] = $user;
					$result['message'] = '帳號巳確認';
				} else {
					$result['message'] = '帳號已停用';
				}
			}
		}

		return $result;
	}

	public function setToken($id)
	{
		$rand = uniqid(rand(), TRUE);
		$token = hash('sha1', md5($rand) . microtime(TRUE));
		$this->update($id, array('token'=>$token));

		return $token;
	}

	// 加密函數
	public function encrypt($str)
	{
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return base64_encode(trim(mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $this->key, $str, MCRYPT_MODE_ECB, $iv)));
	}

	// 解密函數
	public function decrypt($str)
	{
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $this->key, base64_decode($str), MCRYPT_MODE_ECB, $iv));
	}


}

