<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_goods_model extends MY_Model
{
    public $table = 'category_goods';
    public $pk = NULL;

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getCategoryIds($goods_id)
    {
        $data = array();
        $params = array(
            'select' => 'category_id',
            'conditions' => array('goods_id'=>$goods_id),
        );
        $queryset = $this->getData($params,'array');
        
		foreach ($queryset as $row) {
            $data[] = $row['category_id'];
        }
        unset($queryset);

        return $data;
    }

}


