<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Category_model extends MY_Model
{
    public $table = 'category';
    public $pk = 'id';
    public $level_max = 3;
    public $kind = array('goods' => '廠商商品分類'
		, 'activity_goods' => '展覽商品分類'
		/*
		, 'news' => '最新消息分類'
		, 'album' => '相簿分類'
		, 'download' => '下載分類'
		*/
    );

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'kind' => '',
            'parent_id' => 0,
            'name' => '',
            'description' => '',
            'image' => '',
            'enable' => 0,
            'sort_order' => 0,
			'exhibit_id' => 0,
        );

        return array_merge($fields, $data);
    }

    public function getVerifyConfig()
    {
        $config = array(
            'kind' => array(
                'field' => 'kind',
                'label' => '功能類別',
                'rules' => 'required',
            ),
            'level' => array(
                'field' => 'level',
                'label' => '等級',
                // 'rules' => 'required|integer'
            ),
            'parent_id' => array(
                'field' => 'parent_id',
                'label' => '上層分類',
                'rules' => 'required|integer'
            ),
            'name' => array(
                'field' => 'name',
                'label' => '類別名稱',
                'rules' => 'required',
            ),
            'description' => array(
                'field' => 'description',
                'label' => '類別描述',
                'rules' => '',
            ),
            'image' => array(
                'field' => 'image',
                'label' => 'Image',
                // 'rules' => ''
            ),
            'sort_order' => array(
                'field' => 'sort_order',
                'label' => '排序',
                'rules' => 'required|integer',
                'errors' => array(
                    'required'=>'%s 請勿空白',
                    'integer'=>'%s 只能輸入數字',
                ),
            ),
            'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|in_list[0,1]',
            ),
        );

        return $config;
    }

    public function getListByKind($condition=array())
    {
        $data = array();
		$level = $condition['level'];
		
		$conditions = array('status' => 0
			, 'kind' => $condition['kind']
			, 'level' => 1
		);
		
		if($condition['exhibit_id'] !== null){
			$conditions['exhibit_id'] = $condition['exhibit_id'];
		}
		
        $params = array(
            'conditions' => $conditions,
			'order_by' => 'exhibit_id desc, parent_id asc, sort_order asc',
        );
		
        $m1 = $this->getData($params,'array');
        
		foreach ($m1 as $m1_row)
		{
            $data[$m1_row['id']] = $m1_row;
            $data[$m1_row['id']]['full_name'] = "{$m1_row['name']}";

            if ($level >= 2)
			{
                $params['conditions']['level'] = 2;
                $params['conditions']['parent_id'] = $m1_row['id'];
                $m2 = $this->getData($params);
                
				foreach ($m2 as $m2_row)
				{
                    $data[$m2_row['id']] = $m2_row;
                    $data[$m2_row['id']]['full_name'] = "{$m1_row['name']} > {$m2_row['name']}";

                    if ($level >= 3)
					{
                        $params['conditions']['level'] = 3;
                        $params['conditions']['parent_id'] = $m2_row['id'];
                        $m3 = $this->getData($params);
                        
						foreach ($m3 as $m3_row) {
                            $data[$m3_row['id']] = $m3_row;
                            $data[$m3_row['id']]['full_name'] = "{$m1_row['name']} > {$m2_row['name']} > {$m3_row['name']}";
                        }
                    }
                }
            }
        }

        return $data;
    }

    public function getChoicesByKind($condition=array())
    {
		$choices = array();
		$level = $condition['level'];
		
		$conditions = array('status' => 0);
		
		if($condition)
		foreach ($condition as $k => $v){
			$conditions[$k] = $v;
		}
		
        $params = array(
            'conditions' => $conditions,
            'order_by' => 'parent_id asc, sort_order asc',
        );
        $m1 = $this->getData($params);
        
		foreach ($m1 as $m1_row)
		{
            $choices[$m1_row['id']] = "{$m1_row['name']}";

            if ($level >= 2)
			{
                $params['conditions']['level'] = 2;
                $params['conditions']['parent_id'] = $m1_row['id'];
                $m2 = $this->getData($params);
                
				foreach ($m2 as $m2_row)
				{
                    $choices[$m2_row['id']] = "{$m1_row['name']} > {$m2_row['name']}";

                    if ($level >= 3)
					{
                        $params['conditions']['level'] = 3;
                        $params['conditions']['parent_id'] = $m2_row['id'];
                        $m3 = $this->getData($params);
                        
						foreach ($m3 as $m3_row) {
                            $choices[$m3_row['id']] = "{$m1_row['name']} > {$m2_row['name']} > {$m3_row['name']}";
                        }
                    }
                }
            }
        }
		
        return $choices;
    }

    public function getParentChoices($condition=array())
    {
		$data = array();
		$level = $condition['level'];
		
		$conditions = array('status' => 0
			, 'kind' => $condition['kind']
			, 'exhibit_id' => $condition['exhibit_id']
            , 'level' => 1
		);
		
        $params = array(
            'conditions' => $conditions,
            'order_by' => 'parent_id asc, sort_order asc',
        );
        $m1 = $this->getData($params);
        
		foreach ($m1 as $m1_row)
		{
            $data[$m1_row['id']] = "{$m1_row['name']}";

            if ($level >= 2)
			{
                $params['conditions']['level'] = 2;
                $params['conditions']['parent_id'] = $m1_row['id'];
                $m2 = $this->getData($params);
                
				foreach ($m2 as $m2_row)
				{
                    $data[$m2_row['id']] = "{$m1_row['name']} > {$m2_row['name']}";

                    if ($level >= 3)
					{
                        $params['conditions']['level'] = 3;
                        $params['conditions']['parent_id'] = $m2_row['id'];
                        $m3 = $this->getData($params);
                        
						foreach ($m3 as $m3_row) {
                            $data[$m2_row['id']] = "{$m1_row['name']} > {$m2_row['name']} > {$m3_row['name']}";
                        }
                    }
                }
            }
        }

        return $data;
    }

    public function getList($params=array(), $count=NULL)
    {
        $default = array(
            'select' => '*',
            'conditions' => array(),
            'order_by' => 'sort_order asc, create_at desc',
        );

        if (isset($params['select'])) {
            $default['select'] = $params['select'];
        }
		
        if (isset($params['conditions']) && is_array($params['conditions'])) {
            foreach ($params['conditions'] as $k => $v) {
                switch ($k) {
                    default:
                        $default['conditions']["{$this->table}.{$k}"] = $v;
                        break;
                }
            }
        }
		
        if (isset($params['q']) && $params['q'] != '') {
            $default['or_like'] = array(
                'many' => TRUE,
                'data' => array(
                    array('field'=>'title', 'value'=>$params['q'], 'position'=>'both'),
                    array('field'=>'slogan', 'value'=>$params['q'], 'position'=>'both'),
                ),
            );
        }

        if (isset($params['count']) && $params['count'] == TRUE) {
            $default['count'] = TRUE;
            return $this->getData($default);
        }

        if (isset($params['rows']) && $params['rows'] != '') $default['rows'] = $params['rows'];
        if (isset($params['offset']) && $params['offset'] != '') $default['offset'] = $params['offset'];
        if (isset($params['order_by']) && $params['order_by'] != '') $default['order_by'] = $params['order_by'];

        $data = $this->getData($default);

        return $data;
    }

    public function getListCount($params=array())
    {
        $params['count'] = TRUE;
        return $this->getList($params);
    }

    public function getLevel($parent_id)
    {
        if ($parent_id == 0) {
            $level = 1;
        } else {
            $parent = parent::get($parent_id);
            $level = $parent['level'] + 1;
        }

        return $level;
    }
	
	public function getRowById($id_arr=null)
    {
		$params = array(
			'where_in'=> array('field'=>'id', 'value'=>$id_arr)
		);
		$data = $this->getData($params);

        return $data;
    }
	
	public function getRowByKind($condition=null)
    {
		$params = array(
			'conditions' => $condition,
		);
		$data = $this->getData($params);

        return $data;
    }
	
	public function getRowByCategory($condition=null)
    {
		$params = array(
			'conditions' => $condition,
		);
		$data = $this->getData($params); //echo $this->get_query();

        return $data;
    }

}

