<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goods_category_model extends MY_Model
{
    public $table = 'goods_category';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'status' => 0,
			'company_id' => 0,
            'parents_category' => 0,
			'clicks' => 0,
			'sort' => 0,
			'qty' => 0,
			'user_id' => 0,
			'category_title' => '',
            'category_description' => '',
			'category_keyword' => '',
            'category_photo' => '',
        ); 

        return array_merge($fields, $data);
    }
	
    public function getVerifyConfig()
    {
        $config = array(
            'category_title' => array(
                'field' => 'category_title',
                'label' => '分類標題',
                'rules' => 'required',
            ),
            'category_description' => array(
                'field' => 'category_description',
                'label' => '分類描述',
                'rules' => '',
            ),
            'category_photo' => array(
                'field' => 'category_photo',
                'label' => '分類代表圖',
                'rules' => ''
            ),
            /*
			'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|in_list[0,1]',
            ),
			*/
        );

        return $config;
    }

    public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }

		$data = $this->getData($params);
		
		return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );
		
		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }
		
        $data = $this->getList($params);
        return count($data);
    }
	
	public function getRowById($id)
    {
        $conditions = array(
            'id' => $id,
        );

        return $this->get($conditions,'array');
    }

}

