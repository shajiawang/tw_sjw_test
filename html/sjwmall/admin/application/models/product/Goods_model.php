<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goods_model extends MY_Model
{
    public $table = 'goods';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);

        //$this->load->model('catalog/category_goods_model');
    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'category_id' => array(),
            'subimage' => array(),
            'no' => '',
            'name' => '',
            'description' => '',
            'price' => 0,
            'sale_price' => 0,
            'sale_start_date' => '',
            'sale_end_date' => '',
            'image' => '',
            'sort_order' => 0,
			'recommend' => 0,
            'enable' => 0,
			'goods_id' => 0,
        );

        return array_merge($fields, $data);
    }

    public function getVerifyConfig()
    {
        $config = array(
            /*
			'category_id' => array(
                'field' => 'category_id[]',
                'label' => '類別',
                'rules' => 'required',
            ),*/
			
			/*|is_unique[goods.no]*/
			'no' => array(
                'field' => 'no',
                'label' => '商品編號',
                'rules' => 'trim|required',
            ),
            'name' => array(
                'field' => 'name',
                'label' => '商品名稱',
                'rules' => 'required',
            ),
            'description' => array(
                'field' => 'description',
                'label' => '商品介紹',
                'rules' => 'required',
            ),
            'image' => array(
                'field' => 'image',
                'label' => '圖片',
                'rules' => 'required',
            ),
            'price' => array(
                'field' => 'price',
                'label' => '價格',
                'rules' => 'integer|min[1]',
            ),
            'sale_price' => array(
                'field' => 'sort_order',
                'label' => '優惠價',
                'rules' => 'integer|min[1]',
            ),
            'sale_start_date' => array(
                'field' => 'sale_start_date',
                'label' => '優惠價自動上架時間',
                'rules' => 'valid_datetime',
            ),
            'sale_end_date' => array(
                'field' => 'sale_end_date',
                'label' => '優惠價自動下架時間',
                'rules' => 'valid_datetime',
            ),
            'sort_order' => array(
                'field' => 'sort_order',
                'label' => '排序',
                'rules' => 'required|integer'
            ),
			'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|in_list[0,1]',
            ),
            /*
			'recommend' => array(
                'field' => 'recommend',
                'label' => '推薦商品',
                'rules' => 'required|in_list[0,1]',
            ),*/
            
        );

        return $config;
    }

    public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }

        //$data = $this->getData($params,'array',true);
		$data = $this->getData($params,'array');
		
		return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );
		
		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }
		
        $data = $this->getList($params);
        return count($data);
    }
	
	public function getRowById($id)
    {
        $conditions = array(
            'id' => $id,
        );

        return $this->get($conditions,'array');
    }
	
}

