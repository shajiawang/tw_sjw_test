<?php

class LIb_acl {

	var $permissions = array(); //Array : Stores the permissions for the user
	var $user_id = null; //Integer : Stores the id of the current user
	var $user_roles = array(); //Array : Stores the roles of the current user
	var $permission_data = array();
	var $CI;

	function __construct() {
		$this->CI = &get_instance();

		$this->user_id = empty($_SESSION['userdata']['user']->id) ? null : $_SESSION['userdata']['user']->id;
		$this->user_roles = $this->getUserRoles();
		//jd($this->user_roles);
		$this->buildACL();
	}

	function getUserRoles() { // get user's roles and user's group's roles
		if (empty($_SESSION['userdata']) || (empty($_SESSION['userdata']['user']) && empty($_SESSION['userdata']['member']) && empty($_SESSION['userdata']['company']))) {
			return array(ACL_ROLE_GUEST); // default role
		}

		$role_ids = array();
		if (!empty($this->user_id)) {
			$role_ids = $this->get_all_role_ids($this->user_id);
		}
		if (!empty($_SESSION['userdata']['member'])) {
			$role_ids[] = ACL_ROLE_MEMBER;
		}
		if (is_company()) {
			if(is_passed_company()){
				$role_ids[] = ACL_ROLE_PASSED_COMPANY;
			}
			else{
				$role_ids[] = ACL_ROLE_COMPANY;
			}
		}
		return $role_ids;
	}

	function buildACL() {
		//first, get the rules for the user's role
		if (count($this->user_roles) > 0) {
			$this->permissions = $this->getRolePermissions($this->user_roles);
		}
		//then, get the individual user permissions
		$this->permissions = array_merge($this->permissions, $this->getUserPermissions($this->user_id));
	}

	function getRolePermissions($role = 0) {
		if (empty($role)) {
			return array();
		}
		$this->CI->load->model('acl_role_permission_model');
		$role_permissions = $this->CI->acl_role_permission_model->get_role_permissions($role);
		//jd($role_permissions);
		$permissions = array();
		foreach ($role_permissions as $rp) {
			$pK = strtolower($this->getPermKeyFromId($rp->permission_id));
			if ($pK == '') {
				continue;
			}
			if ($rp->value === '1') {
				$hP = true;
			} else {
				$hP = false;
			}
			$permissions[$pK] = array('key' => $pK, 'value' => $hP, 'name' => $this->getPermNameFromId($rp->permission_id), 'id' => $rp->permission_id);
		}
//jd($permissions);
		return $permissions;
	}

	function getUserPermissions($user_id = null) {
		if (empty($user_id)) {
			return array();
		}
		$this->CI->load->model('acl_user_permission_model');
		$user_permissions = $this->CI->acl_user_permission_model->get_user_permissions($user_id);
		$permissions = array();
		foreach ($user_permissions as $up) {
			$pK = strtolower($this->getPermKeyFromId($up->permission_id));
			if ($pK == '') {
				continue;
			}
			if ($up->value == '1') {
				$hP = true;
			} else {
				$hP = false;
			}
			$permissions[$pK] = array('permission' => $pK, 'inheritted' => false, 'value' => $hP, 'name' => $this->getPermNameFromId($up->permission_id), 'id' => $up->permission_id);
		}
		return $permissions;
	}

	function getPermKeyFromId($permission_id = null) {
		if (!isset($this->permission_data[$permission_id])) {
			$this->CI->load->model('acl_permission_model');
			$permission = $this->CI->acl_permission_model->id($permission_id);
			if (empty($permission->permission_name) || empty($permission->permission_key)) {
				return false;
			} else {
				$this->permission_data[$permission_id] = array('name' => $permission->permission_name, 'key' => $permission->permission_key);
			}
		}
		return $this->permission_data[$permission_id]['key'];
	}

	function getPermNameFromId($permission_id = null) {
		if (!isset($this->permission_data[$permission_id])) {
			$this->CI->load->model('acl_permission_model');
			$permission = $this->CI->acl_permission_model->id($permission_id);
			if (empty($permission->permission_name) || empty($permission->permission_key)) {
				return false;
			} else {
				$this->permission_data[$permission_id] = array('name' => $permission->permission_name, 'key' => $permission->permission_key);
			}
		}
		return $this->permission_data[$permission_id]['name'];
	}

	function hasPermission($permission_key) {
		//jd($this->permissions);
		if (in_array(ACL_ROLE_SUPERVISOR, $this->user_roles)) {
			return true;
		}
		$permission_key = strtolower($permission_key);
		//jd($permission_key);
		//jd($this->permissions);
		if (array_key_exists($permission_key, $this->permissions)) {
			if ($this->permissions[$permission_key]['value'] === '1' || $this->permissions[$permission_key]['value'] === true) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	function get_all_roles_permission_ids($id = null) {
		if (empty($id)) {
			return false;
		}
		if (isset($_SESSION['userdata']['user']) && $_SESSION['userdata']['user']->id == $id) {
			$user = $_SESSION['userdata']['user'];
		} else {
			$this->CI->load->model('user_model');
			$user = $this->CI->user_model->id($id);
		}
		if (empty($user)) {
			return false;
		}

		$CI = &get_instance();

		$role_ids = array();

		// get user groups
		$this->CI->load->model('user_group_model');
		$qpam = array(
			'conditions' => array('user_id' => $id)
		);
		$user_groups = $this->CI->user_group_model->find($qpam);
		if (!empty($user_groups)) {
			$group_roles = array();
			$this->CI->load->model('acl_group_role_model');
			foreach ($user_groups as $v) {
				$group_roles = array_merge($group_roles, $this->CI->acl_group_role_model->get_roles($v->group_id));
			}
			if (!empty($group_roles))
				;
			foreach ($group_roles as $v) {
				$role_ids[] = $v->role_id;
			}
		}
		//jd($group_roles);
		if(in_array(ACL_ROLE_SUPERVISOR, $role_ids)){
			return $this->all_permission_ids();
		}

		// get user_roles;
		$this->CI->load->model('acl_user_role_model');
		$user_roles = $this->CI->acl_user_role_model->get_roles($id);
		//jd($user_roles);
		if (!empty($user_roles)) {
			foreach ($user_roles as $v) {
				$role_ids[] = $v->role_id;
			}
		}
		$role_ids = array_unique($role_ids);
		if(in_array(ACL_ROLE_SUPERVISOR, $role_ids)){
			return $this->all_permission_ids();
		}

		// get role permissions
		$permission_ids = array();
		if (!empty($role_ids)) {
			$this->CI->load->model('acl_role_permission_model');
			$qpam = array(
				'conditions' => array(
					'role_id in (' . join(',', $role_ids) . ')' => null
					, 'value' => 1
				)
				, 'fields' => 'permission_id'
			);
			$this->CI->acl_role_permission_model->distinct();
			$permissions = $this->CI->acl_role_permission_model->find($qpam);
			foreach ($permissions as $v) {
				$permission_ids[] = $v->permission_id;
			}
		}
		return $permission_ids;
	}

	function get_all_role_ids($id = null) {
		if (empty($id)) {
			return false;
		}
		if (isset($_SESSION['userdata']['user']) && $_SESSION['userdata']['user']->id == $id) {
			$user = $_SESSION['userdata']['user'];
		} else {
			$this->CI->load->model('user_model');
			$user = $this->CI->user_model->id($id);
		}
		if (empty($user)) {
			return false;
		}

		$role_ids = array();

		// get user groups
		$this->CI->load->model('user_group_model');
		$qpam = array(
			'conditions' => array('user_id' => $id)
		);
		$user_groups = $this->CI->user_group_model->find($qpam);
		//jd($user_groups);
		if (!empty($user_groups)) {
			$group_roles = array();
			$this->CI->load->model('acl_group_role_model');
			foreach ($user_groups as $v) {
				$group_roles = array_merge($group_roles, $this->CI->acl_group_role_model->get_roles($v->group_id));
			}
			if (!empty($group_roles))
				;
			foreach ($group_roles as $v) {
				$role_ids[] = $v->role_id;
			}
		}

		// get user_roles;
		$this->CI->load->model('acl_user_role_model');
		$user_roles = $this->CI->acl_user_role_model->get_roles($id);
		//jd($user_roles);
		if (!empty($user_roles)) {
			foreach ($user_roles as $v) {
				$role_ids[] = $v->role_id;
			}
		}
		$role_ids = array_unique($role_ids);
		return $role_ids;
	}

	function all_permission_ids(){
		$this->CI->load->model('acl_permission_model');
		$permissions = $this->CI->acl_permission_model->find();
		$permission_ids = array();
		foreach($permissions as $v){
			$permission_ids[] = $v->id;
		}
		return $permission_ids;
	}

}
