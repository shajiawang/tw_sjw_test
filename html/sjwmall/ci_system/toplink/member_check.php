<?php
class member_check
{
	public $CI;
	public $model;
	public $response;
	public $user;
	public $city_array;
	protected $sess_id;

	public function __construct()
	{
		$this->CI = &get_instance();

		$this->response = new stdClass();
		$this->response->error = null;
		$this->response->back = null;

		$this->model = new stdClass();
		$this->model->member = '`alpha_user`.`member`';
		$this->model->activity_sign = '`liucms`.`lc_activity_sign`';

		$this->city_array = array("台北", "基隆", "新北", "宜蘭", "新竹", "桃園", "苗栗", "台中", "彰化", "南投", "嘉義", "雲林", "台南", "高雄", "澎湖", "屏東", "台東", "花蓮", "金門", "連江");
	}

	/////////////////////////////////////////////////
	//已註冊會員登入驗證
	public function exec()
	{
		if($this->user['join_user'] !=='1' || empty($this->user['email']) ){
			return false;
		}

		//檢查帳密
		$get_email = strtolower(trim($this->user['email']));
		$get_password = $this->user['password'];

		//查驗 e-mail
		$this->CI->load->helper('email');
		if(! valid_email($get_email) ){
			$this->response->error = array(
					'code' => 401
					, 'message' => '無效的E-mail: '. $get_email.' !'
			);
			return $this->response; //json_encode($this->response);
		}

		//查驗登入者
		$sql = "SELECT * FROM {$this->model->member} WHERE `email`='{$get_email}' AND `status`=1 ";
		$members = $this->get_query($sql);
		if(empty($members)){
			$this->response->error = array(
					'code' => 402
					, 'message' => '無效的帳號: '. $get_email .' !'
			);
			return $this->response;
		}

		//撈取 會員資料
		$member = $members[0];

		//查驗密碼
		$decoded_pass = $this->password_decode($member->password, $get_email);
		if($get_password !== $decoded_pass){
			$this->response->error = array(
					'code' => 403
					, 'message' => '密碼錯誤!'
			);
			return $this->response;
		}

		$code = null;
		//會員資料不完整
		if($member->level=='0' || empty($member->name) || empty($member->cid) || empty($member->phone_number) ){
			$code = 404;
		}

		//回傳 會員資料
		$member_ret = array('code' => $code
				, 'mid' => $member->id
				, 'cemail' => $member->email
				, 'cname' => $member->name
				, 'cid' => $member->cid
				, 'ctel' => $member->phone_number
				, 'level' => $member->level
		);
		$this->response->back = $member_ret;

		return $this->response;
	}

	/////////////////////////////////////////////////
	//會員更新個資
	public function info_edit()
	{
		//查驗登入者
		$sql = "SELECT * FROM {$this->model->member} WHERE `id`='%d'; ";
		$sql = sprintf($sql, $this->user['mid']);
		$members = $this->get_query($sql);
		if(empty($members)){
			return false;
		}

		//更新 `alpha_user`.`member`
		$sql = "UPDATE {$this->model->member} SET `level`=1, `updated_at`=now() ";
		if(isset($this->user['cname']) && !empty($this->user['cname']) ){ $sql .= ",`name`='{$this->user['cname']}' "; }
		if(isset($this->user['cid']) && !empty($this->user['cid']) && $this->user['cid'] !='000000'){ $sql .= ", `cid`='{$this->user['cid']}' "; }
		if(isset($this->user['ctel']) && !empty($this->user['ctel']) && $this->user['ctel'] !='0000000000'){ $sql .= ", `phone_number`='{$this->user['ctel']}' "; }
		if(isset($this->user['location']) && !empty($this->user['location']) ){ $sql .= ", `location`='{$this->user['location']}' "; }
		if(isset($this->user['birthday']) && !empty($this->user['birthday']) && $this->user['birthday'] !=='0000-00-00'){ $sql .= ", `birthday`='{$this->user['birthday']}' "; }
		//if(isset($this->user['gender']) ){ $sql .= ", `gender`='{$this->user['gender']}' "; }
		$sql .= "WHERE `id`='%d'; ";
		$sql = sprintf($sql, $this->user['mid']);
		$this->set_query($sql);

		//更新 `liucms`.`lc_activity_sign`
		// $sql1 = "UPDATE {$this->model->activity_sign} SET `auto_update_at`='0000-00-00 00:00:00' ";
		// if(isset($this->user['cname']) && !empty($this->user['cname']) ){ $sql1 .= ",`cname`='{$this->user['cname']}' "; }
		// if(isset($this->user['cid']) && !empty($this->user['cid']) && $this->user['cid'] !='000000'){ $sql1 .= ", `cid`='{$this->user['cid']}' "; }
		// if(isset($this->user['ctel']) && !empty($this->user['ctel']) && $this->user['ctel'] !='0000000000'){ $sql1 .= ", `ctel`='{$this->user['ctel']}' "; }
		// $sql1 .= "WHERE `email`='%s'; ";
		// $sql2 = sprintf($sql1, $this->user['email']);
		// $this->set_query($sql2);

		return true;
	}

	/////////////////////////////////////////////////
	//新會員
	public function new_mem()
	{
		//email是否已存在
		$check_email = $this->user_chk_email($this->user['email']);
		if(isset($check_email->error) ){
			return $check_email;
		}

		//查驗手機
		$check_cell = $this->user_chk_cell($this->user['cell']);
		if(isset($check_cell->error) ){
			return $check_cell;
		}

		$encode_pass = $this->password_encode($this->user['password'], $this->user['email']);

		$sql = "INSERT INTO {$this->model->member} SET
				`email`='%s', `phone_number`='%s', `password`='%s', `epaper`=1, `level`=1, `created_at`=now(), `updated_at`=now();";
		$sql = sprintf($sql, $this->user['email'], $this->user['cell'], $encode_pass);
		$this->set_query($sql);

		$mid = $this->CI->db->insert_id();

		//查驗登入者
		$sql = "SELECT * FROM {$this->model->member} WHERE `id`='{$mid}' ";
		$members = $this->get_query($sql);

		//撈取 會員資料
		$member = $members[0];

		//回傳 會員資料
		$member_ret = array('code' => null
				, 'mid' => $mid
				, 'cemail' => $member->email
				, 'cname' => ''
				, 'cid' => ''
				, 'ctel' => $member->phone_number
				, 'level' => $member->level
		);
		$this->response->back = $member_ret;

		return $this->response;
	}

	//檢查 Email帳號
	protected function user_chk_email($email)
	{
		if(empty($email)){
			$this->response->error = array(
					'code' => 301
					, 'message' => '未輸入 E-mail !'
			);
			return $this->response;
		}

		$sql = "SELECT * FROM {$this->model->member} WHERE `email`='%s';";
		$sql = sprintf($sql, $email);
		$exist_email = $this->get_query($sql);
		if(!empty($exist_email)){
			$this->response->error = array(
					'code' => 302
					, 'message' => 'E-mail 已存在 !'
			);
			return $this->response;
		}

		return true;
	}

	//檢查手機號碼
	protected function user_chk_cell($cell, $email=null)
	{
		if(empty($cell)){
			$this->response->error = array(
					'code' => 303
					, 'message' => '未輸入 手機號碼'
			);
			return $this->response;
		}

		if($email){
			$conditions = array(
				'email != '.$email => null
			);
		}

		$sql = "SELECT * FROM {$this->model->member} WHERE `phone_number`='%s';";
		$sql = sprintf($sql, $cell);
		$exist_cell = $this->get_query($sql);
		if(!empty($exist_cell)){
			$this->response->error = array(
					'code' => 304
					, 'message' => '手機號碼 已存在'
			);
			return $this->response;
		}

		return true;
	}


	/////////////////////////////////////////////////
	// 公用 DB Function
	public function get_query($sql, $as_array = false)
	{
		if($sql){
			$query = $this->CI->db->query($sql);

			if ($query->num_rows() > 0){
				if($as_array){
					return $query->result_array();
				} else {
					return $query->result();
				}
			}
		} else {
			return array();
		}
	}
	public function set_query($sql)
	{
		if($sql){
			$query = $this->CI->db->query($sql);
		}
	}

	//回傳台灣縣市
	public function get_city()
	{
		return $this->city_array;
	}

	/////////////////////////////////////////////////
	// 私用 Function
	private function password_decode($password = null, $email = null)
	{
		$decode = base64_decode($password);
		$member_length = strlen($email);
		$decode_length = strlen($decode);
		$len = 0 - ($decode_length - $member_length);
		$paserdecode = substr($decode, $len);
		$plantext = explode('|', base64_decode($paserdecode));
		return $plantext[0];
	}
	private function password_encode($password, $member_email)
	{
		$member_length = strlen($member_email);
		$rand = $this->getrand($member_length);
		$enpassword = base64_encode($rand . base64_encode($password . '|' . $member_email));
		return $enpassword;
	}
	private function getrand($length)
	{
		global $template;
		settype($template, "string");
		$template = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		settype($length, "integer");
		settype($rndstring, "string");
		settype($a, "integer");
		settype($b, "integer");

		for ($a = 0; $a <= $length - 1; $a++) {
			$b = rand(0, strlen($template) - 1);
			$rndstring .= $template[$b];
		}

		return $rndstring;
	}

}
?>
