<?php
/*
 * 表單防止重複提交
 */
/* 範例如下:
	<?php    
	if ($_POST['submit'] == "go"){   
		//check token    
		if ($_POST['from_token'] == $_SESSION['from_token']){   
			//continue processing....    
		} else {    
			//stop all processing !    
		}    
	}    
	?>    
	<form action="" method="post">    
	<input type="hidden" name="from_token" value="<?php echo from_token();?>" >    
	<input type="submit" name="submit" value="go" >  
	</form>
*/
function from_token(){
	$token = md5(uniqid(rand(), true));    
	$_SESSION['from_token']= $token;
	echo $token;
}

// IP訪問限制
function ip_allow($denied_ip = array(), $denied_ip_area = array() ){
	if(getenv('HTTP_CLIENT_IP') && strcasecmp(getenv('HTTP_CLIENT_IP'), 'unknown')) {
		$userip = getenv('HTTP_CLIENT_IP');
	} elseif(getenv('HTTP_X_FORWARDED_FOR') && strcasecmp(getenv('HTTP_X_FORWARDED_FOR'), 'unknown')) {
		$userip = getenv('HTTP_X_FORWARDED_FOR');
	} elseif(getenv('REMOTE_ADDR') && strcasecmp(getenv('REMOTE_ADDR'), 'unknown')) {
		$userip = getenv('REMOTE_ADDR');
	} elseif(isset($_SERVER['REMOTE_ADDR']) && $_SERVER['REMOTE_ADDR'] && strcasecmp($_SERVER['REMOTE_ADDR'], 'unknown')) {
		$userip = $_SERVER['REMOTE_ADDR'];
	}
	
	// 限制 IP
	if($denied_ip){
		if(in_array($userip, $denied_ip) ){
			return false;
		} else {
			return true;
		}
	}
	
	// 限制IP段
	$ip_arr = explode('.', $userip);
	
	// 限制的IP段，假设是192.168.*.*
	if($denied_ip_area){
		if(!($ip_arr[0] == $denied_ip_area[0] && $ip_arr[1] == $denied_ip_area[1]) ){
			return false;
		} else {
			return true;
		}
	}
	
	return true;
}

/*
 * clean out any potential hexadecimal characters
 */
function cleanHex($input){    
	/*
	//strip_tags    
	$name = strip_tags($_POST['name']);    
	$name = substr($name,0,40);    
	$name = cleanHex($name);
	*/
	return preg_replace("![\][xX]([A-Fa-f0-9]{1,3})!", "", $input);    
}

//驗證台灣手機號碼
function tw_cell($str,$type='a'){
	if(empty($str)){
		return false;
	}
	
	if($type=='c'){
		// 09xx-xxx-xxx
		if (preg_match("/09[0-9]{2}-[0-9]{3}-[0-9]{3}/", $str)){
			return true;    
		}
	} else if($type=='b'){
		// 09xx-xxxxxx
		if(preg_match("/09[0-9]{2}-[0-9]{6}/", $str)){
			return true;    
		}
	} else {
		// 09xxxxxxxx 
		if(preg_match("/09[0-9]{8}/", $str)) {
			return true;    
		}
	}
	return false;
}

//驗證台灣身份證字號
function is_twid($id){
    $id=strtoupper($id);
    $d0=strlen($id);
    if ($d0 <= 0) {return false;}
    if ($d0 > 10) {return false;}
    if ($d0 < 10 && $d0 > 0) {return false;}
    $d1=substr($id,0,1);
    $ds=ord($d1);
    if ($ds > 90 || $ds < 65) {return false;}
    $d2=substr($id,1,1);
    if($d2!="1" && $d2!="2") {return false;}
    for ($i=1;$i<10;$i++) {
        $d3=substr($id,$i,1);
        $ds=ord($d3);
        if ($ds > 57 || $ds < 48) {
            $n=$i+1;
            return false;
            break;
        }
    }
    $num=array("A" => "10","B" => "11","C" => "12","D" => "13","E" => "14",
        "F" => "15","G" => "16","H" => "17","J" => "18","K" => "19","L" => "20",
        "M" => "21","N" => "22","P" => "23","Q" => "24","R" => "25","S" => "26",
        "T" => "27","U" => "28","V" => "29","X" => "30","Y" => "31","W" => "32",
        "Z" => "33","I" => "34","O" => "35");
    $n1=substr($num[$d1],0,1)+(substr($num[$d1],1,1)*9);
    $n2=0;
    for ($j=1;$j<9;$j++) {
        $d4=substr($id,$j,1);
        $n2=$n2+$d4*(9-$j);
    }
    $n3=$n1+$n2+substr($id,9,1);
    if(($n3 % 10)!=0) {return false;}
    return true;
}

//縣市代碼
function tw_area(){
	$area[100] = '台北';
	$area[207] = '新北';
	$area[200] = '基隆';
	$area[320] = '桃園';
	$area[300] = '新竹';
	$area[350] = '苗栗';
	$area[400] = '台中';
	$area[500] = '彰化';
	$area[540] = '南投';
	$area[630] = '雲林';
	$area[600] = '嘉義';
	$area[700] = '台南';
	$area[800] = '高雄';
	$area[900] = '屏東';
	$area[260] = '宜蘭';
	$area[970] = '花蓮';
	$area[950] = '台東';
	$area[880] = '澎湖';
	$area[890] = '金門';
	$area[209] = '馬祖';
	$area[999] = '其他';
	return $area;
}
