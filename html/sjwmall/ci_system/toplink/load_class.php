<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class load_class
{
	protected $_classes			= array();
	protected $base_classes		= array(); // Set by the controller class
	protected $library_paths	= array();
	protected $loaded_files		= array();
	protected $varmap			= array('unit_test' => 'unit',
									'user_agent' => 'agent');
	
	/**
	 * Constructor
	 *
	 * Sets the path to the view files and gets the initial output buffering level
	 */
	public function __construct()
	{
		$this->_classes = array();
		$this->loaded_files = array();
		$this->library_paths = array(APPPATH, BASEPATH);
	}
	
	public function lib($library)
	{
		if($library == ''){
			return FALSE;
		}

		//if(! is_null($params) && ! is_array($params)){ $params = NULL; }

		//$this->_load_class($library, $params, $object_name);
		$this->_load_class($library);
	}
	
	protected function &get_component($component)
	{
		$CI =& get_instance();
		return $CI->$component;
	}
	
	/**
	 * Load class
	 _load_class($class, $params = NULL, $object_name = NULL)
	 */
	public function _load_class($class)
	{
		$class = str_replace('.php', '', trim($class, '/'));

		$subdir = '';
		if(($last_slash = strrpos($class, '/')) !== FALSE){
			$subdir = substr($class, 0, $last_slash + 1);

			$class = substr($class, $last_slash + 1);
		}
		
		$subclass = BASEPATH .'lib/'. $class .'.php';

		exit;
		if(file_exists($subclass))
		{
			
			include_once($subclass);
			$this->loaded_files[] = $subclass;

			//return $this->_init_class($class, config_item('subclass_prefix'), $params, $object_name);
			return $this->_init_class($class, config_item('subclass_prefix'), $object_name);
		}

		/*
		foreach(array(ucfirst($class), strtolower($class)) as $class)
		{
			$subclass = APPPATH.'libraries/'.$subdir.config_item('subclass_prefix').$class.'.php';

			if(file_exists($subclass))
			{
				$baseclass = BASEPATH.'libraries/'.ucfirst($class).'.php';

				if(! file_exists($baseclass) ){
					log_message('error', "Unable to load the requested class: ".$class);
					show_error("Unable to load the requested class: ".$class);
				}

				if(in_array($subclass, $this->loaded_files))
				{
					if(! is_null($object_name) ){
						$CI =& get_instance();
						if(! isset($CI->$object_name) ){
							//return $this->_init_class($class, config_item('subclass_prefix'), $params, $object_name);
							return $this->_init_class($class, config_item('subclass_prefix'), $object_name);
						}
					}

					$is_duplicate = TRUE;
					log_message('debug', $class." class already loaded. Second attempt ignored.");
					return;
				}

				include_once($baseclass);
				include_once($subclass);
				$this->loaded_files[] = $subclass;

				//return $this->_init_class($class, config_item('subclass_prefix'), $params, $object_name);
				return $this->_init_class($class, config_item('subclass_prefix'), $object_name);
			}

			
			$is_duplicate = FALSE;
			foreach($this->library_paths as $path)
			{
				$filepath = $path.'libraries/'.$subdir.$class.'.php';

				if(! file_exists($filepath)){
					continue;
				}

				if(in_array($filepath, $this->loaded_files))
				{
					if(! is_null($object_name) ){
						$CI =& get_instance();
						if(! isset($CI->$object_name) ){
							//return $this->_init_class($class, '', $params, $object_name);
							return $this->_init_class($class, '', $object_name);
						}
					}

					$is_duplicate = TRUE;
					log_message('debug', $class." class already loaded. Second attempt ignored.");
					return;
				}

				include_once($filepath);
				$this->loaded_files[] = $filepath;
				//return $this->_init_class($class, '', $params, $object_name);
				return $this->_init_class($class, '', $object_name);
			}

		} // END FOREACH

		if($subdir == ''){
			$path = strtolower($class).'/'.$class;
			return $this->_load_class($path, $params);
		}

		if($is_duplicate == FALSE){
			log_message('error', "Unable to load the requested class: ".$class);
			show_error("Unable to load the requested class: ".$class);
		}
		*/
	}
	
	/**
	 * Instantiates a class
	 
	 _init_class($class, $prefix = '', $config = FALSE, $object_name = NULL)
	 */
	protected function _init_class($class, $prefix = '', $object_name = NULL) 
	{
		// Is there an associated config file for this class?  Note: these should always be lowercase
		/*
		if($config === NULL)
		{
			// Fetch the config paths containing any package paths
			$config_component = $this->get_component('config');

			if(is_array($config_component->_config_paths))
			{
				// Break on the first found file, thus package files
				// are not overridden by default paths
				foreach($config_component->_config_paths as $path)
				{
					// We test for both uppercase and lowercase, for servers that
					// are case-sensitive with regard to file names. Check for environment
					// first, global next
					if(defined('ENVIRONMENT') AND file_exists($path .'config/'.ENVIRONMENT.'/'.strtolower($class).'.php'))
					{
						include($path .'config/'.ENVIRONMENT.'/'.strtolower($class).'.php');
						break;
					}
					elseif(defined('ENVIRONMENT') AND file_exists($path .'config/'.ENVIRONMENT.'/'.ucfirst(strtolower($class)).'.php'))
					{
						include($path .'config/'.ENVIRONMENT.'/'.ucfirst(strtolower($class)).'.php');
						break;
					}
					elseif(file_exists($path .'config/'.strtolower($class).'.php'))
					{
						include($path .'config/'.strtolower($class).'.php');
						break;
					}
					elseif(file_exists($path .'config/'.ucfirst(strtolower($class)).'.php'))
					{
						include($path .'config/'.ucfirst(strtolower($class)).'.php');
						break;
					}
				}
			}
		}*/

		/*
		if($prefix == '')
		{
			if(class_exists('CI_'.$class)){
				$name = 'CI_'.$class;
			} elseif(class_exists(config_item('subclass_prefix').$class)) {
				$name = config_item('subclass_prefix').$class;
			} else {
				$name = $class;
			}
		}
		else
		{
			$name = $prefix.$class;
		}*/
		$name = $prefix . $class;

		if(! class_exists($name) ){
			log_message('error', "Non-existent class: ".$name);
			show_error("Non-existent class: ".$class);
		}

		$class = strtolower($class);

		if(is_null($object_name)){
			$classvar = (! isset($this->varmap[$class]) ) ? $class : $this->varmap[$class];
		} else {
			$classvar = $object_name;
		}

		$this->_classes[$class] = $classvar;

		$CI =& get_instance();
		if($config !== NULL){
			$CI->$classvar = new $name($config);
		} else {
			$CI->$classvar = new $name;
		}
	}
	
}

/* End of file Loader.php */
/* Location: ./system/core/Loader.php */