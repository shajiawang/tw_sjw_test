<?php
if (!class_exists('CI_Profiler'))
	include(BASEPATH . 'libraries/Profiler.php');

class AJAX_Profiler extends CI_Profiler {

	function __construct($config = array()) {
		parent::__construct($config);
	}

	private function _compile_session_data() {
		if ( ! isset($this->CI->session))
		{
			return;
		}

		$output = '<fieldset id="ci_profiler_csession" style="border:1px solid #000;padding:6px 10px 10px 10px;margin:20px 0 20px 0;background-color:#eee">';
		$output .= '<legend style="color:#000;">&nbsp;&nbsp;'.$this->CI->lang->line('profiler_session_data').'&nbsp;&nbsp;(<span style="cursor: pointer;" onclick="var s=document.getElementById(\'ci_profiler_session_data'. '_' . $this->id_key . '\').style;s.display=s.display==\'none\'?\'\':\'none\';this.innerHTML=this.innerHTML==\''.$this->CI->lang->line('profiler_section_show').'\'?\''.$this->CI->lang->line('profiler_section_hide').'\':\''.$this->CI->lang->line('profiler_section_show').'\';">'.$this->CI->lang->line('profiler_section_show').'</span>)</legend>';
		$output .= "<table style='width:100%;display:none' id='ci_profiler_session_data_{$this->id_key}'>";

		foreach ($this->CI->session->all_userdata() as $key => $val)
		{
			if (is_array($val))
			{
				$val = print_r($val, TRUE);
			}

			$output .= "<tr><td style='padding:5px; vertical-align: top;color:#900;background-color:#ddd;'>".$key."&nbsp;&nbsp;</td><td style='padding:5px; color:#000;background-color:#ddd;'>".htmlspecialchars($val)."</td></tr>\n";
		}

		$output .= '</table>';
		$output .= "</fieldset>";
		return $output;
	}

	function run() { 
		$output = "<div id='codeigniter_profiler' style='font-size: 0.7em; clear:both;background-color:#fff;padding:10px;'>";
		foreach ($this->_available_sections as $section) {
			if ($this->_compile_{$section} !== FALSE) {
				$func = "_compile_{$section}";
				$output .= $this->{$func}();
			}
		}
		$output .= '</div>';
		return $output;
	}

}
