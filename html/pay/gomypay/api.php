<?php
    
	function getRetJSONArray($code='', $msg='', $type='') {
         $ret=array();
		 $ret['retCode']=$code;
		 $ret['retMsg']=$msg;
		 $ret['retType']=$type;
		 if($type=='LIST') {
		    $ret['retObj']=array("data"=>array());
		 } else if($type=='JSON') {
		    $ret['retObj']=new stdClass();
		 }
		 return $ret;
    }

    function postReq($url='', $arrPost='') {
            $ret = getRetJSONArray(0,"NO_DATA","JSON");
                     
            if(empty($url)) {
              $ret['retCode']=-1;
              $ret['retMsg']="NO_URL";
              return $ret;    
            }
            if(empty($arrPost)) {
              $ret['retCode']=-2;
              $ret['retMsg']="NO_DATA";
              return $ret;    
            }
			$post_str=http_build_query($arrPost);
            error_log("[postReq] post data : ".$post_str);
            $ch = curl_init();
 
			// 設定擷取的URL網址
			curl_setopt($ch, CURLOPT_URL, $url);
 
			//強制轉為UTF-8
			curl_setopt($ch,CURLOPT_HTTPHEADER, array("Content-Type: application/x-www-form-urlencoded; charset=utf-8"));
 
			//將curl_exec()獲取的訊息以文件流的形式返回，而不是直接輸出。
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
 
			//設定CURLOPT_POST 為 1或true，表示要用POST方式傳遞
			curl_setopt($ch, CURLOPT_POST, 1); 
			
			//CURLOPT_POSTFIELDS 後面則是要傳接的POST資料。
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_str);
 
			// 執行
			$ret['retObj']=curl_exec($ch);
            $ret['retCode']=1;
            $ret['retMsg']="OK";
			
			// 關閉CURL連線
			curl_close($ch);
       
            return 	$ret;		
    }
	
	$arrPost=array();
	$arrPost['Send_Type']='0';
	$arrPost['Pay_Mode_No']='2';
	$arrPost['CustomerId']='25089889';
	$arrPost['Order_No']=date('YmdHis');
	$arrPost['CardNo']='4907060600015101';
    $arrPost['ExpireDate']='2012';
	$arrPost['CVV']='615';
	$arrPost['TransCode']='00'; 
	$arrPost['Amount']='35';
	$arrPost['Installment']='0';
	$arrPost['TransMode']='1';
	$arrPost['Buyer_Name']='王殺價';
	$arrPost['Buyer_Telm']='0983333119';
	$arrPost['Buyer_Mail']='shajiawang333@gmail.com';
	$arrPost['Buyer_Memo']='殺價王串接測試';
	$arrPost['e_return']='1';
	$arrPost['Str_Check']='ed4q74rd7ubu1f6j13uiy9bjr5sc0qeg';
	
	$ret=postReq('https://n.gomypay.asia/TestShuntClass.aspx', $arrPost);
	
	echo json_encode($ret);
	
?> 
<!-- FORM action="https://n.gomypay.asia/TestShuntClass.aspx" name="theForm" id="theForm" method="post"> 
		<input name='Send_Type'  value='0'> 
		<input name='Pay_Mode_No'  value='2'> 
		<input name='CustomerId'  value='25089889'> 
		<input name='CardNo'  value='1111222233334444'> 
		<input name='ExpireDate'  value='2010'> 
		<input name='CVV' value='888'> 
		<input name='TransMode' value='1'> 
		<input name='Amount' value='100'> 
		<input name='Installment' value='0'> 
		<input name='TransCode' value='00'> 
		<input name='Buyer_Name'  value='王殺價'> 
		<input name='Buyer_Telm'  value='0983333119'> 
		<input name='Buyer_Mail'  value='shajiawang333@gmail.com'> 
		<input name='Buyer_Memo'  value='殺價王串接測試'> 
		<input name='e_return'  value='1'> 
		<input name='Str_Check'  value='Sjw#333'>
	  </FORM>
<script>
     document.theForm.submit();
</script -->