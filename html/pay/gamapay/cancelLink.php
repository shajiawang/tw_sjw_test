<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
    session_start();
    $Token=$_REQUEST['Token'];
    $MerchantID=$_ID; 
    $MerchantAccount=$_REQUEST['MerchantAccount'];
    if(empty($MerchantAccount)) {
        $MerchantAccount = $_SESSION['user']['name'];  
    }
    
    error_log("[gamapay/cancelLink]  Token:".$Token.", MerchantAccount:".$MerchantAccount.", MerchantID:".$MerchantID);

    if(empty($Token) || empty($MerchantAccount)) {
       echo "https://www.saja.com.tw/pay/gamapay/cancelLink.php?Token=...&MerchantAccount=...";
       return;       
    }
    
    $arrPost=array(
      "Token"=>$Token,
      "MerchantID"=>$MerchantID,
      "MerchantAccount"=>$MerchantAccount,
      "MAC"=>getMAC($Token.$MerchantID.$MerchantAccount.$_IV)
    );

    $return = requestGamapayAPI($_GAMA_URL."Link/CancelLink",$arrPost);
    
    if($return) {
       error_log("[gamapay/cancelLink] return : ".$return);
       echo $return;
    }
?>