<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
   
    session_start();
    $Token=$_REQUEST['Token'];
    $MerchantID=$_ID;
       
    error_log("[gamapay/getLinkList]  Token:".$Token.", MerchantID:".$MerchantID);

    if(empty($Token)) {
        echo "Usage : https://www.saja.com.tw/pay/gamapay/getLinkList.php?Token=...";
        return;
    }    
        
    
    $arrPost=array(
      "Token"=>$Token,
      "MerchantID"=>$MerchantID,
      "MAC"=>getMAC($Token.$MerchantID.$_IV)
    );

    $return = requestGamapayAPI($_GAMA_URL."Link/GetLinkList",$arrPost);
    
    if($return) {
       // $arrRet = json_decode($return,TRUE);
       error_log("[gamapay/getLinkList] return : ".$return);
       echo $return;
       
    }
?>