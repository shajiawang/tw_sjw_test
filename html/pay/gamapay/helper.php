<?php

function getMAC($p1) {
    global $_IV, $_KEY, $_ID, $_GAMAPAY_ID;
    // $key = base64_decode('aEhEFEew27JdLsLpsKo/ZOYBnZZ5MKUgoaLgeAVTzE4=');
    // $iv = base64_decode('ji6DCVfCKks7qLx8PDx3qQ==');
    $key = base64_decode($_KEY);
    $iv = base64_decode($_IV);
    error_log("[getMAC] to encode : ".$p1);
    $p1 = mb_convert_encoding($p1, 'utf-16le', 'UTF-8');
    $encrypted = openssl_encrypt($p1, "AES-256-CBC", $key, 0, $iv);
    $MAC = base64_encode(hash('sha256', $encrypted, true));
    error_log("[getMAC] MAC : ".$MAC);
    return  $MAC;
}


function requestGamapayAPI($url="", $arrPost="") {
             // set Header
             if(empty($url)) {
                error_log("[pay/gamapay/requestGamapayAPI] empty target URL !!");
                return false;
             }
             error_log("[pay/gamapay/requestGamapayAPI] requestURL : ".$url);

             $ch = curl_init();

             // set Timeout
             curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
             curl_setopt($ch, CURLOPT_TIMEOUT, 20);
             curl_setopt($ch, CURLOPT_POST, 1);

             
             // set Post Data
             // if(!empty($arrPost) && is_array($arrPost)) {
                // $postJson = json_encode($arrPost);
                // error_log("[requestGamapayAPI] : ".$postJson);
                // curl_setopt ($ch, CURLOPT_POSTFIELDS,$postJson);
                // curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
                    // 'Content-Type: application/json',                                                                                
                    // 'Content-Length: ' . strlen($postJson))                                                                       
                // );
             // }
             // curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
             // curl_setopt($ch, CURLOPT_URL, $url);
             // curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
             // $response = curl_exec($ch);
             // curl_close($ch);
             // error_log("[pay/gamapay/requestGamapayAPI] response : ".$response);
			 
			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
						'method' => "POST",
						'content' => json_encode($arrPost),
						'header' => "Content-Type:application/json" 
				) 
			);
			$context  = stream_context_create($stream_options);

			// 送出json內容並取回結果
			$response = file_get_contents($url, false, $context);
			// 讀取json內容
			$arr = json_decode($response,true);
			error_log("[pay/gamapay/requestGamapayAPI] response : ".json_encode($arr));			 
            
			return $response;
     }

     
     function formatTransAmount($Amount) {
              if($Amount<0)
                 return false;
              
              
              // 四捨五入到小數第二位後轉成字串
              $strAmount=strval(round(floatval($Amount),2)*100);
              //去掉小數點
              $strAmount=str_replace(".","",$strAmount);
             
              // 補零
              return str_pad($strAmount,14,"0",STR_PAD_LEFT);
     }
?>
