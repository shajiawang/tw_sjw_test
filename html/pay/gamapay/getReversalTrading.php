<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
  
/*
{
"TransactionToken":"da5bce71560144cf9b62149a38a963c0",
"TransactionID":"20180806100084689617",
"MerchantID":"2052",
"SubMerchantID":null,
"MerchantOrderID":"20180806105800",
"TransAmount":5.0,
"CurrencyCode":"TWD",
"TransType":"10",
"ExpiredDateTime":"2018/08/06 11:39:12",
"MAC":"CD/1jT9C/1DR4XNyxWTsbTJ2MQ/1NJrVCQ+p1ZSLyrU=",
"QRCodeData":"{\"Type\":3,\"ReceiverLoginID\":\"saja\",\"TransactionToken\":\"da5bce71560144cf9b62149a38a963c0\",\"Source\":\"Vender\",\"TransType\":\"10\",\"SubMerchantID\":null,\"CashierSN\":null}",
"ResultCode":"0000","ResultMessage":"成功",
"PayerID":"2055","LoveCode":null,"CarrierID":null,"PayTypeID":"8000","KeyID":null,
"TransFinallyAmount":0.0,"TransFee":0.0,"PerformanceBondDateTime":null,
"CreatedDate":"2018/08/06 11:09:12","TransDate":null,"StatusCode":"0","Relation_ID_Trans":null,"CashierSN":null
}
*/
    session_start();
   
    $MerchantID=$_ID;
    $MerchantOrderID=$_REQUEST['MerchantOrderID'];
    // 沖正專用
    $TransactionID="ReversalTrading";
    $TransAmount=$_REQUEST['TransAmount'];
    
    $TransType=$_REQUEST['TransType'];
    if(empty($TransType))    
       $TransType=10;
    
    $CurrencyCode=$_REQUEST['CurrencyCode'];
    if(empty($CurrencyCode))    // 交易類別
       $CurrencyCode="TWD";
       
    error_log("[gamapay/getReversalTrasing] MerchantOrderID:".$MerchantOrderID.", TransAmount:".$TransAmount.", TransactionID:".$TransactionID);

     if(empty($MerchantOrderID) ||empty($TransAmount) || empty($TransactionID)) {
       echo "Usage : https://www.saja.com.tw/pay/gamapay/getReversalTrading.php?MerchantOrderID=...&TransactionID=...&TransAmount=...";
       return;       
    }
    
    $arrPost=array(
      "MerchantID"=>$MerchantID,
      "MerchantOrderID"=>$MerchantOrderID,
      "TransactionID"=>$TransactionID,
      "TransType"=>$TransType,
      "TransAmount"=>$TransAmount,
      "CurrencyCode"=>$CurrencyCode,
      "MAC"=>getMAC($MerchantID.$MerchantOrderID.$CurrencyCode.formatTransAmount($TransAmount).$_IV)
    );

    $return = requestGamapayAPI($_GAMA_URL."Transaction/getTransOrder",$arrPost);
    
    
    if($return) {
       error_log("[gamapay/getReversalTrasing] response : ".$return);
       echo $return;
    }
?>