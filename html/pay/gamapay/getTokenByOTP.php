<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
    $MerchantID=$_ID;
    $MerchantAccount=$_REQUEST['MerchantAccount'];
    if(empty($MerchantAccount))
       $MerchantAccount=$_SESSION['auth_id'];
    $GamaPayID=$_REQUEST['GamaPayID'];
    
    error_log("[gtTokenByOTP] GamaPayID:".$GamaPayID);
    $arrRet=array();
    
    if(empty($GamaPayID)) {
       echo "Usage : https://www.saja.com.tw/pay/gamapay/getTokenByOTP.php?GamaPayID=...";
       return ;        
    }
    
    $arrPost=array(
      "MerchantID"=>$MerchantID,
      "GamaPayID"=>$GamaPayID,
      "MAC"=>getMAC($MerchantID.$GamaPayID.$_IV)
    );

    $return = requestGamapayAPI($_GAMA_URL."Identity/GetTokenByOTP",$arrPost);

    // error_log("[gamapay/GetTokenByOTP] return : ".$return);
    if($return) {
       $arrRet = json_decode($return,TRUE);
       $arrRet['MerchantAccount']=$MerchantAccount;
       error_log("[gamapay/GetTokenByOTP] ".json_encode($arrRet));
       echo json_encode($arrRet);     
    }
?>

