<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
    // $userid=$_POST['userid'];
    $token=$_REQUEST['Token'];
    $verifyData=$_REQUEST['VerifyData'];
    
    error_log("[gamapay/verify]  Token:".$token.", VerifyData:".$verifyData);

    if(empty($token) || empty($verifyData)) {
       echo "Usage : https://www.saja.com.tw/pay/gamapay/verify.php?Token=...&VerifyData=...";
       return ;       
    }
    
    $arrPost=array(
      "Token"=>$token,
      "VerifyData"=>$verifyData,
      "MAC"=>getMAC($token.$verifyData.$_IV)
    );

    $return = requestGamapayAPI($_GAMA_URL."Identity/VerifyTokenByOTP",$arrPost);
    
    if($return) {
       // $arrRet = json_decode($return,TRUE);
       error_log("[gamapay/verify] return : ".$return);
       echo $return;
    }
?>


