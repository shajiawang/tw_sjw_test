<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
   
    session_start();
    $Token=$_REQUEST['Token'];
    $MerchantID=$_REQUEST['MerchantID'];
    $GamaPayID=$_REQUEST['GamaPayID'];
    if(empty($MerchantID)) {
       $MerchantID=$_ID;   
    }
    $MerchantAccount=$_REQUEST['MerchantAccount'];
    if(empty($MerchantAccount)) {
        $MerchantAccount = $_SESSION['user']['name'];  
    }
    
    error_log("[gamapay/createLink]  Token:".$Token.", MerchantAccount:".$MerchantAccount.", GamaPayID:".$GamaPayID);

    
    $arrPost=array(
      "Token"=>$Token,
      "MerchantID"=>$MerchantID,
      "MerchantAccount"=>$MerchantAccount,
      "MAC"=>getMAC($Token.$MerchantID.$MerchantAccount.$_IV)
    );

    $return = requestGamapayAPI($_GAMA_URL."Link/CreateLink",$arrPost);
    
    if($return) {
       error_log("[gamapay/createLink] response : ".$return);
       $arrRet = json_decode($return,TRUE);
       if($arrRet.ResultCode=='0000') {
          $arrRet['GamaPayID']= $GamaPayID;
          $arrRet['MerchantAccount']= $MerchantAccount;         
       }
       $r = json_encode($arrRet);
       // 回傳GamaPayAccount (橘子支虛擬帳號)
       error_log("[gamapay/createLink] return : ".$r);
       echo $r;
    }
?>