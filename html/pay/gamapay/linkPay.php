<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
   
    // 
    session_start();
    // echo json_encode($_SESSION['user']);
    // echo "<p>";
    // Mandatory
    $MerchantID=$_ID;   
    $GamaPayAccount=$_REQUEST['GamaPayAccount'];
    $TransAmount=$_REQUEST['TransAmount'];
        
    $MerchantOrderID=$_REQUEST['MerchantOrderID'];             // 原始商家訂單編號
    if(empty($MerchantOrderID))
        $MerchantOrderID=date('YmdHis');
   
    $MerchantAccount=$_REQUEST['MerchantAccount'];
    if(empty($MerchantAccount))
       $MerchantAccount=$_SESSION['user']['name'];
   
    $BusinessDate=$_REQUEST['BusinessDate'];  
    if(empty($BusinessDate))
       $BusinessDate=date('Ymd');
   
    $TransType=$_REQUEST['TransType'];
    if(empty($TransType))    
       $TransType=10;
    
    $CurrencyCode=$_REQUEST['CurrencyCode'];
    if(empty($CurrencyCode))    // 交易類別
       $CurrencyCode="TWD";
    
    $TradingContent=$_REQUEST['TradingContent'];       // 多筆商品請以 ; 分隔
    if(empty($TradingContent))
       $TradingContent="Test OrderID : ".$MerchantOrderID.", TransAmount : ".$TransAmount.", GamaPayAccount:".$GamaPayAccount.", MerchantAccount:".$MerchantAccount;   
    // $TradingContent="Test TX...";          
    $CashierSN=$_REQUEST['CashierSN'];                 
    $ExcludeItem=$_REQUEST['ExcludeItem'];
    $ExcludeAmount=$_REQUEST['ExcludeAmount'];
    $Memo=$_REQUEST['Memo'];
    $Remark1 =$_REQUEST['Remark1'];
    $Remark2 =$_REQUEST['Remark2']; 
    
    error_log("[gamapay/linkPay] MerchantOrderID:".$MerchantOrderID.", GamaPayAccount:".$GamaPayAccount.
              ", MerchantAccount:".$MerchantAccount.", TransAmount:".$TransAmount.", BusinessDate:".$BusinessDate.", TradingContent:".$TradingContent);

    $arrRet = array("ResultCode"=>"-1", "ResultMessage"=>"INVALID INPUT");
    if(empty($MerchantOrderID) || empty($GamaPayAccount) || empty($MerchantAccount) || empty($TransAmount) || empty($TradingContent)) {
       echo json_encode($arrRet);
       error_log("Usage : https://www.saja.com.tw/pay/gamapay/linkPay.php?MerchantOrderID=...&GamaPayAccount=...&MerchantAccount=...&TransAmount=...&BusinessDate=...");
       return;       
    }
    
    $arrPost=array(
      "MerchantOrderID"=>$MerchantOrderID,
      "GamaPayAccount"=>$GamaPayAccount,
      "MerchantID"=>$MerchantID,
      "MerchantAccount"=>$MerchantAccount,
      "TransType"=>$TransType,
      "TradingContent"=>$TradingContent,
      "TransAmount"=>$TransAmount,
      "CurrencyCode"=>$CurrencyCode,
      "BusinessDate"=>$BusinessDate,
      "MAC"=>getMAC($MerchantOrderID.$GamaPayAccount.$MerchantID.$MerchantAccount.$CurrencyCode.formatTransAmount($TransAmount).$_IV)
    );
    /*
     $arrPost=array(
      "MerchantOrderID"=>$MerchantOrderID,
      "GamaPayAccount"=>$GamaPayAccount,
      "MerchantID"=>$MerchantID,
      "SubMerchantID"=>$SubMerchantID,
      "MerchantAccount"=>$MerchantAccount,
      "TransType"=>intVal($TransType),
      "TradingContent"=>$TradingContent,
      "TransAmount"=>round($TransAmount),
      "CurrencyCode"=>$CurrencyCode,
      "BusinessDate"=>$BusinessDate,
      "MAC"=>getMAC($MerchantOrderID.$GamaPayAccount.$MerchantID.$SubMerchantID.$MerchantAccount.$CurrencyCode.formatTransAmount($TransAmount).$_IV)
    );
    */
    $response = requestGamapayAPI($_GAMA_URL."Link/LinkPay",$arrPost);
    
    if($response) {
       $arrRet = json_decode($response,TRUE);
       if($arrRet['ResultCode']='0000') {
       
       } else {
           
       }
       error_log("[gamapay/linkPay] response : ".$response);
       echo $response;
    }
    return;
?>