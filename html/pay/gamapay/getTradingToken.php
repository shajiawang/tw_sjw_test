<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
   
    session_start();
    $MerchantID=$_ID;   
    $MerchantOrderID=$_REQUEST['MerchantOrderID'];
    // 因字元數需要大於8, 建議傳手機號或橘子支付虛擬帳號
    $MerchantMemberID=$_REQUEST['MerchantMemberID'];   
    $AccountType=$_REQUEST['AccountType'];
    if(empty($AccountType))
        $AccountType="1"; 
    $AccountID=$_REQUEST['AccountID'];
     
       
    error_log("[gamapay/getTradingToken] MerchantOrderID:".$MerchantOrderID.", MerchantMemberID:".$MerchantMemberID.
              ", AccountType:".$AccountType.":AccountID".$AccountID);

    if(empty($MerchantOrderID) || empty($MerchantMemberID) || empty($AccountType) || empty($AccountID)) {
       echo "usage : https://www.saja.com.tw/pay/gamapay/getTradingToken.php?MerchantOrderID=...&MerchantMemberID=...&AccountType=...&AccountID=...&TransAmount=...";
       return;       
    }
   
    $arrPost=array(
      "MerchantID"=>$MerchantID,
      "MerchantOrderID"=>$MerchantOrderID,
      "MerchantMemberID"=>$MerchantMemberID,
      "AccountType"=>$AccountType,
      "AccountID"=>$AccountID,
      "MAC"=>getMAC($MerchantID.$MerchantOrderID.$MerchantMemberID.$AccountType.$AccountID.$_IV)
    );

    $response = requestGamapayAPI($_GAMA_URL."Transaction/GetTradingToken",$arrPost);
    
    if($response) {
       $arrRet = json_decode($response,TRUE);
       if($arrRet['ResultCode']='0000') {
       
       } else {
           
       }
       error_log("[gamapay/getTradingToken] response : ".$response);
       echo $response;
    }
    return;
?>