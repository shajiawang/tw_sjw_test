<?php
  require_once("config.php");
  require_once("helper.php");
?>
<?php
   /*
   {"TradingToken":"693040219110707563","MerchantID":"2052","SubMerchantID":null,"MerchantOrderID":"20180806105800","MerchantMemberID":"675a11c2-6bfb-43ab-8810-55a55d3e1f7d","AccountType":"1","AccountID":"saja250","MAC":"DsUH4juDBSVXciDdBmLT3ImpDvA1aBFOcj1HCDHhKKY=","ResultCode":"0000","ResultMessage":"成功"}
   */
    session_start();
   
    $MerchantID=$_ID;
    $MerchantOrderID=$_REQUEST['MerchantOrderID'];
    $TradingToken=$_REQUEST['TradingToken'];
    $TransAmount=$_REQUEST['TransAmount'];
    
    $TradingContent=$_REQUEST['TradingContent'];
    if(empty($TradingContent))
        $TradingContent="Test OrderID : ".$MerchantOrderID.", TransAmount : ".$TransAmount.", TradingToken:".$TradingToken;
    
    $BusinessDate=$_REQUEST['BusinessDate'];  
    if(empty($BusinessDate))
       $BusinessDate=date('Ymd');
   
    $TransType=$_REQUEST['TransType'];
    if(empty($TransType))    
       $TransType=10;
    
    $CurrencyCode=$_REQUEST['CurrencyCode'];
    if(empty($CurrencyCode))    // 交易類別
       $CurrencyCode="TWD";
       
    $CashierSN=$_REQUEST['CashierSN'];                 
    $ExcludeItem=$_REQUEST['ExcludeItem'];
    $ExcludeAmount=$_REQUEST['ExcludeAmount'];
    $Memo=$_REQUEST['Memo'];
    $Remark1 =$_REQUEST['Remark1'];
    $Remark2 =$_REQUEST['Remark2']; 
    
    error_log("[gamapay/orderCreate] TradingToken: ".$TradingToken.", MerchantOrderID:".$MerchantOrderID.", TransAmount:".$TransAmount.", TradingContent:".$TradingContent);

     if(empty($TradingToken) || empty($MerchantOrderID) ||empty($TransAmount) || empty($TradingContent)) {
       echo "Usage : https://www.saja.com.tw/pay/gamapay/orderCreate.php?MerchantOrderID=...&TradingToken=...&TransAmount=...";
       return;       
    }
    
    $arrPost=array(
      "MerchantID"=>$MerchantID,
      "MerchantOrderID"=>$MerchantOrderID,
      "TransType"=>$TransType,
      "TradingContent"=>$TradingContent,
      "TradingToken"=>$TradingToken,
      "TransAmount"=>$TransAmount,
      "CurrencyCode"=>$CurrencyCode,
      "BusinessDate"=>$BusinessDate,
      "MAC"=>getMAC($MerchantID.$MerchantOrderID.$CurrencyCode.formatTransAmount($TransAmount).$_IV)
    );

    $return = requestGamapayAPI($_GAMA_URL."Transaction/OrderCreate",$arrPost);
    
    /*
    {"TransactionToken":"da5bce71560144cf9b62149a38a963c0",
    "TransactionID":"20180806100084689617",
    "MerchantID":"2052",
    "SubMerchantID":null,
    "MerchantOrderID":"20180806105800",
    "TransAmount":5.0,
    "CurrencyCode":"TWD",
    "TransType":"10",
    "ExpiredDateTime":"2018/08/06 11:39:12",
    "MAC":"CD/1jT9C/1DR4XNyxWTsbTJ2MQ/1NJrVCQ+p1ZSLyrU=",
    "QRCodeData":"{\"Type\":3,\"ReceiverLoginID\":\"saja\",\"TransactionToken\":\"da5bce71560144cf9b62149a38a963c0\",\"Source\":\"Vender\",\"TransType\":\"10\",\"SubMerchantID\":null,\"CashierSN\":null}",
    "ResultCode":"0000",
    "ResultMessage":"成功",
    "PayerID":"2055",
    "LoveCode":null,
    "CarrierID":null,
    "PayTypeID":"8000",
    "KeyID":null,
    "TransFinallyAmount":0.0,
    "TransFee":0.0,
    "PerformanceBondDateTime":null,
    "CreatedDate":"2018/08/06 11:09:12",
    "TransDate":null,
    "StatusCode":"0",
    "Relation_ID_Trans":null,
    "CashierSN":null}
    */
    if($return) {
       error_log("[gamapay/orderCreate] response : ".$return);
       echo $return;
    }
?>