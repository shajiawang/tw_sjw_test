// JavaScript Document

$(function() {
	$( "body>[data-role='panel']" ).panel();
	$.mobile.ignoreContentEnabled;
});

//--

function getNowTime() {
	var n = new Date();
	var y = n.getFullYear();
	var m = (n.getMonth()+1< 10)?("0" + (n.getMonth() + 1)):(n.getMonth() + 1);
	var d = (n.getDate()< 10)?("0" + (n.getDate())):(n.getDate());
	var h = (n.getHours() + 8< 10)?("0" + (n.getHours() + 8)):(n.getHours() + 8);
	var mi = (n.getMinutes()< 10)?("0" + (n.getMinutes())):(n.getMinutes());
	var s = (n.getSeconds()< 10)?("0" + (n.getSeconds())):(n.getSeconds());
	return now = y+""+m+""+d+""+h+""+mi+""+s;
}

/* cdntime.php */
var now_time;
$(document).on("pagecreate",function(event){
	$.get("/site/lib/cdntime.php?"+getNowTime(), function(data) {
		now_time = data; //系統時間
	});
});

$(this).everyTime('1s','losttime', function(i) {
	var pageid = $.mobile.activePage.attr('id');
	now_time = parseInt(now_time) + 1;
	for(i=0; i<$('#'+pageid+' .countdown').length; i++)	{
		var end_time = $('#'+pageid+' .countdown:eq('+i+')').attr('data-offtime'); //截標時間
		var end_plus = $('#'+pageid+' .countdown:eq('+i+')').attr('data-tag'); //標籤
		var lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
		if(lost_time > 0) {
			var o = lost_time; //原始剩餘時間
			var d = parseInt(o /24 / 60 / 60);
			if(d<10){d = "0" + d;};
			var h = parseInt((o - parseInt(d * 24 * 60 * 60)) / 60 / 60); //37056
			if(h<10){h = "0" + h;};
			var m = parseInt((o - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60)) / 60);
			if(m<10){m = "0" + m;};
			var s = parseInt((o - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60) - parseInt(m * 60)));
			if(s<10){s = "0" + s;};
			$('#'+pageid+' .countdown:eq('+i+')').html(end_plus+d+"天"+h+"时"+m+"分"+s+"秒");
		}
		else if(lost_time <= 0) {
			$(this).stopTime('losttime');
			$('#'+pageid+' .countdown:eq('+i+')').html(end_plus+"00天00时00分00秒");
			alert('商品已结标');
			location.href="/site/"; 
		}
	}
});

/* logout.php */
function logout() {
	$.post("/site/ajax/logout.php?"+getNowTime(), function(data){
		alert('注销成功');
		location.href="/site/";
	});
}

function change_page() {
	//window.location.href = window.location.href.replace(/p=\d+/, 'p='+$(this).val());
};

//商品分類
function cat_select(sel) {
	var url=sel.value;
	location.href = url;
}

//-
/*function recharge() confirm.php */
function deposit2() {
	var pageid = $.mobile.activePage.attr('id');
	var odriid = $('#'+pageid+' #driid :radio:checked').val();
	var odrid = $('#'+pageid+' #drid :radio:checked').val();
	if (odriid == undefined) {
		alert("請選擇支付點數！");
		return false;
	} else if (odrid == undefined) {
		alert("請選擇支付方式！");
		return false;
	} else {
		$.mobile.changePage("/site/deposit/confirm/?drid="+ odrid +"&driid="+ odriid, {transition:"slide"} );
	}
}

function deposit1() {
    var pageid = $.mobile.activePage.attr('id');
	var odriid = $('#'+pageid+' #driid :radio:checked').val();
	var odrid = $('#'+pageid+' #drid :radio:checked').val();
	if (odriid == undefined) {
		alert("請選擇支付點數！");
		return false;
	} else if (odrid == undefined) {
		alert("請選擇支付方式！");
		return false;
	} else {
		$.mobile.changePage("/site/deposit/confirm1/?drid="+ odrid +"&driid="+ odriid, {transition:"slide"} );
	}
}

function deposit() {
    var pageid = $.mobile.activePage.attr('id');
	var odriid = $('#'+pageid+' #driid :radio:checked').val();
	var odrid = $('#'+pageid+' #drid :radio:checked').val();
	
	if (odrid == undefined) {
	    alert("请选择支付方式！");
		return false;
	}

	if(odrid==7) {
	   $.mobile.changePage("/site/deposit/confirm/?drid="+ odrid, {transition:"slide"} );
	} else if(odrid!=7){
	   if (odriid == undefined) {
	   	   alert("请选择支付点数！");
		   return false;
	   }
	   $.mobile.changePage("/site/deposit/confirm/?drid="+ odrid +"&driid="+ odriid, {transition:"slide"} );   
	} 
	
}