var options = {
    dom : {
        caller : '.keyboard',                             //呼叫鍵盤的物件
        keyidName : 'numkeyModal',                        //小鍵盤本體(無id符號)
        callerId : ''                                     //紀錄呼叫者ID
    },
    btn : {
        sure : '送出'                                      //sure按鈕的名稱
    }
};

//增加底層 預留鍵盤的假div,num高度帶入鍵盤高度
function addBlank(num) {
    $("body").append('<div class="addBlank"></div>');
    $(".addBlank").css("paddingTop",num);
}
//移除預留鍵盤的假div
function removeBlank() {
    $(".addBlank").remove();
}

//調整輸入框位置
    //控制鍵盤捲動位置
        //情況一 最頂 (直接顯示)
        //情況二 中間 (捲動到輸入區域)
        //情況三 最底 (頁面底部增加鍵盤高度，並捲到最下方) (關閉時記得減回來)
function controlSize($wherekey) {
    
    //原始scroll
    var $olderScroll = $(window).scrollTop();
    
    //先計算視窗高度
    var $phH = $(window).innerHeight();
    
    //整個頁面高度
    var $pageH = $("body").height();
    
    //計算鍵盤高度
    var $boardlineH = parseFloat($(".number-style").css('line-height'));
    var $boardtopP = parseFloat($(".number-style").css('padding-top'));
    var $boardbottP = parseFloat($(".number-style").css('padding-bottom'));
    var $boardborder = parseFloat($(".number-style").css('border-bottom-width'));
    
    var $boardPx = $boardlineH + $boardtopP + $boardbottP + $boardborder;
    var $keyboardH = (parseFloat($boardPx * 4 ) + 6 * 5);

    //輸入框位置
    var $inputPlace = $wherekey.offset().top;
    
    //輸入框元件高度
    var $inputH = $wherekey.height();
    
    //儲存當前捲動值
    var $nowScroll = $(window).scrollTop();

    //輸入框與鍵盤間的安全距離
    var $safeH = 30;

    //計算扣除鍵盤剩餘高度可容許值 下限
    var $mintotalH = $phH - $keyboardH - $inputH - $safeH;
    
    //計算扣除鍵盤剩餘高度可容許值 上限
    var $maxtotalH = $pageH - $keyboardH;

    if ($inputPlace > $mintotalH){
        if($inputPlace < $maxtotalH) {
            $('html, body').animate({scrollTop:$inputPlace-($mintotalH - $inputH /2)},300);
            //console.log(1)
        }else{
            addBlank($keyboardH);
            $('html, body').animate({scrollTop:$inputPlace-($mintotalH - $inputH /2)},300);
            //console.log(2)
//            alert('推推')
        }
    }
            
            
        //toDebug("視窗高度:"+$phH+"..."+"頁面高度:"+$pageH+"..."+"鍵盤高度:"+$keyboardH+"..."+"輸入框位置:"+$inputPlace+"..."+"可容許值 下限:"+$mintotalH+"..."+"可容許值 上限:"+$pageH+"..."+"輸入框元件高度:"+$inputH+"..."+"安全距離:"+$safeH);
}

/*按鈕動作*/
function Calculation(which,$domEle){
    //建立閃爍文字游標
    //$numBoxLi.eq($tabNum).append($pic);
    options.dom.callerId = which;
    function addnum(num) {
        var $domEle = $("#passport");
        var oldtext = $domEle.val();
        which.val(oldtext+num);
        $domEle.val(oldtext+num);
        return;
    };
    $("#b1").click(function(){
        addnum("1");
    });
    $("#b2").click(function(){
        addnum("2");
    });
    $("#b3").click(function(){
        addnum("3");
    });
    $("#b4").click(function(){
        addnum("4");
    });
    $("#b5").click(function(){
        addnum("5");
    });
    $("#b6").click(function(){
        addnum("6");
    });
    $("#b7").click(function(){
        addnum("7");
    });
    $("#b8").click(function(){
        addnum("8");
    });
    $("#b9").click(function(){
        addnum("9");
    });
    $("#b0").click(function(){
        addnum("0");
    });
     
    $("#x").click(function(){
        
        var old = $domEle.val();
        if(old.length > 0){
            $tabNum = which.val().length
            which.val(old.substr(0,old.length-1));
            $domEle.val(old.substr(0,old.length-1));
            //$numBoxLi.eq($tabNum-1).removeClass("act").find("span").text('');
            //$numBoxLi.find(".pic").remove();
            //$numBoxLi.eq($tabNum-1).append($pic);
        }
    });
    
    //確定按鈕
    $("#sure").on("click",function(e){
        if ($domEle.val()!=''){
            which.val(String(which.val()));
        }else{
            which.val('');
        };

        $('[id^='+options.dom.keyidName+']').modal('toggle');
        e.stopPropagation;
        e.preventDefault;
    })
    //動畫效果
    function testAnim(x) {
        $('.modal .modal-dialog').attr('class', 'modal-dialog ' + x + ' animated');
    };
    
    $('[id^='+options.dom.keyidName+']').on('show.bs.modal', function (e) {
        var anim = "fadeInUp";
        testAnim(anim);
        
    });
    
    $('[id^='+options.dom.keyidName+']').on('hide.bs.modal', function (e) {
        var anim = "fadeOutDown";
        testAnim(anim);
        //$(options.dom.caller).unbind();
        $('[id^='+options.dom.keyidName+']').remove();
        //$numBoxLi.find(".pic").remove();
    });
}

/*生成鍵盤*/
function hasDianClass(Boolean,modalNum) {
    var backBtn = '/site/static/img/Delete.png';
    if(Boolean){
        //true時，有小數點的鍵盤
        $("body").append('<div class="modal fade" id="'+modalNum+'" tabindex="-1" role="dialog" aria-labelledby="numkeyModalLabel" aria-hidden="true">\
            <div class="modal-dialog" role="document">\
                <div class="modal-content numberkeyboard-box">\
                    <div class="hide">\
                        <input id="passport" type="text" class="form-control" placeholder="請輸入數字"/>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="c" class="col btn number-style gray">C</div>\
                        <div id="sure" class="col btn number-style gray">'+options.btn.sure+'</div>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="b1" class="col btn number-style">1</div>\
                        <div id="b2" class="col btn number-style">2</div>\
                        <div id="b3" class="col btn number-style">3</div>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="b4" class="col btn number-style">4</div>\
                        <div id="b5" class="col btn number-style">5</div>\
                        <div id="b6" class="col btn number-style">6</div>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="b7" class="col btn number-style">7</div>\
                        <div id="b8" class="col btn number-style">8</div>\
                        <div id="b9" class="col btn number-style">9</div>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="dn" class="col btn number-style">.</div>\
                        <div id="b0" class="col btn number-style">0</div>\
                        <div id="x" class="col btn number-style"><img src="'+backBtn+'"></div>\
                    </div>\
                </div>\
            </div>\
        </div>');
    }else{
        //false時，無小數點的鍵盤
        $("body").append('<div class="modal fade" id="'+modalNum+'" tabindex="-1" role="dialog" aria-labelledby="numkeyModalLabel" aria-hidden="true">\
            <div class="modal-dialog" role="document">\
                        <div class="modal-content numberkeyboard-box">\
                            <div class="hide">\
                                <input id="passport" type="text" class="form-control" placeholder="請輸入數字"/>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="b1" class="col btn number-style">1</div>\
                                <div id="b2" class="col btn number-style">2</div>\
                                <div id="b3" class="col btn number-style">3</div>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="b4" class="col btn number-style">4</div>\
                                <div id="b5" class="col btn number-style">5</div>\
                                <div id="b6" class="col btn number-style">6</div>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="b7" class="col btn number-style">7</div>\
                                <div id="b8" class="col btn number-style">8</div>\
                                <div id="b9" class="col btn number-style">9</div>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="x" class="col btn number-style gray"><img src="'+backBtn+'"></div>\
                                <div id="b0" class="col btn number-style">0</div>\
                                <div id="sure" class="col btn number-style gray">'+options.btn.sure+'</div>\
                            </div>\
                        </div>\
                    </div>\
                </div>');
    };
}

/*小鍵盤動作*/
$(function(){
    
    $(options.dom.caller).each(function(i,e){
        var $modalNum = options.dom.keyidName+i;                        //小鍵盤本體
        var $modalAddId = "#" + $modalNum;                              //小鍵盤本體(加id符號)
        var $that = $(this);
        
        //自動處理input
            /*自動將輸入區域type改為text，才能即時顯示小數點，否則number時，如「56.」會顯示空白*/
            /*readonly 使用原因：防止預設鍵盤跳出*/
        var $type = $that.attr("type");
        switch($type) {
            case "number":
                $(this).attr("type","text");
                break;
            case "tel":
                $(this).attr("type","text");
                break;
            case "password":
                //生成明暗碼按鈕
                //changeBtn($(this));
                break;
            case "text":
                break;
        }
        $that
            .attr("data-toggle","modal")
            .attr("data-target",$modalAddId)
            .attr("readonly","readonly");
        
        $that.on("click",function(e){
            var $dom = $(this);
            /* 判斷生成哪個鍵盤 */
            var hasDian = $dom.hasClass('dian');                        //回傳true or false
            hasDianClass(hasDian,$modalNum);                            //判斷生成有無小數點鍵盤
            var $domEle = $($modalAddId).find("#passport");             //暫存區
            
            //將原先輸入框裡的值帶入鍵盤的暫存區
            if ($dom.val()!=''){
                $domEle.val($dom.val());
            }else{
                $domEle.val('');
            }
            
            e.stopPropagation;
            e.preventDefault;
            
            //計算
            Calculation($that,$domEle);
            
        })
        
    })
})
