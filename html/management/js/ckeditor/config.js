/*
   Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
   For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config )
{
    config.width = '700'; 
    config.resize_enabled = false; 
    config.linkShowAdvancedTab = false,
    config.linkShowTargetTab = true
    config.colorButton_enableMore = false;
    config.toolbar = 
    [
        ['TextColor', 'BGColor'],
        ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript'],
        ['NumberedList', 'BulletedList', 'Outdent', 'Indent'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        '/',
        ['Undo', 'Redo'],
        ['Cut', 'Copy', 'Paste', 'Find', 'Replace'],
        ['Link', 'Unlink', 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar'],
        ['Maximize', 'Preview'],
        '/',
        ['FontSize', 'Styles', 'Format'],
        ['Source']
    ]; /*'Font',*/

config.filebrowserBrowseUrl = 'ckfinder/ckfinder.html';
config.filebrowserImageBrowseUrl = 'ckfinder/ckfinder.html?Type=Images';
config.filebrowserFlashBrowseUrl = 'ckfinder/ckfinder.html?Type=Flash';
// config.filebrowserUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files'; //可上傳一般檔案
// config.filebrowserFlashUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash';//可上傳Flash檔案 
config.filebrowserImageUploadUrl = 'ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images';//可上傳圖檔
};
