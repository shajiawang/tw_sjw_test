﻿<?php
	require_once '/var/www/html/management/phpmodule/include/config.ini.php';
?>
    <!DOCTYPE html>
    <html>

    <head>
        <title>Main Frame</title>
        <meta charset="UTF-8">
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
<!--        <script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>-->
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
        <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery.Validate/1.6/localization/messages_tw.js"></script>
<!--        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/le-frog/jquery-ui.css" type="text/css">-->

    </head>

    <body>
        <style>
            label {
                font-weight: bold;
                border: 0;
                font-size: 14px;
                vertical-align: baseline;
            }

            label.error {
                color: red;
            }
            .border1 {
                border: 1px solid #2193b0;
            }
            .bb-radius {
                border-bottom-left-radius: .7rem;
                border-bottom-right-radius: .7rem;
            }
            .bt-radius {
                border-top-left-radius: .7rem;
                border-top-right-radius: .7rem;
            }
            #LoginConfirm {
                background: #2193b0;  /* fallback for old browsers */
                background: -webkit-linear-gradient(to bottom, #6dd5ed, #2193b0);  /* Chrome 10-25, Safari 5.1-6 */
                background: linear-gradient(to bottom, #6dd5ed, #2193b0); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
                background: linear-gradient(to bottom, #6dd5ed, #2193b0); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
                border: 0;
                padding: .5rem 2rem;
                border-radius: .5rem;
                cursor: pointer;
                outline: none;
                font-weight: 600;
                color: #FFF;
                text-shadow: 0 0 5px rgba(0, 0, 0, .8);
            }
            .bg-teal {
                background: #2193b0;
            }
        </style>
        <script language="javascript" type="text/javascript">
            //<!--
            $(document).ready(function() {
//                $("button, input:button, input:submit").button();                 //jquery UI
//                $("#LoginForm").validate({
//                    submitHandler: function(form) {
//                        $("button, input:button, input:submit").button({
//                            disabled: true
//                        });
//                        form.submit();
//                    }
//                });
                $("#LoginConfirm").click(function() {
                    $("#LoginForm").submit();
                });
                $("#Reset").click(function() {
                    $("#LoginForm").each(function() {
                        this.reset();
                    });
                    $("#LoginForm").validate().resetForm();
                    $("input,select").removeClass("ui-state-error");
                });
            });
            //-->

        </script>
        <form action="/management/user_login.php" method="post" name="LoginForm" id="LoginForm">
            <table class="ui-widget" align="center">
                <tbody>
                    <tr>
                        <td valign="top" width="250">
                            <table border="0" cellspacing="0" cellpadding="0" align="right">
                                <tbody>
                                    <tr>
                                        <td height="274">
                                            <table border="0" cellspacing="0" cellpadding="0" width="234">
                                                <tbody>
                                                    <tr>
                                                        <td height="40" class="bg-teal border1 bt-radius">
                                                            <div align="center"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td class="border1 bb-radius" height="180">
                                                            <table border="0" cellspacing="0" cellpadding="5" align="center">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="bottom"><label for="name">商家管理帳號：</label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:215px;" class="ui-state-error-text"><input type="text" name="name" id="name" class="required" style="width:210px;" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td valign="bottom"><label for="passwd">商家管理密碼：</label></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="width:215px;" class="ui-state-error-text"><input type="password" name="passwd" id="passwd" class="required" style="width:210px;" minlength="4" maxlength="16" /></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="center">
                                                                            <input type="button" id="LoginConfirm" class="fg-white" name="LoginConfirm" alt="登入" title="登入" value="登入" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                        <td valign="top" width="30"></td>
                        <!--td valign="top" width="723">
        			<table border="0" cellspacing="0" cellpadding="0" width="440">
        				<tbody>
        					<tr><td height="15"></td></tr>
        					<tr><td><img width="640" height="320" alt="形象" title="形象" src="/admin/images/admin/banner-1.jpg" /></td></tr>
        				</tbody>
        			</table>
        			</td-->
                    </tr>
                </tbody>
            </table>
        </form>
    </body>

    </html>
