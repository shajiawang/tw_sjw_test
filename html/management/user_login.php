<?php
require_once '/var/www/html/management/phpmodule/include/config.ini.php';
require_once '/var/www/html/management/libs/admin.inc.php';

session_start();
$io = new intputOutput();

// Check Variable Start
if (empty($io->input['post']["name"]) ) {
	jsAlertMsg('登錄帳號錯誤!!');
}
if (empty($io->input['post']["passwd"]) ) {
	jsAlertMsg('登錄密碼錯誤!!');
}
// Check Variable End
require_once "saja/mysql.ini.php";

$model = new mysql($config["db"][0]);
$model->connect();
$db_user = $config["db"][0]["dbname"];
//print_R($db_user);exit;
require_once "saja/convertString.ini.php";
$str = new convertString();

##############################################################################################################################################
// Table Start
$query = "SELECT * FROM `{$db_user}`.`{$config['default_prefix']}enterprise` 
WHERE 
	prefixid = '".$config['default_prefix_id']."' 
	AND loginname = '".$io->input['post']["name"]."' 
	AND switch = 'Y' 
";
$table = $model->getQueryRecord($query);

if (empty($table['table']['record'])) {
	jsAlertMsg('登錄帳號不存在!!');
	die();
}
$enterprise = $table['table']['record'][0];
$passwd = $str->strEncode($io->input['post']['passwd'], $config['encode_key']);

if ($enterprise['passwd'] !== $passwd ) {
	jsAlertMsg('登錄密碼錯誤!!');
}

// Table End 
##############################################################################################################################################

$_SESSION['sajamanagement']['enterprise'] = $enterprise; 

setcookie('enterpriseid',$enterprise['enterpriseid'],time() + (86400),"/",COOKIE_DOMAIN); //set cookie for 1 day

header("location:".$config['default_main']);
