<?php

$debug_mod   = "Y";
//$host_name   = "localhost";
$host_name   = "sajadb01:3306";
$protocol    = "http";
$domain_name = 'test.shajiawang.com';

define("BASE_URL","http://". $domain_name);
define("COOKIE_DOMAIN", $domain_name);
define("APP_DIR", "/management");
define("IMAGES_DIR", APP_DIR."/images");
define("HEADIMGS",  IMAGES_DIR."/headimgs");

############################################################################
# defient
############################################################################
$config["project_id"]             = "saja"; 
$config["default_main"]           = "/management"; 
$config["default_charset"]        = "UTF-8";
$config["sql_charset"]            = "utf8";
$config["default_prefix"]         = "saja_" ;  			// prefix
$config["default_prefix_id"]      = "saja" ;		// prefixid
$config["cookie_path"]            = "/";  
$config["cookie_domain"]          = "";  
$config["tpl_type"]               = "php"; 
$config["module_type"]            = "php";	  		// or xml
$config["fix_time_zone"]          = -8 ;                          // modify time zone 
$config["default_time_zone"]      = 8 ;                           // default time zone 
$config["max_page"]               = 20 ;
$config["max_range"]              = 10 ;
$config["encode_type"]            = "crypt" ; 			// crypt or md5
$config["encode_key"]             = "%^$#@%S_d_+!J_j=" ; 			// crypt encode key
$config["session_time"]           = 15	; 			// Session time out
$config["default_lang"]           = "zh_TW"	; 			// en , tc
$config["default_template"]       = "default"	; 		// 
$config["default_topn"]           = 10	; 			// default top n
$config["debug_mod"]              = $debug_mod ; 			// enable all the debug console , N : disable , Y : enable 
$config["max_upload_file_size"]   = 800000000 ; 			//unit is k
$config["expire"]                 = "60" ; 			// memcace expire time . sec
$config["domain_name"]            = $domain_name;
$config["protocol"]               = $protocol;
$config["admin_email"]            = array('pc028771@gmail.com') ; 
$config["currency_email"]         = array('pc028771@gmail.com') ; 
$config["transaction_time_limit"] = 5; //The minimized time between two transaction
############################################################################
# FaceBook
############################################################################
$config['fb']['appid' ]  = "";
$config['fb']['secret']  = "";


############################################################################
# path
############################################################################
$config["path_admin"]           = "/var/www/html/management/phpmodule";
$config["path_class"]           = $config["path_admin"]."/class" ; 
$config["path_function"]        = $config["path_admin"]."/function" ; 
$config["path_include"]         = $config["path_admin"]."/include" ; 
$config["path_bin"]             = $config["path_admin"]."/bin" ; 
$config["path_data"]            = $config["path_admin"]."/data/" ; 
$config["path_cache"]           = $config["path_admin"]."/data/cache" ; 
$config["path_sources"]         = $config["path_admin"]."/sources/".$config["module_type"] ; 
$config["path_style"]           = $config["path_admin"]."/template/".$config["tpl_type"] ; 
$config["path_language"]        = $config["path_admin"]."/language" ; 
$config["path_images"]          = dirname($config["path_admin"])."/images" ; 
//$config["path_site_products_images"] = dirname(dirname($config["path_admin"]))."/images/products" ; 
$config["path_products_images"] = "/var/www/html/site/images/site/product" ; 
$config["path_ad_images"]       = "/var/www/html/site/images/site/ad" ; 
$config["path_upload_images"]   = "/var/www/html/site/images/site/upload" ; 
$config["path_admins"]          = $config["path_admin"]."/admins/" ; 
$config["path_javascript"]      = $config["path_admin"]."/javascript/" ; 
$config["path_pdf_template"]    = $config["path_class"]."/tcpdf/template/" ; 
$config["path_eamil_api"]       = "/usr/bin/" ;  // email api path 

############################################################################
# sql Shaun 
############################################################################
$config["db"][0]["charset"]  = "utf8" ;
$config["db"][0]["host"]     = $host_name ;
$config["db"][0]["type"]     = "mysql";
$config["db"][0]["username"] = "saja"; 
$config["db"][0]["password"] = 'saja#333' ;
#$config["db_port"][0]       = "/var/lib/mysql/mysql.sock";

$config["db"][0]["dbname"]   = "saja_user" ;
$config["db"][1]["dbname"]   = "saja_cash_flow" ;
$config["db"][2]["dbname"]   = "saja_channel" ;
$config["db"][3]["dbname"]   = "saja_exchange" ;
$config["db"][4]["dbname"]   = "saja_shop" ;

define("PASSWORD_SALT", $config["encode_key"]);

####### socket ########
$config['wss_url']="ws://ws.shajiawang.com:443/";
