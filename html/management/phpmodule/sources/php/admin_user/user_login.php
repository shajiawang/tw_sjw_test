<?php
// Check Variable Start
if (empty($this->io->input['post']["name"]) ) {
	$this->jsAlertMsg('登錄帳號錯誤!!');
}
if (empty($this->io->input['post']["passwd"]) ) {
	$this->jsAlertMsg('登錄密碼錯誤!!');
}
// Check Variable End

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]["dbname"];

require_once "saja/convertString.ini.php";
$this->str = new convertString();

##############################################################################################################################################
// Table Start

$query ="
SELECT u.*
FROM `{$db_user}`.`{$this->config->default_prefix}enterprise` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND loginname = '".$this->io->input['post']["name"]."' 
	AND switch = 'Y' 
";
$table = $this->model->getQueryRecord($query);

if (empty($table['table']['record'])) {
	$this->jsAlertMsg('登錄帳號不存在!!');
	die();
}
$user = $table['table']['record'][0];
	
$passwd = $this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key);

if ($user['passwd'] !== $passwd ) {
	$this->jsAlertMsg('登錄密碼錯誤!!');
}

// Table End 
##############################################################################################################################################

$_SESSION['user'] = $user; 

if (!empty($this->io->input['get']['location_url'])) {
	header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));
}
else {
	header("location:".$this->config->default_main .'/onload/default');
}
