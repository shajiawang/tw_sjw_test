<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/main.css" type="text/css">
	    <style type="text/css">
		.table-wrapper {
		    padding: 16px 0;
		}
		.table {
		    margin: 0 23px;
		    list-style: none;
		    padding: 0;
		}
		.table .header {
			height: 30px;
			border: 1px solid #67982d;
			border-bottom: 0;
			background: #d4e6c9;
			background: -moz-linear-gradient(top,  #d4e6c9 0%, #acce95 50%, #98c37b 50%, #9ec682 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#d4e6c9), color-stop(50%,#acce95), color-stop(50%,#98c37b), color-stop(100%,#9ec682));
			background: -webkit-linear-gradient(top,  #d4e6c9 0%,#acce95 50%,#98c37b 50%,#9ec682 100%);
			background: -o-linear-gradient(top,  #d4e6c9 0%,#acce95 50%,#98c37b 50%,#9ec682 100%);
			background: -ms-linear-gradient(top,  #d4e6c9 0%,#acce95 50%,#98c37b 50%,#9ec682 100%);
			background: linear-gradient(top,  #d4e6c9 0%,#acce95 50%,#98c37b 50%,#9ec682 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#d4e6c9', endColorstr='#9ec682',GradientType=0 );
		}
		.table .body table {
			border-top: 1px solid #67982d;
			border-left: 1px solid #67982d;
			margin: 0;
		}
		.table .body th,
		.table .body td {
			border-bottom: 1px solid #67982d;
			border-right: 1px solid #67982d;
			text-align: center;
		}
		.table .body th {
			padding: 0;
			height: 25px;
			color: #4b6d23;
			line-height: 25px;
			background: #c4e2a1;
			background: -moz-linear-gradient(top,  #c4e2a1 0%, #def1c7 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#c4e2a1), color-stop(100%,#def1c7));
			background: -webkit-linear-gradient(top,  #c4e2a1 0%,#def1c7 100%);
			background: -o-linear-gradient(top,  #c4e2a1 0%,#def1c7 100%);
			background: -ms-linear-gradient(top,  #c4e2a1 0%,#def1c7 100%);
			background: linear-gradient(top,  #c4e2a1 0%,#def1c7 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c4e2a1', endColorstr='#def1c7',GradientType=0 );
			-webkit-user-select : none;
			-moz-user-select : none;
			user-select : none;
		}
		.table .body td {
			height: 20px;
			line-height: 20px;
		    padding: 0 10px;
		    background-color: #edf2ea;
		}
		.table .sortable {
			cursor: pointer;
			text-decoration: underline;
		}
		.table .sortable a {
			float: right;
			display: block;
			width: 11px;
			height: 22px;
			margin: 2px 3px 0 0 ;
			text-decoration: none;
		}
		.table .sortable .init {
			background-position: 0 0;
		}
		.table .sortable .asc {
			background-position: -11px 0;
		}
		.table .sortable .desc {
			background-position: -22px 0;
		}
		.table .icon {
			width: 4.5em;
			text-align: center;
		}
		.table .icon a {
			display: block;
			width: 18px;
			height: 18px;
			cursor: pointer;
			margin: 0 auto;
		}
		.table .icon-edit {
			background-position: -18px top;
		}
		.table .icon-delete {
			background-position: 0 top;
		}
		.table .icon-edit:hover {
			background-position: -18px bottom;
		}
		.table .icon-delete:hover {
			background-position: 0 bottom;
		}
		.table .footer {
			text-align: center;
			padding: 5px 0;
		}
		.table .footer ul {
			padding: 0;
			list-style: none;
			float: right;
		}
		.table .footer li {
			height: 25px;
			float: left;
			color: #4b6d23;
		}
		.table .footer .splitter {
			height: 10px;
			margin: 3px;
			border-right: 1px solid #4b6d23;
		}
		.table .footer a {
			text-decoration: none;
			color: #4b6d23;
		}
		.table .footer a:hover {
			text-decoration: underline;
		}

		.form {
			margin: 23px;
			background-color: #edf2ea;
			border: 1px solid #85b151;
			padding: 10px;
			color: #4b6d23;
			display: none;
		}
		.form .form-label {
			font-weight: bold;
			margin-bottom: 5px;
		}
		.form .field {
			height: 30px;
		}
		.form .field input {
			height: 19px;
		}
		.form .field label {
			font-weight: normal;
		}
		.form .functions {
			padding-top: 10px;
		}
		.form .button {
			float: left;
			margin-right: 10px;
		}
	    </style>
		<!-- script src="http://code.jquery.com/jquery-1.10.1.min.js"></script -->
		<script src="<?php echo $this->config->default_main; ?>/js/jquery.min.js"></script>
		<script type="text/javascript">
		(function($){
			$.fn.bindFormAction = function() {
				return this.each(function() {
					var $this = $(this);

					if ($this.is('form')) {
						$this.find('.button.cancel').click(function(){
							$this.slideUp();
						});
					}
				});
			};
		})(jQuery);

		jQuery(document).ready(function($) {
			$('#left-control').click(function() {
				$(this).toggleClass('expanded collapsed');
				window.parent.toggleLeft();
			});
			$('.sortable').click(function() {
				$(this).children('a').toggleClass($(this).has('.init').length?'init asc':'asc desc');
			});
			$('.icon-edit').click(function(){
				var form = $('#form-edit').slideDown();

				$(this).parents('tr').children('.column').each(function() {
					form.find('input[name='+$(this).attr('name')+']').val($(this).text());
				});
			});
			$('#form-edit,#form-add').bindFormAction();
		});
		</script>
	</head>
	<body>
		殺價王商家管理系統
	</body>
</html>