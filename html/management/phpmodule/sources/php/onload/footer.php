<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Footer Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
		<style type="text/css">
		.footer {
			color: white;
			height: 20px;
			line-height: 20px;
			text-align: right;
            
/*
			background: #8abb46;
			background: -moz-linear-gradient(left,  #8abb46 0%, #538e0e 100%);
			background: -webkit-gradient(linear, left top, right top, color-stop(0%,#8abb46), color-stop(100%,#538e0e));
			background: -webkit-linear-gradient(left,  #8abb46 0%,#538e0e 100%);
			background: -o-linear-gradient(left,  #8abb46 0%,#538e0e 100%);
			background: -ms-linear-gradient(left,  #8abb46 0%,#538e0e 100%);
			background: linear-gradient(left,  #8abb46 0%,#538e0e 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#8abb46', endColorstr='#538e0e',GradientType=1 );
*/
            
            background: #2193b0;  /* fallback for old browsers */
            background: -webkit-linear-gradient(to right, #6dd5ed, #2193b0);  /* Chrome 10-25, Safari 5.1-6 */
            background: linear-gradient(to right, #6dd5ed, #2193b0); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */

		}
		</style>
	</head>
<body>
	<div class="footer">Copyright &copy; 2012 殺價王股份有限公司 All Rights Reserved.</div>
</body>
</html>


