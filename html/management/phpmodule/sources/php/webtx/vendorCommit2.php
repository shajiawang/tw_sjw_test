<?php

include_once "saja/mysql.ini.php";
include_once("/var/www/html/site/lib/helpers.php");
//include_once "websocket/WebsocketClient.php";
include_once("/var/www/html/site/lib/vendor/autoload.php"); // 載入wss client 套件(composer)

// functions
/*
function notify($message) {

        $ws = new Client($config['wss_url']);
        try {
          $ws->send($message,'text');
		} catch (Exception $e) {
		    error_log($e->getMessage());
		}
}
*/

function replyAndExit($retArr) {
	   $json_str=json_encode($retArr);
	   error_log("[vendorCommit2]: ".$json_str);
	   if ($_POST['json'] != 'Y'){
		   // echo $retArr['retMsg'];
		   echo "<script>alert('".$retArr['retMsg']."');location.href = '".BASE_URL."/site/enterprise/';</script>";
		   exit;
	   } else {
		   	$ret['retCode']=$retArr['retCode'];
			$ret['retMsg']=$retArr['retMsg'];
			$ret['retType']='MSG';
			echo json_encode($ret);
			exit; 
	   }
}
	
function getSafeStr($str) {
		 $safeStr=addslashes($str);
		 if($safeStr!=$str) {
			error_log("[getSafeStr] ".$str."-->".$safeStr);
		 }
		 return $safeStr;
}

// 查詢交易資料
function getQrcodeTxRecord($arrCond,$db) {
		global $config, $model;
	if(count($arrCond)>0) {
	   $query=" SELECT evr.*, sum(IFNULL(b.amount,0)) as curr_bonus ";
	   $query.=" FROM saja_user.saja_user_profile up ";
	   $query.=" LEFT JOIN saja_cash_flow.saja_bonus b ";
	   $query.=" ON up.switch='Y' and b.switch='Y'  AND up.userid=b.userid ";
	   $query.=" LEFT JOIN saja_exchange.saja_exchange_vendor_record evr ";
	   $query.=" ON up.switch='Y' and evr.switch='Y' AND up.userid=evr.userid ";
	   $query.=" WHERE 1=1 ";

	   foreach($arrCond as $key => $value) {
		  $query.=(" AND ".$key."='".addslashes($value)."'");
	   }

	   error_log("[vendorCommit2] ".$query);
	   $table = $db->getQueryRecord($query);
           // error_log("[vendorCommit2]: evrid : ".$table['table']['record'][0]['evrid']);
	   if(!empty($table['table']['record'][0]['evrid'])) {
		  return $table['table']['record'][0];
	   }
   }
   return false;
}


// 取得商家detail
function getEnterpriseDetail($vendorid,$db) {
	global $config, $model;

	if(count($vendorid)>0) {
	   $query = "SELECT * , st.name as marketingname
				FROM `saja_user`.`saja_enterprise_profile` p
				JOIN `saja_user`.`saja_enterprise_shop` s
				  ON p.enterpriseid = s.enterpriseid
				JOIN `saja_user`.`saja_enterprise` e
				  ON p.enterpriseid = e.enterpriseid
		   LEFT OUTER JOIN `saja_exchange`.`saja_exchange_store` st
				  ON e.esid = st.esid
			   WHERE p.prefixid = 'saja'
			     AND p.enterpriseid = '".$vendorid."'
			     AND p.switch = 'Y'
				 AND s.switch = 'Y'
				 AND e.switch = 'Y'
				 AND st.switch = 'Y'
				 ";
	   /*
	   $query=" SELECT thumbnail_url, thumbnail_file ";
	   $query.=" FROM saja_user.saja_enterprise_profile ";
	   $query.=" WHERE enterpriseid=".$vendorid;
       */
	   error_log("[vendorCommit2] ".$query);
	   $table = $db->getQueryRecord($query);
	   if(!empty($table['table']['record'][0])) {
		  return $table['table']['record'][0];
	   }
   }
   return false;
}

// 修改Qrcode交易資料
function updQrcodeTxRecord($arrUpd,$arrCond,$db) {
	       global $config, $model;
		   if(empty($arrUpd)) {
		      return false;
		   }
		   $ret=false;
		   $sql=" UPDATE saja_exchange.saja_exchange_vendor_record SET modifyt=NOW() ";
		   foreach($arrUpd as $key => $value) {
		       $sql.=(" ,".$key."='".addslashes($value)."'");
		   }

		   $sql.=" WHERE 1=1 ";
		   if(count($arrCond)>0) {
			   foreach($arrCond as $key => $value) {
				   $sql.=(" AND ".$key."='".addslashes($value)."'");
			   }
		   }
		   error_log("[vendorCommit2] ".$sql);
		   $ret = $db->query($sql);
		   return $ret;
}

//產生紅利交易紀錄
$cdnTime = date("YmdHis");

// $WSClient=new Client($this->config->wss_url);
// 接收傳入資料
$tx_code_md5 	= empty($this->io->input['post']['tx_code_md5']) ? $_POST['tx_code_md5'] : $this->io->input['post']['tx_code_md5'];
$evrid		 	= empty($this->io->input['post']['evrid']) ? $_POST['evrid'] : $this->io->input['post']['evrid'];
$userid		 	= empty($this->io->input['post']['userid']) ? $_POST['userid'] : $this->io->input['post']['userid'];
$vendor_name	= empty($this->io->input['post']['vendor_name']) ? $_POST['vendor_name'] : $this->io->input['post']['vendor_name'];
$vendorid		= empty($this->io->input['post']['vendorid']) ? $_POST['vendorid'] : $this->io->input['post']['vendorid'];
$prod_name		= empty($this->io->input['post']['prod_name']) ? $_POST['prod_name'] : $this->io->input['post']['prod_name'];
$tx_quantity	= empty($this->io->input['post']['tx_quantity']) ? $_POST['tx_quantity'] : $this->io->input['post']['tx_quantity'];
$tx_currency	= empty($this->io->input['post']['tx_currency']) ? $_POST['tx_currency'] : $this->io->input['post']['tx_currency'];
$total_bonus	= empty($this->io->input['post']['total_price']) ? $_POST['total_price'] : $this->io->input['post']['total_price'];
$total_price	= empty($this->io->input['post']['total_price']) ? $_POST['total_price'] : $this->io->input['post']['total_price'];
$tx_type		= empty($this->io->input['post']['tx_type']) ? $_POST['tx_type'] : $this->io->input['post']['tx_type'];
$keyin_ts		= empty($this->io->input['post']['keyin_time']) ? strtotime($_POST['keyin_time']) : strtotime($this->io->input['post']['keyin_time']);
$tx_status		= empty($this->io->input['post']['tx_status']) ? $_POST['tx_status'] : $this->io->input['post']['tx_status'];

$now_ts=time();
/*
  if(empty($evrid)) {
      $evrid=$_POST['evrid'];
	  if(empty($evrid)) {
		  $retArr['retCode']='-1';
		  $retArr['retMsg']=('交易序号錯誤 !!');
		  replyAndExit($retArr);
	  }
  }
*/

$etprs_id=empty($_SESSION['sajamanagement']['enterprise']['enterpriseid']) ? $_REQUEST['enterpriseid'] : $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
error_log("[vendorCommit2] session Enterprise id :".$etprs_id);
enterprise_login_required();
/*if(empty($etprs_id)) {
	if ($_POST['json'] != 'Y'){
		echo '<!DOCTYPE html>';
		echo '<html>';
		echo '<body>';
		echo '<form name="loginto" id="loginto" action="/site/enterprise/loginto" method="post">';
		echo '<input type="hidden" name="_tourl" value="/management/webtx/vendorCommit2" />';
		if (is_array($_REQUEST)) {
			foreach ($_REQUEST as $key => $value) {
				echo '<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
			}
		}
		echo '</form>';
		echo '<script>document.forms.loginto.submit();</script>';
		echo '</body>';
		echo '</html>';
		exit;
	} else {
		$ret['retCode']=10003;
		$ret['retMsg']='請先登入商家號 !!';
		$ret['retType']='MSG';
		echo json_encode($ret);
		exit;
	}
}*/

error_log("[vendorCommit2] evrid : ".$evrid);
error_log("[vendorCommit2] tx_code_md5 : ".$tx_code_md5);

// 連接資料庫
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$retArr=array();
$retArr['evrid']=$evrid;

$arrCond=array();
$arrCond['evrid']=$evrid;

$arrUpd=array();

if($tx_status=='-2') {
    $arrUpd['tx_status']=$tx_status;
    $arrUpd['keyin_time']=$cdnTime;
    $retCode=updQrcodeTxRecord($arrUpd,$arrCond,$this->model);
    if($retCode) {
       $retArr['retCode']='-2';
       $retArr['retMsg']=('賣方取消本次交易 !!');
       // $WSClient->send("NTFY|".$evrid."|".$tx_status);
		if($_REQUEST['json'] == 'Y') {
			$ret['retCode'] = 10001;
			$ret['retMsg'] = '賣方取消本次交易 !!';
			$ret['retType'] = 'MSG';
			echo json_encode($ret);
			exit;
		}else{
			replyAndExit($retArr);
		}
    }
}
error_log("[vendorCommit2] keyin_ts : ".$keyin_ts);
// 判斷交易時效
if(time()-$keyin_ts>180) {
  $retArr['retCode']='-102';
  $retArr['retMsg']=('商家確認時效已過 !!');
	//if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10002;
		$ret['retMsg'] = '商家確認時效已過 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	/*}else{
		replyAndExit($retArr);
	}*/
}
// 判斷交易驗證碼
if(empty($tx_code_md5)) {
  $retArr['retCode']='-2';
  $retArr['retMsg']=('交易驗證碼錯誤 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10004;
		$ret['retMsg'] = '交易驗證碼錯誤 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
}

$record=getQrcodeTxRecord($arrCond,$this->model);
error_log("[vendorCommit2] vendorid : ".json_encode($record['vendorid']));

$enterprise=getEnterpriseDetail($record['vendorid'],$this->model);

// error_log("[vendorCommit2] pic : ".json_encode($enterprise));

if(!empty($enterprise['thumbnail_file']))
  $thumbnail=HEADIMGS."/".$enterprise['thumbnail_file'];
else
  $thumbnail=$enterprise['thumbnail_url'];

// 判斷交易編號是否存在
if(empty($record['evrid'])) {
  $retArr['retCode']='-100';
  $retArr['retMsg']=('交易數據錯誤 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10005;
		$ret['retMsg'] = '交易數據錯誤 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
}

// 判斷商家名稱是否有值
if(!empty($vendor_name)) {
  $arrUpd['vendor_name']=$vendor_name;
} else {
  $retArr['retCode']='-4';
  $retArr['retMsg']=('商家名稱錯誤 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10006;
		$ret['retMsg'] = '商家名稱錯誤 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
}

// 判斷商家編號是否有值
if(!empty($vendorid)) {
  $arrUpd['vendorid']=$vendorid;
} else {
  $retArr['retCode']='-5';
  $retArr['retMsg']=('商家id錯誤 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10007;
		$ret['retMsg'] = '商家id錯誤 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
}

// 判斷交易類別是否有值
if(!empty($tx_type)) {
  $arrUpd['tx_type']=$tx_type;
} else {
  $retArr['retCode']='-11';
  $retArr['retMsg']=('交易類别錯誤 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10008;
		$ret['retMsg'] = '交易類别錯誤 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
}

// 判斷商品名稱是否有值
if(!empty($prod_name)) {
  $arrUpd['prod_name']=$prod_name;
} else {
  $retArr['retCode']='-4';
  $retArr['retMsg']=('商品名稱錯誤 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10009;
		$ret['retMsg'] = '商品名稱錯誤 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
}

/*
//交易幣別
if(!empty($tx_currency)) {
  $arrUpd['tx_currency']=$tx_currency;
} else {
  $retArr['retCode']='-103';
  $retArr['retMsg']=('交易幣別缺誤 !!');
  replyAndExit($retArr);
}
*/

// 判斷商品交易數量是否有值
if(!empty($tx_quantity)) {
  if(is_numeric($tx_quantity) && $tx_quantity>0) {
	 $arrUpd['tx_quantity']=$tx_quantity;
  } else {
	 $retArr['retCode']='-110';
	 $retArr['retMsg']=('交易數量格式異常 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10010;
		$ret['retMsg'] = '交易數量格式異常 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
  }
}

// 判斷所需鯊魚點點數是否有值
if(!empty($total_price)) {
  if(is_numeric($total_price) && $total_price>0) {
 	 $arrUpd['total_bonus']=$total_bonus;
	 $arrUpd['total_price']=$total_price;
  } else {
	 $retArr['retCode']='-110';
	 $retArr['retMsg']=('鯊魚點數格式異常 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10011;
		$ret['retMsg'] = '鯊魚點數格式異常 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
  }
} else {
  $retArr['retCode']='-9';
  $retArr['retMsg']=('所需鯊魚點數錯誤 !!');
	if($_REQUEST['json'] == 'Y') {
		$ret['retCode'] = 10012;
		$ret['retMsg'] = '所需鯊魚點數錯誤 !!';
		$ret['retType'] = 'MSG';
		echo json_encode($ret);
		exit;
	}else{
		replyAndExit($retArr);
	}
}

// 驗證通過, 修改交易資料
$arrUpd['keyin_time']=date("YmdHis");
$arrUpd['tx_status']='3';
$retCode=updQrcodeTxRecord($arrUpd,$arrCond,$this->model);

/*
if($retCode) {
   $WSClient->send("NTFY|".$evrid."|3");
}
*/

$txArr=array();
$txArr['evrid']=$evrid;
$txArr['userid']=$userid;
$txArr['vendorid']=$vendorid;
$txArr['tx_code_md5']=$tx_code_md5;
$txArr['keyin_time']=$arrUpd['keyin_time'];
$txArr['expired_ts']=strtotime($arrUpd['keyin_time'])+180;
$txArr['vendor_name']=$vendor_name;
$txArr['total_bonus']=$total_bonus;
$txArr['total_price']=$total_price;
$txArr['thumbnail']=$thumbnail;
	$ts = time();
	$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
	$client = new WebSocket\Client($wss_url);
	$ary_bidder = array('actid'		=> 'B2C_Wait_Approve',
						'evrid' 		=> $evrid,
						'productid' => "{$product['productid']}",
						'ts'		=> $ts,
						'sign'		=> MD5($ts."|sjW333-_@")
				);
	$client->send(json_encode($ary_bidder));

	$ret['retCode'] = 1;
	$ret['retMsg'] = '等待客戶確認金額中';
	echo json_encode($ret);
	die();
/*
if($_REQUEST['json'] == 'Y') {
	$ret['retCode'] = 1;
	$ret['retMsg'] = '等待客戶確認金額中';
	//$ret['type'] = 'MSG';
	// $ret['data'] = $txArr;
	echo json_encode($ret);
	error_log(439);
	exit;
}else{
	error_log(441);

	echo json_encode($txArr);
}*/