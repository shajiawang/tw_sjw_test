<?php
include_once("saja/mysql.ini.php");
include_once("vendor/autoload.php");

$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");

use WebSocket\Client;

function sendNotify($arrMsg='',$ws_url='') {
       	if(empty($arrMsg))
		   return;
		try {
		  // Socket通知
		  if(empty($ws_url)) 
		     $ws_url='wss://53152801.qcloud.la:4433/';
	      $client = new Client($ws_url);
		  error_log("[ajax/product] socket notify :".json_encode($arrMsg));
		  $client->send(json_encode($arrMsg));         
		} catch (Exception $e) {
		    error_log($e->getMessage());
		}
}

function replyAndExit($retArr) {
	   $json_str=json_encode($retArr);
	   error_log($json_str);
	   echo $retArr['retMsg'];
	   exit;
}
	
function getSafeStr($str) {
		 $safeStr=addslashes($str);
		 if($safeStr!=$str) {
			error_log("[getSafeStr] ".$str."-->".$safeStr);
		 }
		 return $safeStr;
}
	
function getQrcodeTxRecord($arrCond,$db) {
		
	if(count($arrCond)>0) {
	   $query=" SELECT evr.*, sum(IFNULL(b.amount,0)) as curr_bonus ";
	   $query.="  FROM saja_user.saja_user_profile up ";
	   $query.="  LEFT JOIN saja_cash_flow.saja_bonus b ";
	   $query.="    ON up.switch='Y' and b.switch='Y'  AND up.userid=b.userid ";
	   $query.="  LEFT JOIN saja_exchange.saja_exchange_vendor_record evr ";
	   $query.="    ON up.switch='Y' and evr.switch='Y' AND up.userid=evr.userid ";
	   $query.=" WHERE 1=1 ";
	   
	   foreach($arrCond as $key => $value) {
		  $query.=(" AND ".$key."='".addslashes($value)."'");
	   }
   
	   error_log("[vendorConfirm]".$query);
	   $table = $db->getQueryRecord($query);
	   
	   if(!empty($table['table']['record'][0]['evrid'])) {
		  return $table['table']['record'][0];
	   } 
   }
   return false;
}

// 修改Qrcode交易資料
function updQrcodeTxRecord($arrUpd,$arrCond,$db) {
	       		   
		   if(empty($arrUpd)) {
		      return false;
		   }
		   $ret=false;
		   $sql=" UPDATE saja_exchange.saja_exchange_vendor_record SET modifyt=NOW() ";
		   foreach($arrUpd as $key => $value) {
		       $sql.=(" ,".$key."='".addslashes($value)."'");
		   }
		   
		   $sql.=" WHERE 1=1 ";
		   if(count($arrCond)>0) {
			   foreach($arrCond as $key => $value) {
				   $sql.=(" AND ".$key."='".addslashes($value)."'");
			   }
		   }
		   error_log("[vendorConfirm] ".$sql);
		   $ret = $db->query($sql);
		   return $ret;
}

   $etprs_id=$_COOKIE['enterpriseid'];
   error_log("Enterprise id :".$etprs_id);
   
   if(empty($etprs_id)) {
		$html='<!DOCTYPE html><body>
		<form name="loginto" id="loginto" action="/site/enterprise/loginto" method="post">
		<input type="hidden" name="_tourl" value="/management/webtx/vendorConfirm" />';
		if (is_array($_REQUEST)) {
			foreach ($_REQUEST as $key => $value) {
				$html.='<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
			}
		}
		$html.='</form>
		<script>document.forms.loginto.submit();</script>
		</body>
		</html>';
		error_log("[vendorConfirm] html : ".$html);
		echo $html;
		exit;
   }
	
   // $tx_key=$_REQUEST['tx_key'];
   $tx_key=$_POST["tx_key"];	
   if($tx_key=='') {
      $tx_key=$_GET["tx_key"];
   }  
   $tx_key=base64_decode($tx_key);
   error_log("[vendorConfirm] tx_key : ".$tx_key);   
   
   $arrTxKey=explode("|",$tx_key);	   
   $evrid=$arrTxKey[0];
   $tx_code_md5=$arrTxKey[1];
		   
   error_log("[vendorConfirm] evrid : ".$evrid);

   $retArr=array();
	
   $retArr['retCode']=1;
			   
   if(empty($evrid)) {
	  $retArr['retCode']=-1;
	  $retArr['retMsg']=('交易序號缺误 !!');
	  replyAndExit($retArr);
   }
   
   $this->model = new mysql($this->config->db[0]);
   $this->model->connect();

   $arrCond=array();
   $arrCond['evrid']=$evrid;
   $record=getQrcodeTxRecord($arrCond,$this->model);
      
   if(empty($record['evrid'])) {
	  $retArr['retCode']=-100;
	  $retArr['retMsg']=('交易数据缺误 !!');
	  replyAndExit($retArr);
   }		   
   
   // 验证时间
   $keyin_ts = time();
   if($keyin_ts-strtotime($record['qrcode_time'])>180) {
	  $retArr['retCode']=-101;
	  $retArr['retMsg']=('User交易时效已过 !!');
	  replyAndExit($retArr);
	  exit;
   }
     
   // 交易码验证
   if($tx_code_md5!=md5($record['tx_code'])) {
	  $retArr['retCode']=-102;
	  $retArr['retMsg']=('交易码验证失败 !!');
	  replyAndExit($retArr);
   }
	  
   /*帶出商家資料
   $query = "SELECT * FROM `saja_user`.`saja_enterprise_profile` 
			  WHERE prefixid = 'saja' 
			  AND enterpriseid = '".$etprs_id."' 
			  AND switch = 'Y' ";
   */
	/*
     $query = "SELECT p.*, es.*
               	FROM `saja_user`.`saja_enterprise_profile` p
				JOIN `saja_user`.`saja_enterprise` es
				  ON p.enterpriseid = es.enterpriseid
				JOIN `saja_exchange`.`saja_exchange_store` est
				  ON es.esid = est.esid 
			   WHERE p.prefixid = 'saja' 
			     AND p.enterpriseid = '".$etprs_id."' 
			     AND p.switch = 'Y' 
				 AND es.switch='Y' ";
	*/
	$query = "SELECT *,est.name as exchange_store_name,est.name as name 
	               FROM `saja_user`.`saja_enterprise` e
		           JOIN `saja_user`.`saja_enterprise_profile` ep
				     ON e.enterpriseid=ep.enterpriseid
				   JOIN `saja_user`.`saja_enterprise_shop` es
				     ON e.enterpriseid=es.enterpriseid
				 LEFT OUTER JOIN `saja_exchange`.`saja_exchange_store` est
				     ON e.esid=est.esid				
		          WHERE e.prefixid = 'saja' 
			        AND e.enterpriseid = '${etprs_id}' 
			        AND e.switch = 'Y' 
		          LIMIT 1 ";
    
				 
   error_log($query);			  
   $result = $this->model->getQueryRecord($query);
   if(empty($result['table']['record'][0]['enterpriseid'])) {
	  $retArr['retCode']=-4;
	  $retArr['retMsg']=('商家資料缺誤 !!');
	  replyAndExit($retArr);
   }
   
   $enterprise=$result['table']['record'][0];
   
   error_log("[vendorConfirm] enterprise id : ".$enterprise['enterpriseid']);
   error_log("[vendorConfirm] enterprise marketing name : ".$enterprise['exchange_store_name']);
   
   // sendNotify via. socket
   
   
   $arrUpd=array();
   $arrUpd['vendorid']=$etprs_id;
   $arrUpd['tx_status']='2';
   $arrUpd['keyin_time']=$keyin_ts;
   $arrUpd['vendor_name']=$enterprise['exchange_store_name'];
   $retCode=updQrcodeTxRecord($arrUpd,$arrCond,$this->model);
   if($retCode==1) {
        $record=getQrcodeTxRecord(array("evrid"=>$evrid),$this->model);
		if($record)
		   sendNotify($record);
   }
   $txArr=array();
   $txArr['evrid']=$record['evrid'];
   $txArr['userid']=$record['userid'];
   $txArr['vendorid']=$enterprise['enterpriseid'];
   $txArr['companyname']=$enterprise['companyname'];
   $txArr['vendor_name']=$enterprise['marketingname'];
   $txArr['tx_status']='2';
   $txArr['tx_currency']=$record['tx_currency'];
   $txArr['keyin_time']=$keyin_ts;
   $txArr['tx_code']=$record['tx_code'];
   $txArr['tx_code_md5']=md5($record['tx_code']);
   $txArr['ClientType']='app';
		   
$this->tplVar('txArr' , $txArr) ;

$this->display();


