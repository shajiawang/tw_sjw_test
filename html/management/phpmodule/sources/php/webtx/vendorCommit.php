<?php
include_once("saja/mysql.ini.php");
include_once("vendor/autoload.php");

use WebSocket\Client;

function sendNotify($arrMsg='',$ws_url='') {
       	if(empty($arrMsg))
		   return;
		try {
		  // Socket通知
		  if(empty($ws_url)) 
		     $ws_url='wss://53152801.qcloud.la:4433/';
	      $client = new Client($ws_url);
		  error_log("[ajax/product] socket notify :".json_encode($arrMsg));
		  $client->send(json_encode($arrMsg));         
		} catch (Exception $e) {
		    error_log($e->getMessage());
		}
}

/// functions
function replyAndExit($retArr) {
	   $json_str=json_encode($retArr);
	   error_log($json_str);
	   echo $retArr['retMsg'];
	   exit;
}
	
function getSafeStr($str) {
		 $safeStr=addslashes($str);
		 if($safeStr!=$str) {
			error_log("[getSafeStr] ".$str."-->".$safeStr);
		 }
		 return $safeStr;
}
	
// 查詢交易資料	
function getQrcodeTxRecord($arrCond,$db) {
		global $config, $model;
	if(count($arrCond)>0) {
	   $query=" SELECT evr.*, sum(IFNULL(b.amount,0)) as curr_bonus ";
	   $query.=" FROM saja_user.saja_user_profile up ";
	   $query.=" LEFT JOIN saja_cash_flow.saja_bonus b ";
	   $query.=" ON up.switch='Y' and b.switch='Y'  AND up.userid=b.userid ";
	   $query.=" LEFT JOIN saja_exchange.saja_exchange_vendor_record evr ";
	   $query.=" ON up.switch='Y' and evr.switch='Y' AND up.userid=evr.userid ";
	   $query.=" WHERE 1=1 ";
	   
	   foreach($arrCond as $key => $value) {
		  $query.=(" AND ".$key."='".addslashes($value)."'");
	   }
   
	   error_log("[vendorCommit] ".$query);
	   $table = $db->getQueryRecord($query);
	   
	   if(!empty($table['table']['record'][0]['evrid'])) {
		  return $table['table']['record'][0];
	   } 
   }
   return false;
}

// 修改Qrcode交易資料
function updQrcodeTxRecord($arrUpd,$arrCond,$db) {
	       global $config, $model;	   
		   if(empty($arrUpd)) {
		      return false;
		   }
		   $ret=false;
		   $sql=" UPDATE saja_exchange.saja_exchange_vendor_record SET modifyt=NOW() ";
		   foreach($arrUpd as $key => $value) {
		       $sql.=(" ,".$key."='".addslashes($value)."'");
		   }
		   
		   $sql.=" WHERE 1=1 ";
		   if(count($arrCond)>0) {
			   foreach($arrCond as $key => $value) {
				   $sql.=(" AND ".$key."='".addslashes($value)."'");
			   }
		   }
		   error_log("[vendorCommit] ".$sql);
		   $ret = $db->query($sql);
		   return $ret;
}

//產生紅利交易紀錄

$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");

$tx_code_md5=$this->io->input['post']['tx_code_md5'];
$evrid=$this->io->input['post']['evrid'];
$userid=$this->io->input['post']['userid'];
$vendor_name=$this->io->input['post']['vendor_name'];
$vendorid=$this->io->input['post']['vendorid'];
$prod_name=$this->io->input['post']['prod_name'];
$tx_quantity=$this->io->input['post']['tx_quantity'];
$tx_currency=$this->io->input['post']['tx_currency'];
$total_bonus=$this->io->input['post']['total_bonus'];
$tx_type=$this->io->input['post']['tx_type'];
$keyin_ts=$this->io->input['post']['keyin_time'];	  
$now_ts=time();

if($nowts-$keyin_ts>180) {
  $retArr['retCode']='-102';
  $retArr['retMsg']=('商家確認時效已過 !!');
  replyAndExit($retArr);
}
	   
$retArr=array();
if(empty($evrid)) {
  $retArr['retCode']='-1';
  $retArr['retMsg']=('交易序號缺誤 !!');
  replyAndExit($retArr);
} 
$retArr['evrid']=$evrid;
	   
if(empty($tx_code_md5)) {
  $retArr['retCode']='-2';
  $retArr['retMsg']=('交易驗證碼缺誤 !!');
  replyAndExit($retArr);
}

$arrCond=array();
$arrCond['evrid']=$evrid;
// $arrCond['tx_status']='2';

$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$record=getQrcodeTxRecord($arrCond,$this->model);

if(empty($record['evrid'])) {
  $retArr['retCode']='-100';
  $retArr['retMsg']=('交易資料缺誤 !!');
  replyAndExit($retArr);
}
//商家名稱
if(!empty($vendor_name)) {
  $arrUpd['vendor_name']=$vendor_name;
} else {
  $retArr['retCode']='-4';
  $retArr['retMsg']=('商家名稱缺誤 !!');
  replyAndExit($retArr);
}
if(!empty($vendorid)) {
  $arrUpd['vendorid']=$vendorid;
} else {
  $retArr['retCode']='-5';
  $retArr['retMsg']=('商家id缺誤 !!');
  replyAndExit($retArr);
}

// 交易類別
if(!empty($tx_type)) {
  $arrUpd['tx_type']=$tx_type;
} else {
  $retArr['retCode']='-11';
  $retArr['retMsg']=('交易類別缺誤 !!');
  replyAndExit($retArr);
}

//商品名稱
if(!empty($prod_name)) {
  $arrUpd['prod_name']=$prod_name;
} else {
  $retArr['retCode']='-102';
  $retArr['retMsg']=('商品名稱缺誤 !!');
  replyAndExit($retArr);
}

/*
//交易幣別
if(!empty($tx_currency)) {
  $arrUpd['tx_currency']=$tx_currency;
} else {
  $retArr['retCode']='-103';
  $retArr['retMsg']=('交易幣別缺誤 !!');
  replyAndExit($retArr);
}
*/

//所需紅利點數
if(!empty($total_bonus)) {
  if(is_numeric($total_bonus) && $total_bonus>0) {
	 $arrUpd['total_bonus']=$total_bonus;
	 $arrUpd['total_price']=$total_bonus;
  } else {
	 $retArr['retCode']='-110';
	 $retArr['retMsg']=('所需紅利點數格式異常 !!');
	 replyAndExit($retArr);
  }
} else {
  $retArr['retCode']='-9';
  $retArr['retMsg']=('所需紅利點數缺誤 !!');
  replyAndExit($retArr);
}

// 驗證通過, 修改交易資料
$arrUpd['keyin_time']=$now_ts;
$arrUpd['tx_status']='3';
$retCode=updQrcodeTxRecord($arrUpd,$arrCond,$this->model);

if($retCode==1) {
   $record=getQrcodeTxRecord(array("evrid"=>$evrid),$this->model);
   if($record)
	  sendNotify($record);
}
/*   
if($retCode==1) {
   sendNotify(array("ACTION"=>"qrcode_tx",
				"evrid"=>$evrid,
				"txStatus"=>'3',
				"totalBonus"=>$total_bonus,
				"note"=>''));
}
*/
$txArr=array();
$txArr['evrid']=$evrid;
$txArr['userid']=$userid;
$txArr['vendorid']=$vendorid;
$txArr['tx_code_md5']=$tx_code_md5;
$txArr['expired_time']=$now_ts+180;
$txArr['vendor_name']=$vendor_name;
$txArr['total_bonus']=$total_bonus;
$this->tplVar('txArr' , $txArr);
$this->display();
