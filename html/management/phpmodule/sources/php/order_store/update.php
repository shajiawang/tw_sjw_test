<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

// Check Variable Start
if (empty($this->io->input["post"]["vendor_prodid"])) {
	$this->jsAlertMsg('請選擇商品名稱!!');
}
if (empty($this->io->input["post"]["vendor_name"])) {
	$this->jsAlertMsg('請填寫訂單名稱!!');
}
if (empty($this->io->input["post"]["unit_price"])) {
	$this->jsAlertMsg('請填寫商品單價!!');
}
if (empty($this->io->input["post"]["tx_quantity"])) {
	$this->jsAlertMsg('請填寫商品數量!!');
}
if (!empty($this->io->input["post"]["unit_price"]) && !is_numeric($this->io->input["post"]["unit_price"])) {
	$this->jsAlertMsg('商品單價格式錯誤!!');
}
if (!empty($this->io->input["post"]["tx_quantity"]) && !is_numeric($this->io->input["post"]["tx_quantity"])) {
	$this->jsAlertMsg('商品數量格式錯誤!!');
}
// Check Variable End





##############################################################################################################################################
// Update Start

$userid = isset($this->io->input["post"]["userid"]) ? $this->io->input["post"]["userid"] : '';
$num = isset($this->io->input["post"]["tx_quantity"]) ? (int)$this->io->input["post"]["tx_quantity"] : 1;
$unit_price = isset($this->io->input["post"]["unit_price"]) ? (float)$this->io->input["post"]["unit_price"] : 0;

//總費用
$total_price = $unit_price * $num;

//檢查訂單
$query ="SELECT *
FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_vendor_record` 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `evrid` = '{$this->io->input["post"]["evrid"]}'
	AND `switch` = 'Y'
LIMIT 1	
";
$recArr = $this->model->getQueryRecord($query); 

//檢查使用者的紅利點數
$query = "SELECT SUM(amount) bonus 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus`  
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `userid` = '{$userid}' 
	AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;

if($user_bonus < $total_price) {
	$this->jsAlertMsg('使用者的紅利點數不足!!');	
}
else {
	//查詢紅利商品資訊
	$query = "SELECT name FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product`  
    WHERE 
    	`prefixid` = '{$this->config->default_prefix_id}' 
    	AND `epid` = '{$this->io->input["post"]['vendor_prodid']}' 
    	AND `switch` = 'Y'
    ";
    $table = $this->model->getQueryRecord($query);
	
	//編輯訂單 Exchange_vendor_record
	$query = "UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_vendor_record` SET 
		`vendor_name` = '{$this->io->input["post"]['vendor_name']}',
		`vendor_prodid` = '{$this->io->input["post"]['vendor_prodid']}',
		`prod_name` = '{$table['table']['record'][0]['name']}',
		`unit_price` = '{$unit_price}', 
		`tx_quantity` = '{$num}', 
		`total_bonus` = '{$total_price}', 
		`memo` = '{$this->io->input["post"]['memo']}',
		`modifyt` = now()
	where
		`prefixid`='{$this->config->default_prefix_id}'
		and `evrid` = '{$this->io->input["post"]['evrid']}'
		and `vendorid` = '{$this->io->input['session']['sajamanagement']['enterprise']['enterpriseid']}'
		and `userid` = '{$userid}'
		and `tx_status` = '1'
		and `switch` = 'Y'
	";
	error_log("sql:".$query);
	$this->model->query($query);
}

// Update End
##############################################################################################################################################

header("location:/management/order_store/preview/evrid=".$this->io->input["post"]['evrid']."&location_url=".$this->io->input['post']['location_url']);