<?php
if (empty($this->io->input['session']['sajamanagement']['enterprise'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['sajamanagement']['enterprise']["enterpriseid"];

header("location:".$this->config->default_main."/order_store/add");