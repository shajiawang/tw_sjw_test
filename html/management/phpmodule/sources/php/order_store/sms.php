<?php
session_start();

if(empty($_SESSION['sajamanagement']['enterprise']['enterpriseid'])) { //'请先登入商家账号'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	
	include_once "saja/mysql.ini.php";
	//error_log("config: ".$this->config->db[0]['dbname']);
	$db = new mysql($this->config->db[0]);
	//$db = new mysql($this->config->db[0]);
	$config = $this->config;
	//$this->model->connect();
	//$db_user = $this->config->db[0]['dbname'];
	
	$s = new SMSAuth;
	
	
	if($_POST['type'] == 'sms') {
		$s->verify_phone($db, $config);
	}
	if ($_POST['type'] == 'check') {
		$s->check_sms($db, $config);
	}
	if ($_POST['type'] == 'success') {
		$s->send_message($db, $config);
	}
}

class SMSAuth 
{
	//傳送簡訊證認碼
	public function verify_phone($db, $config)
	{
		$ret['status'] = 0;
		
		$chk1 = $this->chk_phone($db, $config);
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
		}
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	public function chk_phone($db, $config)
	{
		$r['err'] = '';
		
		$user_auth = $this->validUserAuth($_POST['userid'], $db, $config);
		
		if (empty($user_auth) ) {
			//'手机号码不正确' 
			$r['err'] = 3;
		} 
		else 
		{
			$uid = $_POST['userid']; 
			$code = $user_auth['code']; 
			$user_phone = $user_auth['phone'];
			$user_phone_0 = substr($user_phone, 0, 1);
			$user_phone_1 = substr($user_phone, 1, 1);
	
			if($user_phone_0 == '0' && $user_phone_1 == '9') {
				$phone = $user_phone;
				$area = 1;
			}
			else if($user_phone_0 == '1') {
				$phone = $user_phone;
				$area = 2;
			}
			else {
				//'手机号码只提供台灣及大陸驗證' 
				$r['err'] = 5;
			}
			
			if(! $this->mk_sms($uid, $phone, $code, $area) ) {
				//'手机号码不正确' 
				$r['err'] = 3;
			}
		}
		return $r;
	}
	
	//SMS驗證碼
	public function mk_sms($uid, $phone, $code, $area)
	{
		if ($area == 1) {
			//簡訊王 SMS-API
			$sMessage = urlencode("ShaJiaWang: ". $code ."");
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
		  
			if(!$getfile=file($to_url)) {
				//('ERROR: SMS-API 无法连接 !', $this->config->default_main ."/user/register");
				return false;
			}
			else 
			{
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				if($kmsgid < 0) {
					//('手机号码错误!!', $this->config->default_main ."/user/register");
					return false;
				}
				else {
					return true;
				}
			}
		}
		else if ($area == 2) {
			//中國短信網
			$url = 'http://smsapi.c123.cn/OpenPlatform/OpenApi';
			
			$data = array
			(
				'action' => 'sendOnce',
				'ac' => '1001@501091960001',
				'authkey' => 'F99AD9CBBB17B21DEA3494115E228D30',
				'cgid' => '184',
				'm' => $phone,
				'c' => $code.'（手机认证验证码，三十分钟内有效）',
				'csid' => '',
				't' => ''
			);
			$xml = $this->postSMS($url,$data);
		    $re = simplexml_load_string(utf8_encode($xml));
			if (trim($re['result']) == 1)
			{
			    return true;
		    }
			else
			{
				return false;
			}
		}
	}
	
	//產生交易成功簡訊
	public function mk_usersms($uid, $phone, $message, $area)
	{
		if ($area == 1) {
			//簡訊王 SMS-API
			$sMessage = urlencode(mb_convert_encoding($message, 'BIG5', 'UTF-8'));
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
		  
			if(!$getfile=file($to_url)) {
				//('ERROR: SMS-API 无法连接 !', $this->config->default_main ."/user/register");
				return false;
			}
			else 
			{
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				if($kmsgid < 0) {
					//('手机号码错误!!', $this->config->default_main ."/user/register");
					return false;
				}
				else {
					return true;
				}
			}
		}
		else if ($area == 2) {
			//中國短信網
			$url = 'http://smsapi.c123.cn/OpenPlatform/OpenApi';
			
			$data = array
			(
				'action' => 'sendOnce',
				'ac' => '1001@501091960001',
				'authkey' => 'F99AD9CBBB17B21DEA3494115E228D30',
				'cgid' => '184',
				'm' => $phone,
				'c' => $message,
				'csid' => '',
				't' => ''
			);
			$xml = $this->postSMS($url,$data);
		    $re = simplexml_load_string(utf8_encode($xml));
			if (trim($re['result']) == 1)
			{
			    return true;
		    }
			else
			{
				return false;
			}
		}
	}
	
	public function postSMS($url, $data=''){
		$row = parse_url($url);
		$host = $row['host'];
		$port = $row['port'] ? $row['port'] : 80;
		$file = $row['path'];
		while (list($k,$v) = each($data)) 
		{
			$post .= rawurlencode($k)."=".rawurlencode($v)."&";
		}
		$post = substr($post , 0 , -1);
		$len = strlen($post);
		$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
		if (!$fp) {
			return "$errstr ($errno)\n";
		} else {
			$receive = '';
			$out = "POST $file HTTP/1.0\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Content-type: application/x-www-form-urlencoded\r\n";
			$out .= "Connection: Close\r\n";
			$out .= "Content-Length: $len\r\n\r\n";
			$out .= $post;		
			fwrite($fp, $out);
			while (!feof($fp)) {
				$receive .= fgets($fp, 128);
			}
			fclose($fp);
			$receive = explode("\r\n\r\n",$receive);
			unset($receive[0]);
			return implode("",$receive);
		}
	}
		
	//SMS完成驗證
	public function check_sms($db, $config)
	{
		$db->connect();
		$db_user = $config->db[0]['dbname'];
		//error_log("SMSCount: ".$_SESSION['SMSCount']);
		$user_auth = $this->validUserAuth($_POST['userid'], $db, $config);
			
		if (empty($user_auth) ) {
			//'手机号码不正确' 
			$ret['status'] = 2;
		}
		elseif (empty($_POST['smscode']) || ($_POST['smscode'] !== $user_auth['code']) ) {
			//'手机号码不正确' 
			$_SESSION['SMSCount']--;
			if ($_SESSION['SMSCount'] > 0) {
				$ret['status'] = 4;		//證認碼輸入錯誤
			}
			else {
				$ret['status'] = 6;		//證認碼輸入錯誤3次，請重新建立訂單
			}
		}
		
		if(empty($ret['status']) || $ret['status'] == 6) 
		{
			//修改SMS check code
			$shuffle = $this->get_shuffle();
			$checkcode = substr($shuffle, 0, 6);
				
			$query = "UPDATE `{$db_user}`.`{$config->default_prefix}user_sms_auth` 
			SET 
				code = '{$checkcode}'
			WHERE 
				`prefixid` = '{$config->default_prefix_id}'
				AND `userid` = '{$_POST['userid']}'
			";
			$db->query($query);
		}
		
		//回傳輸入次數
		$ret['SMSCount'] = $_SESSION['SMSCount'];
		
		//回傳:
		if (empty($ret['status'])) {
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	//交易成功簡訊
	public function send_message($db, $config)
	{
		$db->connect();
		$db_user = $config->db[0]['dbname'];
		//error_log("SMSCount: ".$_SESSION['SMSCount']);
		$user_auth = $this->validUserAuth($_POST['userid'], $db, $config);
		
		if (empty($user_auth) ) {
			//'手机号码不正确' 
			$ret['status'] = 2;
		}
		
		if(empty($ret['status'])) 
		{
			$user_phone = $user_auth['phone'];
			$user_phone_0 = substr($user_phone, 0, 1);
			$user_phone_1 = substr($user_phone, 1, 1);
	
			if($user_phone_0 == '0' && $user_phone_1 == '9') {
				$phone = $user_phone;
				$area = 1;
			}
			else if($user_phone_0 == '1') {
				$phone = $user_phone;
				$area = 2;
			}
			else {
				//'手机号码只提供台灣及大陸驗證' 
				$ret['status'] = 5;
			}
			
			$query = "select * from `{$config->db[3]['dbname']}`.`{$config->default_prefix}exchange_vendor_record` 
			where 
				`prefixid` = '{$config->default_prefix_id}'
				and `evrid` = '{$_POST['evrid']}'
				AND `userid` = '{$_POST['userid']}'
				and `tx_status` = '3'
				and `switch`= 'Y'
			";
			//error_log("query: ".$query);
			$recArr = $db->getQueryRecord($query);
			
			if ($recArr['table']['record'][0]['vendor_prodid'] == 490) {
				$prod_name = '成功兌換國內外跟團遊';
			} else if ($recArr['table']['record'][0]['vendor_prodid'] == 491) {
				$prod_name = '成功兌換國內外機票';
			} else if ($recArr['table']['record'][0]['vendor_prodid'] == 492) {
				$prod_name = '成功兌換國內外酒店住宿';
			}
			$commit_time = date("Y-m-d H:i", strtotime($recArr['table']['record'][0]['commit_time']));
			$message = $commit_time.$prod_name.'：'.$recArr['table']['record'][0]['vendor_name'].'，單價：'.$recArr['table']['record'][0]['unit_price'].'點，數量：'.$recArr['table']['record'][0]['tx_quantity'].'，共：'.$recArr['table']['record'][0]['total_bonus'].'點';
			
			if(! $this->mk_usersms($_POST['userid'], $phone, $message, $area) ) {
				//'手机号码不正确' 
				$ret['status'] = 3;
			}
			
		}
		
		//回傳:
		if (empty($ret['status'])) {
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
		
	public function validUserAuth($id, $db, $config) 
	{
		//資料庫連結介面
		$db->connect();
		
	    $query = "select phone from `{$config->db[0]['dbname']}`.`{$config->default_prefix}user_profile` 
		where 
			`prefixid` = '{$config->default_prefix_id}'
			AND `userid` = '{$id}'
			and `switch`= 'Y'
		";
		//error_log("query: ".$query);
		$recArr = $db->getQueryRecord($query);
		
		if(empty($recArr['table']['record'][0]) ) {
			return false;
		}
		else
		{
			$query = "SELECT * 
			FROM `{$config->db[0]['dbname']}`.`{$config->default_prefix}user_sms_auth`
			WHERE 
				`prefixid` = '{$config->default_prefix_id}' 
				AND `userid` = '{$id}'
				AND `switch` = 'Y'
			";
			$table = $db->getQueryRecord($query);
			
			if(!empty($table['table']['record']) ) {
				$table['table']['record'][0]['phone'] = $recArr['table']['record'][0]['phone'];
				return $table['table']['record'][0];
			}
			else
			{
				//新增SMS check code
				$shuffle = get_shuffle();
				$checkcode = substr($shuffle, 0, 6);
				
				$query = "INSERT INTO `{$config->db[0]['dbname']}`.`{$config->default_prefix}user_sms_auth` 
				SET
					`prefixid` = '{$config->default_prefix_id}',
					`userid` = '{$id}',
					`code` = '{$checkcode}',
					`verified` = 'N',
					`insertt` = NOW()
				";
				$db->query($query);
				
				return false;
			}
		}
	}
	
	public function get_shuffle()
	{
		$v = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
		shuffle($v);
		
		$rand = time() * rand(1,9);
		$rand1 = substr($rand, -6, 2);	
		$rand2 = substr($rand, -4, 2);
		$rand3 = substr($rand, -2);
		
		$code = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $rand3;
		
		return $code;
	}
}
?>