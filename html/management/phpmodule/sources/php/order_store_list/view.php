<?php
if (empty($this->io->input['session']['sajamanagement']['enterprise'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$enterpriseid = $this->io->input['session']['sajamanagement']['enterprise']["enterpriseid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(evrid|commit_time)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}


if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
else{
	$sub_sort_query =  " ORDER BY commit_time desc";
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$rt_search_query = "";
/*if($this->io->input["get"]["search_channelid"] != ''){
	$status["status"]["search"]["search_channelid"] = $this->io->input["get"]["search_channelid"] ;
	$status["status"]["search_path"] .= "&search_channelid=".$this->io->input["get"]["search_channelid"] ;
	$rt_search_query .= "
	LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}channel_news_rt` cn ON 
		n.prefixid = cn.prefixid 
		AND n.newsid = cn.newsid 
		AND cn.`channelid` = '{$this->io->input["get"]["search_channelid"]}' 
		AND cn.`switch`='Y'
	";
	$sub_search_query .=  "
		AND cn.newsid IS NOT NULL";
}
if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND n.`name` like '%".$this->io->input["get"]["search_name"]."%' ";
}
if($this->io->input["get"]["search_description"] != ''){
	$status["status"]["search"]["search_description"] = $this->io->input["get"]["search_description"] ;
	$status["status"]["search_path"] .= "&search_description=".$this->io->input["get"]["search_description"] ;
	$sub_search_query .=  "
		AND n.`description` like '%".$this->io->input["get"]["search_description"]."%' ";
}
*/
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query = "
SELECT count(*) as num 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_vendor_record` evr 
{$rt_search_query}
WHERE 
evr.`prefixid` = '{$this->config->default_prefix_id}' 
and evr.`vendorid` = '{$enterpriseid}'
AND evr.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT evr.*, 
(select nickname from `{$db_user}`.`{$this->config->default_prefix}user_profile` up where up.prefixid = evr.prefixid and up.userid = evr.userid and up.switch = 'Y') nickname, 
(select name from `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` ep where ep.prefixid = evr.prefixid and ep.epid = evr.vendor_prodid and ep.switch = 'Y') pname 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_vendor_record` evr 
{$rt_search_query}
WHERE 
evr.`prefixid` = '{$this->config->default_prefix_id}' 
and evr.`vendorid` = '{$enterpriseid}'
AND evr.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query);  
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();