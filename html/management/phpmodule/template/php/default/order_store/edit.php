<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
		</div>
		<div class="table-wrapper">
		<?php if (!empty($this->tplVar['table']['user']['userid'])) { ?>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯訂單</div>
			<table class="form-table">
				<tr>
					<td><label for="nickname">會員暱稱</label></td>
					<td><?php echo $this->tplVar['table']['user']['nickname']; ?></td>
				</tr>
				<tr>
					<td><label for="vendor_prodid">*商品名稱</label></td>
					<td>
						<select name="vendor_prodid">
							<option value="">請選擇</option>
							<?php 
								if (is_array($this->tplVar['table']['exchange_product'])) {
									foreach ($this->tplVar['table']['exchange_product'] as $key => $value) {
							?>
							<option value="<?php echo $value['epid']; ?>" <?php echo ($value['epid'] == $this->tplVar['table']['exchange_vendor_record']['vendor_prodid']) ? 'selected' : ''; ?> ><?php echo $value['name']; ?></option>
							<?php } } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="vendor_name">訂單名稱</label></td>
					<td><input name="vendor_name" type="text" size="20" maxlength="20" value="<?php echo $this->tplVar['table']['exchange_vendor_record']['vendor_name']; ?>" />(限填寫20個字)</td>
				</tr>
				<tr>
					<td><label for="unit_price">商品單價：</label></td>
					<td><input name="unit_price" type="text" size="5" value="<?php echo $this->tplVar['table']['exchange_vendor_record']['unit_price']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="tx_quantity">商品數量：</label></td>
					<td><input name="tx_quantity" type="text" size="5" maxlength="3" value="<?php echo $this->tplVar['table']['exchange_vendor_record']['tx_quantity']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="memo">備註：</label></td>
					<td><textarea name="memo" rows="3" cols="50"><?php echo $this->tplVar['table']['exchange_vendor_record']['memo']; ?></textarea></td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="evrid" value="<?php echo $this->tplVar['table']["exchange_vendor_record"]["evrid"] ;?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']["exchange_vendor_record"]["userid"] ;?>">
				<div class="button submit"><input type="submit" class="submit" value="預覽"></div>
				<div class="button cancel"><input type="reset" class="submit" value="重置"></div>
				<div class="clear"></div>
			</div>
		</form>
		<?php } ?>
		</div>
	</body>
</html>