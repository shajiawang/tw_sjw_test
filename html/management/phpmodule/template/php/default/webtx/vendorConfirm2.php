<?php
   $txArr=$this->tplVar['txArr'];
   $evrid=$txArr['evrid'];
  
   error_log("[webtx/vendorconfirm2] APP_DIR : ".APP_DIR);
  
   $APP_DIR='/site';
?>
<!DOCTYPE html>
<html>
	<head>
	    <title>殺價王</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" src="/management/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="/management/js/jquery.timers-1.2.js"></script> 
		<script type="text/javascript" src="/management/js/jquery.mobile-1.4.3.min.js"></script>
		<script type="text/javascript" src="/management/js/jquery.gracefulWebSocket.js"></script>
		<script type="text/javascript" src="/management/js/jquery-ui.custom.min.js"></script>
<!--		<script type="text/javascript" src="/management/js/messages_tw.js"></script>-->
	    <link rel="stylesheet"  href="/management/css/jquery-ui.css"  />
		<link rel="stylesheet"  href="/management/css/jquery.mobile-1.4.5.min.css" />
		
        <!-- flexbox 框架架構 -->
		<link rel="stylesheet"  href="/management/css/flexbox.css" />
        <!-- es自訂義CSS -->
		<link rel="stylesheet"  href="/management/css/vendorconfirm.css" />
	</head>
	<body>
       
        <script>
            var evrid='<?php echo $evrid; ?>';
            function chkTxData() {
                f = document.qrcodeTx;
                if(f.vendorid.value=="") {
                    alert("請填寫商家編號 !!");
                    return false;
                }
                if(f.vendor_name.value=="") {
                    alert("請填寫商家名稱 !!");
                    return false;
                }
                if(f.total_price.value=="" || isNaN(f.total_price.value)) {
                    alert("收取鯊魚點數 : 必需為數字 !!");
                    return false;
                }
                if(f.total_price.value<0) {
                    alert("收取鯊魚點數 : 必需為正數 !!");
                    return false;
                }
                f.prod_name.value=f.vendor_name.value+" : "+f.total_price.value;
                // document.qrcodeTx.submit();
                checkloading("交易狀態 : 買家兌換中, 請稍候....");	
				
                $.ajax({
                    url : '/management/webtx/vendorCommit2/',
                    type : 'post',
                    data:  $('form').serialize(),
                    success : function(ret) {
                        // console.log(ret);
                        var retdata = $.parseJSON(ret);
                        // if (json.status == 1){
                        var now_time;
                        var evrid = '<?php echo $evrid; ?>';
                        var socket;
                        var wssurl = '<?php echo $this->config->wss_url; ?>';
                        var baseurl= '<?php echo BASE_URL; ?>';		
                        var pageid;
                        var end_time ;
                        var lost_time;
                        
                        baseurl='';
                        
                        $.get(baseurl+"<?php echo $APP_DIR; ?>/lib/cdntime.php",
                            function(data) {
								now_time = data; //系統時間
                            }
                        );
								
						if (true) {
				            // if(is_weixn()) {
							$(this).stopTime('losttime2');
							$(this).everyTime('1s','losttime2', function() {
								pageid = $.mobile.activePage.attr('id');
								now_time++;
								end_time = retdata.expired_ts; //截標時間
								lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
								
                                if(lost_time>=0) {
									// $('#'+pageid+' .countdown').html(lost_time);
									$.get(baseurl+'<?php echo $APP_DIR; ?>/mall/getQrcodeTxStatus/?evrid='+evrid, 
										function(status) {
											intStatus=parseInt(status);
											if(intStatus==4) {
												// 買家已兌換完成
												$(document).stopTime('losttime2');
                                                // $('#tx_status_desc').text("交易状狀態 : 完成 !!");
                                                checkloading("交易狀態 : 完成 !!");
                                                // $('#txOK')[0].load();
                                                // $('#txOK')[0].play();
                                                // window.alert("交易完成 !!");
                                                window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/storebonuslist/';
                                            } else if(intStatus==0) {
                                                // 交易取消
                                                $(document).stopTime('losttime2');
                                                // $('#tx_status_desc').text("交易狀態 : 逾時取消 !!");
                                                checkloading("交易狀態 : 逾時取消 !!");
                                                // window.alert("逾時取消交易 !!");
                                                window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/';
                                            } else if(intStatus<0) {
                                                // 交易取消
                                                $(document).stopTime('losttime2');
                                                // $('#tx_status_desc').h("交易狀態 : 買家取消 !!");
                                                checkloading("交易狀態 : 買家取消!!");
                                                // window.alert("買家取消交易 !!");
                                                window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/';
                                            }
                                        }
                                    );
								} else if(lost_time<0) {
									// 交易取消;
									$.get(baseurl+"<?php echo $APP_DIR; ?>/mall/cancelQrcodeTx/?evrid="+evrid, 
										function(data) {
											var msg=$.parseJSON(data);
											if(msg.retCode=='1') {
												// $('#tx_status_desc').html("交易狀態 : 逾時取消 !!");
												checkloading("交易狀態 : 逾時取消 !!");
												// window.alert("逾时取消交易 !!");
                                                window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/';
											}								
										}
									);
									$(this).stopTime('losttime2');
									window.location.href='<?php echo $APP_DIR; ?>/enterprise/';  // ToDo
                                }
							});
						} else {
							socket = $.gracefulWebSocket(wssurl);
							socket.onopen =  function(evt){
                                // alert("socket"+evrid+"|V....");
                                socket.send("ADD|"+evrid+"|V");
                            };
                            socket.onmessage = function(evt){ 
								// message format evrid|status|message;
								var ret = evt.data;
								var arr = ret.split("|");
								var id=arr[0];
								var status=arr[1];
								if(id==evrid) {
									process(status);
								}
                            };
                            
                            $(this).stopTime('losttime2');
                            $(this).everyTime('1s','losttime2', function() {
                                pageid = $.mobile.activePage.attr('id');
                                end_time = $('#'+pageid+' .countdown').attr('data-offtime2'); //截標時間
                                now_time++;
                                lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
                                // $('#'+pageid+' #cntdown').html(lost_time);
                                if(lost_time<0) {
                                    // 交易取消;
                                    $(this).stopTime('losttime2');
                                    $.get(baseurl+"<?php echo $APP_DIR; ?>/mall/cancelQrcodeTx/?evrid="+evrid,
                                        function(data) {
                                            var msg=$.parseJSON(data);
											if(msg.retCode=='1') {
                                                // $('#tx_status_desc').html("交易狀態 : 逾時取消 !!!");
                                                checkloading("交易狀態 : 逾時取消 !!");
                                            }
                                        }
                                    );
                                }
                            });
						}

					}
				});
            }

            function cancelTx() {
                if(confirm("確定取消本次交易 ??")) {
                    document.qrcodeTx.tx_status.value="-2";
                    document.qrcodeTx.submit();
                } else {
                    return false;
                }
            }
        </script>
        <div data-role="page" data-title="殺價王" >
            <div data-role="header" id="header">
                <h1>商家確認交易</h1>
                <div class="header-btn" onclick="cancelTx()">取消</div>
            </div>
            <div id="content">
               <div class="article">
                    <form name="qrcodeTx" method="post" action="/management/webtx/vendorCommit2" >
                        <input type="hidden" name="tx_status" value="3">
                        <input type="hidden" name="evrid" value="<?php echo $txArr['evrid']; ?>" >
                        <input type="hidden" name="userid" value="<?php echo $txArr['userid']; ?>" >
                        <input type="hidden" name="vendorid" value="<?php echo $txArr['vendorid']; ?>">
                        <input type="hidden" name="prod_name" value="<?php echo $txArr['companyname']; ?>">
                        <input type="hidden" name="tx_code_md5" value="<?php echo $txArr['tx_code_md5']; ?>" >
                        <input type="hidden" name="tx_currency" value="<?php echo $txArr['tx_currency']; ?>" >
                        <input type="hidden" name="tx_quantity" value="1">
                        <input type="hidden" name="tx_type" value="web_tx">
                        <input type="hidden" name="vendor_name" value="<?php echo $txArr['vendor_name']; ?>" >
                        <input type="hidden" name="keyin_time" value="<?php echo $txArr['keyin_time']; ?>" >
                        <div class="evul">
                            <?php
                                error_log("[webtx/vendorconfirm2] thumbnail : ".$txArr['thumbnail']);
                            ?>
                            <div class="header-img">
                                <!--<img class="img-fluid" src="https://www.saja.com.tw/site/images/site/product/6b7e87cb49e9972e8825bdbacfba0bf5.png">-->
                                <img src="<?php echo $txArr['thumbnail']; ?>">
                            </div>
                            <div class="header-title">
                                <!--<span>台北遠東國際大飯店</span>-->
                                <span><?php echo $txArr['vendor_name']; ?></span>
                            </div>
                            <div class="charge-box d-flex align-items-center">
                                <div class="input-box">
                                    <input type="number" id="total_price" class="keyboard" name="total_price" size="20" placeholder="請輸入收取鯊魚點">
                                </div>
                                <div class="unit-txt align-self-end">點</div>
                            </div>
                            <div class="feedback"></div>
                            
                            <button type="button" class="check-btn" onclick="chkTxData()" disabled='disabled'>確認交易</button>
                            
                            
<!-- 2018/11/20舊資料 -->
<!--
                            <li>
                            <div style="text-align:center" data-role="controlgroup" data-type="horizontal">			
                                <a href="#" data-role="button" data-icon="delete" data-iconpos="left" onclick="cancelTx()">取消交易</a>
                                <a href="#" data-role="button" data-icon="check" data-iconpos="right" onclick="chkTxData()">確認交易</a>		    
                            </div>
-->
                            <!--button data-icon="delete" data-iconpos="left" onclick="cancelTx()">取消交易</button>
                            <button type="submit" data-icon="check" data-iconpos="right">确认交易</button -->
<!--                            </li>-->
<!-- 2018/11/20舊資料 / -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
<!-- 2018/11/20舊資料 -->
<!--
<div data-role="footer" id="footer">
  <h4>全球首创杀价式拍卖导购平台</h4>
</div>
-->
<!-- 2018/11/20舊資料 / -->
   
   
   
        <!--  鍵盤-自訂義CSS  -->
        <link rel="stylesheet" href="/management/css/keyboard.min.css">
        <!--  CDN Bootstrap 4.1.1  -->
        <script src="/management/js/bootstrap.bundle.min.js"></script>
        <!--  鍵盤-自訂義JS  -->
        <script src="/management/js/keyboard.js"></script>

        <!--  小鍵盤動作  -->
        <script>
            options.dom.caller = '.keyboard';                   //命名:呼叫小鍵盤的物件
            options.dom.keyidName = 'numkeyModal';              //命名:小鍵盤(無id符號)
            options.dom.keyModal = '#' + options.dom.keyidName; //命名:小鍵盤(有id符號)
            options.dom.callerId = '#total_price';                    //預設一開始的呼叫者,在keyboard.JS會做變更 點擊者
            options.btn.sure = '確定';                           //小鍵盤送出按鈕名稱

            $(function(){
                //checkloading('賣家兌換中請稍後...');
                $(options.dom.caller).each(function(i){
                    var $modalNum = options.dom.keyidName+i;
                    //console.log($modalNum);
                    var $that = $(this);
                    $that.on("click",function(e){
                        console.log(5);
                        options.dom.callerId = $that;                           //記錄點擊哪個input的ID
                        //開啟小鍵盤的動作
                        //若預設開啟時，此段不會執行
                        $('#'+$modalNum).on('show.bs.modal', function (e) {
                            controlSize($that);
                        });
                        //關閉小鍵盤的動作
                        $('#'+$modalNum).on('hide.bs.modal', function (e) {
                            var $callerId = $(options.dom.callerId);            //取得按鈕ID
                            var $feedback = $('.feedback');                     //驗證msg
                            var $callerVL = $callerId.val().length;

                            if($callerVL < 1) {
                                $('.check-btn').attr('disabled',true);
                            }
                            else if($callerVL > 10){
                                $callerId.val($callerId.val().substr(0,10));
                                $feedback.text('限制 最多10個字元');
                                $('.check-btn').attr('disabled',true);
                            }else{
                                $feedback.text('');
                                $('.check-btn').attr('disabled',false);
                            }
                            
                            //移除 將輸入框上推的方塊
                            removeBlank();

                        });
                    })
                });
                setTimeout(function(){
                    $(options.dom.callerId).click();                //小鍵盤預設開啟 (類似input的focus)
                },200);
            })

            //等待賣家兌換中
            function checkloading(msg){
                var esCheckMsgOptions = {
                    msgbox: 'esCheckMsgBox',
                    msgbg: 'esCheckBg'
                };

                $('body')
                .append(
                    $('<div/>')
                    .addClass(esCheckMsgOptions.msgbox)
                    .text(msg)
                )
                .append(
                    $('<div/>')
                    .addClass(esCheckMsgOptions.msgbg)
                );
                $('body').addClass('modal-open');
            }
        </script>
    
    </body>
</html>