	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/main.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/widget.css" type="text/css">
	    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/le-frog/jquery-ui.css" type="text/css">
		<!-- link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/jquery-ui.css" type="text/css" -->
	    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<!-- script src="<?php echo $this->config->default_main; ?>/js/jquery.min.js"></script -->
	    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
		<!-- script src="<?php echo $this->config->default_main; ?>/js/jquery-ui.js"></script -->
	    <script type="text/javascript" src="<?php echo $this->config->default_main; ?>/js/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="<?php echo $this->config->default_main; ?>/js/ckeditor/ckeditor.js"></script>
	    <script type="text/javascript">
	    jQuery(function($){
	    	//清除搜尋表單
	    	$('button.clear').on('click', function(e){
	    		$(this)
	    		.parents('form')
	    		.find('input,select,textarea')
	    		.val('')
	    	});
	    	//在列表點選刪除按鈕時，自動刪除被勾選的項目
			$('.table .functions .delete-all').click(function(){
				if (!confirm('是否確認刪除資料？')) {
					return false;
				}

				var now = 0,
					total = $('.table .body tr:has(.list-item:checked)').each(function(){
						$.get($('.icon-delete.icons', this).prop('href'), function(){
							now++;
							if (now == total) {
								window.location.reload();
							}
						});
					}).length;
			});
			$('.icon-delete.icons').click(function(){
				return confirm('是否確認刪除資料？');
			});
			//列表上方全選按鈕
			$('.table .checkbox-all').click(function(){
				$('.table .list-item:checkbox').prop('checked', $(this).is(':checked') );
			});
			//點擊rt列表名稱時，自動勾選
			$('.form .relation-list .relation-item-name').click(function(){
				$(this).siblings('input:checkbox').prop('checked', !$(this).siblings('input:checkbox').prop('checked'));
			});
			//日曆widget
			$('.datetime').datetimepicker({dateFormat:"yy-mm-dd"});

			$.fn.showPicker = function(options) {
				var defaults = {
						modal : true,
						height : 420,
						width : 320,
						autoOpen : true
		    		},
		    		settings = $.extend( {}, defaults, options );

	    		return this.each(function(){
	    			if (settings.fun) {
	    				var funid = settings.fun.replace('_','-'),
	    					$this = $(this);

						if (!$('#dialog-' + funid).length) {
							$.get('<?php echo $this->config->default_main; ?>/' + settings.fun + '/view/tpl=widget', function(html){
								if (html) {
									$(html).appendTo($this);
									$('#dialog-' + funid)
									.dialog(settings)
									.find('.rt-list .relation, .button-block a')
									.button();
								}
							});
						}
						else {
							$('#dialog-' + funid).dialog('open');
						}
	    			}
	    		});
			};

			if ($('#ckeditor_description').length) {
				CKEDITOR.replace('ckeditor_description', {
					filebrowserBrowseUrl: '<?php echo $this->config->default_main; ?>/ckeditor/view/',
					filebrowserImageBrowseUrl: '<?php echo $this->config->default_main; ?>/ckeditor/view/',
					filebrowserFlashBrowseUrl: '<?php echo $this->config->default_main; ?>/ckeditor/view/',
					filebrowserUploadUrl: '<?php echo $this->config->default_main; ?>/ckeditor/upload',
					filebrowserImageUploadUrl: '<?php echo $this->config->default_main; ?>/ckeditor/upload',
					filebrowserFlashUploadUrl: '<?php echo $this->config->default_main; ?>/ckeditor/upload'
				});				
			}
	    });
	    </script>
		<script type="text/javascript"><!--

		//--></script>