<div class="breadcrumb header-style">
<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>

<?php
switch($this->io->input['get']['fun']) {
	case 'oscode':
		$name = "限定S碼活動管理";
		break;
	case 'scode':
		$name = "S碼活動管理";
		break;
	case 'scode_spr':
		$name = "S碼活動類別";
		break;
	case 'exchange_mall':
		$name = "兌換中心";
		break;
	case 'exchange_store':
		$name = "兌換店家";
		break;
	case 'exchange_product_category':
		$name = "兌換商品分類";
		break;
	case 'exchange_product':
		$name = "兌換商品";
		break;
	case 'order':
		$name = "訂單管理";
		break;
	case 'stock':
		$name = "庫存管理";
		break;
	case 'promote_rule':
		$name = "活動規則";
		break;
	case 'promote_rule_item':
		$name = "活動規則項目";
		break;
	case 'exchange_bonus_history':
		$name = "兌換紅利記錄";
		break;
	case 'exchange_gift_history':
		$name = "兌換禮券記錄";
		break;
	case 'return_history':
		$name = "退貨記錄";
		break;
	case 'channel':
		$name = "經銷商管理";
		break;
	case 'country':
		$name = "國別管理";
		break;
	case 'language':
		$name = "語系管理";
		break;
	case 'bonus':
		$name = "紅利使用明細";
		break;
	case 'deposit':
		$name = "儲值帳戶";
		break;
	case 'deposit_rule':
		$name = "儲值規則";
		break;
	case 'deposit_rule_item':
		$name = "儲值規則項目";
		break;
	case 'gift':
		$name = "禮券帳戶";
		break;
	case 'user':
		$name = "會員帳號";
		break;
	case 'user_profile':
		$name = "會員詳細資料";
		break;
	case 'user_sms_auth':
		$name = "會員手機驗證記錄";
		break;
	case 'news':
		$name = "會員公告";
		break;
	case 'issues_category':
		$name = "客服訊息分類";
		break;
	case 'issues':
		$name = "客服訊息";
		break;
	case 'faq_category':
		$name = "新手教學分類";
		break;
	case 'faq':
		$name = "新手教學問答";
		break;
	case 'tech_category':
		$name = "殺價密技分類";
		break;
	case 'tech':
		$name = "殺價密技問答";
		break;
	case 'admin_user':
		$name = "管理員帳號";
		break;
	case 'product_today':
		$name = "今日必殺商品";
		break;
	case 'ad_list':
		$name = "廣告查詢";
		break;
	case 'ad_category':
		$name = "廣告分類";
		break;
	case 'response':
		$name = "自動回覆";
		break;
	case 'response_category':
		$name = "自動回覆分類";
		break;
	case 'member_bonus':
		$name = "會員紅利點數";
		break;
	case 'user_extrainfo':
		$name = "會員其他資訊";
		break;
	case 'enterprise':
		$name = "商家帳號";
		break;
	case 'enterprise_bonus':
		$name = "商家紅利查詢";
		break;
	case 'order_store':
		$name = "商家訂單";
		break;
	case 'order_store_list':
		$name = "商家訂單查詢";
		break;
}
?>
<a href="<?php echo $this->config->default_main.'/'.$this->io->input['get']['fun']; ?>"><?php echo $name; ?></a>>>

<?php
switch($this->tplVar['status']['get']['act']) {
	case 'add':
		$act = "新增".$name;
		break;
	case 'edit':
		$act = "編輯".$name;
		break;
	default:
		$act = $name."列表";
		break;
}
?>
<a href="<?php echo $this->config->default_main.'/'.$this->io->input['get']['fun'].'/'.$this->tplVar['status']['get']['act']; ?>"><?php echo $act; ?></a>
</div>