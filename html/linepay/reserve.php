<?php

$postJson=json_encode($_POST);
error_log("[linapay/reserve] request : ".$postJson);

$url = 'https://sandbox-api-pay.line.me/v2/payments/request';

$line_id="1564625040";
$line_secret="69e9157cc1d158eb7ea67c6976624a2d";
$ch = curl_init();

// set Header
$header=array();
$header[0]="Content-Type:application/json; charset=UTF-8";
$header[1]="X-LINE-ChannelId:".$line_id;
$header[2]="X-LINE-ChannelSecret:".$line_secret;

curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

// set Timeout
curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
curl_setopt($ch, CURLOPT_TIMEOUT, 20);
curl_setopt($ch, CURLOPT_POST, 1);

// set Post Data
curl_setopt ($ch, CURLOPT_POSTFIELDS,$postJson);

curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
$response = curl_exec($ch);
curl_close($ch);
error_log("[linapay/reserve] response : ".$response);
$arr_response=json_decode($response,TRUE);

if($arr_response['returnCode']=='0000') {
   Header("Location: ".$arr_response['info']['paymentUrl']['web']);
} else {
   echo $response;
}

?>
