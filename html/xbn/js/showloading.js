$(function(){

    // 顯示載入中動畫
    (function(){

        var $loadingbox = $('.loadingbox');
        var $loadingicon = $('> img', $loadingbox);

        window.showLoading = function(){
            $loadingicon.css({top: $(window).outerHeight()/2-$loadingicon.height()/2});
            $loadingbox.addClass('act').fadeIn(250);
        }
        window.hideLoading = function(){
            $loadingbox.fadeOut(250, function(){  $loadingbox.removeClass('act'); });
        }

    })();

});