<script>
  $(document).ready(function() {
	  $('#prev').hide();	  
  });
</script>
<?php 
    $logined=false;
	if(isset($_SESSION['user'])) {
        $logined=true;
    }
	$this->lang->load('index', $_SESSION['lang']); 	
?>
<article style="height:1.5em;margin-bottom: 0px" >
		 <!-- div style="margin-bottom: 0px; height:0.8em;"  -->		
			
			<select name="lang" id="lang" style="height:2em; margin-bottom:0px;padding:1px;width:18%;float:right;font-size:0.8em;text-align:center;" >
			  <option value="zh_TW" <?php echo ("zh_TW"==$_SESSION['lang'])?"selected":""; ?> >正體中文</option>
			  <option value="zh_CN" <?php echo ("zh_CN"==$_SESSION['lang'])?"selected":""; ?> >简体中文</option>
			  <option value="en_US" <?php echo ("en_US"==$_SESSION['lang'])?"selected":""; ?> >English</option>
			</select>
			<label for="lang" style="margin:0 5px 0 0;padding:1px;float:right;font-size:1em;" ><?php echo $this->lang->line('index_language'); ?> :</label>
		 <!-- /div --> 
</article>

<?php  if(!$logined) {  ?>
   <article>
    	<div class="client4" >
			<a href="<?=base_url()?>get_login/?web=Y" ><img src="<?=base_url()?>images/bonus1.png" style="width:60px"><span><?php if ($_SESSION['user']['user_type'] == "B" ){ ?><?php echo $this->lang->line('index_bonus_b'); ?><?php }else{ ?><?php echo $this->lang->line('index_bonus_c'); ?><?php } ?></span></a>
	    	<a href="<?=base_url()?>get_login/?web=Y" ><img src="<?=base_url()?>images/exchange1.png" style="width:60px"><span><?php echo $this->lang->line('index_exchange'); ?></span></a>
	    	<a href="<?=base_url()?>get_login/?web=Y" ><img src="<?=base_url()?>images/handsel1.png" style="width:60px"><span><?php echo $this->lang->line('index_handsel'); ?></span></a>
	    	
    	</div>
    </article>
<?php  } else { ?>
   <article>
    	<div class="client4">
			<?php if ($_SESSION['user']['user_type'] == "B" ){ ?>
				<a onClick="scan()">
			<?php }else{ ?>
				<a onClick="qc()">
			<?php } ?>	
			<img src="<?=base_url()?>images/bonus1.png" style="width:60px"><span><?php if ($_SESSION['user']['user_type'] == "B" ){ ?><?php echo $this->lang->line('index_bonus_b'); ?><?php }else{ ?><?php echo $this->lang->line('index_bonus_c'); ?><?php } ?></span></a>
	    	<?php if ($_SESSION['user']['user_type'] == "C" ){ ?>
				<a onClick="scan()">
			<?php }else{ ?>
				<a onClick="qc2()">
			<?php } ?>				
			<img src="<?=base_url()?>images/exchange1.png" style="width:60px"><span><?php echo $this->lang->line('index_exchange'); ?></span></a>
	    	<a href="javascript:location.href='<?=base_url()?>get_handsel/?web=Y'"><img src="<?=base_url()?>images/handsel1.png" style="width:60px"><span><?php echo $this->lang->line('index_handsel'); ?></span></a>
	    		
    	</div>
    </article>
<?php  }  ?>

    <!--a ><img src="<?=base_url()?>images/nav05.png"><span>提 現</span></a-->	
	<!--a href="javascript:location.href='<?=base_url()?>get_cash/?web=Y'"><img src="<?=base_url()?>images/nav05.png"><span>提 現</span></a-->
    <!-- 雪幣大聯盟商戶 -->
    <article>
    	<h1><?php echo $this->lang->line('index_store'); ?></h1>
    <div class="store-box">
		<ul class="store">
		    <?php
				$i = 1;
				foreach ($store as $item) { 
			    if($i > 4)  continue;
				$logo = $item['thumbnail_file']; // logo图片是你自己放到文件夹里的  
				if (empty($logo)){
					$logo = $item['thumbnail_url'];
				} 
				if(strpos(strtolower($logo),"http://")!==0 && strpos(strtolower($logo),"https://")!==0) {
				   $logo_file=base_url()."uploads/sp/".$logo."?t=".$ts;
				} else {
				   $logo_file=$logo;
				}				
			?>
				<li <?php if (($i%4) == 0){ ?>style="margin-right: 0;"<?php } ?> >
				<a href="<?=base_url()?>get_store/store_detail/<?=$item['userid']?>/?web=Y">
					<img style="height:3.4rem; overflow:hidden" src="<?=$logo_file?>">
					<span ><?=$item['name']?></span>
				</a>
			</li>
			<?php
				$i++;
				}
			?>
		</ul>	
	</div>
    </article>
    <!-- 精選促销 -->
    <article style="margin-bottom: 0px;">
    	<h1><?php echo $this->lang->line('index_product'); ?></h1>
    </article>
    <div class="commodity-box" style="margin-bottom: 0rem">
		<ul class="commodity">
		    <?php
				$m = 1;
				foreach ($product as $item2) { 
			    if($m > 6)  continue;
				$pic = $item2['thumbnail_url']; // logo图片是你自己放到文件夹里的  
				if (empty($pic)){
					$pic = $item2['thumbnail_file'];
				} 
				if(strpos(strtolower($pic),"http://")!==0 && strpos(strtolower($pic),"https://")!==0) {
				   $pic_file=base_url()."uploads/sp/".$pic."?t=".$ts;
				} else {
				   $pic_file=$pic;
				}					
			?>
			<li <?php if (($m%3) == 0){ ?>style="margin-right: 0;"<?php } ?> >
					<?php if (!empty($_SESSION['user']['userid'])){?>
						<?php if (empty($item2['tx_url'])){ ?>
							<a href="<?=base_url()?>get_product/product_detail/<?=$item2['epid']?>/?web=Y">
						<?php }else{ ?>
							<a href="http://www.shajiawang.com/site/mall/extExchangeService/?userid=<?=$_SESSION['user']['userid']?>&epid=<?=$item2['epid']?>&form=sbn" data-transition="slide">			
						<?php } ?>
					<?php }else { ?>
					<a href="<?=base_url()?>get_login/?web=Y" >
					<?php } ?>
						<img src="<?=$pic_file?>" style="overflow:overflow;height:100px" id="<?=$item2['name']?>" name="<?=$item2['name']?>"/>
					   <span align="center"><?=$item2['name']?></span>
					   <span class="price" style="font-size:0.65em">RMB : <?=sprintf("%0.0f", $item2['point_price']) ?> <s></s></span>					
				</a>
			</li>			
			<?php
				$m++;
				}
			?>
		</ul>	
    </div>
	<?php  if(!$logined) {  ?>
	          <article>
			     <div style="margin-bottom: 0;" align="center">
				   <a href="<?=base_url() ?>get_login/"><?php echo $this->lang->line('index_login'); ?></a>
				 </div>
			  </article>
	<?php  }  ?>
	<article>
	   <div align="center" style="font-size:0.65em" >
	     <a href="http://www.miitbeian.gov.cn">www.Sharebonus.com 版權所有 盜版必究</br>發明專利號: I502528 蘇ICP備17039401號</a>
		 <!-- a href="http://www.miitbeian.gov.cn">版权所有&nbsp;&nbsp上海巴根网络科技有限公司<br/>沪ICP备14035708号</a -->
	   </div>
	 </article>
<div style="height: 2.5rem;"></div>
<script type="text/javascript"> 
    $(document).ready(
	    function() {
		      $('#lang').on('change',function() {
		          window.location.href="/home/?lang="+this.value;
		      });
	});
	<?php if ($_SESSION['user']['user_type'] == "C" ){ ?>
	function qc(){
		$.ajax({
			url: "<?=base_url()?>get_bonus/bonus_step1/",
			data:"userid=<?php echo $userid; ?>",
			type:"POST",
			dataType:'text',

			success: function(msg){
				if(msg) {
					// alert(msg);
					var obj = jQuery.parseJSON(msg);
					// alert( obj.tx_url);
					if (obj.recode == '1'){
						document.location.href="<?=base_url()?>get_bonus/qrcode/"+obj.txid+"/Y/";	
					} 
				}
			},

			error:function(){ 
				var obj = jQuery.parseJSON(msg);
				alert(obj.remsg ); 
			}
		});
	}	
	<?php } ?>	

	<?php if ($_SESSION['user']['user_type'] == "B" ){ ?>
	function qc2(){
		$.ajax({
			url: "<?=base_url()?>get_exchange/exchange_step1/",
			data:"userid=<?php echo $userid; ?>",
			type:"POST",
			dataType:'text',

			success: function(msg){
				if(msg) {
					// alert(msg);
					var obj = jQuery.parseJSON(msg);
					// alert( obj.tx_url);
					if (obj.recode == '1'){
						document.location.href="<?=base_url()?>get_exchange/qrcode/"+obj.txid+"/Y/";	
					} 
				}
			},

			error:function(){ 
				var obj = jQuery.parseJSON(msg);
				alert(obj.remsg ); 
			}
		});
	}	
	<?php } ?>		
	
	function scan(){
		if(is_weixin()) {
		    var uri="<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>";
			uri=(uri.split('#')[0]);
			// alert(uri);
			$.ajax({
				url: "http://www.sharebonus.com/oauth/weixin/weixinSDK.php",
				data: { wurl:uri },
				type: "POST",
				contentType : "application/x-www-form-urlencoded; charset=utf-8",
				success: function(data) {
					var obj;
					if("string"==(typeof data)) {
					   obj=JSON.parse(data);
					} else  {
					   obj=data;
					}
					var configData={
						debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参數，可以在pc端打开，参數信息会通过log打出，仅在pc端时才会打印。
						appId: obj.appId, // 必填，公众号的唯一标识
						timestamp: obj.timestamp, // 必填，生成签名的时间戳
						nonceStr: obj.nonceStr, // 必填，生成签名的随机串
						signature: obj.signature, // 必填，签名
						jsApiList: [
							'scanQRCode'
						]
					};
					wx.config(configData);
					wx.scanQRCode({
						needResult: 1, 
						scanType: ["qrCode"],
						success: function (res) {
							//alert(res);
							var result = res.resultStr;
							document.location.href=result;
						}
					});
					
				}
			});		   
		} else if(is_kingkr_obj()){
		   qrcode(1);
		} else {
		   alert('<?php echo $this->lang->line('index_scanner'); ?> qrcode scanner');
		}		
	}

	// appbsl 的qrcode callback function
	function qrcodeCallback(result){
		alert(result);
	}
	function gpscallback(result){
　　　　alert(result);
	}
	function logincallback(result) {
		alert(result);
	}

</script>
