<html>
<head>
<title> <?php echo $lang['select_language_title'];?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $ec_charset; ?>" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
</head>

<body class="body">
<div class="main">
<?php include ROOT_PATH . 'upgrade/templates/header.php';?>

<div id="wrapper" class="wrapper">
	<div class="common-head">
        <a href="ucimport.php" class="btn"><?php echo $lang['goto_members_import'];?></a>
        <a href="convert.php" class="btn"><?php echo $lang['goto_charset_convert'];?></a>
    </div>
<form target="_parent" action="" method="post">
<fieldset>
    <legend dir="ltr"><?php echo $lang['lang_select']; ?><i class="r"></i><i class="b"></i></legend>
    <div class="items">
    	<div class="item">
        	<div class="label"><?php echo $lang['lang_title']; ?>：</div>
            <div class="value">
            	<select dir="ltr" onchange="this.form.submit();" name="lang" style="width:300px;">
				<?php
                	foreach($lang['lang_charset'] as $key => $val) {
						if ($updater_lang.'_'.$ec_charset == $key) {
							$lang_selected = 'selected="selected" ';
						} else {
							$lang_selected = '';
						}
                ?>
                		<option <?php echo $lang_selected; ?>value="<?php echo $key; ?>"><?php echo $val; ?></option>
                <?php
					}
                ?>
                </select>
            </div>
        </div>
    	<div class="item">
        	<div class="label"><?php echo $lang['ui_title']; ?>：</div>
            <div class="value">
            	<div class="checkbox_items">
                	<div class="checkbox_item">
                    	<input type="radio" id="ui_1" name="ui" value="ecshop" class="ui_radio" checked="checked" />
                        <label for="ui_1" class="ui_radio_label"><?php echo $lang['ui_ordinary']; ?></label>
                    </div>
                    <div class="checkbox_item">
                    	<input type="radio" id="ui_2" name="ui" value="ucenter" class="ui_radio" />
                        <label for="ui_2" class="ui_radio_label"><?php echo $lang['ui_ucenter']; ?></label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</fieldset>
</form>
<form target="_parent" action="" method="post">
    <fieldset>
        <legend><?php echo $lang['lang_description']; ?><i class="r"></i><i class="b"></i></legend>
        <ul class="list">
            <?php
			$i = 1;
            foreach($lang['lang_desc'] as $desc) {
            ?>
            <li><?php echo $i; $i++; ?>、<?php echo $desc; ?></li>
            <?php
            }
            ?>
        </ul>
    </fieldset>
    <div class="btn_info mt50 mb20">
        <input type="hidden" name="step" value="readme" />
        <input type="hidden" name="lang" value="<?php echo $updater_lang.'_'.$ec_charset; ?>" />
        <input type="submit" class="button" value="<?php echo $lang['next_step'];?><?php echo $lang['readme_page'];?>" />
    </div>
</form>

</div>

<?php include ROOT_PATH . 'upgrade/templates/copyright.php';?>
</div>
</body>
</html>
