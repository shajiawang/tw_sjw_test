<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $lang['upgrade_error_title'];?></title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $ec_charset; ?>" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
</head>

<body class="body">
<div class="main">
	<?php include ROOT_PATH . 'upgrade/templates/header.php';?>
    <div class="wrapper">
        <div class="w_content">
            <?php echo $err_msg;?>
        </div>
    </div>
    <?php include ROOT_PATH . 'upgrade/templates/copyright.php';?>
</div>
</body>
</html>