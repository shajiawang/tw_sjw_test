<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title><?php echo $lang['upgrade_done_title'];?></title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $ec_charset; ?>" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
</head>

<body class="body">
<div class="main">
<?php include ROOT_PATH . 'upgrade/templates/header.php';?>

<div class="wrapper">
	<div class="w_content">
    	<div class="end_left"><img src="images/tangshu1.png"></div>
        <div class="end_right">
            <h1><?php printf($lang['done'], VERSION);?></h1>
            <ul>
                <li><a href="../"><?php echo $lang['go_to_view_my_ecshop'];?></a></li>
                <li><a href="../admin"><?php echo $lang['go_to_view_control_panel'];?></a></li>
            </ul>
        </div>
	</div>
</div>

<?php include ROOT_PATH . 'upgrade/templates/copyright.php';?>
</div>
</body>
</html>