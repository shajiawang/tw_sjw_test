<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $ec_charset; ?>" />
<title><?php echo $lang['checking_title'];?></title>
<link href="styles/general.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/transport.js" charset="<?php echo EC_CHARSET ?>"></script>
<script type="text/javascript" src="js/common.js"></script>
<script type="text/javascript" src="js/draggable.js"></script>
<script type="text/javascript" src="js/check.js"></script>
<script type="text/javascript">
var $_LANG = {};
<?php foreach($lang['js_languages'] as $key => $item): ?>
$_LANG["<?php echo $key;?>"] = "<?php echo $item;?>";
<?php endforeach; ?>
</script>
</head>
<body id="checking" class="body">
<div class="main">
<?php include ROOT_PATH . 'upgrade/templates/header.php';?>
<form method="post">
<div id="wrapper" class="wrapper checking">
    <fieldset>
        <legend><?php echo $lang['basic_config'];?><i class="r"></i><i class="b"></i></legend>
        <ul class="list">
			<?php foreach($config_info as $config_item): ?>
            <li>
				<span class="detail"><?php echo $config_item[0];?></span>
                <span class="route green"><?php echo $config_item[1];?></span>
            </li>
			<?php endforeach;?>
        </ul>
    </fieldset>
    
    <fieldset>
        <legend><?php echo $lang['dir_priv_checking'];?><i class="r"></i><i class="b"></i></legend>
        <ul class="list">
			<?php foreach($dir_checking as $checking_item): ?>
            <li>
				<span class="detail"><?php echo $checking_item[0];?></span>
				<?php if ($checking_item[1] == $lang['can_write']):?>
                <span class="route green"><?php echo $checking_item[1];?></span>
                <?php else:?>
                <span class="route red"><?php echo $checking_item[1];?></span>
                <?php endif;?>
            </li>
            <?php endforeach;?>
        </ul>
    </fieldset>
    
    <fieldset>
        <legend><?php echo $lang['template_writable_checking'];?><i class="r"></i><i class="b"></i></legend>
        <ul class="list">
			<?php if ($has_unwritable_tpl == "yes"):?>
            <?php foreach($template_checking as $checking_item): ?>
            <li>
            	<span class="route red"><?php echo $checking_item;?></span>
            </li>    
            <?php endforeach; ?>
            <?php else:?>
            <li class="success">
            	<span class="green"><?php echo $template_checking;?></span>
            </li>
            <?php endif;?>
        </ul>
    </fieldset>
    
    <?php if (!empty($rename_priv)) :?>
    <fieldset>
        <legend><?php echo $lang['rename_priv_checking']; ?><i class="r"></i><i class="b"></i></legend>
        <ul class="list">
			<?php foreach($rename_priv as $checking_item): ?>
            <li><span class="route red"><?php echo $checking_item;?></span></li>
            <?php endforeach; ?>
        </ul>
    </fieldset> 
    <?php endif;?>
    
    <div class="btn_info mt50 mb20" id="install-btn">
        <input type="button" class="button mr15" value="<?php echo $lang['prev_step'];?><?php echo $lang['readme_page'];?>" onclick="location.href='index.php'" />
        <input type="button" class="button mr15" value="<?php echo $lang['recheck'];?>" onclick="location.href='index.php?step=check'" />
        <input type="button" class="button" value="<?php echo $lang['update_now'];?>" <?php echo $disabled;?> id="js-submit" />
    </div>
    
    <div id="js-monitor_bg" class="js-monitor_bg"></div>
    <div id="js-monitor">
    	<div class="loading_bg">
    		<img id="js-monitor-loading" src='images/loading.gif' />
        </div>
        <div class="con">
    	<fieldset>
            <legend id="js-monitor-title"><?php echo $lang['monitor_title'];?><i class="r"></i><i class="b"></i></legend>
            <div class="head_title">
                <span id="js-monitor-wait-please" class="please"></span>
                <span id="js-monitor-rollback" class="rollback hide"></span>
                <i id="js-monitor-view-detail" class="view"></i>
            </div>
            <iframe id="js-monitor-notice" src="templates/notice.htm" height="70px" width="100%" style="display:none;"></iframe>
            <a href="javascript:void(0);" id="js-monitor-close" class="js-monitor-close"></a>
        </fieldset>
        </div>
    </div>
</div>

<div id="copyright">
    <div id="copyright-inside">
      <?php include ROOT_PATH . 'upgrade/templates/copyright.php';?></div>
</div>
</form>
</div>
</body>
</html>
