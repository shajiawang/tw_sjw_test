<html>
<head>
<title> <?php echo $lang['readme_title'];?> </title>
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo $ec_charset; ?>" />
<link href="styles/general.css" rel="stylesheet" type="text/css" />
</head>

<body class="body">
<div class="main">
<?php include ROOT_PATH . 'upgrade/templates/header.php';?>

<div id="wrapper" class="wrapper">
<fieldset>
	<legend><?php echo $lang['method']; ?><i class="r"></i><i class="b"></i></legend>
    <p class="tit"><?php printf($lang['notice'], $new_version);?></p>
    <ul class="list">
        <li>1、<?php echo $lang['usage1'];?></li>
        <li>2、<?php echo $lang['usage2'];?></li>
        <li>3、<?php printf($lang['usage3'], $old_version);?></li>
        <li>4、<?php printf($lang['usage4'], $new_version);?></li>
        <li>5、<?php echo $lang['usage5'];?></li>
        <li>6、<?php echo $lang['usage6'];?></li>
    </ul>
</fieldset>
<div class="btn_info mt50 mb20">
    <input type="submit" id="js-submit" class="button" value="<?php echo $lang['next_step'];?><?php echo $lang['check_system_environment'];?>" onClick="top.location='index.php?step=check&ui=<?php echo $ui;?>'" />
</div>
<fieldset>
	<legend><?php echo $lang['faq']; ?><i class="r"></i><i class="b"></i></legend>
    <iframe src="templates/faq_<?php echo $updater_lang;?>_<?php echo $ec_charset;?>.htm" width="100%" height="500"></iframe>
</fieldset>
</div>
<?php include ROOT_PATH . 'upgrade/templates/copyright.php';?>
</div>
</body>
</html>
