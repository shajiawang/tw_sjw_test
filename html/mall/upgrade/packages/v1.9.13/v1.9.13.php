<?php

/**
 * 大商创v1.9 升级程序
 * ============================================================================
 * 版权所有 2005-2016 上海商创网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecmoban.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: fuxi $
 * $Date: 2009-12-14 17:22:19 +0800 (周一, 2009-12-14) $
 * $Id: v2.1.0.php 16882 2009-12-14 09:22:19Z fuxi $
 */

class up_v1_9_13
{
    /**
     * 本升级包中SQL文件存放的位置（相对于升级包所在的路径）。每个版本类必须有该属性。
     */
    var $sql_files = array(
                            'structure' => 'structure.sql',
                            'data' => array(
                                            'zh_cn_utf-8' => 'data_zh_cn_utf-8.sql',
                            )
        );
    
   /**
     * 本升级包是否进行智能化的查询操作。每个版本类必须有该属性。
     */
    var $auto_match = true;

    function __construct(){}
    function up_v1_9_13(){}

    /**
     * 提供给控制器的 接口 函数。每个版本类必须有该函数。
     */
    function update_database_optionally()
    {
        global $db, $ecs; 
		
		include_once(ROOT_PATH . 'includes/inc_constant.php');
		
		$sql = "ALTER TABLE " .$ecs->table('coupons'). " ADD `spec_cat` TEXT NOT NULL AFTER `cou_goods` ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('coupons'). " ADD `cou_ok_cat` TEXT NOT NULL AFTER `cou_ok_goods` ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('goods_activity'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `is_hot` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('bonus_type'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `min_goods_amount` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('topic'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `description` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('favourable_activity'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `userFav_type` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('wholesale'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `enabled` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('exchange_goods'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `goods_id` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('exchange_goods'). " DROP PRIMARY KEY ,
		ADD UNIQUE (
		`goods_id`
		);";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('exchange_goods'). " ADD `eid` INT( 10 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('presale_activity'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `is_finished` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('coupons'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `cou_title` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);

		$sql = "ALTER TABLE " .$ecs->table('gift_gard_type'). " ADD `review_status` TINYINT( 1 ) UNSIGNED NOT NULL DEFAULT '1' AFTER `gift_number` ,
		ADD `review_content` VARCHAR( 1000 ) NOT NULL AFTER `review_status` ,
		ADD INDEX ( `review_status` ) ;";
		$db->query($sql);
		
		
		$sql = "UPDATE " .$ecs->table('coupons'). " SET review_status = 3 WHERE ru_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('goods_activity'). " SET review_status = 3 WHERE user_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('bonus_type'). " SET review_status = 3 WHERE user_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('topic'). " SET review_status = 3 WHERE user_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('favourable_activity'). " SET review_status = 3 WHERE user_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('wholesale'). " SET review_status = 3 WHERE user_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('exchange_goods'). " SET review_status = 3 WHERE user_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('presale_activity'). " SET review_status = 3 WHERE user_id = 0 ;";
		$db->query($sql);
		
		$sql = "UPDATE " .$ecs->table('gift_gard_type'). " SET review_status = 3 WHERE ru_id = 0 ;";
		$db->query($sql);
	}
    
    /**
     * 提供给控制器的 接口 函数。每个版本类必须有该函数。
     */
    function update_files()
    {
        $result = file_mode_info(ROOT_PATH . 'data/');

        if ($result < 2)
        {
            die('ERROR, ' . ROOT_PATH . 'data/ isn\'t a writeable directory.');
        }

        if (!file_exists(ROOT_PATH . 'data/config.php'))
        {
            if (file_exists(ROOT_PATH . 'includes/config.php'))
            {            
                copy(ROOT_PATH . 'includes/config.php', ROOT_PATH . 'data/config.php');
                //unlink(ROOT_PATH . 'includes/config.php');
            }
            else
            {
                die("ERROR, can't find config.php.");
            }
        }

        if (!file_exists(ROOT_PATH . 'data/install.lock.php'))
        {
            if (file_exists(ROOT_PATH . 'includes/install.lock.php'))
            {
                copy(ROOT_PATH . 'includes/install.lock.php', ROOT_PATH . 'data/install.lock.php');
                //unlink(ROOT_PATH . 'includes/install.lock.php');
            }
            else
            {
                die("ERROR, can't find install.lock.php.");
            }
        }
    }
}

?>