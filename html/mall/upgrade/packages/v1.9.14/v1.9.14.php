<?php

/**
 * 大商创v1.9 升级程序
 * ============================================================================
 * 版权所有 2005-2016 上海商创网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecmoban.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: fuxi $
 * $Date: 2009-12-14 17:22:19 +0800 (周一, 2009-12-14) $
 * $Id: v2.1.0.php 16882 2009-12-14 09:22:19Z fuxi $
 */

class up_v1_9_14
{
    /**
     * 本升级包中SQL文件存放的位置（相对于升级包所在的路径）。每个版本类必须有该属性。
     */
    var $sql_files = array(
                            'structure' => 'structure.sql',
                            'data' => array(
                                            'zh_cn_utf-8' => 'data_zh_cn_utf-8.sql',
                            )
        );
    
   /**
     * 本升级包是否进行智能化的查询操作。每个版本类必须有该属性。
     */
    var $auto_match = true;

    function __construct(){}
    function up_v1_9_14(){}

    /**
     * 提供给控制器的 接口 函数。每个版本类必须有该函数。
     */
    function update_database_optionally()
    {
        global $db, $ecs; 
		
		include_once(ROOT_PATH . 'includes/inc_constant.php');
		
		$sql = "UPDATE " .$ecs->table('admin_action'). " SET `seller_show` = 0 WHERE `action_code` = 'order_back_cause';";
		$db->query($sql);
		
		$sql = "CREATE TABLE IF NOT EXISTS " .$ecs->table('touch_topic'). " (
  `topic_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '''''',
  `intro` text NOT NULL,
  `start_time` int(11) NOT NULL DEFAULT '0',
  `end_time` int(10) NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `template` varchar(255) NOT NULL DEFAULT '''''',
  `css` text NOT NULL,
  `topic_img` varchar(255) DEFAULT NULL,
  `title_pic` varchar(255) DEFAULT NULL,
  `base_style` char(6) DEFAULT NULL,
  `htmls` mediumtext,
  `keywords` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `review_status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `review_content` varchar(1000) NOT NULL DEFAULT '',
  KEY `topic_id` (`topic_id`),
  KEY `review_status` (`review_status`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
		$db->query($sql);
		
	}
    
    /**
     * 提供给控制器的 接口 函数。每个版本类必须有该函数。
     */
    function update_files()
    {
        $result = file_mode_info(ROOT_PATH . 'data/');

        if ($result < 2)
        {
            die('ERROR, ' . ROOT_PATH . 'data/ isn\'t a writeable directory.');
        }

        if (!file_exists(ROOT_PATH . 'data/config.php'))
        {
            if (file_exists(ROOT_PATH . 'includes/config.php'))
            {            
                copy(ROOT_PATH . 'includes/config.php', ROOT_PATH . 'data/config.php');
                //unlink(ROOT_PATH . 'includes/config.php');
            }
            else
            {
                die("ERROR, can't find config.php.");
            }
        }

        if (!file_exists(ROOT_PATH . 'data/install.lock.php'))
        {
            if (file_exists(ROOT_PATH . 'includes/install.lock.php'))
            {
                copy(ROOT_PATH . 'includes/install.lock.php', ROOT_PATH . 'data/install.lock.php');
                //unlink(ROOT_PATH . 'includes/install.lock.php');
            }
            else
            {
                die("ERROR, can't find install.lock.php.");
            }
        }
    }
}

?>