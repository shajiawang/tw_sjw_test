<?php

/**
 * 大商创v2.0 升级程序
 * ============================================================================
 * 版权所有 2005-2016 上海商创网络科技有限公司，并保留所有权利。
 * 网站地址: http://www.ecmoban.com
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: fuxi $
 * $Date: 2009-12-14 17:22:19 +0800 (周一, 2009-12-14) $
 * $Id: v2.1.0.php 16882 2009-12-14 09:22:19Z fuxi $
 */

class up_v2_1_2
{
    /**
     * 本升级包中SQL文件存放的位置（相对于升级包所在的路径）。每个版本类必须有该属性。
     */
    var $sql_files = array(
                            'structure' => 'structure.sql',
                            'data' => array(
                                            'zh_cn_utf-8' => 'data_zh_cn_utf-8.sql',
                            )
        );
    
   /**
     * 本升级包是否进行智能化的查询操作。每个版本类必须有该属性。
     */
    var $auto_match = true;

    function __construct(){}
    function up_v2_1_2(){}

    /**
     * 提供给控制器的 接口 函数。每个版本类必须有该函数。
     */
    function update_database_optionally()
    {
        global $db, $ecs; 
		
		include_once(ROOT_PATH . 'includes/inc_constant.php');
		
		$sql = "ALTER TABLE " .$GLOBALS['ecs']->table('shop_config'). " ADD `shop_group` VARCHAR( 250 ) NOT NULL AFTER `sort_order` ;";
		$GLOBALS['db']->query($sql);

		$sql = "ALTER TABLE " .$GLOBALS['ecs']->table('order_info'). " ADD `confirm_take_time` INT( 10 ) UNSIGNED NOT NULL DEFAULT '0' AFTER `shipping_time`;";
		$GLOBALS['db']->query($sql);
		
		$sql = "ALTER TABLE " .$GLOBALS['ecs']->table('merchants_shop_information'). " ADD INDEX `merchants_audit` ( `merchants_audit` ) ;";
		$GLOBALS['db']->query($sql);
		
		$sql = "ALTER TABLE " .$GLOBALS['ecs']->table('merchants_shop_information'). " ADD INDEX `is_street` ( `is_street` ) ;";
		$GLOBALS['db']->query($sql);
		
		$sql = "ALTER TABLE " .$GLOBALS['ecs']->table('snatch_log'). " 
MODIFY COLUMN `log_id`  int(10) UNSIGNED NOT NULL AUTO_INCREMENT FIRST ,
MODIFY COLUMN `snatch_id`  int(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `log_id`,
MODIFY COLUMN `user_id`  int(10) UNSIGNED NOT NULL DEFAULT 0 AFTER `snatch_id`;";
		$GLOBALS['db']->query($sql);

		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'marticle';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'marticle_id';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'customer_service';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'review_goods';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'delete_seller';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'grade_apply_time';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'apply_options';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'commission_model';";
		$GLOBALS['db']->query($sql);
		
		$sql = "UPDATE " .$GLOBALS['ecs']->table('shop_config'). " SET shop_group = 'seller' WHERE code = 'merchants_prefix';";
		$GLOBALS['db']->query($sql);
		
		$sql = "INSERT INTO " .$GLOBALS['ecs']->table('shop_config'). " ( `parent_id`, `code`, `type`, `store_range`, `store_dir`, `value`, `sort_order`) VALUES ( '7', 'show_give_integral', 'select', '1,0', '', '0', '1');";
		$GLOBALS['db']->query($sql);
	}
    
    /**
     * 提供给控制器的 接口 函数。每个版本类必须有该函数。
     */
    function update_files()
    {
        $result = file_mode_info(ROOT_PATH . 'data/');

        if ($result < 2)
        {
            die('ERROR, ' . ROOT_PATH . 'data/ isn\'t a writeable directory.');
        }

        if (!file_exists(ROOT_PATH . 'data/config.php'))
        {
            if (file_exists(ROOT_PATH . 'includes/config.php'))
            {            
                copy(ROOT_PATH . 'includes/config.php', ROOT_PATH . 'data/config.php');
                //unlink(ROOT_PATH . 'includes/config.php');
            }
            else
            {
                die("ERROR, can't find config.php.");
            }
        }

        if (!file_exists(ROOT_PATH . 'data/install.lock.php'))
        {
            if (file_exists(ROOT_PATH . 'includes/install.lock.php'))
            {
                copy(ROOT_PATH . 'includes/install.lock.php', ROOT_PATH . 'data/install.lock.php');
                //unlink(ROOT_PATH . 'includes/install.lock.php');
            }
            else
            {
                die("ERROR, can't find install.lock.php.");
            }
        }
    }
}

?>