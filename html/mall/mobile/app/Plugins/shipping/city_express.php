<?php
//阿莫之家社区 QQ 465420700
class city_express
{
	/**
     * 配置信息
     */
	public $configure;

	public function __construct($cfg = array())
	{
		foreach ($cfg as $key => $val) {
			$this->configure[$val['name']] = $val['value'];
		}
	}

	public function calculate($goods_weight, $goods_amount)
	{
		if ((0 < $this->configure['free_money']) && ($this->configure['free_money'] <= $goods_amount)) {
			return 0;
		}
		else {
			return $this->configure['base_fee'];
		}
	}

	public function query($invoice_sn)
	{
		return $invoice_sn;
	}

	public function api($invoice_sn = '')
	{
		return false;
	}
}


?>
