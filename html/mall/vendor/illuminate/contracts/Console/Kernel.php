<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Console;

interface Kernel
{
	public function handle($input, $output = NULL);

	public function call($command, array $parameters = array());

	public function queue($command, array $parameters = array());

	public function all();

	public function output();
}


?>
