<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Bus;

interface Dispatcher
{
	public function dispatch($command);

	public function dispatchNow($command, $handler = NULL);

	public function pipeThrough(array $pipes);
}


?>
