<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Auth;

interface CanResetPassword
{
	public function getEmailForPasswordReset();

	public function sendPasswordResetNotification($token);
}


?>
