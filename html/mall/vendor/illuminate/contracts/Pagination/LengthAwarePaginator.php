<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Pagination;

interface LengthAwarePaginator extends Paginator
{
	public function getUrlRange($start, $end);

	public function total();

	public function lastPage();
}

?>
