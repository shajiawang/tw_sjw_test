<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Support\Facades;

class Blade extends Facade
{
	static protected function getFacadeAccessor()
	{
		return static::$app['view']->getEngineResolver()->resolve('blade')->getCompiler();
	}
}

?>
