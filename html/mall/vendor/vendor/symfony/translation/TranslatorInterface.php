<?php
//阿莫之家社区 QQ 465420700
namespace Symfony\Component\Translation;

interface TranslatorInterface
{
	public function trans($id, array $parameters = array(), $domain = NULL, $locale = NULL);

	public function transChoice($id, $number, array $parameters = array(), $domain = NULL, $locale = NULL);

	public function setLocale($locale);

	public function getLocale();
}


?>
