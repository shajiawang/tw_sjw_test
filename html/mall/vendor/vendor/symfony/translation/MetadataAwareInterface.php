<?php
//阿莫之家社区 QQ 465420700
namespace Symfony\Component\Translation;

interface MetadataAwareInterface
{
	public function getMetadata($key = '', $domain = 'messages');

	public function setMetadata($key, $value, $domain = 'messages');

	public function deleteMetadata($key = '', $domain = 'messages');
}


?>
