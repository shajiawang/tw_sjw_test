<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Support\Facades;

class Notification extends Facade
{
	static public function fake()
	{
		static::swap(new \Illuminate\Support\Testing\Fakes\NotificationFake());
	}

	static protected function getFacadeAccessor()
	{
		return 'Illuminate\\Notifications\\ChannelManager';
	}
}

?>
