<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Database\Eloquent;

class RelationNotFoundException extends \RuntimeException
{
	static public function make($model, $relation)
	{
		$class = get_class($model);
		return new static('Call to undefined relationship [' . $relation . '] on model [' . $class . '].');
	}
}

?>
