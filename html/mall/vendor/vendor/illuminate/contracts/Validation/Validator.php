<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Validation;

interface Validator extends \Illuminate\Contracts\Support\MessageProvider
{
	public function fails();

	public function failed();

	public function sometimes($attribute, $rules,  $callback);

	public function after($callback);
}

?>
