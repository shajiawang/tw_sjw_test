<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Hashing;

interface Hasher
{
	public function make($value, array $options = array());

	public function check($value, $hashedValue, array $options = array());

	public function needsRehash($hashedValue, array $options = array());
}


?>
