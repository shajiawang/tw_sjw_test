<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Http;

interface Kernel
{
	public function bootstrap();

	public function handle($request);

	public function terminate($request, $response);

	public function getApplication();
}


?>
