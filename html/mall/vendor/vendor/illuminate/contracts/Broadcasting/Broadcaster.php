<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Broadcasting;

interface Broadcaster
{
	public function auth($request);

	public function validAuthenticationResponse($request, $result);

	public function broadcast(array $channels, $event, array $payload = array());
}


?>
