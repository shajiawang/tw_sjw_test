<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Queue;

class EntityNotFoundException extends \InvalidArgumentException
{
	public function __construct($type, $id)
	{
		$id = (string) $id;
		parent::__construct('Queueable entity [' . $type . '] not found for ID [' . $id . '].');
	}
}

?>
