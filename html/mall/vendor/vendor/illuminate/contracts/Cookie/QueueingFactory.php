<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Cookie;

interface QueueingFactory extends Factory
{
	public function queue(...$parameters);

	public function unqueue($name);

	public function getQueuedCookies();
}

?>
