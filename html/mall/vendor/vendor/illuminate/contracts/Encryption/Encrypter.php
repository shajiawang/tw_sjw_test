<?php
//阿莫之家社区 QQ 465420700
namespace Illuminate\Contracts\Encryption;

interface Encrypter
{
	public function encrypt($value, $serialize = true);

	public function decrypt($payload, $unserialize = true);
}


?>
