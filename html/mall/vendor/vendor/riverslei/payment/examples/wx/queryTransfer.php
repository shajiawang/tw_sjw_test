<?php
//阿莫之家社区 QQ 465420700
require_once __DIR__ . '/../../autoload.php';
date_default_timezone_set('Asia/Shanghai');
$wxConfig = require_once __DIR__ . '/../wxconfig.php';
$data = array('trans_no' => '1489852933');

try {
	$ret = \Payment\Client\Query::run(\Payment\Config::WX_CHARGE, $wxConfig, $data);
}
catch (\Payment\Common\PayException $e) {
	echo $e->errorMessage();
	exit();
}

echo json_encode($ret, JSON_UNESCAPED_UNICODE);

?>
