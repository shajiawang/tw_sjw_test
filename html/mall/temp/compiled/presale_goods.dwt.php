<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="<?php echo $this->_var['keywords']; ?>" />
<meta name="Description" content="<?php echo $this->_var['description']; ?>" />

<title><?php echo $this->_var['page_title']; ?></title>



<link rel="shortcut icon" href="favicon.ico" />
<?php echo $this->fetch('library/js_languages_new.lbi'); ?>
<link rel="stylesheet" type="text/css" href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/css/other/presale.css" />
<link rel="stylesheet" type="text/css" href="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/css/suggest.css" />
</head>

<body>
    <?php echo $this->fetch('library/page_header_presale.lbi'); ?>
    <div class="full-main-n">
        <div class="w w1200 relative">
        <?php echo $this->fetch('library/ur_here.lbi'); ?>
        </div>
    </div>
    <div class="container">
        <div class="w w1200">
            <div class="product-info">
                <?php echo $this->fetch('library/goods_gallery.lbi'); ?>
                <div class="product-wrap<?php if ($this->_var['goods']['user_id']): ?> product-wrap-min<?php endif; ?>">
                    <form action="presale.php?act=buy" method="post" name="ECS_FORMBUY" id="ECS_FORMBUY">
                        <div class="name"><?php echo $this->_var['presale']['goods_name']; ?></div>
                        <?php if ($this->_var['goods']['goods_brief']): ?>
                        <div class="newp"><?php echo $this->_var['goods']['goods_brief']; ?></div>
                        <?php endif; ?>
                        
                        <div class="activity-title">
                            <div class="activity-type"><i class="icon icon-promotion"></i><?php echo $this->_var['lang']['presell']; ?></div>
                            <div class="sk-time-cd">
                                <div class="sk-cd-tit"><?php if ($this->_var['presale']['no_start'] == 1): ?><?php echo $this->_var['lang']['Start_from_presell']; ?><?php else: ?><?php echo $this->_var['lang']['residual_time']; ?><?php endif; ?></div>
                                <div class="cd-time time" ectype="time" data-time="<?php if ($this->_var['presale']['no_start'] == 1): ?><?php echo $this->_var['presale']['formated_start_date']; ?><?php else: ?><?php echo $this->_var['presale']['formated_end_date']; ?><?php endif; ?>">
                                    <div class="days">00</div>
                                    <span class="split">:</span>
                                    <div class="hours">00</div>
                                    <span class="split">:</span>
                                    <div class="minutes">00</div>
                                    <span class="split">:</span>
                                    <div class="seconds">00</div>
                                </div>
                            </div>
                            <div class="activity-message-item">
                                <i class="sprite-person"></i><em class="J-count"><?php echo $this->_var['pre_num']; ?></em><?php echo $this->_var['lang']['subscribe_p']; ?>
                            </div>
                        </div>
                        
                        <div class="summary">
                        	<div class="summary-price-wrap">
                                <div class="s-p-w-wrap">
                                    <div class="summary-item si-shop-price">
                                        <div class="si-tit"><?php echo $this->_var['lang']['presale_price']; ?></div>
                                        <div class="si-warp">
                                            <strong class="shop-price" id="ECS_SHOPPRICE"><em>￥</em><?php echo $this->_var['presale']['shop_price']; ?></strong>
                                        </div>
                                    </div>
                                    <div class="summary-item si-market-price">
                                        <div class="si-tit"><?php echo $this->_var['lang']['Deposit_new']; ?></div>
                                        <div class="si-warp"><div class="price"><?php echo $this->_var['presale']['formated_deposit']; ?></div></div>
                                    </div>
                                    <div class="summary-item si-market-price">
                                        <div class="si-tit"><?php echo $this->_var['lang']['Retainage_new']; ?></div>
                                        <div class="si-warp"><div class="price"><?php echo $this->_var['presale']['formated_final_payment']; ?></div></div>
                                    </div>
                                    <div class="si-info">
                                        <dl class="sp-rule">
                                            <dt><?php echo $this->_var['lang']['Pre_sale_rules']; ?><i></i></dt>
                                            <dd>
                                                <i></i><em></em><b class="close"></b>
                                                <ul id="presell-rule">
                                                    <?php echo $this->_var['lang']['presell_desc']; ?><?php echo $this->_var['dwt_shop_name']; ?><?php echo $this->_var['lang']['presell_desc_one']; ?><?php echo $this->_var['dwt_shop_name']; ?><?php echo $this->_var['lang']['presell_desc_two']; ?>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div class="clear"></div>
                                </div>
                            </div>
							<div class="summary-basic-info">
                                <div class="summary-item is-stock">
                                    <div class="si-tit"><?php echo $this->_var['lang']['Distribution_new']; ?></div>
                                    <div class="si-warp">
                                        <span class="initial-area">
                                            <?php if ($this->_var['adress']['city']): ?>
                                                <?php echo $this->_var['adress']['city']; ?>
                                            <?php else: ?>
                                                <?php echo $this->_var['basic_info']['city']; ?>
                                            <?php endif; ?> 
                                        </span>
                                        <span>至</span>
                                        <div class="store-selector">
                                            <div class="text-select" id="area_address" ectype="areaSelect"></div>
                                        </div>
                                        <div class="store-warehouse">
                                        </div>
                                    </div>
                                </div>

                                <div class="summary-item is-service">
                                    <div class="si-tit"><?php echo $this->_var['lang']['service']; ?></div>
                                    <div class="si-warp">
                                        <div class="fl"> 
                                        <?php if ($this->_var['goods']['user_id'] > 0): ?>
                                            由 <a href="<?php echo $this->_var['goods']['store_url']; ?>" class="link-red" target="_blank"><?php echo $this->_var['goods']['rz_shopName']; ?></a> 发货并提供售后服务
                                        <?php else: ?>
                                            由 <a href="javascript:void(0)" class="link-red"><?php echo $this->_var['goods']['rz_shopName']; ?></a> 发货并提供售后服务
                                        <?php endif; ?>
                                        </div>
                                        <div class="fl pl10" id="user_area_shipping"></div>
                                    </div>
                                </div>
                            
                                <?php if ($this->_var['presale']['xiangou_num'] && $this->_var['xiangou'] == 1): ?>
                                <div class="summary-item is-xiangou">
                                    <div class="si-tit"><?php echo $this->_var['lang']['gb_limited']; ?></div>
                                    <div class="si-warp">
                                        <em id="restrict_amount" ectype="restrictNumber" data-value="<?php echo $this->_var['presale']['xiangou_num']; ?>"><?php echo $this->_var['presale']['xiangou_num']; ?></em>
                                        <span><?php if ($this->_var['goods']['goods_unit']): ?><?php echo $this->_var['goods']['goods_unit']; ?><?php else: ?><?php echo $this->_var['goods']['measure_unit']; ?><?php endif; ?></span>
                                        <span>（<?php echo $this->_var['lang']['js_languages']['Already_buy']; ?>：<em id="orderG_number" ectype="orderGNumber"><?php echo empty($this->_var['orderG_number']) ? '0' : $this->_var['orderG_number']; ?></em> <?php if ($this->_var['goods']['goods_unit']): ?><?php echo $this->_var['goods']['goods_unit']; ?><?php else: ?><?php echo $this->_var['goods']['measure_unit']; ?><?php endif; ?>）</span>
                                    </div>
                                </div>
                                <?php endif; ?>
                            
                                <?php $_from = $this->_var['specification']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('spec_key', 'spec');if (count($_from)):
    foreach ($_from AS $this->_var['spec_key'] => $this->_var['spec']):
?>
                                <?php if ($this->_var['spec']['values']): ?>
                                <div class="summary-item is-attr goods_info_attr" ectype="is-attr" data-type="<?php if ($this->_var['spec']['attr_type'] == 1): ?>radio<?php else: ?>checkeck<?php endif; ?>">
                                    <div class="si-tit"><?php echo $this->_var['spec']['name']; ?></div>
                                    <?php if ($this->_var['cfg']['goodsattr_style'] == 1): ?>
                                    <div class="si-warp">
                                        <ul>
                                        <?php $_from = $this->_var['spec']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'value');$this->_foreach['attrvalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['attrvalues']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['value']):
        $this->_foreach['attrvalues']['iteration']++;
?>  
                                        <?php if ($this->_var['spec']['is_checked'] > 0): ?>
                                        <li class="item <?php if ($this->_var['value']['checked'] == 1 && $this->_var['cfg']['add_shop_price'] == 1): ?> selected<?php endif; ?>" date-rev="<?php echo $this->_var['value']['img_site']; ?>" data-name="<?php echo $this->_var['value']['id']; ?>">
                                            <b></b>
                                            <a href="javascript:void(0);">
                                                <?php if ($this->_var['value']['img_flie']): ?>
                                                <img src="<?php echo $this->_var['value']['img_flie']; ?>" width="24" height="24" />
                                                <?php endif; ?>
                                                <i><?php echo $this->_var['value']['label']; ?></i>
                                                <input id="spec_value_<?php echo $this->_var['value']['id']; ?>" type="<?php if ($this->_var['spec']['attr_type'] == 2): ?>checkbox<?php else: ?>radio<?php endif; ?>" data-attrtype="<?php if ($this->_var['spec']['attr_type'] == 2): ?>2<?php else: ?>1<?php endif; ?>" name="spec_<?php echo $this->_var['spec_key']; ?>" value="<?php echo $this->_var['value']['id']; ?>" autocomplete="off" class="hide" />
                                                <?php if ($this->_var['value']['checked'] == 1): ?>
                                                <script type="text/javascript">
                                                    $(function(){
                                                        <?php if ($this->_var['cfg']['add_shop_price'] == 1): ?>
                                                        $("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", true);
                                                        <?php else: ?>
                                                        $("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", false);
                                                        <?php endif; ?>
                                                    });
                                                </script>
                                                <?php endif; ?>
                                            </a>
                                        </li>
                                        <?php else: ?>
                                        <li class="item <?php if ($this->_var['key'] == 0 && $this->_var['cfg']['add_shop_price'] == 1): ?> selected<?php endif; ?>">
                                            <b></b>
                                            <a href="javascript:void(0);" name="<?php echo $this->_var['value']['id']; ?>" class="noimg">
                                                <i><?php echo $this->_var['value']['label']; ?></i>
                                                <input id="spec_value_<?php echo $this->_var['value']['id']; ?>" type="<?php if ($this->_var['spec']['attr_type'] == 2): ?>checkbox<?php else: ?>radio<?php endif; ?>" data-attrtype="<?php if ($this->_var['spec']['attr_type'] == 2): ?>2<?php else: ?>1<?php endif; ?>" name="spec_<?php echo $this->_var['spec_key']; ?>" value="<?php echo $this->_var['value']['id']; ?>" autocomplete="off" class="hide" /></a> 
                                                <?php if ($this->_var['key'] == 0): ?>
                                                <script type="text/javascript">
                                                    $(function(){
                                                        <?php if ($this->_var['cfg']['add_shop_price'] == 1): ?>
                                                        $("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", true);
                                                        <?php else: ?>
                                                        $("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", false);
                                                        <?php endif; ?>
                                                    });
                                                </script>
                                                <?php endif; ?>											
                                            </a>
                                        </li>									
                                        <?php endif; ?>
                                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                        </ul>
                                    </div>
                                    <?php else: ?>
                                    ...
                                    <?php endif; ?>
                                </div>
                                <?php endif; ?>
                                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            
                                <div class="summary-item is-number">
                                    <div class="si-tit">数量</div>
                                    <div class="si-warp">
                                        <div class="amount-warp">
                                            <input class="text buy-num" ectype="quantity" value="1" name="number" defaultnumber="1">
                                            <div class="a-btn">
                                                <a href="javascript:void(0);" class="btn-add" ectype="btnAdd"><i class="iconfont icon-up"></i></a>
                                                <a href="javascript:void(0);" class="btn-reduce btn-disabled" ectype="btnReduce"><i class="iconfont icon-down"></i></a>
                                                <input type="hidden" name="perNumber" id="perNumber" ectype="perNumber" value="0">
                                                <input type="hidden" name="perMinNumber" id="perMinNumber" ectype="perMinNumber" value="1">
                                            </div>
                                            <input name="confirm_type" id="confirm_type" type="hidden" value="3" />
                                        </div>
                                        <?php if ($this->_var['cfg']['show_goodsnumber']): ?>
                                        <span><?php echo $this->_var['lang']['goods_inventory']; ?>&nbsp;<em id="goods_attr_num" ectype="goods_attr_num"></em>&nbsp;<?php if ($this->_var['goods']['goods_unit']): ?><?php echo $this->_var['goods']['goods_unit']; ?><?php else: ?><?php echo $this->_var['goods']['measure_unit']; ?><?php endif; ?></span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            	<div class="clear"></div>
                            </div>
                            <div class="choose-btns ml60 clearfix">
                            	<input name="warehouse_id" value="<?php echo $this->_var['region_id']; ?>" type="hidden" />
                                <input name="area_id" value="<?php echo $this->_var['area_id']; ?>" type="hidden" />
                                <input name="goods_spec" value="" type="hidden" />
                                <input type="hidden" name="presale_id" value="<?php echo $this->_var['presale']['act_id']; ?>" />
                                
                                <input type="hidden" value="<?php echo $this->_var['user_id']; ?>" id="user_id" name="user_id">
                                <input type="hidden" value="<?php echo $this->_var['goods_id']; ?>" id="good_id" name="good_id">
                                <input type="hidden" value="<?php echo $this->_var['region_id']; ?>" id="region_id" name="region_id">
                                
                                <?php if ($this->_var['presale']['no_start'] == 1): ?>
                                    <a href="javascript:;" class="btn-append btn_disabled"><?php echo $this->_var['lang']['presale_no_start']; ?></a>
                                <?php elseif ($this->_var['presale']['already_over'] == 1): ?>
                                    <a href="javascript:;" class="btn-append btn_disabled"><?php echo $this->_var['lang']['presale_end']; ?></a>
                                <?php else: ?>
                                    <a href="javascript:presale_submit()" class="btn-append presale_submit"><?php echo $this->_var['lang']['Pay_deposit']; ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </form>
                </div>
                <?php if (! $this->_var['goods']['user_id']): ?>
				<?php if ($this->_var['look_top']): ?>
                <div class="track">
                    <div class="track_warp">
                    	<div class="track-tit"><h3>看了又看</h3><span></span></div>
                        <div class="track-con">
                            <ul>
                                <?php $_from = $this->_var['look_top']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'look_top_0_94254300_1564561177');$this->_foreach['looktop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['looktop']['total'] > 0):
    foreach ($_from AS $this->_var['look_top_0_94254300_1564561177']):
        $this->_foreach['looktop']['iteration']++;
?>
                                <li>
                                	<div class="p-img"><a href="<?php echo $this->_var['look_top_0_94254300_1564561177']['url']; ?>" target="_blank" title="<?php echo $this->_var['look_top_0_94254300_1564561177']['goods_name']; ?>"><img src="<?php echo $this->_var['look_top_0_94254300_1564561177']['goods_thumb']; ?>" width="140" height="140"></a></div>
                                    <div class="p-name"><a href="<?php echo $this->_var['look_top_0_94254300_1564561177']['url']; ?>" target="_blank" title="<?php echo $this->_var['look_top_0_94254300_1564561177']['goods_name']; ?>"><?php echo $this->_var['look_top_0_94254300_1564561177']['goods_name']; ?></a></div>
                                    <div class="price">
                                            <?php echo $this->_var['look_top_0_94254300_1564561177']['shop_price']; ?>								
                                    </div>
                                </li>
                                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            </ul>
                        </div>
                        <div class="track-more">
                            <a href="javascript:void(0);" class="sprite-up"><i class="iconfont icon-up"></i></a>
                            <a href="javascript:void(0);" class="sprite-down"><i class="iconfont icon-down"></i></a>
                        </div>
                    </div>
                </div>
				<?php endif; ?>
                <?php else: ?>
                <div class="seller-pop">
                    <div class="seller-logo">
                    	<?php if ($this->_var['goods']['shopinfo']['brand_thumb']): ?>
                            <a href="<?php echo $this->_var['goods']['store_url']; ?>" target="_blank"><img src="<?php echo $this->_var['goods']['shopinfo']['brand_thumb']; ?>" /></a>
                        <?php else: ?>
                            <a href="<?php echo $this->_var['goods']['goods_brand_url']; ?>" target="_blank"><?php echo $this->_var['goods']['goods_brand']; ?></a>
                        <?php endif; ?>
                    </div>
                    <div class="seller-info">
                    	<a href="<?php echo $this->_var['goods']['store_url']; ?>" title="<?php echo $this->_var['goods']['rz_shopName']; ?>" target="_blank" class="name"><?php echo $this->_var['goods']['rz_shopName']; ?></a>
                        <?php if ($this->_var['shop_information']['is_IM'] == 1 || $this->_var['shop_information']['is_dsc']): ?>
                            <a id="IM" href="javascript:;" im_type="dsc" onclick="openWin(this)" goods_id="<?php echo $this->_var['goods']['goods_id']; ?>"><i class="icon i-kefu"></i></a>
                        <?php else: ?>
                            <?php if ($this->_var['basic_info']['kf_type'] == 1): ?>
                                <a href="http://www.taobao.com/webww/ww.php?ver=3&touid=<?php echo $this->_var['basic_info']['kf_ww']; ?>&siteid=cntaobao&status=1&charset=utf-8" target="_blank"><i class="icon i-kefu"></i></a>
                            <?php else: ?>
                                <a href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $this->_var['basic_info']['kf_qq']; ?>&site=qq&menu=yes" target="_blank"><i class="icon i-kefu"></i></a>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                    <?php if ($this->_var['goods']['shopinfo']['self_run']): ?>
                    <div class="seller-sr" ><?php echo $this->_var['lang']['platform_self']; ?></div>
                    <?php endif; ?>
                    <div class="seller-pop-box">
                    	<div class="s-score">
                            <span class="score-icon"><span class="score-icon-bg" style="width:<?php echo $this->_var['merch_cmt']['cmt']['all_zconments']['allReview']; ?>%;"></span></span>
                            <span><?php echo $this->_var['merch_cmt']['cmt']['all_zconments']['score']; ?></span>
                            <i class="iconfont icon-down"></i>
                        </div>
                        <div class="g-s-parts">
                            <div class="parts-item parts-goods">
                                <span class="col1">商品评价</span>
                                <span class="col2 <?php if ($this->_var['merch_cmt']['cmt']['commentRank']['zconments']['is_status'] == 1): ?>ftx-01<?php elseif ($this->_var['merch_cmt']['cmt']['commentRank']['zconments']['is_status'] == 2): ?>average<?php else: ?>ftx-02<?php endif; ?>"><?php echo $this->_var['merch_cmt']['cmt']['commentRank']['zconments']['score']; ?><i class="iconfont icon-arrow-<?php if ($this->_var['merch_cmt']['cmt']['commentRank']['zconments']['is_status'] == 1): ?>up<?php elseif ($this->_var['merch_cmt']['cmt']['commentRank']['zconments']['is_status'] == 2): ?>average<?php else: ?>down<?php endif; ?>"></i></span>
                            </div>
                            <div class="parts-item parts-goods">
                                <span class="col1">服务态度</span>
                                <span class="col2 <?php if ($this->_var['merch_cmt']['cmt']['commentServer']['zconments']['is_status'] == 1): ?>ftx-01<?php elseif ($this->_var['merch_cmt']['cmt']['commentServer']['zconments']['is_status'] == 2): ?>average<?php else: ?>ftx-02<?php endif; ?>"><?php echo $this->_var['merch_cmt']['cmt']['commentServer']['zconments']['score']; ?><i class="iconfont icon-arrow-<?php if ($this->_var['merch_cmt']['cmt']['commentServer']['zconments']['is_status'] == 1): ?>up<?php elseif ($this->_var['merch_cmt']['cmt']['commentServer']['zconments']['is_status'] == 2): ?>average<?php else: ?>down<?php endif; ?>"></i></span>
                            </div>
                            <div class="parts-item parts-goods">
                                <span class="col1">发货速度</span>
                                <span class="col2 <?php if ($this->_var['merch_cmt']['cmt']['commentDelivery']['zconments']['is_status'] == 1): ?>ftx-01<?php elseif ($this->_var['merch_cmt']['cmt']['commentDelivery']['zconments']['is_status'] == 2): ?>average<?php else: ?>ftx-02<?php endif; ?>"><?php echo $this->_var['merch_cmt']['cmt']['commentDelivery']['zconments']['score']; ?><i class="iconfont icon-arrow-<?php if ($this->_var['merch_cmt']['cmt']['commentDelivery']['zconments']['is_status'] == 1): ?>up<?php elseif ($this->_var['merch_cmt']['cmt']['commentDelivery']['zconments']['is_status'] == 2): ?>average<?php else: ?>down<?php endif; ?>"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="seller-item">
                    	<a href="javascript:void(0);" ectype="collect_store" data-type='goods' data-value='{"userid":<?php echo $this->_var['user_id']; ?>,"storeid":<?php echo $this->_var['goods']['user_id']; ?>}' class="gz-store" data-url="<?php echo $this->_var['goods']['goods_url']; ?>"></a>
                        <a href="<?php echo $this->_var['goods']['store_url']; ?>" class="store-home"><i class="iconfont icon-home-store"></i>店铺</a>
                        <input type="hidden" name="error" value="<?php echo $this->_var['goods']['error']; ?>" id="error"/>
                    </div>
                    <div class="seller-tel"><i class="iconfont icon-tel"></i><?php echo $this->_var['shop_information']['kf_tel']; ?></div>
                </div>
                <?php endif; ?>
            </div>
            <div id="pingou-process" class="pingou-process">
            	<h3><?php echo $this->_var['lang']['presale_process']; ?></h3>
                <div class="item">
                    <i class="sprite-step1"></i>
                    <dl>
                        <dt>1.<?php echo $this->_var['lang']['Pay_deposit']; ?></dt>
                        <dd class="presale-time"><?php echo $this->_var['presale']['start_time']; ?> ~ <?php echo $this->_var['presale']['end_time']; ?></dd>
                    </dl>
                    <span class="sprite-arrow"></span>
                </div>
                <div class="item">
                    <i class="sprite-step2"></i>
                    <dl>
                        <dt>2.<?php echo $this->_var['lang']['Payment_tail']; ?><em>（<?php echo $this->_var['lang']['operationinmy_oreder']; ?>）</em></dt>
                        <dd class="balance-time"><?php echo $this->_var['presale']['pay_start_time']; ?> ~ <?php echo $this->_var['presale']['pay_end_time']; ?></dd>
                    </dl>
                    <span class="sprite-arrow"></span>
                </div>
                <div class="item">
                    <i class="sprite-step3"></i>
                    <dl>
                        <dt>3.<?php echo $this->_var['lang']['Goods_delivery']; ?></dt>
                        <dd><?php echo $this->_var['lang']['presale_Estimate']; ?><span class="" id="shipping_time"></span> <?php echo $this->_var['lang']['presale_Estimate_two']; ?></dd>
                    </dl>
                </div>
            </div>
            <div class="clear"></div>
            <div class="goods-main-layout">
                <div class="g-m-left">
                    <?php echo $this->fetch('library/goods_merchants.lbi'); ?>

                    <?php if ($this->_var['basic_info']['kf_type'] == 1): ?>
                        <?php if ($this->_var['basic_info']['kf_ww_all']): ?>
                        <div class="g-main service_list">
                            <div class="mt"><h3><?php echo $this->_var['lang']['store_Customer_service']; ?></h3></div>
                            <div class="mc">
                                <ul>
                                    <?php $_from = $this->_var['basic_info']['kf_ww_all']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'kf_ww');if (count($_from)):
    foreach ($_from AS $this->_var['kf_ww']):
?>
                                    <li><a href="http://www.taobao.com/webww/ww.php?ver=3&touid=<?php echo $this->_var['kf_ww']['1']; ?>&siteid=cntaobao&status=1&charset=utf-8" target="_blank"><i class="icon_service_ww"></i><span><?php echo $this->_var['kf_ww']['0']; ?></span></a></li>
                                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php else: ?>
                        <?php if ($this->_var['basic_info']['kf_qq_all']): ?>
                        <div class="g-main service_list">
                            <div class="mt"><h3><?php echo $this->_var['lang']['store_Customer_service']; ?></h3></div>
                            <div class="mc">
                                <ul>
                                    <?php $_from = $this->_var['basic_info']['kf_qq_all']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'kf_qq');if (count($_from)):
    foreach ($_from AS $this->_var['kf_qq']):
?>
                                        <li class="service_qq"><a href="http://wpa.qq.com/msgrd?v=3&uin=<?php echo $this->_var['kf_qq']['1']; ?>&site=qq&menu=yes" target="_blank"><i class="icon i-kefu"></i><span><?php echo $this->_var['kf_qq']['0']; ?></span></a></li>
                                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                </ul>
                            </div>
                        </div>
                        <?php endif; ?>
                    <?php endif; ?>
                    
                    <?php if ($this->_var['goods_store_cat']): ?>
                    <div class="g-main g-store-cate">
                    	<div class="mt">
                            <h3>店内分类</h3>
                        </div>
                        <div class="mc">
                            <div class="g-s-cate-warp">
                                <?php $_from = $this->_var['goods_store_cat']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'cat');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['cat']):
?>
                            	<dl<?php if ($this->_var['cat']['cat_id']): ?> ectype="cateOpen"<?php else: ?> class="hover"<?php endif; ?>>
                                    <dt><i class="icon"></i><a href="<?php echo $this->_var['cat']['url']; ?>" target="_blank"><?php echo $this->_var['cat']['name']; ?></a></dt>
                                    <?php if ($this->_var['cat']['cat_id']): ?>
                                    <dd>
                                        <?php $_from = $this->_var['cat']['cat_id']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'cat');$this->_foreach['nocat'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['nocat']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['cat']):
        $this->_foreach['nocat']['iteration']++;
?>
                                    	<a href="<?php echo $this->_var['cat']['url']; ?>" class="a-item" target="_blank">&gt; <?php echo $this->_var['cat']['name']; ?></a>
                                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                    </dd>
                                    <?php endif; ?>
                                </dl>
                                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                    
                    <?php if ($this->_var['goods_related_cat']): ?>
                    <div class="g-main">
                    	<div class="mt">
                            <h3>相关分类</h3>
                        </div>
                        <div class="mc">
                            <div class="mc-warp">
                            	<div class="items">
                                    <?php $_from = $this->_var['goods_related_cat']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'cat');$this->_foreach['cat'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['cat']['total'] > 0):
    foreach ($_from AS $this->_var['cat']):
        $this->_foreach['cat']['iteration']++;
?>
                                    <?php if ($this->_foreach['cat']['iteration'] < 11): ?>
                                    <div class="item"><a href="<?php echo $this->_var['cat']['url']; ?>" target="_blank"><?php echo $this->_var['cat']['cat_name']; ?></a></div>
                                    <?php endif; ?>
                                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>
                  
                    
                    <?php echo $this->fetch('library/goods_related.lbi'); ?>
                    
                </div>
                <div class="g-m-detail">
                	<div class="gm-tabbox" ectype="gm-tabs">
                    	<ul class="gm-tab">
                            <li class="curr" ectype="gm-tab-item">商品详情</li>
                            <?php if ($this->_var['properties']): ?><li ectype="gm-tab-item-spec">规格参数</li><?php endif; ?>
                            <li ectype="gm-tab-item">用户评论（<em class="ReviewsCount"><?php echo $this->_var['comment_all']['allmen']; ?></em>）</li>
                            <li ectype="gm-tab-item">网友讨论圈</li>
                        </ul>
                        <div class="extra">
                        	<div class="item">
								<?php if ($this->_var['two_code']): ?>
                                <div class="si-phone-code">
                                    <div class="qrcode-wrap">
                                        <div class="qrcode_tit"><?php echo $this->_var['lang']['summary_phone']; ?><i class="iconfont icon-qr-code"></i></div>
                                        <div class="qrcode_pop">
                                            <div class="mobile-qrcode"><img src="<?php echo $this->_var['weixin_img_url']; ?>" alt="<?php echo $this->_var['lang']['two_code']; ?>" title="<?php echo $this->_var['weixin_img_text']; ?>" width="175"></div>
                                        </div>
                                    </div>
                                </div>
								<?php endif; ?>
                            </div>
                        </div>
                        <div class="gm-tab-qp-bort" ectype="qp-bort"></div>
                    </div>
                    <div class="gm-floors" ectype="gm-floors">
                        <div class="gm-f-item gm-f-details" ectype="gm-item">
                            <div class="gm-title">
                                <h3>商品详情</h3>
                            </div>
                            <div class="goods-para-list">
                                <dl class="goods-para">
                                    <dd class="column"><span><?php echo $this->_var['lang']['goods_name']; ?>：<?php echo htmlspecialchars($this->_var['goods']['goods_name']); ?></span></dd>
                                    <dd class="column"><span><?php echo $this->_var['lang']['Commodity_number']; ?>：<?php echo $this->_var['goods']['goods_sn']; ?></span></dd>
                                    <dd class="column"><span><?php echo $this->_var['lang']['seller_store']; ?>：<a href="<?php echo $this->_var['goods']['store_url']; ?>" title="<?php echo $this->_var['goods']['rz_shopName']; ?>" target="_blank"><?php echo $this->_var['goods']['rz_shopName']; ?></a></span></dd>
                                    <?php if ($this->_var['cfg']['show_goodsweight']): ?>
                                    <dd class="column"><span><?php echo $this->_var['lang']['weight']; ?>：<?php echo $this->_var['goods']['goods_weight']; ?></span></dd>
                                    <?php endif; ?>
                                    <?php if ($this->_var['cfg']['show_addtime']): ?>
                                    <dd class="column"><span><?php echo $this->_var['lang']['add_time']; ?><?php echo $this->_var['goods']['add_time']; ?></span></dd>
                                    <?php endif; ?>
                                </dl>
                            
                                <?php if ($this->_var['properties']): ?>
                                <dl class="goods-para">
                                    <?php $_from = $this->_var['properties']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'property_group');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['property_group']):
?>
                                    <?php $_from = $this->_var['property_group']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'property');$this->_foreach['noproperty'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['noproperty']['total'] > 0):
    foreach ($_from AS $this->_var['property']):
        $this->_foreach['noproperty']['iteration']++;
?>
                                    <?php if ($this->_foreach['noproperty']['iteration'] < 13): ?>
                                    <dd class="column"><span title="<?php echo $this->_var['property']['value']; ?>"><?php echo htmlspecialchars($this->_var['property']['name']); ?>：<?php echo $this->_var['property']['value']; ?></span></dd>
                                    <?php endif; ?>
                                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                </dl>
                                <?php endif; ?>
                                <?php if ($this->_var['extend_info']): ?>
                                <dl class="goods-para">
                                    <?php $_from = $this->_var['extend_info']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'info');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['info']):
?>	
                                    <dd class="column"><span title="<?php echo htmlspecialchars($this->_var['info']); ?>"><?php echo $this->_var['key']; ?>：<?php echo htmlspecialchars($this->_var['info']); ?></span></dd>
                                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                </dl>
                                <?php endif; ?>
                                <?php if ($this->_var['properties']): ?>
                                <p class="more-par">
                                    <a href="javascript:void(0);" ectype="product-detail" class="ftx-05">更多参数>></a>
                                </p>
                                <?php endif; ?>
                            </div>
                            
                            <?php echo $this->_var['goods']['goods_desc']; ?>
                        </div>
                        <?php if ($this->_var['properties']): ?>
                        <div class="gm-f-item gm-f-parameter" ectype="gm-item" id="product-detail" style="display:none;">
                            <div class="gm-title">
                                <h3>规格参数</h3>
                            </div>
                            <div class="Ptable">
                                <?php $_from = $this->_var['properties']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'property_group');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['property_group']):
?>
                                <div class="Ptable-item">
                                    <h3><?php echo $this->_var['key']; ?></h3>
                                    <dl>
                                        <?php $_from = $this->_var['property_group']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'property');if (count($_from)):
    foreach ($_from AS $this->_var['property']):
?>
                                        <dt><?php echo htmlspecialchars($this->_var['property']['name']); ?></dt>
                                        <dd title="<?php echo $this->_var['property']['value']; ?>"><?php echo $this->_var['property']['value']; ?></dd>
                                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                    </dl>
                                </div>
                                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            </div>
                        </div>
                        <?php endif; ?>
                        <div class="gm-f-item gm-f-comment" ectype="gm-item">
                            <div class="gm-title">
                                <h3>评论晒单</h3>
                                <?php 
$k = array (
  'name' => 'goods_comment_title',
  'goods_id' => $this->_var['goods']['goods_id'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
                            </div>
                            <div class="gm-warp">
                                <div class="praise-rate-warp">
                                    <div class="rate">
                                        <strong><?php echo $this->_var['comment_all']['goodReview']; ?></strong>
                                        <span class="rate-span">
                                            <span class="tit">好评率</span>
                                            <span class="bf">%</span>
                                        </span>
                                    </div>
                                    <div class="actor-new">
                                        <?php if ($this->_var['goods']['impression_list']): ?>
                                        <dl>
                                            <?php $_from = $this->_var['goods']['impression_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'tag');if (count($_from)):
    foreach ($_from AS $this->_var['tag']):
?>
                                            <dd><?php echo $this->_var['tag']['txt']; ?>(<?php echo $this->_var['tag']['num']; ?>)</dd>
                                            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                        </dl>
                                        <?php else: ?>
                                        <div class="not_impression">此商品还没有设置买家印象，陪我一起等下嘛~</div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <div class="com-list-main">
                                <?php echo $this->fetch('library/comments.lbi'); ?>
                                </div>
                            </div>
                        </div>
                        <div class="gm-f-item gm-f-tiezi" ectype="gm-item">
                            <?php 
$k = array (
  'name' => 'goods_discuss_title',
  'goods_id' => $this->_var['goods']['goods_id'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
                            <div class="table" id='discuss_list_ECS_COMMENT'>
                                <?php echo $this->fetch('library/comments_discuss_list1.lbi'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="clear"></div>
                <div class="rection">
                	<div class="ftit"><h3><?php echo $this->_var['lang']['guess_love']; ?></h3></div>
                    <?php echo $this->fetch('library/guess_goods_love.lbi'); ?>
                </div>
            </div>
        </div>
    </div>

    	 
	<?php echo $this->fetch('library/duibi.lbi'); ?>
    

    <?php echo $this->fetch('library/page_footer.lbi'); ?>
    
    <?php 
$k = array (
  'name' => 'user_menu_position',
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
  
	<?php echo $this->smarty_insert_scripts(array('files'=>'jquery.SuperSlide.2.1.1.js,jquery.yomi.js,common.js,compare.js,cart_common.js,warehouse_area.js,magiczoomplus.js,cart_quick_links.js')); ?>
	<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/dsc-common.js"></script>
    <script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/other/jquery.smint.js"></script>
    <script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/other/floor.js"></script>
    <script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/jquery.purebox.js"></script>
    
    <script type="text/javascript">
		//商品详情
		goods_desc_floor();
	
		function presale_submit()
		{
			var is_ok = 1;
			var status=<?php echo empty($this->_var['presale']['status']) ? '0' : $this->_var['presale']['status']; ?>;
			var user_id = <?php echo $this->_var['user_id']; ?>;
			var qty = Number($("#quantity").val());
			
			if(user_id >0){
				//商品属性组是否有选中
				var attr_list = 0;
				var is_selected = 0;
				$(".goods_info_attr").each(function(index, element) {
					attr_list = index + 1;
					
					if($(this).find(".item").hasClass("selected")){
						is_selected = is_selected + 1
					} 
				});

				//先判断是否选中商品规格
				if($(".goods_info_attr").length > 0){
					if(attr_list != is_selected){
						get_goods_prompt_message(json_languages.Product_spec_prompt);
						is_ok = 0;
					}else{
						is_ok = 1;
					}
				}
				
				//判断商品限购
				<?php if ($this->_var['xiangou'] == 1): ?>
					<?php if ($this->_var['presale']['is_xiangou'] == 1 && $this->_var['presale']['xiangou_num'] > 0): ?>
						var xuangou_num = <?php echo $this->_var['presale']['xiangou_num']; ?>;
						var xiangou = <?php echo $this->_var['xiangou']; ?>;
						var total_goods = Number(<?php echo empty($this->_var['orderG_number']) ? '0' : $this->_var['orderG_number']; ?>);
						
						if(total_goods >= xuangou_num){
							var message = '已购买' + total_goods + '件商品达到限购条件,无法再购买';
							is_ok = 0;
						}else if(((qty >= total_goods && total_goods > 0) || qty > xuangou_num) && xuangou_num > 0 && xiangou == 1){
							if ((qty >= total_goods && total_goods > 0) || total_goods >= xuangou_num){
								var message = '已购买<?php echo $this->_var['orderG_number']; ?>件商品达到限购条件,无法再购买';
							}else{
								var message = '超过限购数量';
							}
							
							is_ok = 0;
						}
						
						if(is_ok == 0){
							pbDialog(message,"",0);
						}
						
					<?php endif; ?>
				<?php endif; ?>
				
				if(is_ok == 1){
					if(status != 1){
						pbDialog(presrll_desc_end,"",0);
					}else{
						document.getElementById("ECS_FORMBUY").submit();
					}
				}
			}else{
				var back_url = "presale.php?act=view&id=" + <?php echo $this->_var['presale']['act_id']; ?>;
				$.notLogin("get_ajax_content.php?act=get_login_dialog",back_url);
			}
		}
	</script>
    <script type="text/javascript">
    region.detail = true;
	
	var seller_id = <?php echo empty($this->_var['goods']['user_id']) ? '0' : $this->_var['goods']['user_id']; ?>;
	var goods_id = <?php echo empty($this->_var['goods_id']) ? '0' : $this->_var['goods_id']; ?>;
	var goodsId = <?php echo empty($this->_var['goods_id']) ? '0' : $this->_var['goods_id']; ?>;
	var goodsattr_style = <?php echo empty($this->_var['cfg']['goodsattr_style']) ? '1' : $this->_var['cfg']['goodsattr_style']; ?>;
	var gmt_end_time = <?php echo empty($this->_var['promote_end_time']) ? '0' : $this->_var['promote_end_time']; ?>;
	var now_time = <?php echo $this->_var['now_time']; ?>;
	var isReturn = false;
	var act_id = <?php echo $this->_var['act_id']; ?>;
	var add_shop_price = $("*[ectype='add_shop_price']").val(); 
	
	$(function(){
		if(add_shop_price == 0){
			$(":input[name='goods_spec']").val('');
		}
		
		if(seller_id > 0){
			goods_collect_store(seller_id);
		}
		
		//对比默认加载
		Compare.init();
	});
	
    /* 点击可选属性或改变数量时修改商品价格的函数 */
    function changePrice(onload){
       	var qty = $("*[ectype='quantity']").val();
		var goods_attr_id = '';
		var goods_attr = '';
		var attr_id = '';
		var attr = '';
		
		goods_attr_id = getSelectedAttributes(document.forms['ECS_FORMBUY']);	
		
		if(onload != 'onload'){
			if(add_shop_price == 0){
				attr_id = getSelectedAttributesGroup(document.forms['ECS_FORMBUY']);
				goods_attr = '&goods_attr=' + attr_id;
			}
			Ajax.call('presale.php', 'act=price&id=' + goodsId + '&attr=' + goods_attr_id + goods_attr + '&number=' + qty + '&warehouse_id=' + <?php echo empty($this->_var['region_id']) ? '0' : $this->_var['region_id']; ?> + '&area_id=' + <?php echo empty($this->_var['area_id']) ? '0' : $this->_var['area_id']; ?>, changePriceResponse, 'GET', 'JSON');
		}else{
			if(add_shop_price == 1){
				$(":input[name='goods_spec']").val(goods_attr_id);
				attr = '&attr=' + goods_attr_id;
			}
			Ajax.call('presale.php', 'act=price&id=' + goodsId + attr + '&number=' + qty + '&warehouse_id=' + <?php echo empty($this->_var['region_id']) ? '0' : $this->_var['region_id']; ?> + '&area_id=' + <?php echo empty($this->_var['area_id']) ? '0' : $this->_var['area_id']; ?> + '&onload=' + onload, changePriceResponse, 'GET', 'JSON');
		}
    }
    
    /**
     * 接收返回的信息
     */
    function changePriceResponse(res){
		if(res.err_msg.length > 0){
			pbDialog(res.err_msg," ",0,450,80,50);
		}else{
        	//document.forms['ECS_FORMBUY'].elements['number'].value = res.qty;
		
			//更新库存
			if($("*[ectype='goods_attr_num']").length > 0){
				$("*[ectype='goods_attr_num']").html(res.attr_number);
				$("*[ectype='perNumber']").val(res.attr_number);
			}
			
			//更新已购买数量
			if($("#orderG_number").length > 0){
				$("#orderG_number").html(res.orderG_number);
			}
		
			if($("#ECS_SHOPPRICE").length > 0){
				//商品价格
				if(res.onload == 'onload'){
					$("#ECS_SHOPPRICE").html(res.result);
				}else{
					if(add_shop_price == 1){
						$("#ECS_SHOPPRICE").html(res.result);
					}else{
						if(res.show_goods == 1){
							$("#ECS_SHOPPRICE").html(res.spec_price);
						}else{
							$("#ECS_SHOPPRICE").html(res.result);
						}
					}
				}
			}
        
			if(res.err_no == 2){
				$('#isHas_warehouse_num').html(json_languages.shiping_prompt);
			}else{
				var isHas;
				var is_shipping = Number($("#is_shipping").val());
				if($("#isHas_warehouse_num").length > 0){
					var status = <?php echo $this->_var['cfg']['add_shop_price']; ?>;
					if(res.attr_number > 0){
						$("a[ectype='btn-append']").attr('href','javascript:addToCartShowDiv(<?php echo $this->_var['goods']['goods_id']; ?>)').removeClass('btn_disabled');
						$("a[ectype='btn-buynow']").attr('href','<?php if ($this->_var['user_id'] <= 0 && $this->_var['one_step_buy']): ?>#none<?php else: ?>javascript:addToCart(<?php echo $this->_var['goods']['goods_id']; ?>)<?php endif; ?>').removeClass('btn_disabled');
						$("a[ectype='byStages']").removeClass('btn_disabled');
						$('a').remove('#quehuo');
						isHas = '<strong>'+json_languages.Have_goods+'</strong>，'+json_languages.Deliver_back_order;
						
						$("a[ectype='btn-buynow']").show();
						$("a[ectype='btn-append']").show();
						$("a[ectype='byStages']").show();
					}else{
						isHas = '<strong>'+json_languages.No_goods+'</strong>，'+json_languages.goods_over;
						
						$("a[ectype='btn-buynow']").attr('href','javascript:void(0)').addClass('btn_disabled');
						$("a[ectype='btn-append']").attr('href','javascript:void(0)').addClass('btn_disabled');
						$("a[ectype='byStages']").addClass('btn_disabled');
					}
					
					if(is_shipping == 0){
						isHas = '<strong>'+json_languages.Have_goods+'</strong>，' + json_languages.shiping_prompt;
					}
					
					$("#isHas_warehouse_num").html(isHas);
				}
			}
			
			$("#shipping_time").html(res.presale.str_time);
			$('.ECS_fittings_interval').html(res.shop_price);
			//ecmoban模板堂 --zhuo end			
		}

		if(res.onload == "onload"){
			quantity();
		}
    }
    
    
    region.changedDis_pre = function(district_id,user_id,d_null)
    {
        var province_id = document.getElementById('province_id').value;
        var city_id = document.getElementById('city_id').value;
        var area_div = document.getElementById('area_list');
        var goods_id = document.getElementById('good_id').value;
        
        if(d_null == 1){
            var d_null = "&d_null=" + d_null;
        }else{
            d_null = '';
        }
        area_div.style.display = 'none';
        Ajax.call('presale.php', 'act=in_stock&gid='+ goods_id + '&act_id='+ act_id + '&province=' + province_id + "&city=" + city_id + "&district=" + district_id + "&user_id=" + user_id + d_null, inStockResponse, "GET", "JSON");
    
    }
    
    inStockResponse = function(res)
    {
        if(res.isRegion == 0){
    
            if (confirm(res.message))  
              {
                    var district_id = document.getElementById('district_id');
                    district_id.value = res.district;    
                    location.href = 'user.php?act=address_list';
              }else{
                    location.href = "presale.php?act=view&id=" + res.act_id + "&t=" + parseInt(Math.random()*1000) + "#areaAddress";
              }
    
            return false;
        }else{
            location.href = "presale.php?act=view&id=" + res.act_id + "&t=" + parseInt(Math.random()*1000) + "#areaAddress";
        }
    }
    
    /**
     * 猜你喜欢-换一组
     */
    function change_group(){
        var page = 1;
        $(".ecsc-goods-love .ec-huan").click(function(){
                page++;
                if(page == 4){
                        page = 1;
                }
                Ajax.call('presale.php?act=guess_goods', 'page=' + page , guessGoodsResponse, 'GET', 'JSON');
        });
    }
    function guessGoodsResponse(data){
        $("#goodsLove_content").html(data.result);
    }
    </script>
    <script type="text/javascript">
		$(window).load(function(){
			$("#footer").css({"z-index":666});
		});
	
        //倒计时
        $(".time").each(function(){
            $(this).yomi();
        });
        
        $(".choose .attr-radio .item").click(function(){
            $(this).addClass("selected").siblings().removeClass("selected");
            $(this).find("input[type='radio']").prop("checked",true);
            
        });
        
        $(".choose .attr-check .item").click(function(){
            var len = $(this).parent().find(".selected").length;
            if($(this).hasClass("selected")){
                if(len<=1)return;
                $(this).removeClass("selected");
                $(this).find("input[type='checkbox']").prop("checked",false);
            }else{
                $(this).addClass("selected");
                $(this).find("input[type='checkbox']").prop("checked",true);			
            }
        });
		
		//店铺分类展开
		$("#sp-category dt").click(function(){
			if($(this).parent().hasClass("open")){
				$(this).parent().removeClass("open");
			}else{
				$(this).parent().addClass("open");
				$(this).parent().siblings().removeClass("open");
			}
		});
    </script>
    <?php 
$k = array (
  'name' => 'goods_delivery_area_js',
  'area' => $this->_var['area'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
</body>
</html>
