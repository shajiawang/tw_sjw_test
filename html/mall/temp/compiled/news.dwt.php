<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="<?php echo $this->_var['keywords']; ?>" />
<meta name="Description" content="<?php echo $this->_var['description']; ?>" />

<title><?php echo $this->_var['page_title']; ?></title>



<link rel="shortcut icon" href="favicon.ico" />
<?php echo $this->fetch('library/js_languages_new.lbi'); ?>
</head>

<body class="bg-ligtGary">
<?php echo $this->fetch('library/page_header_common.lbi'); ?>
<div class="article-index">
	<div class="w w1200">
            
            <div class="banner-article">
                <div class="b"><?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['news_banner_small_left'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?></div>
                <?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['news_banner_small_right'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
            </div>
            
            <div class="article-col-2 clearfix">
                <?php $_from = $this->_var['cat_child_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'categories_0_38561800_1564561156');$this->_foreach['cat_child'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['cat_child']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['categories_0_38561800_1564561156']):
        $this->_foreach['cat_child']['iteration']++;
?>
                <div class="article-box">
                    <div class="ab-hd"><h2><i class="iconfont icon-icon02"></i><?php echo $this->_var['categories_0_38561800_1564561156']['cat_name']; ?></h2><a href="article_cat.php?id=<?php echo $this->_var['categories_0_38561800_1564561156']['cat_id']; ?>" class="more">more&gt;</a></div>
                    <div class="ab-bd">
                    	<?php $_from = $this->_var['articles_list'][$this->_var['key']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'articles');$this->_foreach['article'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['article']['total'] > 0):
    foreach ($_from AS $this->_var['articles']):
        $this->_foreach['article']['iteration']++;
?>
                        <?php if (($this->_foreach['article']['iteration'] - 1) < 1): ?>
                        <div class="focus">
                        	<div class="img"><a href="<?php echo $this->_var['articles']['url']; ?>" title="<?php echo $this->_var['articles']['title']; ?>"><img src="<?php if ($this->_var['articles']['file_url']): ?><?php echo $this->_var['articles']['file_url']; ?><?php else: ?>images/article_default.jpg<?php endif; ?>" /></a></div>
                            <div class="info">
                                <div class="info-name"><a href="<?php echo $this->_var['articles']['url']; ?>" title="<?php echo $this->_var['articles']['title']; ?>"><?php echo $this->_var['articles']['title']; ?></a></div>
                                <div class="info-intro"><?php echo $this->_var['articles']['description']; ?></div>
                                <div class="info-time"><?php echo $this->_var['articles']['add_time']; ?></div>
                            </div>
                        </div>
                        <?php endif; ?>
                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                        <ul class="list">
                        	<?php $_from = $this->_var['articles_list'][$this->_var['key']]; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'articles');$this->_foreach['article'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['article']['total'] > 0):
    foreach ($_from AS $this->_var['articles']):
        $this->_foreach['article']['iteration']++;
?>
                        	<?php if (($this->_foreach['article']['iteration'] - 1) > 0 && ($this->_foreach['article']['iteration'] - 1) < 5): ?>
                            <li><a href="<?php echo $this->_var['articles']['url']; ?>" target="_blank"><?php echo $this->_var['articles']['title']; ?></a><span class="time"><?php echo $this->_var['articles']['add_time']; ?></span></li>
                            <?php endif; ?>
                        	<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>                 
                        </ul>
                    </div>
                </div>
                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
            </div>  
            
            <div class="article-col-3 clearfix">
                <div class="article-box">
                    <div class="ab-hd"><h2><i class="iconfont icon-article"></i>新手上路</h2><a href="article_cat.php?id=5" class="more">more&gt;</a></div>
                    <div class="ab-bd">
                        <ul class="list">
                        	<?php $_from = $this->_var['new_articles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'new_categories');$this->_foreach['new_articles'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['new_articles']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['new_categories']):
        $this->_foreach['new_articles']['iteration']++;
?>
                            <?php if (($this->_foreach['new_articles']['iteration'] - 1) < 4): ?>
                            <li><a href="<?php echo $this->_var['new_categories']['url']; ?>" target="_blank"><?php echo $this->_var['new_categories']['title']; ?></a><span class="time"><?php echo $this->_var['articles']['add_time']; ?></span></li>
                            <?php endif; ?>
                            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                        </ul>
                    </div>
                </div>
                <div class="article-box">
                    <div class="ab-hd"><h2><i class="iconfont icon-article"></i>服务保证</h2><a href="article_cat.php?id=8" class="more">more&gt;</a></div>
                    <div class="ab-bd">
                        <ul class="list">
                            <?php $_from = $this->_var['serve_articles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'serve_categories');$this->_foreach['serve_articles'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['serve_articles']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['serve_categories']):
        $this->_foreach['serve_articles']['iteration']++;
?>
                            <?php if (($this->_foreach['serve_articles']['iteration'] - 1) < 4): ?>
                            <li><a href="<?php echo $this->_var['serve_categories']['url']; ?>" target="_blank"><?php echo $this->_var['serve_categories']['title']; ?></a><span class="time"><?php echo $this->_var['articles']['add_time']; ?></span></li>
                            <?php endif; ?>
                            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                        </ul>
                    </div>
                </div>
                <div class="article-box">
                    <div class="ab-hd"><h2><i class="iconfont icon-article"></i>配送与支付</h2><a href="article_cat.php?id=7" class="more">more&gt;</a></div>
                    <div class="ab-bd">
                        <ul class="list">
                            <?php $_from = $this->_var['pay_articles']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'pay_categories');$this->_foreach['pay_articles'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['pay_articles']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['pay_categories']):
        $this->_foreach['pay_articles']['iteration']++;
?>
                            <?php if (($this->_foreach['pay_articles']['iteration'] - 1) < 4): ?>
                            <li><a href="<?php echo $this->_var['pay_categories']['url']; ?>" target="_blank"><?php echo $this->_var['pay_categories']['title']; ?></a><span class="time"><?php echo $this->_var['articles']['add_time']; ?></span></li>
                            <?php endif; ?>
                            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="article-box">
                <div class="ab-hd"><h2><i class="iconfont icon-article"></i><?php echo $this->_var['video_cat_info']['cat_name']; ?></h2><a href="article_cat.php?id=<?php echo $this->_var['video_cat_info']['cat_id']; ?>" class="more">more&gt;</a></div>
                <div class="ab-bd">
                    <ul class="quick clearfix">
                    	<?php $_from = $this->_var['cat_id_articles_video']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'video');$this->_foreach['no'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['no']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['video']):
        $this->_foreach['no']['iteration']++;
?>
                        <?php if ($this->_foreach['no']['iteration'] < 5): ?>
                        <li>
                            <div class="q-img"><a href="<?php echo $this->_var['video']['url']; ?>" target="_blank"><img src="<?php echo $this->_var['video']['file_url']; ?>" alt=""></a></div>
                            <div class="q-name"><a href="<?php echo $this->_var['video']['url']; ?>" target="_blank"><?php echo $this->_var['video']['title']; ?></a></div>
                            <div class="q-info"><?php echo $this->_var['video']['description']; ?></div>
                        </li>
                        <?php endif; ?>
                        <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                    </ul>
                </div>
            </div>
            
            <div class="article-col-1-2 clearfix">
                <div class="article-box">
                    <div class="ab-hd"><h2><i class="iconfont icon-article"></i><?php echo $this->_var['lang']['Recent_popular']; ?></h2></div>
                    <div class="ab-bd">
                        <ul class="list">
                        	<?php $_from = $this->_var['hot_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');$this->_foreach['hot'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['hot']['total'] > 0):
    foreach ($_from AS $this->_var['goods']):
        $this->_foreach['hot']['iteration']++;
?>
                            <?php if ($this->_foreach['hot']['iteration'] < 6): ?>
                            <li><a href="<?php echo $this->_var['goods']['url']; ?>" target="_blank" title="<?php echo $this->_var['goods']['name']; ?>"><?php echo $this->_var['goods']['name']; ?></a><span class="time"><?php echo $this->_var['goods']['add_time']; ?></span></li>
                            <?php endif; ?>
                            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                        </ul>
                    </div>
                </div>
                <div class="article-box">
                    <div class="ab-hd"><h2><i class="iconfont icon-article"></i><?php echo $this->_var['lang']['best_goods']; ?></h2><a href="category.php?id=<?php echo $this->_var['custom_catb_id']; ?>" class="more">more&gt;</a></div>
                    <div class="ab-bd">
                        <ul class="g-list clearfix">
                        	<?php $_from = $this->_var['best_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');$this->_foreach['tj'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['tj']['total'] > 0):
    foreach ($_from AS $this->_var['goods']):
        $this->_foreach['tj']['iteration']++;
?>
                            <?php if ($this->_foreach['tj']['iteration'] < 5): ?>
                            <li>
                                <a href="<?php echo $this->_var['goods']['url']; ?>" title="<?php echo $this->_var['goods']['name']; ?>" target="_blank">
                                    <img src="<?php echo $this->_var['goods']['thumb']; ?>" alt="">
                                    <p><?php echo $this->_var['goods']['name']; ?></p>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</div>
<?php echo $this->fetch('library/page_footer.lbi'); ?>
<?php echo $this->smarty_insert_scripts(array('files'=>'jquery.SuperSlide.2.1.1.js')); ?>
<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/dsc-common.js"></script>
</body>
</html>
