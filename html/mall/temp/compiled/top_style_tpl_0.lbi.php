<div class="catetop-tree-blank"></div>
<div class="banner catetop-banner">
	<?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['category_top_banner'],
  'id' => $this->_var['cate_info']['cat_id'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
</div>
<div class="catetop-main w w1200">
	
	<div class="toprank" id="toprank">
		<div class="hd">
			<ul>
				<li>热销榜</li>
				<li>促销榜</li>
			</ul>
		</div>
		<div class="bd">
			<ul>
				<?php $_from = $this->_var['cate_top_hot_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');$this->_foreach['hot'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['hot']['total'] > 0):
    foreach ($_from AS $this->_var['goods']):
        $this->_foreach['hot']['iteration']++;
?>
				<?php if ($this->_foreach['hot']['iteration'] <= 5): ?>
				<li>
					<div class="p-img"><a href="<?php echo $this->_var['goods']['url']; ?>"><img src="<?php echo $this->_var['goods']['thumb']; ?>" alt=""></a></div>
					<div class="p-name"><a href="<?php echo $this->_var['goods']['url']; ?>" title="<?php echo htmlspecialchars($this->_var['goods']['name']); ?>"><?php echo htmlspecialchars($this->_var['goods']['name']); ?></a></div>
					<div class="p-price">
						<?php if ($this->_var['goods']['promote_price'] != ''): ?>
							 <?php echo $this->_var['goods']['promote_price']; ?>
						<?php else: ?>
							 <?php echo $this->_var['goods']['shop_price']; ?>
						<?php endif; ?>					
					</div>
				</li>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
			</ul>
			<ul>
				<?php $_from = $this->_var['cate_top_promote_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');$this->_foreach['promote'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['promote']['total'] > 0):
    foreach ($_from AS $this->_var['goods']):
        $this->_foreach['promote']['iteration']++;
?>
				<?php if ($this->_foreach['promote']['iteration'] <= 5): ?>
				<li>
					<div class="p-img"><a href="<?php echo $this->_var['goods']['url']; ?>"><img src="<?php echo $this->_var['goods']['thumb']; ?>" alt=""></a></div>
					<div class="p-name"><a href="<?php echo $this->_var['goods']['url']; ?>" title="<?php echo htmlspecialchars($this->_var['goods']['name']); ?>"><?php echo htmlspecialchars($this->_var['goods']['name']); ?></a></div>
					<div class="p-price">
						<?php if ($this->_var['goods']['promote_price'] != ''): ?>
							 <?php echo $this->_var['goods']['promote_price']; ?>
						<?php else: ?>
							 <?php echo $this->_var['goods']['shop_price']; ?>
						<?php endif; ?>					
					</div>
				</li>
				<?php endif; ?>
				<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
			</ul>
		</div>
	</div>
	
	<div class="catetop-brand" id="catetop-brand">
		<div class="hd"><h2>品牌旗舰</h2></div>
		<div class="bd">
			<ul class="clearfix brand-recommend">
				<?php $_from = $this->_var['cat_store']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'store');$this->_foreach['store'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['store']['total'] > 0):
    foreach ($_from AS $this->_var['store']):
        $this->_foreach['store']['iteration']++;
?>
				<li <?php if ($this->_foreach['store']['iteration'] > 3): ?>class="reverse"<?php endif; ?>>
					<div class="cover"><a href="<?php echo $this->_var['store']['shop_url']; ?>"><img src="<?php echo $this->_var['store']['street_thumb']; ?>" alt=""></a></div>
					<div class="info">
						<div class="info-logo"><a href="<?php echo $this->_var['store']['shop_url']; ?>"><img src="<?php echo $this->_var['store']['brand_thumb']; ?>" alt=""></a></div>
						<div class="info-name"><a href="<?php echo $this->_var['store']['shop_url']; ?>"><?php echo $this->_var['store']['shop_title']; ?></a></div>
						<div class="info-intro"><?php echo sub_str($this->_var['store']['street_desc'],20); ?></div>
					</div>
				</li>				
				<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
			<?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['category_top_default_brand'],
  'id' => $this->_var['cat_id'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>				
			</ul>
			<div class="brand-slide">
				<a href="javascript:;" class="prev iconfont icon-left"></a>
				<a href="javascript:;" class="next iconfont icon-right"></a>
				<div class="bs-bd">
					<ul>
						<?php $_from = $this->_var['cat_brand']['brands']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('kid', 'brand');$this->_foreach['brand'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['brand']['total'] > 0):
    foreach ($_from AS $this->_var['kid'] => $this->_var['brand']):
        $this->_foreach['brand']['iteration']++;
?>
						<?php if ($this->_var['kid'] < 100): ?>
						<li><a href="brand.php?id=<?php echo $this->_var['brand']['brand_id']; ?>&cat=<?php echo $this->_var['cate_info']['cat_id']; ?>"><img src="<?php echo $this->_var['brand']['brand_logo']; ?>" alt="<?php echo $this->_var['brand']['brand_name']; ?>"></a></li>
						<?php endif; ?>
						<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>						
					</ul>
				</div>
			</div>
		</div>
	</div>
	
	<div class="catetop-floor-wp">
		<div class="catetop-floor" id="recToday">
			<div class="f-banner">
				<?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['category_top_default_best_head'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
			</div>
			<div class="f-hd">
				<h2>今日推荐</h2>
			</div>
			<div class="f-bd">
				<ul class="clearfix">
					<li class="first">
						<?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['category_top_default_best_left'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
					</li>
					<?php $_from = $this->_var['cate_top_best_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');$this->_foreach['best'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['best']['total'] > 0):
    foreach ($_from AS $this->_var['goods']):
        $this->_foreach['best']['iteration']++;
?>
					<?php if ($this->_foreach['best']['iteration'] <= 10): ?>
					<li class="goods-item">
						<div class="p-img"><a href="<?php echo $this->_var['goods']['url']; ?>"><img src="<?php echo $this->_var['goods']['thumb']; ?>" alt=""></a></div>
						<div class="p-name"><a href="<?php echo $this->_var['goods']['url']; ?>"><?php echo htmlspecialchars($this->_var['goods']['name']); ?></a></div>
						<div class="p-price">
							<?php if ($this->_var['goods']['promote_price'] != ''): ?>
								 <?php echo $this->_var['goods']['promote_price']; ?>
							<?php else: ?>
								 <?php echo $this->_var['goods']['shop_price']; ?>
							<?php endif; ?>						
						</div>
					</li>
					<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
				</ul>
			</div>
		</div>
		<div class="catetop-floor" id="newPic">
			<div class="f-banner">
				<?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['category_top_default_new_head'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
			</div>
			<div class="f-hd">
				<h2>新品到货</h2>
			</div>
			<div class="f-bd">
				<ul class="clearfix">
					<li class="first">
						<?php 
$k = array (
  'name' => 'get_adv_child',
  'ad_arr' => $this->_var['category_top_default_new_left'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
					</li>
					<?php $_from = $this->_var['cate_top_new_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods');$this->_foreach['new'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['new']['total'] > 0):
    foreach ($_from AS $this->_var['goods']):
        $this->_foreach['new']['iteration']++;
?>
					<?php if ($this->_foreach['new']['iteration'] <= 10): ?>
					<li class="goods-item">
						<div class="p-img"><a href="<?php echo $this->_var['goods']['url']; ?>"><img src="<?php echo $this->_var['goods']['thumb']; ?>" alt=""></a></div>
						<div class="p-name"><a href="<?php echo $this->_var['goods']['url']; ?>" title="<?php echo htmlspecialchars($this->_var['goods']['name']); ?>"><?php echo htmlspecialchars($this->_var['goods']['name']); ?></a></div>
						<div class="p-price">
							<?php if ($this->_var['goods']['promote_price'] != ''): ?>
								 <?php echo $this->_var['goods']['promote_price']; ?>
							<?php else: ?>
								 <?php echo $this->_var['goods']['shop_price']; ?>
							<?php endif; ?>						
						</div>
					</li>
					<?php endif; ?>
					<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
				</ul>
			</div>
		</div>
	</div>
	
	<?php 
$k = array (
  'name' => 'history_goods_pro',
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>

	
	<div class="catetop-lift lift-hide" ectype="lift">
    	<div class="lift-list" ectype="liftList">
            <div class="catetop-lift-item lift-item-current" ectype="liftItem" data-target="#toprank"><span>排行榜</span></div>
            <?php if ($this->_var['cat_store']): ?><div class="catetop-lift-item" ectype="liftItem" data-target="#catetop-brand"><span>品牌旗舰</span></div><?php endif; ?>
            <div class="catetop-lift-item" ectype="liftItem" data-target="#recToday"><span>今日推荐</span></div>
            <div class="catetop-lift-item" ectype="liftItem" data-target="#newPic"><span>新品到货</span></div>
            <div class="catetop-lift-item lift-history" ectype="liftItem" data-target="#atwillgo"><span>浏览记录</span></div>
            <div class="catetop-lift-item lift-item-top" ectype="liftItem"><span>TOP</span></div>
        </div>
	</div>
    <input name="tpl" value="<?php echo $this->_var['cate_info']['top_style_tpl']; ?>" type="hidden">
</div>