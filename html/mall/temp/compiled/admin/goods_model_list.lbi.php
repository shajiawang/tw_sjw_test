<?php if ($this->_var['model'] == 1): ?>
<div class="step_top_btn">	
	<a href="javascript:void(0);" class="btn red_btn" ectype="addWarehouse" data-userid="<?php echo $this->_var['user_id']; ?>"><i class="sc_icon sc_icon_warehouse"></i>添加仓库</a>
	<a href="goods_warehouse_batch.php?act=add&goods_id=<?php echo $this->_var['goods_id']; ?>" class="btn red_btn" target="_blank">CSV批量上传</a>	
</div>
<div class="list-div">
	<table cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th width="5%"><div class="tDiv">编号</div></th>
				<th width="9%"><div class="tDiv">仓库名称</div></th>
                <th width="10%"><div class="tDiv">仓库商品货号</div></th>
				<th width="11%"><div class="tDiv">仓库库存</div></th>
				<th width="11%"><div class="tDiv">仓库价格</div></th>
				<th width="11%"><div class="tDiv">仓库促销价格</div></th>
				<th width="11%"><div class="tDiv">赠送消费积分数</div></th>
				<th width="11%"><div class="tDiv">赠送等级积分数</div></th>
				<th width="11%"><div class="tDiv">积分购买金额</div></th>
				<th width="10%" class="handle">操作</th>
			</tr>
		</thead>
		<tbody>
			<?php $_from = $this->_var['warehouse_goods_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('i', 'warehouse');if (count($_from)):
    foreach ($_from AS $this->_var['i'] => $this->_var['warehouse']):
?>
			<tr id="warehouse_<?php echo $this->_var['warehouse']['w_id']; ?>">
				<td><div class="tDiv"><?php echo $this->_var['warehouse']['w_id']; ?></div></td>
				<td><div class="tDiv"><?php echo $this->_var['warehouse']['region_name']; ?></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_warehouse_sn', <?php echo $this->_var['warehouse']['w_id']; ?>)"><?php echo empty($this->_var['warehouse']['region_sn']) ? $this->_var['lang']['n_a'] : $this->_var['warehouse']['region_sn']; ?></span><i class="edit_icon"></i></div></td>
                <td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_warehouse_number', <?php echo $this->_var['warehouse']['w_id']; ?>)"><?php echo $this->_var['warehouse']['region_number']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_warehouse_price', <?php echo $this->_var['warehouse']['w_id']; ?>)"><?php echo $this->_var['warehouse']['warehouse_price']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_warehouse_promote_price', <?php echo $this->_var['warehouse']['w_id']; ?>)"><?php echo $this->_var['warehouse']['warehouse_promote_price']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_warehouse_give_integral', <?php echo $this->_var['warehouse']['w_id']; ?>)"><?php echo $this->_var['warehouse']['give_integral']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_warehouse_rank_integral', <?php echo $this->_var['warehouse']['w_id']; ?>)"><?php echo $this->_var['warehouse']['rank_integral']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_warehouse_pay_integral', <?php echo $this->_var['warehouse']['w_id']; ?>)"><?php echo $this->_var['warehouse']['pay_integral']; ?></span><i class="edit_icon"></i></div></td>
				<td class="handle">
                	<div class="tDiv a1 pl0">
                    <a href="javascript:void(0);" class="btn_trash" ectype="dropWarehouse" data-wid="<?php echo $this->_var['warehouse']['w_id']; ?>"><i class="icon icon-trash"></i>删除</a>
                    </div>
                    <input name="warehouse_id[]" value="<?php echo $this->_var['warehouse']['w_id']; ?>" type="hidden">
               	</td>
			</tr>
			<?php endforeach; else: ?>
			<tr>
				<td colspan="10" align="center" class="no_record"><div class="tDiv">无记录</div></td>
			</tr>
			<?php endif; unset($_from); ?><?php $this->pop_vars();; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="12"></td>
			</tr>
		</tfoot>
	</table>        
</div>
<?php endif; ?>
<?php if ($this->_var['model'] == 2): ?>
<div class="step_top_btn">	
	<a href="javascript:void(0);" class="btn red_btn" ectype="addRegion" data-userid="<?php echo $this->_var['user_id']; ?>" data-goodsid="<?php echo $this->_var['goods_id']; ?>"><i class="sc_icon sc_icon_warehouse"></i><?php echo $this->_var['lang']['add_areaRegion']; ?></a>
	<a href="goods_area_batch.php?act=add&goods_id=<?php echo $this->_var['goods_id']; ?>" class="btn red_btn" target="_blank"><?php echo $this->_var['lang']['add_batch_areaRegion']; ?></a>	
</div>
<div class="list-div">
	<table cellpadding="0" cellspacing="0" border="0">
		<thead>
			<tr>
				<th width="5%"><div class="tDiv">编号</div></th>
				<th width="9%"><div class="tDiv">所属仓库</div></th>
				<th width="7%"><div class="tDiv">地区名称</div></th>
                <th width="9%"><div class="tDiv">地区商品货号</div></th>
				<th width="9%"><div class="tDiv">地区库存</div></th>
				<th width="9%"><div class="tDiv">地区价格</div></th>
				<th width="9%"><div class="tDiv">地区促销价格</div></th>
				<th width="9%"><div class="tDiv">赠送消费积分数</div></th>
				<th width="9%"><div class="tDiv">赠送等级积分数</div></th>
				<th width="9%"><div class="tDiv">积分购买金额</div></th>
				<th width="6%"><div class="tDiv">排序</div></th>
				<th width="6%" class="handle">操作</th>
			</tr>
		</thead>
		<tbody>
			<?php $_from = $this->_var['warehouse_area_goods_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('i', 'area');if (count($_from)):
    foreach ($_from AS $this->_var['i'] => $this->_var['area']):
?>
			<tr>
				<td><div class="tDiv"><?php echo $this->_var['area']['a_id']; ?></div></td>
				<td><div class="tDiv"><?php echo $this->_var['area']['warehouse_name']; ?></div></td>
				<td><div class="tDiv"><?php echo $this->_var['area']['region_name']; ?></div></td>
                <td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_sn', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo empty($this->_var['area']['region_sn']) ? $this->_var['lang']['n_a'] : $this->_var['area']['region_sn']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_number', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo $this->_var['area']['region_number']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_price', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo $this->_var['area']['region_price']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_promote_price', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo $this->_var['area']['region_promote_price']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_give_integral', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo $this->_var['area']['give_integral']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_rank_integral', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo $this->_var['area']['rank_integral']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_pay_integral', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo $this->_var['area']['pay_integral']; ?></span><i class="edit_icon"></i></div></td>
				<td><div class="tDiv"><span onclick="listTable.edit(this, 'edit_region_sort', <?php echo $this->_var['area']['a_id']; ?>)"><?php echo $this->_var['area']['region_sort']; ?></span><i class="edit_icon"></i></div></td>
				<td class="handle">
                    <div class="tDiv a1 pl0">
                    <a href="javascript:void(0);" class="btn_trash" ectype="dropWarehouseArea" data-aid="<?php echo $this->_var['area']['a_id']; ?>"><i class="icon icon-trash"></i>删除</a>
                    </div>
                    <input name="warehouse_area_id[]" value="<?php echo $this->_var['area']['a_id']; ?>" type="hidden">
                </td>
			</tr>
			<?php endforeach; else: ?>
			<tr>
				<td colspan="12" align="center" class="no_record"><div class="tDiv">无记录</div></td>
			</tr>			
			<?php endif; unset($_from); ?><?php $this->pop_vars();; ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="12"></td>
			</tr>
		</tfoot>
	</table>        
</div>
<?php endif; ?>