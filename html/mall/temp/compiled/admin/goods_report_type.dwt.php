<?php if ($this->_var['full_page']): ?>
<!doctype html>
<html>
<head><?php echo $this->fetch('library/admin_html_head.lbi'); ?></head>

<body class="iframe_body">
    <div class="warpper">
        <div class="title">商品 - <?php echo $this->_var['ur_here']; ?></div>
        <div class="content">
            <div class="tabs_info">
                <ul>
                    <li <?php if ($this->_var['act_type'] == 'report_conf'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link4']['href']; ?>"><?php echo $this->_var['action_link4']['text']; ?></a></li>
                    <li <?php if ($this->_var['act_type'] == 'list'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link']['href']; ?>"><?php echo $this->_var['action_link']['text']; ?></a></li>
                    <li <?php if ($this->_var['act_type'] == 'type'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link1']['href']; ?>"><?php echo $this->_var['action_link1']['text']; ?></a></li>
                    <li <?php if ($this->_var['act_type'] == 'title'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link2']['href']; ?>"><?php echo $this->_var['action_link2']['text']; ?></a></li>
                </ul>
            </div>
            <div class="explanation" id="explanation">
                <div class="ex_tit"><i class="sc_icon"></i><h4>操作提示</h4><span id="explanationZoom" title="收起提示"></span></div>
                <ul>
                    <li>商品举报类型管理</li>
                </ul>
            </div>
            <div class="flexilist">
                <div class="common-head">
                    <div class="refresh ml0">
                    	<div class="refresh_tit" title="刷新数据"><i class="icon icon-refresh"></i></div>
                    	<div class="refresh_span">刷新 - 共<?php echo $this->_var['record_count']; ?>条记录</div>
                    </div>
                    <div class="fl">
                    	<a href="<?php echo $this->_var['action_link3']['href']; ?>"><div class="fbutton"><div class="add" title="<?php echo $this->_var['action_link3']['text']; ?>"><span><i class="icon icon-plus"></i><?php echo $this->_var['action_link3']['text']; ?></span></div></div></a>
                    </div>
                </div>
                <div class="common-content">
                    <div class="list-div" id="listDiv">
                        <?php endif; ?>
                        <table cellpadding="0" cellspacing="0" border="0">
                            <thead>
                                <tr>
                                    <th width="10%"><div class="tDiv"><?php echo $this->_var['lang']['record_id']; ?></div></th>
                                    <th width="15%"><div class="tDiv"><?php echo $this->_var['lang']['goods_report_type']; ?></div></th>
                                    <th width="58%"><div class="tDiv"><?php echo $this->_var['lang']['type_desc']; ?></div></th>
                                    <th width="10%"><div class="tDiv"><?php echo $this->_var['lang']['is_show']; ?></div></th>
                                    <th width="12%"><div align="center"><?php echo $this->_var['lang']['handler']; ?></div></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $_from = $this->_var['type_info']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'info');if (count($_from)):
    foreach ($_from AS $this->_var['info']):
?>
                                <tr>
                                    <td><div class="tDiv"><?php echo $this->_var['info']['type_id']; ?></div></td>
                                    <td><div class="tDiv"><?php echo $this->_var['info']['type_name']; ?></div></td>
                                    <td><div class="tDiv"><?php echo $this->_var['info']['type_desc']; ?></div></td>
                                    <td>
                                    	<div class="tDiv">
                                            <div class="switch <?php if ($this->_var['info']['is_show'] == 1): ?>active<?php endif; ?>" title="<?php if ($this->_var['info']['is_show'] == 1): ?>是<?php else: ?>否<?php endif; ?>" onclick="listTable.switchBt(this, 'toggle_show', <?php echo $this->_var['info']['type_id']; ?>)">
                                            	<div class="circle"></div>
                                            </div>
                                            <input type="hidden" value="<?php echo $this->_var['info']['is_show']; ?>" name="">
                                        </div>
                                    </td>
                                    <td class="handle">
                                        <div class="tDiv a3" align="center">
                                            <a href="goods_report.php?act=type_edit&type_id=<?php echo $this->_var['info']['type_id']; ?>" class="btn_see"><i class="sc_icon icon-edit"></i><?php echo $this->_var['lang']['edit']; ?></a>
                                            <a href="javascript:;" onclick="listTable.remove(<?php echo $this->_var['info']['type_id']; ?>, '<?php echo $this->_var['lang']['drop_confirm']; ?>','remove_type')" title="<?php echo $this->_var['lang']['remove']; ?>" class="btn_trash"><i class="icon icon-trash"></i><?php echo $this->_var['lang']['remove']; ?></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php endforeach; else: ?>
                                <tr><td class="no-records" colspan="8"><?php echo $this->_var['lang']['no_records']; ?></td></tr>
                                <?php endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            </tbody>  
                            <tfoot>
                            	<tr>
                                    <td colspan="8">
                                        <div class="list-page">
                                            <?php echo $this->fetch('library/page.lbi'); ?>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>    
                        <?php if ($this->_var['full_page']): ?>
                    </div>
                </div>
                <!--商品分类列表end-->
            </div>

        </div>
    </div>
 <?php echo $this->fetch('library/pagefooter.lbi'); ?>
    <script type="text/javascript">
            listTable.recordCount = <?php echo empty($this->_var['record_count']) ? '0' : $this->_var['record_count']; ?>;
                listTable.pageCount = <?php echo empty($this->_var['page_count']) ? '1' : $this->_var['page_count']; ?>;
                listTable.act_type = '<?php echo $this->_var['act_type']; ?>';
                listTable.query = 'type_query';
                <?php $_from = $this->_var['filter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
                listTable.filter.<?php echo $this->_var['key']; ?> = '<?php echo $this->_var['item']; ?>';
                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
    </script>
</body>
</html>
<?php endif; ?>