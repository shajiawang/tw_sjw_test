<?php if ($this->_var['full_page']): ?>
<!doctype html>
<html>
<head><?php echo $this->fetch('library/admin_html_head.lbi'); ?></head>

<body class="iframe_body">
	<div class="warpper">
    	<div class="title">商品 - <?php echo $this->_var['ur_here']; ?></div>
        <div class="content">
        	<div class="tabs_info">
            	<ul>
                    <li <?php if ($this->_var['act_type'] == 'report_conf'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link3']['href']; ?>"><?php echo $this->_var['action_link3']['text']; ?></a></li>
                    <li <?php if ($this->_var['act_type'] == 'list'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link']['href']; ?>"><?php echo $this->_var['action_link']['text']; ?></a></li>
                    <li <?php if ($this->_var['act_type'] == 'type'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link1']['href']; ?>"><?php echo $this->_var['action_link1']['text']; ?></a></li>
                    <li <?php if ($this->_var['act_type'] == 'title'): ?>class="curr"<?php endif; ?>><a href="<?php echo $this->_var['action_link2']['href']; ?>"><?php echo $this->_var['action_link2']['text']; ?></a></li>
                    
                </ul>
            </div>
        	<div class="explanation" id="explanation">
            	<div class="ex_tit"><i class="sc_icon"></i><h4>操作提示</h4><span id="explanationZoom" title="收起提示"></span></div>
                <ul>
                	<li>商品举报相关信息管理。</li>
			<li>举报类型和举报主题由管理员在后台设置，在商品信息页会员可根据举报主题举报违规商品，点击详细，查看举报内容</li>
                </ul>
            </div>
            <div class="flexilist">
                <div class="common-head">
                   	<div class="refresh ml0">
                    	<div class="refresh_tit" title="刷新数据"><i class="icon icon-refresh"></i></div>
                    	<div class="refresh_span">刷新 - 共<?php echo $this->_var['record_count']; ?>条记录</div>
                    </div>
                    <form action="javascript:searchUser()" name="searchForm">
                        <div class="search">
                            <div class="select">
                                <div class="fl"><?php echo $this->_var['lang']['report_state']; ?>：</div>
                                <div id="" class="imitate_select select_w170">
                                    <div class="cite">请选择</div>
                                    <ul>
                                       <li><a href="javascript:;" data-value="-1">请选择</a></li>
                                       <li><a href="javascript:;" data-value="6"><?php echo $this->_var['lang']['handle_type_off']; ?></a></li>
                                        <li><a href="javascript:;" data-value="7"><?php echo $this->_var['lang']['handle_type_on']; ?></a></li>
                                    </ul>
                                    <input name="handle_type" type="hidden" value="-1">
                                </div>
                            </div>
                            <div class="input">
                                <input type="text" name="keywords" class="text nofocus" placeholder="<?php echo $this->_var['lang']['report_user']; ?>/<?php echo $this->_var['lang']['report_goods']; ?>" autocomplete="off" /><input type="submit" value="" class="not_btn" />
                            </div>
                            
                        </div>
                    </form>
                </div>
                <div class="common-content">
                	<div class="list-div" id="listDiv">
                            <?php endif; ?>
                    	<table cellpadding="0" cellspacing="0" border="0">
                        	<thead>
                            	<tr>
                                  <th width="5%"><div class="tDiv"><?php echo $this->_var['lang']['record_id']; ?></div></th>
                                  <th width="8%"><div class="tDiv"><?php echo $this->_var['lang']['involve_shop']; ?></div></th>
                                  <th width="9%"><div class="tDiv"><?php echo $this->_var['lang']['report_user']; ?></div></th>
                                  <th width="13%"><div class="tDiv"><?php echo $this->_var['lang']['goods_report_type']; ?></div></th>
                                  <th width="13%"><div class="tDiv"><?php echo $this->_var['lang']['goods_report_title']; ?></div></th>
                                  <th width="13%"><div class="tDiv"><?php echo $this->_var['lang']['report_goods']; ?></div></th>
                                  <th width="13%"><div class="tDiv"><?php echo $this->_var['lang']['report_should']; ?></div></th>
                                  <th width="10%"><div class="tDiv"><?php echo $this->_var['lang']['report_state']; ?></div></th>
                                  <th width="10%"><div class="tDiv"><?php echo $this->_var['lang']['action_info']; ?></div></th>
                                  <th width="6%"><div align="center"><?php echo $this->_var['lang']['handler']; ?></div></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php $_from = $this->_var['goods_report']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'report');if (count($_from)):
    foreach ($_from AS $this->_var['report']):
?>
                                <tr>
                                  <td><div class="tDiv"><?php echo $this->_var['report']['report_id']; ?></div></td>
                                  <td><div class="tDiv"><font class="red"><?php echo $this->_var['report']['shop_name']; ?></font></div></td>
                                  <td><div class="tDiv"><?php echo $this->_var['report']['user_name']; ?></div></td>
                                  <td><div class="tDiv"><?php echo $this->_var['report']['type_name']; ?></div></td>
                                  <td><div class="tDiv"><?php echo $this->_var['report']['title_name']; ?></div></td>
                                  <td>
                                      <div class="tDiv goods_list_info">
                                          <div class="img"><a href="../<?php echo $this->_var['report']['url']; ?>" target="_blank" title="<?php echo $this->_var['report']['goods_name']; ?>"><img src="<?php echo $this->_var['report']['goods_image']; ?>" width="60" height="60"></a></div>
                                          <div class="desc">
                                              <div class="name">
                                                  <span title="<?php echo $this->_var['report']['goods_name']; ?>" data-toggle="tooltip" class="span"><?php echo $this->_var['report']['goods_name']; ?></span>
                                              </div>
                                          </div>
                                  </td>
                                  <td>
                                      <div class="tDiv">
                                          <?php $_from = $this->_var['report']['img_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'img');if (count($_from)):
    foreach ($_from AS $this->_var['img']):
?>
                                          <span class="show">
                                              <a target="_blank" href="<?php echo $this->_var['img']['img_file']; ?>" class="nyroModal"><i class="icon icon-picture" onmouseover="toolTip('<img src=<?php echo $this->_var['img']['img_file']; ?>>')" onmouseout="toolTip()"></i></a>
                                          </span>
                                          <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                      </div>
                                  </td>  
                                  <td>
                                        <div class="tDiv">
                                            <?php if ($this->_var['report']['report_state'] == 0): ?>
                                            未处理
                                            <?php elseif ($this->_var['report']['report_state'] == 2): ?>
                                            用户取消
                                            <?php elseif ($this->_var['report']['report_state'] == 3): ?>
                                            用户删除
                                            <?php else: ?>
                                            <?php if ($this->_var['report']['handle_type'] == 1): ?>无效举报<?php elseif ($this->_var['report']['handle_type'] == 2): ?>恶意举报<?php else: ?>有效举报<?php endif; ?>
                                            <?php endif; ?>
                                        </div>
                                  </td>  
                                  <td>
                                      <div class="tDiv">
                                          <p><?php echo $this->_var['report']['admin_name']; ?></p>
                                          <p><?php echo $this->_var['report']['handle_time']; ?></p>
                                      </div>
                                  </td>
                                  <td class="handle"><div class="tDiv a3" align="center">
                                    <a href="goods_report.php?act=check_state&report_id=<?php echo $this->_var['report']['report_id']; ?>" class="btn_see"><i class="sc_icon sc_icon_see"></i><?php echo $this->_var['lang']['view']; ?></a>
                                    <?php if ($this->_var['report']['report_state'] == 3): ?>
                                    <a href="javascript:;" onclick="listTable.remove(<?php echo $this->_var['report']['report_id']; ?>, '<?php echo $this->_var['lang']['drop_confirm']; ?>')" title="<?php echo $this->_var['lang']['remove']; ?>" class="btn_trash"><i class="icon icon-trash"></i><?php echo $this->_var['lang']['remove']; ?></a>
                                    <?php endif; ?>
                                  </div></td>
                                </tr>
                              <?php endforeach; else: ?>
                              <tr><td class="no-records" colspan="10"><?php echo $this->_var['lang']['no_records']; ?></td></tr>
                              <?php endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            </tbody>   
                            <tfoot>
                            	<tr>
                                    <td colspan="10">
                                        <div class="list-page">
                                            <?php echo $this->fetch('library/page.lbi'); ?>
                                        </div>
                                    </td>
                                </tr>
                            </tfoot>
                        </table>    
                            <?php if ($this->_var['full_page']): ?>
                    </div>
                </div>
                <!--商品分类列表end-->
            </div>
		</div>
	</div>
 <?php echo $this->fetch('library/pagefooter.lbi'); ?>
    <script type="text/javascript" src="js/jquery.purebox.js"></script>
    
    <script type="text/javascript">
        
                listTable.recordCount = <?php echo empty($this->_var['record_count']) ? '0' : $this->_var['record_count']; ?>;
                listTable.pageCount = <?php echo empty($this->_var['page_count']) ? '1' : $this->_var['page_count']; ?>;
                listTable.act_type = '<?php echo $this->_var['act_type']; ?>';

                <?php $_from = $this->_var['filter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
                listTable.filter.<?php echo $this->_var['key']; ?> = '<?php echo $this->_var['item']; ?>';
                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
		

/**
 * 搜索用户
 */
function searchUser()
{
    
    var frm = $("form[name='searchForm']");
    listTable.filter['handle_type'] = Utils.trim(frm.find("input[name='handle_type']").val());
    listTable.filter['keywords'] = Utils.trim(frm.find("input[name='keywords']").val());
	
    listTable.filter['page'] = 1;
    listTable.loadList();
}
    </script>
</body>
</html>
<?php endif; ?>
