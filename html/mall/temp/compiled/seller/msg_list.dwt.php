<?php if ($this->_var['full_page']): ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head><?php echo $this->fetch('library/seller_html_head.lbi'); ?></head>

<body>
<?php echo $this->fetch('library/seller_header.lbi'); ?>
 <div class="ecsc-layout">
    <div class="site wrapper">
		<?php echo $this->fetch('library/seller_menu_left.lbi'); ?>
		<div class="ecsc-layout-right">
            <div class="main-content" id="mainContent">
				<?php echo $this->fetch('library/url_here.lbi'); ?>
				<?php echo $this->fetch('library/seller_menu_tab.lbi'); ?>
                <?php endif; ?>
                <form method="POST" action="user_msg.php?act=batch_drop" name="listForm" onsubmit="return confirm_bath()">
                <!-- start article list -->
                <div class="list-div" id="listDiv">
                <table class="ecsc-default-table ecsc-table-seller mt20">
                  <tbody>
                  <tr>
                    <th width="8%">
                      <div class="first_all">
                          <input onclick='listTable.selectAll(this, "checkboxes")' type="checkbox" id="all" class="ui-checkbox" />
                          <label for="all" class="ui-label"><a href="javascript:listTable.sort('msg_id'); "><?php echo $this->_var['lang']['msg_id']; ?></a></label>
                          <div class="img"><?php echo $this->_var['sort_msg_id']; ?></div>
                      </div>
                    </th>
                    <th width="10%"><a href="javascript:listTable.sort('user_name'); "><?php echo $this->_var['lang']['user_name']; ?></a><?php echo $this->_var['sort_user_name']; ?></th>
                    <th width="30%"><a href="javascript:listTable.sort('msg_title'); "><?php echo $this->_var['lang']['msg_title']; ?></a><?php echo $this->_var['sort_msg_title']; ?></th>
                    <th width="10%"><a href="javascript:listTable.sort('msg_type'); "><?php echo $this->_var['lang']['msg_type']; ?></a><?php echo $this->_var['sort_msg_type']; ?></th>
                    <th width="15%"><a href="javascript:listTable.sort('msg_time'); "><?php echo $this->_var['lang']['msg_time']; ?></a><?php echo $this->_var['sort_msg_time']; ?></th>
                    <th width="10%"><a href="javascript:listTable.sort('msg_status'); "><?php echo $this->_var['lang']['msg_status']; ?></a><?php echo $this->_var['sort_msg_status']; ?></th>
                    <th width="10%"><a href="javascript:listTable.sort('reply'); "><?php echo $this->_var['lang']['reply']; ?></a><?php echo $this->_var['sort_reply']; ?></th>
                    <th width="10%"><?php echo $this->_var['lang']['handler']; ?></th>
                  </tr>
                  <?php $_from = $this->_var['msg_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'msg');if (count($_from)):
    foreach ($_from AS $this->_var['msg']):
?>
                  <tr>
                    <td class="first_td_checkbox"><div class="first_all"><input type="checkbox" name="checkboxes[]" value="<?php echo $this->_var['msg']['msg_id']; ?>" id="checkbox_<?php echo $this->_var['msg']['msg_id']; ?>" class="ui-checkbox" /><label for="checkbox_<?php echo $this->_var['msg']['msg_id']; ?>" class="ui-label"><?php echo $this->_var['msg']['msg_id']; ?></label></div></td>
                    <td align="center"><?php echo $this->_var['msg']['user_name']; ?></td>
                    <td align="left"><?php echo htmlspecialchars(sub_str($this->_var['msg']['msg_title'],40)); ?></td>
                    <td align="center"><?php echo $this->_var['msg']['msg_type']; ?><?php if ($this->_var['msg']['order_id']): ?><br><a href="order.php?act=info&order_id=<?php echo $this->_var['msg']['order_id']; ?>"><?php echo $this->_var['msg']['order_sn']; ?><?php endif; ?></a></td>
                    <td align="center"  nowrap="nowrap"><?php echo $this->_var['msg']['msg_time']; ?></td>
                    <?php if ($this->_var['msg']['msg_area'] == 0): ?>
                    <td align="center"><?php echo $this->_var['lang']['display']; ?></td>
                    <?php else: ?>
                    <td align="center"><?php if ($this->_var['msg']['msg_status'] == 0): ?><?php echo $this->_var['lang']['hidden']; ?><?php else: ?><?php echo $this->_var['lang']['display']; ?><?php endif; ?></td>
                    <?php endif; ?>
                    <td align="center"><?php if ($this->_var['msg']['reply'] == 0): ?><?php echo $this->_var['lang']['unreplyed']; ?><?php else: ?><?php echo $this->_var['lang']['replyed']; ?><?php endif; ?></td>
                    <td align="center" class="ecsc-table-handle tr">
                      <a href="user_msg.php?act=view&id=<?php echo $this->_var['msg']['msg_id']; ?>" title="<?php echo $this->_var['lang']['view']; ?>"><i class="icon icon-search"></i></a>
                      <a href="javascript:;" onclick="listTable.remove(<?php echo $this->_var['msg']['msg_id']; ?>, '<?php echo $this->_var['lang']['drop_confirm']; ?>')"  title="<?php echo $this->_var['lang']['remove']; ?>"><i class="icon icon-trash"></i></a>
                    </td>
                  </tr>
                  <?php endforeach; else: ?>
                  <tr><td class="no-records" colspan="7"><?php echo $this->_var['lang']['no_records']; ?></td></tr>
                  <?php endif; unset($_from); ?><?php $this->pop_vars();; ?>
                  </tbody>
                  <tfoot>
					<tr>
					  <td class="td_border" colspan="10">
						<div class="shenhe">
						  <select name="sel_action" class="select mr10">
							<option value=""><?php echo $this->_var['lang']['select_please']; ?></option>
							<option value="remove"><?php echo $this->_var['lang']['delete']; ?></option>
							<option value="allow"><?php echo $this->_var['lang']['allow']; ?></option>
							<option value="deny"><?php echo $this->_var['lang']['forbid']; ?></option>
						  </select>
						  <input type="hidden" name="act" value="batch" />
						  <input type="submit" name="drop" id="btnSubmit" value="<?php echo $this->_var['lang']['button_submit']; ?>" class="sc-btn btn_disabled" disabled="true" />
						</div>
					  </td>
					</tr>
                    <tr>
                        <td colspan="20"><?php echo $this->fetch('page.dwt'); ?></td>
                    </tr>
                  </tfoot>
                </table>
                </div>
                <!-- end article list -->
                </form>
			</div>
		</div>

	</div>
</div>
<?php echo $this->fetch('library/seller_footer.lbi'); ?>
<?php if ($this->_var['full_page']): ?>
<script type="text/javascript">
listTable.recordCount = <?php echo $this->_var['record_count']; ?>;
listTable.pageCount = <?php echo $this->_var['page_count']; ?>;
cfm = new Object();
cfm['allow'] = '<?php echo $this->_var['lang']['cfm_allow']; ?>';
cfm['remove'] = '<?php echo $this->_var['lang']['cfm_remove']; ?>';
cfm['deny'] = '<?php echo $this->_var['lang']['cfm_deny']; ?>';
<?php $_from = $this->_var['filter']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'item');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['item']):
?>
listTable.filter.<?php echo $this->_var['key']; ?> = '<?php echo $this->_var['item']; ?>';
<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>

<!--
onload = function()
{
    // 开始检查订单
    startCheckOrder();
}

/**
 * 搜索标题
 */
function searchMsg()
{
    var keyword = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    var msgType = document.forms['searchForm'].elements['msg_type'].value;

    listTable.filter['keywords'] = Utils.trim(document.forms['searchForm'].elements['keyword'].value);
    listTable.filter['msg_type'] = document.forms['searchForm'].elements['msg_type'].value;
    listTable.filter['page'] = 1;
    listTable.loadList();
}

function confirm_bath()
{
    var action = document.forms['listForm'].elements['sel_action'].value;
    if (action == 'allow'||action == 'remove'||action == 'deny')
      {
          return confirm(cfm[action]);
      }
}
//-->
</script>


</body>
</html>
<?php endif; ?>