<?php if ($this->_var['history_goods']): ?>
<div class="atwillgo" id="atwillgo">
	<div class="awg-hd">
		<h2>浏览记录</h2>
	</div>
	<div class="awg-bd">
		<div class="atwillgo-slide">
			<a href="javascript:;" class="prev"><i class="iconfont icon-left"></i></a>
			<a href="javascript:;" class="next"><i class="iconfont icon-right"></i></a>
			<div class="hd">
				<ul></ul>
			</div>
			<div class="bd">
				<ul>
					<?php $_from = $this->_var['history_count']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'hi');if (count($_from)):
    foreach ($_from AS $this->_var['hi']):
?>
					<?php $_from = $this->_var['hi']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods_0_63707100_1564561026');if (count($_from)):
    foreach ($_from AS $this->_var['goods_0_63707100_1564561026']):
?>
					<li>
						<div class="p-img"><a href="<?php echo $this->_var['goods_0_63707100_1564561026']['url']; ?>"><img src="<?php echo $this->_var['goods_0_63707100_1564561026']['goods_thumb']; ?>" alt=""></a></div>
						<div class="p-price">
                            <?php if ($this->_var['goods_0_63707100_1564561026']['promote_price'] != ''): ?>
                                <?php echo $this->_var['goods_0_63707100_1564561026']['promote_price']; ?>
                            <?php else: ?>
                                <?php echo $this->_var['goods_0_63707100_1564561026']['shop_price']; ?>
                            <?php endif; ?>
						</div>
						<div class="p-name"><a href="<?php echo $this->_var['goods_0_63707100_1564561026']['url']; ?>"><?php echo $this->_var['goods_0_63707100_1564561026']['short_name']; ?></a></div>
						<div class="p-btn"><a href="<?php echo $this->_var['goods_0_63707100_1564561026']['url']; ?>">加入购物车</a></div>
					</li>
					<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
					<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
				</ul>
			</div>
		</div>
	</div>
</div>
<input type="hidden" name="history" value="1">
<?php else: ?>
<input type="hidden" name="history" value="0">
<?php endif; ?>
