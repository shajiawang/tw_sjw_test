<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="Keywords" content="<?php echo $this->_var['keywords']; ?>" />
<meta name="Description" content="<?php echo $this->_var['description']; ?>" />

<title><?php echo $this->_var['page_title']; ?></title>



<link rel="shortcut icon" href="favicon.ico" />
<?php echo $this->fetch('library/js_languages_new.lbi'); ?>
</head>

<body>
	<?php echo $this->fetch('library/page_header_common.lbi'); ?>
	<div class="full-main-n">
        <div class="w w1200 relative">
			<?php echo $this->fetch('library/ur_here.lbi'); ?>
        </div>
    </div>
    <div class="container">
    	<div class="w w1200">
        	<div class="product-info">
            	<?php echo $this->fetch('library/seckill_goods_gallery.lbi'); ?>
                <div class="product-wrap">
                    <form action="seckill.php?act=buy" method="post" name="ECS_FORMBUY" id="ECS_FORMBUY" >
                	<div class="name"><?php echo htmlspecialchars($this->_var['goods']['goods_name']); ?></div>
                    <?php if ($this->_var['goods']['goods_brief']): ?>
                    <div class="newp"><?php echo $this->_var['goods']['goods_brief']; ?></div>
					<?php endif; ?>
                    <div class="activity-title">
                    	<div class="activity-type"><i class="icon icon-promotion"></i><?php echo $this->_var['lang']['seckill']; ?></div>
                        <div class="sk-time-cd">
                            <div class="sk-cd-tit"><?php if ($this->_var['goods']['is_end'] && ! $this->_var['goods']['status']): ?><?php echo $this->_var['lang']['end_time']; ?><?php elseif (! $this->_var['goods']['is_end'] && $this->_var['goods']['status']): ?><?php echo $this->_var['lang']['residual_time']; ?><?php else: ?><?php echo $this->_var['lang']['begin_time_soon']; ?><?php endif; ?></div>
                            <div class="cd-time time" ectype="time" data-time="<?php if (! $this->_var['goods']['is_end'] && $this->_var['goods']['status']): ?><?php echo $this->_var['goods']['formated_end_date']; ?><?php else: ?><?php echo $this->_var['goods']['formated_start_date']; ?><?php endif; ?>">
                                <?php if ($this->_var['goods']['is_end'] && ! $this->_var['goods']['status']): ?>
                                    <?php echo $this->_var['goods']['formated_end_date']; ?>
                                <?php else: ?>
                                    <div class="days hide">00</div>
                                    <span class="split hide">:</span>
                                    <div class="hours">00</div>
                                    <span class="split">:</span>
                                    <div class="minutes">00</div>
                                    <span class="split">:</span>
                                    <div class="seconds">00</div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="summary">
                    	<div class="summary-price-wrap">
                        	<div class="s-p-w-wrap">
                                <div class="summary-item si-shop-price">
                                    <div class="si-tit"><?php echo $this->_var['lang']['seckill_price']; ?></div>
                                    <div class="si-warp">
                                        <strong class="shop-price"><em>￥</em><?php echo $this->_var['goods']['sec_price']; ?></strong>
                                    </div>
                                </div>
                                <div class="summary-item si-market-price">
                                    <div class="si-tit"><?php echo $this->_var['lang']['market_prices']; ?></div>
                                    <div class="si-warp"><div class="m-price"><em>￥</em><?php echo $this->_var['goods']['market_price']; ?></div></div>
                                </div>
                                <div class="si-info">
                                    <div class="si-cumulative"><?php echo $this->_var['lang']['Sales_count']; ?><em><?php echo $this->_var['goods']['valid_goods']; ?></em></div>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </div>
                        
                        <div class="summary-basic-info">
                        	<div class="summary-item is-stock">
                            	<div class="si-tit"><?php echo $this->_var['lang']['Distribution']; ?></div>
                                <div class="si-warp">
                                    <span class="initial-area">
                                        <?php if ($this->_var['adress']['city']): ?>
                                            <?php echo $this->_var['adress']['city']; ?>
                                        <?php else: ?>
                                            <?php echo $this->_var['basic_info']['city']; ?>
                                        <?php endif; ?> 
                                    </span>
									<span>至</span>
                                    <div class="store-selector">
                                    	<div class="text-select" id="area_address" ectype="areaSelect"></div>
                                    </div>
                                    <div class="store-warehouse">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="summary-item is-service">
                            	<div class="si-tit"><?php echo $this->_var['lang']['gs_service']; ?></div>
                                <div class="si-warp">
									由 <a href="<?php echo $this->_var['goods']['store_url']; ?>" class="link-red" target="_blank"><?php echo $this->_var['goods']['rz_shopName']; ?></a> 发货并提供售后服务
									<?php if ($this->_var['shippingFee']['is_shipping'] != 1): ?>
									<span class="gary"><?php echo $this->_var['lang']['is_shipping_area']; ?></span>
									<?php else: ?>
									<span class="gary">[ <?php echo $this->_var['lang']['shipping']; ?>：<?php echo $this->_var['shippingFee']['shipping_fee_formated']; ?> ]</span>
									<?php endif; ?>
								</div>
                            </div>

                            <?php if ($this->_var['goods']['sec_limit']): ?>
                            <div class="summary-item is-xiangou">
                            	<div class="si-tit"><?php echo $this->_var['lang']['gb_limited']; ?></div>
                                <div class="si-warp">
                                	<em id="restrict_amount" ectype="restrictNumber" data-value="<?php echo $this->_var['goods']['sec_limit']; ?>"><?php echo $this->_var['goods']['sec_limit']; ?></em>
                                    <span><?php if ($this->_var['goods']['goods_unit']): ?><?php echo $this->_var['goods']['goods_unit']; ?><?php else: ?><?php echo $this->_var['goods']['measure_unit']; ?><?php endif; ?></span>
                                    <span>（<?php echo $this->_var['lang']['js_languages']['Already_buy']; ?>：<em id="orderG_number" ectype="orderGNumber"><?php echo empty($this->_var['orderG_number']) ? '0' : $this->_var['orderG_number']; ?></em> <?php if ($this->_var['goods']['goods_unit']): ?><?php echo $this->_var['goods']['goods_unit']; ?><?php else: ?><?php echo $this->_var['goods']['measure_unit']; ?><?php endif; ?>）</span>
                                </div>
                            </div>
                            <?php endif; ?>
                            
                            <?php $_from = $this->_var['specification']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('spec_key', 'spec');if (count($_from)):
    foreach ($_from AS $this->_var['spec_key'] => $this->_var['spec']):
?>
							<?php if ($this->_var['spec']['values']): ?>
                            <div class="summary-item is-attr goods_info_attr" ectype="is-attr" data-type="<?php if ($this->_var['spec']['attr_type'] == 1): ?>radio<?php else: ?>checkeck<?php endif; ?>">
                            	<div class="si-tit"><?php echo $this->_var['spec']['name']; ?></div>
								<?php if ($this->_var['cfg']['goodsattr_style'] == 1): ?>
                                <div class="si-warp">
									<ul>
                                    <?php $_from = $this->_var['spec']['values']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'value');$this->_foreach['attrvalues'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['attrvalues']['total'] > 0):
    foreach ($_from AS $this->_var['key'] => $this->_var['value']):
        $this->_foreach['attrvalues']['iteration']++;
?>  
									<?php if ($this->_var['spec']['is_checked'] > 0): ?>
                                    <li class="item <?php if ($this->_var['value']['checked'] == 1 && $this->_var['cfg']['add_shop_price'] == 1): ?> selected<?php endif; ?>" date-rev="<?php echo $this->_var['value']['img_site']; ?>" data-name="<?php echo $this->_var['value']['id']; ?>">
                                        <b></b>
                                        <a href="javascript:void(0);">
											<?php if ($this->_var['value']['img_flie']): ?>
											<img src="<?php echo $this->_var['value']['img_flie']; ?>" width="24" height="24" />
											<?php endif; ?>
											<i><?php echo $this->_var['value']['label']; ?></i>
											<input id="spec_value_<?php echo $this->_var['value']['id']; ?>" type="<?php if ($this->_var['spec']['attr_type'] == 2): ?>checkbox<?php else: ?>radio<?php endif; ?>" data-attrtype="<?php if ($this->_var['spec']['attr_type'] == 2): ?>2<?php else: ?>1<?php endif; ?>" name="spec_<?php echo $this->_var['spec_key']; ?>" value="<?php echo $this->_var['value']['id']; ?>" autocomplete="off" class="hide" />
											<?php if ($this->_var['value']['checked'] == 1): ?>
											<script type="text/javascript">
												$(function(){
													<?php if ($this->_var['cfg']['add_shop_price'] == 1): ?>
													$("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", true);
													<?php else: ?>
													$("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", false);
													<?php endif; ?>
												});
											</script>
											<?php endif; ?>
                                        </a>
                                    </li>
									<?php else: ?>
                                    <li class="item <?php if ($this->_var['key'] == 0 && $this->_var['cfg']['add_shop_price'] == 1): ?> selected<?php endif; ?>">
                                        <b></b>
                                        <a href="javascript:void(0);" name="<?php echo $this->_var['value']['id']; ?>" class="noimg">
											<i><?php echo $this->_var['value']['label']; ?></i>
											<input id="spec_value_<?php echo $this->_var['value']['id']; ?>" type="<?php if ($this->_var['spec']['attr_type'] == 2): ?>checkbox<?php else: ?>radio<?php endif; ?>" data-attrtype="<?php if ($this->_var['spec']['attr_type'] == 2): ?>2<?php else: ?>1<?php endif; ?>" name="spec_<?php echo $this->_var['spec_key']; ?>" value="<?php echo $this->_var['value']['id']; ?>" autocomplete="off" class="hide" /></a> 
											<?php if ($this->_var['key'] == 0): ?>
											<script type="text/javascript">
												$(function(){
													<?php if ($this->_var['cfg']['add_shop_price'] == 1): ?>
													$("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", true);
													<?php else: ?>
													$("#spec_value_<?php echo $this->_var['value']['id']; ?>").prop("checked", false);
													<?php endif; ?>
												});
											</script>
											<?php endif; ?>											
                                        </a>
                                    </li>									
									<?php endif; ?>
									<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                    </ul>
                                </div>
								<?php else: ?>
								...
								<?php endif; ?>
                            </div>
							<?php endif; ?>
							<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            
                            <div class="summary-item is-number">
                            	<div class="si-tit"><?php echo $this->_var['lang']['gs_number']; ?></div>
                                <div class="si-warp">
                                	<div class="amount-warp">
                                        <input class="text buy-num" id="quantity" ectype="quantity" value="1" name="number" defaultnumber="1">
                                        <div class="a-btn">
                                            <a href="javascript:void(0);" class="btn-add" ectype="btnAdd"><i class="iconfont icon-up"></i></a>
                                            <a href="javascript:void(0);" class="btn-reduce btn-disabled" ectype="btnReduce"><i class="iconfont icon-down"></i></a>
                                            <input type="hidden" name="perNumber" id="perNumber" ectype="perNumber" value="0">
                                            <input type="hidden" name="perMinNumber" id="perMinNumber" ectype="perMinNumber" value="1">
                                        </div>
                                    </div>
                                    <span><?php echo $this->_var['lang']['goods_inventory']; ?>&nbsp;<em id="goods_attr_num" ectype="goods_attr_num"></em></span>
                                </div>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="choose-btns ml60 clearfix">
                            <?php if ($this->_var['goods']['is_end'] && ! $this->_var['goods']['status']): ?>
                            <a href="javascript:void(0);" class="btn-invalid"><?php echo $this->_var['lang']['seckill_end']; ?></a>
                            <?php elseif ($this->_var['goods']['sec_num'] <= 0 && $this->_var['goods']['status']): ?>
                            <a href="javascript:void(0);" class="btn-invalid"><?php echo $this->_var['lang']['over_tobuy']; ?></a>
                            <?php elseif (! $this->_var['goods']['is_end'] && ! $this->_var['goods']['status']): ?>
                            <a href="javascript:void(0);" class="btn-invalid"><?php echo $this->_var['lang']['comming_soon']; ?></a>
                            <?php else: ?>
                            <a href="javascript:void(0);" class="btn-append" ectype="btn-seckill"><?php echo $this->_var['lang']['seckill_now']; ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                    <input type="hidden" value="<?php echo $this->_var['goods']['goods_id']; ?>" id="good_id" name="good_id">
                    <input type="hidden" value="<?php echo $this->_var['region_id']; ?>" id="region_id" name="region_id">
                    <input type="hidden" value="<?php echo $this->_var['area_id']; ?>" id="area_id" name="area_id">
                    <input type="hidden" value="<?php echo $this->_var['goods']['id']; ?>" name="sec_goods_id" />
                    <input type="hidden" value="<?php echo $this->_var['goods']['sec_limit']; ?>" name="restrictShop" ectype="restrictShop" />
                    <input type="hidden" name="goods_spec" />
                    </form>
                </div>
                <div class="track">
                	<div class="track_warp">
                    	<div class="track-tit"><h3><?php echo $this->_var['lang']['see_to_see']; ?></h3><span></span></div>
                        <div class="track-con">
                        	<ul>
                                <?php $_from = $this->_var['look_top']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'look_top_0_72183300_1564028648');$this->_foreach['looktop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['looktop']['total'] > 0):
    foreach ($_from AS $this->_var['look_top_0_72183300_1564028648']):
        $this->_foreach['looktop']['iteration']++;
?>
                                <li>
                                	<div class="p-img"><a href="<?php echo $this->_var['look_top_0_72183300_1564028648']['url']; ?>" target="_blank" title="<?php echo $this->_var['look_top_0_72183300_1564028648']['goods_name']; ?>"><img src="<?php echo $this->_var['look_top_0_72183300_1564028648']['goods_thumb']; ?>" width="140" height="140"></a></div>
                                    <div class="p-name"><a href="<?php echo $this->_var['look_top_0_72183300_1564028648']['url']; ?>" target="_blank" title="<?php echo $this->_var['look_top_0_72183300_1564028648']['goods_name']; ?>"><?php echo $this->_var['look_top_0_72183300_1564028648']['goods_name']; ?></a></div>
                                    <div class="price">
                                        <?php if ($this->_var['look_top_0_72183300_1564028648']['promote_price'] != ''): ?>
                                            <?php echo $this->_var['look_top_0_72183300_1564028648']['promote_price']; ?>
                                        <?php else: ?>
                                            <?php echo $this->_var['look_top_0_72183300_1564028648']['shop_price']; ?>
                                        <?php endif; ?>										
                                    </div>
                                </li>
                                <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            </ul>
                        </div>
                        <div class="track-more">
                        	<a href="javascript:void(0);" class="sprite-up"><i class="iconfont icon-up"></i></a>
                            <a href="javascript:void(0);" class="sprite-down"><i class="iconfont icon-down"></i></a>
                        </div>
                    </div>
                </div>
                <div class="clear"></div>
            </div>
            <div class="goods-main-layout">
            	<div class="g-m-left">
                	<?php echo $this->fetch('library/goods_merchants.lbi'); ?>
                    <div class="g-main g-recommend">
                    	<div class="mt">
                        	<h3><?php echo $this->_var['lang']['merchant_rec']; ?></h3>
                        </div>
                        <div class="mc">
                        	<div class="mc-warp">
                            	<ul>
                                    <?php $_from = $this->_var['merchant_seckill_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'merchant_seckill_goods_0_72193700_1564028648');$this->_foreach['buytop'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['buytop']['total'] > 0):
    foreach ($_from AS $this->_var['merchant_seckill_goods_0_72193700_1564028648']):
        $this->_foreach['buytop']['iteration']++;
?>
                                        <li>
                                            <div class="p-img"><a href="<?php echo $this->_var['merchant_seckill_goods_0_72193700_1564028648']['url']; ?>" target="_blank"><img src="<?php echo $this->_var['merchant_seckill_goods_0_72193700_1564028648']['goods_thumb']; ?>" width="130" height="130"></a></div>
                                            <div class="p-name"><a href="<?php echo $this->_var['merchant_seckill_goods_0_72193700_1564028648']['url']; ?>" target="_blank"><?php echo $this->_var['merchant_seckill_goods_0_72193700_1564028648']['goods_name']; ?></a></div>
                                            <div class="p-price"><?php echo $this->_var['merchant_seckill_goods_0_72193700_1564028648']['act_name']; ?></div>
                                        </li>
                                    <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="g-m-detail">
                	<div class="gm-tabbox" ectype="gm-tabs">
                    	<ul class="gm-tab">
                            <li ectype="gm-tab-item" class="curr"><?php echo $this->_var['lang']['details_order']; ?></li>
                            <li ectype="gm-tab-item"><?php echo $this->_var['lang']['introduce_pic']; ?></li>
                            <li ectype="gm-tab-item"><?php echo $this->_var['lang']['evaluate_user']; ?></li>
                        </ul>
                        <div class="extra"></div>
                        <div class="gm-tab-qp-bort" ectype="qp-bort"></div>
                    </div>
                    <div class="gm-floors" ectype="gm-floors">
                        <div class="gm-floors" ectype="gm-floors">
                    	<div class="gm-f-item gm-f-parameter" ectype="gm-item">
                            <dl class="goods-para">
                                <dd class="column"><span><?php echo $this->_var['lang']['goods_name']; ?>：<?php echo htmlspecialchars($this->_var['goods']['goods_name']); ?></span></dd>
                                <dd class="column"><span><?php echo $this->_var['lang']['Commodity_number']; ?>：<?php echo $this->_var['goods']['goods_sn']; ?></span></dd>
                                <dd class="column"><span><?php echo $this->_var['lang']['seller_store']; ?>：<a href="<?php echo $this->_var['goods']['store_url']; ?>" title="<?php echo $this->_var['goods']['rz_shopName']; ?>" target="_blank"><?php echo $this->_var['goods']['rz_shopName']; ?></a></span></dd>
                            </dl>
							<?php if ($this->_var['properties']): ?>
                        	<dl class="goods-para">
								<?php $_from = $this->_var['properties']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'property_group');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['property_group']):
?>
								<?php $_from = $this->_var['property_group']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'property');if (count($_from)):
    foreach ($_from AS $this->_var['property']):
?>
                                <dd class="column"><span title="<?php echo $this->_var['property']['value']; ?>"><?php echo htmlspecialchars($this->_var['property']['name']); ?>：<?php echo $this->_var['property']['value']; ?></span></dd>
								<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
								<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                            </dl>
							<?php endif; ?>
							<?php if ($this->_var['extend_info']): ?>
							<dl class="goods-para">
								<?php $_from = $this->_var['extend_info']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('key', 'info');if (count($_from)):
    foreach ($_from AS $this->_var['key'] => $this->_var['info']):
?>	
								<dd class="column"><span title="<?php echo htmlspecialchars($this->_var['info']); ?>"><?php echo $this->_var['key']; ?>：<?php echo htmlspecialchars($this->_var['info']); ?></span></dd>
								<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
							</dl>
							<?php endif; ?>
                        </div>
                        <div class="gm-f-item gm-f-details" ectype="gm-item">
                        	<div class="gm-title">
                            	<h3>商品详情</h3>
                            </div>
                            <?php echo $this->_var['goods']['goods_desc']; ?>
                        </div>
                        <div class="gm-f-item gm-f-comment" ectype="gm-item">
                        	<div class="gm-title">
                            	<h3>评论晒单</h3>
								<?php 
$k = array (
  'name' => 'goods_comment_title',
  'goods_id' => $this->_var['goods']['goods_id'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
                            </div>
                            <div class="gm-warp">
                            	<div class="praise-rate-warp">
                                	<div class="rate">
                                    	<strong><?php echo $this->_var['comment_all']['goodReview']; ?></strong>
                                        <span class="rate-span">
                                        	<span class="tit">好评率</span>
                                            <span class="bf">%</span>
                                        </span>
                                    </div>
                                    <div class="actor-new">
                                    	<dl>
											<?php $_from = $this->_var['goods']['impression_list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'tag');if (count($_from)):
    foreach ($_from AS $this->_var['tag']):
?>
											<dd><?php echo $this->_var['tag']['txt']; ?>(<?php echo $this->_var['tag']['num']; ?>)</dd>
											<?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                                        </dl>
                                    </div>
                                </div>
								<div class="com-list-main">
								<?php echo $this->fetch('library/comments.lbi'); ?>
								</div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="clear"></div>
                <?php if ($this->_var['history_goods']): ?>
                <div class="rection">
                	<div class="ftit"><h3><?php echo $this->_var['lang']['guess_love']; ?></h3></div>
                        <ul>
                            <?php $_from = $this->_var['history_goods']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }; $this->push_vars('', 'goods_0_72234500_1564028648');$this->_foreach['his_goods'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['his_goods']['total'] > 0):
    foreach ($_from AS $this->_var['goods_0_72234500_1564028648']):
        $this->_foreach['his_goods']['iteration']++;
?>
                            <?php if ($this->_foreach['his_goods']['iteration'] <= 5): ?>
                            <li>
                                <div class="p-img"><a href="<?php echo $this->_var['goods_0_72234500_1564028648']['url']; ?>" target="_blank"><img src="<?php echo $this->_var['goods_0_72234500_1564028648']['goods_thumb']; ?>" width="232" height="232"></a></div>
                                <div class="p-name"><a href="<?php echo $this->_var['goods_0_72234500_1564028648']['url']; ?>" target="_blank"><?php echo $this->_var['goods_0_72234500_1564028648']['short_name']; ?></a></div>
                                <div class="p-price">
                                    <?php if ($this->_var['releated_goods_data']['promote_price'] != ''): ?>
                                    <?php echo $this->_var['goods_0_72234500_1564028648']['formated_promote_price']; ?>
                                    <?php else: ?>
                                    <?php echo $this->_var['goods_0_72234500_1564028648']['shop_price']; ?>
                                    <?php endif; ?>
                                </div>
                            </li>
                            <?php endif; ?>
                            <?php endforeach; endif; unset($_from); ?><?php $this->pop_vars();; ?>
                        </ul>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
    
    <?php 
$k = array (
  'name' => 'user_menu_position',
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
    
    <?php echo $this->fetch('library/page_footer.lbi'); ?>
   
    <?php echo $this->smarty_insert_scripts(array('files'=>'jquery.SuperSlide.2.1.1.js,jquery.yomi.js,common.js,cart_common.js,warehouse.js,magiczoomplus.js,cart_quick_links.js')); ?>
    
	<script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/dsc-common.js"></script>
    <script type="text/javascript" src="themes/<?php echo $GLOBALS['_CFG']['template']; ?>/js/jquery.purebox.js"></script>
    
    <script type="text/javascript">
	//商品详情悬浮框
	$(".goods-main-layout").jfloor();
	
	//商品相册小图滚动
	$(".spec-list").slide({mainCell:".spec-items ul",effect:"left",trigger:"click",pnLoop:false,autoPage:true,scroll:1,vis:5,prevCell:".spec-prev",nextCell:".spec-next"});
	
	//右侧看了又看上下滚动
	$(".track_warp").slide({mainCell:".track-con ul",effect:"top",pnLoop:false,autoPlay:false,autoPage:true,prevCell:".sprite-up",nextCell:".sprite-down",vis:3});
	
	/*团购倒计时*/
	$(".time").each(function(){
		$(this).yomi();
	});
    </script>
    <script type="text/javascript">
	var goods_id = <?php echo $this->_var['goods']['goods_id']; ?>;
	var goodsId = <?php echo $this->_var['goods']['id']; ?>;
	var isReturn = false;

	/**
	 * 点选可选属性或改变数量时修改商品价格的函数
	 */
	function changePrice()
	{
	   var attr = getSelectedAttributes(document.forms['ECS_FORMBUY']);
	   var qty = document.forms['ECS_FORMBUY'].elements['number'].value;
	   $("input[name='goods_spec']").val(attr);
	
	   Ajax.call('seckill.php', 'act=price&id=' + goodsId + '&number=' + qty + '&warehouse_id=' + <?php echo empty($this->_var['region_id']) ? '0' : $this->_var['region_id']; ?> + '&area_id=' + <?php echo empty($this->_var['area_id']) ? '0' : $this->_var['area_id']; ?>, changePriceResponse, 'GET', 'JSON');
	}
	
	/**
	 * 接收返回的信息
	 */
	function changePriceResponse(res)
	{
	  if (res.err_msg.length > 0)
	  {
		alert(res.err_msg);
	  }
	  else
	  {
		document.forms['ECS_FORMBUY'].elements['number'].value = res.qty;
		//ecmoban模板堂 --zhuo satrt
		if (document.getElementById('goods_attr_num'))
		{
			$("*[ectype='goods_attr_num']").html(res.attr_number);
		  	$("*[ectype='perNumber']").val(res.attr_number); 
		 
			if(res.err_no == 2){
				$('#isHas_warehouse_num').html(json_languages.shiping_prompt);
			}else{
				if (document.getElementById('isHas_warehouse_num')){
				  var isHas;
				  if(res.attr_number > 0){
						  isHas = '<strong>'+json_languages.Have_goods+'</strong>，'+json_languages.Deliver_back_order;
				  }else{
						  isHas = '<strong>'+json_languages.No_goods+'</strong>，'+此商品暂时售完;
				  }

				  $('#isHas_warehouse_num').html(isHas);
				}
			}
		}
	  }
	  //isReturn = true;
	  quantity();
	}

	//等商品价格及库存加载完成 后执行quantity商品数量修改函数 
	/*setTimeout(function(){
		if(isReturn == true){
			quantity();
		}
	},500);*/

    //未登录团购弹出登录框
    $("*[ectype='btn-seckill']").on('click',function(){
		var user_id = Number(<?php echo $this->_var['user_id']; ?>);
		var quantity = Number($("#quantity").val());
		var goods_number = Number( $('#goods_attr_num').html());
		var restrict_amount = $('#restrict_amount').html();
		var err = true;
		var cssType = true;
		if(user_id > 0){
			//限购
			if(restrict_amount > 0){
				var orderG_number = <?php echo $this->_var['orderG_number']; ?> + quantity;
				if(<?php echo $this->_var['orderG_number']; ?> > 0 && <?php echo $this->_var['orderG_number']; ?> >= restrict_amount){
					message = json_languages.Already_buy+'<?php echo $this->_var['orderG_number']; ?>'+json_languages.Already_buy_two;
					quantity = 1;
					err = false;
					cssType = false;
				}else if(<?php echo $this->_var['orderG_number']; ?> > 0 && orderG_number > restrict_amount){
					var buy_num = restrict_amount - <?php echo $this->_var['orderG_number']; ?>;
					message = json_languages.Already_buy+'<?php echo $this->_var['orderG_number']; ?>'+json_languages.Already_buy_three + buy_num + json_languages.jian;
					quantity = buy_num;
					err = false;
				}else if(quantity > restrict_amount){
					  message = json_languages.Purchase_quantity;
					  quantity = restrict_amount;
					  err = false;
				}

				if(err == false){
					pbDialog(message,"",0);
					return false;
				}
			}
			
			if(goods_number == 0 || quantity > goods_number){
				pbDialog(json_languages.Stock_goods_null,"",0);
				return false;
			}else{
				$("form[name='ECS_FORMBUY']").submit();
			}
		}else{
			var back_url = "seckill.php?act=view&id=" + <?php echo $this->_var['goods']['id']; ?>;
			$.notLogin("get_ajax_content.php?act=get_login_dialog",back_url);
			return false;
		}
    });
    </script>
    <?php 
$k = array (
  'name' => 'goods_delivery_area_js',
  'area' => $this->_var['area'],
);
echo $this->_echash . $k['name'] . '|' . serialize($k) . $this->_echash;
?>
</body>
</html>
