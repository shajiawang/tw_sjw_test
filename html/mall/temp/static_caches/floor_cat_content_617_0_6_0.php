<?php
$data = array (
  0 => 
  array (
    'goods_id' => '747',
    'cat_id' => '638',
    'goods_name' => '北京特产 御食园蜜麻花500g 特色小吃休闲年货零食 食品芝麻糕点 2件起送试 吃，多买多送',
    'market_price' => '<em>¥</em>23.76',
    'org_price' => '19.80',
    'promote_price' => '',
    'shop_price' => '<em>¥</em>19.80',
    'is_promote' => '0',
    'promote_start_date' => '0',
    'promote_end_date' => '0',
    'goods_brief' => '',
    'goods_thumb' => 'images/201703/thumb_img/0_thumb_G_1490165975986.jpg',
    'goods_img' => 'images/201703/goods_img/0_G_1490165975602.jpg',
    'short_name' => '北京特产 御食园蜜麻花500g 特色小吃休闲年货零食 食品芝麻糕点 2件起送试 吃，多买多送',
    'url' => 'goods.php?id=747',
  ),
  1 => 
  array (
    'goods_id' => '746',
    'cat_id' => '638',
    'goods_name' => '软香奶萨萨210g休闲零食牛轧糖沙琪玛蔓越莓味 松鼠新风尚 300款零食 低至3折起',
    'market_price' => '<em>¥</em>20.16',
    'org_price' => '16.80',
    'promote_price' => '',
    'shop_price' => '<em>¥</em>16.80',
    'is_promote' => '0',
    'promote_start_date' => '0',
    'promote_end_date' => '0',
    'goods_brief' => '',
    'goods_thumb' => 'images/201703/thumb_img/0_thumb_G_1490165942091.jpg',
    'goods_img' => 'images/201703/goods_img/0_G_1490165942547.jpg',
    'short_name' => '软香奶萨萨210g休闲零食牛轧糖沙琪玛蔓越莓味 松鼠新风尚 300款零食 低至3折起',
    'url' => 'goods.php?id=746',
  ),
  2 => 
  array (
    'goods_id' => '745',
    'cat_id' => '638',
    'goods_name' => '金锣 肉粒多香肠40g*8支/袋 火腿肠 零食 食品',
    'market_price' => '<em>¥</em>13.08',
    'org_price' => '10.90',
    'promote_price' => '',
    'shop_price' => '<em>¥</em>10.90',
    'is_promote' => '0',
    'promote_start_date' => '0',
    'promote_end_date' => '0',
    'goods_brief' => '',
    'goods_thumb' => 'images/201703/thumb_img/0_thumb_G_1490165905743.jpg',
    'goods_img' => 'images/201703/goods_img/0_G_1490165905142.jpg',
    'short_name' => '金锣 肉粒多香肠40g*8支/袋 火腿肠 零食 食品',
    'url' => 'goods.php?id=745',
  ),
  3 => 
  array (
    'goods_id' => '744',
    'cat_id' => '638',
    'goods_name' => '【三只松鼠_蔓越莓曲奇饼干260g】办公室休闲零食早餐美食糕点 松鼠新风尚 300款零食 低至3折起',
    'market_price' => '<em>¥</em>19.20',
    'org_price' => '16.00',
    'promote_price' => '',
    'shop_price' => '<em>¥</em>16.00',
    'is_promote' => '0',
    'promote_start_date' => '0',
    'promote_end_date' => '0',
    'goods_brief' => '',
    'goods_thumb' => 'images/201703/thumb_img/0_thumb_G_1490165799096.jpg',
    'goods_img' => 'images/201703/goods_img/0_G_1490165799287.jpg',
    'short_name' => '【三只松鼠_蔓越莓曲奇饼干260g】办公室休闲零食早餐美食糕点 松鼠新风尚 300款零食 低至3折起',
    'url' => 'goods.php?id=744',
  ),
  4 => 
  array (
    'goods_id' => '743',
    'cat_id' => '638',
    'goods_name' => '江中猴姑饼干20天装960g 酥性零食猴头菇饼干 早餐代餐饼干40包装 【胃要天天养】上午一包，下午一包！',
    'market_price' => '<em>¥</em>139.20',
    'org_price' => '116.00',
    'promote_price' => '',
    'shop_price' => '<em>¥</em>116.00',
    'is_promote' => '0',
    'promote_start_date' => '0',
    'promote_end_date' => '0',
    'goods_brief' => '',
    'goods_thumb' => 'images/201703/thumb_img/0_thumb_G_1490165766277.jpg',
    'goods_img' => 'images/201703/goods_img/0_G_1490165766521.jpg',
    'short_name' => '江中猴姑饼干20天装960g 酥性零食猴头菇饼干 早餐代餐饼干40包装 【胃要天天养】上午一包，下午一包！',
    'url' => 'goods.php?id=743',
  ),
  5 => 
  array (
    'goods_id' => '742',
    'cat_id' => '638',
    'goods_name' => '猪肉猪肉脯210g 休闲食品小吃零食靖江特产猪肉干 松鼠新风尚 300款零食 低至3折起',
    'market_price' => '<em>¥</em>22.67',
    'org_price' => '18.90',
    'promote_price' => '',
    'shop_price' => '<em>¥</em>18.90',
    'is_promote' => '0',
    'promote_start_date' => '0',
    'promote_end_date' => '0',
    'goods_brief' => '',
    'goods_thumb' => 'images/201703/thumb_img/0_thumb_G_1490165722464.jpg',
    'goods_img' => 'images/201703/goods_img/0_G_1490165722476.jpg',
    'short_name' => '猪肉猪肉脯210g 休闲食品小吃零食靖江特产猪肉干 松鼠新风尚 300款零食 低至3折起',
    'url' => 'goods.php?id=742',
  ),
);
?>