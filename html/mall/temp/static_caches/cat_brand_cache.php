<?php
$data = array (
  858 => 
  array (
    'brand_count' => 1,
    'name' => '家用电器',
    'oldname' => '家用电器',
    'url' => 'category.php?id=858',
    'cat_id' => 
    array (
      1105 => 
      array (
        'id' => '1105',
        'name' => '大家电',
        'url' => 'category.php?id=1105',
        'cat_id' => 
        array (
          1106 => 
          array (
            'id' => '1106',
            'name' => '平板电视',
            'url' => 'category.php?id=1106',
            'cat_id' => 
            array (
              1475 => 
              array (
                'id' => '1475',
                'name' => '乐视电视',
                'url' => 'category.php?id=1475',
                'cat_id' => 
                array (
                ),
              ),
            ),
          ),
          1107 => 
          array (
            'id' => '1107',
            'name' => '空调',
            'url' => 'category.php?id=1107',
            'cat_id' => 
            array (
            ),
          ),
          1108 => 
          array (
            'id' => '1108',
            'name' => '冰箱',
            'url' => 'category.php?id=1108',
            'cat_id' => 
            array (
            ),
          ),
          1109 => 
          array (
            'id' => '1109',
            'name' => '洗衣机',
            'url' => 'category.php?id=1109',
            'cat_id' => 
            array (
            ),
          ),
          1110 => 
          array (
            'id' => '1110',
            'name' => '家庭影院',
            'url' => 'category.php?id=1110',
            'cat_id' => 
            array (
            ),
          ),
          1111 => 
          array (
            'id' => '1111',
            'name' => 'DVD',
            'url' => 'category.php?id=1111',
            'cat_id' => 
            array (
            ),
          ),
          1112 => 
          array (
            'id' => '1112',
            'name' => '迷你音响',
            'url' => 'category.php?id=1112',
            'cat_id' => 
            array (
            ),
          ),
          1113 => 
          array (
            'id' => '1113',
            'name' => '热水器',
            'url' => 'category.php?id=1113',
            'cat_id' => 
            array (
            ),
          ),
          1114 => 
          array (
            'id' => '1114',
            'name' => '冷吧/冰柜',
            'url' => 'category.php?id=1114',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1115 => 
      array (
        'id' => '1115',
        'name' => '生活电器',
        'url' => 'category.php?id=1115',
        'cat_id' => 
        array (
          1116 => 
          array (
            'id' => '1116',
            'name' => '电风扇',
            'url' => 'category.php?id=1116',
            'cat_id' => 
            array (
            ),
          ),
          1117 => 
          array (
            'id' => '1117',
            'name' => '冷风扇',
            'url' => 'category.php?id=1117',
            'cat_id' => 
            array (
            ),
          ),
          1118 => 
          array (
            'id' => '1118',
            'name' => '净化器',
            'url' => 'category.php?id=1118',
            'cat_id' => 
            array (
            ),
          ),
          1119 => 
          array (
            'id' => '1119',
            'name' => '加湿器',
            'url' => 'category.php?id=1119',
            'cat_id' => 
            array (
            ),
          ),
          1120 => 
          array (
            'id' => '1120',
            'name' => '扫地机器人',
            'url' => 'category.php?id=1120',
            'cat_id' => 
            array (
            ),
          ),
          1121 => 
          array (
            'id' => '1121',
            'name' => '吸尘器',
            'url' => 'category.php?id=1121',
            'cat_id' => 
            array (
            ),
          ),
          1122 => 
          array (
            'id' => '1122',
            'name' => '插座',
            'url' => 'category.php?id=1122',
            'cat_id' => 
            array (
            ),
          ),
          1123 => 
          array (
            'id' => '1123',
            'name' => '电话机',
            'url' => 'category.php?id=1123',
            'cat_id' => 
            array (
            ),
          ),
          1124 => 
          array (
            'id' => '1124',
            'name' => '饮水机',
            'url' => 'category.php?id=1124',
            'cat_id' => 
            array (
            ),
          ),
          1125 => 
          array (
            'id' => '1125',
            'name' => '取暖电器',
            'url' => 'category.php?id=1125',
            'cat_id' => 
            array (
            ),
          ),
          1126 => 
          array (
            'id' => '1126',
            'name' => '净水设备',
            'url' => 'category.php?id=1126',
            'cat_id' => 
            array (
            ),
          ),
          1127 => 
          array (
            'id' => '1127',
            'name' => '干衣机',
            'url' => 'category.php?id=1127',
            'cat_id' => 
            array (
            ),
          ),
          1128 => 
          array (
            'id' => '1128',
            'name' => '收音机/录音机',
            'url' => 'category.php?id=1128',
            'cat_id' => 
            array (
            ),
          ),
          1175 => 
          array (
            'id' => '1175',
            'name' => '电器开关',
            'url' => 'category.php?id=1175',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1129 => 
      array (
        'id' => '1129',
        'name' => '厨房电器',
        'url' => 'category.php?id=1129',
        'cat_id' => 
        array (
          1131 => 
          array (
            'id' => '1131',
            'name' => '电饭煲',
            'url' => 'category.php?id=1131',
            'cat_id' => 
            array (
            ),
          ),
          1130 => 
          array (
            'id' => '1130',
            'name' => '电压力锅',
            'url' => 'category.php?id=1130',
            'cat_id' => 
            array (
            ),
          ),
          1132 => 
          array (
            'id' => '1132',
            'name' => '豆浆机',
            'url' => 'category.php?id=1132',
            'cat_id' => 
            array (
            ),
          ),
          1133 => 
          array (
            'id' => '1133',
            'name' => '面包机',
            'url' => 'category.php?id=1133',
            'cat_id' => 
            array (
            ),
          ),
          1134 => 
          array (
            'id' => '1134',
            'name' => '咖啡机',
            'url' => 'category.php?id=1134',
            'cat_id' => 
            array (
            ),
          ),
          1135 => 
          array (
            'id' => '1135',
            'name' => '微波炉',
            'url' => 'category.php?id=1135',
            'cat_id' => 
            array (
            ),
          ),
          1136 => 
          array (
            'id' => '1136',
            'name' => '料理/榨汁机',
            'url' => 'category.php?id=1136',
            'cat_id' => 
            array (
            ),
          ),
          1137 => 
          array (
            'id' => '1137',
            'name' => '电烤箱',
            'url' => 'category.php?id=1137',
            'cat_id' => 
            array (
            ),
          ),
          1138 => 
          array (
            'id' => '1138',
            'name' => '电磁炉',
            'url' => 'category.php?id=1138',
            'cat_id' => 
            array (
            ),
          ),
          1139 => 
          array (
            'id' => '1139',
            'name' => '电饼铛/烧火盘',
            'url' => 'category.php?id=1139',
            'cat_id' => 
            array (
            ),
          ),
          1140 => 
          array (
            'id' => '1140',
            'name' => '煮蛋器',
            'url' => 'category.php?id=1140',
            'cat_id' => 
            array (
            ),
          ),
          1141 => 
          array (
            'id' => '1141',
            'name' => '酸奶机',
            'url' => 'category.php?id=1141',
            'cat_id' => 
            array (
            ),
          ),
          1142 => 
          array (
            'id' => '1142',
            'name' => '电水壶/热水瓶',
            'url' => 'category.php?id=1142',
            'cat_id' => 
            array (
            ),
          ),
          1143 => 
          array (
            'id' => '1143',
            'name' => '电饭盒',
            'url' => 'category.php?id=1143',
            'cat_id' => 
            array (
            ),
          ),
          1144 => 
          array (
            'id' => '1144',
            'name' => '其他厨房电器',
            'url' => 'category.php?id=1144',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1145 => 
      array (
        'id' => '1145',
        'name' => '个护健康',
        'url' => 'category.php?id=1145',
        'cat_id' => 
        array (
          1152 => 
          array (
            'id' => '1152',
            'name' => '按摩椅',
            'url' => 'category.php?id=1152',
            'cat_id' => 
            array (
            ),
          ),
          1146 => 
          array (
            'id' => '1146',
            'name' => '剃须刀',
            'url' => 'category.php?id=1146',
            'cat_id' => 
            array (
            ),
          ),
          1147 => 
          array (
            'id' => '1147',
            'name' => '脱毛器',
            'url' => 'category.php?id=1147',
            'cat_id' => 
            array (
            ),
          ),
          1148 => 
          array (
            'id' => '1148',
            'name' => '口腔护理',
            'url' => 'category.php?id=1148',
            'cat_id' => 
            array (
            ),
          ),
          1149 => 
          array (
            'id' => '1149',
            'name' => '电吹风',
            'url' => 'category.php?id=1149',
            'cat_id' => 
            array (
            ),
          ),
          1150 => 
          array (
            'id' => '1150',
            'name' => '美容器',
            'url' => 'category.php?id=1150',
            'cat_id' => 
            array (
            ),
          ),
          1151 => 
          array (
            'id' => '1151',
            'name' => '理发器',
            'url' => 'category.php?id=1151',
            'cat_id' => 
            array (
            ),
          ),
          1153 => 
          array (
            'id' => '1153',
            'name' => '按摩器',
            'url' => 'category.php?id=1153',
            'cat_id' => 
            array (
            ),
          ),
          1154 => 
          array (
            'id' => '1154',
            'name' => '足浴盆',
            'url' => 'category.php?id=1154',
            'cat_id' => 
            array (
            ),
          ),
          1155 => 
          array (
            'id' => '1155',
            'name' => '血压计',
            'url' => 'category.php?id=1155',
            'cat_id' => 
            array (
            ),
          ),
          1156 => 
          array (
            'id' => '1156',
            'name' => '健康秤/厨房秤',
            'url' => 'category.php?id=1156',
            'cat_id' => 
            array (
            ),
          ),
          1157 => 
          array (
            'id' => '1157',
            'name' => '血糖计',
            'url' => 'category.php?id=1157',
            'cat_id' => 
            array (
            ),
          ),
          1158 => 
          array (
            'id' => '1158',
            'name' => '计步器',
            'url' => 'category.php?id=1158',
            'cat_id' => 
            array (
            ),
          ),
          1159 => 
          array (
            'id' => '1159',
            'name' => '其他健康电器',
            'url' => 'category.php?id=1159',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1160 => 
      array (
        'id' => '1160',
        'name' => '五金家装',
        'url' => 'category.php?id=1160',
        'cat_id' => 
        array (
          1161 => 
          array (
            'id' => '1161',
            'name' => '电动工具',
            'url' => 'category.php?id=1161',
            'cat_id' => 
            array (
            ),
          ),
          1162 => 
          array (
            'id' => '1162',
            'name' => '手动工具',
            'url' => 'category.php?id=1162',
            'cat_id' => 
            array (
            ),
          ),
          1163 => 
          array (
            'id' => '1163',
            'name' => '仪器',
            'url' => 'category.php?id=1163',
            'cat_id' => 
            array (
            ),
          ),
          1164 => 
          array (
            'id' => '1164',
            'name' => '仪表',
            'url' => 'category.php?id=1164',
            'cat_id' => 
            array (
            ),
          ),
          1165 => 
          array (
            'id' => '1165',
            'name' => '浴霸/排气扇',
            'url' => 'category.php?id=1165',
            'cat_id' => 
            array (
            ),
          ),
          1166 => 
          array (
            'id' => '1166',
            'name' => '灯具',
            'url' => 'category.php?id=1166',
            'cat_id' => 
            array (
            ),
          ),
          1167 => 
          array (
            'id' => '1167',
            'name' => 'LED灯',
            'url' => 'category.php?id=1167',
            'cat_id' => 
            array (
            ),
          ),
          1168 => 
          array (
            'id' => '1168',
            'name' => '洁身器',
            'url' => 'category.php?id=1168',
            'cat_id' => 
            array (
            ),
          ),
          1169 => 
          array (
            'id' => '1169',
            'name' => '水槽',
            'url' => 'category.php?id=1169',
            'cat_id' => 
            array (
            ),
          ),
          1170 => 
          array (
            'id' => '1170',
            'name' => '龙头',
            'url' => 'category.php?id=1170',
            'cat_id' => 
            array (
            ),
          ),
          1171 => 
          array (
            'id' => '1171',
            'name' => '沐浴花洒',
            'url' => 'category.php?id=1171',
            'cat_id' => 
            array (
            ),
          ),
          1172 => 
          array (
            'id' => '1172',
            'name' => '厨卫五金',
            'url' => 'category.php?id=1172',
            'cat_id' => 
            array (
            ),
          ),
          1173 => 
          array (
            'id' => '1173',
            'name' => '家具五金',
            'url' => 'category.php?id=1173',
            'cat_id' => 
            array (
            ),
          ),
          1174 => 
          array (
            'id' => '1174',
            'name' => '门铃',
            'url' => 'category.php?id=1174',
            'cat_id' => 
            array (
            ),
          ),
          1176 => 
          array (
            'id' => '1176',
            'name' => '监控安防',
            'url' => 'category.php?id=1176',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  3 => 
  array (
    'brand_count' => 1,
    'name' => '<a style="color:#333333;font-weight:bold;" href="category.php?id=33" target="_blank">手机</a>、<a style="color:#333333;font-weight:bold;" href="category.php?id=64" target="_blank">数码</a>、<a style="color:#333333;font-weight:bold;" href="category.php?id=37" target="_blank">通信</a>',
    'category_link' => 1,
    'oldname' => '手机、数码、通信',
    'url' => 'category.php?id=3',
    'cat_id' => 
    array (
      112 => 
      array (
        'id' => '112',
        'name' => '智能设备',
        'url' => 'category.php?id=112',
        'cat_id' => 
        array (
          113 => 
          array (
            'id' => '113',
            'name' => '智能手环',
            'url' => 'category.php?id=113',
            'cat_id' => 
            array (
            ),
          ),
          114 => 
          array (
            'id' => '114',
            'name' => '智能手表',
            'url' => 'category.php?id=114',
            'cat_id' => 
            array (
            ),
          ),
          115 => 
          array (
            'id' => '115',
            'name' => '智能眼镜',
            'url' => 'category.php?id=115',
            'cat_id' => 
            array (
            ),
          ),
          116 => 
          array (
            'id' => '116',
            'name' => '运动跟踪器',
            'url' => 'category.php?id=116',
            'cat_id' => 
            array (
            ),
          ),
          117 => 
          array (
            'id' => '117',
            'name' => '健康监测',
            'url' => 'category.php?id=117',
            'cat_id' => 
            array (
            ),
          ),
          119 => 
          array (
            'id' => '119',
            'name' => '智能配饰',
            'url' => 'category.php?id=119',
            'cat_id' => 
            array (
            ),
          ),
          120 => 
          array (
            'id' => '120',
            'name' => '智能家居',
            'url' => 'category.php?id=120',
            'cat_id' => 
            array (
            ),
          ),
          122 => 
          array (
            'id' => '122',
            'name' => '体感车',
            'url' => 'category.php?id=122',
            'cat_id' => 
            array (
            ),
          ),
          124 => 
          array (
            'id' => '124',
            'name' => '其他配件',
            'url' => 'category.php?id=124',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      76 => 
      array (
        'id' => '76',
        'name' => '数码配件',
        'url' => 'category.php?id=76',
        'cat_id' => 
        array (
          81 => 
          array (
            'id' => '81',
            'name' => '存储卡',
            'url' => 'category.php?id=81',
            'cat_id' => 
            array (
            ),
          ),
          82 => 
          array (
            'id' => '82',
            'name' => '读卡器',
            'url' => 'category.php?id=82',
            'cat_id' => 
            array (
            ),
          ),
          84 => 
          array (
            'id' => '84',
            'name' => '滤镜',
            'url' => 'category.php?id=84',
            'cat_id' => 
            array (
            ),
          ),
          85 => 
          array (
            'id' => '85',
            'name' => '闪光灯/手柄',
            'url' => 'category.php?id=85',
            'cat_id' => 
            array (
            ),
          ),
          87 => 
          array (
            'id' => '87',
            'name' => '相机包',
            'url' => 'category.php?id=87',
            'cat_id' => 
            array (
            ),
          ),
          88 => 
          array (
            'id' => '88',
            'name' => '三脚架/云台',
            'url' => 'category.php?id=88',
            'cat_id' => 
            array (
            ),
          ),
          90 => 
          array (
            'id' => '90',
            'name' => '相机清洁',
            'url' => 'category.php?id=90',
            'cat_id' => 
            array (
            ),
          ),
          91 => 
          array (
            'id' => '91',
            'name' => '相机贴膜',
            'url' => 'category.php?id=91',
            'cat_id' => 
            array (
            ),
          ),
          93 => 
          array (
            'id' => '93',
            'name' => '机身附件',
            'url' => 'category.php?id=93',
            'cat_id' => 
            array (
            ),
          ),
          95 => 
          array (
            'id' => '95',
            'name' => '镜头附件',
            'url' => 'category.php?id=95',
            'cat_id' => 
            array (
            ),
          ),
          96 => 
          array (
            'id' => '96',
            'name' => '电池/充电器',
            'url' => 'category.php?id=96',
            'cat_id' => 
            array (
            ),
          ),
          97 => 
          array (
            'id' => '97',
            'name' => '移动电源',
            'url' => 'category.php?id=97',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      33 => 
      array (
        'id' => '33',
        'name' => '手机通讯',
        'url' => 'category.php?id=33',
        'cat_id' => 
        array (
          34 => 
          array (
            'id' => '34',
            'name' => '手机',
            'url' => 'category.php?id=34',
            'cat_id' => 
            array (
            ),
          ),
          35 => 
          array (
            'id' => '35',
            'name' => '对讲机',
            'url' => 'category.php?id=35',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      42 => 
      array (
        'id' => '42',
        'name' => '运营商',
        'url' => 'category.php?id=42',
        'cat_id' => 
        array (
          43 => 
          array (
            'id' => '43',
            'name' => '购机送费',
            'url' => 'category.php?id=43',
            'cat_id' => 
            array (
            ),
          ),
          44 => 
          array (
            'id' => '44',
            'name' => '0元购机',
            'url' => 'category.php?id=44',
            'cat_id' => 
            array (
            ),
          ),
          46 => 
          array (
            'id' => '46',
            'name' => '选号入网',
            'url' => 'category.php?id=46',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      47 => 
      array (
        'id' => '47',
        'name' => '手机配件',
        'url' => 'category.php?id=47',
        'cat_id' => 
        array (
          49 => 
          array (
            'id' => '49',
            'name' => '电池',
            'url' => 'category.php?id=49',
            'cat_id' => 
            array (
            ),
          ),
          51 => 
          array (
            'id' => '51',
            'name' => '蓝牙耳机',
            'url' => 'category.php?id=51',
            'cat_id' => 
            array (
            ),
          ),
          52 => 
          array (
            'id' => '52',
            'name' => '充电器/数据线',
            'url' => 'category.php?id=52',
            'cat_id' => 
            array (
            ),
          ),
          53 => 
          array (
            'id' => '53',
            'name' => '手机耳机',
            'url' => 'category.php?id=53',
            'cat_id' => 
            array (
            ),
          ),
          54 => 
          array (
            'id' => '54',
            'name' => '贴膜',
            'url' => 'category.php?id=54',
            'cat_id' => 
            array (
            ),
          ),
          55 => 
          array (
            'id' => '55',
            'name' => '存储卡',
            'url' => 'category.php?id=55',
            'cat_id' => 
            array (
            ),
          ),
          56 => 
          array (
            'id' => '56',
            'name' => '保护套',
            'url' => 'category.php?id=56',
            'cat_id' => 
            array (
            ),
          ),
          57 => 
          array (
            'id' => '57',
            'name' => '车载',
            'url' => 'category.php?id=57',
            'cat_id' => 
            array (
            ),
          ),
          59 => 
          array (
            'id' => '59',
            'name' => 'iPhone配件',
            'url' => 'category.php?id=59',
            'cat_id' => 
            array (
            ),
          ),
          60 => 
          array (
            'id' => '60',
            'name' => '创意配件',
            'url' => 'category.php?id=60',
            'cat_id' => 
            array (
            ),
          ),
          61 => 
          array (
            'id' => '61',
            'name' => '便携/无线音箱',
            'url' => 'category.php?id=61',
            'cat_id' => 
            array (
            ),
          ),
          62 => 
          array (
            'id' => '62',
            'name' => '手机饰品',
            'url' => 'category.php?id=62',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      64 => 
      array (
        'id' => '64',
        'name' => '摄影摄像',
        'url' => 'category.php?id=64',
        'cat_id' => 
        array (
          66 => 
          array (
            'id' => '66',
            'name' => '数码相机',
            'url' => 'category.php?id=66',
            'cat_id' => 
            array (
            ),
          ),
          68 => 
          array (
            'id' => '68',
            'name' => '单电/微单相机',
            'url' => 'category.php?id=68',
            'cat_id' => 
            array (
            ),
          ),
          69 => 
          array (
            'id' => '69',
            'name' => '单反相机',
            'url' => 'category.php?id=69',
            'cat_id' => 
            array (
            ),
          ),
          70 => 
          array (
            'id' => '70',
            'name' => '拍立得',
            'url' => 'category.php?id=70',
            'cat_id' => 
            array (
            ),
          ),
          71 => 
          array (
            'id' => '71',
            'name' => '运动相机',
            'url' => 'category.php?id=71',
            'cat_id' => 
            array (
            ),
          ),
          72 => 
          array (
            'id' => '72',
            'name' => '摄像机',
            'url' => 'category.php?id=72',
            'cat_id' => 
            array (
            ),
          ),
          73 => 
          array (
            'id' => '73',
            'name' => '镜头',
            'url' => 'category.php?id=73',
            'cat_id' => 
            array (
            ),
          ),
          74 => 
          array (
            'id' => '74',
            'name' => '户外器材',
            'url' => 'category.php?id=74',
            'cat_id' => 
            array (
            ),
          ),
          75 => 
          array (
            'id' => '75',
            'name' => '摄影器材',
            'url' => 'category.php?id=75',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      99 => 
      array (
        'id' => '99',
        'name' => '时尚影音',
        'url' => 'category.php?id=99',
        'cat_id' => 
        array (
          101 => 
          array (
            'id' => '101',
            'name' => '耳机/耳麦',
            'url' => 'category.php?id=101',
            'cat_id' => 
            array (
            ),
          ),
          102 => 
          array (
            'id' => '102',
            'name' => '音箱/音响',
            'url' => 'category.php?id=102',
            'cat_id' => 
            array (
            ),
          ),
          103 => 
          array (
            'id' => '103',
            'name' => '麦克风',
            'url' => 'category.php?id=103',
            'cat_id' => 
            array (
            ),
          ),
          105 => 
          array (
            'id' => '105',
            'name' => 'MP3/MP4',
            'url' => 'category.php?id=105',
            'cat_id' => 
            array (
            ),
          ),
          106 => 
          array (
            'id' => '106',
            'name' => '数码相框',
            'url' => 'category.php?id=106',
            'cat_id' => 
            array (
            ),
          ),
          108 => 
          array (
            'id' => '108',
            'name' => '专业音频',
            'url' => 'category.php?id=108',
            'cat_id' => 
            array (
            ),
          ),
          111 => 
          array (
            'id' => '111',
            'name' => '苹果周边',
            'url' => 'category.php?id=111',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  4 => 
  array (
    'brand_count' => 1,
    'name' => '电脑、办公',
    'oldname' => '电脑、办公',
    'url' => 'category.php?id=4',
    'cat_id' => 
    array (
      158 => 
      array (
        'id' => '158',
        'name' => '服务产品',
        'url' => 'category.php?id=158',
        'cat_id' => 
        array (
          345 => 
          array (
            'id' => '345',
            'name' => '上门服务',
            'url' => 'category.php?id=345',
            'cat_id' => 
            array (
            ),
          ),
          346 => 
          array (
            'id' => '346',
            'name' => '远程服务',
            'url' => 'category.php?id=346',
            'cat_id' => 
            array (
            ),
          ),
          348 => 
          array (
            'id' => '348',
            'name' => '电脑软件',
            'url' => 'category.php?id=348',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      132 => 
      array (
        'id' => '132',
        'name' => '电脑整机',
        'url' => 'category.php?id=132',
        'cat_id' => 
        array (
          168 => 
          array (
            'id' => '168',
            'name' => '笔记本',
            'url' => 'category.php?id=168',
            'cat_id' => 
            array (
            ),
          ),
          171 => 
          array (
            'id' => '171',
            'name' => '超级本',
            'url' => 'category.php?id=171',
            'cat_id' => 
            array (
            ),
          ),
          174 => 
          array (
            'id' => '174',
            'name' => '游戏本',
            'url' => 'category.php?id=174',
            'cat_id' => 
            array (
            ),
          ),
          178 => 
          array (
            'id' => '178',
            'name' => '平板电脑',
            'url' => 'category.php?id=178',
            'cat_id' => 
            array (
            ),
          ),
          181 => 
          array (
            'id' => '181',
            'name' => '平板电脑配件',
            'url' => 'category.php?id=181',
            'cat_id' => 
            array (
            ),
          ),
          186 => 
          array (
            'id' => '186',
            'name' => '台式机',
            'url' => 'category.php?id=186',
            'cat_id' => 
            array (
            ),
          ),
          203 => 
          array (
            'id' => '203',
            'name' => '笔记本配件',
            'url' => 'category.php?id=203',
            'cat_id' => 
            array (
            ),
          ),
          204 => 
          array (
            'id' => '204',
            'name' => '服务器/工作站',
            'url' => 'category.php?id=204',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      141 => 
      array (
        'id' => '141',
        'name' => '电脑配件',
        'url' => 'category.php?id=141',
        'cat_id' => 
        array (
          208 => 
          array (
            'id' => '208',
            'name' => 'CPU',
            'url' => 'category.php?id=208',
            'cat_id' => 
            array (
            ),
          ),
          213 => 
          array (
            'id' => '213',
            'name' => '主板',
            'url' => 'category.php?id=213',
            'cat_id' => 
            array (
            ),
          ),
          215 => 
          array (
            'id' => '215',
            'name' => '显卡',
            'url' => 'category.php?id=215',
            'cat_id' => 
            array (
            ),
          ),
          216 => 
          array (
            'id' => '216',
            'name' => '硬盘',
            'url' => 'category.php?id=216',
            'cat_id' => 
            array (
            ),
          ),
          218 => 
          array (
            'id' => '218',
            'name' => 'SSD固态硬盘',
            'url' => 'category.php?id=218',
            'cat_id' => 
            array (
            ),
          ),
          220 => 
          array (
            'id' => '220',
            'name' => '内存',
            'url' => 'category.php?id=220',
            'cat_id' => 
            array (
            ),
          ),
          222 => 
          array (
            'id' => '222',
            'name' => '机箱',
            'url' => 'category.php?id=222',
            'cat_id' => 
            array (
            ),
          ),
          224 => 
          array (
            'id' => '224',
            'name' => '电源',
            'url' => 'category.php?id=224',
            'cat_id' => 
            array (
            ),
          ),
          225 => 
          array (
            'id' => '225',
            'name' => '显示器',
            'url' => 'category.php?id=225',
            'cat_id' => 
            array (
            ),
          ),
          228 => 
          array (
            'id' => '228',
            'name' => '刻录机/光驱',
            'url' => 'category.php?id=228',
            'cat_id' => 
            array (
            ),
          ),
          234 => 
          array (
            'id' => '234',
            'name' => '声卡、扩展卡',
            'url' => 'category.php?id=234',
            'cat_id' => 
            array (
            ),
          ),
          236 => 
          array (
            'id' => '236',
            'name' => '散热器',
            'url' => 'category.php?id=236',
            'cat_id' => 
            array (
            ),
          ),
          239 => 
          array (
            'id' => '239',
            'name' => '装机配件',
            'url' => 'category.php?id=239',
            'cat_id' => 
            array (
            ),
          ),
          241 => 
          array (
            'id' => '241',
            'name' => '组装电脑',
            'url' => 'category.php?id=241',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      144 => 
      array (
        'id' => '144',
        'name' => '外设产品',
        'url' => 'category.php?id=144',
        'cat_id' => 
        array (
          245 => 
          array (
            'id' => '245',
            'name' => '鼠标',
            'url' => 'category.php?id=245',
            'cat_id' => 
            array (
            ),
          ),
          247 => 
          array (
            'id' => '247',
            'name' => '键盘',
            'url' => 'category.php?id=247',
            'cat_id' => 
            array (
            ),
          ),
          249 => 
          array (
            'id' => '249',
            'name' => '游戏设备',
            'url' => 'category.php?id=249',
            'cat_id' => 
            array (
            ),
          ),
          251 => 
          array (
            'id' => '251',
            'name' => 'U盘',
            'url' => 'category.php?id=251',
            'cat_id' => 
            array (
            ),
          ),
          253 => 
          array (
            'id' => '253',
            'name' => '移动硬盘',
            'url' => 'category.php?id=253',
            'cat_id' => 
            array (
            ),
          ),
          254 => 
          array (
            'id' => '254',
            'name' => '鼠标垫',
            'url' => 'category.php?id=254',
            'cat_id' => 
            array (
            ),
          ),
          256 => 
          array (
            'id' => '256',
            'name' => '摄像头',
            'url' => 'category.php?id=256',
            'cat_id' => 
            array (
            ),
          ),
          258 => 
          array (
            'id' => '258',
            'name' => '线缆',
            'url' => 'category.php?id=258',
            'cat_id' => 
            array (
            ),
          ),
          260 => 
          array (
            'id' => '260',
            'name' => '电玩',
            'url' => 'category.php?id=260',
            'cat_id' => 
            array (
            ),
          ),
          262 => 
          array (
            'id' => '262',
            'name' => '手写板',
            'url' => 'category.php?id=262',
            'cat_id' => 
            array (
            ),
          ),
          265 => 
          array (
            'id' => '265',
            'name' => '外置盒',
            'url' => 'category.php?id=265',
            'cat_id' => 
            array (
            ),
          ),
          267 => 
          array (
            'id' => '267',
            'name' => '电脑工具',
            'url' => 'category.php?id=267',
            'cat_id' => 
            array (
            ),
          ),
          268 => 
          array (
            'id' => '268',
            'name' => '电脑清洁',
            'url' => 'category.php?id=268',
            'cat_id' => 
            array (
            ),
          ),
          269 => 
          array (
            'id' => '269',
            'name' => 'UPS',
            'url' => 'category.php?id=269',
            'cat_id' => 
            array (
            ),
          ),
          270 => 
          array (
            'id' => '270',
            'name' => '插座',
            'url' => 'category.php?id=270',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      148 => 
      array (
        'id' => '148',
        'name' => '网络产品',
        'url' => 'category.php?id=148',
        'cat_id' => 
        array (
          272 => 
          array (
            'id' => '272',
            'name' => '路由器',
            'url' => 'category.php?id=272',
            'cat_id' => 
            array (
            ),
          ),
          274 => 
          array (
            'id' => '274',
            'name' => '网卡',
            'url' => 'category.php?id=274',
            'cat_id' => 
            array (
            ),
          ),
          275 => 
          array (
            'id' => '275',
            'name' => '交换机',
            'url' => 'category.php?id=275',
            'cat_id' => 
            array (
            ),
          ),
          278 => 
          array (
            'id' => '278',
            'name' => '网络存储',
            'url' => 'category.php?id=278',
            'cat_id' => 
            array (
            ),
          ),
          281 => 
          array (
            'id' => '281',
            'name' => '4G/3G上网',
            'url' => 'category.php?id=281',
            'cat_id' => 
            array (
            ),
          ),
          282 => 
          array (
            'id' => '282',
            'name' => '网络盒子',
            'url' => 'category.php?id=282',
            'cat_id' => 
            array (
            ),
          ),
          284 => 
          array (
            'id' => '284',
            'name' => '网络配件',
            'url' => 'category.php?id=284',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      157 => 
      array (
        'id' => '157',
        'name' => '办公打印',
        'url' => 'category.php?id=157',
        'cat_id' => 
        array (
          291 => 
          array (
            'id' => '291',
            'name' => '打印机',
            'url' => 'category.php?id=291',
            'cat_id' => 
            array (
            ),
          ),
          292 => 
          array (
            'id' => '292',
            'name' => '一体机',
            'url' => 'category.php?id=292',
            'cat_id' => 
            array (
            ),
          ),
          294 => 
          array (
            'id' => '294',
            'name' => '投影机',
            'url' => 'category.php?id=294',
            'cat_id' => 
            array (
            ),
          ),
          296 => 
          array (
            'id' => '296',
            'name' => '投影配件',
            'url' => 'category.php?id=296',
            'cat_id' => 
            array (
            ),
          ),
          298 => 
          array (
            'id' => '298',
            'name' => '传真机',
            'url' => 'category.php?id=298',
            'cat_id' => 
            array (
            ),
          ),
          299 => 
          array (
            'id' => '299',
            'name' => '复合机',
            'url' => 'category.php?id=299',
            'cat_id' => 
            array (
            ),
          ),
          300 => 
          array (
            'id' => '300',
            'name' => '碎纸机',
            'url' => 'category.php?id=300',
            'cat_id' => 
            array (
            ),
          ),
          301 => 
          array (
            'id' => '301',
            'name' => '扫描仪',
            'url' => 'category.php?id=301',
            'cat_id' => 
            array (
            ),
          ),
          302 => 
          array (
            'id' => '302',
            'name' => '墨盒',
            'url' => 'category.php?id=302',
            'cat_id' => 
            array (
            ),
          ),
          307 => 
          array (
            'id' => '307',
            'name' => '硒鼓',
            'url' => 'category.php?id=307',
            'cat_id' => 
            array (
            ),
          ),
          309 => 
          array (
            'id' => '309',
            'name' => '墨粉',
            'url' => 'category.php?id=309',
            'cat_id' => 
            array (
            ),
          ),
          310 => 
          array (
            'id' => '310',
            'name' => '色带',
            'url' => 'category.php?id=310',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  5 => 
  array (
    'brand_count' => 1,
    'name' => '家居、家具、家装、厨具',
    'oldname' => '家居、家具、家装、厨具',
    'url' => 'category.php?id=5',
    'cat_id' => 
    array (
      143 => 
      array (
        'id' => '143',
        'name' => '厨具',
        'url' => 'category.php?id=143',
        'cat_id' => 
        array (
          146 => 
          array (
            'id' => '146',
            'name' => '烹饪锅具',
            'url' => 'category.php?id=146',
            'cat_id' => 
            array (
            ),
          ),
          149 => 
          array (
            'id' => '149',
            'name' => '刀剪菜板',
            'url' => 'category.php?id=149',
            'cat_id' => 
            array (
            ),
          ),
          152 => 
          array (
            'id' => '152',
            'name' => '厨房配件',
            'url' => 'category.php?id=152',
            'cat_id' => 
            array (
            ),
          ),
          153 => 
          array (
            'id' => '153',
            'name' => '水具酒具',
            'url' => 'category.php?id=153',
            'cat_id' => 
            array (
            ),
          ),
          154 => 
          array (
            'id' => '154',
            'name' => '餐具',
            'url' => 'category.php?id=154',
            'cat_id' => 
            array (
            ),
          ),
          156 => 
          array (
            'id' => '156',
            'name' => '茶具/咖啡具',
            'url' => 'category.php?id=156',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      159 => 
      array (
        'id' => '159',
        'name' => '家装建材',
        'url' => 'category.php?id=159',
        'cat_id' => 
        array (
          160 => 
          array (
            'id' => '160',
            'name' => '灯饰照明',
            'url' => 'category.php?id=160',
            'cat_id' => 
            array (
            ),
          ),
          161 => 
          array (
            'id' => '161',
            'name' => '厨房卫浴',
            'url' => 'category.php?id=161',
            'cat_id' => 
            array (
            ),
          ),
          163 => 
          array (
            'id' => '163',
            'name' => '五金工具',
            'url' => 'category.php?id=163',
            'cat_id' => 
            array (
            ),
          ),
          165 => 
          array (
            'id' => '165',
            'name' => '电工电料',
            'url' => 'category.php?id=165',
            'cat_id' => 
            array (
            ),
          ),
          167 => 
          array (
            'id' => '167',
            'name' => '墙地面材料',
            'url' => 'category.php?id=167',
            'cat_id' => 
            array (
            ),
          ),
          169 => 
          array (
            'id' => '169',
            'name' => '装饰材料',
            'url' => 'category.php?id=169',
            'cat_id' => 
            array (
            ),
          ),
          172 => 
          array (
            'id' => '172',
            'name' => '装修服务',
            'url' => 'category.php?id=172',
            'cat_id' => 
            array (
            ),
          ),
          175 => 
          array (
            'id' => '175',
            'name' => '吸顶灯',
            'url' => 'category.php?id=175',
            'cat_id' => 
            array (
            ),
          ),
          176 => 
          array (
            'id' => '176',
            'name' => '淋浴花洒',
            'url' => 'category.php?id=176',
            'cat_id' => 
            array (
            ),
          ),
          179 => 
          array (
            'id' => '179',
            'name' => '开关插座',
            'url' => 'category.php?id=179',
            'cat_id' => 
            array (
            ),
          ),
          182 => 
          array (
            'id' => '182',
            'name' => '油漆涂料',
            'url' => 'category.php?id=182',
            'cat_id' => 
            array (
            ),
          ),
          184 => 
          array (
            'id' => '184',
            'name' => '龙头',
            'url' => 'category.php?id=184',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      188 => 
      array (
        'id' => '188',
        'name' => '家纺',
        'url' => 'category.php?id=188',
        'cat_id' => 
        array (
          190 => 
          array (
            'id' => '190',
            'name' => '床品套件',
            'url' => 'category.php?id=190',
            'cat_id' => 
            array (
            ),
          ),
          191 => 
          array (
            'id' => '191',
            'name' => '被子',
            'url' => 'category.php?id=191',
            'cat_id' => 
            array (
            ),
          ),
          192 => 
          array (
            'id' => '192',
            'name' => '蚊帐',
            'url' => 'category.php?id=192',
            'cat_id' => 
            array (
            ),
          ),
          193 => 
          array (
            'id' => '193',
            'name' => '凉席',
            'url' => 'category.php?id=193',
            'cat_id' => 
            array (
            ),
          ),
          194 => 
          array (
            'id' => '194',
            'name' => '床单被罩',
            'url' => 'category.php?id=194',
            'cat_id' => 
            array (
            ),
          ),
          195 => 
          array (
            'id' => '195',
            'name' => '枕芯',
            'url' => 'category.php?id=195',
            'cat_id' => 
            array (
            ),
          ),
          196 => 
          array (
            'id' => '196',
            'name' => '毛巾浴巾',
            'url' => 'category.php?id=196',
            'cat_id' => 
            array (
            ),
          ),
          197 => 
          array (
            'id' => '197',
            'name' => '布艺软饰',
            'url' => 'category.php?id=197',
            'cat_id' => 
            array (
            ),
          ),
          198 => 
          array (
            'id' => '198',
            'name' => '毯子',
            'url' => 'category.php?id=198',
            'cat_id' => 
            array (
            ),
          ),
          199 => 
          array (
            'id' => '199',
            'name' => '抱枕靠垫',
            'url' => 'category.php?id=199',
            'cat_id' => 
            array (
            ),
          ),
          200 => 
          array (
            'id' => '200',
            'name' => '床垫/床褥',
            'url' => 'category.php?id=200',
            'cat_id' => 
            array (
            ),
          ),
          201 => 
          array (
            'id' => '201',
            'name' => '窗帘/窗纱',
            'url' => 'category.php?id=201',
            'cat_id' => 
            array (
            ),
          ),
          202 => 
          array (
            'id' => '202',
            'name' => '电热毯',
            'url' => 'category.php?id=202',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      205 => 
      array (
        'id' => '205',
        'name' => '家具',
        'url' => 'category.php?id=205',
        'cat_id' => 
        array (
          206 => 
          array (
            'id' => '206',
            'name' => '卧室家具',
            'url' => 'category.php?id=206',
            'cat_id' => 
            array (
            ),
          ),
          207 => 
          array (
            'id' => '207',
            'name' => '客厅家具',
            'url' => 'category.php?id=207',
            'cat_id' => 
            array (
            ),
          ),
          209 => 
          array (
            'id' => '209',
            'name' => '餐厅家具',
            'url' => 'category.php?id=209',
            'cat_id' => 
            array (
            ),
          ),
          210 => 
          array (
            'id' => '210',
            'name' => '书房家具',
            'url' => 'category.php?id=210',
            'cat_id' => 
            array (
            ),
          ),
          211 => 
          array (
            'id' => '211',
            'name' => '储物家具',
            'url' => 'category.php?id=211',
            'cat_id' => 
            array (
            ),
          ),
          212 => 
          array (
            'id' => '212',
            'name' => '阳台/户外',
            'url' => 'category.php?id=212',
            'cat_id' => 
            array (
            ),
          ),
          214 => 
          array (
            'id' => '214',
            'name' => '商业办公',
            'url' => 'category.php?id=214',
            'cat_id' => 
            array (
            ),
          ),
          217 => 
          array (
            'id' => '217',
            'name' => '床',
            'url' => 'category.php?id=217',
            'cat_id' => 
            array (
            ),
          ),
          219 => 
          array (
            'id' => '219',
            'name' => '床垫',
            'url' => 'category.php?id=219',
            'cat_id' => 
            array (
            ),
          ),
          221 => 
          array (
            'id' => '221',
            'name' => '沙发',
            'url' => 'category.php?id=221',
            'cat_id' => 
            array (
            ),
          ),
          223 => 
          array (
            'id' => '223',
            'name' => '电脑椅',
            'url' => 'category.php?id=223',
            'cat_id' => 
            array (
            ),
          ),
          226 => 
          array (
            'id' => '226',
            'name' => '衣柜',
            'url' => 'category.php?id=226',
            'cat_id' => 
            array (
            ),
          ),
          227 => 
          array (
            'id' => '227',
            'name' => '茶几',
            'url' => 'category.php?id=227',
            'cat_id' => 
            array (
            ),
          ),
          229 => 
          array (
            'id' => '229',
            'name' => '电视柜',
            'url' => 'category.php?id=229',
            'cat_id' => 
            array (
            ),
          ),
          230 => 
          array (
            'id' => '230',
            'name' => '餐桌',
            'url' => 'category.php?id=230',
            'cat_id' => 
            array (
            ),
          ),
          231 => 
          array (
            'id' => '231',
            'name' => '电脑桌',
            'url' => 'category.php?id=231',
            'cat_id' => 
            array (
            ),
          ),
          232 => 
          array (
            'id' => '232',
            'name' => '鞋架/衣帽架',
            'url' => 'category.php?id=232',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      233 => 
      array (
        'id' => '233',
        'name' => '灯具',
        'url' => 'category.php?id=233',
        'cat_id' => 
        array (
          235 => 
          array (
            'id' => '235',
            'name' => '台灯',
            'url' => 'category.php?id=235',
            'cat_id' => 
            array (
            ),
          ),
          237 => 
          array (
            'id' => '237',
            'name' => '吸顶灯',
            'url' => 'category.php?id=237',
            'cat_id' => 
            array (
            ),
          ),
          238 => 
          array (
            'id' => '238',
            'name' => '筒灯射灯',
            'url' => 'category.php?id=238',
            'cat_id' => 
            array (
            ),
          ),
          240 => 
          array (
            'id' => '240',
            'name' => 'LED灯',
            'url' => 'category.php?id=240',
            'cat_id' => 
            array (
            ),
          ),
          242 => 
          array (
            'id' => '242',
            'name' => '节能灯',
            'url' => 'category.php?id=242',
            'cat_id' => 
            array (
            ),
          ),
          243 => 
          array (
            'id' => '243',
            'name' => '落地灯',
            'url' => 'category.php?id=243',
            'cat_id' => 
            array (
            ),
          ),
          244 => 
          array (
            'id' => '244',
            'name' => '五金电器',
            'url' => 'category.php?id=244',
            'cat_id' => 
            array (
            ),
          ),
          246 => 
          array (
            'id' => '246',
            'name' => '应急灯/手电',
            'url' => 'category.php?id=246',
            'cat_id' => 
            array (
            ),
          ),
          248 => 
          array (
            'id' => '248',
            'name' => '装饰灯',
            'url' => 'category.php?id=248',
            'cat_id' => 
            array (
            ),
          ),
          250 => 
          array (
            'id' => '250',
            'name' => '吊灯',
            'url' => 'category.php?id=250',
            'cat_id' => 
            array (
            ),
          ),
          252 => 
          array (
            'id' => '252',
            'name' => '氛围照明',
            'url' => 'category.php?id=252',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      255 => 
      array (
        'id' => '255',
        'name' => '生活日用',
        'url' => 'category.php?id=255',
        'cat_id' => 
        array (
          257 => 
          array (
            'id' => '257',
            'name' => '收纳用品',
            'url' => 'category.php?id=257',
            'cat_id' => 
            array (
            ),
          ),
          259 => 
          array (
            'id' => '259',
            'name' => '雨伞雨具',
            'url' => 'category.php?id=259',
            'cat_id' => 
            array (
            ),
          ),
          261 => 
          array (
            'id' => '261',
            'name' => '浴室用品',
            'url' => 'category.php?id=261',
            'cat_id' => 
            array (
            ),
          ),
          263 => 
          array (
            'id' => '263',
            'name' => '缝纫/针织用品',
            'url' => 'category.php?id=263',
            'cat_id' => 
            array (
            ),
          ),
          264 => 
          array (
            'id' => '264',
            'name' => '洗晒/熨烫',
            'url' => 'category.php?id=264',
            'cat_id' => 
            array (
            ),
          ),
          266 => 
          array (
            'id' => '266',
            'name' => '净化除味',
            'url' => 'category.php?id=266',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  6 => 
  array (
    'brand_count' => 1,
    'name' => '男装、女装、内衣',
    'oldname' => '男装、女装、内衣',
    'url' => 'category.php?id=6',
    'cat_id' => 
    array (
      347 => 
      array (
        'id' => '347',
        'name' => '女装',
        'url' => 'category.php?id=347',
        'cat_id' => 
        array (
          349 => 
          array (
            'id' => '349',
            'name' => '连衣裙',
            'url' => 'category.php?id=349',
            'cat_id' => 
            array (
            ),
          ),
          350 => 
          array (
            'id' => '350',
            'name' => '蕾丝/雪纺衫',
            'url' => 'category.php?id=350',
            'cat_id' => 
            array (
            ),
          ),
          351 => 
          array (
            'id' => '351',
            'name' => '衬衫',
            'url' => 'category.php?id=351',
            'cat_id' => 
            array (
            ),
          ),
          352 => 
          array (
            'id' => '352',
            'name' => 'T恤',
            'url' => 'category.php?id=352',
            'cat_id' => 
            array (
            ),
          ),
          354 => 
          array (
            'id' => '354',
            'name' => '半身裙',
            'url' => 'category.php?id=354',
            'cat_id' => 
            array (
            ),
          ),
          356 => 
          array (
            'id' => '356',
            'name' => '休闲裤',
            'url' => 'category.php?id=356',
            'cat_id' => 
            array (
            ),
          ),
          358 => 
          array (
            'id' => '358',
            'name' => '短裤',
            'url' => 'category.php?id=358',
            'cat_id' => 
            array (
            ),
          ),
          361 => 
          array (
            'id' => '361',
            'name' => '牛仔裤',
            'url' => 'category.php?id=361',
            'cat_id' => 
            array (
            ),
          ),
          363 => 
          array (
            'id' => '363',
            'name' => '针织衫',
            'url' => 'category.php?id=363',
            'cat_id' => 
            array (
            ),
          ),
          365 => 
          array (
            'id' => '365',
            'name' => '吊带/背心',
            'url' => 'category.php?id=365',
            'cat_id' => 
            array (
            ),
          ),
          367 => 
          array (
            'id' => '367',
            'name' => '打底衫',
            'url' => 'category.php?id=367',
            'cat_id' => 
            array (
            ),
          ),
          369 => 
          array (
            'id' => '369',
            'name' => '打底裤',
            'url' => 'category.php?id=369',
            'cat_id' => 
            array (
            ),
          ),
          370 => 
          array (
            'id' => '370',
            'name' => '正装裤',
            'url' => 'category.php?id=370',
            'cat_id' => 
            array (
            ),
          ),
          372 => 
          array (
            'id' => '372',
            'name' => '小西服',
            'url' => 'category.php?id=372',
            'cat_id' => 
            array (
            ),
          ),
          374 => 
          array (
            'id' => '374',
            'name' => '马甲',
            'url' => 'category.php?id=374',
            'cat_id' => 
            array (
            ),
          ),
          377 => 
          array (
            'id' => '377',
            'name' => '风衣',
            'url' => 'category.php?id=377',
            'cat_id' => 
            array (
            ),
          ),
          379 => 
          array (
            'id' => '379',
            'name' => '羊毛衫',
            'url' => 'category.php?id=379',
            'cat_id' => 
            array (
            ),
          ),
          381 => 
          array (
            'id' => '381',
            'name' => '羊绒衫',
            'url' => 'category.php?id=381',
            'cat_id' => 
            array (
            ),
          ),
          383 => 
          array (
            'id' => '383',
            'name' => '短外套',
            'url' => 'category.php?id=383',
            'cat_id' => 
            array (
            ),
          ),
          385 => 
          array (
            'id' => '385',
            'name' => '棉服',
            'url' => 'category.php?id=385',
            'cat_id' => 
            array (
            ),
          ),
          388 => 
          array (
            'id' => '388',
            'name' => '毛呢大衣',
            'url' => 'category.php?id=388',
            'cat_id' => 
            array (
            ),
          ),
          390 => 
          array (
            'id' => '390',
            'name' => '加绒裤',
            'url' => 'category.php?id=390',
            'cat_id' => 
            array (
            ),
          ),
          395 => 
          array (
            'id' => '395',
            'name' => '羽绒服',
            'url' => 'category.php?id=395',
            'cat_id' => 
            array (
            ),
          ),
          400 => 
          array (
            'id' => '400',
            'name' => '皮草',
            'url' => 'category.php?id=400',
            'cat_id' => 
            array (
            ),
          ),
          429 => 
          array (
            'id' => '429',
            'name' => '真皮皮衣',
            'url' => 'category.php?id=429',
            'cat_id' => 
            array (
            ),
          ),
          431 => 
          array (
            'id' => '431',
            'name' => '仿皮皮衣',
            'url' => 'category.php?id=431',
            'cat_id' => 
            array (
            ),
          ),
          444 => 
          array (
            'id' => '444',
            'name' => '旗袍/唐装',
            'url' => 'category.php?id=444',
            'cat_id' => 
            array (
            ),
          ),
          448 => 
          array (
            'id' => '448',
            'name' => '礼服',
            'url' => 'category.php?id=448',
            'cat_id' => 
            array (
            ),
          ),
          451 => 
          array (
            'id' => '451',
            'name' => '婚纱',
            'url' => 'category.php?id=451',
            'cat_id' => 
            array (
            ),
          ),
          454 => 
          array (
            'id' => '454',
            'name' => '中老年女装',
            'url' => 'category.php?id=454',
            'cat_id' => 
            array (
            ),
          ),
          455 => 
          array (
            'id' => '455',
            'name' => '大码女装',
            'url' => 'category.php?id=455',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      463 => 
      array (
        'id' => '463',
        'name' => '男装',
        'url' => 'category.php?id=463',
        'cat_id' => 
        array (
          473 => 
          array (
            'id' => '473',
            'name' => 'T恤',
            'url' => 'category.php?id=473',
            'cat_id' => 
            array (
            ),
          ),
          466 => 
          array (
            'id' => '466',
            'name' => '裤子',
            'url' => 'category.php?id=466',
            'cat_id' => 
            array (
            ),
          ),
          474 => 
          array (
            'id' => '474',
            'name' => 'POLO衫',
            'url' => 'category.php?id=474',
            'cat_id' => 
            array (
            ),
          ),
          475 => 
          array (
            'id' => '475',
            'name' => '针织衫',
            'url' => 'category.php?id=475',
            'cat_id' => 
            array (
            ),
          ),
          478 => 
          array (
            'id' => '478',
            'name' => '夹克',
            'url' => 'category.php?id=478',
            'cat_id' => 
            array (
            ),
          ),
          480 => 
          array (
            'id' => '480',
            'name' => '卫衣',
            'url' => 'category.php?id=480',
            'cat_id' => 
            array (
            ),
          ),
          485 => 
          array (
            'id' => '485',
            'name' => '风衣',
            'url' => 'category.php?id=485',
            'cat_id' => 
            array (
            ),
          ),
          486 => 
          array (
            'id' => '486',
            'name' => '马甲/背心',
            'url' => 'category.php?id=486',
            'cat_id' => 
            array (
            ),
          ),
          488 => 
          array (
            'id' => '488',
            'name' => '短裤',
            'url' => 'category.php?id=488',
            'cat_id' => 
            array (
            ),
          ),
          494 => 
          array (
            'id' => '494',
            'name' => '休闲裤',
            'url' => 'category.php?id=494',
            'cat_id' => 
            array (
            ),
          ),
          498 => 
          array (
            'id' => '498',
            'name' => '牛仔裤',
            'url' => 'category.php?id=498',
            'cat_id' => 
            array (
            ),
          ),
          502 => 
          array (
            'id' => '502',
            'name' => '西服',
            'url' => 'category.php?id=502',
            'cat_id' => 
            array (
            ),
          ),
          504 => 
          array (
            'id' => '504',
            'name' => '西裤',
            'url' => 'category.php?id=504',
            'cat_id' => 
            array (
            ),
          ),
          505 => 
          array (
            'id' => '505',
            'name' => '西服套装',
            'url' => 'category.php?id=505',
            'cat_id' => 
            array (
            ),
          ),
          507 => 
          array (
            'id' => '507',
            'name' => '真皮皮衣',
            'url' => 'category.php?id=507',
            'cat_id' => 
            array (
            ),
          ),
          508 => 
          array (
            'id' => '508',
            'name' => '仿皮皮衣',
            'url' => 'category.php?id=508',
            'cat_id' => 
            array (
            ),
          ),
          510 => 
          array (
            'id' => '510',
            'name' => '羽绒服',
            'url' => 'category.php?id=510',
            'cat_id' => 
            array (
            ),
          ),
          512 => 
          array (
            'id' => '512',
            'name' => '毛呢大衣',
            'url' => 'category.php?id=512',
            'cat_id' => 
            array (
            ),
          ),
          513 => 
          array (
            'id' => '513',
            'name' => '棉服',
            'url' => 'category.php?id=513',
            'cat_id' => 
            array (
            ),
          ),
          516 => 
          array (
            'id' => '516',
            'name' => '羊绒衫',
            'url' => 'category.php?id=516',
            'cat_id' => 
            array (
            ),
          ),
          517 => 
          array (
            'id' => '517',
            'name' => '羊毛衫',
            'url' => 'category.php?id=517',
            'cat_id' => 
            array (
            ),
          ),
          520 => 
          array (
            'id' => '520',
            'name' => '卫裤/运动裤',
            'url' => 'category.php?id=520',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      630 => 
      array (
        'id' => '630',
        'name' => '服饰配件',
        'url' => 'category.php?id=630',
        'cat_id' => 
        array (
          641 => 
          array (
            'id' => '641',
            'name' => '配饰',
            'url' => 'category.php?id=641',
            'cat_id' => 
            array (
            ),
          ),
          657 => 
          array (
            'id' => '657',
            'name' => '光学镜架/镜片',
            'url' => 'category.php?id=657',
            'cat_id' => 
            array (
            ),
          ),
          663 => 
          array (
            'id' => '663',
            'name' => '防辐射眼镜',
            'url' => 'category.php?id=663',
            'cat_id' => 
            array (
            ),
          ),
          664 => 
          array (
            'id' => '664',
            'name' => '女士丝巾/围巾/披肩',
            'url' => 'category.php?id=664',
            'cat_id' => 
            array (
            ),
          ),
          665 => 
          array (
            'id' => '665',
            'name' => '棒球帽',
            'url' => 'category.php?id=665',
            'cat_id' => 
            array (
            ),
          ),
          666 => 
          array (
            'id' => '666',
            'name' => '遮阳伞/雨伞',
            'url' => 'category.php?id=666',
            'cat_id' => 
            array (
            ),
          ),
          668 => 
          array (
            'id' => '668',
            'name' => '遮阳帽',
            'url' => 'category.php?id=668',
            'cat_id' => 
            array (
            ),
          ),
          669 => 
          array (
            'id' => '669',
            'name' => '领带/领结/领带夹',
            'url' => 'category.php?id=669',
            'cat_id' => 
            array (
            ),
          ),
          670 => 
          array (
            'id' => '670',
            'name' => '男士腰带/礼盒',
            'url' => 'category.php?id=670',
            'cat_id' => 
            array (
            ),
          ),
          671 => 
          array (
            'id' => '671',
            'name' => '防晒手套',
            'url' => 'category.php?id=671',
            'cat_id' => 
            array (
            ),
          ),
          672 => 
          array (
            'id' => '672',
            'name' => '老花镜',
            'url' => 'category.php?id=672',
            'cat_id' => 
            array (
            ),
          ),
          673 => 
          array (
            'id' => '673',
            'name' => '袖扣',
            'url' => 'category.php?id=673',
            'cat_id' => 
            array (
            ),
          ),
          674 => 
          array (
            'id' => '674',
            'name' => '鸭舌帽',
            'url' => 'category.php?id=674',
            'cat_id' => 
            array (
            ),
          ),
          675 => 
          array (
            'id' => '675',
            'name' => '装饰眼镜',
            'url' => 'category.php?id=675',
            'cat_id' => 
            array (
            ),
          ),
          676 => 
          array (
            'id' => '676',
            'name' => '女士腰带/礼盒',
            'url' => 'category.php?id=676',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      547 => 
      array (
        'id' => '547',
        'name' => '内衣',
        'url' => 'category.php?id=547',
        'cat_id' => 
        array (
          592 => 
          array (
            'id' => '592',
            'name' => '内衣配件',
            'url' => 'category.php?id=592',
            'cat_id' => 
            array (
            ),
          ),
          550 => 
          array (
            'id' => '550',
            'name' => '文胸',
            'url' => 'category.php?id=550',
            'cat_id' => 
            array (
            ),
          ),
          554 => 
          array (
            'id' => '554',
            'name' => '睡衣/家居服',
            'url' => 'category.php?id=554',
            'cat_id' => 
            array (
            ),
          ),
          555 => 
          array (
            'id' => '555',
            'name' => '情侣睡衣',
            'url' => 'category.php?id=555',
            'cat_id' => 
            array (
            ),
          ),
          557 => 
          array (
            'id' => '557',
            'name' => '文胸套装',
            'url' => 'category.php?id=557',
            'cat_id' => 
            array (
            ),
          ),
          560 => 
          array (
            'id' => '560',
            'name' => '少女文胸',
            'url' => 'category.php?id=560',
            'cat_id' => 
            array (
            ),
          ),
          562 => 
          array (
            'id' => '562',
            'name' => '女式内裤',
            'url' => 'category.php?id=562',
            'cat_id' => 
            array (
            ),
          ),
          564 => 
          array (
            'id' => '564',
            'name' => '男式内裤',
            'url' => 'category.php?id=564',
            'cat_id' => 
            array (
            ),
          ),
          566 => 
          array (
            'id' => '566',
            'name' => '商务男袜',
            'url' => 'category.php?id=566',
            'cat_id' => 
            array (
            ),
          ),
          567 => 
          array (
            'id' => '567',
            'name' => '休闲棉袜',
            'url' => 'category.php?id=567',
            'cat_id' => 
            array (
            ),
          ),
          568 => 
          array (
            'id' => '568',
            'name' => '吊带/背心',
            'url' => 'category.php?id=568',
            'cat_id' => 
            array (
            ),
          ),
          569 => 
          array (
            'id' => '569',
            'name' => '打底衫',
            'url' => 'category.php?id=569',
            'cat_id' => 
            array (
            ),
          ),
          570 => 
          array (
            'id' => '570',
            'name' => '抹胸',
            'url' => 'category.php?id=570',
            'cat_id' => 
            array (
            ),
          ),
          571 => 
          array (
            'id' => '571',
            'name' => '连裤袜/丝袜',
            'url' => 'category.php?id=571',
            'cat_id' => 
            array (
            ),
          ),
          580 => 
          array (
            'id' => '580',
            'name' => '美腿袜',
            'url' => 'category.php?id=580',
            'cat_id' => 
            array (
            ),
          ),
          582 => 
          array (
            'id' => '582',
            'name' => '打底裤袜',
            'url' => 'category.php?id=582',
            'cat_id' => 
            array (
            ),
          ),
          583 => 
          array (
            'id' => '583',
            'name' => '塑身美体',
            'url' => 'category.php?id=583',
            'cat_id' => 
            array (
            ),
          ),
          590 => 
          array (
            'id' => '590',
            'name' => '大码内衣',
            'url' => 'category.php?id=590',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1442 => 
      array (
        'id' => '1442',
        'name' => '运动户外',
        'url' => 'category.php?id=1442',
        'cat_id' => 
        array (
          1443 => 
          array (
            'id' => '1443',
            'name' => '跑步运动',
            'url' => 'category.php?id=1443',
            'cat_id' => 
            array (
            ),
          ),
          1444 => 
          array (
            'id' => '1444',
            'name' => '室内健身',
            'url' => 'category.php?id=1444',
            'cat_id' => 
            array (
            ),
          ),
          1445 => 
          array (
            'id' => '1445',
            'name' => '自行车运动',
            'url' => 'category.php?id=1445',
            'cat_id' => 
            array (
            ),
          ),
          1446 => 
          array (
            'id' => '1446',
            'name' => '轮滑运动',
            'url' => 'category.php?id=1446',
            'cat_id' => 
            array (
            ),
          ),
          1447 => 
          array (
            'id' => '1447',
            'name' => '羽毛球/网球/乒乓球',
            'url' => 'category.php?id=1447',
            'cat_id' => 
            array (
            ),
          ),
          1448 => 
          array (
            'id' => '1448',
            'name' => '足球/篮球/排球',
            'url' => 'category.php?id=1448',
            'cat_id' => 
            array (
            ),
          ),
          1449 => 
          array (
            'id' => '1449',
            'name' => '运动休闲',
            'url' => 'category.php?id=1449',
            'cat_id' => 
            array (
            ),
          ),
          1450 => 
          array (
            'id' => '1450',
            'name' => '钓鱼用品',
            'url' => 'category.php?id=1450',
            'cat_id' => 
            array (
            ),
          ),
          1451 => 
          array (
            'id' => '1451',
            'name' => '野营烧烤',
            'url' => 'category.php?id=1451',
            'cat_id' => 
            array (
            ),
          ),
          1452 => 
          array (
            'id' => '1452',
            'name' => '游泳运动',
            'url' => 'category.php?id=1452',
            'cat_id' => 
            array (
            ),
          ),
          1453 => 
          array (
            'id' => '1453',
            'name' => '舞蹈运动',
            'url' => 'category.php?id=1453',
            'cat_id' => 
            array (
            ),
          ),
          1454 => 
          array (
            'id' => '1454',
            'name' => '瑜伽运动',
            'url' => 'category.php?id=1454',
            'cat_id' => 
            array (
            ),
          ),
          1455 => 
          array (
            'id' => '1455',
            'name' => '防狼防身',
            'url' => 'category.php?id=1455',
            'cat_id' => 
            array (
            ),
          ),
          1456 => 
          array (
            'id' => '1456',
            'name' => '水上运动',
            'url' => 'category.php?id=1456',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  8 => 
  array (
    'brand_count' => 1,
    'name' => '鞋靴、箱包、钟表、奢侈品',
    'oldname' => '鞋靴、箱包、钟表、奢侈品',
    'url' => 'category.php?id=8',
    'cat_id' => 
    array (
      362 => 
      array (
        'id' => '362',
        'name' => '奢侈品',
        'url' => 'category.php?id=362',
        'cat_id' => 
        array (
          443 => 
          array (
            'id' => '443',
            'name' => '鞋靴',
            'url' => 'category.php?id=443',
            'cat_id' => 
            array (
            ),
          ),
          439 => 
          array (
            'id' => '439',
            'name' => '箱包',
            'url' => 'category.php?id=439',
            'cat_id' => 
            array (
            ),
          ),
          440 => 
          array (
            'id' => '440',
            'name' => '钱包',
            'url' => 'category.php?id=440',
            'cat_id' => 
            array (
            ),
          ),
          441 => 
          array (
            'id' => '441',
            'name' => '服饰',
            'url' => 'category.php?id=441',
            'cat_id' => 
            array (
            ),
          ),
          442 => 
          array (
            'id' => '442',
            'name' => '腰带',
            'url' => 'category.php?id=442',
            'cat_id' => 
            array (
            ),
          ),
          445 => 
          array (
            'id' => '445',
            'name' => '太阳镜/眼镜框',
            'url' => 'category.php?id=445',
            'cat_id' => 
            array (
            ),
          ),
          446 => 
          array (
            'id' => '446',
            'name' => '饰品',
            'url' => 'category.php?id=446',
            'cat_id' => 
            array (
            ),
          ),
          447 => 
          array (
            'id' => '447',
            'name' => '配件',
            'url' => 'category.php?id=447',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      360 => 
      array (
        'id' => '360',
        'name' => '功能箱包',
        'url' => 'category.php?id=360',
        'cat_id' => 
        array (
          427 => 
          array (
            'id' => '427',
            'name' => '拉杆箱/拉杆包',
            'url' => 'category.php?id=427',
            'cat_id' => 
            array (
            ),
          ),
          428 => 
          array (
            'id' => '428',
            'name' => '旅行包',
            'url' => 'category.php?id=428',
            'cat_id' => 
            array (
            ),
          ),
          430 => 
          array (
            'id' => '430',
            'name' => '电脑包',
            'url' => 'category.php?id=430',
            'cat_id' => 
            array (
            ),
          ),
          432 => 
          array (
            'id' => '432',
            'name' => '休闲运动包',
            'url' => 'category.php?id=432',
            'cat_id' => 
            array (
            ),
          ),
          433 => 
          array (
            'id' => '433',
            'name' => '相机包',
            'url' => 'category.php?id=433',
            'cat_id' => 
            array (
            ),
          ),
          434 => 
          array (
            'id' => '434',
            'name' => '腰包/胸包',
            'url' => 'category.php?id=434',
            'cat_id' => 
            array (
            ),
          ),
          435 => 
          array (
            'id' => '435',
            'name' => '登山包',
            'url' => 'category.php?id=435',
            'cat_id' => 
            array (
            ),
          ),
          436 => 
          array (
            'id' => '436',
            'name' => '旅行配件',
            'url' => 'category.php?id=436',
            'cat_id' => 
            array (
            ),
          ),
          437 => 
          array (
            'id' => '437',
            'name' => '书包',
            'url' => 'category.php?id=437',
            'cat_id' => 
            array (
            ),
          ),
          438 => 
          array (
            'id' => '438',
            'name' => '妈咪包',
            'url' => 'category.php?id=438',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      355 => 
      array (
        'id' => '355',
        'name' => '流行男鞋',
        'url' => 'category.php?id=355',
        'cat_id' => 
        array (
          399 => 
          array (
            'id' => '399',
            'name' => '休闲鞋',
            'url' => 'category.php?id=399',
            'cat_id' => 
            array (
            ),
          ),
          401 => 
          array (
            'id' => '401',
            'name' => '凉鞋/沙滩鞋',
            'url' => 'category.php?id=401',
            'cat_id' => 
            array (
            ),
          ),
          402 => 
          array (
            'id' => '402',
            'name' => '帆布鞋',
            'url' => 'category.php?id=402',
            'cat_id' => 
            array (
            ),
          ),
          403 => 
          array (
            'id' => '403',
            'name' => '商务休闲鞋',
            'url' => 'category.php?id=403',
            'cat_id' => 
            array (
            ),
          ),
          404 => 
          array (
            'id' => '404',
            'name' => '正装鞋',
            'url' => 'category.php?id=404',
            'cat_id' => 
            array (
            ),
          ),
          405 => 
          array (
            'id' => '405',
            'name' => '增高鞋',
            'url' => 'category.php?id=405',
            'cat_id' => 
            array (
            ),
          ),
          406 => 
          array (
            'id' => '406',
            'name' => '拖鞋/人字拖',
            'url' => 'category.php?id=406',
            'cat_id' => 
            array (
            ),
          ),
          407 => 
          array (
            'id' => '407',
            'name' => '工装鞋',
            'url' => 'category.php?id=407',
            'cat_id' => 
            array (
            ),
          ),
          408 => 
          array (
            'id' => '408',
            'name' => '男靴',
            'url' => 'category.php?id=408',
            'cat_id' => 
            array (
            ),
          ),
          409 => 
          array (
            'id' => '409',
            'name' => '传统布鞋',
            'url' => 'category.php?id=409',
            'cat_id' => 
            array (
            ),
          ),
          410 => 
          array (
            'id' => '410',
            'name' => '功能鞋',
            'url' => 'category.php?id=410',
            'cat_id' => 
            array (
            ),
          ),
          411 => 
          array (
            'id' => '411',
            'name' => '鞋配件',
            'url' => 'category.php?id=411',
            'cat_id' => 
            array (
            ),
          ),
          412 => 
          array (
            'id' => '412',
            'name' => '定制鞋',
            'url' => 'category.php?id=412',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      353 => 
      array (
        'id' => '353',
        'name' => '时尚女鞋',
        'url' => 'category.php?id=353',
        'cat_id' => 
        array (
          368 => 
          array (
            'id' => '368',
            'name' => '凉鞋',
            'url' => 'category.php?id=368',
            'cat_id' => 
            array (
            ),
          ),
          371 => 
          array (
            'id' => '371',
            'name' => '单鞋',
            'url' => 'category.php?id=371',
            'cat_id' => 
            array (
            ),
          ),
          373 => 
          array (
            'id' => '373',
            'name' => '高跟鞋',
            'url' => 'category.php?id=373',
            'cat_id' => 
            array (
            ),
          ),
          375 => 
          array (
            'id' => '375',
            'name' => '坡跟鞋',
            'url' => 'category.php?id=375',
            'cat_id' => 
            array (
            ),
          ),
          376 => 
          array (
            'id' => '376',
            'name' => '松糕鞋',
            'url' => 'category.php?id=376',
            'cat_id' => 
            array (
            ),
          ),
          378 => 
          array (
            'id' => '378',
            'name' => '鱼嘴鞋',
            'url' => 'category.php?id=378',
            'cat_id' => 
            array (
            ),
          ),
          380 => 
          array (
            'id' => '380',
            'name' => '休闲鞋',
            'url' => 'category.php?id=380',
            'cat_id' => 
            array (
            ),
          ),
          382 => 
          array (
            'id' => '382',
            'name' => '帆布鞋',
            'url' => 'category.php?id=382',
            'cat_id' => 
            array (
            ),
          ),
          384 => 
          array (
            'id' => '384',
            'name' => '拖鞋/人字拖',
            'url' => 'category.php?id=384',
            'cat_id' => 
            array (
            ),
          ),
          386 => 
          array (
            'id' => '386',
            'name' => '妈妈鞋',
            'url' => 'category.php?id=386',
            'cat_id' => 
            array (
            ),
          ),
          387 => 
          array (
            'id' => '387',
            'name' => '防水台',
            'url' => 'category.php?id=387',
            'cat_id' => 
            array (
            ),
          ),
          389 => 
          array (
            'id' => '389',
            'name' => '雨鞋/雨靴',
            'url' => 'category.php?id=389',
            'cat_id' => 
            array (
            ),
          ),
          391 => 
          array (
            'id' => '391',
            'name' => '内增高',
            'url' => 'category.php?id=391',
            'cat_id' => 
            array (
            ),
          ),
          392 => 
          array (
            'id' => '392',
            'name' => '布鞋/绣花鞋',
            'url' => 'category.php?id=392',
            'cat_id' => 
            array (
            ),
          ),
          393 => 
          array (
            'id' => '393',
            'name' => '女靴',
            'url' => 'category.php?id=393',
            'cat_id' => 
            array (
            ),
          ),
          394 => 
          array (
            'id' => '394',
            'name' => '雪地靴',
            'url' => 'category.php?id=394',
            'cat_id' => 
            array (
            ),
          ),
          396 => 
          array (
            'id' => '396',
            'name' => '踝靴',
            'url' => 'category.php?id=396',
            'cat_id' => 
            array (
            ),
          ),
          397 => 
          array (
            'id' => '397',
            'name' => '马丁靴',
            'url' => 'category.php?id=397',
            'cat_id' => 
            array (
            ),
          ),
          398 => 
          array (
            'id' => '398',
            'name' => '鞋配件',
            'url' => 'category.php?id=398',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      357 => 
      array (
        'id' => '357',
        'name' => '潮流女包',
        'url' => 'category.php?id=357',
        'cat_id' => 
        array (
          413 => 
          array (
            'id' => '413',
            'name' => '单肩包',
            'url' => 'category.php?id=413',
            'cat_id' => 
            array (
            ),
          ),
          414 => 
          array (
            'id' => '414',
            'name' => '手提包',
            'url' => 'category.php?id=414',
            'cat_id' => 
            array (
            ),
          ),
          415 => 
          array (
            'id' => '415',
            'name' => '斜跨包',
            'url' => 'category.php?id=415',
            'cat_id' => 
            array (
            ),
          ),
          416 => 
          array (
            'id' => '416',
            'name' => '双肩包',
            'url' => 'category.php?id=416',
            'cat_id' => 
            array (
            ),
          ),
          417 => 
          array (
            'id' => '417',
            'name' => '钱包',
            'url' => 'category.php?id=417',
            'cat_id' => 
            array (
            ),
          ),
          418 => 
          array (
            'id' => '418',
            'name' => '手拿包/晚宴包',
            'url' => 'category.php?id=418',
            'cat_id' => 
            array (
            ),
          ),
          419 => 
          array (
            'id' => '419',
            'name' => '卡包/零钱包',
            'url' => 'category.php?id=419',
            'cat_id' => 
            array (
            ),
          ),
          420 => 
          array (
            'id' => '420',
            'name' => '钥匙包',
            'url' => 'category.php?id=420',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      359 => 
      array (
        'id' => '359',
        'name' => '精品男包',
        'url' => 'category.php?id=359',
        'cat_id' => 
        array (
          421 => 
          array (
            'id' => '421',
            'name' => '商务公文包',
            'url' => 'category.php?id=421',
            'cat_id' => 
            array (
            ),
          ),
          422 => 
          array (
            'id' => '422',
            'name' => '单肩/斜跨包',
            'url' => 'category.php?id=422',
            'cat_id' => 
            array (
            ),
          ),
          423 => 
          array (
            'id' => '423',
            'name' => '男生手包',
            'url' => 'category.php?id=423',
            'cat_id' => 
            array (
            ),
          ),
          424 => 
          array (
            'id' => '424',
            'name' => '双肩包',
            'url' => 'category.php?id=424',
            'cat_id' => 
            array (
            ),
          ),
          425 => 
          array (
            'id' => '425',
            'name' => '钱包/卡包',
            'url' => 'category.php?id=425',
            'cat_id' => 
            array (
            ),
          ),
          426 => 
          array (
            'id' => '426',
            'name' => '钥匙包',
            'url' => 'category.php?id=426',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      364 => 
      array (
        'id' => '364',
        'name' => '钟表',
        'url' => 'category.php?id=364',
        'cat_id' => 
        array (
          449 => 
          array (
            'id' => '449',
            'name' => '男表',
            'url' => 'category.php?id=449',
            'cat_id' => 
            array (
            ),
          ),
          450 => 
          array (
            'id' => '450',
            'name' => '女表',
            'url' => 'category.php?id=450',
            'cat_id' => 
            array (
            ),
          ),
          452 => 
          array (
            'id' => '452',
            'name' => '儿童表',
            'url' => 'category.php?id=452',
            'cat_id' => 
            array (
            ),
          ),
          453 => 
          array (
            'id' => '453',
            'name' => '座钟挂钟',
            'url' => 'category.php?id=453',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  860 => 
  array (
    'brand_count' => 1,
    'name' => '个人化妆、清洁用品',
    'oldname' => '个人化妆、清洁用品',
    'url' => 'category.php?id=860',
    'cat_id' => 
    array (
      876 => 
      array (
        'id' => '876',
        'name' => '面部护肤',
        'url' => 'category.php?id=876',
        'cat_id' => 
        array (
          877 => 
          array (
            'id' => '877',
            'name' => '清洁',
            'url' => 'category.php?id=877',
            'cat_id' => 
            array (
            ),
          ),
          878 => 
          array (
            'id' => '878',
            'name' => '护肤',
            'url' => 'category.php?id=878',
            'cat_id' => 
            array (
            ),
          ),
          879 => 
          array (
            'id' => '879',
            'name' => '面膜',
            'url' => 'category.php?id=879',
            'cat_id' => 
            array (
            ),
          ),
          1177 => 
          array (
            'id' => '1177',
            'name' => '洗面奶',
            'url' => 'category.php?id=1177',
            'cat_id' => 
            array (
            ),
          ),
          1178 => 
          array (
            'id' => '1178',
            'name' => 'BB霜',
            'url' => 'category.php?id=1178',
            'cat_id' => 
            array (
            ),
          ),
          1179 => 
          array (
            'id' => '1179',
            'name' => '指甲油',
            'url' => 'category.php?id=1179',
            'cat_id' => 
            array (
            ),
          ),
          1180 => 
          array (
            'id' => '1180',
            'name' => '洗面泥',
            'url' => 'category.php?id=1180',
            'cat_id' => 
            array (
            ),
          ),
          1181 => 
          array (
            'id' => '1181',
            'name' => '水润护肤',
            'url' => 'category.php?id=1181',
            'cat_id' => 
            array (
            ),
          ),
          1182 => 
          array (
            'id' => '1182',
            'name' => '卸妆水',
            'url' => 'category.php?id=1182',
            'cat_id' => 
            array (
            ),
          ),
          1183 => 
          array (
            'id' => '1183',
            'name' => '雪花膏',
            'url' => 'category.php?id=1183',
            'cat_id' => 
            array (
            ),
          ),
          1184 => 
          array (
            'id' => '1184',
            'name' => '爽肤水',
            'url' => 'category.php?id=1184',
            'cat_id' => 
            array (
            ),
          ),
          1185 => 
          array (
            'id' => '1185',
            'name' => '清洁套装',
            'url' => 'category.php?id=1185',
            'cat_id' => 
            array (
            ),
          ),
          1186 => 
          array (
            'id' => '1186',
            'name' => '剃须',
            'url' => 'category.php?id=1186',
            'cat_id' => 
            array (
            ),
          ),
          1187 => 
          array (
            'id' => '1187',
            'name' => '洁面刷',
            'url' => 'category.php?id=1187',
            'cat_id' => 
            array (
            ),
          ),
          1188 => 
          array (
            'id' => '1188',
            'name' => '修眉笔',
            'url' => 'category.php?id=1188',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      880 => 
      array (
        'id' => '880',
        'name' => '洗发护发',
        'url' => 'category.php?id=880',
        'cat_id' => 
        array (
          881 => 
          array (
            'id' => '881',
            'name' => '洗发',
            'url' => 'category.php?id=881',
            'cat_id' => 
            array (
            ),
          ),
          882 => 
          array (
            'id' => '882',
            'name' => '护发',
            'url' => 'category.php?id=882',
            'cat_id' => 
            array (
            ),
          ),
          883 => 
          array (
            'id' => '883',
            'name' => '染发',
            'url' => 'category.php?id=883',
            'cat_id' => 
            array (
            ),
          ),
          884 => 
          array (
            'id' => '884',
            'name' => '造型',
            'url' => 'category.php?id=884',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      885 => 
      array (
        'id' => '885',
        'name' => '身体护肤',
        'url' => 'category.php?id=885',
        'cat_id' => 
        array (
          886 => 
          array (
            'id' => '886',
            'name' => '沐浴',
            'url' => 'category.php?id=886',
            'cat_id' => 
            array (
            ),
          ),
          887 => 
          array (
            'id' => '887',
            'name' => '润肤',
            'url' => 'category.php?id=887',
            'cat_id' => 
            array (
            ),
          ),
          888 => 
          array (
            'id' => '888',
            'name' => '手足',
            'url' => 'category.php?id=888',
            'cat_id' => 
            array (
            ),
          ),
          889 => 
          array (
            'id' => '889',
            'name' => '美胸',
            'url' => 'category.php?id=889',
            'cat_id' => 
            array (
            ),
          ),
          890 => 
          array (
            'id' => '890',
            'name' => '套装',
            'url' => 'category.php?id=890',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      891 => 
      array (
        'id' => '891',
        'name' => '口腔护理',
        'url' => 'category.php?id=891',
        'cat_id' => 
        array (
          892 => 
          array (
            'id' => '892',
            'name' => '牙膏/牙粉',
            'url' => 'category.php?id=892',
            'cat_id' => 
            array (
            ),
          ),
          893 => 
          array (
            'id' => '893',
            'name' => '牙刷/牙线',
            'url' => 'category.php?id=893',
            'cat_id' => 
            array (
            ),
          ),
          894 => 
          array (
            'id' => '894',
            'name' => '漱口水',
            'url' => 'category.php?id=894',
            'cat_id' => 
            array (
            ),
          ),
          895 => 
          array (
            'id' => '895',
            'name' => '套装',
            'url' => 'category.php?id=895',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      896 => 
      array (
        'id' => '896',
        'name' => '香水彩妆',
        'url' => 'category.php?id=896',
        'cat_id' => 
        array (
          897 => 
          array (
            'id' => '897',
            'name' => '香水',
            'url' => 'category.php?id=897',
            'cat_id' => 
            array (
            ),
          ),
          898 => 
          array (
            'id' => '898',
            'name' => '底妆',
            'url' => 'category.php?id=898',
            'cat_id' => 
            array (
            ),
          ),
          899 => 
          array (
            'id' => '899',
            'name' => '腮红',
            'url' => 'category.php?id=899',
            'cat_id' => 
            array (
            ),
          ),
          900 => 
          array (
            'id' => '900',
            'name' => '眼部',
            'url' => 'category.php?id=900',
            'cat_id' => 
            array (
            ),
          ),
          901 => 
          array (
            'id' => '901',
            'name' => '美甲',
            'url' => 'category.php?id=901',
            'cat_id' => 
            array (
            ),
          ),
          1349 => 
          array (
            'id' => '1349',
            'name' => '精油放疗',
            'url' => 'category.php?id=1349',
            'cat_id' => 
            array (
            ),
          ),
          1350 => 
          array (
            'id' => '1350',
            'name' => '假睫毛',
            'url' => 'category.php?id=1350',
            'cat_id' => 
            array (
            ),
          ),
          1351 => 
          array (
            'id' => '1351',
            'name' => '彩妆套装',
            'url' => 'category.php?id=1351',
            'cat_id' => 
            array (
            ),
          ),
          1352 => 
          array (
            'id' => '1352',
            'name' => '蜜粉',
            'url' => 'category.php?id=1352',
            'cat_id' => 
            array (
            ),
          ),
          1353 => 
          array (
            'id' => '1353',
            'name' => '遮瑕',
            'url' => 'category.php?id=1353',
            'cat_id' => 
            array (
            ),
          ),
          1354 => 
          array (
            'id' => '1354',
            'name' => '化妆棉',
            'url' => 'category.php?id=1354',
            'cat_id' => 
            array (
            ),
          ),
          1355 => 
          array (
            'id' => '1355',
            'name' => '双眼皮贴',
            'url' => 'category.php?id=1355',
            'cat_id' => 
            array (
            ),
          ),
          1356 => 
          array (
            'id' => '1356',
            'name' => '高光阴影',
            'url' => 'category.php?id=1356',
            'cat_id' => 
            array (
            ),
          ),
          1357 => 
          array (
            'id' => '1357',
            'name' => '隔离',
            'url' => 'category.php?id=1357',
            'cat_id' => 
            array (
            ),
          ),
          1358 => 
          array (
            'id' => '1358',
            'name' => '粉饼',
            'url' => 'category.php?id=1358',
            'cat_id' => 
            array (
            ),
          ),
          1359 => 
          array (
            'id' => '1359',
            'name' => '气垫BB',
            'url' => 'category.php?id=1359',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1205 => 
      array (
        'id' => '1205',
        'name' => '女性护理',
        'url' => 'category.php?id=1205',
        'cat_id' => 
        array (
          1206 => 
          array (
            'id' => '1206',
            'name' => '卫生巾',
            'url' => 'category.php?id=1206',
            'cat_id' => 
            array (
            ),
          ),
          1207 => 
          array (
            'id' => '1207',
            'name' => '卫生护垫',
            'url' => 'category.php?id=1207',
            'cat_id' => 
            array (
            ),
          ),
          1208 => 
          array (
            'id' => '1208',
            'name' => '私密护理',
            'url' => 'category.php?id=1208',
            'cat_id' => 
            array (
            ),
          ),
          1209 => 
          array (
            'id' => '1209',
            'name' => '脱毛膏',
            'url' => 'category.php?id=1209',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1210 => 
      array (
        'id' => '1210',
        'name' => '清洁用品',
        'url' => 'category.php?id=1210',
        'cat_id' => 
        array (
          1211 => 
          array (
            'id' => '1211',
            'name' => '纸品湿巾',
            'url' => 'category.php?id=1211',
            'cat_id' => 
            array (
            ),
          ),
          1212 => 
          array (
            'id' => '1212',
            'name' => '衣物清洁',
            'url' => 'category.php?id=1212',
            'cat_id' => 
            array (
            ),
          ),
          1213 => 
          array (
            'id' => '1213',
            'name' => '清洁工具',
            'url' => 'category.php?id=1213',
            'cat_id' => 
            array (
            ),
          ),
          1214 => 
          array (
            'id' => '1214',
            'name' => '家庭清洁',
            'url' => 'category.php?id=1214',
            'cat_id' => 
            array (
            ),
          ),
          1215 => 
          array (
            'id' => '1215',
            'name' => '一次性用品',
            'url' => 'category.php?id=1215',
            'cat_id' => 
            array (
            ),
          ),
          1216 => 
          array (
            'id' => '1216',
            'name' => '驱蚊用品',
            'url' => 'category.php?id=1216',
            'cat_id' => 
            array (
            ),
          ),
          1217 => 
          array (
            'id' => '1217',
            'name' => '皮具护理',
            'url' => 'category.php?id=1217',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  11 => 
  array (
    'brand_count' => 1,
    'name' => '母婴、玩具乐器',
    'oldname' => '母婴、玩具乐器',
    'url' => 'category.php?id=11',
    'cat_id' => 
    array (
      491 => 
      array (
        'id' => '491',
        'name' => '玩具乐器',
        'url' => 'category.php?id=491',
        'cat_id' => 
        array (
          602 => 
          array (
            'id' => '602',
            'name' => '使用年龄',
            'url' => 'category.php?id=602',
            'cat_id' => 
            array (
            ),
          ),
          603 => 
          array (
            'id' => '603',
            'name' => '遥控/电动',
            'url' => 'category.php?id=603',
            'cat_id' => 
            array (
            ),
          ),
          604 => 
          array (
            'id' => '604',
            'name' => '毛绒布艺',
            'url' => 'category.php?id=604',
            'cat_id' => 
            array (
            ),
          ),
          605 => 
          array (
            'id' => '605',
            'name' => '娃娃玩具',
            'url' => 'category.php?id=605',
            'cat_id' => 
            array (
            ),
          ),
          606 => 
          array (
            'id' => '606',
            'name' => '模型玩具',
            'url' => 'category.php?id=606',
            'cat_id' => 
            array (
            ),
          ),
          607 => 
          array (
            'id' => '607',
            'name' => '健身玩具',
            'url' => 'category.php?id=607',
            'cat_id' => 
            array (
            ),
          ),
          608 => 
          array (
            'id' => '608',
            'name' => '动漫玩具',
            'url' => 'category.php?id=608',
            'cat_id' => 
            array (
            ),
          ),
          609 => 
          array (
            'id' => '609',
            'name' => '益智玩具',
            'url' => 'category.php?id=609',
            'cat_id' => 
            array (
            ),
          ),
          610 => 
          array (
            'id' => '610',
            'name' => '积木拼插',
            'url' => 'category.php?id=610',
            'cat_id' => 
            array (
            ),
          ),
          611 => 
          array (
            'id' => '611',
            'name' => 'DIY玩具',
            'url' => 'category.php?id=611',
            'cat_id' => 
            array (
            ),
          ),
          612 => 
          array (
            'id' => '612',
            'name' => '创意减压',
            'url' => 'category.php?id=612',
            'cat_id' => 
            array (
            ),
          ),
          613 => 
          array (
            'id' => '613',
            'name' => '乐器相关',
            'url' => 'category.php?id=613',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      476 => 
      array (
        'id' => '476',
        'name' => '奶粉',
        'url' => 'category.php?id=476',
        'cat_id' => 
        array (
          492 => 
          array (
            'id' => '492',
            'name' => '婴幼奶粉',
            'url' => 'category.php?id=492',
            'cat_id' => 
            array (
            ),
          ),
          493 => 
          array (
            'id' => '493',
            'name' => '成人奶粉',
            'url' => 'category.php?id=493',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      477 => 
      array (
        'id' => '477',
        'name' => '营养辅食',
        'url' => 'category.php?id=477',
        'cat_id' => 
        array (
          495 => 
          array (
            'id' => '495',
            'name' => 'DHA',
            'url' => 'category.php?id=495',
            'cat_id' => 
            array (
            ),
          ),
          496 => 
          array (
            'id' => '496',
            'name' => '钙铁锌/维生素',
            'url' => 'category.php?id=496',
            'cat_id' => 
            array (
            ),
          ),
          497 => 
          array (
            'id' => '497',
            'name' => '益生菌/初乳',
            'url' => 'category.php?id=497',
            'cat_id' => 
            array (
            ),
          ),
          499 => 
          array (
            'id' => '499',
            'name' => '清火/开胃',
            'url' => 'category.php?id=499',
            'cat_id' => 
            array (
            ),
          ),
          500 => 
          array (
            'id' => '500',
            'name' => '米粉/菜粉',
            'url' => 'category.php?id=500',
            'cat_id' => 
            array (
            ),
          ),
          501 => 
          array (
            'id' => '501',
            'name' => '果泥/果汁',
            'url' => 'category.php?id=501',
            'cat_id' => 
            array (
            ),
          ),
          503 => 
          array (
            'id' => '503',
            'name' => '面条/粥',
            'url' => 'category.php?id=503',
            'cat_id' => 
            array (
            ),
          ),
          506 => 
          array (
            'id' => '506',
            'name' => '宝宝零食',
            'url' => 'category.php?id=506',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      479 => 
      array (
        'id' => '479',
        'name' => '尿裤湿巾',
        'url' => 'category.php?id=479',
        'cat_id' => 
        array (
          509 => 
          array (
            'id' => '509',
            'name' => '婴儿尿裤',
            'url' => 'category.php?id=509',
            'cat_id' => 
            array (
            ),
          ),
          511 => 
          array (
            'id' => '511',
            'name' => '拉拉裤',
            'url' => 'category.php?id=511',
            'cat_id' => 
            array (
            ),
          ),
          514 => 
          array (
            'id' => '514',
            'name' => '成人尿裤',
            'url' => 'category.php?id=514',
            'cat_id' => 
            array (
            ),
          ),
          515 => 
          array (
            'id' => '515',
            'name' => '湿巾',
            'url' => 'category.php?id=515',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      481 => 
      array (
        'id' => '481',
        'name' => '洗护用品',
        'url' => 'category.php?id=481',
        'cat_id' => 
        array (
          518 => 
          array (
            'id' => '518',
            'name' => '宝宝护肤',
            'url' => 'category.php?id=518',
            'cat_id' => 
            array (
            ),
          ),
          519 => 
          array (
            'id' => '519',
            'name' => '宝宝洗浴',
            'url' => 'category.php?id=519',
            'cat_id' => 
            array (
            ),
          ),
          521 => 
          array (
            'id' => '521',
            'name' => '理发器',
            'url' => 'category.php?id=521',
            'cat_id' => 
            array (
            ),
          ),
          523 => 
          array (
            'id' => '523',
            'name' => '洗衣液/皂',
            'url' => 'category.php?id=523',
            'cat_id' => 
            array (
            ),
          ),
          524 => 
          array (
            'id' => '524',
            'name' => '奶瓶清洗',
            'url' => 'category.php?id=524',
            'cat_id' => 
            array (
            ),
          ),
          525 => 
          array (
            'id' => '525',
            'name' => '日常护理',
            'url' => 'category.php?id=525',
            'cat_id' => 
            array (
            ),
          ),
          527 => 
          array (
            'id' => '527',
            'name' => '座便器',
            'url' => 'category.php?id=527',
            'cat_id' => 
            array (
            ),
          ),
          528 => 
          array (
            'id' => '528',
            'name' => '驱蚊防蚊',
            'url' => 'category.php?id=528',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      482 => 
      array (
        'id' => '482',
        'name' => '喂养用品',
        'url' => 'category.php?id=482',
        'cat_id' => 
        array (
          529 => 
          array (
            'id' => '529',
            'name' => '奶瓶奶嘴',
            'url' => 'category.php?id=529',
            'cat_id' => 
            array (
            ),
          ),
          531 => 
          array (
            'id' => '531',
            'name' => '吸奶器',
            'url' => 'category.php?id=531',
            'cat_id' => 
            array (
            ),
          ),
          533 => 
          array (
            'id' => '533',
            'name' => '牙胶安抚',
            'url' => 'category.php?id=533',
            'cat_id' => 
            array (
            ),
          ),
          534 => 
          array (
            'id' => '534',
            'name' => '暖奶消毒',
            'url' => 'category.php?id=534',
            'cat_id' => 
            array (
            ),
          ),
          535 => 
          array (
            'id' => '535',
            'name' => '辅食料理机',
            'url' => 'category.php?id=535',
            'cat_id' => 
            array (
            ),
          ),
          536 => 
          array (
            'id' => '536',
            'name' => '碗盘叉勺',
            'url' => 'category.php?id=536',
            'cat_id' => 
            array (
            ),
          ),
          537 => 
          array (
            'id' => '537',
            'name' => '水壶/水杯',
            'url' => 'category.php?id=537',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      483 => 
      array (
        'id' => '483',
        'name' => '童车童床',
        'url' => 'category.php?id=483',
        'cat_id' => 
        array (
          538 => 
          array (
            'id' => '538',
            'name' => '婴儿床',
            'url' => 'category.php?id=538',
            'cat_id' => 
            array (
            ),
          ),
          539 => 
          array (
            'id' => '539',
            'name' => '婴儿推车',
            'url' => 'category.php?id=539',
            'cat_id' => 
            array (
            ),
          ),
          540 => 
          array (
            'id' => '540',
            'name' => '餐椅摇椅',
            'url' => 'category.php?id=540',
            'cat_id' => 
            array (
            ),
          ),
          541 => 
          array (
            'id' => '541',
            'name' => '学步车',
            'url' => 'category.php?id=541',
            'cat_id' => 
            array (
            ),
          ),
          543 => 
          array (
            'id' => '543',
            'name' => '三轮车',
            'url' => 'category.php?id=543',
            'cat_id' => 
            array (
            ),
          ),
          545 => 
          array (
            'id' => '545',
            'name' => '自行车',
            'url' => 'category.php?id=545',
            'cat_id' => 
            array (
            ),
          ),
          546 => 
          array (
            'id' => '546',
            'name' => '扭扭车',
            'url' => 'category.php?id=546',
            'cat_id' => 
            array (
            ),
          ),
          548 => 
          array (
            'id' => '548',
            'name' => '滑板车',
            'url' => 'category.php?id=548',
            'cat_id' => 
            array (
            ),
          ),
          549 => 
          array (
            'id' => '549',
            'name' => '电动车',
            'url' => 'category.php?id=549',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      484 => 
      array (
        'id' => '484',
        'name' => '安全座椅',
        'url' => 'category.php?id=484',
        'cat_id' => 
        array (
          551 => 
          array (
            'id' => '551',
            'name' => '安全座椅',
            'url' => 'category.php?id=551',
            'cat_id' => 
            array (
            ),
          ),
          552 => 
          array (
            'id' => '552',
            'name' => '提篮式',
            'url' => 'category.php?id=552',
            'cat_id' => 
            array (
            ),
          ),
          553 => 
          array (
            'id' => '553',
            'name' => '增高垫',
            'url' => 'category.php?id=553',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      487 => 
      array (
        'id' => '487',
        'name' => '寝居服饰',
        'url' => 'category.php?id=487',
        'cat_id' => 
        array (
          556 => 
          array (
            'id' => '556',
            'name' => '婴儿外出服',
            'url' => 'category.php?id=556',
            'cat_id' => 
            array (
            ),
          ),
          558 => 
          array (
            'id' => '558',
            'name' => '婴儿内衣',
            'url' => 'category.php?id=558',
            'cat_id' => 
            array (
            ),
          ),
          559 => 
          array (
            'id' => '559',
            'name' => '婴儿礼盒',
            'url' => 'category.php?id=559',
            'cat_id' => 
            array (
            ),
          ),
          561 => 
          array (
            'id' => '561',
            'name' => '婴儿鞋帽袜',
            'url' => 'category.php?id=561',
            'cat_id' => 
            array (
            ),
          ),
          563 => 
          array (
            'id' => '563',
            'name' => '家居床品',
            'url' => 'category.php?id=563',
            'cat_id' => 
            array (
            ),
          ),
          565 => 
          array (
            'id' => '565',
            'name' => '安全防护',
            'url' => 'category.php?id=565',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      489 => 
      array (
        'id' => '489',
        'name' => '妈妈专区',
        'url' => 'category.php?id=489',
        'cat_id' => 
        array (
          572 => 
          array (
            'id' => '572',
            'name' => '妈咪包/背婴带',
            'url' => 'category.php?id=572',
            'cat_id' => 
            array (
            ),
          ),
          573 => 
          array (
            'id' => '573',
            'name' => '待产/新生',
            'url' => 'category.php?id=573',
            'cat_id' => 
            array (
            ),
          ),
          574 => 
          array (
            'id' => '574',
            'name' => '产后塑身',
            'url' => 'category.php?id=574',
            'cat_id' => 
            array (
            ),
          ),
          575 => 
          array (
            'id' => '575',
            'name' => '文胸/内裤',
            'url' => 'category.php?id=575',
            'cat_id' => 
            array (
            ),
          ),
          576 => 
          array (
            'id' => '576',
            'name' => '防辐射服',
            'url' => 'category.php?id=576',
            'cat_id' => 
            array (
            ),
          ),
          577 => 
          array (
            'id' => '577',
            'name' => '孕妇装',
            'url' => 'category.php?id=577',
            'cat_id' => 
            array (
            ),
          ),
          578 => 
          array (
            'id' => '578',
            'name' => '月子装',
            'url' => 'category.php?id=578',
            'cat_id' => 
            array (
            ),
          ),
          579 => 
          array (
            'id' => '579',
            'name' => '孕期营养',
            'url' => 'category.php?id=579',
            'cat_id' => 
            array (
            ),
          ),
          581 => 
          array (
            'id' => '581',
            'name' => '孕妈美容',
            'url' => 'category.php?id=581',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      490 => 
      array (
        'id' => '490',
        'name' => '童装童鞋',
        'url' => 'category.php?id=490',
        'cat_id' => 
        array (
          584 => 
          array (
            'id' => '584',
            'name' => '套装',
            'url' => 'category.php?id=584',
            'cat_id' => 
            array (
            ),
          ),
          585 => 
          array (
            'id' => '585',
            'name' => '上衣',
            'url' => 'category.php?id=585',
            'cat_id' => 
            array (
            ),
          ),
          586 => 
          array (
            'id' => '586',
            'name' => '裤子',
            'url' => 'category.php?id=586',
            'cat_id' => 
            array (
            ),
          ),
          587 => 
          array (
            'id' => '587',
            'name' => '裙子',
            'url' => 'category.php?id=587',
            'cat_id' => 
            array (
            ),
          ),
          588 => 
          array (
            'id' => '588',
            'name' => '内衣',
            'url' => 'category.php?id=588',
            'cat_id' => 
            array (
            ),
          ),
          589 => 
          array (
            'id' => '589',
            'name' => '羽绒服/棉服',
            'url' => 'category.php?id=589',
            'cat_id' => 
            array (
            ),
          ),
          591 => 
          array (
            'id' => '591',
            'name' => '亲子装',
            'url' => 'category.php?id=591',
            'cat_id' => 
            array (
            ),
          ),
          593 => 
          array (
            'id' => '593',
            'name' => '配饰',
            'url' => 'category.php?id=593',
            'cat_id' => 
            array (
            ),
          ),
          595 => 
          array (
            'id' => '595',
            'name' => '演出服',
            'url' => 'category.php?id=595',
            'cat_id' => 
            array (
            ),
          ),
          596 => 
          array (
            'id' => '596',
            'name' => '运动服',
            'url' => 'category.php?id=596',
            'cat_id' => 
            array (
            ),
          ),
          597 => 
          array (
            'id' => '597',
            'name' => '运动鞋',
            'url' => 'category.php?id=597',
            'cat_id' => 
            array (
            ),
          ),
          598 => 
          array (
            'id' => '598',
            'name' => '皮鞋/帆布鞋',
            'url' => 'category.php?id=598',
            'cat_id' => 
            array (
            ),
          ),
          599 => 
          array (
            'id' => '599',
            'name' => '靴子',
            'url' => 'category.php?id=599',
            'cat_id' => 
            array (
            ),
          ),
          600 => 
          array (
            'id' => '600',
            'name' => '凉鞋',
            'url' => 'category.php?id=600',
            'cat_id' => 
            array (
            ),
          ),
          601 => 
          array (
            'id' => '601',
            'name' => '功能鞋',
            'url' => 'category.php?id=601',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  12 => 
  array (
    'brand_count' => 1,
    'name' => '食品、酒类、生鲜、特产',
    'oldname' => '食品、酒类、生鲜、特产',
    'url' => 'category.php?id=12',
    'cat_id' => 
    array (
      616 => 
      array (
        'id' => '616',
        'name' => '进口食品',
        'url' => 'category.php?id=616',
        'cat_id' => 
        array (
          636 => 
          array (
            'id' => '636',
            'name' => '冲调饮品',
            'url' => 'category.php?id=636',
            'cat_id' => 
            array (
            ),
          ),
          632 => 
          array (
            'id' => '632',
            'name' => '牛奶',
            'url' => 'category.php?id=632',
            'cat_id' => 
            array (
            ),
          ),
          633 => 
          array (
            'id' => '633',
            'name' => '饼干蛋糕',
            'url' => 'category.php?id=633',
            'cat_id' => 
            array (
            ),
          ),
          634 => 
          array (
            'id' => '634',
            'name' => '糖果/巧克力',
            'url' => 'category.php?id=634',
            'cat_id' => 
            array (
            ),
          ),
          635 => 
          array (
            'id' => '635',
            'name' => '休闲零食',
            'url' => 'category.php?id=635',
            'cat_id' => 
            array (
            ),
          ),
          637 => 
          array (
            'id' => '637',
            'name' => '粮油调味',
            'url' => 'category.php?id=637',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      623 => 
      array (
        'id' => '623',
        'name' => '生鲜食品',
        'url' => 'category.php?id=623',
        'cat_id' => 
        array (
          1415 => 
          array (
            'id' => '1415',
            'name' => '家禽',
            'url' => 'category.php?id=1415',
            'cat_id' => 
            array (
            ),
          ),
          1416 => 
          array (
            'id' => '1416',
            'name' => '牛羊肉',
            'url' => 'category.php?id=1416',
            'cat_id' => 
            array (
            ),
          ),
          1417 => 
          array (
            'id' => '1417',
            'name' => '蔬菜水果',
            'url' => 'category.php?id=1417',
            'cat_id' => 
            array (
            ),
          ),
          1418 => 
          array (
            'id' => '1418',
            'name' => '海鲜干货',
            'url' => 'category.php?id=1418',
            'cat_id' => 
            array (
            ),
          ),
          1419 => 
          array (
            'id' => '1419',
            'name' => '其他',
            'url' => 'category.php?id=1419',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      615 => 
      array (
        'id' => '615',
        'name' => '中外名酒',
        'url' => 'category.php?id=615',
        'cat_id' => 
        array (
          627 => 
          array (
            'id' => '627',
            'name' => '洋酒',
            'url' => 'category.php?id=627',
            'cat_id' => 
            array (
            ),
          ),
          625 => 
          array (
            'id' => '625',
            'name' => '白酒',
            'url' => 'category.php?id=625',
            'cat_id' => 
            array (
            ),
          ),
          626 => 
          array (
            'id' => '626',
            'name' => '葡萄酒',
            'url' => 'category.php?id=626',
            'cat_id' => 
            array (
            ),
          ),
          628 => 
          array (
            'id' => '628',
            'name' => '啤酒',
            'url' => 'category.php?id=628',
            'cat_id' => 
            array (
            ),
          ),
          629 => 
          array (
            'id' => '629',
            'name' => '黄酒/养生酒',
            'url' => 'category.php?id=629',
            'cat_id' => 
            array (
            ),
          ),
          631 => 
          array (
            'id' => '631',
            'name' => '收藏酒',
            'url' => 'category.php?id=631',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      620 => 
      array (
        'id' => '620',
        'name' => '茗茶',
        'url' => 'category.php?id=620',
        'cat_id' => 
        array (
          662 => 
          array (
            'id' => '662',
            'name' => '乌龙茶',
            'url' => 'category.php?id=662',
            'cat_id' => 
            array (
            ),
          ),
          656 => 
          array (
            'id' => '656',
            'name' => '铁观音',
            'url' => 'category.php?id=656',
            'cat_id' => 
            array (
            ),
          ),
          658 => 
          array (
            'id' => '658',
            'name' => '普洱',
            'url' => 'category.php?id=658',
            'cat_id' => 
            array (
            ),
          ),
          659 => 
          array (
            'id' => '659',
            'name' => '龙井',
            'url' => 'category.php?id=659',
            'cat_id' => 
            array (
            ),
          ),
          660 => 
          array (
            'id' => '660',
            'name' => '绿茶',
            'url' => 'category.php?id=660',
            'cat_id' => 
            array (
            ),
          ),
          661 => 
          array (
            'id' => '661',
            'name' => '红茶',
            'url' => 'category.php?id=661',
            'cat_id' => 
            array (
            ),
          ),
          667 => 
          array (
            'id' => '667',
            'name' => '花草茶',
            'url' => 'category.php?id=667',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      621 => 
      array (
        'id' => '621',
        'name' => '饮料冲调',
        'url' => 'category.php?id=621',
        'cat_id' => 
        array (
          1432 => 
          array (
            'id' => '1432',
            'name' => '牛奶乳品',
            'url' => 'category.php?id=1432',
            'cat_id' => 
            array (
            ),
          ),
          1433 => 
          array (
            'id' => '1433',
            'name' => '饮料',
            'url' => 'category.php?id=1433',
            'cat_id' => 
            array (
            ),
          ),
          1434 => 
          array (
            'id' => '1434',
            'name' => '饮用水',
            'url' => 'category.php?id=1434',
            'cat_id' => 
            array (
            ),
          ),
          1435 => 
          array (
            'id' => '1435',
            'name' => '咖啡/奶茶',
            'url' => 'category.php?id=1435',
            'cat_id' => 
            array (
            ),
          ),
          1436 => 
          array (
            'id' => '1436',
            'name' => '成人奶粉',
            'url' => 'category.php?id=1436',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      617 => 
      array (
        'id' => '617',
        'name' => '休闲食品',
        'url' => 'category.php?id=617',
        'cat_id' => 
        array (
          638 => 
          array (
            'id' => '638',
            'name' => '休闲零食',
            'url' => 'category.php?id=638',
            'cat_id' => 
            array (
            ),
          ),
          639 => 
          array (
            'id' => '639',
            'name' => '坚果炒货',
            'url' => 'category.php?id=639',
            'cat_id' => 
            array (
            ),
          ),
          640 => 
          array (
            'id' => '640',
            'name' => '肉干肉脯',
            'url' => 'category.php?id=640',
            'cat_id' => 
            array (
            ),
          ),
          642 => 
          array (
            'id' => '642',
            'name' => '蜜饯果干',
            'url' => 'category.php?id=642',
            'cat_id' => 
            array (
            ),
          ),
          643 => 
          array (
            'id' => '643',
            'name' => '糖果/巧克力',
            'url' => 'category.php?id=643',
            'cat_id' => 
            array (
            ),
          ),
          644 => 
          array (
            'id' => '644',
            'name' => '饼干蛋糕',
            'url' => 'category.php?id=644',
            'cat_id' => 
            array (
            ),
          ),
          645 => 
          array (
            'id' => '645',
            'name' => '无糖食品',
            'url' => 'category.php?id=645',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      624 => 
      array (
        'id' => '624',
        'name' => '食品礼券',
        'url' => 'category.php?id=624',
        'cat_id' => 
        array (
          1427 => 
          array (
            'id' => '1427',
            'name' => '瓜子',
            'url' => 'category.php?id=1427',
            'cat_id' => 
            array (
            ),
          ),
          1428 => 
          array (
            'id' => '1428',
            'name' => '小面包',
            'url' => 'category.php?id=1428',
            'cat_id' => 
            array (
            ),
          ),
          1429 => 
          array (
            'id' => '1429',
            'name' => '碧根果',
            'url' => 'category.php?id=1429',
            'cat_id' => 
            array (
            ),
          ),
          1430 => 
          array (
            'id' => '1430',
            'name' => '坚果',
            'url' => 'category.php?id=1430',
            'cat_id' => 
            array (
            ),
          ),
          1431 => 
          array (
            'id' => '1431',
            'name' => '卡券',
            'url' => 'category.php?id=1431',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
);
?>