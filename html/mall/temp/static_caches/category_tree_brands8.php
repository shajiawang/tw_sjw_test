<?php
$data = array (
  'ad_position' => 
  array (
    1 => 
    array (
      'ad_name' => 'cat_tree_8_1',
      'ad_code' => 'data/afficheimg/1490296765062943272.jpg',
      'ad_link' => 'https://www.dscmall.cn/',
      'link_man' => '',
      'ad_width' => '199',
      'ad_height' => '97',
      'link_color' => '',
      'b_title' => '',
      's_title' => '',
      'start_time' => '2017-03-24 11:19:07',
      'end_time' => '2021-04-30 13:46:56',
      'ad_type' => '0',
      'goods_name' => '0',
    ),
  ),
  'brands' => 
  array (
    0 => 
    array (
      'brand_id' => '204',
      'brand_name' => '金士顿',
      'brand_logo' => 'data/brandlogo/1490039286075654490.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=204',
      'selected' => 0,
    ),
    1 => 
    array (
      'brand_id' => '93',
      'brand_name' => '同庆和堂',
      'brand_logo' => 'data/brandlogo/1490072850306019115.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=93',
      'selected' => 0,
    ),
    2 => 
    array (
      'brand_id' => '84',
      'brand_name' => '同仁堂',
      'brand_logo' => 'data/brandlogo/1490072746651935979.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=84',
      'selected' => 0,
    ),
    3 => 
    array (
      'brand_id' => '85',
      'brand_name' => '喜瑞',
      'brand_logo' => 'data/brandlogo/1490072756032175204.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=85',
      'selected' => 0,
    ),
    4 => 
    array (
      'brand_id' => '87',
      'brand_name' => '汤臣倍健',
      'brand_logo' => 'data/brandlogo/1490072777790374054.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=87',
      'selected' => 0,
    ),
    5 => 
    array (
      'brand_id' => '88',
      'brand_name' => '养生堂',
      'brand_logo' => 'data/brandlogo/1490072787223453617.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=88',
      'selected' => 0,
    ),
    6 => 
    array (
      'brand_id' => '92',
      'brand_name' => '一品玉',
      'brand_logo' => 'data/brandlogo/1490072835176110718.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=92',
      'selected' => 0,
    ),
    7 => 
    array (
      'brand_id' => '115',
      'brand_name' => '西门子',
      'brand_logo' => 'data/brandlogo/1490074006660107941.jpg',
      'goods_num' => '2',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=115',
      'selected' => 0,
    ),
    8 => 
    array (
      'brand_id' => '116',
      'brand_name' => '伊莱克斯',
      'brand_logo' => 'data/brandlogo/1490073109529817869.jpg',
      'goods_num' => '2',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=116',
      'selected' => 0,
    ),
    9 => 
    array (
      'brand_id' => '124',
      'brand_name' => '亿健',
      'brand_logo' => 'data/brandlogo/1490073971637187496.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=124',
      'selected' => 0,
    ),
    10 => 
    array (
      'brand_id' => '130',
      'brand_name' => 'TP-LINL',
      'brand_logo' => 'data/brandlogo/1490074180745676140.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=130',
      'selected' => 0,
    ),
    11 => 
    array (
      'brand_id' => '131',
      'brand_name' => 'ZIPPO',
      'brand_logo' => 'data/brandlogo/1490073919711003101.jpg',
      'goods_num' => '1',
      'tag' => '1',
      'url' => 'category.php?id=8&amp;brand=131',
      'selected' => 0,
    ),
  ),
);
?>