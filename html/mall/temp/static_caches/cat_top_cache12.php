<?php
$data = array (
  616 => 
  array (
    'id' => '616',
    'name' => '进口食品',
    'url' => 'category.php?id=616',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      636 => 
      array (
        'id' => '636',
        'name' => '冲调饮品',
        'url' => 'category.php?id=636',
      ),
      632 => 
      array (
        'id' => '632',
        'name' => '牛奶',
        'url' => 'category.php?id=632',
      ),
      633 => 
      array (
        'id' => '633',
        'name' => '饼干蛋糕',
        'url' => 'category.php?id=633',
      ),
      634 => 
      array (
        'id' => '634',
        'name' => '糖果/巧克力',
        'url' => 'category.php?id=634',
      ),
      635 => 
      array (
        'id' => '635',
        'name' => '休闲零食',
        'url' => 'category.php?id=635',
      ),
      637 => 
      array (
        'id' => '637',
        'name' => '粮油调味',
        'url' => 'category.php?id=637',
      ),
    ),
  ),
  623 => 
  array (
    'id' => '623',
    'name' => '生鲜食品',
    'url' => 'category.php?id=623',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      1415 => 
      array (
        'id' => '1415',
        'name' => '家禽',
        'url' => 'category.php?id=1415',
      ),
      1416 => 
      array (
        'id' => '1416',
        'name' => '牛羊肉',
        'url' => 'category.php?id=1416',
      ),
      1417 => 
      array (
        'id' => '1417',
        'name' => '蔬菜水果',
        'url' => 'category.php?id=1417',
      ),
      1418 => 
      array (
        'id' => '1418',
        'name' => '海鲜干货',
        'url' => 'category.php?id=1418',
      ),
      1419 => 
      array (
        'id' => '1419',
        'name' => '其他',
        'url' => 'category.php?id=1419',
      ),
    ),
  ),
  615 => 
  array (
    'id' => '615',
    'name' => '中外名酒',
    'url' => 'category.php?id=615',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      627 => 
      array (
        'id' => '627',
        'name' => '洋酒',
        'url' => 'category.php?id=627',
      ),
      625 => 
      array (
        'id' => '625',
        'name' => '白酒',
        'url' => 'category.php?id=625',
      ),
      626 => 
      array (
        'id' => '626',
        'name' => '葡萄酒',
        'url' => 'category.php?id=626',
      ),
      628 => 
      array (
        'id' => '628',
        'name' => '啤酒',
        'url' => 'category.php?id=628',
      ),
      629 => 
      array (
        'id' => '629',
        'name' => '黄酒/养生酒',
        'url' => 'category.php?id=629',
      ),
      631 => 
      array (
        'id' => '631',
        'name' => '收藏酒',
        'url' => 'category.php?id=631',
      ),
    ),
  ),
  620 => 
  array (
    'id' => '620',
    'name' => '茗茶',
    'url' => 'category.php?id=620',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      662 => 
      array (
        'id' => '662',
        'name' => '乌龙茶',
        'url' => 'category.php?id=662',
      ),
      656 => 
      array (
        'id' => '656',
        'name' => '铁观音',
        'url' => 'category.php?id=656',
      ),
      658 => 
      array (
        'id' => '658',
        'name' => '普洱',
        'url' => 'category.php?id=658',
      ),
      659 => 
      array (
        'id' => '659',
        'name' => '龙井',
        'url' => 'category.php?id=659',
      ),
      660 => 
      array (
        'id' => '660',
        'name' => '绿茶',
        'url' => 'category.php?id=660',
      ),
      661 => 
      array (
        'id' => '661',
        'name' => '红茶',
        'url' => 'category.php?id=661',
      ),
      667 => 
      array (
        'id' => '667',
        'name' => '花草茶',
        'url' => 'category.php?id=667',
      ),
    ),
  ),
  621 => 
  array (
    'id' => '621',
    'name' => '饮料冲调',
    'url' => 'category.php?id=621',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      1432 => 
      array (
        'id' => '1432',
        'name' => '牛奶乳品',
        'url' => 'category.php?id=1432',
      ),
      1433 => 
      array (
        'id' => '1433',
        'name' => '饮料',
        'url' => 'category.php?id=1433',
      ),
      1434 => 
      array (
        'id' => '1434',
        'name' => '饮用水',
        'url' => 'category.php?id=1434',
      ),
      1435 => 
      array (
        'id' => '1435',
        'name' => '咖啡/奶茶',
        'url' => 'category.php?id=1435',
      ),
      1436 => 
      array (
        'id' => '1436',
        'name' => '成人奶粉',
        'url' => 'category.php?id=1436',
      ),
    ),
  ),
  617 => 
  array (
    'id' => '617',
    'name' => '休闲食品',
    'url' => 'category.php?id=617',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      638 => 
      array (
        'id' => '638',
        'name' => '休闲零食',
        'url' => 'category.php?id=638',
      ),
      639 => 
      array (
        'id' => '639',
        'name' => '坚果炒货',
        'url' => 'category.php?id=639',
      ),
      640 => 
      array (
        'id' => '640',
        'name' => '肉干肉脯',
        'url' => 'category.php?id=640',
      ),
      642 => 
      array (
        'id' => '642',
        'name' => '蜜饯果干',
        'url' => 'category.php?id=642',
      ),
      643 => 
      array (
        'id' => '643',
        'name' => '糖果/巧克力',
        'url' => 'category.php?id=643',
      ),
      644 => 
      array (
        'id' => '644',
        'name' => '饼干蛋糕',
        'url' => 'category.php?id=644',
      ),
      645 => 
      array (
        'id' => '645',
        'name' => '无糖食品',
        'url' => 'category.php?id=645',
      ),
    ),
  ),
  624 => 
  array (
    'id' => '624',
    'name' => '食品礼券',
    'url' => 'category.php?id=624',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      1427 => 
      array (
        'id' => '1427',
        'name' => '瓜子',
        'url' => 'category.php?id=1427',
      ),
      1428 => 
      array (
        'id' => '1428',
        'name' => '小面包',
        'url' => 'category.php?id=1428',
      ),
      1429 => 
      array (
        'id' => '1429',
        'name' => '碧根果',
        'url' => 'category.php?id=1429',
      ),
      1430 => 
      array (
        'id' => '1430',
        'name' => '坚果',
        'url' => 'category.php?id=1430',
      ),
      1431 => 
      array (
        'id' => '1431',
        'name' => '卡券',
        'url' => 'category.php?id=1431',
      ),
    ),
  ),
);
?>