<?php
$data = array (
  858 => 
  array (
    'cat_id' => '858',
    'cat_name' => '家用电器',
    'cat_alias_name' => '家用电器',
    'cat_icon' => 'images/cat_icon/14586287901326.png',
    'style_icon' => 'ele',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=858',
    'child_tree' => 
    array (
      1105 => 
      array (
        'id' => '1105',
        'name' => '大家电',
        'url' => 'category.php?id=1105',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1106 => 
          array (
            'id' => '1106',
            'name' => '平板电视',
            'url' => 'category.php?id=1106',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
              1475 => 
              array (
                'id' => '1475',
                'name' => '乐视电视',
                'url' => 'category.php?id=1475',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
            ),
          ),
          1107 => 
          array (
            'id' => '1107',
            'name' => '空调',
            'url' => 'category.php?id=1107',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1108 => 
          array (
            'id' => '1108',
            'name' => '冰箱',
            'url' => 'category.php?id=1108',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1109 => 
          array (
            'id' => '1109',
            'name' => '洗衣机',
            'url' => 'category.php?id=1109',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1110 => 
          array (
            'id' => '1110',
            'name' => '家庭影院',
            'url' => 'category.php?id=1110',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1111 => 
          array (
            'id' => '1111',
            'name' => 'DVD',
            'url' => 'category.php?id=1111',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1112 => 
          array (
            'id' => '1112',
            'name' => '迷你音响',
            'url' => 'category.php?id=1112',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1113 => 
          array (
            'id' => '1113',
            'name' => '热水器',
            'url' => 'category.php?id=1113',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1114 => 
          array (
            'id' => '1114',
            'name' => '冷吧/冰柜',
            'url' => 'category.php?id=1114',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1115 => 
      array (
        'id' => '1115',
        'name' => '生活电器',
        'url' => 'category.php?id=1115',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1116 => 
          array (
            'id' => '1116',
            'name' => '电风扇',
            'url' => 'category.php?id=1116',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1117 => 
          array (
            'id' => '1117',
            'name' => '冷风扇',
            'url' => 'category.php?id=1117',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1118 => 
          array (
            'id' => '1118',
            'name' => '净化器',
            'url' => 'category.php?id=1118',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1119 => 
          array (
            'id' => '1119',
            'name' => '加湿器',
            'url' => 'category.php?id=1119',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1120 => 
          array (
            'id' => '1120',
            'name' => '扫地机器人',
            'url' => 'category.php?id=1120',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1121 => 
          array (
            'id' => '1121',
            'name' => '吸尘器',
            'url' => 'category.php?id=1121',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1122 => 
          array (
            'id' => '1122',
            'name' => '插座',
            'url' => 'category.php?id=1122',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1123 => 
          array (
            'id' => '1123',
            'name' => '电话机',
            'url' => 'category.php?id=1123',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1124 => 
          array (
            'id' => '1124',
            'name' => '饮水机',
            'url' => 'category.php?id=1124',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1125 => 
          array (
            'id' => '1125',
            'name' => '取暖电器',
            'url' => 'category.php?id=1125',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1126 => 
          array (
            'id' => '1126',
            'name' => '净水设备',
            'url' => 'category.php?id=1126',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1127 => 
          array (
            'id' => '1127',
            'name' => '干衣机',
            'url' => 'category.php?id=1127',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1128 => 
          array (
            'id' => '1128',
            'name' => '收音机/录音机',
            'url' => 'category.php?id=1128',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1175 => 
          array (
            'id' => '1175',
            'name' => '电器开关',
            'url' => 'category.php?id=1175',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1129 => 
      array (
        'id' => '1129',
        'name' => '厨房电器',
        'url' => 'category.php?id=1129',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1131 => 
          array (
            'id' => '1131',
            'name' => '电饭煲',
            'url' => 'category.php?id=1131',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1130 => 
          array (
            'id' => '1130',
            'name' => '电压力锅',
            'url' => 'category.php?id=1130',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1132 => 
          array (
            'id' => '1132',
            'name' => '豆浆机',
            'url' => 'category.php?id=1132',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1133 => 
          array (
            'id' => '1133',
            'name' => '面包机',
            'url' => 'category.php?id=1133',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1134 => 
          array (
            'id' => '1134',
            'name' => '咖啡机',
            'url' => 'category.php?id=1134',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1135 => 
          array (
            'id' => '1135',
            'name' => '微波炉',
            'url' => 'category.php?id=1135',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1136 => 
          array (
            'id' => '1136',
            'name' => '料理/榨汁机',
            'url' => 'category.php?id=1136',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1137 => 
          array (
            'id' => '1137',
            'name' => '电烤箱',
            'url' => 'category.php?id=1137',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1138 => 
          array (
            'id' => '1138',
            'name' => '电磁炉',
            'url' => 'category.php?id=1138',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1139 => 
          array (
            'id' => '1139',
            'name' => '电饼铛/烧火盘',
            'url' => 'category.php?id=1139',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1140 => 
          array (
            'id' => '1140',
            'name' => '煮蛋器',
            'url' => 'category.php?id=1140',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1141 => 
          array (
            'id' => '1141',
            'name' => '酸奶机',
            'url' => 'category.php?id=1141',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1142 => 
          array (
            'id' => '1142',
            'name' => '电水壶/热水瓶',
            'url' => 'category.php?id=1142',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1143 => 
          array (
            'id' => '1143',
            'name' => '电饭盒',
            'url' => 'category.php?id=1143',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1144 => 
          array (
            'id' => '1144',
            'name' => '其他厨房电器',
            'url' => 'category.php?id=1144',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1145 => 
      array (
        'id' => '1145',
        'name' => '个护健康',
        'url' => 'category.php?id=1145',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1152 => 
          array (
            'id' => '1152',
            'name' => '按摩椅',
            'url' => 'category.php?id=1152',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1146 => 
          array (
            'id' => '1146',
            'name' => '剃须刀',
            'url' => 'category.php?id=1146',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1147 => 
          array (
            'id' => '1147',
            'name' => '脱毛器',
            'url' => 'category.php?id=1147',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1148 => 
          array (
            'id' => '1148',
            'name' => '口腔护理',
            'url' => 'category.php?id=1148',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1149 => 
          array (
            'id' => '1149',
            'name' => '电吹风',
            'url' => 'category.php?id=1149',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1150 => 
          array (
            'id' => '1150',
            'name' => '美容器',
            'url' => 'category.php?id=1150',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1151 => 
          array (
            'id' => '1151',
            'name' => '理发器',
            'url' => 'category.php?id=1151',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1153 => 
          array (
            'id' => '1153',
            'name' => '按摩器',
            'url' => 'category.php?id=1153',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1154 => 
          array (
            'id' => '1154',
            'name' => '足浴盆',
            'url' => 'category.php?id=1154',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1155 => 
          array (
            'id' => '1155',
            'name' => '血压计',
            'url' => 'category.php?id=1155',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1156 => 
          array (
            'id' => '1156',
            'name' => '健康秤/厨房秤',
            'url' => 'category.php?id=1156',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1157 => 
          array (
            'id' => '1157',
            'name' => '血糖计',
            'url' => 'category.php?id=1157',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1158 => 
          array (
            'id' => '1158',
            'name' => '计步器',
            'url' => 'category.php?id=1158',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1159 => 
          array (
            'id' => '1159',
            'name' => '其他健康电器',
            'url' => 'category.php?id=1159',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1160 => 
      array (
        'id' => '1160',
        'name' => '五金家装',
        'url' => 'category.php?id=1160',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1161 => 
          array (
            'id' => '1161',
            'name' => '电动工具',
            'url' => 'category.php?id=1161',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1162 => 
          array (
            'id' => '1162',
            'name' => '手动工具',
            'url' => 'category.php?id=1162',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1163 => 
          array (
            'id' => '1163',
            'name' => '仪器',
            'url' => 'category.php?id=1163',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1164 => 
          array (
            'id' => '1164',
            'name' => '仪表',
            'url' => 'category.php?id=1164',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1165 => 
          array (
            'id' => '1165',
            'name' => '浴霸/排气扇',
            'url' => 'category.php?id=1165',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1166 => 
          array (
            'id' => '1166',
            'name' => '灯具',
            'url' => 'category.php?id=1166',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1167 => 
          array (
            'id' => '1167',
            'name' => 'LED灯',
            'url' => 'category.php?id=1167',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1168 => 
          array (
            'id' => '1168',
            'name' => '洁身器',
            'url' => 'category.php?id=1168',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1169 => 
          array (
            'id' => '1169',
            'name' => '水槽',
            'url' => 'category.php?id=1169',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1170 => 
          array (
            'id' => '1170',
            'name' => '龙头',
            'url' => 'category.php?id=1170',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1171 => 
          array (
            'id' => '1171',
            'name' => '沐浴花洒',
            'url' => 'category.php?id=1171',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1172 => 
          array (
            'id' => '1172',
            'name' => '厨卫五金',
            'url' => 'category.php?id=1172',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1173 => 
          array (
            'id' => '1173',
            'name' => '家具五金',
            'url' => 'category.php?id=1173',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1174 => 
          array (
            'id' => '1174',
            'name' => '门铃',
            'url' => 'category.php?id=1174',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1176 => 
          array (
            'id' => '1176',
            'name' => '监控安防',
            'url' => 'category.php?id=1176',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  3 => 
  array (
    'cat_id' => '3',
    'cat_name' => '手机、数码、通信',
    'cat_alias_name' => '手机数码',
    'cat_icon' => '',
    'style_icon' => 'digital',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=3',
    'child_tree' => 
    array (
      112 => 
      array (
        'id' => '112',
        'name' => '智能设备',
        'url' => 'category.php?id=112',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          113 => 
          array (
            'id' => '113',
            'name' => '智能手环',
            'url' => 'category.php?id=113',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          114 => 
          array (
            'id' => '114',
            'name' => '智能手表',
            'url' => 'category.php?id=114',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          115 => 
          array (
            'id' => '115',
            'name' => '智能眼镜',
            'url' => 'category.php?id=115',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          116 => 
          array (
            'id' => '116',
            'name' => '运动跟踪器',
            'url' => 'category.php?id=116',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          117 => 
          array (
            'id' => '117',
            'name' => '健康监测',
            'url' => 'category.php?id=117',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          119 => 
          array (
            'id' => '119',
            'name' => '智能配饰',
            'url' => 'category.php?id=119',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          120 => 
          array (
            'id' => '120',
            'name' => '智能家居',
            'url' => 'category.php?id=120',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          122 => 
          array (
            'id' => '122',
            'name' => '体感车',
            'url' => 'category.php?id=122',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          124 => 
          array (
            'id' => '124',
            'name' => '其他配件',
            'url' => 'category.php?id=124',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      76 => 
      array (
        'id' => '76',
        'name' => '数码配件',
        'url' => 'category.php?id=76',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          81 => 
          array (
            'id' => '81',
            'name' => '存储卡',
            'url' => 'category.php?id=81',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          82 => 
          array (
            'id' => '82',
            'name' => '读卡器',
            'url' => 'category.php?id=82',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          84 => 
          array (
            'id' => '84',
            'name' => '滤镜',
            'url' => 'category.php?id=84',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          85 => 
          array (
            'id' => '85',
            'name' => '闪光灯/手柄',
            'url' => 'category.php?id=85',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          87 => 
          array (
            'id' => '87',
            'name' => '相机包',
            'url' => 'category.php?id=87',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          88 => 
          array (
            'id' => '88',
            'name' => '三脚架/云台',
            'url' => 'category.php?id=88',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          90 => 
          array (
            'id' => '90',
            'name' => '相机清洁',
            'url' => 'category.php?id=90',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          91 => 
          array (
            'id' => '91',
            'name' => '相机贴膜',
            'url' => 'category.php?id=91',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          93 => 
          array (
            'id' => '93',
            'name' => '机身附件',
            'url' => 'category.php?id=93',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          95 => 
          array (
            'id' => '95',
            'name' => '镜头附件',
            'url' => 'category.php?id=95',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          96 => 
          array (
            'id' => '96',
            'name' => '电池/充电器',
            'url' => 'category.php?id=96',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          97 => 
          array (
            'id' => '97',
            'name' => '移动电源',
            'url' => 'category.php?id=97',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      33 => 
      array (
        'id' => '33',
        'name' => '手机通讯',
        'url' => 'category.php?id=33',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          34 => 
          array (
            'id' => '34',
            'name' => '手机',
            'url' => 'category.php?id=34',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          35 => 
          array (
            'id' => '35',
            'name' => '对讲机',
            'url' => 'category.php?id=35',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      42 => 
      array (
        'id' => '42',
        'name' => '运营商',
        'url' => 'category.php?id=42',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          43 => 
          array (
            'id' => '43',
            'name' => '购机送费',
            'url' => 'category.php?id=43',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          44 => 
          array (
            'id' => '44',
            'name' => '0元购机',
            'url' => 'category.php?id=44',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          46 => 
          array (
            'id' => '46',
            'name' => '选号入网',
            'url' => 'category.php?id=46',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      47 => 
      array (
        'id' => '47',
        'name' => '手机配件',
        'url' => 'category.php?id=47',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          49 => 
          array (
            'id' => '49',
            'name' => '电池',
            'url' => 'category.php?id=49',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          51 => 
          array (
            'id' => '51',
            'name' => '蓝牙耳机',
            'url' => 'category.php?id=51',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          52 => 
          array (
            'id' => '52',
            'name' => '充电器/数据线',
            'url' => 'category.php?id=52',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          53 => 
          array (
            'id' => '53',
            'name' => '手机耳机',
            'url' => 'category.php?id=53',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          54 => 
          array (
            'id' => '54',
            'name' => '贴膜',
            'url' => 'category.php?id=54',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          55 => 
          array (
            'id' => '55',
            'name' => '存储卡',
            'url' => 'category.php?id=55',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          56 => 
          array (
            'id' => '56',
            'name' => '保护套',
            'url' => 'category.php?id=56',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          57 => 
          array (
            'id' => '57',
            'name' => '车载',
            'url' => 'category.php?id=57',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          59 => 
          array (
            'id' => '59',
            'name' => 'iPhone配件',
            'url' => 'category.php?id=59',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          60 => 
          array (
            'id' => '60',
            'name' => '创意配件',
            'url' => 'category.php?id=60',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          61 => 
          array (
            'id' => '61',
            'name' => '便携/无线音箱',
            'url' => 'category.php?id=61',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          62 => 
          array (
            'id' => '62',
            'name' => '手机饰品',
            'url' => 'category.php?id=62',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      64 => 
      array (
        'id' => '64',
        'name' => '摄影摄像',
        'url' => 'category.php?id=64',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          66 => 
          array (
            'id' => '66',
            'name' => '数码相机',
            'url' => 'category.php?id=66',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          68 => 
          array (
            'id' => '68',
            'name' => '单电/微单相机',
            'url' => 'category.php?id=68',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          69 => 
          array (
            'id' => '69',
            'name' => '单反相机',
            'url' => 'category.php?id=69',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          70 => 
          array (
            'id' => '70',
            'name' => '拍立得',
            'url' => 'category.php?id=70',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          71 => 
          array (
            'id' => '71',
            'name' => '运动相机',
            'url' => 'category.php?id=71',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          72 => 
          array (
            'id' => '72',
            'name' => '摄像机',
            'url' => 'category.php?id=72',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          73 => 
          array (
            'id' => '73',
            'name' => '镜头',
            'url' => 'category.php?id=73',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          74 => 
          array (
            'id' => '74',
            'name' => '户外器材',
            'url' => 'category.php?id=74',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          75 => 
          array (
            'id' => '75',
            'name' => '摄影器材',
            'url' => 'category.php?id=75',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      99 => 
      array (
        'id' => '99',
        'name' => '时尚影音',
        'url' => 'category.php?id=99',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          101 => 
          array (
            'id' => '101',
            'name' => '耳机/耳麦',
            'url' => 'category.php?id=101',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          102 => 
          array (
            'id' => '102',
            'name' => '音箱/音响',
            'url' => 'category.php?id=102',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          103 => 
          array (
            'id' => '103',
            'name' => '麦克风',
            'url' => 'category.php?id=103',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          105 => 
          array (
            'id' => '105',
            'name' => 'MP3/MP4',
            'url' => 'category.php?id=105',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          106 => 
          array (
            'id' => '106',
            'name' => '数码相框',
            'url' => 'category.php?id=106',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          108 => 
          array (
            'id' => '108',
            'name' => '专业音频',
            'url' => 'category.php?id=108',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          111 => 
          array (
            'id' => '111',
            'name' => '苹果周边',
            'url' => 'category.php?id=111',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  4 => 
  array (
    'cat_id' => '4',
    'cat_name' => '电脑、办公',
    'cat_alias_name' => '电脑办公',
    'cat_icon' => '',
    'style_icon' => 'computer',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=4',
    'child_tree' => 
    array (
      158 => 
      array (
        'id' => '158',
        'name' => '服务产品',
        'url' => 'category.php?id=158',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          345 => 
          array (
            'id' => '345',
            'name' => '上门服务',
            'url' => 'category.php?id=345',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          346 => 
          array (
            'id' => '346',
            'name' => '远程服务',
            'url' => 'category.php?id=346',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          348 => 
          array (
            'id' => '348',
            'name' => '电脑软件',
            'url' => 'category.php?id=348',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      132 => 
      array (
        'id' => '132',
        'name' => '电脑整机',
        'url' => 'category.php?id=132',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          168 => 
          array (
            'id' => '168',
            'name' => '笔记本',
            'url' => 'category.php?id=168',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          171 => 
          array (
            'id' => '171',
            'name' => '超级本',
            'url' => 'category.php?id=171',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          174 => 
          array (
            'id' => '174',
            'name' => '游戏本',
            'url' => 'category.php?id=174',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          178 => 
          array (
            'id' => '178',
            'name' => '平板电脑',
            'url' => 'category.php?id=178',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          181 => 
          array (
            'id' => '181',
            'name' => '平板电脑配件',
            'url' => 'category.php?id=181',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          186 => 
          array (
            'id' => '186',
            'name' => '台式机',
            'url' => 'category.php?id=186',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          203 => 
          array (
            'id' => '203',
            'name' => '笔记本配件',
            'url' => 'category.php?id=203',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          204 => 
          array (
            'id' => '204',
            'name' => '服务器/工作站',
            'url' => 'category.php?id=204',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      141 => 
      array (
        'id' => '141',
        'name' => '电脑配件',
        'url' => 'category.php?id=141',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          208 => 
          array (
            'id' => '208',
            'name' => 'CPU',
            'url' => 'category.php?id=208',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          213 => 
          array (
            'id' => '213',
            'name' => '主板',
            'url' => 'category.php?id=213',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          215 => 
          array (
            'id' => '215',
            'name' => '显卡',
            'url' => 'category.php?id=215',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          216 => 
          array (
            'id' => '216',
            'name' => '硬盘',
            'url' => 'category.php?id=216',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          218 => 
          array (
            'id' => '218',
            'name' => 'SSD固态硬盘',
            'url' => 'category.php?id=218',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          220 => 
          array (
            'id' => '220',
            'name' => '内存',
            'url' => 'category.php?id=220',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          222 => 
          array (
            'id' => '222',
            'name' => '机箱',
            'url' => 'category.php?id=222',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          224 => 
          array (
            'id' => '224',
            'name' => '电源',
            'url' => 'category.php?id=224',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          225 => 
          array (
            'id' => '225',
            'name' => '显示器',
            'url' => 'category.php?id=225',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          228 => 
          array (
            'id' => '228',
            'name' => '刻录机/光驱',
            'url' => 'category.php?id=228',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          234 => 
          array (
            'id' => '234',
            'name' => '声卡、扩展卡',
            'url' => 'category.php?id=234',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          236 => 
          array (
            'id' => '236',
            'name' => '散热器',
            'url' => 'category.php?id=236',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          239 => 
          array (
            'id' => '239',
            'name' => '装机配件',
            'url' => 'category.php?id=239',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          241 => 
          array (
            'id' => '241',
            'name' => '组装电脑',
            'url' => 'category.php?id=241',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      144 => 
      array (
        'id' => '144',
        'name' => '外设产品',
        'url' => 'category.php?id=144',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          245 => 
          array (
            'id' => '245',
            'name' => '鼠标',
            'url' => 'category.php?id=245',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          247 => 
          array (
            'id' => '247',
            'name' => '键盘',
            'url' => 'category.php?id=247',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          249 => 
          array (
            'id' => '249',
            'name' => '游戏设备',
            'url' => 'category.php?id=249',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          251 => 
          array (
            'id' => '251',
            'name' => 'U盘',
            'url' => 'category.php?id=251',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          253 => 
          array (
            'id' => '253',
            'name' => '移动硬盘',
            'url' => 'category.php?id=253',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          254 => 
          array (
            'id' => '254',
            'name' => '鼠标垫',
            'url' => 'category.php?id=254',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          256 => 
          array (
            'id' => '256',
            'name' => '摄像头',
            'url' => 'category.php?id=256',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          258 => 
          array (
            'id' => '258',
            'name' => '线缆',
            'url' => 'category.php?id=258',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          260 => 
          array (
            'id' => '260',
            'name' => '电玩',
            'url' => 'category.php?id=260',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          262 => 
          array (
            'id' => '262',
            'name' => '手写板',
            'url' => 'category.php?id=262',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          265 => 
          array (
            'id' => '265',
            'name' => '外置盒',
            'url' => 'category.php?id=265',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          267 => 
          array (
            'id' => '267',
            'name' => '电脑工具',
            'url' => 'category.php?id=267',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          268 => 
          array (
            'id' => '268',
            'name' => '电脑清洁',
            'url' => 'category.php?id=268',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          269 => 
          array (
            'id' => '269',
            'name' => 'UPS',
            'url' => 'category.php?id=269',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          270 => 
          array (
            'id' => '270',
            'name' => '插座',
            'url' => 'category.php?id=270',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      148 => 
      array (
        'id' => '148',
        'name' => '网络产品',
        'url' => 'category.php?id=148',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          272 => 
          array (
            'id' => '272',
            'name' => '路由器',
            'url' => 'category.php?id=272',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          274 => 
          array (
            'id' => '274',
            'name' => '网卡',
            'url' => 'category.php?id=274',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          275 => 
          array (
            'id' => '275',
            'name' => '交换机',
            'url' => 'category.php?id=275',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          278 => 
          array (
            'id' => '278',
            'name' => '网络存储',
            'url' => 'category.php?id=278',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          281 => 
          array (
            'id' => '281',
            'name' => '4G/3G上网',
            'url' => 'category.php?id=281',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          282 => 
          array (
            'id' => '282',
            'name' => '网络盒子',
            'url' => 'category.php?id=282',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          284 => 
          array (
            'id' => '284',
            'name' => '网络配件',
            'url' => 'category.php?id=284',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      157 => 
      array (
        'id' => '157',
        'name' => '办公打印',
        'url' => 'category.php?id=157',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          291 => 
          array (
            'id' => '291',
            'name' => '打印机',
            'url' => 'category.php?id=291',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          292 => 
          array (
            'id' => '292',
            'name' => '一体机',
            'url' => 'category.php?id=292',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          294 => 
          array (
            'id' => '294',
            'name' => '投影机',
            'url' => 'category.php?id=294',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          296 => 
          array (
            'id' => '296',
            'name' => '投影配件',
            'url' => 'category.php?id=296',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          298 => 
          array (
            'id' => '298',
            'name' => '传真机',
            'url' => 'category.php?id=298',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          299 => 
          array (
            'id' => '299',
            'name' => '复合机',
            'url' => 'category.php?id=299',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          300 => 
          array (
            'id' => '300',
            'name' => '碎纸机',
            'url' => 'category.php?id=300',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          301 => 
          array (
            'id' => '301',
            'name' => '扫描仪',
            'url' => 'category.php?id=301',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          302 => 
          array (
            'id' => '302',
            'name' => '墨盒',
            'url' => 'category.php?id=302',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          307 => 
          array (
            'id' => '307',
            'name' => '硒鼓',
            'url' => 'category.php?id=307',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          309 => 
          array (
            'id' => '309',
            'name' => '墨粉',
            'url' => 'category.php?id=309',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          310 => 
          array (
            'id' => '310',
            'name' => '色带',
            'url' => 'category.php?id=310',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  5 => 
  array (
    'cat_id' => '5',
    'cat_name' => '家居、家具、家装、厨具',
    'cat_alias_name' => '家居家纺',
    'cat_icon' => '',
    'style_icon' => 'bed',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=5',
    'child_tree' => 
    array (
      143 => 
      array (
        'id' => '143',
        'name' => '厨具',
        'url' => 'category.php?id=143',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          146 => 
          array (
            'id' => '146',
            'name' => '烹饪锅具',
            'url' => 'category.php?id=146',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          149 => 
          array (
            'id' => '149',
            'name' => '刀剪菜板',
            'url' => 'category.php?id=149',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          152 => 
          array (
            'id' => '152',
            'name' => '厨房配件',
            'url' => 'category.php?id=152',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          153 => 
          array (
            'id' => '153',
            'name' => '水具酒具',
            'url' => 'category.php?id=153',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          154 => 
          array (
            'id' => '154',
            'name' => '餐具',
            'url' => 'category.php?id=154',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          156 => 
          array (
            'id' => '156',
            'name' => '茶具/咖啡具',
            'url' => 'category.php?id=156',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      159 => 
      array (
        'id' => '159',
        'name' => '家装建材',
        'url' => 'category.php?id=159',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          160 => 
          array (
            'id' => '160',
            'name' => '灯饰照明',
            'url' => 'category.php?id=160',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          161 => 
          array (
            'id' => '161',
            'name' => '厨房卫浴',
            'url' => 'category.php?id=161',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          163 => 
          array (
            'id' => '163',
            'name' => '五金工具',
            'url' => 'category.php?id=163',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          165 => 
          array (
            'id' => '165',
            'name' => '电工电料',
            'url' => 'category.php?id=165',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          167 => 
          array (
            'id' => '167',
            'name' => '墙地面材料',
            'url' => 'category.php?id=167',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          169 => 
          array (
            'id' => '169',
            'name' => '装饰材料',
            'url' => 'category.php?id=169',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          172 => 
          array (
            'id' => '172',
            'name' => '装修服务',
            'url' => 'category.php?id=172',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          175 => 
          array (
            'id' => '175',
            'name' => '吸顶灯',
            'url' => 'category.php?id=175',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          176 => 
          array (
            'id' => '176',
            'name' => '淋浴花洒',
            'url' => 'category.php?id=176',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          179 => 
          array (
            'id' => '179',
            'name' => '开关插座',
            'url' => 'category.php?id=179',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          182 => 
          array (
            'id' => '182',
            'name' => '油漆涂料',
            'url' => 'category.php?id=182',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          184 => 
          array (
            'id' => '184',
            'name' => '龙头',
            'url' => 'category.php?id=184',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      188 => 
      array (
        'id' => '188',
        'name' => '家纺',
        'url' => 'category.php?id=188',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          190 => 
          array (
            'id' => '190',
            'name' => '床品套件',
            'url' => 'category.php?id=190',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          191 => 
          array (
            'id' => '191',
            'name' => '被子',
            'url' => 'category.php?id=191',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          192 => 
          array (
            'id' => '192',
            'name' => '蚊帐',
            'url' => 'category.php?id=192',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          193 => 
          array (
            'id' => '193',
            'name' => '凉席',
            'url' => 'category.php?id=193',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          194 => 
          array (
            'id' => '194',
            'name' => '床单被罩',
            'url' => 'category.php?id=194',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          195 => 
          array (
            'id' => '195',
            'name' => '枕芯',
            'url' => 'category.php?id=195',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          196 => 
          array (
            'id' => '196',
            'name' => '毛巾浴巾',
            'url' => 'category.php?id=196',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          197 => 
          array (
            'id' => '197',
            'name' => '布艺软饰',
            'url' => 'category.php?id=197',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          198 => 
          array (
            'id' => '198',
            'name' => '毯子',
            'url' => 'category.php?id=198',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          199 => 
          array (
            'id' => '199',
            'name' => '抱枕靠垫',
            'url' => 'category.php?id=199',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          200 => 
          array (
            'id' => '200',
            'name' => '床垫/床褥',
            'url' => 'category.php?id=200',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          201 => 
          array (
            'id' => '201',
            'name' => '窗帘/窗纱',
            'url' => 'category.php?id=201',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          202 => 
          array (
            'id' => '202',
            'name' => '电热毯',
            'url' => 'category.php?id=202',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      205 => 
      array (
        'id' => '205',
        'name' => '家具',
        'url' => 'category.php?id=205',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          206 => 
          array (
            'id' => '206',
            'name' => '卧室家具',
            'url' => 'category.php?id=206',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          207 => 
          array (
            'id' => '207',
            'name' => '客厅家具',
            'url' => 'category.php?id=207',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          209 => 
          array (
            'id' => '209',
            'name' => '餐厅家具',
            'url' => 'category.php?id=209',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          210 => 
          array (
            'id' => '210',
            'name' => '书房家具',
            'url' => 'category.php?id=210',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          211 => 
          array (
            'id' => '211',
            'name' => '储物家具',
            'url' => 'category.php?id=211',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          212 => 
          array (
            'id' => '212',
            'name' => '阳台/户外',
            'url' => 'category.php?id=212',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          214 => 
          array (
            'id' => '214',
            'name' => '商业办公',
            'url' => 'category.php?id=214',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          217 => 
          array (
            'id' => '217',
            'name' => '床',
            'url' => 'category.php?id=217',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          219 => 
          array (
            'id' => '219',
            'name' => '床垫',
            'url' => 'category.php?id=219',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          221 => 
          array (
            'id' => '221',
            'name' => '沙发',
            'url' => 'category.php?id=221',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          223 => 
          array (
            'id' => '223',
            'name' => '电脑椅',
            'url' => 'category.php?id=223',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          226 => 
          array (
            'id' => '226',
            'name' => '衣柜',
            'url' => 'category.php?id=226',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          227 => 
          array (
            'id' => '227',
            'name' => '茶几',
            'url' => 'category.php?id=227',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          229 => 
          array (
            'id' => '229',
            'name' => '电视柜',
            'url' => 'category.php?id=229',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          230 => 
          array (
            'id' => '230',
            'name' => '餐桌',
            'url' => 'category.php?id=230',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          231 => 
          array (
            'id' => '231',
            'name' => '电脑桌',
            'url' => 'category.php?id=231',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          232 => 
          array (
            'id' => '232',
            'name' => '鞋架/衣帽架',
            'url' => 'category.php?id=232',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      233 => 
      array (
        'id' => '233',
        'name' => '灯具',
        'url' => 'category.php?id=233',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          235 => 
          array (
            'id' => '235',
            'name' => '台灯',
            'url' => 'category.php?id=235',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          237 => 
          array (
            'id' => '237',
            'name' => '吸顶灯',
            'url' => 'category.php?id=237',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          238 => 
          array (
            'id' => '238',
            'name' => '筒灯射灯',
            'url' => 'category.php?id=238',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          240 => 
          array (
            'id' => '240',
            'name' => 'LED灯',
            'url' => 'category.php?id=240',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          242 => 
          array (
            'id' => '242',
            'name' => '节能灯',
            'url' => 'category.php?id=242',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          243 => 
          array (
            'id' => '243',
            'name' => '落地灯',
            'url' => 'category.php?id=243',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          244 => 
          array (
            'id' => '244',
            'name' => '五金电器',
            'url' => 'category.php?id=244',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          246 => 
          array (
            'id' => '246',
            'name' => '应急灯/手电',
            'url' => 'category.php?id=246',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          248 => 
          array (
            'id' => '248',
            'name' => '装饰灯',
            'url' => 'category.php?id=248',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          250 => 
          array (
            'id' => '250',
            'name' => '吊灯',
            'url' => 'category.php?id=250',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          252 => 
          array (
            'id' => '252',
            'name' => '氛围照明',
            'url' => 'category.php?id=252',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      255 => 
      array (
        'id' => '255',
        'name' => '生活日用',
        'url' => 'category.php?id=255',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          257 => 
          array (
            'id' => '257',
            'name' => '收纳用品',
            'url' => 'category.php?id=257',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          259 => 
          array (
            'id' => '259',
            'name' => '雨伞雨具',
            'url' => 'category.php?id=259',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          261 => 
          array (
            'id' => '261',
            'name' => '浴室用品',
            'url' => 'category.php?id=261',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          263 => 
          array (
            'id' => '263',
            'name' => '缝纫/针织用品',
            'url' => 'category.php?id=263',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          264 => 
          array (
            'id' => '264',
            'name' => '洗晒/熨烫',
            'url' => 'category.php?id=264',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          266 => 
          array (
            'id' => '266',
            'name' => '净化除味',
            'url' => 'category.php?id=266',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  6 => 
  array (
    'cat_id' => '6',
    'cat_name' => '男装、女装、内衣',
    'cat_alias_name' => '男装女装',
    'cat_icon' => 'images/cat_icon/14586287606671.png',
    'style_icon' => 'clothes',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=6',
    'child_tree' => 
    array (
      347 => 
      array (
        'id' => '347',
        'name' => '女装',
        'url' => 'category.php?id=347',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          349 => 
          array (
            'id' => '349',
            'name' => '连衣裙',
            'url' => 'category.php?id=349',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          350 => 
          array (
            'id' => '350',
            'name' => '蕾丝/雪纺衫',
            'url' => 'category.php?id=350',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          351 => 
          array (
            'id' => '351',
            'name' => '衬衫',
            'url' => 'category.php?id=351',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          352 => 
          array (
            'id' => '352',
            'name' => 'T恤',
            'url' => 'category.php?id=352',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          354 => 
          array (
            'id' => '354',
            'name' => '半身裙',
            'url' => 'category.php?id=354',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          356 => 
          array (
            'id' => '356',
            'name' => '休闲裤',
            'url' => 'category.php?id=356',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          358 => 
          array (
            'id' => '358',
            'name' => '短裤',
            'url' => 'category.php?id=358',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          361 => 
          array (
            'id' => '361',
            'name' => '牛仔裤',
            'url' => 'category.php?id=361',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          363 => 
          array (
            'id' => '363',
            'name' => '针织衫',
            'url' => 'category.php?id=363',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          365 => 
          array (
            'id' => '365',
            'name' => '吊带/背心',
            'url' => 'category.php?id=365',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          367 => 
          array (
            'id' => '367',
            'name' => '打底衫',
            'url' => 'category.php?id=367',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          369 => 
          array (
            'id' => '369',
            'name' => '打底裤',
            'url' => 'category.php?id=369',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          370 => 
          array (
            'id' => '370',
            'name' => '正装裤',
            'url' => 'category.php?id=370',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          372 => 
          array (
            'id' => '372',
            'name' => '小西服',
            'url' => 'category.php?id=372',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          374 => 
          array (
            'id' => '374',
            'name' => '马甲',
            'url' => 'category.php?id=374',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          377 => 
          array (
            'id' => '377',
            'name' => '风衣',
            'url' => 'category.php?id=377',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          379 => 
          array (
            'id' => '379',
            'name' => '羊毛衫',
            'url' => 'category.php?id=379',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          381 => 
          array (
            'id' => '381',
            'name' => '羊绒衫',
            'url' => 'category.php?id=381',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          383 => 
          array (
            'id' => '383',
            'name' => '短外套',
            'url' => 'category.php?id=383',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          385 => 
          array (
            'id' => '385',
            'name' => '棉服',
            'url' => 'category.php?id=385',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          388 => 
          array (
            'id' => '388',
            'name' => '毛呢大衣',
            'url' => 'category.php?id=388',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          390 => 
          array (
            'id' => '390',
            'name' => '加绒裤',
            'url' => 'category.php?id=390',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          395 => 
          array (
            'id' => '395',
            'name' => '羽绒服',
            'url' => 'category.php?id=395',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          400 => 
          array (
            'id' => '400',
            'name' => '皮草',
            'url' => 'category.php?id=400',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          429 => 
          array (
            'id' => '429',
            'name' => '真皮皮衣',
            'url' => 'category.php?id=429',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          431 => 
          array (
            'id' => '431',
            'name' => '仿皮皮衣',
            'url' => 'category.php?id=431',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          444 => 
          array (
            'id' => '444',
            'name' => '旗袍/唐装',
            'url' => 'category.php?id=444',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          448 => 
          array (
            'id' => '448',
            'name' => '礼服',
            'url' => 'category.php?id=448',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          451 => 
          array (
            'id' => '451',
            'name' => '婚纱',
            'url' => 'category.php?id=451',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          454 => 
          array (
            'id' => '454',
            'name' => '中老年女装',
            'url' => 'category.php?id=454',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          455 => 
          array (
            'id' => '455',
            'name' => '大码女装',
            'url' => 'category.php?id=455',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      463 => 
      array (
        'id' => '463',
        'name' => '男装',
        'url' => 'category.php?id=463',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          473 => 
          array (
            'id' => '473',
            'name' => 'T恤',
            'url' => 'category.php?id=473',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          466 => 
          array (
            'id' => '466',
            'name' => '裤子',
            'url' => 'category.php?id=466',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          474 => 
          array (
            'id' => '474',
            'name' => 'POLO衫',
            'url' => 'category.php?id=474',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          475 => 
          array (
            'id' => '475',
            'name' => '针织衫',
            'url' => 'category.php?id=475',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          478 => 
          array (
            'id' => '478',
            'name' => '夹克',
            'url' => 'category.php?id=478',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          480 => 
          array (
            'id' => '480',
            'name' => '卫衣',
            'url' => 'category.php?id=480',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          485 => 
          array (
            'id' => '485',
            'name' => '风衣',
            'url' => 'category.php?id=485',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          486 => 
          array (
            'id' => '486',
            'name' => '马甲/背心',
            'url' => 'category.php?id=486',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          488 => 
          array (
            'id' => '488',
            'name' => '短裤',
            'url' => 'category.php?id=488',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          494 => 
          array (
            'id' => '494',
            'name' => '休闲裤',
            'url' => 'category.php?id=494',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          498 => 
          array (
            'id' => '498',
            'name' => '牛仔裤',
            'url' => 'category.php?id=498',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          502 => 
          array (
            'id' => '502',
            'name' => '西服',
            'url' => 'category.php?id=502',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          504 => 
          array (
            'id' => '504',
            'name' => '西裤',
            'url' => 'category.php?id=504',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          505 => 
          array (
            'id' => '505',
            'name' => '西服套装',
            'url' => 'category.php?id=505',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          507 => 
          array (
            'id' => '507',
            'name' => '真皮皮衣',
            'url' => 'category.php?id=507',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          508 => 
          array (
            'id' => '508',
            'name' => '仿皮皮衣',
            'url' => 'category.php?id=508',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          510 => 
          array (
            'id' => '510',
            'name' => '羽绒服',
            'url' => 'category.php?id=510',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          512 => 
          array (
            'id' => '512',
            'name' => '毛呢大衣',
            'url' => 'category.php?id=512',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          513 => 
          array (
            'id' => '513',
            'name' => '棉服',
            'url' => 'category.php?id=513',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          516 => 
          array (
            'id' => '516',
            'name' => '羊绒衫',
            'url' => 'category.php?id=516',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          517 => 
          array (
            'id' => '517',
            'name' => '羊毛衫',
            'url' => 'category.php?id=517',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          520 => 
          array (
            'id' => '520',
            'name' => '卫裤/运动裤',
            'url' => 'category.php?id=520',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      630 => 
      array (
        'id' => '630',
        'name' => '服饰配件',
        'url' => 'category.php?id=630',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          641 => 
          array (
            'id' => '641',
            'name' => '配饰',
            'url' => 'category.php?id=641',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          657 => 
          array (
            'id' => '657',
            'name' => '光学镜架/镜片',
            'url' => 'category.php?id=657',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          663 => 
          array (
            'id' => '663',
            'name' => '防辐射眼镜',
            'url' => 'category.php?id=663',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          664 => 
          array (
            'id' => '664',
            'name' => '女士丝巾/围巾/披肩',
            'url' => 'category.php?id=664',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          665 => 
          array (
            'id' => '665',
            'name' => '棒球帽',
            'url' => 'category.php?id=665',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          666 => 
          array (
            'id' => '666',
            'name' => '遮阳伞/雨伞',
            'url' => 'category.php?id=666',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          668 => 
          array (
            'id' => '668',
            'name' => '遮阳帽',
            'url' => 'category.php?id=668',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          669 => 
          array (
            'id' => '669',
            'name' => '领带/领结/领带夹',
            'url' => 'category.php?id=669',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          670 => 
          array (
            'id' => '670',
            'name' => '男士腰带/礼盒',
            'url' => 'category.php?id=670',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          671 => 
          array (
            'id' => '671',
            'name' => '防晒手套',
            'url' => 'category.php?id=671',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          672 => 
          array (
            'id' => '672',
            'name' => '老花镜',
            'url' => 'category.php?id=672',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          673 => 
          array (
            'id' => '673',
            'name' => '袖扣',
            'url' => 'category.php?id=673',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          674 => 
          array (
            'id' => '674',
            'name' => '鸭舌帽',
            'url' => 'category.php?id=674',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          675 => 
          array (
            'id' => '675',
            'name' => '装饰眼镜',
            'url' => 'category.php?id=675',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          676 => 
          array (
            'id' => '676',
            'name' => '女士腰带/礼盒',
            'url' => 'category.php?id=676',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      547 => 
      array (
        'id' => '547',
        'name' => '内衣',
        'url' => 'category.php?id=547',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          592 => 
          array (
            'id' => '592',
            'name' => '内衣配件',
            'url' => 'category.php?id=592',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          550 => 
          array (
            'id' => '550',
            'name' => '文胸',
            'url' => 'category.php?id=550',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          554 => 
          array (
            'id' => '554',
            'name' => '睡衣/家居服',
            'url' => 'category.php?id=554',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          555 => 
          array (
            'id' => '555',
            'name' => '情侣睡衣',
            'url' => 'category.php?id=555',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          557 => 
          array (
            'id' => '557',
            'name' => '文胸套装',
            'url' => 'category.php?id=557',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          560 => 
          array (
            'id' => '560',
            'name' => '少女文胸',
            'url' => 'category.php?id=560',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          562 => 
          array (
            'id' => '562',
            'name' => '女式内裤',
            'url' => 'category.php?id=562',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          564 => 
          array (
            'id' => '564',
            'name' => '男式内裤',
            'url' => 'category.php?id=564',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          566 => 
          array (
            'id' => '566',
            'name' => '商务男袜',
            'url' => 'category.php?id=566',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          567 => 
          array (
            'id' => '567',
            'name' => '休闲棉袜',
            'url' => 'category.php?id=567',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          568 => 
          array (
            'id' => '568',
            'name' => '吊带/背心',
            'url' => 'category.php?id=568',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          569 => 
          array (
            'id' => '569',
            'name' => '打底衫',
            'url' => 'category.php?id=569',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          570 => 
          array (
            'id' => '570',
            'name' => '抹胸',
            'url' => 'category.php?id=570',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          571 => 
          array (
            'id' => '571',
            'name' => '连裤袜/丝袜',
            'url' => 'category.php?id=571',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          580 => 
          array (
            'id' => '580',
            'name' => '美腿袜',
            'url' => 'category.php?id=580',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          582 => 
          array (
            'id' => '582',
            'name' => '打底裤袜',
            'url' => 'category.php?id=582',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          583 => 
          array (
            'id' => '583',
            'name' => '塑身美体',
            'url' => 'category.php?id=583',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          590 => 
          array (
            'id' => '590',
            'name' => '大码内衣',
            'url' => 'category.php?id=590',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1442 => 
      array (
        'id' => '1442',
        'name' => '运动户外',
        'url' => 'category.php?id=1442',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1443 => 
          array (
            'id' => '1443',
            'name' => '跑步运动',
            'url' => 'category.php?id=1443',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1444 => 
          array (
            'id' => '1444',
            'name' => '室内健身',
            'url' => 'category.php?id=1444',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1445 => 
          array (
            'id' => '1445',
            'name' => '自行车运动',
            'url' => 'category.php?id=1445',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1446 => 
          array (
            'id' => '1446',
            'name' => '轮滑运动',
            'url' => 'category.php?id=1446',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1447 => 
          array (
            'id' => '1447',
            'name' => '羽毛球/网球/乒乓球',
            'url' => 'category.php?id=1447',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1448 => 
          array (
            'id' => '1448',
            'name' => '足球/篮球/排球',
            'url' => 'category.php?id=1448',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1449 => 
          array (
            'id' => '1449',
            'name' => '运动休闲',
            'url' => 'category.php?id=1449',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1450 => 
          array (
            'id' => '1450',
            'name' => '钓鱼用品',
            'url' => 'category.php?id=1450',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1451 => 
          array (
            'id' => '1451',
            'name' => '野营烧烤',
            'url' => 'category.php?id=1451',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1452 => 
          array (
            'id' => '1452',
            'name' => '游泳运动',
            'url' => 'category.php?id=1452',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1453 => 
          array (
            'id' => '1453',
            'name' => '舞蹈运动',
            'url' => 'category.php?id=1453',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1454 => 
          array (
            'id' => '1454',
            'name' => '瑜伽运动',
            'url' => 'category.php?id=1454',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1455 => 
          array (
            'id' => '1455',
            'name' => '防狼防身',
            'url' => 'category.php?id=1455',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1456 => 
          array (
            'id' => '1456',
            'name' => '水上运动',
            'url' => 'category.php?id=1456',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  8 => 
  array (
    'cat_id' => '8',
    'cat_name' => '鞋靴、箱包、钟表、奢侈品',
    'cat_alias_name' => '鞋靴箱包',
    'cat_icon' => '',
    'style_icon' => 'shoes',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=8',
    'child_tree' => 
    array (
      362 => 
      array (
        'id' => '362',
        'name' => '奢侈品',
        'url' => 'category.php?id=362',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          443 => 
          array (
            'id' => '443',
            'name' => '鞋靴',
            'url' => 'category.php?id=443',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          439 => 
          array (
            'id' => '439',
            'name' => '箱包',
            'url' => 'category.php?id=439',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          440 => 
          array (
            'id' => '440',
            'name' => '钱包',
            'url' => 'category.php?id=440',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          441 => 
          array (
            'id' => '441',
            'name' => '服饰',
            'url' => 'category.php?id=441',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          442 => 
          array (
            'id' => '442',
            'name' => '腰带',
            'url' => 'category.php?id=442',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          445 => 
          array (
            'id' => '445',
            'name' => '太阳镜/眼镜框',
            'url' => 'category.php?id=445',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          446 => 
          array (
            'id' => '446',
            'name' => '饰品',
            'url' => 'category.php?id=446',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          447 => 
          array (
            'id' => '447',
            'name' => '配件',
            'url' => 'category.php?id=447',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      360 => 
      array (
        'id' => '360',
        'name' => '功能箱包',
        'url' => 'category.php?id=360',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          427 => 
          array (
            'id' => '427',
            'name' => '拉杆箱/拉杆包',
            'url' => 'category.php?id=427',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          428 => 
          array (
            'id' => '428',
            'name' => '旅行包',
            'url' => 'category.php?id=428',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          430 => 
          array (
            'id' => '430',
            'name' => '电脑包',
            'url' => 'category.php?id=430',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          432 => 
          array (
            'id' => '432',
            'name' => '休闲运动包',
            'url' => 'category.php?id=432',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          433 => 
          array (
            'id' => '433',
            'name' => '相机包',
            'url' => 'category.php?id=433',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          434 => 
          array (
            'id' => '434',
            'name' => '腰包/胸包',
            'url' => 'category.php?id=434',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          435 => 
          array (
            'id' => '435',
            'name' => '登山包',
            'url' => 'category.php?id=435',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          436 => 
          array (
            'id' => '436',
            'name' => '旅行配件',
            'url' => 'category.php?id=436',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          437 => 
          array (
            'id' => '437',
            'name' => '书包',
            'url' => 'category.php?id=437',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          438 => 
          array (
            'id' => '438',
            'name' => '妈咪包',
            'url' => 'category.php?id=438',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      355 => 
      array (
        'id' => '355',
        'name' => '流行男鞋',
        'url' => 'category.php?id=355',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          399 => 
          array (
            'id' => '399',
            'name' => '休闲鞋',
            'url' => 'category.php?id=399',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          401 => 
          array (
            'id' => '401',
            'name' => '凉鞋/沙滩鞋',
            'url' => 'category.php?id=401',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          402 => 
          array (
            'id' => '402',
            'name' => '帆布鞋',
            'url' => 'category.php?id=402',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          403 => 
          array (
            'id' => '403',
            'name' => '商务休闲鞋',
            'url' => 'category.php?id=403',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          404 => 
          array (
            'id' => '404',
            'name' => '正装鞋',
            'url' => 'category.php?id=404',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          405 => 
          array (
            'id' => '405',
            'name' => '增高鞋',
            'url' => 'category.php?id=405',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          406 => 
          array (
            'id' => '406',
            'name' => '拖鞋/人字拖',
            'url' => 'category.php?id=406',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          407 => 
          array (
            'id' => '407',
            'name' => '工装鞋',
            'url' => 'category.php?id=407',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          408 => 
          array (
            'id' => '408',
            'name' => '男靴',
            'url' => 'category.php?id=408',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          409 => 
          array (
            'id' => '409',
            'name' => '传统布鞋',
            'url' => 'category.php?id=409',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          410 => 
          array (
            'id' => '410',
            'name' => '功能鞋',
            'url' => 'category.php?id=410',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          411 => 
          array (
            'id' => '411',
            'name' => '鞋配件',
            'url' => 'category.php?id=411',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          412 => 
          array (
            'id' => '412',
            'name' => '定制鞋',
            'url' => 'category.php?id=412',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      353 => 
      array (
        'id' => '353',
        'name' => '时尚女鞋',
        'url' => 'category.php?id=353',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          368 => 
          array (
            'id' => '368',
            'name' => '凉鞋',
            'url' => 'category.php?id=368',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          371 => 
          array (
            'id' => '371',
            'name' => '单鞋',
            'url' => 'category.php?id=371',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          373 => 
          array (
            'id' => '373',
            'name' => '高跟鞋',
            'url' => 'category.php?id=373',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          375 => 
          array (
            'id' => '375',
            'name' => '坡跟鞋',
            'url' => 'category.php?id=375',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          376 => 
          array (
            'id' => '376',
            'name' => '松糕鞋',
            'url' => 'category.php?id=376',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          378 => 
          array (
            'id' => '378',
            'name' => '鱼嘴鞋',
            'url' => 'category.php?id=378',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          380 => 
          array (
            'id' => '380',
            'name' => '休闲鞋',
            'url' => 'category.php?id=380',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          382 => 
          array (
            'id' => '382',
            'name' => '帆布鞋',
            'url' => 'category.php?id=382',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          384 => 
          array (
            'id' => '384',
            'name' => '拖鞋/人字拖',
            'url' => 'category.php?id=384',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          386 => 
          array (
            'id' => '386',
            'name' => '妈妈鞋',
            'url' => 'category.php?id=386',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          387 => 
          array (
            'id' => '387',
            'name' => '防水台',
            'url' => 'category.php?id=387',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          389 => 
          array (
            'id' => '389',
            'name' => '雨鞋/雨靴',
            'url' => 'category.php?id=389',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          391 => 
          array (
            'id' => '391',
            'name' => '内增高',
            'url' => 'category.php?id=391',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          392 => 
          array (
            'id' => '392',
            'name' => '布鞋/绣花鞋',
            'url' => 'category.php?id=392',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          393 => 
          array (
            'id' => '393',
            'name' => '女靴',
            'url' => 'category.php?id=393',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          394 => 
          array (
            'id' => '394',
            'name' => '雪地靴',
            'url' => 'category.php?id=394',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          396 => 
          array (
            'id' => '396',
            'name' => '踝靴',
            'url' => 'category.php?id=396',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          397 => 
          array (
            'id' => '397',
            'name' => '马丁靴',
            'url' => 'category.php?id=397',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          398 => 
          array (
            'id' => '398',
            'name' => '鞋配件',
            'url' => 'category.php?id=398',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      357 => 
      array (
        'id' => '357',
        'name' => '潮流女包',
        'url' => 'category.php?id=357',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          413 => 
          array (
            'id' => '413',
            'name' => '单肩包',
            'url' => 'category.php?id=413',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          414 => 
          array (
            'id' => '414',
            'name' => '手提包',
            'url' => 'category.php?id=414',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          415 => 
          array (
            'id' => '415',
            'name' => '斜跨包',
            'url' => 'category.php?id=415',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          416 => 
          array (
            'id' => '416',
            'name' => '双肩包',
            'url' => 'category.php?id=416',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          417 => 
          array (
            'id' => '417',
            'name' => '钱包',
            'url' => 'category.php?id=417',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          418 => 
          array (
            'id' => '418',
            'name' => '手拿包/晚宴包',
            'url' => 'category.php?id=418',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          419 => 
          array (
            'id' => '419',
            'name' => '卡包/零钱包',
            'url' => 'category.php?id=419',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          420 => 
          array (
            'id' => '420',
            'name' => '钥匙包',
            'url' => 'category.php?id=420',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      359 => 
      array (
        'id' => '359',
        'name' => '精品男包',
        'url' => 'category.php?id=359',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          421 => 
          array (
            'id' => '421',
            'name' => '商务公文包',
            'url' => 'category.php?id=421',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          422 => 
          array (
            'id' => '422',
            'name' => '单肩/斜跨包',
            'url' => 'category.php?id=422',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          423 => 
          array (
            'id' => '423',
            'name' => '男生手包',
            'url' => 'category.php?id=423',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          424 => 
          array (
            'id' => '424',
            'name' => '双肩包',
            'url' => 'category.php?id=424',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          425 => 
          array (
            'id' => '425',
            'name' => '钱包/卡包',
            'url' => 'category.php?id=425',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          426 => 
          array (
            'id' => '426',
            'name' => '钥匙包',
            'url' => 'category.php?id=426',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      364 => 
      array (
        'id' => '364',
        'name' => '钟表',
        'url' => 'category.php?id=364',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          449 => 
          array (
            'id' => '449',
            'name' => '男表',
            'url' => 'category.php?id=449',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          450 => 
          array (
            'id' => '450',
            'name' => '女表',
            'url' => 'category.php?id=450',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          452 => 
          array (
            'id' => '452',
            'name' => '儿童表',
            'url' => 'category.php?id=452',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          453 => 
          array (
            'id' => '453',
            'name' => '座钟挂钟',
            'url' => 'category.php?id=453',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  860 => 
  array (
    'cat_id' => '860',
    'cat_name' => '个人化妆、清洁用品',
    'cat_alias_name' => '个人化妆',
    'cat_icon' => '',
    'style_icon' => 'heal',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=860',
    'child_tree' => 
    array (
      876 => 
      array (
        'id' => '876',
        'name' => '面部护肤',
        'url' => 'category.php?id=876',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          877 => 
          array (
            'id' => '877',
            'name' => '清洁',
            'url' => 'category.php?id=877',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          878 => 
          array (
            'id' => '878',
            'name' => '护肤',
            'url' => 'category.php?id=878',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          879 => 
          array (
            'id' => '879',
            'name' => '面膜',
            'url' => 'category.php?id=879',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1177 => 
          array (
            'id' => '1177',
            'name' => '洗面奶',
            'url' => 'category.php?id=1177',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1178 => 
          array (
            'id' => '1178',
            'name' => 'BB霜',
            'url' => 'category.php?id=1178',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1179 => 
          array (
            'id' => '1179',
            'name' => '指甲油',
            'url' => 'category.php?id=1179',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1180 => 
          array (
            'id' => '1180',
            'name' => '洗面泥',
            'url' => 'category.php?id=1180',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1181 => 
          array (
            'id' => '1181',
            'name' => '水润护肤',
            'url' => 'category.php?id=1181',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1182 => 
          array (
            'id' => '1182',
            'name' => '卸妆水',
            'url' => 'category.php?id=1182',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1183 => 
          array (
            'id' => '1183',
            'name' => '雪花膏',
            'url' => 'category.php?id=1183',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1184 => 
          array (
            'id' => '1184',
            'name' => '爽肤水',
            'url' => 'category.php?id=1184',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1185 => 
          array (
            'id' => '1185',
            'name' => '清洁套装',
            'url' => 'category.php?id=1185',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1186 => 
          array (
            'id' => '1186',
            'name' => '剃须',
            'url' => 'category.php?id=1186',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1187 => 
          array (
            'id' => '1187',
            'name' => '洁面刷',
            'url' => 'category.php?id=1187',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1188 => 
          array (
            'id' => '1188',
            'name' => '修眉笔',
            'url' => 'category.php?id=1188',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      880 => 
      array (
        'id' => '880',
        'name' => '洗发护发',
        'url' => 'category.php?id=880',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          881 => 
          array (
            'id' => '881',
            'name' => '洗发',
            'url' => 'category.php?id=881',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          882 => 
          array (
            'id' => '882',
            'name' => '护发',
            'url' => 'category.php?id=882',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          883 => 
          array (
            'id' => '883',
            'name' => '染发',
            'url' => 'category.php?id=883',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          884 => 
          array (
            'id' => '884',
            'name' => '造型',
            'url' => 'category.php?id=884',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      885 => 
      array (
        'id' => '885',
        'name' => '身体护肤',
        'url' => 'category.php?id=885',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          886 => 
          array (
            'id' => '886',
            'name' => '沐浴',
            'url' => 'category.php?id=886',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          887 => 
          array (
            'id' => '887',
            'name' => '润肤',
            'url' => 'category.php?id=887',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          888 => 
          array (
            'id' => '888',
            'name' => '手足',
            'url' => 'category.php?id=888',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          889 => 
          array (
            'id' => '889',
            'name' => '美胸',
            'url' => 'category.php?id=889',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          890 => 
          array (
            'id' => '890',
            'name' => '套装',
            'url' => 'category.php?id=890',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      891 => 
      array (
        'id' => '891',
        'name' => '口腔护理',
        'url' => 'category.php?id=891',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          892 => 
          array (
            'id' => '892',
            'name' => '牙膏/牙粉',
            'url' => 'category.php?id=892',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          893 => 
          array (
            'id' => '893',
            'name' => '牙刷/牙线',
            'url' => 'category.php?id=893',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          894 => 
          array (
            'id' => '894',
            'name' => '漱口水',
            'url' => 'category.php?id=894',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          895 => 
          array (
            'id' => '895',
            'name' => '套装',
            'url' => 'category.php?id=895',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      896 => 
      array (
        'id' => '896',
        'name' => '香水彩妆',
        'url' => 'category.php?id=896',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          897 => 
          array (
            'id' => '897',
            'name' => '香水',
            'url' => 'category.php?id=897',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          898 => 
          array (
            'id' => '898',
            'name' => '底妆',
            'url' => 'category.php?id=898',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          899 => 
          array (
            'id' => '899',
            'name' => '腮红',
            'url' => 'category.php?id=899',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          900 => 
          array (
            'id' => '900',
            'name' => '眼部',
            'url' => 'category.php?id=900',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          901 => 
          array (
            'id' => '901',
            'name' => '美甲',
            'url' => 'category.php?id=901',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1349 => 
          array (
            'id' => '1349',
            'name' => '精油放疗',
            'url' => 'category.php?id=1349',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1350 => 
          array (
            'id' => '1350',
            'name' => '假睫毛',
            'url' => 'category.php?id=1350',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1351 => 
          array (
            'id' => '1351',
            'name' => '彩妆套装',
            'url' => 'category.php?id=1351',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1352 => 
          array (
            'id' => '1352',
            'name' => '蜜粉',
            'url' => 'category.php?id=1352',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1353 => 
          array (
            'id' => '1353',
            'name' => '遮瑕',
            'url' => 'category.php?id=1353',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1354 => 
          array (
            'id' => '1354',
            'name' => '化妆棉',
            'url' => 'category.php?id=1354',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1355 => 
          array (
            'id' => '1355',
            'name' => '双眼皮贴',
            'url' => 'category.php?id=1355',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1356 => 
          array (
            'id' => '1356',
            'name' => '高光阴影',
            'url' => 'category.php?id=1356',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1357 => 
          array (
            'id' => '1357',
            'name' => '隔离',
            'url' => 'category.php?id=1357',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1358 => 
          array (
            'id' => '1358',
            'name' => '粉饼',
            'url' => 'category.php?id=1358',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1359 => 
          array (
            'id' => '1359',
            'name' => '气垫BB',
            'url' => 'category.php?id=1359',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1205 => 
      array (
        'id' => '1205',
        'name' => '女性护理',
        'url' => 'category.php?id=1205',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1206 => 
          array (
            'id' => '1206',
            'name' => '卫生巾',
            'url' => 'category.php?id=1206',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1207 => 
          array (
            'id' => '1207',
            'name' => '卫生护垫',
            'url' => 'category.php?id=1207',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1208 => 
          array (
            'id' => '1208',
            'name' => '私密护理',
            'url' => 'category.php?id=1208',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1209 => 
          array (
            'id' => '1209',
            'name' => '脱毛膏',
            'url' => 'category.php?id=1209',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1210 => 
      array (
        'id' => '1210',
        'name' => '清洁用品',
        'url' => 'category.php?id=1210',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1211 => 
          array (
            'id' => '1211',
            'name' => '纸品湿巾',
            'url' => 'category.php?id=1211',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1212 => 
          array (
            'id' => '1212',
            'name' => '衣物清洁',
            'url' => 'category.php?id=1212',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1213 => 
          array (
            'id' => '1213',
            'name' => '清洁工具',
            'url' => 'category.php?id=1213',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1214 => 
          array (
            'id' => '1214',
            'name' => '家庭清洁',
            'url' => 'category.php?id=1214',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1215 => 
          array (
            'id' => '1215',
            'name' => '一次性用品',
            'url' => 'category.php?id=1215',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1216 => 
          array (
            'id' => '1216',
            'name' => '驱蚊用品',
            'url' => 'category.php?id=1216',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1217 => 
          array (
            'id' => '1217',
            'name' => '皮具护理',
            'url' => 'category.php?id=1217',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  11 => 
  array (
    'cat_id' => '11',
    'cat_name' => '母婴、玩具乐器',
    'cat_alias_name' => '母婴玩具',
    'cat_icon' => '',
    'style_icon' => 'baby',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=11',
    'child_tree' => 
    array (
      491 => 
      array (
        'id' => '491',
        'name' => '玩具乐器',
        'url' => 'category.php?id=491',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          602 => 
          array (
            'id' => '602',
            'name' => '使用年龄',
            'url' => 'category.php?id=602',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          603 => 
          array (
            'id' => '603',
            'name' => '遥控/电动',
            'url' => 'category.php?id=603',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          604 => 
          array (
            'id' => '604',
            'name' => '毛绒布艺',
            'url' => 'category.php?id=604',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          605 => 
          array (
            'id' => '605',
            'name' => '娃娃玩具',
            'url' => 'category.php?id=605',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          606 => 
          array (
            'id' => '606',
            'name' => '模型玩具',
            'url' => 'category.php?id=606',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          607 => 
          array (
            'id' => '607',
            'name' => '健身玩具',
            'url' => 'category.php?id=607',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          608 => 
          array (
            'id' => '608',
            'name' => '动漫玩具',
            'url' => 'category.php?id=608',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          609 => 
          array (
            'id' => '609',
            'name' => '益智玩具',
            'url' => 'category.php?id=609',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          610 => 
          array (
            'id' => '610',
            'name' => '积木拼插',
            'url' => 'category.php?id=610',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          611 => 
          array (
            'id' => '611',
            'name' => 'DIY玩具',
            'url' => 'category.php?id=611',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          612 => 
          array (
            'id' => '612',
            'name' => '创意减压',
            'url' => 'category.php?id=612',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          613 => 
          array (
            'id' => '613',
            'name' => '乐器相关',
            'url' => 'category.php?id=613',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      476 => 
      array (
        'id' => '476',
        'name' => '奶粉',
        'url' => 'category.php?id=476',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          492 => 
          array (
            'id' => '492',
            'name' => '婴幼奶粉',
            'url' => 'category.php?id=492',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          493 => 
          array (
            'id' => '493',
            'name' => '成人奶粉',
            'url' => 'category.php?id=493',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      477 => 
      array (
        'id' => '477',
        'name' => '营养辅食',
        'url' => 'category.php?id=477',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          495 => 
          array (
            'id' => '495',
            'name' => 'DHA',
            'url' => 'category.php?id=495',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          496 => 
          array (
            'id' => '496',
            'name' => '钙铁锌/维生素',
            'url' => 'category.php?id=496',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          497 => 
          array (
            'id' => '497',
            'name' => '益生菌/初乳',
            'url' => 'category.php?id=497',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          499 => 
          array (
            'id' => '499',
            'name' => '清火/开胃',
            'url' => 'category.php?id=499',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          500 => 
          array (
            'id' => '500',
            'name' => '米粉/菜粉',
            'url' => 'category.php?id=500',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          501 => 
          array (
            'id' => '501',
            'name' => '果泥/果汁',
            'url' => 'category.php?id=501',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          503 => 
          array (
            'id' => '503',
            'name' => '面条/粥',
            'url' => 'category.php?id=503',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          506 => 
          array (
            'id' => '506',
            'name' => '宝宝零食',
            'url' => 'category.php?id=506',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      479 => 
      array (
        'id' => '479',
        'name' => '尿裤湿巾',
        'url' => 'category.php?id=479',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          509 => 
          array (
            'id' => '509',
            'name' => '婴儿尿裤',
            'url' => 'category.php?id=509',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          511 => 
          array (
            'id' => '511',
            'name' => '拉拉裤',
            'url' => 'category.php?id=511',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          514 => 
          array (
            'id' => '514',
            'name' => '成人尿裤',
            'url' => 'category.php?id=514',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          515 => 
          array (
            'id' => '515',
            'name' => '湿巾',
            'url' => 'category.php?id=515',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      481 => 
      array (
        'id' => '481',
        'name' => '洗护用品',
        'url' => 'category.php?id=481',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          518 => 
          array (
            'id' => '518',
            'name' => '宝宝护肤',
            'url' => 'category.php?id=518',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          519 => 
          array (
            'id' => '519',
            'name' => '宝宝洗浴',
            'url' => 'category.php?id=519',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          521 => 
          array (
            'id' => '521',
            'name' => '理发器',
            'url' => 'category.php?id=521',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          523 => 
          array (
            'id' => '523',
            'name' => '洗衣液/皂',
            'url' => 'category.php?id=523',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          524 => 
          array (
            'id' => '524',
            'name' => '奶瓶清洗',
            'url' => 'category.php?id=524',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          525 => 
          array (
            'id' => '525',
            'name' => '日常护理',
            'url' => 'category.php?id=525',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          527 => 
          array (
            'id' => '527',
            'name' => '座便器',
            'url' => 'category.php?id=527',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          528 => 
          array (
            'id' => '528',
            'name' => '驱蚊防蚊',
            'url' => 'category.php?id=528',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      482 => 
      array (
        'id' => '482',
        'name' => '喂养用品',
        'url' => 'category.php?id=482',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          529 => 
          array (
            'id' => '529',
            'name' => '奶瓶奶嘴',
            'url' => 'category.php?id=529',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          531 => 
          array (
            'id' => '531',
            'name' => '吸奶器',
            'url' => 'category.php?id=531',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          533 => 
          array (
            'id' => '533',
            'name' => '牙胶安抚',
            'url' => 'category.php?id=533',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          534 => 
          array (
            'id' => '534',
            'name' => '暖奶消毒',
            'url' => 'category.php?id=534',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          535 => 
          array (
            'id' => '535',
            'name' => '辅食料理机',
            'url' => 'category.php?id=535',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          536 => 
          array (
            'id' => '536',
            'name' => '碗盘叉勺',
            'url' => 'category.php?id=536',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          537 => 
          array (
            'id' => '537',
            'name' => '水壶/水杯',
            'url' => 'category.php?id=537',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      483 => 
      array (
        'id' => '483',
        'name' => '童车童床',
        'url' => 'category.php?id=483',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          538 => 
          array (
            'id' => '538',
            'name' => '婴儿床',
            'url' => 'category.php?id=538',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          539 => 
          array (
            'id' => '539',
            'name' => '婴儿推车',
            'url' => 'category.php?id=539',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          540 => 
          array (
            'id' => '540',
            'name' => '餐椅摇椅',
            'url' => 'category.php?id=540',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          541 => 
          array (
            'id' => '541',
            'name' => '学步车',
            'url' => 'category.php?id=541',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          543 => 
          array (
            'id' => '543',
            'name' => '三轮车',
            'url' => 'category.php?id=543',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          545 => 
          array (
            'id' => '545',
            'name' => '自行车',
            'url' => 'category.php?id=545',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          546 => 
          array (
            'id' => '546',
            'name' => '扭扭车',
            'url' => 'category.php?id=546',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          548 => 
          array (
            'id' => '548',
            'name' => '滑板车',
            'url' => 'category.php?id=548',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          549 => 
          array (
            'id' => '549',
            'name' => '电动车',
            'url' => 'category.php?id=549',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      484 => 
      array (
        'id' => '484',
        'name' => '安全座椅',
        'url' => 'category.php?id=484',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          551 => 
          array (
            'id' => '551',
            'name' => '安全座椅',
            'url' => 'category.php?id=551',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          552 => 
          array (
            'id' => '552',
            'name' => '提篮式',
            'url' => 'category.php?id=552',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          553 => 
          array (
            'id' => '553',
            'name' => '增高垫',
            'url' => 'category.php?id=553',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      487 => 
      array (
        'id' => '487',
        'name' => '寝居服饰',
        'url' => 'category.php?id=487',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          556 => 
          array (
            'id' => '556',
            'name' => '婴儿外出服',
            'url' => 'category.php?id=556',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          558 => 
          array (
            'id' => '558',
            'name' => '婴儿内衣',
            'url' => 'category.php?id=558',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          559 => 
          array (
            'id' => '559',
            'name' => '婴儿礼盒',
            'url' => 'category.php?id=559',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          561 => 
          array (
            'id' => '561',
            'name' => '婴儿鞋帽袜',
            'url' => 'category.php?id=561',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          563 => 
          array (
            'id' => '563',
            'name' => '家居床品',
            'url' => 'category.php?id=563',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          565 => 
          array (
            'id' => '565',
            'name' => '安全防护',
            'url' => 'category.php?id=565',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      489 => 
      array (
        'id' => '489',
        'name' => '妈妈专区',
        'url' => 'category.php?id=489',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          572 => 
          array (
            'id' => '572',
            'name' => '妈咪包/背婴带',
            'url' => 'category.php?id=572',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          573 => 
          array (
            'id' => '573',
            'name' => '待产/新生',
            'url' => 'category.php?id=573',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          574 => 
          array (
            'id' => '574',
            'name' => '产后塑身',
            'url' => 'category.php?id=574',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          575 => 
          array (
            'id' => '575',
            'name' => '文胸/内裤',
            'url' => 'category.php?id=575',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          576 => 
          array (
            'id' => '576',
            'name' => '防辐射服',
            'url' => 'category.php?id=576',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          577 => 
          array (
            'id' => '577',
            'name' => '孕妇装',
            'url' => 'category.php?id=577',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          578 => 
          array (
            'id' => '578',
            'name' => '月子装',
            'url' => 'category.php?id=578',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          579 => 
          array (
            'id' => '579',
            'name' => '孕期营养',
            'url' => 'category.php?id=579',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          581 => 
          array (
            'id' => '581',
            'name' => '孕妈美容',
            'url' => 'category.php?id=581',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      490 => 
      array (
        'id' => '490',
        'name' => '童装童鞋',
        'url' => 'category.php?id=490',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          584 => 
          array (
            'id' => '584',
            'name' => '套装',
            'url' => 'category.php?id=584',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          585 => 
          array (
            'id' => '585',
            'name' => '上衣',
            'url' => 'category.php?id=585',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          586 => 
          array (
            'id' => '586',
            'name' => '裤子',
            'url' => 'category.php?id=586',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          587 => 
          array (
            'id' => '587',
            'name' => '裙子',
            'url' => 'category.php?id=587',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          588 => 
          array (
            'id' => '588',
            'name' => '内衣',
            'url' => 'category.php?id=588',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          589 => 
          array (
            'id' => '589',
            'name' => '羽绒服/棉服',
            'url' => 'category.php?id=589',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          591 => 
          array (
            'id' => '591',
            'name' => '亲子装',
            'url' => 'category.php?id=591',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          593 => 
          array (
            'id' => '593',
            'name' => '配饰',
            'url' => 'category.php?id=593',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          595 => 
          array (
            'id' => '595',
            'name' => '演出服',
            'url' => 'category.php?id=595',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          596 => 
          array (
            'id' => '596',
            'name' => '运动服',
            'url' => 'category.php?id=596',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          597 => 
          array (
            'id' => '597',
            'name' => '运动鞋',
            'url' => 'category.php?id=597',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          598 => 
          array (
            'id' => '598',
            'name' => '皮鞋/帆布鞋',
            'url' => 'category.php?id=598',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          599 => 
          array (
            'id' => '599',
            'name' => '靴子',
            'url' => 'category.php?id=599',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          600 => 
          array (
            'id' => '600',
            'name' => '凉鞋',
            'url' => 'category.php?id=600',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          601 => 
          array (
            'id' => '601',
            'name' => '功能鞋',
            'url' => 'category.php?id=601',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  1 => 
  array (
    'cat_id' => '1',
    'cat_name' => '图书、音像、电子书',
    'cat_alias_name' => '图书音像',
    'cat_icon' => '',
    'style_icon' => 'books',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=1',
    'child_tree' => 
    array (
      15 => 
      array (
        'id' => '15',
        'name' => '电子书',
        'url' => 'category.php?id=15',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          26 => 
          array (
            'id' => '26',
            'name' => '免费',
            'url' => 'category.php?id=26',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          27 => 
          array (
            'id' => '27',
            'name' => '小说',
            'url' => 'category.php?id=27',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          28 => 
          array (
            'id' => '28',
            'name' => '励志与成功',
            'url' => 'category.php?id=28',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          29 => 
          array (
            'id' => '29',
            'name' => '婚恋/两性',
            'url' => 'category.php?id=29',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          30 => 
          array (
            'id' => '30',
            'name' => '文学',
            'url' => 'category.php?id=30',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          31 => 
          array (
            'id' => '31',
            'name' => '经管',
            'url' => 'category.php?id=31',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          32 => 
          array (
            'id' => '32',
            'name' => '畅读VIP',
            'url' => 'category.php?id=32',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      16 => 
      array (
        'id' => '16',
        'name' => '数字音乐',
        'url' => 'category.php?id=16',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          36 => 
          array (
            'id' => '36',
            'name' => '通俗流行',
            'url' => 'category.php?id=36',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          38 => 
          array (
            'id' => '38',
            'name' => '古典音乐',
            'url' => 'category.php?id=38',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          41 => 
          array (
            'id' => '41',
            'name' => '摇滚说唱',
            'url' => 'category.php?id=41',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          45 => 
          array (
            'id' => '45',
            'name' => '爵士蓝调',
            'url' => 'category.php?id=45',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          48 => 
          array (
            'id' => '48',
            'name' => '乡村民谣',
            'url' => 'category.php?id=48',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          50 => 
          array (
            'id' => '50',
            'name' => '有声读物',
            'url' => 'category.php?id=50',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      17 => 
      array (
        'id' => '17',
        'name' => '音像',
        'url' => 'category.php?id=17',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          58 => 
          array (
            'id' => '58',
            'name' => '音乐',
            'url' => 'category.php?id=58',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          63 => 
          array (
            'id' => '63',
            'name' => '教育音像',
            'url' => 'category.php?id=63',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          65 => 
          array (
            'id' => '65',
            'name' => '游戏',
            'url' => 'category.php?id=65',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          67 => 
          array (
            'id' => '67',
            'name' => '影视',
            'url' => 'category.php?id=67',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      18 => 
      array (
        'id' => '18',
        'name' => '文艺',
        'url' => 'category.php?id=18',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          77 => 
          array (
            'id' => '77',
            'name' => '小说',
            'url' => 'category.php?id=77',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          78 => 
          array (
            'id' => '78',
            'name' => '文学',
            'url' => 'category.php?id=78',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          79 => 
          array (
            'id' => '79',
            'name' => '传纪',
            'url' => 'category.php?id=79',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          80 => 
          array (
            'id' => '80',
            'name' => '艺术',
            'url' => 'category.php?id=80',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          83 => 
          array (
            'id' => '83',
            'name' => '青春文学',
            'url' => 'category.php?id=83',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      19 => 
      array (
        'id' => '19',
        'name' => '人文社科',
        'url' => 'category.php?id=19',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          86 => 
          array (
            'id' => '86',
            'name' => '历史',
            'url' => 'category.php?id=86',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          89 => 
          array (
            'id' => '89',
            'name' => '心理学',
            'url' => 'category.php?id=89',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          92 => 
          array (
            'id' => '92',
            'name' => '政治/军事',
            'url' => 'category.php?id=92',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          94 => 
          array (
            'id' => '94',
            'name' => '国学/古籍',
            'url' => 'category.php?id=94',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          98 => 
          array (
            'id' => '98',
            'name' => '宗教/哲学',
            'url' => 'category.php?id=98',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          100 => 
          array (
            'id' => '100',
            'name' => '社会科学',
            'url' => 'category.php?id=100',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      20 => 
      array (
        'id' => '20',
        'name' => '经管励志',
        'url' => 'category.php?id=20',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          104 => 
          array (
            'id' => '104',
            'name' => '经济',
            'url' => 'category.php?id=104',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          107 => 
          array (
            'id' => '107',
            'name' => '金融与投资',
            'url' => 'category.php?id=107',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          109 => 
          array (
            'id' => '109',
            'name' => '管理',
            'url' => 'category.php?id=109',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          110 => 
          array (
            'id' => '110',
            'name' => '励志与成功',
            'url' => 'category.php?id=110',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      21 => 
      array (
        'id' => '21',
        'name' => '生活',
        'url' => 'category.php?id=21',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          118 => 
          array (
            'id' => '118',
            'name' => '家教与育儿',
            'url' => 'category.php?id=118',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          121 => 
          array (
            'id' => '121',
            'name' => '旅游/地图',
            'url' => 'category.php?id=121',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          123 => 
          array (
            'id' => '123',
            'name' => '烹饪/美食',
            'url' => 'category.php?id=123',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          125 => 
          array (
            'id' => '125',
            'name' => '时尚/美妆',
            'url' => 'category.php?id=125',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          126 => 
          array (
            'id' => '126',
            'name' => '家居',
            'url' => 'category.php?id=126',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          127 => 
          array (
            'id' => '127',
            'name' => '婚恋与两性',
            'url' => 'category.php?id=127',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          128 => 
          array (
            'id' => '128',
            'name' => '娱乐/休闲',
            'url' => 'category.php?id=128',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          129 => 
          array (
            'id' => '129',
            'name' => '健身与保健',
            'url' => 'category.php?id=129',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          130 => 
          array (
            'id' => '130',
            'name' => '动漫/幽默',
            'url' => 'category.php?id=130',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          131 => 
          array (
            'id' => '131',
            'name' => '体育/运动',
            'url' => 'category.php?id=131',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      22 => 
      array (
        'id' => '22',
        'name' => '科技',
        'url' => 'category.php?id=22',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          133 => 
          array (
            'id' => '133',
            'name' => '科普',
            'url' => 'category.php?id=133',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          134 => 
          array (
            'id' => '134',
            'name' => 'IT',
            'url' => 'category.php?id=134',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          135 => 
          array (
            'id' => '135',
            'name' => '建筑',
            'url' => 'category.php?id=135',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          136 => 
          array (
            'id' => '136',
            'name' => '医学',
            'url' => 'category.php?id=136',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          137 => 
          array (
            'id' => '137',
            'name' => '工业技术',
            'url' => 'category.php?id=137',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          138 => 
          array (
            'id' => '138',
            'name' => '电子/通信',
            'url' => 'category.php?id=138',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          139 => 
          array (
            'id' => '139',
            'name' => '农林',
            'url' => 'category.php?id=139',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          140 => 
          array (
            'id' => '140',
            'name' => '科学与自然',
            'url' => 'category.php?id=140',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      25 => 
      array (
        'id' => '25',
        'name' => '其它',
        'url' => 'category.php?id=25',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          177 => 
          array (
            'id' => '177',
            'name' => '英文原版书',
            'url' => 'category.php?id=177',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          180 => 
          array (
            'id' => '180',
            'name' => '港台图书',
            'url' => 'category.php?id=180',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          183 => 
          array (
            'id' => '183',
            'name' => '工具书',
            'url' => 'category.php?id=183',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          185 => 
          array (
            'id' => '185',
            'name' => '套装书',
            'url' => 'category.php?id=185',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          187 => 
          array (
            'id' => '187',
            'name' => '杂志/期刊',
            'url' => 'category.php?id=187',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      142 => 
      array (
        'id' => '142',
        'name' => '少儿',
        'url' => 'category.php?id=142',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          145 => 
          array (
            'id' => '145',
            'name' => '少儿',
            'url' => 'category.php?id=145',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          147 => 
          array (
            'id' => '147',
            'name' => '0-2岁',
            'url' => 'category.php?id=147',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          150 => 
          array (
            'id' => '150',
            'name' => '3-6岁',
            'url' => 'category.php?id=150',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          155 => 
          array (
            'id' => '155',
            'name' => '7-10岁',
            'url' => 'category.php?id=155',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      162 => 
      array (
        'id' => '162',
        'name' => '教育',
        'url' => 'category.php?id=162',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          164 => 
          array (
            'id' => '164',
            'name' => '教材',
            'url' => 'category.php?id=164',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          166 => 
          array (
            'id' => '166',
            'name' => '中小学教辅',
            'url' => 'category.php?id=166',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          170 => 
          array (
            'id' => '170',
            'name' => '考试',
            'url' => 'category.php?id=170',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          173 => 
          array (
            'id' => '173',
            'name' => '外语学习',
            'url' => 'category.php?id=173',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  866 => 
  array (
    'cat_id' => '866',
    'cat_name' => '休闲、运动、户外健身',
    'cat_alias_name' => '休闲运动',
    'cat_icon' => '',
    'style_icon' => 'outdoors',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=866',
    'child_tree' => 
    array (
      902 => 
      array (
        'id' => '902',
        'name' => '运动鞋包',
        'url' => 'category.php?id=902',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          903 => 
          array (
            'id' => '903',
            'name' => '跑步鞋',
            'url' => 'category.php?id=903',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          904 => 
          array (
            'id' => '904',
            'name' => '篮球鞋',
            'url' => 'category.php?id=904',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          905 => 
          array (
            'id' => '905',
            'name' => '休闲鞋',
            'url' => 'category.php?id=905',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          906 => 
          array (
            'id' => '906',
            'name' => '板鞋',
            'url' => 'category.php?id=906',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          907 => 
          array (
            'id' => '907',
            'name' => '运动鞋',
            'url' => 'category.php?id=907',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          908 => 
          array (
            'id' => '908',
            'name' => '皮鞋',
            'url' => 'category.php?id=908',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      909 => 
      array (
        'id' => '909',
        'name' => '运动服饰',
        'url' => 'category.php?id=909',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          910 => 
          array (
            'id' => '910',
            'name' => '运动套装',
            'url' => 'category.php?id=910',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          911 => 
          array (
            'id' => '911',
            'name' => '羽绒服',
            'url' => 'category.php?id=911',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          912 => 
          array (
            'id' => '912',
            'name' => '卫衣/套头衫',
            'url' => 'category.php?id=912',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          913 => 
          array (
            'id' => '913',
            'name' => '棉衣',
            'url' => 'category.php?id=913',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          914 => 
          array (
            'id' => '914',
            'name' => '运动裤',
            'url' => 'category.php?id=914',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          915 => 
          array (
            'id' => '915',
            'name' => '夹克/风衣',
            'url' => 'category.php?id=915',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          916 => 
          array (
            'id' => '916',
            'name' => '西裤',
            'url' => 'category.php?id=916',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          917 => 
          array (
            'id' => '917',
            'name' => '训练服',
            'url' => 'category.php?id=917',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          918 => 
          array (
            'id' => '918',
            'name' => 'T恤',
            'url' => 'category.php?id=918',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1218 => 
          array (
            'id' => '1218',
            'name' => '乒羽网服',
            'url' => 'category.php?id=1218',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1219 => 
          array (
            'id' => '1219',
            'name' => '毛衣/衬衣',
            'url' => 'category.php?id=1219',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1224 => 
          array (
            'id' => '1224',
            'name' => '软壳衣物',
            'url' => 'category.php?id=1224',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      919 => 
      array (
        'id' => '919',
        'name' => '健身训练',
        'url' => 'category.php?id=919',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          920 => 
          array (
            'id' => '920',
            'name' => '跑步机',
            'url' => 'category.php?id=920',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          921 => 
          array (
            'id' => '921',
            'name' => '健身车/动感单车',
            'url' => 'category.php?id=921',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          922 => 
          array (
            'id' => '922',
            'name' => '哑铃',
            'url' => 'category.php?id=922',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          923 => 
          array (
            'id' => '923',
            'name' => '甩脂机',
            'url' => 'category.php?id=923',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          924 => 
          array (
            'id' => '924',
            'name' => '踏步机',
            'url' => 'category.php?id=924',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          925 => 
          array (
            'id' => '925',
            'name' => '运动护具',
            'url' => 'category.php?id=925',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          926 => 
          array (
            'id' => '926',
            'name' => '武术搏击',
            'url' => 'category.php?id=926',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1220 => 
          array (
            'id' => '1220',
            'name' => '舞蹈瑜伽',
            'url' => 'category.php?id=1220',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1221 => 
          array (
            'id' => '1221',
            'name' => '综合训练器',
            'url' => 'category.php?id=1221',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1222 => 
          array (
            'id' => '1222',
            'name' => '其他大型器械',
            'url' => 'category.php?id=1222',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1223 => 
          array (
            'id' => '1223',
            'name' => '其他中小型器材',
            'url' => 'category.php?id=1223',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      927 => 
      array (
        'id' => '927',
        'name' => '骑行运动',
        'url' => 'category.php?id=927',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          928 => 
          array (
            'id' => '928',
            'name' => '山地车/自行车',
            'url' => 'category.php?id=928',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          929 => 
          array (
            'id' => '929',
            'name' => '折叠车',
            'url' => 'category.php?id=929',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          930 => 
          array (
            'id' => '930',
            'name' => '电动车',
            'url' => 'category.php?id=930',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          931 => 
          array (
            'id' => '931',
            'name' => '平衡车',
            'url' => 'category.php?id=931',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          932 => 
          array (
            'id' => '932',
            'name' => '骑行装备',
            'url' => 'category.php?id=932',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          933 => 
          array (
            'id' => '933',
            'name' => '骑行服',
            'url' => 'category.php?id=933',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      934 => 
      array (
        'id' => '934',
        'name' => '体育用品',
        'url' => 'category.php?id=934',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          935 => 
          array (
            'id' => '935',
            'name' => '篮球',
            'url' => 'category.php?id=935',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          936 => 
          array (
            'id' => '936',
            'name' => '足球',
            'url' => 'category.php?id=936',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          937 => 
          array (
            'id' => '937',
            'name' => '羽毛球',
            'url' => 'category.php?id=937',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          938 => 
          array (
            'id' => '938',
            'name' => '乒乓球',
            'url' => 'category.php?id=938',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          939 => 
          array (
            'id' => '939',
            'name' => '网球',
            'url' => 'category.php?id=939',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          940 => 
          array (
            'id' => '940',
            'name' => '高尔夫',
            'url' => 'category.php?id=940',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          941 => 
          array (
            'id' => '941',
            'name' => '台球',
            'url' => 'category.php?id=941',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      942 => 
      array (
        'id' => '942',
        'name' => '户外鞋服',
        'url' => 'category.php?id=942',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          943 => 
          array (
            'id' => '943',
            'name' => '冲锋衣裤',
            'url' => 'category.php?id=943',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          944 => 
          array (
            'id' => '944',
            'name' => '速干衣裤',
            'url' => 'category.php?id=944',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          945 => 
          array (
            'id' => '945',
            'name' => '滑雪服',
            'url' => 'category.php?id=945',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          946 => 
          array (
            'id' => '946',
            'name' => '休闲衣裤',
            'url' => 'category.php?id=946',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          947 => 
          array (
            'id' => '947',
            'name' => '登山鞋',
            'url' => 'category.php?id=947',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          948 => 
          array (
            'id' => '948',
            'name' => '徒步鞋',
            'url' => 'category.php?id=948',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          949 => 
          array (
            'id' => '949',
            'name' => '户外风衣',
            'url' => 'category.php?id=949',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          950 => 
          array (
            'id' => '950',
            'name' => '功能内衣',
            'url' => 'category.php?id=950',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1225 => 
          array (
            'id' => '1225',
            'name' => '羽绒服/棉服',
            'url' => 'category.php?id=1225',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1226 => 
          array (
            'id' => '1226',
            'name' => '军迷服饰',
            'url' => 'category.php?id=1226',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1227 => 
          array (
            'id' => '1227',
            'name' => '越野跑鞋',
            'url' => 'category.php?id=1227',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1228 => 
          array (
            'id' => '1228',
            'name' => '雪地靴',
            'url' => 'category.php?id=1228',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1229 => 
          array (
            'id' => '1229',
            'name' => '工装靴',
            'url' => 'category.php?id=1229',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1230 => 
          array (
            'id' => '1230',
            'name' => '沙滩鞋',
            'url' => 'category.php?id=1230',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1231 => 
          array (
            'id' => '1231',
            'name' => '户外袜',
            'url' => 'category.php?id=1231',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1232 => 
          array (
            'id' => '1232',
            'name' => '凉拖',
            'url' => 'category.php?id=1232',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1233 => 
          array (
            'id' => '1233',
            'name' => '抓绒衣裤',
            'url' => 'category.php?id=1233',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1234 => 
          array (
            'id' => '1234',
            'name' => '软壳衣物',
            'url' => 'category.php?id=1234',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1235 => 
          array (
            'id' => '1235',
            'name' => '溯身鞋',
            'url' => 'category.php?id=1235',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1236 => 
          array (
            'id' => '1236',
            'name' => '休闲鞋服',
            'url' => 'category.php?id=1236',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      951 => 
      array (
        'id' => '951',
        'name' => '户外装备',
        'url' => 'category.php?id=951',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          952 => 
          array (
            'id' => '952',
            'name' => '帐篷',
            'url' => 'category.php?id=952',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          953 => 
          array (
            'id' => '953',
            'name' => '背包',
            'url' => 'category.php?id=953',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          954 => 
          array (
            'id' => '954',
            'name' => '睡袋/吊床',
            'url' => 'category.php?id=954',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          955 => 
          array (
            'id' => '955',
            'name' => '野餐烧烤',
            'url' => 'category.php?id=955',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          956 => 
          array (
            'id' => '956',
            'name' => '户外工具',
            'url' => 'category.php?id=956',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1237 => 
          array (
            'id' => '1237',
            'name' => '帐篷/垫子',
            'url' => 'category.php?id=1237',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1238 => 
          array (
            'id' => '1238',
            'name' => '用户照明',
            'url' => 'category.php?id=1238',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1239 => 
          array (
            'id' => '1239',
            'name' => '攀山蹬岩',
            'url' => 'category.php?id=1239',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1240 => 
          array (
            'id' => '1240',
            'name' => '便携桌椅床',
            'url' => 'category.php?id=1240',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1241 => 
          array (
            'id' => '1241',
            'name' => '望远镜',
            'url' => 'category.php?id=1241',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1242 => 
          array (
            'id' => '1242',
            'name' => '军迷用品',
            'url' => 'category.php?id=1242',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1243 => 
          array (
            'id' => '1243',
            'name' => '旅游用品',
            'url' => 'category.php?id=1243',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1244 => 
          array (
            'id' => '1244',
            'name' => '救援装备',
            'url' => 'category.php?id=1244',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1245 => 
          array (
            'id' => '1245',
            'name' => '极限运动',
            'url' => 'category.php?id=1245',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  864 => 
  array (
    'cat_id' => '864',
    'cat_name' => '腕表、珠宝配饰、眼镜',
    'cat_alias_name' => '腕表珠宝',
    'cat_icon' => '',
    'style_icon' => 'watch',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=864',
    'child_tree' => 
    array (
      957 => 
      array (
        'id' => '957',
        'name' => '珠宝',
        'url' => 'category.php?id=957',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          679 => 
          array (
            'id' => '679',
            'name' => '时尚饰品',
            'url' => 'category.php?id=679',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
              868 => 
              array (
                'id' => '868',
                'name' => '项链',
                'url' => 'category.php?id=868',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
              869 => 
              array (
                'id' => '869',
                'name' => '手链/脚链',
                'url' => 'category.php?id=869',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
              870 => 
              array (
                'id' => '870',
                'name' => '戒指',
                'url' => 'category.php?id=870',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
              871 => 
              array (
                'id' => '871',
                'name' => '耳饰',
                'url' => 'category.php?id=871',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
              872 => 
              array (
                'id' => '872',
                'name' => '头饰',
                'url' => 'category.php?id=872',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
              873 => 
              array (
                'id' => '873',
                'name' => '胸针',
                'url' => 'category.php?id=873',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
              874 => 
              array (
                'id' => '874',
                'name' => '婚庆饰品',
                'url' => 'category.php?id=874',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
              875 => 
              array (
                'id' => '875',
                'name' => '饰品配件',
                'url' => 'category.php?id=875',
                'level' => 3,
                'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
                'cat_id' => 
                array (
                ),
              ),
            ),
          ),
          682 => 
          array (
            'id' => '682',
            'name' => '纯金K金饰品',
            'url' => 'category.php?id=682',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          683 => 
          array (
            'id' => '683',
            'name' => '金银投资',
            'url' => 'category.php?id=683',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          684 => 
          array (
            'id' => '684',
            'name' => '银饰',
            'url' => 'category.php?id=684',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          685 => 
          array (
            'id' => '685',
            'name' => '水晶玛瑙',
            'url' => 'category.php?id=685',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          686 => 
          array (
            'id' => '686',
            'name' => '彩宝',
            'url' => 'category.php?id=686',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          688 => 
          array (
            'id' => '688',
            'name' => '天然木饰',
            'url' => 'category.php?id=688',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          958 => 
          array (
            'id' => '958',
            'name' => '黄金吊坠',
            'url' => 'category.php?id=958',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          959 => 
          array (
            'id' => '959',
            'name' => '钻石吊坠',
            'url' => 'category.php?id=959',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          960 => 
          array (
            'id' => '960',
            'name' => '白金吊坠',
            'url' => 'category.php?id=960',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          961 => 
          array (
            'id' => '961',
            'name' => '黄金项链',
            'url' => 'category.php?id=961',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          962 => 
          array (
            'id' => '962',
            'name' => '翡翠玉石',
            'url' => 'category.php?id=962',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          963 => 
          array (
            'id' => '963',
            'name' => '黄金戒指',
            'url' => 'category.php?id=963',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          964 => 
          array (
            'id' => '964',
            'name' => '钻石',
            'url' => 'category.php?id=964',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          965 => 
          array (
            'id' => '965',
            'name' => '手镯',
            'url' => 'category.php?id=965',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          974 => 
          array (
            'id' => '974',
            'name' => '彩金',
            'url' => 'category.php?id=974',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          975 => 
          array (
            'id' => '975',
            'name' => '铂金',
            'url' => 'category.php?id=975',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          976 => 
          array (
            'id' => '976',
            'name' => '珍珠',
            'url' => 'category.php?id=976',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1193 => 
          array (
            'id' => '1193',
            'name' => '坦桑石',
            'url' => 'category.php?id=1193',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1194 => 
          array (
            'id' => '1194',
            'name' => '玉镯',
            'url' => 'category.php?id=1194',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1195 => 
          array (
            'id' => '1195',
            'name' => '结婚钻戒',
            'url' => 'category.php?id=1195',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1196 => 
          array (
            'id' => '1196',
            'name' => '玛瑙项链',
            'url' => 'category.php?id=1196',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1246 => 
          array (
            'id' => '1246',
            'name' => '珠宝',
            'url' => 'category.php?id=1246',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1247 => 
          array (
            'id' => '1247',
            'name' => '转运珠',
            'url' => 'category.php?id=1247',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      966 => 
      array (
        'id' => '966',
        'name' => '饰品',
        'url' => 'category.php?id=966',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          967 => 
          array (
            'id' => '967',
            'name' => '时尚饰品',
            'url' => 'category.php?id=967',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          968 => 
          array (
            'id' => '968',
            'name' => '木手串',
            'url' => 'category.php?id=968',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          969 => 
          array (
            'id' => '969',
            'name' => '银手镯',
            'url' => 'category.php?id=969',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          970 => 
          array (
            'id' => '970',
            'name' => '佛珠',
            'url' => 'category.php?id=970',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          971 => 
          array (
            'id' => '971',
            'name' => '星月菩提',
            'url' => 'category.php?id=971',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          972 => 
          array (
            'id' => '972',
            'name' => '纯银饰品',
            'url' => 'category.php?id=972',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          973 => 
          array (
            'id' => '973',
            'name' => '施华洛世奇',
            'url' => 'category.php?id=973',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1197 => 
          array (
            'id' => '1197',
            'name' => '石榴石手链',
            'url' => 'category.php?id=1197',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1198 => 
          array (
            'id' => '1198',
            'name' => '开口戒指',
            'url' => 'category.php?id=1198',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1199 => 
          array (
            'id' => '1199',
            'name' => '胸针',
            'url' => 'category.php?id=1199',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1200 => 
          array (
            'id' => '1200',
            'name' => '项坠',
            'url' => 'category.php?id=1200',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1201 => 
          array (
            'id' => '1201',
            'name' => '耳环',
            'url' => 'category.php?id=1201',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1202 => 
          array (
            'id' => '1202',
            'name' => '耳饰',
            'url' => 'category.php?id=1202',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1203 => 
          array (
            'id' => '1203',
            'name' => 'PH7',
            'url' => 'category.php?id=1203',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1204 => 
          array (
            'id' => '1204',
            'name' => 'Monchhichi',
            'url' => 'category.php?id=1204',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1248 => 
          array (
            'id' => '1248',
            'name' => '宝宝银饰',
            'url' => 'category.php?id=1248',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1249 => 
          array (
            'id' => '1249',
            'name' => '发饰',
            'url' => 'category.php?id=1249',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1250 => 
          array (
            'id' => '1250',
            'name' => '伊泰莲娜',
            'url' => 'category.php?id=1250',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1251 => 
      array (
        'id' => '1251',
        'name' => '腕表',
        'url' => 'category.php?id=1251',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1253 => 
          array (
            'id' => '1253',
            'name' => '大牌手表',
            'url' => 'category.php?id=1253',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1254 => 
          array (
            'id' => '1254',
            'name' => '国货精表',
            'url' => 'category.php?id=1254',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1255 => 
          array (
            'id' => '1255',
            'name' => '瑞士名表',
            'url' => 'category.php?id=1255',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1256 => 
          array (
            'id' => '1256',
            'name' => '欧美手表',
            'url' => 'category.php?id=1256',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1257 => 
          array (
            'id' => '1257',
            'name' => '经典国表',
            'url' => 'category.php?id=1257',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1258 => 
          array (
            'id' => '1258',
            'name' => '日韩港表',
            'url' => 'category.php?id=1258',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1259 => 
          array (
            'id' => '1259',
            'name' => '男表',
            'url' => 'category.php?id=1259',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1260 => 
          array (
            'id' => '1260',
            'name' => '女表',
            'url' => 'category.php?id=1260',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1261 => 
          array (
            'id' => '1261',
            'name' => '情侣表',
            'url' => 'category.php?id=1261',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1262 => 
          array (
            'id' => '1262',
            'name' => '儿童手表',
            'url' => 'category.php?id=1262',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1263 => 
          array (
            'id' => '1263',
            'name' => '机械表',
            'url' => 'category.php?id=1263',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1264 => 
          array (
            'id' => '1264',
            'name' => '石英表',
            'url' => 'category.php?id=1264',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1265 => 
          array (
            'id' => '1265',
            'name' => '光能表',
            'url' => 'category.php?id=1265',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1266 => 
          array (
            'id' => '1266',
            'name' => '防水表',
            'url' => 'category.php?id=1266',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1267 => 
          array (
            'id' => '1267',
            'name' => '运动表',
            'url' => 'category.php?id=1267',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1268 => 
          array (
            'id' => '1268',
            'name' => '学生表',
            'url' => 'category.php?id=1268',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1269 => 
          array (
            'id' => '1269',
            'name' => '军表',
            'url' => 'category.php?id=1269',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1270 => 
          array (
            'id' => '1270',
            'name' => '陶瓷表',
            'url' => 'category.php?id=1270',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1252 => 
      array (
        'id' => '1252',
        'name' => '眼镜',
        'url' => 'category.php?id=1252',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1271 => 
          array (
            'id' => '1271',
            'name' => '眼镜',
            'url' => 'category.php?id=1271',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1272 => 
          array (
            'id' => '1272',
            'name' => '太阳镜',
            'url' => 'category.php?id=1272',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1273 => 
          array (
            'id' => '1273',
            'name' => '眼镜框',
            'url' => 'category.php?id=1273',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1274 => 
          array (
            'id' => '1274',
            'name' => '配镜',
            'url' => 'category.php?id=1274',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1275 => 
          array (
            'id' => '1275',
            'name' => '防辐射眼镜',
            'url' => 'category.php?id=1275',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1276 => 
          array (
            'id' => '1276',
            'name' => '驾驶镜',
            'url' => 'category.php?id=1276',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1277 => 
          array (
            'id' => '1277',
            'name' => '运动眼镜',
            'url' => 'category.php?id=1277',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1278 => 
          array (
            'id' => '1278',
            'name' => '老花镜',
            'url' => 'category.php?id=1278',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1279 => 
          array (
            'id' => '1279',
            'name' => '3D眼镜',
            'url' => 'category.php?id=1279',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1280 => 
          array (
            'id' => '1280',
            'name' => '派对眼镜',
            'url' => 'category.php?id=1280',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1281 => 
          array (
            'id' => '1281',
            'name' => '电子烟',
            'url' => 'category.php?id=1281',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1282 => 
          array (
            'id' => '1282',
            'name' => '替烟',
            'url' => 'category.php?id=1282',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1283 => 
          array (
            'id' => '1283',
            'name' => '烟嘴',
            'url' => 'category.php?id=1283',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1284 => 
          array (
            'id' => '1284',
            'name' => '打火机',
            'url' => 'category.php?id=1284',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1285 => 
          array (
            'id' => '1285',
            'name' => '瑞士军刀',
            'url' => 'category.php?id=1285',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1286 => 
          array (
            'id' => '1286',
            'name' => '宝岛眼镜',
            'url' => 'category.php?id=1286',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1287 => 
          array (
            'id' => '1287',
            'name' => '雷朋',
            'url' => 'category.php?id=1287',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  861 => 
  array (
    'cat_id' => '861',
    'cat_name' => '汽车、汽车用品',
    'cat_alias_name' => '汽车汽配',
    'cat_icon' => '',
    'style_icon' => 'car',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=861',
    'child_tree' => 
    array (
      1527 => 
      array (
        'id' => '1527',
        'name' => '代步车',
        'url' => 'category.php?id=1527',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1528 => 
          array (
            'id' => '1528',
            'name' => '自行车',
            'url' => 'category.php?id=1528',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1529 => 
          array (
            'id' => '1529',
            'name' => '电动车',
            'url' => 'category.php?id=1529',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1530 => 
          array (
            'id' => '1530',
            'name' => '老人车',
            'url' => 'category.php?id=1530',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1531 => 
          array (
            'id' => '1531',
            'name' => '三轮车',
            'url' => 'category.php?id=1531',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      985 => 
      array (
        'id' => '985',
        'name' => '汽车品牌',
        'url' => 'category.php?id=985',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          986 => 
          array (
            'id' => '986',
            'name' => '上海大众',
            'url' => 'category.php?id=986',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          987 => 
          array (
            'id' => '987',
            'name' => '斯柯达',
            'url' => 'category.php?id=987',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          988 => 
          array (
            'id' => '988',
            'name' => '别克君威',
            'url' => 'category.php?id=988',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          989 => 
          array (
            'id' => '989',
            'name' => '丰田',
            'url' => 'category.php?id=989',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          990 => 
          array (
            'id' => '990',
            'name' => '哈弗',
            'url' => 'category.php?id=990',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          991 => 
          array (
            'id' => '991',
            'name' => '福特',
            'url' => 'category.php?id=991',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          992 => 
          array (
            'id' => '992',
            'name' => '东风雪铁龙',
            'url' => 'category.php?id=992',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          993 => 
          array (
            'id' => '993',
            'name' => '一汽奔腾',
            'url' => 'category.php?id=993',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          994 => 
          array (
            'id' => '994',
            'name' => '海马',
            'url' => 'category.php?id=994',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1012 => 
      array (
        'id' => '1012',
        'name' => '汽车装饰',
        'url' => 'category.php?id=1012',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1013 => 
          array (
            'id' => '1013',
            'name' => '脚垫',
            'url' => 'category.php?id=1013',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1014 => 
          array (
            'id' => '1014',
            'name' => '坐垫',
            'url' => 'category.php?id=1014',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1015 => 
          array (
            'id' => '1015',
            'name' => '座套',
            'url' => 'category.php?id=1015',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1016 => 
          array (
            'id' => '1016',
            'name' => '空气净化',
            'url' => 'category.php?id=1016',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1023 => 
          array (
            'id' => '1023',
            'name' => '储物箱',
            'url' => 'category.php?id=1023',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      977 => 
      array (
        'id' => '977',
        'name' => '汽车车型',
        'url' => 'category.php?id=977',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          978 => 
          array (
            'id' => '978',
            'name' => '微型车',
            'url' => 'category.php?id=978',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          979 => 
          array (
            'id' => '979',
            'name' => '小型车',
            'url' => 'category.php?id=979',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          980 => 
          array (
            'id' => '980',
            'name' => '紧凑车型',
            'url' => 'category.php?id=980',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          981 => 
          array (
            'id' => '981',
            'name' => '中型车',
            'url' => 'category.php?id=981',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          982 => 
          array (
            'id' => '982',
            'name' => '豪华车',
            'url' => 'category.php?id=982',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          983 => 
          array (
            'id' => '983',
            'name' => '跑车',
            'url' => 'category.php?id=983',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          984 => 
          array (
            'id' => '984',
            'name' => 'SUV',
            'url' => 'category.php?id=984',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      995 => 
      array (
        'id' => '995',
        'name' => '维修保养',
        'url' => 'category.php?id=995',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          996 => 
          array (
            'id' => '996',
            'name' => '润滑油',
            'url' => 'category.php?id=996',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          997 => 
          array (
            'id' => '997',
            'name' => '防冻剂',
            'url' => 'category.php?id=997',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          998 => 
          array (
            'id' => '998',
            'name' => '滤清器',
            'url' => 'category.php?id=998',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          999 => 
          array (
            'id' => '999',
            'name' => '火花塞',
            'url' => 'category.php?id=999',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1297 => 
          array (
            'id' => '1297',
            'name' => '雨刷',
            'url' => 'category.php?id=1297',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1298 => 
          array (
            'id' => '1298',
            'name' => '车灯',
            'url' => 'category.php?id=1298',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1299 => 
          array (
            'id' => '1299',
            'name' => '后视镜',
            'url' => 'category.php?id=1299',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1300 => 
          array (
            'id' => '1300',
            'name' => '轮胎',
            'url' => 'category.php?id=1300',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1301 => 
          array (
            'id' => '1301',
            'name' => '刹车盘/片',
            'url' => 'category.php?id=1301',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1302 => 
          array (
            'id' => '1302',
            'name' => '维修配件',
            'url' => 'category.php?id=1302',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1303 => 
          array (
            'id' => '1303',
            'name' => '蓄电池',
            'url' => 'category.php?id=1303',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1304 => 
          array (
            'id' => '1304',
            'name' => '底盘装甲/护板',
            'url' => 'category.php?id=1304',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1305 => 
          array (
            'id' => '1305',
            'name' => '贴膜',
            'url' => 'category.php?id=1305',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1306 => 
          array (
            'id' => '1306',
            'name' => '汽车工具',
            'url' => 'category.php?id=1306',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1307 => 
          array (
            'id' => '1307',
            'name' => '改装配件',
            'url' => 'category.php?id=1307',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1000 => 
      array (
        'id' => '1000',
        'name' => '车载电器',
        'url' => 'category.php?id=1000',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1001 => 
          array (
            'id' => '1001',
            'name' => '导航仪',
            'url' => 'category.php?id=1001',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1002 => 
          array (
            'id' => '1002',
            'name' => '倒车仪器',
            'url' => 'category.php?id=1002',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1003 => 
          array (
            'id' => '1003',
            'name' => '行车记录仪',
            'url' => 'category.php?id=1003',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1004 => 
          array (
            'id' => '1004',
            'name' => '净化器',
            'url' => 'category.php?id=1004',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1005 => 
          array (
            'id' => '1005',
            'name' => '蓝牙',
            'url' => 'category.php?id=1005',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1308 => 
          array (
            'id' => '1308',
            'name' => '安全预警仪',
            'url' => 'category.php?id=1308',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1309 => 
          array (
            'id' => '1309',
            'name' => '倒车雷达',
            'url' => 'category.php?id=1309',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1310 => 
          array (
            'id' => '1310',
            'name' => '智能驾驶',
            'url' => 'category.php?id=1310',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1311 => 
          array (
            'id' => '1311',
            'name' => '车载电台',
            'url' => 'category.php?id=1311',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1312 => 
          array (
            'id' => '1312',
            'name' => '吸尘器',
            'url' => 'category.php?id=1312',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1313 => 
          array (
            'id' => '1313',
            'name' => '冰箱',
            'url' => 'category.php?id=1313',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1314 => 
          array (
            'id' => '1314',
            'name' => '电源',
            'url' => 'category.php?id=1314',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1006 => 
      array (
        'id' => '1006',
        'name' => '美容清洗',
        'url' => 'category.php?id=1006',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1007 => 
          array (
            'id' => '1007',
            'name' => '车蜡',
            'url' => 'category.php?id=1007',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1008 => 
          array (
            'id' => '1008',
            'name' => '补漆笔',
            'url' => 'category.php?id=1008',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1009 => 
          array (
            'id' => '1009',
            'name' => '玻璃水',
            'url' => 'category.php?id=1009',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1010 => 
          array (
            'id' => '1010',
            'name' => '清洁器',
            'url' => 'category.php?id=1010',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1011 => 
          array (
            'id' => '1011',
            'name' => '洗车工具',
            'url' => 'category.php?id=1011',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1017 => 
      array (
        'id' => '1017',
        'name' => '安全自驾',
        'url' => 'category.php?id=1017',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1018 => 
          array (
            'id' => '1018',
            'name' => '胎压监测',
            'url' => 'category.php?id=1018',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1019 => 
          array (
            'id' => '1019',
            'name' => '安全座椅',
            'url' => 'category.php?id=1019',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1020 => 
          array (
            'id' => '1020',
            'name' => '充气泵',
            'url' => 'category.php?id=1020',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1021 => 
          array (
            'id' => '1021',
            'name' => '防盗设备',
            'url' => 'category.php?id=1021',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1022 => 
          array (
            'id' => '1022',
            'name' => '保温箱',
            'url' => 'category.php?id=1022',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1024 => 
      array (
        'id' => '1024',
        'name' => '线下服务',
        'url' => 'category.php?id=1024',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1025 => 
          array (
            'id' => '1025',
            'name' => '清洗美容服务',
            'url' => 'category.php?id=1025',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1026 => 
          array (
            'id' => '1026',
            'name' => '功能升级服务',
            'url' => 'category.php?id=1026',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1027 => 
          array (
            'id' => '1027',
            'name' => '保养维系服务',
            'url' => 'category.php?id=1027',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1489 => 
          array (
            'id' => '1489',
            'name' => '拖车吊车',
            'url' => 'category.php?id=1489',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1288 => 
      array (
        'id' => '1288',
        'name' => '汽车价格',
        'url' => 'category.php?id=1288',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1289 => 
          array (
            'id' => '1289',
            'name' => '5万以下',
            'url' => 'category.php?id=1289',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1290 => 
          array (
            'id' => '1290',
            'name' => '5-8万',
            'url' => 'category.php?id=1290',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1291 => 
          array (
            'id' => '1291',
            'name' => '8-10万',
            'url' => 'category.php?id=1291',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1292 => 
          array (
            'id' => '1292',
            'name' => '10-15万',
            'url' => 'category.php?id=1292',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1293 => 
          array (
            'id' => '1293',
            'name' => '15-20万',
            'url' => 'category.php?id=1293',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1294 => 
          array (
            'id' => '1294',
            'name' => '20-25万',
            'url' => 'category.php?id=1294',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1295 => 
          array (
            'id' => '1295',
            'name' => '25-40万',
            'url' => 'category.php?id=1295',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1296 => 
          array (
            'id' => '1296',
            'name' => '40万以上',
            'url' => 'category.php?id=1296',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1490 => 
      array (
        'id' => '1490',
        'name' => '二手车',
        'url' => 'category.php?id=1490',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
        ),
      ),
      1524 => 
      array (
        'id' => '1524',
        'name' => '工程车',
        'url' => 'category.php?id=1524',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
        ),
      ),
      1525 => 
      array (
        'id' => '1525',
        'name' => '汽车保险',
        'url' => 'category.php?id=1525',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
        ),
      ),
      1532 => 
      array (
        'id' => '1532',
        'name' => '紧急救援',
        'url' => 'category.php?id=1532',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
        ),
      ),
    ),
  ),
  12 => 
  array (
    'cat_id' => '12',
    'cat_name' => '食品、酒类、生鲜、特产',
    'cat_alias_name' => '食品酒水',
    'cat_icon' => 'images/cat_icon/14586287506892.png',
    'style_icon' => 'food',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=12',
    'child_tree' => 
    array (
      616 => 
      array (
        'id' => '616',
        'name' => '进口食品',
        'url' => 'category.php?id=616',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          636 => 
          array (
            'id' => '636',
            'name' => '冲调饮品',
            'url' => 'category.php?id=636',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          632 => 
          array (
            'id' => '632',
            'name' => '牛奶',
            'url' => 'category.php?id=632',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          633 => 
          array (
            'id' => '633',
            'name' => '饼干蛋糕',
            'url' => 'category.php?id=633',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          634 => 
          array (
            'id' => '634',
            'name' => '糖果/巧克力',
            'url' => 'category.php?id=634',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          635 => 
          array (
            'id' => '635',
            'name' => '休闲零食',
            'url' => 'category.php?id=635',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          637 => 
          array (
            'id' => '637',
            'name' => '粮油调味',
            'url' => 'category.php?id=637',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      623 => 
      array (
        'id' => '623',
        'name' => '生鲜食品',
        'url' => 'category.php?id=623',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1415 => 
          array (
            'id' => '1415',
            'name' => '家禽',
            'url' => 'category.php?id=1415',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1416 => 
          array (
            'id' => '1416',
            'name' => '牛羊肉',
            'url' => 'category.php?id=1416',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1417 => 
          array (
            'id' => '1417',
            'name' => '蔬菜水果',
            'url' => 'category.php?id=1417',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1418 => 
          array (
            'id' => '1418',
            'name' => '海鲜干货',
            'url' => 'category.php?id=1418',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1419 => 
          array (
            'id' => '1419',
            'name' => '其他',
            'url' => 'category.php?id=1419',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      615 => 
      array (
        'id' => '615',
        'name' => '中外名酒',
        'url' => 'category.php?id=615',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          627 => 
          array (
            'id' => '627',
            'name' => '洋酒',
            'url' => 'category.php?id=627',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          625 => 
          array (
            'id' => '625',
            'name' => '白酒',
            'url' => 'category.php?id=625',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          626 => 
          array (
            'id' => '626',
            'name' => '葡萄酒',
            'url' => 'category.php?id=626',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          628 => 
          array (
            'id' => '628',
            'name' => '啤酒',
            'url' => 'category.php?id=628',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          629 => 
          array (
            'id' => '629',
            'name' => '黄酒/养生酒',
            'url' => 'category.php?id=629',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          631 => 
          array (
            'id' => '631',
            'name' => '收藏酒',
            'url' => 'category.php?id=631',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      620 => 
      array (
        'id' => '620',
        'name' => '茗茶',
        'url' => 'category.php?id=620',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          662 => 
          array (
            'id' => '662',
            'name' => '乌龙茶',
            'url' => 'category.php?id=662',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          656 => 
          array (
            'id' => '656',
            'name' => '铁观音',
            'url' => 'category.php?id=656',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          658 => 
          array (
            'id' => '658',
            'name' => '普洱',
            'url' => 'category.php?id=658',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          659 => 
          array (
            'id' => '659',
            'name' => '龙井',
            'url' => 'category.php?id=659',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          660 => 
          array (
            'id' => '660',
            'name' => '绿茶',
            'url' => 'category.php?id=660',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          661 => 
          array (
            'id' => '661',
            'name' => '红茶',
            'url' => 'category.php?id=661',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          667 => 
          array (
            'id' => '667',
            'name' => '花草茶',
            'url' => 'category.php?id=667',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      621 => 
      array (
        'id' => '621',
        'name' => '饮料冲调',
        'url' => 'category.php?id=621',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1432 => 
          array (
            'id' => '1432',
            'name' => '牛奶乳品',
            'url' => 'category.php?id=1432',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1433 => 
          array (
            'id' => '1433',
            'name' => '饮料',
            'url' => 'category.php?id=1433',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1434 => 
          array (
            'id' => '1434',
            'name' => '饮用水',
            'url' => 'category.php?id=1434',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1435 => 
          array (
            'id' => '1435',
            'name' => '咖啡/奶茶',
            'url' => 'category.php?id=1435',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1436 => 
          array (
            'id' => '1436',
            'name' => '成人奶粉',
            'url' => 'category.php?id=1436',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      617 => 
      array (
        'id' => '617',
        'name' => '休闲食品',
        'url' => 'category.php?id=617',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          638 => 
          array (
            'id' => '638',
            'name' => '休闲零食',
            'url' => 'category.php?id=638',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          639 => 
          array (
            'id' => '639',
            'name' => '坚果炒货',
            'url' => 'category.php?id=639',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          640 => 
          array (
            'id' => '640',
            'name' => '肉干肉脯',
            'url' => 'category.php?id=640',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          642 => 
          array (
            'id' => '642',
            'name' => '蜜饯果干',
            'url' => 'category.php?id=642',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          643 => 
          array (
            'id' => '643',
            'name' => '糖果/巧克力',
            'url' => 'category.php?id=643',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          644 => 
          array (
            'id' => '644',
            'name' => '饼干蛋糕',
            'url' => 'category.php?id=644',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          645 => 
          array (
            'id' => '645',
            'name' => '无糖食品',
            'url' => 'category.php?id=645',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      624 => 
      array (
        'id' => '624',
        'name' => '食品礼券',
        'url' => 'category.php?id=624',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1427 => 
          array (
            'id' => '1427',
            'name' => '瓜子',
            'url' => 'category.php?id=1427',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1428 => 
          array (
            'id' => '1428',
            'name' => '小面包',
            'url' => 'category.php?id=1428',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1429 => 
          array (
            'id' => '1429',
            'name' => '碧根果',
            'url' => 'category.php?id=1429',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1430 => 
          array (
            'id' => '1430',
            'name' => '坚果',
            'url' => 'category.php?id=1430',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1431 => 
          array (
            'id' => '1431',
            'name' => '卡券',
            'url' => 'category.php?id=1431',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  865 => 
  array (
    'cat_id' => '865',
    'cat_name' => '保健滋补、医药、成人',
    'cat_alias_name' => '营养滋补',
    'cat_icon' => '',
    'style_icon' => 'drug',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=865',
    'child_tree' => 
    array (
      1066 => 
      array (
        'id' => '1066',
        'name' => '保健品',
        'url' => 'category.php?id=1066',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1067 => 
          array (
            'id' => '1067',
            'name' => '玛咖片',
            'url' => 'category.php?id=1067',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1068 => 
          array (
            'id' => '1068',
            'name' => '营养补充剂',
            'url' => 'category.php?id=1068',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1069 => 
          array (
            'id' => '1069',
            'name' => '芦荟',
            'url' => 'category.php?id=1069',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1070 => 
          array (
            'id' => '1070',
            'name' => '鱼油',
            'url' => 'category.php?id=1070',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1071 => 
          array (
            'id' => '1071',
            'name' => '维生素C',
            'url' => 'category.php?id=1071',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1072 => 
          array (
            'id' => '1072',
            'name' => '保健食品',
            'url' => 'category.php?id=1072',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1073 => 
          array (
            'id' => '1073',
            'name' => '辅助降血脂',
            'url' => 'category.php?id=1073',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1360 => 
          array (
            'id' => '1360',
            'name' => '美容养颜',
            'url' => 'category.php?id=1360',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1361 => 
          array (
            'id' => '1361',
            'name' => '缓解体力疲劳',
            'url' => 'category.php?id=1361',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1362 => 
          array (
            'id' => '1362',
            'name' => '增加骨密度',
            'url' => 'category.php?id=1362',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1363 => 
          array (
            'id' => '1363',
            'name' => '护肝',
            'url' => 'category.php?id=1363',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1364 => 
          array (
            'id' => '1364',
            'name' => '减肥',
            'url' => 'category.php?id=1364',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1365 => 
          array (
            'id' => '1365',
            'name' => '通便',
            'url' => 'category.php?id=1365',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1366 => 
          array (
            'id' => '1366',
            'name' => '祛黄褐斑',
            'url' => 'category.php?id=1366',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1367 => 
          array (
            'id' => '1367',
            'name' => '减肥茶',
            'url' => 'category.php?id=1367',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1368 => 
          array (
            'id' => '1368',
            'name' => '酵素',
            'url' => 'category.php?id=1368',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1369 => 
          array (
            'id' => '1369',
            'name' => '左旋肉碱',
            'url' => 'category.php?id=1369',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1370 => 
          array (
            'id' => '1370',
            'name' => '增肌粉',
            'url' => 'category.php?id=1370',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1371 => 
          array (
            'id' => '1371',
            'name' => '安神补脑',
            'url' => 'category.php?id=1371',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1372 => 
          array (
            'id' => '1372',
            'name' => '维矿物质',
            'url' => 'category.php?id=1372',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1373 => 
          array (
            'id' => '1373',
            'name' => '调脾养胃',
            'url' => 'category.php?id=1373',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1074 => 
      array (
        'id' => '1074',
        'name' => '医药',
        'url' => 'category.php?id=1074',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1075 => 
          array (
            'id' => '1075',
            'name' => '感冒咳嗽',
            'url' => 'category.php?id=1075',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1076 => 
          array (
            'id' => '1076',
            'name' => '腰腿疼痛',
            'url' => 'category.php?id=1076',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1077 => 
          array (
            'id' => '1077',
            'name' => '妇科用药',
            'url' => 'category.php?id=1077',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1078 => 
          array (
            'id' => '1078',
            'name' => '男科用药',
            'url' => 'category.php?id=1078',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1079 => 
          array (
            'id' => '1079',
            'name' => '保健理疗',
            'url' => 'category.php?id=1079',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1080 => 
          array (
            'id' => '1080',
            'name' => '按摩器材',
            'url' => 'category.php?id=1080',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1081 => 
          array (
            'id' => '1081',
            'name' => '针灸拔罐',
            'url' => 'category.php?id=1081',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1082 => 
          array (
            'id' => '1082',
            'name' => '肠胃用药',
            'url' => 'category.php?id=1082',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1189 => 
          array (
            'id' => '1189',
            'name' => '云南白药',
            'url' => 'category.php?id=1189',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1190 => 
          array (
            'id' => '1190',
            'name' => '吗丁啉',
            'url' => 'category.php?id=1190',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1191 => 
          array (
            'id' => '1191',
            'name' => '阿莫西林胶囊',
            'url' => 'category.php?id=1191',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1192 => 
          array (
            'id' => '1192',
            'name' => '西瓜霜',
            'url' => 'category.php?id=1192',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1374 => 
          array (
            'id' => '1374',
            'name' => '体检套餐',
            'url' => 'category.php?id=1374',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1375 => 
          array (
            'id' => '1375',
            'name' => '隐形眼镜',
            'url' => 'category.php?id=1375',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1376 => 
          array (
            'id' => '1376',
            'name' => '肾宝片',
            'url' => 'category.php?id=1376',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1377 => 
          array (
            'id' => '1377',
            'name' => '皮肤瘙痒',
            'url' => 'category.php?id=1377',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1378 => 
          array (
            'id' => '1378',
            'name' => '儿科用药',
            'url' => 'category.php?id=1378',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1379 => 
          array (
            'id' => '1379',
            'name' => '解热镇痛',
            'url' => 'category.php?id=1379',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1083 => 
      array (
        'id' => '1083',
        'name' => '成人',
        'url' => 'category.php?id=1083',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1084 => 
          array (
            'id' => '1084',
            'name' => '避孕套',
            'url' => 'category.php?id=1084',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1085 => 
          array (
            'id' => '1085',
            'name' => '女用情趣',
            'url' => 'category.php?id=1085',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1086 => 
          array (
            'id' => '1086',
            'name' => '男用延时',
            'url' => 'category.php?id=1086',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1087 => 
          array (
            'id' => '1087',
            'name' => '成人用品',
            'url' => 'category.php?id=1087',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1088 => 
          array (
            'id' => '1088',
            'name' => '飞机杯',
            'url' => 'category.php?id=1088',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1089 => 
          array (
            'id' => '1089',
            'name' => '振动棒',
            'url' => 'category.php?id=1089',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1380 => 
      array (
        'id' => '1380',
        'name' => '营养补品',
        'url' => 'category.php?id=1380',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1381 => 
          array (
            'id' => '1381',
            'name' => '滋补养生',
            'url' => 'category.php?id=1381',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1382 => 
          array (
            'id' => '1382',
            'name' => '蜂蜜',
            'url' => 'category.php?id=1382',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1383 => 
          array (
            'id' => '1383',
            'name' => '石斛',
            'url' => 'category.php?id=1383',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1384 => 
          array (
            'id' => '1384',
            'name' => '阿胶',
            'url' => 'category.php?id=1384',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1385 => 
          array (
            'id' => '1385',
            'name' => '黑枸杞',
            'url' => 'category.php?id=1385',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1386 => 
          array (
            'id' => '1386',
            'name' => '冬虫夏草',
            'url' => 'category.php?id=1386',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1387 => 
          array (
            'id' => '1387',
            'name' => '青钱柳',
            'url' => 'category.php?id=1387',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1388 => 
          array (
            'id' => '1388',
            'name' => '西洋参',
            'url' => 'category.php?id=1388',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1389 => 
          array (
            'id' => '1389',
            'name' => '人参',
            'url' => 'category.php?id=1389',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1390 => 
          array (
            'id' => '1390',
            'name' => '藏红花',
            'url' => 'category.php?id=1390',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1391 => 
          array (
            'id' => '1391',
            'name' => '蜂胶',
            'url' => 'category.php?id=1391',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1392 => 
          array (
            'id' => '1392',
            'name' => '阿胶膏',
            'url' => 'category.php?id=1392',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1393 => 
          array (
            'id' => '1393',
            'name' => '黄芪',
            'url' => 'category.php?id=1393',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1394 => 
          array (
            'id' => '1394',
            'name' => '阿胶块',
            'url' => 'category.php?id=1394',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1395 => 
          array (
            'id' => '1395',
            'name' => '燕窝',
            'url' => 'category.php?id=1395',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1396 => 
          array (
            'id' => '1396',
            'name' => '铁皮枫斗',
            'url' => 'category.php?id=1396',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1397 => 
          array (
            'id' => '1397',
            'name' => '三七粉',
            'url' => 'category.php?id=1397',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1398 => 
          array (
            'id' => '1398',
            'name' => '蜂王浆',
            'url' => 'category.php?id=1398',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1399 => 
          array (
            'id' => '1399',
            'name' => '雪蛤',
            'url' => 'category.php?id=1399',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1400 => 
          array (
            'id' => '1400',
            'name' => '山药',
            'url' => 'category.php?id=1400',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1401 => 
          array (
            'id' => '1401',
            'name' => '当归',
            'url' => 'category.php?id=1401',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1402 => 
          array (
            'id' => '1402',
            'name' => '鹿茸',
            'url' => 'category.php?id=1402',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  867 => 
  array (
    'cat_id' => '867',
    'cat_name' => '礼品、卡、旅游、充值',
    'cat_alias_name' => '礼品卡券',
    'cat_icon' => '',
    'style_icon' => 'package',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=867',
    'child_tree' => 
    array (
      1090 => 
      array (
        'id' => '1090',
        'name' => '通讯充值',
        'url' => 'category.php?id=1090',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1091 => 
          array (
            'id' => '1091',
            'name' => '手机话费充值',
            'url' => 'category.php?id=1091',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1092 => 
          array (
            'id' => '1092',
            'name' => '手机流量充值',
            'url' => 'category.php?id=1092',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1093 => 
      array (
        'id' => '1093',
        'name' => '游戏',
        'url' => 'category.php?id=1093',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1094 => 
          array (
            'id' => '1094',
            'name' => 'QQ充值',
            'url' => 'category.php?id=1094',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1095 => 
          array (
            'id' => '1095',
            'name' => '游戏点卡',
            'url' => 'category.php?id=1095',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1096 => 
          array (
            'id' => '1096',
            'name' => '网页游戏',
            'url' => 'category.php?id=1096',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1097 => 
          array (
            'id' => '1097',
            'name' => '手机游戏',
            'url' => 'category.php?id=1097',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1098 => 
          array (
            'id' => '1098',
            'name' => '单机游戏',
            'url' => 'category.php?id=1098',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1099 => 
      array (
        'id' => '1099',
        'name' => '机票预订',
        'url' => 'category.php?id=1099',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1100 => 
          array (
            'id' => '1100',
            'name' => '国内机票',
            'url' => 'category.php?id=1100',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1101 => 
          array (
            'id' => '1101',
            'name' => '国际机票',
            'url' => 'category.php?id=1101',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1102 => 
      array (
        'id' => '1102',
        'name' => '酒店预订',
        'url' => 'category.php?id=1102',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1103 => 
          array (
            'id' => '1103',
            'name' => '国内酒店',
            'url' => 'category.php?id=1103',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1104 => 
          array (
            'id' => '1104',
            'name' => '国际酒店',
            'url' => 'category.php?id=1104',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1403 => 
      array (
        'id' => '1403',
        'name' => '教育培训',
        'url' => 'category.php?id=1403',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1405 => 
          array (
            'id' => '1405',
            'name' => '早教幼教',
            'url' => 'category.php?id=1405',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1406 => 
          array (
            'id' => '1406',
            'name' => '艺术培训',
            'url' => 'category.php?id=1406',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1407 => 
          array (
            'id' => '1407',
            'name' => '语言培训',
            'url' => 'category.php?id=1407',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1408 => 
          array (
            'id' => '1408',
            'name' => '网络课程',
            'url' => 'category.php?id=1408',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1409 => 
          array (
            'id' => '1409',
            'name' => '学习培训',
            'url' => 'category.php?id=1409',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1410 => 
          array (
            'id' => '1410',
            'name' => '其它',
            'url' => 'category.php?id=1410',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1404 => 
      array (
        'id' => '1404',
        'name' => '商旅服务',
        'url' => 'category.php?id=1404',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1411 => 
          array (
            'id' => '1411',
            'name' => '企业差旅',
            'url' => 'category.php?id=1411',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1412 => 
          array (
            'id' => '1412',
            'name' => '奖励旅游',
            'url' => 'category.php?id=1412',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1413 => 
          array (
            'id' => '1413',
            'name' => '会议周边',
            'url' => 'category.php?id=1413',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1414 => 
          array (
            'id' => '1414',
            'name' => '会议活动',
            'url' => 'category.php?id=1414',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  1476 => 
  array (
    'cat_id' => '1476',
    'cat_name' => '房屋租售',
    'cat_alias_name' => '',
    'cat_icon' => '',
    'style_icon' => 'other',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=1476',
    'child_tree' => 
    array (
      1478 => 
      array (
        'id' => '1478',
        'name' => '房屋售卖',
        'url' => 'category.php?id=1478',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1533 => 
          array (
            'id' => '1533',
            'name' => '一手房源',
            'url' => 'category.php?id=1533',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1534 => 
          array (
            'id' => '1534',
            'name' => '二手房源',
            'url' => 'category.php?id=1534',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1535 => 
          array (
            'id' => '1535',
            'name' => '中介',
            'url' => 'category.php?id=1535',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1536 => 
          array (
            'id' => '1536',
            'name' => '特卖',
            'url' => 'category.php?id=1536',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1479 => 
      array (
        'id' => '1479',
        'name' => '房屋出租',
        'url' => 'category.php?id=1479',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1537 => 
          array (
            'id' => '1537',
            'name' => '自家出租',
            'url' => 'category.php?id=1537',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1538 => 
          array (
            'id' => '1538',
            'name' => '中介',
            'url' => 'category.php?id=1538',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1539 => 
          array (
            'id' => '1539',
            'name' => '转租',
            'url' => 'category.php?id=1539',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1540 => 
          array (
            'id' => '1540',
            'name' => '合租',
            'url' => 'category.php?id=1540',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1480 => 
      array (
        'id' => '1480',
        'name' => '商铺租售',
        'url' => 'category.php?id=1480',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1541 => 
          array (
            'id' => '1541',
            'name' => '整栋出租',
            'url' => 'category.php?id=1541',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1542 => 
          array (
            'id' => '1542',
            'name' => '临街旺铺',
            'url' => 'category.php?id=1542',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1481 => 
      array (
        'id' => '1481',
        'name' => '地皮租售',
        'url' => 'category.php?id=1481',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1543 => 
          array (
            'id' => '1543',
            'name' => '建筑地皮',
            'url' => 'category.php?id=1543',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1544 => 
          array (
            'id' => '1544',
            'name' => '农用地皮',
            'url' => 'category.php?id=1544',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1545 => 
          array (
            'id' => '1545',
            'name' => '商用地皮',
            'url' => 'category.php?id=1545',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  1477 => 
  array (
    'cat_id' => '1477',
    'cat_name' => '饮食娱乐',
    'cat_alias_name' => '',
    'cat_icon' => '',
    'style_icon' => 'other',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=1477',
    'child_tree' => 
    array (
      1488 => 
      array (
        'id' => '1488',
        'name' => '吃喝专区',
        'url' => 'category.php?id=1488',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1492 => 
          array (
            'id' => '1492',
            'name' => '小吃快餐',
            'url' => 'category.php?id=1492',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1493 => 
          array (
            'id' => '1493',
            'name' => '自助餐',
            'url' => 'category.php?id=1493',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1494 => 
          array (
            'id' => '1494',
            'name' => '火锅外卖',
            'url' => 'category.php?id=1494',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1495 => 
          array (
            'id' => '1495',
            'name' => '香锅烤鱼',
            'url' => 'category.php?id=1495',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1496 => 
          array (
            'id' => '1496',
            'name' => '甜点饮品',
            'url' => 'category.php?id=1496',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1497 => 
          array (
            'id' => '1497',
            'name' => '聚会包厢',
            'url' => 'category.php?id=1497',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1498 => 
          array (
            'id' => '1498',
            'name' => '婚庆',
            'url' => 'category.php?id=1498',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1483 => 
      array (
        'id' => '1483',
        'name' => '休闲娱乐',
        'url' => 'category.php?id=1483',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1499 => 
          array (
            'id' => '1499',
            'name' => '夜店酒吧',
            'url' => 'category.php?id=1499',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1501 => 
          array (
            'id' => '1501',
            'name' => 'K歌',
            'url' => 'category.php?id=1501',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1503 => 
          array (
            'id' => '1503',
            'name' => '洗浴汗蒸',
            'url' => 'category.php?id=1503',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1505 => 
          array (
            'id' => '1505',
            'name' => '网吧网咖',
            'url' => 'category.php?id=1505',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1506 => 
          array (
            'id' => '1506',
            'name' => '茶馆',
            'url' => 'category.php?id=1506',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1487 => 
      array (
        'id' => '1487',
        'name' => '美丽专区',
        'url' => 'category.php?id=1487',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1508 => 
          array (
            'id' => '1508',
            'name' => '美容美发',
            'url' => 'category.php?id=1508',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1510 => 
          array (
            'id' => '1510',
            'name' => '洗浴汗蒸',
            'url' => 'category.php?id=1510',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1511 => 
          array (
            'id' => '1511',
            'name' => '化妆美甲',
            'url' => 'category.php?id=1511',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1485 => 
      array (
        'id' => '1485',
        'name' => '运动健身',
        'url' => 'category.php?id=1485',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1512 => 
          array (
            'id' => '1512',
            'name' => '健身器材',
            'url' => 'category.php?id=1512',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1513 => 
          array (
            'id' => '1513',
            'name' => '健身馆',
            'url' => 'category.php?id=1513',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1514 => 
          array (
            'id' => '1514',
            'name' => '运动馆',
            'url' => 'category.php?id=1514',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1515 => 
          array (
            'id' => '1515',
            'name' => '儿童馆',
            'url' => 'category.php?id=1515',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1486 => 
      array (
        'id' => '1486',
        'name' => '理疗护理',
        'url' => 'category.php?id=1486',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1516 => 
          array (
            'id' => '1516',
            'name' => '足浴按摩',
            'url' => 'category.php?id=1516',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1517 => 
          array (
            'id' => '1517',
            'name' => '理疗护理',
            'url' => 'category.php?id=1517',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
      1518 => 
      array (
        'id' => '1518',
        'name' => '约赛',
        'url' => 'category.php?id=1518',
        'level' => 1,
        'select' => '&nbsp;&nbsp;&nbsp;&nbsp;',
        'cat_id' => 
        array (
          1519 => 
          array (
            'id' => '1519',
            'name' => '足球PK',
            'url' => 'category.php?id=1519',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1520 => 
          array (
            'id' => '1520',
            'name' => '篮球比PK',
            'url' => 'category.php?id=1520',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1521 => 
          array (
            'id' => '1521',
            'name' => '羽毛球PK',
            'url' => 'category.php?id=1521',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
          1522 => 
          array (
            'id' => '1522',
            'name' => '其他',
            'url' => 'category.php?id=1522',
            'level' => 2,
            'select' => '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;',
            'cat_id' => 
            array (
            ),
          ),
        ),
      ),
    ),
  ),
  1546 => 
  array (
    'cat_id' => '1546',
    'cat_name' => '餐券',
    'cat_alias_name' => '餐券',
    'cat_icon' => '',
    'style_icon' => 'outdoors',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=1546',
    'child_tree' => 
    array (
    ),
  ),
  1547 => 
  array (
    'cat_id' => '1547',
    'cat_name' => '住宿',
    'cat_alias_name' => '住宿',
    'cat_icon' => '',
    'style_icon' => 'bed',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=1547',
    'child_tree' => 
    array (
    ),
  ),
  1548 => 
  array (
    'cat_id' => '1548',
    'cat_name' => '中油卡',
    'cat_alias_name' => '中油卡',
    'cat_icon' => '',
    'style_icon' => 'car',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=1548',
    'child_tree' => 
    array (
    ),
  ),
  1549 => 
  array (
    'cat_id' => '1549',
    'cat_name' => '票劵',
    'cat_alias_name' => '票劵',
    'cat_icon' => '',
    'style_icon' => 'food',
    'level' => 0,
    'select' => '',
    'url' => 'category.php?id=1549',
    'child_tree' => 
    array (
    ),
  ),
);
?>