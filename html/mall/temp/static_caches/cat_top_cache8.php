<?php
$data = array (
  362 => 
  array (
    'id' => '362',
    'name' => '奢侈品',
    'url' => 'category.php?id=362',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      443 => 
      array (
        'id' => '443',
        'name' => '鞋靴',
        'url' => 'category.php?id=443',
      ),
      439 => 
      array (
        'id' => '439',
        'name' => '箱包',
        'url' => 'category.php?id=439',
      ),
      440 => 
      array (
        'id' => '440',
        'name' => '钱包',
        'url' => 'category.php?id=440',
      ),
      441 => 
      array (
        'id' => '441',
        'name' => '服饰',
        'url' => 'category.php?id=441',
      ),
      442 => 
      array (
        'id' => '442',
        'name' => '腰带',
        'url' => 'category.php?id=442',
      ),
      445 => 
      array (
        'id' => '445',
        'name' => '太阳镜/眼镜框',
        'url' => 'category.php?id=445',
      ),
      446 => 
      array (
        'id' => '446',
        'name' => '饰品',
        'url' => 'category.php?id=446',
      ),
      447 => 
      array (
        'id' => '447',
        'name' => '配件',
        'url' => 'category.php?id=447',
      ),
    ),
  ),
  360 => 
  array (
    'id' => '360',
    'name' => '功能箱包',
    'url' => 'category.php?id=360',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      427 => 
      array (
        'id' => '427',
        'name' => '拉杆箱/拉杆包',
        'url' => 'category.php?id=427',
      ),
      428 => 
      array (
        'id' => '428',
        'name' => '旅行包',
        'url' => 'category.php?id=428',
      ),
      430 => 
      array (
        'id' => '430',
        'name' => '电脑包',
        'url' => 'category.php?id=430',
      ),
      432 => 
      array (
        'id' => '432',
        'name' => '休闲运动包',
        'url' => 'category.php?id=432',
      ),
      433 => 
      array (
        'id' => '433',
        'name' => '相机包',
        'url' => 'category.php?id=433',
      ),
      434 => 
      array (
        'id' => '434',
        'name' => '腰包/胸包',
        'url' => 'category.php?id=434',
      ),
      435 => 
      array (
        'id' => '435',
        'name' => '登山包',
        'url' => 'category.php?id=435',
      ),
      436 => 
      array (
        'id' => '436',
        'name' => '旅行配件',
        'url' => 'category.php?id=436',
      ),
      437 => 
      array (
        'id' => '437',
        'name' => '书包',
        'url' => 'category.php?id=437',
      ),
      438 => 
      array (
        'id' => '438',
        'name' => '妈咪包',
        'url' => 'category.php?id=438',
      ),
    ),
  ),
  355 => 
  array (
    'id' => '355',
    'name' => '流行男鞋',
    'url' => 'category.php?id=355',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      399 => 
      array (
        'id' => '399',
        'name' => '休闲鞋',
        'url' => 'category.php?id=399',
      ),
      401 => 
      array (
        'id' => '401',
        'name' => '凉鞋/沙滩鞋',
        'url' => 'category.php?id=401',
      ),
      402 => 
      array (
        'id' => '402',
        'name' => '帆布鞋',
        'url' => 'category.php?id=402',
      ),
      403 => 
      array (
        'id' => '403',
        'name' => '商务休闲鞋',
        'url' => 'category.php?id=403',
      ),
      404 => 
      array (
        'id' => '404',
        'name' => '正装鞋',
        'url' => 'category.php?id=404',
      ),
      405 => 
      array (
        'id' => '405',
        'name' => '增高鞋',
        'url' => 'category.php?id=405',
      ),
      406 => 
      array (
        'id' => '406',
        'name' => '拖鞋/人字拖',
        'url' => 'category.php?id=406',
      ),
      407 => 
      array (
        'id' => '407',
        'name' => '工装鞋',
        'url' => 'category.php?id=407',
      ),
      408 => 
      array (
        'id' => '408',
        'name' => '男靴',
        'url' => 'category.php?id=408',
      ),
      409 => 
      array (
        'id' => '409',
        'name' => '传统布鞋',
        'url' => 'category.php?id=409',
      ),
      410 => 
      array (
        'id' => '410',
        'name' => '功能鞋',
        'url' => 'category.php?id=410',
      ),
      411 => 
      array (
        'id' => '411',
        'name' => '鞋配件',
        'url' => 'category.php?id=411',
      ),
      412 => 
      array (
        'id' => '412',
        'name' => '定制鞋',
        'url' => 'category.php?id=412',
      ),
    ),
  ),
  353 => 
  array (
    'id' => '353',
    'name' => '时尚女鞋',
    'url' => 'category.php?id=353',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      368 => 
      array (
        'id' => '368',
        'name' => '凉鞋',
        'url' => 'category.php?id=368',
      ),
      371 => 
      array (
        'id' => '371',
        'name' => '单鞋',
        'url' => 'category.php?id=371',
      ),
      373 => 
      array (
        'id' => '373',
        'name' => '高跟鞋',
        'url' => 'category.php?id=373',
      ),
      375 => 
      array (
        'id' => '375',
        'name' => '坡跟鞋',
        'url' => 'category.php?id=375',
      ),
      376 => 
      array (
        'id' => '376',
        'name' => '松糕鞋',
        'url' => 'category.php?id=376',
      ),
      378 => 
      array (
        'id' => '378',
        'name' => '鱼嘴鞋',
        'url' => 'category.php?id=378',
      ),
      380 => 
      array (
        'id' => '380',
        'name' => '休闲鞋',
        'url' => 'category.php?id=380',
      ),
      382 => 
      array (
        'id' => '382',
        'name' => '帆布鞋',
        'url' => 'category.php?id=382',
      ),
      384 => 
      array (
        'id' => '384',
        'name' => '拖鞋/人字拖',
        'url' => 'category.php?id=384',
      ),
      386 => 
      array (
        'id' => '386',
        'name' => '妈妈鞋',
        'url' => 'category.php?id=386',
      ),
      387 => 
      array (
        'id' => '387',
        'name' => '防水台',
        'url' => 'category.php?id=387',
      ),
      389 => 
      array (
        'id' => '389',
        'name' => '雨鞋/雨靴',
        'url' => 'category.php?id=389',
      ),
      391 => 
      array (
        'id' => '391',
        'name' => '内增高',
        'url' => 'category.php?id=391',
      ),
      392 => 
      array (
        'id' => '392',
        'name' => '布鞋/绣花鞋',
        'url' => 'category.php?id=392',
      ),
      393 => 
      array (
        'id' => '393',
        'name' => '女靴',
        'url' => 'category.php?id=393',
      ),
      394 => 
      array (
        'id' => '394',
        'name' => '雪地靴',
        'url' => 'category.php?id=394',
      ),
      396 => 
      array (
        'id' => '396',
        'name' => '踝靴',
        'url' => 'category.php?id=396',
      ),
      397 => 
      array (
        'id' => '397',
        'name' => '马丁靴',
        'url' => 'category.php?id=397',
      ),
      398 => 
      array (
        'id' => '398',
        'name' => '鞋配件',
        'url' => 'category.php?id=398',
      ),
    ),
  ),
  357 => 
  array (
    'id' => '357',
    'name' => '潮流女包',
    'url' => 'category.php?id=357',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      413 => 
      array (
        'id' => '413',
        'name' => '单肩包',
        'url' => 'category.php?id=413',
      ),
      414 => 
      array (
        'id' => '414',
        'name' => '手提包',
        'url' => 'category.php?id=414',
      ),
      415 => 
      array (
        'id' => '415',
        'name' => '斜跨包',
        'url' => 'category.php?id=415',
      ),
      416 => 
      array (
        'id' => '416',
        'name' => '双肩包',
        'url' => 'category.php?id=416',
      ),
      417 => 
      array (
        'id' => '417',
        'name' => '钱包',
        'url' => 'category.php?id=417',
      ),
      418 => 
      array (
        'id' => '418',
        'name' => '手拿包/晚宴包',
        'url' => 'category.php?id=418',
      ),
      419 => 
      array (
        'id' => '419',
        'name' => '卡包/零钱包',
        'url' => 'category.php?id=419',
      ),
      420 => 
      array (
        'id' => '420',
        'name' => '钥匙包',
        'url' => 'category.php?id=420',
      ),
    ),
  ),
  359 => 
  array (
    'id' => '359',
    'name' => '精品男包',
    'url' => 'category.php?id=359',
    'style_icon' => 'other',
    'cat_icon' => '',
    'cat_id' => 
    array (
      421 => 
      array (
        'id' => '421',
        'name' => '商务公文包',
        'url' => 'category.php?id=421',
      ),
      422 => 
      array (
        'id' => '422',
        'name' => '单肩/斜跨包',
        'url' => 'category.php?id=422',
      ),
      423 => 
      array (
        'id' => '423',
        'name' => '男生手包',
        'url' => 'category.php?id=423',
      ),
      424 => 
      array (
        'id' => '424',
        'name' => '双肩包',
        'url' => 'category.php?id=424',
      ),
      425 => 
      array (
        'id' => '425',
        'name' => '钱包/卡包',
        'url' => 'category.php?id=425',
      ),
      426 => 
      array (
        'id' => '426',
        'name' => '钥匙包',
        'url' => 'category.php?id=426',
      ),
    ),
  ),
);
?>