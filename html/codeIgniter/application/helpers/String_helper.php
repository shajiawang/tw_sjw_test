<?php

// 移除emoji符號
function emoji_remove($text) {
        $len = mb_strlen($text);
        $new_text = '';
        for ($i = 0; $i < $len; $i++) {
        $word = mb_substr($text, $i, 1);
        error_log("[emoji_remove] word : ".$word);
        if (strlen($word) <= 3) {
                $new_text .= $word;
        }
        }
        return $new_text;
}

function strEncode($data,$key,$encode_type = "crypt"){
        if($encode_type == "crypt"){
                $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
                $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
                return urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$key,$data,MCRYPT_MODE_ECB,$iv));
        }
        elseif($encode_type == "md5"){
                return urlencode($data);
        }
        else{
                echo "Please write \$encode_type type to config.ini.php!!"; exit ;
        }
}

function strDecode($data,$key,$encode_type = 'crypt'){
        if($encode_type == "crypt"){
                $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256 , MCRYPT_MODE_ECB);
                $iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
                return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$key,urldecode($data),MCRYPT_MODE_ECB,$iv));
        }
        elseif($encode_type == "md5"){
                return urldecode($data);
        }
        else{
                echo "Please write \$encode_type type to config.ini.php file!!"; exit ;
        }

}
