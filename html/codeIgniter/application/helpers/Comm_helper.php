<?php

function getRetJSONArray($code='', $msg='', $type='') {
    $ret=array();
    $ret['retCode']=$code;
    $ret['retMsg']=$msg;
    $ret['retType']=$type;
    if($type=='LIST') {
       $ret['retObj']=array("data"=>array());
    } else if($type=='JSON') {
       $ret['retObj']=new stdClass();
    }
    
    return $ret;
}
