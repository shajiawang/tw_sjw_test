<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class sms {

    /**
     * 定义常量-发送短信的接入地址
     */
    const SENDURL = 'http://api.qirui.com:7891/mt';

    /**
     * 接口账号
     */
    const APIKEY = '501091960002';

    /**
     * 接口密码
     */
    const APISECRET = '0750ef68e249e7fcedd17b3408d30911';


    /**
     * 短信发送
     */
    public static function sendSms($phone, $message, $area=2, $charset='utf-8'){

       log_message('error',"[Sms_helper/sendSms] message :".$message);

       if($area == 2){//大陸簡訊

          $requestData = array(
              'un' => self::APIKEY,
              'pw' => self::APISECRET,
              'sm' => $message,
              'da' => $phone,
              'rd' => 1,
              'dc' => 15,
              'rf' => 2,
              'tf' => 3,
          );

          $url = self::SENDURL . '?' . http_build_query($requestData);
          $url = self::curl($url);
          $url = json_decode($url,true);

          if($url['success'] == 1){
             $data['retcode'] = 1;
             $data['retmsg'] = "OK";
             self::creatJSON($data);
          }elseif($url['success'] == 0){
             $data['retcode'] = $url['r'];
             $data['retmsg'] = "发送失败";
             self::creatJSON($data);
          }else{
             $data['retmsg'] = "未知错误";
             self::creatJSON($data);
          }

       }else{//台灣 或其他國家 簡訊

          //簡訊王 SMS-API
  			  if(empty($message)){
             $data['retcode'] = -1;
             $data['retmsg'] = "簡訊內容不可為空白!!";
             log_message('error',"[Sms_helper/sendSms] ret_data :".json_encode($data));
             self::creatJSON($data);
          }

          $sMessage = urlencode(mb_convert_encoding($message, 'BIG5', 'UTF-8'));
  			  $msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
  		    $to_url = "http://api.kotsms.com.tw/kotsmsapi-1.php?". $msg;

          if(!$getfile=file($to_url)) {
             $data['retcode'] = -2;
             $data['retmsg'] = "簡訊無法傳送，請重新再試一次!!";
             log_message('error',"[Sms_helper/sendSms] ret_data :".json_encode($data));
             self::creatJSON($data);
  			  }else{
  				   $term_tmp = implode('', $getfile);
  				   $check_kmsgid = explode('=', $term_tmp);
  				   $kmsgid = (int)$check_kmsgid[1];

             if($kmsgid < 0) {
                $data['retcode'] = -3;
                $data['retmsg'] = "手機號碼錯誤!!";
                log_message('error',"[Sms_helper/sendSms] ret_data :".json_encode($data));
                self::creatJSON($data);
  				   }else{
                $data['retcode'] = 1;
                $data['retmsg'] = "OK";
                log_message('error',"[Sms_helper/sendSms] ret_data :".json_encode($data));
                self::creatJSON($data);
  				   }

          }

       }
       
    }

    /**
     * 请求发送
     * @return string 返回发送状态
     */
    private static function curl($url)
    {
        $ch = curl_init();    //初始化curl
        curl_setopt($ch, CURLOPT_HEADER, 0);    //设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);    //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_URL, $url);    //抓取指定网页
        $result = curl_exec($ch);    //运行curl
        curl_close($ch);    //关闭
        return $result;
    }

    /**
     * 转为json格式
     * $arr array
     * @return string
     */
    public static function creatJSON($arr)
    {
        $r = json_encode($arr);
        return ($r);
    }
}
