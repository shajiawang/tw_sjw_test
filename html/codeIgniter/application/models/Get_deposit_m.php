<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Get_deposit_m extends CI_Model {

    public $datetime;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->datetime = date('Y-m-d H:i:s');
    }

	/*
	 * 取得儲值項目
	 * $drid		int 	項目編號
	 */
	public function get_deposit_rule_list($deposit_dbname,$drid) {
		
		$kind_db = $this->load->database($deposit_dbname, TRUE);
		
        $kind_db->select('dr.drid, dr.name, dr.description, dr.act, dr.notify_url, dr.logo, dr.banner');
        $kind_db->from('deposit_rule dr');
		if (!empty($drid)){
			$kind_db->where('dr.drid', $drid);
		}
		$kind_db->where('dr.switch', 'Y');
		$kind_db->order_by('dr.seq','DESC');
		$kind_db->order_by('dr.drid','DESC');		
        $query = $kind_db->get();
        return $query->result_array();
    }

	/*
	 * 取得儲值額度細項
	 * $driid		int 	額度細項編號
	 * $drid		int 	項目編號
	 */
	public function get_deposit_rule_item($deposit_dbname,$driid,$drid) {
		
		$kind_db = $this->load->database($deposit_dbname, TRUE);
		
        $kind_db->select('dri.driid, dri.drid, dri.name, dri.amount, dri.spoint');
        $kind_db->from('deposit_rule_item dri');
		if (!empty($driid)){
			$kind_db->where('dri.driid', $driid);
		}
		if (!empty($drid)){
			$kind_db->where('dri.drid', $drid);
		}
		$kind_db->where('dri.switch', 'Y');
		$kind_db->order_by('dri.seq','DESC');
		$kind_db->order_by('dri.driid','DESC');
        $query = $kind_db->get();
        return $query->result_array();
    }

	/*
	 * 寫入儲值訂單
	 * $data	array	新增資料項目
	 */
	public function insert_order($deposit_dbname,$data) {
        
		$kind_db = $this->load->database($deposit_dbname, TRUE);
        
		$query = $kind_db->insert_string('order', $data);
        $kind_db->query($query);
		return $query;
	}
	
	/*
	 * 寫入儲值訂單細項
	 * $data	array	新增資料項目
	 */
	public function insert_order_detail($deposit_dbname,$data) {
        
		$kind_db = $this->load->database($deposit_dbname, TRUE);
        
		$query = $kind_db->insert_string('order_deposit', $data);
        $kind_db->query($query);
		return $query;
	}	

	/*
	 * 寫入儲值訂單物流狀態
	 * $data	array	新增資料項目
	 */
	public function insert_order_shipping($deposit_dbname,$data) {
        
		$kind_db = $this->load->database($deposit_dbname, TRUE);
        
		$query = $kind_db->insert_string('order_shipping', $data);
        $kind_db->query($query);
		return $query;
	}	

	/*
	 * 取得儲值訂單
	 * $orderid		int 	訂單編號
	 */
	public function get_order($deposit_dbname,$orderid) {
        
		$kind_db = $this->load->database($deposit_dbname, TRUE);
		
		$kind_db->select('o.orderid, o.userid, o.type, o.num, o.memo, o.tx_data, o.thumbnail');
        $kind_db->from('order o');
		if (!empty($orderid)){
			$kind_db->where('o.orderid', $orderid);
		}
		$kind_db->where('o.switch', 'Y');
		$kind_db->where('o.type', 'deposit');
		$kind_db->order_by('o.insertt','DESC');
		$kind_db->order_by('o.drid','DESC');		
        $query = $kind_db->get();
        return $query->result_array();
    }
	
	/*
	 * 取得儲值訂單明細
	 * $orderid		int 	訂單編號
	 */
	public function get_order_deposit($deposit_dbname,$orderid) {
        
		$kind_db = $this->load->database($deposit_dbname, TRUE);
		
		$kind_db->select('od.orderid, od.driid, od.spointid, od.status, od.currency, od.amount, od.invoiceno, od.insertid, od.insertname');
        $kind_db->from('order_deposit od');
		if (!empty($orderid)){
			$kind_db->where('od.orderid', $orderid);
		}
		$kind_db->where('od.switch', 'Y');
		$kind_db->order_by('od.insertt','DESC');
		$kind_db->order_by('od.drid','DESC');		
        $query = $kind_db->get();
        return $query->result_array();
    }

	/*
	 * 取得儲值訂單物流狀態
	 * $orderid		int 	訂單編號
	 */
	public function get_order_shipping($deposit_dbname,$orderid) {
        
		$kind_db = $this->load->database($deposit_dbname, TRUE);
		
		$kind_db->select('os.orderid, os.outtime, os.outtype, os.outcode, os.outmemo, os.backtime, os.backmemo, os.closetime, os.closememo');
        $kind_db->from('order_shipping os');
		if (!empty($orderid)){
			$kind_db->where('os.orderid', $orderid);
		}
		$kind_db->where('os.switch', 'Y');
		$kind_db->order_by('os.insertt','DESC');
		$kind_db->order_by('os.drid','DESC');		
        $query = $kind_db->get();
        return $query->result_array();
    }	

}
?>
