<?php
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Get_ad_m extends CI_Model {

    public $datetime;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->datetime = date('Y-m-d H:i:s');
    }
	
	/*
	 * 取得廣告相關資料
	 */
	public function get_ad_list($ad_dbname) {
		
		$kind_db = $this->load->database($ad_dbname, TRUE);
        
		$kind_db->select('ad.name, ad.thumbnail, ad.thumbnail2, ad.thumbnail_url, ad.qrcode_url, ad.promotetype');
        $kind_db->from('ad ad');
		$kind_db->where('ad.ontime <= ', $this->datetime);
		$kind_db->where('ad.offtime >= ', $this->datetime);		
		$kind_db->where('ad.switch', 'Y');
		$kind_db->order_by('ad.seq','DESC');
		$kind_db->order_by('ad.adid','DESC');
		$query = $kind_db->get();
		
        return $query->result_array();
	}
	
}   
?>