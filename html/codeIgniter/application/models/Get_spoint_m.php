<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Get_spoint_m extends CI_Model {

    public $datetime;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->datetime = date('Y-m-d H:i:s');
    }

	/*
	 * 寫入殺價幣
	 * $data				array				新增資料項目
	 */	
	public function insert_spoint($spoint_dbname,$data) {
        
		$kind_db = $this->load->database($spoint_dbname, TRUE);
        
		$query = $kind_db->insert_string('spoint', $data);
        $kind_db->query($query);
		return $query;
	}	

	/*
	 * 更新殺價幣
	 * $spointid			int					殺價幣編號
	 * $data				array				更新資料項目
	 */
	public function update_spoint($spoint_dbname,$spointid,$data) {
		
		$kind_db = $this->load->database($spoint_dbname, TRUE);
		
		$where = "`spointid` = '{$spointid}'";
		$query = $kind_db->update_string('spoint', $data, $where);
		$kind_db->query($query);
		return $query;
    }	
	
	/*
	 * 取得特定殺價幣資料
	 * $userid				int					會員編號		 	 
	 * $spointid			int					殺價幣編號	
	 */		
	public function get_spoint($spoint_dbname,$userid,$spointid) {

		$kind_db = $this->load->database($spoint_dbname, TRUE);
        
		$kind_db->select('s.spointid, s.userid, s.fromid, s.behav, s.amount, s.insertt');
        $kind_db->from('spoint s');
		if(!empty($spointid)) {
			$kind_db->where('s.behav', $spointid);
		}
		$kind_db->where('s.switch', 'Y');
		$kind_db->order_by('s.insertt','DESC');
		$kind_db->order_by('s.spointid','DESC');
		$query = $kind_db->get();
        return $query->result_array();		
	}
	
	/*
	 * 取得殺價幣清單
	 * $userid				int					會員編號		 
	 * $behav				varchar				殺價幣行為	
	 */
	public function get_spoint_list($spoint_dbname,$userid,$behav) {
		
		$kind_db = $this->load->database($spoint_dbname, TRUE);
        
		$kind_db->select('s.spointid, s.userid, s.fromid, s.behav, s.amount, s.insertt');
        $kind_db->from('spoint s');
		if(!empty($behav)) {
			$kind_db->where('s.behav', $behav);
		}
		$kind_db->where('s.switch', 'Y');
		$kind_db->order_by('s.insertt','DESC');
		$kind_db->order_by('s.spointid','DESC');
		$query = $kind_db->get();
        return $query->result_array();
	}

	/*
	 * 取得殺價幣收入清單
	 * $userid				varchar				殺價幣行為	
	 */
	public function get_spoint_in_lsit($spoint_dbname,$userid) {
		
		$kind_db = $this->load->database($spoint_dbname, TRUE);
		
        $behav = "('prod_sell','bid_by_saja','user_deposit','gift','bid_refund','sajabonus','feedback')";
		
		$kind_db->select('s.behav, s.insertt, od.invoiceno, IFNULL(s.amount,0) as amount');
        $kind_db->from('spoint s');
		$kind_db->join('order_deposit od','o.spointid = od.spointid', 'left');		
		$kind_db->where('s.userid', $userid);
		$kind_db->where('s.behav', $behav);
		$kind_db->where('s.switch', 'Y');
		$query = $kind_db->get();
        return $query->result_array();
	}

	/*
	 * 取得殺價幣支出清單
	 * $userid				varchar				殺價幣行為	
	 */
	public function get_spoint_out_lsit($spoint_dbname,$userid) {
		
		$kind_db = $this->load->database($spoint_dbname, TRUE);

        $behav = "('prod_sell','process_fee','user_saja','other_system_use')";
        
		$kind_db->select('s.behav, s.insertt, IFNULL(s.amount,0) as amount');
        $kind_db->from('spoint s');
		$kind_db->where('s.userid', $userid);
		$kind_db->where('s.behav in ', $behav);
		$kind_db->where('s.switch', 'Y');
		$query = $kind_db->get();
        return $query->result_array();
	}

	/*
	 * 取得殺價幣收入總計
	 * $userid				varchar				殺價幣行為	
	 */
	public function get_spoint_in_count($spoint_dbname,$userid) {
		
		$kind_db = $this->load->database($spoint_dbname, TRUE);
		
        $behav = "('prod_sell','bid_by_saja','user_deposit','gift','bid_refund','sajabonus','feedback')";
		
		$kind_db->select('count(*) as num');
        $kind_db->from('spoint s');
		$kind_db->where('s.userid', $userid);
		$kind_db->where('s.behav', $behav);
		$kind_db->where('s.switch', 'Y');
		$query = $kind_db->get();
        return $query->result_array();
	}

	/*
	 * 取得殺價幣支出總計
	 * $userid				varchar				殺價幣行為	
	 */
	public function get_spoint_out_count($spoint_dbname,$userid) {
		
		$kind_db = $this->load->database($spoint_dbname, TRUE);

        $behav = "('prod_sell','process_fee','user_saja','other_system_use')";
        
		$kind_db->select('count(*) as num');
        $kind_db->from('spoint s');
		$kind_db->where('s.userid', $userid);
		$kind_db->where('s.behav in ', $behav);
		$kind_db->where('s.switch', 'Y');
		$query = $kind_db->get();
        return $query->result_array();
	}
	
}
