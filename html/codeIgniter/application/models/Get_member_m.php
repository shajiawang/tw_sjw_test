<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Get_member_m extends CI_Muplel {

    public $datetime;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $this->datetime = date('Y-m-d H:i:s');
    }

	/*
	 * 寫入員驗證資料
	 * $data				array				新增資料項目
	 */	
	public function insert_member_mobile_check($member_dbname,$data) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
        
		$query = $kind_db->insert_string('user_mobile_check', $data);
        $kind_db->query($query);
		return $query;
	}

	/*
	 * 寫入會員基本資料(登入)
	 * $data				array				新增資料項目
	 */	
	public function insert_member_profile_login($member_dbname,$data) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
        
		$query = $kind_db->insert_string('user_profile_login', $data);
        $kind_db->query($query);
		return $query;
	}

	/*
	 * 寫入會員基本資料
	 * $data				array				新增資料項目
	 */	
	public function insert_member_profile_main($member_dbname,$data) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
        
		$query = $kind_db->insert_string('spoint', $data);
        $kind_db->query($query);
		return $query;
	}

	/*
	 * 寫入會員收件人明細
	 * $data				array				新增資料項目
	 */	
	public function insert_member_shipping_address($member_dbname,$data) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
        
		$query = $kind_db->insert_string('spoint', $data);
        $kind_db->query($query);
		return $query;
	}	
	
	/*
	 * 更新會員驗證資料
	 * $userid				int					會員編號
	 * $data				array				更新資料項目
	 */
	public function update_member_mobile_check($member_dbname,$spointid,$data) {
		
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$where = "`userid` = '{$userid}'";
		$query = $kind_db->update_string('user_mobile_check', $data, $where);
		$kind_db->query($query);
		return $query;
    }	
	
	/*
	 * 更新會員基本資料(登入)
	 * $userid				int					會員編號
	 * $data				array				更新資料項目
	 */
	public function update_member_profile_login($member_dbname,$userid,$data) {
		
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$where = "`userid` = '{$userid}'";
		$query = $kind_db->update_string('user_profile_login', $data, $where);
		$kind_db->query($query);
		return $query;
    }

	/*
	 * 更新會員基本資料
	 * $userid				int					會員編號
	 * $data				array				更新資料項目
	 */
	public function update_member_profile_main($member_dbname,$userid,$data) {
		
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$where = "`userid` = '{$userid}'";
		$query = $kind_db->update_string('user_profile_main', $data, $where);
		$kind_db->query($query);
		return $query;
    }	

	/*
	 * 更新會員收件人明細
	 * $userid				int					會員編號
	 * $data				array				更新資料項目
	 */
	public function update_member_shipping_address($member_dbname,$userid,$data) {
		
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$where = "`userid` = '{$userid}'";
		$query = $kind_db->update_string('user_shipping_address', $data, $where);
		$kind_db->query($query);
		return $query;
    }	

	/*
	 * 取得會員驗證資料
	 * $userid				int				會員編號
	 */
	public function get_member_mobile_check($member_dbname,$userid) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$kind_db->select('umc.userid, umc.code, umc.verified');
        $kind_db->from('user_mobile_check umc');
		$kind_db->where('umc.userid',$userid);
		$kind_db->where('umc.switch','Y');
        $query = $kind_db->get();
        return $query->result_array();
    }

	/*
	 * 取得會員基本資料(登入)
	 * $userid				int				會員編號
	 */
	public function get_member_profile_login($member_dbname,$userid) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$kind_db->select('upl.userid, upl.driid, upl.spointid, upl.status, upl.currency, upl.amount, upl.invoiceno, upl.insertid, upl.insertname');
        $kind_db->from('user_profile_login upl');
		$kind_db->where('upl.userid',$userid);
		$kind_db->where('upl.switch','Y');
		$kind_db->order_by('upl.insertt','DESC');
        $query = $kind_db->get();
        return $query->result_array();
    }

	/*
	 * 取得會員基本資料
	 * $userid				int				會員編號
	 */
	public function get_member_profile_main($member_dbname,$userid) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$kind_db->select('upm.userid, upm.gender, upm.birthday, upm.mobile, upm.email, upm.countryid, upm.regionid, upm.provinceid, upm.zipcuple, upm.address, upm.memo');
        $kind_db->from('user_profile_main upm');
		$kind_db->where('upm.userid',$userid);
		$kind_db->where('upm.switch','Y');
		$kind_db->order_by('upm.insertt','DESC');		
        $query = $kind_db->get();
        return $query->result_array();
    }	

	/*
	 * 取得會員收件人明細
	 * $userid				int				會員編號
	 */
	public function get_member_shipping_address($member_dbname,$userid) {
        
		$kind_db = $this->load->database($member_dbname, TRUE);
		
		$kind_db->select('usa.userid, usa.disp_name, usa.Recipient, usa.phone, usa.countryid, usa.regionid, usa.provinceid, usa.zipcuple, usa.address, usa.default_record');
        $kind_db->from('user_shipping_address usa');
		$kind_db->where('usa.userid',$userid);
		$kind_db->where('usa.switch','Y');
		$kind_db->order_by('usa.insertt','DESC');
        $query = $kind_db->get();
        return $query->result_array();
    }

}
