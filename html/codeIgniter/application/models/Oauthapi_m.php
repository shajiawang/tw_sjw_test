<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Oauthapi_m extends CI_Model {

    public $datetime;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $db = $this->load->database();
    }


	/*
	 * 確認驗證碼資料是否正確
	 * $UserID			int				使用者編號
	 * $ChkCode			varchar			驗證碼
	 */
	public function get_user_sso($auth_by, $uid, $uid2=''){
        
        $this->db->select('userid, auth_by, uid');
        $this->db->from('user_login_sso');
        $this->db->where('uid', $uid);
        $this->db->where('auth_by', $auth_by);
        $query = $this->db->get();
        return $query->row_array();
    }

}
?>
