<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class User_register_m extends CI_Model {

    public $datetime;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Taipei');
        $db = $this->load->database();
    }

	public function chk_sso($auth_by, $uid, $uid2=''){
        $r['err'] = '';
        $this->db->select('userid, auth_by, uid');
        $this->db->from('user_login_sso');
        $this->db->where('uid', $uid);
        $this->db->where('auth_by', $auth_by);
        $query = $this->db->get();
        $res = $query->row_array();
        
        if(!empty($res)){
            ////////////////////////////////////////////////////////////
            if($res['userid'] > 0) {
                error_log("[m/user_register/chk_sso] uid : ".$uid."( uid2:".$uid2.") is existed and will not create sso mapping !!");
                $r['err']=16;
                $r['retCode']=-16;
                $r['userid']=$res['userid'];
                $r['retMsg']="此帳號已綁定過 !"; 
            }
        }
        error_log("[chkchk_sso_sso]:".json_encode($r));
        return $r;
    }

    //寫入註冊資料
    public function insert_user_index($data) {
        $query = $this->db->insert_string('user_index', $data);
        $this->db->query($query);
        return $this->db->insert_id();
    }

    public function insert_user_profile_main($data) {
        $ret=0;
		try {
            $query = $this->db->insert_string('user_profile_main', $data);
            error_log("[m/user_register/insert_user_profile_main] : ".$query);
            $this->db->query($query);
            $ret = $this->db->affected_rows();
		} catch (Exception $e) {
            error_log("[m/user_register/insert_user_profile_main] Exception : ".$e->getMessage());
            $ret=-99;
        }
        //error_log("[m/user_register/insert_user_profile_main] ret : ".$ret);
		return $ret;
	}

    public function insert_user_profile_login($data) {
        $ret=0;
		try {
            $query = $this->db->insert_string('user_profile_login', $data);
            error_log("[m/user_register/insert_user_profile_login] : ".$query);
            $this->db->query($query);
            $ret = $this->db->affected_rows();
		} catch (Exception $e) {
            error_log("[m/user_register/insert_user_profile_login] Exception : ".$e->getMessage());
            $ret=-99;
        }
        //error_log("[m/user_register/insert_user_profile_login] ret : ".$ret);
		return $ret;
	}

    public function insert_user_exchange_pwd($data) {
        $ret=0;
		try {
            $query = $this->db->insert_string('user_exchange_pwd', $data);
            error_log("[m/insert_user_exchange_pwd] : ".$query);
            $this->db->query($query);
            $ret = $this->db->affected_rows();
		} catch (Exception $e) {
            error_log("[m/user_register/insert_user_exchange_pwd] Exception : ".$e->getMessage());
            $ret=-99;
        }
        //error_log("[m/user_register/insert_user_exchange_pwd] ret : ".$ret);
		return $ret;
    }
    
    public function insert_user_login_sso($data) {
        $ret=0;
		try {
            $query = $this->db->insert_string('user_login_sso', $data);
            error_log("[m/insert_user_login_sso] : ".$query);
            $this->db->query($query);
            $ret = $this->db->affected_rows();
		} catch (Exception $e) {
            error_log("[m/user_register/insert_user_login_sso] Exception : ".$e->getMessage());
            $ret=-99;
        }
        //error_log("[m/user_register/insert_user_login_sso] ret : ".$ret);
		return $ret;
    }
    
    public function get_user_profile_login($userid) {
        $this->db->select('userid, nickname ');
        $this->db->from('user_profile_login');
        $this->db->where('userid', $userid);
        $this->db->where('switch', 'Y');
        $this->db->limit(1);
        $query = $this->db->get();
        return $query->row_array();
    }


    public function mk_appuser(){

    }

}
?>
