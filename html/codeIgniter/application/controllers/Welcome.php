<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index()
	{
		
		$client = new WebSocket\Client("wss://ws.saja.com.tw:3334");
		// $client = new WebSocket\Client("wss://echo.websocket.org");
		// $client = new WebSocket\Client("wss://demos.kaazing.com/echo");
		$client->send("Hello WebSocket.org!");
		echo $client->receive();

		// $client = new WSSC\WebSocketClient('wss://echo.websocket.org');
		// $client = new WSSC\WebSocketClient('wss://ws.saja.com.tw:3334');
		// $client->send('安安');
		// echo $client->receive();

		// $this->load->view("welcome_message");
	}
}
