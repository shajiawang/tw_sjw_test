<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Taipei');
		$ts=date("YmdHis");
		$this->load->helper('url');

		$this->load->library('session');
		$this->load->library('json');

		$this->load->model('get_ad_m');
		// $this->load->model('get_product_m');
	}
	
	/*
	 *	首頁顯示內容
	 */
	public function index(){

		$ad_dbname = "main";
		$prod_dbname = "main";
		$bid_dbname = "main";
		
		$redis = new Redis();
		$redis->connect('127.0.0.1', '6379');
		
		//首頁廣告
		if($redis) {
			$rkey = "adlist";
			$ret = $redis->get($rkey); 
			if(!empty($ret)) {				
				echo $ret;
				exit;
			}
		}
		
		$data['ad'] = $this->get_ad_m->get_ad_list($ad_dbname);

		if(!empty($data['ad']) && is_array($data['ad'])) {
			$ret = json_encode($data['ad']);
		}

		if($redis) {
			$redis->set($rkey,$ret);   
		}
		
		//商品清單
		// $data['product_list'] = $this->get_product_m->get_product_List('', '', '', '', 'Y', 'O');
		
		//成交清單
		// $data['bid_list'] = $this->get_bid_m->get_bid_List('', '', '', '', 'Y', 'O');

		// $data['recode'] = 1;
		// $data['remsg'] = "取得資料成功 !!";
		// $this->json->creatJSON($ret);
		
	}

	public function logout() {

		$Web = htmlspecialchars($this->input->get_post('web', TRUE));

		//清除自動登入帳密
		setcookie("Accid","",time(),'/');
		setcookie("Acckey","",time(),'/');

		unset($_SESSION['user']);
		unset($_SESSION['lang']);
		session_destroy();

		//首頁廣告
		$data['ad'] = $this->get_ad_m->get_ad_list();

		//商品清單
		// $data['product_list'] = $this->get_product_m->get_product_List('', '', '', '', 'Y', 'O');
		
		//成交清單
		// $data['bid_list'] = $this->get_bid_m->get_bid_List('', '', '', '', 'Y', 'O');
		
		$data['logout'] = true;

		$resdata['recode'] = 1;
		$resdata['remsg'] = "登出成功 !!";
		$this->json->creatJSON($resdata);

	}
}
