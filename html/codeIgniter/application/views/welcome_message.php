<!DOCTYPE html>
<head>
	<meta charset="utf-8" />
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<title>WebSocket Test</title>
</head>
<body>
	<input type="text" id="ws_ip" placeholder="ip"> <br> <br>
	<textarea id="send_box" placeholder="文字"></textarea><br>
	<button id="ws_send">SEND</button>
</body>
<script language="javascript" type="text/javascript">

	$("#ws_send").click(function(event) {
		var wsUri = $("#ws_ip").val();
		websocket = new WebSocket(wsUri);
		websocket.onopen = function(evt) { onOpen(evt) };
	});

	

	function testWebSocket()
	{
		websocket = new WebSocket(wsUri);
		websocket.onopen = function(evt) { onOpen(evt) };
		websocket.onclose = function(evt) { onClose(evt) };
		websocket.onmessage = function(evt) { onMessage(evt) };
		websocket.onerror = function(evt) { onError(evt) };
	}

	function onOpen(evt)
	{
		writeToScreen("CONNECTED");
		doSend( $("#send_box").val() );
	}

	function onClose(evt)
	{
		writeToScreen("DISCONNECTED");
	}

	function onMessage(evt)
	{
		writeToScreen('<span style="color: blue;">RESPONSE: ' + evt.data+'</span>');
		websocket.close();
	}

	function onError(evt)
	{
		writeToScreen('<span style="color: red;">ERROR:</span> ' + evt.data);
	}

	function doSend(message)
	{
		writeToScreen("SENT: " + message);
		websocket.send(message);
	}

	function writeToScreen(message)
	{
		var pre = document.createElement("p");
		pre.style.wordWrap = "break-word";
		pre.innerHTML = message;
		output.appendChild(pre);
	}

	

</script>

<h2>WebSocket Test</h2>

<div id="output"></div>
