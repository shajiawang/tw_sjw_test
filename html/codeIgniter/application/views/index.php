
<link rel="stylesheet" href="/site/static/css/share.min.css" />
<link rel="stylesheet" href="/site/static/themes/jquery.mobile.icons.min.css" />
<link rel="stylesheet" href="/site/static/css/jquery.mobile.structure-1.4.3.min.css" />
<!-- 瀏覽器初始化 -->
<link rel="stylesheet" href="/site/static/css/reset.css">
<!-- 抓取最新CSS 測試期用 會拖慢效率-->
<link rel="stylesheet" href="/site/static/css/eason.style.css?ver=1553508095">

<!-- RWD自定義 -->
<link rel="stylesheet" href="/site/static/css/responsive.min.css">

<!-- body 最后 -->
<script src="/site/static/js/jquery-2.1.4.js"></script>
<script src="/site/static/js/jquery-weui.js"></script>

<!-- 如果使用了某些拓展插件还需要額外的JS -->
<script src="/site/static/js/city-picker.min.js"></script>
<script src="/site/static/js/jquery.mobile-1.4.3.js"></script>
<script src="/site/static/js/jweixin-1.1.0.js"></script>
<script src="/site/static/js/jquery.timers-1.2.js"></script>
<script src="/site/static/js/share.js"></script>

<!-- 驗證 -->
<script src="/site/ajax/auth.js"></script>
<script src="/site/ajax/bid.js"></script>
<script src="/site/ajax/mall.js"></script>
<script src="/site/ajax/product.js?t=20190329114833"></script>
<script src="/site/ajax/scode.js"></script>
<script src="/site/ajax/user.js"></script>
<script src="/site/ajax/user_register.js"></script>
<script src="/site/ajax/enterprise.js"></script>

<!-- Flexslider -->
<script src="/site/static/js/Flexslider/modernizr.js"></script>
<script defer src="/site/static/js/Flexslider/jquery.flexslider.js"></script>

<!-- lazyload 2.0.0 懶載入-->
<script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-beta.2/lazyload.js"></script>

<!-- Syntax Highlighter -->
<script type="text/javascript" src="/site/static/js/Flexslider/shCore.js"></script>
<script type="text/javascript" src="/site/static/js/Flexslider/shBrushXml.js"></script>
<script type="text/javascript" src="/site/static/js/Flexslider/shBrushJScript.js"></script>
<!-- Optional FlexSlider Additions -->
<script src="/site/static/js/Flexslider/jquery.easing.js"></script>
<script src="/site/static/js/Flexslider/jquery.mousewheel.js"></script>

<!-- jqm自訂義滑動換頁效果 -->
<script src="/site/static/js/swipe.js?t=20190329114833"></script>

<!-- lustertech SDK-->
<script src="/site/static/js/lustertech_sdk.js?t=20190329114833"></script>
<!-- lustertech SDK -->

<!-- 頁面loading -->
<script type="text/javascript" src="http://test.shajiawang.com/site/static/js/es-page-loading.js"></script>

<!-- reCAPTCHA v2-->
<script src='https://www.google.com/recaptcha/api.js'></script>

<body style="height:100%" ontouchstart>
<div data-role="page" id="site_site_home20190329114833" style="" >
    <!--  data-role="page"開頭，panel結尾  -->
        <!-- 
            -----20190122 新----
            共用 es-content
            有預留header高度 hasHeader
            有預留footer高度 hasFooter
        -->
                    <div class="es-content hasFooter" >
        <!--  class="first-content"開頭，footer結尾  -->
        <!--  first-content 有預留footerBar高度，other-content 內頁無footerBar無預留高度 -->
	
	<div class="navbar sajaNavBar navbarHome d-flex align-items-center">
		<div class="search-box d-inlineflex align-items-center">
            <i class="esfas wcolor fas-search"></i>
			<form method="post" action="http://test.shajiawang.com/site/product/getProductSearch/" id="contactform" name="contactform">
            <input class="search" type="search" name="prodKeyword" id="prodKeyword" value="" placeholder="殺價搜尋" data-mini="true" data-role="none"/>
			</form>
		</div>
		<div class="sajaNavBarIcon Burger">
            <a href="#left-panel-c880a181947689696fb624bd5742437b" data-iconpos="right">
                <img src="/site/static/img/hamburger.png">
            </a>
        </div>
	</div>

<!-- 最新成交-跑馬燈 -->
<script src="/site/static/js/jquery.marquee.js"></script>

<!-- 成功殺價節省 -->
<script src="/site/static/js/counter.js"></script>

<script type="text/javascript">
    function openPage(url) {
		if(url=='') {
		   return false;
		}
		if(is_kingkr_obj()){
		   awakeOtherBrowser(url);
		} else {
		   window.location.href=url;
		}
    }
</script>
<div id="container">
    <div class="flexslider clearfix">
	    <ul class="slides">
            <?php
                if(!empty($ad_list)) {
                    foreach($ad_list as $rk => $rv){
                        if ($rv['adid'] != 11){
            ?>
            <li>
                <div class="cui-slide-img-item" >
                    <a onclick="openPage('<?php echo $rv['action']; ?>');">
                        <img src="<?php if(!empty($rv['thumbnail'])) {?> <?PHP //echo APP_DIR; ?>/images/site/ad/<?php echo $rv['thumbnail']; ?><?php }else{ ?><?php echo $rv['thumbnail_url']; ?><?php } ?>" alt="<?php echo $rv['name']; ?>"/>
                    </a>
                </div>
            </li>
			<?php } } }?>
		</ul>
	</div>
	
    <!--  成功殺價  -->
	<div class="success-box">
	    <div class="success-title">
	        <span>已有</span>
	        <span class="num"></span>
	        <span>人成功殺價節省</span>
	    </div>
	    <div class="success-money">
	        <span class="symbol align-middle">NT</span>
	        <ul class="jackpot counter align-middle">
                <!-- js生成 -->
            </ul>
	    </div>
	</div>
   
    <!--  menu  -->
    
    <div id="category" class="clearfix">
        <div id="category-wrapper" class="text-center d-flex justify-content-around">
            <div class="item">
                <a href="">
                    <img src="/site/static/img/homeMenu/i1.png">
                    <p>新手教學</p>
                </a>
            </div>
            <div class="item">
                <a href="">
                    <img src="/site/static/img/homeMenu/i2.png">
                    <p>殺戮戰場</p>
                </a>
            </div>
            <div class="item">
                <a href="">
                    <img src="/site/static/img/homeMenu/i3.png">
                    <p>最新得標</p>
                </a>
            </div>
            <div class="item">
                <a href="">
                    <img src="/site/static/img/homeMenu/i4.png">
                    <p>王者秘笈</p>
                </a>
            </div>
        </div>
    </div>

    <!--  最新得標  -->
    <div class="deal-box d-flex">
        <div class="deal-title"><span>最新得標</span></div>
        <div class="marqueebox">
            <ul id="marquee" class="marquee">
			</ul>
        </div>
    </div>

    <!--  廣告版位  -->
    <div class="mModalBox"></div>
    <!-- 蓋板浮動版位 -->
    <div class="cModalBox"></div>
    
    <div class="subtitle-box">
        <div class="subtitle left-title text-center">
            <span>即將結標</span>
        </div>
    </div>

    
       
    
</div>

<script type="text/javascript">
	function groupsaja(){ alert('敬請期待!'); }
	function flashsaja(){ alert('招募地推人員，請洽ditui@shajiawang.com'); }

	function ShowTimer(otime,ntime,divId) {

        var now = new Date();

        //getTime()获取的值/1000=秒数
        var leftTime=(otime*1000) - now.getTime();
		
        var leftSecond=parseInt(leftTime/1000);
		
		if (leftTime > 0) {
			
			var day=Math.floor(leftSecond/(60*60*24));
			
			var hour=Math.floor((leftSecond-day*24*60*60)/3600);
			if (hour < 10) {
				hour = "0" + hour;
			};

			var minute=Math.floor((leftSecond - day * 24 * 60 * 60 - hour * 3600) / 60);
			if (minute < 10) {
				minute = "0" + minute;
			};

			var second = Math.floor(leftSecond - day * 24 * 60 * 60 - hour * 3600 - minute * 60);
			if (second < 10) {
				second = "0" + second;
			};  
			var msecond = Math.floor(leftTime % 100);	
			if (msecond < 10) {
				msecond = "0" + msecond;
			};  
			
			var htmlElement=document.getElementById(divId);
       
			if (day == "00") {
				$('.'+divId).html(hour + ":" + minute + ":" + second + ":" + msecond);
				htmlElement.innerHTML = "" + hour + ":" + minute + ":" + second + ":"+ msecond;
			} else {
				htmlElement.innerHTML = "" + day + " 天 "  + hour + ":" + minute + ":" + second + ":"+ msecond;				
			}
		} else if (leftTime < 0) {
			var htmlElement=document.getElementById(divId);
			htmlElement.innerHTML = "已結標";
			window.clearInterval('intervalId'+divId);
            
            var $location = <?php echo json_encode(BASE_URL.APP_DIR) ?>;
            var $cdnTime = <?php echo json_encode($cdnTime) ?>;
            var $that = $('#'+divId),
                $alert = 'alert("此商品已結標")',
                $href = 'location.href="'+$location+'/product/?'+$cdnTime+'"';
            $that.parents(".sajalist-item").find("a").attr('onclick',$alert+','+$href);
            
		}	
    }
	
    $(function(){
      SyntaxHighlighter.all();
    });
    $(document).on('pageshow', function(){ //幻燈片banner
        $('.flexslider').flexslider({
            animation: "slide", //圖片切換方式 (滑動)
            slideshowSpeed: 5000, //自動播放速度 (毫秒)
            animationSpeed: 1000, //切換速度 (毫秒)
            directionNav: false, //顯示左右控制按鈕
            controlNav: true, //隱藏下方控制按鈕
            prevText:"", //左邊控制按鈕顯示文字
            nextText:"", //右邊控制按鈕顯示文字
            pauseOnHover: false, //hover時停止播放
            slideshow: true, //自動播放 (debug用)
			touch: true,
            start: function(slider){ //載入第一張圖片時觸發
                $('body').removeClass('loading');
            }
        });
    });   
    
	$(function(){
        /*跑馬燈啟動*/
		$("#marquee").marquee({yScroll: "bottom"});
        
        /*懶載入初始化*/
        $(".sajalist-img .lazyload").lazyload({
            threshold : 50                     //提前加載(像素)
        });
	})
    
    //懶加載漏網之魚
    $(window).on('load',function(){
        //loader漏網之魚
        var interval = null;
        var options = {
            dom:{
                item: '.sajalist-item',
                img: '.lazyload'
            }
        }
       
        function $reload() {
            var $winH = $(window).height();                                 //螢幕高度
            var $winPos = $(window).scrollTop();                            //目前捲軸位置
            var $item = $(options.dom.item);
            var $img = $item.find(options.dom.img);                         //抓取所有lazyload img
            //讓作用中頁面的圖片都跑一次迴圈，判斷是否加載
            $img.each(function(){
                var $that = $(this);
                var $imgPos = $(this).offset().top;                                                     //圖片位置
                if($imgPos > $winPos && $imgPos < $winPos + $winH){
                    var $showSrc = $that.attr('src');                                                   //loading圖片
                    var $showDsrc = $that.attr('data-src');                                             //產品圖片
                    //判斷兩者不相同時 表示未載入,這張圖 重新lazyload
                    if($showSrc !== $showDsrc){
                        $(this).lazyload();
                    }
                }
                
            })
        }   
        //判斷loader漏網之魚，2秒判斷一次
        //reloadimg = setInterval($reload,2000);
        //因為只有單頁，改為 滾動時判斷
        $(window).on('scroll', function(){
            $reload();
        })
    })
    
</script>

<?php if(false) { ?>
	<script type="text/javascript" >
		function showLoginPanel() {
			var rightid;
			for(i=0; i<$('[data-role="panel"]').length; i++)	{
				var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
				if(rtemp.match("right-panel-") == "right-panel-") {
					rightid = rtemp;
				}
			}
			var pageid = $.mobile.activePage.attr('id');
			// alert(pageid);
			$('#'+pageid+' #'+rightid).panel("open");
		}
		 showLoginPanel();
	</script>
<?php  }  ?>

</div>
</body>
