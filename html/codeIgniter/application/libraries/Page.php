<?php

 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class page {

	/*
	 *	分頁參數設定
	 */
    public function pageshow($pagenum, $url, $count)
    {

		$config['full_tag_open']	= ' <p> ';
		$config['full_tag_close']	= ' </p> ';
		$config['first_link']		= ' 首頁 ';
		$config['last_link']		= ' 末頁 ';
		$config['next_link']		= ' 下一頁>>';
		$config['prev_link']		= '<<上一頁 ';
		$config['per_page']			= $pagenum;				//每页展示几个项目
		$config['base_url']			= $url;					//包含分页控制器类和方法
		$config['total_rows']		= $count;				//需分页的总数据行数，我这里从数据库查询到
        //$config['num_links'] = 5;
		//$config['uri_segment'] = 3;
		$config['use_page_numbers'] = TRUE;
    $config['num_tag_open'] = ' ';
    $config['num_tag_close'] = ' ';
        return $config;

    }
}
