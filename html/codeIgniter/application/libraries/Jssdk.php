<?php

 if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class jssdk {

	public function getSignPackage($in_url='') {

		$jsapiTicket = $this->getJsApiTicket();
		
		// 注意 URL 一定要动态获取，不能 hardcode.
		$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
		$url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if(!empty($in_url)) {
			$fn_array=explode("&_=",$in_url);//
			$url = $fn_array[0]; 
		} 

		$timestamp = time();
		$nonceStr = $this->createNonceStr();

		// 这里参数的顺序要按照 key 值 ASCII 码升序排序
		$string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";  
		$signature = sha1($string);

		$signPackage = array(
		  "appId"     => 'wx110f9758932fb5ab',
		  "nonceStr"  => $nonceStr,
		  "timestamp" => $timestamp,
		  "url"       => $url,
		  "signature" => $signature,
		  "rawString" => $string
		);
        error_log("[jssdk] signPackage : ".json_encode($signPackage));
		return $signPackage; 
	}

	public function createNonceStr($length = 16) {
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		$str = "";
		for ($i = 0; $i < $length; $i++) {
		  $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
		}
		return $str;
	}

	private function getJsApiTicket() {

		$accessToken = $this->getAccessToken();
		
		// 如果是企业号用以下 URL 获取 ticket
		$url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken&type=jsapi";
		$res = json_decode($this->httpGet($url));
		$ticket = $res->ticket;
		if ($ticket) {
			$data->expire_time = time() + 7000;
			$data->jsapi_ticket = $ticket;
		}
		error_log("[libraries/jssdk] ticket : ".json_encode($ticket));
		return $ticket;
	}

	private function getAccessToken() {
		
		// 如果是企业号用以下URL获取access_token
		$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wx110f9758932fb5ab&secret=3d8410f84ca8d8606c426acf1e2fca09";
		$res = json_decode($this->httpGet($url));
		$access_token = $res->access_token;
		if ($access_token) {
			$data->expire_time = time() + 7000;
			$data->access_token = $access_token;
		}
        error_log("[libraries/jssdk] token : ".json_encode($access_token));
		return $access_token;
	}

	private function httpGet($url) {
		// $curl = curl_init();
		// curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		// curl_setopt($curl, CURLOPT_TIMEOUT, 500);
		// 为保证第三方服务器与微信服务器之间数据传输的安全性，所有微信接口采用https方式调用，必须使用下面2行代码打开ssl安全校验。
		// 如果在部署过程中代码在此处验证失败，请到 http://curl.haxx.se/ca/cacert.pem 下载新的证书判别文件。
		// curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, true);
		// curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
		// curl_setopt($curl, CURLOPT_URL, $url);

		// $res = curl_exec($curl);
		// curl_close($curl);
		// return $res;
		
		// 送出json內容並取回結果
		$response = file_get_contents($url);
		return $response;

	}

	private function get_php_file($filename) {
		return trim(substr(file_get_contents($filename), 15));
	}
	
	private function set_php_file($filename, $content) {
		$fp = fopen($filename, "w");
		fwrite($fp, "<?php exit();?>".$content);
		fclose($fp);
	}
	
}