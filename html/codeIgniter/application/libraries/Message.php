<?php 

 if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class message {
	
    function sys_messege($var)
    {
        if ($var != '')
		{
			log_message('error', 'Some variable did not contain a value.');
		} else {
			log_message('debug', $var);
		}

		//log_message('info', 'The purpose of some variable is to provide some value.');
    }  

    function getLangTerr($accept_lang) {
		if(strtok(strtolower($accept_lang),",")=='zh-tw') {
		   return "zh_TW";    
		} else {
		   return "zh_CN";       
		}
	} 
}