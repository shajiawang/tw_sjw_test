<?php

 if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class inbetween {
	
	/* 
	 *	分頁參數設定
	 */
    public function Inbetween($start, $end, $str)  
    {  
		$matches = array();
		$regex = "/$start([a-zA-Z0-9_]*)$end/";
		preg_match_all($regex, $str, $matches);
		return $matches[1];
    } 
	
}