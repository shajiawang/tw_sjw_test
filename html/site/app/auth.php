<?php
session_start();

if(empty($_SESSION['auth_id']) ) { //'请先登入会员账号'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");
	include_once(LIB_DIR ."/convertString.ini.php");
	include_once(LIB_DIR ."/ini.php");
	include_once(BASE_DIR ."/model/user.php");
	//$app = new AppIni; 

	$c = new UserAuth;

	if($_POST['type']=='check_sms') {
		$c->check_sms();
	} else if($_POST['type']=='verify_phone') {
		$c->verify_phone();
	} else if($_POST['type']=='upd_verify_phone') {
	    $r=$c->update_phone();
		if($r['status']==1) {
		   $c->verify_phone();
		} else {
		   echo json_encode($r);
		}
	}
	else {
		$c->home();
	}
}


class UserAuth 
{
	public $str;
	
	public function home()
	{
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$ret['status'] = 0;
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}


	//傳送簡訊驗證碼
	public function verify_phone()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		
		$ret['status'] = 0;
		
		$chk1 = $this->chk_phone();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
		}
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	//手机检查
	/*
	   手機號碼09開頭=>臺灣
	   手機號碼1開頭=>大陸
	*/
	public function chk_phone()
	{
		global $db, $config, $usermodel;
	
		$r['err'] = '';
		
		if (empty($_POST['phone'])) {
			//'手机号码不能空白' 
			$r['err'] = 2;
		} else if (ctype_digit($_POST['phone'])==false) {
		    //'手机号码非数字'
			$r['err'] = 9;
		} else{
			
			$user_auth = $usermodel->validUserAuth($_SESSION['auth_id'], $_POST['phone']);
			
			if (empty($user_auth) ) {
				//'手机号码不正确' 
				$r['err'] = 3;
			} 
			else 
			{
				if($user_auth['verified']=='Y') {
					//('手机号码已验证!!', $this->config->default_main);
					$r['err'] = 4;
				}
				else
				{
					$uid = $_SESSION['auth_id']; 
					$code = $user_auth['code']; 
					$user_phone = $_POST['phone'];
					$user_phone_0 = substr($user_phone, 0, 1);
					$user_phone_1 = substr($user_phone, 1, 1);

					if($user_phone_0 == '0' && $user_phone_1 == '9') {
						$phone = $user_phone;
						$area = 1;
					}
					else if($user_phone_0 == '1') {
						$phone = $user_phone;
						$area = 2;
					}
					else {
						//'手机号码只提供台灣及大陸驗證' 
						$r['err'] = 5;
					}
					
					if(! $this->mk_sms($uid, $phone, $code, $area) ) {
						//'手机号码不正确' 
						$r['err'] = 3;
					}
				}
			}
		}
		
		//if (!is_numeric($_POST['phone']) ) {
		    //只允许13、150、151、158、159和189开头并且长度是11位的手机号码，事实上现在可用的手机号码格式非常多
			//if(preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/", $_POST['phone']) ){ //验证通过 }
			//else{ //'此手机号码格式不对' $r['err'] = 5; }
			
			//'手机号码格式不正确'
			//$r['err'] = 5;
		//}
		
		return $r;
	}
	
	//SMS驗證碼
	/*
	   手機號碼09開頭=>臺灣, 呼叫簡訊王傳送驗證碼
	   手機號碼1開頭=>大陸, 呼叫中國短信網傳送驗證碼
	*/
	public function mk_sms($uid, $phone, $code, $area)
	{
		global $db, $config;
		
		if ($area == 1) {
			//簡訊王 SMS-API
			$sMessage = urlencode("ShaJiaWang: ". $code ."");
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
		  
			if(!$getfile=file($to_url)) {
				//('ERROR: SMS-API 无法连接 !', $this->config->default_main ."/user/register");
				return false;
			}
			else 
			{
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				if($kmsgid < 0) {
					//('手机号码错误!!', $this->config->default_main ."/user/register");
					return false;
				}
				else {
					return true;
				}
			}
		}
		else if ($area == 2) {
			//中國短信網
			$url = 'http://smsapi.c123.cn/OpenPlatform/OpenApi';
			
			$data = array
			(
				'action' => 'sendOnce',
				'ac' => '1001@501091960001',
				'authkey' => 'F99AD9CBBB17B21DEA3494115E228D30',
				'cgid' => '184',
				'm' => $phone,
				'c' => $code.'（手机认证验证码，三十分钟内有效）',
				'csid' => '',
				't' => ''
			);
			$xml = $this->postSMS($url,$data);
		    $re = simplexml_load_string(utf8_encode($xml));
			if (trim($re['result']) == 1)
			{
			    return true;
		    }
			else
			{
				return false;
			}
		}
	}
	
	// 以HTTP Post 方式傳送發簡訊的request (中國短信網用)
	public function postSMS($url, $data=''){
		$row = parse_url($url);
		$host = $row['host'];
		$port = $row['port'] ? $row['port'] : 80;
		$file = $row['path'];
		while (list($k,$v) = each($data)) 
		{
			$post .= rawurlencode($k)."=".rawurlencode($v)."&";
		}
		$post = substr($post , 0 , -1);
		$len = strlen($post);
		$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
		if (!$fp) {
			return "$errstr ($errno)\n";
		} else {
			$receive = '';
			$out = "POST $file HTTP/1.0\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Content-type: application/x-www-form-urlencoded\r\n";
			$out .= "Connection: Close\r\n";
			$out .= "Content-Length: $len\r\n\r\n";
			$out .= $post;		
			fwrite($fp, $out);
			while (!feof($fp)) {
				$receive .= fgets($fp, 128);
			}
			fclose($fp);
			$receive = explode("\r\n\r\n",$receive);
			unset($receive[0]);
			return implode("",$receive);
		}
	}
	
	//SMS完成驗證
	/*
	   檢查session中的userid和($_POST)phone no是否有對應資料(saja_user_profile & saja_user_sms_auth)
	   No->回傳錯誤訊息
	   Yes->修改saja_user_sms_auth中該userid的check code
	        檢查是否有介紹人(saja_scode_history)
	        No->直接回傳完成訊息
			Yes->介紹人贈送S碼(saja_scode)
			     介紹人S碼領取狀態enable(saja_scode_history)
			     回傳完成訊息
	*/
	public function check_sms()
	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		
		$ret['status'] = 0;
		
		$user_auth = $usermodel->validUserAuth($_SESSION['auth_id'], $_POST['phone']);
			
		if (empty($user_auth) ) {
			//'手机号码不正确' 
			$ret['status'] = 2;
		}
		elseif (empty($_POST['smscode']) || ($_POST['smscode'] !== $user_auth['code']) ) {
			//'验证码不正确' 
			$ret['status'] = 3;
		}
		
		if(empty($ret['status']) ) 
		{
			//修改SMS check code
			$shuffle = get_shuffle();
			$checkcode = substr($shuffle, 0, 6);
				
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
			SET verified='Y', code='{$checkcode}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$_SESSION['auth_id']}'
			";
			$db->query($query);
			
			//檢查是否介紹人(送scode)
			$query = "select * from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history`
				where `prefixid` = '{$config['default_prefix_id']}' 
				and memo = '{$_SESSION['auth_id']}'
				and switch = 'N'";
			$table = $db->getQueryRecord($query);
			
			if(!empty($table['table']['record'])) {
				//return $table['table']['record'];
				$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` 
				SET switch = 'Y', modifyt = NOW() 
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `scodeid` = '{$table['table']['record'][0]['scodeid']}' 
					and `userid` = '{$table['table']['record'][0]['userid']}'
				";
				$db->query($query);
				
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}scode_history` 
				SET switch = 'Y', modifyt = NOW() 
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}'
					AND `sphid` = '{$table['table']['record'][0]['sphid']}'
				";
				$db->query($query);
			}
			
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	public function update_phone() {
	       
		   global $db, $config, $usermodel;
			
		   $userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
		   $phone= $_POST['phone'];
		   $ret= array();
		   $ret['status']='';
		   try {
			   if(empty($userid)) {
			      $ret['status']=-1;
			   } else if(empty($phone)){
			      $ret['status']=-4;
			   } else if(ctype_digit($phone)==false) {
			      $ret['status']=-5;
			   } else {
				   // 初始化資料庫連結介面
				   $db = new mysql($config["db"]);
				   $db->connect();
				   
				   // 檢查重複
				   $query = " SELECT count(userid) as dup_cnt from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
				              where prefixid='{$config['default_prefix_id']}' and userid!='{$userid}' and name='{$phone}' ";
				   $table = $db->getQueryRecord($query);
                   if($table['table']['record'][0]['dup_cnt']>0) {
					      $ret['status']=-2;
				   } else {				   
					   $query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
							SET name='{$phone}' 
							WHERE `prefixid`='{$config['default_prefix_id']}' 
							AND `userid`='{$userid}' 
						";
						$ret['status']=$db->query($query);
						$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							SET phone='{$phone}' 
							WHERE `prefixid`='{$config['default_prefix_id']}' 
							AND `userid`='{$userid}' 
						";
						$ret['status']=$db->query($query);
						$_SESSION['user']['profile']['phone']=$phone;
						$_SESSION['user']['name']=$phone;
				  }
			  } 
	      } catch (Exception $e) {
			  $ret['status'] = -99;		 
		  } finally {
		      return $ret;
			 // echo json_encode($ret);
		  }
	}
	
}
?>