<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");	
include_once(LIB_DIR ."/ini.php");
//$app = new AppIni; 

$c = new Register;
$c->home();

class Register
{
	public $str;
	
	public function home()
	{
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$this->str = new convertString();
		
		$ret['status'] = 0;
		$ret['pid'] = 0;
			
		$chk1 = $this->chk_nickname();
		$chk2 = $this->chk_phone();
		$chk3 = $this->chk_passwd();
		$chk4 = $this->chk_product();
		
		if($_POST['type'] == 'flash') {
			if(!empty($chk4['err'])) {
			   $ret['status'] = $chk4['err'];
			}
		}
		
		if ($_POST['type'] != 'flash') {
			if(!empty($chk1['err'])) {
				$ret['status'] = $chk1['err'];
			}
			if(!empty($chk3['err'])) {
				$ret['status'] = $chk3['err'];
			}
		}
		
		if(!empty($chk2['err'])) {
			$ret['status'] = $chk2['err'];
		}
		if(!empty($_POST['sso_uid']))
		   $ret['status']=$this->chk_sso();
		
		if(empty($ret['status'])) 
		{
			//建立会员账号
			$this->mk_user();
			
			$ret['sessid'] = session_id();
			
			if ($_POST['type'] == 'flash') {
				$ret['pid'] = $_POST['productid'];
			}
			
			//回傳: 完成
			$ret['status'] = 200;
			
		}
		error_log("[ajax/user_register] home return :".json_encode($ret));
		echo json_encode($ret);
	}
	
	// SSO 檢查
	public function chk_sso() {
	       // 先檢測SSO
		global $db, $config;
	    $r['err'] = '';
		
	    // $sso_name = $_POST['sso_name']; //$_SESSION['sso']['provider'];
		$sso_uid = $_POST['sso_uid']; //$_SESSION['sso']['identifier'];
				
		$query= "SELECT ssoid FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
		         WHERE `prefixid`='{$config['default_prefix_id']}' 
				   AND `uid`='{$sso_uid}'
				   AND  `switch`='Y' ";
				   
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			if($table['table']['record'][0]['ssoid']>=1) {
			   error_log("[ajax/user_register] uid : ".$sso_uid." is existed and will not create sso mapping !!");
			   $r['err']=16;		   
			}
		}
		return $r['err'];
	}
	
	//昵称检查
	public function chk_nickname()
	{
		global $db, $config;
	
		$r['err'] = '';
		$nickname = trim($_POST['nickname']);
		
		if (empty($nickname)) {
			//'请填写昵称' 
			$r['err'] = 2;
		}
		else {
		    /*
			$query = "SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `nickname` = '{$_POST['nickname']}' 
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			
			if (!empty($table['table']['record'])) {
				//'此昵称已存在' 
				$r['err'] = 3;
			}
			*/
			$query = "SELECT count(*) as cnt
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND (`nickname` = '{$_POST['nickname']}' OR `nickname` like '{$_POST['nickname']}\_\_%')
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			// error_log("[ajax/user_register] cnt : ".$table['table']['record'][0]['cnt']);
			if (!empty($table['table']['record']) && $table['table']['record'][0]['cnt']>0) {
			    $_POST['nickname']=	$_POST['nickname']."__".($table['table']['record'][0]['cnt']);
			}
		}
		
		return $r;
	}
	
	//手机号码检查
	public function chk_phone()
	{
		global $db, $config;
	
		$r['err'] = '';
		
		if(!empty($_POST['sso_name'])) {
		   return $r['err'];
		}

		if (empty($_POST['phone'])) {
			//'请填写手机号码' 
			$r['err'] = 4;
		} else if (!is_numeric($_POST['phone'])) {
		    //只允许13、150、151、158、159和189开头并且长度是11位的手机号码，事实上现在可用的手机号码格式非常多
			//if(preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/", $_POST['phone']) ){ //验证通过 }
			//else{ //'此手机号码格式不对' $r['err'] = 5; }
			
			//'手机号码格式不正确'
			$r['err'] = 5;
		} else if(strpos($_POST['phone'],".")!=false || strpos($_POST['phone'],"+")!=false ||  strpos($_POST['phone'],"-")!=false) {
		    $r['err'] = 5;
		} else {
			/*
			$query = "SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `phone` = '{$_POST['phone']}' 
				AND switch = 'Y'
			";
			*/
			$query = "SELECT *FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			WHERE `prefixid` = '{$config['default_prefix_id']}' AND `name` = '{$_POST['phone']}' AND switch = 'Y' ";
			
			// error_log("[ajax/user_register] : ".$query);
			$table = $db->getQueryRecord($query);
			
			if (!empty($table['table']['record'])) {
				//'此手机号码已存在' 
				$r['err'] = 6;
			}
		}
		return $r;
	}
	
	//检查密码
	public function chk_passwd()
	{
		global $db, $config;
	
		$r['err'] = '';
		
		if (empty($_POST['passwd']) ) {
			//'请填写密码' 
			$r['err'] = 7;
		}
		elseif (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd']) ) {
			//'密码格式不正确'
			$r['err'] = 8;
		}
		elseif (empty($_POST['repasswd']) ) {
			//'请填写确认密码'
			$r['err'] = 9;
		}
		elseif ($_POST['passwd'] != $_POST['repasswd']) {
			//'密码确认不正确'
			$r['err'] = 10;
		}
		elseif (!empty($_POST['expasswd']) && !preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['expasswd'])) {
			//'兑换密码格式不正确'
			$r['err'] = 11;
		}
		elseif (!empty($_POST['expasswd']) && empty($_POST['exrepasswd'])) {
			//'请填写兑换密码确认'
			$r['err'] = 12;
		}
		elseif ($_POST['expasswd'] != $_POST['exrepasswd']) {
			//'兑换密码确认不正确'
			$r['err'] = 13;
		}
		
		return $r;
	}
	
	//檢查閃殺活動
	public function chk_product()
	{
		global $db, $config;
	
		$r['err'] = '';
		$productid = trim($_POST['productid']);
		
		if (empty($productid)) {
			//无此闪杀活动
			$r['err'] = 14;
		}
		else {
			$query = "SELECT *
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `productid` = '{$_POST['productid']}' 
				AND `is_flash` = 'Y' 
				AND `display` = 'Y' 
				AND `closed` = 'N' 
				and now() between `ontime` and `offtime` 
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			
			if (empty($table['table']['record'])) {
				//闪杀活动已结束，下次请早
				$r['err'] = 15;
			}
		}
		
		return $r;
	}
	
	//建立会员账号
	public function mk_user()
	{
		global $db, $config;
		
		//自動產生密碼
		$shuffle = get_shuffle();
		$pwcode = substr($shuffle, 0, 6);
		$reg_type=$_POST['type'];
		if ($reg_type== 'flash') {
			$passwd = $this->str->strEncode($pwcode, $config['encode_key']);
		}else {
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);
		}
		
		$exchangepasswd = $this->str->strEncode(substr($_POST['phone'], -6), $config['encode_key']);
       			
		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`name`='{$_POST['phone']}',
			`passwd`='{$passwd}',
			`exchangepasswd`='{$exchangepasswd}',
			`email`='',
			`insertt`=NOW()
		";
		
		$db->query($query);			
		$userid = $db->_con->insert_id;
	

        /*
		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`nickname`='{$_POST['nickname']}',
			`gender`='{$_POST['gender']}',
			`cityid`='',
			`area`='',
			`address`='',
			`addressee`='',
			`phone`='{$_POST['phone']}',
			`insertt`=NOW()
		";
		*/
		$countryid = $config['country'];
		$regionid = $config['region'];
		$provinceid = $config['province'];
		$channelid = $config['channel'];
			
		if ($reg_type == 'flash') {
			$query = "
			select ch.channelid, ch.provinceid, pr.regionid, ch.countryid 
			from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel` ch 
			left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` pr on
				pr.prefixid = ch.prefixid 
				and pr.provinceid = ch.provinceid
				and pr.switch = 'Y'
			where 
				ch.prefixid = '{$config['default_prefix_id']}' 
				and ch.channelid = '{$_POST['channelid']}'
				and ch.switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
		}
		
		if(!empty($table['table']['record'][0]['countryid'])) {
		   //$countryid = $_POST['countryid'];
		   $countryid = $table['table']['record'][0]['countryid'];
		}
		if(!empty($table['table']['record'][0]['regionid'])) {
		   //$regionid = $_POST['regionid'];
		   $regionid = $table['table']['record'][0]['regionid'];
		}
		if(!empty($table['table']['record'][0]['provinceid'])) {
		   //$provinceid = $_POST['provinceid'];
		   $provinceid = $table['table']['record'][0]['provinceid'];
		}
		if(!empty($table['table']['record'][0]['channelid'])) {
		   //$channelid = $_POST['channelid'];
		   $channelid = $table['table']['record'][0]['channelid'];
		}

		// 記錄連線來源
		// 抓來源IP
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
			$ip = $temp_ip[0];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		// 抓User Agent
		$user_agent='BROWSER';
		$memo=$_SERVER['HTTP_USER_AGENT'];
		if(strpos($memo, 'MicroMessenger')>0) {
		   $user_agent='WEIXIN';  
		}
		$productid=$_POST['productid'];
		$src_from = 'SAJA';
        $intro_by='';
		$act='REG';
		$goto='';
		
		
		if ($reg_type=='flash') {
		    if(empty($_POST['nickname']))
			   $_POST['nickname'] = '_guest_'.$userid;
			if(empty($_POST['gender']))
			   $_POST['gender'] = 'male';
			$src_from = 'FLASH';
		} else if ($reg_type=='sso') {
			$src_from = strtoupper($_POST['sso_name']);
		}
				
		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`nickname`='{$_POST['nickname']}',
			`gender`='{$_POST['gender']}',
			`countryid`='{$countryid}',
			`regionid`='{$regionid}',
			`provinceid`='{$provinceid}',
			`channelid`='{$channelid}',
			`thumbnail_url`='{$_POST['thumbnail_url']}',
			`area`='',
			`address`='',
			`addressee`='',
			`phone`='{$_POST['phone']}',
			`src_from` = '{$src_from}|{$productid}',
			`insertt`=NOW()
		";
		
		$db->query($query);

		//新增SMS check code
		$shuffle = get_shuffle();
		$checkcode = substr($shuffle, 0, 6);
		
		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`code`='{$checkcode}',
			`verified`='N',
			`insertt`=NOW()
		";
		$db->query($query);
					
		if ($reg_type=='flash') {
			//閃殺活動
			$this->spoint_by_flash($userid);
			
			$user_phone_0 = substr($_POST['phone'], 0, 1);
			$user_phone_1 = substr($_POST['phone'], 1, 1);
			
			if($user_phone_0 == '0' && $user_phone_1 == '9') {
				$area = 1;
			}
			else if($user_phone_0 == '1') {
				$area = 2;
			}
			else {
				//'手机号码只提供台灣及大陸驗證' 
				$r['err'] = 5;
			}
			//傳簡訊
			if($area==1) {
			   $this->mk_sms($userid, $_POST['phone'], $pwcode, $area);
            }			
		}
		
		if(!empty($_POST['user_src'])) {
			/*拉人進來的會員送S碼
			$scodeModel = new ScodeModel;  
			$scodeModel->register_scode($userid, $_POST['user_src']);
			*/
			// 拉人進來的會員
    		$this->str = new convertString();
			$user_src = $this->str->decryptAES128($config['encode_key'], base64_decode($_POST['user_src']));
		    $arr = explode("&&", $user_src);
			if(is_array($arr)) {
			   $intro_by=$arr[0];
			}
						
			if(!empty($_POST['sso_uid']) && !empty($_POST['productid'])) {
               $insert="insert into saja_user.saja_passphrase set openid='{$_POST['sso_uid']}', 
				userid='{$userid}', 
				user_src='{$_POST['user_src']}', 
			    productid='{$_POST['productid']}', 
				switch='N', insertt=NOW(), modifyt=NOW() "; 
                error_log("[ajax/user_register] passphrase :".$insert);	
                $db->query($insert);
                
				$intro_by=$_POST['user_src'];				
			}
		}
		
		/*		
		if($reg_type=='sso') {
			//建立会员SSO账号
			$this->mk_sso($userid);
		}
		*/
				
		if(!empty($_POST['sso_uid']) && !empty($userid)) {
			//建立会员SSO账号
			$this->mk_sso($userid);
		}
		
		//註冊送限定S碼活動
		$this->give_oscode_by_promote($userid);
		
		// 設成已登入
		$this->mk_login($userid);
		
		// 記錄連線及註冊來源
		$this->logAffiliate($ip, $intro_by, $src_from, $productid, $userid, $goto, $memo, $user_agent );
		
		error_log("[ajax/user_register] mk_user : {type:".$reg_type.",userid:".$userid.",user_src:".$intro_by.",pwd:".$_POST['passwd'].",expwd:".substr($_POST['phone'], -6)."}");

	}
	
	public function logAffiliate($come_from, $intro_by, $act, $productid, $userid, $goto, $memo, $user_agent ) {
	       global $db, $config;
	       $insert = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` SET
            	   	    `come_from`='{$come_from}',
			    		`intro_by`='{$intro_by}',
						`act`='{$act}',
						`productid`='{$productid}',
						`userid`='{$userid}',
						`goto`='{$goto}',
						`memo`='{$memo}',
						`user_agent`='{$user_agent}',
						`insertt`=NOW(),
						`modifyt`=NOw() ";
			return $db->query($insert);
	}
	
	//送限定S碼活動
	public function give_oscode_by_promote($userid) {
    	global $db, $config;
		
		$query = "SELECT * 
		FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `switch`='Y'
			AND `behav`='b'
			AND NOW() between `ontime` and `offtime` 
			AND `productid` > 0 ";
		
		//error_log("[give_oscode_by_promoite] ".$query);
		$table = $db->getQueryRecord($query);
		$total = count($table['table']['record']);
		
		if($total > 0) {
			//error_log("[give_oscode_by_promoite] promotes total:".$total);
			for($i=0;$i<$total;++$i) {
				$spid = $table['table']['record'][$i]['spid'];
				$onum = $table['table']['record'][$i]['onum'];
				$prodid=$table['table']['record'][$i]['productid'];
				//error_log("[oscode_by_promoite] spid: ".$spid." onum: ".$onum." productid: ".$prodid);
				if($onum>0) {
					for($j=0;$j<$onum;++$j) {
						$insert = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		                SET 
		                	prefixid = '{$config['default_prefix_id']}',
                            userid = '{$userid}',
							spid = '{$spid}',
							productid = '{$prodid}',
							behav = 'user_register',
							amount = 1,
							insertt = NOW()";
						if($j==0) {
							error_log($insert);
						}
						$db->query($insert);
					}
					if($spid>0) {
						$update = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` 
						SET 
							amount=amount+1 ,
							scode_sum=scode_sum+".$onum." 
						WHERE 
							spid='".$spid."'"; 
						//error_log("[oscode_by_promoite] update : ".$update);
						$db->query($update);
					}
				}
            }
				 
        } else {
			error_log("[give_oscode_by_promoite] No oscode by promote to give..");
		}
    }	
	
	//建立閃殺活動
	public function spoint_by_flash($userid)
	{
		global $db, $config;
		
		$query = "SELECT userid, by_flash
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND userid = '{$userid}' 
			AND switch = 'Y' 
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		
		if( isset($table['table']['record'][0]) && $table['table']['record'][0]['by_flash'] !=='Y') 
		{
			/*$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint`
			SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$userid}',
			   `countryid`='{$config['country']}',
			   `behav`='by_flash',
			   `amount`='1000',
			   `insertt`=NOW()
			";
			$db->query($query);*/
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			SET
			   `by_flash`='Y'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND userid = '{$userid}' 
				AND switch = 'Y' 
			";
			$db->query($query);
		}
	}
	
	//建立会员SSO账号
	public function mk_sso($userid)
	{
		global $db, $config;
		
		$sso_name = $_POST['sso_name']; //$_SESSION['sso']['provider'];
		$sso_uid = $_POST['sso_uid']; //$_SESSION['sso']['identifier'];
     		
		//加入SSO資料
		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`name`='{$sso_name}',
			`uid`='{$sso_uid}',
			`insertt`=NOW()
		";
		$db->query($query);
		$ssoid = $db->_con->insert_id;

		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`ssoid`='{$ssoid}',
			`insertt`=NOW()
		";
		$db->query($query);
	}
	
	// 設成已登入
	public function mk_login($userid)
	{
		global $db, $config, $usermodel;
		
		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';
		
		$query = "SELECT u.* 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u 
		WHERE 
			u.prefixid = '{$config['default_prefix_id']}' 
			AND u.userid = '{$userid}' 
			AND u.switch = 'Y' 
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);

		$user = $table['table']['record'][0];
		
		$query = "SELECT *
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `userid` = '{$userid}'
			AND `switch` =  'Y'
		";
		$table = $db->getQueryRecord($query);
		
		unset($user['passwd']);
		$user['profile'] = $table['table']['record'][0];
		$_SESSION['user'] = $user;
		
		// Set session and cookie information.
		$_SESSION['auth_id'] = $userid;
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = md5($userid . $user['name']);
		setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", md5($userid . $user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);

		// Set local publiciables with the user's info.
		$usermodel->user_id = $userid;
		$usermodel->name = $user['profile']['nickname'];
		$usermodel->email = '';
		$usermodel->ok = true;
		$usermodel->is_logged = true;
	}
	
	//SMS驗證碼
	public function mk_sms($uid, $phone, $code, $area)
	{
		global $db, $config;
		
		if ($area == 1) {
			//簡訊王 SMS-API
			$message = "殺價王密碼: ".$code.",兌換密碼：".substr($phone, -6);
			$sMessage = urlencode(mb_convert_encoding($message, 'BIG5', 'UTF-8'));
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
		  
			if(!$getfile=file($to_url)) {
				error_log('ERROR: SMS-API 无法连接 !', $this->config->default_main ."/user/register");
				return false;
			}
			else 
			{
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				if($kmsgid < 0) {
					error_log('手机号码错误!!', $this->config->default_main ."/user/register");
					return false;
				}
				else {
					return true;
				}
			}
		}
		else if ($area == 2) {
			//中國短信網
			$url = 'http://smsapi.c123.cn/OpenPlatform/OpenApi';
			
			$data = array
			(
				'action' => 'sendOnce',
				'ac' => '1001@501091960001',
				'authkey' => 'F99AD9CBBB17B21DEA3494115E228D30',
				'cgid' => '184',
				'm' => $phone,
				'c' => $code.'（手机密码）'.substr($phone,-6).'（兑换密码）',
				'csid' => '',
				't' => ''
			);
			$xml = $this->postSMS($url,$data);
		    $re = simplexml_load_string(utf8_encode($xml));
			if (trim($re['result']) == 1)
			{
			    return true;
		    }
			else
			{
				return false;
			}
		}
	}
}
?>