<?php
session_start();

if(empty($_SESSION['auth_id']) ) { //'请先登入会员账号'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/convertString.ini.php");
	include_once(LIB_DIR ."/ini.php");
	//$app = new AppIni; 
	
	$c = new Mall;
	if ($_POST['type'] == "qrcode") {
		$c->qrcode();
	}
	else {
		$c->home();
	}
}

	
class Mall 
{
	public $userid = '';
	
	//下标
	public function home()
	{
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
			
		$ret['status'] = 0;
	
		//兌換數量
		$order_info['num'] = isset($_POST['num']) ? (int)$_POST['num'] : 0;
		
		// Check Variable Start
		
		if(empty($order_info['num']) || empty($_GET['epid']) ) {
			$ret['status'] = 0;
		}
		
		$chk = $this->expw_check();
		
		if($chk['err']) {
			$ret['status'] = $chk['err'];
		}
		
		$query ="SELECT pc.epcid 
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` pc 
		left outer join `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category_rt` rt on 
			pc.prefixid = rt.prefixid 
			AND pc.epcid = rt.epcid
			AND rt.switch = 'Y'
		WHERE 
			pc.prefixid = '{$config['default_prefix_id']}' 
			AND rt.epid = '{$_GET['epid']}' 
			AND pc.switch = 'Y' 
			ORDER BY pc.seq
		";
		$epcArr = $db->getQueryRecord($query); 
		
		/*if ($epcArr['table']['record'][0]['epcid'] != 38) {
			if(empty($_POST['name']) ) {
				//('收件人姓名错误!!');
				$ret['status'] = 2;
			}
			elseif(empty($_POST['zip']) ) {
				//('收件人邮编错误!!');
				$ret['status'] = 3;
			}
			elseif(empty($_POST['address']) ) {
				//('收件人地址错误!!');
				$ret['status'] = 4;
			}
		}*/
		
		if(empty($ret['status']) ) 
		{
			$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
			WHERE 
				p.`prefixid` = '{$config['default_prefix_id']}'  
				AND p.epid = '{$_GET['epid']}'
				AND p.switch = 'Y'
				LIMIT 1
			" ;
			$table = $db->getQueryRecord($query); 
			//if (empty($table['table']['record'])) { $this->jsPrintMsg('商品不存在!', $location_url); }
			//if ($table['table']['record'][0]['now'] > $table['table']['record'][0]['offtime']) { $this->jsPrintMsg('商品兑换已结束', $location_url); }

			$retail_price = ($table['table']['record'][0]['retail_price']) ? (float)$table['table']['record'][0]['retail_price'] : 0;
			$cost_price = ($table['table']['record'][0]['cost_price']) ? (float)$table['table']['record'][0]['cost_price'] : 0;
			$process_fee = ($table['table']['record'][0]['process_fee']) ? (float)$table['table']['record'][0]['process_fee'] : 0;
			$order_info['point_price'] = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;

			//分潤=(市價-進貨價)*數量
			$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

			//商品兌換總點數
			$used_point = $order_info['point_price'] * $order_info['num'];

			//商品處理費總數 $process_fee * $num;
			$order_info['used_process'] = $process_fee;

			//總費用
			$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			
		
			//檢查使用者的紅利點數
			$query ="SELECT SUM(amount) bonus 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `switch` = 'Y'
				AND `userid` = '{$this->userid}' 
			";
			$recArr = $db->getQueryRecord($query); 
			$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;

			if($user_bonus < $order_info['total_fee']) {
				//('红利点数不足');	
				$ret['status'] = 5;
			}
			
			//查詢企業id
			$query = "SELECT enterpriseid 
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `esid` = '{$_GET['esid']}' 
				AND `switch` = 'Y'
			";
			$recArr = $db->getQueryRecord($query); 
			$order_info['enterpriseid'] = $recArr['table']['record'][0]['enterpriseid'];
		}
		
		
		if(empty($ret['status']) ) 
		{
			//產生訂單記錄
			$mk = $this->mk_order($order_info);
				
			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
		}
		
		echo json_encode($ret);
	}
	
	//qrcode兌換商品
	public function qrcode() {
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
		
		//兌換數量
		$order_info['num'] = 1;
		
		// Check Variable Start
		
		if(empty($order_info['num']) || empty($_POST['epid']) ) {
			$ret['status'] = 0;
		}
		
		//查詢兌換密碼
		$chk = $this->expw_check();
		
		if($chk['err']) {
			$ret['status'] = $chk['err'];
		}
		
		if(empty($ret['status']) ) 
		{
			if ($_POST['epid'] > 0) {
				$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
				FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
				WHERE 
					p.`prefixid` = '{$config['default_prefix_id']}'  
					AND p.epid = '{$_POST['epid']}'
					AND p.switch = 'Y'
					LIMIT 1
				" ;
				$table = $db->getQueryRecord($query); 
				//if (empty($table['table']['record'])) { $this->jsPrintMsg('商品不存在!', $location_url); }
				//if ($table['table']['record'][0]['now'] > $table['table']['record'][0]['offtime']) { $this->jsPrintMsg('商品兑换已结束', $location_url); }
	            
				$retail_price = ($table['table']['record'][0]['retail_price']) ? (float)$table['table']['record'][0]['retail_price'] : 0;
				$cost_price = ($table['table']['record'][0]['cost_price']) ? (float)$table['table']['record'][0]['cost_price'] : 0;
				$process_fee = ($table['table']['record'][0]['process_fee']) ? (float)$table['table']['record'][0]['process_fee'] : 0;
				$order_info['point_price'] = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;
	            
				//分潤=(市價-進貨價)*數量
				$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];
	            
				//商品兌換總點數
				$used_point = $order_info['point_price'] * $order_info['num'];
	            
				//商品處理費總數 $process_fee * $num;
				$order_info['used_process'] = $process_fee;
	            
				//總費用
				$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			}
            else {
            	$query ="select * from `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
            	WHERE 
					`prefixid` = '{$config['default_prefix_id']}'  
					AND orderid = '{$_POST['orderid']}'
					AND switch = 'Y'
					LIMIT 1
				";
				$table = $db->getQueryRecord($query); 
				
				$order_info['point_price'] = $table['table']['record'][0]['point_price'];
				$order_info['used_process'] = 0;
				$order_info['total_fee'] = $table['table']['record'][0]['total_fee'];
				$order_info['profit'] = 0;
            }
            
			//檢查使用者的紅利點數
			$query ="SELECT SUM(amount) bonus 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `switch` = 'Y'
				AND `userid` = '{$this->userid}' 
			";
			$recArr = $db->getQueryRecord($query); 
			$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;
            
			if($user_bonus < $order_info['total_fee']) {
				//('红利点数不足');	
				$ret['status'] = 5;
			}
		}
		
		if(empty($ret['status']) ) 
		{
			//產生訂單記錄
			$mk = $this->set_order($order_info);
				
			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
		}
		
		echo json_encode($ret);
	}
	
	//產生訂單記錄
	public function mk_order($order_info)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$r['err'] = '';
		
		if(empty($r['err']) )
		{
			//新增訂單
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`status`='0',
				`epid`='{$_GET['epid']}',
				`esid`='{$_GET["esid"]}',
				`num`='{$order_info['num']}',
				`point_price`='{$order_info['point_price']}',
				`process_fee`='{$order_info['used_process']}',
				`total_fee`='{$order_info['total_fee']}',
				`profit`='{$order_info['profit']}',
				`memo` = '', 
				`confirm`='Y',
				`insertt`=NOW()
			";
			$res = $db->query($query); 
			$orderid = $db->_con->insert_id;
			
			//新增訂單收件人
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` 
			SET 
				`userid`='{$this->userid}',
				`orderid`='{$orderid}',
				`name`='{$_POST['name']}',
				`phone`='{$_SESSION['user']['profile']['phone']}',
				`zip`='{$_POST['zip']}',
				`address`='{$_POST['address']}',
				`gender`='{$_SESSION['user']['profile']['gender']}',
				`prefixid`='{$config['default_prefix_id']}',
				`insertt`=now()
			";
			$db->query($query); 
			
			//扣除點數
			$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$_SESSION['auth_id']}', 
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', 
			          `amount` = '-{$order_info['total_fee']}', 
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bonusid = $db->_con->insert_id;
			
			//商家增加點數(Frank-14/12/16)
			$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `bonusid` = '{$bonusid}', 
			          `esid` = '{$_GET["esid"]}', 
			          `enterpriseid` = '{$order_info['enterpriseid']}', 
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', 
			          `amount` = '{$order_info['total_fee']}', 
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bsid = $db->_con->insert_id;
			
			//新增紅利點數歷史資料(Frank-14/12/16)
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_store_history` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `esid` = '{$_GET["esid"]}', 
			          `orderid` = '{$orderid}',
			          `bsid` = '{$bsid}',
			          `enterpriseid` = '{$order_info['enterpriseid']}', 
			          `amount` = '{$order_info['total_fee']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			
			//新增商家紅利點數歷史資料
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$_SESSION['auth_id']}', 
			          `orderid` = '{$orderid}',
			          `bonusid` = '{$bonusid}',
			          `amount` = '-{$order_info['total_fee']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			
			
			
			//扣除庫存
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}stock` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `behav` = 'user_exchange', 
			          `epid` = '{$_GET['epid']}',
			          `num` = '-{$order_info['num']}', 
			          `orderid` = '{$orderid}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
		}
		
		return $r;
	}
	
	//兑换密码确认
	public function expw_check()
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$this->str = new convertString();
		$exchangepasswd = $this->str->strEncode($_POST['expw'], $config['encode_key']);
		
		$r['err'] = '';
		
		$query = "SELECT * 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$_SESSION['auth_id']}' 
			AND exchangepasswd = '{$exchangepasswd}' 
			AND switch = 'Y' 
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			//'兑换密码错误'
			$r['err'] = 6;
		}
		
		return $r;
	}
	
	//更改訂單記錄
	public function set_order($order_info)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$r['err'] = '';
		
		if(empty($r['err']) )
		{
			//更新訂單
			$query = "update `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			SET
				confirm = 'Y', 
				modifyt = NOW()
			where 
				orderid = '{$_POST['orderid']}'
				and userid = '{$_SESSION['auth_id']}'
				and confirm = 'N'
				and switch = 'Y'
			";
			
			$res = $db->query($query); 
			//$orderid = $db->_con->insert_id;
			
			//扣除點數
			$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$_SESSION['auth_id']}', 
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', 
			          `amount` = '-{$order_info['total_fee']}', 
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bonusid = $db->_con->insert_id;
			
			//新增紅利點數歷史資料
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$_SESSION['auth_id']}', 
			          `orderid` = '{$_POST['orderid']}',
			          `bonusid` = '{$bonusid}',
			          `amount`='-{$order_info['total_fee']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			
			//扣除庫存
			if ($_POST['epid'] > 0) {
				$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}stock` set 
				          `prefixid` = '{$config['default_prefix_id']}', 
				          `behav` = 'user_exchange', 
				          `epid` = '{$_POST['epid']}',
				          `num` = '-1', 
				          `orderid` = '{$_POST['orderid']}',
				          `seq` = '0', 
				          `switch` = 'Y', 
				          `insertt` = now()";
				$db->query($query);
			}
		}
		
		return $r;
	}
}
?>