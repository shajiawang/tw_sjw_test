<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/dbconnect.php");	
include_once(LIB_DIR ."/ini.php");
//$app = new AppIni; 

$c = new Logout;
$c->home();


class Logout 
{
	public function home()
	{
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
			
		$ret['status'] = 0;
		
		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_secret'] = '';
		$_SESSION['auth_email'] = '';
		
		setcookie("auth_id", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_email", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", "", time() - 3600, "/", COOKIE_DOMAIN);
		
		//回傳:
		$ret['status'] = 200;
		
		echo json_encode($ret);
	}
	
}
?>