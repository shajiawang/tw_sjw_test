// JavaScript Document

/* login.php */
function enterprise_login() {
	var pageid = $.mobile.activePage.attr('id');
	var ologinname = $.trim($('#'+pageid+' #loginname').val());
	var opw = $('#'+pageid+' #pw').val();
	
	if(ologinname=="" && opw=="") {
		alert('商家账号及密码不能空白');
	}
	else 
	{
		$.post("/site/ajax/enterprise.php?t="+getNowTime(), 
		{type:'enterprise_login', loginname:ologinname, passwd:opw}, 
		function(str_json) 
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status < 1) {
					alert('登录失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('杀价王商家欢迎您!');
						location.href = '/site/enterprise/home';
						//location.reload();
					}
					else if (json.status==201){ 
						location.reload();
					} 
					else if(json.status==2){ alert('请填写商家账号'); }
					else if(json.status==3){ alert('商家账号不存在'); }
					else if(json.status==4){ alert('请填写密码'); }
					else if(json.status==5){ alert('密码不正确'); }
				}
			}
		});
	}
}
