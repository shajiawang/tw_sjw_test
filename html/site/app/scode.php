<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");
include_once(BASE_DIR ."/model/oscode.php");
//$app = new AppIni; 

$c = new s_code;

if(empty($_SESSION['auth_id']) ) { //'请先登入会员账号'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	if($_POST['type']=='act_osc'){
		$c->act_osc();
	}
	else {
		$c->home();
	}
} 


class s_code 
{
	public $str;
	
	//S码激活
	public function home()
	{
		global $db, $config, $scodeModel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$scodeModel = new ScodeModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		if (empty($_POST['scsn']) || empty($_POST['scpw']) ) {
			//S码序号、S码密码不能空白
			$ret['status'] = 2;
		}
		else
		{
			$query = "SELECT * 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_item` 
			WHERE 
				prefixid = '{$config['default_prefix_id']}' 
				AND serial = '{$_POST['scsn']}' 
				AND switch = 'Y' 
			";
			$table = $db->getQueryRecord($query);
			$scode_item = isset($table['table']['record'][0]) ? $table['table']['record'][0] : '';
			
			if(empty($scode_item) ) {
				//'不存在'
				$ret['status'] = 3;
			}
			elseif($_POST['scpw'] !==$scode_item['pwd'] ) {
				//'密码错误'
				$ret['status'] = 4;
			}
			elseif($scode_item['verified'] == 'Y') {
				//已激活
				$ret['status'] = 5;
			}
		}
		
		if(empty($ret['status']) )
		{
			//S码激活
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_item` 
			SET `verified`='Y', userid='{$_SESSION['auth_id']}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND serial = '{$_POST['scsn']}'
			";
			$db->query($query);
			
			//實體S碼序號
			$scode_promote['spid'] = $scode_item['spid'];
			$scode_promote['behav'] = 'e';
			$scode_promote['promote_amount'] = 1;
			$scode_promote['num'] = $scode_item['amount'];
				
			//插入S碼收取記錄
			$scodeModel->insert_scode($scode_promote, $_SESSION['auth_id'], $_POST['scsn']);
			
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
	//限定S码激活
	public function act_osc()
	{
		global $db, $config;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$oscModel = new OscodeModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		if (empty($_POST['scsn']) || empty($_POST['scpw']) ) {
			//S码序号、S码密码不能空白
			$ret['status'] = 2;
		}
		else
		{
			$query = "SELECT * 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` 
			WHERE 
				prefixid = '{$config['default_prefix_id']}' 
				AND serial = '{$_POST['scsn']}' 
				AND switch = 'Y' 
			";
			$table = $db->getQueryRecord($query);
			$scode_item = isset($table['table']['record'][0]) ? $table['table']['record'][0] : '';
			
			if(empty($scode_item) ) {
				//'不存在'
				$ret['status'] = 3;
			}
			elseif($_POST['scpw'] !==$scode_item['pwd'] ) {
				//'密码错误'
				$ret['status'] = 4;
			}
			elseif($scode_item['verified'] == 'Y') {
				//已激活
				$ret['status'] = 5;
			}
		}
		
		if(empty($ret['status']) )
		{
			//限定S码激活
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` 
			SET `verified`='Y', userid='{$_SESSION['auth_id']}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND serial = '{$_POST['scsn']}'
			";
			$db->query($query);
			
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
			SET
			   `scode_sum`=`scode_sum` + 1,
			   `amount`=`amount` + 1
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND spid = '{$scode_item['spid']}' 
			";
			$db->query($query);
			
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}
	
}
?>