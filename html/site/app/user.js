// JavaScript Document

/* login.php */
function login() {
	var pageid = $.mobile.activePage.attr('id');
	var olphone = $.trim($('#'+pageid+' #lphone').val());
	var olpw = $('#'+pageid+' #lpw').val();
	
	if(olphone=="" && olpw=="") {
		alert('账号及密码不能空白');
	}
	else 
	{
		$.post("/site/ajax/user.php?t="+getNowTime(), 
		{type:'login', username:olphone, passwd:olpw}, 
		function(str_json) 
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status < 1) {
					alert('登录失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('杀友欢迎您!');
						// location.reload();
						location.href='/site/';
					}
					else if (json.status==201){ 
						// location.reload();
						location.href='/site/';
					} 
					else if(json.status==2){ alert('请填写账号'); }
					else if(json.status==3){ alert('账号不存在'); }
					else if(json.status==4){ alert('请填写密码'); }
					else if(json.status==5){ alert('密码不正确'); }
				}
			}
		});
	}
}


function profile_confirm() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var oname = $('#'+pageid+' #name').val();
	var ozip = $('#'+pageid+' #zip').val();
	var oaddress = $('#'+pageid+' #address').val();
	
	if(oname == "" && ozip == "" && oaddress == "") {
		alert('收件人姓名、邮编及地址不能空白');
	}
	else 
	{
		$.post("/site/ajax/user.php?t="+getNowTime(), 
		{type:'profile', name:oname, zip:ozip, address:oaddress}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('收件人信息注册失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('确认收件人信息');
						location.href="/site/member/";
					} 
					else if(json.status==2){ alert('请填写收件人姓名'); } 
					else if(json.status==3){ alert('请填写收件人邮编'); }
					else if(json.status==4){ alert('请填写收件人地址'); }
				}
			}
		});
	}
}

function profile() {
	var pageid = $.mobile.activePage.attr('id');
	var oname = $('#'+pageid+' #name').val();
	var ozip = $('#'+pageid+' #zip').val();
	var oaddress = $('#'+pageid+' #address').val();
	
	if(oname == "" && ozip == "" && oaddress == "") {
		alert('收件人姓名、邮编及地址不能空白');
	}
	else 
	{
		$.mobile.changePage("/site/user/profile_confirm/", {transition:"slide"} );
		
		$(this).everyTime('1s','profile',
		function()
		{
			var pageid = $.mobile.activePage.attr('id');
			if ($('#'+pageid+' #liname').length > 0) {
			
				$('#'+pageid+' #liname').html('');
				$('#'+pageid+' #lizip').html('');
				$('#'+pageid+' #liaddress').html('');
				$('#'+pageid+' #name').val('');
				$('#'+pageid+' #zip').val('');
				$('#'+pageid+' #address').val('');
				
				$('#'+pageid+' #liname').html($('#'+pageid+' #liname').html()+oname);
				$('#'+pageid+' #lizip').html($('#'+pageid+' #lizip').html()+ozip);
				$('#'+pageid+' #liaddress').html($('#'+pageid+' #liaddress').html()+oaddress);
				$('#'+pageid+' #name').val(oname);
				$('#'+pageid+' #zip').val(ozip);
				$('#'+pageid+' #address').val(oaddress);
				
				$(this).stopTime('profile');
			}
		});
	}
}

/* cdnforget.php */
function forget() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}

	var pageid = $.mobile.activePage.attr('id');
	var onickname = $('#'+pageid+' #nickname').val();
	var ophone = $('#'+pageid+' #phone').val();
	
	if(onickname == "" && ophone == "")	{
		alert('昵称及手机号码不能空白');
	}
	else 
	{
		$.post("/site/ajax/user.php?t="+getNowTime(), 
		{type:'forget', nickname:onickname, phone:ophone}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('新密码发送失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('新密码已发送至您的手机，请注意查收');
						location.href="/site/";
					} 
					else if(json.status==2){ alert('请填写昵称'); } 
					else if(json.status==3){ alert('请填写手机号码'); } 
					else if(json.status==4){ alert('昵称或手机不正确'); }
				}
			}
		});
	}
}

/* forgetnickname.php */
function forgetnickname() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}

	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#'+pageid+' #phone').val();
	
	if($.trim(ophone) == "")	{
		alert('手机号码不能空白');
	}
	else 
	{
		$.post("/site/ajax/user.php?t="+getNowTime(), 
		{type:'forgetnickname', phone:ophone}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('新密码发送失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('昵称查询发送成功');
						location.href="/site/";
					} 
					else if(json.status==3){ alert('请填写手机号码'); } 
					else if(json.status==4){ alert('手机不正确'); }
				}
			}
		});
	}
}

/* cdnforget.php */
function forgetexchange() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#'+pageid+' #phone').val();
	var opw = $('#'+pageid+' #passwd').val();
	
	if($.trim(ophone) == "" && opw == "")	{
		alert('手机号码及密码不能空白');
	}
	else 
	{
		$.post("/site/ajax/user.php?t="+getNowTime(), 
		{type:'forgetexchange', phone:ophone, pw:opw}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('新兑换密码发送失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('新兑换密码发送成功');
						location.href="/site/";
					}
					else if(json.status==2){ alert('请填写账号'); }
					else if(json.status==3){ alert('请填写密码'); }
					else if(json.status==4){ alert('账号或密码不正确'); }
				}
			}
		});
	}
}

/* profile.php */
function user_pw() {
	var pageid = $.mobile.activePage.attr('id');
	var ooldpw = $('#'+pageid+' #oldpw').val();
	var onewpw = $('#'+pageid+' #newpw').val();
	var onewrepw = $('#'+pageid+' #newrepw').val();
	
	if(ooldpw == "" && onewpw == "" && onewrepw == "") {
		alert('旧密码、新密码及密码确认不能空白');
	}
	else 
	{
		if(onewpw !== onewrepw) {
			alert('新密码与密码確認不相同，请再确认');
		}
		else 
		{
			$.post("/site/ajax/user.php?t="+getNowTime(), 
			{type:'pw', oldpasswd:ooldpw, passwd:onewpw, repasswd:onewrepw}, 
			function(str_json) 
			{
				if(str_json) 
				{
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					
					if (json.status==1) {
						alert('杀友请先登入');
						$('#'+pageid+' #'+rightid).panel("open");
					}
					else if (json.status < 1) {
						alert('密码修改失败');
					}
					else {
						var msg = '';
						if (json.status==200){ 
							alert('密码修改成功');
							location.reload();
						}
						else if(json.status==2){ alert('账号不存在'); }
						else if(json.status==3){ alert('旧密码错误'); }
						else if(json.status==4){ alert('新密码格式错误'); }
						else if(json.status==5){ alert('密码确认错误'); }
					}
				}
			});
		}
	}
}

/* profile.php */
function user_expw() {
	var pageid = $.mobile.activePage.attr('id');
	var ooldexpw = $('#'+pageid+' #oldexpw').val();
	var onewexpw = $('#'+pageid+' #newexpw').val();
	var onewexrepw = $('#'+pageid+' #newexrepw').val();
	
	if(ooldexpw == "" && onewexpw == "" && onewexrepw == "") {
		alert('兑换密码：旧密码、新密码及密码确认不能空白');
	}
	else 
	{
		if(onewexpw !== onewexrepw) {
			alert('兑换密码：新密码与密码確認不相同，请再确认');
		}
		else 
		{
			$.post("/site/ajax/user.php?t="+getNowTime(), 
			{type:'expw', oldexpasswd:ooldexpw, expasswd:onewexpw, exrepasswd:onewexrepw}, 
			function(str_json) 
			{
				if(str_json) 
				{
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					
					if (json.status==1) {
						alert('杀友请先登入');
						$('#'+pageid+' #'+rightid).panel("open");
					}
					else if (json.status < 1) {
						alert('兑换密码修改失败');
					}
					else {
						var msg = '';
						if (json.status==200){ 
							alert('兑换密码修改成功');
							location.reload();
						}
						else if(json.status==2){ alert('账号不存在'); }
						else if(json.status==3){ alert('兑换密码旧密码错误'); }
						else if(json.status==4){ alert('兑换密码新密码格式错误,或是密码长度不足6码'); }
						else if(json.status==5){ alert('兑换密码确认错误'); }
					}
				}
			});
		}
	}
}

/* user.php */
function user_qqno() {
         var pageid = $.mobile.activePage.attr('id');
         var qq = $('#'+pageid+' #qqno').val();
		 if(qq=="") {
		    alert('qq账号不能为空白 !!');
			return false;
		 } else {
		    if(confirm('请再确认您输入的qq账号: '+qq+' 是否正确 ??\n一旦绑定QQ账号之后即无法修改。')==false) {
			   return false;
			}
		    $.post("/site/ajax/user.php?t="+getNowTime(),
                   {type:'updateqqno',qqno:qq},
                    function(req_str) {
                        if(req_str) { 
						   var json = $.parseJSON(req_str);
						       console && console.log(json);
							   if (json.status<0) {
									alert(json.retmsg);
									if(json.status==-1) {
									   // redir to 登入页面
									  $('#'+pageid+' #'+rightid).panel("open");
									}
							   } else if(json.status==200) {
							        alert(json.retmsg);
									location.reload();
							   }
						}
					});
		 } 
}

/* profile.php */
function extrainfo() {
	console.log($().length);
	var pageid = $.mobile.activePage.attr('id');
	var uecArr = {};
	
	$('#' + pageid + ' input[name^="uecid"]').each(function() {
		var uecid = $(this).val();
		var uecname = $('#' + pageid + ' input[name="uecname[' + uecid + ']"]').val();
		var sum = 0;
		var total = 0;
		uecArr = new Object();
		
		$('#' + pageid + ' input[name^="uecfield[' + uecid + ']"]').each(function() {
			var fieldname = $(this).attr("id").replace('uecfield_' + uecid + '_', '');
			uecArr[fieldname] = $(this).val();
			
			if ($(this).val() != '') {
				sum++;
			}
			
			total++;
		});
		
		if (uecid == 2) {
			uecArr['field2name'] = $("select[name='city']").val() + "_" + $("select[name='school']").val();
			//sum++;
			//total++;
		}
		
		if((sum > 0) && (sum != total)) {
			alert('请将 ' + uecname + ' 资料，填写完整');
			return false;
		}
		else if ((sum > 0) && (sum == total)){
			$.post("/site/ajax/user.php?t="+getNowTime(), 
			{type:'extrainfo',  uecid:uecid, uecArr:uecArr}, 
			function(str_json) 
			{
				if(str_json) 
				{
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					
					if (json.status==1) {
						alert('杀友请先登入');
						$('#'+pageid+' #'+rightid).panel("open");
					}
					else if (json.status < 1) {
						alert(uecname + ' 资料修改失败');
					}
					else {
						var msg = '';
						if (json.status==200){ 
							alert(uecname + '修改成功');
							location.reload();
						}
						else if(json.status==2){ alert('账号不存在'); }
					}
				}
			});
		}
		
	});
	//alert('卡包修改成功');
}

/* profile.php */
function bonus_noexpw() {
         
		 var pageid = $.mobile.activePage.attr('id');
	     var bonus = $('#'+pageid+' #bonus_noexpw').val();
         if(isNaN(bonus)) {
		    alert('额度必须为数字 !!');
			return false;
		 } 
		 if(bonus<0) {
		    alert('额度必须为正数 !!');
			return false;
		 }
		 $.post("/site/ajax/user.php?t="+getNowTime(),
                   {'type':'bonus_noexpw','bonus_noexpw':bonus},
                    function(req_str) {
                        if(req_str) { 
						   var json = $.parseJSON(req_str);
						       console && console.log(json);
							   if (json.status<0) {
									alert(json.retmsg);
									if(json.status==-1) {
									   // redir to 登入页面
									  $('#'+pageid+' #'+rightid).panel("open"); 
									}
							   } else if(json.status==200) {
							        alert('兑换额度修改成功');
									location.reload();
							   }
						}
					});
}


/* profile.php */
function user_nickname() {
	var pageid = $.mobile.activePage.attr('id');
	var onickname = $('#'+pageid+' #nickname').val();
	
	if(onickname == "") {
		alert('昵称不能空白');
	} else if(onickname.substr(0,1)=="_") {
	    alert('昵称不能以 "_" 开头 !!');
	} else 
	{
		$.post("/site/ajax/user.php?t="+getNowTime(), 
		{type:'nicknamechange', nickname:onickname}, 
		function(str_json) 
		{
			if(str_json) 
			{
				console.log(str_json);
				var json = $.parseJSON(str_json);
								
				if (json.status==0) {
					alert('杀友请先登入 !!');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 0) {
					if(json.status==-1){ alert('昵稱修改失敗'); }
					else if(json.status==-2){ alert('昵称重覆'); }
					else if(json.status==-3){ alert('昵称错误'); }
					else if(json.status==-4){ alert('昵称不能空白'); }
					else if(json.status==-5){ alert('昵称不能含特殊字符'); }
				}
				else if (json.status==1) {
					alert('昵称修改成功 !!');
					location.reload();
				}
			}
		});
	}
}

/* profile.php */
function channel_modify() {
	var pageid = $.mobile.activePage.attr('id');
	var ocountryid = $('#'+pageid+' #countryid').val();
	var oregionid = $('#'+pageid+' #regionid').val();
	var oprovinceid = $('#'+pageid+' #provinceid').val();
	var ochannelid = $('#'+pageid+' #channelid').val();
	
	if(ocountryid == '' || oregionid == '' || oprovinceid == '' || ochannelid == '') {
		alert('国家、区域、省/直辖市及分区不能空白');
	}
	else 
	{
		$.post("/site/ajax/user.php?t="+getNowTime(), 
		{type:'channelmodify', countryid:ocountryid, regionid:oregionid, provinceid:oprovinceid, channelid:ochannelid}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status == 1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('资料修改失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('资料修改成功');
						location.reload();
					}
					else if(json.status == 2) { alert('国家、区域、省/直辖市及分区不能空白'); }
					else if(json.status == 3) { alert('請勿選擇與預設值相同'); }
				}
			}
		});
	}
}