<?php
session_start();

$time_start = microtime(true);

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
//echo "php 開始 $time 秒<br>";

error_log("[ajax/product] POST : ".json_encode($_POST));
if($_POST['pay_type']=='saja_test') {
   $_SESSION['auth_id']=$_POST['userid'];
   $_SESSION['user']['profile']['nickname']=$_POST['username'];
}

if(empty($_SESSION['auth_id']) ) { //'请先登入会员账号'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/ini.php");
	include_once(LIB_DIR ."/wechat.class.php");
	include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");
    require_once("/var/www/lib/elephant.io/autoload.php");	
	//$app = new AppIni; 

	$c = new ProductBid;
	$c->saja();
}


class ProductBid 
{
	public $userid = '';
	public $countryid = 2;//國家ID
	public $currency_unit = 0.01;//貨幣最小單位
	public $last_rank = 11;//預設順位提示（順位排名最後一位）
	public $saja_time_limit = 0;//每次下標時間間隔限制
	public $display_rank_time_limit = 60;//（秒）下標後離結標還有n秒才顯示順位
	public $range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標
	public $tickets = 1;
	public $price;
	public $username='';
	
	//下标
	public function saja()
	{	
		$time_start = microtime(true);
				
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
		$this->username= (empty($_SESSION['user']) ) ? '' : $_SESSION['user']['profile']['nickname'];	
		$ret['status'] = 0;
		$ret['winner'] = '';
		$ret['rank'] = '';
		$ret['value'] = 0;
		
		//讀取商品資料
		$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.productid = '{$_POST['productid']}'
			AND p.switch = 'Y'
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		unset($table['table']['record'][0]['description']);
		$product = $table['table']['record'][0]; 

		if( ($product['offtime'] == 0 && $product['locked'] == 'Y') || ($product['offtime'] > 0 && $product['now'] > $product['offtime']) ) {
			//回傳: 本商品已结标
			$ret['status'] = 2;
		}
		/*
		// 此功能需要優化
		elseif($this->chk_time_limit() ) {
			//回傳: 下标时间间隔过短，请稍后再下标
			$ret['status'] = 3;
		}
		*/
		//下標資格
		$chk = $this->chk_saja_rule($product);
		if ($chk['err']) {
			$ret['status'] = $chk['err'];
			$ret['value'] = $chk['value'];
		}
		
		if($_POST['type']=='single') { //單次下標
			$chk = $this->chk_single($product);
			if($chk['err']) {
				$ret['status'] = $chk['err'];
			}
			
		}
		elseif($_POST['type']=='range') { //連續下標
			$chk = $this->chk_range($product);
			if($chk['err']) {
				$ret['status'] = $chk['err'];
			}
		}
		
		if(empty($ret['status']) || $ret['status']==0 ) {
			
			//產生下標歷史記錄
			$mk = $this->mk_history($product);
			
			//回傳: 下标完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			$ret['winner'] = $mk['winner'];
			$ret['rank'] = $mk['rank'];
			if($_POST['type']=='range') {
			    $ret['total_num'] = $mk['total_num'];
				$ret['num_uniprice'] = $mk['num_uniprice'];
				$ret['lowest_uniprice'] = $mk['lowest_uniprice'];
			}
		}
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "下標 $time 秒<br>";
		error_log("[ajax/product] saja : ".json_encode($ret));
		echo json_encode($ret);
	}
	
	//下標資格
	public function chk_saja_rule($product)
	{
		global $db, $config;
		
		// 20150804 Add By Thomas 房子下標資格判斷
		if($product['productid']==2854) {
		    /*
		    $chk=" SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` 
			        WHERE switch='Y' AND productid='{$product['productid']}' AND userid='".$_SESSION['auth_id']."' ";
		    if (empty($table['table']['record']) || empty($table['table']['record'][0]['userid'])) {
			    $r['err'] = 18; 
				return $r;
			}
			*/
			// 20151019 改為只要活動期間內有充值500元以上就算
			$chk=" SELECT count(*) as deposit_cnt FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` 
			        WHERE prefixid = '{$config['default_prefix_id']}'  
					  AND switch='Y' 
					  AND userid={$_SESSION['auth_id']} 
					  AND amount>=500.00 
					  AND insertt between '2015-08-05 00:00:00' AND '2016-01-01 00:00:00' ";
			$table = $db->getQueryRecord($chk);	
		    if($table['table']['record'][0]['deposit_cnt']<1) {
			    $r['err'] = 18; 
				return $r;
			}
		}
		//
		
		$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt` 
		where 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND productid = '{$product['productid']}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		$r['err'] = '';
		$r['value'] = 0;
		$count = $this->pay_get_product_count($this->userid);
		
		if ($table['table']['record'][0]['srid'] == 'never_win_saja') {		//新手(未曾得標)
			if ($count > 0) {
				$r['err'] = 15;
			}
		}
		else if ($table['table']['record'][0]['srid'] == 'le_win_saja') {	//得標次數n次以內
			if ($count > $table['table']['record'][0]['value']) {
				$r['err'] = 16;
				$r['value'] = $table['table']['record'][0]['value'];
			}
		}
		else if ($table['table']['record'][0]['srid'] == 'ge_win_saja') {	//得標次數n次以上
			if ($count < $table['table']['record'][0]['value']) {
				$r['err'] = 17;
				$r['value'] = $table['table']['record'][0]['value'];
			}
		}
		
		return $r;
	}
	
	//得標次數
	public function pay_get_product_count($userid)
	{
		global $db, $config;
		
		$query = "SELECT count(*) as count 
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`  
		where 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND userid = '{$userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		return $table['table']['record'][0]['count'];
	}
	
	// 連續下標
	public function chk_range($product)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		//殺價記錄數量
		$this->tickets = ((float)$_POST['price_stop'] - (float)$_POST['price_start']) / $this->currency_unit + 1;
		//error_log("殺價記錄數量：".$this->tickets);
		//殺價起始價格
		$this->price = $_POST['price_start'];
		
		$r['err'] = '';
		
		if (empty($_POST['price_start']) || empty($_POST['price_stop']) ) {
		    //'金额不可空白'
			$r['err'] = 4;
		}
		elseif (!is_numeric($_POST['price_start']) || !is_numeric($_POST['price_stop']) ) {
		    //'金额格式错误'
			$r['err'] = 5;
		}
		elseif ((float)$_POST['price_start'] < (float)$product['price_limit']) {
		    //'金额低于底价'
			$r['err'] = 6;
		}
		elseif ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] < $this->range_saja_time_limit) ) {
		    //'超过可连续下标时间'
			$r['err'] = 7;
		}
		elseif ($this->tickets > (int)$product['usereach_limit']) {
		    //'超过可连续下标次数限制'
			$r['err'] = 8;
		}
		elseif ((float)$_POST['price_start'] > (float)$_POST['price_stop']) {
		    //'起始价格不可超过结束价格'
			$r['err'] = 9;
		}
		elseif ((float)$_POST['price_stop'] > (float)$product['retail_price']) {
		    //'金额超过商品市价'
			$r['err'] = 10;
		}
		elseif ((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid']) ) {
		    //'超过可下标次数限制'
			$r['err'] = 11;
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "連續下標 $time 秒<br>";
		
		return $r;
	}
	
	// 單次下標
	public function chk_single($product)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		//殺價記錄數量
		$this->tickets = 1;
		
		//殺價起始價格
		$this->price = $_POST['price'];
		
		$r['err'] = '';
		
		if (empty($this->price)) {
		    //'金额不可空白'
			$r['err'] = 4;
		}
		elseif (!is_numeric($this->price) ) {
		    //'金额格式错误'
			$r['err'] = 5;
		}
		elseif ((float)$this->price < (float)$product['price_limit']) {
		    //'金额低于底价'
			$r['err'] = 6;
		}
		elseif ((float)$this->price > (float)$product['retail_price']) {
		    //'金额超过商品市价'
			$r['err'] = 10;
		}
		elseif ((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid']) ) {
		    //'超过可下标次数限制'
			$r['err'] = 11;
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "單次下標 $time 秒<br>";
		
		return $r;
	}
	
	//檢查使用者下標次數限制
	public function chk_user_limit($user_limit, $pid)
	{
		$time_start = microtime(true);
		
		global $db, $config;
	
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND productid = '{$pid}'
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		$tickets_avaiable = (int)$user_limit - (int)$table['table']['record'][0]['count'];
		/*
		error_log("[ajax/product/chk_user_limit] count : ".$query."==>".(int)$table['table']['record'][0]['count']);
		error_log("[ajax/product/chk_user_limit] user_limit : ".(int)$user_limit);
		error_log("[ajax/product/chk_user_limit] this->tickets : ".$this->tickets);
		error_log("[ajax/product/chk_user_limit] tickets_avaiable : ".$tickets_avaiable);
		*/
		if((int)$this->tickets > $tickets_avaiable) {
			//'超过可下标次数限制'
			$time_end = microtime(true);
			$time = number_format(($time_end - $time_start),10);
			//echo "檢查使用者下標次數限制 $time 秒<br>";
		    error_log("[ajax/product/chk_user_limit] 1檢查使用者下標次數限制 ${time} 秒");
			return 1;
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "檢查使用者下標次數限制 $time 秒<br>";
		error_log("[ajax/product/chk_user_limit] 0檢查使用者下標次數限制 ${time} 秒");
		return 0;
	}
	
	//檢查最後下標時間
	public function chk_time_limit()
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$query = "
		SELECT unix_timestamp(MAX(insertt)) as insertt, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			if( (int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $this->saja_time_limit) {
				//'下标时间间隔过短，请稍后再下标!'
				$time_end = microtime(true);
				$time = number_format(($time_end - $time_start),10);
				//echo "檢查最後下標時間 $time 秒<br>";
				
				return 1;
			}
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "檢查最後下標時間 $time 秒<br>";
		
		return 0;
	}
	
	public function getBidWinner($productid) {
	       global $db, $config;
		   // 得標者
		   $ret='';
		   $query="SELECT h.userid, h.productid, h.nickname, h.price ".
		          ",(select name from `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}product` where productid=h.productid) as prodname ".
				  ",(select uid from `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}sso` where ssoid=(SELECT ssoid FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sso_rt` WHERE userid=h.userid and switch='Y' ) and switch='Y' and name='weixin') as openid ".
				  " FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h ".
			      " WHERE (h.productid, h.price) = (SELECT productid, min_price FROM  `{$config['db'][4]["dbname"]}`.`v_min_unique_price` WHERE productid='{$productid}') ";
					
		   // error_log("[ajax/product] getBidWinner : ".$query);
		   $table = $db->getQueryRecord($query);
		   if(!empty($table['table']['record'][0])) {
			  $ret=array();
			  $ret['userid'] = $table['table']['record'][0]['userid']; 
              $ret['nickname'] = $table['table']['record'][0]['nickname'];
              $ret['productid'] = $table['table']['record'][0]['productid']; 
              $ret['prodname'] = $table['table']['record'][0]['prodname'];  
              $ret['price'] = $table['table']['record'][0]['price'];			  
              $ret['openid'] = $table['table']['record'][0]['openid'];  	 			  
           }
           return $ret;		   
	}
	
	//產生下標歷史記錄
	public function mk_history($product)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = '';
		$values = array();
		
		if($_POST['use_sgift'] == 'Y') 
		{
			/****************************
			* 使用禮券
			****************************/
			
			//檢查禮券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}sgift`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}' 
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
			ORDER BY seq
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$sgift = 0;
			}
			else {
				$sgift = count($table['table']['record']); //礼券數量
			}

			if($this->tickets > $sgift) {
				//'礼券不足'
				$r['err'] = 15;
			}
			else 
			{
				//使用礼券
				for($i = 0; $i < round($this->tickets); $i++) 
				{
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}sgift` 
					SET 
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND sgiftid = '{$table['table']['record'][$i]['sgiftid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);
					
					//殺價下標歷史記錄（對應下標記錄與下標禮券id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'0',
						'{$table['table']['record'][$i]['sgiftid']}',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						NOW()
					)";
				}
				
				if(empty($values)) {
					//'下标失败'
					$r['err'] = -1;
				}
			}
		}
		elseif($_POST['pay_type'] == 'oscode') 
		{
			/****************************
			* 使用殺價券 : 為針對某特定商品免費的「下標次數」， 1張殺價券可以下標乙次
			****************************/
			
			//檢查殺價券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`  
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}' 
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
			    ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			}
			else {
				$osq = count($table['table']['record']); 
			}
			error_log("[ajax/product/mk_history]: needed ==>".round($this->tickets));
			error_log("[ajax/product/mk_history]:".$query."==>".$osq);

			if(round($this->tickets) > $osq) {
				//'殺價券不足'
				$r['err'] = 14;
			}
			else 
			{
				//使用殺價券
				for($i = 0; $i < round($this->tickets); $i++) 
				{
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode` 
					SET 
						`behav`='user_saja',
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);
					
					//殺價下標歷史記錄（對應下標記錄與 oscodeid）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'0',
						'0',
						'{$table['table']['record'][$i]['oscodeid']}',
						'".($this->price + $i * $this->currency_unit)."',
						NOW()
					)";
				}
				
				if(empty($values)) {
					//'下标失败'
					$r['err'] = -1;
				}
			}
		}
		elseif($_POST['pay_type'] == 'scode') //elseif($_POST['use_scode'] == 'Y') 
		{
			/****************************
			* 使用S碼 : 為免費的「下標次數」，1組S碼可以下標乙次
			* 有效時間為30天(發送當天開始算30天)，逾期失效不補回。
			****************************/
			
			$scodeModel = new ScodeModel;
			$scode = $scodeModel->get_scode($this->userid);
			
			//檢查S碼數量
			if($this->tickets > $scode) {
				//'S碼不足'
				$r['err'] = 12;
			}
			else 
			{
				//確認S碼
				$query = "select scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode` 
					where 
					`prefixid`='{$config['default_prefix_id']}' 
					and `userid` = '{$this->userid}' 
					and `behav` != 'a' 
					and `closed` = 'N'
					and `remainder` > 0
					and `switch` = 'Y'
					order by insertt asc
					limit 0, 1
				";
				$table = $db->getQueryRecord($query);
				
				//使用S碼
				$query = "update `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode` 
				SET
					`remainder`=`remainder` - {$this->tickets},
					`modifyt`=NOW()
				where
					`prefixid`='{$config['default_prefix_id']}' 
					and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
					and `userid` = '{$this->userid}' 
					and `behav` != 'a' 
					and `closed` = 'N'
					and `remainder` > 0
					and `switch` = 'Y'
				";
				$res = $db->query($query);
				
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`behav`='a',
					`productid`='{$product['productid']}',
					`amount`='". - $this->tickets ."',
					`insertt`=NOW()
				";
				$res = $db->query($query);
				$scodeid = $db->_con->insert_id;

				for($i = 0 ; $i < round($this->tickets); $i++) 
				{
					//殺價下標歷史記錄（對應下標記錄與使用S碼id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'{$scodeid}',
						'0',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						NOW()
					)";
				}
				
				if(empty($values)) {
					//'下标失败'
					$r['err'] = -1;
				}
			}
		}
		elseif($_POST['pay_type'] == 'saja_test')
		{
		    /****************************
			* 壓力測試 : 使用殺價券下標, 測試商品productid=2923
			****************************/
			
			//檢查殺價券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`  
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '2923' 
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
			    ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			
			if(empty($table['table']['record'])) {
				$osq = 0;
			}
			else {
				$osq = count($table['table']['record']); 
			}
			$osq=9999;
						
			// error_log("[ajax/product/saja_test]: needed ==>".round($this->tickets));
			// error_log("[ajax/product/saja_test]:".$query."==>".$osq);

			if(round($this->tickets) > $osq) {
				//'殺價券不足'
				$r['err'] = 14;
			}
			else 
			{
				//使用殺價券
				for($i = 0; $i < round($this->tickets); $i++) 
				{
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode` 
					SET behav ='saja_test',
						used = 'Y',
						used_time = NOW()
					WHERE
						prefixid = '{$config['default_prefix_id']}'
						AND oscodeid = 0
						AND switch = 'Y'
					";
					$res = $db->query($query);
					// error_log("[ajax/product/saja_test]:".$query."==>".$res);
					//殺價下標歷史記錄（對應下標記錄與 oscodeid）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'2923',
						'{$_POST['userid']}',
						'{$_POST['username']}',
						0,
						0,
						0,
						0,
						'".($this->price + $i * $this->currency_unit)."',
						NOW()
					)";
				}
				
				if(empty($values)) {
					//'下标失败'
					$r['err'] = -1;
				}
			}
		}
		else
		{
			/****************************
			* 使用殺幣
			****************************/
			$scode_fee = 0;
			
			//檢查殺幣餘額
			$query = "SELECT SUM(amount) as amount
			FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
			WHERE userid = '{$this->userid}'
				AND prefixid = '{$config['default_prefix_id']}' 
				AND countryid = '{$this->countryid}'
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);

			$saja_fee = (float)$this->tickets * (float)$product['saja_fee'];//下標手續費
			$saja_fee_amount = $saja_fee; //下標費用 = 手續費
			
			if($saja_fee_amount > (float)$table['table']['record'][0]['amount']) {
				//'杀价币不足'
				$r['err'] = 13;
			}
			else
			{
				//使用殺幣
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`countryid`='{$this->countryid}',
					`behav`='user_saja',
					`amount`='". - $saja_fee_amount ."',
					`insertt`=NOW()
				";
				
				$res = $db->query($query);
				$spointid = $db->_con->insert_id;
				
				for($i = 0 ; $i < round($this->tickets); $i++) 
				{
					//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'{$spointid}',
						'0',
						'0',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						NOW()
					)";
					
				}
				
				if(empty($values)) {
					//'下标失败'
					$r['err'] = -1;
				}
			}
		}
		
		
		if(empty($r['err']) && !empty($values) )
		{
			// 取得目前winner
			$arr_ori_winner=array();
			$arr_ori_winner=$this->getBidWinner($product['productid']);
			if(!empty($arr_ori_winner)) {
			   error_log("[ajax/product/mk_history]: ori Winner of ".$arr_ori_winner['prodname']." is :".$arr_ori_winner['nickname']);
			} else {
			   error_log("[ajax/product/mk_history]: ori Winner is Empty !");
			}
			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
			(
				prefixid,
				productid,
				userid,
				nickname,
				spointid,
				scodeid,
				sgiftid,
				oscodeid,
				price,
				insertt
			)
			VALUES
			".implode(',', $values)."
			";
			
			$res = $db->query($query);
			// error_log("[ajax/product]:".$query."-->".$res);
			$r['rank'] = $last_rank;
			
			// 結標一分鐘以前, 單次下標將提示順位
			if($_POST['type'] == 'single') {
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit) ) {
					// 檢查重複
					$query= "SELECT count(userid) as cnt 
							   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
							  WHERE productid='{$product['productid']}' 
								AND price={$_POST['price']} 
								AND switch='Y' ";
					
					$table = $db->getQueryRecord($query);
					$dup_cnt=$table['table']['record'][0]['cnt'];
					// error_log("[ajax/product]Dup chk: ".$query." --> ".$dup_cnt);	
					if($dup_cnt>1) {
						// 重複出價
						$r['rank']=-1;
					} else if($dup_cnt<=1) {
						// 無重複出價
						$query1 = "SELECT count(*) as cnt FROM  `{$config['db'][4]["dbname"]}`.`v_unique_price` 
								   WHERE productid='{$product['productid']}' AND price<{$_POST['price']} ";
						error_log("[ajax/product]rank chk cnt : ".$query1);
						$table1 = $db->getQueryRecord($query1);	
						$r['rank'] =$table1['table']['record'][0]['cnt'];
						error_log("[ajax/product]rank : ".$r['rank']);
					}
				}
			}  
			
			// 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出价個數, 及該次下標的唯一最低價
			if($_POST['type'] == 'range') {
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit) ) {
				    $r['total_num']=$this->tickets;
					error_log("range tickets : ".$this->tickets);
					// 顯示提示
					// 取得該區間內唯一出價的個數
					if($this->tickets>1) {
						$query="SELECT * FROM saja_shop.saja_history sh JOIN saja_shop.v_unique_price up 
								   ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y' 
								WHERE sh.switch='Y'  
								  AND sh.productid='{$product['productid']}' 
								  AND sh.price between {$_POST['price_start']} AND {$_POST['price_stop']}
								  AND sh.userid='{$this->userid}' ORDER BY sh.price asc ";
								  
						error_log("[ajax/product]range chk: ".$query);
						$table = $db->getQueryRecord($query);
						error_log("[ajax/product]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);	
						
						if(empty($table['table']['record'])) {
						   // 無唯一出价
						   $r['num_uniprice'] = 0;	
						   $r['lowest_uniprice'] = 0;					   
						} else{
						   // 有唯一出价
						   $r['num_uniprice'] = count($table['table']['record']);	
						   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
						}
					} else {
					    // 不提示
						$r['total_num']=-1;
					}
			    } else {
				    // 不提示
				    $r['total_num']=-1;
				}
			}
			
			$arr_new_winner=array();
			$arr_new_winner=$this->getBidWinner($product['productid']);
			$r['winner']=empty($arr_new_winner['nickname'])?'无':$arr_new_winner['nickname'];
			$redis = new Redis();
			$redis->connect('127.0.0.1');
			$redis->set('PROD:'.$product['productid'].':BIDDER',$r['winner']); 	
						
			$elephant = new ElephantIO\Client(new ElephantIO\Engine\SocketIO\Version1X('http://127.0.0.1:3333'));
			$json='{"FROM":"ajax|product","PRODID":"'.$product['productid'].'","WINNER":"'.$r['winner'].'"}';
			error_log("[ajax/product] socket: ".$json);
			$elephant->initialize();
			$elephant->emit('broadcast', json_decode($json,true));
			$elephant->close();
			
			// 結標一分鐘以前, 得標者改變會發微信通知(openid)
			if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit) ) {
				/*
				$arr_new_winner=array();
				$arr_new_winner=$this->getBidWinner($product['productid']);
				$r['winner']=empty($arr_new_winner['nickname'])?'无':$arr_new_winner['nickname'];
				$redis = new Redis();
				$redis->connect('127.0.0.1');
				$redis->set('PROD:'.$product['productid'].':BIDDER',$r['winner']); 				
				*/
				if($_POST['pay_type']!='saja_test') {					
					if($arr_ori_winner['userid']!=$arr_new_winner['userid']) {
					   $options['appid'] = APP_ID;
					   $options['appsecret'] = APP_SECRET;
					   $wx=new WeixinChat($options);
					
					   if(!empty($arr_new_winner['openid'])) {		
						  $msg='【目前中标通知】 恭喜你！刚刚干掉之前<a href="http://www.shajiawang.com/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$arr_new_winner['productid'].'">《'.$arr_new_winner['prodname'].'》</a>的中标者。请持续关注，以确保领先地位。';
						  $wx->sendCustomMessage($arr_new_winner['openid'],$msg,'text');
						  $wx->sendCustomMessage('oNmoZtxOu0pKnlUjZjO0beFCSfEY',$msg,'text');
					   }
					   if(!empty($arr_ori_winner['openid'])) {			   
						  if(!empty($arr_new_winner['userid'])) {
							 $msg='【紧急通知】原本你中标的<a href="http://www.shajiawang.com/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$arr_ori_winner['productid'].'">《'.$arr_ori_winner['prodname'].'》</a>在刚刚被某个小伙伴给抢走了！赶紧去看看是哪个家伙！再把它抢回来吧！';
						  } else if (empty($arr_new_winner['userid'])) {
							 $msg='【紧急通知】原本你中标的<a href="http://www.shajiawang.com/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$arr_ori_winner['productid'].'">《'.$arr_ori_winner['prodname'].'》</a>目前无人中标 ! 赶紧再去把它抢回来吧！';
						  }
						  $wx->sendCustomMessage($arr_ori_winner['openid'],$msg,'text');
						  $wx->sendCustomMessage('oNmoZtxOu0pKnlUjZjO0beFCSfEY',$msg,'text');
					   }
					} 
				}
			}
			// 得標者
			/*
			$query2="SELECT userid, productid, nickname, price FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
					  WHERE (productid, price) = (SELECT productid, min_price FROM  `{$config['db'][4]["dbname"]}`.`v_min_unique_price` WHERE productid='{$product['productid']}') ";
					
			error_log("[ajax/product]winner chk: ".$query2);
			$table2 = $db->getQueryRecord($query2);
			$r['winner'] = $table2['table']['record'][0]['nickname'];  
			*/
			
						
			
			
			//單次下標而且離結標還有$this->display_rank_time_limit秒以上，回傳目前順位
			//if($_POST['type'] == 'single')
			/*
			$rank = $this->last_rank-1;//最大顯示排名為第10位
            $query = "SELECT h.userid, h.nickname, h.price
      			FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h 
			  WHERE h.prefixid = '{$config['default_prefix_id']}'
				AND h.productid = '{$product['productid']}'
				AND h.price < '{$_POST['price']}'
				AND h.price is not null
				AND h.switch = 'Y'
			GROUP BY h.price
			HAVING COUNT(*) = 1
			ORDER BY h.price asc
			LIMIT {$rank}
			";
			$table = $db->getQueryRecord($query);

			if (!empty($table['table']['record'])) 
			{
				//回傳中標者
				$r['winner'] = $table['table']['record'][0]['nickname']; 

				//下標而且離結標還有十分鐘以上，回傳目前順位
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit) ) 
				{
					foreach ($table['table']['record'] as $rk => $rv)
					{
						if ($rv['price'] == $_POST['price']) {
							$rank = $rk + 1;
							break;
						}
					}
					$r['rank'] = $rank;
				}
			}
			else 
			{
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit) ) 
				{
					$r['rank'] = $rank;
				}
			}
			*/
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "產生下標歷史記錄 $time 秒<br>";
		error_log("[ajax/mk_history] r : ".json_encode($r));
		return $r;
	}
	
}

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
//echo "<br>php 結束 $time 秒";
?>
