// JavaScript Document
// 閃殺连续出价
function kuso_bid_n(pid) {
	// console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	//var ouscode = $('#'+pageid+' #uscode-2').prop("checked");
	var obida = $('#'+pageid+' #bidb').val();
	var obidb = $('#'+pageid+' #bide').val();
	var oscode_num=$('#'+pageid+' #oscode_num').text();
	var oscode_needed = 0;
	
	if (obida == "" || obidb == "") {
		alert('出价起讫区间不能空白!');
	} 
	else if(isNaN(Number(obida))==true || isNaN(Number(obidb))==true) {
	    alert('出价必须为数字!');   
	}
	else if (Number(obida) < 0.01 || Number(obidb) < 0.01) {
		alert('连续出价不能小于0.01!');
	}
	else if (!obida.match("^[0-9]+(.[0-9]{1,2})?$") || !obidb.match("^[0-9]+(.[0-9]{1,2})?$")) {
		alert('价格最小单位为小数点两位!');
	}
	else if(Number(obida)==Number(obidb)) {
	    alert('出价起讫区间不能相同!');
	} 
	else if(Number(obida)>Number(obidb)) {
	    alert('价格请由低填到高!');
	}	
	else
	{
		oscode_needed = Math.round((Number(obidb)-Number(obida))/0.01)+1;
		if(oscode_needed>parseInt(oscode_num)) {
		   alert('现有杀价券数量不足!');
		   return ;
		}
		//o_uscode = (ouscode==true)?'Y':'N'; //alert(o_uscode); 
		$.mobile.loading('show', {text: '下标处理中', textVisible: true, theme: 'b', html: ""});
		
		//use_scode: o_uscode,
		$.post("/site/ajax/product.php?t="+getNowTime(), 
		{
			type: "range",
			productid: pid,
			price_start: obida,
			price_stop: obidb,
			pay_type: 'oscode',
			use_sgift: 'N'
		}, 
		function(str_json) 
		{
			if(str_json)
			{
				$.mobile.loading('hide');
				
				//var json = $.parseJSON(str_json);
				var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
				console && console.log(json);
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('连续出价失败');
				}
				else {
					var msg = '';
					var msg2 = '';
					var total_num=json.total_num;     // 下標次數
					var num_uprice=json.num_uniprice;  // 唯一出价的個數
					var lowest_uprice=json.lowest_uniprice; // 最低的唯一出价
					if (json.status==200){ 
						if (json.winner) { 
							//msg2 = ' , 本次中标者 ' + json.winner; 
							$('#'+pageid+' #bidded').html(json.winner);
						}
						if(total_num>=2) {
							if(num_uprice>0) {
							   msg='本次有 '+num_uprice+' 个出价是唯一价, 其中最低价格是 '+lowest_uprice+ ' 元 !\n';
							} else {
							   msg='本次下标的出价都与其他人重复了 !\n';
							}
						}
						alert('连续出价 '+obida+'至'+obidb+'元 成功 !\n'+ msg);
						$('#'+pageid+' #bidb').val("");
						$('#'+pageid+' #bide').val("");
						$('#'+pageid+' #oscode_num').text(oscode_num-oscode_needed);
						// location.reload();
					} 
					else if(json.status==2){ alert('亲，这档商品已结标啰'); } 
					else if(json.status==3){ alert('下标间隔过短，请稍后再下'); } 
					else if(json.status==4){ alert('金额记得填!'); } 
					else if(json.status==5){ alert('金额格式错误'); } 
					else if(json.status==6){ alert('金额低于底价'); } 
					else if(json.status==7){ alert('已经超过连续下标时间啰!'); }
					else if(json.status==8){ alert('超过下标次数限制!'); }
					else if(json.status==9){ alert('价格请由低填到高!'); }
					else if(json.status==10){ alert('出价金额超过市价，太贵啰!'); }
					else if(json.status==11){ alert('超过下标次数限制!!'); }
					else if(json.status==12){ alert('杀价券不足!'); }
					else if(json.status==13){ alert('杀价币不足，请充值!'); }
					else if(json.status==14){ alert('杀价券不足!'); }
					else if(json.status==15){ alert('限新手(未中标过者)'); }
					else if(json.status==16){ alert('限新手(限中标次数 ' + json.value +' 次以下)'); }
					else if(json.status==17){ alert('限老手(限中标次数 ' + json.value +' 次以上)'); }
					else if(json.status==18){ 
					     alert('您还没有《万人杀房》的下标资格, 将跳转至活动页激活资格 !'); 
					     location.href='/evt/2854.php';                     
					}
				}
			}
		});
	}
}

//下標连续出价
function bid_n(pid) {
	// console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	//var ouscode = $('#'+pageid+' #uscode-2').prop("checked");
	var obida = $('#'+pageid+' #bidb').val();
	var obidb = $('#'+pageid+' #bide').val();
	var paytype=$('#'+pageid+' #radio-saja').val();
	if(paytype=='') 
	   paytype='spoint';
	
	if (obida == "" || obidb == "") {
		alert('连续出价不能空白');
	} 
	else if(isNaN(Number(obida))==true || isNaN(Number(obidb))==true) {
	    alert('出价必须为数字');   
	}
	else if (Number(obida) < 0.01 || Number(obidb) < 0.01) {
		alert('连续出价不能小于0.01');
	}
	else if (!obida.match("^[0-9]+(.[0-9]{1,2})?$") || !obidb.match("^[0-9]+(.[0-9]{1,2})?$")) {
		alert('价格最小单位为小数点两位');
	}
	else if(Number(obida)==Number(obidb)) {
	    alert('出价起讫区间不能相同');
	}
	else
	{
		//o_uscode = (ouscode==true)?'Y':'N'; //alert(o_uscode); 
		$.mobile.loading('show', {text: '下标处理中', textVisible: true, theme: 'b', html: ""});
		
		//use_scode: o_uscode,
		$.post("/site/ajax/product.php?t="+getNowTime(), 
		{
			type: "range",
			productid: pid,
			price_start: obida,
			price_stop: obidb,
			pay_type: paytype,
			use_sgift: 'N'
		}, 
		function(str_json) 
		{
			if(str_json)
			{
				$.mobile.loading('hide');
				
				//var json = $.parseJSON(str_json);
				var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
				console && console.log(json);
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('连续出价失败');
				}
				else {
					var msg = '';
					var msg2 = '';
					var total_num=json.total_num;     // 下標次數
					var num_uprice=json.num_uniprice;  // 唯一出价的個數
					var lowest_uprice=json.lowest_uniprice; // 最低的唯一出价
					if (json.status==200){ 
						if (json.winner) { 
							//msg2 = ' , 本次中标者 ' + json.winner; 
							$('#'+pageid+' #bidded').html(json.winner);
						}
						if(total_num>=2) {
							if(num_uprice>0) {
							   msg='本次有 '+num_uprice+' 个出价是唯一价, 其中最低价格是 '+lowest_uprice+ ' 元 !\n';
							} else {
							   msg='本次下标的出价都与其他人重复了 !\n';
							}
						}
						alert('连续出价 '+obida+'至'+obidb+'元 成功 !\n'+ msg);
						$('#'+pageid+' #bidb').val("");
						$('#'+pageid+' #bide').val("");
						// location.reload();
					} 
					else if(json.status==2){ alert('亲，这档商品已结标啰'); } 
					else if(json.status==3){ alert('下标间隔过短，请稍后再下'); } 
					else if(json.status==4){ alert('金额记得填!'); } 
					else if(json.status==5){ alert('金额格式错误'); } 
					else if(json.status==6){ alert('金额低于底价'); } 
					else if(json.status==7){ alert('已经超过连续下标时间啰!'); }
					else if(json.status==8){ alert('超过下标次数限制!'); }
					else if(json.status==9){ alert('价格请由低填到高!'); }
					else if(json.status==10){ alert('出价金额超过市价，太贵啰!'); }
					else if(json.status==11){ alert('超过下标次数限制!'); }
					else if(json.status==12){ alert('杀价券不足!'); }
					else if(json.status==13){ alert('杀价币不足，请充值!'); }
					else if(json.status==15){ alert('限新手(未中标过者)'); }
					else if(json.status==16){ alert('限新手(限中标次数 ' + json.value +' 次以下)'); }
					else if(json.status==17){ alert('限老手(限中标次数 ' + json.value +' 次以上)'); }
					else if(json.status==18){ 
					     alert('您还没有《万人杀房》的下标资格, 将跳转至活动页激活资格 !'); 
					     location.href='/evt/2854.php';                     
					}
				}
			}
		});
	}
}

//閃殺单次出价
function kuso_bid_1(pid) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	// var opaytype = $('#'+pageid+' #pay_type :radio:checked').val();
	var opaytype = 'oscode';
	var obid = $('#'+pageid+' #bid').val();
	
	if(obid == "") {
		alert('单次出价不能空白');
	}
	else if(isNaN(Number(obid))==true) {
	    alert('出价必须为数字');  
	}
	else if (Number(obid) < 0.01) {
		alert('单次出价不能小于0.01');
	}
	else if (!obid.match("^[0-9]+(.[0-9]{1,2})?$")) {
		alert('价格最小单位为小数点两位');
	}
	else
	{
		$.mobile.loading('show', {text: '下标处理中', textVisible: true, theme: 'b', html: ""});
		
		//use_scode: o_uscode,
		$.post("/site/ajax/product.php?t="+getNowTime(), 
		{
			type: "single",
			productid: pid,
			price: obid,
			pay_type: opaytype,
			use_sgift: 'N'
		}, 
		function(str_json) 
		{
			if(str_json)
			{
				$.mobile.loading('hide');
				
				// var json = $.parseJSON(str_json);
				var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
				console && console.log(json);
				
				if (json.status==1) {
					alert('杀友请先登入');
					// $('#'+pageid+' #'+rightid).panel("open");
					$('#'+rightid).panel("open");
				} else if (json.status < 1) {
					alert('单次出价失败');
				} else {
					var msg = '';
					var msg2 = '';
					if (json.status=='200'){ 
					    // alert(json.rank);
						if (json.rank) {
						    if (json.rank==-1) {
							    msg='很抱歉！这个价钱与其他人重复了！\n请您试试其他价钱或许会有好运喔！';
							} else if(json.rank==0) {
							    msg = '恭喜！您是目前的中标者！\n记得在时间到之前要多布局！';
							} else if (json.rank>=1 && json.rank<=10) {
								// msg = ' , 本次下标顺位为第 ' + json.rank + ' 名'; 
								msg = '恭喜！这个价钱是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的还有 '+(json.rank)+' 位。';
							} else if (json.rank>10) {
								// msg = ' , 本次下标顺位超过第 12 名或与他人重覆下标';
								msg = '恭喜！这个价钱是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超过 10 位。';
							}
						}
						if (json.winner) { 
							//msg2 = ' , 本次中标者 ' + json.winner; 
							// $('#'+pageid+' #bidded').html(json.winner);
							$('#bidded').html(json.winner);
						}
						alert('单次出价 '+obid+'元 成功\n'+ msg); 
						$('#'+pageid+' #bid').val("");
						var o_num=$('#'+pageid+' #oscode_num').text();
						$('#'+pageid+' #oscode_num').text(parseInt(o_num)-1);
						// location.reload();
					} 
					else if(json.status==2){ alert('亲，这档商品已结标啰'); } 
					else if(json.status==3){ alert('下标间隔过短，请稍后再下'); } 
					else if(json.status==4){ alert('金额记得填!'); } 
					else if(json.status==5){ alert('金额格式错误'); } 
					else if(json.status==6){ alert('金额低于底价'); } 
					else if(json.status==7){ alert('已经超过连续下标时间啰!'); }
					else if(json.status==8){ alert('超过下标次数限制'); }
					else if(json.status==9){ alert('价格请由低填到高!'); }
					else if(json.status==10){ alert('金额超过商品市价'); }
					else if(json.status==11){ alert('超过可下标次数限制!'); }
					else if(json.status==12){ alert('杀价券(Ｓ码)不足!'); }
					else if(json.status==13){ alert('杀价币不足，请充值!'); }
					else if(json.status==14){ alert('杀价券不足!'); }
					else if(json.status==15){ alert('限新手(未中标过者)'); }
					else if(json.status==16){ alert('限新手(限中标次数 ' + json.value +' 次以下)'); }
					else if(json.status==17){ alert('限老手(限中标次数 ' + json.value +' 次以上)'); }
					else if(json.status==18){ 
					     alert('您还没有《万人杀房》的下标资格, 将跳转至活动页激活资格 !'); 
					     location.href='/evt/2854.php';                     
					}
				}
			}
		});
	}
}

//单次出价
function bid_1(pid) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var opaytype = $('#'+pageid+' #pay_type :radio:checked').val();
	var obid = $('#'+pageid+' #bid').val();
	
	if(obid == "") {
		alert('单次出价不能空白');
	}
	else if(isNaN(Number(obid))==true) {
	    alert('出价必须为数字');  
	}
	else if (Number(obid) < 0.01) {
		alert('单次出价不能小于0.01');
	}
	else if (!obid.match("^[0-9]+(.[0-9]{1,2})?$")) {
		alert('价格最小单位为小数点两位');
	}
	else
	{
		$.mobile.loading('show', {text: '下标处理中', textVisible: true, theme: 'b', html: ""});
		
		//use_scode: o_uscode,
		$.post("/site/ajax/product.php?t="+getNowTime(), 
		{
			type: "single",
			productid: pid,
			price: obid,
			pay_type: opaytype,
			use_sgift: 'N'
		}, 
		function(str_json) 
		{
			if(str_json)
			{
				$.mobile.loading('hide');
				
				// var json = $.parseJSON(str_json);
				var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
				console && console.log(json);
				
				if (json.status==1) {
					alert('杀友请先登入');
					// $('#'+pageid+' #'+rightid).panel("open");
					$('#'+rightid).panel("open");
				} else if (json.status < 1) {
					alert('单次出价失败');
				} else {
					var msg = '';
					var msg2 = '';
					if (json.status=='200'){ 
					    // alert(json.rank);
						if (json.rank) {
						    if (json.rank==-1) {
							    msg='很抱歉！这个价钱与其他人重复了！\n请您试试其他价钱或许会有好运喔！';
							} else if(json.rank==0) {
							    msg = '恭喜！您是目前的中标者！\n记得在时间到之前要多布局！';
							} else if (json.rank>=1 && json.rank<=10) {
								// msg = ' , 本次下标顺位为第 ' + json.rank + ' 名'; 
								msg = '恭喜！这个价钱是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的还有 '+(json.rank)+' 位。';
							} else if (json.rank>10) {
								// msg = ' , 本次下标顺位超过第 12 名或与他人重覆下标';
								msg = '恭喜！这个价钱是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超过 10 位。';
							}
						}
						if (json.winner) { 
							//msg2 = ' , 本次中标者 ' + json.winner; 
							// $('#'+pageid+' #bidded').html(json.winner);
							$('#bidded').html(json.winner);
						}
						alert('单次出价 '+obid+'元 成功\n'+ msg); 
						$('#'+pageid+' #bid').val("");
						// location.reload();
					} 
					else if(json.status==2){ alert('亲，这档商品已结标啰'); } 
					else if(json.status==3){ alert('下标间隔过短，请稍后再下'); } 
					else if(json.status==4){ alert('金额记得填!'); } 
					else if(json.status==5){ alert('金额格式错误'); } 
					else if(json.status==6){ alert('金额低于底价'); } 
					else if(json.status==7){ alert('已经超过连续下标时间啰!'); }
					else if(json.status==8){ alert('超过下标次数限制'); }
					else if(json.status==9){ alert('价格请由低填到高!'); }
					else if(json.status==10){ alert('金额超过商品市价'); }
					else if(json.status==11){ alert('超过可下标次数限制!'); }
					else if(json.status==12){ alert('杀价券(Ｓ码)不足!'); }
					else if(json.status==13){ alert('杀价币不足，请充值!'); }
					else if(json.status==14){ alert('杀价券不足!'); }
					else if(json.status==15){ alert('限新手(未中标过者)'); }
					else if(json.status==16){ alert('限新手(限中标次数 ' + json.value +' 次以下)'); }
					else if(json.status==17){ alert('限老手(限中标次数 ' + json.value +' 次以上)'); }
					else if(json.status==18){ 
					     alert('您还没有《万人杀房》的下标资格, 将跳转至活动页激活资格 !'); 
					     location.href='/evt/2854.php';                     
					}
				}
			}
		});
	}
}
