<?php
session_start();

if(empty($_SESSION['auth_id']) ) { //'请先登入会员账号'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/ini.php");
	//$app = new AppIni; 

	$c = new Bid;
	$c->home();
}


class Bid
{
	public $userid = '';
	
	//下标结账
	public function home()
	{
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
			
		$ret['status'] = 0;
	
		// Check Variable Start
		
		if(empty($_GET['pgpid']) ) {
			$ret['status'] = 0;
		}
		elseif(empty($_POST['name']) ) {
			//('收件人姓名错误!!');
			$ret['status'] = 2;
		}
		elseif(empty($_POST['zip']) ) {
			//('收件人邮编错误!!');
			$ret['status'] = 3;
		}
		elseif(empty($_POST['address']) ) {
			//('收件人地址错误!!');
			$ret['status'] = 4;
		}
		else
		{
			$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now`, pgp.pgpid, pgp.price, pgp.complete 
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp 
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
				pgp.prefixid = p.prefixid
				AND pgp.productid = p.productid
				AND p.switch = 'Y' 
			WHERE 
				pgp.`prefixid` = '{$config['default_prefix_id']}' 
				AND pgp.pgpid  = '{$_GET["pgpid"]}'
				AND pgp.userid = '{$this->userid}'
				AND pgp.switch = 'Y'
			" ;
			$table = $db->getQueryRecord($query); 
			//if (empty($table['table']['record'])) { $this->jsPrintMsg('商品不存在!', $location_url); }

			$product = $table['table']['record'][0];
			$product['price'] = round($product['price'] * $config['sjb_rate'], 2);
			$product['real_process_fee'] = round($product['retail_price'] * $product['process_fee']/100 * $config['sjb_rate'], 2);
			$product['total'] = $product['real_process_fee'] + $product['price'];
			
			$query = "select sum(amount) as total_amount
			from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
			where
				userid = '{$this->userid}'
				and switch = 'Y'
			GROUP BY userid
			";
			$table = $db->getQueryRecord($query); 
			
			if ($table['table']['record'][0]['total_amount'] < $product['total']) {
				//餘額不足
				$ret['status'] = 6;
			}
			
			if($product['complete']=='Y') {
				//'已结账'
				$ret['status'] = 5;
			}
		}
		
		if(empty($ret['status']) ) {
			//產生訂單記錄
			$mk = $this->mk_order($product);
				
			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
		}
		
		echo json_encode($ret);
	}
	
	//產生訂單記錄
	public function mk_order($product)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$r['err'] = '';
		
		if(empty($r['err']) )
		{
			//新增訂單
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`status`='0',
				`pgpid`='{$_GET["pgpid"]}',
				`type`='saja',
				`num`='1',
				`point_price`='{$product['price']}',
				`process_fee`='{$product['real_process_fee']}',
				`total_fee`='{$product['total']}',
				`confirm`='Y',
				`insertt`=NOW()
			";
			$res = $db->query($query); 
			$orderid = $db->_con->insert_id;
			
			//新增訂單收件人
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` 
			SET 
				`userid`='{$this->userid}',
				`orderid`='{$orderid}',
				`name`='{$_POST['name']}',
				`phone`='{$_SESSION['user']['profile']['phone']}',
				`zip`='{$_POST['zip']}',
				`address`='{$_POST['address']}',
				`gender`='{$_SESSION['user']['profile']['gender']}',
				`prefixid`='{$config['default_prefix_id']}',
				`insertt`=now()
			";
			$db->query($query);
			
			//更新 pay_get_product
			$query = "UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` 
			SET `complete`='Y', `modifyt`=now()
			WHERE `prefixid`='{$config['default_prefix_id']}' 
				AND `pgpid`='{$_GET["pgpid"]}' 
			";
			$db->query($query);
			
			//扣除處理費
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`countryid` = '{$config['country']}', 
				`behav` = 'process_fee', 
				`amount` = '-{$product['total']}', 
				`seq` = '0', 
				`switch` = 'Y', 
				`insertt` = now()
			";
			$db->query($query); 
			
			if((int)$product['bid_oscode'] > 0) { 
				//中標取得殺價券
				//殺價券贈送數量定義於saja_scode_promote
				//saja_product.bid_oscode 紀錄串到saja_scode_promote的Pk
				$this->oscode_by_bid($orderid, $this->userid, $product['bid_oscode'], $product['productid']);  
			}
			elseif((int)$product['bid_scode'] > 0) { 
				//中標取得S碼
				// S碼贈送數量定義於saja_scode_promote_rt, 
				// saja_product.bid_scode中紀錄串到saja_scode_promote_rt的pk
				$this->scode_by_bid($orderid, $this->userid, $product['bid_scode']);  
			}
			elseif((float)$product['bid_spoint'] > 0) { 
				//中標取得殺價幣
				//殺幣的數量直接定義於saja_product.bid_spoint
				$this->spoint_by_bid($orderid, $this->userid, $product['bid_spoint']); 
			}
		}
		
		return $r;
	}
	
	
	/*
	* S碼、限定S碼 、殺幣  的訂單紀錄 改成 "已發送" (saja_order.status=3)
	*/
	private function set_order_status($orderid)
	{
		global $db, $config;
		
		$query ="UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` SET 
			`status` ='3',
			`modifyt`=now() 
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND `orderid`='{$orderid}'
		" ;
		$db->query($query); 
	}
	
	//中標取得 殺價券(一般發送活動編號)
	/*
	    活動資料紀錄於 saja_scode_promote (贈送數量:onum)
		限定S碼明細記錄於 saja_oscode
		在saja_oscode中增加 onum 筆資料 => 一筆saja_oscode表示使用一次的scode
	*/
	public function oscode_by_bid($orderid, $uid, $spid, $productid) 
	{
		global $db, $config;
		
		//送殺價券活動
		$query = "SELECT s.*
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` s
		WHERE 
			s.prefixid = '{$config['default_prefix_id']}' 
			AND s.spid = '{$spid}'
			AND s.behav = 'h'
			AND s.switch = 'Y'
		";
		$rs = $db->getQueryRecord($query);
		
		if(!empty($rs['table']['record'][0]) ) 
		{
			$sp_productid = $rs['table']['record'][0]['productid']; 
			
			//殺價券發送組數
			$onum = (empty($rs['table']['record'][0]['onum']) ) ? 1 : (int)$rs['table']['record'][0]['onum'];
			
			for($i=1; $i<=$onum; $i++)
			{
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
				SET
				   `prefixid`='{$config['default_prefix_id']}',
				   `userid`='{$uid}',
				   `spid`='{$spid}',
				   `productid`='{$sp_productid}',
				   `amount`='1',
				   `insertt`=NOW()
				";
				$db->query($query); 
				//var_dump($query); 
				//$oscodeid = $db->_con->insert_id;
			}
			
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` SET
			   `scode_sum`=`scode_sum` + {$onum},
			   `amount`=`amount` + {$onum} 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND spid = '{$spid}' 
				AND switch = 'Y'
			";
			$db->query($query);

			//訂單紀錄 改成 "已發送"
			$this->set_order_status($orderid);
			
			return true;
		}
		
		return false;
	}
	
	//中標取得 S碼(一般發送活動編號)
	/*
	    活動資料紀錄於 saja_scode_promote & saja_scode_promote_rt (贈送數量: saja_scode_promote_rt.num )
		S碼明細記錄於 saja_scode
		在saja_scode中增加1筆資料, 以amount欄紀錄所贈送的數量
		在saja_scode_history中記錄該次贈送資料(活動代號, 受贈者userid, 該活動被使用1次promote_amount)
		scode_promote記錄該活動被使用的累積次數以及累積S碼贈送數量	
	*/
	public function scode_by_bid($orderid, $uid, $spid) 
	{
		global $db, $config;
		
		//取得 S碼活動資訊
		$query = "SELECT s.name, sp.name spname, sp.num num
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` s
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` sp ON 
			sp.prefixid = s.prefixid
			AND sp.spid = s.spid
			AND sp.switch = 'Y'
		WHERE 
			s.prefixid = '{$config['default_prefix_id']}' 
			AND s.spid = '{$spid}'
			AND s.behav = 'd'
			AND unix_timestamp( s.offtime ) >0 
			AND unix_timestamp() >= unix_timestamp( s.ontime ) 
			AND unix_timestamp() <= unix_timestamp( s.offtime ) 
			AND s.switch = 'Y'
			AND sp.spid IS NOT NULL
		";
		$rs = $db->getQueryRecord($query);
		
		if(!empty($rs['table']['record'][0]) ) 
		{
			$info = $rs['table']['record'][0];
			$batch = time();
						
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$uid}',
			   `spid`='{$spid}',
			   `behav`='d',
			   `amount`='{$info['num']}',
			   `remainder`='{$info['num']}',
			   `insertt`=NOW()
			";
			$db->query($query);
			$scodeid = $db->_con->insert_id;
			
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history`
			SET `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$uid}',
			   `scodeid`='{$scodeid}',
			   `spid`='{$spid}',
			   `promote_amount`='1',
			   `num`='{$info['num']}',
			   `memo`='{$info['name']}',
			   `batch`='{$batch}',
			   `insertt`=NOW()
			";
			$db->query($query);
			
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
			SET
			   `scode_sum`=`scode_sum` + '{$info['num']}',
			   `amount`=`amount` + 1 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND spid = '{$spid}' 
				AND switch = 'Y'
			";
			$db->query($query);
			
			//訂單紀錄 改成 "已發送"
			$this->set_order_status($orderid);
			
			return true;
		}
		
		return false;
	}
	
	//中標取得 殺價幣
	/*
	   新增一筆殺幣紀錄(saja_deposit), behav='bid_by_saja'
	   新增一筆儲值紀錄(saja_deposit), behav='bid_deposit'
	   新增一筆儲值history紀錄(saja_deposit_history), 
	        driid	= '0',
			data    = '中标发送',
			status  = 'bid'
	*/
	public function spoint_by_bid($orderid, $userid, $spoint) 
	{
		global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint`
		SET
		   `prefixid`='{$config['default_prefix_id']}',
		   `userid`='{$userid}', 
		   `countryid`='{$config['country']}',
		   `behav`='bid_by_saja', 
		   `amount`='{$spoint}', 
		   `insertt`=NOW()
		";
		$db->query($query);
		$spointid = $db->_con->insert_id;
		error_log("[ajax/bid/spoint_by_bid] spointid : ".$spointid);
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
		SET
			 `prefixid`='{$config['default_prefix_id']}',
			 `userid`='{$userid}', 
			 `countryid`='{$config['country']}', 
			 `behav`='bid_deposit', 
			 `currency`='RMB',
			 `amount`='{$spoint}', 
			 `insertt`=NOW()
		";
		$db->query($query);
		$depositid = $db->_con->insert_id;
		error_log("[ajax/bid/spoint_by_bid] depositit : ".$depositid);
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history`
		SET prefixid = '{$config['default_prefix_id']}',
			userid = '{$userid}',
			driid = '0',
			data = '中标发送',
			status = 'bid',
			spointid = '{$spointid}',
			depositid = '{$depositid}',
			insertt=NOW()
		";
		error_log("[ajax/bid/spoint_by_bid] : ".$query);
		$res = $db->query($query);
		error_log("[ajax/bid/spoint_by_bid] : res->".$res);
		
		//訂單紀錄 改成 "已發送"
		$this->set_order_status($orderid);
		
		return true;
	}
	
}
?>