// JavaScript Document

//限定S码激活
function act_oscode() {
	var pageid = $.mobile.activePage.attr('id');
	var oscsn = $.trim($('#'+pageid+' #scsn').val()).toUpperCase();
	var oscpw = $('#'+pageid+' #scpw').val();
	
	if(oscsn == "" && oscpw == "" ) {
		alert('序号、密码不能空白');
	}
	else 
	{
		$.post("/site/ajax/scode.php?t="+getNowTime(), 
		{type:'act_osc', scsn:oscsn, scpw:oscpw}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('序号激活失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('序号激活成功');
						location.reload();
					}
					else if(json.status==2){ alert('序号、密码不能空白'); }
					else if(json.status==3){ alert('序号不存在'); }
					else if(json.status==4){ alert('序号密码错误'); }
					else if(json.status==5){ alert('序号已激活'); }
				}
			}
		});
	}
}

//S码激活
function act_scode() {
	var pageid = $.mobile.activePage.attr('id');
	var oscsn = $('#'+pageid+' #scsn').val();
	var oscpw = $('#'+pageid+' #scpw').val();
	
	if(oscsn == "" && oscpw == "" ) {
		alert('S码序号、S码密码不能空白');
	}
	else 
	{
		$.post("/site/ajax/scode.php?t="+getNowTime(), 
		{type:'act', scsn:oscsn, scpw:oscpw}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status==1) {
					alert('杀友请先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status < 1) {
					alert('S码激活失败');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('S码激活成功');
						location.reload();
					}
					else if(json.status==2){ alert('S码序号、S码密码不能空白'); }
					else if(json.status==3){ alert('S码序号不存在'); }
					else if(json.status==4){ alert('S码密码错误'); }
					else if(json.status==5){ alert('S码已激活'); }
				}
			}
		});
	}
}
