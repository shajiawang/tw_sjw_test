<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/enterprise.php");
//$app = new AppIni; 

$c = new Enterprise;

if($_POST['type'] == 'enterprise_login') {
	$c->home();
}


class Enterprise 
{
	public $str;
	
	public function home()
	{
		global $db, $config, $enterprisemodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$enterprisemodel = new EnterpriseModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		if(!empty($_SESSION['sajamanagement']['enterprise']['enterpriseid'])) {
			//$logged = $usermodel->check($_SESSION['auth_id'], $_SESSION['auth_secret']);
			
			//'已登入'
			if($logged) { 
				$ret['status'] = 201; 
			}
		}
		
		if (empty($_POST['loginname']) ) {
			//'请填写账号'
			$ret['status'] = 2;
		}
		elseif (empty($_POST['passwd']) ) {
			//'请填写密码'
			$ret['status'] = 4;
		}
		
		if(empty($ret['status']) ) {
			$chk = $this->chk_login();
			
			//回傳: 
			$ret['status'] = (empty($chk['err']) ) ? 200 : $chk['err'];
		}
		
		echo json_encode($ret);
	}
	
	// 查驗账号密码
	public function chk_login()
	{
		global $db, $config, $enterprisemodel;
		
		$r['err'] = '';
		
		$db2 = new mysql($config["db2"]);
		$db2->connect();
		
		$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` e
		           JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` ep
				     ON e.enterpriseid=ep.enterpriseid and e.switch='Y'
		          WHERE e.prefixid = '{$config['default_prefix_id']}' 
			        AND e.loginname = '{$_POST['loginname']}' 
			        AND e.switch = 'Y' 
		          LIMIT 1
		";
		error_log("[ajax/enterprise.chk_login]".$query);
		$table = $db2->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			//'账号不存在'
			$r['err'] = 3;
		}
		else 
		{
			$_SESSION['sajamanagement']['enterprise'] = '';
			
			$enterprise = $table['table']['record'][0];
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);
			
			if($enterprise['passwd'] === $passwd) 
			{
				$_SESSION['sajamanagement']['enterprise'] = $enterprise;
				setcookie("enterpriseid", $enterprise['enterpriseid'], time() + 43200, "/", COOKIE_DOMAIN);
				$r['err'] = '';
			}
			else {
				//'密码不正确'
				$r['err'] = 5;
			}
		}
		
		return $r;
	}
	
}
?>
