<?php
$meta = _v('meta');

$og_title = (empty($meta['title']) ? "殺價王" : $meta['title']);
$og_type = "website";
$og_image = (empty($meta['image']) ? BASE_URL.APP_DIR."/static/img/share-img.png" : $meta['image']);
$og_url = (empty($meta['url']) ? BASE_URL.$_SERVER['REQUEST_URI'] : $meta['url']);
//$og_url= "http://bit.ly/2WOpQZ6";
$og_description = (empty($meta['description']) ? "全球首創殺價式拍賣，沒得標 金額全返 讓你無所顧忌" : $meta['description']);
$og_site_name = "殺價王";
?>
<meta charset="utf-8">
<!--
    og:title  網頁標題
    og:type  網站類型
    og:url  網址
    og:image  縮圖網址
    og:description  網頁敘述
    og:site_name  網站名稱
-->
<meta name="application-name" content="<?php echo $og_site_name; ?>" />
<meta property="og:title" content="<?php echo $og_title; ?>" />
<meta property="og:type" content="<?php echo $og_type; ?>" />
<meta property="og:image" content="<?php echo $og_image; ?>" />
<meta property="og:url" content="<?php echo $og_url; ?>" />
<meta property="og:description" content="<?php echo $og_description; ?>" />
<meta property="og:site_name" content="<?php echo $og_site_name; ?>" />

<meta name="mobileoptimized" content="0" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="format-detection" content="telephone=no" />
<meta name="screen-orientation" content="portrait" />
<meta name="browsermode" content="application" />
<meta property="wb:webmaster" content="1a4d5f9e7b901534" />
<meta property="qc:admins" content="2405036366630121171676375731457" />

<!-- SEO: -->
<?php if(!empty($meta['description'])) { ?>
   <meta name="description" content="<?php echo $meta['description']; ?>" /> 
<?php } else { ?>
   <meta name="description" content="<?php echo APP_DESCRIPTION; ?>" /> 
<?php } ?>
<meta name="keywords" content="<?php echo APP_KEYWORDS; ?>" /> 
<!-- /SEO -->

<!--Web，Android，Microsoft和iOS（iPhone和iPad）應用程式圖標-->
<link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-16x16.png">
<link rel="manifest" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">
