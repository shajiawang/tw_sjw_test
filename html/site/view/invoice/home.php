<?php 
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$invoice = _v('invoice_data');
$APP = _v('APP');
error_log("[v/invoice] data : ".json_encode($invoice));
?>

<style>
    #invoiceBox {
        width: 19.167rem;
        margin: 1rem auto 0;
        padding: 3.362rem 2.353rem;
        box-sizing: border-box;
        background: #FFF;
        box-shadow: 0 0 2px 2px #d3d3d3;
    }
    #invoiceBox2 {
        width: 19.167rem;
        margin: 1rem auto 0;
        padding: 3.362rem 2.353rem;
        box-sizing: border-box;
        background: #FFF;
        box-shadow: 0 0 2px 2px #d3d3d3;
    }
    .titleBox {
        font-size: 2rem;
        text-align: center;
    }
        .invoiceLogo {
            font-size: 1.6rem;
            letter-spacing: -.1rem;
            font-weight: 600;
            margin: .5rem auto;
        }
    
    .infoBox {
        font-size: 1rem;
        line-height: 1.6rem;
        margin: .3rem 0 0;
    }
            .infoDateBox .infoDate {
                margin-right: 1rem;
            }
            .infoShopping .randomNum {
                margin-right: 3rem;
            }
            .infoShopping .priceBox {
                flex: 1;
            }
            .qrcodeBox .codeNum {
                flex: 1;
                margin-right: 1.5rem;
            }
            .qrcodeBox .shopDetails {
                flex: 1;
            }
    .print-btn {
        width: 60%;
        max-width: 500px;
        margin: 3rem auto 0;
        padding: 1.5rem;
        text-align: center;
        box-sizing: border-box;
        background: #f9a823;
        color: #FFF;
        font-size: 1.4rem;
        border-radius: 50vh;
        display:block;
        cursor: pointer;
    }
    .canvasBox {
        padding-top: 3rem;
        padding-bottom: 3rem;
        text-align: center;
        display: block;
    }
    .canvasBox #myCanvas {
        margin: 0 auto;
        border: 1px solid #333;
    }
</style>
<div class="userEdit">
	<div class="required" style="width: 22.167rem;margin: 1rem auto 0;"> 
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="name">發票種類</div>
                <font style="padding: .2rem;margin: .2rem; 0;height: 1.2rem;"><?php echo $invoice['invoice_kind'];?></font>
            </li>
			<?php if($invoice['buyer_id'] != '00000000'){?>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="name">買方統編</div>
                <font style="padding: .2rem;margin: .2rem; 0;height: 1.2rem;"><?php echo $invoice['buyer_id'];?></font>
            </li>
			<?php } ?>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="zip">索取紙本電子發票</div>
                <font style="padding: .2rem;margin: .2rem; 0;height: 1.2rem;"><?php echo $invoice['print_mark'];?></font>
            </li>
			<?php if($invoice['buyer_id'] != '00000000'){?>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="address">發票抬頭</div>
                <font style="padding: .2rem;margin: .2rem; 0;height: 1.2rem;"><?php echo $invoice['buyer_name'];?></font>
            </li> 
			<?php } ?>
			
            <li class="input_group d-flex align-items-center">
                <div class="label" for="rphone">發票載具</div>
                <font style="padding: .2rem;margin: .2rem; 0;height: 1.2rem;"><?php echo $invoice['carrier_kind'];?></font>
            </li>
			<?php if(!empty($invoice['carrier_code'])) { ?>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="rphone">載具編號</div>
                <font style="padding: .2rem;margin: .2rem; 0;height: 1.2rem;"><?php echo $invoice['carrier_code'];?></font>
            </li>
			<?php } ?>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="rphone">發票狀態</div>
                <font style="padding: .2rem;margin: .2rem; 0;height: 1.2rem;"><?php echo $invoice['invoice_type'];?></font>
            </li>
			
        </ul>
	</div>
</div>
<div id="invoiceBox">
    <div class="titleBox">
        <div class="invoiceLogo"><?php echo $invoice['SellerName']; ?></div>
        <div class="invoiceTitle">電子發票副本</div>
        <div class="invoiceMonth"><?php echo $invoice['year']; ?>年<?php echo $invoice['start_month']; ?>-<?php echo $invoice['end_month']; ?>月</div>
        <div class="invoiceId"><?php echo $invoice['invoiceno']; ?></div>
    </div>
    <div class="infoBox">
        <div class="infoDateBox d-flex">
            <div class="infoDate"><?php echo $invoice['date']; ?></div>
            <div class="infoTime"><?php echo $invoice['time']; ?></div>
        </div>
        <div class="infoShopping d-flex">
            <!--div class="randomNum">隨機碼 <?php echo $invoice['random_code']; ?></div-->
            <div class="priceBox d-flex">
                <div class="label">總計</div>
                <div class="price ml-auto"><?php echo $invoice['amount']; ?></div>
            </div>
        </div>
        <div class="infoSeller">賣方<?php echo $invoice['SellerId']; ?>
				買方<?php if($invoice['buyer_id'] != '00000000'){?><?php echo $invoice['buyer_id']; ?>
		<?php } ?>
		</div>
    </div>
    <div class="invoiceCodeBox">
        <div class="barCode">
            <img id="barcode" class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/barcodegen/genbarcode_invoice.php?text=<?php echo $invoice['tx_code_bar']; ?>" alt="">
        </div>
        <div class="qrcodeBox d-flex">
            <div class="codeNum">
                <img id="codeNum" class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/phpqrcode/?data=<?php echo $invoice['tx_code_qrcl']; ?>" alt="">
            </div>
            <div class="shopDetails">
                <img id="shopDetails" class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/phpqrcode/?data=<?php echo $invoice['tx_code_qrcr']; ?>" alt="">
            </div>
        </div>
    </div>
</div>
<div id="invoiceBox2">
	<div class="infoBox">
		<div>交易明細</div>
		<div class="infoDateBox d-flex">
			<div>品名：</div>
			<div><?php echo $invoice['description']; ?></div>
		</div>
		<div class="infoDateBox d-flex">
			<div>數量：</div>
			<div><?php echo $invoice['quantity']; ?></div>
		</div>
		<div class="infoDateBox d-flex">
			<div>金額：</div>
			<div><?php echo $invoice['sales_amt']; ?></div>
		</div>
		<div style="padding: 0;border: none;border-top: 1px solid #8c8c8c;color: #8c8c8c;text-align: center;"> </div>
		<div class="infoDateBox d-flex">
			<div>課稅別：</div>
			<div>應稅 </div>
		</div>
		<div class="infoDateBox d-flex">
			<div>稅率：</div>
			<div><?php echo $invoice['tax_rate']; ?></div>
		</div>
		<div class="infoDateBox d-flex">
			<div>營業稅：</div>
			<div><?php echo $invoice['tax_amt']; ?></div>
		</div>
		<div style="padding: 0;border: none;border-top: 1px solid #8c8c8c;color: #8c8c8c;text-align: center;"> </div>
		<div class="infoDateBox d-flex">
			<div>總計：</div>
			<div><?php echo $invoice['amount']; ?></div>
		</div>
		<br>
		<div class="infoDateBox d-flex">
			<div>備註：</div>
			<div>　　</div>
		</div>
	</div>
</div>
<div class="required" style="width: 19.167rem;margin:1rem auto 0;color:#FF0000;"> 
<p style="margin: 0.5rem;line-height: 1.3rem;">*圖示樣張為副本，無法兌換領獎。</p>
<?php if ($invoice['is_winprize']=="Y"){ ?>
<p style="margin: 0.5rem;line-height: 1.3rem;">*(註1)歸戶至共通性載具可至超商多楳體服務機KIOSK以手機條碼或自然人憑證列印。</p>
<p style="margin: 0.5rem;line-height: 1.3rem;">*(註2)已於整合服務平台設定中獎獎金自動匯入帳戶功能。</p>
<?php } ?>
</div>
<?php  // Temp disable for webview of APP 
  if($APP!='Y')  {
?>
<?php if($invoice['print_mark']=="否"){?>
<div class="btnBox">
    <a class="print-btn">下載列印檔</a>
</div>
<?php }} ?>
<div class="canvasBox" style="display:none;">
    <canvas id="myCanvas"></canvas>
</div>

<script>
    var invoiceOptions = {
        dom: {
            title: {
                box: '.titleBox',
                logo: '.invoiceLogo',
                title: '.invoiceTitle',
                month: '.invoiceMonth',
                id: '.invoiceId'
            },
            info: {
                box: '.infoBox',
                date: '.infoDate',
                time: '.infoTime',
                random: '.randomNum',
                price: '.priceBox .price',
                seller: '.infoSeller'
            },
            code: {
                barcode: '.barCode',
                codeNum: '.codeNum',
                shopDetails: '.shopDetails'
            }
        },
        draw: {         //文字、FontSize、字型、顏色、x座標、y座標、文字對齊方式
                        /*
                            新細明體    PMingLiU
                            細明體    MingLiU
                            標楷體   DFKai-sb
                            黑體    SimHei
                            宋體    SimSun
                            新宋體    NSimSun
                            楷體    KaiTi
                            仿宋_GB2312    FangSong_GB2312
                            楷體_GB2312    KaiTi_GB2312
                            微軟正黑體    Microsoft JhengHei
                            微軟雅黑體    Microsoft YaHei
                        */
            title: {            
                logo: ['','600 17',' Microsoft JhengHei','#000','0','40','top','center'],
                title: ['','21',' Microsoft Yahei','#000','0','60','top','center'],
                month: ['','20',' Microsoft Yahei','#000','0','85','top','center'],
                id: ['','21',' Microsoft Yahei','#000','0','105','top','center'],
            },
            info: {
                date: ['','12',' Microsoft Yahei','#000','25','132','top','left'],
                time: ['','12',' Microsoft Yahei','#000','100','132','top','left'],
                random: ['','12',' Microsoft Yahei','#000','25','150','top','left'],
                priceTitle: ['','12',' Microsoft Yahei','#000','125','150','top','left'],
                price: ['','12',' Microsoft Yahei','#000','25','150','top','right'],
                seller: ['','12',' Microsoft Yahei','#000','25','168','top','left'],
                
            },
            code: {     //圖片id、x座標、y座標、sizeW、sizeH
                barcode: ['barcode','25','195','165','41'],
                codeNum: ['codeNum','25','230','77','77'],
                shopDetails: ['shopDetails','112','230','77','77'],
            }
        },
        printInfo: {
            ext: 'png',
        }
    }
    
    /* 1inch = 2.54cm, 實際尺寸(inch) = px / dpi */
    var $printW = 5.7,       //(cm)
        $printH = 9,         //(cm)
        $inchToCM = 2.54,    //inch 換算 cm
        $canvasDpi = 96;     //canvas 預設dpi

    var $icanvasW = cmtoinch($printW),
        $icanvasH = cmtoinch($printH);
    
    var canvas,ctx;
    var ext,dataURL;      //儲存副檔名、base64_Url
    
    $(function(){
        whDevice();                     //判斷使用者裝置
        getInvoiceInfo();               //取得發票資訊
        
    })
    $(window).on('load',function(){
        changeDomText();                //更改畫面發票資訊
        creatCanvas();                  //創建canvas
        creatTitle();
    })
    
    //判斷使用者裝置
    var $device;        //android, ios, other
    function whDevice() {
        //判斷使用者裝置是否支援觸控功能，判斷是否為行動版 (有觸控功能的筆電也判斷為行動版)
        function isMobile() {
            try{ document.createEvent("TouchEvent"); return true; }
            catch(e){ return false;}
        }
        
        //判斷使用者裝置為Android或iOS
        function isDevice() {
            var u = navigator.userAgent;
            var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android終端
            var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios終端
            if(isAndroid){
                return 'Android';
            }else if(isiOS){
                return 'iOS';
            }
        }
        
        if(isMobile()){
            var $mobile = isDevice();
            switch($mobile){
                case 'Android':
                    $device = 'android';
                    break;
                case 'iOS':
                    $device = 'ios';
                    break;
            }
        }else{
            $device = 'other';
        }
    }
    if($device == 'android') {
        
    } else if($device == 'ios') {
//        invoiceOptions.draw.title.logo[2] = ' ' + '' 
    } else {
        
    }
    
    //取得發票資訊
    function getInvoiceInfo() {
        invoiceOptions.draw.title.logo[0] = '<?php echo $invoice['SellerName']; ?>';
        invoiceOptions.draw.title.title[0] = '電子發票副本';
        invoiceOptions.draw.title.month[0] = '<?php echo $invoice['year']; ?>年<?php echo $invoice['start_month']; ?>-<?php echo $invoice['end_month']; ?>月';
        invoiceOptions.draw.title.id[0] = '<?php echo $invoice['invoiceno']; ?>';
        
        invoiceOptions.draw.info.date[0] = '<?php echo $invoice['date']; ?>';
        invoiceOptions.draw.info.time[0] = '<?php echo $invoice['time']; ?>';
        invoiceOptions.draw.info.random[0] = '隨機碼 '+'<?php echo $invoice['random_code']; ?>';
        invoiceOptions.draw.info.priceTitle[0] = '總計';
        invoiceOptions.draw.info.price[0] = '<?php echo $invoice['amount']; ?>';
		<?php if($invoice['buyer_id'] != '00000000'){?>
        invoiceOptions.draw.info.seller[0] = '賣方'+'<?php echo $invoice['SellerId']; ?>　買方'+'<?php echo $invoice['buyer_id']; ?>';
        <?php }else{ ?>
		invoiceOptions.draw.info.seller[0] = '賣方'+'<?php echo $invoice['SellerId']; ?>　買方';
		<?php } ?>
        $('#' + invoiceOptions.draw.code.barcode[0]).src = '<?php echo BASE_URL.APP_DIR;?>/barcodegen/genbarcode_invoice.php?text=<?php echo $invoice['tx_code_bar']; ?>';
        $('#' + invoiceOptions.draw.code.codeNum[0]).src = '<?php echo BASE_URL.APP_DIR;?>/phpqrcode/?data=<?php echo $invoice['tx_code_qrcl']; ?>';
        $('#' + invoiceOptions.draw.code.shopDetails[0]).src = '<?php echo BASE_URL.APP_DIR;?>/phpqrcode/?data=<?php echo $invoice['tx_code_qrcr']; ?>';
    }
    
    //更改畫面發票內容
    function changeDomText() {
        $(invoiceOptions.dom.title.logo).text(invoiceOptions.draw.title.logo[0]);
        $(invoiceOptions.dom.title.title).text(invoiceOptions.draw.title.title[0]);
        $(invoiceOptions.dom.title.month).text(invoiceOptions.draw.title.month[0]);
        $(invoiceOptions.dom.title.id).text(invoiceOptions.draw.title.id[0]);
        
        $(invoiceOptions.dom.info.date).text(invoiceOptions.draw.info.date[0]);
        $(invoiceOptions.dom.info.time).text(invoiceOptions.draw.info.time[0]);
        $(invoiceOptions.dom.info.random).text(invoiceOptions.draw.info.random[0]);
        $(invoiceOptions.dom.info.price).text(invoiceOptions.draw.info.price[0]);
        $(invoiceOptions.dom.info.seller).text(invoiceOptions.draw.info.seller[0]);
        
        $(invoiceOptions.dom.code.barcode).find('img').attr('src',$('#' + invoiceOptions.draw.code.barcode[0]).src);
        $(invoiceOptions.dom.code.codeNum).find('img').attr('src',$('#' + invoiceOptions.draw.code.codeNum[0]).src);
        $(invoiceOptions.dom.code.shopDetails).find('img').attr('src',$('#' + invoiceOptions.draw.code.shopDetails[0]).src);
    }
    
    //創建canvas
    function creatCanvas() {
        canvas = document.getElementById('myCanvas');
        canvas.width = $icanvasW;
        canvas.height = $icanvasH;
        ctx = canvas.getContext("2d");
        ctx.fillStyle = "#FFFFFF";
        
        ctx.strokeStyle = "#999999";          
        ctx.lineWidth = "1px";
        
        ctx.fillRect(0,0,$icanvasW,$icanvasH);
        ctx.strokeRect(0,0,$icanvasW - 2,$icanvasH - 2);
    }
    
    //cm 換算 inch, 取小數第3位
    function cmtoinch(num) {
        return (parseInt((num / $inchToCM) * $canvasDpi * 1000) / 1000);
    }
    
    //繪製canvas
    function creatTitle() {
        //logo部分
        $.each(invoiceOptions.draw.title,function(i,item){
            invoiceOptions.draw.title[i][4] = $icanvasW / 2;
            ctx.font = invoiceOptions.draw.title[i][1] +'px' + invoiceOptions.draw.title[i][2];
            ctx.fillStyle = invoiceOptions.draw.title[i][3];
            ctx.textBaseline = invoiceOptions.draw.title[i][6];
            ctx.textAlign = invoiceOptions.draw.title[i][7];
            ctx.fillText(invoiceOptions.draw.title[i][0], invoiceOptions.draw.title[i][4], invoiceOptions.draw.title[i][5]);
        })
        
        //info部分
        $.each(invoiceOptions.draw.info,function(i,item){
            ctx.font = invoiceOptions.draw.info[i][1] +'px' + invoiceOptions.draw.info[i][2];
            ctx.fillStyle = invoiceOptions.draw.info[i][3];
            ctx.textBaseline = invoiceOptions.draw.info[i][6];
            ctx.textAlign = invoiceOptions.draw.info[i][7];
            if(i == 'price'){                   //價格靠右對其
                invoiceOptions.draw.info[i][4] = $icanvasW - invoiceOptions.draw.info[i][4];
            }
            ctx.fillText(invoiceOptions.draw.info[i][0], invoiceOptions.draw.info[i][4], invoiceOptions.draw.info[i][5]);
        })
        
        //code部分
        $.each(invoiceOptions.draw.code,function(i,item){

            var id = invoiceOptions.draw.code[i][0];
            var image = document.getElementById(id);
                ctx.drawImage(image, invoiceOptions.draw.code[i][1], invoiceOptions.draw.code[i][2], invoiceOptions.draw.code[i][3], invoiceOptions.draw.code[i][4]);
        })

        //生成base64
        getBase64Image();
        
    }
    
    function getBase64Image() {
        ext = invoiceOptions.printInfo.ext;
        dataURL = canvas.toDataURL("image/"+ext+",1.0");
        $(".btnBox .print-btn").attr('href',dataURL).attr('download','發票號-'+invoiceOptions.draw.title.id[0]+'.'+ext);
    }
    
</script>
