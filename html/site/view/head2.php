<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");

function is_weixin() {
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
                // error_log("Weixin!");
		return true;
	}
        // error_log("Not Weixin!");
	return false;
}
$header = _v('header');
$style = "";
if ($tpl->page_id == "site_oscode_home" ){
	$style = "background-color:#EFEFEF;"; 
}
?>		
<!DOCTYPE html>
<html prefix="og:http://ogp.me/ns# fb:http://ogp.me/ns/fb#" >
<head>
<?php include_once 'meta.php'; ?>
<title><?php echo $tpl->page_header; ?></title>
<?php include_once 'cssjs.php'; ?>
<style>
.content {
    width:100%;
	height:100%;
    border:0px; 
    margin:0 auto;
    background-color:#FFFFFF;
}
.nav {
    width:100%;
    height:30px;
    margin:0 auto;
    background-color:#00a0e9;
    position:fixed;
    bottom:0;
    text-align:center;
}
.userinfo-avatar2 {
	width: 30px;
	height: 30px;
	margin: 0px;
	border-radius: 50%;
}
.placeholder2 {
	margin: 0px;
	padding: 5px 5px;
	height:30px;
	background-color: #00a0e9;
	text-align: center;
	color: #cfcfcf;
}
</style>
<!–[if IE]>
    <script src="<?PHP echo BASE_URL.APP_DIR; ?>/javascript/html5.js"></script>
<![endif]–>
</head>
<body style="height:100%" ontouchstart>
<div class="weui-pull-to-refresh__layer">
  <div class='weui-pull-to-refresh__arrow'></div>
  <div class='weui-pull-to-refresh__preloader'></div>
  <div class="down">下拉刷新</div>
  <div class="up">释放刷新</div>
  <div class="refresh">正在刷新</div>
</div>
<div data-role="page" id="<?php echo $tpl->page_id.$cdnTime; ?>" style="<?php echo $style; ?>" >
<div class="content" >
