<?php
// 本頁面棄用, 改至  kusosaja/adctrl.php
$product = _v('product');
$status = _v('status');
$meta=_v('meta');
$cdnTime = date("YmdHis");
$qrcode=_v('qrcode');
$qrcode_url=_v('qrcode_url');
$prod_sel_list=_v('prod_sel_list');
$bidded=urldecode(_v('bidded'));
$hostid=_v('hostid');
$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
?>
<?php

    if(empty($product)) {
?>
       <script>
	      alert("目前無閃殺商品 !!");
		  //window.location.href='https://www.saja.com.tw/site/index.php';
		  //exit;
	   </script>
<?php
	}
?>
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/css/colorbox.css" />
<script src="<?PHP echo APP_DIR; ?>/javascript/jquery.colorbox-min.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/md5.js"></script>
<!-- script src="<?PHP echo APP_DIR; ?>/static/js/socket.io-1.3.7.js"></script -->
<script>

    function reloadPage(sel) {
	   var url='<?php echo APP_DIR."/kusosaja/adctrl?productid="; ?>'+sel.value;
	   location.href = url;
    }

	function md5(s) {
      return binl2hex(core_md5(str2binl(s), s.length * chrsz));
    }


</script>
<style>
    .footer-btn {
        background: #F9A823;
        border-radius: 100vh;
        color: #FFF;
        font-size: 1.5rem;
        flex: 1;
        text-align: center;
        margin: 1rem;
        padding: 1rem;
    }
</style>
<style>
.Color-Red {color:red !important;}
</style>
<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/flexbox.css">

		<div class="article">

            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                    <?php if(!empty($product['productid'])) { ?>

					<div id="tagBox" style="width:50%;float: left;">
					<?php
						if(!empty($product['thumbnail2'])) { //$status['path_image']
							$img_src = $product['thumbnail2'];
						} elseif (!empty($product['thumbnail_url'])) {
							$img_src = $product['thumbnail_url'];
						}
					?>

						<img <?php echo $flash_img; ?> src="<?php echo $img_src; ?>"  >
                    </div>
                    <div align="center" style="width:50%;">
						<img width="80%" height="80%" src="<?php  echo BASE_URL ?>/site/phpqrcode/?data=<?php  echo $qrcode_url; ?>" />
                    </div>
					<div style="font-size:24px;" align="center">
                       <?php echo $product['name']; ?>
                    </div>
					<?php if (!empty($product['offtime'])){ ?>
					           <!-- div align="center"><p class="countdown" style="font-size:30px;color:red" data-offtime="<?php echo $product['offtime']; ?>" data-tag=" "> 00时00分00秒</p></div -->
							   <input type="hidden" name="data-tag" id="data-tag" value="  "/>
							   <input type="hidden" name="data-offtime" id="data-offtime" value="<?php echo $product['offtime']; ?>"/>
							   <div align="center"  >
							     <span class="countdown" style="font-size:30px;color:red" id="countdown" >00時00分00秒</span>
							   </div>
					<?php }//endif; ?>

					     <div align="center" id="final_bidded" style="color:blue;font-size:24px;">
                            <span id="bidded"><?php echo $bidded; ?></span> &nbsp;
                         </div>

						 <div align="center" id="final_price" style="color:blue;font-size:24px;">
                           <span id="price"></span>
                         </div>
					<p></p>




					<?php if($product['is_flash']=='Y')  {  ?>
					<div class="d-flex">
					   <a class="footer-btn" href="javascript:;" data-role="button" id="btn_endsaja" name="btn_endsaja" >結標</a>
					   <a class="footer-btn" href="javascript:;" data-role="button" id="btn_reload" name="btn_reload" >頁面更新</a>
					</div>

					<!-- div align="center" >
					   <input type="text" length="16" id="winner" name="winner"/>
					   <a href="#" data-role="button" id="btn_winner" name="btn_winner" >更新中標</a>
					</div -->
					<?php  }   ?>

					<?php  } ?>

					<div class="d-flex">
					   <!--<a class="footer-btn" href="<?php echo BASE_URL.APP_DIR;?>/kusobid/?hostid=<?php echo $hostid;?>" data-role="button" id="btn_bid" name="btn_bid" >最新得標結果</a>-->
					</div>

                </li>
            </ul>

        </div>
		<!-- /article -->

<script>
    $(function(){
        //esAOptions.vClass.vModal.show = false;          //隱藏看影片領殺價券
    })
</script>

<?php if(!empty($product['productid'])) { ?>

		<script type="text/javascript">

        var chkprodid="<?php echo $product['productid']; ?>";
		var now_time;
		var socket;
		var timer;
		function show_history(){
			if($('#countdown').html().indexOf('已结標')>0){
				location.href='/site/bid/detail/?productid='+chkprodid;
			}else{
				alert('競標尚未結束');
			}
		}
		var timer_run = function() {
       				now_time = now_time + 1;
					// var end_time = $('.countdown').attr('data-offtime'); //截標時間
					var end_time =document.getElementById('data-offtime').value;
					// var end_plus = $('.countdown').attr('data-tag'); //標籤
					var end_plus =document.getElementById('data-tag').value; //標籤
					var lost_time = parseInt(end_time) - now_time; //剩餘時間
					if(lost_time >= 0) {
						var o = lost_time; //原始剩餘時間
						// var d = parseInt(o /24 / 60 / 60);
						// if(d<10){d = "0" + d;};
						var h = parseInt((o) / 60 / 60); //37056
						if(h<10){h = "0" + h;};
						var m = parseInt((o - parseInt(h * 60 * 60)) / 60);
						if(m<10){m = "0" + m;};
						var s = parseInt((o - parseInt(h * 60 * 60) - parseInt(m * 60)));
						if(s<10){s = "0" + s;};
						// $('.countdown:eq('+i+')').html(end_plus+d+"天"+h+"时"+m+"分"+s+"秒");
						$('.countdown').html(end_plus+" "+h+"時"+m+"分"+s+"秒");
						// 3秒更新得標者一次
                        if(now_time%3==0) {
                            // console.log('3sec');
                            autoUpdate('');
                        }
                        // 10秒調整一次時間
                        if(now_time%10==0) {
							console.log('10sec');
                            $.get("<?php echo UNIX_TS_URL; ?>", function(data) {
							      now_time = parseInt(data); //系統時間
							});
							// autoUpdate('');
						}
					} else if(lost_time < 0) {
						if(lost_time>=-3) {
						   $('.countdown').html(end_plus+" <b>..結標作業中..</b>");
						   $('#final_bidded').html('<b><span id="bidded"></span></b>');
						} else {
						   if(timer)
                              clearInterval(timer);
						   $.get("/site/kusosaja/closeProduct/?productid="+chkprodid, function(str){
							   // alert(str);
                               var json = JSON && JSON.parse(str) || $.parseJSON(str);
							   if(json.productid==chkprodid) {
								  var wname=decodeURIComponent(json.nickname);
								  var wprice=json.price;
								  var wuserid=json.userid;
								  $('.countdown').html(end_plus+" <b>已结標</b>");
								  if(wuserid=='') {
									 $('#final_bidded').html('<b><span id="bidded">..無人中標..</span></b>');
								  } else {
									 $('#final_bidded').html('<b><span id="bidded">'+wname+' <p align="center">('+wprice+' 元)</p></span></b>');
								  }
							   }
						   });
						}
					}
			};


        function autoUpdate(showReloadIcon) {
			if(showReloadIcon=='Y') {
			   $.mobile.loading('show', {text: '更新中', textVisible: true, theme: 'b', html: ""});
			} else {
			   $.mobile.loading('hide');
			}
			$.ajax({
				url: "/site/product/winner/?_t="+(new Date()).getTime(),
				data: { productid: "<?php echo $product['productid']; ?>"},
				type: "GET",
				dataType: 'text',
				success: function(str_json){
					$.mobile.loading('hide');
					var json = $.parseJSON(str_json);
					// console && console.log($.parseJSON(str_json));
					// $('#bidded').html(json.name);
					if(json.productid == chkprodid) {
						$('#bidded').html(decodeURIComponent(json.name));
					}
				},
				error:function(xhr, ajaxOptions, thrownError){

				}
			});
		}
        function ws_bidding(result) {
            var json = JSON.parse(result);
            if (json.actid == 'BID_WINNER' && json.productid == chkprodid) {
                //有人得標時增加class套用黑色(#bidded)
                if (json.status == 1) {
                    $('#bidded').text(json.name);
                    $("#bidded").addClass("Color-Red");
                }else{
                    $('#bidded').text("目前無人得標");
                    $("#bidded").removeClass("color-black");
                }

            }
        }
		$(document).ready(function(){
		    $.mobile.loading('show', {text: '準備中....', textVisible: true, theme: 'b', html: ""});
			$.ajaxSetup({ cache: false });
            // 啟動倒數
			$.get("<?php echo UNIX_TS_URL; ?>", function(data) {
				  now_time = parseInt(data); //系統時間
				  autoUpdate('');
				  timer=setInterval(timer_run,1000);
				  $.mobile.loading('hide');
    		});
            var ua = navigator.userAgent.toLowerCase();
            var WS_URL = '<?php echo $config['WS_URL ']; ?>';
            WS_URL = '<?php echo $wss_url ?>';
            if (ua.match(/MicroMessenger/i) == "micromessenger" && ua.match(/Android/i) == "android") {
                WS_URL = "ws://www.saja.com.tw/";
            }
            socket1 = new WebSocket(WS_URL);
            socket1.onmessage = function(ret){
                ws_bidding(ret.data);
            }
            $("#btn_endsaja").on('click', function() {
			    var secToClose=10;
			    var doublecheck=confirm("本商品將於 "+secToClose+" 秒內結標 !");
			    if (doublecheck){
	                $.post("/site/kusosaja/setSecToClose",
	                    {productid:chkprodid, secToClose:secToClose},
	                    function(ret) {
	                        var json ;
	                        if(typeof(ret)=="string") {
	                           json = JSON && JSON.parse(ret) || $.parseJSON(ret);
	                        }
	                        if(json.retCode==1) {
	                           /*alert("本商品將於 "+secToClose+" 秒內結標 !");*/
	                           autoUpdate('');
	                           $('#data-offtime').val(json.offtime_ts);
	                           if(timer)
	                              clearInterval(timer);
	                           timer=setInterval(timer_run,1000);
	                        }
	                        // window.location.reload();
	                    }
				    );
        		}
			});
			$("#btn_reload").on('click', function() {
				  location.href="/site/kusosaja/adctrl/?hostid=<?php echo $hostid; ?>";
			});
			$.mobile.loading('hide');


		 });
		</script>
<?php  } else {  ?>
		<script>
		   	var now_sec=0;
            setInterval(function(){
				   now_sec = now_sec + 1;
				   if(now_sec==30) {
					  // $(this).stopTime('losttime');
					  window.location.reload();
				   }
				},1000);
		</script>
<?php  }  ?>
