<?php
   $type = _v('type');
   $channelid = _v('channelid');
   $product = _v('product');
   $productid = _v('productid');
   $user_bid_count = _v('user_bid_count');
   
   $navbar_html = _v('navbar_html');
   $bidSelf_html = _v('bidSelf_html');
   $bidList_html = _v('bidList_html');
     
   //if(file_exists('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html')) {
     // include_once('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html');
   //}
   //test.shajiawang.com/site/bid/detail/?productid=8991
?>

<style>
/*----- MENU滑動選項 -------------------------------------------*/
	.bidDetail {
		position: fixed;
		width: 100%;
		top:2.5rem;
	}
	.bidDetail .listsimg-box-group .listsimg-box {
		background-color: #F9F9F9;
		margin-top: 1.3rem;
	}
	
	<?php $special_bg = empty($navbar_html)? '#F9F9F9' : '#fff';?>
	.special {
		position: fixed;
		width: 100%;
		top: 16rem;
		background-color: <?php echo $special_bg;?>;
		height: 3.6rem;
	}
	.special .navbar-inverse {
		background-color: #fff;
		border-color: #fff;
	}
	
	.special ul{
			padding:0;
			margin:0;
			list-style: none;
	}
	.special ul.nav>li {
		display: inline-block;
		float: none;
		margin: 0rem 0.1rem 0rem 1.2rem;
		
		width: 102px;
		height: 35px;
		line-height: 35px;
		text-align: center;
		font-size: 14px;
			
		background-image: linear-gradient(180deg, #FFFFFF 0%, #F0F0F0 100%);
			border: 1px solid #E8E8E8;
			border-radius: 4px;
	}
		.special ul.nav>li>a.active {
			color: #F9A823;
			font-weight: bolder;
		}
		.special ul.nav>li>a{
			color: #B4B4B4;
		}
		.special ul.nav>li>a:hover,
		.special ul.nav>li>a:focus{
			color: #F9A823;
			font-weight: bolder;
			/*
			width: 102px;
			background-image:linear-gradient(top, #000 0%, transparent 70%);
			border: 1px solid #E8E8E8;
			border-radius: 4px;
			*/
		}
		
		.special ul.nav>li ul{
			padding:10px;
			background:#222;
			position: absolute;
			z-index:100;
			top:50px;
			left:-50%;
			width:200px;
		}
		.special ul.nav>li ul>li>a{
			color:#fff;
			display:block;
			padding: 10px;
			border-bottom: solid 1px #313131;
		}
		.special .navbar-nav{
			float:none;
		}
		.special .navbar-nav > li > a {
			padding-top: 0.8rem;
			padding-bottom: 0.8rem;
			line-height: 20px;
		}
		
	#nav-menu {
		/*	margin-top:-72px;*/
		margin-top:0.5rem;
	}
	#nav-menu .scroll {
		overflow-x: auto;
		white-space: nowrap;
		-webkit-overflow-scrolling: touch;
		-ms-overflow-style: -ms-autohiding-scrollbar;
	}
	.scroll::-webkit-scrollbar {
		display: none;
	}
	@media(max-width:768px){
		#nav-menu {
			width: 30rem;
		}
		.special .collapse {
			display: block;
		}
		.special ul.nav>li ul{
			top:40px;
		}
	}
	
	<?php $list_top = empty($navbar_html)? '10.4rem' : '13.8rem';?>
	.bidSelfList {
		padding-top: 1.75rem;
		margin-top: <?php echo $list_top;?>;
	}
	.bidList-group {
		margin: 0 3.167rem;
		padding: 0.417rem 0 2.5rem;
	}
		.bidList-group .bidList-header{
			position: fixed;
			width: 100%;
			background-color: #F9F9F9;
			height: 2.2rem;
			padding: .875rem 0;
		}
		.bidList-group .header-title {
			text-align: center;
			/*height:2.2rem;*/
			line-height:2.2rem;
		}
		.bidList-group ul {
			margin-top: 4.0rem;
		}
	
	#godom {
		top: 9rem;
	}
	
	.bidList-group .moreBtn {
		margin-top: 4.8rem;
		text-align: center;
	}
	
</style>

<!--  競標資訊  -->
<div class="bidDetail">
<?php echo $bidSelf_html;?>
</div>

<!-- 欲查看的範圍 -->
<div class="special">
	<div class="navbar navbar-inverse" role="navigation">
	<div class="container"><?php echo $navbar_html;?></div>
	</div>
</div>
	
<div class="article">

    <!--  競標資訊  -->
    <div class="bidSelfList">
	<div class="bidList-group">
		<div class="bidList-header d-flex">
			<div class="header-title" >下標金額 (元)</div>
			<div class="header-title" style="padding-right:6rem;">下標人數 (人)</div>
		</div>
        <!-- 生成List -->
		<?php echo $bidList_html;?>
    </div>
	</div>
	
</div><!-- /article -->

<script>
/* MENU，滑動按鈕 */
$(function(){
	$('.special .navbar').after('<div id="nav-menu" class="container"></div>');
	$('#nav-menu').append('<div class="collapse navbar-collapse"></div>');
	$('.navbar-collapse').append('<div class="scroll"></div>');
	$('.scroll').append($('.navbar-nav').clone(true));
	$('#nav-menu .scroll>ul>li>ul').hide();
	$('.navbar>div>.navbar-collapse').remove();
	
	if($(window).width()<568){
		$('#nav-menu .scroll>ul>li.li_show').hide();
	}
});
</script>


<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>
<!-- 指定位置按鈕 -->
<div id="godom" style="display: block;">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gobid.png" alt="得標位置">
</div>

<!-- 回頂與指定位置JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            godomid: '#godom',      //指定移動按鈕名稱
            gotowin: '.listMark',   //指定移動到
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };

    $(function(){
        var $godomMove,             //指定按鈕上移位置
            $btnH,                  //按鈕高度
            $btnSpacing;            //按鈕間距
        
            $(options.dom.gotopid).hide(); //隱藏回頂按鈕
            //偵測平台高度
            var $windowH = $(window).height();
            $(options.dom.godomid).hide();
            if($(options.dom.gotowin).length>0){
                //偵測Mark位置
                var $toPosition = $(options.dom.gotowin).offset().top;
                function godomIsShow() {
                    if($toPosition < $windowH) {
                        $(options.dom.godomid).hide();  //指定移動按鈕(Mark小於原始視窗高度時隱藏)
                    }else{
                        $(options.dom.godomid).show();
                    }
                };
                godomIsShow();
            }
        
		$(window).on('load',function(){
            $btnH = parseInt($(options.dom.gotopid).height());                //抓取按鈕高度
            $btnSpacing = parseInt($(options.dom.gotopid).css('bottom'));     //抓取按鈕間距
            $godomMove = $btnH+$btnSpacing*2;                                 //計算需要上移的距離
            
            function changeShow() {                                           //偵測視窗捲軸動作
				if ($(window).scrollTop() > options.scroll){                    //當捲軸向下移動多少時，按鈕顯示
                    $(options.dom.godomid).css('bottom',$godomMove);          //將指定移動按鈕上移，下方顯示回頂按鈕
                    $(options.dom.gotopid).show();                            //回頂按鈕出現  
                }else{
                    $(options.dom.godomid).css('bottom',$btnSpacing);         //指定按鈕回到原始位置
                    $(options.dom.gotopid).hide();                            //回頂按鈕隱藏
                }
                if ($(window).scrollTop() > $toPosition+$windowH || $(window).scrollTop() < $toPosition-$windowH){                       
                    $(options.dom.godomid).show();                            //指定位置按鈕出現
                }else{
                    $(options.dom.godomid).hide();                            //指定位置按鈕隱藏
                }
            };
            
			$(window).on('scroll resize',function(){
                changeShow();
            });
			
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed); //單擊按鈕，捲軸捲動到頂部
            })
			
            $(options.dom.godomid).click(function(){
                $('body,html').stop().animate({scrollTop:$toPosition},options.scrollspeed); //單擊按鈕，捲軸捲動到指定位置
            })
        });
    })
</script>

<!-- 查看全部按鈕 -->
<script>
/* 若已顯示筆數等於全部筆數，則按鈕不顯示 */
    var esOptions = {
        dom: {
            parent: ".bidList-group",
            bidList: ".bidList"
        }
    };
	console.log($(esOptions.dom.bidList).length );
    function moreBtn () {
        var $parent = $(esOptions.dom.parent),
            $lists = $parent.find(esOptions.dom.bidList),
            $listCount = $lists.length,                                 //目前顯示的筆數
            $allCount = $('#allbidcount').val(),
            
            $link = "location.href='<?PHP echo BASE_URL.APP_DIR; ?>/bid/bidlist/?type=<?php echo $type;?>&channelid=<?php echo $channelid;?>&productid=<?php echo $productid;?>&perpage=50';",                           //查看更多 路徑
            $linkTxt = '此區間沒有下標紀錄'; //'看更多出價紀錄';        //查看更多文字
        
			$parent.append(
                $('<div class="moreBtn"/>').text($linkTxt)
				<?php /*.append(
                    $('<a onclick="'+$link+'"/>')
                    .text($linkTxt)
                )*/?>
            )
    }
    $(window).on('load',function(){
        if($(esOptions.dom.bidList).length <1){
			moreBtn();
		}
    });
</script>
