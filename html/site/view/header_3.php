<?php
    $header = _v('header');
    if($header=='N') {
        return;
    }
    $noback = _v('noback');
?>	
<?php if (!is_weixin()) { ?>
    <div class="navbar sajaNavBar d-flex align-items-center">
		<div class="sajaNavBarIcon">
            <?php if (empty($noback) && $noback != "Y") { ?>
            <a href="javascript:history.back();" >
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
            </a>
            <?php } else { ?>
            <a href="#" >
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-qrcode.png">
            </a>				　
            <?php  } ?>
        </div>
		<div class="navbar-brand">
			<h3><?php echo $tpl->page_header; ?></h3>
		</div>
		<div class="sajaNavBarIcon">
			<a href="#left-panel<?php get_panel_id(); ?>" data-iconpos="right">
				<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/hamburger.png">
			</a>
        </div>
	</div>
<script>
    // 不是在微信  也不是在appbsl的app裏  才顯示Navigator bar

    $(document).ready(function() {
        if(is_kingkr_obj()) {
            // controllNavigateLayout(0);
        }
        //if(!is_kingkr_obj()) {
        $('#navibar').show();
        //}
    });
</script>
<?php } ?>	

<!--div data-role="header" >
<h1><?php echo $tpl->page_header; ?></h1>
<a href="#left-panel<?php get_panel_id(); ?>" data-icon="bars" >功能</a>
<a href="#right-panel<?php get_panel_id(); ?>" data-icon="gear" data-iconpos="right">会员</a>
</div-->
<!--data-role="header"-->
