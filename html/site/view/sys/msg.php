<?php
	$status = _v('status');
	$msgdata = _v('data');
?>

<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/sysmsg.css">
<?php if(!empty($msgdata['content']) ) { ?>
    <div>
        <div class="sajamsg-imgbox">
            <div class="sajamsg_lay">
                <?php if ($msgdata['kind'] == "product") { ?>
                    <?php
                        $img_src= APP_DIR ."/static/img/productMsg-img2.png";
                    ?>
                    <div class="msg-img">
                        <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>">
                    </div>
                    <div id="changeNum" class="msg-txt">
                        <p class="price_su">成功下標</p>
                        <div class="price_num">

                        </div>
                   
                     <!-- 下方文字說明 -->
                    <!-- <div class="productMsg-rule">
                        <p>*2019/1/21起 得標商品請於3日內完成結帳程序，</p>
                        <p>逾期視同放棄商品。</p>
                        <p>放棄商品下標時全部的支出將不會轉贈鯊魚點！</p>
                    </div> -->
                </div>
            </div>
            <pre class="msg_txt"><?php echo $msgdata['msg'];?></pre>
            <?php } elseif($msgdata['kind'] == "mall") { ?>
                <?php
                    if(!empty($msgdata['content']['thumbnail'])) {
                        $img_src = APP_DIR .'/images/site/product/'. $msgdata['content']['thumbnail'];
                    } elseif (!empty($msgdata['content']['thumbnail_url'])) {
                        $img_src = $msgdata['content']['thumbnail_url'];
                    } else {
                        $img_src= APP_DIR ."/static/img/sa.png";
                    }
                ?>
                <div class="mallMsg-box">
                    <div class="msg-img">
                        <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>">
                    </div>
                    <h4 class="msg-txt"><?php echo $msgdata['content']['name']; ?></h4>
                    <h4 class="msg-txt"><?php echo $msgdata['msg'];?></h4>
                    <div class="msg-price">- <span><?php echo $msgdata['total']; ?></span> 點</div>
                </div>

            <?php }elseif($msgdata['kind'] == "bid"){ ?>
                <?php
                    $img_src= APP_DIR ."/static/img/bid-checkout-img.png";
                ?>
                <div class="bidMsg-box">
                    <div class="msg-img">
                        <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>">
                    </div>
                    <div class="msg-txt">結帳成功 !!</div>
                </div>
            <?php } ?>
        </div>
        <div class="sajamsg-btnbox">
            <?php if ($msgdata['kind'] == "mall"){?>
                <button class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/<?php echo $msgdata['kind'];?>'">
                    <div>完成</div>
                </button>
                <?php if ($msgdata['kid']=='689'){ ?>
                    <div class="msg-ps text-center">*點數充值需3-5個工作天</div>		
                <?php } ?>
            <?php }elseif ($msgdata['kind'] == "product"){ ?>
                <?php if($_SESSION['fb_share_link_data'] && $msgdata['content']['lb_used'] == 'N'){ ?>
                    <button class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/<?php echo $msgdata['kind'];?>/sajabid/?productid=<?php echo $msgdata['kid'];?>'">
                        <div>再次殺價去</div>
                        <div class="arrow">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/sysMsg-arrow.png" alt="">
                        </div>
                    </button>
                    <div class="msg-share-btns d-flex justify-content-between">
                        <div class="go-prohome-btn d-flex align-items-center justify-content-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/<?php echo $msgdata['kind'];?>/saja/?productid=<?php echo $msgdata['kid'];?>'">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/msg-back.png" alt="">
                        </div>
                        <div class="go-fb-btn">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/msg_FB_link.png" alt="">
                        </div>
                    </div>
                <?php }else{ ?>
                    <button class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/<?php echo $msgdata['kind'];?>/sajabid/?productid=<?php echo $msgdata['kid'];?>'">
                        <div>再次下標</div>
                        <div class="arrow back_up">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/sysMsg-arrow.png" alt="">
                        </div>
					</button>
					<?php if($msgdata['content']['lb_used'] == 'N'){ ?>
						<button class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/<?php echo $msgdata['kind'];?>/saja/?>'">
							<div>回到首頁</div>
							<div class="arrow">
								<img src="<?PHP echo APP_DIR; ?>/static/img/sysMsg-arrow.png" alt="">
							</div>
						</button>
					<?php }else{?>
						<button class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/livebroadcast'">
							<div>回到直播頁</div>
							<div class="arrow">
								<img src="<?PHP echo APP_DIR; ?>/static/img/sysMsg-arrow.png" alt="">
							</div>
						</button>
					<?php }
				} ?>

            <?php }elseif($msgdata['kind'] == "bid"){ ?>
                <button class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button"onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/history/saja/?<?php echo $cdnTime; ?>'">
                    <div>回殺價紀錄</div>
                </button>
            <?php }else { ?>

            <?php }?>
        </div>
    </div>
<?php } ?>
<script>
<?php if ($msgdata['kind'] == "product") { ?>
    $(function(){
        var $self = $(".msg_txt");
        var $str = $(".msg_txt").html();

        //單次競標
        var $num = '<span class="num">'+<?php echo json_encode($msgdata['obid']);?>+'</span>';

        //連續下標
        var $numA = '<div class="num_price"><span class="num">'+<?php echo json_encode($msgdata['obida']);?>+'</span>';
        var $numB = '<span class="num">'+<?php echo json_encode($msgdata['obidb']);?>+'</span></div>';

        //替換文字
        //有xxx代表單次下標或隨機下標金額
        function $change() {
            var $changed = '';
            if($str.indexOf("yyy")!=-1) {
                //連續下標
                $changed = $str.replace(/xxx/,'').replace(/yyy/,'');
                
                
                $self.html($changed);
                $(".price_num").html($numA+$numB);
            }else if($str.indexOf("xxx")!=-1){
                //單次下標 && 隨機下標
                $changed = $str.replace(/xxx/,'');
                $self.html($changed);
                $(".price_num").html($num);
               
            }else{
                //鯊魚商城
                return;
            }

        }
        // function $change() {
        //     var $changed = '';
        //     if($str.indexOf("yyy")!=-1) {
        //         //連續下標
        //         $changed = $str.replace(/xxx/, $numA).replace(/yyy/, $numB);
        //         $self.html($changed);
        //     }else if($str.indexOf("xxx")!=-1){
        //         //單次下標 && 隨機下標
        //         $changed = $str.replace(/xxx/, $num);
        //         $self.html($changed);
        //     }else{
        //         //鯊魚商城
        //         return;
        //     }
        // }
        $change();
    })
<?php }else if($msgdata['kind'] == "mall"){ ?>
    $(function(){
        $('.sajaNavBarIcon.back').hide();           //回上一頁按鈕隱藏
    })
<?php } ?>
</script>

<!--分享按鈕-->
<script>

    // $(function(){
    //     $('.share-btn').on('click', function(){
    //         var chkuserid = "<?php echo $_SESSION['auth_id']; ?>";
    //         var pdid = "<?php echo $msgdata['kid'];?>";
    //         var behav = 'b';
    //         window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('http://test.shajiawang.com/site/shareoscode?user_src='+chkuserid+'&productid='+pdid+'&behav='+behav)) );
    //     })
    // });

    $(function(){
        $('.go-fb-btn').on('click', function(){
            var userid = "<?php echo $_SESSION['fb_share_link_data']['userid']; ?>";
            var productid = "<?php echo $_SESSION['fb_share_link_data']['productid'];?>";
            var behav = "<?php echo $_SESSION['fb_share_link_data']['behav'];?>";
            var lbuserid = "<?php echo $_SESSION['fb_share_link_data']['lbuserid'];?>";
            window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('<?php echo BASE_URL.APP_DIR; ?>/shareoscode/activityhome/?user_src='+userid+'&behav='+behav+'&lbuserid='+lbuserid)) );
        })
    });
    //隱藏回上一頁按鈕
    $('.sajaNavBarIcon.back').hide();
</script>
