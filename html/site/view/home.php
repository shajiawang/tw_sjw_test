<?php
$cdnTime = date("YmdHis");
$status = _v('status');
$product_list = _v('product_list');
$bid_list = _v('bid_list');
$bid_user_count = _v('bid_user_count');
$bid_money_count = _v('bid_money_count');
$ad_list = _v('ad_list')['record'];
$imgrand = rand(1,3);
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') >0 ) {
	$browser = 1;
}else{
	$browser = 2;
}
if(!empty($_SESSION['auth_id'])) {
	$faq_url=BASE_URL.APP_DIR.'/faq/';
	$tech_url=BASE_URL.APP_DIR.'/tech/';
} else {
	$faq_url=BASE_URL.APP_DIR.'/faq/';
	$tech_url=BASE_URL.APP_DIR.'/tech/';
}
?>

<!-- 最新成交-跑馬燈 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/jquery.marquee.js"></script>
<!-- 成功圓夢 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/counter.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Counter-Up/1.0.0/jquery.counterup.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/waypoints/4.0.1/jquery.waypoints.js"></script>
<!-- 此頁css -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/sajahome.css">

<script type="text/javascript">
    function openPage(url) {
        if (url == '') {
            return false;
        }
        if (is_kingkr_obj()) {
            awakeOtherBrowser(url);
        } else {
            window.location.href = url;
        }
    } 
</script>

<div id="container">
    <!-- 20190506 廣告輪播 -->
    <script> 
        var picurl; 
        $.ajax({
            type: 'POST',
            url: '<?php echo BASE_URL.APP_DIR;?>/ad/?json=Y&kind=1',
            dataType: 'json',
            success: function(msg) {
                var adproductpic = msg.retObj.data; 
                var onedate =adproductpic.length-1 ;
                    if(onedate == 0){
                        if(adproductpic.thumbnail === null || adproductpic.thumbnail === ""){
                            picurl=adproductpic[0].thumbnail_url;
                        }else{
                            picurl=adproductpic[0].thumbnail;
                        }
                        $(".adproductpic1").css('display','none');
                        $(".slider_show1").append('<figure><img src="http://test.shajiawang.com/site/images/site/ad/' + picurl +'"alt="'+ adproductpic[0].name +'" > </figure>')
                    }else{
                        for (var key in adproductpic) {
                        if(adproductpic[key].thumbnail.length>0){
                            picurl=adproductpic[key].thumbnail;
                        }else{
                            picurl=adproductpic[key].thumbnail_url;
                        }
                        if(key <= 11){
                            $(".adproductpic1").append('<li><div class="cui-slide-img-item"><a class="ui-link" href=' + adproductpic[key].action+'><img src="http://test.shajiawang.com/site/images/site/ad/' + picurl +'"alt="'+ adproductpic[key].name +'" >' + '</a></div></li>' );
                        }
                    }
                   
                }
            }
        });  
   </script>
    <div class="flexslider clearfix slider_show1">
        <ul class="slides adproductpic1">
        </ul>
    </div>

    <!--  最新得標  -->
    <div class="deal-box d-flex">
        <div class="deal-title"><span>最新得標</span></div>
        <div class="marqueebox">
            <ul id="marquee" class="marquee">
                <?php
				foreach($bid_list as $r => $rv){
			?>
                <li><a
                        href="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/bid/detail/?productid=<?php echo $rv['productid']; ?>'">恭喜
                        <?php echo $rv['nickname']?> 以 <?php echo $rv['price']?> 元獲得 <?php echo $rv['name']?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <!--  menu  -->
    <div id="category" class="clearfix bg_newking">
        <div id="category-wrapper" class="text-center d-flex justify-content-around">
            <div class="item">
                <a href="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/faq/?<?php echo $cdnTime; ?>'">
                    <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/homeMenu/i1.png">
                    <p>新手教學</p>
                </a>
            </div>
            <div class="item">
                <a href="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/product/?<?php echo $cdnTime; ?>'">
                    <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/homeMenu/i2.png">
                    <p>殺戮戰場</p>
                </a>
            </div>
            <div class="item">
                <a href="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/bid/?<?php echo $cdnTime; ?>'">
                    <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/homeMenu/i3.png">
                    <p>最新得標</p>
                </a>
            </div>
            <div class="item">
                <a href="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/tech/?<?php echo $cdnTime; ?>'"
                    <?php if ($browser == 1){ ?>target="_blank" <?php } ?>>
                    <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/homeMenu/i4.png">
                    <p>王者秘笈</p>
                </a>
            </div>
        </div>
    </div>
    <!--  廣告版位  -->
    <div class="mModalBox"></div>
    <!-- 蓋板浮動版位 -->
    <div class="cModalBox"></div>
    <!--  成功殺價  -->
    <div class="success-box">
        <div class="success-title">
            <span>已幫助</span>
            <!-- <span class="num"></span> -->
            <span class="counter">1</span>
            <span>人成功圓夢</span>
        </div>
        <div class="success-money">
            <span class="symbol align-middle">圓夢金額 NT</span>
            <ul class="jackpot counter align-middle">
                js生成
            </ul>
        </div>
    </div>

    <div class="subtitle-box soon bgw">
        <div class="subtitle left-title p_l20">
            <span class="icon_soon">即將結標</span>
        </div>
    </div>

    <div class="sajalist-home soon d-inlineflex flex-wrap">
        <?php
            $i = 0;
            foreach($product_list as $r => $rv){
            if ($i<12){
				if($rv['lb_used'] == 'Y'){
					$product_url = BASE_URL.APP_DIR.'/livebroadcast';
				}else{
					$product_url = BASE_URL.APP_DIR.'/product/saja/?'.$status['status']['args'].'&productid='.$rv['productid'].'&type='.$_GET['type'].'&t='.$cdnTime;
				}
        ?>
        <div class="sajalist-item align-items-center justify-content-center">
            <a href="#"
                onclick="javascript:location.href='<?php echo $product_url;?>'">
                <!-- 201904251ul 產品標籤 -->
                <div class="sajalist-img d-flex flex-column psr">
                    <header class="who_limit">
                        <span style= background-color:#<?php echo $rv['colorcode'];?>>
                            <?php echo $rv['plname'];?>
                        </span> 
                    </header>
                    <i>
                        <?php if(!empty($rv['thumbnail'])){ ?>
                        <img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif"
                            data-src="<?php echo IMG_URL.$status['path_image'].'/product/'. $rv['thumbnail']; ?>">
                        <!-- img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif" data-src="<?php echo $status['path_image'] .'/product/'. $rv['thumbnail']; ?>" -->
                        <?php }elseif (!empty($rv['thumbnail_url'])){ ?>
                        <img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif"
                            data-src="<?php echo $rv['thumbnail_url']; ?>">
                        <?php }else{ ?>
                        <img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif"
                            data-src="http://howard.sajar.com.tw/site/images/site/product/1ee1b3f296827f55d52783119f036316.jpg">
                        <?php }
                            ?>
                    </i>
                    <p class="sajalist-label"><?php echo $rv['name']; ?></p>
                    <div class="sajalist-overtime mt-auto">
                        <span id="ot-<?php echo $rv['productid']; ?>" class="overtime"
                            data-offtime="<?php echo $rv['offtime']; ?>" data-tag="">0天 00:00:00</span>
                    </div>
                </div>
            </a>
        </div>
        <script type="text/javascript">
            $(document).ready(function () {
                var interval = 59;
                var intervalId<?php echo $rv['productid']; ?> = window.setInterval(function () {
                    ShowTimer(<?php echo $rv['unix_offtime'] ?>, '',
                        'ot-<?php echo $rv['productid']; ?>');
                }, interval);
            });
        </script>
        <?php
            }
            $i++;
            }
        ?>
    </div>
    <!-- 20190502 即時結標下方廣告-->
    <div class="th_adproduct">
       
    </div>
    <!-- 20190508 即時結標下方廣告 -->
    <script> 
            $.ajax({
            type: 'POST',
            url: '<?php echo BASE_URL.APP_DIR;?>/product/today?json=Y&p=1',
            dataType: 'json',
            success: function(msg) {
                console.log('首頁',msg)
                var adproductpic = msg.retObj.data; 
                var onedate =adproductpic.length-1 ;
                var nowpic1,nowpic2;
                    if(onedate < 0){
                        $(".th_adproduct").css('display','none');
                    }else{
                        //全部顯示
                        // for (var key in adproductpic) {
                        //     if(adproductpic[key].thumbnail === null || adproductpic[key].thumbnail=== ""){
                        //         nowpic1=adproductpic[key].thumbnail_url2;
                        //         nowpic2=adproductpic[key].thumbnail_url;
                        //     }else{
                        //         nowpic1=adproductpic[key].thumbnail2;
                        //         nowpic2 =adproductpic[key].thumbnail;
                        //     }

                        //    $(".th_adproduct").append('<a href="'+adproductpic[key].link+';" class="md_adproduct">'+
                        //         '<header>'+
                        //         '<p class="more_txt">'+adproductpic[key].name+'</p>'+
                        //         '<div class="timeoff" data-offtime="'+adproductpic[key].offtime+'">'+
                        //             '<span></span>'+
                        //         '</div>'+
                        //         '</header>'+
                        //         '<div>'+
                        //             '<p class="more_txt abourtxt">'+adproductpic[key].description+'</p>'+
                        //             '<div class="nowpic1"><figure style="background-image: url(http://test.shajiawang.com/site/images/site/product/'+nowpic1 +');"></figure></div>'+
                        //             '<div class="nowpic2"><figure style="background-image: url(http://test.shajiawang.com/site/images/site/product/'+nowpic2 +');"></figure></div>'+
                        //         '</div>'+
                        //     '</a>');
                            
                        // }
                        //只出現一張
                        if(adproductpic[0].thumbnail === null || adproductpic[0].thumbnail=== ""){
                                nowpic1=adproductpic[0].thumbnail_url2;
                                nowpic2=adproductpic[0].thumbnail_url;
                            }else{
                                nowpic1=adproductpic[0].thumbnail2;
                                nowpic2 =adproductpic[0].thumbnail;
                            }
                        $(".th_adproduct").append('<a href="'+adproductpic[0].link+';" class="md_adproduct">'+
                                '<header>'+
                                '<p class="more_txt">'+adproductpic[0].name+'</p>'+
                                '<div class="timeoff" data-offtime="'+adproductpic[0].offtime+'">'+
                                    '<span></span>'+
                                '</div>'+
                                '</header>'+
                                '<div>'+
                                    '<p class="more_txt abourtxt">'+adproductpic[0].description+'</p>'+
                                    '<div class="nowpic1"><figure style="background-image: url(http://test.shajiawang.com/site/images/site/product/'+nowpic1 +');"></figure></div>'+
                                    '<div class="nowpic2"><figure style="background-image: url(http://test.shajiawang.com/site/images/site/product/'+nowpic2 +');"></figure></div>'+
                                '</div>'+
                            '</a>');
                            
                            var initEndTime2;//基础结束时间
                            var init2=[];
                            var endTime2;//结束时间
                            var leftTime2;//时间差
                            $(".timeoff").each(function(index){
                                initEndTime2= $(".timeoff").eq(index).attr('data-offtime');
                                    init2.push(initEndTime2);
                                    var timer = setInterval(function(){
                                        timeLeft($(".timeoff").eq(index) ,init2[index]);
                                        // console.log(init[index]);
                                    },59)
                            })

                            function timeLeft(select,initEndTime2){
                               
                                endTime2 = new Date(initEndTime2);//结束时间
                                leftTime2 = endTime2 - new Date();//时间差
                                if(leftTime2 < 0){
                                    initEndTime2 += 1000*60;//时间差为0时，基础结束时间自增一分钟
                                }else{
                                    var d = Math.floor(leftTime2/(1000*60*60*24));
                                    var h = Math.floor(leftTime2/(1000*60*60)%60);
                                    var m = Math.floor(leftTime2/(1000*60)%60);
                                    var s = Math.floor(leftTime2/1000%60);
                                    var ms = Math.floor(leftTime2%60);
                                    if(d < 10){
                                        d= "0"+d;
                                    }
                                    if(h < 10){
                                        h= "0"+h;
                                    }
                                    if(m < 10){
                                        m= "0"+m;
                                    }
                                    if(s < 10){
                                        s= "0"+s;
                                    }
                                    if(ms < 10){
                                        ms= "0"+ms;
                                    }
                                    $(select).find('span').text(d+"天"+h+"時"+m+"分"+s+"秒"+ms);
                                }
                            }
                    }
                }
            });  
    </script>

    <!-- 20190425 熱門商品 -->
    <div class="th_hotproduct d-none">
        <div class="subtitle-box hot">
            <div class="subtitle left-title p_l20">
                <span class="icon_fire">熱門商品</span>
            </div>
        </div>
        
        <div class="sajalist-home hot d-inlineflex flex-wrap hotproduct">
            <?php
            $i = 0;
            foreach($product_list as $r => $rv){
            if ($i<12){
                if($rv['lb_used'] == 'Y'){
                    $product_url = BASE_URL.APP_DIR.'/livebroadcast';
                }else{
                    $product_url = BASE_URL.APP_DIR.'/product/saja/?'.$status['status']['args'].'&productid='.$rv['productid'].'&type='.$_GET['type'].'&t='.$cdnTime;
                }
            ?>
            <div class="sajalist-item align-items-center justify-content-center">
                <a href="#"
                    onclick="javascript:location.href='<?php echo $product_url;?>'">
                    <!-- 201904251ul 產品標籤 -->
                    <div class="sajalist-img d-flex flex-column psr">
                        <header class="who_limit">
                            <span style= background-color:#<?php echo $rv['colorcode'];?>>
                                <?php echo $rv['plname'];?>
                            </span> 
                        </header>
                        <i>
                            <?php if(!empty($rv['thumbnail'])){ ?>
                            <img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif"
                                data-src="<?php echo IMG_URL.$status['path_image'].'/product/'. $rv['thumbnail']; ?>">
                            <!-- img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif" data-src="<?php echo $status['path_image'] .'/product/'. $rv['thumbnail']; ?>" -->
                            <?php }elseif (!empty($rv['thumbnail_url'])){ ?>
                            <img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif"
                                data-src="<?php echo $rv['thumbnail_url']; ?>">
                            <?php }else{ ?>
                            <img class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif"
                                data-src="http://howard.sajar.com.tw/site/images/site/product/1ee1b3f296827f55d52783119f036316.jpg">
                            <?php }
                                ?>
                        </i>
                        <p class="sajalist-label"><?php echo $rv['name']; ?></p>
                        <div class="sajalist-overtime mt-auto">
                            <span id="ot-<?php echo $rv['productid']; ?>" class="overtime"
                                data-offtime="<?php echo $rv['offtime']; ?>" data-tag="">0天 00:00:00</span>
                        </div>
                    </div>
                </a>
            </div>
            <script type="text/javascript">
                $(document).ready(function () {
                    var interval = 59;
                    var intervalId<?php echo $rv['productid']; ?> = window.setInterval(function () {
                        ShowTimer(<?php echo $rv['unix_offtime'] ?>, '',
                            'ot-<?php echo $rv['productid']; ?>');
                    }, interval);
                });
            </script>
            <?php
                }
                $i++;
                }
            ?>            
        </div>
                
        <script type="text/javascript">
            var hotpic,nowhotpic=0;
            $.ajax({
                type: 'POST',
                url: '<?php echo BASE_URL.APP_DIR;?>/product/hot_product_list/?json=Y',
                dataType: 'json',
                success: function(msg) {
                    console.log('熱門商品', msg)
                    var hotproductpic = msg.retObj.data; 
                    $(hotproductpic).each(function(index){
                        if(hotproductpic[index].thumbnail.length>0){
                            hotpic=hotproductpic[index].thumbnail;
                        }else{
                            hotpic=hotproductpic[index].thumbnail_url;
                        }

                        if(index <= 11){
                            $(".th_hotproduct").removeClass('d-none');
                            $(".hotproduct").append('<div class="sajalist-item align-items-center justify-content-center">'+
                                '<a href="<?php echo BASE_URL.APP_DIR;?>/product/saja/?channelid=' 
                                + hotproductpic[index].channelid +'&'+hotproductpic[index].productid +
                                '&type=&t='+hotproductpic[index].now +'">'+
                                '<div class="sajalist-img d-flex flex-column psr">'+
                                '<header class="who_limit"><span style="background-color:#'+hotproductpic[index].colorcode+'">'+
                                hotproductpic[index].plname+'</span></header>'+
                                '<i>'+
                                '<img class="lazyload" src="<?php echo BASE_URL.APP_DIR;?>/static/img/loading.gif" data-src="<?php echo BASE_URL.APP_DIR;?>/images/site/product/' +
                                hotpic+'"></i>'+
                                '<p class="sajalist-label">'+ hotproductpic[index].name +'</p>'+ 
                                '<div class="sajalist-overtime mt-auto"><span id="'+hotproductpic[index].productid+
                                '"class="overtime hottime" data-offtime="'+hotproductpic[index].offtime+'" data-tag="">'+
                                '<span class="first_letter">0天</span>'+ 
                                '<span class="timetxt_letter">0:0:0:0</span>'+
                                '</span></div></div></a></div>'
                            );
                            $("img.lazyload").lazyload(); 
                            
                        }
                        var initEndTime;//基础结束时间
                        var init=[];
                        var endTime;//结束时间
                        var leftTime;//时间差
                        $(".hottime").each(function(index){
                                initEndTime= $(".hottime").eq(index).attr('data-offtime');
                                
                                    init.push(initEndTime);
                                    var timer = setInterval(function(){
                                        timeLeft($(".hotproduct .hottime").eq(index),init[index]);
                                    },59)
                            })

                            function timeLeft(select,initEndTime){
                                
                                endTime = new Date(initEndTime);//结束时间
                                leftTime = endTime - new Date();//时间差

                                    
                                if(leftTime < 0){
                                    initEndTime += 1000*60;//时间差为0时，基础结束时间自增一分钟
                                }else{
                                    var d = Math.floor(leftTime/(1000*60*60*24));
                                    var h = Math.floor(leftTime/(1000*60*60)%60);
                                    var m = Math.floor(leftTime/(1000*60)%60);
                                    var s = Math.floor(leftTime/1000%60);
                                    var ms = Math.floor(leftTime%60);
                                    if(d < 10){
                                        d= "0"+d;
                                    }
                                    if(h < 10){
                                        h= "0"+h;
                                    }
                                    if(m < 10){
                                        m= "0"+m;
                                    }
                                    if(s < 10){
                                        s= "0"+s;
                                    }
                                    if(ms < 10){
                                        ms= "0"+ms;
                                    }
                                    
                                    $(select).find('.first_letter').text(d+"天");
                                    $(select).find('.timetxt_letter').text(+h+":"+m+":"+s+":"+ms);
                                }
                            }
                    }); 
                } 
            
            });

        </script>
    </div>
</div>
    <!-- 20190506 廣告輪播 -->
    <script> 
       var picur2; 
        $.ajax({
           type: 'POST',
           url: '<?php echo BASE_URL.APP_DIR;?>/ad/?json=Y&kind2',
           dataType: 'json',
           success: function(msg) {
                var adproductpic = msg.retObj.data;     
                var onedate2 =adproductpic.length-1 ;       
                for (var key in adproductpic) {
                    if(adproductpic[key].thumbnail.length>0){
                        picur2=adproductpic[key].thumbnail;
                    }else{
                        picur2=adproductpic[key].thumbnail_url;
                    }
                   if(key <= 11){
                       console.log('廣告', msg)
                       $(".adproductpic2").append('<li><div class="cui-slide-img-item"><a class="ui-link" href=' + adproductpic[key].action+'><img src="<?php echo BASE_URL.APP_DIR;?>/images/site/ad/' + picur2 +'"alt="'+ adproductpic[key].name +'" >' + '</a></div></li>' );
                   }
               }     
           }
       });  
   </script>
    <div class="flexslider clearfix adpic2">
        <ul class="slides adproductpic2">
        </ul>
    </div>
   
    <!-- 20190425 宅公益 -->
    <div class="th_publicwelfare">
        <div class="subtitle-box">
            <div class="subtitle left-title p_l20">
                <span class="icon_love">宅公益</span>
                <a class="seemore" href="javascript:;">
                    <span>查看更多</span>
                </a>
            </div>
        </div>
        <div class="sajalist-home welfare d-inlineflex flex-wrap houseproduct">
            </div>
                
            <script type="text/javascript">
                var housepic,nowhousepic=0;
                $.ajax({
                    type: 'POST',
                    url: '<?php echo BASE_URL.APP_DIR;?>/product/getCategoryProduct/?json=Y&pcid=91&p=1',
                    dataType: 'json',
                    success: function(msg) {
                        console.log('宅公益', msg)
                        var houseproductpic = msg.retObj.data; 
                        if(houseproductpic !== undefined){
                            $(houseproductpic).each(function(index){
                            if(houseproductpic[index].thumbnail.length>0){
                                housepic=houseproductpic[index].thumbnail;
                            }else{
                                housepic=houseproductpic[index].thumbnail_url;
                            }

                            if(index <= 11){
                                $(".th_publicwelfare").removeClass('d-none');
                                $(".houseproduct").append('<div class="sajalist-item align-items-center justify-content-center">'+
                                    '<a href="<?php echo BASE_URL.APP_DIR;?>/product/saja/?channelid=' 
                                    + houseproductpic[index].channelid +'&'+houseproductpic[index].productid +
                                    '&type=&t='+houseproductpic[index].now +'">'+
                                    '<div class="sajalist-img d-flex flex-column psr">'+
                                    '<header class="who_limit"><span style="background-color:#'+houseproductpic[index].colorcode+'">'+
                                    houseproductpic[index].plname+'</span></header>'+
                                    '<i>'+
                                    '<img class="lazyload" src="<?php echo BASE_URL.APP_DIR;?>/static/img/loading.gif" data-src="<?php echo BASE_URL.APP_DIR;?>/images/site/product/' +
                                    housepic+'"></i>'+
                                    '<p class="sajalist-label">'+ houseproductpic[index].name +'</p>'+ 
                                    '<div class="sajalist-overtime mt-auto"><span id="'+houseproductpic[index].productid+
                                    '"class="overtime housetime" data-offtime="'+houseproductpic[index].offtime+'" data-tag="">'+
                                    '<span class="first_letter">0天</span>'+ 
                                    '<span class="timetxt_letter">0:0:0:0</span>'+
                                    '</span></div></div></a></div>'
                                );
                                $("img.lazyload").lazyload(); 
                                
                            }
                            var initEndTime;//基础结束时间
                            var init=[];
                            var endTime;//结束时间
                            var leftTime;//时间差
                            $(".housetime").each(function(index){
                                    initEndTime= $(".housetime").eq(index).attr('data-offtime');
                                    
                                        init.push(initEndTime);
                                        var timer = setInterval(function(){
                                            timeLeft($(".hotproduct .housetime").eq(index),init[index]);
                                        },59)
                                })

                                function timeLeft(select,initEndTime){
                                   
                                    endTime = new Date(initEndTime);//结束时间
                                    leftTime = endTime - new Date();//时间差
                                        
                                    if(leftTime < 0){
                                        initEndTime += 1000*60;//时间差为0时，基础结束时间自增一分钟
                                    }else{
                                        var d = Math.floor(leftTime/(1000*60*60*24));
                                        var h = Math.floor(leftTime/(1000*60*60)%60);
                                        var m = Math.floor(leftTime/(1000*60)%60);
                                        var s = Math.floor(leftTime/1000%60);
                                        var ms = Math.floor(leftTime%60);
                                        if(d < 10){
                                            d= "0"+d;
                                        }
                                        if(h < 10){
                                            h= "0"+h;
                                        }
                                        if(m < 10){
                                            m= "0"+m;
                                        }
                                        if(s < 10){
                                            s= "0"+s;
                                        }
                                        if(ms < 10){
                                            ms= "0"+ms;
                                        }
                                        
                                        $(select).find('.first_letter').text(d+"天");
                                        $(select).find('.timetxt_letter').text(+h+":"+m+":"+s+":"+ms);
                                    }
                                }
                            }); 
                        }else{
                            $(".th_publicwelfare").css('display','none');
                        }
                        
                    }
                
                });

            </script>
    </div>

    <div class="th_nothing">
        <p>沒有任何東西</p>
    </div>
</div>


<script type="text/javascript">
    function groupsaja() {
        alert('敬請期待!');
    }

    function flashsaja() {
        alert('招募地推人員，請洽ditui@shajiawang.com');
    }

    function ShowTimer(otime, ntime, divId) {
        var now = new Date();

        //getTime()获取的值/1000=秒数
        var leftTime = (otime * 1000) - now.getTime();

        var leftSecond = parseInt(leftTime / 1000);

        if (leftTime > 0) {

            var day = Math.floor(leftSecond / (60 * 60 * 24));

            var hour = Math.floor((leftSecond - day * 24 * 60 * 60) / 3600);
            if (hour < 10) {
                hour = "0" + hour;
            };

            var minute = Math.floor((leftSecond - day * 24 * 60 * 60 - hour * 3600) / 60);
            if (minute < 10) {
                minute = "0" + minute;
            };

            var second = Math.floor(leftSecond - day * 24 * 60 * 60 - hour * 3600 - minute * 60);
            if (second < 10) {
                second = "0" + second;
            };
            var msecond = Math.floor(leftTime % 100);
            if (msecond < 10) {
                msecond = "0" + msecond;
            };

            var htmlElement = document.getElementById(divId);

            if (day == "00") {
                // 小於一天時不要顯示天數
                $('.' + divId).html(hour + "時" + minute + "分" + second + "秒" + msecond);
                 htmlElement.innerHTML = "" + hour + "時" + minute + "分" + second + "秒" + msecond;
            } else {
                // 20190502
                 htmlElement.innerHTML = '<span class="first_letter">' + day + "天</span> " +'<span class="timetxt_letter">'+ hour + "時" + minute + "分" + second + "秒" + msecond+"</span>";
            }
        } else if (leftTime < 0) {
            var htmlElement = document.getElementById(divId);
            htmlElement.innerHTML = "已結標";
            window.clearInterval('intervalId' + divId);

            var $location = <?php echo json_encode(BASE_URL.APP_DIR) ?>;
            var $cdnTime = <?php echo json_encode($cdnTime) ?>;
            var $that = $('#' + divId),
                $alert = 'alert("此商品已結標")',
                $href = 'location.href="' + $location + '/product/?' + $cdnTime + '"';
            $that.parents(".sajalist-item").find("a").attr('onclick', $alert + ',' + $href);

        }
    }

    $(function () {
        SyntaxHighlighter.all();
    });
  
    $(function () {
        /*跑馬燈啟動*/
        $("#marquee").marquee({
            yScroll: "bottom"
        });

        /*懶載入初始化*/
        $(".sajalist-img .lazyload").lazyload({
            threshold: 50 //提前加載(像素)
        });
    })

    //懶加載漏網之魚
    $(window).on('load', function () {
        //loader漏網之魚
        var interval = null;
        var options = {
            dom: {
                item: '.sajalist-item',
                img: '.lazyload'
            }
        }

        function $reload() {
            var $winH = $(window).height(); //螢幕高度
            var $winPos = $(window).scrollTop(); //目前捲軸位置
            var $item = $(options.dom.item);
            var $img = $item.find(options.dom.img); //抓取所有lazyload img
            //讓作用中頁面的圖片都跑一次迴圈，判斷是否加載
            $img.each(function () {
                var $that = $(this);
                var $imgPos = $(this).offset().top; //圖片位置
                if ($imgPos > $winPos && $imgPos < $winPos + $winH) {
                    var $showSrc = $that.attr('src'); //loading圖片
                    var $showDsrc = $that.attr('data-src'); //產品圖片
                    //判斷兩者不相同時 表示未載入,這張圖 重新lazyload
                    if ($showSrc !== $showDsrc) {
                        $(this).lazyload();
                    }
                }

            })
        }
        //判斷loader漏網之魚，2秒判斷一次
        //reloadimg = setInterval($reload,2000);
        //因為只有單頁，改為 滾動時判斷
        $(window).on('scroll', function () {
            var st = $(window).scrollTop(); 
            $reload();
        })
    })
</script>

<?php if(false) { ?>
<script type="text/javascript">
    function showLoginPanel() {
        var rightid;
        for (i = 0; i < $('[data-role="panel"]').length; i++) {
            var rtemp = $('[data-role="panel"]:eq(' + i + ')').attr('id');
            if (rtemp.match("right-panel-") == "right-panel-") {
                rightid = rtemp;
            }
        }
        var pageid = $.mobile.activePage.attr('id');
        // alert(pageid);
        $('#' + pageid + ' #' + rightid).panel("open");
    }
    showLoginPanel();
</script>
<?php  }  ?>

<!-- 文字限制 -->
<script>
    var $len = 18; // 超過50個字以"..."取代
    $(".sajalist-label").each(function(){
        if($(this).text().length > $len){
            var $text=$(this).text().substring(0,$len-1)+"...";
            $(this).text($text);
        }
    });
</script>

<!-- animate plugin -->
<script>
    $(document).on('pageshow', function () { //幻燈片banner
        $('.flexslider').flexslider({
            animation: "slide", //圖片切換方式 (滑動)
            slideshowSpeed: 5000, //自動播放速度 (毫秒)
            animationSpeed: 1000, //切換速度 (毫秒)
            directionNav: false, //顯示左右控制按鈕
            controlNav: true, //隱藏下方控制按鈕
            prevText: "", //左邊控制按鈕顯示文字
            nextText: "", //右邊控制按鈕顯示文字
            pauseOnHover: false, //hover時停止播放
            slideshow: true, //自動播放 (debug用)
            touch: true,
            start: function (slider) { //載入第一張圖片時觸發
                $('body').removeClass('loading');
            }
        });
    });

    // 20190507 第二個廣告輪播
    $(window).load(function() {
        $('.adpic2').flexslider({
        animation: "slide", //圖片切換方式 (滑動)
        slideshowSpeed: 5000, //自動播放速度 (毫秒)
        animationSpeed: 1000, //切換速度 (毫秒)
        directionNav: false, //顯示左右控制按鈕
        controlNav: true, //隱藏下方控制按鈕
        prevText: "", //左邊控制按鈕顯示文字
        nextText: "", //右邊控制按鈕顯示文字
        pauseOnHover: false, //hover時停止播放
        slideshow: true, //自動播放 (debug用)
        touch: true,
        start: function (slider) { //載入第一張圖片時觸發
            $('body').removeClass('loading');
        }
        });
    });

    // 圓夢數字跑動 
    $('.counter').counterUp();
</script>

