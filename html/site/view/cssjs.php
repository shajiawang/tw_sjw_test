<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$share_js=_v('share_js');
?>
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/share.min.css" />
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/themes/jquery.mobile.icons.min.css" />
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/jquery.mobile.structure-1.4.3.min.css" />

<!-- Flexslider -->
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/Flexslider/flexslider.css" media="screen" />
<!-- 瀏覽器初始化 -->
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/reset.css">


<!-- EasonChen自定義 -->
<!-- 抓取最新CSS 測試期用 會拖慢效率-->
<?php $css = '/static/css/eason.style.css'; ?>
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?><?php echo $css; ?>?ver=<?php echo filemtime(BASE_DIR.$css); ?>">

<!-- RWD自定義 -->
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/responsive.min.css">

<!-- 第三方登入 -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/hellojs/2.0.0-4/hello.all.js"></script>

<script> var APP_DIR='<?php echo APP_DIR; ?>'; </script>
<!-- body 最后 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/jquery-2.1.4.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/jquery-weui.js"></script>

<!-- 如果使用了某些拓展插件还需要額外的JS -->
<script src="<?PHP echo APP_DIR; ?>/static/js/city-picker.min.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/jquery.mobile-1.4.3.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/jweixin-1.4.0.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/jquery.timers-1.2.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/share.js"></script>

<!-- 驗證 -->
<script src="<?PHP echo APP_DIR; ?>/ajax/auth.js"></script>
<script src="<?PHP echo APP_DIR; ?>/ajax/bid.js"></script>
<script src="<?PHP echo APP_DIR; ?>/ajax/mall.js<?php echo "?t=".$cdnTime; ?>"></script>
<script src="<?PHP echo APP_DIR; ?>/ajax/product.js<?php echo "?t=".$cdnTime; ?>"></script>
<script src="<?PHP echo APP_DIR; ?>/ajax/scode.js"></script>
<script src="<?PHP echo APP_DIR; ?>/ajax/user.js<?php echo "?t=".$cdnTime; ?>"></script>
<script src="<?PHP echo APP_DIR; ?>/ajax/user_register.js<?php echo "?t=".$cdnTime; ?>"></script>
<script src="<?PHP echo APP_DIR; ?>/ajax/enterprise.js"></script>
<!--<script src="<?PHP echo APP_DIR; ?>/ajax/ajax.js"></script>-->

<!-- Flexslider -->
<script src="<?PHP echo APP_DIR; ?>/static/js/Flexslider/modernizr.js"></script>
<script defer src="<?PHP echo APP_DIR; ?>/static/js/Flexslider/jquery.flexslider.js"></script>

<!-- lazyload 2.0.0 懶載入-->
<script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-beta.2/lazyload.js"></script>

<!-- Syntax Highlighter -->
<script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/js/Flexslider/shCore.js"></script>
<script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/js/Flexslider/shBrushXml.js"></script>
<script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/js/Flexslider/shBrushJScript.js"></script>
<!-- Optional FlexSlider Additions -->
<script src="<?PHP echo APP_DIR; ?>/static/js/Flexslider/jquery.easing.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/Flexslider/jquery.mousewheel.js"></script>

<!-- jqm自訂義滑動換頁效果 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/swipe.js<?php echo "?t=".$cdnTime; ?>"></script>

<!-- Flexslider -->

<!-- appbsl SDK-->
<!-- script src="<?PHP echo APP_DIR; ?>/static/js/appbsl_sdk.js"></script -->
<!-- appbsl SDK -->


<!-- lustertech SDK-->
<script src="<?PHP echo APP_DIR; ?>/static/js/lustertech_sdk.js<?php echo "?t=".$cdnTime; ?>"></script>
<!-- lustertech SDK -->

<!--Google Adsense-->
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6411506499383810",
    enable_page_level_ads: true
  });
</script>

<!-- 判斷廣告阻擋用 -->
<!--<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/static/js/advertisement.js"></script>-->

<!-- 域動聯播網 -->
<!--
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/static/js/amodal.js"></script>
<link href="<?php echo BASE_URL.APP_DIR;?>/static/css/amodal.css" rel="stylesheet"/>
-->

<!-- 頁面loading -->
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/static/js/es-page-loading.js"></script>

<!-- reCAPTCHA v2-->
<script src='https://www.google.com/recaptcha/api.js'></script>




