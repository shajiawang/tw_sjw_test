<?php 
$product = _v('product'); 
$status = _v('status');
$meta=_v('meta');
$canbid=_v('canbid');
$cdnTime = date("YmdHis");
if(!isset($canbid)) {
   $canbid=$_REQUEST['canbid'];  
}
if(empty($canbid)) {
   $canbid='Y';
}
//閃殺商品
//$flash_img = ($product['is_flash']=='Y') ? 'class="flash_img"' : '';
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
?>
		<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/css/colorbox.css" />
		<script src="<?PHP echo APP_DIR; ?>/javascript/jquery.colorbox-min.js"></script>
		<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/css/colorbox.css" />
		<script src="<?PHP echo APP_DIR; ?>/javascript/jquery.colorbox-min.js"></script>
		<script src="<?php echo APP_DIR; ?>/static/js/flash_countdown.js<?php echo '?'.$cdnTime; ?>"></script>
		<script>
		var websocket;
		$(document).ready(function(){
			$(".inline").colorbox({inline:true, width:"80%"});
			$(".example").colorbox({iframe:true, width:$(window).width() * 0.8, height: $(window).width() * 0.8 * 0.75});
			//$(".example").colorbox({inline:true, width:"80%"});
			websocket = new WebSocket("ws://121.40.19.35:33333"); 
			websocket.onopen = function(evt) { onOpen(evt) }; 
			websocket.onclose = function(evt) { onClose(evt) }; 
			websocket.onmessage = function(evt) { onMessage(evt) }; 
			websocket.onerror = function(evt) { onError(evt) } 
		});
	</script>
        <!-- <?php echo $_SESSION['auth_id']; ?> -->
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                    <div id="tagBox">
                        
						<div id="tagBoxPlus">

							<?php if (!empty($product['srid']) ){
										if($product['srid']=='any_saja'){
										
										}elseif($product['srid']=='never_win_saja'){ ?>
											<div id="tagBox_qualifications" title="新手限定(未中標过者)" >限新手</div>
										<?php } elseif($product['srid']=='le_win_saja'){ ?>
											<div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以下)" >限新手</div>
										<?php } elseif($product['srid']=='ge_win_saja'){ ?>
											<div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以上)" >限老手</div>
										<?php 
										} 
								  }//endif; 
							?>
							<?php if ((float)$product['saja_fee'] == 0.00){ ?>
								<div id="tagBox_free" title="免費(出價免扣殺價幣)">免費</div>
							<?php }//endif; ?>
						</div>
						
						<?php 
						if(!empty($product['thumbnail'])) { //$status['path_image'] 
							$img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail'];
						} elseif (!empty($product['thumbnail_url'])) {
							$img_src = $product['thumbnail_url']; 
						} 
						?>
						
						<img <?php echo $flash_img; ?> src="<?php echo $img_src; ?>" >
                    </div>
					
					<h2 style="white-space:pre-wrap;"><?php echo $product['name']; ?></h2>
                    <p style="white-space:pre-wrap;">闪殺地點：<?php echo $product['flash_loc']; ?>&nbsp;&nbsp;<?php if (!empty($product['flash_loc_map'])) { ?><a class="example" href="<?php echo $product['flash_loc_map']; ?>"><img src="<?php echo APP_DIR; ?>/images/site/icon/map-icon.png" width="16" height="16"/></a><?php } ?></p>
					<?php if (!empty($product['offtime'])){ ?>
					           <p class="countdown" data-offtime="<?php echo $product['offtime']; ?>" data-tag="">00天00时00分00秒</p>
					<?php }  //endif; ?>
					<?php // if ($_GET['type'] != 'flash') { ?>
					<h2 style="color:red;line-height:23px;">目前中標：<span id="bidded"><?php echo _v('bidded'); ?></span>&nbsp;
					    <a href="#" onclick="autoUpdate();" data-role="button" data-icon="refresh" data-iconpos="notext" style="display:inline;">
					        <span class="ui-btn-inner">
					        	<span class="ui-btn-text">&nbsp;</span>
					        	<span class="ui-icon ui-icon-refresh ui-icon-shadow" />&nbsp;</span>
					        </span>
						</a>					
					</h2>
                    <?php // } //endif; ?>
					<?php  if($_SESSION['auth_id']=='995') {  ?>
							<span id="line-share-button"  style="display:none" >
							   <a href="http://line.me/R/msg/text/?<?php echo $meta['title']."  ".BASE_URL.$_SERVER['REQUEST_URI']; ?>"><img src="<?php echo BASE_URL.APP_DIR; ?>/images<?php echo APP_DIR; ?>/linebutton_en/linebutton_78x20_en.png" width="78px" height="20px" alt="LINE it!" /></a>
							</span>
							<div class="fb-share-button" id="fb-share-button"  style="display:none"
								 data-href="http://test.shajiawang.com/site/product/saja/?productid=<?php echo $product['productid']; ?>" data-layout="button_count">
							</div>
					<?php  } else { //endif; 
					             if (!empty($_SESSION['auth_id'])) {      
					?>
					            <h2><?php echo $_SESSION['user']['profile']['nickname']; ?>, 加油 !!</h2>
					<?php  
					             }
					        } 
					//endif; ?>
                </li>
            </ul>
			
            <div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">     	
			<?php
                if($canbid!='N') {
			?>
			<div data-role="collapsible" >
                    <h2>连續出價</h2>
                    <ul data-role="listview" data-theme="a">
						<li class="ui-field-contain">
                            <input name="bidb" id="bidb" value="" data-clear-btn="true"  pattern="[0-9.]*" step="0.01" placeholder="例:1.00">
                            至
                            <input name="bide" id="bide" value="" data-clear-btn="true"  pattern="[0-9.]*" step="0.01" placeholder="例:2.05">
                        </li>
						<li class="ui-field-contain">   
							<button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="bid_n(<?php echo $product['productid']; ?>)">殺價幣连續出價</button>
							<h4 style="color: red;white-space:normal;">连續出價不提示下標顺位，結標前最后一分钟禁用连續下標</h4>
                        </li>
                    </ul>
                </div>
                <div data-role="collapsible" data-collapsed="false" >
                    <h2>单次出價</h2>
                    <ul data-role="listview" data-theme="a">
                        					
						<li class="ui-field-contain">
                            <input name="bid" id="bid" value="" data-clear-btn="true" pattern="[0-9.]*" step="0.01" placeholder="例:3.02">
                        </li>						
						<li class="ui-field-contain"> 
						    <input type="hidden"  id="radio-1" name="radio-saja" value="oscode" >
							<button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="bid_1(<?php echo $product['productid']; ?>)">单次出價</button>
							<h4 style="color: red;white-space:normal;">結標前最后一分钟不提示下標顺位</h4>
                        </li>
                    </ul>
                </div>
				<?php } ?>
            </div>
        </div><!-- /article -->
		<script type="text/javascript">
		    var chkprodid="<?php echo $product['productid']; ?>";
            var t;
            var URL0 = "<?php echo BASE_URL . APP_DIR .'/product/winner/?timestamp='; ?>" + new Date().getTime();
			 // stop the timer 
			 $(window).on('popstate', function(event) {
				    $.ajaxSetup({ cache: false });
					$(this).stopTime('losttime');
                    			
             });
					
			<?php if($_SESSION['auth_id']==995 || $_SESSION['auth_id']==116) { ?>
			
				$(document).on('pageshow',
				  function(d, s, id) {
					  var js, fjs = d.getElementsByTagName(s)[0];
					  if (d.getElementById(id)) return;
					  js = d.createElement(s); js.id = id;
					  // js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.3&appId=292445974189260";
					  js.src = "//connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v2.3&appId=1638592476360630";
					  fjs.parentNode.insertBefore(js, fjs);
					}(document, 'script', 'facebook-jssdk'));
					
			<?php  }  ?>	
			
			function addpageview(introBy,sMemo) {
			        $.post("<?PHP echo APP_DIR; ?>/ajax/user.php", 
					       {type:"logAff",
						     goto:"<?PHP echo BASE_URL.APP_DIR; ?>/product/saja/?productid=<?php echo $product['productid'] ?>&type=flash",
						     act:"PAGEVIEW",
							 intro_by:introBy,
							 memo:sMemo
						   },
						   function(str_json) {
						       // console && console.log($.parseJSON(str_json));
							   var json=JSON && JSON.parse(str_json) || $.parseJSON(str_json);
							   console && console.log(json);
						   }
					);
					return;
			}

			// call other API to check if it comes from CN
			function showShareButton() {
			         $.get("http://api.hostip.info/country.php", 
					       {ip:"<?php echo $ip; ?>"},
						   function(str) {
						       console.log(str);
							   if(str!='CN') {
							     $('#fb-share-button').show();
								 $('#line-share-button').show();
							   } else {
							     $('#fb-share-button').hide();
								 $('#line-share-button').hide();
							   }
						   }
					);
					return;
			}

            var dataForWeixin={
                appId:"",
                MsgImg:"<?php echo $img_src; ?>",
                TLImg:"",
                url:location.href,
                title:"<?php echo $product['name']; ?> 低價抢殺中!",
                desc:"<?php echo $product['price_limit']; ?>元起 热烈抢殺中，你还琢磨什么?",
                fakeid:"",
                callback:function(){}
            };
            (function(){
                var onBridgeReady=function(){
                    WeixinJSBridge.on('menu:share:appmessage', function(argv){
                        WeixinJSBridge.invoke('sendAppMessage',{
                            "appid":dataForWeixin.appId,
                            "img_url":dataForWeixin.MsgImg,
                            "img_width":"120",
                            "img_height":"120",
                            "link":dataForWeixin.url,
                            "desc":dataForWeixin.desc,
                            "title":dataForWeixin.title
                        }, function(res){(dataForWeixin.callback)();});
                    });
                    WeixinJSBridge.on('menu:share:timeline', function(argv){
                        (dataForWeixin.callback)();
                         WeixinJSBridge.invoke('shareTimeline',{
                             "img_url":dataForWeixin.TLImg,
                             "img_width":"120",
                             "img_height":"120",
                             "link":dataForWeixin.url,
                             "desc":dataForWeixin.desc,
                             "title":dataForWeixin.title
                         }, function(res){});
                    });
                    WeixinJSBridge.on('menu:share:weibo', function(argv){
                         WeixinJSBridge.invoke('shareWeibo',{
                             "content":dataForWeixin.title,
                             "url":dataForWeixin.url
                         }, function(res){(dataForWeixin.callback)();});
                    });
                    WeixinJSBridge.on('menu:share:facebook', function(argv){
                        (dataForWeixin.callback)();
                         WeixinJSBridge.invoke('shareFB',{
                             "img_url":dataForWeixin.TLImg,
                             "img_width":"120",
                             "img_height":"120",
                             "link":dataForWeixin.url,
                             "desc":dataForWeixin.desc,
                             "title":dataForWeixin.title
                         }, function(res){});
                    });
                };
				if(document.addEventListener){
                    document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
                }else if(document.attachEvent){
                    document.attachEvent('WeixinJSBridgeReady'   , onBridgeReady);
                    document.attachEvent('onWeixinJSBridgeReady' , onBridgeReady);
                }
            })();
					
			function onOpen(evt) { 
				autoUpdate();
				doSend('{"CONNECTED":"saja.php !!"}'); 
			}  

			function onClose(evt) { 
                console.log('{"CLOSED":"saja.php !!"}'); 				
			}  

			function onMessage(evt) { 
				var j = JSON && JSON.parse(evt.data) || $.parseJSON(evt.data);
				console.log(j);
				if(j.ACTION=='reload') {
				   websocket.close(); 
				   window.location.reload();
				} 
				if(j.PRODID==chkprodid) {
				   document.getElementById("bidded").innerHTML=json.WINNER;
				}
			}  

			function onError(evt) { 

			}  

			function doSend(message) { 
				if(websocket)
				   websocket.send(message); 
			}   
	</script> 
