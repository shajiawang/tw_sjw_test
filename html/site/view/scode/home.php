<?php //S碼紀錄 
$scode_info = _v('scode_info'); 
$retCode = _v('retCode'); 
?>
<div class="getscodeBox">
    <div id="getscode">
        <div class="input_group">
            <label for="scsn">序號</label>
            <input type="text" name="scsn" id="scsn" value="" placeholder="請輸入序號">
        </div>
        <div class="input_group">
            <label for="scpw">密碼</label>
            <input type="text" name="scpw" id="scpw" value="" placeholder="請輸入密碼">
        </div>
        <div class="g-recaptcha d-flex justify-content-center" data-sitekey="6LfBr4sUAAAAACaH-J1djOFo4Kf8OwWIb7Krco4D" data-callback="recaptcha_callback"></div>
        <div class="footer-btnBar">
            <div class="scode-btn get-btn disabled" onClick="act_scode()">立刻領取</div>
        </div>
    </div>
    
    
    <div id="getscode-confirm" style="display:none">
        <div class="getscode-imgBox">
            <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/supershaja.png" alt="">
        </div>
        <div class="footer-btnBar">
            <div class="scode-btn scode-list-btn">殺價去</div>
        </div>
        <div class="d-flex justify-content-center">
            <ul class="infoBox flex-column d-inlineflex">
                <li>＊此超級殺價券可選擇任一商品進行下標競爭，且無使用期限。</li>
            </ul>
        </div>
    </div>
</div>
<script>
    
//<!-- reCAPTCHA V3 調整 -->
    
//    $(document).ready(function () {
//        function reCAPTCHA_execute () {
//            grecaptcha.execute('6LccrosUAAAAAPcQb3EBHqSkhSmPCBl4yI_ltS69', { action: 'getscode' }).then(function (token) {
//                $('[name="g-recaptcha-response"]').val(token);
//            }, function (reason) {
//                console.log(reason);
//            });
//        }
//
//        if (typeof grecaptcha !== 'undefined' && typeof reCAPTCHA_site_key !== 'undefined') {
//            grecaptcha.ready(reCAPTCHA_execute);
//            setInterval(reCAPTCHA_execute, 60000);
//        }
//    });
//    
//    //調整recaptcha圖示位置
//    $(window).on('load',function(){
//        $('.grecaptcha-badge').css('bottom','54px');
//    });
    
    var $scsn = $('#getscode').find('.input_group #scsn');
    var $scpw = $('#getscode').find('.input_group #scpw');
    
    $(function(){
        $('.sajaNavBarIcon.back .ui-link').hide();           //回上一頁按鈕隱藏
        $('.sajaNavBarIcon.qrcode .ui-link').hide();           //漢堡按鈕隱藏
        <?php if($retCode == '200'){ ?>
            $('#getscode').hide();
            $('#getscode-confirm').show();
        
            $('.scode-btn.scode-list-btn').on('click', function(){
                location.href = '<?php echo BASE_URL.APP_DIR;?>/product/';
            })
        <?php }else{ ?>
            $('#getscode').show();
            $('#getscode-confirm').hide();
        <?php } ?>
        $scsn.on('change keyup', function(){
            changeBtn();
        });
        $scpw.on('change keyup', function(){
            changeBtn();
        });
    });
    
    //google recaptcha 驗證後回調
    function recaptcha_callback(){
        changeBtn();
    }
    function changeBtn(){
        var $recap = $('#g-recaptcha-response').val();
        if($scsn.val()=='' || $scpw.val()=='' || $recap==''){
            $('.scode-btn.get-btn').addClass('disabled');
        }else{
            $('.scode-btn.get-btn').removeClass('disabled');
        }
    }
    //Msg彈窗
    function createMsgModal(msg,href,btnName){
        $('body').addClass('modal-open');
        $('body').append(
            $('<div class="msgModalBox d-flex justify-content-center align-items-center"/>')
            .append(
            $('<div class="modal-content"/>')
                .append(
                $('<div class="modal-body"/>')
                    .append(
                        $('<div class="modal-title d-flex align-items-center justify-content-center"/>')
                        .text(msg)
                    )
                )
                .append(
                    $('<div class="modal-close"/>')
                    .attr('onClick',href)
                    .text(btnName)
                )
            )
            .append(
            $('<div class="msgModalBg">')
            )
        )
        $('.modal-close').on('click', function(){
            $('body').removeClass('modal-open');
            $('.msgModalBox').remove();
        })
    }
</script>


<!-- 舊檔案 -->
<!--
		<div class="article">
            <div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">
                <div data-role="collapsible">
                    <h2>S碼激活</h2>
                    <ul data-role="listview" data-theme="a">
                        <li class="ui-field-contain">
                            <label for="scsn">输入S碼序號</label>
                            <input name="scsn" id="scsn" value="" type="text" >
                        </li>
                        <li class="ui-field-contain">    
                            <label for="scpw">输入S碼密碼</label>
                            <input name="scpw" id="scpw" value="" type="text" >
                        </li>
                        <div class="g-recaptcha d-flex justify-content-center" data-sitekey="6LcECYsUAAAAALHECK8lnCc7P9AwsLn6BQh5xgst"></div>
                        <li class="ui-field-contain">    
                            <button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="act_scode()">確認</button>
                        </li>
                    </ul>
                </div>
            </div>
			
			<table data-role="table" id="table-custom" data-mode="reflow" class="dable-list ui-responsive">
              <thead>
                <tr>
                  <th data-priority="1">现有可用</th>
                  <th data-priority="2">收取總數</th>
                  <th data-priority="3">过期總數</th>
				  <th data-priority="3">已用總數</th>
                </tr>
              </thead>
              <tbody>
				<tr>
                  <td><span style="color:green"><b><?php echo $scode_info['now']; ?></b></span></td>
                  <td><?php echo $scode_info['accept']; ?></td>
                  <td><?php echo -$scode_info['expired']; ?></td>
				  <td><?php echo $scode_info['used']; ?></td>
                </tr>
              </tbody>
            </table>
        </div> /article 
-->
		