<?php //S碼-收取明细
$row_list = _v('row_list'); 
?>
		<div class="article">
            <table data-role="table" id="table-custom" data-mode="reflow" class="dable-list ui-responsive">
              <thead>
                <tr>
                  <th data-priority="1">活動名称</th>
                  <th data-priority="2">赠送组數</th>
                  <th data-priority="3">赠送日期</th>
				  <th data-priority="4">到期日期</th>
				  <th data-priority="5">备注</th>
                </tr>
              </thead>
              <tbody>
                <?php 
				if(!empty($row_list['table']['record']) ) {
				foreach($row_list['table']['record'] as $rk => $rv){
				
				if($rv['behav']=='b') {
					$memo = '分享连结'; //$rv['memo'];
				} elseif($rv['behav']=='c') {
					$memo = $rv['memo'];
				} elseif($rv['behav']=='d') {
					$memo = '系统发送'; //$rv['memo'];
				} else {
					$memo = 'S碼序號:'. $rv['memo'];
				}
				?>
				<tr>
                  <td><?php echo $rv['spname']; ?></td>
                  <td><?php echo $rv['amount']; ?></td>
                  <td><?php echo date("Y-m-d", strtotime($rv['insertt']) ); ?></td>
				  <td><?php echo date("Y-m-d", strtotime("+30 day ". $rv['insertt']) ); ?></td>
				  <td><?php echo $memo; ?></td>
                </tr>
				<?php } } else { ?>
				<tr>
                  <td> </td>
                  <td> </td>
                  <td> </td>
				  <td> </td>
                  <td> </td>
                </tr>
				<?php }//endif; ?>
                
              </tbody>
            </table>
			
			<?php 
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content'); 
			
			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : ''; 
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
			?>
			<div>
				<div class="ui-grid-a">
                    <div class="ui-block-a <?php echo $arrow_l_able; ?>">
					<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
					</div>
                    <div class="ui-block-b <?php echo $arrow_r_able; ?>">
					<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
					</div>
                </div>
                <?php /*<form>
					<select name="select-native-1" id="change-page">
					<?php echo $page_content['change']; ?>
                    </select>
                </form>*/?>
            </div>
			<?php } ?>
			
        </div><!-- /article -->
		