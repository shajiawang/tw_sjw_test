<?php
    $header = _v('header');
    if($header=='N') {
        return;
    }
    $noback = _v('noback');
?>	
<?php if (!is_weixin()) { ?>
    <div class="navbar sajaNavBar login-page d-flex align-items-center">
		<div class="sajaNavBarIcon back">
            <?php if (empty($noback) && $noback != "Y") { ?>
            <a href="javascript:history.back();" >
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
            </a>
            <?php } ?>
        </div>
		<div class="navbar-brand">
			<h3><?php if (!empty($tpl->title)){ echo $tpl->title; } else { echo $tpl->page_header;} ?></h3>
		</div>
		<div class="sajaNavBarIcon qrcode">
			<a href="#left-panel<?php get_panel_id(); ?>" data-iconpos="right">
				<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/hamburger.png">
			</a>
        </div>
	</div>
    <script>
        // 不是在微信  也不是在appbsl的app裏  才顯示Navigator bar

        $(document).ready(function() {
            if(is_kingkr_obj()) {
                // controllNavigateLayout(0);
            }
            //if(!is_kingkr_obj()) {
            $('#navibar').show();
            //}
        });
    </script>
    <!-- 往下捲動後 header 增加陰影 -->
    <script>
        $(document).ready(function() {
            var $header = $(".sajaNavBar"),
                $lastScrollY = $header.height();
            $(window).scroll(function(){
                var st = this.scrollY;
                // 捲動超過 header 才加陰影
                if( st < $lastScrollY) {
                    $header.removeClass("navShadow");
                }else{
                    $header.addClass("navShadow");
                }
            });
        });
        $(window).on('load',function(){
        //側邊攔
            var $panelBtn = $('.sajaNavBarIcon.Burger').find('a[href^="#left-panel"]');
            var $panelBox = $('.sajaPanel');
            var $panelClose = $('.sajaPanel').find('[data-rel="close"]');
            var $coverClose = $('.ui-panel-dismiss');
            $panelBtn.on('click', function(){
                if(!$panelBox.hasClass('.ui-panel-open')){
                    $('body').addClass('modal-open');
                    $('[data-role="page"]').find('[class$="-content"]').addClass('modal-open');
                }
            });
            $panelClose.on('click', function(){
                if(!$panelBox.hasClass('.ui-panel-closed')){
                    $('body').removeClass('modal-open');
                    $('[data-role="page"]').find('[class*="modal-open"]').removeClass('modal-open');
                }
            });
            $coverClose.on('click', function(){
                if(!$panelBox.hasClass('.ui-ui-panel-closed')){
                    $('body').removeClass('modal-open');
                    $('[data-role="page"]').find('[class*="modal-open"]').removeClass('modal-open');
                }
            })
        })
    </script>
<?php } ?>
