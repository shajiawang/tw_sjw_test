<?php
$flash_loc = _v('flash_loc');
$product_id = _v('productid');
?>

<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/keyboard.min.css">
<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">

<!-- 閃殺CSS -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/instantkill.min.css">

<div id="instant-box" class="userbid">
    <div class="container">
        <!-- 商品資訊 -->
        <div class="productSaja-imgbox">
            <div class="productSaja-imgbox">
                <div class="clearfix sajaprod-img">
                    <ul class="slides d-flex">
                        <li class="d-flex align-items-center">
                            <img class="img-fluid product-img" src="">
                        </li>
                    </ul>
                </div>
                
                <h4 class="sajaprod-name"></h4>
                <p class="market-price-box">原價： <span class="market-price"></span></p>
            </div>
        </div>
        
        <div class="bind-container">
            <!-- 下標紀錄 -->
            <div class="user-bind-history">
                <div class="bind-history-title">下標紀錄</div>
                <div class="bind-history-table">
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">555555555555</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                </div>
            </div>

            <div class="user-bind-title"><span class="user-name">肥仔</span>，請出價</div>

            <div class="bind-inputBox">
                <input type="text" name="price" id="price" class="keyboard allinput-type2" min="1" pattern="[0-9]*" step="1" placeholder="請輸入要下標的金額">
                <div class="bind-unit">元</div>
            </div>
            <div class="feedback"></div>
            
            
            <div class="footer-btn">
                <div class="btn-item bind-btn">
                    <button class="btn-item-a" disabled>下標</button>
                </div>
                <div class="btn-item next-btn">
                    <button class="btn-item-a">下一檔</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  CDN Bootstrap 4.1.1  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--  鍵盤-自訂義JS  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/js/keyboard.js"></script>

<!--  小鍵盤動作  -->
<script>
    options.dom.caller = '.keyboard';                   //命名:呼叫小鍵盤的物件
    options.dom.keyidName = 'numkeyModal';              //命名:小鍵盤(無id符號)
    options.dom.keyModal = '#' + options.dom.keyidName; //命名:小鍵盤(有id符號)
    options.dom.callerId = '#price';                    //預設一開始的呼叫者,在keyboard.JS會做變更 點擊者
    options.btn.sure = '確定';                           //小鍵盤送出按鈕名稱
    
    $(function(){
        $(options.dom.caller).each(function(i){
            var $modalNum = options.dom.keyidName+i;
            //console.log($modalNum);
            var $that = $(this);
            $that.on("click",function(e){
                options.dom.callerId = $that;                           //記錄點擊哪個input的ID
                //開啟小鍵盤的動作
                //若預設開啟時，此段不會執行
                $('#'+$modalNum).on('show.bs.modal', function (e) {
                    controlSize($that);
                });
                
                //關閉小鍵盤的動作
                $('#'+$modalNum).on('hide.bs.modal', function (e) {
                    var $callerId = $(options.dom.callerId);            //取得按鈕ID
                    var $feedback = $('.feedback');                     //驗證msg
                    var $callerVL = $callerId.val().length;
                    
                    //console.log($callerVL)
                    if($callerVL < 1) {
                        $(esUserbidOptions.btn.bind).attr('disabled',true);
                    }
                    else if($callerVL > 10){
                        $callerId.val($callerId.val().substr(0,10));
                        $feedback.text('限制 最多10個字元');
                        $(esUserbidOptions.btn.bind).attr('disabled',true);
                    }else{
                        $feedback.text('');
                        $(esUserbidOptions.btn.bind).attr('disabled',false);
                    }
                    
                    //移除 將輸入框上推的方塊
                    removeBlank();
                });
            })
        });
//        setTimeout(function(){
//            $(options.dom.callerId).click();                //小鍵盤預設開啟 (類似input的focus)
//        },200);
    })

</script>

<script>
    var esUserbidOptions = {
        dom: {
            productImg: '.product-img',
            productName: '.sajaprod-name',
            productPrice: '.market-price',
            downtime: '.countdown-num',
            bindHistory: '.bind-history-table'
        },
        getData: {
            productImgUrl: '',
            productName: '',
            productPrice: '',
            QRcodeUrl: '',
        },
        input: {
            bind: '#price'
        },
        btn: {
            bind: '.footer-btn .bind-btn .btn-item-a',
            next: '.footer-btn .next-btn .btn-item-a'
        }
    }
    $(function(){
        esUserbidOptions.nopicText = '暫無閃殺商品';
        esUserbidOptions.nopicUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';          //無資料時顯示

        $('.sajaNavBar').addClass('d-flex').show();
        $('.sajaNavBar').find('h3').text('閃殺下標');
        $('.sajaNavBar').find('.sajaNavBarIcon').hide();
        $('.other-content').removeClass('instant-content').addClass('other-content').css({'padding-top':'3.583rem','min-height':'calc(100vh - 3.583rem)'});
        
        //撈取資料
//        getInstantData_user(function($hasInstant){
//            if($hasInstant){         //有殺品時，執行此行
//
//            }else{                  //無殺品時，執行此行
//                var $nodataText = esUserbidOptions.nopicText;
//                $('#instant-box').html(
//                    $('<div/>')
//                    .addClass('nohistory-box')
//                    .append(
//                        $('<div/>')
//                        .addClass('nohistory-img')
//                        .append(
//                            $('<img/>')
//                            .addClass('img-fluid')
//                            .attr('src',esUserbidOptions.nopicUrl)
//                        )
//                    )
//                    .append(
//                        $('<div/>')
//                        .addClass('nohistory-txt')
//                        .text($nodataText)
//                    )
//                )
//                
//            }
//        });
        
        getInstantData();           //取得閃殺資訊
        getbidData_user();          //取得下標紀錄
        
        //按鈕群
        $(esUserbidOptions.btn.bind).on('click', function(){
            //開啟確認框
            if($('body').find('.check-modal').length <= 0){
                $('body').append(
                    $('<div/>')
                    .addClass('check-modal')
                    .append(
                        $('<div/>')
                        .addClass('check-body')
                        .append(
                            $('<div/>')
                            .addClass('check-title')
                            .text('下標金額')
                        )
                        .append(
                            $('<div/>')
                            .addClass('check-price')
                            .append(
                                $('<span/>')
                                .text('100')
                            )
                            .append(
                                $('<span/>')
                                .text('元')
                            )
                        )
                        .append(
                            $('<div/>')
                            .addClass('check-btn-group d-flex')
                            .append(
                                $('<button/>')
                                .addClass('close-btn')
                                .text('取消')
                            )
                            .append(
                                $('<button/>')
                                .addClass('ok-btn')
                                .text('確定')
                            )
                        )
                    )
                    .append(
                        $('<div/>')
                        .addClass('check-bg')
                    )
                )
                $('body').addClass('modal-open');
                
                $('.close-btn').on('click', function(){
                    $('body').find('.check-modal').remove();
                    $('body').removeClass('modal-open');
                    $('#price').val('');
                    $(esUserbidOptions.btn.bind).attr('disabled',true);
                });
                
                $('.ok-btn').on('click', function(){
                    $('body').find('.check-modal').remove();
                    $('body').removeClass('modal-open');
                    $('#price').val('');
                    $(esUserbidOptions.btn.bind).attr('disabled',true);
                });
                
            }
        })
        $(esUserbidOptions.btn.next).on('click', function(){
            //判斷目前閃殺商品是否換檔
        })
    })
    
//取得閃殺資訊
    function getInstantData() {
        var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/instantkill/show_room/';

        var formData = new FormData();
        formData.append('flash_loc','<?php echo $flash_loc ?>');
        formData.append('json','Y');

        $.ajax({
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            data: formData,
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,
            success: showResponse
        });

        function showResponse(data){
            //console.log(data);
            if(data['retCode'] == '1'){
                //更換商品圖片
                if(data['thumbnail_url'] !== ''){
                    esUserbidOptions.getData.productImgUrl = data['thumbnail_url'];
                }else{
                    $(esUserbidOptions.dom.productImg).parent('li').css({'min-height':'63.3vmin','min-width':'100%'});
                    $(esUserbidOptions.dom.productImg).parent('li').append(
                        $('<div/>')
                        .addClass('noproductImg')
                        .text('no image')
                    )
                }
                $(esUserbidOptions.dom.productImg).attr('src',esUserbidOptions.getData.productImgUrl);

                //更換商品名稱
                if(data['product_name'] !== ''){
                    esUserbidOptions.getData.productName = data['product_name'];
                }else{
                    esUserbidOptions.getData.productName = ' ';
                }
                $(esUserbidOptions.dom.productName).text(esUserbidOptions.getData.productName);

                if(data['retail_price'] ==''){
                    esUserbidOptions.getData.productPrice = parseInt(data['retail_price']);
                    $(esUserbidOptions.dom.productPrice).text(esUserbidOptions.getData.productPrice);
                }else{
                    $('.market-price-box').hide();
                    $('.market-price-box').siblings('.sajaprod-name').css('padding-bottom','1rem');
                }
                
            }else if(data['retCode'] == '-1'){
                alert(data['retMsg']);
                feedbackModal(data['retMsg'],'確定');
                
            }else if(data['retCode'] == '-2'){
                alert(data['retMsg']);
                feedbackModal(data['retMsg'],'確定');
            }

        }

        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }

        //加載錯誤訊息
        function showFailure() {
        }
    }
    
//取得下標紀錄
    function getbidData_user() {
        
        var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/instantkill/get_user_bid_record/';
        
        var formData = new FormData();
        formData.append('productid','<?php echo $product_id ?>');
        formData.append('json','Y');
        
        $.ajax({  
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            data: formData,
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,  
            success: showResponse
        });

        function showResponse(data){
            console.log(data);
        }
        
        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }
        
        //加載錯誤訊息
        function showFailure() {
        }
    }
    
    //更改DOM資訊，之後用 ajax的success取代
//    function showInstantData_user() {
//        var $downtime = esUserbidOptions.getData.downtime;                              
//        $(esUserbidOptions.dom.downtime).attr('data-countdowm',$downtime);                          //儲存data 毫秒數
//        mSecondeChange($downtime,function($minute,$second,$msecond){                                //換算分秒
//            $(esUserbidOptions.dom.downtime).text($minute+':'+$second);
//        })
//    }
    var $timeCountDown;
    
    //毫秒 > 換算
    function mSecondeChange(time,callback) {
        var $downtime = time;
        var $minute = parseInt($downtime / 60 / 1000);
        var $second = parseInt(($downtime - $minute * 60 * 1000) / 1000);
        var $msecond = $downtime - $minute * 60 * 1000 - $second * 1000;
        
        //秒--補零
        if($second < 10){
            $second = '0'+String($second);
        }
        
        //毫秒--補零
        if($msecond < 10){
            $msecond = '00'+String($msecond);
        }else if($msecond < 100){
            $msecond = '0'+String($msecond);
        }
        
        callback($minute,$second,$msecond);
    }
    
//訊息視窗，含按鈕
    function feedbackModal($msg,$btnName,$url) {
        if($('body').find('.feedback-modal').length <= 0){
            $('body').append(
                $('<div/>')
                .addClass('feedback-modal')
                .append(
                    $('<div/>')
                    .addClass('feedback-txt')
                    .text($msg)
                )
                .append(
                    $('<button/>')
                    .addClass('feedback-btn')
                    .text($btnName)
                )
            )
            .append(
                $('<div/>')
                .addClass('feedback-bg')
            )
             $('body').addClass('modal-open');
                    
            $('.feedback-btn').on('click', function(){
                if($url !== ''){                //有值跳轉，沒有則重整頁面
                    location.href = $url;
                }else{
                    location.reload();
                }
            })
        }
    }
</script>
