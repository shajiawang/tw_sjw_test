<?php
$location = _v('locatios');
$authid = _v('auth_id');
$date = _v('date');
//return;
?>

<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">

<!-- 閃殺CSS -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/instantkill.min.css">
   
<div id="instant-box" class="choice">
    <div class="container">
<!--        <div class="title">場次</div>-->
        <div class="instant-select-box">
            <!-- ajax生成 -->
        </div>
    </div>
</div>

<script>
    var esChoiceOptions = {
        dom: {
            parent: '.instant-select-box'
        },
        class: {
            item: 'select-item'
        },
        getData: {
            date: '',
            locations: '',
        }
    }
    $(function(){
        esChoiceOptions.nopicText = '暫無閃殺場次';
        esChoiceOptions.nopicUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';          //無資料時顯示
        
        $('.sajaNavBar').addClass('d-flex').show();
        $('.sajaNavBar').find('h3').text('主持閃殺')
        $('.other-content').removeClass('instant-content').addClass('other-content');
        
        //撈取資料
        esloading(1);
        getChoiceData();
    })
    
    //畫面一開啟 取得商品圖、商品名稱、倒數時間、QRcode
    function getChoiceData() {
        var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/instantkill/choice_location/';

        var formData = new FormData();
        formData.append('authid','<?php echo $authid ?>');
        formData.append('json','Y');
//        formData.append('date','2018-11-22');
        
        $.ajax({  
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            data: formData,
            dataType: 'json',
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,  
            success: showResponse
        });

        function showResponse(data){
            if(data['retCode'] == '1'){
                //console.dir(data);
                esChoiceOptions.getData.date = data['date'];                //場次時間
                esChoiceOptions.getData.locations = data['locatios'];       //地點
                var $href;

                if(!(esChoiceOptions.getData.locations == null || esChoiceOptions.getData.locations == undefined || esChoiceOptions.getData.locations == '')){
                    $.each(esChoiceOptions.getData.locations,function(i,item){
                        $(esChoiceOptions.dom.parent)
                        .append(
                            $('<a/>')
                            .addClass(esChoiceOptions.class.item)
                            .attr('href','<?php echo BASE_URL.APP_DIR;?>/instantkill/show_room/?flash_loc='+item['flash_loc'])
                            .text(item['flash_loc'])
                        )
                    })
                }else{              //無資料時顯示
                    $('#instant-box').html(
                        $('<div/>')
                        .addClass('nohistory-box')
                        .append(
                            $('<div/>')
                            .addClass('nohistory-img')
                            .append(
                                $('<img/>')
                                .addClass('img-fluid')
                                .attr('src',esChoiceOptions.nopicUrl)
                            )
                        )
                        .append(
                            $('<div/>')
                            .addClass('nohistory-txt')
                            .text(esChoiceOptions.nopicText)
                        )
                    )
                }
            }else{
                alert(data['retMsg']);
            }
        }
        
        //加載動作完成 (不論結果success或error)
        function endLoading() {
            esloading(0);
        }
        
        //加載錯誤訊息
        function showFailure() {
        }
    }

</script>
