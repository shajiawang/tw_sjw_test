<?php
$flash_loc = _v('flash_loc');
$product_id = _v('productid');
?>

<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">

<!-- 閃殺CSS -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/instantkill.min.css">

<div id="instant-box" class="final">
    <div class="container">
        <div class="winner-imgBox">
            <img class="winner-img" src="<?php echo BASE_URL.APP_DIR;?>/static/img/order-default.png" alt="">
        </div>
        <div class="winner-txt">
            <div class="txt-1">恭喜<span class="winner-name"></span><span class="winner-id">(<span class="id"></span>)</span></div>
            <div class="txt-2">以<span class="winner-price"></span>元</div>
            <div class="txt-3">得標</div>
        </div>
        <div class="footer-btn">
            <a class="next-btn" href="javascript:void(0)">下一檔</a>
        </div>
    </div>

</div>


<script>
    var esfinalOptions = {
        dom: {
            winnerName: '.winner-name',
            winnerId: '.winner-id .id',
            winnerImg: '.winner-img',
            winnerPrice: '.winner-price'
        },
        getData: {
            winnerName: '',
            winnerUserId: '',
            thumbnail_file: '',
            thumbnail_url: '',
            price: '',
        },
        nodata: {
            winnerImg: '',
        }
    };
    
    $(function(){
        $('#instant-box .container').hide();
        esfinalOptions.nopicText = '暫無閃殺商品';
        esfinalOptions.nopicUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';          //無資料時顯示

        $('.sajaNavBar').removeClass('d-flex').hide();
        $('.other-content').removeClass('other-content').addClass('instant-content');
        
        //取得ajax資料
        getFinalWinner();
    });
    
    //ajax 戳 最終得標者資料
    function getFinalWinner() {
        esfinalOptions.nodata.winnerImg = '<?php echo BASE_URL.APP_DIR;?>/static/img/order-default.png';      //目前得標者沒照片時顯示預設
        
        var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/instantkill/get_winner_final/';
        
        var formData = new FormData();
        formData.append('productid','<?php echo $product_id ?>');
        formData.append('json','Y');
        
        $.ajax({
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            data: formData,
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,
            success: showResponse
        });
        
        function showResponse(data){
            //console.log(data);
            if(data['retCode'] == '1'){         
                $('#instant-box .container').show();
                esfinalOptions.getData.winnerName = data['nickname'];
                esfinalOptions.getData.winnerUserId = data['userid'];
                esfinalOptions.getData.thumbnail_file = '<?php echo BASE_URL.APP_DIR;?>/images/headimgs/'+data['thumbnail_file'];
                esfinalOptions.getData.thumbnail_url = data['thumbnail_url'];
                esfinalOptions.getData.price = parseInt(data['price']);
                
                //替換--目前得標者名稱
                $(esfinalOptions.dom.winnerName).text(esfinalOptions.getData.winnerName);
                
                //替換--目前得標者id
                $(esfinalOptions.dom.winnerId).text(esfinalOptions.getData.winnerUserId);
                
                //替換--目前得標金額
                $(esfinalOptions.dom.winnerPrice).text(esfinalOptions.getData.price);
                
                //替換--目前得標者大頭照
                if(esfinalOptions.getData.thumbnail_file !== ''){
                    $(esfinalOptions.dom.winnerImg).attr('src',esfinalOptions.getData.thumbnail_file);
                }else if(esfinalOptions.getData.thumbnail_url !== ''){
                    $(esfinalOptions.dom.winnerImg).attr('src',esfinalOptions.getData.thumbnail_url);
                }else{
                    $(esfinalOptions.dom.winnerImg).attr('src',esfinalOptions.nodata.winnerImg);
                }
                
                //設定下一檔連結路徑
                $('.footer-btn .next-btn').on('click', function(){
                    location.href = '<?php echo BASE_URL.APP_DIR;?>/instantkill/show_room/?flash_loc=<?php echo $flash_loc; ?>';
                })
            }else if(data['retCode'] == '-2'){                              //查無結標紀錄
                var $goUrl = '<?php echo BASE_URL.APP_DIR; ?>/instantkill/show_room/?flash_loc=<?php echo $flash_loc; ?>';
                feedbackModal(data['retMsg'],'回閃殺大廳',$goUrl);
            }else if(data['retCode'] == '-3'){                              //結標紀錄有誤 非最低唯一
                var $goUrl = '<?php echo BASE_URL.APP_DIR; ?>/instantkill/show_room/?flash_loc=<?php echo $flash_loc; ?>';
                feedbackModal(data['retMsg'],'回閃殺大廳',$goUrl);
            }
        }

        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }

        //加載錯誤訊息
        function showFailure() {
        }
    };
    
    //訊息視窗，含按鈕
    function feedbackModal($msg,$btnName,$url) {
        if($('body').find('.feedback-modal').length <= 0){
            $('body').append(
                $('<div/>')
                .addClass('feedback-modal')
                .append(
                    $('<div/>')
                    .addClass('feedback-txt')
                    .text($msg)
                )
                .append(
                    $('<button/>')
                    .addClass('feedback-btn')
                    .text($btnName)
                )
            )
            .append(
                $('<div/>')
                .addClass('feedback-bg')
            )
             $('body').addClass('modal-open');
                    
            $('.feedback-btn').on('click', function(){
                if($url !== ''){                //有值跳轉，沒有則重整頁面
                    location.href = $url;
                }else{
                    location.reload();
                }
            })
        }
    }
</script>
