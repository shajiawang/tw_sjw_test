
<?php
$thumbnail_url = _v('thumbnail_url');
$product_name = _v('product_name');
$product_id = _v('productid');
$retail_price = _v('retail_price');
$time_limit = _v('time_limit');
$qr_value = _v('qr_value');
$now_time = _v('now_time');
$total_product_count = _v('total_product_count');
$flash_loc = _v('flash_loc');
//return;
?>

<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">

<!-- 閃殺CSS -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/instantkill.min.css">

<div id="instant-box" class="show">
    <div class="container">
        <div class="instant-title">殺價王閃殺</div>

        <div class="instant-header">
            <div class="winner-box">
                <div class="win-title">目前得標者</div>
                <div class="winner d-flex align-items-center">
                    <img class="winner-img" src="<?php echo BASE_URL.APP_DIR;?>/static/img/order-default.png" alt="">
                    <div class="winner-name-box d-flex flex-column">
                        <div class="winner-name">無得標者</div>
                        <div class="winner-id">(<span class="id"></span>)</div>
                   </div>
                </div>
            </div>
            <div class="countdown-box d-flex align-items-center justify-content-center">
                <span class="count-title">結標倒數:</span><span class="countdown-num"></span>
            </div>
            <div class="header-box d-flex align-items-center justify-content-around">
                <div class="instant-qrcode">
                    <img class="img-fluid qrcode-img" src="" alt="">
                </div>
                <div class="instant-product">
                    <img class="img-fluid product-img" src="" alt="">
                </div>
                <div class="instant-qrcode">
                    <img class="img-fluid qrcode-img" src="" alt="">
                </div>
            </div>
        </div>

        <div class="instant-footer">
            <div class="product-info d-flex">
                <div class="product-name mr-auto"></div>
                <div class="market-price-box">市價: <span class="market-price"></span></div>
            </div>

<!--
            <div class="footer-btn ml-auto">
                <button class="instant-go-btn">一起搶殺去</button>
            </div>
-->
        </div>

        <div class="timeController d-flex align-items-center justify-content-center">
            <div class="paly-btn d-flex align-items-center">
                <i class="esfas gcolor play"></i>
            </div>
            <div class="pause-btn d-flex align-items-center">
                <i class="esfas pcolor pause disabled"></i>
            </div>
            <div class="end-btn">結標</div>
        </div>
    </div>
</div>

<script>
    var esinstantOptions = {
        dom: {
            productImg: '.product-img',
            productName: '.product-name',
            productPrice: '.market-price',
            QRcodeImg: '.qrcode-img',
            downtime: '.countdown-num',
            winnerName: '.winner-name',
            winnerId: '.winner-id .id',
            winnerImg: '.winner-img',
        },
        getData: {
            productImgUrl: '',
            productName: '',
            productPrice: '',
            downtime: '',
            QRcodeUrl: '',
            total_count: '',
            winnerName: '',
            winnerUserId: '',
            thumbnail_file: '',
            thumbnail_url: '',
        },
        nodata: {
            winnerImg: '',
        }
    }

    $(function(){
        esinstantOptions.nopicText = '暫無閃殺商品';
        esinstantOptions.nopicUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';          //無資料時顯示

        $('.sajaNavBar').removeClass('d-flex').hide();
        $('.other-content').removeClass('other-content').addClass('instant-content');

        $('.pause-btn').css({'pointer-events':'none'}).find('i').addClass('disabled');

    //生成頁面資料
        showInstantData();

    //控制列按鈕群
        $('.paly-btn').on('click',function(){
            startCountDown();           //開始倒數
            startGetWinner();           //開始抓目前得標者
        })
        $('.pause-btn').on('click',function(){
            pauseCountDown();           //停止倒數
            stopGetWinner();            //停止抓目前得標者
        })
        $('.end-btn').on('click',function(){
            stopCountDown();
//            getWinnerData();            //最後抓一次目前得標者
            stopGetWinner();            //停止抓目前得標者
        })

        //放大QRcode
        $('.instant-qrcode').on('click',function(){
            //自動暫停
            $('.pause-btn').click();
            zoomQRcodeUp();
        })

//替代方案 用ajax戳閃殺資訊
//        getInstantData();
                        //撈取資料
//        getInstantData(function($hasInstant){
//            console.log($hasInstant)
//            if($hasInstant){         //有殺品時，執行此行
//
//                $('.paly-btn').on('click',function(){
//                    startCountDown();
//                })
//                $('.pause-btn').on('click',function(){
//                    pauseCountDown();
//                })
//                $('.end-btn').on('click',function(){
//                    stopCountDown();
//                })
//
//                //放大QRcode
//                $('.instant-qrcode').on('click',function(){
//                    //自動暫停
//                    $('.pause-btn').click();
//                    zoomQRcodeUp();
//                })
//            }else{                  //無殺品時，執行此行
//                var $nodataText = esinstantOptions.nopicText;
//                $('#instant-box').html(
//                    $('<div/>')
//                    .addClass('nohistory-box')
//                    .append(
//                        $('<div/>')
//                        .addClass('nohistory-img')
//                        .append(
//                            $('<img/>')
//                            .addClass('img-fluid')
//                            .attr('src',esinstantOptions.nopicUrl)
//                        )
//                    )
//                    .append(
//                        $('<div/>')
//                        .addClass('nohistory-txt')
//                        .text($nodataText)
//                    )
//                )
//
//            }
//        });

    })

//用ajax 戳閃殺資料
    function getInstantData() {
//        esinstantOptions.getData.productImgUrl = '<?php echo $thumbnail_url ?>';
//        esinstantOptions.getData.productName = '<?php echo $product_name ?>';
//        esinstantOptions.getData.productPrice = '<?php echo $retail_price ?>';
//
//        var $downtimeMin = '<?php echo $time_limit ?>';    //秒
//        var $timeToMSecond = $downtimeMin * 1000;          //轉換毫秒
//        esinstantOptions.getData.downtime = $timeToMSecond;
//        esinstantOptions.getData.QRcodeUrl = '/site/phpqrcode/?data=<?php echo $qr_value ?>';
//
//        $hasInstant = ('<?php echo $total_product_count ?>' > 0) ? true : false;  //判斷有無下標商品
//        callback($hasInstant); //回傳判斷

        var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/instantkill/show_room/';

        var formData = new FormData();
        formData.append('flash_loc','<?php echo $flash_loc ?>');
        formData.append('json','Y');

        $.ajax({
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            data: formData,
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,
            success: showResponse
        });

        function showResponse(data){
            //console.log(data);
        }

        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }

        //加載錯誤訊息
        function showFailure() {
        }

        showInstantData();
    }

//用ajax 戳得標者資料
    function getWinnerData() {
        esinstantOptions.nodata.winnerImg = '<?php echo BASE_URL.APP_DIR;?>/static/img/order-default.png';      //目前得標者沒照片時顯示預設

        var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/instantkill/get_winner_now/';

        var formData = new FormData();
        formData.append('productid','<?php echo $product_id ?>');
        formData.append('json','Y');

        $.ajax({
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            data: formData,
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,
            success: showResponse
        });

        function showResponse(data){
            console.log(data['retCode'])
            if(data['retCode'] == '1'){
                esinstantOptions.getData.winnerName = data['nickname'];
                esinstantOptions.getData.winnerUserId = data['userid'];
                esinstantOptions.getData.thumbnail_file = '<?php echo BASE_URL.APP_DIR;?>/images/headimgs/'+data['thumbnail_file'];
                esinstantOptions.getData.thumbnail_url = data['thumbnail_url'];

                //替換--目前得標者名稱
                $(esinstantOptions.dom.winnerName).text(esinstantOptions.getData.winnerName);

                //替換--目前得標者id
                $('.winner-id').show();
                $(esinstantOptions.dom.winnerId).text(esinstantOptions.getData.winnerUserId);

                //替換--目前得標者頭貼
                if(esinstantOptions.getData.thumbnail_file !==''){
                    $(esinstantOptions.dom.winnerImg).attr('src',esinstantOptions.getData.thumbnail_file);
                }else if(esinstantOptions.getData.thumbnail_url !==''){
                    $(esinstantOptions.dom.winnerImg).attr('src',esinstantOptions.getData.thumbnail_url);
                }else{
                    $(esinstantOptions.dom.winnerImg).attr('src',esinstantOptions.nodata.winnerImg);
                }
            }else if(!data['retCode'] == '-2'){
                //預設--目前無得標者名稱 顯示
                $(esinstantOptions.dom.winnerName).text('無得標者');

                //預設--目前無得標者頭貼 顯示
                $(esinstantOptions.dom.winnerImg).attr('src',esinstantOptions.nodata.winnerImg);
            }
        }

        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }

        //加載錯誤訊息
        function showFailure() {
        }
    }

        //取得目前得標者資訊
        var periodic;
        function startGetWinner() {
            periodic = setInterval(function(){
                getWinnerData();
            },2000);
        }
        function stopGetWinner() {
            clearInterval(periodic)
        }

//依頁面資料更改資料
    function showInstantData() {
        esinstantOptions.getData.productImgUrl = '<?php echo $thumbnail_url ?>';
        esinstantOptions.getData.productName = '<?php echo $product_name ?>';
        esinstantOptions.getData.productPrice = parseInt('<?php echo $retail_price ?>');

        var $downtimeMin = '<?php echo $time_limit ?>';    //秒
        var $timeToMSecond = $downtimeMin * 1000;          //轉換毫秒
        esinstantOptions.getData.downtime = $timeToMSecond;
        esinstantOptions.getData.QRcodeUrl = '/site/phpqrcode/?data=<?php echo $qr_value ?>';

        esinstantOptions.getData.total_count = '<?php echo $total_product_count ?>';

        if(esinstantOptions.getData.total_count > 0){
            $(esinstantOptions.dom.productImg).attr('src',esinstantOptions.getData.productImgUrl);
            $(esinstantOptions.dom.QRcodeImg).each(function(){
                var $that = $(this);
                $that.attr('src',esinstantOptions.getData.QRcodeUrl);
            });
            $(esinstantOptions.dom.productName).text(esinstantOptions.getData.productName);
            $(esinstantOptions.dom.productPrice).text(esinstantOptions.getData.productPrice);

            var $downtime = esinstantOptions.getData.downtime;
            $(esinstantOptions.dom.downtime).attr('data-countdowm',$downtime);                          //儲存data 毫秒數
            mSecondeChange($downtime,function($minute,$second,$msecond){                                //換算分秒
                $(esinstantOptions.dom.downtime).text($minute+':'+$second);
            })
        }else{
            $('#instant-box').html(
                $('<div/>')
                .addClass('nohistory-box')
                .append(
                    $('<div/>')
                    .addClass('nohistory-img')
                    .append(
                        $('<img/>')
                        .addClass('img-fluid')
                        .attr('src',esinstantOptions.nopicUrl)
                    )
                )
                .append(
                    $('<div/>')
                    .addClass('nohistory-txt')
                    .text(esinstantOptions.nopicText)
                )
            )
        }
    }

//時間控制函式群
    var $timeCountDown;
    function startCountDown() {
        var $timedownDom = $(esinstantOptions.dom.downtime);
        var $startTime = parseInt($timedownDom.attr('data-countdowm'));
        if($startTime > 0 ){
            $timeCountDown = setInterval(function(){
                var $downtime = parseInt($timedownDom.attr('data-countdowm'));

                var $leftMSecond = $downtime - 1000;

                $timedownDom.attr('data-countdowm',$leftMSecond);
                mSecondeChange($leftMSecond,function($minute,$second,$msecond){                                //換算分秒
                    $(esinstantOptions.dom.downtime).text($minute+':'+$second);
                })

                if($leftMSecond <= 0){                                                                         //時間結束停止迴圈
                    stopCountDown();
                }
            },1000);
        }
        $('.pause-btn').css({'pointer-events':'auto'}).find('.pause').removeClass('disabled');
        $('.paly-btn').css({'pointer-events':'none'}).find('.play').addClass('disabled');
    }

    function pauseCountDown() {
        clearInterval($timeCountDown);
        $('.pause-btn').css({'pointer-events':'none'}).find('.pause').addClass('disabled');
        $('.paly-btn').css({'pointer-events':'auto'}).find('.play').removeClass('disabled');
    }

    function stopCountDown() {
        clearInterval($timeCountDown);
        close_bid();
        $('.pause-btn').css({'pointer-events':'none'}).find('.pause').addClass('disabled');
        $('.paly-btn').css({'pointer-events':'none'}).find('.play').addClass('disabled');
    }

    //毫秒 > 換算
    function mSecondeChange(time,callback) {
        var $downtime = time;
        var $minute = parseInt($downtime / 60 / 1000);
        var $second = parseInt(($downtime - $minute * 60 * 1000) / 1000);
        var $msecond = $downtime - $minute * 60 * 1000 - $second * 1000;

        //秒--補零
        if($second < 10){
            $second = '0'+String($second);
        }

        //毫秒--補零
        if($msecond < 10){
            $msecond = '00'+String($msecond);
        }else if($msecond < 100){
            $msecond = '0'+String($msecond);
        }

        callback($minute,$second,$msecond);
    }

//手動結標 並送參
    function close_bid() {
        //alert('執行結標動畫');
        var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/instantkill/close_bid/';

        var formData = new FormData();
        formData.append('productid','<?php echo $product_id ?>');
        formData.append('json','Y');

        $.ajax({
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            data: formData,
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,
            success: showResponse
        });

        function showResponse(data){
            //console.log(data);
            if(data['retCode'] == '1'){
                //alert(data['retMsg'])
                location.href = "<?php echo BASE_URL.APP_DIR; ?>/instantkill/show_final_winner/?flash_loc=<?php echo $flash_loc; ?>&productid=<?php echo $product_id; ?>";
                
            }else if(data['retCode'] == '-2'){
                alert(data['retMsg'])          //查無可結標商品
                location.reload();

            }else if(data['retCode'] == '-3'){
                alert(data['retMsg'])          //商家與商品提供者不同無法結標
                location.reload();

            }else if(data['retCode'] == '-4'){
                alert(data['retMsg'])          //未達到總標數限制 流標
                location.reload();

            }else if(data['retCode'] == '-5'){
                alert(data['retMsg'])          //無人最低且唯一 流標
                location.reload();
            }

        }

        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }

        //加載錯誤訊息
        function showFailure() {
        }



    }

//放大QRcode
    function zoomQRcodeUp() {
        if($('body').find('.zoomQRcode-modal').length <= 0){
            $('body').append(
                $('<div/>')
                .addClass('zoomQRcode-modal')
                .append(
                    $('<div/>')
                    .addClass('zoomQRcode-img')
                    .append(
                        $('<img/>')
                        .attr('src',esinstantOptions.getData.QRcodeUrl)
                    )
                )
                .append(
                    $('<div/>')
                    .addClass('zoomQRcode-bg')
                )
            )
            $('body').addClass('modal-open');
        }
        $('.zoomQRcode-modal').on('click', function(){
            zoomQRcodeClose();
        })
    }

//移除放大QRcode
    function zoomQRcodeClose() {
        $('body').find('.zoomQRcode-modal').remove();
        $('body').removeClass('modal-open');
    }
</script>
