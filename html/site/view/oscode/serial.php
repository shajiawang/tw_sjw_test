<?php //限定S碼激活
$row_list = _v('row_list'); 
$status = _v('status');
?>
		<div class="article">
            <div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">
				<div data-role="collapsible">
                    <h2>殺價券激活</h2>
                    <ul data-role="listview" data-theme="a">
                        <li class="ui-field-contain">
                            <label for="scsn">输入殺價券序號</label>
                            <input name="scsn" id="scsn" value="" data-clear-btn="true" type="text" >
                        </li>
						<li class="ui-field-contain">    
							<label for="scpw">输入殺價券密碼</label>
                            <input name="scpw" id="scpw" value="" data-clear-btn="true" type="text" >
						</li>	
                        <li class="ui-field-contain">    
							<button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="act_oscode()">確認</button>
                        </li>
                    </ul>
                </div>
            </div>
			
			<table data-role="table" id="table-custom" data-mode="reflow" class="dable-list ui-responsive">
              <thead>
                <tr>
                  <?php /*<th data-priority="1">活動名称</th>*/?>
				  <th data-priority="1">商品名称</th>
                  <th data-priority="2">赠送组數</th>
                  <th data-priority="3">激活日期</th>
				  <th data-priority="4">序號</th>
                </tr>
              </thead>
              <tbody>
                <?php 
				if(!empty($row_list['table']['record']) ) {
				foreach($row_list['table']['record'] as $rk => $rv){
				
				if($rv['closed'] == 'Y') { 
					$href = APP_DIR .'/bid/detail/?'. $status['status']['args'] .'&productid='. $rv['productid'];
				} elseif($rv['closed'] == 'N') { 
					$href = APP_DIR .'/product/saja/?'. $status['status']['args'] .'&productid='. $rv['productid'];
				} else { 
					$href = APP_DIR .'/bid/detail/?'. $status['status']['args'] .'&productid='. $rv['productid'];
				}
				?>
				<tr>
                  <?php /*<td><?php echo $rv['spname']; ?></td>*/?>
				  <td><a href="<?php echo $href; ?>"><?php echo $rv['name']; ?></a></td>
                  <td><?php echo $rv['amount']; ?></td>
                  <td><?php echo date("Y-m-d H:i", strtotime($rv['modifyt']) ); ?></td>
				  <td><?php echo $rv['serial']; ?></td>
                </tr>
				<?php } } else { ?>
				<tr>
                  <td> </td>
                  <td> </td>
                  <td> </td>
				  <td> </td>
                </tr>
				<?php }//endif; ?>
                
              </tbody>
            </table>
			
			<?php 
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content'); 
			
			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : ''; 
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
			?>
			<div>
				<div class="ui-grid-a">
                    <div class="ui-block-a <?php echo $arrow_l_able; ?>">
					<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
					</div>
                    <div class="ui-block-b <?php echo $arrow_r_able; ?>">
					<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
					</div>
                </div>
                <?php /*<form>
					<select name="select-native-1" id="change-page">
					<?php echo $page_content['change']; ?>
                    </select>
                </form>*/?>
            </div>
			<?php } ?>
			
        </div><!-- /article -->
		