<?php
  $txArr=$this->tplVar['txArr'];
  $evrid=$txArr['evrid'];
  $tx_code_md5=$txArr['tx_code_md5'];
  $total_bonus=$txArr['total_bonus'];
  $vendor_name=$txArr['vendor_name'];
?>
<!DOCTYPE html>
<html>
	<head>
		<title>杀价王</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script type="text/javascript" src="/management/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="/management/js/jquery.timers-1.2.js"></script> 
		<script type="text/javascript" src="/management/js/jquery.mobile-1.4.3.min.js"></script>
		<script type="text/javascript" src="/management/js/share.js"></script>
	    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
	    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery.Validate/1.6/localization/messages_tw.js"></script>
	    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/le-frog/jquery-ui.css" type="text/css">
		<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	</head>
  <body>
   <div data-role="page" data-title="杀价王" id="vendorCommit">
	   <div data-role="header" id="hdr">
		  <h1>交易結果</h1>
	   </div>
	   <div data-role="content" id="ctnt">
			<div class="article">
				<input type="hidden" name="evrid" value="<?php echo $txArr['evrid']; ?>">
				<ul data-role="listview" data-inset="true" data-icon="false">
					<li>
						<label>商家名称 : <?php echo $vendor_name; ?></label>
					</li>
					<li>
						<label>收取紅利 : <?php echo $total_bonus; ?> 点</label>
					</li>
					<li>
						<label id="tx_status_desc">交易状态 : 买家兑换中....</label>
					</li>
				</ul>
				<div style="display:none">
				   <label class="countdown" data-offtime2="<?php echo $txArr['expired_time']; ?>" data-tag2=""></label>
				</div>
				<audio id="txOK" preload="auto">
				  <source src="https://www.shajiawang.com/management/audio/txOK.mp3" type="audio/mpeg" />
				  <source src="https://www.shajiawang.com/management/audio/txOK.ogg" type="audio/ogg" />
				  <source src="https://www.shajiawang.com/management/audio/txOK.wav" type="audio/wav" />
				   Your browser does not support the audio element.
				</audio>
			</div>
		</div>
		<div data-role="footer" id="fter">
			<h4>全球首创杀价式拍卖导购平台</h4>
		</div>
	
	<script type="text/javascript">
		var now_time;
		var tx_status='1';
		var evrid = '<?php echo $evrid; ?>';
		
		$(document).on("pagecreate",
		    function(event){
				$.get("https://www.shajiawang.com/site/lib/cdntime.php?"+getNowTime(), 
						function(data) {
							now_time = data; //系統時間
							// window.alert(data);
						}
				);		
					
		    $(this).stopTime('losttime');
			
			$(this).everyTime('2s','losttime', function() {
					var pageid = $.mobile.activePage.attr('id');
					now_time = parseInt(now_time);
					var end_time = $('#'+pageid+' .countdown').attr('data-offtime2'); //截標時間
					var lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
					if(lost_time >=0) {
						  // $('#'+pageid+' .countdown').html(end_plus+lost_time);
					  	  getTxStatus(evrid);
						  if(tx_status==4) {
							 // 買家已兌換完成
							 $(this).stopTime('losttime');
							 $('#tx_status_desc').text("交易状态 : 完成 !!");
							 $('#txOK')[0].load();
							 $('#txOK')[0].play();
							 window.alert("交易完成 !!");
							 window.location.href='/site/enterprise/storebonuslist/';
						  } else if(tx_status<=0) {
							 // 交易取消 
							 $(this).stopTime('losttime');
							 $('#tx_status_desc').text("交易状态 : 买家取消 !!");
							 window.alert("买家取消本次交易 !!"); 
							 window.location.href='/site/enterprise/'; 
						  }
						   
					} else if(lost_time<0) {
						// 交易取消;
						$.get("https://www.shajiawang.com/site/mall/cancelQrcodeTx/?evrid="+evrid, 
						     function(data) {
							    var msg=$.parseJSON(data);
								if(msg.retCode=='1') {
								  $('#tx_status_desc').text("交易状态 : 逾时取消 !!");
								  window.alert("逾时取消本次交易 !!");
								  window.location.href='/site/enterprise/'; 
								}								
							 }
						 );
						$(this).stopTime('losttime');
					}
			});
			
		});
		
		function getTxStatus(evrid) {
		         $.get("https://www.shajiawang.com/site/mall/getQrcodeTxStatus/?evrid="+evrid, 
					 function(ret) {
					       tx_status=ret;  
					 }
				 );
		}
        
</script>
</div>
</body>
</html>
