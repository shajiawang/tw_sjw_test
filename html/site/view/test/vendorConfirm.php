<?php
  $txArr=$this->tplVar['txArr'];
?>
<!DOCTYPE html>
<html>
	<head>
		<title>杀价王</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
	    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery.Validate/1.6/localization/messages_tw.js"></script>
	    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/le-frog/jquery-ui.css" type="text/css">
		<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
	</head>
	<body>
<script>
   function chkTxData() {
          f = document.qrcodeTx;
		  if(f.vendorid.value=="") {
		     alert("请填写商家编号 !!");
			 return false;
		  }
		  if(f.vendor_name.value=="") {
		     alert("请填写商家名称 !!");
			 return false;
		  }
		  if(f.total_bonus.value=="" || isNaN(f.total_bonus.value)) {
		     alert("收取鲨鱼点数 : 必须为数字 !!");
			 return false;
		  }
		  if(f.total_bonus.value<0) {
		     alert("收取鲨鱼点数 : 必须为正数 !!");
			 return false;
		  }
		  f.prod_name.value=f.vendor_name.value+":"+f.total_bonus.value;
		  document.qrcodeTx.submit(); 
   }
   
   function cancelTx() {
         if(confirm("确定取消本次交易 ??")) {
		    document.qrcodeTx.tx_status.value="-2";
		    document.qrcodeTx.submit(); 
		 } else {
		    return false;
		 }
   }
</script>
<div data-role="page" data-title="杀价王" >
   <div data-role="header" id="header">
      <h1>商家确认交易</h1>
   </div>
   <div data-role="content" id="content">
   <div class="article">
		<form name="qrcodeTx" method="POST" action="/management/webtx/vendorCommit/" onsubmit="return chkTxData();return false;" >
		<input type="hidden" name="tx_status" value="3">
		<input type="hidden" name="evrid" value="<?php echo $txArr['evrid']; ?>" >
		<input type="hidden" name="userid" value="<?php echo $txArr['userid']; ?>" >
		<input type="hidden" name="vendorid" value="<?php echo $txArr['vendorid']; ?>">
		<input type="hidden" name="prod_name" value="<?php echo $txArr['companyname']; ?>">
		<input type="hidden" name="tx_code_md5" value="<?php echo $txArr['tx_code_md5']; ?>" >
		<input type="hidden" name="tx_currency" value="<?php echo $txArr['tx_currency']; ?>" >
		<input type="hidden" name="tx_type" value="web_tx" >
		<input type="hidden" name="vendor_name" value="<?php echo $txArr['companyname']; ?>" >
		<input type="hidden" name="keyin_time" value="<?php echo $txArr['keyin_time']; ?>" >
		<ul data-role="listview" data-inset="true" data-icon="false">
			<li>
			<label>商家名称 : <?php echo $txArr['companyname']; ?>
			</li>
			<li>
			   收取鲨鱼点数 : <input type="text" id="total_bonus" name="total_bonus" size="6">
			</li>
			<li>
			<div data-role="controlgroup" data-type="horizontal">
				<a href="#" data-role="button" data-icon="delete" data-iconpos="left" onclick="cancelTx()">取消交易</a>
				<a href="#" data-role="button" data-icon="check" data-iconpos="right" onclick="chkTxData()">确认交易</a>
			</div>
			<!--button data-icon="delete" data-iconpos="left" onclick="cancelTx()">取消交易</button>
			<button type="submit" data-icon="check" data-iconpos="right">确认交易</button -->
			</li>
		</ul>
		</form>
	</div>
  </div>
<div data-role="footer" id="footer">
  <h4>全球首创杀价式拍卖导购平台</h4>
</div>
</body>
</html>