<?php
  $txArr=$this->tplVar['txArr'];
  $evrid=$txArr['evrid'];
  $tx_code_md5=$txArr['tx_code_md5'];
  $total_bonus=$txArr['total_bonus'];
  $total_price=$txArr['total_price'];
  $vendor_name=$txArr['vendor_name'];
  // $expired_ts=strtotime($txArr['keyin_time'])+90;
  $expired_ts=$txArr['expired_ts'];
  $thumbnail=$txArr['thumbnail'];
  
  error_log("[webtx/vendorCommit2] :".$evrid."-".$tx_code_md5."-".$expired_ts);
  
  // error_log("[webtx/vendorCommit2] APP_DIR : ".APP_DIR);
  
  $APP_DIR='/site';
?>
<!DOCTYPE html>
<html>
	<head>
	<title>杀价王</title>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
	    <script type="text/javascript" src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
	    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script>
	    <script type="text/javascript" src="https://ajax.aspnetcdn.com/ajax/jQuery.Validate/1.6/localization/messages_tw.js"></script>
	    <link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/le-frog/jquery-ui.css" type="text/css">
		<link rel="stylesheet" href="https://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" / -->
		<script type="text/javascript" src="/management/js/jquery-1.11.1.min.js"></script>
        <script type="text/javascript" src="/management/js/jquery.timers-1.2.js"></script> 
		<script type="text/javascript" src="/management/js/jquery.mobile-1.4.3.min.js"></script>
		<script type="text/javascript" src="/management/js/jquery.gracefulWebSocket.js"></script>
		<script type="text/javascript" src="/management/js/jquery-ui.custom.min.js"></script>
		<script type="text/javascript" src="/management/js/messages_tw.js"></script>
	    <link rel="stylesheet"  href="/management/css/jquery-ui.css"  />
		<link rel="stylesheet"  href="/management/css/jquery.mobile-1.4.5.min.css" />
	</head>
  <body>
   <div data-role="page" data-title="杀价王" id="vendorCommit">
	   <div data-role="header" id="hdr">
	      <h1>交易结果</h1>
	   </div>
	   <div data-role="content" id="ctnt">
			<div class="article">
				<input type="hidden" name="evrid" value="<?php echo $evrid; ?>">
				<input type="hidden" name="tx_qnantity" value="<?php echo $tx_quantity; ?>">
				<ul data-role="listview" data-inset="true" data-icon="false">
					<li>	
						<h3><img src="<?php echo $thumbnail; ?>" width="50%" border="0" align="absmiddle" style="display:block; margin:auto;"></h3>
					<li>	
					<li>
						<label>商家名称 : <?php echo $vendor_name; ?></label>
					</li>
					<li>
						<label>收取紅利 : <?php echo $total_price; ?> 点</label>
					</li>
					<li>
						<label id="tx_status_desc">交易状态 : 买家兑换中, 请稍候....</label>
					</li>
				</ul>
				<div style="display:none">
				   <label id="cntdown" class="countdown" data-offtime2="<?php echo $expired_ts; ?>" ></label>
				</div>
				<audio id="txOK" preload="auto">
				  <source src="/management/audio/txOK.mp3" type="audio/mpeg" />
				  <source src="/management/audio/txOK.ogg" type="audio/ogg" />
				  <source src="/management/audio/txOK.wav" type="audio/wav" />
				   Your browser does not support the audio element.
				</audio>
			</div>
		</div>
		<div data-role="footer" id="fter">
			<h4>全球首创杀价式拍卖导购平台</h4>
		</div>
	
	<script type="text/javascript">
		var now_time;
		var evrid = '<?php echo $evrid; ?>';
		var socket;
		var wssurl = '<?php echo $this->config->wss_url; ?>';
        var baseurl= '<?php echo BASE_URL; ?>';		
		var pageid;
        var end_time ;
		var lost_time;
		
		baseurl='';
		// $(document).ready( function(){
		$(document).on("pagecreate",function(event){            
     				$.get(baseurl+"<?php echo $APP_DIR; ?>/lib/cdntime.php", 
						   function(data) {
								 now_time = data; //系統時間
						   }
					);
					
					if(true) {
					// if(is_weixn()) {
						$(this).stopTime('losttime2');
						$(this).everyTime('1s','losttime2', function() {
						        pageid = $.mobile.activePage.attr('id');
								now_time++;
								end_time = $('#'+pageid+' .countdown').attr('data-offtime2'); //截標時間
								lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
								
								if(lost_time>=0) {
									  // $('#'+pageid+' .countdown').html(lost_time);
									  $.get(baseurl+'<?php echo $APP_DIR; ?>/mall/getQrcodeTxStatus/?evrid='+evrid, 
											 function(status) {
											       intStatus=parseInt(status);
											       if(intStatus==4) {
													 // 買家已兌換完成
													 $(document).stopTime('losttime2');
													 $('#tx_status_desc').text("交易状态 : 完成 !!");
													 $('#txOK')[0].load();
													 $('#txOK')[0].play();
													 window.alert("交易完成 !!");
													 window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/storebonuslist/'; 
												} else if(intStatus==0) {
													 // 交易取消 
													 $(document).stopTime('losttime2');
													 $('#tx_status_desc').text("交易状态 : 逾時取消 !!");
													 window.alert("逾時取消交易 !!"); 
													 window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/';  
												} else if(intStatus<0) {
													 // 交易取消 
													 $(document).stopTime('losttime2');
													 $('#tx_status_desc').h("交易状态 : 买家取消 !!");
													 window.alert("买家取消交易 !!"); 
													 window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/';  
												}
											}
									  );
								} else if(lost_time<0) {
									// 交易取消;
									$.get(baseurl+"<?php echo $APP_DIR; ?>/mall/cancelQrcodeTx/?evrid="+evrid, 
										 function(data) {
											var msg=$.parseJSON(data);
											if(msg.retCode=='1') {
												  $('#tx_status_desc').html("交易状态 : 逾时取消 !!");
												  window.alert("逾时取消交易 !!");
											}								
										 }
									 );
									 $(this).stopTime('losttime2');
									 window.location.href='<?php echo $APP_DIR; ?>/enterprise/';  // ToDo
								}
						});
					} else {
						// socket
						
						socket = $.gracefulWebSocket(wssurl);
						socket.onopen =  function(evt){
										   // alert("socket"+evrid+"|V....");
										   socket.send("ADD|"+evrid+"|V");
						};
						
						socket.onmessage = function(evt){ 
											 // message format evrid|status|message;
											 var ret = evt.data;
											 var arr = ret.split("|");
											 var id=arr[0];
											 var status=arr[1];
											 if(id==evrid) {
											    process(status);
											 }
						};
										  
						$(this).stopTime('losttime2');
						$(this).everyTime('1s','losttime2', function() {
								pageid = $.mobile.activePage.attr('id');
								end_time = $('#'+pageid+' .countdown').attr('data-offtime2'); //截標時間
								now_time++;
								lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
								 // $('#'+pageid+' #cntdown').html(lost_time);
								if(lost_time<0) {
									// 交易取消;
									$(this).stopTime('losttime2');
									$.get(baseurl+"<?php echo $APP_DIR; ?>/mall/cancelQrcodeTx/?evrid="+evrid, 
										 function(data) {
											var msg=$.parseJSON(data);
											if(msg.retCode=='1') {
											   $('#tx_status_desc').html("交易状态 : 逾时取消 !!!");
											}								
										 }
									 );
									
								}
						});
					}
					// alert(evrid);		
					/*
					socket = new WebSocket(host);
					console.log('WebSocket - status ' + socket.readyState);
					
					socket.onopen = function(evt){onOpen(evt)};
					//Message received from websocket server
					socket.onmessage = function(evt){onMessage(evt)};
					 
					//Connection closed
					socket.onclose = function(evt){onClose(evt)};
					 
					socket.onerror = function(evt){onError(evt)};
				    */
				}
		);
	    
		/*
		function is_weixn(){
		        var ua = navigator.userAgent.toLowerCase();
				if(ua.match(/MicroMessenger/i)=='micromessenger') {
					return true;
				} else {
					return false;
				}
		}
		
		function process(status) {
		        if(parseInt(status)==4) {
					 // 買家已兌換完成
					 $(document).stopTime('losttime2');
					 $('#tx_status_desc').text("交易状态 : 完成 !!");
					 $('#txOK')[0].load();
					 $('#txOK')[0].play();
					 window.alert("交易完成 !!");
					 window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/'; 
				} else if(parseInt(status)==0) {
					 // 交易取消 
					 $(document).stopTime('losttime2');
					 $('#tx_status_desc').text("交易状态 : 逾時取消 !!");
					 window.alert("逾時取消交易 !!"); 
					 window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/';  
				} else if(parseInt(status)<0) {
					 // 交易取消 
					 $(document).stopTime('losttime2');
					 $('#tx_status_desc').h("交易状态 : 买家取消 !!");
					 window.alert("买家取消交易 !!"); 
					 window.location.href=baseurl+'<?php echo $APP_DIR; ?>/enterprise/';  
				}
		}
		
		function getTS() {
			var n = new Date();
			var y = n.getFullYear();
			var m = (n.getMonth()+1< 10)?("0" + (n.getMonth() + 1)):(n.getMonth() + 1);
			var d = (n.getDate()< 10)?("0" + (n.getDate())):(n.getDate());
			var h = (n.getHours() + 8< 10)?("0" + (n.getHours() + 8)):(n.getHours() + 8);
			var mi = (n.getMinutes()< 10)?("0" + (n.getMinutes())):(n.getMinutes());
			var s = (n.getSeconds()< 10)?("0" + (n.getSeconds())):(n.getSeconds());
			return now = y+""+m+""+d+""+h+""+mi+""+s;
		}
		
		function getCurrTS() {  
		       $.get(baseurl+"/management/libs/cdntime.php", 
				 function(data) {
					    now_time = parseInt(data); //系統時間
				 }
			  );
        }					
		*/
		
		
		/*
		$(document).on("pagecreate",
		    function(event){
				$.get("http://www.shajiawang.com/site/lib/cdntime.php?"+getNowTime(), 
						function(data) {
							now_time = data; //系統時間
							// window.alert(data);
						}
				);		
					
		    $(this).stopTime('losttime2');
			
			$(this).everyTime('1s','losttime2', function() {
					var pageid = $.mobile.activePage.attr('id');
					now_time = parseInt(now_time);
					var end_time = $('#'+pageid+' .countdown').attr('data-offtime2'); //截標時間
					var lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
					if(lost_time >=0) {
						  // $('#'+pageid+' .countdown').html(end_plus+lost_time);
					  	  getTxStatus(evrid);
						  if(tx_status==4) {
							 // 買家已兌換完成
							 $(this).stopTime('losttime2');
							 $('#tx_status_desc').text("交易状态 : 完成 !!");
							 $('#txOK')[0].load();
							 $('#txOK')[0].play();
							 window.alert("交易完成 !!");
						  } else if(tx_status<=0) {
							 // 交易取消 
							 $(this).stopTime('losttime2');
							 $('#tx_status_desc').text("交易状态 : 买家取消 !!");
							 window.alert("买家取消本次交易 !!"); 
						  }
						   
					} else if(lost_time<0) {
						// 交易取消;
						$.get("http://test.shajiawang.com/site/mall/cancelQrcodeTx/?evrid="+evrid, 
						     function(data) {
							    var msg=$.parseJSON(data);
								if(msg.retCode=='1') {
								  $('#tx_status_desc').text("交易状态 : 逾时取消 !!");
								  window.alert("交易逾时取消 !!");
								}								
							 }
						 );
						$(this).stopTime('losttime2');
					}
			});
			
		});
		
		function getTxStatus(evrid) {
		         $.get("http://www.shajiawang.com/site/mall/getQrcodeTxStatus/?evrid="+evrid, 
					 function(ret) {
					       tx_status=ret;  
					 }
				 );
		}
        */
</script>
</div>
</body>
</html>
