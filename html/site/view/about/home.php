<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
?>	
<div class="article">
    <div class="saja_lists_box">
        <!-- 隱私權政策 (另開頁面) -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/about/privacy/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">隱私權政策</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <!-- 服務條款 (另開頁面) -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/about/service/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">服務條款</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <!-- 用戶使用規範 (另開頁面) -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/about/usage/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">用戶使用規範</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <!-- 版本資訊 -->
        <div class="list-group">
           
            <!-- 頁簽二擇一使用 -->
            
            <!-- 版號直接顯示 start -->
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">版本資訊</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="saja_list_rtxt"><span id="version"></span></div>
                    </div>
                </a>
            </div>
            <!-- 版號直接顯示 end -->
            
            <!-- 版號展開式頁簽 start -->
            <!--
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('ab4',this,'blod');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">版本資訊</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="ab4" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p class="text-center">殺價王 V<span id="version" class="text-center"></span></p> 
                        <p class="text-center">版號 : <span id="build" class="text-center"></span></p>     
                        <p class="text-center">環境 : <span id="userAgent" class="text-center"></span></p>                    
                    </div>
                </div>
            </div>
            
            -->
            <!-- 版號展開式頁簽 end -->
        </div>
    </div>
</div>
<script>
   // buildNo="0";
   // userAgent = navigator.userAgent;

   versionNo = getVersion0();
   if(!versionNo)
       versionNo = "2.0.4"; 
   // userAgent= getUserAgent0();
   // buildNo=getBuild0();
   
   document.getElementById('version').innerHTML=versionNo;          //版本資訊
   // document.getElementById('build').innerHTML=buildNo;           //版號
   // document.getElementById('userAgent').innerHTML=userAgent;     //環境    
    
</script>