<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$data = _v('data');
?>
<div class="article">
    <div class="saja_lists_box">
        <!-- 用戶使用規範 -->
        <div class="list-group">
            <div id="ab3" class="linkBox">
                <div class="list-collapse">
                    <div id="onlyContent">
					<?php echo $data; ?>
                    </div>
                </div>
            </div>
        </div>   
    </div>
</div>