<?php
date_default_timezone_set('Asia/Shanghai');
$static_html=_V('static_html');

	if(!empty($static_html)) { 
		// error_log("[view/tech] including {$static_html} ....");
		include_once($static_html);  
	} else { 
		$cdnTime = date("YmdHis");
		$row_list = _v('row_list'); 
		$detail = _v('detail'); 
		$status = _v('status'); 
?>
    <div class="article">
        <div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">

            <?php 
				if(!empty($row_list) ) {
				foreach($row_list as $mk1 => $mv1) {
				?>
            <div data-role="collapsible" data-collapsed="false">
                <h2>
                    <?php echo $mv1["cname"];?>
                </h2>
                <!--<ul data-role="listview" data-theme="b">
                        <?php foreach($mv1["menu"] as $fid => $fname) { ?>
						<li><a href="<?php echo APP_DIR; ?>/tech/detail/?fid=<?php echo $fid;?>&<?php echo $cdnTime; ?>" data-transition="slide">
						<?php echo $fname;?></a></li>
						<?php }//endforeach; ?>
                    </ul>-->
                <div data-role="collapsible-set" data-collapsed="false">
                    <?php foreach($mv1["menu"] as $fid => $fname) { ?>
                    <div data-role="collapsible">
                        <h4>
                            <?php echo $detail[$fid]['menu']; ?>
                        </h4>
                            <div id="onlyContent">
                                <?php echo $detail[$fid]['description']; ?>
                            </div>
                    </div>
                    <?php }//endforeach; ?>
                </div>
            </div>
            <?php } }//endforeach; ?>

        </div>
    </div>
    <!-- /article -->
    <?php  }  ?>
    <script type="text/javascript">
        // Wechat
        var dataForWeixin = {
            MsgImg: "http://www.shajiawang.com/site/images/site/shark_logo.png",
            TLImg: "http://www.shajiawang.com/site/images/site/shark_logo.png",
            url: "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>",
            title: "若练殺價武功，必读秘笈 - iPhone7火爆抢殺中，最低且唯一中標。如何下標，先看秘笈。",
            desc: "若练殺價武功，必读秘笈 - iPhone7火爆抢殺中，最低且唯一中標。如何下標，先看秘笈。",
            fakeid: "",
            callback: function() {}
        };


        //var uri = encodeURIComponent(location.href.split('#')[0]);
        var uri = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
        $.ajax({
            type: "POST",
            url: "http://www.shajiawang.com/site/product/weixinSDK/",
            data: {
                wurl: uri
            },
            dataType: "text",
            success: function(data) {
                console.log(data);
                var configData = jQuery.parseJSON(data);
                console.log('sig is ' + configData.signature);
                wx.config({
                    //debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参數，可以在pc端打开，参數信息会通过log打出，仅在pc端时才会打印。
                    appId: configData.appId, // 必填，公众號的唯一標识
                    timestamp: configData.timestamp, // 必填，生成签名的時間戳
                    nonceStr: configData.nonceStr, // 必填，生成签名的随機串
                    signature: configData.signature, // 必填，签名，见附录1
                    jsApiList: [ //需要调用的接口
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage',
                        'onMenuShareQQ',
                        'onMenuShareWeibo'
                    ]
                });

                wx.error(function(res) {
                    // alert('wx.error: '+JSON.stringify(res));
                });

                wx.ready(function() {
                    wx.onMenuShareTimeline({
                        title: dataForWeixin.title, //分享后自定义標题
                        link: dataForWeixin.url, //分享后的URL
                        imgUrl: dataForWeixin.MsgImg, //分享的LOGO
                        desc: dataForWeixin.desc, // 分享描述
                        success: function(res) {
                            alert('已分享');
                        },
                        cancel: function(res) {
                            alert('已取消');
                        },
                        fail: function(res) {
                            alert('wx.onMenuShareTimeline:fail: ' + JSON.stringify(res));
                        }
                    });

                    wx.onMenuShareAppMessage({
                        title: dataForWeixin.title, //分享后自定义標题
                        link: dataForWeixin.url, //分享后的URL
                        imgUrl: dataForWeixin.MsgImg, //分享的LOGO
                        desc: dataForWeixin.desc, // 分享描述
                        success: function(res) {
                            alert('已分享');
                        },
                        cancel: function(res) {
                            alert('已取消');
                        },
                        fail: function(res) {
                            alert('wx.onMenuShareAppMessage:fail: ' + JSON.stringify(res));
                        }
                    });
                    wx.onMenuShareQQ({
                        title: dataForWeixin.title, //分享后自定义標题
                        link: dataForWeixin.url, //分享后的URL
                        imgUrl: dataForWeixin.MsgImg, //分享的LOGO
                        desc: dataForWeixin.desc, // 分享描述
                        success: function(res) {
                            alert('已分享');
                        },
                        cancel: function(res) {
                            alert('已取消');
                        },
                        fail: function(res) {
                            alert('wx.onMenuShareQQ:fail: ' + JSON.stringify(res));
                        }
                    });
                    wx.onMenuShareWeibo({
                        title: dataForWeixin.title, //分享后自定义標题
                        link: dataForWeixin.url, //分享后的URL
                        imgUrl: dataForWeixin.MsgImg, //分享的LOGO
                        desc: dataForWeixin.desc, // 分享描述
                        success: function(res) {
                            alert('已分享');
                        },
                        cancel: function(res) {
                            alert('已取消');
                        },
                        fail: function(res) {
                            alert('wx.onMenuShareWeibo:fail: ' + JSON.stringify(res));
                        }
                    });
                });

            },
            error: function(data) {

            }

        });

    </script>
