<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$app = _v('APP');
$help_category = _v('help_category');
?>  
<div class="article">
	<?php foreach($help_category as $cgk => $cgv){ ?>
    <div class="saja_lists_box">
        <div class="header-title">
            <div class="title"><?php echo $cgv['name'];?></div>
        </div>
        <?php foreach($cgv['help_list'] as $rk => $rv){ ?>
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help<?php echo $rv['hid'];?>',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title"><?php echo $rv['name'];?></div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help<?php echo $rv['hid'];?>" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p><?php echo $rv['description'];?></p>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    </div>
<!--<?php echo $cgv['name'];?>-->	
	<?php } ?>

    <?php if (empty($app)){ ?>
    <div class="help-bottomBtn-box">
        <div class="header-title">
            <div class="title">客服</div>
        </div>
        <!-- 問答紀錄(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">您的問答紀錄</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <!-- 聯繫客服(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/issues_add?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">詢問客服</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <?php } ?>
	
    <div class="faq-link-box">
        <!-- 新手教學(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
                <?php if($app=='Y') {?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/faq/?APP=Y'">
                <?php }else{ ?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/faq/?<?php echo $cdnTime; ?>'">
                <?php } ?>  
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">新手教學</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
        <p style=" margin-top: 12px; margin-left: 12px;">
            <a href="https://line.me/R/ti/p/%40fgz1243t" target="https://line.me/R/ti/p/%40fgz1243t" title="殺價王官方Line@" class="ui-link" style="color: #689AD9; text-decoration: underline;">如有問題，請私訊殺價王官方Line@</a>
        </p>

    </div>
    
</div>