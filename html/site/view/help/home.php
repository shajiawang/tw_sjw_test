<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$app = _v('APP');
?>  
<div class="article">
    <div class="saja_lists_box">
        <div class="header-title">
            <div class="title">帳號安全</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help1',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">忘記密碼怎麼辦？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help1" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>在登入頁按下忘記密碼，系統會要求輸入您已經驗證的手機號碼，輸入完成後，按下發送新密碼，就會收到一組新密碼，使用其登入即可。</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help2',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何驗證手機號碼？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help2" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【我的帳號】，按下【手機驗證】，輸入手機號碼後，按下發送新密碼，此時系統會要求輸入簡訊傳送來的驗證碼，後按下驗證即可驗證成功。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help3',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何修改登入密碼？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help3" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【我的帳號】，按下【修改登入密碼】，輸入舊密碼、新密碼，並再次輸入一次新密碼後，按下確認修改即可完成修改密碼。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help4',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何修改兌換密碼？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help4" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【我的帳號】，按下【修改兌換密碼】，輸入舊兌換碼、新兌換碼，並再次輸入一次新兌換密碼後，按下確認修改即可完成修改密碼。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help5',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">忘記兌換密碼怎麼辦？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help5" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【我的帳號】，按下【忘記兌換密碼】，系統會要求輸入您已經驗證的手機號碼，輸入完成後，按下發送新兌換密碼，就會收到一組新兌換密碼，使用其去ibon機器使用。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--帳號安全-->
	
	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">殺價幣相關</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help6',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是殺價幣？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help6" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價幣為殺價王下標的代幣，須先購買殺價幣才能進行下標及結帳。</p>
                    </div>
                </div>
            </div>
        </div>
        
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help7',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何購買殺價幣？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help7" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【購買殺價幣】，選擇欲購買的額度並選擇購買的方式後按下確認。購買方式如下：</p>
                        <p>【信用卡】購買殺價幣：按照系統的要求，輸入卡號等基本資料完成後按下付款，即可完成購買。</p>
                        <p>【繳費匯款】購買殺價幣：須至ＡＴＭ前操作，使用“繳費”的方式，將款項匯入您個人專屬的匯款帳號，才能完成繳費，切記不能使用“轉帳”功能，因為系統無法自動將殺價幣代入您的遊戲帳號中。</p>
                        <p>備註１：用不同的方式購買殺價幣會贈送不同的殺價券且購買的總金額會有所不同。請殺友們妥善選擇您喜歡的方式購買！</p>
                        <p>備註２：贈送的殺價券會配合不同活動有不同變化，詳情請追蹤本公司臉書粉絲團。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help8',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何查詢殺價幣剩餘額度？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help8" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【殺價幣】，即可查詢其資訊，內有購買的發票證明、使用及獲得的紀錄。</p>
                    </div>
                </div>
            </div>
        </div>
		
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help9',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">刷退殺價幣</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help9" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>請聯繫殺價王LINE客服：@fgz1243t。備註：僅限於七天內，未使用殺價幣與殺價券的狀況下得以刷退。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help10',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">剩餘的殺價幣退費</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help10" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>請聯繫殺價王LINE客服：@fgz1243t。剩餘殺價幣必須扣除其百分之六當作退費的費用，再扣除已經使用掉的殺價王平台贈與的殺價券市值後退費方可完成。工作時間五到七個工作日。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help11',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是鯊魚點</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help11" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>鯊魚點是殺友們殺完價以後，未得標，殺價王平台贈與客戶的紅利點數，殺價幣與鯊魚點的比例為100%！能去殺價王合作的商家或平台兌換商品。讓您完全沒有後顧之憂！平時本來要去便利商店買東西的錢可以先在殺價王玩一把以後再去購物，錢當兩次花，開心沒煩惱。依此類推，只要在殺價王有合作能去繳費的品項，您都可以先將之能來殺價王殺價一次，再將錢拿去本來應該使用的地方做使用喔！備註：鯊魚點無法退還為任何有價貨幣。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help12',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何查詢鯊魚點剩餘額度</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help12" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【鯊魚點】，即可查詢其資訊，內有獲點紀錄以及使用的紀錄。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help13',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是殺價券</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help13" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價券為殺價王平台贈與殺友們下標一次的手續費抵用券，不得轉換為現金，無法退還成任何有價貨幣，也無法找零，有不同的面額與可下標商品的限制。請注意：</p>
						<p>1.殺價券有使用期限，其使用期限為可抵用商品的結標時間，逾期無法使用。</p>
						<p>2.殺價商品若流標時，無法退還。</p>
						<p>3.殺價商品若未得標時，無法轉贈鯊魚點。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help14',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是超級殺價券</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help14" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>超級殺價券為殺價王平台贈與殺友們下標一次的手續費抵用券，不得轉換為現金，無法退還成任何有價貨幣，也無法找零，無面額大小與使用期限。所以建議使用在高手續費的殺價商品，請注意：</p>
						<p>1.殺價商品若流標時，無法退還。</p>
						<p>2.殺價商品若未得標時，無法轉贈鯊魚點。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help15',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">下標時忘了用殺價券怎麼辦？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help15" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>親愛的殺友們，殺價王平台，無法回溯您忘記使用殺價券而扣除的殺價幣，也無法回溯您已經過期的殺價券，請用戶在下標時，切記使用殺價券喔！</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!--殺價幣相關-->
	
	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">遊戲規則</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help16',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是殺價？殺價王遊戲規則</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help16" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>需購買殺價幣以後才得以享受殺價的服務，每樣商品，殺友們皆可以出價您認為最合適的價格，此下標的動作稱為殺價。<br><br></p>
                        <p>而這些商品誰才能得標呢？得標者須滿足以下兩個條件：</p>
                        <p>1.出價金額不與他人重複。</p>
                        <p>2.出價金額是最低價。</p>
                        <p>總結來說出價必須是最低且唯一。<br><br></p>
                        <p>為什麼要最低且唯一呢？因為如果每位殺友都想用一塊錢購得同一項商品的話，我們不知道要選擇哪一位殺友成為得標者，所以殺價王平台規定，必須是不與他人重複的金額，才能得標。</p>
						<p>那為什麼不重複還要最低才能得標呢？因為雖然您殺價成功，是唯一的金額但是必須是最低出價的“王者”才能成為殺價王！因此最低還要唯一才能成為殺價王得標者喔！</p>
					</div>
                </div>
            </div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help17',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">怎麼下標？單筆下標與連續下標</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help17" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價王最刺激最重要的當然就是殺價下標囉！在【首頁】選擇殺友感興趣的商品，點擊進入即進入【殺價商品】頁，在下標前，請切記詳閱【下標規則】喔！瞭解清楚後再做下標的動作，才能殺得開心喔！看完【下標規則】以後，如果對商品有興趣也可以參閱【商品詳情】。此時按下立即下標即可進入下標頁囉！<br><br></p>             
						<p>單筆下標：可任選一種數字出價，但出價金額不可高於該商品市售價格。<br><br></p>
						<p>連續下標：可設定下標的金額範圍，每次下標標數不得超過100標。注意！在商品結標前一分鐘禁止包牌下標，只能使用單次下標。<br><br></p>
						<p>此時選擇您下標的方式與下標的金額後，按【下一步】，進入結算頁面，會計算您的需要付的殺價幣或是選擇要不要用殺價券或超級殺價券折抵，按【下一步】，會進入【付款確認】頁面，此時是您最後猶豫機會，按下【付款】就會付款完成囉！</p>
					</div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help18',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是下標提示？心機？諜對諜？團體戰才是王道！</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help18" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>每次完成付款後，會出現【成功下標】的頁面，此時，請各位注意！這裡面會有提示！讓各位知道您當下出價的金額的狀況，提示的類別如下：</p>
						<p>1.這個價錢與其他人重複了！</p>
						<p>2.您是目前的得標者！</p>
						<p>3.這個出價是「唯一」但不是「最低」，目前是「唯一」又比您「低」的還有XX位。</p>
						<p>4.這個出價是「唯一」但不是「最低」，目前是「唯一」又比您「低」的超過 10 位。</p>
						<p>5.本次有XX個出價是唯一價, 其中最低價格是xx元！</p>
						<p>切記，這些提示都是殺友您殺價的那一瞬間，系統判定您出的那個數字，並不太表其他殺友不會更進殺價喔，所以這些下標提示是一種心理戰，讓各位知道：喔～原來這個數字之下或有其他人更低的狀況。或許或有人認為會誤導殺友，其實，這句話只對一半，在沒人下標時，這是個情報戰，去搜集情報與佈局之用，而當快要結標時，這時候就是諜對諜了，因會收集的情報只在出價的一瞬間是對的，所以請早做佈局，並且，如果殺友自己單打獨鬥的話，很快會陷入一直標不到的狀況，所以建議要找朋友一起玩，用多個帳號去佈局，這樣子才能享受到殺價的樂趣並且提高得標的機率喔！預祝各位殺友們在殺戮戰場內武運昌隆！各位殺友都能成為殺價王！</p>
					</div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help19',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何查詢得標者？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help19" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>在【首頁】的【最新得標】，按下去以後即可查詢每一檔殺價商品（簡稱殺品）的得標者囉！點進去那一檔殺品，還能知道廣大殺友殺價是殺得多麽激烈。</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help20',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">殺品已得標如何結帳呢？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help20" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【殺價紀錄】，選擇【以得標】後可以看到您的得標殺品，在那檔殺品的右下角按下【結帳去】，完成結帳流程後就可以在家安心等殺品到手囉！</p> 
						<p>切記：得標三天內未結帳視為棄標，請得標的殺友們記得在時限內結帳喔！</p>              
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<!--遊戲規則-->
	
	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">訂單寄件</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help21',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何修改寄件人資訊</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help21" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【我的帳號】，按下【修改收件人資訊】，系統自動跳轉進入修改頁面，輸入完其內容後，按下確定，即可修改完成。</p>
                    </div>
                </div>
            </div>
        </div>
		
    </div>
<!--訂單寄件-->

	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">兌換</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help22',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何繳納電信費？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help22" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【鯊魚商城】後，在【生活代繳】類別，按下【電信費】後，進入【電信費代繳】頁面，輸入完資訊後按【下一步】（由殺友決定新增為常用資料與否），此時進入二次確認電信代繳資訊的頁面，按【下一步】後會要求用戶輸入兌換密碼，輸入完成後即扣點成功，申請代繳成功！電信費代繳需要7個工作天，請提早完成繳費申請以免延誤殺友的使用電話的權益喔！切記，繳費前請詳閱注意事項以免權益受損。</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help23',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何繳納信用卡費？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help23" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【鯊魚商城】後，在【生活代繳】類別，按下【信用卡費】後，進入【信用卡費代繳】頁面，在【信用卡機構】選擇殺友欲繳費的信用卡是哪間銀行，在【銷帳編號】內輸入您這張信用卡要繳到哪個帳戶的帳戶號碼，這樣才能順利完成您使用的信用卡的繳費。【繳費截止日期】是此張信用卡最後要繳納的截止時日，不在那天前繳的話，就逾期了。【繳費金額】為您預繳那多少錢給信用卡公司喔！另外請殺友們在輸入完資訊後，一定要多次確認資訊是正確的，以免造成信用卡繳款失敗的問題，若因殺友的失誤造成繳款失敗，殺價王平台無法幫親愛的殺友您負擔責任喔！，輸入完資訊後按【下一步】（由殺友決定新增為常用資料與否），此時進入二次確認信用卡費代繳資訊的頁面，按【下一步】後會要求用戶輸入兌換密碼，輸入完成後即扣點成功！申請代繳成功！</p>
						<p>備註：請詳閱【信用卡費代繳】頁面下方的注意事項，此事項涉及殺友的權益！非常重要一定要看喔！</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help24',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何兌換City Café？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help24" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【鯊魚商城】後，在【超商/便利店】類別，按下【City Café】後，進入【兌換商品】頁面，選擇您要兌換的數量，按【結算去】進入二次確認兌換商品的頁面，確認無誤後，按下【確認兌換】後會要求用戶輸入兌換密碼，輸入完成後即扣點成功！兌換成功！兌換成功的咖啡卡會在殺友的【票券/卡片】內喔！</p>
						<p>備註：兌換前，請先詳閱商品詳情。</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help25',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何兌換7-11 商品卡？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help25" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【鯊魚商城】後，在【超商/便利店】類別，按下【7-11 商品卡】後，進入【兌換商品】頁面，選擇您要兌換的數量，按【結算去】進入二次確認兌換商品的頁面，確認無誤後，按下【確認兌換】後會要求用戶輸入兌換密碼，輸入完成後即扣點成功！兌換成功！兌換成功的卡券會在殺友的【票券/卡片】內喔！</p>
						<p>備註：兌換前，請先詳閱商品詳情。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help26',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何兌換全家禮物卡？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help26" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【鯊魚商城】後，在【超商/便利店】類別，按下【全家禮物卡】後，進入【兌換商品】頁面，選擇您要兌換的數量，按【結算去】進入二次確認兌換商品的頁面，確認無誤後，按下【確認兌換】後會要求用戶輸入兌換密碼，輸入完成後即扣點成功！兌換成功！兌換成功的卡券會在殺友的【票券/卡片】內喔！</p>
						<p>備註：兌換前，請先詳閱商品詳情。</p>
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help27',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">兌換完的卡券會跑去哪？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help27" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【票券/卡片】，即可查詢您的卡券喔！每張卡券都可以查詢其詳細資料，點擊殺友欲查詢的卡券後，按右上角的【詳細資料】即可查詢。卡券兌換完畢後，按下使用完畢，即可將已經換過的卡片歸類為已兌換！</p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<!--兌換-->	
	
	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">殺品出貨</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help28',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">殺價王提供的殺品是否為正品呢？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help28" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>是的，殺價王嚴格把關貨源，上架商品皆為正版商品，且為正當管道取得，各個商品退換貨，於售後保固內所有的售後服務皆受原廠或指定經銷商負責。從不同的通路來源，會有不同的商品檢查標準，若已拆封為實體店家販賣，其為避免外觀有損傷。為避免紛爭，此流程是商品必須檢查流程，請勿擔心，商品並非二手商品。</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help29',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">得標、兌換商品可以要求變成現金或是補差價換商品嗎?</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help29" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>得標或兌換商品不得要求變現或是補差價換商品。供貨不足無法提供相同商品時將更換同等值商品或高於等值商品作為替換及補償。</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help30',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如果商品有功能問題的話有維修服務嗎?</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help30" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>商品於送達3天內出現商品功能問題可向殺價王客服聯繫並索取該商品之影本發票至該品牌或廠商進行後續保固維修之服務。殺價王無法提供送修服務喔！</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help31',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如果商品有外觀問題的話有維修服務嗎?</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help31" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價王提供商品為原廠出貨，一律由原廠認定外觀瑕疵維修與保固規範，殺價王不提供維修與替換或服務喔！</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help32',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">請問殺品預計出貨時間？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help32" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價王提供的殺品都為原廠公司正貨，因為渠道的不同以及統一管理出貨緣由，出貨時間為7個工作天。</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help33',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">請問為什麼一直沒拿到殺品？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help33" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價王提供給廣大殺友們一個安心的收貨方式，殺價王會在出貨的時候與您聯絡收貨地址與聯絡人，如果聯繫不到且出貨地址有問題的話，可能就會暫時不出貨，以避免您寶貴的商品遺失的情況發生。所以，若您遲遲沒收到貨的話，請聯繫殺價王LINE客服：@fgz1243t</p>              
                    </div>
                </div>
            </div>
        </div>
		
		<div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help34',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">請問超高單價的殺品，要怎麼領取呢？例如：機車、汽車、房子？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help34" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價王提供給廣大殺友們的夢幻級殺品的領取流程會有所不同，為了留下一個美好的紀念！所以需要殺友們跟殺價王一起接受採訪與活動舉辦，詳情請聯繫殺價王LINE客服：@fgz1243t</p>              
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<!--殺品出貨-->		
	
	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">發票</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help35',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何查詢購買殺價幣發票？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help35" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>進入【殺友專區】後，按下【購買殺價幣】，選擇【獲得】類別，選擇您歷史購買的殺價幣項次的發票，即可查詢。</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help36',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">購買殺價幣的發票中獎怎麼領回？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help36" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>當期發票開獎後，系統將會發信通知中獎發票的訂購人，且在10天內以掛號方式把中獎發票寄至給訂購人填寫的發票寄件地址，如您為使用手機條碼或自然人憑證條碼作為電子發票載具，請您也可以至電子發票整合服務平台『載具查詢消費發票』查詢中獎發票資料。</p>              
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<!--發票-->
	
	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">其他</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help37',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是得標處理費?</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help37" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>得標處理費是根據不同合作廠商有不同的處理費用以及人力、系統維護等費用，因此每個商品得標處理費用都不一樣，且在每個商品頁的下標規則及下標時都有清楚註明，請於下標前詳細觀看規則。</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help38',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">為什麼結標前顯示我是得標者但最後我卻沒有得標呢?</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help38" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>每台手機或電腦判斷秒數經常會不太一樣，因此多少都會有時間差，尤其在接近結標時下標者眾多的關係，雖然這一秒您看到目前得標者顯示是您，但可能因為時間差的關係，其實目前得標者已經換人了，建議您提早佈局，以免在接近結標前因為系統壅塞而失去得標的機會，最終得標者以【最新得標】公告為準。</p>              
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<!--其他-->

	<div class="saja_lists_box">
        <div class="header-title">
            <div class="title">新手相關</div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help39',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是殺價王？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help39" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>殺價王是一個提供先抽獎後消費的娛樂式購物平台，在這個平台你可以將你平常要花費的費用，先在殺價王用“殺價”的方式去競標一個商品，而沒得標的話，所花費的費用再去做消費；得標的話，就能用很低的價格購得您喜歡的商品喔！</p>
                        </div>
                </div>
            </div>
        </div>
        
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help40',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">誰會成為得標者？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help40" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>只要在結標時，您出的價格是最低價且只有你出這個價格，你就能成為得標者囉。</p>              
                    </div>
                </div>
            </div>
        </div>
        
    </div>
<!--新手相關-->

	<?php /*
	<div class="saja_lists_box">
	
        <div class="header-title">
            <div class="title">常見問題</div>
        </div>
        
        <!-- 我該如何用鯊魚點兌換商品？ -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help2',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">如何用鯊魚點兌換商品？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help2" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>鯊魚點可以兌換的服務如下：</p>
                        <p class="list-collapse-title">鯊魚商城</p>
                        <p>當你於線上兌換商品及服務時，只需在鯊魚商城中選擇數量並輸入兌換密碼即可。</p>
                        <p class="list-collapse-title">7-11 ibon兌換</p>
                        <p>當你於7-11兌換商品時須使用ibon系統，點選【好康紅利】>【網路遊戲會員】>殺價王 閱讀服務須知 點選【同意，繼續下一步】> 輸入帳號ID及密碼 > 點選【下一步】> 選擇兌換方式及產品，列印出條碼券後，即可至7-11櫃台掃碼領取商品！</p>
                        <p><font color="#E02020">*若想使用鯊魚點兌換商品需經過手機驗證後取得兌換密碼，即可進行兌換。</font>兌換密碼預設為【手機末六碼】，若想修改兌換密碼可至殺友專區進行設定。</strong></p>
                        <!--p class="list-collapse-title">叫車或實體服務</p>
                        <p>若需兌換叫車或其他實體、客製化服務時，需要主動告知商家你的會員編號(殺有專區 > 我的帳戶旁右方的數字)，待商家確認後，則會寄送驗證簡訊給你，並和商家核對收取到的驗證碼，商家確認無誤後即可抵扣鯊魚點享有服務。</p><-->
                        </div>
                </div>
            </div>
        </div>
        
        <!-- 什麼是得標處理費？ -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('help5',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">什麼是得標處理費？</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="help5" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>得標處理費是根據不同合作廠商有不同的處理費用以及人力、系統維護等費用，因此每個商品得標處理費用都不一樣，且在每個商品頁的<span class="bid-rule d-inlineflex align-items-center"><img src="<?PHP echo APP_DIR; ?>/static/img/rule.png">下標規則</span>都有清楚註明，請於下標前詳細觀看規則。</p>              
                        <P><font color="#E02020">*得標處理費可由已下標手續費100%全額折抵。</font>
                   </div>
                </div>
            </div>
        </div>
 
        
    </div>
	*/?>
	
    <?php if (empty($app)){ ?>
    <div class="help-bottomBtn-box">
        <div class="header-title">
            <div class="title">客服</div>
        </div>
        <!-- 問答紀錄(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">您的問答紀錄</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
        
        <!-- 聯繫客服(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/issues_add?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">聯繫客服</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <div class="faq-link-box">
        <!-- 新手教學(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
                <?php if($app=='Y') {?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/faq/?APP=Y'">
                <?php }else{ ?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/faq/?<?php echo $cdnTime; ?>'">
                <?php } ?>  
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">新手教學</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
        <p style=" margin-top: 12px; margin-left: 12px;">
            <a href="https://line.me/R/ti/p/%40fgz1243t" target="https://line.me/R/ti/p/%40fgz1243t" title="殺價王官方Line@" class="ui-link" style="color: #689AD9; text-decoration: underline;">如有問題，請私訊殺價王官方Line@</a>
        </p>



    </div>
    
</div>