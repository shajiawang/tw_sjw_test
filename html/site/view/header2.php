<?php
    $header = _v('header');
    if($header=='N') {
        return;
    }
    $noback = _v('noback');
?>	
<?php if (!is_weixin()) { ?>
	<div class="navbar sajaNavBar navbarOther d-flex align-items-center">
		<div class="search-box d-inlineflex align-items-center">
            <i class="esfas wcolor fas-search"></i>
			<form method="post" action="<?php echo BASE_URL.APP_DIR; ?>/product/getProductSearch/" id="contactform" name="contactform">
            <input class="search" type="search" name="prodKeyword" id="prodKeyword" value="" placeholder="殺價搜尋" data-mini="true" data-role="none"/>
			</form>
        </div>
        <div class="push_new">
            <!-- 20190425 推撥燈箱按鈕 -->
            <a href="javascript:;">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/message.png">
            </a>
        </div>
		<?php /*
		<div class="sajaNavBarIcon Burger">
            <!-- 20190425 選單 -->
            <a class="d-none" href="#left-panel<?php get_panel_id(); ?>" data-iconpos="right">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/hamburger.png">
            </a>
        </div>
		*/?>
    </div>
    <!-- 20190502推撥燈箱 -->
    <div class="th_pushmessage">
        <div>
            <header class="close_pushmessage">
                <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/icon_x-circle.png">
            </header>
            <div>
                <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/icon_pushlove.png">
            </div>
            <footer>
                <h4>推播資訊</h4> 
                <p>敬請期待</p>
                <a class="btn_back close_pushmessage" href="javascript:;"> 返回</a>
            </footer>
           
        </div>
    </div>
    <script>
        // 不是在微信  也不是在appbsl的app裏  才顯示Navigator bar
        $(document).ready(function() {
            if(is_kingkr_obj()) {
                // controllNavigateLayout(0);
            }
            //if(!is_kingkr_obj()) {
            $('#navibar').show();
            //}
        });
    </script>

    <!-- 往下捲動後 header 增加陰影 -->
    <script>
        $(document).ready(function() {
            var $header = $(".sajaNavBar"),
                $lastScrollY = $header.height();
            $(window).scroll(function(){
                var st = this.scrollY;
                // 捲動超過 header 才加陰影
                if( st < $lastScrollY) {
                    $header.removeClass("navShadow");
                }else{
                    $header.addClass("navShadow");
                }
            });
        });

        // 20190425 選單側推
        // $(window).on('load',function(){
        // //側邊攔
        //     var $panelBtn = $('.sajaNavBarIcon.Burger').find('a[href^="#left-panel"]');
        //     var $panelBox = $('.sajaPanel');
        //     var $panelClose = $('.sajaPanel').find('[data-rel="close"]');
        //     var $coverClose = $('.ui-panel-dismiss');
        //     $panelBtn.on('click', function(){
        //         if(!$panelBox.hasClass('.ui-panel-open')){
        //             $('body').addClass('modal-open');
        //             $('[data-role="page"]').find('[class$="-content"]').addClass('modal-open');
        //         }
        //     });
        //     $panelClose.on('click', function(){
        //         if(!$panelBox.hasClass('.ui-panel-closed')){
        //             $('body').removeClass('modal-open');
        //             $('[data-role="page"]').find('[class*="modal-open"]').removeClass('modal-open');
        //         }
        //     });
        //     $coverClose.on('click', function(){
        //         if(!$panelBox.hasClass('.ui-ui-panel-closed')){
        //             $('body').removeClass('modal-open');
        //             $('[data-role="page"]').find('[class*="modal-open"]').removeClass('modal-open');
        //         }
        //     })
        // })
    </script>

      <!-- 推播燈箱 -->
      <script>
        $(".push_new").click(function () { 
            $(".th_pushmessage").addClass('show_pushmessage');
        });
        $(".close_pushmessage").click(function () { 
           $(".th_pushmessage").removeClass('show_pushmessage');
        });
        $(".th_pushmessage").click(function () { 
           $(".th_pushmessage").removeClass('show_pushmessage');
        });
    </script>

<?php } ?>
