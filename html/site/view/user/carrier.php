<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$status = _v('status'); 
$user_profile = _v('user_profile'); 
?>
<div class="userEdit">

	<div class="required">
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="name">載具</div>
                <font class="carrier-font">殺價王會員載具</font>
            </li>
            <!--li class="input_group d-flex align-items-center">
                <div class="label" for="name">載具類別編號</div>
                <font class="carrier-font"><?php echo $user_profile['carrier_type'];?></font>
            </li-->
            <li class="input_group d-flex align-items-center">
                <div class="label" for="zip">載具是否歸戶</div>
                <font class="carrier-font"><?php if ($user_profile['carrier_aggregate']=='Y'){echo "是";}else{echo "否";}?></font>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="address">載具明碼</div>
                <font class="carrier-font"><?php echo $user_profile['carrier_id1'];?></font>
            </li>
            <!--li class="input_group d-flex align-items-center">
                <div class="label" for="rphone">載具隱碼</div>
                <font class="carrier-font"><?php echo $user_profile['carrier_id2'];?></font>
            </li-->
        </ul>
		<div style="margin:1rem;font-size:0.3rem;">
					<?php echo $user_profile['content'];?>
		</div>
		<div class="footernav">
            <div class="footernav-button">
				<?php if(!empty($user_profile['carrier_type']) && !empty($user_profile['carrier_id1']) && !empty($user_profile['carrier_id2']) && $user_profile['carrier_aggregate'] == 'Y'){ ?>
                <button type="button" id="address_btn" class="ui-btn ui-btn-a ui-corner-all" disabled>已完成歸戶</button>
				<?php }else{ ?>
				<button type="button" id="address_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/invoice/Aggregate/?<?php echo $cdnTime; ?>'">會員載具歸戶</button>
				<?php } ?>
			</div>
        </div>					
	</div>
	
	<!--div class="required">
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="phonecode">手機條碼</div>
                <input id="phonecode" name="phonecode" value="<?php echo $user_profile['phonecode'];?>" type="text" placeholder="請輸入手機條碼">
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="npcode">自然人憑證條碼</div>
                <input id="npcode" name="npcode" value="<?php echo $user_profile['npcode'];?>" type="text" placeholder="請輸入自然人憑證條碼">
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="donate_mark">是否捐贈</div>
                <font style="padding: .667rem;"><input type="radio" name="donate_mark" id="donate_mark" value="Y" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['donate_mark']=='Y'){echo "checked";}?>> 是 <input type="radio" name="donate_mark" id="donate_mark" value="N" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['donate_mark']=='N'){echo "checked";}?>> 否 </font>
			</li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="npcode">捐贈單位</div>
                <input id="npoban" name="npoban" value="<?php echo $user_profile['npoban'];?>" type="text" placeholder="請輸入愛心碼">
			</li>
		</ul>
		<div class="footernav">
            <div class="footernav-button">
                <button type="button" id="address_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="carrier_confirm();">確認修改</button>
			</div>
        </div>					
	</div-->
</div>