<ul class="input_group_box">
    <li class="input_group d-flex align-items-center">
        <div class="label" for="phone">手機號碼</div>
        <input id="phone" name="phone" value="" type="number" placeholder="請填寫註冊時使用的手機號碼">
    </li>
</ul>
<div class="footernav">
    <div class="footernav-button">
        <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="forget()">發送新密碼</button>
    </div>
</div>


<script>
    //調整標題寬度一致
    var options = {
        dom: {
            ul: 'ul.input_group_box',
            li: 'li.input_group',
            label: '.label',
            input: 'input',
            select: 'select'
        }
    }
    $(function(){
        //目前顯示的ul區塊
        var $isShow = $(options.dom.ul);
        
        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find(options.dom.li);
            var $selfLabel = $selfLi.find(options.dom.label);
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];

            $liW = $selfLi.width();                         //取得li總寬度   

            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })

            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                $selfInput.attr('style','');
                $selfSelect.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                    }
                })
                $selfSelect.each(function(){
                    if($selfSelect.length > 1){                 
                        $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                    }else{
                        $(this).width($remain);
                    }
                })
            })
        }
        $isShow.each(function(){
            changeWidth($(this));
        })

        //尺寸改變重新撈取寬度
        $(window).resize(function(){
            $isShow.each(function(){
                changeWidth($(this));
            })
        })

    })
</script>