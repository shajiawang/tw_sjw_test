<?php
    // 用Login ID, 不是 userid
    $MerchantAccount=$_SESSION['user']['name'];
    $act=_v("act");
    $GamaPayID=_v("GamaPayID");
?>
<style>
    .other-content {
        background: #FFF;
    }
</style>
   
    <div class="login-title login-tele-title">
	    <h1>橘子支付帳號驗證</h1>
	</div>
    <div class="phone-box">
       <div class="phone-icon">
            <div class="iconBox">
                <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/register-phone1.png" alt="">
            </div>
            <small>輸入橘子支帳號   系統將發送簡訊驗證碼</small>
        </div>
		<form method="post" onsubmit="return chk(this);" action="" id="theForm" name="theForm">
            <input type="text" class="input-field phone-input" id="GamaPayID" name="GamaPayID" value="<?php echo $GamaPayID; ?>" />
            <input type="hidden" id="Token" name="Token" value="" />
            <input type="hidden" id="act" name="act" value="<?php echo $act; ?>" />
            <input type="hidden" id="MerchantAccount" name="MerchantAccount" value="<?php echo $MerchantAccount; ?>" />
        </form>
    </div>
    <a class="send-btn" onclick="getSms();" >獲取驗證碼</a>

<!-- ============================= JS 控制 ============================= -->

<!--  CDN Bootstrap 4.1.1  -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script>

    function chk(theForm) {
            if(theForm.Token.value=='' || theForm.GamaPayID.value=='' || theForm.MerchantAccount.value=='') {
               return false;
            } else {
               return true;   
            }
    }
    //送出表單	
	function getSms(){
        
		var vid = $('#GamaPayID').val();
        var vid2 = $('#MerchantAccount').val();
        if(vid=='') {
           alert('橘子支付帳號不可為空 !!');
           return false;           
        }
       	$.post(
			'<?php echo BASE_URL; ?>/pay/gamapay/getTokenByOTP.php',
			{ 
              GamaPayID:vid, 
              MerchantAccount:vid2
            }, 
			function(r) {
                var ret =""; 
                // alert(r);                
                if((typeof r)=="string") {
                    ret = JSON.parse(r);    
                } else  if((typeof r)=="object") {
                    ret = r;
                }                
				if(ret.ResultCode == "0000") {
					alert('手機驗證碼發送成功');
                    $('#Token').val(ret.Token);
                    $('#theForm').attr("action","<?php echo BASE_URL.APP_DIR;?>/user/inputVerifyCode/").submit();
                    return;
                    // window.location.href="<?php echo BASE_URL.APP_DIR;?>/user/inputVerifyCode;
				} else {
                    alert(ret.ResultMessage);
                }                
				
			}
		);
	}
    
</script>
