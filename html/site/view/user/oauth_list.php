<?php
    $arrSSO=_v('sso_list');
    $MerchantOrderID=date('YmdHis');
    $MerchantMemberID=$_SESSION['user']['name'];
?>
<ul class="user-lists-box">
    <li class="user-list">
         <a href="javascript:void(0);" 
            class="user-list-link d-flex align-items-center" 
            <?php if(empty($arrSSO['gama']['uid2'])) {  ?>
                       onclick="javascript:handlePayAccount('bind','gama','');"
            <?php  } else {  ?> 
                       onclick="javascript:handlePayAccount('unbind','gama','<?php echo $arrSSO['gama']['uid2']; ?>');"   
            <?php  }  ?>
            >
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/membername.png">
                </div>
                <div class="list-title">橘子支付</div>
            </div>
            <?php if($arrSSO['gama']['uid2']) {  ?>
                <div><?php echo $arrSSO['gama']['uid2']; ?></div>
                <div class="list-ricon"></div>
            <?php  } else {   ?>
                <div>綁定</div>
                <div class="list-ricon"></div>
            <?php  }  ?>
        </a>
    </li>
</ul>
<script>
    function handlePayAccount(act,name,uid) {
        if(act=='bind') {
            window.location.href = '<?php echo BASE_URL.APP_DIR; ?>/user/bind_oauth/?sso_name='+name;   
        } else if (act=='unbind') {
            if(confirm('要解綁這個帳號嗎 ?'))
            　　window.location.href = '<?php echo BASE_URL.APP_DIR; ?>/user/unbind_oauth/?sso_name='+name+'&uid='+uid;             
        }
        return false;
    }
</script>
<!-- 暫時移除 從殺價王調起橘子支付的測試 
<ul class="user-lists-box">
    <li class="user-list">
        <a href="javascript:openAPPToPay()" class="user-list-link d-flex align-items-center">
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/membername.png">
                </div>
                <div class="list-title">調起橘子APP</div>
            </div>
        </a>
    </li>

</ul>
<script>
   function openAPPToPay() {
       var URLScheme='gashpay://payment?ReturnUrl=saja://firstpage&Type=3&ReceiverName=saja&Source=Vender&TransType=10&ScenesType=1&ReceiverLoginID=<?php echo $MerchantID; ?>';
       $.post(
           '<?php echo BASE_URL."/pay/gamapay/getTradingToken.php?MerchantOrderID=".$MerchantOrderID."&MerchantMemberID=".$MerchantMemberID."&AccountID=".$AccountID; ?>',
           function(r) {
                 alert(r);
                 var obj;
                 if((typeof r)=="string") {
                    obj=JSON.parse(r);
                 } else if((typeof r)=="object"){
                    obj = r;
                 } 
                 URLScheme=URLScheme+"&TradingToken="+obj.TradingToken;
                 window.location.href=URLScheme;                 
           }
       ); 

   }   
</script>
-->
<!-- 暫時移除 從殺價王調起橘子支付的測試 -->