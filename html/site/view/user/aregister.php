<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$status = _v('status'); 
?>
<div class="user-register">
    <ul class="input_group_box">
        <li class="input_group d-flex align-items-center">
            <div class="label" for="nickname">暱稱</div>
            <input id="nickname" name="nickname" value="" type="text" placeholder="請輸入暱稱">
        </li>
        <li class="input_group d-flex align-items-center">
            <div class="label" for="phone">手機號碼</div>
            <input id="phone" name="phone" value="" type="number" pattern="[0-9.]*" placeholder="殺友認證開通使用">
        </li>
        <li class="input_group d-flex align-items-center">
            <div class="label" for="pw">密碼</div>
            <input id="pw" name="passwd" value=""  type="password" placeholder="4~12個英文或數字為限">
        </li>
        <li class="input_group d-flex align-items-center">
            <div class="label" for="repw">密碼確認</div>
            <input id="repw" name="repasswd" value="" type="password" placeholder="請再次輸入密碼確認">
        </li>
    </ul>
    <div class="user_check d-flex justify-content-center align-items-center">
        <span class="align-middle">註冊代表你同意
        <a class="check_link" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/about/privacy/?<?php echo $cdnTime; ?>'">殺價王隱私權保護條款</a>
        和
        <a class="check_link" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/about/service/?<?php echo $cdnTime; ?>'">服務條款</a>
        以及
        <a class="check_link" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/about/usage/?<?php echo $cdnTime; ?>'">用戶使用規範</a>
        </span>
    </div>
    <div class="footernav">
        <div class="footernav-button">
            <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="aregistration();">註冊</button>
        </div>
    </div>
</div>



<script>
    //調整標題寬度一致
    var options = {
        dom: {
            ul: 'ul.input_group_box',
            li: 'li.input_group',
            label: '.label',
            input: 'input',
            select: 'select'
        }
    }
    $(function(){
        //目前顯示的ul區塊
        var $isShow = $(options.dom.ul);
        
        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find(options.dom.li);
            var $selfLabel = $selfLi.find(options.dom.label);
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];

            $liW = $selfLi.width();                         //取得li總寬度   

            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })

            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                $selfInput.attr('style','');
                $selfSelect.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                    }
                })
                $selfSelect.each(function(){
                    if($selfSelect.length > 1){                 
                        $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                    }else{
                        $(this).width($remain);
                    }
                })
            })
        }
        $isShow.each(function(){
            changeWidth($(this));
        })

        //尺寸改變重新撈取寬度
        $(window).resize(function(){
            $isShow.each(function(){
                changeWidth($(this));
            })
        })

    })
</script>