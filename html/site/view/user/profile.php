<?php 
$status = _v('status');
$user_profile = _v('user_profile'); 
$hide_qq=($_GET['showqq']=='Y'?'false':'true');
$hide_upd=($hide_qq=='false'?'true':'false');
$user_extrainfo_category = _v('user_extrainfo_category');
$user_extrainfo = _v('user_extrainfo');
$countryArr = _v('countryArr');
$regionArr = _v('regionArr');
$provinceArr = _v('provinceArr');
$channelArr = _v('channelArr');
$auth = _v('auth');
$user_enterprise_rt = _v('user_enterprise_rt');
?>

<ul class="user-lists-box">
    <?php if ($user_profile['countryid'] < 1 || $user_profile['regionid'] < 1 || $user_profile['provinceid'] < 1 || $user_profile['channelid'] < 1) { ?>
        <li class="user-list">
            <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/uarea/'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_memberAdd.png">
                    </div>
                    <div class="list-title">修改分區</div>
                </div>
                <div class="list-ricon"></div>
            </a>
        </li>
    <?php } ?>
    
    <li class="user-list">
        <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/upwd/'">
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_pwd.png">
                </div>
                <div class="list-title">修改登入密碼</div>
            </div>
            <div class="list-ricon"></div>
        </a>
    </li>
    
    <li class="user-list">
        <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/uepwd/'">
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_pxwd.png">
                </div>
                <div class="list-title">修改兌換密碼</div>
            </div>
            <div class="list-ricon"></div>
        </a>
    </li>
    
    <li class="user-list">
        <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/unopwd/'">
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_oppwd.png">
                </div>
                <div class="list-title">小額免密設定</div>
            </div>
            <div class="list-ricon"></div>
        </a>
    </li>
    
    <li class="user-list">
        <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/ufpwd/'">
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_nopwd.png">
                </div>
                <div class="list-title">忘記兌換密碼</div>
            </div>
            <div class="list-ricon"></div>
        </a>
    </li>
	
	<?php /*
	<li class="user-list">
			<a href="javascript:void(0);" <?php if( empty($user_profile['intro_by']) ){ ?>onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/user_referral/'" <?php } ?> class="user-list-link d-flex align-items-center" >
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/prodList_0.png">
                    </div>
                    <div class="list-title">我的推薦人</div>
                </div>
                <div class="list-rtxt d-inline-flex">
					<div class="r-arrow">
						<div class="rtxt-num" ><?php echo empty($user_profile['intro_by'])?'': '<span style="color:#ff0000">'.$user_profile['intro_by'].'</span>'. $user_profile['intro_name'];?></div>
					</div>
                </div>
			</a>
    </li>
	*/?>
    
    <?php if(!empty($user_extrainfo_category) ) { ?>
        <li class="user-list">
            <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/upackage/'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_cardpage.png">
                    </div>
                    <div class="list-title">我的卡片</div>
                </div>
                <div class="list-ricon"></div>
            </a>
        </li>
    <?php } ?>
    
    <li class="user-list">
        <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/uaddress/'">
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_memberaddress.png">
                </div>
                <div class="list-title">修改收件人資訊</div>
            </div>
            <div class="list-ricon"></div>
        </a>
    </li>

    <?php 
    // 通過手機驗證  saja_user.name (LoginID) 是手機號的才可以綁定其他帳號
       error_log("[v/profile] user session : ".json_encode($_SESSION['user']));
       if(is_numeric($_SESSION['user']['name']) && $_SESSION['user']['name']!='0921719956') {  
    ?>
    <li class="user-list">
         <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" 
            onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/oauth_list/'">
             <div class="list-titlebox d-flex align-items-center">
                 <div class="list-icon">
                     <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_lock.png">
                 </div>
                 <div class="list-title">綁定支付帳號</div>
             </div>
             <div class="list-ricon"></div>
         </a>
     </li> 
    <?php  }   ?>


	<?php if ($_SESSION['auth_id']=='1705'){ ?>
    <li class="user-list">
         <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" 
            onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/user_carrier/'">
             <div class="list-titlebox d-flex align-items-center">
                 <div class="list-icon">
                     <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_lock.png">
                 </div>
                 <div class="list-title">會員載具歸戶</div>
             </div>
             <div class="list-ricon"></div>
         </a>
     </li> 
    <!--li class="user-list">
         <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" 
            onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/user_phonecode/'">
             <div class="list-titlebox d-flex align-items-center">
                 <div class="list-icon">
                     <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_lock.png">
                 </div>
                 <div class="list-title">修改手機條碼</div>
             </div>
             <div class="list-ricon"></div>
         </a>
     </li> 
    <li class="user-list">
         <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" 
            onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/user_npcode/'">
             <div class="list-titlebox d-flex align-items-center">
                 <div class="list-icon">
                     <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_lock.png">
                 </div>
                 <div class="list-title">修改自然人憑證條碼</div>
             </div>
             <div class="list-ricon"></div>
         </a>
     </li-->
    <li class="user-list">
         <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" 
            onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/user_invoice/'">
             <div class="list-titlebox d-flex align-items-center">
                 <div class="list-icon">
                     <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_lock.png">
                 </div>
                 <div class="list-title">發票選項設定</div>
             </div>
             <div class="list-ricon"></div>
         </a>
     </li> 
	<?php } ?>
    <li class="user-list">
        <a href="javascript:void(0);" class="user-list-link d-flex align-items-center" onclick="logout();">
            <div class="list-titlebox d-flex align-items-center">
                <div class="list-icon">
                    <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/profile_exit.png">
                </div>
                <div class="list-title">登出</div>
            </div>
            <div class="list-ricon"></div>
        </a>
    </li>

</ul>