<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$gotourl=_v('gotourl');
$jdata=_v('jdata');
$canbid=_v('canbid');
$productid=_v('productid');
$behav=_v('behav');
$reg_type=_v('type');

if(empty($reg_type))
   $reg_type = $_SESSION['reg_type'];
   
$nickname="_guest";
$openid= $_SESSION['sso']["identifier"];
$unionid=$_SESSION['sso']['unionid'];
$sso_uid2=$_SESSION['sso']['unionid'];

$amp="?";
if(!empty($productid)) {
   $gotourl.=($amp."productid=".$productid);
   $amp="&";
}
if(!empty($user_src)) {
  $gotourl.=($amp."user_src=".$user_src);
  $amp="&";   
}
if(!empty($reg_type)) {
  $gotourl.=($amp."type=".$reg_type);
  $amp="&";  
}
if(!empty($behav)) {
  $gotourl.=($amp."behav=".$behav);
  $amp="&";  
}
if(!empty($canbid)) {
  $gotourl.=($amp."canbid=".$canbid);
  $amp="&";  
}

if (isset($_SESSION['sso']['nickname'])) { 
    $nickname=$_SESSION['sso']['nickname']; 
}

$gender="male";
if ($_SESSION['sso']['sex']==2)
    $gender="female"; 
else
    $gender="male";

$pwd=get_shuffle();

error_log("[v/sso_register] gotourl:".$gotourl);
error_log("[v/sso_register] default pwd:".$pwd);
error_log("[v/sso_register] jdata=".$jdata);
error_log("[v/sso_register] user_src=".$user_src);
error_log("[v/sso_register] type=".$reg_type);
error_log("[v/sso_register] behav=".$behav);
error_log("[v/sso_register] sso_uid=".$openid);
error_log("[v/sso_register] sso_uid2=".$unionid);
error_log("[v/sso_register] sso_data=".json_encode($_SESSION['sso']['sso_data']));
if(empty($openid)) {   // 非微信認證註冊
?>	
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
					<?php if(!empty($_SESSION['sso']['headimgurl'])) { ?>
					    <div align="center"><label><img width="100" height="100" src='<?php echo $_SESSION['sso']['headimgurl']; ?>' background-size="cover" style="border: 0px solid rgba(0, 0, 0, .35); border-radius: 50%; webkit-border-radius: 50%; moz-border-radius: 50%;"></label></div>
					<?php  }  ?>
				   	<label style="text-align:center;"><?php echo $nickname; ?>, 欢迎来到殺價王 !! </label>
					<label>&nbsp;</label>
                    <label for="phone">請輸入手機號碼(中標后身分認证用) : </label>
                    <input id="phone" name="phone" value="" data-clear-btn="true" type="number" pattern="[0-9.]*" placeholder="手機號碼">
                    <label>&nbsp;</label>
					<button type="button" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0" onClick="sso_register()">確認提交</button>
					<input type="hidden" name="thumbnail_url" id="thumbnail_url"  value="<?php echo $_SESSION['sso']['headimgurl']; ?>" />
					<input type="hidden" name="pw"  id="pw" value="<?php echo $pwd; ?>" />
					<input type="hidden" name="repw"  id="repw" value="<?php echo $pwd; ?>" />
					<input type="hidden" name="nickname" id="nickname" value="<?php echo $nickname; ?>" />
					<input type="hidden" name="gender" id="gender"  value="<?php echo $gender; ?>" />
					<input type="hidden" name="gotourl" id="gotourl" value="<?php echo $gotourl; ?>" />
					<input type="hidden" name="sso_name" id="sso_name" value="<?php echo $_SESSION['sso']['provider']; ?>"/>
					<input type="hidden" name="sso_uid" id="sso_uid" value="<?php echo $openid; ?>"/>					
					<input type="hidden" name="sso_uid2" id="sso_uid2" value="<?php echo $unionid; ?>"/>
					<input type="hidden" name="user_src" id="user_src" value="<?php echo $user_src; ?>" />
					<input type="hidden" name="type" id="type" value="<?php echo $reg_type; ?>" />
					<input type="hidden" name="behav" id="behav" value="<?php echo $behav; ?>" />
					<input type="hidden" name="productid" id="productid" value="<?php echo $productid; ?>" />
					<input type="hidden" name="language" id="language" value="<?php echo $_SESSION['sso']['language']; ?>" />
					<input type="hidden" name="city" id="city" value="<?php echo $_SESSION['sso']['city']; ?>" />
					<input type="hidden" name="province" id="province" value="<?php echo $_SESSION['sso']['province']; ?>" />
					<input type="hidden" name="country" id="country" value="<?php echo $_SESSION['sso']['country']; ?>" />
				</li>
            </ul>
        </div><!-- /article -->
<?php 
    } else if(!empty($openid)) {  
        $phone=$_SESSION['sso']['provider']."_".str_replace('.','',strval(microtime(true))); 
?>	
        <div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
				   	<?php if(!empty($_SESSION['sso']['headimgurl'])) { ?>
					    <div align="center"><label><img width="100" height="100" src='<?php echo $_SESSION['sso']['headimgurl']; ?>' background-size="cover" style="border: 0px solid rgba(0, 0, 0, .35); border-radius: 50%; webkit-border-radius: 50%; moz-border-radius: 50%;"></label></div>
					<?php  }  ?>
					<label style="text-align:center;"><?php echo $nickname; ?>, 欢迎来到殺價王 !! </label>
				</li>
				<li>
				    <label>&nbsp;</label>
				</li>
					<input type="hidden" name="phone" id="phone"  value="<?php echo $phone; ?>" >
					<input type="hidden" name="thumbnail_url" id="thumbnail_url"  value="<?php echo $_SESSION['sso']['headimgurl']; ?>" />
					<input type="hidden" name="pw"  id="pw" value="<?php echo $pwd; ?>" />
					<input type="hidden" name="nickname" id="nickname" value="<?php echo $nickname; ?>" />
					<input type="hidden" name="gender" id="gender"  value="<?php echo $gender; ?>" />
					<input type="hidden" name="gotourl" id="gotourl" value="<?php echo $gotourl; ?>" />
					<input type="hidden" name="sso_name" id="sso_name" value="<?php echo $_SESSION['sso']['provider']; ?>"/>
					<input type="hidden" name="sso_uid" id="sso_uid" value="<?php echo $openid; ?>"/>
					<input type="hidden" name="sso_uid2" id="sso_uid2" value="<?php echo $unionid; ?>"/>
					<input type="hidden" name="user_src" id="user_src" value="<?php echo $user_src; ?>" />
					<input type="hidden" name="type" id="type" value="<?php echo $reg_type; ?>" />
					<input type="hidden" name="behav" id="behav" value="<?php echo $behav; ?>" />
					<input type="hidden" name="productid" id="productid" value="<?php echo $productid; ?>" />
					<input type="hidden" name="language" id="language" value="<?php echo $_SESSION['sso']['language']; ?>" />
					<input type="hidden" name="city" id="city" value="<?php echo $_SESSION['sso']['city']; ?>" />
					<input type="hidden" name="province" id="province" value="<?php echo $_SESSION['sso']['province']; ?>" />
					<input type="hidden" name="country" id="country" value="<?php echo $_SESSION['sso']['country']; ?>" />
				</ul>
			</div><!-- /article -->
		<script>
		 var onickname = $('#nickname').val();
		 var ophone = $('#phone').val();
		 var osex = $('#gender').val();
		 var opw = $('#pw').val();
		 var ouser_src = $('#user_src').val();
		 var osso_name = $('#sso_name').val();
		 var osso_uid = $('#sso_uid').val();
		 var ogotourl=$('#gotourl').val();
		 var oproductid=$('#productid').val();
		 var othumbnail_url=$('#thumbnail_url').val();
		 var osso_uid2 = $('#sso_uid2').val();
		 var obehav=$('#behav').val();
		 var otype=$('#type').val();
		 
		 if(otype=='') {
		    otype='flash';
		 }
		 var ocity=$('#city').val();
		 var oprovince=$('#province').val();
		 var ocountry=$('#country').val();
		 var olang=$('#language').val();
	     $.post("/site/ajax/user_register.php?t="+getNowTime(), 
				{   type:otype, 
					thumbnail_url:othumbnail_url,
					user_src:ouser_src, 
					nickname:onickname, 
					gender:osex, 
					phone:ophone, 
					sso_name:osso_name, 
					sso_uid:osso_uid, 
					passwd:opw, 
					repasswd:opw, 
					expasswd:opw,
					exrepasswd:opw,
					productid:oproductid,
					behav:obehav,
					sso_uid2:osso_uid2,
					city:ocity,
					province:oprovince,
					country:ocountry,
					language:olang
				}, 
				function(str_json)
				{
					if(str_json)
					{
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						
						if (json.status < 1) {
							alert('註冊失败');
						}
						else {
							var msg = '';
							if (json.status==200){ 
								alert('註冊成功! 欢迎加入殺友行列! ');
								if(ogotourl!='') {
								   location.href = ogotourl;
								} else {
								   location.href = "/site/product/";
								}
								//location.href = "/site/user/profile/";
							} 
							else if(json.status==2){ alert('請填写暱稱'); } 
							else if(json.status==3){ alert('此暱稱已经有一样的了!'); } 
							else if(json.status==4){ alert('請填写手機號碼'); } 
							else if(json.status==5){ alert('手機號碼格式不正確'); } 
							else if(json.status==6){ alert('此手機號碼已存在'); }
							else if(json.status==7){ alert('請填写密碼'); }
							else if(json.status==8){ alert('密碼格式不正確'); }
							else if(json.status==9){ alert('請填写確認密碼'); }
							else if(json.status==10){ alert('密碼確認不正確'); }
							else if(json.status==11){ alert('兌換密碼格式不正確'); }
							else if(json.status==12){ alert('請填写兌換密碼確認'); }
							else if(json.status==13){ alert('兌換密碼確認不正確'); }
							else if(json.status==16){ alert('您已绑定过微信');history.back(); }
						}
					}
				}
		 );
	
		</script>
<?php  }  ?>
