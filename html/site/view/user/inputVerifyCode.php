<?php
   $token = _v('Token');
   $gamapayid=_v('GamaPayID');
   $merchantaccount=_v('MerchantAccount');
   $act=_v('act');
?>
<style>
    .other-content {
        background: #FFF;
    }
</style>
    <div class="login-title login-tele-title">
	    <h1>橘子支付帳號驗證</h1>
	</div>
    <div class="phone-box">
        <div class="phone-icon">
            <div class="iconBox">
                <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/register-phone1.png" alt="">
            </div>
            <small>輸入驗證碼</small>
        </div>
		<form method="post" onsubmit="chk(this);" action="" id="contactform" name="contactform">
            <input type="text" class="input-field" id="VerifyData" name="VerifyData" >
            <input type="hidden" class="input-field" id="act" name="act" value="<?php echo $act; ?>" >
            <input type="hidden" class="input-field" id="Token" name="Token" value="<?php echo $token; ?>" >
            <input type="hidden" class="input-field" id="GamaPayID" name="GamaPayID" value="<?php echo $gamapayid; ?>" >
            <input type="hidden" class="input-field" id="MerchantAccount" name="MerchantAccount" value="<?php echo $merchantaccount; ?>" >
        </form>
    </div>
    <a class="send-btn" onclick="verify();" >提交驗證</a>

<!-- ============================= JS 控制 ============================= -->

<!--  CDN Bootstrap 4.1.1  -->
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
<script>

    function chk(theForm) {
       return false;
        
    }
    
	//送出表單	
    var act = $('#act').val();
	function verify(){
        var verifyData = $('#VerifyData').val();
        var gamaPayID = $('#GamaPayID').val();
        var f=document.getElementById('contactform');
        
        if(f.Token.value=='') {
           alert("Token不可為空 !!");
           return false;
        } 
        if(f.VerifyData.value=='') {
           alert("驗證碼不可為空 !!");
           return false;
        }        
        if(f.GamaPayID.value=='') {
           alert("橘子支付帳號不可為空 !!");
           return false;
        } 
        if(f.act.value=='') {
           alert("act不可為空 !!");
           return false;
        }         
		$.post(
			'<?php echo BASE_URL; ?>/pay/gamapay/verifyTokenByOTP.php',
			{ 
              VerifyData: $('#VerifyData').val()
              ,Token:$('#Token').val()
             }, 
			function(r) {
                var ret ="";    
                if((typeof r)=="string") {
                    ret = JSON.parse(r);    
                } else  if((typeof r)=="object") {
                    ret = r;
                }                
				if(ret.ResultCode == "0000") {
					switch(act) {
                        case 'unbind':
                              unbindPayAccount();
                              break;
                        case 'bind' :                              
                        default: bindPayAccount();
                                 break;
					}
				} else {
                    alert("驗證失敗 : "+ ret.ResultMessage);
                    window.location.href="<?php echo BASE_URL.APP_DIR; ?>/member/"; 
                    return;
                }                
				
			}
		);
	}
    
    function bindPayAccount() {
         $.post(
                '<?php echo BASE_URL; ?>/pay/gamapay/createLink.php',
                {
                  MerchantAccount:"<?php echo $merchantaccount; ?>",
                  Token:$('#Token').val(),
                  GamaPayID:"<?php echo $gamapayid; ?>"
                },
                function(r2) {
                    var obj="";
                    if((typeof r2)=="string") {
                        obj = JSON.parse(r2);    
                    } else  if((typeof r2)=="object") {
                        obj = r2;
                    } 
                    if(obj.ResultCode=='0000') {
                        // 將取得的虛擬帳號儲存到DB
                        // uid 為橘子會員帳號
                        // uid2 為橘子會員帳號綁定後的虛擬帳號
                        $.post(
                            '<?php echo BASE_URL.APP_DIR; ?>/oauthapi/bindPayAccount/',
                            {
                                sso_name:"gama",
                                uid2:"<?php echo $gamapayid; ?>",
                                uid:obj.GamaPayAccount,
                                MerchantAccount:"<?php echo $merchantaccount; ?>",
                                userid:"<?php echo $_SESSION['auth_id']; ?>"
                            },
                            function(rr) {
                                var obj="";
                                if((typeof rr)=="string") {
                                    obj = JSON.parse(rr);    
                                } else  if((typeof rr)=="object") {
                                    obj = rr;
                                } 
                                if(obj.retCode=='1') {
                                   alert("橘子支付帳號 : <?php echo $gamapayid; ?> 綁定完成!!");
                                   window.location.href="<?php echo BASE_URL.APP_DIR; ?>/member/";
                                } else {
                                   alert("綁定失敗 : "+obj.retMsg);
                                   window.location.href="<?php echo BASE_URL.APP_DIR; ?>/user/oauth_list/";                                    
                                }
                               
                            }
                        );
                    } else {
                        alert("綁定失敗 : "+obj.ResultMessage);
                        window.location.href="<?php echo BASE_URL.APP_DIR; ?>/user/oauth_list/"; 
                    }
                }
           );
           return false;
    }
    
    function unbindPayAccount() {
           $.post(
                '<?php echo BASE_URL; ?>/pay/gamapay/cancelLink.php',
                {
                  MerchantAccount:"<?php echo $merchantaccount; ?>",
                  Token:$('#Token').val()
                },
                function(r2) {
                    var obj="";
                    if((typeof r2)=="string") {
                        obj = JSON.parse(r2);    
                    } else  if((typeof r2)=="object") {
                        obj = r2;
                    } 
                    if(obj.ResultCode=='0000') {
                        // 將虛擬帳號刪除
                        // uid 為橘子會員帳號
                        // uid2 為橘子會員帳號綁定後的虛擬帳號
                        // alert(r2);
                        $.post(
                            '<?php echo BASE_URL.APP_DIR; ?>/oauthapi/unbindPayAccount/',
                            {
                                sso_name:"gama",
                                uid2:"<?php echo $gamapayid; ?>",
                                uid:obj.GamaPayAccount,
                                MerchantAccount:"<?php echo $merchantaccount; ?>",
                                userid:"<?php echo $_SESSION['auth_id']; ?>"
                            },
                            function(rr) {
                                var obj="";
                                if((typeof rr)=="string") {
                                    obj = JSON.parse(rr);    
                                } else  if((typeof rr)=="object") {
                                    obj = rr;
                                } 
                                if(obj.retCode=='1') {
                                   alert("已解除綁定 <?php echo $gamapayid; ?> !!");
                                   window.location.href="<?php echo BASE_URL.APP_DIR; ?>/member/";
                                } else {
                                   alert(obj.retMsg);   
                                }
                            }
                        );
                        
                    } else {
                        alert("解綁失敗 : " +obj.ResultMessage);
                        window.location.href="<?php echo BASE_URL.APP_DIR; ?>/member/"; 
                    }
                }
           );
           return false;
    }
</script>