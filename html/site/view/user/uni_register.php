<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
?>
	<form method="post" action="" id="contactform" name="contactform">

	<div class="weui-cellS">
		<div class="weui-cell">
			<div class="weui-cell__hd sajapay-input" style="width:30px;height:100%;">
				<?php if (is_weixin()) { ?>
					<input type="radio" name="kind" id="radio1" value="user" onClick="javascript:changAction('user');" style="width:18px;height:18px"/>
				<?php } else { ?>		
					<input type="radio" name="kind" id="radio1" value="noweixin" onClick="javascript:changAction('noweixin');" style="width:18px;height:18px">
				<?php } ?>	
			</div>
			<div class="weui-cell__bd"><font class="sajabid-font3">一般用户</font></div>
		</div>	
		<div class="weui-cell">
			<div class="weui-cell__hd sajapay-input" style="width:30px;height:100%;">
				<input type="radio" name="kind" id="radio2" value="spoint" onClick="javascript:changAction('store');" style="width:18px;height:18px">
			</div>
			<div class="weui-cell__bd"><font class="sajabid-font3">商家用户</font></div>
		</div>			
	</div>

	<div class="nav">
		<div class="weui-tabbar" style="background-color:#FFFFFF">
			<div style="width:3%;"> </div>
			<div style="width:94%; margin:0 auto;" >
				<button type="button" id="topay" onClick="javascript:register();" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0px 0 ">確認提交</button>
			</div>
			<div style="width:3%;"> </div>
		</div>			
	</div>

<!-- /article -->
	</form>

<script type="text/javascript">
	function changAction(n){
		if( n=="user" ) {
			$("#contactform").attr("action", "<?php echo BASE_URL.APP_DIR; ?>/user/weixinapi/?u=<?php echo $user_src; ?>");
		} else if( n=="store" ) {
			$("#contactform").attr("action", "<?php echo APP_DIR; ?>/enterprise/member_register/?user_src=<?php echo $user_src; ?>");
		} else if( n=="noweixin" ) {
			$("#contactform").attr("action", "");
		}
	}

	function register() {
		var uniurl = $('input[type="radio"][name="kind"]:checked').val();  
		if (uniurl == "noweixin"){
			alert('从殺價王微信公众號註冊');
		} else {
			document.getElementById("contactform").submit();
		}
	}
</script>