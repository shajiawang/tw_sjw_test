<style>
    .other-content {
        background: #FFF;
    }
</style>
   
    <div class="login-title login-tele-title">
	    <h1>橘子支帳號驗證</h1>
	</div>
    <div class="phone-box">
        <div class="phone-icon">
            <div class="iconBox">
                <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/register-phone1.png" alt="">
            </div>
            <small>輸入橘子支付帳號</small>
        </div>
		<form method="post" action="" id="contactform" name="contactform">
            <input type="text" class="input-field" id="GamaPayId" name="GamaPayId" >
        </form>
    </div>
    <a class="send-btn" onclick="getSms();" >獲取驗證碼</a>

<!-- ============================= JS 控制 ============================= -->

<!--  CDN Bootstrap 4.1.1  -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<script>

	//送出表單	
	function getSms(){
		var vid = $('#GamaPayId').val();
 		$.post(
			'<?php echo BASE_URL; ?>/pay/gamapay/getTokenByOTP.php',
			{GamaPayId:vid, userid:"<?php echo $_SESSION['auth_id']; ?>"}, 
			function(r) {
                var ret ="";    
                if((typeof r)=="string") {
                    ret = JSON.parse(r);    
                } else  if((typeof r)=="object") {
                    ret = r;
                }                
				if(ret.ResultCode == "0000") {
					alert('手機驗證碼發送成功');
					window.location.href="<?php echo BASE_URL.APP_DIR;?>/verified/register_captcha/?phone="+ophone;
				} else {
                    alert(ret.ResultMessage);
                }                
				
			}
		);
	}
    
</script>
    

<!--  驗證訊息  -->
<script type="text/javascript">
    var array;
    function setError(msg){
        array.push('<li>'+msg+'</li>');
    };
    //驗證錯誤訊息彈窗
    var $ErrorModal = $(".errorModal .modal-content"),
        $ModalBg = $(".errorModalBg"),
        $ErrorBtn = $(".close_btn");
    function openErrorModal() {
        $ErrorModal.show();
        $ModalBg.show();
    }
    function closeErrorModal() {
        $ErrorModal.hide();
        $ModalBg.hide();
    }
    $ErrorBtn.on("click",function(){
        closeErrorModal();
    })

</script>




