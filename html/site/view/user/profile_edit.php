<?php 
$status = _v('status');
$useview = _v('useview');
$user_profile = _v('user_profile'); 
$hide_qq=($_GET['showqq']=='Y'?'false':'true');
$hide_upd=($hide_qq=='false'?'true':'false');
$user_extrainfo_category = _v('user_extrainfo_category');
$user_extrainfo = _v('user_extrainfo');
$countryArr = _v('countryArr');
$regionArr = _v('regionArr');
$provinceArr = _v('provinceArr');
$channelArr = _v('channelArr');
$auth = _v('auth');
$user_enterprise_rt = _v('user_enterprise_rt');

?>
<div class="userEdit">

<?php /*<div <?php if ($useview != "user_referral"){ ?> style="display:none;" <?php } ?> >
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="introby">設定推薦人ID</div>
                <input id="introby" name="introby" value="<?php echo $user_profile['intro_by'];?>" type="tel" placeholder="請輸入推薦人ID">
            </li>
        </ul>
        <div class="footernav">
            <div class="footernav-button">
                <button type="submit" id="affiliate_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="gift_affiliate();">確認提交</button>
            </div>
        </div>
	</div> */ ?>
 
	<div <?php if ($useview != "uname"){ ?> style="display:none;" <?php } ?> class="required">
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="nickname">暱稱</div>
                <input id="nickname" name="nickname" value="<?php echo $user_profile['nickname'];?>" type="text" placeholder="請輸入暱稱">
            </li>
        </ul>
        <div class="footernav">
            <div class="footernav-button">
                <button type="submit" id="nickname_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="user_nickname();">確認修改</button>
            </div>
        </div>
	</div>

	<?php if ($user_profile['countryid'] < 1 || $user_profile['regionid'] < 1 || $user_profile['provinceid'] < 1 || $user_profile['channelid'] < 1) { ?>
	<div <?php if ($useview != "uarea"){ ?> style="display:none;" <?php } ?>>       
        <ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="countryid">國家</div>
                <select name="countryid" id="countryid" onchange="country(this.value);">
                    <option value="">請選擇</option>
						<?php
							foreach ($countryArr as $key => $value) {
								if ($user_profile['countryid'] == $value['countryid']) {
									echo '<option value="'.$value['countryid'].'" selected>'.$value['name'].'</option>';
								} else {
									echo '<option value="'.$value['countryid'].'">'.$value['name'].'</option>';
								}
							}
						?>
				</select>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="provinceid">省/直轄市</div>
                <select name="provinceid" id="provinceid" onchange="province(this.value);">
                    <option value="">請選擇</option>
						<?php
							foreach ($provinceArr as $key => $value) {
								if ($user_profile['provinceid'] == $value['provinceid'] && $user_profile['countryid'] > 0 && $user_profile['regionid'] > 0) {
									echo '<option rel="'.$value['countryid'].'" value="'.$value['provinceid'].'" selected>'.$value['name'].'</option>';
								} else {
									echo '<option rel="'.$value['countryid'].'" value="'.$value['provinceid'].'">'.$value['name'].'</option>';
								}
							}
						?>
				</select>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="channelid">分區</div>
                <select name="channelid" id="channelid">
                    <option value="">請選擇</option>
						<?php
							foreach ($channelArr as $key => $value) {
								if ($user_profile['channelid'] == $value['channelid'] && $user_profile['countryid'] > 0 && $user_profile['regionid'] > 0 && $user_profile['provinceid'] > 0) {
									echo '<option rel="'.$value['provinceid'].'" value="'.$value['channelid'].'" selected>'.$value['name'].'</option>';
								} else {
									echo '<option rel="'.$value['provinceid'].'" value="'.$value['channelid'].'">'.$value['name'].'</option>';
								}
							}
						?>
				</select>
            </li>
            <input id="regionid" type="text" name="regionid" value="" style="display:none">
        </ul>
        <div class="footernav">
            <div class="footernav-button">
                <button type="submit" class="ui-btn ui-btn-a ui-corner-all" onClick="channel_modify();">確認修改</button>
            </div>
        </div>					
	</div>
	<?php } ?>
	
	<div <?php if ($useview != "upwd"){ ?> style="display:none;" <?php } ?> class="required">
        <ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="oldpw">舊密碼</div>
                <input name="oldpw" id="oldpw" value="" type="password" placeholder="請輸入舊密碼">
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="newpw">新密碼</div>
                <input name="newpw" id="newpw" value="" type="password" placeholder="以4~12個英文字母或數字為限">
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="newrepw">密碼確認</div>
                <input name="newrepw" id="newrepw" value="" type="password" placeholder="請再次輸入密碼確認">
            </li>
        </ul>
        <div class="footernav">
            <div class="footernav-button">
                <button type="button" id="pw_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="user_pw();">確認修改</button>
            </div>
        </div>			
	</div>
	
	<div <?php if ($useview != "uepwd"){ ?> style="display:none;" <?php } ?> class="required">
	    <ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="oldexpw">舊密碼</div>
                <input name="oldexpw" id="oldexpw" value="" class="password" type="tel" pattern="[0-9]" onkeyup="value=value.replace(/[^\d]/g,'')" placeholder="請輸入舊密碼">
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="newexpw">新密碼</div>
                <input name="newexpw" id="newexpw" value="" class="password" type="tel" pattern="[0-9]" onkeyup="value=value.replace(/[^\d]/g,'')" placeholder="以6個數字為限">
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="newrepw">密碼確認</div>
                <input name="newexrepw" id="newexrepw" value="" class="password" type="tel" pattern="[0-9]" onkeyup="value=value.replace(/[^\d]/g,'')" placeholder="請再次輸入密碼確認">
            </li>
        </ul>
        <div class="precautions text-right">兌換密碼未修改前默認為(帳號)或(驗證手機號)末6碼</div>
        <div class="footernav">
            <div class="footernav-button">
                <button type="button" id="expw_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="user_expw();">確認修改</button>
            </div>
        </div>					
	</div>			

	<div <?php if ($useview != "unopwd"){ ?> style="display:none;" <?php } ?> class="required">
	    <ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" class="bonus_noexpw" for="bonus_noexpw">免密兌換額度</div>
                <input name="bonus_noexpw" id="bonus_noexpw" value="<?php echo round($user_profile['bonus_noexpw']); ?>" type="number" pattern="[0-9]" onkeyup="value=value.replace(/[^\d]/g,'')" placeholder="請輸入免密兌換額度" >
            </li>
        </ul>
        <div class="precautions text-right">每次兌換低於此額度 免輸入兌換密碼</div>
        <div class="footernav">
            <div class="footernav-button">
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="bonus_noexpw();">確認修改</button>
            </div>
        </div>
	</div>	

	<div <?php if ($useview != "ufpwd"){ ?> style="display:none;" <?php } ?> class="required">
	    <?php if ($auth['verified'] == 'Y') { ?>
            <ul class="input_group_box">
                <li class="input_group d-flex align-items-center">
                    <div class="label" for="phone">手機號碼</div>
                    <input name="phone" id="phone" value="" type="tel" placeholder="請輸入手機號碼" >
                </li>
            </ul>
            <div class="footernav">
                <div class="footernav-button">
                    <button type="button" id="phone_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="forgetexchange();">發送新密碼</button>
                </div>
            </div>
		<?php }else{ ?>
            <div class="nohistory-box">
                <div class="nohistory-txt">請先進行手機驗證</div>
            </div>
            <div class="footernav">
                <div class="footernav-button">
                    <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="javascript:location.href='http://www.shajiawang.com/site/member/'">前去驗證</button>
                </div>
            </div>					
		<?php } ?>				
	</div>			
	
	<?php if(!empty($user_extrainfo_category) ) { ?>
	    <div <?php if ($useview != "upackage"){ ?> style="display:none;" <?php } ?>>
			<?php foreach($user_extrainfo_category as $uk => $uv) {
					$flagArr = str_split($uv['flag']);
					
					switch($uv['uecid']) {
				    case '2':
					     $iconname='cardpage-school';
						 break;
					case '3':
					     $iconname='cardpage-alipay';
						 break;
					case '7':
					     $iconname='cardpage-yidom';
						 break;
					default :
					     $iconname='card';
					}	 
			?>
			<ul class="input_group_box">
                <li class="input_group d-flex align-items-center" onClick="ReverseDisplay('capo<?php echo $uv['uecid'];?>',this);">
                    <div class="input_group_titlebox d-inlineflex align-items-center">
                        <div class="input_group_title"><?php echo $uv['name']; ?></div>
                    </div>
                    <div class="saja_list_rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </li>
            </ul>				
            <div id="capo<?php echo $uv['uecid'];?>" class="linkBox">
			    <div>
			        <ul class="input_group_box">
				    <?php
                        if (!empty($flagArr)) {
                            foreach ($flagArr as $fk => $fv) {
                                $val = 'field'.($fk + 1).'name';
                                if ($fv) {
                                    if ($uv['uecid'] == 1 && $fk == 0){
                                        $title = substr($uv[$val], 0, 9)."</br>".substr($uv[$val], 9, 16);
                                    }else{
                                        $title = $uv[$val];
                                    }
				    ?>
                        <li class="input_group d-flex align-items-center">
                            <div class="label" for="<?php echo 'uecfield_'.$uv['uecid'].'_'.$val; ?>"><?php echo $title; ?></div>
                            <?php
								include_once('/var/www/html/site/lib/school.lib.php');
								
								$City_Array = getCity();
								$School_Array = getSchool();
								
								if ($uv['uecid'] == 2 && $uv[$val] == '所属學校') {
									echo '
									<select name="city">';
											foreach ($City_Array as $CityKey => $CityValue) {
												echo '
										<option value="'.$CityKey.'">'.$CityValue.'</option>';
											}
											echo '
									</select>
									<select name="school">';
											foreach ($School_Array[array_shift(array_keys($City_Array))] as $SchoolKegy => $SchoolValue) {
												echo '
												<option value="'.$SchoolKey.'">'.$SchoolValue.'</option>';
											}
											echo '
									</select>';
								} else { 
							?>
                            <input name="uecfield[<?php echo $uv['uecid']; ?>][<?php echo $val; ?>]" id="<?php echo 'uecfield_'.$uv['uecid'].'_'.$val; ?>" value="<?php echo $user_extrainfo[$uv['uecid']][$val];?>" type="text" placeholder="<?php echo $uv[$val]; ?>" >
                            <?php } ?>
                        </li>
				    <?php
									} 
								}
						if (!$user_extrainfo[$uv['uecid']]) {		
					?>
						
						<input name="uecid[<?php echo $uv['uecid']; ?>]" type="hidden" value="<?php echo $uv['uecid']; ?>" />
						<input name="uecname[<?php echo $uv['uecid']; ?>]" type="hidden" value="<?php echo $uv['name']; ?>" />
						<div class="footernav">
							<div class="footernav-button">
								<button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="extrainfo();">確認</button>
							</div>
						</div>					
					<?php			
							} 
						} 
					?>	
				    </ul>
				</div>
			
            </div>
			<?php }  ?>

					
	    </div>			
	<?php } ?>
	
	<div <?php if ($useview != "uaddress"){ ?> style="display:none;" <?php } ?> class="required">
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="name">收件人姓名</div>
                <input name="name" id="name" value="<?php echo $user_profile['addressee']; ?>" type="text" placeholder="郵寄需要，請輸入真實姓名"  >
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="zip">收件人郵遞區號</div>
                <input name="zip" id="zip" value="<?php echo $user_profile['area']; ?>" type="tel" maxlength="7" placeholder="請輸入正確完整的郵遞區號" >
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="address">收件人地址</div>
                <input name="address" id="address" value="<?php echo $user_profile['address']; ?>" type="text" placeholder="請輸入正確的收件人地址" >
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="rphone">收件人電話</div>
                <input name="rphone" id="rphone" value="<?php echo $user_profile['rphone']; ?>" type="tel" placeholder="請輸入正確的收件人電話" >
            </li>
        </ul>
        <div class="precautions text-right">若因地址填寫錯誤造成商品寄送遺失，須自行負責後續責任</div>
		<div class="footernav">
            <div class="footernav-button">
                <button type="button" id="address_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="profile_confirm();">確認修改</button>
            </div>
        </div>					
	</div>
</div>

	<?php if (!empty($useview)) { ?>		
    <script>
        var selectArea = {
            dom: {
                country: 'select#countryid',
                region: 'select#regionid',
                province: 'select#provinceid',
                channel: 'select#channelid'
            }
        }
        var $country = $(selectArea.dom.country),
            $province = $(selectArea.dom.province),
            $channel = $(selectArea.dom.channel);
        
        var $countryOp = $country.find('option'),
            $provinceOp = $province.find('option'),
            $channelOp = $channel.find('option');
        
    	//省(直轄市)
		function country(id) {
            var $id = id;
			$province.val("");
            $provinceOp.each(function(){
                var $that = $(this);
                $that.hide();
                var $rel = $that.attr('rel');
                if($rel == undefined || $rel == $id){
                    $that.show();
                }
            })
            var $provinceItem = $province.find('option[style="display: block;"][rel="'+$id+'"]').eq(0),
                $provinceTxt = $provinceItem.text(),
                $provinceVal = $provinceItem.val();
            if($country.val()!==''){
                $province.parent().find('span').text($provinceTxt);	
                $("#regionid").val('8');
                $province.val($provinceVal);
                province($provinceVal)
            }
		}
	    
	    //省(直轄市)(台灣版分區拿掉)
//		function region(id) {
//            var $id = id;
//            console.log($id);
//            //$province.val("");
//            $provinceOp.each(function(i,item){
//                var $that = $(this);
//                var $rel = $that.attr('rel');
//                if($rel == undefined || $rel == $id){
//                    $that.show();
//                }
//            })
//            var $provinceItem = $province.find('option[style="display: block;"][rel="'+$id+'"]').eq(0),
//                $provinceTxt = $provinceItem.text(),
//                $provinceVal = $provinceItem.val();
//            //console.log($changeVal);
//            if($region.val()!==''){
//                $province.parent().find('span').text($provinceTxt);
//                $provinceOp.val($provinceVal);
//            }
//			$('select[name="provinceid"] option').hide();
//			$('select[name="provinceid"]').val("");
//			$('select[name="provinceid"] option[rel=""]').show();
//			$('select[name="provinceid"] option[rel="'+id+'"]').show();
//			$('select[name="provinceid"]').parent().find('span').text($('select[name="provinceid"] option[style="display: block;"]').eq(0).text());
//			
//			province($('select[name="regionid"] option').val());
//            
//            $provinceOp.val($provinceVal);
//            $channelOp.val($provinceVal);
//		}
		
		//經銷商(分區)
		function province(id) {
            var $id = id;
            $channel.val("");
            $channelOp.each(function(){
                var $that = $(this);
                var $rel = $that.attr('rel');
                if($rel == undefined || $rel == $id){
                    $that.show();
                }
            })
            var $channelItem = $channel.find('option[style="display: block;"][rel="'+$id+'"]').eq(0),
                $channelTxt = $channelItem.text(),
                $channelVal = $channelItem.val();
            if($country.val()!=='' && $province.val()!==''){
                $channel.parent().find('span').text($channelTxt);	
                $channel.val($channelVal);
            }
		}
    	
    	$(document).ready(function() {
    		var schoolArr = <?php echo json_encode($School_Array); ?>;
   		
    		$('select[name="city"]').change(function(e) {
				var cityid = $('select[name="city"] option:selected').val();
				var schoolhtml = '';
				
				for (var key in schoolArr[cityid]){
					schoolhtml += '<option value="' + key + '">' + schoolArr[cityid][key] + '</option>';
				}
				
				$('select[name="school"]').html(schoolhtml).selectmenu('refresh');
			});
            
			$('select[name="regionid"] option').hide();
            $('select[name="provinceid"] option').hide();
            $('select[name="channelid"] option').hide();
		});
	</script>
	
	<script>
        //調整標題寬度一致
        var options = {
            dom: {
                ul: 'ul.input_group_box',
                li: 'li.input_group',
                label: '.label',
                input: 'input',
                select: 'select'
            }
        }
        $(function(){
            //目前顯示的ul區塊
            var $isShow = $('.userEdit > div >'+ options.dom.ul).has(':visible');
            var $cap = $("div[id^='capo']").find($(options.dom.ul).has(':visible'));
            
            //取得應該顯示的寬度
            function changeWidth(dom) {
                var $selfLi = dom.find($(options.dom.li));
                var $selfLabel = $selfLi.find($(options.dom.label));
                
                //初始化
                $selfLabel.attr('style','');
                
                var $liW = '';
                var $arr = [];

                $liW = $selfLi.width();                         //取得li總寬度   

                $selfLabel.each(function(){                     //取得label寬度並存入陣列
                    $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
                });
                var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
                var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

                //改變寬度
                $selfLabel.each(function(){
                    $(this).css('width',$maxW);
                })
                
                $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                    var $selfInput = $(this).find($(options.dom.input));
                    var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                    $selfInput.attr('style','');
                    $selfSelect.attr('style','');
                    $selfInput.each(function(){
                        if($selfInput.length > 1){
                            $(this).width(($remain / $selfInput.length));
                        }else{
                            $(this).width($remain);
                        }
                    })
                    $selfSelect.each(function(){
                        if($selfSelect.length > 1){                 
                            $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                        }else{
                            $(this).width($remain);
                        }
                    })
                })
            }
            $isShow.each(function(){
                changeWidth($(this));
            })
            $cap.each(function(){
                changeWidth($(this));
                $(this).parents("div[id^='capo']").hide();
            })
        })
    </script>
    
    <!-- 欄位驗證 -->
    <script>
        $(function(){
			//推薦人
            <?php /* if ($useview == "user_referral"){ ?>
			var intro1=$('#introby').val();
			var intro2='<?php echo $user_profile['intro_by'];?>';
			console.log(intro1 +' , '+intro2);
			
				if($('#introby').val()!=='' && ($('#introby').val()=='<?php echo $user_profile['intro_by'];?>') ){
					$('#affiliate_btn').attr('disabled', true);
					$('#affiliate_btn').addClass('disabled');
                }
                $('#introby').on('keyup', function(){
                    var $that = $(this);
                    if($that.val()==''){
                        $('#affiliate_btn').addClass('disabled');
                    }else{
                        $('#affiliate_btn').removeClass('disabled');
                    }
                })
            <?php }*/ ?>
			
            //暱稱
            <?php if ($useview == "uname"){ ?>
                if($('#nickname').val()==''){
                    $('#nickname_btn').addClass('disabled');
                }
                $('#nickname').on('keyup', function(){
                    var $that = $(this);
                    if($that.val()==''){
                        $('#nickname_btn').addClass('disabled');
                    }else{
                        $('#nickname_btn').removeClass('disabled');
                    }
                })
            <?php } ?>
            
            //登入密碼
            <?php if ($useview == "upwd"){ ?>
                if($('#oldpw').val()=='' || $('#newpw').val()=='' || $('#newrepw').val()==''){
                    $('#pw_btn').addClass('disabled');
                }
                $('#oldpw').on('keyup', function(){
                    if($('#oldpw').val()=='' || $('#newpw').val()=='' || $('#newrepw').val()==''){
                        $('#pw_btn').addClass('disabled');
                    }else{
                        $('#pw_btn').removeClass('disabled');
                    }
                })
                $('#newpw').on('keyup', function(){
                    if($('#oldpw').val()=='' || $('#newpw').val()=='' || $('#newrepw').val()==''){
                        $('#pw_btn').addClass('disabled');
                    }else{
                        $('#pw_btn').removeClass('disabled');
                    }
                })
                $('#newrepw').on('keyup', function(){
                    if($('#oldpw').val()=='' || $('#newpw').val()=='' || $('#newrepw').val()==''){
                        $('#pw_btn').addClass('disabled');
                    }else{
                        $('#pw_btn').removeClass('disabled');
                    }
                })
            <?php } ?>
            
            //兌換密碼
            <?php if ($useview == "uepwd"){ ?>
                if($('#oldexpw').val()=='' || $('#newexpw').val()=='' || $('#newexrepw').val()==''){
                    $('#expw_btn').addClass('disabled');
                }
                $('#oldexpw').on('keyup', function(){
                    if($('#oldexpw').val()=='' || $('#newexpw').val()=='' || $('#newexrepw').val()==''){
                        $('#expw_btn').addClass('disabled');
                    }else{
                        $('#expw_btn').removeClass('disabled');
                    }
                })
                $('#newexpw').on('keyup', function(){
                    if($('#oldexpw').val()=='' || $('#newexpw').val()=='' || $('#newrepw').val()==''){
                        $('#expw_btn').addClass('disabled');
                    }else{
                        $('#expw_btn').removeClass('disabled');
                    }
                })
                $('#newexrepw').on('keyup', function(){
                    if($('#oldexpw').val()=='' || $('#newexpw').val()=='' || $('#newexrepw').val()==''){
                        $('#expw_btn').addClass('disabled');
                    }else{
                        $('#expw_btn').removeClass('disabled');
                    }
                })
            <?php } ?>
            
            <?php if ($useview == "upwd" || $useview == "uepwd"){ ?>
                //明暗碼切換
                var $parent = $('.input_group');
                $parent.each(function(){
                    var $thatp = $(this);
                    var $inpt = $thatp.find('input[type^="password"]');
                    
                    //針對 數字密碼做設定
                    var $inpTel = $thatp.find('input[type^="tel"]');
                    var $inpTelPass;
                    $inpTel.each(function(){
                        if($(this).hasClass('password')){
                            var $that = $(this);
                            $that.parent($parent)
                                .append(
                                    $('<div class="assword-btn d-flex align-items-center"/>')
                                    .append(
                                        $('<i class="esfas lightcolor eye-slash"/>')
                                    )
                                );

                            var $thisEyeBtn = $that.parent($parent).find('.password-btn');
                            $thisEyeBtn.removeClass('d-flex').hide();

                            //明暗碼切換
                            function toggleBtn(who,$thisBtn){
                                if(who.val()==''){
                                    $thisEyeBtn.removeClass('d-flex').hide();
                                }else{
                                    $thisEyeBtn.addClass('d-flex').show();
                                }
                            }
                            //切到明碼
                            function passwordEye() {
                                $thisEyeBtn.find("i")
                                    .removeClass('eye-slash')
                                    .addClass('eye');
                                $that.removeClass('password');
                                $thisEyeBtn.one("click",passwordEyeSlash);
                            }
                            //切到暗碼
                            function passwordEyeSlash() {
                                $thisEyeBtn.find("i")
                                    .removeClass('eye')
                                    .addClass('eye-slash');
                                $that.addClass('password');
                                $thisEyeBtn.one("click",passwordEye);
                            }

                            $that.on("keyup blur",function(){
                                toggleBtn($(this),$thisEyeBtn);
                            })
                            $thisEyeBtn.one("click",passwordEye);
                        }
                    });
                    
                    //一般密碼顯示
                    $inpt.each(function(){
                        var $that = $(this);
                        $that.parent($parent)
                            .append(
                                $('<div class="password-btn d-flex align-items-center"/>')
                                .append(
                                    $('<i class="esfas lightcolor eye-slash"/>')
                                )
                            );

                        var $thisEyeBtn = $that.parent($parent).find('.password-btn');
                        $thisEyeBtn.removeClass('d-flex').hide();

                        //明暗碼切換
                        function toggleBtn(who,$thisBtn){
                            if(who.val()==''){
                                $thisEyeBtn.removeClass('d-flex').hide();
                            }else{
                                $thisEyeBtn.addClass('d-flex').show();
                            }
                        }
                        //切到明碼
                        function passwordEye() {
                            $thisEyeBtn.find("i")
                                .removeClass('eye-slash')
                                .addClass('eye');
                            $inpt.attr("type","text");
                            $thisEyeBtn.one("click",passwordEyeSlash);
                        }
                        //切到暗碼
                        function passwordEyeSlash() {
                            $thisEyeBtn.find("i")
                                .removeClass('eye')
                                .addClass('eye-slash');
                            $inpt.attr("type","password");
                            $thisEyeBtn.one("click",passwordEye);
                        }

                        $that.on("keyup blur",function(){
                            toggleBtn($(this),$thisEyeBtn);
                        })
                        $thisEyeBtn.one("click",passwordEye);
                    });
                })
            <?php } ?>
            
            //忘記兌換密碼
            <?php if ($useview == "ufpwd"){ ?>
            	var verifiedPhone = "<?php echo $auth['verified'];?>"
                if (verifiedPhone != 'Y'){
                     alert("尚未進行手機驗證!!");
                     location.href='<?php echo BASE_URL.APP_DIR; ?>/verified/register_phone/';			
                }else{
                    if($('#phone').val()==''){
                        $('#phone_btn').addClass('disabled');
                    }
                    $('#phone').on('keyup', function(){
                        var $that = $(this);
                        if($that.val().length < 10 ){
                            $('#phone_btn').addClass('disabled');
                        }else{
                            $('#phone_btn').removeClass('disabled');
                        }
                    })
                }
            <?php } ?>
            
            //收件人
            <?php if ($useview == "uaddress"){ ?>
                if($('#name').val()=='' || $('#zip').val()=='' || $('#address').val()=='' || $('#rphone').val()==''){
                    $('#address_btn').addClass('disabled');
                }
                $('#name').on('keyup', function(){
                    if($('#name').val()=='' || $('#zip').val()=='' || $('#address').val()=='' || $('#rphone').val()==''){
                        $('#address_btn').addClass('disabled');
                    }else{
                        $('#address_btn').removeClass('disabled');
                    }
                })
                $('#zip').on('keyup', function(){
					if($('#name').val()=='' || $('#zip').val()=='' || $('#address').val()=='' || $('#rphone').val()==''){
                        $('#address_btn').addClass('disabled');
                    } else {
                        $('#address_btn').removeClass('disabled');
                    }
                })
                $('#address').on('keyup', function(){
                    if($('#name').val()=='' || $('#zip').val()=='' || $('#address').val()=='' || $('#rphone').val()==''){
                        $('#address_btn').addClass('disabled');
                    }else{
                        $('#address_btn').removeClass('disabled');
                    }
                })
                $('#rphone').on('keyup', function(){
                    if($('#name').val()=='' || $('#zip').val()=='' || $('#address').val()=='' || $('#rphone').val()==''){
                        $('#address_btn').addClass('disabled');
                    }else{
                        $('#address_btn').removeClass('disabled');
                    }
                })
            <?php } ?>
        })
        
    </script>
	<?php } ?>
