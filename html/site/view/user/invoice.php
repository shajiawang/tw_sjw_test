<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$status = _v('status'); 
$user_profile = _v('user_profile'); 
$donate=_v("donate");
?>
<div class="userEdit">

	<div class="required">
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="name">是否列印紙本發票</div>
                <font style="padding: .667rem;"><input type="radio" name="invoice_paper" id="invoice_paper" value="Y" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['invoice_paper']=='Y'){echo "checked";}?>> 是 <input type="radio" name="invoice_paper" id="invoice_paper" value="N" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['invoice_paper']=='N'){echo "checked";}?>> 否 </font>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="name">電子發票類型</div>
                <font style="padding: .667rem;"><input type="radio" name="invoice_type" id="invoice_type" value="P" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['invoice_type']=='P'){echo "checked";}?>> 個人 <input type="radio" name="invoice_type" id="invoice_type" value="C" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['invoice_type']=='C'){echo "checked";}?>> 公司 </font>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="zip">統一編號</div>
                <font ><input id="buyer_id" name="buyer_id" value="<?php echo $user_profile['buyer_id'];?>" type="text" placeholder="統一編號"></font>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="address">發票抬頭</div>
                <font ><input id="buyer_name" name="buyer_name" value="<?php echo $user_profile['buyer_name'];?>" type="text" placeholder="發票抬頭"></font>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="rphone">寄送地址</div>
                <font style="padding: .667rem;"><input type="radio" name="invoice_send" id="invoice_send" value="A" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['invoice_send']=='A'){echo "checked";}?>> 同訂購人 <input type="radio" name="invoice_send" id="invoice_send" value="B" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['invoice_send']=='B'){echo "checked";}?>> 同收件人 </font>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="donate_mark">是否捐贈</div>
                <font style="padding: .667rem;"><input type="radio" name="donate_mark" id="donate_mark" value="Y" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['donate_mark']=='Y'){echo "checked";}?>> 是 <input type="radio" name="donate_mark" id="donate_mark" value="N" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['donate_mark']=='N'){echo "checked";}?>> 否 </font>
			</li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="npcode">捐贈單位</div>
                <!--input id="npoban" name="npoban" value="<?php echo $user_profile['npoban'];?>" type="text" placeholder="請輸入愛心碼"-->
				<select name="npoban" id="npoban" >
					<option value="">(請選擇)</option>
					<?php
						foreach ($donate as $key => $value) {
							if ($user_profile['npoban'] == $value['code']) {
								echo '<option value="'.$value['code'].'" selected>'.$value['name'].'</option>';
							} else {
								echo '<option value="'.$value['code'].'">'.$value['name'].'</option>';
							} 
						}
					?>
				</select> 
			</li>
        </ul>
		<div style="margin:1rem;color:#FF0000;font-size:1.1rem;">*依規定公司發票開立不能改為個人發票<br><br>*電子發票選擇公司類型，需填寫統一編號及發票抬頭</div>
		<div class="footernav">
            <div class="footernav-button">
				<button type="submit" id="invoice_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="invoice_confirm();">確認修改</button>
			</div>
        </div>					
	</div>
	
</div>