<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$status = _v('status'); 
$user_profile = _v('user_profile'); 
?>
<div class="userEdit">

	<div class="required">
		<ul class="input_group_box">
            <li class="input_group d-flex align-items-center">
                <div class="label" for="rphone">手機條碼</div>
                <input id="phonecode" name="phonecode" value="<?php echo $user_profile['phonecode'];?>" type="text" placeholder="請輸入手機條碼">
            </li>
        </ul>
		<div class="footernav">
            <div class="footernav-button">
                <button type="button" id="address_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="carrier_confirm();">修改手機條碼</button>
			</div>
        </div>					
	</div>
	
</div>