<?php
date_default_timezone_set('Asia/Shanghai');
?>		
		<div class="article">
            <div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">

				<div data-role="collapsible">
                    <h2>隱私條款</h2>
                    <div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">
                        <div data-role="collapsible">
		                    <h4>隱私條款</h4>
						    <p><div id="onlyContent" style="word-wrap:break-word; overflow:hidden;" >保護消費者的個人隱私是「殺價王」重要的經營理念，在無消費者同意之下，我們絕不會將您的個人資料提供予任何與「殺價王」網站服務無關之第三人。「殺價王」確保僅為履行訂單及行銷之目的使用消費者的資料。因此殺價王制訂了隱私權保護政策。請您仔細閱讀以下有關隱私權保護政策的內容。<br/>
            <br/>
            <b>※資料收集</b><br/>
            當您在「殺價王」註冊時，我們會問及您的姓名、身分證字號、電子郵件地址、出生日期、性別、所在地區等資料。<br/>
            當您註冊「殺價王」帳號、使用「殺價王」的產品或服務、瀏覽「殺價王」網頁、參加活動或遊戲時，「殺價王」將會收集您的個人識別資料。<br/>
            「殺價王」會使用資料作以下用途：履行訂單及行銷之目的、改進為您提供的網頁內容與活動廣告、完成您對某項產品的要求及通知您特別活動或新產品。<br/>
            <br/>
            <b>※資料分享與公開方式</b><br/>
            「殺價王」不會向任何人出售或出借您的個人識別資料。在以下的情況下，「殺價王」會向政府機關提供你的個人識別資料：<br/>
            1.應遵守法令或政府機關的要求<br/>
            2.我們發覺您在網站上的行為違反「殺價王」服務條款或相關法令。<br/>
            3.需要與其他人士或公司共用您的資料，才能夠提供你要求的產品或服務。<br/>
            為了保護使用者個人隱私， 我們無法為您查詢其他使用者的帳號資料，若您有相關法律上問題需查閱他人資料時，請務必向警政單位提出告訴，我們將全力配合警政單位調查並提供所有相關資料，以協助調查。<br/>
            <br/>
            <b>※修改個人資料</b><br/>
            殺價王賦予您在任何時候修改部分個人「殺價王」帳號資料的權力，包括接受殺價王通知您特別活動或新產品文宣的決定權。<br/>
            <br/>
            <b>※帳號保護</b><br/>
            為保障您的隱私及安全，您的「殺價王」帳號資料會用密碼保護。在部分情況下殺價王使用通行標準的SSL保全系統，保障資料傳送的安全性。隱私權保護政策修訂殺價王可以不時修訂本政策。當我們在使用個人資料的規定上作出大修改時，我們會在網頁上張貼告示，並通知您相關事項。
</div></p>
		                </div>
		            </div>
                </div>
				
            </div>
        </div><!-- /article -->
	