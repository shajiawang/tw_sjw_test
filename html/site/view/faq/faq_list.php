<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$app = _v('APP');
$faq_category = _v('faq_category');
?> 
<style> 
.faq .header-title {
    padding: .834rem;
    border-bottom: 1px solid 
    #f3f3f3;
}
.faq .header-title .title {
    color: 
    #7C8495;
    font-size: 1.084rem;
}
</style>
<div class="article">
	<?php foreach($faq_category as $cgk => $cgv){ ?>
    <div class="saja_lists_box">
        <div class="header-title">
            <div class="title"><?php echo $cgv['cname'];?></div>
        </div>
		
        <?php foreach($cgv['faq_list'] as $rk => $rv){ ?>
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq<?php echo $rv['faqid'];?>',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title"><?php echo $rv['name'];?></div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq<?php echo $rv['faqid'];?>" class="linkBox" style="display:none">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <?php echo $rv['description'];?>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
    
	</div>
	<?php } ?>

    <div class="faq-link-box">
        <?php /*<!-- 我的發問(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
                <?php if($app=='Y') {?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/?APP=Y'">
                <?php }else{ ?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/?<?php echo $cdnTime; ?>'">
                <?php } ?>  
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">我的發問</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>*/?>
		
		<!-- 幫助中心(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
				<?php if($app=='Y') { ?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/help/?APP=Y'">
                <?php }else{ ?>
				<a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/help/?<?php echo $cdnTime; ?>'">
                <?php } ?>
					<div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">幫助中心</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
		
        <p style=" margin-top: 12px; margin-left: 12px;">
            <a href="https://line.me/R/ti/p/%40fgz1243t" target="https://line.me/R/ti/p/%40fgz1243t" title="殺價王官方Line@" class="ui-link" style="color: #689AD9; text-decoration: underline;">如有問題，請私訊殺價王官方Line@</a>
        </p>
		
    </div>

</div>