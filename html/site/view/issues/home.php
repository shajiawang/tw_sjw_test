<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list');
$noreply = 0;
$yesreply = 0;

/*
foreach($row_list['record'] as $rk => $rv) {
	if($rv['status'] == '1') { 
		$noreply = $noreply + 1;
	}else{
		if ($rv['status'] == '2'){
			$yesreply = $yesreply + 1;
		}
	}
}
*/
?>
    <div id="tobid" class="swipe-tab">
			<div id="tab1" class="content-item on-active">
				<?php if(!empty($row_list['record']) ) { ?>
				
				<div class="saja_lists_box">
					<?php 
					foreach($row_list['record'] as $rk => $rv) {
					$txt_color = "color:{$rv['stat_color']};";
					?>
                    <div class="list-group">
                        <div class="list-title-group">
                            <a class="list-title-link d-flex align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/issues_detail/?uiid=<?php echo $rv['uiid']; ?>'">
                                <div class="list-titlebox d-inlineflex align-items-center mr-auto">
									<div class="list-title">
									<span style="<?php echo $txt_color;?>"><?php echo $rv['content'];?></span>
									</div>
                                </div>
                                <div class="list-rtxtbox d-inlineflex align-items-center">
                                    <div class="saja_list_rtxt"><?php echo Date("Y-m-d", strtotime($rv['insertt'])); ?></div>
                                    <div class="r-arrow"></div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                
				<?php }else{ ?>
                
				<div class="nohistory-box" style="display:none;">
                    <div class="nohistory-img">
                        <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/nohistory-img.png" alt="">
                    </div>
                    <div class="nohistory-txt">目前沒有相關紀錄</div>
                </div>
                
				<?php } ?>
			</div>
    </div>
    
<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };
    
    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現  
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }
    
    $(function(){
        //有footerBar時，回頂按鈕 位置往上移動
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
    })
</script>

<!-- 觸底加載 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/es-bottom-scroll.js"></script>

<!-- 客製化加載 -->
<script>
    //各元件
    esOptions.nopicUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';        //無資料時顯示
    esOptions.loadingUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/bottomLoading.gif';        //資料loading顯示
    
    //存入 json 開頭
    esOptions.jsonInfo.dir = '<?php echo BASE_URL.APP_DIR;?>';
    esOptions.jsonInfo.hpage = '/issues/';                //網頁page識別碼，抓取json用
    esOptions.isTab = 'true';                                   //是否為多頁tab
    
    esOptions.contentdom.ulClass = 'saja_lists_box';
    createDom();
    //追加 tabItem : kind & jsonUrl & nopicText
    $.each(esOptions.tabItem,function(i,item){
        //添加特性 kind & jsonUrl
        switch(item['id']){
                case 'tab1' :
                    item['status'] = '1';
                    item['nopicText'] = '目前沒有相關紀錄';
                    break;
                case 'tab2' :
                    item['status'] = '2';
                    item['nopicText'] = '目前沒有相關紀錄';
                    break;
				default :
                    item['status'] = '1';
                    item['nopicText'] = '目前沒有相關紀錄';
                    break;				
            }
        item['jsonUrl'] = esOptions.jsonInfo.dir + esOptions.jsonInfo.hpage +'?json=Y&status='+item['status'];
        //console.log(esOptions.tabItem);
    });
    
    //載入json(第一次與其他頁共用同一個)
    esOptions.show = function($id) {
        var $ajaxUrl,$nowPage,$endPage;
        var $wrapper = $id;
        var $note,$textcolor;
        $.map(esOptions.tabItem,function(item, index){
            if(item['id'] === $id){
                $endPage = item['endpage'];
                item['page']++;                                 //這次要加載的頁數
                $nowPage = item['page'];
                $ajaxUrl = item['jsonUrl']+'&p='+$nowPage;      //json路徑
            }
        });
        //console.log($ajaxUrl);
		
        if ($endPage == $nowPage){                                  //比對是否是這次要加載的頁數
            $.ajax({  
                url: $ajaxUrl,
                contentType: esOptions.jsonInfo.contentType,
                type: esOptions.jsonInfo.type,  
                dataType: esOptions.jsonInfo.dataType,
                async: esOptions.jsonInfo.async === true,
                cache: esOptions.jsonInfo.cache === true,
                timeout: esOptions.jsonInfo.timeout,
                complete: endLoading,
                error: showFailure,  
                success: showResponse
            }).then(
                showResponse,                       //加載成功
                showFailure                         //加載失敗
            ).always(
                endLoading                          //加載後 不管成功失敗
            )

            function showResponse(data){       //生成div的function
                var $itemWrapper = $('#'+$wrapper);
                var $arr = data["retObj"]["data"];
                var $total = data["retObj"]["page"]["totalpages"];                    //抓取總頁數
                //console.dir($arr);
                if(!($arr == null || $arr == 'undefined' || $arr == '')){
                    $.map(esOptions.tabItem,function(item, index){                        //儲存總頁數到對應ID  存入esOptions.tabItem物件
                        if(item['id'] === $wrapper){
                            item['total'] = $total
                        }
                    })
                    if($nowPage <= $total){
                        $.each(data["retObj"]["data"],function(i,item){
                            var checkoutHref = "javascript:location.href='<?php echo BASE_URL.APP_DIR;?>/issues/issues_detail/?uiid=" +item['uiid']+ "'"

                            $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                            .append(
                                $('<li/>')
                                .addClass('list-group')
                                .append(
                                    $('<div/>')
                                    .addClass('list-title-group')
                                    .append(
                                        $('<a/>')
                                        .addClass('list-title-link d-flex align-items-center')
                                        .attr('href',checkoutHref)
                                        .append(
                                            $('<div/>')
                                            .addClass('list-titlebox d-inlineflex align-items-center mr-auto')
                                            .append(
                                                $('<div/>')
                                                .addClass('list-title')
                                                .text(item['name'])
                                            )
                                        )
                                        .append(
                                            $('<div/>')
                                            .addClass('list-rtxtbox d-inlineflex align-items-center')
                                            .append(
                                                $('<div/>')
                                                .addClass('saja_list_rtxt')
                                                .text(item['insertt'])
                                            )
                                            .append(
                                                $('<div/>')
                                                .addClass('r-arrow')
                                            )
                                        )
                                    )
                                )
                            )
                        }) 
                    }else{
                        return false;
                    }
                }else{
                    //不同頁簽顯示不同文字 (先前存進物件的資料)
                    var $nopicText = ''
                    var $nopicgroup = $.map(esOptions.tabItem, function(item, index){
                        if(item['id']===$wrapper){
                            $nopicText = item['nopicText'];
                        }
                    });
                    $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                    .after(
                        $('<div/>')
                        .addClass('nohistory-box')
                        .append(
                            $('<div/>')
                            .addClass('nohistory-img')
                            .append(
                                $('<img/>')
                                .addClass('img-fluid')
                                .attr('src',esOptions.nopicUrl)
                            )
                        )
                        .append(
                            $('<div/>')
                            .addClass('nohistory-txt')
                            .text($nopicText)
                        )
                    )
                }
            }
			//加載動作完成 (不論結果success或error)
            function endLoading() {
                $.map(esOptions.tabItem,function(item, index){
                    if(item['id'] === $id){
                        $totalPage = item['total'];              //找到對應ID的總頁數
                        item['endpage']++;                       //預計下次要加載的頁數,無論如何載完都加1，事後比對用
                    }
                });
                //console.log($nowPage+"___"+$totalPage);
                var $loadingDom = $('#'+$id).find('.esloading');
                if($totalPage == 1){
                    $loadingDom.hide();
                }else if($nowPage == $totalPage){
//                if($nowPage == $totalPage){
                    $loadingDom.hide();
					$loadingDom.html('<span>沒有其他資料了</span>'); 
                }else if($nowPage < $totalPage){
					$loadingDom.show();
                }else{
                    $loadingDom.hide();
                }
            }
            //加載錯誤訊息
            function showFailure() {
            }
        }
    };
    
    $(window).on('load', function(){
        //初始撈取
        $.each(esOptions.tabItem,function(i,item){
            var $id = item['id'];
            //先秀出預設顯示筆數
            esOptions.show($id);
        });
    });
    $(window).on('resize', function(){
        getPageHeight();
    });
</script>
