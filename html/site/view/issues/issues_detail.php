<?php 
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$issues_record = _v('Issues'); 
$reply_data = _v('Issues_reply');
?>
<style>
.img-list img{
    margin-left: .834rem;
    width: 4rem;
    height: 4rem;
    border: 2px solid #fff;
    border-radius: 20%;
}
.reply_img img{
    margin-left: .834rem;
    width: 4rem;
    height: 4rem;
    border: 2px solid #fff;
    border-radius: 20%;
}
</style>

<div class="issues_detail">
    <!-- 發問內容 -->
    <div class="saja_lists_box">
        
		<!-- 使用者標頭 -->
		<?php /*<div class="list-group list-title d-flex align-items-center">
            <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                <div class="list-title">
				<img src="<?php echo $issues_record['thumbnail']; ?>" >
				<?php echo $issues_record['username'];?></div>
            </div>
            <div class="list-rtxtbox d-inlineflex align-items-center">
                <div class="saja_list_rtxt"><?php echo $issues_record['insertt']; ?></div>
            </div>
        </div>*/?>
       
        <!-- 使用者發問內容 -->
        <div class="list-group">
            <div class="list-info">
                <div class="txt" style="min-height: 1.16rem;"><span style="color:red;"><?php echo $issues_record['content'];?></span></div>
                
				<div class="txt" style="min-height: 1.16rem;"><?php echo $issues_record['info'];?></div>
            
				<div class="list-upload-box">
                    <div class="img-group d-flex">
						<?php if (!empty($issues_record['ispid'])){ ?>
                        <div class="img-list">
                            <img src="<?php echo $issues_record['ispid'];?>" alt="上傳圖示一">
                        </div>
						<?php } ?>
						<?php if (!empty($issues_record['ispid2'])){ ?>
                        <div class="img-list">
                            <img src="<?php echo $issues_record['ispid2'];?>" alt="上傳圖示二">
                        </div>
						<?php } ?>
						<?php if (!empty($issues_record['ispid3'])){ ?>
                        <div class="img-list">
                            <img src="<?php echo $issues_record['ispid3'];?>" alt="上傳圖示三">
                        </div>
						<?php } ?>
                    </div>
                </div>
            </div>
			
        </div>
    </div>
    
    <?php /*
	<!-- 回覆內容 -->
    <!--
        * 已回覆 在外層 增加 class="already"
        * 已回覆 在回覆內容處 保留 class="txt",其餘class 不要
    -->
    <div class="saja_lists_box reply <?php if ($issues_record['status'] == 2){ ?>already<?php } ?>">
        <!-- 客服標頭 -->
        <div class="list-group list-title d-flex align-items-center">
            <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                <div class="list-title">殺價王小編：</div>
            </div>
            <div class="list-rtxtbox d-inlineflex align-items-center">
<!--<div class="saja_list_rtxt">2018-08-20 15:30</div>-->
                <div class="saja_list_rtxt"><?php if ($issues_record['status'] == 1){ ?>尚未回覆<?php }else{ ?>已回覆<?php } ?></div>
            </div>
        </div>
       
        <!-- 客服回覆內容 -->
        <div class="list-group">
            <div class="list-info">
<!--<div class="txt">針對如何新增信用卡回覆</div>-->
                <div class="txt<?php if ($issues_record['status'] == 1){ ?> d-flex align-items-center justify-content-center<?php } ?>"><?php if ($issues_record['status'] == 1){ ?>我們將盡快回覆您的問題，謝謝<?php }else{ ?><?php echo $issues_reply['record'][0]['content'];?><?php } ?></div>
            </div>
        </div>
    </div>
	*/?>
	
	<div class="saja_lists_box ">
					
					<?php 
					if(!empty($reply_data['record'] ) ) {
					foreach($reply_data['record'] as $rk => $rv) {
					?>
                    <div class="list-group">
                        <div class="list-title-group" style="background:<?php echo $rv['bg_color'];?>;">
								<div class="list-titlebox d-inlineflex align-items-center ">
									
									<div class="bid-user d-inlineflex align-items-center mr-auto">
									<div class="bid-img"><img class="img-fluid" src="<?php echo $rv['user_img'];?>"></div>
									<div class="bid-userName"><?php echo $rv['user_name'];?></div>
									</div>
									
									<div class="list-info">
									<?php echo $rv['content'];?>
									</div>
									
									<?php if(!empty($rv['rpfid']) ){ ?>
									<div class="reply_img">
										<img src="<?php echo $rv['rpfid'];?>" alt="上傳圖示">
									</div>
									<?php } ?>
                                </div>
                                <div class="list-rtxtbox d-inlineflex align-items-center">
                                    <div class="saja_list_rtxt"><?php echo Date("Y-m-d", strtotime($rv['insertt'])); ?></div>
                                </div>
                        </div>
                    </div>
                    
					<?php }
					} else { ?>
					
					<div class="list-group">
                        <div class="list-info">我們將盡快回覆您的問題，謝謝</div>
                    </div>	
					<?php } ?>
    </div>
	
	<!-- 再次詢問按鈕 -->
	<?php /*
	<div class="saja_lists_box">
		<div class="list-group">
			<a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/issues_add?uicid=<?php echo $issues_record['uicid']; ?>'">
				<div class="list-info">ℹ️ 詢問客服</div>
			</a>
		</div>
	</div>
	*/?>
	<form method="post" action="<?php echo BASE_URL.APP_DIR;?>/issues/issues_reply_add?json=Y" id="contactform" name="contactform">		
		<div id="issues_add" class="issues_add">
			<!--  輸入發問內容  -->
			<div class="saja_lists_box">
				<!-- 文字輸入框 -->
				<div class="list-group">
					<div class="textarea-box">
						<input name="content" id="content" placeholder="" type="text" >
					</div>
				</div>
			</div>
			
			<!--  上傳圖片區  -->
			<div class="saja_lists_box">
				<div class="list-group">
					<?php /*<div class="upload-box">
						<div class="labelBlock">上傳照片(限1張)</div>
						<div class="img-group d-flex">
							<div id="pid1" class="img-list d-none">
								<img id="preview_logo1" name="preview_logo1" src="http://www.saja.com.tw/site/images/site/product/121a8901e96cc3d806fa68b22b88c5b7.jpg" alt="">
							</div>
							<div id="pid-add" class="img-list add-btn d-flex justify-content-center align-items-center" onclick="$('#ispid').trigger('click'); return false;">
								<div class="icon">
									<img src="<?PHP echo APP_DIR; ?>/static/img/add-pic.png" alt="">
								</div>
							</div>
						</div>
					</div>
					*/?>
					<div class="textarea-box">
						<input name="rpfid" id="rpfid" placeholder="" type="text" value="">
					</div>
				</div>
			</div>
			
			<?php /*<!-- 圖檔內容 	
			<div style="visibility:hidden">
				<input name="rpfid" id="rpfid" class="uploader__input" type="file" accept="image/*" placeholder="" >
			</div>
			*/?>
			
			<!-- 確認按鈕 -->
			<div class="footernav">
				<div class="footernav-button">
					<?php /*<button id="issend" name="issend" type="submit" class="ui-btn ui-btn-a ui-corner-all" >送出</button>*/?>
				</div>
			</div>
			
			<input type="hidden" id="auth_id" name="auth_id" value="<?php echo $_SESSION['auth_id'];?>">
			<input type="hidden" id="uiid" name="uiid" value="<?php echo $issues_record['uiid'];?>">

		</div>
	</form>
	
</div>
