<?php 
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$issues_category = _v('category_list');
$uicid = _v('uicid');
?>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/croppie.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/exif.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/megapix-image.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/weui_loading.js"></script>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/croppie.css" rel="stylesheet"/>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/weui_loading.css" rel="stylesheet"/>

<form method="post" action="" id="contactform" name="contactform">		
	<div id="issues_add" class="issues_add">
		<!--  輸入發問內容  -->
		<div class="saja_lists_box">
			<div class="list-group d-flex align-items-center">
				<div class="label" for="project">分類項目</div>
				
				<select name="uicid" id="uicid" onchange="checksend(this.value);">
					<option value="" >請選擇</option>
					<?php foreach($issues_category['record'] as $rk => $rv) { ?>				
					<option value="<?php echo $rv['uicid']; ?>" <?php echo ($uicid==$rv['uicid'])?'selected':'';?> ><?php echo $rv['name']; ?></option>
					<?php } ?>                    
				</select>
			</div>
			
			<!-- 文字輸入框 -->
			<div class="list-group">
				<div class="textarea-box">
					<textarea name="content" id="content" placeholder="必填! 請描述你遇到的問題" onkeyup="checksend();" ></textarea>
				</div>
			</div>
			
			<!-- 文字輸入框 -->
			<div class="list-group">
				<div class="textarea-box">
					<textarea name="info" id="info" placeholder="選填, 描述訂單編號或商品名稱" ></textarea>
				</div>
			</div>
			
		</div>
		
		<!--  上傳圖片區  -->
		<div class="saja_lists_box">
			<div class="list-group">
				<div class="upload-box">
					<div class="labelBlock">上傳照片(限制3張)</div>
					<div class="img-group d-flex">
						<div id="pid1" class="img-list d-none">
							<img id="preview_logo1" name="preview_logo1" src="" alt="">
						</div>
						
						<div id="pid2" class="img-list d-none">
							<img id="preview_logo2" name="preview_logo2" src="" alt="">
						</div>
						
						<div id="pid3" class="img-list d-none">
							<img id="preview_logo3" name="preview_logo3" src="" alt="">
						</div>
						
						<div id="pid-add" class="img-list add-btn d-flex justify-content-center align-items-center" onclick="$('#ispid').trigger('click'); return false;">
							<div class="icon">
								<img src="<?PHP echo APP_DIR; ?>/static/img/add-pic.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- 圖檔內容-->	
		<div style="visibility:hidden">
			<input name="ispid" id="ispid" class="uploader__input" type="file" accept="image/*" placeholder="" >
		</div>
		
		<!-- 確認按鈕 -->
		<div class="footernav">
			<div class="footernav-button">
				<button id="issend" name="issend" type="button" class="ui-btn ui-btn-a ui-corner-all" onclick="issues_send();" disabled>提出問題</button>
			</div>
		</div>
		
		<input type="hidden" id="auth_id" name="auth_id" value="<?php echo $_SESSION['auth_id'];?>">
		<input type="hidden" id="thumbnail1" name="thumbnail1" >
		<input type="hidden" id="thumbnail2" name="thumbnail2" >
		<input type="hidden" id="thumbnail3" name="thumbnail3" >
		<input type="hidden" id="kind" name="kind" value="insertt">
		<input type="hidden" id="pidcount" name="pidcount" value="1">

	</div>
</form>

<div id="actions" style="display:none;">
    <div class="pic-box">
        <div id="new_logo" style="text-align: center;"></div>
    </div>
    <canvas id="cvs" style="display:none"></canvas>
    <canvas id="cvs-wfp" style="display:none"></canvas>

    <div class="btn-box">
        <div class="rotate-group mx-auto d-flex">
            <button onclick="rotate_left()">向左旋轉90度</button>
            <button onclick="rotate_right()">向右旋轉90度</button>
        </div>
        <div class="submit-group mx-auto">
            <button onclick="save_pic()">儲存</button>
            <button onclick="cancel_edit()">取消</button>
        </div>
    </div>
</div>

<script>
    //調整標題寬度一致
    var options = {
        dom: {
            ul: '.saja_lists_box',
            li: '.list-group',
            label: '.label',
            input: 'input',
            select: 'select'
        }
    }
    $(function(){
        function changeWidth(dom) {
            
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];
            
            $liW = parseInt($selfLi.width());                         //取得li總寬度   
            
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

            

            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                $selfInput.attr('style','');
                $selfSelect.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                    }
                })
                $selfSelect.each(function(){
                    if($selfSelect.length > 1){                 
                        $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                        
                    }else{
                        $(this).width($remain);
                    }
                })
            })
            
            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
        }
        $(options.dom.ul).each(function(){
            changeWidth($(this));
        })

        //尺寸改變重新撈取寬度
        $(window).resize(function(){
            $(options.dom.ul).each(function(){
                changeWidth($(this));
            })
        })

    })
	

	
	//绑定input change事件
	$("#ispid").unbind("change").on("change",function(){

		lnv.pageloading();
		var file = document.querySelector('#ispid').files[0];
		if(file){
			//验证图片文件类型
			if(file.type && !/image/i.test(file.type)){
				return false;
			}
			var reader = new FileReader();
			reader.onload = function(e){
				//readAsDataURL后执行onload，进入图片压缩逻辑
				//e.target.result得到的就是图片文件的base64 string
				render(file,e.target.result,1);
			};
			//以dataurl的形式读取图片文件
			reader.readAsDataURL(file);
		}
	});

	//定义照片的最大高度
	var MAX_HEIGHT = 480;
	var render = function(file,src,num){
		EXIF.getData(file,function(){
			//获取照片本身的Orientation
			var orientation = EXIF.getTag(this, "Orientation");
			var image = new Image();
			image.onload = function(){
				var cvs = document.getElementById("cvs");
				var w = image.width;
				var h = image.height;
				//计算压缩后的图片长和宽
				if(h>MAX_HEIGHT){
					w *= MAX_HEIGHT/h;
					h = MAX_HEIGHT;
				}
				//使用MegaPixImage封装照片数据
				var mpImg = new MegaPixImage(file);
				//按照Orientation来写入图片数据，回调函数是上传图片到服务器
				mpImg.render(cvs, {maxWidth:w,maxHeight:h,orientation:orientation}, sendImg);
			};
			image.src = src;
		});
	};

	//上传图片到服务器
	var sendImg = function(){
		var cvs = document.getElementById("cvs");
		//调用Canvas的toDataURL接口，得到的是照片文件的base64编码string
		var data = cvs.toDataURL("image/png");
		//base64 string过短显然就不是正常的图片数据了，过滤の。
		if(data.length<48){
			console.log("data error.");
			return;
		}

		var formData = new FormData();
		//formData.append('pic', reader.result );
		formData.append('pic', data );
		formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
		formData.append('pic_name','issuestemp');
		formData.append('filedir','site/issues/temp/');

		$.ajax(
			{
				url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
				type : 'POST',
				data : formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false,  // tell jQuery not to set contentType
				success : function(data)
				{
					load_img();
				}
			}
		);
	};


	function load_img()
	{
	  destory_img();

	  var el = document.getElementById('new_logo');
		vanilla = new Croppie(el, {
		viewport: { width: 196, height: 196 },
		boundary: { width: 300, height: 300 },
		showZoomer: true,
		enableOrientation: true
	  });

	  vanilla.bind({
		  url: '<?php echo BASE_URL.APP_DIR;?>/images/site/issues/temp/_'+<?php echo $_SESSION['auth_id'];?>+'_issuestemp.png?t=' + new Date().getTime(),
	  });

	  document.getElementById('issues_add').style.display = "none";
	  document.getElementById('actions').style.display = "inline";
	  lnv.destroyloading();
	}

	function destory_img()
	{
	  document.getElementById('new_logo').innerHTML="";
	}

	function cancel_edit()
	{
	  location.reload();
	}

	function page_reload()
	{
	  location.reload();
	}

	function rotate_right()
	{
	   vanilla.rotate(90);
	}

	function rotate_left()
	{
	   vanilla.rotate(-90);
	}

	function save_pic()
	{
		var pic = vanilla.result('canvas','viewport');
		console.log(pic);

		pic.then(function(value){
		console.log(value);
		
		var count = document.getElementById('pidcount').value;
		var formData = new FormData();
		formData.append('pic',value);
		formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
		formData.append('pic_name','<?php echo md5($cdnTime."issues");?>_'+count);
		formData.append('filedir','site/issues/');
		//console.log(count);

		$.ajax({
			url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
			type : 'POST',
			data : formData,
			processData: false,  // tell jQuery not to process the data
			contentType: false,  // tell jQuery not to set contentType
			success : function(data)
				{
					var tf = document.getElementById('thumbnail'+count).value="_<?php echo $_SESSION['auth_id'];?>_<?php echo md5($cdnTime."issues");?>_"+count+".png";
					//console.log(tf);
					// change_db_data();
					// page_reload();

					document.getElementById('issues_add').style.display = "block";
					document.getElementById('actions').style.display = "none";
					
					document.getElementById("pid"+count).classList.remove('d-none');
					document.getElementById("preview_logo"+count).style.display = "";
					document.getElementById("preview_logo"+count).src = "<?php echo BASE_URL.APP_DIR;?>/images/site/issues/_<?php echo $_SESSION['auth_id'];?>_<?php echo md5($cdnTime."issues");?>_"+count+".png?t=" + new Date().getTime();
					if (parseInt(count) < 3){
						document.getElementById('pidcount').value = (parseInt(count)+1);
					}else{
						document.getElementById('pid-add').classList.remove('d-flex');
						document.getElementById('pid-add').classList.add('d-none');
					}
				}
			});
		});
	}

	function issues_send() {
		var category_value = $('select[name="uicid"] option:selected').val();
		var content_value = $("#content").val();
		//送出前檢查資料是否正常
		if ((category_value != "") && (content_value != "")){
			$.ajax({
				url: "<?php echo BASE_URL.APP_DIR;?>/issues/issues_add/?web=Y",
				data: $('#contactform').serialize(),
				type:"POST",
				dataType:'text',
				success: function(msg){
					var obj = jQuery.parseJSON(msg);
					//console.log(obj);
					alert( obj.retMsg );
					if (obj.retCode == 1){
						location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/';
					}
				},
				error:function(){
					var obj = jQuery.parseJSON();
					alert( obj.retMsg );
				}
			});
		}else{
			if (category_value == ""){
				alert("請選擇分類項目 !!");
			}else{
				alert("請填寫遇到的問題內容 !!");	
			}
			return;
		}
	}

	function checksend(){
		var category_value = $('select[name="uicid"] option:selected').val();
		var content_value = $("#content").val();
		//console.log("category_value => "+ category_value +" content_value => "+ content_value);
		
		if ((category_value != "") && (content_value != "")){
			$('#issend').removeAttr("disabled");
		}
	}	
</script>
