<?php 
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$issues = _v('Issues'); 
$issues_record = $issues['record'][0];
$issues_reply = _v('Issues_reply');
?>
<div class="issues_detail">
    <!-- 發問內容 -->
    <div class="saja_lists_box">
        <!-- 使用者標頭 -->
        <div class="list-group list-title d-flex align-items-center">
            <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                <div class="list-title"><?php echo $issues_record['name'];?></div>
            </div>
            <div class="list-rtxtbox d-inlineflex align-items-center">
                <div class="saja_list_rtxt"><?php echo Date("Y-m-d H:i", strtotime($issues_record['insertt'])); ?></div>
            </div>
        </div>
       
        <!-- 使用者發問內容 -->
        <div class="list-group">
            <div class="list-info">
                <div class="txt"><?php echo $issues_record['content'];?></div>
                
				<div class="txt"><?php echo $issues_record['product'];?></div>
            
				<div class="txt"><?php echo $issues_record['orderid'];?></div>
            
				<div class="list-upload-box">
                    <div class="img-group d-flex">
					
						<?php if (!empty($issues_record['ispid'])){ ?>
                        <div class="img-list">
                            <img src="<?php echo ISSUESIMGS_DIR."/".$issues_record['ispid'];?>" alt="上傳圖示一">
                        </div>
						<?php } ?>
						<?php if (!empty($issues_record['ispid2'])){ ?>
                        <div class="img-list">
                            <img src="<?php echo ISSUESIMGS_DIR."/".$issues_record['ispid2'];?>" alt="上傳圖示二">
                        </div>
						<?php } ?>
						<?php if (!empty($issues_record['ispid3'])){ ?>
                        <div class="img-list">
                            <img src="<?php echo ISSUESIMGS_DIR."/".$issues_record['ispid3'];?>" alt="上傳圖示三">
                        </div>
						<?php } ?>
                    </div>
                </div>
            </div>
			
        </div>
    </div>
    
    <!-- 回覆內容 -->
    <!--
        * 已回覆 在外層 增加 class="already"
        * 已回覆 在回覆內容處 保留 class="txt",其餘class 不要
    -->
    <div class="saja_lists_box reply <?php if ($issues_record['status'] == 2){ ?>already<?php } ?>">
        <!-- 客服標頭 -->
        <div class="list-group list-title d-flex align-items-center">
            <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                <div class="list-title">殺價王小編：</div>
            </div>
            <div class="list-rtxtbox d-inlineflex align-items-center">
<!--<div class="saja_list_rtxt">2018-08-20 15:30</div>-->
                <div class="saja_list_rtxt"><?php if ($issues_record['status'] == 1){ ?>尚未回覆<?php }else{ ?>已回覆<?php } ?></div>
            </div>
        </div>
       
        <!-- 客服回覆內容 -->
        <div class="list-group">
            <div class="list-info">
<!--<div class="txt">針對如何新增信用卡回覆</div>-->
                <div class="txt<?php if ($issues_record['status'] == 1){ ?> d-flex align-items-center justify-content-center<?php } ?>"><?php if ($issues_record['status'] == 1){ ?>我們將盡快回覆您的問題，謝謝<?php }else{ ?><?php echo $issues_reply['record'][0]['content'];?><?php } ?></div>
            </div>
        </div>
    </div>
	
	<!-- 再次詢問按鈕 -->
	<div class="saja_lists_box">
		<div class="list-group">
			<a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/issues_add?uicid=<?php echo $issues_record['uicid']; ?>'">
				<div class="list-info">ℹ️ 詢問客服</div>
			</a>
		</div>
	</div>
	
</div>
