<?php
$status = _v('status');
$product = _v('product');
$product_category = _v('product_category');
$stock = _v('stock');
$order_exchange = _v('order_exchange');
$user_extrainfoArr = _v('user_extrainfoArr');
$spoint = _v('spoint');

if($user->is_logged && !empty($_SESSION['user']) ) {
	$getuser['name'] = $_SESSION['user']['profile']['addressee']; 
	$getuser['zip'] = $_SESSION['user']['profile']['area'];
	$getuser['address'] = $_SESSION['user']['profile']['address'];
	$auth = _v('auth'); 
} else {
	$getuser['name'] = ''; 
	$getuser['zip'] = '';
	$getuser['address'] = '';
}
	if(!empty($product['thumbnail'])) { //$status['path_image'] 
		$img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail'];
	} elseif (!empty($product['thumbnail_url'])) {
		$img_src = $product['thumbnail_url']; 
	} else {
		$img_src="http://www.shajiawang.com/site/images/site/shark_logo.png";
	}
?>
<!DOCTYPE html5 PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide{display:none !important;}ng\:form{display:block;}.ng-animate-block-transitions{transition:0s all!important;-webkit-transition:0s all!important;}.ng-hide-add-active,.ng-hide-remove{display:block!important;}</style>
<style type="text/css">@charset "UTF-8";[ng\:cloak],[ng-cloak],[data-ng-cloak],[x-ng-cloak],.ng-cloak,.x-ng-cloak,.ng-hide:not(.ng-hide-animate){display:none !important;}ng\:form{display:block;}</style>
  
<meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no, width=device-width">
<!-- uc强制竖屏 -->
<meta name="screen-orientation" content="portrait">
<!-- QQ强制竖屏 -->
<meta name="x5-orientation" content="portrait">
<title ng-bind="userData.title" class="ng-binding"><?php echo $product['name']; ?></title>

<link type="text/css" rel="stylesheet" href="<?PHP echo APP_DIR; ?>/css/files/style.css">

<script src="<?PHP echo APP_DIR; ?>/static/js/jquery-1.11.1.min.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/jquery.timers-1.2.js"></script>
<script> var APP_DIR='<?php echo APP_DIR; ?>'; </script>
<script src="<?PHP echo APP_DIR; ?>/ajax/mall.js"></script>
<script src="<?PHP echo APP_DIR; ?>/js/jquery.mobile-1.4.0.min.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/appbsl_sdk.js"></script>

</head>

<body style="background: #ededed;" class="grade-a platform-browser platform-win32 platform-ready">
    <div>
        <div class="content">
            <div class="recharge-pay padding  body-white">
                <ul class="card-logo clearfix">
                    <li class="li-icon">
                        <div class="inline">
                            <img src="<?php echo $img_src; ?>" style="height:auto;">
                        </div>
                        <dl class="inline" style="margin:0 0 0 0.5rem;">
                            <dd>
								<span class="card-logo-dd ng-binding"><?php echo $product['name']; ?></span>
                            </dd>
                            <dt><span></span></dt>
                        </dl>						
                    </li>
                </ul>
            </div>

            <div class="recharge-p-list mt10 body-white">
                <ul class="row row-wrap row-block">
                    <li class="col-33  recharge-bill mt5">
                        <p class="row-block-p" href="#">面值</p>
                    </li>
                </ul>
                <ul class="row row-wrap clearfix">
                    <li class="col-33" >
                        <a id="10" <?php if (sprintf("%0.2f", $spoint['amount']) > 10){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">10元</h3>
                            <p  class="ng-binding">售價10元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="30" <?php if (sprintf("%0.2f", $spoint['amount']) > 30){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">30元</h3>
                            <p  class="ng-binding">售價30元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="50" <?php if (sprintf("%0.2f", $spoint['amount']) > 50){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">50元</h3>
                            <p  class="ng-binding">售價50元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="100" <?php if (sprintf("%0.2f", $spoint['amount']) > 100){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">100元</h3>
                            <p  class="ng-binding">售價100元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="200" <?php if (sprintf("%0.2f", $spoint['amount']) > 200){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">200元</h3>
                            <p  class="ng-binding">售價200元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="500" <?php if (sprintf("%0.2f", $spoint['amount']) > 500){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">500元</h3>
                            <p  class="ng-binding">售價500元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="1000" <?php if (sprintf("%0.2f", $spoint['amount']) > 1000){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">1000元</h3>
                            <p  class="ng-binding">售價1000元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="2000" <?php if (sprintf("%0.2f", $spoint['amount']) > 2000){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?>>
                            <h3 class="ng-binding">2000元</h3>
                            <p  class="ng-binding">售價2000元</p>
                        </a>
                    </li>
					<li class="col-33" >
                        <a id="30000" <?php if (sprintf("%0.2f", $spoint['amount']) > 30000){ ?>class="row-p"<?php }else{ ?>class="row-p disabled" disabled<?php } ?> >
                            <h3 class="ng-binding">3000元</h3>
                            <p  class="ng-binding">售價3000元</p>
                        </a>
                    </li>					
                </ul>
            </div>

            <div class="recharge-pay padding recharge-pay02 mt10">
                <ul class="card clearfix">
                    <li><span>
							1.最低兌換額度為10元<br>
							2.须3个工作天<br>
					</span></li>
                </ul>
            </div>
			
	<input type="hidden" name="epcid" id="epcid" value="<?php echo $_GET["epcid"]; ?>">
	<input type="hidden" name="epid" id="epid" value="<?php echo $product['epid']; ?>">
	<input type="hidden" name="esid" id="esid" value="<?php echo $product["esid"] ;?>">		
	<input type="hidden" name="name" id="name" value="<?php echo $getuser['name'];?>">
	<input type="hidden" name="zip" id="zip" value="<?php echo $getuser['zip'];?>">
	<input type="hidden" name="address" id="address" value="<?php echo $getuser['address'];?>">
	<input type="hidden" name="bonus" id="bonus" value="0">
	<input type="hidden" name="handling" id="handling" value="<?php echo sprintf("%0.2f", $product['process_fee']); ?>">
	<input type="hidden" name="all_bonus" id="all_bonus" value="<?php echo sprintf("%0.2f", $spoint['amount']); ?>">
	<input type="hidden" name="autobid" value="<?php echo $paydata['autobid'];?>"/>
	<input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
	<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">	
	
    <div class="recharge-pay">
				<?php 
				if(!empty($stock) ) {
					if($user->is_logged) {
				?>
						<?php if(empty($auth['verified']) || $auth['verified']!=='Y'){ ?>
							<button  id="btnsend" type="button" onClick="checkphone()"  class="button button-block button-positive">立即支付</button>
						<?php } else { ?>
							<button id="btnsend" type="button" onClick="mall_product()"  class="button button-block button-positive">立即支付</button>
						<?php } ?>
					<?php } else { ?>
						<button id="btnsend" type="button" onClick="mall_login()"  class="button button-block button-positive">立即支付</button>
					<?php } ?>
				<?php } else { ?>
					<button id="btnsend1" type="button" class="button button-block button-positive">库存不足</button>
				<?php }//endif ?>   
				
		
		<p class="tips">
            <span>支付后自動发货，不可退换</span>
        </p>
    </div>
</div>

</ion-content>
</div>

</body>
</html>	

<script type="text/javascript">
$(document).ready(function() {

	$("a").click( function(){
		var id = $(this).attr('id');
		if (id < <?php echo sprintf("%0.2f", $spoint['amount']); ?>){
			$("a").removeClass('checked');
			$("#"+id).addClass('checked');
			$('#bonus').val(id);
			$("#btnsend").html('立即支付 ￥'+id+'元');
		}	
	});

});
</script>