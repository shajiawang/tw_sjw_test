<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$product = _v('product');
$order = _v('order');
?>
<script type="text/javascript">
function qrcode_back() {
	alert("取消交易");
	location.href = BASE_URL.APP_DIR."/mall/?t=<?php echo $cdnTime; ?>";
}
</script>
	<div class="article">
		<input type="hidden" name="orderid" id="orderid" value="<?php echo $order['orderid']; ?>" />
		<input type="hidden" name="epid" id="epid" value="<?php echo $order['epid']; ?>" />		
		
		<div class="weui-cells2" >
		  <div class="weui-cell" >
			<div class="weui-cell__bd">
				<div width="100%">
					<div style="text-align:center;height:100%;float:left;"><h2><?php if ($order['epid'] != 0) { echo $product['name']; } else { echo "鯊魚點數"; } ?></h2></div>
				</div>	
			</div>
		  </div>
		</div>
		<div class="weui-cells2" >
		  <div class="weui-cell" >
			<div class="weui-cell__bd">
				<div width="100%">
					<div style="text-align:center;height:100%;float:left;"><h2><?php echo $order['total_fee']; ?></h2></div>
				</div>	
			</div>
		  </div>
		</div>					
		<div class="weui-cells2" >
		  <div class="weui-cell" >
			<div class="weui-cell__bd">
				<div width="100%">
					<div style="width:30%;height:100%;float:left;padding:20px 0 0 0;"><h5>兌換密碼</h5></div>
					<div style="width:70%;float:left;">
						<input type="password" name="expw" id="expw" style="border-radius:10px;" placeholder="兌換密碼">
					</div>
				</div>	
			</div>
		  </div>
		</div>		

		<div class="nav">
			<div class="weui-tabbar" style="background-color:#FFFFFF">
				<div style="width:1%;"> </div>
				<div style="width:98%; text-align:center;" >
					<button type="button" onClick="qrcode_back()" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0px 0 ">取消</button>
					<button type="button" onclick="qrcode_exchange();" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0">兌換</button>
				</div>	
				<div style="width:1%;"> </div>
			</div>			
		</div>			
    </div><!-- /article -->