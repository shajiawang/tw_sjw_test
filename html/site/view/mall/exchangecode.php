<?php
$order = _v('order');

?>

<!--  CDN Bootstrap 4.1.1  -->
<script src="<?php echo APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo APP_DIR;?>/static/css/keyboard.min.css">

<div class="exchangeThirdCode">
    <form method="post" action="<?php echo BASE_URL.APP_DIR."/mall/extExchange/"; ?>" id="contactform" name="contactform" target="_parent">
        <input type="hidden" name="json" id="json" value="<?php echo $order["json"]; ?>">
        <input type="hidden" name="type" id="type" value="<?php echo $order['type']; ?>">
        <input type="hidden" name="uuid" id="uuid" value="<?php echo $order["uuid"] ;?>">
        <input type="hidden" name="tx_num" id="tx_num" value="<?php echo $order["tx_num"] ;?>">
        <input type="hidden" name="vndr_txid" id="vndr_txid" value="<?php echo $order["vndr_txid"] ;?>">
        <input type="hidden" name="vndr_odrid" id="vndr_odrid" value="<?php echo $order["vndr_odrid"] ;?>">
        <input type="hidden" name="vndr_prodid" id="vndr_prodid" value="<?php echo $order["vndr_prodid"] ;?>">
        <input type="hidden" name="total_bonus" id="total_bonus" value="<?php echo $order["total_bonus"] ;?>">
        <input type="hidden" name="cost" id="cost" value="<?php echo $order["cost"] ;?>">
        <input type="hidden" name="sign" id="sign" value="<?php echo $order["sign"] ;?>">

        <div class="deduct">應扣鯊魚點數： <span id="litotal"><?php echo $order['total_bonus']; ?></span></div>

        <div class="login-title login-captcha-title">
            <h1>輸入兌換密碼</h1>
        </div>
        <div class="captcha-box captcha_keyboard">
            <ul class="num-box d-flex justify-content-center">
                <li class="d-flex justify-content-center align-items-center"><span></span></li>
                <li class="d-flex justify-content-center align-items-center"><span></span></li>
                <li class="d-flex justify-content-center align-items-center"><span></span></li>
                <li class="d-flex justify-content-center align-items-center"><span></span></li>
                <li class="d-flex justify-content-center align-items-center"><span></span></li>
                <li class="d-flex justify-content-center align-items-center"><span></span></li>
            </ul>	
        </div>
        <input type="hidden" class="input-field numHideBox" id="captcha" name="captcha" value="">
        
        <!--  驗證訊息  -->
        <ul class="captcha-msgError"></ul>

        <div class="forget-box">
            <a class="forget-text" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/ufpwd/'">忘記兌換密碼</a>
        </div>

        <!--  彈窗-驗證訊息  -->
        <div class="errorModal">
            <div class="modal-content">
                <div class="modal-body">
                    <ul></ul>
                </div>
                <div class="modal-footer">
                    <a class="close_btn">確認</a>
                </div>
            </div>
        </div>
        <div class="errorModalBg"></div>
                 
        <div class="footernav">
            <div class="footernav-button">
                <button id="submitBtn" type="button" class="ui-btn ui-btn-a ui-corner-all disabled" onclick="mall_submit()">確認兌換</button>
                <button id="cancel" type="button" class="ui-btn ui-btn-a ui-corner-all" onclick="closeWV()">取消兌換</button>
            </div>
        </div>
    </form>
</div>

<!--  鍵盤-自訂義JS  -->
    <script src="<?php echo BASE_URL.APP_DIR;?>/static/js/_captcha_keyboard.js"></script>
<!--  小鍵盤動作  -->
<script>
    var $wherekey,
    $open,
    $thisBtn,
    $much = $(".captcha_keyboard li").length;
    _captcha_options.btn.sure = '確認';
    $(".captcha_keyboard").each(function(i,e){
        $(this)
            .attr("data-toggle","modal")
            .attr("data-target","#captcha_numkeyModal");
        $(this).on("click",function(e){
            //生成按鈕(畫面沒有小鍵盤才生成)
            if(!$("#captcha_numkeyModal").length > 0){
                keyModal();
            }else{
                $('#captcha_numkeyModal').remove();
            }
            var $domEle=$("#passport");

            //先儲存點選哪個輸入框
            $wherekey = $(".numHideBox");
            $open = $(this).data("target");

            e.stopPropagation;
            e.preventDefault;

            //將原先輸入框裡的值帶入鍵盤的暫存區
            if ($wherekey.val()!=''){
                $domEle.val(String($wherekey.val()));
            }else{
                $domEle.val('');
            }

            //計算
            _captcha_Calculation($wherekey,$much);

            //Event-關閉小鍵盤後的動作
            $('#captcha_numkeyModal').on('hide.bs.modal', function (e) {
                $('#captcha_numkeyModal').remove();
                $(".num-box li").find(".pic").remove();

                //驗證
                var $tabNum = String($wherekey.val()).length,
                    $error = $(".captcha-msgError"),
                    $errorBox = $(".errorModal .modal-body ul");

                array = [];

                //判斷是否填滿所有格數
                if($tabNum == $much){
                    if(array.length !== 0){
//                            $error.html(array.join(''));
//                            $errorBox.html(array.join(''));
//                            e.preventDefault;
//                            openErrorModal();
                    }else{
                        $('.footernav #submitBtn')
                        .removeClass('disabled')
                        .attr('onClick','mall_submit()');
                        $error.html('');
                    }
                }else{
                    $('.footernav #submitBtn')
                        .addClass('disabled')
                        .attr('onClick','');
                    setError("請填寫完整兌換密碼");
                    $error.html(array.join(''));
                }
            })
        });
    })
    
    
    //防止小鍵盤沒吊起，每600毫秒判斷未吊起時重新點擊開啟
    var interval = null;
    $(function(){
        interval = setInterval(Continuous,600);
    })
    //持續判斷小鍵盤
    function Continuous() {
        var $modal = $("#captcha_numkeyModal");
        if($modal.hasClass("show")){
            //已吊起，要移除持續判斷
            clearInterval(interval);
        }else{
            $(".captcha_keyboard").click();
        }
    }

    var array;
    function setError(msg){
        array.push('<li>'+msg+'</li>');
    };
</script>
