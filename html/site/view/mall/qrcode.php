<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$qrcodeArr = _v('qrcodeArr'); 
?>
<script type="text/javascript">

$(function(){
	
	refreshData();
});

function refreshData() {
	$.ajax({
		url: "<?php echo BASE_URL.APP_DIR.'/mall/check_qrcode/'; ?>",
		type: "POST", 
		data: { uid: "<?php echo $_SESSION['auth_id']; ?>" },
		dataType: "json",
		success: function(data) {
			//var json = $.parseJSON(data);
			//console && console.log($.parseJSON(data));
			
			if (data.msg == 'no') {
				setTimeout(refreshData, 1000);
			}
			else if (data.msg == 'yes') {
				location.href = "<?php echo BASE_URL.APP_DIR.'/mall/qrcode_exchange/?data='; ?>" + data.data;
			}
		}
	});
}

function qrcode() {
	var num = $("#num").val();
	
	if (num == "") {
		alert("請輸入商品编號或鯊魚點數");
		return false;
	}
	
	$.ajax({
		url: "<?php echo BASE_URL.APP_DIR.'/mall/timerefresh/'; ?>",
		dateType: "json",
		success: function(data) {
			var json = $.parseJSON(data);
			console && console.log($.parseJSON(data));
			//alert("val" + $("input[name='radio-exchange']:checked").val());
			if ($("input[name='radio-exchange']:checked").val() == "epid") {
				
				$("#tagBox").html('<img style="-webkit-user-select: none" src="http://www.shajiawang.com/site/phpqrcode/?data=http://www.shajiawang.com/site/mall/qrcode_confirm/?data=epid=' + num + '_uid=<?php echo $_SESSION['auth_id']; ?>_t=' + json.t + '">');
			}
			else if ($("input[name='radio-exchange']:checked").val() == "amount") {
				$("#tagBox").html('<img style="-webkit-user-select: none" src="http://www.shajiawang.com/site/phpqrcode/?data=http://www.shajiawang.com/site/mall/qrcode_confirm/?data=amount=' + num + '_uid=<?php echo $_SESSION['auth_id']; ?>_t=' + json.t + '">');
			}
		}
	});
}

</script>
		<div class="article">
			<ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                    <label for="radio-exchange-epid">商品编號</label>
                    <input type="radio" name="radio-exchange" id="radio-exchange-epid" value="epid"  />
                    <label for="radio-exchange-amount">鯊魚點數</label>
                    <input type="radio" name="radio-exchange" id="radio-exchange-amount" value="amount"  />
                </li>
                <li class="ui-field-contain"><input name="num" id="num" value="" data-clear-btn="true" type="text" placeholder="輸入商品编號或鯊魚點數" disabled></li>
                <li id="qrcode"><div id="tagBox"></div></li>
            </ul>
            <div><a href="#" data-role="button" data-rel="qrcode" onClick="qrcode();">產生QRCode</a></div>
        </div><!-- /article -->
<script type="text/javascript">

$(function(){
	var epid = "<?php echo $qrcodeArr['epid']; ?>";
	var amount = "<?php echo $qrcodeArr['amount']; ?>";
	
	if (epid == '' && amount == '') {
		alert('資料錯誤');
		location.href = "<?php echo BASE_URL.APP_DIR.'/mall/'; ?>";
	}
	else {
		//$("input[name='radio-exchange']").buttonset("refresh");
		
		if (epid) { 
			$('input[name="radio-exchange"][value="epid"]').prop('checked', true).checkboxradio("refresh");
			$("#num").val(epid);
		}
		else {
			$('input[name="radio-exchange"][value="amount"]').prop('checked', true).checkboxradio("refresh");
			$("#num").val(amount);
		}
	}
	
});

</script>