<?php
$status = _v('status');
$product = _v('product');
$product_category = _v('product_category');
$stock = _v('stock');
$order_exchange = _v('order_exchange');
$user_extrainfoArr = _v('user_extrainfoArr');
$spoint = _v('spoint');

if($user->is_logged && !empty($_SESSION['user']) ) {
    $getuser['name'] = $_SESSION['user']['profile']['addressee']; 
    $getuser['zip'] = $_SESSION['user']['profile']['area'];
    $getuser['address'] = $_SESSION['user']['profile']['address'];
    $auth = _v('auth'); 
} else {
    $getuser['name'] = ''; 
    $getuser['zip'] = '';
    $getuser['address'] = '';
}

//圖片載入路徑
if(!empty($product['thumbnail_file'])) { //$status['path_image'] 
    $img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail_file'];
} else if(!empty($product['thumbnail'])) { //$status['path_image'] 
    $img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail'];
} else if (!empty($product['thumbnail_url'])) {
    $img_src = $product['thumbnail_url']; 
} else {
    $img_src = BASE_URL . APP_DIR .'/images/site/shark_logo.png';
}
?>

<?php if (($_GET['epid'] != 581) && ($_GET['epid'] != 525)){ ?>
    <div class="header-box">
        <div class="header-img">
            <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>" alt="">
        </div>
       
        <div class="header-title-box d-flex flex-column align-items-center justify-content-center">
            <h2 class="header-title"><?php echo $product['name']; ?></h2>
            <div class="head-pinttxt">
                <span>巿價 <?php echo sprintf("%d", (float)$product['retail_price']); ?> 元</span>
            </div>
        </div>
    </div>
    <div class="mall-lists">
        <div class="group">
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
                        <p>所需鯊魚點</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <span id="totalpoint" class="pint"><?php echo sprintf("%d", ((float)$product['point_price'] * 1)); ?></span>
                    <span>點</span>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
						<p>處理費 <?php echo $product['msg']; ?></p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <span id="processing" class="pint"><?php echo sprintf("%d", (float)$product['process_fee'])+$product['eimoney']; ?></span>
					<span>點</span>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
                        <p>兌換數量</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <div class="change-btn btnLess" onClick="number_count('dwn');">
                        <img src="<?PHP echo APP_DIR; ?>/static/img/numChange/num-less2.png" alt="">
                    </div>
                    <input name="num" id="num" type="number" class="change-num" value="1" readonly>
                    <div class="change-btn btnAdd" onClick="number_count('up');">
                        <img src="<?PHP echo APP_DIR; ?>/static/img/numChange/num-add.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="mall-list d-flex align-items-center" onClick="ReverseDisplay('detail');">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
                        <p>商品詳情</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <div class="rtext-box r-icon">
                        <p></p>
                    </div>
                </div>
            </div>
            <div id="detail" style="display:none">
                <div class="detail-txt"><?php echo $product['description']; ?></div>
            </div>
        </div>
    </div>
    <div class="footernav">
        <div class="footernav-button">
            <?php if(!empty($stock) ) {
                if($user->is_logged) {
            ?>
            <?php if(empty($auth['verified']) || $auth['verified']!=='Y'){ ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="checkphone()">結算去</button>
            <?php } else { ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="mall_product()">結算去</button>
            <?php } ?>
            <?php } else { ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="mall_login()">結算去</button>
            <?php } ?>
            <?php } else { ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all disabled">庫存不足</button>
            <?php }//endif ?>
        </div>
    </div>

    <input type="hidden" name="epcid" id="epcid" value="<?php echo $_GET[" epcid "]; ?>">
    <input type="hidden" name="epid" id="epid" value="<?php echo $product['epid']; ?>">
    <input type="hidden" name="esid" id="esid" value="<?php echo $product[" esid "] ;?>">
    <input type="hidden" name="name" id="name" value="<?php echo $getuser['name'];?>">
    <input type="hidden" name="zip" id="zip" value="<?php echo $getuser['zip'];?>">
    <input type="hidden" name="address" id="address" value="<?php echo $getuser['address'];?>">
    <input type="hidden" name="bonus" id="bonus" value="<?php echo sprintf("%d", $product['point_price']); ?>">
    <input type="hidden" name="handling" id="handling" value="<?php echo sprintf("%d", $product['process_fee']); ?>">
    <input type="hidden" name="all_bonus" id="all_bonus" value="<?php echo sprintf("%d", $spoint['amount']); ?>">
    <input type="hidden" name="autobid" value="<?php echo $paydata['autobid'];?>" />
    <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
	<input type="hidden" name="eimoney" id="eimoney" value="<?php echo $product['eimoney']; ?>">
    <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">

<?php } else if ($_GET['epid'] == 581) { ?>
    <div class="header-box">
        <div class="header-img">
            <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>" alt="">
        </div>
        <h2 class="header-title"><?php echo $product['name']; ?></h2>
    </div>
    <form method="post" action="<?php echo BASE_URL.APP_DIR; ?>/mall/exchangeBonusToSpoint/" id="contactform" name="contactform">
        <div class="mall-lists">
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
                        <p>現有鯊魚點：</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <div class="rtext-box r-icon">
                        <p><span><?php echo (int)$spoint['amount']; ?></span> 點</p>
                    </div>
                </div>
            </div>
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
                        <p>兌換殺價幣：</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <input type="number" name="amount" id="amount" class="allinput-type1 rtext-input" placeholder="請輸入兌換殺價幣" min="100" max="<?php echo ($spoint[" amount "]-($spoint["amount "]*0.1));?>" onchange="mk_extotalpoint(this.id)">
                    <div>點</div>
                </div>
            </div>
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
                        <p>手續費：</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <div class="rtext-box r-icon">
                        <p><span id="handling">0</span> 鯊魚點</p>
                    </div>
                </div>
            </div>
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-title">
                        <p>總計：</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <div class="rtext-box r-icon">
                        <p><span id="totalbonus">0</span> 鯊魚點</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="footernav">
            <div class="footernav-button">
                <?php if($user->is_logged) { ?>
                <?php if(empty($auth['verified']) || $auth['verified']!=='Y'){ ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="checkphone()">兌換殺價幣</button>
                <?php } else { ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="exchangespoint()">兌換殺價幣</button>
                <?php } ?>
                <?php } else { ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="mall_login()">兌換殺價幣</button>
                <?php } ?>
            </div>
        </div>
        <input type="hidden" name="epcid" id="epcid" value="<?php echo $_GET[" epcid "]; ?>">
        <input type="hidden" name="epid" id="epid" value="<?php echo $product['epid']; ?>">
        <input type="hidden" name="esid" id="esid" value="<?php echo $product[" esid "] ;?>">
        <input type="hidden" name="json" id="json" value="N">
        <input type="hidden" name="use" id="use" value="<?php echo ($spoint[" amount "]-($spoint["amount "]*0.1));?>">
    </form>

<?php } else if ($_GET['epid'] == 525){ ?>
    <div class="header-box">
        <div class="header-img">
            <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>" alt="">
        </div>
        <h2 class="header-title"><?php echo $product['name']; ?></h2>
    </div>
    <form method="post" action="<?php echo BASE_URL.APP_DIR; ?>mall/" id="contactform" name="contactform">
        <div class="mall-lists">
            <div class="mall-list d-flex align-items-center">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-icon">
                        <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/epcount.png">
                    </div>
                    <div class="list-title">
                        <p>兌換</p>
                    </div>
                </div>
            </div>
            <div class="list-details" id="exp" style="">
                <div class="list-detail d-flex align-items-center">
                    <div class="detail-title">現有鯊魚點</div>
                    <div class="detail-info"><?php echo (int)$spoint['amount']; ?> 點</div>
                </div>
                <div class="list-detail d-flex align-items-center">
                    <div class="detail-title">兌換金額</div>
                    <div class="detail-info">
                        <input type="number" class="allinput-type1 rtext-input" placeholder="請輸入兌換金額" name="amount" id="amount" min="50" max="<?php echo $spoint[" amount "];?>" onchange="mall_ddcar(this.id)">
                    </div>
                </div>
                <div class="list-detail d-flex align-items-center">
                    <div class="detail-title">手機號</div>
                    <div class="detail-info">
                        <input type="phone" class="allinput-type1 rtext-input" placeholder="請輸入手機號" name="phone" id="phone">
                    </div>
                </div>
            </div>

            <div class="mall-list d-flex align-items-center" onClick="ReverseDisplay('detail');">
                <div class="list-title-box d-inlineflex align-items-center">
                    <div class="list-icon">
                        <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/camera.png">
                    </div>
                    <div class="list-title">
                        <p>商品詳情</p>
                    </div>
                </div>
                <div class="list-rtext d-inlineflex align-items-center">
                    <div class="rtext-box r-icon">
                        <p></p>
                    </div>
                </div>
            </div>
            <div class="list-details" id="detail" style="display:none">
                <div class="list-detail d-flex align-items-center">
                    <div class="detail-txt"><?php echo $product['description']; ?></div>
                </div>
            </div>
        </div>

        <div class="footernav">
            <div class="footernav-button">
                <?php if($user->is_logged) { ?>
                <?php if(empty($auth['verified']) || $auth['verified']!=='Y'){ ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="checkphone()">確定兌換</button>
                <?php } else { ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="mall_product()">確定兌換</button>
                <?php } ?>
                <?php } else { ?>
                <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="mall_login()">確定兌換</button>
                <?php } ?>
            </div>
        </div>

        <input type="hidden" name="num" id="num" value="1">
        <input type="hidden" name="epcid" id="epcid" value="<?php echo $_GET[" epcid "]; ?>">
        <input type="hidden" name="epid" id="epid" value="<?php echo $product['epid']; ?>">
        <input type="hidden" name="esid" id="esid" value="<?php echo $product[" esid "] ;?>">
        <input type="hidden" name="json" id="json" value="N">
        <input type="hidden" name="use" id="use" value="<?php echo ($spoint[" amount "]);?>">
    </form>

<?php } ?>
<!-- /article -->
<script type="text/javascript">
    function number_count(key) {
        var pageid = $.mobile.activePage.attr('id');
        var onum = $('#' + pageid + ' #num').val();
        var obonus = $('#' + pageid + ' #bonus').val();
        var ohandling = $('#' + pageid + ' #handling').val();
        var eimoney = '<?php echo $product['eimoney']; ?>';
		var epid = $('#' + pageid + ' #epid').val();
		
        var $btnLess = $(".btnLess img");
        var $lessSrc1 = <?php echo json_encode(APP_DIR);?>+'/static/img/numChange/num-less.png';  //黑色
        var $lessSrc2 = <?php echo json_encode(APP_DIR);?>+'/static/img/numChange/num-less2.png';  //灰色
        
        oldValue = onum,
            newVal = 0;
        
        if (key == 'up') {
			newVal = parseInt(oldValue) + 1;
            $btnLess.attr('src',$lessSrc1);
			var total = parseFloat(obonus) * newVal 
			if (epid == '689' && (parseFloat(obonus) * newVal > 30000)){
				newVal = parseInt(oldValue);
				$btnLess.attr('src',$lessSrc1);
				var total = parseFloat(obonus) * newVal; 			
			}
        } else {
            if (oldValue > 2) {
                newVal = parseInt(oldValue) - 1;
				var total = parseFloat(obonus) * newVal ;
            } else {
                newVal = 1;
                $btnLess.attr('src',$lessSrc2);
				var total = parseFloat(obonus) * newVal ;
            }
        }

        $('#' + pageid + ' #totalpoint').html(parseFloat(total).toFixed(0));
        $('#' + pageid + ' #num').val(newVal);
    }
</script>