<script>
    $(function(){
        //判斷使用者裝置
        var $device;        //android, ios, other
        (function whDevice() {
            //判斷使用者裝置是否支援觸控功能，判斷是否為行動版 (有觸控功能的筆電也判斷為行動版)
            function isMobile() {
                try{ document.createEvent("TouchEvent"); return true; }
                catch(e){ return false;}
            }

            //判斷使用者裝置為Android或iOS
            function isDevice() {
                var u = navigator.userAgent;
                var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android終端
                var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios終端
                if(isAndroid){
                    return 'Android';
                }else if(isiOS){
                    return 'iOS';
                }
            }

            if(isMobile()){
                var $mobile = isDevice();
                switch($mobile){
                    case 'Android':
                        $device = 'android';
                        break;
                    case 'iOS':
                        $device = 'ios';
                        break;
                }
            }else{
                $device = 'other';
            }
        }())
        //自動跳轉
        if($device == 'android') {
            window.location.href='https://play.google.com/store/apps/details?id=tw.com.saja.userapp';
        } else if($device == 'ios') {
            window.location.href='https://itunes.apple.com/tw/app/id1441402090';
        }
        
        //延遲跳轉 先保留
//            setTimeout(ChangeTime, 1000);
//            function ChangeTime() {
//                if($device == 'android') {
//                    window.location.href='https://play.google.com/store/apps/details?id=tw.com.saja.userapp';
//                } else if($device == 'ios') {
//                    window.location.href='https://itunes.apple.com/tw/app/id1441402090';
//                }
//                setTimeout(ChangeTime, 1000);
//            }
    })
    
</script>
<div></div>