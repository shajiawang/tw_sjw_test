<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$txArr=_v('txArr');
$bonus=_v('bonus');
$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
?>
<div class="article">
    <div class="codeBox">
        <?php if(!empty($txArr) && !empty($bonus) ){ ?>
        <div id="barcodeBox">
            <img src="<?php echo APP_DIR; ?>/barcodegen/genbarcode.php?text=<?php echo $txArr['tx_code_bar']; ?>">
        </div>
        <div id="qrcodeBox">
            <img src="<?php echo $txArr['thumbnail_url']; ?>" >
        </div>
        <div id="genQrCode2" >
            <a href="javascript:void(0);" id="genQrCode" class="weui-cell weui-cell_access">
                <div class="CodeBox">
                    <img width="20px" src="<?php echo APP_DIR;?>/static/img/mallqrre.png">
                    <h5>
                        <p>每分鐘自動更新</p>
                    </h5>
                </div>
            </a>
        </div>
        <?php } else { ?>
        <div id="genQrCode2" >
            <div class="CodeBox">
                <h1>
                    <span style="color:red">無可用餘額 ! </span>
                </h1>
            </div>
        </div>
        <?php } ?>

        <div id="bonus" class="d-flex align-items-center justify-content-center">
            <img class="bonusIcon" src="<?php echo APP_DIR;?>/static/img/member/depositspoint.png">
            <p class="title">餘額：</p>
            <div class="bonusPoint"><?php echo $bonus;?> 點</div>
        </div>
    </div>
    <div style="display:none">
        <label class="countdown" data-offtime2="<?php echo $txArr['expired_time']; ?>" ></label>
    </div>
    <input type="hidden" name="evrid" id="evrid" value="<?php echo $txArr['evrid']; ?>">
    <input type="hidden" name="tx_status" id="tx_status" value="<?php echo $txArr['tx_status']; ?>">
</div>

<script type="text/javascript">
    var now_time;
    var evrid = '<?php echo $txArr['evrid']; ?>';
    var socket;
    var host = '<?php echo $wss_url; ?>';
    function ws_bidding(result) {
        var json = JSON.parse(result);
        console.log(json.actid+" - "+json.evrid);
        if (json.evrid == evrid){
            switch(json.actid ) {
                case 'B2C_Vendor_Processing':
                    process(2);
                    break;
                case 'B2C_Wait_Approve':
                    process(3);
                    break;
                case 'B2C_Timeout_Cancel':
                    process(-1);
                    break;
                case 'B2C_Vendor_Cancel':

                    process(-2);
                    break;
            }
        }
    }
    $(document).on("pagecreate",function(event){
            var ua = navigator.userAgent.toLowerCase();
            var WS_URL = '<?php echo $config['wss_url']; ?>';
            WS_URL = '<?php echo $wss_url; ?>';
            if (ua.match(/MicroMessenger/i) == "micromessenger" && ua.match(/Android/i) == "android") {
                WS_URL = "ws://www.saja.com.tw/";
            }
            socket1 = new WebSocket(WS_URL);
            socket1.onmessage = function(ret){
                ws_bidding(ret.data);
            }
        $.get("<?php echo APP_DIR; ?>/lib/cdntime.php?"+getNowTime(), function(data) {
            now_time = data; //系統時間
        });

        $('#genQrCode').click(function() {
            window.location.href="<?php echo APP_DIR ?>/mall/genUserTxQrcode2/";
        });

<?php if(!empty($txArr) && !empty($bonus) ){ ?>
        if('1'=='2') {
        // if(is_weixn()) {  // disable socket templrary
        // alert('Polling !!');

            $(this).stopTime('losttime');
            $(this).everyTime('3s','losttime', function() {
                    var pageid = $.mobile.activePage.attr('id');
                    now_time = parseInt(now_time);
                    var end_time = $('#'+pageid+' .countdown').attr('data-offtime2'); //Deadline
                    var lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
                    if(lost_time>=0) {
                          // if(lost_time<10){lost_time = " " + lost_time;};
                          // $('#'+pageid+' .countdown').text(lost_time);
                          $.get("<?php echo APP_DIR ?>/mall/getQrcodeTxStatus/?evrid="+evrid,
                                 function(ret) {
                                       process(ret);
                                 }
                          );
                    } else if(lost_time<0) {
                        var now_status = $('#tx_status').val();
                        // alert("now_status : "+now_status);
                        if(now_status=='1') {
                           $(this).stopTime('losttime');
                            window.location.href="<?php echo APP_DIR ?>/mall/genUserTxQrcode2/";
                        }
                    }
            });

        } else {
            // alert('Socket !!');
            try {
                // alert(evrid);
                socket = new WebSocket(host);
                console.log('WebSocket - status ' + socket.readyState);
                socket.onopen = function(evt) {onOpen(evt)};
                socket.onmessage = function(evt) {onMessage(evt)};
                socket.onclose = function(evt) {onClose(evt)};
                socket.onerror = function(evt) {onError(evt)};
            } catch(ex) {
               console.log(ex);
               socket.close();
               socket=null;
            }
        }
<?php } ?>

    });

    // functions for socket begin

    function onOpen(evt) {
        sendSocket("ADD|"+evrid+"|U");
    }

    function onMessage(evt) {
        // alert("message:"+evt.data);
        var info = evt.data;
        var arr = info.split("|");
        var id=arr[0];
        var status=arr[1];
        if(id==evrid) {
            process(status);
            /*
            if (parseInt(status)==2){
                // 商家確認中(不需要再每分鐘更新了);
                $('#genQrCode2').html('<br>&nbsp;</br><font  color="#EDEDED">商家作?中...</font><br>&nbsp;</br>');
                $(this).stopTime('losttime');
            } else if(parseInt(status)==3) {
                if(socket)
                    socket.close();
                location.href="<?php echo APP_DIR ?>/mall/userConfirmTx/?evrid="+evrid;
            } else if(parseInt(status)<=0){
                $(this).stopTime('losttime');
                $('#genQrCode2').html('<br>&nbsp;</br><font  color="#EDEDED">交易取消 !!</font><br>&nbsp;</br>');
                alert("交易已取消 !!");
                location.href="<?php echo APP_DIR ?>/member/?"+evrid;
            }
            */
        }
    }

    function onClose(evt) {
        // console.log("close:"+evt.data);
    }

    function onError(evt){
        // console.log("err:"+evt.data);
    }

    function is_weixn(){
        var ua = navigator.userAgent.toLowerCase();
        if(ua.match(/MicroMessenger/i)=='micromessenger') {
            return true;
        } else {
            return false;
        }
    }
    // functions for socket end

    function process(status) {
        $('#tx_status').val(status);
        //console.log(status);
        switch(parseInt(status)){
            case 2:
                // 商家確認中(不需要再每分鐘更新了);
                $(this).stopTime('losttime');
                $('#genQrCode2 .CodeBox').html('<h5 style="color:#797979;font-size:1.334rem">商家作業中...</h5>');
                break;
            case 3:
                $(this).stopTime('losttime');
                window.location.href="<?php echo APP_DIR ?>/mall/userConfirmTx/?evrid="+evrid;
                break;
            case -1:
                $(this).stopTime('losttime');
                alert("整體操作逾時,交易已取消 !!");
                window.location.href="<?php echo APP_DIR ?>/member/";
                break;
            case -2:
                $(this).stopTime('losttime');
                alert("商家取消,交易已取消 !!");
                window.location.href="<?php echo APP_DIR ?>/member/";
                break;
        }

    }

    function sendSocket(message) {
        if(!is_weixn() && socket!=null) {
            socket.send(message);
        }
    }

</script>

<script type="text/javascript">
    //2018.10.09 改版
    $(function(){
        //更改標題
        $('.navbar-brand h3').text('兌換碼');
        //更改背景色
        $('.mall').css({'background':'#F9A823','height':'calc(100vmax - 3.75rem)','min-height':'unset'});
    })
</script>