<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$dataArr = _v('dataArr');
$order = _v('order');
$product = _v('product');
?>
<script type="text/javascript">
$(function(){
	refreshData();
});

function refreshData() {
	$.ajax({
		url: "<?php echo BASE_URL.APP_DIR.'/mall/qrcode_checkout/'; ?>",
		type: "POST", 
		data: { orderid: "<?php echo $dataArr[0]; ?>", userid: "<?php echo $dataArr[1]; ?>" },
		dataType: "json",
		success: function(data) {
			//var json = $.parseJSON(data);
			//console && console.log($.parseJSON(data));
			
			if (data.msg == 'no') {
				setTimeout(refreshData, 1000);
			}
			else if (data.msg == 'yes') {
				$("#success").text("兌換成功");
			}
		}
	});
}
</script>

	
		<div class="article">
			<ul data-role="listview" data-inset="true" data-icon="false">
                <li><h2><?php if ($order['epid'] != 0) { echo $product['name']; } else { echo "鯊魚點數"; } ?></h2></li>
                <li><h2><?php echo $order['total_fee']; ?></h2></li>
                <li><h2 id="success" style="height:200px;"></h2></li>
            </ul>
            <div><a href="#" data-role="button" onClick="qrcode_close()">關閉</a></div>
        </div><!-- /article -->