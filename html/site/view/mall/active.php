<style>
    .mallActiveContent {
        padding: 1.5rem 0;
    }

    .mallActiveContent .m-title {
        font-size: 1.584rem;
        text-align: center;
        padding-bottom: 1.5rem;
    }
    .mallActiveContent .m-title ~ .m-title {
        padding-top: 4rem;
    }
    .mallActiveContent .m-step {
        width: 85%;
        max-width: 400px;
        font-size: 1.334rem;
        margin: 0 auto;
        line-height: 1.6rem;
        padding: 1rem;
    }
    .mallActiveContent .m-step ~ .m-step {
        padding-top: 4rem;
    }
    .mallActiveContent .m-step .num {
        
    }
    .mallActiveContent .m-step .txt {
        flex: 1;
    }
    .mallActiveContent .m-img1 {
        margin: 0 auto;
        text-align: center;
        max-width: 60%;
    }
    .mallActiveContent .m-img2 {
        margin: 0 auto;
        text-align: center;
        width: 80%;
        max-width: 800px;
    }
    .mallActiveContent .m-img2 img + img {
        padding-top: 1rem;
    }
    .mallActiveContent .m-info {
        font-size: 1.2rem;
        line-height: 1.5rem;
        margin: 1rem;
    }
    .mallActiveContent .m-info .small {
        font-size: 1rem;
    }
    .mallActiveContent .m-info .red {
        color: #ff5722;
    }
</style>
   
<div class="mallActiveContent">
    <div class="m-title">鯊魚點ibon系統兌換流程</div>
    <div class="m-img1">
        <img class="img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-01.png" alt="">
    </div>
    <div class="d-flex justify-content-center">
        <div class="m-info">
            <p>會員帳號：顯示於我的帳戶欄位後方</p>
            <p>密　　碼：預設手機末六碼</p>
            <p class="red small">*為保障您的帳戶安全，請務必更改兌換密碼</p>
        </div>
    </div>
    
    <div class="m-title">ibon系統操作流程</div>
    <div class="m-step d-flex">
        <div class="num">步驟 1. </div>
        <div class="txt">點選好康紅利</div>
    </div>
    <div class="m-img2">
        <img class="img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-02.png" alt="">
    </div>
    <div class="m-step d-flex">
        <div class="num">步驟 2. </div>
        <div class="txt">選擇網路遊戲會員</div>
    </div>
    <div class="m-img2">
        <img class="img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-03.png" alt="">
    </div>
    <div class="m-step d-flex">
        <div class="num">步驟 3. </div>
        <div class="txt">點選殺價王及同意</div>
    </div>
    <div class="m-img2">
        <img class="img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-04.png" alt="">
        <img class="img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-05.png" alt="">
    </div>
    <div class="m-step d-flex">
        <div class="num">步驟 4. </div>
        <div class="txt">輸入帳號密碼</div>
    </div>
    <div class="m-img2">
        <img class="m-img img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-06.png" alt="">
        <img class="m-img img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-07.png" alt="">
        <img class="m-img img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-08.png" alt="">
    </div>
    <div class="m-step d-flex">
        <div class="num">步驟 5. </div>
        <div class="txt">就可以選擇要兌換商品還是列印購物金囉!</div>
    </div>
    <div class="m-img2">
        <img class="img-fluid" src="<?php echo APP_DIR; ?>/static/img/mall-active/ibon-09.png" alt="">
    </div>
</div>