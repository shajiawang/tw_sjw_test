<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$status = _v('status');
$product_list = _v('product_list');
$category = _v('category');
$dowmswitch = 'close'; 
//$dowmswitch = 'open';          //是否關閉WEB的鯊魚商城兌換功能  close / open
?>
<style>
    .qrcodebox {
        max-width: 15rem;
        margin: 0 auto .5rem;
    }
    .qrcodebox img {
        width: 100%;
    }
</style>
<div class="mall-header d-flex align-items-center">
    <div class="mall-header-item">
        <div onclick="scan()">
            <a href="#weixinpopupParis" data-rel="popup" data-position-to="window" data-transition="fade">
                <img class="item-icon" src="<?php echo BASE_URL.APP_DIR;?>/static/img/mallscan.png">
                <div class="item-label">掃碼器</div>
            </a>
        </div>
    </div>
    <div class="mall-header-item">
        <div onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/mall/genUserTxQrcode2/?<?php echo $cdnTime; ?>'">
            <a href="#weixinpopupParis" data-rel="popup" data-position-to="window" data-transition="fade">
                <img class="item-icon" src="<?php echo BASE_URL.APP_DIR;?>/static/img/mallqr.png">
                <div class="item-label">兌換碼</div>
            </a>
        </div>			  
    </div>
</div>
<div class="mall-grids">
    <div class="mall-grids-title">生活代繳</div>
    <div class="d-flex flex-wrap">
        <div class="mall-item app">
            <a class="mall-item-link dowmswitch" onclick="createMsgModal();" href="javascript:void(0)">
                <div class="mall-item-img">
                    <img src="/site/images/site/product/icon_phone.png" title="ibon" alt="ibon">
                </div>
                <p class="mall-item-title">電信費</p>
            </a>
        </div>   
        <div class="mall-item app">
            <a class="mall-item-link dowmswitch" onclick="createMsgModal();" href="javascript:void(0)">
                <div class="mall-item-img">
                    <img src="/site/images/site/product/icon_card.png" title="ibon" alt="ibon">
                </div>
                <p class="mall-item-title">信用卡費</p>
            </a>
        </div>   
        <div class="mall-item app">
            <a class="mall-item-link dowmswitch" onclick="createMsgModal();" href="javascript:void(0)">
                <div class="mall-item-img">
                    <img src="/site/images/site/product/icon_loan.png" title="ibon" alt="ibon">
                </div>
                <p class="mall-item-title">貸款費</p>
            </a>
        </div>                       	
    </div>
</div>

<?php $s1 =0;
foreach($category as $cgk => $cgv){
if(! empty($cgv['level2_list'])){ 
?>
<div class="mall-grids">
	<div class="mall-grids-title"><?php echo $cgv['name'];?></div>
            
	<div class="d-flex flex-wrap">
    <?php 
	$_count = count($cgv['level2_list']);
	$keys = array_keys($cgv['level2_list']);
	$s1 = $_count % 3;
	if($s1==2){
		array_push($cgv['level2_list'], '');
	} elseif($s1==1) {
		array_push($cgv['level2_list'], '','');
	}
	
	foreach($cgv['level2_list'] as $rk => $rv){ 
		$s2 = $rk % 3;
		if($s2 >0 && empty($rv)){
	?>
		<div class="mall-item free">
            <a class="mall-item-link ui-link" href="javascript:void(0)"/>
                <div class="mall-item-img"/>
                    <img src="/site/images/site/product/icon_wait.png" title="虛位以待" alt="虛位以待"/>
                </div>
				<p class="mall-item-title">虛位以待</p>
            </a>
        </div>
	<?php } else { ?>
		<div class="mall-item app" id="<?php echo $rv['epcid'];?>">
			<a class="mall-item-link dowmswitch" href="javascript:void(0)" >
				<div class="mall-item-img" >
					<?php if(!empty($rv['thumbnail'])){ ?>
					<img src="<?php echo $status['path_image']; ?>/product/<?php echo $rv['thumbnail']; ?>" title="<?php echo $rv['name'];?>" alt="<?php echo $rv['name'];?>" >
					<?php } else { ?>
					<img src="" >
					<?php }//endif; ?>
				</div>
				<p class="mall-item-title"><?php echo $rv['name'];?></p>
			</a>
		</div>
	<?php }//endif; ?> 
	<?php }//end_foreach; ?>
	
	<?php /*foreach($product_list['table']['record'] as $rk => $rv){ ?>
    <?php if ($cgv['epcid'] == $rv['epcid']){ ?>
				<div class="mall-item app" id="<?php echo $rv['epcid'];?>">
                    <?php if ($dowmswitch == 'close'){?>
                    <a class="mall-item-link dowmswitch" href="javascript:void(0)">
                    <?php }else{ ?>
						<?php if (!empty($rv['tx_url'])){ ?>
                        <a class="mall-item-link" href="#" onclick="javascript:location.href='<?php echo $rv['tx_url']; ?>'">
                        <?php }else{ ?>
                        <a class="mall-item-link" href="#" onclick="javascript:location.href='<?php echo APP_DIR;?>/mall/exchange/?<?php echo $status['status']['args'];?>&epcid=<?php echo $rv['epcid']; ?>&epid=<?php echo $rv['epid']; ?>'">
                        <?php } ?>
					<?php } ?>
						<div class="mall-item-img" >
							<?php if(!empty($rv['thumbnail'])){ ?>
							<img src="<?php echo $status['path_image']; ?>/product/<?php echo $rv['thumbnail']; ?>"   title="<?php echo $rv['name'];?>" alt="<?php echo $rv['name'];?>" >
							<?php } elseif(!empty($rv['thumbnail_url'])){ ?>
							<img src="<?php echo $rv['thumbnail_url']; ?>">
							<?php } else { ?>
							<img src="" >
							<?php }//endif; ?>
						</div>
						<p class="mall-item-title"><?php echo $rv['name'];?></p>
						</a>
                </div>
	<?php } ?>	
	<?php }*/ ?>
    </div>
			
</div>
<?php } } ?>

<script type="text/javascript">
window.onload=function (){
    $('#3, #7, #10, #14, #17').parent().append(
        $('<div class="mall-item free"/>')
        .append(
        $('<a class="mall-item-link ui-link" href="#"/>')
            .append(
            $('<div class="mall-item-img"/>')
                .append(
                    $('<img src="/site/images/site/product/icon_wait.png" title="虛位以待" alt="虛位以待"/>')
                )
            )
            .append(
                $('<p class="mall-item-title">虛位以待</p>')
            )
        )
    );

    
    $('#20').parent().append(
        $('<div class="mall-item free"/>')
        .append(
        $('<a class="mall-item-link ui-link" href="#"/>')
            .append(
            $('<div class="mall-item-img"/>')
                .append(
                    $('<img src="/site/images/site/product/icon_wait.png" title="虛位以待" alt="虛位以待"/>')
                )
            )
            .append(
                $('<p class="mall-item-title">虛位以待</p>')
            )
        ).after(

            $('<div class="mall-item free"/>')
            .append(
            $('<a class="mall-item-link ui-link" href="#"/>')
                .append(
                $('<div class="mall-item-img"/>')
                    .append(
                        $('<img src="/site/images/site/product/icon_wait.png" title="虛位以待" alt="虛位以待"/>')
                    )
                )
                .append(
                    $('<p class="mall-item-title">虛位以待</p>')
                )
            )
        )
    );
	
    $('.mall-item.free .mall-item-link').on('click',function(){
            createModal();
    })
    
    function createModal(){
        $('body').addClass('modal-open');
        $('body').append(
            $('<div class="coModalBox d-flex justify-content-center align-items-center"/>')
            .append(
            $('<div class="modal-content"/>')
                .append(
                $('<div class="modal-body"/>')
                    .append(
                        $('<div class="modal-title"/>')
                        .append(
                            $('<h1>商城版位合作</h1>')
                        )
                        .append(
                            $('<p>歡迎個人及公司進行洽談</p>')
                        )
                        .append(
                            $('<p>請私訊官方粉絲團</p>')
                        )
                    )
                )
                .append(
                    $('<div class="modal-footer d-flex justify-content-center align-items-center"/>')
                    .append(
                        $('<div class="footer-btn m-check">確定</div>')
                    )
                )
            )
            .append(
                $('<div class="msgModalBg">')
            )
        )
        $('.m-check , .msgModalBg').on('click', function(){
            $('body').removeClass('modal-open');
            $('.coModalBox').remove();
        })
    } 
}

function scan(){
	var uri="<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']; ?>";
	uri=(uri.split('#')[0]);
	$.ajax({
		url: "https://www.shajiawang.com/site/product/weixinSDK/",
		data: { wurl:uri },
		dataType: "JSON",
		type: "POST",
		contentType : "application/x-www-form-urlencoded; charset=utf-8",
		success: function(data) {
			console.log(data);
			var configData={
				debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参數，可以在pc端打开，参數信息会通过log打出，仅在pc端时才会打印。
				appId: data.appId, // 必填，公众號的唯一標识
				timestamp: data.timestamp, // 必填，生成签名的時間戳
				nonceStr: data.nonceStr, // 必填，生成签名的随機串
				signature: data.signature, // 必填，签名
				jsApiList: [
					'scanQRCode'
				]
			};
			console.log(configData);
			wx.config(configData);	
		}
	});

	wx.error(function (res) {
		// alert('wx.error: '+JSON.stringify(res));
	});

	wx.ready(function () {
				
	});
	
	var ua = navigator.userAgent.toLowerCase();
	// alert(ua);
	
	if(ua.match(/MicroMessenger/i)=="micromessenger") {
	    wx.scanQRCode({
			needResult: 0, 
			scanType: ["qrCode","barCode"],
			success: function (res) {
				// alert(res);
				var result = res.resultStr;
			}
		});
	} else {
		// appbsl
		if(is_kingkr_obj()){
            qrcode(0);
        }else{
           alert('瀏覽器內無法調用 qrcode scanner');
        }		
	}
}

// appbsl 的qrcode callback function
function qrcodeCallback(result){
    alert(result);
    //result為扫描的數据结果。
    //請添加业务逻辑
}
function gpscallback(result){
    alert(result);
}
function logincallback(result) {
    alert(result);
}

function esservice(a,b) {
	$.ajax({
		url : '<?php echo APP_DIR;?>/mall/extExchangeService/?<?php echo $status['status']['args'];?>&epcid='+a+'&epid='+b,
		type : 'POST',
		data : '',
		processData: false,  // tell jQuery not to process the data
		contentType: false,  // tell jQuery not to set contentType
		success : function(req) {
			console.log(req);
			var json = $.parseJSON(req);
			console.log(json.url);
			location.href = json.url;
		}
	});
}

function nologin() {
	// alert("請登入 享受更多優惠 !!");
	location.href="/site/member/userlogin/";
}
</script>

<script type="text/javascript">
    //判斷使用者裝置
    var $device;        //android, ios, other
    (function whDevice() {
        //判斷使用者裝置是否支援觸控功能，判斷是否為行動版 (有觸控功能的筆電也判斷為行動版)
        function isMobile() {
            try{ document.createEvent("TouchEvent"); return true; }
            catch(e){ return false;}
        }
        
        //判斷使用者裝置為Android或iOS
        function isDevice() {
            var u = navigator.userAgent;
            var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android終端
            var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios終端
            if(isAndroid){
                return 'Android';
            }else if(isiOS){
                return 'iOS';
            }
        }
        
        if(isMobile()){
            var $mobile = isDevice();
            switch($mobile){
                case 'Android':
                    $device = 'android';
                    console.log('裝置', $device)
                    break;
                case 'iOS':
                    $device = 'ios';
                    console.log('裝置', $device)
                    break;
            }
        }else{
            $device = 'other';
            console.log('裝置', $device)
        }
    }())
    
    function mallClose() {
        $('.mall-item.app .mall-item-link').on('click',function(){
            createMsgModal();
        })
    }
    
    function createMsgModal(){
        $('body').addClass('modal-open');
        $('body').append(
            $('<div class="downloadModalBox d-flex justify-content-center align-items-center"/>')
            .append(
            $('<div class="modal-content"/>')
                .append(
                $('<div class="modal-body"/>')
                    .append(
                        ($device !== 'android' && $device !== 'ios')?
                            $('<div class="qrcodebox"><img src="<?php echo BASE_URL.APP_DIR; ?>/phpqrcode/?data=<?php echo BASE_URL.APP_DIR."/mall/qrpage"; ?>" ></div>'):''
                    )
                    .append(
                        $('<div class="modal-title"/>')
                        .append(
                            $('<p>為了安全起見</p>')
                        )
                        .append(
                            $('<p>本功能僅能在APP使用</p>')
                        )
                        .append(
                            ($device == 'android' || $device == 'ios')?
                                $('<p>點擊按鈕下載</p>'):$('<p>請用手機掃描QRcode</p>')
                        )
                    )
                )
                .append(
                    $('<div class="modal-footer d-flex justify-content-center align-items-center"/>')
                    .append(
                        $('<div class="footer-btn m-close">取消</div>')
                    )
                    .append(
                        ($device == 'android')?
                            $('<div class="footer-btn android">下載</div>'):''
                    )
                    .append(
                        ($device == 'ios')?
                            $('<div class="footer-btn ios">下載</div>'):''
                    )
                )
            )
            .append(
                $('<div class="msgModalBg">')
            )
        )
        $('.footer-btn.android').on('click', function(){
            location.href='https://play.google.com/store/apps/details?id=tw.com.saja.userapp';
        })
        $('.footer-btn.ios').on('click', function(){
            location.href='https://itunes.apple.com/tw/app/id1441402090';
        })
        $('.m-close , .msgModalBg').on('click', function(){
            $('body').removeClass('modal-open');
            $('.downloadModalBox').remove();
        })
    }              //下載彈窗
    
    //將鯊魚商城改為僅限APP
    <?php if ($dowmswitch == 'close'){?>
        mallClose();
    <?php } ?>
</script>
		