<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$txArr=_v('txArr'); 
// Test to set cookie
setcookie('etprs_id','2',time() + (86400 * 7)); // 86400 = 1 day
?>
<style type="text/css">
    .codeBox {
	    background-color:#ffffff;
		padding-top:8%;
		padding-left:5%;
		padding-bottom:8%;
		padding-right:5%;
        border:#3B3B3B 30px solid;
	};
</style>
<hidden name="evrid" id="evrid" value="<?php echo $txArr['evrid']; ?>">
<div class="article" style="background-color:#3B3B3B" >
		          <label align="center" style="color:#EDEDED" >给收银员扫一扫完成線下購物</label>
				  <label align="center" style="color:#EDEDED" >或让对方扫碼付款给我</label>
				  <div class="codeBox">
					  <div id="barcodeBox" align="center" >
						<img style="-webkit-user-select: none" width="95%" src="http://test.shajiawang.com/site/barcodegen/genbarcode.php?text=<?php echo $txArr['tx_key_base64']; ?>">
					  </div>
					  <p>
					  <div id="qrcodeBox" align="center" >
					  <?php if($txArr['ClientType']=='web') { ?>
								<img style="-webkit-user-select: none" width="70%" src="http://test.shajiawang.com/site/phpqrcode/?data=<?php echo "http://test.shajiawang.com/site/mall/vendorConfirmWebTx/?key=".$txArr['evrid']."|".$txArr['tx_key_md5']; ?>" >
					  <?php } else {?>
								<img style="-webkit-user-select: none" width="70%" src="http://test.shajiawang.com/site/phpqrcode/?data=<?php echo $txArr['tx_key_base64']; ?>" >
					  <?php } ?>
					  </div>
				  </div>
				  <label align="center" id="txStatusDesc" style="color:#EDEDED;display:none" ></label>
				  <div id="genQrCode"  align="center">
				      <a href="#" style="background-color:#3B3B3B;border:none;" data-role="button" data-icon="refresh" data-iconpos="right" data-inline="true">
		 			  <font color="#EDEDED">
			          每分钟自動更新
        			  </font>
					  </a>
				  </div>
				  <div style="display:none">
				      <label class="countdown" data-offtime2="<?php echo $txArr['expired_time']; ?>" data-tag2=""></label>
                  </div>
            <!-- /ul -->

</div><!-- /article -->
<script type="text/javascript">
		var now_time;
		var tx_status='1';
		var tx_status_desc='送出交易資訊..';
		var evrid = '<?php echo $txArr['evrid']; ?>';
		
		$(document).on("pagecreate",function(event){
		    $.get("/site/lib/cdntime.php?"+getNowTime(), function(data) {
				now_time = data; //系統時間
			});		
			
			$('#genQrCode').click(function() {
			    window.location.href="<?php echo APP_DIR ?>/mall/genUserTxQrcode/?<?php echo $txArr['expired_time']; ?>";
			});
		    $(this).stopTime('losttime');
			$(this).everyTime('1s','losttime', function() {
					var pageid = $.mobile.activePage.attr('id');
					now_time = parseInt(now_time);
					var end_time = $('#'+pageid+' .countdown').attr('data-offtime2'); //截標時間
					var end_plus = $('#'+pageid+' .countdown').attr('data-tag2'); //標籤
					var lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
					if(lost_time >=0) {
						  // if(lost_time<10){lost_time = " " + lost_time;};
						  $('#'+pageid+' .countdown').html(end_plus+lost_time);
						  if((lost_time % 2)==0) {
							  getTxStatus(evrid);
							  if(tx_status==2) {
								  // 商家確認中(不需要再每分鐘更新了);
								  $('#genQrCode').attr("disabled", true);
								  $('#genQrCode').hide();
								  $('#txStatusDesc').text('商家受理中..');
								  $('#txStatusDesc').show();
							  } else if(tx_status==3) {
							     // 商家已輸入完成
								 $(this).stopTime('losttime');
								 location.href="<?php echo APP_DIR ?>/mall/userConfirmTx/?evrid="+evrid;
							  } else if(tx_status<=0) {
							      $(this).stopTime('losttime');
								  $('#genQrCode').attr("disabled", true);
								  $('#genQrCode').hide();
								  $('#txStatusDesc').text("交易已取消 !");
								  $('#txStatusDesc').show();
							  }
							  // $('#'+pageid+' #tx_status').text('交易状态 : '+tx_status_desc);
						   }
					} else if(lost_time <0) {
						if(tx_status=='1') {
						   $(this).stopTime('losttime');
						   window.location.href="<?php echo APP_DIR ?>/mall/genUserTxQrcode/?<?php echo $txArr['expired_time']; ?>";
						} else if(tx_status=='2') {
                           $('#'+pageid+' .countdown').html(end_plus+lost_time+60);
						}
                    	// $('#'+pageid+' #tx_status').text('交易状态 : 已取消');
						// $('#'+pageid+' #qrcodeBox').hide();
						// $('#'+pageid+' #genQrCode').show(); 
					}
			});
		});
		
		function getTxStatus(evrid) {
		         $.get("<?php echo APP_DIR ?>/mall/getQrcodeTxStatus/?evrid="+evrid, 
					 function(retdata) {
					   tx_status=retdata;  
					 }
				 );
		}

</script>