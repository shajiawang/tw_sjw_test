<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$record = _v('record');

$keyin_time=strtotime($record['keyin_time']);
$expired_time=$keyin_time+300;

if (!empty($record['thumbnail_url'])){
    $src = $record['thumbnail_url'];
} else {
    $src = BASE_URL.'/management/images/headimgs/'.$record['thumbnail_file'];
}
?>
    <style>
        input#expw {
            text-security: disc;
            -webkit-text-security: disc;
            -moz-text-security: disc;
        }
    </style>
    <div>
        <div class="sajamsg-imgbox">
            <div class="mallMsg-box">
                <div class="msg-img" style="min-height: 8.334rem;">
                    <img class="img-fluid" src="<?php echo $src; ?>">
                </div>
                <h4 class="msg-txt"><?php echo $record['vendor_name'] ?></h4>
                <h4 class="msg-txt">使用鯊魚點：<span class="msg-price"><?php echo intval($record['total_price']) ?></span> 點</h4>
                <input type="hidden" name="bonus_noexpw" id="bonus_noexpw" value="<?php echo $record['bonus_noexpw']; ?>" />
                <input type="hidden" name="userid" id="userid" value="<?php echo $record['userid']; ?>" />
                <input type="hidden" name="evrid" id="evrid" value="<?php echo $record['evrid']; ?>" />
                <input type="hidden" name="bonus_total" id="bonus_total" value="<?php echo $record['total_bonus'] ?>" />
                <div  style="display:none">
                    <label class="countdown" data-offtime2="<?php echo $expired_time; ?>" data-tag2=""></label>
                </div>
            </div>
        </div>
        <div class="sajamsg-btnbox">
            <button id="cancelTx" class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button">
                <div>我要取消</div>
            </button>
            <button id="popup_confirm" class="saja-btn ui-btn ui-btn-a ui-corner-all" type="button">
                <div>確認兌換</div>
            </button>
        </div>
    </div>

    <div id="question">
        <div class="quest_modal">
            <div class="header"><span>請輸入兌換密碼</span></div>
            <div class="main">
                <div class="listview">
                    <div><?php echo intval($record['total_bonus']) ?> 點</div>
                    <div>鯊魚點支付</div>
                    <div class="expw-box d-flex align-items-center">
                        <input type="tel" value="" size="16" name="expw" id="expw"  pattern="[0-9]*" inputmode="numeric" placeholder="兌換密碼">
                    </div>
                    <div class="ui-grid-solo">
                        <button id="no">取消</button>
                        <button id="yes" disabled='disabled'>兌換</button>
                    </div>
                </div>
            </div>
        </div>


        <div class="quest_modalbg"></div>
    </div>

    <script src="/site/static/js/blockUI.js"></script>
    <script type="text/javascript">
        // var now_time;   // has been defined in share.js
        var jevrid, juserid, jbonus, jexpw, jbonus_noexpw;
        var socket;
        var wssurl = '<?php echo $config['wss_url']; ?>';
        var evrid = '<?php echo $record['evrid']; ?>';
        var baseurl = '<?php echo BASE_URL; ?>';
        var pageid;
        var end_time;
        var lost_time;

        baseurl = '';

        $(document).on("pagecreate", function(event) {
            /*
            $.get(baseurl+"/site/lib/cdntime.php?"+getNowTime(),
                    function(data) {
                       now_time = data; //系統時間
                    }
            );
            */
            const dateTime = new Date().getTime();
            const now_time = Math.floor(dateTime / 1000);

            // if(is_weixn()) {
            if (true) {
                $(this).stopTime('losttime2');
                $(this).everyTime('1s', 'losttime2', function() {
                    pageid = $.mobile.activePage.attr('id');
                    end_time = $('#' + pageid + ' .countdown').attr('data-offtime2'); //結束時間
                    // var end_plus = $('#'+pageid+' .countdown').attr('data-tag2'); //標籤
                    lost_time = parseInt(end_time) - parseInt(now_time); //剩餘時間
                    if (lost_time >= 0) {
                        // if(lost_time<10){lost_time = "0" + lost_time;};
                        $('#' + pageid + ' .countdown').html(lost_time);
                    } else if (lost_time < 0) {
                        $(this).stopTime('losttime2');
                        $.get(baseurl + "<?php echo APP_DIR; ?>/mall/cancelQrcodeTx/?evrid=" + evrid,
                            function(data) {
                                var json = $.parseJSON(data);
                                if (json.retCode == '1') {
                                    alert(decodeURIComponent(json.retMsg));
                                }
                                location.href = baseurl + "<?php echo APP_DIR; ?>/member/";
                            }
                        );
                    }
                });
            } else {
                socket = new WebSocket(wssurl);
                socket.onopen = function(evt) {
                    onOpen(evt)
                };
                socket.onmessage = function(evt) {
                    onMessage(evt)
                };
                socket.onclose = function(evt) {
                    onClose(evt)
                };
                socket.onerror = function(evt) {
                    onError(evt)
                };
            }

        });

        $(document).ready(function() {
            $('#expw').val('');
            $('#question').hide();
            $('#popup_confirm').click(function() {
                // $.blockUI({ message: $('#question'), css: { width: '50%' } });
                jevrid = $('#evrid').val();
                juserid = $('#userid').val();
                jbonus = $('#bonus_total').val();
                jbonus_noexpw = $('#bonus_noexpw').val();
                // $.blockUI({ message: $('#question') });
                console.log(jevrid + "-" + juserid + "-" + jbonus + "-" + jbonus_noexpw);
                if (jbonus_noexpw == '') {
                    jbonus_noexpw = 0.0;
                }
                jbonus = parseFloat(jbonus);
                jbonus_noexpw = parseFloat(jbonus_noexpw);

                if (jbonus > jbonus_noexpw) {
                    $.blockUI({
                        message: $('#question')
                    });
                } else {
                    $.post(baseurl + "<?php echo APP_DIR; ?>/mall/userCommitTx/", {
                            "evrid": jevrid,
                            "userid": juserid,
                            "bonus_total": jbonus,
                            "bonus_noexpw": jbonus_noexpw
                        },
                        function(data) {
                            //console && console.log(data);
                            json = $.parseJSON(data);
                            alert(decodeURIComponent(json.retMsg));
                            if (json.retCode == '1') {
                                // sendSocket("NTFY|"+evrid+"|4");
                                location.href = baseurl + "<?php echo APP_DIR; ?>/history/bonus/?<?php echo date("YmdHis");?>&tab=2";
                            }
                        }
                    );
                }

            });

            $('#yes').click(function() {
                // update the block message
                jevrid = $('#evrid').val();
                juserid = $('#userid').val();
                jbonus = $('#bonus_total').val();
                jbonus_noexpw = $('#bonus_noexpw').val();
                jexpw = $('#expw').val();
                $('#expw').val('');
                $.blockUI({
                    message: ''
                });
                $.post(baseurl + "<?php echo APP_DIR; ?>/mall/userCommitTx/", {
                        "evrid": jevrid,
                        "userid": juserid,
                        "bonus_total": jbonus,
                        "expw": jexpw,
                        "bonus_noexpw": jbonus_noexpw
                    },
                    function(data) {
                        $.unblockUI();
                        // console && console.log(data);
                        json = $.parseJSON(data);
                        window.alert(decodeURIComponent(json.retMsg));
                        if (json.retCode == '1') {
                            // sendSocket("NTFY|"+evrid+"|4");
                            location.href = baseurl + "<?php echo APP_DIR; ?>/history/bonus/?<?php echo date("YmdHis");?>&tab=2";
                        }
                    }
                );

            });
            /*
                $('#no').click(function() {
                    $.unblockUI();
                    return false;
                });
                */
            $('#cancelTx').click(function() {
                $.post(baseurl + "<?php echo APP_DIR; ?>/mall/userCancelTx", {
                        "evrid": evrid,
                        "tx_status": "-1"
                    },
                    function(data) {
                        json = $.parseJSON(data);
                        // alert(decodeURIComponent(json.retMsg));
                        if (json.retCode == '1') {
                            $(this).stopTime('losttime2');
//                            location.href = baseurl + "<?php echo APP_DIR; ?>/history/bonus/?<?php echo date("YmdHis");?>&tab=2";
                            location.href = baseurl + "<?php echo APP_DIR; ?>/mall/?<?php echo date("YmdHis");?>";
                        }
                    }
                );
            });

            $('#no').click(function() {
                $.post(baseurl + "<?php echo APP_DIR; ?>/mall/userCancelTx", {
                        "evrid": evrid,
                        "tx_status": "-1"
                    },
                    function(data) {
                        json = $.parseJSON(data);
                        // alert(decodeURIComponent(json.retMsg));
                        if (json.retCode == '1') {
                            $(this).stopTime('losttime2');
//                            location.href = baseurl + "<?php echo APP_DIR; ?>/history/bonus/?<?php echo date("YmdHis");?>&tab=2";
                            location.href = baseurl + "<?php echo APP_DIR; ?>/mall/?<?php echo date("YmdHis");?>";
                        }
                    }
                );
            });

        });


        $(function(){
            //生成明暗碼切換按鈕
            var $parent = $('expw-box'),
                $inpt = $('input[type^="password"]');
            $inpt.each(function(){
                var $that = $(this);
                $that.parent($parent)
                    .append(
                        $('<div/>')
                        .addClass('password-btn d-flex align-items-center')
                        .append(
                            $('<i/>')
                            .addClass('esfas graycolor eye-slash')
                        )
                    );

                var $thisEyeBtn = $that.parents($parent).find('.password-btn');
                $thisEyeBtn.removeClass('d-flex').hide();

                //明暗碼切換
                function toggleBtn(who,$thisBtn){
                    if(who.val()==''){
                        $thisEyeBtn.removeClass('d-flex').hide();
                    }else{
                        $thisEyeBtn.addClass('d-flex').show();
                    }
                }
                //切到明碼
                function passwordEye() {
                    $thisEyeBtn.find("i")
                        .removeClass('eye-slash')
                        .addClass('eye');
                    $inpt.attr("type","text");
                    $thisEyeBtn.one("click",passwordEyeSlash);
                }
                //切到暗碼
                function passwordEyeSlash() {
                    $thisEyeBtn.find("i")
                        .removeClass('eye')
                        .addClass('eye-slash');
                    $inpt.attr("type","password");
                    $thisEyeBtn.one("click",passwordEye);
                }

                $that.on("keyup blur",function(){
                    toggleBtn($(this),$thisEyeBtn);
                })
                $thisEyeBtn.one("click",passwordEye);
            });

            //yes 按鈕
            $('#expw').on('keyup',function(){
                var $that = $(this);

                if($that.val() == ''){
                    $('#yes').attr('disabled',true);
                }else{
                    $('#yes').attr('disabled',false);
                }
            })
        })


        function sendSocket(message) {
            if (is_weixn() == false) {
                socket.send(message);
            }
        }

        function is_weixn() {
            var ua = navigator.userAgent.toLowerCase();
            if (ua.match(/MicroMessenger/i) == 'micromessenger') {
                return true;
            } else {
                return false;
            }
        }

        function onOpen(evt) {
            socket.send("ADD|" + evrid + "|U");
        }

        function onMessage(evt) {
            // message format evrid|status|message;
            var ret = evt.data;
            var arr = ret.split("|");
            var id = arr[0];
            var status = arr[1];
            if (id == evrid) {
                if (parseInt(status) < 0) {
                    // 交易取消
                    // $(this).stopTime('losttime2');
                    window.alert("商家取消交易 !!");
                    location.href = baseurl + "<?php echo APP_DIR; ?>/member/";
                } else if (parseInt(status) == 0) {
                    window.alert("逾時取消交易 !! !!");
                    location.href = baseurl + "<?php echo APP_DIR; ?>/member/";
                }
            }
        }

        function onClose(evt) {

        }

        function onError(evt) {

        }
    </script>
