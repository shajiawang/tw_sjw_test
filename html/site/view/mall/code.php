<?php
$cdnTime = date("YmdHis");
$data=_v('data');
?>
<p>
<!--  CDN Bootstrap 4.1.1  -->
<!--    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>-->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/keyboard.min.css">

<style>
    .es-content {
        background: #FFF;
    }
</style>
<form method="post" action="" id="contactform" name="contactform">
    <div class="login-title login-captcha-title">
	    <h1>輸入兌換密碼</h1>
	</div>
    <div class="captcha-box captcha_keyboard">
        <ul class="num-box d-flex justify-content-center">
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
        </ul>
        <input type="hidden" class="input-field numHideBox" id="captcha" name="captcha" value="">
		<input type="hidden" name="epcid" id="epcid" value="<?php echo $data["epcid"]; ?>">
		<input type="hidden" name="epid" id="epid" value="<?php echo $data['epid']; ?>">
		<input type="hidden" name="esid" id="esid" value="<?php echo $data["esid"] ;?>">	
		<input type="hidden" name="total" id="total" value="<?php echo $data["total"] ;?>" >					
		<input type="hidden" name="bonus" id="bonus" value="<?php echo $data['bonus']; ?>" >
		<input type="hidden" name="all_bonus" id="all_bonus" value="<?php echo $data['all_bonus']; ?>" >	
		<input type="hidden" name="handling" id="handling" value="<?php echo $data['handling']; ?>">
		<input type="hidden" name="num" id="num" value="<?php echo $data["num"] ;?>" >
		<input type="hidden" name="amount" id="amount" value="<?php echo $data["amount"] ;?>" >
		<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
		<input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
		<input type="hidden" name="name" id="name" value="<?php echo $data["name"] ;?>" >
		<input type="hidden" name="zip" id="zip" value="<?php echo $data["zip"] ;?>" >
		<input type="hidden" name="address" id="address" value="<?php echo $data["address"] ;?>" >		
    </div>
    
    <!--  驗證訊息  -->
    <ul class="captcha-msgError"></ul>
    
    <div class="forget-box">
        <a class="forget-text" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/ufpwd/'">忘記兌換密碼</a>
    </div>
    
    <!--  彈窗-驗證訊息  -->
    <div class="errorModal">
        <div class="modal-content">
            <div class="modal-body">
                <ul></ul>
            </div>
            <div class="modal-footer">
                <a class="close_btn">確認</a>
            </div>
        </div>
    </div>
    <div class="errorModalBg"></div>
</form>	
<!-- ============================= JS 控制 ============================= -->


<!--  鍵盤-自訂義JS  -->
    <script src="<?php echo BASE_URL.APP_DIR;?>/static/js/_captcha_keyboard.js"></script>
<!--  小鍵盤動作  -->
<script>
    var $wherekey,
        $open,
        $thisBtn,
        $much = $(".captcha_keyboard li").length;
    $(".captcha_keyboard").each(function(i,e){
        $(this)
            .attr("data-toggle","modal")
            .attr("data-target","#captcha_numkeyModal");
        $(this).on("click",function(e){
            //生成按鈕(畫面沒有小鍵盤才生成)
            if(!$("#captcha_numkeyModal").length > 0){
                keyModal();
            }else{
                $('#captcha_numkeyModal').remove();
            }
                var $domEle=$("#passport");

                //先儲存點選哪個輸入框
                $wherekey = $(".numHideBox");
                $open = $(this).data("target");

                e.stopPropagation;
                e.preventDefault;

                //將原先輸入框裡的值帶入鍵盤的暫存區
                if ($wherekey.val()!=''){
                    $domEle.val(String($wherekey.val()));
                }else{
                    $domEle.val('');
                }
                //控制輸入框位置
                //controlSize($(this));

                //計算
                _captcha_Calculation($wherekey,$much);

                //Event-關閉小鍵盤後的動作
                $('#captcha_numkeyModal').on('hide.bs.modal', function (e) {
                    $('#captcha_numkeyModal').remove();
                    $(".num-box li").find(".pic").remove();

                    //removeBlank();

                    //驗證
                    var $tabNum = String($wherekey.val()).length,
                        $error = $(".captcha-msgError"),
                        $errorBox = $(".errorModal .modal-body ul");

                    array = [];

                    //判斷是否填滿所有格數
                    if($tabNum == $much){
                        if(array.length !== 0){
                            $error.html(array.join(''));
                            $errorBox.html(array.join(''));
                            e.preventDefault;
                            openErrorModal();
                        }else{
                            $error.html('');
                            var osmscode = $('#captcha').val();
                            $.ajax({
                                url: "<?php echo BASE_URL.APP_DIR;?>/ajax/mall.php",
                                data: {phone:"<?php echo $phone;?>",type:"checkexpwd", expw:osmscode},
                                type:"POST",
                                dataType:'JSON',

                                success: function(msg){
                                   var obj ;
                                   // var obj = $.parseJSON(msg);
                                   if((typeof msg)=="string") {
                                      obj = JSON.parse(trim(msg));
                                   } else if((typeof msg)=="object") {
                                      obj = msg;
                                   }
                                    console.log("retCode : "+obj.retCode);
                                    if(obj.retCode == 1) {
                                        msg = '兌換密碼確認成功 !!'; 
                                    } else if(obj.retCode==-6){ 
                                        msg = '兌換密碼錯誤，請重新輸入'; 
                                    } 
                                    setError(msg);
                                    $error.css("display","block").html(array.join(''));
                                    if(obj.retCode == 1) {
                                        mall_checkout();
                                    }
                                },
                                error:function(){
                                    var obj ;
                                    // var obj = $.parseJSON(msg);
                                    if((typeof msg)=="string") {
                                       obj = JSON.parse(trim(msg));
                                    } else if((typeof msg)=="object") {
                                       obj = msg;
                                    }
                                    setError("兌換密碼確認失敗");
                                    $error.html(array.join(''));
                                    $errorBox.html(array.join(''));
                                    openErrorModal();
                                }
                            });
                        }
                    }else{
                        setError("請填寫完整兌換密碼");
                        $error.html(array.join(''));
                    }
                })
            
        });
    })

    
    //防止小鍵盤沒吊起，每600毫秒判斷未吊起時重新點擊開啟
    var interval = null;
    $(function(){
        //$(".captcha_keyboard").click();
        interval = setInterval(Continuous,600);
    })
    //持續判斷小鍵盤
    function Continuous() {
        var $modal = $("#captcha_numkeyModal");
        if($modal.hasClass("show")){
            //已吊起，要移除持續判斷
            clearInterval(interval);
        }else{
            $(".captcha_keyboard").click();
        }
    }

    
    var array;
    function setError(msg){
        array.push('<li>'+msg+'</li>');
    };
</script>
