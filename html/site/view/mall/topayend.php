<?php

$profile = _v('enterpriseprofile');
$userprofile = _v('userprofile');
$order = _v('order');
$esid = _v('esid');
$json = _v('json');
$userid = _v('userid');

if (!empty($profile['thumbnail_file'])) { 
	$pic = 'http://www.shajiawang.com/management/images/headimgs/'.$profile['thumbnail_file']; 
}else{
	$pic = $profile['thumbnail_url'];
} 

?>
<div class="article">
	<input type="hidden" name="json" id="json" value="<?php echo $json; ?>">
	<input type="hidden" name="esid" id="esid" value="<?php echo $esid; ?>">
	<input type="hidden" name="userid" id="userid" value="<?php echo $userid ;?>">
	

	<div style="text-align:center;">
		<img src="<?php echo $pic; ?>" style="width:100%;max-height:250px;">
		<h3 style="white-space:pre-wrap;text-align:center;"><?php echo $profile['companyname']; ?></h3>
	</div>					
	<div class="weui-cells2" >
	  <div class="weui-cell" >
		<div class="weui-cell__bd">
			<div width="100%">
				<div style="width:30%;height:100%;float:left;"><h5>兌換鯊魚點數：</h5></div>
				<div style="width:70%;float:left;">
					<?php echo sprintf("%.2f",$order['total_fee']); ?> 點
				</div>
			</div>	
		</div>
	  </div>
	</div>
	<div class="weui-cells2" >
	  <div class="weui-cell" >
		<div class="weui-cell__bd">
			<div width="100%">
				<div style="width:30%;height:100%;float:left;"><h5>兌換時間：</h5></div>
				<div style="width:70%;float:left;">
					<?php echo $order['insertt']; ?>
				</div>
			</div>	
		</div>
	  </div>
	</div>	

	<div class="nav">
		<div class="weui-tabbar" style="background-color:#FFFFFF">
			<div style="width:1%;"> </div>
			<div style="width:98%; text-align:center;" >
				<button type="button" onClick="javascript:location.href='http://www.shajiawang.com/site/'" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0px 0 ">確定完成</button>
			</div>	
			<div style="width:1%;"> </div>
		</div>			
	</div>				
</div><!-- /article -->
