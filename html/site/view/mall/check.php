<?php
$status = _v('status');
$product = _v('product');
$product_category = _v('product_category');
$stock = _v('stock');
$allbonus = _v('bonus');
?>
<?php 
	if(!empty($product['thumbnail_file'])) { //$status['path_image'] 
		$img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail_file'];
	} elseif(!empty($product['thumbnail'])) { //$status['path_image'] 
        $img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail'];
    } elseif (!empty($product['thumbnail_url'])) {
        $img_src = $product['thumbnail_url']; 
    } 
?>

<form method="post" action="<?php echo BASE_URL.APP_DIR;?>/mall/exchangeCode/?epcid=<?php echo $_GET["epcid"]; ?>&epid=<?php echo $product['epid']; ?>" id="contactform" name="contactform" >

    <div class="mallCheck">
        <div class="mallCheck-box">
            <div class="mallCheck-imgbox d-flex align-items-center justify-content-center">
                <div class="imgbox-img d-flex align-items-center">
                    <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>" alt="">
                </div>
                <div class="imgbox-info d-inflex">
                    <div class="mallCheck-price-box d-flex align-items-center justify-content-center">
                        <p class="mallCheck-name mr-auto"><?php echo $product['name']; ?></p>
                    </div>
                </div>
            </div>

            <div class="mallCheck-info">
                <div class="payinfo-item d-flex align-items-center">
                    <div class="pay-title mr-auto">兌換數量</div>
                    <div class="pay-rightTxt">
                        <div class="num"><span id="linum"></span> 個</div>
                    </div>
                </div>
                <div class="payinfo-item d-flex align-items-center">
                    <div class="pay-title mr-auto">所需鯊魚點</div>
                    <div class="pay-rightTxt"><span id="litotal"></span> 點</div>
                </div>
                <div class="payinfo-item d-flex align-items-center">
                    <div class="pay-title mr-auto">處理費</div>
                    <div class="pay-rightTxt">
                        <span><?php echo sprintf("%d", $product['process_fee']); ?></span>
                    </div>
                </div>
            </div>

            <div class="mallCheck-paytotal d-flex align-items-center">
                <div class="pay-title mr-auto">總計</div>
                <div class="pay-rightTxt red"><span id="malltotal"></span> 點</div>
            </div>
        </div>
        <div class="mallCheck-select">
            <!--
             <div class="pay-item d-flex align-items-center disabled">
                <div class="all-radio">
                    <input name="radio" id="radio1" value="spoint" type="radio">
                    <label for="radio2" class="radio-label"> </label>
                </div>
                <div class="radio-title-box d-flex align-items-center">
                    <div class="radio-title mr-auto">
                        <div>免費兌換卷</div>
                    </div>
                    <div class="item-pint"><div class="red">- <span id="litotal"></span></div></div>
                </div>
            </div>      
            -->
            <div class="pay-item d-flex align-items-center">
                <div class="all-radio">
                    <input name="radio" id="radio2" value="spoint" type="radio" checked>
                    <label for="radio2" class="radio-label"> </label>
                </div>
                <div class="radio-title-box d-flex align-items-center">
                    <div class="radio-title mr-auto">
                        <div>鯊魚點數</div>
                        <div class="small">( 帳户餘額：<?php echo sprintf("%d", $allbonus['amount']); ?> )</div>
                    </div>
                    <div class="item-pint"><div class="red">- <span id="malltotal"></span></div> 點</div>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" name="epcid" id="epcid" value="<?php echo $_GET["epcid"]; ?>">
    <input type="hidden" name="epid" id="epid" value="<?php echo $product['epid']; ?>">
    <input type="hidden" name="esid" id="esid" value="<?php echo $product["esid"] ;?>">	
    <input type="hidden" name="total" id="total" value="" >					
    <input type="hidden" name="bonus" id="bonus" value="<?php echo sprintf("%d", $product['point_price']); ?>" >
    <input type="hidden" name="all_bonus" id="all_bonus" value="<?php echo sprintf("%d", $allbonus['amount']); ?>" >	
    <input type="hidden" name="handling" id="handling" value="<?php echo sprintf("%d", $product['process_fee']); ?>">
    <input type="hidden" name="eimoney" id="eimoney" value="<?php echo sprintf("%d", $product['eimoney']); ?>" >
    <input type="hidden" name="num" id="num" value="" >
    <input type="hidden" name="amount" id="amount" value="" >
    <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
    <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
    <input type="hidden" name="name" id="name" value="" >
    <input type="hidden" name="zip" id="zip" value="" >
    <input type="hidden" name="address" id="address" value="" >

    <div class="footernav">
        <div class="footernav-button">
            <button type="submit" id="topay" class="ui-btn ui-btn-a ui-corner-all" >確認兌換</button>
            <!--button type="submit" id="topay" class="ui-btn ui-btn-a ui-corner-all" onClick="mall_checkout();">確認兌換</button-->
        </div>
    </div>
</form>	
