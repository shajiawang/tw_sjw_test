<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') >0 ) {
	$browser = 1;
}else{
	$browser = 2;
}
$params = _v('params');
?>
<div class="login-container">

<!-- 舊登入頁 -->
<!--
	<div class="login-logo">
		<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/login-logo-2.png" alt="殺價王">
	</div>
	<div class="login-content">
		<div class="login-account d-flex align-items-center">
			<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/account.png" alt="帳號">
			<input type="number" name="lphone" id="lphone" class="login-input"  pattern="[0-9]*"  focus/>
		</div>
		<div class="login-password d-flex align-items-center">
			<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/password.png" alt="密碼">
			<input type="password" name="lpw" id="lpw" class="login-input" focus/>
		</div>
		<div class="login-btn-login" onClick="sajalogin()">殺價登入</div>
		<p class="login-forgotpassword" onClick="javascript:location.href='<?php echo APP_DIR; ?>/user/forget/?<?php echo $cdnTime; ?>'">忘記密碼?</p>
	</div>
	<div class="login-other" style="display:none">
		<p class="login-login">或由以下方式註冊/登入</p>
	</div>
	<div class="login-kind" >
		<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/facebook.png" alt="fb">
		<img id="line_login" name="line_login_icon" onclick="line_oauth();" src="<?php echo BASE_URL.APP_DIR;?>/static/img/line.png" alt="line login" style="display:none" >
    </div>			
	<div class="login-footer">
		<p><span class="login-footer-txt">還不是會員嗎?</span><a href="javascript:location.href='<?php echo APP_DIR; ?>/user/register/?<?php echo $cdnTime; ?>'">按此註冊</a></p>
	</div>
-->
	
	
	<div class="login-title">
	    <h1>歡迎登入殺價王</h1>
	</div>
	<ul class="es-login-kind">
	    <li id="line_login" name="line_login_icon" onclick="line_oauth();" class="item item-line">
	        <div class="icon"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/login-icon/Line.png" alt=""></div>
	        <div class="title">使用 Line 登入</div>
        </li>
	    <li id="facebook_login" name="facebook_login_icon" onclick="fb_oauth();" class="item item-facebook" >
	        <div class="icon"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/login-icon/FB.png" alt=""></div>
	        <div class="title">使用 Facebook 登入</div>
	    </li>
	    <li id="twitter_login" name="twitter_login_icon" class="item item-twitter" style="display:none;">
	        <div class="icon"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/login-icon/twitter.png" alt=""></div>
	        <div class="title">使用 Twitter 登入</div>
	    </li>
    </ul>
    
    
</div>
<script>
    window.fbAsyncInit = function() {
    FB.init({
      appId      : '202083143845729',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

   $(document).ready(function(){
      if(is_kingkr_obj()) {
          $('#login-other').show(); 
          $('#line_login').show();
          $('#fb_login').show();
      } 
       
   });
   function line_oauth() {
         if(is_kingkr_obj()) {
             login('LINE','','lineLoginResult');
         } else {
              // alert("瀏覽器中無法啟用Line登入 !!");
			  // window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/use_line_login/';
         <?php
                 if($params && count($params)>0) {
                    $queryString = http_build_query($params);
         ?>
                   window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/use_line_login/?<?php echo $queryString; ?>';
         <?php  }  else { ?>
                   window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/use_line_login/';
         <?php  }  ?>
              return; 
         }         
   }
   
    function fb_oauth() {
         
         if(is_kingkr_obj()) {
             login('FB','','fbLoginResult');
         } else {
              checkFBLoginState();
              return; 
         }         
   }
   
   function lineLoginResult(r) {
         // alert(r); 
         // alert((typeof r));         
         if((typeof r)=="string") {
             var obj=JSON.parse(r);
             if(obj.retcode=="1") {
                var oauthData = {
                                 json:"Y",
                                 type:"sso",
                                 nickname:obj.displayName,
                                 sso_uid:obj.userId,
                                 headimgurl:obj.pictureUrl,
                                 sso_name:"line",
                                 gender:"male",
                                 sso_data:r};
                 $.post(
                    '<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                    oauthData,
                    function(r2) {
                          if((typeof r2)=="string") { 
                             // alert(r2);
                             var ret=JSON.parse(r2);
                             if(ret.retCode==-16) {
                                  // 帳號已註冊 => 跳到會員中心頁 
                                  window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";                              
                             } else if (ret.retCode==1) {
                                  // alert("ToDo : 新帳號登入程序 !!");
                                  // alert("註冊完成 !!");
                                  window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";       
                             }
                          }                          
                    }
                );
             }    
         } 
         
   }
   
   function fbLoginResult(r) {
         var obj;
         if((typeof r)=="string") {
             alert(r);
             obj=JSON.parse(r);
         } else if((typeof r)=="object") {
             obj=r;
             alert(JSON.stringify(obj));
         }
         // alert("000000");
         if(obj.retcode=="1") {
            // alert("11111");
            var oauthData = {
                     json:"Y",
                     type:"sso",
                     nickname:obj.name,
                     sso_uid:obj.id,
                     headimgurl:obj.picture.url,
                     sso_name:"fb"                    
                };
             // sso_data:JSON.stringify(obj)
             // alert("22222");
             $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                oauthData,
                function(r2) {
                     // alert("33333");
                     var ret;
                      if((typeof r2)=="string") {g
                          ret=JSON.parse(r2);
                      } else if((typeof r2)=="object") {
                          // alert("55555");
                          ret=r2;
                      }                      
                      // alert(r2);
                      if(ret.retCode==-16) {
                          // 帳號已註冊 => 跳到會員中心頁 
                          window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";                              
                      } else if (ret.retCode==1) {
                          // alert("ToDo : 新帳號登入程序 !!");
                          // alert("註冊完成 !!");
                          window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";       
                      }                         
                }
            );
         }    
   }


 <!-- Facebook JS SDK -->
  
   
   function checkFBLoginState() {
      // console.log("rrr");
      FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
      });
   }
   
   function statusChangeCallback(response) {
       FB.login(function(response) {
          if (response.status==='connected') {
                // Logged into your app and Facebook.
                FB.api('/me','GET',{"fields" : "id,name,gender,email,picture"}, 
                    function(response) {
                        var oauthData = {
                                 json:"Y",
                                 type:"sso",
                                 nickname:response.name,
                                 sso_uid:response.id,
                                 headimgurl:response.picture.url,
                                 sso_name:"fb",
                                 gender:response.gender,
                                 sso_data:JSON.stringify(response)
                        };
                        $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                            oauthData,
                            function(r2) {
                                var ret;
                                if((typeof r2)=="string") { 
                                     ret = JSON.parse(r2);
                                } else if((typeof r2)=="object") {
                                     ret = r2;
                                }
                                if(ret.retCode==-16) {
                                    // 帳號已註冊 => 跳到會員中心頁 
                                    window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";                              
                                } else if (ret.retCode==1) {
                                    // alert("ToDo : 新帳號登入程序 !!");
                                    // alert("註冊完成 !!");
                                    window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";       
                                }                       
                            }
                        );
                    }
                );
          } else {
            // The person is not logged into this app or we are unable to tell. 
          }
       });
       return;
   }
 
</script>