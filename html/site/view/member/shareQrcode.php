<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$member = _v('member');
$ret = _v('ret');

?>
<div class="codeBox d-flex align-items-center">
    <div class="w-100 text-center">
        <div id="qrcodeBox">
            <img src='<?php echo APP_DIR; ?>/phpqrcode/?data=<?php echo $member['weixin_url'];?>' >
        </div>
        <div class="member-share-qrcode">
			<?php /*<p style="font-size:14px;">ID : <?php echo $member['userid'];?></p>*/?>
			<p><?php echo empty($ret["usetid"])?'':$ret["usetid"];?></p>
			</br>
            <?php /*</br>
			<p>邀請好友加入</p>
            <p>換取免費好康</p>
			<div class="copy" data-clipboard-text="<?php echo $member['share_url'];?>" style="width: 70%;display: inline-block;background-color: #F9A823;;border-radius: 6px;font-size: 1.1rem;padding: 5px 3px;box-sizing: border-box;color: #FFFFFF;text-align: center;margin: 10px auto 0px;" >分享連結</div>
			*/
			?>
			</br></br>
			<div style="text-align:center; padding: 2.5rem 0.5rem 5rem 4.2rem; line-height: 2.5rem; " >
				<div style="text-align:left; font-weight: normal;"><?php echo empty($ret["note"])?'':$ret["note"];?></div>
			</div>
		</div>
		
    </div>
</div>

<!-- /article -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/js/clipboard.min.js"></script>

<script>
    //背景底色改白色
    $(function(){
        $(".codeBox")
            .parents(".es-content")
            .css("background","#FFF");
    })

	/* 複製內容功能設定 */
	var clipboard = new ClipboardJS('.copy');

	//複製成功執行
	clipboard.on('success', function(e) {
		e.clearSelection();
		// alert("複製成功 "+e.text+"」");
		alert("網址複製成功！\n趕快貼上社群，分享給好友，換取免費殺價券~");
	});

	//複製失敗執行
	clipboard.on('error', function(e) {
		alert("複製失敗");
	});

</script>
