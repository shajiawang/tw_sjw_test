<?php 
$spoint = _v('spoint');
$userid = _v('userid');
?>
<form action="<?php echo BASE_URL.APP_DIR;?>/member/spoint_confirm/" method="POST" id="form_spoint"> 
    <div class="userEdit">
        
        <div style="margin:4vmin;background-color: white;">
            <ul class="input_group_box">
                <li class="input_group align-items-center">
                    <div class="label" for="name" style="width: 161px;line-height:30px">受贈會員編號</div>
                    <input name="receiver" id="receiver" value="" type="text" style="width: 100%;background-color:#F9F9F9;margin-right:0.667rem">
                </li>
                <li class="input_group align-items-center">
                    <div class="label" for="zip" style="width: 161px;">贈送數量</div>
                    <hr>
                </li>
                <li class="input_group d-flex align-items-center">
                    <div class="label" for="zip" style="width: 60%">殺價幣<br><span class="precautions">餘額:<?php echo number_format($spoint);?>元</span></div>
                    <div class="input_group d-flex align-items-center" style="width:
                    40% ;justify-content:flex-start"><input name="qty" id="qty" value="" type="number" min=1 max="<?php echo $spoint;?>" style="width:30%;background-color:#F9F9F9;">                <div class="label" for="name">元  </div>
                    </div>
                </li>
            </ul>
            <input type="hidden" name="auth_id" value="<?php echo $userid;?>">
            <input type="hidden" name="json" value="Y">
            <input type="hidden" name="lang" value="zh_tw">
            <input type="hidden" name="ts" value="<?php echo date('Ymd'); ?>">
            <input type="hidden" name="tk" value="<?php echo md5($userid."|".date('Ymd')); ?>">
            <div class="footernav">
                <div class="footernav-button">
                    <button type="button" id="address_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="spoint_confirm();">確認</button>
                </div>
            </div>					
        </div>
    </div>
</form>
	

	
	<script>
        //調整標題寬度一致
        var options = {
            dom: {
                ul: 'ul.input_group_box',
                li: 'li.input_group',
                label: '.label',
                input: 'input',
                select: 'select'
            }
        }

        function spoint_confirm(){
            if (!($('#receiver').val()>0)){
                alert("請輸入受贈者編號");
            }else if($('#qty').val()<1){
                alert("請輸入數量");
            }else{
                $('#form_spoint').submit();
            }
        }
        $(function(){
            //目前顯示的ul區塊
            var $isShow = $('.userEdit > div >'+ options.dom.ul).has(':visible');
            var $cap = $("div[id^='capo']").find($(options.dom.ul).has(':visible'));
            
            //取得應該顯示的寬度
            function changeWidth(dom) {
                var $selfLi = dom.find($(options.dom.li));
                var $selfLabel = $selfLi.find($(options.dom.label));
                
                //初始化
                $selfLabel.attr('style','');
                
                var $liW = '';
                var $arr = [];

                $liW = $selfLi.width();                         //取得li總寬度   

                $selfLabel.each(function(){                     //取得label寬度並存入陣列
                    $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
                });
                var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
                var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

                //改變寬度
                $selfLabel.each(function(){
                    $(this).css('width',$maxW);
                })
                
                $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                    var $selfInput = $(this).find($(options.dom.input));
                    var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                    $selfInput.attr('style','');
                    $selfSelect.attr('style','');
                    $selfInput.each(function(){
                        if($selfInput.length > 1){
                            $(this).width(($remain / $selfInput.length));
                        }else{
                            $(this).width($remain);
                        }
                    })
                    $selfSelect.each(function(){
                        if($selfSelect.length > 1){                 
                            $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                        }else{
                            $(this).width($remain);
                        }
                    })
                })
            }
            $isShow.each(function(){
                changeWidth($(this));
            })
            $cap.each(function(){
                changeWidth($(this));
                $(this).parents("div[id^='capo']").hide();
            })
        })
    </script>
