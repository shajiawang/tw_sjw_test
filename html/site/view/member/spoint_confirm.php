<?php 
$member = _v('member');
$userid = _v('userid');
$receiver = _v('receiver');
$qty = _v('qty');
?>
<div class="userEdit">
	<div style="margin:4vmin;background-color: white;">
        <ul class="input_group_box">
            <li class="input_group align-items-center">
                <div class="label" for="name" style="width: 161px;line-height:30px">受贈會員</div>
                <hr/>
            </li>
            <li class="input_group align-items-center">
                <div class="userinfo" align="center">
                    <div class="user-img">
                        <a href="#">
                        <?php if (!empty($member['thumbnail_file'])) { ?>
                            <img class="userinfo-avatar" src="<?php echo BASE_URL.HEADIMGS_DIR;?>/<?php echo $member['thumbnail_file'];?>?t=<?php echo $cdnTime;?>" background-size="cover">
                        <?php } elseif (!empty($member['thumbnail_url'])){ ?>
                            <img class="userinfo-avatar" src="<?php echo $member['thumbnail_url']; ?>" background-size="cover">
                        <?php } else { ?>
                            <img class="userinfo-avatar" src="<?php echo APP_DIR;?>/static/img/login-logo-2.png" background-size="cover">
                        <?php } ?>
                        </a>
                    </div>
                    <div class="user-name">
                        <div style="font-size: 5vmin;color:black;"><?php echo empty($member["profile"]['nickname'])?urldecode($member['nickname']):urldecode($member["profile"]['nickname']); ?></div>
                    </div>
                    <div class="user-name">
                        <div style="font-size: 5vmin;color:gray;">會員編號:<?php echo $member["userid"]; ?></div>
                    </div>                    
                </div>
            </li>
            <li class="input_group align-items-center">
                <div class="label" for="name" style="width: 161px;line-height:30px">贈送數量</div>
                <hr/>
            </li>
            <li class="input_group d-flex align-items-center">
                <div class="label" for="zip" style="width: 50%">殺價幣</div>
                <div class="label" style="width:50% ;text-align:right"><span style='color:red'><?php echo $qty?></span>元</div>
            </li>
            <li class="input_group align-items-center">
                <div class="label" for="name" style="width: 161px;line-height:30px">兌換密碼</div>
                <input name="expw" id="expw" value="" type="text" style="width: 100%;background-color:#F9F9F9;margin-right:0.667rem">
            </li>
        </ul>
		<div class="footernav">
            <div class="footernav-button">
                <button type="button" id="address_btn" class="ui-btn ui-btn-a ui-corner-all" onClick="profile_confirm();">確認修改</button>
            </div>
        </div>					
	</div>
</div>

	

	
	<script>
        //調整標題寬度一致
        var options = {
            dom: {
                ul: 'ul.input_group_box',
                li: 'li.input_group',
                label: '.label',
                input: 'input',
                select: 'select'
            }
        }
        $(function(){
            //目前顯示的ul區塊
            var $isShow = $('.userEdit > div >'+ options.dom.ul).has(':visible');
            var $cap = $("div[id^='capo']").find($(options.dom.ul).has(':visible'));
            
            //取得應該顯示的寬度
            function changeWidth(dom) {
                var $selfLi = dom.find($(options.dom.li));
                var $selfLabel = $selfLi.find($(options.dom.label));
                
                //初始化
                $selfLabel.attr('style','');
                
                var $liW = '';
                var $arr = [];

                $liW = $selfLi.width();                         //取得li總寬度   

                $selfLabel.each(function(){                     //取得label寬度並存入陣列
                    $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
                });
                var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
                var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

                //改變寬度
                $selfLabel.each(function(){
                    $(this).css('width',$maxW);
                })
                
                $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                    var $selfInput = $(this).find($(options.dom.input));
                    var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                    $selfInput.attr('style','');
                    $selfSelect.attr('style','');
                    $selfInput.each(function(){
                        if($selfInput.length > 1){
                            $(this).width(($remain / $selfInput.length));
                        }else{
                            $(this).width($remain);
                        }
                    })
                    $selfSelect.each(function(){
                        if($selfSelect.length > 1){                 
                            $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                        }else{
                            $(this).width($remain);
                        }
                    })
                })
            }
            $isShow.each(function(){
                changeWidth($(this));
            })
            $cap.each(function(){
                changeWidth($(this));
                $(this).parents("div[id^='capo']").hide();
            })
        })
        var k=0;
    function profile_confirm(){
        if (k==0){
            k=1;
            var formData = new FormData();
            formData.append('auth_id',<?php echo $userid; ?>);
            formData.append('qty',<?php echo $qty; ?>);
            formData.append('gift_type','spoint');
            formData.append('expw',$('#expw').val());
            formData.append('json','Y');
            formData.append('lang','zh_tw');
            formData.append('receiver',<?php echo $receiver; ?>);
            formData.append('ts','<?php echo date('Ymd'); ?>');
            formData.append('tk','<?php echo md5($userid."|".date('Ymd')); ?>');

            $.ajax({
                url : '<?php echo BASE_URL.APP_DIR;?>/member/transfer_gift/',
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data){
                    var info = JSON.parse(data);
                    alert(info.retMsg);
                    if (info.retCode==1) location.href='<?php echo BASE_URL.APP_DIR;?>/member/?<?php echo $cdnTime;?>';
                    k=0;
                }
            });
        }
        
    }        
    </script>
    


