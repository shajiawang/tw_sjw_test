<?php
$phone = empty($_GET['phone']) ? $_SESSION['user']['profile']['phone'] : $_GET['phone'];
$ep = empty($_GET['ep']) ? $_POST['ep'] : $_GET['ep'];
?>
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                    <label for="smscode">手機驗證</label>
                    <input name="smscode" id="smscode" value="" data-clear-btn="true" type="text" placeholder="請輸入驗證碼">
                    <input name="phone" id="phone" value="<?php echo $phone; ?>" type="hidden">
					<input name="ep" id="ep" value="<?php echo $ep; ?>" type="hidden">
					<input name="mid" id="mid" value="<?php echo strtolower(MD5($_SESSION['auth_id'])); ?>" type="hidden">
					<button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="check_sms()">確認</button>
                </li>
            </ul>
        </div><!-- /article -->
