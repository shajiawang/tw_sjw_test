<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list');
$status = _v('status'); 

?>	
    
<!-- 
    * 獲得類 ： 正常色 (不用其他設定)
    * 使用類 ： 在li.history-list 增加class "red"
    * "+"跟"-"符號，使用CSS增加，判斷條件為父層li是否有 "red",
      所以輸出price時，使用絕對值
    
    * 若無資料，整個"ul"都隱藏 切換顯示class="nohistory"
    * #tab1 ： 全部顯示
      #tab2 ： 處理中
      #tab3 ： 已出貨
 -->
<div id="tobid" class="swipe-tab">
    <div class="sajaProduct-select d-flex">
        <!-- 橫向menu -->
        <div class="horscroll">
            <div class="menu-wrapper d-flex flex-nowrap">

                <a class="menu-a on-active" href="#tab1">全部</a>
                <a class="menu-a" href="#tab2">處理中</a>
                <a class="menu-a" href="#tab3">完成</a>

                <div class="sideline"><!-- menu底線 --></div>
            </div>
        </div>
        <!-- 下拉menu -->
        <!--
        <div id="sajaProduct-select-btn" class="menu-btn"></div>
        -->
    </div>
    <div class="swipe-navbar-content member-order">
        <!-- ajax生成 -->
    </div>
</div>

<!--  彈窗-運送狀況訊息  -->
<div class="memoModal justify-content-center align-items-center">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-title">配送進度</div>
            <ul class="order-item-group">
                <li class="order-item active d-flex">
                    <div class="order-type d-flex justify-content-center align-items-center">
                        <div class="serial">1</div>
                        <div class="line"></div>
                    </div>
                    <div class="order-info-box">
                        <div class="order-title">訂單成立時間</div>
                        <div class="order-info"></div>
                    </div>
                </li>
                <li class="order-item active d-flex">
                    <div class="order-type d-flex justify-content-center align-items-center">
                        <div class="serial">2</div>
                        <div class="line"></div>
                    </div>
                    <div class="order-info-box">
                        <div class="order-title">付款時間</div>
                        <div class="order-info"></div>
                    </div>
                </li>
                <li class="order-item d-flex">
                    <div class="order-type d-flex justify-content-center align-items-center">
                        <div class="serial">3</div>
                        <div class="line"></div>
                    </div>
                    <div class="order-info-box">
                        <div class="order-title">出貨時間</div>
                        <div class="order-info"></div>
                    </div>
                </li>
                <li class="order-item d-flex">
                    <div class="order-type d-flex justify-content-center align-items-center">
                        <div class="serial">4</div>
                    </div>
                    <div class="order-info-box">
                        <div class="order-title">到貨時間</div>
                        <div class="order-info"></div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="modal-proinfo-close"><img class="img-fluid" src="/site/static/img/close-circle.png"></div>
    </div>
    <div class="memoModalBg"></div>
</div>

<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };
    
    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現  
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }
    
    $(function(){
        //有footerBar時，回頂按鈕 位置往上移動
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
    })
</script>

<!-- 觸底加載 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/es-bottom-scroll.js"></script>

<!-- 客製化加載 -->
<script>
    //各元件
    esOptions.nopicUrl = '<?php echo APP_DIR;?>/static/img/nohistory-img.png';         //無資料時顯示
    esOptions.loadingUrl = '<?php echo APP_DIR;?>/static/img/bottomLoading.gif';       //資料loading顯示
    esOptions.memopic = '<?php echo APP_DIR;?>/static/img/order-memo.svg';             //查看配送狀態按鈕
    
    //存入 json 開頭
    esOptions.jsonInfo.dir = '<?php echo BASE_URL.APP_DIR;?>';
    esOptions.jsonInfo.hpage = '/member/order/';                     //網頁page識別碼，抓取json用
    esOptions.isTab = 'true';                  		                 //是否為多頁tab
    esOptions.contentdom.ulClass = 'meorder-lists';                  //設置外框ul的class名稱
    createDom();                                                     //生成分頁容器
    //追加 tabItem : kind & jsonUrl & nopicText
    $.each(esOptions.tabItem,function(i,item){
        //添加特性 kind & jsonUrl
        switch(item['id']){
            case 'tab1' :
                item['kind'] = 'all';
                item['nopicText'] = '目前沒有任何紀錄';
                break;
            case 'tab2' :
                item['kind'] = 'process';
                item['nopicText'] = '目前沒有任何紀錄';
                break;
            case 'tab3' :
                item['kind'] = 'close';
                item['nopicText'] = '目前沒有任何紀錄';
                break;
        }
        item['jsonUrl'] = esOptions.jsonInfo.dir + esOptions.jsonInfo.hpage +'?json=Y&kind='+item['kind'];
    });
    
    //載入json
    esOptions.show = function($id) {
        var $ajaxUrl,$nowPage,$endPage,$total;
        var $wrapper = $id;
        var $statusNote,$type,$typeNote,$imgUrl;
        $.map(esOptions.tabItem,function(item, index){
            if(item['id'] === $id){
                $endPage = item['endpage'];
                $total = item['total'];
                if(item['page'] < $total){                          //與總頁數比對，若已達總頁數不再累加，避免重複加載
                    item['page']++;                                 //這次要加載的頁數
                }
                $nowPage = item['page'];
                $ajaxUrl = item['jsonUrl']+'&p='+$nowPage;      //json路徑
            }
        });
        if ($endPage == $nowPage){                                  //比對是否是這次要加載的頁數
            $.ajax({  
                url: $ajaxUrl,
                contentType: esOptions.jsonInfo.contentType,
                type: esOptions.jsonInfo.type,  
                dataType: esOptions.jsonInfo.dataType,
                async: esOptions.jsonInfo.async,
                cache: esOptions.jsonInfo.cache,
                timeout: esOptions.jsonInfo.timeout,
                processData: esOptions.jsonInfo.processData,
                contentType: esOptions.jsonInfo.contentType
            }).then(
                showResponse,                       //加載成功
                showFailure                         //加載失敗
            ).always(
                endLoading                          //加載後 不管成功失敗
            )
           
            function showResponse(data){       //生成div的function
                var $itemWrapper = $('#'+$wrapper);
                var $arr = data["retObj"]["data"];
                if(!($arr == null || $arr == 'undefined' || $arr == '')){
                    if($nowPage <= $total){
                        $.each($arr,function(i,item){
                            switch(item['status']){
                                case '0' :
                                    $statusNote = '處理中';
                                    break;
                                case '1' :
                                    $statusNote = '配送中';
                                    break;
                                case '2' :
                                    $statusNote = '退費中';
                                    break;
                                case '3' :
                                    $statusNote = '已發送';
                                    break;
								case '4' :
                                    $statusNote = '已到貨';
                                    break;
                                case '5' :
                                    $statusNote = '已退費';
                                    break;
                                default :
                                    $statusNote = '其他';
                            };
                            switch(item['type']){
                                case 'exchange' :
                                    $type = '處理中';
                                    $typeNote = '鯊魚點';
                                    break;
                                case 'saja' :
                                    $type = '已出貨';
                                    $typeNote = '殺價幣';
                                    break;
                                case 'user_qrcode_tx':
                                    $type = '鯊魚點支付';
                                    $typeNote = '鯊魚點';
                                    break;
                                default :
                                    $type = '其他';
                                    $typeNote = '';
                                    break;                                    
                            };
                            
                            //判斷顯示圖示
                            if(item['thumbnail'] !== '' && item['thumbnail'] !== null){
                                $imgUrl = '<?php echo BASE_URL.APP_DIR;?>/images/site/product/'+item['thumbnail'];
                            }else if(item['thumbnail_url'] !== ''){
                                $imgUrl = item['thumbnail_url'];
                            }else{
                                $imgUrl = '<?php echo APP_DIR;?>/static/img/order-default.png';
                            }
                            
                            //生成li
                            $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                            .append(
                                $('<li class="meorder-list"/>')
                                .append(
                                    $('<div class="meorder-header d-flex align-items-center"/>')
                                    .append(
                                        $('<div class="meorder-status d-flex align-items-center mr-auto"/>')
                                        .append(
                                            $('<div class="status-title d-flex align-items-center"/>')
                                            .append(
                                                $('<span>'+$statusNote+'</span>')
                                            )
                                        )
                                    )
                                    .append(
                                        $('<div class="bid-price d-flex align-items-center"/>')
                                        .append(
                                            $('<span>'+item['curr_type_desc']+'</span>')
											// $('<span>'+$typeNote + '使用點數：' + parseInt(item['total_fee']) + '點</span>')
                                        )
                                    )
                                )
                                .append(
                                    $('<div class="meorder-contant d-flex align-items-start"/>')
                                    .append(
                                        $('<div class="meorder-img"/>')
                                        .append(
                                            $('<i class="d-flex align-items-center"/>')
                                            .append(
                                                $('<img src="'+$imgUrl+'"/>')
                                            )
                                        )
                                    )
                                    .append(
                                        $('<div class="meorder-info align-self-center"/>')
                                        .append(
                                            $('<p class="meorder-name">'+item['name']+'</p>')
                                        )
                                        .append(
                                            $('<p class="meorder-ordid">訂單編號：'+item['orderid']+'</p>')
                                        )
                                        .append(
                                            $('<div class="meorder-footer d-flex"/>')
                                            .append(
                                                $('<div class="memo-time mr-auto"/>')
                                                .text(item['modifyt'])
                                            )
                                            .append(
                                                $('<a class="memo-btn'+((item['status']=='0' || item['status']=='1' || item['status']=='2')?' undone':'')+'"/>')
                                                .attr({'data-status':item['status'],'data-order':item['insertt'],'data-onbid':item['insertt'],'data-delivery':item['outtime'],'data-logisticsId':item['outcode'],'data-arrival':item['closetime']})
                                            )
                                        )
                                    )
                                )
                            )
                        })
                    }else{
                        return false;
                    }
                }else{
                    //不同頁簽顯示不同文字 (先前存進物件的資料)
                    var $nopicText = ''
                    var $nopicgroup = $.map(esOptions.tabItem, function(item, index){
                        if(item['id']===$wrapper){
                            $nopicText = item['nopicText'];
                        }
                    });
                    $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                    .after(
                        $('<div class="nohistory-box"/>')
                        .append(
                            $('<div class="nohistory-img"/>')
                            .append(
                                $('<img class="img-fluid" src="'+esOptions.nopicUrl+'"/>')
                            )
                        )
                        .append(
                            $('<div class="nohistory-txt">'+$nopicText+'</div>')
                        )
                    )
                }   
            } 
            //加載動作完成 (不論結果success或error)
            function endLoading() {
                $.map(esOptions.tabItem,function(item, index){
                    if(item['id'] === $id){
                        $totalPage = item['total'];                 //找到對應ID的總頁數
                        item['endpage']++;                          //預計下次要加載的頁數,無論如何載完都加1，事後比對用
                    }
                });
                var $loadingDom = $('#'+$id).find('.esloading');
                if($totalPage == 1){
                    $loadingDom.hide();
                }else if($nowPage == $totalPage){
                    $loadingDom.html('<span>沒有其他資料了</span>'); 
                }else if($nowPage < $totalPage){
                    $loadingDom.show();
                }else{
                    $loadingDom.hide();
                }
            }
            //加載錯誤訊息
            function showFailure(data) {
                console.log(data);
            }
        }
    };
    
    var $menuParent = $(esOptions.menudom.parent),                        //按鈕的顯示窗
        $wrapper = $(esOptions.menudom.wrapper),                          //包住所有按鈕的外框
        $allmenu = $menuParent.find(esOptions.menudom.menuItem);          //按鈕群
    
    var $showParent = $(esOptions.contentdom.parent),                    //翻頁物件的顯示窗
        $item = $(esOptions.contentdom.showItem);                         //翻頁物件群
    
    $(window).on('load', function(){
        //撈取各頁籤總頁數後，生成畫面
        $.when.apply($, esOptions.tabItem.map(function(item) {
            getTotalPages(item);
        })).then(function() {
            //開始生成
            esOptions.tabItem.map(function(item) {
                esOptions.show(item['id']);
            })
        });
        
        if(esOptions.tabItem.length > 1){                 //menu數量大於1，才開放滑動效果
            //向左滑
            $showParent.bind('swipeleft', function(e){
                menuGoNext();
                e.stopPropagation();
            });
            $showParent.bind('swipe taphold', function(e){
                e.stopPropagation();
            })
            //向右滑
            $showParent.bind('swiperight', function(e){
                menuGoPrevious();
                e.stopPropagation();
            });
        }

        $allmenu.bind('click', function() {
            var $that = $(this);
            menuGoNumber($that.index() + 1);
        });
        
        $('.meorder-lists').on('click', '.memo-btn', function(){
            var $that = $(this);
            var $status = $that.attr('data-status');              //訂單狀態
            var $orderTime = $that.attr('data-order');            //訂單成立時間
            var $onbidTime = $that.attr('data-onbid');            //付款時間
            var $deliveryTime = $that.attr('data-delivery');      //出貨時間
            var $logisticsId = $that.attr('data-logisticsId');    //物流單號
            var $arrivalTime = $that.attr('data-arrival');        //到貨時間
            
            $('body').addClass('modal-open');
            $('.memoModal').addClass('d-flex').show();
            $('.memoModalBg').show();

            var $itemArry = $('.memoModal').find('.order-item');
            
            //不顯示沒有資料的時間
            if($orderTime !=='' && $orderTime !== null && $orderTime !== '0000-00-00 00:00:00'){
                $itemArry.eq(0).find('.order-info')
                .text('')
                .append($('<p/>').text($orderTime));
            }

            if($onbidTime !=='' && $onbidTime !== null && $onbidTime !== '0000-00-00 00:00:00'){
                $itemArry.eq(1).find('.order-info')
                .text('')
                .append($('<p/>').text($onbidTime));
            }

            if($deliveryTime !=='' && $deliveryTime !== null && $deliveryTime !== '0000-00-00 00:00:00'){
                $itemArry.eq(2).find('.order-info')
                .text('')
                .append($('<p/>').text($deliveryTime))
                .append(
                    ($logisticsId !== '' && $logisticsId !== undefined && $logisticsId !== null)?                  //判斷物流單號是否為空，空則不顯示
                        '<p class="d-flex flex-wrap"><span class="d-inlineflex">物流單號：</span><span class="d-inlineflex">'+$logisticsId+'</span></p>'
                    :
                        ''
                );
            }

            if($arrivalTime !=='' && $arrivalTime !== null && $arrivalTime !== '0000-00-00 00:00:00'){
                $itemArry.eq(3).find('.order-info')
                .text('')
                .append($('<p/>').text($arrivalTime));
            }

            if($status == '3' || $status == '4' || $status == '5'){
                status('4');                        //4個燈
            }else if($status == '1'){
                status('3');                        //3個燈
            }else{
                status('2');                        //2個燈
            }
        })

        $('.modal-proinfo-close').on('click', function(){
            $('body').removeClass('modal-open');
            $('.memoModal').removeClass('d-flex').hide();
            $('.memoModalBg').hide();
        })
        $('.memoModalBg').on('click', function(){
            $('body').removeClass('modal-open');
            $('.memoModal').removeClass('d-flex').hide();
            $('.memoModalBg').hide();
        })
    });
    $(window).on('resize', function(){
        getPageHeight();
        menuLine();
    });
    
    //指定跳頁
    function menuGoNumber(number) {
        var $that = $menuParent.find(esOptions.menudom.menuItem + ':nth-child(' + number + ')'),                  //按鈕active
            $thatItem = $showParent.find(esOptions.contentdom.showItem + ':nth-child(' + number + ')'),           //對應的翻頁物件
            $menuLeft = 0,                                                                                      //按鈕定位
            $showLeft = 0;
        
        //翻頁物件
        if($thatItem.length > 0){
            //按鈕
            if($that.length > 0){
                $menuLeft = $that.position().left;
                var $maxW = $wrapper.width();               //總寬度
                var $minW = $menuParent.width();            //螢幕寬度
                if ($menuLeft > $minW / 2) {
                    if($menuLeft < $maxW - $minW / 2) {
                        $menuLeft = ($that.position().left - ($menuParent.width() * 0.5) + $that.width());
                        $menuParent.stop().animate({scrollLeft:$menuLeft},300)    //移動位置，讓按鈕置中
                    }else{
                        $menuParent.stop().animate({scrollLeft:$maxW},300)       //最尾段，如果點選的按鈕未超過螢幕一半時，保持在最後
                    }
                }else{
                    $menuParent.stop().animate({scrollLeft:0},300)               //最前段，如果點選的按鈕未超過螢幕一半時，保持在最前
                }
                $allmenu.removeClass(esOptions.class.active);
                $that.addClass(esOptions.class.active);
                menuLine();
            }
            //翻頁物件active切換
            $item.removeClass(esOptions.class.active);
            $thatItem.addClass(esOptions.class.active);
            
            return true;
        }
        return false;
    }
    
    //上一個(右滑)
    function menuGoPrevious() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        $('body,html').stop().animate({scrollTop:0},500);
        if ($that.index() > 0) {
            $that.prev().click();
            return true;
        }
        return false;
    }
    
    //下一個(左滑)
    function menuGoNext() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var menuLength = $(esOptions.menudom.wrapper).find(esOptions.menudom.menuItem).length;
        $('body,html').stop().animate({scrollTop:0},500);
        if ($that.index() < menuLength - 1) {
            $that.next().click();
            return true;
        }
        return false;
    }
    
    //menu底線變換
    function menuLine() {
        var $that = $(esOptions.menudom.line);
        var $actMenu = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var $actMenuW = $actMenu.innerWidth();
        $that.stop().animate({left:$actMenu.position().left},200).animate({width:$actMenuW},200);
    }
    
    //判斷配送進度流程增加active 判斷樣式
    function status(num) {
        var $ul = $('.order-item-group');
        var $lis = $ul.find('li.order-item');
        $lis.removeClass('active');                                   //active 先歸位
        for(var i=0; i<num; i++){
            $lis.eq(i).addClass('active');
        }
        isLineFull();                       //改變線段
    }
    
    //判斷彈窗線段垂直滿版與否
    function isLineFull() {
        var $activeItem = $('.memoModal').find('.order-item');
        $activeItem.each(function(){
            var $that = $(this);
            $that.find('.line').removeClass('full');                  //先歸零
            var $isFull = $that.next().hasClass('active');
            if($isFull){
                $that.find('.line').addClass('full');
            }
        });
    }
    
    //ajax用 取得總頁數
    function getTotalPages(item){
        var $getTotalPagesId = item['id'];
        var $ajaxUrl;
        $.map(esOptions.tabItem,function(item){
            if(item['id'] == $getTotalPagesId){
                $ajaxUrl = item['jsonUrl'];      //json路徑      
            }
        });
        return $.ajax({
            url: $ajaxUrl,
            contentType: esOptions.jsonInfo.contentType,
            type: esOptions.jsonInfo.type,  
            dataType: esOptions.jsonInfo.dataType,
            async: esOptions.jsonInfo.async === true,
            cache: esOptions.jsonInfo.cache === true,
            timeout: esOptions.jsonInfo.timeout,
            processData: esOptions.jsonInfo.processData,
            contentType: esOptions.jsonInfo.contentType
        }).then(
            function(data) {             //取得成功
                //將總頁數 寫入 tabItem
                $.map(esOptions.tabItem,function(item){
                    if(item['id'] == $getTotalPagesId){
                        item['total'] = data['retObj']['page']['totalpages'] || 1;
                    }
                });
            },
            function(data) {            //取得失敗
                console.log(data);
            }
        );
    };
</script>