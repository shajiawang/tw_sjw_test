<?php 
$userid = _v('userid');
$cdnTime = date("YmdHis");
?>	
<form>
        <div class="userEdit">
            <ul class="input_group_box">
                <li class="input_group d-flex align-items-center">
                    <div class="label" for="name">推薦人會員编號：</div>
                    <input name="intro_by" id="intro_by" value="" type="text">
                    <input name="userid" id="userid" value="<?php echo $userid;?>" type="hidden">                
                </li>
            </ul>            
            <div class="footernav">
                <div class="footernav-button">
                    <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="profile_confirm();">確認修改</button>
                </div>
            </div>	
        </div><!-- /article -->
</form>
<script>
    //調整標題寬度一致
    var options = {
        dom: {
            ul: 'ul.input_group_box',
            li: 'li.input_group',
            label: '.label',
            input: 'input',
            select: 'select'
        }
    }
    $(function(){
        //目前顯示的ul區塊
        var $isShow = $('.userEdit > div >'+ options.dom.ul).has(':visible');
        var $cap = $("div[id^='capo']").find($(options.dom.ul).has(':visible'));
        
        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));
            
            //初始化
            $selfLabel.attr('style','');
            
            var $liW = '';
            var $arr = [];

            $liW = $selfLi.width();                         //取得li總寬度   

            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
            
            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                $selfInput.attr('style','');
                $selfSelect.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                    }
                })
                $selfSelect.each(function(){
                    if($selfSelect.length > 1){                 
                        $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                    }else{
                        $(this).width($remain);
                    }
                })
            })
        }
        $isShow.each(function(){
            changeWidth($(this));
        })
        $cap.each(function(){
            changeWidth($(this));
            $(this).parents("div[id^='capo']").hide();
        })
    })
</script>
<script>
    var k=0;
    function profile_confirm(){
        if (k==0){
            k=1;
            var formData = new FormData();
            formData.append('auth_id',$('#userid').val());
            formData.append('intro_by',$('#intro_by').val());
            formData.append('json','Y');
            formData.append('lang','zh_tw');
            formData.append('type','giftAff');
            formData.append('ts','<?php echo date('Ymd'); ?>');
            formData.append('tk','<?php echo md5($userid."|".date('Ymd')); ?>');

            $.ajax({
                url : '<?php echo BASE_URL.APP_DIR;?>/ajax/user.php',
                type : 'POST',
                data : formData,
                processData: false,  // tell jQuery not to process the data
                contentType: false,  // tell jQuery not to set contentType
                success : function(data){
                    var info = JSON.parse(data);
                    alert(info.retMsg);
                    if (info.retCode==1) location.href='<?php echo BASE_URL.APP_DIR;?>/user/profile/?<?php echo $cdnTime;?>';
                    k=0;
                }
            });
        }
        
    }
    
</script>
		