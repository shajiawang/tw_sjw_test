<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$oscode = _v('oscode');
$scode = _v('scode');
?>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/croppie.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/exif.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/megapix-image.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/weui_loading.js"></script>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/croppie.css" rel="stylesheet"/>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/weui_loading.css" rel="stylesheet"/>

    <script>
		function openWindow() {
			if(is_kingkr_obj()) {
				awakeOtherBrowser('<?php echo BASE_URL.APP_DIR; ?>/deposit/?<?php echo $cdnTime; ?>');
			} else {
				location.href='<?php echo BASE_URL.APP_DIR; ?>/deposit/?<?php echo $cdnTime; ?>';
			}
		}
    </script>

<div id="pcd1">
<form method="post" action="" id="contactform4" name="contactform4">
	<div class="article" >
		<ul class="user-lists-box">
            <!-- 殺價券 -->
            <li class="user-list">
                <a href="javascript:void(0);" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/oscode/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-flex align-items-center">
                        <div class="list-icon">
                            <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/oscode.png">
                        </div>
                        <div class="list-title">殺價券</div>
                    </div>
                    <div class="list-rtxt d-inline-flex">
                        <div class="r-arrow">
                            <div class="rtxt-num"><?php echo $oscode; ?></div>
                            <div class="rtxt-unit"> 張</div>
                        </div>
                    </div>
                </a>
            </li>

            <!-- S碼 超級殺價券，屏蔽沒有看過超級殺價券的人 -->
            <li class="user-list">
                <a href="javascript:void(0);" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/scode/scode_list/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-flex align-items-center">
                        <div class="list-icon">
                            <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/scode.png">
                        </div>
                        <div class="list-title">超級殺價券</div>
                    </div>
                    <div class="list-rtxt d-inline-flex">
                        <div class="r-arrow">
                            <div class="rtxt-num"><?php echo $scode; ?></div>
                            <div class="rtxt-unit"> 張</div>
                        </div>
                    </div>
                </a>
            </li>
             <!-- 轉贈超級殺價券 -->
             <li class="user-list" id="item_deposit" >
                <a href="javascript:void(0);" class="user-list-link d-flex justify-content-between align-items-center" onClick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/member/scode_cho_receiver/?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-flex align-items-center">
                        <div class="list-icon">
                            <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/depositspoint.png">
                        </div>
                        <div class="list-title">轉贈超級殺價券</div>
                    </div>
                    <div class="list-rtxt d-inline-flex">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </li>   
            <!-- 開通殺價卷 -->
            <li class="user-list">
                <a href="javascript:void(0);" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/ajax/scode.php?<?php echo $cdnTime; ?>'">
                    <div class="list-titlebox d-flex align-items-center">
                        <div class="list-icon">
                            <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/member/bonus.png">
                        </div>
                        <div class="list-title">開通殺價卷</div>
                    </div>
                    <div class="list-rtxt d-inline-flex">
                        <div class="r-arrow">
                        </div>
                    </div>
                </a>
            </li>       
        <div style="visibility:hidden">
            <input name="logo" id="logo" class="uploader__input" type="file" accept="image/*" placeholder="" >
		    <input type="hidden" class="input-field" name="ThumbnailFile" id="ThumbnailFile" value="<?php echo BASE_URL.HEADIMGS_DIR;?>/<?php echo $member['thumbnail_file'];?>">
		    <input type="hidden" class="input-field" name="oldThumbnailFile" id="oldThumbnailFile" value="<?php echo BASE_URL.HEADIMGS_DIR;?>/<?php echo $member['thumbnail_file'];?>">
	    </div>
	</div>
</form>
</div>

<div id="actions" style="display:none;">
    <div class="pic-box">
        <div id="new_logo" style="text-align: center;"></div>
    </div>
    <canvas id="cvs" style="display:none"></canvas>

    <div class="btn-box">
        <div class="rotate-group mx-auto d-flex">
            <button onclick="rotate_left()">向左旋轉90度</button>
            <button onclick="rotate_right()">向右旋轉90度</button>
        </div>
        <div class="submit-group mx-auto">
            <button onclick="save_pic()">儲存</button>
            <button onclick="cancel_edit()">取消</button>
        </div>
    </div>
</div>
<div class="weui-cells" style="margin-top:0rem;">
    <?php if(empty($auth['verified']) || $auth['verified']!=='Y'){ ?>
    <!-- a class="weui-cell weui-cell_access" href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/tech/?<?php echo $cdnTime; ?>'" >
        <div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/iconfont-help.png" style="width:20px;"></div>
        <div class="weui-cell__bd">
          <h5><p style="font-weight:normal;color:#696969">手機驗證</p></h5>
        </div>
        <div class="weui-cell__ft"> </div>
    </a -->
    <?php } ?>


    <div data-role="popup" id="popupParis" data-overlay-theme="b" data-theme="b" data-corners="false">
        <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
        <!-- img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo $member['share_url'];?>" style="max-height:290px;" alt="PHP QRcode" -->
        <!-- img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo $member['share_url'];?>" style="max-height:290px;" alt="PHP QRcode" -->
        <img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo urlencode($share_url); ?>" style="max-height:290px;" alt="PHP QRcode">
    </div>
    <div data-role="popup" id="weixinpopupParis" data-overlay-theme="b" data-theme="b" data-corners="false">
        <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
        <!-- img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo urlencode($share_url); ?>" style="max-height:290px;" alt="WeiXin QRcode" -->
        <img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo urlencode($member['weixin_url']);?>" style="max-height:290px;" alt="WeiXin QRcode">
    </div>
    <div data-role="popup" id="weixinpopupParis2" data-overlay-theme="b" data-theme="b" data-corners="false">
        <a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
        <img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/images/site/sjw_mp_qrcode.jpg" style="max-height:290px;" alt="WeiXin QRcode2">
    </div>
    <div id="BG" style="position:fixed;top:0px;left:0px;z-index:2"></div>
    <!--<td id="qrcode_image" style="position:absolute;z-index:3;display:none;background-image:url('<?php echo BASE_URL . APP_DIR;?>/images/site/qr/weixin_share.png');"></td>-->
</div>

<script>

	var $device;        //android, ios, other
    (function whDevice() {
        //判斷使用者裝置是否支援觸控功能，判斷是否為行動版 (有觸控功能的筆電也判斷為行動版)
        function isMobile() {
            try{ document.createEvent("TouchEvent"); return true; }
            catch(e){ return false;}
        }

        //判斷使用者裝置為Android或iOS
        function isDevice() {
            var u = navigator.userAgent;
            var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android終端
            var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios終端
            if(isAndroid){
                return 'Android';
            }else if(isiOS){
                return 'iOS';
            }
        }

        if(isMobile()){
            var $mobile = isDevice();
            switch($mobile){
                case 'Android':
                    $device = 'android';
                    break;
                case 'iOS':
                    $device = 'ios';
                    break;
            }
        }else{
            $device = 'other';
        }
    }())

	$('#phone_verified').on('click',function(){
		alert();
    })

	// 不是在微信  也不是在appbsl的app裏  才顯示Navigator bar

	$(document).ready(function() {
		//console.log(navigator.userAgent);
        // 送審暫時移除 購買殺價幣選項
        if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
           // $('#item_deposit').hide();
           // $('#item_logout').hide();
        } else {
           // $('#item_deposit').show();
           // $('#item_logout').show();
        }
        /*
        if(is_kingkr_obj()) {
			$('#deposit1').show();
		}else {
			$('#deposit2').show();
		}
		*/
		var verifiedPhone = "<?php echo $auth['verified'];?>"
		if (verifiedPhone != 'Y'){
			// alert("尚未進行手機驗證!!");
			// location.href='<?php echo BASE_URL.APP_DIR; ?>/verified/register_phone/';
		}
	});

//绑定input change事件
$("#logo").unbind("change").on("change",function()  {
	lnv.pageloading();
    var file = document.querySelector('#logo').files[0];
    if(file){
        //验证图片文件类型
        if(file.type && !/image/i.test(file.type)){
            return false;
        }
        var reader = new FileReader();
        reader.onload = function(e){
            //readAsDataURL后执行onload，进入图片压缩逻辑
            //e.target.result得到的就是图片文件的base64 string
            render(file,e.target.result);
        };
        //以dataurl的形式读取图片文件
        reader.readAsDataURL(file);
    }
});

//定义照片的最大高度
var MAX_HEIGHT = 480;
var render = function(file,src){
	EXIF.getData(file,function(){
	    //获取照片本身的Orientation
		var orientation = EXIF.getTag(this, "Orientation");
		var image = new Image();
		image.onload = function(){
			var cvs = document.getElementById("cvs");
			var w = image.width;
			var h = image.height;
			//计算压缩后的图片长和宽
			if(h>MAX_HEIGHT){
				w *= MAX_HEIGHT/h;
				h = MAX_HEIGHT;
			}
			//使用MegaPixImage封装照片数据
			var mpImg = new MegaPixImage(file);
			//按照Orientation来写入图片数据，回调函数是上传图片到服务器
			mpImg.render(cvs, {maxWidth:w,maxHeight:h,orientation:orientation}, sendImg);
		};
		image.src = src;
	});
};

//上传图片到服务器
var sendImg = function(){
    var cvs = document.getElementById("cvs");
    //调用Canvas的toDataURL接口，得到的是照片文件的base64编码string
    var data = cvs.toDataURL("image/png");
    //base64 string过短显然就不是正常的图片数据了，过滤の。
    if(data.length<48){
    	console.log("data error.");
    	return;
    }

    var formData = new FormData();
    //formData.append('pic', reader.result );
    formData.append('pic', data );
    formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
    formData.append('pic_name','logotemp');
    formData.append('filedir','headimgs/temp/');

    $.ajax({
        url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
        type : 'POST',
        data : formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success : function(data){
            load_img();
        }
    });
};


function load_img(){
  destory_img();

  var el = document.getElementById('new_logo');
    vanilla = new Croppie(el, {
    viewport: { width: 196, height: 196 },
    boundary: { width: 300, height: 300 },
    showZoomer: true,
    enableOrientation: true
  });

  vanilla.bind({
      url: '<?php echo BASE_URL.APP_DIR;?>/images/headimgs/temp/_'+<?php echo $_SESSION['auth_id'];?>+'_logotemp.png?t=' + new Date().getTime(),
  });

  document.getElementById('pcd1').style.display = "none";
  document.getElementById('actions').style.display = "inline";
  lnv.destroyloading();
}

function destory_img(){
  document.getElementById('new_logo').innerHTML="";
}

function cancel_edit(){
  location.reload();
}

function page_reload(){
  location.reload();
}

function rotate_right(){
   vanilla.rotate(90);
}

function rotate_left(){
   vanilla.rotate(-90);
}

function save_pic(){
	var pic = vanilla.result('canvas','viewport');
	//console.log(pic);
	pic.then(function(value){
				//console.log(value);
				var formData = new FormData();
				formData.append('pic',value);
				formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
				formData.append('pic_name','logo');
				formData.append('filedir','headimgs/');

				$.ajax({
						url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
						type : 'POST',
						data : formData,
						dataType: 'text',
						processData: false,  // tell jQuery not to process the data
						contentType: false,  // tell jQuery not to set contentType
						success : function(msg){

								var obj = JSON.parse(msg);
								console.log('retcode: '+obj.retCode+' retmsg: '+obj.retMsg);
								if (obj.retCode >= 1) {
									var tf = document.getElementById('ThumbnailFile').value="_<?php echo $_SESSION['auth_id'];?>_logo.png";
									change_db_data();
								} else {
									alert('檔案上傳失敗!!');
								}

								page_reload();

						}
		});
	});
}

function change_db_data() {
		$.ajax({
				url: "<?php echo BASE_URL.APP_DIR;?>/user/edit_user_logo/?web=Y",
				data: $('#contactform4').serialize(),
				type:"POST",
				dataType: 'text',
				success: function(msg){
						var obj = JSON.parse(msg);
						console.log('retcode: '+obj.retCode+' retmsg: '+obj.retMsg);
						alert( obj.retMsg );
				},
				error:function(msg){
						var obj = JSON.parse(msg);
						console.log('retcode: '+obj.retCode+' retmsg: '+obj.retMsg);
						alert( obj.retMsg );
				}
		});
}

function goDownload(){

	if($device == 'android'){

	window.location.href="https://play.google.com/store/apps/details?id=tw.com.saja.userapp";
	// console.log('android')
	}else if($device == 'ios'){

	window.location.href="https://itunes.apple.com/tw/app/id1441402090";
	// console.log('ios')

	} else {

	/* 非手機裝置開啟QRCODE彈窗 */
	$('body').addClass('modal-open');
	$('body').append(
		$('<div class="downloadModalBox d-flex justify-content-center align-items-center"/>')
		.append(
		$('<div class="modal-content"/>')
			.append(
			$('<div class="modal-body"/>')
				.append(

				$('<div class="qrcodebox"><img style="max-width: 100%;" src="https://www.saja.com.tw/site/phpqrcode/?data=https://www.saja.com.tw/site/mall/qrpage"></div>')
				)
				.append(
					$('<div class="modal-title"/>')
					.append(
						$('<p>為了安全起見</p>')
					)
					.append(
						$('<p>本功能僅能在APP使用</p>')
					)
					.append(
						$('<p>請用手機掃描QRcode</p>')
					)
				)
			)
			.append(
				$('<div class="modal-footer d-flex justify-content-center align-items-center"/>')
				.append(
					$('<div class="footer-btn m-close">取消</div>')
				)
			)
		)
		.append(
			$('<div class="msgModalBg">')
		)
	)
	$('.m-close , .msgModalBg').on('click', function(){
		$('body').removeClass('modal-open');
		$('.downloadModalBox').remove();
	})
	}

}

</script>
