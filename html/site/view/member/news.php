<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list'); 
$status = _v('status'); 
?>	
		<?php //站内公告紀錄 ?>
		<div class="article">
            <ul data-role="listview" data-inset="true">
				<?php 
				if(!empty($row_list['table']['record']) ) {
				foreach($row_list['table']['record'] as $rk => $rv) {
					if ($_SESSION['auth_id'] == 181 || $rv['newsid'] <= 1) {
				?>
				<li>
				<a href="<?php echo APP_DIR; ?>/member/news_detail/?<?php echo $status['status']['args'];?>&newsid=<?php echo $rv['newsid']; ?>" data-transition="slide">
                	<h2><?php echo $rv['name'];?></h2>
                    <p><?php echo $rv['ontime']; ?></p>
                </a>
				</li>
				<?php } } } ?>
            </ul>
			
			<?php 
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content'); 
			
			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : ''; 
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
			?>
			<div>
				<div class="ui-grid-a">
                   <div class="ui-block-a <?php echo $arrow_l_able; ?>">
					<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
					</div>
                    <div class="ui-block-b <?php echo $arrow_r_able; ?>">
					<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
					</div>
                </div>
                <?php /*<form>
					<select name="select-native-1" id="change-page">
					<?php echo $page_content['change']; ?>
                    </select>
                </form>*/?>
            </div>
			<?php } ?>
        </div><!-- /article -->
<?php /*
 <div id="rows" style="width: 100%;">
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
			<a href="<?php echo $this->config->default_main; ?>/member/news_detail?<?php echo $this->tplVar['status']['args']."&newsid=".$rv['newsid']; ?>">
                <div id="content_news">
                    <div id="content_title_blue"><?php echo $rv['name'];?></div>
                    <div id="content_date"><?php echo $rv['ontime']; ?></div>
                </div>
            </a>
			<?php endforeach; ?>
			</div>
*/?>

		