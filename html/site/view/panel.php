<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') >0 ) {
    $browser = 1;
}else{
    $browser = 2;
}
?>
<div class="sajaPanel" data-role="panel" id="left-panel<?php get_panel_id(); ?>" data-display="overlay" data-position="right">
    <div class="panel_close" data-rel="close">
        <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/hamburger.png">
    </div>
    <div class="panel_lists">
        <div class="panelList">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/'" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                   <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-home.png">
                </div>
                <div class="panelList-label">
                    <span>回首頁</span>
                </div>
            </a>
        </div>
        <div class="panelList">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/bid/?channelid=<?php echo $_GET['channelid']; ?>&<?php echo $cdnTime; ?>'"  <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-bid.png">
                </div>
                <div class="panelList-label">
                    <span>最新得標</span>
                </div>
            </a>
        </div>
        <div class="panelList">
           <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/faq/?<?php echo $cdnTime; ?>'" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
               <div class="panelList-icon">
                   <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-faq.png">
                </div>
               <div class="panelList-label">
                   <span>新手教學</span>
               </div>
           </a>
       </div>
        <div class="panelList">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/tech/?<?php echo $cdnTime; ?>'"  <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-tech.png">
                </div>
                <div class="panelList-label">
                    <span>王者秘笈</span>
                </div>
            </a>
        </div>
       <div class="panelList">
           <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/member/?<?php echo $cdnTime; ?>'"  <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
               <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/member.png">
                </div>
               <div class="panelList-label">
                   <span>殺友專區</span>
               </div>
           </a>
       </div>
        <div class="panelList panelListPlace">
    <?php if($user->is_logged || !empty($_SESSION['auth_id'])){ ?>
            <a href="#" onClick="javascript:logout()">
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-loginout.png">
                </div>
                <div class="panelList-label">
                    <span>登出</span>
                </div>
            </a>
    <?php }else{ ?>
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/member/userlogin/?<?php echo $cdnTime; ?>'" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-login.png">
                </div>
                <div class="panelList-label">
                    <span>殺友登錄</span>
                </div>
            </a>
        <?php } ?>
        </div>
    </div>
</div><!-- /panel -->
</div><!--data-role="page"-->
</body>
</html>
<script>
    // 不是在微信  也不是在appbsl的app裏  才顯示Navigator bar

    $(document).ready(function() {
        // if(is_kingkr_obj()) {
        //     $('#deposititem1').show();
        // }else {
        //     $('#deposititem2').show();
        // }
    });
</script>
