<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>領卷</title>
    <link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/collar.css">
    <!--[if (lt IE 9) & (!IEMobile)]>
        <script src="js/html5shiv.min.js" type="text/javascript"></script>
    <!--<![endif]-->
</head>

<body>

<?php
        //接收後端狀態參數至頁面
        $retCode = _v('retCode');
        $retMsg = _v('retMsg');
				$get_oscode_result_arr = _v('get_oscode_result_arr');
?>
<script>
    //輸出後端狀態參數至console log
    console.log('retCode = '+'<?php echo $retCode; ?>');
    console.log('retMsg = '+'<?php echo $retMsg; ?>');
		console.log('get_oscode_result_arr = '+'<?php echo $get_oscode_result_arr; ?>');
</script>


    <?php if( $retCode == 1 ||  $retCode == -1){?>
    <div class="Ticket_collar">
        <div class="th_collar">
            <div class="collar">
                <header>
                    <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/logo_Ticket.png">
                </header>
                <div>
                    <!--領卷成功-->
										<p><?php echo $retMsg; ?></p>
                </div>
                <footer>
                    <a href="https://play.google.com/store/apps/details?id=tw.com.saja.userapp"> <img
                            src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/Android-app-store.svg" alt=""></a>
                    <a href="https://itunes.apple.com/tw/app/id1441402090"> <img
                            src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/IOS-app-store.svg" alt=""></a>
                </footer>
            </div>
        </div>
    </div>
    <?php  }else if($retCode == 0 ||  $retCode == -3){?>
        <div class="Ticket_collar">
            <div class="th_collar">
                <div class="failure">
                    <!--領券失敗！-->
										<p><?php echo $retMsg; ?></p>
                    <a class="btn_go" href="javascript:window.location.reload()">
                        <span>再試一次</span>
                    </a>
                </div>
            </div>
        </div>
    <?php }else{?>
        <div class="Ticket_collar">
            <div class="th_collar">
                <div class="failure">
                    <!--未登入！-->
										<p><?php echo $retMsg; ?></p>
                    <a class="btn_go" href="<?php echo APP_DIR;?>/member/auserlogin/">
                        <span>請重新登入</span>
                    </a>
                </div>
            </div>
        </div>
    <?php }?>
</body>
</html>
