<!DOCTYPE html>
<html lang="zh-Hant-TW">

<head>
    <meta charset="UTF-8">
    <meta name="viewport"
        content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <title>領卷</title>
    <link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/collar.css">
    <!--[if (lt IE 9) & (!IEMobile)]>
        <script src="js/html5shiv.min.js" type="text/javascript"></script>
    <!--<![endif]-->
</head>

<body>

	<?php
			// $retCode = _v('retCode');
			// $retMsg = _v('retMsg');
			// $oscode_num = _v('oscode_num');
			// $product_id = _v('product_id');
			// $product_name = _v('product_name');
			// $product_pic = _v('product_pic');
			// $product_offtime = _v('product_offtime');
			//
			// $product_display = _v('product_display');
			// $product_closed = _v('product_closed');
			//
			// $activity_name = _v('activity_name');
			// $behav =  _v('behav');
			// $lbuserid = _v('lbuserid');
			// $userid = _v('userid');
			// $user_src = _v('user_src');
			// $promote_menu = _v('promote_menu');
			//
			// $user_already_taken_oscode = _v('user_already_taken_oscode');

			$cdnTime = date("YmdHis");
	?>

<?php echo $cdnTime; ?>

    <div id="wrap">
        <main>
            <div class="main_content">
                <div class="Ticket_collar">
                    <!-- 成功 -->
                    <div class="th_collar">
                        <div class="collar">
                            <header>
                                <img src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/logo_Ticket.png">
                            </header>
                            <div>
                                <p>領卷成功</p>
                            </div>
                            <footer>
                                <a href="https://play.google.com/store/apps/details?id=tw.com.saja.userapp"> <img
                                        src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/Android-app-store.svg" alt=""></a>
                                <a href="https://itunes.apple.com/tw/app/id1441402090"> <img
                                        src="<?PHP echo IMG_URL.APP_DIR; ?>/static/img/IOS-app-store.svg" alt=""></a>
                            </footer>
                        </div>
                    </div>
                    <!-- 失敗 -->
                    <div class="th_collar">
                        <!-- <div class="failure">
                            <p>領券失敗！</p>
                            <a class="btn_go" href="javascript:;"">
                                <span>再試一次</span>
                            </a>
                        </div> -->
                    </div>
                    <!-- 已領卷 -->
                    <div class="th_collar">
                        <!-- <div class="failure">
                            <p>已領卷</p>
                            <a class="btn_go" href="javascript:;"">
                                <span>再試一次</span>
                            </a>
                        </div> -->
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
<script>
</script>
