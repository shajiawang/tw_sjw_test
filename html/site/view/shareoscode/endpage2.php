<html>
    <head>
        <meta charset="utf-8">
       
        <meta property="og:url"           content="<?php echo BASE_URL.$_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="新殺價王" />
		<meta property="og:description"   content="創新殺價式購物，突破傳統，出價最低唯一者可獲得商品，免費送下標券" />
		
		<meta property="og:image"         content="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/activity-share-img-live.jpg" />
		<meta property="og:image:type"    content="image/png">
        <meta property="og:image:width"   content="1200">
        <meta property="og:image:height"  content="630">
        <title>殺價王 直播限定送券</title>
        <!-- RWD -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        
        <!--Web，Android，Microsoft和iOS（iPhone和iPad）應用程式圖標-->
        <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		  
		<!-- 瀏覽器初始化 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/reset.css">

        <!-- flexbox 框架 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/flexbox.css">
        
        <!-- shareoscode.CSS -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/shareoscode.min.css">

        <script src="<?PHP echo APP_DIR; ?>/static/js/jquery-2.1.4.js"></script>
        
        <script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/vendor/base64/base64.js"></script>
    </head>
    <body>
        <?php 
            $retCode = _v('retCode');
            $retMsg = _v('retMsg');
            $oscode_num = _v('oscode_num');
            $product_id = _v('product_id');
            $product_name = _v('product_name');
			$product_pic = _v('product_pic');

            $product_offtime = _v('product_offtime');
			$product_offtime = date('Y-m-d H:i:s', $product_offtime);

            $product_display = _v('product_display');
            $product_closed = _v('product_closed');

            $activity_name = _v('activity_name');
            $behav =  _v('behav');
            $lbuserid = _v('lbuserid');
            $userid = _v('userid');
            $user_src = _v('user_src');
            $promote_menu = _v('promote_menu');
        
            $user_already_taken_oscode = _v('user_already_taken_oscode');
			$json = _v('json');
            $cdnTime = date("YmdHis");
        ?>
       
        <div id="shareoscode-activityhome">
			<?php if($json!='Y'){?>
            <div class="sajaNavBar">
                <div class="sajaNavBarIcon back">
                    <a href="javascript:history.back();return false;" class="ui-link">
                        <img src="<?PHP echo APP_DIR; ?>/static/img/back.png">
                    </a>
                </div>
                <div class="share-title">殺價王 直播限定送券</div>
			</div>
			<?php } ?>
            <div class="share-flexbox">
                <div class="activityhome-solgan-box">
                    <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-live-bg.jpg?<?php echo $cdnTime; ?>" alt="">
<!-- 舊banner 的遊戲規則按鈕 & 連結FB粉專按鈕 -->
<!--
                    <div class="header-info-box">
                        <div class="header-rule-btn">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-rule-btn.png" alt="">
                        </div>
                        <div class="header-txt">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-txt.png" alt="">
                        </div>
                        <div class="header-fb-bar d-flex align-items-center justify-content-center">
                            <div class="header-fb-btn">
                                <a href="https://www.facebook.com/newsajawang/" target="_blank">
                                    <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-fb-btn.png" alt="">
                                </a>
                            </div>
                            <div class="header-gofb-btn">
                                <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/header-gofb-btn.png" alt="">
                            </div>
                        </div>
                    </div>
-->
<!-- / 舊banner 的遊戲規則按鈕 & 連結FB粉專按鈕 -->
                </div>
                <div class="activityhome-box">
                    <ul class="activityhome-ul">
                        <!-- 生成list -->
                    </ul>
                </div>
                <!-- 遊戲規則彈窗(有自己的遮罩) -->
                <div class="headerRuleModal" style="display:none">
                    <div class="img-box">
                        <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/open-game-rule.png" alt="">
                    </div>
                    <div class="headerRuleModal-cover"></div>
                </div>
            </div>
        </div>
        <script>
            var esActivity = {
                class:{
                    isOver: 'isover',
                },
                dom: {
                    ul: '.activityhome-ul',
                    li: '.activityhome-li'
                }
            }
            var $APP_DIR = '<?PHP echo APP_DIR; ?>';
            var b = new Base64();
            var user_already_taken_oscode = '<?php echo $user_already_taken_oscode; ?>' || false;
            var $arr_uato = $.parseJSON('<?php echo $user_already_taken_oscode;?>');
            
            $(function(){
                //header-規則按鈕
                $('.activityhome-solgan-box .header-rule-btn').on('click', function(){
                    $('.headerRuleModal').show();
                    $('#shareoscode-activityhome').addClass('modal-open');
                    $('.headerRuleModal-cover').on('click', function(){
                        $('.headerRuleModal').hide();
                        $('#shareoscode-activityhome').removeClass('modal-open');
                        event.stopPropagation();
                    })
                });
                
                //生成list-group
                createLi();
                
                //自動彈窗
                    var $retCode = '<?php echo $retCode ?>';
                    var $retMsg = '<?php echo retMsg ?>';
                    var $proid = '<?php echo $product_id ?>';
                    var $date = '<?php echo $product_offtime ?>';
                    var $imgsrc = '<?php echo $product_pic ?>';
                    var $proname = '<?php echo $product_name ?>';
                    var $oscodenum = '<?php echo $oscode_num ?>';

                    createGetOscodeModal($retCode,$retMsg,$proid,$date,$imgsrc,$proname,$oscodenum);
            })
            
            //載入畫面
                //生成list
            function createLi(){
                var $noproImg = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';
                var $arrLi = $.parseJSON('<?php echo $promote_menu?>');
                if($arrLi.length > 0){
                    $.each($arrLi, function(i,item){
                        var $isgot = false;
                        var $thatPid = item['productid'];
                        if($arr_uato !== false){
                            $.map($arr_uato,function(item, index){
                                if(item['productid'] == $thatPid) {
                                    $isgot = true;
                                }
                            })
                        }
                        var $nowTime = new Date();                                  //現在時間
                        var $proOffTime = new Date(getDate(item['offtime']));       //商品結標時間
                        var $isOverTime = $proOffTime - $nowTime;                   //計算是否結標 (小於0代表結標)
                        $(esActivity.dom.ul).append(
                            $('<li/>')
                            .addClass('activityhome-li d-flex align-items-stretch'+($isOverTime < 0 ?' isover':'')+($isgot == true ?' isgot':''))
                            .attr('id',item['productid'])
                            .append(
                                $('<div class="activityhome-img"/>')
                                .append(
                                    $('<img class="img-fluid"/>')
                                    .attr('src',$APP_DIR+'/images/site/product/'+item['thumbnail'])
                                )
                            )
                            .append(
                                $('<div class="activityhome-infobox d-flex flex-column justify-content-around"/>')
                                .append(
                                    $('<div class="product-name d-flex text-left"/>')
                                    .append(
                                        $('<div class="content">'+item['name']+'</div>')
                                    )
                                )
                                .append(
                                    $('<div class="overtime d-inlineflex text-left flex-wrap"/>')
                                    .append(
                                        $('<div class="title">結標時間：</div>')
                                    )
                                    .append(
                                        $('<div class="title">'+item['offtime']+'</div>')
                                    )
                                )
                                .append(
                                    $('<div class="btn-box d-flex ml-auto flex-wrap"/>')
    //                                .append(
    //                                     $('<div class="btn-item description-btn">商品說明</div>')
    //                                )
    //                                .append(
    //                                     $('<div class="btn-item rule-btn">下標規則</div>')
    //                                )
    //                                .append(
    //                                     $('<div class="btn-item gosaja-btn">殺價去</div>')
    //                                )
                                    .append(
                                        //未領取 顯示 '領券去'
                                        //已領取 顯示 '已領取'
                                         $('<div class="btn-item getoscode-btn"/>')
                                        .text(($isgot == true ?' 已領取':'領券去'))
                                    )
									/*
                                    .append(
                                        //已領券 顯示分享按鈕
                                        ($isgot == true ?'<div class="btn-item fbshare-btn">分享領券</div>':'')
                                    )
                                    .append(
                                        //已領券 顯示殺價去
                                        ($isgot == true ?'<div class="btn-item gosaja-btn">殺價去</div>':'')
                                    )
									*/
                                )
                            )
                            .append(
                                //商品詳情，彈窗用
                                $('<input type="hidden" name="rule"/>')
                                .attr({'value':(item['rule'] == null || item['rule'] == '')?'':item['rule']})
                            )
                        )

                        //event - 領券去
                        $('#'+item['productid']+' .getoscode-btn').on('click', function(){
                            var $that = $(this);
                            animate(300);                   //轉場動畫
                            function animate($time){
                                $('body,html').stop().animate({scrollTop:0},$time);
                                setTimeout(function(){location.href = $APP_DIR+'/shareoscode/getActivityPromoteScode/?user_src=<?php echo $user_src ?>&productid='+item['productid']+'&behav=lb&lbuserid=<?php echo $lbuserid; ?>&json=<?php echo $json;?>'}, $time)
                            }
                        })
                        
                        //event - 殺價去
                        $('#'+item['productid']+' .gosaja-btn').on('click', function(){
                            location.href = $APP_DIR+'/product/saja/?channelid=1&productid='+item['productid']+'&type=';
                        });

                        //event - 商品說明
                        $('#'+item['productid']+' .activityhome-img').on('click', function(){
                            var $imgsrc = $APP_DIR+'/images/site/product/'+item['thumbnail'];
                            var $proname = item['name'];
                            var $proinfo = item['rule']==null || item['rule']==''?'':item['rule'];
                            createGetProductModal($imgsrc,$proname,$proinfo);
                        })
                    })
                }else{
                     $(esActivity.dom.ul).append(
                        $('<div class="nopro"/>')
                        .append(
                            $('<div class="nopro-img"/>')
                            .append(
                                $('<img class="img-fluid" src="'+$noproImg+'"/>')
                            )
                        )
                        .append(
                            $('<div class="nopro-text">目前無相關活動</div>')
                        )
                     )
                } 
            }
            
            //開啟領券modal
            function createGetOscodeModal($retCode,$retMsg,$proid,$date,$imgsrc,$proname,$oscodenum) {
                var $shareActImg = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';
                var $errorImg = '<?php echo BASE_URL.APP_DIR;?>/static/img/share-error-icon.png';
                var $gotoscode_txt = '您已領過本檔殺價券!!';
                var $overAct_txt = '查無此活動或連結路徑有誤';
                var $error_txt = '資料錯誤!! 請重新操作';
                
                var $product_display = '<?php echo $product_display ?>';
                var $product_closed = '<?php echo $product_closed ?>';

                switch($retCode){
                    case '1':           //成功領取
                        $('#shareoscode-activityhome').append(
                            $('<div class="getOscodeModal modalstyle"/>')
                            .append(
                                $('<div class="modal-overtime">結標時間：<br>'+$date+'</div>')
                            )
                            .append(
                                $('<div class="modal-pro-box"/>')
                                .append(
                                    $('<div class="pro-img-box"/>')
                                    .append(
                                        $('<img class="img-fluid" src="'+$imgsrc+'"/>')
                                    )
                                )
                                .append(
                                    $('<div class="pro-title">'+$proname+'</div>')
                                )
                                .append(
                                    $('<div class="oscode-info"/>')
                                    .append(
                                        $('<div class="p1">成功獲取 殺價券 x '+$oscodenum+'</div>')
                                    )
                                )
                            )
                            .append(
                                $('<div class="modal-btn-box d-flex"/>')
                                .append(
                                    $('<div class="then-btn get-btn" style="margin-left: auto; margin-right: auto;">繼續領券</div>')
                                )
								/*
                                .append(
                                    $('<div class="then-btn go-btn">殺價去</div>')
                                )
								*/
                            )
                        )
                        .append(
                            $('<div class="modalcover"/>')
                        )
                        $('#shareoscode-activityhome').addClass('modal-open');

                        //event - 繼續領券
                        $('.modal-btn-box .then-btn.get-btn').on('click', function(){
                            location.href = $APP_DIR+'/shareoscode/activityhome/?user_src=<?php echo $user_src ?>&behav=lb&lbuserid=<?php echo $lbuserid; ?>&json=<?php echo $json;?>';
                        });
						/*
                        //殺價去按鈕
                        $('.modal-btn-box .then-btn.go-btn').on('click', function(){
                            location.href = $APP_DIR+'/product/saja/?channelid=1&productid='+$proid+'&type=';
                        });
						*/
                        break;
                        
                    case '-1':          //領過
                        $('#shareoscode-activityhome').append(
                            $('<div class="getOscodeModal modalstyle"/>')
                            .append(
                                $('<div class="modal-overtime">'+$gotoscode_txt+'</div>')
                            )
                            .append(
                                $('<div class="modal-pro-box"/>')
                                .append(
                                    $('<div class="pro-img-box"/>')
                                    .append(
                                        $('<img class="img-fluid" src="'+$shareActImg+'"/>')
                                    )
                                )
                            )
                            .append(
                                $('<div/>')
                                .addClass('modal-btn-box d-flex ' + ($product_display == 'N' || $product_closed == 'Y' || $product_closed == 'NB' ?'justify-content-center':''))
                                .append(
                                    $('<div class="then-btn get-btn" style="margin-left: auto; margin-right: auto;">繼續領券</div>')
                                )
								/*
                                .append(
                                    ($product_display == 'N' || $product_closed == 'Y' || $product_closed == 'NB' ?'':'<div class="then-btn go-btn">殺價去</div>')
                                )
								*/

                            )
                        )
                        .append(
                            $('<div class="modalcover"/>')
                        )
                        $('#shareoscode-activityhome').addClass('modal-open');

                        //繼續領券按鈕
                        $('.modal-btn-box .then-btn.get-btn').on('click', function(){
                            location.href = $APP_DIR+'/shareoscode/activityhome/?user_src=<?php echo $user_src ?>&behav=lb&lbuserid=<?php echo $lbuserid; ?>&json=<?php echo $json; ?>';
                        });
						/*
                        //殺價去按鈕
                        $('.modal-btn-box .then-btn.go-btn').on('click', function(){
                            location.href = $APP_DIR+'/product/saja/?channelid=1&productid='+$proid+'&type=';
                        });
						*/
                        break;
                        
                    case '0':           //活動結束 或 商品未開啟
                        $('#shareoscode-activityhome').append(
                            $('<div class="getOscodeModal modalstyle"/>')
                            .append(
                                $('<div class="modal-overtime">'+$overAct_txt+'</div>')
                                .append(
                                    $('<p>請重新操作流程</p>')
                                )
                            )
                            .append(
                                $('<div class="modal-pro-box"/>')
                                .append(
                                    $('<div class="pro-img-box"/>')
                                    .append(
                                        $('<img class="img-fluid" src="'+$shareActImg+'"/>')
                                    )
                                )
                            )
                            .append(
                                $('<div class="modal-btn-box d-flex justify-content-center"/>')
                                .append(
                                    $('<div class="then-btn get-btn">繼續領券</div>')
                                )
                            )
                        )
                        .append(
                            $('<div class="modalcover"/>')
                        )
                        $('#shareoscode-activityhome').addClass('modal-open');

                        //繼續領券按鈕
                        $('.modal-btn-box .then-btn.get-btn').on('click', function(){
                            location.href = $APP_DIR+'/shareoscode/activityhome/?user_src=<?php echo $user_src ?>&behav=lb&lbuserid=<?php echo $lbuserid; ?>&json=<?php echo $json; ?>';
                        });
                        break;
                    case '-2':
                        loginModal();
                        break;
                    default:            //參數無效
                        $('#shareoscode-activityhome').append(
                            $('<div class="getOscodeModal modalstyle"/>')
                            .append(
                                $('<div class="modal-overtime">'+$error_txt+'</div>')
                            )
                            .append(
                                $('<div class="modal-pro-box"/>')
                                .append(
                                    $('<div class="pro-img-box"/>')
                                    .append(
                                        $('<img class="img-fluid" src="'+$errorImg+'"/>')
                                    )
                                )
                            )
                            .append(
                                $('<div class="modal-btn-box d-flex justify-content-center"/>')
                                .append(
                                    $('<div class="then-btn get-btn">關閉</div>')
                                )
                            )
                        )
                        .append(
                            $('<div class="modalcover"/>')
                        )
                        $('#shareoscode-activityhome').addClass('modal-open');

                        //繼續領券按鈕
                        $('.modal-btn-box .then-btn.get-btn').on('click', function(){
                            location.href = $APP_DIR+'/shareoscode/activityhome/?user_src=<?php echo $user_src ?>&behav=lb&lbuserid=<?php echo $lbuserid; ?>&json=<?php echo $json; ?>';
                        });
                }
            };
            
            //開啟商品資訊modal
            function createGetProductModal($imgsrc,$proname,$proinfo) {
                $('#shareoscode-activityhome').append(
                    $('<div class="getProductModal"/>')
                    .append(
                        $('<div class="modal-proinfo-box"/>')
                        .append(
                            $('<div class="proinfo-img-box"/>')
                            .append(
                                $('<img class="img-fluid" src="'+$imgsrc+'"/>')
                            )
                        )
                        .append(
                            $('<div class="proinfo-title">'+$proname+'</div>')
                        )
                        .append(
                            $('<div class="proinfo-info"/>')
                            .append(
                                $('<div class="p1"/>')
                                .html(b.decode($proinfo))
//                                .html(decodeURIComponent($proinfo))
                            )
                        )
                    )
                    .append(
                        $('<div class="modal-proinfo-close"/>')
                        .append(
                            $('<img class="img-fluid"/>')
                            .attr('src','<?PHP echo APP_DIR; ?>/static/img/close-circle.png')
                        )
                    )
                )
                .append(
                    $('<div class="modalcover"/>')
                )
                $('#shareoscode-activityhome').addClass('modal-open');
                $('.modal-proinfo-close').on('click', function(){
                    $('.getProductModal').remove();
                    $('.modalcover').remove();
                    $('#shareoscode-activityhome').removeClass('modal-open')
                })
                $('.modalcover').on('click', function(){ 
                    var event = event || window.event;  //用於IE  
                    if(event.preventDefault) event.preventDefault();  //標準技術
                    if(event.returnValue) event.returnValue = false;  //IE  
                    return false;   //用于处理使用对象属性注册的处理程序 
                })
            }
            
            //請求登入modal
            function loginModal() {
                $('#shareoscode-activityhome').append(
                    $('<div class="loginModal modalstyle"/>')
                    .append(
                        $('<div class="login-modalTitle">登入提醒</div>')
                    )
                    .append(
                        $('<div class="login-modalBody"/>')
                        .append(
                            $('<div class="login-info text-center">領券前，請先登入會員!</div>')
                        )
                    )
                    .append(
                        $('<div class="login-modalFooter"/>')
                        .append(
                            $('<div class="login-btn">前往登入</div>')
                        )
                    )
                )
                .append(
                    $('<div class="modalcover"/>')
                )
                
                $('.login-btn').on('click', function(){
                    location.href = $APP_DIR+'/shareoscode/share_login/?user_src=<?php echo $user_src ?>&behav=lb&lbuserid=<?php echo $lbuserid; ?>&json=<?php echo $json; ?>';
                })
            }
            
            //ios 時間問題轉換
            function getDate(time) {  
                //將xxxx-xx-xx的時間格式，轉換為 xxxx/xx/xx的格式   
                time = time.replace(/\-/g, "\/");  
                return time;
            };
            
        </script>
    </body>
</html>