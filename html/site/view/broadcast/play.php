<?php 
$arrBroadcast=_v('live_broadcast_data');

// 用戶
$userid=$_SESSION['auth_id'];
$lbuserid=$arrBroadcast['lbuserid'];
$lbid=$arrBroadcast['lbid'];
$productid=$arrBroadcast['productid'];
$product_name=$arrBroadcast['product_name'];
?>
<!--  CDN Bootstrap 4.1.1  -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>
    
<div class="broadcast-play">
    <div class="video-header d-flex align-items-center">
        <div class="rm-info d-flex align-items-center mr-auto">
            <div class="spimg">
                <img src="https://picsum.photos/25/25/?random" alt="">
            </div>
            <div class="rm-txt d-inlineflex flex-column">
                <div class="spname"><?php echo $arrBroadcast['lbname']; ?></div>
                <div class="rmname">( <?php echo $arrBroadcast['name']; ?> )</div>
            </div>
        </div>
        <div class="rm-btns">
            <a class="btn-icon" onclick="watchAd()">看廣告拿殺價券</a>
        </div>
    </div>

    <div class="video-container">
<?php if($lbuserid==3 && $lbid==3) { ?>
<!--  Thomas 測試用  -->
    <div id="lb_player"></div>
        <script>
          // 2. This code loads the IFrame Player API code asynchronously.
          var tag = document.createElement('script');

          tag.src = "https://www.youtube.com/iframe_api";
          var firstScriptTag = document.getElementsByTagName('script')[0];
          firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

          // 3. This function creates an <iframe> (and YouTube player)
          //    after the API code downloads.
          var player;
          function onYouTubeIframeAPIReady() {
                player = new YT.Player('lb_player', {
                  height: '270',
                  width: '480',
                  videoId: '4ZVUmEUFwaY',
                  playerVars : {
                      'playsinline':1,
                      'autoplay': 1,
                      'origin':'https://www.saja.com.tw',
                      'controls':2,
                      'color':'red'
                  },
                  events: {
                    'onReady': onPlayerReady,
                    'onStateChange': onPlayerStateChange
                  }
                });
          }

          // 4. The API will call this function when the video player is ready.
          function onPlayerReady(event) {
                // alert("onPlayerReady");
                event.target.playVideo();
          }

          // 5. The API calls this function when the player's state changes.
          //    The function indicates that when playing a video (state=1),
          //    the player should play for six seconds and then stop.
          var done = false;
          function onPlayerStateChange(event) {
             // alert("onPlayerStateChange");
            /*
            if (event.data == YT.PlayerState.PLAYING && !done) {
              setTimeout(stopVideo, 15000);
              done = true;
            }*/
          }
          function stopVideo() {
               player.stopVideo();
          }
        </script>
<!--  Thomas 測試script 完  -->
<?php } else if(strpos($arrBroadcast['lb_url'],"www.youtube.com")>0) {  ?>
    
        <!-- youtube live stream -->
        <iframe id="ytplayer" type="text/html"
                width="480" height="270" 
                src="<?php echo $arrBroadcast['lb_url']; ?>?autoplay=1&controls=1&playsinline=1&origin=https://www.saja.com.tw" 
                scrolling="no"  frameborder="0"
                allow="autoplay; encrypted-media" >
        </iframe>
        <!-- youtube -->

<?php  } else if(strpos($arrBroadcast['lb_url'],"www.facebook.com")>0) {  ?>
       <!-- facebook  live stream --> 
           <!-- iframe width="100%"  scrolling="no"  frameborder="0"   
                   style="border:none;overflow:hidden"
                   allow="autoplay; encrypted-media"
                   allowTransparency="true"
                   allowfullscreen="false" 
                   webkitallowfullscreen="false" 
                   mozallowfullscreen="false" 
                   oallowfullscreen="false" 
                   msallowfullscreen="false"
                   src="https://www.facebook.com/plugins/video.php?href=<?php echo urlencode($arrBroadcast['lb_url']); ?>&width=320&height=180&show_text=false&appId=254725105160559">
           </iframe -->
       <div id="fb-root"></div>
          <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.1";
            fjs.parentNode.insertBefore(js, fjs);
          }(document, 'script', 'facebook-jssdk'));
          </script>
         
          <div class="fb-video" data-href="<?php echo $arrBroadcast['lb_url']; ?>"  
               data-allowfullscreen="false"
               data-width="480" 
               data-height="270"                 
               data-autoplay="true"               
               data-show-text="false">
            <div class="fb-xfbml-parse-ignore">
              <blockquote cite="<?php echo $arrBroadcast['lb_url']; ?>">
                <a href="<?php echo $arrBroadcast['lb_url']; ?>">&nbsp;</a>
                <p>&nbsp;</p>
              </blockquote>
            </div>
          </div>
     
     <!-- facebook -->
<?php  }  ?>
    </div>
    
    <div class="spdesc d-flex justify-content-center align-items-center text-center">
        <span><?php echo $arrBroadcast['lbname']; ?> 這次要帶大家了解殺價王，然後來個殺價券大放送，快來領取殺價券  跟著 <?php echo $arrBroadcast['lbname']; ?> 一起去拚優惠吧 !</span>
    </div>
    
<!--  殺價商品直播時段列表  -->
    <div class="stroke">
        <div class="stTitle">殺價商品直播時段列表</div>
        <ul class="stListGroup">
            <li class="stList d-flex">
                <div class="listTime">19:00 - 20:00</div>
                <div class="listInfo">全護清爽防曬噴霧 SPF50 PA++++ 75ml</div>
            </li>
            <li class="stList d-flex">
                <div class="listTime">20:00 - 21:00</div>
                <div class="listInfo">韓國HAPPYCALL IH彩色陶瓷深湯鍋-20CM活力橘</div>
            </li>
            <li class="stList d-flex">
                <div class="listTime">21:00 - 22:00</div>
                <div class="listInfo">宏碁ACER Swift1超輕薄筆電 SF113-31</div>
            </li>
        </ul>
        <div class="footerBtn d-flex justify-content-between">
            <div class="broadcastBtn d-flex justify-content-center" type="button" name="btn_saja" id="btn_saja" onclick="gosaja('<?php echo $productid; ?>');">
                <div class="d-inlineflex align-items-center">
                    <img class="btnImg" src="<?PHP echo APP_DIR; ?>/static/img/broadcast/broadcast-btn1.png" alt="">
                    <div class="btnName">殺價去</div>
                </div>
                
            </div>
            <div class="broadcastBtn d-flex justify-content-center" type="button" name="btn_receive" id="btn_receive" onclick="receiveOscode('<?php echo $lbuserid; ?>','<?php echo $userid; ?>','<?php echo $productid; ?>')">
                <div class="d-inlineflex align-items-center">
                    <img class="btnImg" src="<?PHP echo APP_DIR; ?>/static/img/broadcast/broadcast-btn2.png" alt="">
                    <div class="btnName">領取殺價券</div>
                </div>
            </div>
        </div>
    </div>

<!-- div class="center">
   <h1>
     <p>活動名稱 : <?php echo $arrBroadcast['spname']; ?></p> 
     <p>活動說明 : <?php echo $arrBroadcast['spdesc']; ?></p> 
     <p>商品 : <?php echo $product_name; ?></p>
     <p>商品 id : <?php echo $productid; ?></p>
     <p>直播間 : <?php echo $arrBroadcast['name']; ?></p>
     <p>直播間id : <?php echo $arrBroadcast['lbid']; ?></p>
     <p>直播主 : <?php echo $arrBroadcast['lbname']; ?></p>
     <p>直播主id : <?php echo $arrBroadcast['lbuserid']; ?></p>
     <p>Stream URL : <?php echo urlencode($arrBroadcast['lb_url']); ?></p>
   </h1>
</div -->

    <!--彈窗-->    
    <div class="modal fade" id="watchAd" tabindex="-1" role="dialog" aria-labelledby="watchAdModalLabel" aria-hidden="true">
        <div class="modal-dialog fadeInUp animated" role="document">
            <div class="modal-content">
                <div type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <img class="btnImg" src="<?PHP echo APP_DIR; ?>/static/img/broadcast/broadcast-close.png" alt="">
                </div>
                <div class="modal-body" onclick="freeCollect();">
                    <img src="https://picsum.photos/200/200/?random" alt="">
                </div>
            </div>
        </div>
    </div>
    <!--/彈窗-->

    <!-- 領取彈窗 -->
    <div class="freeCollect" style="display:none;">
        <img class="collectImg" src="<?PHP echo APP_DIR; ?>/static/img/broadcast/broadcast-freeCollect.png" alt="" onclick="collected();">
        <div class="backdrop"></div>
    </div>
    <!-- /領取彈窗 -->
    
</div>
<script>
    function share() {
         alert('TODO : 分享此直播 !!');
    }
        
    var productid='<?php echo $productid; ?>';
    function gosaja(prodid) {
         window.location.href='/site/product/saja/?productid='+prodid;
    }
    
    function receiveOscode(lbuserid, userid, prodid) {
          $.post(
              '<?php echo BASE_URL.APP_DIR; ?>/broadcast/give_oscode/',
              { lbuserid:lbuserid, 
                userid:userid,
                productid:prodid},
              function(r) {
                    var ret='';
                    // alert(r);
                    if((typeof r)=='undefined') {
                        alert('執行失敗 !!');
                        return false;
                    } else if((typeof r)=="string") {
                       ret = JSON.parse(r); 
                    } else if((typeof r)=="object") {
                        ret = r;   
                    }
                    alert(ret.retMsg);
                    if(ret.retCode==1) {
                        productids = ret.prodids;
                        if(productids.length==1) {
                           window.location.href='/site/product/saja/?productid='+ret.prodids[0]; 
                        } else {
                           window.location.href='/site/product/saja/'; 
                        }                        
                    }
              }
          );         
    }
    
    //看廣告拿廣告按鈕
    function watchAd() {
        $("#watchAd").modal('show');
    }
    
    //看完廣告後，廣告視窗隱藏，領取按鈕出現
    function freeCollect() {
        $("#watchAd").modal('hide');
        $(".freeCollect").show();
        $("body").toggleClass("freeCollect-open");
    }
    
    //點擊領取按鈕後關閉
    function collected() {
        $(".freeCollect").hide();
        alert('領取完成');
        $("body").toggleClass("freeCollect-open");
    }
</script>
<!-- script>
    $(function(){
        function changeBar() {
            var $navbar = $(".broadcast-content").find(".sajaNavBar");
            var $winW = $(window).outerWidth();
            var $switch = '568';   //切換的尺寸
            if($winW < $switch) {
                $navbar.removeClass("d-flex");
                $navbar.hide();
            }else{
                $navbar.addClass("d-flex");
                $navbar.show();
            }
        };
        changeBar();
        $(window).on('resize',function(){
            changeBar();
        })
    })
</script -->
<!-- 影片自動播放 
<script>
    $('.video-container iframe').load(function(){
        var a = $('.video-container iframe').contents().find("._1o0y");
        console.log(a);
    });
</script>
-->