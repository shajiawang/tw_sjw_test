<?php
    $arrOauth=_v('oauth');
    $lbid=_v('lbid');
    $lbuserid=_v('lbuserid');
    $user_src=$_SESSION['auth_id'];
    $live_broadcast_list=_v('live_broadcast_list');

//有直播間編號(lbid or lbuserid) 直接進入直播間
if(!empty($lbid) || !empty($lbuserid)) {
?>
<form id="theForm" name="theForm" action="<?php echo APP_DIR; ?>/broadcast/" method="post">
  <input type="hidden" name="lbid" id="lbid" value="<?php echo $lbid; ?>" />
  <input type="hidden" name="lbuserid" id="lbuserid" value="<?php echo $lbuserid; ?>" />
  <input type="hidden" name="user_src" id="user_src" value="<?php echo $user_src; ?>" />
</form>
<script>
    document.theForm.submit();
</script>
<?php }  else {  ?>
    <div class="broadcast-home">
        <div class="mv-box">
            <!-- play按鈕 -->
            <img class="mv-play-icon" src="<?PHP echo APP_DIR; ?>/static/img/live-mv-play.png" alt="">
            <!-- 小字info -->
            <div class="mv-slogan">影片任務 看廣告拿殺價券</div>
            <!-- 廣告 -->
            <img src="https://picsum.photos/365/78/?random" alt="">
        </div>
        <div class="live-group mx-auto d-flex justify-content-start flex-wrap">
            <?php foreach($live_broadcast_list as $item) {  
                error_log("[v/broadcast/home] : ".json_encode($item));
            ?>
                <div class="live-item">
                    <div class="item-img">
                        <a href="<?php echo BASE_URL.APP_DIR."/broadcast/?user_src=${user_src}&lbid=${item['lbid']}&lbuserid=${item['lbuserid']}" ;?>">
                            <img src="<?php echo $item['thumbnail']; ?>" alt="">
                        </a>
                    </div>
                    <!-- div class="item-liveicon">直播中</div -->
                    <div class="item-info d-flex align-items-center flex-wrap">
                        <div class="info-title"><?php echo $item['lbname']; ?></div>
                        <div class="info-viewers ml-auto d-inlineflex align-items-center">
                            <div class="viewers-img">
                                <!-- img src="<?PHP echo APP_DIR; ?>/static/img/live-viewers.png" alt="" -->
                            </div>
                            <div class="viewers-num">
                                <span class="num"><?php echo $item['name']; ?></span>
                            </div>
                        </div>
                    </div>
                </div>
            <?php  }   ?>
        </div>
    </div>
<!--
<script>
    $(function(){
        function changeBar() {
            var $navbar = $(".broadcast-content").find(".sajaNavBar");
            var $winW = $(window).outerWidth();
            var $switch = '568';   //切換的尺寸
            if($winW < $switch) {
                $navbar.removeClass("d-flex");
                $navbar.hide();
            }else{
                $navbar.addClass("d-flex");
                $navbar.show();
            }
        };
        changeBar();
        $(window).on('resize',function(){
            changeBar();
        })
    })
</script>
-->
<?php  }  ?>




