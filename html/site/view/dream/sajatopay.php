<?php
$product = _v('product');
$sajabid = _v('sajabid');
$status = _v('status');
$meta = _v('meta');
$canbid = _v('canbid');
$cdnTime = date("YmdHis");
$options = _v('options');
$type = _v('type');
$paydata = _v('paydata');
$topay = _v('topay');
$spoint = _v('spoint');
$oscode_num=_v('oscode_num');
$scode_num=_v('scode_num');
$scode_use_num=_v('scode_use_num');
if(!isset($canbid)) {
    $canbid=$_REQUEST['canbid'];
}
if(empty($canbid)) {
    $canbid='Y';
}
//閃殺商品
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
if($oscode_num>0) {
    if($oscode_num >= $topay['count']){
        $oscode_value = $topay['count'];
    }else{
        $oscode_value = $oscode_num;
    }
} 
?>
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/sajatopay.css">



<div class="sajatopay">
    <div class="sajatopay-box">
        <?php
        if(!empty($product['thumbnail'])) {
            $img_src = $product['thumbnail'];
        } elseif (!empty($product['thumbnail_url'])) {
            $img_src = $product['thumbnail_url'];
        }
        ?>
        <div class="th_pay">
            <header>下標</header>
            <div>
                <div class="pay-rightTxt">
                    <!-- 單次下標 -->
                    <?php if($topay['type'] == 'single'){ ?>
                        <span><?php echo $topay['bida']; ?></span>
                    <!-- 包牌 or 隨機下標 -->
                    <?php } else if($topay['type'] == 'range' || $topay['type'] == 'lazy'){ ?>
                        <span><?php echo $topay['bida']; ?> ~ <?php echo $topay['bidb']; ?></span>
                    <?php } ?>
                </div>
            </div>
            <footer>
                <span id="bidded"><?php echo $topay['count']; ?> 標</span>
            </footer>
        </div>
        <div class="sajatopay-payinfo">
            <!-- 下標金 -->
            <div class="payinfo-item d-flex align-items-center">
                <div class="pay-title mr-auto payw100">下標金
                    <div class="pay-rightTxt pay_price">
                        <!-- 單次下標 -->
                        <?php if($topay['type'] == 'single'){ ?>
                            <span> <?php echo '('.$topay['bida'].')'; ?></span>
                            
                        <!-- 包牌 or 隨機下標 -->
                        <?php } else if($topay['type'] == 'range' || $topay['type'] == 'lazy'){ ?>
                            <span> <?php echo '('.$topay['bida']; ?> + ... + <?php echo $topay['bidb'].')'; ?></span>
                            
                        <?php } ?>
                    </div>
                    <div class="free_price">
                        <?php if ($product['totalfee_type'] == 'F' || $product['totalfee_type'] == 'O') {
                            ?><span class="small"> 免收 </span><?php
                             } else{?>
                                <div class="pay-rightTxt pay_price">
                                    <!-- 單次下標 -->
                                    <?php if($topay['type'] == 'single'){ ?>
                                        <span> <?php echo $topay['bida']; ?>元</span>
                                        
                                    <!-- 包牌 or 隨機下標 -->
                                    <?php } else if($topay['type'] == 'range' || $topay['type'] == 'lazy'){ ?>
                                        <span> <?php echo ((($topay['bida']+$topay['bidb'])*$topay['count'])/2); ?> 元</span>
                                        
                                    <?php } ?>
                                </div>
                        <?php  } ?>
                    </div>
                </div>
            </div>
            <!-- 手續費 -->
            <div class="payinfo-item d-flex align-items-center">
                <div class="pay-title mr-auto">手續費
                    <span> (<?php if ($product['totalfee_type'] == 'B' || $product['totalfee_type'] == 'O') { echo "免收"; }else{ echo $product['saja_fee'].'元/標';} ?>) </span>
                    <div class="pay_count">
                        <div class="pay-rightTxt ">
                            x<span id="bidded"><?php echo $topay['count']; ?></span>
                        </div>
                    </div>
                </div>
                <div class="pay-rightTxt">
                    <div class="free_price">
                        <?php if ($topay['fee_all'] > 0 & $product['totalfee_type'] !== 'B' & $product['totalfee_type'] !== 'O'){ ?>
                            <span> <?php echo $topay['fee_all']; ?> 元</span>
                        <?php } else { ?>
                            <span class="small">免收</span>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="sajatopay-paytotal d-flex align-items-center">
            <div class="pay-title mr-auto">小計</div>
            <div class="pay-rightTxt red  "><span class="Subtotal"><?php echo sprintf("%1\$u",$topay['payall']); ?></span> 元</div>
        </div>
    </div>
    <!-- 判斷是否有過票劵 -->
    <script>
       $.ajax({
            type: 'POST',
            dataType: 'json',
            url:'<?php echo BASE_URL.APP_DIR;?>/oscode/?json=Y',
            success: function (msg) {
                // var up_record=msg.retObj.data.record;
                // var totalcount =msg.retObj.data.page.totalcount;
                console.log("可使用種類總數", msg.getcount);
                console.log("失效種類總數", msg.usecount);
                if(msg.getcount == 0 && msg.usecount == 0){
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        url:'<?php echo BASE_URL.APP_DIR;?>/scode/scode_list/?json=Y',
                        success: function (msg) {
                            // var up_record=msg.retObj.data.record;
                            // var totalcount =msg.retObj.data.page.totalcount;
                            // console.log("殺價有效"+"無效"+msg.usecount,msg.getcount == 0 && msg.usecount == 0);
                            if(msg.getcount == 0 && msg.usecount == 0){
                                // $(".total_price").css('display','none');
                                // $(".s_much").text("無");
                                $('.select_discount').css('border-bottom','2px solid rgba(238, 238, 238, 0.582)');
                            }else{
                                $('.total_price').append(' <header>總計</header>'+
                                ' <div class="sumprice">'+
                                '<span><?php echo sprintf("%1\$u",$topay['payall']); ?></span>元'+
                                '</div>'
                                )
                                $('.total_price').hide();

                                
                            }

                        }
                    }); 
                    
                }else{
                    $('.total_price').append(' <header>總計</header>'+
                        ' <div class="sumprice">'+
                            '<span><?php echo sprintf("%1\$u",$topay['payall']); ?></span>元'+
                        '</div>'
                    )
                }

            }
        }); 
    </script>
    
    <!-- 選擇折抵頁面 -->
    <div class="sajatopay-select show_sum">

    <?php if($canbid!='N') { ?>
        <div class="sele-items">
            <!-- 殺價券顯示 -->
           <div class="select_discount">
                <span class="s_discount">選擇折抵</span>
                <span class="s_much"></span>
           </div>
           <div class="total_price">
           </div>
        </div>
    <!-- 折抵方案票券頁 -->
    <div class="th_ticket hasHeader">
        <div class="navbar sajaNavBar d-flex align-items-center">
            <div class="sajaNavBarIcon back icon_back">
                <a href="javascript:;" >
                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
                </a>
            </div>
            <div class="navbar-brand">
                <h3>折抵方案</h3>
            </div>
        </div>
    <div>
    <!-- 殺價券 -->
    <div id="os_ticket" class="sele-item sele-oscode align-items-center disabled opt_ticket">
        <div class="select_ticket">
            <figure>
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/price_ticket.png">
            </figure>
            <figcaption>
                <span>殺價劵</span>
                <p>使用於 <span class="red">指定</span> 殺價商品</p>
            </figcaption>
            <div class="radio-title-box align-items-center">
                <div class="Txt"></div>
            </div>
        </div>
    </div>
                    
    <!-- S券顯示 -->
    <?php if ($scode_num + $scode_use_num > 1){ ?>
        <div id="s_ticket" class="sele-item sele-scode d-flex align-items-center disabled opt_ticket">
            <div class="select_ticket">
                <figure>
                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/price_superticket.png">
                </figure>
                <figcaption>
                    <span>超級殺價劵</span>
                    <p>使用於 <span class="red">所有</span> 商品</p>
                </figcaption>
                <div class="radio-title-box d-flex align-items-center"> 
                    <div class="Txt"> </div>
                </div>
            </div>
        </div>
    <?php } ?>

                    </div>
                </div>
  
            <?php } ?>
        </div>


    </div>
    <!-- 開啟付款確認 -->
    <div class="btn_nextpage">
       <a href="javascript:;">下一步</a> 
    </div>

    <!-- 付款確認 -->
    <div class="check_pay">
        <div class="navbar sajaNavBar d-flex align-items-center">
            <div class="sajaNavBarIcon back icon_back">
                <a href="javascript:;" >
                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
                </a>
            </div>
            <div class="navbar-brand">
                <h3>付款確認</h3>
            </div>
        </div>
        <div class="hasHeader">
            <p class="sum_payment"></p>
            <p class="cost_pay"><?php echo sprintf("%1\$u",$topay['payall']); ?>枚</p>
            <span>總付款金額</span>
            <div class="coin">
                <figure>

                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_price.png">
                </figure>
                <figcaption>殺價幣</figcaption>
                <footer>
                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_check.png">
                </footer>                  
            </div>
        </div>

        <!-- 付款按鈕 -->
        <footer>
            <div class="productSaja-footernav">
                <div class="productSaja-button">

                </div>
            </div>
        </footer>                          
    </div>


    <input type="hidden" name="pay_type" id="pay_type" value="<?php echo $product['pay_type']; ?>">
    <input type="hidden" name="type" id="type" value="<?php echo $topay['type']; ?>">
    <input type="hidden" name="count" id="count" value="<?php echo $topay['count']; ?>"/>
    <input type="hidden" name="fee_all" id="fee_all" value="<?php echo $topay['fee_all']; ?>"/>
    <input type="hidden" name="total_all" id="total_all" value="<?php echo $topay['total_all']; ?>">
    <input type="hidden" name="payall" id="payall" value="<?php echo $topay['payall']; ?>">

    <input type="hidden" name="oscode_num" id="oscode_num" value="<?php echo $oscode_value ?>">
    <!-- <input type="hidden" name="scode_num" id="scode_num" value="<?php echo $scode_value ?>"> -->
    <input type="hidden" name="scode_num" id="scode_num" value="<?php echo $scode_num ?>">
    
    <input type="hidden" name="bida" id="bida" value="<?php echo $topay['bida']; ?>"/>
    <input type="hidden" name="bidb" id="bidb" value="<?php echo $topay['bidb']; ?>"/>
    <input type="hidden" name="saja_fee" id="saja_fee" value="<?php echo $product['saja_fee'];?>"/>
    <input type="hidden" name="autobid" id="autobid" value="<?php echo $paydata['autobid'];?>"/>
    <input type="hidden" name="productid" id="productid" value="<?php echo $product['productid']; ?>">
    <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
    <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
    <input type="hidden" name="prod_le" id="prod_le" value="<?php echo $config['prod_le']; ?>">


</div>



<script type="text/javascript">

    <?php if (!empty($paydata['autobid']) && $paydata['autobid'] == 'Y') { ?>
    $(window).load(function() {
        <?php if ($paydata['type'] == "single"){ ?>
        bid_1(<?php echo $product['productid']; ?>);
        <?php }else{ ?>
        bid_n(<?php echo $product['productid']; ?>);
        <?php } ?>
    });
    <?php } ?>
    
    $(function(){
        var sajatopayOption = {
            dom:{
                $seleOscode: '.sele-item.sele-oscode',
                $seleSpoint: '.sele-item.sele-spoint',
                $seleScode: '.sele-item.sele-scode'
            }
        }
        
        var $data_totalfee_type = '<?php echo $product['totalfee_type'];?>',      //收款類型
            $data_bid_type = '<?php echo $topay['type']; ?>',                     //下標方式(單次、包牌)
            $data_count = Number('<?php echo $topay['count']; ?>'),               //總標數
            $data_bid_a = parseInt('<?php echo $topay['bida']; ?>'),              //起標
            $data_bid_b = parseInt('<?php echo $topay['bidb']; ?>'),              //尾標
            $data_bidArray = [],                                                  //標陣列 (包牌用)
            $data_saja_fee = parseInt('<?php echo $product['saja_fee']; ?>'),     //手續費/標
            $data_range = parseInt('<?php echo $config['prod_le']; ?>'),          //包牌range
            $data_pay_all = parseInt('<?php echo $topay['payall']; ?>'),          //總計:標金+手續費
            $data_pay_type = '<?php echo $product['pay_type']; ?>',               //支付方式限制
            $data_spoint = parseInt('<?php echo $spoint['amount']; ?>'),          //殺幣餘額
            $data_oscode_num = Number('<?php echo $oscode_num; ?>'),                      //殺價券
            $data_scode_num = Number('<?php echo $scode_num; ?>'),                        //超級殺價券
            $data_scode_used = Number('<?php echo $scode_use_num; ?>');                   //超級殺價券使用過的張數
        
        var $isScode_show = ($data_scode_num + $data_scode_used) > 0;        //超級殺價券(現有張數 + 使用過張數，若大於0表示有見過，可顯示)
        
        // $data_totalfee_type 收款類型: 1.收下標金+手續費(A) 2.只收下標金(B) 3.只收手續費(F) 4.免費(O)
        // $data_bid_type 下標方式: 1.單次(single) 2.包牌(range) 3.隨機(lazy)
        // $data_pay_type 下標支付方式: 1.殺幣(a) 2.殺幣+S碼+殺價券(b) 3.殺幣+殺價券(c) 4.S碼+殺價券(d) 5.殺價券(e)
        
    //殺幣
        var $spoint = $(sajatopayOption.dom.$seleSpoint);
        var $spradio = $spoint.find('#sradio');
        var $sptbox_title = $spoint.find('.radio-title-box .radio-title small');
        var $sptbox_txt = $spoint.find('.radio-title-box .Txt');
    //殺券
        var $oscode = $(sajatopayOption.dom.$seleOscode);
        var $osradio = $oscode.find('#oradio');
        var $oscbox_title = $oscode.find('.radio-title-box .radio-title small');
        var $oscbox_txt = $oscode.find('.radio-title-box .Txt');
    //超券
        var $scode = $(sajatopayOption.dom.$seleScode);
        var $sradio = $scode.find('#kradio');
        var $scbox_title = $scode.find('.radio-title-box .radio-title small');
        var $scbox_txt = $scode.find('.radio-title-box .Txt');
        
        $oscbox_title.text('(折抵前殺價券張數：'+$data_oscode_num+' 張)');
        $sptbox_title.text('(折抵前殺價幣餘額：'+$data_spoint+')');
        $scbox_title.text('(折抵前超級殺價券張數：'+$data_scode_num+' 張)');
        
        /* 下標詳細陣列(標金+手續費) */
        function bidArray() {                       
            var $start = $data_bid_a;
            for(var i = 0; i < $data_count ;i++){
                var $num = $start + (i * $data_range);
                if($data_totalfee_type == 'A'){
                    $data_bidArray.push(Number($num + $data_saja_fee));       //標金+手續費
                }else if($data_totalfee_type == 'B'){
                    $data_bidArray.push(Number($num));                        //標金
                }else{
                    return;
                }
            }
;
            console.log('總價:'+sumData($data_bidArray));
        }
        

        function creatSubmitBtn(goto,paytype){
            var $box = $('.productSaja-footernav .productSaja-button');
            $box.empty();                                       //清空內容
            // goto: deposit (購買殺幣), bid (單次&包牌下標), loset (一般券張數不足), losest (超級殺價券張數不足)
            switch(goto){
                case 'deposit':
                    /* 殺價幣不足，跳出購買殺價幣視窗 */
                    $box.append(
                        $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a side">')
                        .text('付款')
                    )
                    break;
                case 'bid':
                    if($data_bid_type == 'single'){
                        // 下標結算跳頁 單次下標
                        $box.append(
                            $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')
                            .attr('onClick',"hp_bid_1(<?php echo $product['productid']; ?>,'"+paytype+"');")
                            .text('付款')
                        )
                    }else if($data_bid_type == 'range'){
                        $box.append(
                            $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')
                            .attr('onClick',"hp_bid_n(<?php echo $product['productid']; ?>,'"+paytype+"');")
                            .text('付款')
                        )
                    }
                    break;
                case 'loset':
                    $box.append(
                        $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')
                        .text('付款')
                    )
                    //彈窗 (一般券)
                    $('#topay').on('click', function(){
                        createMsgModal('殺價券');
                    })
                    break;
                case 'losest':
                    $box.append(
                        $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')        
                        .text('付款')
                    )
                    //彈窗 (超券)
                    $('#topay').on('click', function(){
                        createMsgModal('超級殺價券');
                    })
                    break;
            }
        }       //submit按鈕設定
        
        /* 用來計算陣列的總和 */
        function sumData(arr){
            var sum = 0;
            for(var i = 0; i < arr.length; i++){
                sum += arr[i];
            };
            return sum;
        }
        
        function createMsgModal(title){
            $('body').addClass('modal-open');
            $('.sajatopay').append(
                $('<div class="msgModalBox d-flex justify-content-center align-items-center"/>')
                .append(
                $('<div class="modal-content"/>')
                    .append(
                    $('<div class="modal-body"/>')
                        .append(
                            $('<div class="modal-title d-flex align-items-center justify-content-center"/>')
                            .text(title)
                        )
                        .append(
                            $('<div class="modal-txt d-flex align-items-center justify-content-center"/>')
                            .text('張數不足')
                        )
                    )
                    .append(
                        $('<div class="modal-footer"/>')
                        .append(
                            $('<div class="footer-btn">')
                            .attr('onClick',"javascript:location.href='<?php echo APP_DIR; ?>/product/'")
                            .text('回殺戮戰場')
                        )
                    )
                )
                .append(
                    $('<div class="msgModalBg">')
                )
            )
            $('.modal-close').on('click', function(){
                $('body').removeClass('modal-open');
                $('.msgModalBox').remove();
            })
        }              //彈窗
        
        function createDepositModal(title){
            $('body').addClass('modal-open');
            $('.sajatopay').append(
                $('<div class="msgModalBox d-flex justify-content-center align-items-center depositModal"/>')
                .append(
                $('<div class="modal-content"/>')
                    .append(
                    $('<div class="modal-body"/>')
                        .append(
                            $('<img class="d-close" src="<?php echo BASE_URL.APP_DIR;?>/static/img/x-circle.png">')
                        )
                        .append(
                            $('<img class="d-coin" src="<?php echo BASE_URL.APP_DIR;?>/static/img/deposit-coin.png">')
                        )
                        .append(
                            $('<div class="modal-title d-flex align-items-center justify-content-center"/>')
                            .text(title)
                        )
                        .append(
                            $('<div class="modal-txt d-flex align-items-center justify-content-center"/>')
                            .text('請選擇其他付款方式')
                        )
                    )
                    .append(
                        $('<div class="modal-footer"/>')
                        .append(
                            $('<div class="footer-btn">')
                            .attr('onClick',"javascript:location.href='<?php echo APP_DIR; ?>/deposit/?<?php echo $cdnTime; ?>'")
                            .text('購買殺價幣')
                        )
                    )
                )
                .append(
                    $('<div class="msgModalBg">')
                )
            )
            $('.d-close').on('click', function(){
                $('body').removeClass('modal-open');
                $('.msgModalBox').remove();
            })
        }     

        function deductionScode(num) {
            var $reverseArray,$deduction,$sundata,$less;
            $deduction = [];
            console.log('$data_bidArray',$data_bidArray);
            if(num >= 1){                                                          //殺券使用數量
                if($data_bidArray.length > 1){                                     //下標數大於 1標
                    for(var i = 0;i < num;i++){
                        $reverseArray = $data_bidArray.sort(function (a,b) {       //反過來排序
                            return b - a;
                        })
                        var $add_num = $reverseArray.splice(0, 1)
                        $deduction.push($add_num[0]);                   //使用殺價券扣抵的金額
                    }
                   console.log('幣應扣:'+$reverseArray);
                   console.log('券扣除額:'+$deduction);
                }else{                                                             //否則，不用跑回圈 直接取1張殺券的扣除額
                    $deduction = $data_bidArray.splice(0, 1);
                }
                $sundata = sumData($deduction);                                    //殺券總扣除額
                $less = $data_pay_all - $sundata;                                  //減掉殺價券折抵後 應扣的殺幣
            }else{
                $less = $data_pay_all;                                             //殺幣應扣 = 總金額
            }
            return $less;
          
            
        }               //計算殺價券扣抵
        var $type = $data_pay_type;                                  //debug用  $data_pay_type
        var $sp_less;

        console.log( $data_pay_type);

        (function changeRadioTitle($type) {
            bidArray();

            if($data_totalfee_type == 'O'){
                
                $('.Subtotal').parent().text('此商品免費體驗'); // 小計
                $(".show_sum").css('display','none'); // 選擇折抵整區隱藏

                creatSubmitBtn('bid','spoint'); //結帳按鈕
            }else{
                // $data_totalfee_type 'A': 下標金+手續費 ,'B': 下標金 ,'F': 手續費
                switch($type){
                    case 'a':       
                    /* 使用殺價幣 spoint */
                        console.log('case: a')
                        /* 【折抵方案】頁面顯示無票券 */
                        $(".th_ticket").append('<div class="no_ticket">'+
                            '<figure><img src="<?PHP echo APP_DIR; ?>/static/img/price_noticket.png"></figure>'+
                            '<div><span>無票劵</span></div>'
                         )

                         /* 判斷殺幣餘額是否大於【標金+手續費】總和 */
                         // $data_spoint殺幣餘額，$data_pay_all總計(標金+手續費)
                        if($data_spoint >= $data_pay_all){
                            /* 殺價幣全抵 */
                            // $sptbox_txt.text('- '+$data_pay_all+' 點');
                            creatSubmitBtn('bid','spoint');
                        }else{
                            /* 殺價幣不足 */
                            // $sptbox_txt.addClass('red').text('餘額不足');
                            creatSubmitBtn('deposit');
                        }

                        break;
                    case 'b':       
                    //幣+券+S
                        console.log('case: b')

                        //比對超殺券
                        // if($data_scode_num > 0){

                        //     $scode.removeClass('disabled'); // 超殺券顯示(移除disabled) 
                        //     $scbox_txt.text(<?php //echo $scode_num; ?>+' 張');
                        //     $sp_less = deductionScode($data_scode_num);
                        //     if($data_scode_num >= $data_count){
                        //         $scbox_txt.text($data_count+' 張'); 
                        //         $(".sumprice").find('span').text(0);
                        //         creatSubmitBtn('bid','scode');
                        //     }
                        //     if($data_spoint >= $sp_less){    
                        //         $(".sumprice").find('span').text($sp_less);
                        //         creatSubmitBtn('bid','scode');
                        //     }else{                                                  //幣不足
                        //         $sptbox_txt.addClass('red').text('餘額不足');
                        //         creatSubmitBtn('deposit');
                        //     }
                        // }else{
                        //     $scbox_txt.addClass('red').text('張數不足'); // 超殺券不足時目前預設是隱藏
                        // }

                        //比對超殺券
                        if($data_scode_num >= $data_count){
                            $scode.removeClass('disabled'); // 超殺券顯示(移除disabled) 
                            $scbox_txt.text($data_scode_num+' 張');
                            $(".sumprice").find('span').text(0);
                            creatSubmitBtn('bid','scode');
                        }

                        //比對殺券                        
                        if($data_oscode_num > 0){

                            $oscode.removeClass('disabled');


                        }else if($data_scode_num <= 0 && $data_oscode_num < $data_count){                                    

                            $(".th_ticket").append('<div class="no_ticket">'+
                                '<figure><img src="<?PHP echo APP_DIR; ?>/static/img/price_noticket.png"></figure>'+
                                '<div><span>無票劵</span></div>'
                                 )                

                            $oscbox_txt.text('0 張'); // 目前殺券0張時隱藏未顯示
                            $sp_less = deductionScode(0);

                            if($data_spoint >= $sp_less){    
                                $(".sumprice").find('span').text($sp_less);
                                creatSubmitBtn('bid','spoint');
                            }else{                                                  //幣不足
                                $sptbox_txt.addClass('red').text('餘額不足');
                                creatSubmitBtn('deposit');
                            }
                        }

                        break;
                    case 'c':       
                    //幣+券
                    console.log('case: c')

                        if($data_oscode_num > 0){

                            $sp_less = deductionScode($data_oscode_num);
                            $sp_less_text = $sp_less > 0 ? '- '+$sp_less+' 點' : '0';
                            if($data_oscode_num >= $data_count){ 
                                                  //券全抵
                                $oscode.removeClass('disabled');  

                                // $(".sumprice").find('span').text(0);
                                
                                $oscbox_txt.text($data_count+' 張');
                                $sptbox_txt.text($sp_less_text);
                                creatSubmitBtn('bid','oscode');
                            }

                        }else{                                                      //無券

                            $(".th_ticket").append('<div class="no_ticket">'+
                                '<figure><img src="<?PHP echo APP_DIR; ?>/static/img/price_noticket.png"></figure>'+
                                '<div><span>無票劵</span></div>'
                            )

                            $oscbox_txt.text('0 張');
                            $sp_less = deductionScode(0);
                            if($data_spoint >= $sp_less){                       //幣全抵
                                $sptbox_txt.text('- '+$sp_less+' 點');
                                // 20190516

                                $(".sumprice").find('span').text($sp_less);

                                creatSubmitBtn('bid','spoint');
                            }else{                                              //幣不足
                                $sptbox_txt.addClass('red').text('餘額不足');
                                creatSubmitBtn('deposit');
                            }
                        
                        }
                        break;
                    case 'd':       
                    //S+券
                    console.log('case: d')

                        $sptbox_txt.text('不可使用');
                        
                        //靜態顯示
                        
                        if($data_oscode_num >= $data_count){

                            $oscbox_txt.text($data_count+' 張');
                            $oscode.removeClass('disabled');
                        }else{

                            $oscbox_txt.addClass('red').text('張數不足');
                        }
                        
                        if($data_scode_num >= $data_count){

                            $scode.removeClass('disabled');
                            $scbox_txt.text($data_count+' 張');
                        }else{

                            $scbox_txt.addClass('red').text('張數不足');
                        }

                        //預設選取
                        if($data_oscode_num >= $data_count){   
                                         //券全抵
                            creatSubmitBtn('bid','oscode');

                        }else if($data_scode_num >= $data_count){             //S全抵

                                creatSubmitBtn('bid','scode');

                        }else{                                          //券 & S 都不足

                            $(".th_ticket").append('<div class="no_ticket">'+
                                '<figure><img src="<?PHP echo APP_DIR; ?>/static/img/price_noticket.png"></figure>'+
                                '<div><span>無票劵</span></div>'
                            )
                            // $(".s_much").text("無");
                            creatSubmitBtn('loset');
                        }
                        
                        break;
                    case 'e':       
                    //券
                    console.log('case: e')

                        //靜態顯示
                        if($data_oscode_num >= $data_count){
                            $oscode.removeClass('disabled')
                            $oscbox_txt.text($data_count+' 張');
                        }else{
                            $oscbox_txt.addClass('red').text('張數不足');

                            $(".th_ticket").append('<div class="no_ticket">'+
                            '<figure><img src="<?PHP echo APP_DIR; ?>/static/img/price_noticket.png"></figure>'+
                            '<div><span>無票劵</span></div>'
                            )
                        }

                        //預設按鈕
                        if($data_oscode_num >= $data_count){                    //券全抵
                            creatSubmitBtn('bid','oscode');
                        } 
                        else {                                                //券不足
                            creatSubmitBtn('loset');
                            
                        }
                        break;
                }
            }
        }($type));
        
        (function changeBtnEvent($type){
            $('.sele-item input[type=radio]').on('change',function(e){
                var $change = $(this).attr('id');
                //console.log($change);
                switch($type){
                    case 'b':
                        switch($change){
                            case 'oradio':
                            case 'sradio':
                                $osradio.prop('checked', true);
                                $spradio.prop('checked', true);
                                $sradio.prop('checked', false);
                                if($data_oscode_num > 0){
                                    if($data_oscode_num >= $data_count){                   //券全抵
                                        creatSubmitBtn('bid','oscode');
                                    }else if($data_spoint >= $sp_less){                    //幣+券
                                        creatSubmitBtn('bid','oscode');
                                    }else{                                                 //幣不足
                                        creatSubmitBtn('deposit');
                                    }
                                }else{                                                      //無券
                                    if($data_spoint >= $sp_less){                           //幣全抵
                                        creatSubmitBtn('bid','spoint');
                                    }else{                                                  //幣不足
                                        creatSubmitBtn('deposit');
                                    }
                                }
                                break;
                            case 'kradio':
                                $osradio.prop('checked', false);
                                $spradio.prop('checked', false);
                                $sradio.prop('checked', true);
                                if($data_scode_num >= $data_count){                     //S全底
                                    creatSubmitBtn('bid','scode');
                                }else{
                                    creatSubmitBtn('bid','scode');
                                    // creatSubmitBtn('losest');
                                }
                                break;
                        }
                        break;
                    case 'd':
                        switch($change){
                            case 'oradio':
                                $osradio.prop('checked', true);
                                $sradio.prop('checked', false);
                                if($data_oscode_num >= $data_count){                    //券全抵
                                    creatSubmitBtn('bid','oscode');
                                }else{
                                    creatSubmitBtn('loset');
                                }
                                break;
                            case 'kradio':
                                $osradio.prop('checked', false);
                                $sradio.prop('checked', true);
                                if($data_scode_num >= $data_count){                     //S全底
                                    creatSubmitBtn('bid','scode');
                                }else{
                                    creatSubmitBtn('losest');
                                }
                                break;
                        }
                        break;
                }
            })
        }($type));                                              //切換按鈕導向  
    
        $('#oscode_num').on('change keyup', function(){
            var $that = $(this);
            if($that.val() == ''){
                $that.addClass('empty');
            }else{
                $that.removeClass('empty');
            }
        })

        /* */
        $(".side").click(function () { 
            console.log('click');
            // $('body').addClass('modal-open');
            // $(".depositModal").show()
            // .append(
            //         $('<div class="msgModalBg">')
            //     );
            createDepositModal('殺價幣不足')
        });


        /* 選擇折抵框-點擊開啟【折抵方案】頁面 */
        $(".select_discount").click(function () { 
           $(".th_ticket").addClass('on');
        });

        /* 關閉【折抵方案】頁面 */
        $(".th_ticket").find('.icon_back').click(function () { 
           $(".th_ticket").removeClass('on');
        });


        /* 折抵方案【選取票劵】 */
        $(".opt_ticket").click(function () { 

           var ticket_now　=　$(this).index();

           $(".opt_ticket").eq(ticket_now).toggleClass('on').siblings().removeClass('on'); /* 選取的票券加上紅標 */

           
           var program_name =　$(".opt_ticket").eq(ticket_now).find('figcaption>span').text(); /* 票券名稱 */
           var program_much　=　$(".opt_ticket").eq(ticket_now).find('.Txt').text(); /* 票券張數 */

           var price_now = $(".Subtotal").text();　/* 小計 */

            /* 判斷選擇哪種票卷 */
            var sele_scode = $(".sele-scode").hasClass("on");
            var sele_oscode = $(".sele-oscode").hasClass("on");

            if(sele_scode == true){
                /* 選擇超級殺價券 */
                creatSubmitBtn('bid','scode');
                
                $(".s_much").text(program_name + ' 1 張'); /* 選擇折抵的票券與張數 */
                    // $(".sumprice").find('span').text(0);
                $('.total_price').show(); /* 總計框顯示 */
                $('.check_pay>div p.cost_pay').show(); /* 【付款確認】頁折抵金額顯示 */
                $('.select_discount').css('border-bottom','2px solid rgba(238, 238, 238, 0.582)');
            }else if(sele_oscode == true){
                /* 選擇殺價券 */
                creatSubmitBtn('bid','oscode');
                $sp_less = deductionScode($data_oscode_num);
                $sp_less_text = $sp_less > 0 ? '- '+$sp_less+' 點' : '0';
                
                $(".sumprice").find('span').text($sp_less);
                $('.total_price').show();
                $('.check_pay>div p.cost_pay').hide();
                $('.select_discount').css('border-bottom','2px solid rgba(238, 238, 238, 0.582)');

                console.log($sp_less);
                if($data_oscode_num >= $data_count){
                    $(".s_much").text( $data_count+ ' 張');

                }else{
                    $(".s_much").text( $data_oscode_num+ ' 張');
                }
            }else{
        //    console.log('都沒選',sele_scode, sele_oscode);

                creatSubmitBtn('bid','spoint');
                $(".s_much").text("");
                $(".sumprice").find('span').text(price_now);
                $('.total_price').hide();
                $('.check_pay>div p.cost_pay').hide();
                $('.select_discount').css('border-bottom','none');


            }
           
        //    if($(".s_much").text() == ""){
        //        $(".s_much").text(program_name + program_much);

        //        $(".sumprice").find('span').text(0);
        //        $('.total_price').show();
        //        $('.select_discount').css('border-bottom','2px solid rgba(238, 238, 238, 0.582)');
        //     }else{
        //         creatSubmitBtn('bid','spoint');
        //         // console.log('price_now',price_now)
        //         $(".s_much").text("");
        //         // if(program_much =="超級殺價劵"){
        //         //     creatSubmitBtn('bid','scode');
        //         // }else if(program_much =="殺價劵"){
        //         //     creatSubmitBtn('bid','oscode');
        //         // }
        //         $(".sumprice").find('span').text(price_now);
        //         $('.total_price').hide();
        //         $('.select_discount').css('border-bottom','none');

                

        //    }

        });



        // 開啟付款確認
        $(".btn_nextpage").click(function () { 
           $(".check_pay").addClass('on');
           var sumprice=$(".sumprice").find('span').text();
        //    console.log(sumprice);
           $(".sum_payment").text(sumprice+"枚");
           console.log($("#topay").length)
           if($("#topay").length <= 0){
            creatSubmitBtn('bid','spoint')
           }



        });
        // 關閉付款確認
        $(".check_pay").find('.icon_back').click(function () { 
           $(".check_pay").removeClass('on');
        });

    }); 
 
</script>
