<?php
$cdnTime = date("YmdHis");
$tpfooter=_v('tpfooter');

$footer=_v('footer');
if($footer=='N')
return ;

?>
</div>

<?php if ($tpl->page_action !== 'login' && $tpl->page_action !== 'logout' && $tpl->page_action !== 'loginto') { ?>
<div class="sajaFooterNavBar fixed-bottom" >
    <div class="barItems d-flex align-items-center justify-content-around">
        <div class="barItem home">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/?<?php echo $cdnTime; ?>'" data-transition="slide" class="<?php if ($tpl->page_action == "home") {?>weui-bar__item--on<?php } ?>">
                <div class="footerBarIcon">
                    <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/home_btn_<?php if ($tpl->page_action == "home") {?>1<?php }else{ ?>0<?php } ?>.png" alt="首頁">
                </div>
                <span class="footerBarLabel">首　頁</span>
            </a>
        </div>
        <!-- div class="barItem">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/storeproduct/?flash=Y&<?php echo $cdnTime; ?>'" data-transition="slide" class="<?php if ($tpl->page_action == "storeproduct") {?>weui-bar__item--on<?php } ?>">
                <div class="footerBarIcon">
                    <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/prod_saja_<?php if ($tpl->page_action == "storeproduct") {?>1<?php }else{ ?>0<?php } ?>.png" alt="閃殺商品">
                </div>
                <span class="footerBarLabel">閃殺商品</span>
            </a>
        </div -->
    <!--
            <div class="barItem">
                <a href="<?php echo BASE_URL.APP_DIR; ?>/mall/?channelid=<?php echo $_GET['channelid']; ?>&epcid=1&<?php echo $cdnTime; ?>" data-transition="slide" class="weui-tabbar__item <?php if ($tpl->page_footer == "mall") {?>weui-bar__item--on<?php } ?>" ></a>
            </div>
    -->
        <div class="barItem">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/storebonuslist/?<?php echo $cdnTime; ?>'" data-transition="slide" class="<?php if ($tpl->page_action == "storebonuslist") {?>weui-bar__item--on<?php } ?>">
                <div class="footerBarIcon">
                    <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/shark_<?php if ($tpl->page_action == "storebonuslist") {?>1<?php }else{ ?>0<?php } ?>.png" alt="交易紀錄">
                </div>
                <span class="footerBarLabel">交易紀錄</span>
            </a>
        </div>
    <?php if (empty($_SESSION['auth_id'])){ ?>
        <div class="barItem">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/login/?<?php echo $cdnTime; ?>'" data-transition="slide" class="<?php if ($tpl->page_action == "about") {?>weui-bar__item--on<?php } ?>" >
                <div class="footerBarIcon">
                    <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/me_<?php if ($tpl->page_action == "about") {?>1<?php }else{ ?>0<?php } ?>.png" alt="商戶專區">
                </div>
                <span class="footerBarLabel">商戶專區</span>
            </a>
        </div>
    <?php }else{ ?>
        <div class="barItem">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/about/?<?php echo "t=".$cdnTime; ?>'" data-transition="slide" class="<?php if ($tpl->page_action == "about") {?>weui-bar__item--on<?php } ?>" >
                <div class="footerBarIcon">
                    <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/me_<?php if ($tpl->page_action == "about") {?>1<?php }else{ ?>0<?php } ?>.png" alt="商戶專區">
                </div>
                <span class="footerBarLabel">商戶專區</span>
            </a>
        </div>
    <?php } ?>
    </div>
</div>


<!-- 2018/10/15 舊檔 -->
<!--
<div style="background-color:#FFFFFF;height:49px;">  </div>
<div class="nav" >
      <div class="weui-tabbar" style="background-color:#FFFFFF">
        <a href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/?<?php echo $cdnTime; ?>" data-transition="slide" class="weui-tabbar__item <?php if ($tpl->page_action == "home") {?>weui-bar__item--on<?php } ?>" >
          <div class="weui-tabbar__icon">
            <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/home_btn_<?php if ($tpl->page_action == "home") {?>1<?php }else{ ?>0<?php } ?>.png"  alt="首	頁">
          </div>
          <p class="weui-tabbar__label">首	頁</p>
        </a>
        <a href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/storeproduct/?flash=Y&<?php echo $cdnTime; ?>" data-transition="slide" class="weui-tabbar__item <?php if ($tpl->page_action == "storeproduct") {?>weui-bar__item--on<?php } ?>">
          <div class="weui-tabbar__icon">
            <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/prod_saja_<?php if ($tpl->page_action == "storeproduct") {?>1<?php }else{ ?>0<?php } ?>.png" alt="闪殺商品">
          </div>
          <p class="weui-tabbar__label">闪殺商品</p>
        </a>
		<a href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/storebonuslist/?<?php echo $cdnTime; ?>" data-transition="slide" class="weui-tabbar__item <?php if ($tpl->page_action == "storebonuslist") {?>weui-bar__item--on<?php } ?>">
          <div class="weui-tabbar__icon">
            <img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/shark_<?php if ($tpl->page_action == "storebonuslist") {?>1<?php }else{ ?>0<?php } ?>.png" alt="交易纪录">
          </div>
          <p class="weui-tabbar__label">交易纪录</p>
        </a>
		<?php if (empty($_SESSION['auth_id'])){ ?>
		<a href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/login/?<?php echo $cdnTime; ?>" data-transition="slide" class="weui-tabbar__item <?php if ($tpl->page_action == "about") {?>weui-bar__item--on<?php } ?>" >
		<?php }else{ ?>
		<a href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/about/?<?php echo "t=".$cdnTime; ?>" data-transition="slide" class="weui-tabbar__item <?php if ($tpl->page_action == "about") {?>weui-bar__item--on<?php } ?>" >
		<?php } ?>
			<div class="weui-tabbar__icon">
				<img src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/me_<?php if ($tpl->page_action == "about") {?>1<?php }else{ ?>0<?php } ?>.png" alt="商户专區">
			</div>
          <p class="weui-tabbar__label">商户专區</p>
        </a>
      </div>
</div>
-->
<!-- 2018/10/15 舊檔/ -->

<?php } ?>
