<?php 
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$issues_category = _v('category_list');
?>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/croppie.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/exif.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/megapix-image.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/weui_loading.js"></script>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/croppie.css" rel="stylesheet"/>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/weui_loading.css" rel="stylesheet"/>

<!--  Bootstrap 4.1.1  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--  lazyload 2.0.0 懶載入  -->
<script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-beta.2/lazyload.js"></script>

<form method="post" action="" id="contactform" name="contactform">		
	<div id="issues_add" class="issues_add">
		<!--  輸入發問內容  -->
		<div class="saja_lists_box">
			<div class="list-group d-flex align-items-center">
				<div class="label" for="project">分類項目</div>
				<select name="uicid" id="uicid" onchange="checksend(this.value);">
					<option value="" selected>請選擇</option>
					<?php foreach($issues_category['record'] as $rk => $rv) { ?>				
					<option value="<?php echo $rv['uicid']; ?>"><?php echo $rv['name']; ?></option>
					<?php } ?>                    
				</select>
					
			</div>
			<!-- 文字輸入框 -->
			<div class="list-group">
				<div class="textarea-box">
					<textarea name="content" id="content" placeholder="請描述你遇到的問題" onkeyup="checksend();" ></textarea>
				</div>
			</div>
		</div>
		
		<!--  上傳圖片區  -->
<!--
		<div class="saja_lists_box">
			<div class="list-group">
				<div class="upload-box">
					<div class="labelBlock">上傳照片(限制3張)</div>
					<div class="img-group d-flex">
						<div id="pid1" class="img-list d-none">
							<img id="preview_logo1" name="preview_logo1" src="http://www.saja.com.tw/site/images/site/product/121a8901e96cc3d806fa68b22b88c5b7.jpg" alt="">
						</div>
						
						<div id="pid2" class="img-list d-none">
							<img id="preview_logo2" name="preview_logo2" src="http://www.saja.com.tw/site/images/site/product/121a8901e96cc3d806fa68b22b88c5b7.jpg" alt="">
						</div>
						
						<div id="pid3" class="img-list d-none">
							<img id="preview_logo3" name="preview_logo3" src="http://www.saja.com.tw/site/images/site/product/121a8901e96cc3d806fa68b22b88c5b7.jpg" alt="">
						</div>
						
						<div id="pid-add" class="img-list add-btn d-flex justify-content-center align-items-center" onclick="$('#ispid').trigger('click'); return false;">
							<div class="icon">
								<img src="<?PHP echo APP_DIR; ?>/static/img/add-pic.png" alt="">
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
-->
		<!-- 圖檔內容-->	
<!--
		<div style="visibility:hidden">
			<input name="ispid" id="ispid" class="uploader__input" type="file" accept="image/*" placeholder="" >
		</div>
-->
    
        <!--  上傳圖片區  -->
        <div class="saja_lists_box">
            <div class="list-group">
                <div class="upload-box">
                    <div class="labelBlock">上傳照片(限制3張)</div>
                    <div class="img-group">
                        <div class="content-img d-flex">
                            <ul class="content-img-lists">
                                <!-- <li class="content-img-list-item"><img src="https://www.baidu.com/img/bd_logo1.png" alt=""><a class="delete-btn"><i class="ico-delete"></i></a></li> -->
                            </ul>
                            <div class="file d-flex align-items-center justify-content-center">
                                <i class="ico-plus"></i><input type="file" name="file" multiple="multiple" accept="image/*" id="upload" >
                            </div>
                        </div>
                    </div>
                    <div class="upload-tips">最多上傳3張，單張1MB限制</div>
                    <div class="info"></div>
                </div>
            </div>
        </div>
		
		<!--  彈窗-驗證訊息  -->
        <div class="optionsModal">
            <div class="modal-content">
                <div class="modal-body">
                    <ul>
                        <li id="previewBtn">查看圖片</li>
                        <li id="deleteBtn" class="red">刪除</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <a class="close_btn">取消</a>
                </div>
            </div>
        </div>
        <div class="optionsModalBg"></div>
		
		<!-- 確認按鈕 -->
		<div class="footernav">
			<div class="footernav-button">
				<button id="issend" name="issend" type="button" class="ui-btn ui-btn-a ui-corner-all" onclick="issues_send();" disabled>提出問題</button>
			</div>
		</div>
		
		<input type="hidden" id="userid" name="userid" value="<?php echo $_SESSION['auth_id'];?>">
<!--
		<input type="hidden" id="thumbnail1" name="thumbnail1">
		<input type="hidden" id="thumbnail2" name="thumbnail2">
		<input type="hidden" id="thumbnail3" name="thumbnail3">
-->
		<input type="hidden" id="kind" name="kind" value="insertt">
		<input type="hidden" id="pidcount" name="pidcount" value="1">

	</div>
</form>
<div class="cModalBox"></div>
<!--
<div class="mModalBox"></div>
<div class="lModalBox"></div>
-->
<div id="actions" style="display:none;">
    <div class="pic-box">
        <div id="new_logo" style="text-align: center;"></div>
    </div>
    <canvas id="cvs" style="display:none"></canvas>
    <canvas id="cvs-wfp" style="display:none"></canvas>

    <div class="btn-box">
        <div class="rotate-group mx-auto d-flex">
            <button onclick="rotate_left()">向左旋轉90度</button>
            <button onclick="rotate_right()">向右旋轉90度</button>
        </div>
        <div class="submit-group mx-auto">
            <button onclick="save_pic()">儲存</button>
            <button onclick="cancel_edit()">取消</button>
        </div>
    </div>
</div>
<script>
    //調整標題寬度一致
    var options = {
        dom: {
            ul: '.saja_lists_box',
            li: '.list-group',
            label: '.label',
            input: 'input',
            select: 'select'
        }
    }
    $(function(){
        function changeWidth(dom) {
            
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];
            
            $liW = parseInt($selfLi.width());                         //取得li總寬度   
            
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                $selfInput.attr('style','');
                $selfSelect.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                    }
                })
                $selfSelect.each(function(){
                    if($selfSelect.length > 1){                 
                        $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                        
                    }else{
                        $(this).width($remain);
                    }
                })
            })
            
            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
        }
        $(options.dom.ul).each(function(){
            changeWidth($(this));
        })

        //尺寸改變重新撈取寬度
        $(window).resize(function(){
            $(options.dom.ul).each(function(){
                changeWidth($(this));
            })
        })

    })
	
//	//绑定input change事件
//	$("#ispid").unbind("change").on("change",function(){
//
//		lnv.pageloading();
//		var file = document.querySelector('#ispid').files[0];
//		if(file){
//			//验证图片文件类型
//			if(file.type && !/image/i.test(file.type)){
//				return false;
//			}
//			var reader = new FileReader();
//			reader.onload = function(e){
//				//readAsDataURL后执行onload，进入图片压缩逻辑
//				//e.target.result得到的就是图片文件的base64 string
//				render(file,e.target.result,1);
//			};
//			//以dataurl的形式读取图片文件
//			reader.readAsDataURL(file);
//		}
//	});
//
//	//定义照片的最大高度
//	var MAX_HEIGHT = 480;
//	var render = function(file,src,num){
//		EXIF.getData(file,function(){
//			//获取照片本身的Orientation
//			var orientation = EXIF.getTag(this, "Orientation");
//			var image = new Image();
//			image.onload = function(){
//				var cvs = document.getElementById("cvs");
//				var w = image.width;
//				var h = image.height;
//				//计算压缩后的图片长和宽
//				if(h>MAX_HEIGHT){
//					w *= MAX_HEIGHT/h;
//					h = MAX_HEIGHT;
//				}
//				//使用MegaPixImage封装照片数据
//				var mpImg = new MegaPixImage(file);
//				//按照Orientation来写入图片数据，回调函数是上传图片到服务器
//				mpImg.render(cvs, {maxWidth:w,maxHeight:h,orientation:orientation}, sendImg);
//			};
//			image.src = src;
//		});
//	};
//
//	//上传图片到服务器
//	var sendImg = function(){
//		var cvs = document.getElementById("cvs");
//		//调用Canvas的toDataURL接口，得到的是照片文件的base64编码string
//		var data = cvs.toDataURL("image/png");
//		//base64 string过短显然就不是正常的图片数据了，过滤の。
//		if(data.length<48){
//			console.log("data error.");
//			return;
//		}
//
//		var formData = new FormData();
//		//formData.append('pic', reader.result );
//		formData.append('pic', data );
//		formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
//		formData.append('pic_name','issuestemp');
//		formData.append('filedir','site/issues/temp/');
//
//		$.ajax(
//			{
//				url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
//				type : 'POST',
//				data : formData,
//				processData: false,  // tell jQuery not to process the data
//				contentType: false,  // tell jQuery not to set contentType
//				success : function(data)
//				{
//					load_img();
//				}
//			}
//		);
//	};
//
//
//	function load_img()
//	{
//	  destory_img();
//
//	  var el = document.getElementById('new_logo');
//		vanilla = new Croppie(el, {
//		viewport: { width: 196, height: 196 },
//		boundary: { width: 300, height: 300 },
//		showZoomer: true,
//		enableOrientation: true
//	  });
//
//	  vanilla.bind({
//		  url: '<?php echo BASE_URL.APP_DIR;?>/images/site/issues/temp/_'+<?php echo $_SESSION['auth_id'];?>+'_issuestemp.png?t=' + new Date().getTime(),
//	  });
//
//	  document.getElementById('issues_add').style.display = "none";
//	  document.getElementById('actions').style.display = "inline";
//	  lnv.destroyloading();
//	}
//
//	function destory_img()
//	{
//	  document.getElementById('new_logo').innerHTML="";
//	}
//
//	function cancel_edit()
//	{
//	  location.reload();
//	}
//
//	function page_reload()
//	{
//	  location.reload();
//	}
//
//	function rotate_right()
//	{
//	   vanilla.rotate(90);
//	}
//
//	function rotate_left()
//	{
//	   vanilla.rotate(-90);
//	}
//
//	function save_pic()
//	{
//		var pic = vanilla.result('canvas','viewport');
//		console.log(pic);
//
//		pic.then(function(value){
//		console.log(value);
//		
//		var count = document.getElementById('pidcount').value;
//		var formData = new FormData();
//		formData.append('pic',value);
//		formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
//		formData.append('pic_name','<?php echo md5($cdnTime."issues");?>_'+count);
//		formData.append('filedir','site/issues/');
//		//console.log(count);
//
//		$.ajax({
//			url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
//			type : 'POST',
//			data : formData,
//			processData: false,  // tell jQuery not to process the data
//			contentType: false,  // tell jQuery not to set contentType
//			success : function(data)
//				{
//					var tf = document.getElementById('thumbnail'+count).value="_<?php echo $_SESSION['auth_id'];?>_<?php echo md5($cdnTime."issues");?>_"+count+".png";
//					//console.log(tf);
//					// change_db_data();
//					// page_reload();
//
//					document.getElementById('issues_add').style.display = "block";
//					document.getElementById('actions').style.display = "none";
//					
//					document.getElementById("pid"+count).classList.remove('d-none');
//					document.getElementById("preview_logo"+count).style.display = "";
//					document.getElementById("preview_logo"+count).src = "<?php echo BASE_URL.APP_DIR;?>/images/site/issues/_<?php echo $_SESSION['auth_id'];?>_<?php echo md5($cdnTime."issues");?>_"+count+".png?t=" + new Date().getTime();
//					if (parseInt(count) < 3){
//						document.getElementById('pidcount').value = (parseInt(count)+1);
//					}else{
//						document.getElementById('pid-add').classList.remove('d-flex');
//						document.getElementById('pid-add').classList.add('d-none');
//					}
//				}
//			});
//		});
//	}
//
//	function issues_send() {
//		var category_value = $('select[name="uicid"] option:selected').val();
//		var content_value = $("#content").val();
//		//送出前檢查資料是否正常
//		if ((category_value != "") && (content_value != "")){
//			$.ajax({
//				url: "<?php echo BASE_URL.APP_DIR;?>/issues/issues_add/?web=Y",
//				data: $('#contactform').serialize(),
//				type:"POST",
//				dataType:'text',
//				success: function(msg){
//				  var obj = jQuery.parseJSON(msg);
//				  alert( obj.retMsg );
//				  if (obj.retCode == 1){
//					location.href='<?php echo BASE_URL.APP_DIR; ?>/issues/';
//				  }
//				},
//				error:function(){
//				  var obj = jQuery.parseJSON();
//				  alert( obj.retMsg );
//				}
//			});
//		}else{
//			if (category_value == ""){
//				alert("請選擇分類項目 !!");
//			}else{
//				alert("請填寫遇到的問題內容 !!");	
//			}
//			return;
//		}
//	}
//
//	function checksend(){
//		var category_value = $('select[name="uicid"] option:selected').val();
//		var content_value = $("#content").val();
//		//console.log("category_value => "+ category_value +" content_value => "+ content_value);
//		
//		if ((category_value != "") && (content_value != "")){
//			$('#issend').removeAttr("disabled");
//		}
//	}	
    
    //圖片上傳功能
    var updataOptions = {
        dom: {
            parent: '.upload-box',
            imgUl: '.content-img-lists',
            imgLi: '.content-img-list-item'
        },
        file: {
            fileBtn: '.file',
            input: '#upload'
        },
        width: {
            boxNum: 3,                                                              //方框數量
            spacingVar: 0.064                                                       //方框間距 (為父層的幾倍)
        },
        canvas: {
            canvasW: 400,
            canvasH: 400,
        },
        optionsModal: {
            modal: '.optionsModal',
            modalbg: '.optionsModalBg'
        }
    };
    var $parent = $(updataOptions.dom.parent),                                      //父層 .upload-box
        $ul = $parent.find(updataOptions.dom.imgUl),                                //ul .content-img-lists
        $fileBtn = $parent.find(updataOptions.file.fileBtn),                        //上傳圖片外框
        $input = $fileBtn.find(updataOptions.file.input),                           //上傳圖片主要input
        $info = $('.info');
    var $parentBoxW,$boxW,$spacing,$li;
    

    var imgFile = []; //文件流
    var imgSrc = []; //圖片路徑
    var imgName = []; //圖片名字

    
    
/*-----------------------------------  事件 -----------------------------------*/
    $(function(){
        //先隱藏上傳Box，等寬度計算
        $fileBtn.removeClass('d-flex');
        $fileBtn.hide();
        
        //創建畫布
        var canvas = document.createElement('canvas');
        canvas.width = updataOptions.canvas.canvasW;
        canvas.height = updataOptions.canvas.canvasH;
        var context = canvas.getContext('2d');
    });

    $(window).on('load resize',function(){
        recalculateW();                     //改變圖片框寬度及間距
        recalculateImg();                   //改變框內圖片大小
        
        //寬度計算完成，顯示上傳Box
        $fileBtn.addClass('d-flex');
        $fileBtn.show();
    })

/*-----------------------------------  事件 - 圖片上傳  -----------------------------------*/
    $('#upload').on('change',function(){
console.log('點選上傳按鈕')
        var imgSize = this.files[0].size;  //b  (圖片尺寸)

        var imgBox = '.content-img-lists';
        var fileList = this.files;
        
        if(imgSize>1024*1024*1){//1M
            return alert("上傳圖片不能超過1M");
        }
        if(this.files[0].type != 'image/png' && this.files[0].type != 'image/jpeg' && this.files[0].type != 'image/gif'){
            return alert("圖片上傳格式不正確");
        }

        if((imgSrc.length + fileList.length) <=3){
            //console.log(imgSrc.length);
            //console.log(fileList.length);
            for(var i = 0; i < fileList.length; i++) {
              //console.log(fileList[i])
              var imgSrcI = getObjectURL(fileList[i]);
console.log(imgSrcI);
              imgName.push(fileList[i].name);
              imgSrc.push(imgSrcI);
              imgFile.push(fileList[i]);
            }
        }else{
            return alert("最多只能上傳3張圖片");
        }
//console.log(imgName)
//console.log(imgSrc)
//console.log(imgFile)
        if((imgSrc.length + fileList.length)>=3){//隱藏上傳按鈕
            
            $('.content-img .file').hide();
        }
        addNewContent(imgBox);
        this.value = null;//解决无法上传相同图片的问题
    })    
    
    
    //點選圖片 彈出選項視窗
    	$(".content-img-lists").on("click",'.content-img-list-item',function(){
	    	var index = $(this).attr("index"),
                $modal = $(updataOptions.optionsModal.modal),
                $modalClose = $modal.find('.close_btn'),
                $modalBg = $(updataOptions.optionsModal.modalbg);
			$modal.show().attr('index',index);
            $modalBg.show();
          
            $modalBg.on("click",function(){
              $modal.hide();
              $modalBg.hide();
            })
            $modalClose.on("click",function(){
              $modal.hide();
              $modalBg.hide();
            })
	  });
    
    
    // 	// 滑鼠經過顯示刪除按鈕
// 	$('.content-img-list').on('mouseover','.content-img-list-item',function(){
// 		$(this).children('a').removeClass('hide');
// 	});
// 	// 滑鼠離開隱藏刪除按鈕
// 	$('.content-img-list').on('mouseleave','.content-img-list-item',function(){
// 		$(this).children('a').addClass('hide');
// 	});
        // 單個圖片刪除
        $(updataOptions.optionsModal.modal + ' #deleteBtn').on("click",function(){
                var index = $(this).parents(updataOptions.optionsModal.modal).attr("index");
                imgSrc.splice(index, 1);
                imgFile.splice(index, 1);
                imgName.splice(index, 1);
                var boxId = ".content-img-lists";
                addNewContent(boxId);
                if(imgSrc.length<4){//顯示上傳按鈕
                    $('.content-img .file').show();
                }
                //關閉彈跳視窗
                $(updataOptions.optionsModal.modalbg).click();
          });
    

        //提交請求
        $('#btn-submit-upload').on('click',function(){
            // FormData上传图片
            var formFile = new FormData();
            // formFile.append("type", type); 
            // formFile.append("content", content); 
            // formFile.append("mobile", mobile); 
            // 遍历图片imgFile添加到formFile里面
            $.each(imgFile, function(i, file){
                formFile.append('myFile[]', file);
            });
            console.log(imgFile)
         //    $.ajax({
         //        url: 'http://zhangykwww.yind123.com/webapi/feedback',
         //        type: 'POST',
         //        data: formFile,
         //        async: true,  
         //        cache: false,  
         //        contentType: false, 
         //        processData: false, 
         //        // traditional:true,
         //        dataType:'json',
         //        success: function(res) {
         //            console.log(res);
         //            if(res.code==0){
         //                alert("您的意见反馈已提交，感谢您的宝贵意见")
         //    //             $("#adviceContent").val("");
            // 			// $("#contact").val("");
         //            }else{
         //                alert(res.message);
         //                $('.content-img .file').show();
         //                $("#adviceContent").val("");
         //                $("#cotentLength").text("0/240");
            // 			$("#contact").val("");
            // 			imgSrc = [];imgFile = [];imgName = [];
                        // addNewContent(".content-img-lists");

                        // $('.success-tips').removeClass('hide');
               //      	var time = 3;
               //      	var tipTimer = setInterval(function(){
               //      		time--;
               //      		if(time==0){
               //      			$('.success-tips').addClass('hide');
               //      			$('.success-tips .timer').text('3s');
               //      			clearInterval(tipTimer);
               //      		}else{
               //      			$('.success-tips .timer').text(time+'s');
               //      		}
               //      	},1000);
         //            }
         //        }
         //    })
        });
    
    
/*-----------------------------------  function  -----------------------------------*/    
    
    //改變圖片框寬度及間距
    function recalculateW() {
//console.log('*recalculateW '+'改變圖片框寬度及間距');
        //計算各物件寬度
        $parentBoxW = $parent.width();                                                                             //父層寬度
        $spacing = $parentBoxW * updataOptions.width.spacingVar;                                                   //存放圖片的Box間距
        $boxW = ($parentBoxW - ($spacing * (updataOptions.width.boxNum - 1))) / updataOptions.width.boxNum;        //方框大小
        
        //存放圖片的Box li & 上傳按鈕 li的寬度
        //每次改變時重新抓取畫面中的li
        $li = $ul.find(updataOptions.dom.imgLi);
        $li.css({'width':$boxW,'height':$boxW});
//console.log($li);
        $fileBtn.css({'width':$boxW,'height':$boxW});

//console.log($fileBtn);
        //上傳li的間距變化
        if(imgSrc.length > 0){
            $fileBtn.css('margin-left', $spacing);
        }else{
            $fileBtn.css('margin-left', '0');
        }
    }
    
    //改變框內圖片大小
    function recalculateImg() {
//console.log('*recalculateImg '+'改變框內圖片大小');
        $li = $ul.find(updataOptions.dom.imgLi);                //每次改變時重新抓取畫面中的li
        var $liImg = $li.find('img');
        $liImg.each(function(i,item){
            var $that = $(this);
            var realWidth;//真實的寬度
            var realHeight;//真實的高度
            
            $("<img/>")
                .attr("src", $that.attr("src"))
                .on('load',function() {
                    realWidth = this.width;
                    realHeight = this.height;
                    var HeightWidth = realHeight / realWidth; //設置高寬比
                    var WidthHeight = realWidth / realHeight; //設置寬高比

                    if (realWidth > realHeight) {
                        var offsetL = (($boxW * WidthHeight) - $boxW) / 2 * (-1);
                        $that.css({'height':$boxW,'width':$boxW * WidthHeight,'margin-left':offsetL})
                    }else if (realHeight > realWidth) {
                        var offsetT = (($boxW * HeightWidth) - $boxW) / 2 * (-1);
                        $that.css({'width':$boxW,'height':$boxW * HeightWidth,'margin-top':offsetT});
                    }else{
                        $that.css({'height':$boxW,'width':$boxW});
                    }
//console.log('realWidth '+realWidth);
//console.log('realHeight '+realHeight);
                })
        })
    }

    //删除
    function removeImg(obj, index) {
        imgSrc.splice(index, 1);
        imgFile.splice(index, 1);
        imgName.splice(index, 1);
        var boxId = ".content-img-lists";
        addNewContent(boxId);
    }

    //圖片展示
    function addNewContent(obj) {
        //console.log(imgSrc)
        recalculateW();
        $(obj).html("");
        for(var i = 0; i < imgSrc.length; i++) {
            //var oldBox = $(obj).html();
            $(obj).append(
                $('<li/>')
                .addClass('content-img-list-item')
                .css({'width':$boxW,'height':$boxW+'(i>0)?,"margin-left":'+$spacing+':""'})
                .attr('index',i)
                .append(
                    $('<img/>')
                    .addClass('pic')
                    .attr('src',imgSrc[i])
                )
            )
        }
        recalculateImg();
    }

    //建立一個可存取到該file的url
    function getObjectURL(file) {
        var url = null ;
        if (window.createObjectURL!=undefined) { // basic
            url = window.createObjectURL(file) ;
        } else if (window.URL!=undefined) { // mozilla(firefox)
            url = window.URL.createObjectURL(file) ;
        } else if (window.webkitURL!=undefined) { // webkit or chrome
            url = window.webkitURL.createObjectURL(file) ;
        }
        return url ;
    }
</script>

<script>
    
    $(function(){
        esAOptions.vClass.vModal.show = true;
//        esAOptions.imgClass.cModal.show = true;
        esAOptions.userid = '<?php echo $_SESSION['auth_id']; ?>';
    })
    
</script>
<!--
  <ins class="clickforceads" style="display:inline-block;width:320px;height:480px;left:0;top:0;" data-ad-zone="8628"></ins>
<script async type="text/javascript" src="//cdn.doublemax.net/js/init.js"></script>
-->

