<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");

function is_weixin() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        // error_log("Weixin!");
        return true;
    }
    // error_log("Not Weixin!");
    return false;
}
$header = _v('header');
$footer=_v('footer');
$style = "";

if ($tpl->page_id == "site_oscode_home" ){
    $style = "background-color:#EFEFEF;"; 
}
$APP = _v('APP');

?>
<!DOCTYPE html>
<html prefix="og:http://ogp.me/ns# fb:http://ogp.me/ns/fb#" >
    <head>
        <?php include_once 'meta.php'; ?>
        <title><?php if (!empty($tpl->title)){ echo $tpl->title; } else { echo $tpl->page_header;} ?></title>
        <?php include_once 'cssjs.php'; ?>
        
        <!–[if IE]>
        <script src="<?PHP echo BASE_URL.APP_DIR; ?>/javascript/html5.js"></script>
        <![endif]–>
    </head>
    <body style="height:100%" ontouchstart>
        <div data-role="page" id="<?php echo $tpl->page_id.$cdnTime; ?>" style="<?php echo $style; ?>" >
        <!--  data-role="page"開頭，panel結尾  -->
            <!-- 
                -----20190122 新----
                共用 es-content
                有預留header高度 hasHeader
                有預留footer高度 hasFooter
            -->
            <?php
                $page_class="";
                if (($tpl->page_footer == 'site' && $tpl->page_action == 'home') || ($APP == 'Y')) {
                    $page_class="es-content hasFooter";
                } elseif ($tpl->page_footer == 'broadcast') {
                    $page_class="broadcast-content";
                } elseif ($tpl->page_footer == 'enterprise' && ($tpl->page_action == 'login' || $tpl->page_action == 'logout' || $tpl->page_action == 'loginto')) {
                    if(!is_weixin() && $header!=='N'){
                        $page_class="es-content hasHeader hasFooter";
                    }else{
                        $page_class="es-content hasFooter";
                    }
                } elseif ($tpl->page_footer == 'enterprise' && ($tpl->page_action !== 'login' && $tpl->page_action !== 'logout')) {
                    if(!is_weixin() && $header!=='N'){
                        $page_class="es-content hasHeader hasFooter";
                    }else{
                        $page_class="es-content hasFooter";
                    }
                } elseif (($tpl->page_action == 'home' || $tpl->page_action == 'getProductSearch') && $tpl->page_footer !== 'history' && $footer!=='N') {
                    if(!is_weixin() && $header!=='N'){
                        $page_class="es-content hasHeader hasFooter";
                    }else{
                        $page_class="es-content hasFooter";
                    }
                } else {
                    if(!is_weixin() && $header!=='N'){
                        $page_class="es-content hasHeader";
                    }else{
                        $page_class="es-content";
                    }
                }
            ?>
            <div class="<?php echo $page_class ?>" >
            <!--  class="first-content"開頭，footer結尾  -->
            <!--  first-content 有預留footerBar高度，other-content 內頁無footerBar無預留高度 -->
