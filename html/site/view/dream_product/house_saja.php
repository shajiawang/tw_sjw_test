<?php
/*20201204 停用 */
$product = _v('product');
$sajabid = _v('sajabid');
$status = _v('status');
$meta=_v('meta');
$canbid=_v('canbid');
$cdnTime = date("YmdHis");
$options = _v('options');
$type=_v('type');
$paydata=_v('paydata');
$oscode_num=_v('oscode_num');
$scode_num=_v('scode_num');
$scode_use_num=_v('scode_use_num');
$bidget_count=_v('bidget_count');
$retCheck=_v('retCheck');
if(!isset($canbid)) {
    $canbid=$_REQUEST['canbid'];
}
if(empty($canbid)) {
    $canbid='Y';
}

//閃殺商品
//$flash_img = ($product['is_flash']=='Y') ? 'class="flash_img"' : '';
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
if(empty($product) || empty($product['productid'])) {
?>
<p>
<div align="center" style="color:blue;font-size:30px;">無此商品 !!</div>
<script>
    window.location.href = "/site/product";
</script>
<?php
    exit;
}
?>
<!-- 此頁css -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/saja.css">
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/croppie.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/exif.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/megapix-image.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/weui_loading.js<?php echo "?_t=".date("YmdHis"); ?>"></script>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/croppie.css" rel="stylesheet"/>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/weui_loading.css" rel="stylesheet"/>

<div class="productSaja">
    <!-- <?php echo $_SESSION['auth_id']; ?> -->
    <!-- script src="<?PHP echo BASE_URL.APP_DIR; ?>/static/js/socket.io.js"></script -->
    <div class="swipe-navbar-item" ></div>
    <div class="productSaja-imgbox">

        <!--div>
        <?php if (!empty($product['srid']) ){
            if($product['srid']=='any_saja'){
            }elseif($product['srid']=='never_win_saja'){ ?>
            <div id="tagBox_qualifications" title="新手限定(未中標過者)" >限新手</div>
        <?php } elseif($product['srid']=='le_win_saja'){ ?>
            <div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以下)" >限新手</div>
        <?php } elseif($product['srid']=='ge_win_saja'){ ?>
            <div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以上)" >限老手</div>
        <?php }
        }//endif;
        ?>

        <?php if ((float)$product['saja_fee'] == 0.00){ ?>
        <!-- div id="tagBox_free" title="免費(出價免扣殺價幣)">免費</div>
        <?php }//endif; ?>
        </div -->

        <?php
        if(!empty($product['thumbnail2'])) {
            $img_src = $product['thumbnail2'];
        } elseif (!empty($product['thumbnail_url'])) {
            $img_src = $product['thumbnail_url'];
        }
        if(!empty($product['thumbnail3'])) {
            $img_src2 = $product['thumbnail3'];
        } else {
            $img_src2 = "";
        }
        if(!empty($product['thumbnail4'])) {
            $img_src3 = $product['thumbnail4'];
        } else {
            $img_src3 = "";
        }
        ?>

        <!-- 判斷是否為宅公益 -->
        <?php if ($product['productid'] == '9685' || $product['productid'] == '9684' || $product['productid'] == '9688'){ ?>
            <div class="home-welfare_tag">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/heart_icon.png">
                <p>宅公益</p>
            </div>

        <?php } ?>

        <div class="productSaja-imgbox">
            <!-- 圖片只有一張時 停用幻燈片滑動 -->
            <div class="<?php if (!empty($img_src2) || !empty($img_src3)){ ?>flexslider<?php } ?> clearfix sajaprod-img">
                <ul class="slides d-flex">
                    <li class="d-flex align-items-center">
                        <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>">
                    </li>
                    <?php if (!empty($img_src2)){ ?>
                    <li class="d-flex align-items-center">
                        <img class="img-fluid" id="2" src="<?php echo $img_src2; ?>">
                    </li>
                    <?php } ?>
                    <?php if (!empty($img_src3)){ ?>
                    <li class="d-flex align-items-center">
                        <img class="img-fluid" id="3" src="<?php echo $img_src3; ?>">
                    </li>
                    <?php } ?>
                </ul>
            </div>
        </div>

		<!-- 商品名稱 -->
		<h4 class="sajaprod-name"><?php echo $product['name']; ?></h4>

		<!-- 目前得標者 -->
		<div class="productSaja-info">
			<div class="">
				<div class="info-item d-flex justify-content-between align-items-center">
					<div class="item-titleBox d-flex align-items-center">
						<div class="title" style="font-size: 1.55rem;">
							<p>目前得標者</p>
						</div>
					</div>
					<?php
                    if ($sajabid['userid']!='') {
                        $bidded = $sajabid['nickname'];
                        $bidded_class = "color-black";
                    }else{
                        $bidded = "目前無人得標";
                        $bidded_class = "";
                    }
					?>
					<div class="dream_product_bid_winner_vertical-line"></div>
					<div class="dream_product_bid_winner_img">
						<img class="img-fluid" src="<?php echo $sajabid['curr_winner_headimg'];?>" >
					</div>

					<div class="d-inline-flex dream_product_bid_winner_name">
						<?php /*<span id="bidded"><?php echo urldecode($bid_winner['nickname']); ?></span>*/?>
						<span id="bidded" class="<?php echo $bidded_class;?>"><?php echo urldecode($bidded); ?></span>
					</div>
				</div>
			</div>
		</div>

		<!-- 官方售價 -->
		<div class="market-price-box">官方售價 NT <span class="market-price"><?php echo $product['retail_price']; ?></span> 元</div>

    <?php if (!empty($product['unix_offtime'])){ ?>
        <!-- 倒數計時 -->
		<h2 class="overTime">
            <?php /*<p class="countdown" data-offtime="<?php echo $product['unix_offtime']; ?>" data-tag="">00天 00時 00分 00秒 00</p>*/?>
			<div class="countdown d-flex justify-content-center align-items-center" data-offtime="<?php echo $product['unix_offtime']; ?>" data-tag="">
                <div class="countdown_d_style countdown_d"><span class="data">00</span><span class="small">天</span></div>

                <div class="countdown_style countdown_h"><span class="data">0</span></div>
                <div class="countdown_style countdown_h"><span class="data">0</span></div>

                <div class="countdown_symbol countdown_symbol_h">:</div>

                <div class="countdown_style countdown_m"><span class="data">0</span></div>
                <div class="countdown_style countdown_m"><span class="data">0</span></div>

                <div class="countdown_symbol countdown_symbol_m">:</div>

                <div class="countdown_style countdown_s"><span class="data">0</span></div>
                <div class="countdown_style countdown_s"><span class="data">0</span></div>

                <div class="countdown_symbol countdown_symbol_s">:</div>

                <div class="countdown_style countdown_ms"><span class="data">0</span></div>
                <div class="countdown_style countdown_ms"><span class="data">0</span></div>
            </div>
        </h2>
    <?php } ?>
    </div>

    <div class="productSaja-info">
        <?php /*<div class="group">
            <div class="info-item d-flex justify-content-between align-items-center">
                <div class="item-titleBox d-flex align-items-center">
                    <div class="icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rankman.png">
                    </div>
                    <div class="title">
                        <p>目前得標者</p>
                    </div>
                </div>
                <?php
                    if ($sajabid['userid']!='') {
                        $bidded = $sajabid['nickname'];
                        $bidded_class = "color-black";
                    }else{
                        $bidded = "目前無人得標";
                        $bidded_class = "";
                    }
                ?>
                <div class="rightTxt d-inline-flex">
                    <span id="bidded" class="<?php echo $bidded_class;?>"><?php echo urldecode($bidded); ?></span>
                </div>
            </div>
            <!-- <div name="cf" id="cf" class="info-item d-flex justify-content-between align-items-center" style="display:none;">
               <div class="item-titleBox d-flex align-items-center">
                    <div class="icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/from.png">
                    </div>
                    <div class="title">
                        <p>來自</p>
                    </div>
                </div>
                <div class="rightTxt d-inline-flex">
                    <span id="bidfrom" class="color-black"><?php echo $sajabid['comfrom'];?></span>
                </div>
            </div> -->
        </div>
		*/?>
        <?php /* if(!empty($_SESSION['auth_id'])){?>
            <!-- <div class="group">
                <div class="info-item d-flex justify-content-between align-items-center">
                    <div class="item-titleBox d-flex align-items-center">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/left_oscode.png">
                        </div>
                        <div class="title">
                            <p>我的下標券</p>
                        </div>
                    </div>
                    <div class="rightTxt d-inline-flex">
                        <span><?php echo $oscode_num; ?> 張</span>
                    </div>
                </div>
            </div> -->
        <?php }*/ ?>
        <?php /* if($scode_num + $scode_use_num > 1){?>
            <!-- <div class="group">
                <div class="info-item d-flex justify-content-between align-items-center">
                    <div class="item-titleBox d-flex align-items-center">
                        <div class="icon">
                            <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/left_scode.png">
                        </div>
                        <div class="title">
                            <p>我的超級殺價券</p>
                        </div>
                    </div>
                    <div class="rightTxt d-inline-flex">
                        <span><?php echo $scode_num; ?> 張</span>
                    </div>
                </div>
            </div> -->
        <?php }*/ ?>

    <?php if ($product['is_flash'] != 'Y') {  ?>
        <div class="group">
            <div class="info-item d-flex justify-content-between align-items-center" onClick="ReverseDisplay('rule',this,'noblod','scroll');">
                <div class="item-titleBox d-flex align-items-center">
                    <div class="icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="title">
                        <p>下標規則</p>
                    </div>
                </div>
                <div class="rightTxt more red d-inline-flex">
                    <span class="r-arrow">*請詳閱</span>
                </div>
            </div>
            <div id="rule" class="linkBox more" style="display:none">
                <div class="info-item-detailed">
                    <ul>
                        <?php if (!empty($product['rule'])){ ?>
                        <!-- 有自定義時 -->
                        <li>
                            <?php echo $product['rule']; ?>
                        </li>
                        <?php }else{ ?>
                        <!-- 否則使用預設 -->
                        <li class="detailed-title">
                            <p>◎ 本商品每次下標需支付殺價幣 <span style="color: #000; font-weight: bolder;">$<?php echo $product['saja_fee']; ?>/標</span>。</p>
                        </li>
                        <li class="detailed-title">
                            <p>◎ 時間到時，出價最低且不與他人重複者得標。</p>
                        </li>
                        <li class="detailed-title">
                            <p>◎ 得標者需在3日內結帳，違者視同棄標。</p>
                        </li>
                        <li class="detailed-title">
                            <p>◎ 棄標者不得要求退回已支付手續費。</p>
                        </li>
                        <li class="detailed-title">
                            <p>◎ <span style="color: #E32A00">得標者須在結帳時支付得標處理費 $<?php echo ceil($product['process_fee']+$product['checkout_money']); ?></span>。</p>
                        </li>
                        <li class="detailed-title">
                            <p>◎ <span style="color: #E32A00">得標處理費可由已下標手續費100%全額折抵</span>。</p>
                        </li>
                        <li class="detailed-title">
                            <p>◎ <span style="color: #E32A00">未得標者不損失，手續費<?php echo ceil($product['bonus']); ?>%轉贈鯊魚點</span>。</p>
                        </li>
                        <li class="detailed-title">
                            <p>◎ 鯊魚點可至商城1:1使用  請安心下標。</p>
                        </li>
                        <br>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="group">
            <div class="info-item d-flex justify-content-between align-items-center" >
                <div class="item-titleBox d-flex align-items-center">
                    <div class="icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/camera.png">
                    </div>
                    <div class="title">
                        <p>商品詳情</p>
                    </div>
                </div>
                <div class="rightTxt d-inline-flex">
                    <span class="r-arrow">本商品及競標活動均與Apple Inc. 無關 </span>
                </div>
            </div>
            <div id="detail" class="linkBox" style="display:block">
                <div class="info-item-detailed no-border">
                    <?php echo $product['description']; ?>
                </div>
            </div>
        </div>
    <?php } ?>
        <input type="hidden" name="autobid" value="<?php echo $paydata['autobid'];?>" />
        <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
    </div>


    <div class="productSaja-footernav">
        <div class="productSaja-button d-flex">
            <!-- 網址 -->
            <!-- <?php echo BASE_URL.APP_DIR."/product/sajabid/?".$status['status']['args']."&productid=".$product['productid']."&t=".$cdnTime; ?> -->
            <?php if (!empty($_SESSION['auth_id'])) {
                $alert = "";
                if($retCheck['checkMsg'] != ''){
                    // js \n要改成\\n
                    $retCheck["checkMsg"] = str_replace("\n", "\\n", $retCheck["checkMsg"]);
                    $alert = "if(confirm('".$retCheck["checkMsg"]."'))";
                }
            ?>
                <?php if ($product['kind'] == 'any'){?>
                    <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:<?php echo $alert;?> location.href='<?php echo BASE_URL.APP_DIR;?>/product/sajabid/?<?php echo $status['status']['args'];?>&productid=<?php echo $product['productid']; ?>&t=<?php echo $cdnTime; ?>'">殺價去</button>
                <?php } elseif (($product['kind'] == 'small') && ($bidget_count > $product['mvalue'])){ ?>
                    <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:alert('此限得標<?php echo $product['mvalue']; ?>次以內者 才可下標此商品！')">殺價去</button>
                <?php } elseif (($product['kind'] == 'big') && ($bidget_count < $product['svalue'])){ ?>
                    <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:alert('此商品限定得標次數<?php echo $product['svalue']; ?>次以上者才可下標！')">殺價去</button>
                <?php } elseif ($retCheck['checkCode'] != 1){ ?>
                    <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:alert('<?php echo $retCheck['checkMsg']; ?>')">殺價去</button>
                <?php } else { ?>
                    <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:<?php echo $alert;?> location.href='<?php echo BASE_URL.APP_DIR;?>/product/sajabid/?<?php echo $status['status']['args'];?>&productid=<?php echo $product['productid']; ?>&t=<?php echo $cdnTime; ?>'">殺價去</button>
                <?php } ?>

            <?php }else{ ?>
                <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onclick="nologin();">殺價去</button>
            <?php } ?>

            <button class="shareBtn" style="display:none;">分享</button>
        </div>
    </div>
</div>

<!-- 結標&流標 -->
<div class="th_finish">
    <div class="finish2">
        <header>
            <figure></figure>
        </header>
        <div>
            <div class="Congratulations">

            </div>
            <footer class="Congratulations_footer">
            </footer>
        </div>
    </div>
</div>

<script type="text/javascript">
    var chkprodid = "<?php echo $product['productid']; ?>";
    var chkuserid = "<?php echo $_SESSION['auth_id']; ?>";
    var socket;
    var now_time;
    var timer;
		var product_alerady_closed = 0;
    var agent_type = (is_weixin() ? "WEIXIN" : "BROWSER");
    var URL0 = "<?php echo BASE_URL.APP_DIR.'/product/winner/?timestamp='; ?>";
		var s_previous = "00";
    var timer_run = function() {
				//console.log('timer_is_running');
        if(now_time)
           now_time = now_time + 1;
        var now = new Date();
        var end_time = $('.countdown').attr('data-offtime'); //截標時間
        var end_plus = $('.countdown').attr('data-tag'); //標籤
        var lost_time = new Date(end_time*1000).getTime() - now.getTime(); //剩餘時間
        var o = parseInt(lost_time/1000); //原始剩餘時間



        if (lost_time > 0) {

            var d = Math.floor(o / (24 * 60 * 60));
            if (d < 10) {
                d = "0" + d;
            };
            var h = Math.floor((o - (d * 24 * 3600)) / 3600); //37056
            if (h < 10) {
                h = "0" + h;
            };
            var m = Math.floor((o - (d * 24 * 3600) - (h * 3600)) / 60);
            if (m < 10) {
                m = "0" + m;
            };
            var s = Math.floor((o - (d * 24 * 3600) - (h * 3600) - (m * 60)));
            if (s < 10) {
                s = "0" + s;
            };
            var ms = Math.floor(lost_time%100);
			if (ms < 10 ) {
                ms = "0" + ms;
            }
            // if (d == "00") {
                // $('.countdown').html(end_plus + h + " 時 " + m + " 分 " + s + " 秒 " + ms );
            // } else {
                // $('.countdown').html(end_plus + d + " 天 " + h + " 時 " + m + " 分 " + s + " 秒 " + ms);
            // }

            if (d == "00") {
                // $('.countdown').html(end_plus + h + " : " + m + " : " + s + " : " + ms );

                // 隱藏天
                $('.countdown').find('.countdown_d').hide();

                // 時
                if($('.countdown_h')){
                    var $ele_h = $('.countdown_h');
                    $.map(String(h).split(''), function(item,index){
                        $($('.countdown_h')[index]).find('.data').text(item);
                    });
                }

                // 分
                if($('.countdown_m')){
                    var $ele_m = $('.countdown_m');
                    $.map(String(m).split(''), function(item,index){
                        $($('.countdown_m')[index]).find('.data').text(item);
                    });
                }

                // 秒
                if($('.countdown_s')){
                    var $ele_s = $('.countdown_s');
                    $.map(String(s).split(''), function(item,index){
                        $($('.countdown_s')[index]).find('.data').text(item);
                    });
                }

                // 毫秒
                if($('.countdown_ms')){
                    var $ele_ms = $('.countdown_ms');
                    $.map(String(ms).split(''), function(item,index){
                        $($('.countdown_ms')[index]).find('.data').text(item);
                    });
                }
            } else {
                // $('.countdown').html(end_plus + d + " 天 " + h + " : " + m + " : " + s + " : " + ms );

                // 天
                if($('.countdown_d')){
                    $('.countdown').find('.countdown_d').find('.data').text(d);
                }

                // 時
                if($('.countdown_h')){
                    var $ele_h = $('.countdown_h');
                    $.map(String(h).split(''), function(item,index){
                        $($('.countdown_h')[index]).find('.data').text(item);
                    });
                }

                // 分
                if($('.countdown_m')){
                    var $ele_m = $('.countdown_m');
                    $.map(String(m).split(''), function(item,index){
                        $($('.countdown_m')[index]).find('.data').text(item);
                    });
                }

                // 秒
                if($('.countdown_s')){
                    var $ele_s = $('.countdown_s');
                    $.map(String(s).split(''), function(item,index){
                        $($('.countdown_s')[index]).find('.data').text(item);
                    });
                }

                // 毫秒
                if($('.countdown_ms')){
                    var $ele_ms = $('.countdown_ms');
                    $.map(String(ms).split(''), function(item,index){
                        $($('.countdown_ms')[index]).find('.data').text(item);
                    });
                }
            }

            // 每5秒整更新一次(中標者和倒數計時的時間)
            if (s== "00" || s == "05" || s=="10" || s=="15" || s=="20" || s=="25" || s=="30" || s=="35" || s=="40" || s=="45" || s=="50" || s=="55" ) {
					// 每10秒整更新一次(中標者和倒數計時的時間)
					// if (s== "00" || s=="10" || s=="20" || s=="30" || s=="40" || s=="50" ) {
					//防止同秒數重複執行
					if (s != s_previous){
							/*
							$.get("https://ws.saja.com.tw/site/lib/cdntime.php", function(data) {
							// $.get("<?php echo UNIX_TS_URL; ?>", function(data) {
									now_time = parseInt(data); //系統時間
							});
                            */
							// 20190528 justice_lee 輪詢得標者先拿掉
							// autoUpdate('');

							check_product_closed();//查詢商品是否已結標 如已結標顯示結標結果;
							s_previous = s;//執行完儲存本次秒數
					}
					if (s== "00" || s=="30") {
					    $.get("https://ws.saja.com.tw/site/lib/cdntime.php", function(data) {
									now_time = parseInt(data);  //系統時間
						});
					}
            }
        } else if (!lost_time || lost_time <= 0) {
						//clearInterval(timer);
						$('.countdown').html(end_plus + " 已結標 ");

						// 測試帳號不檢查是否結標 add By Thomas 2019/10/28
						if(chkuserid!='1705')
						   check_product_closed();  //查詢商品是否已結標 如已結標顯示結標結果;

        }
    };


		//查詢商品是否已結標 如已結標顯示結標結果
		function  check_product_closed() {

			//產品查詢網址
			var PURLs = "<?php echo BASE_URL.APP_DIR;?>/product/saja/?json=Y&productid="+chkprodid;

			//產品結標狀態
			var product_info = new Object;

			$.ajax({
					type: 'POST',
					url: PURLs,
					dataType: 'json',
					success: function(msg) {

							//console.log(JSON.stringify(msg));
							product_info.product_name = msg.retObj.name;//產品名稱
							product_info.product_closed = msg.retObj.closed;//產品是否結標狀態

							if(product_info.product_closed == "Y" && product_alerady_closed == 0){//商品已結標
									clearInterval(timer);
									product_alerady_closed = 1;
									product_success_closed(product_info.product_name);//產品成功結標處理
									console.log('product_info = '+ JSON.stringify(product_info));
							}else if(product_info.product_closed == "NB" && product_alerady_closed == 0){//商品已流標
									clearInterval(timer);
									product_alerady_closed = 1;
									show_close_bid_message('NB','','');//顯示已結標訊息
									console.log('product_info = '+ JSON.stringify(product_info));
							}

							return;
					}

			})

		}


		//產品成功結標處理
		function  product_success_closed(product_name) {

				//查詢得標者網址
				var WHOURLS = "<?php echo BASE_URL.APP_DIR;?>/bid/bidlist/?json=Y&productid="+chkprodid+"&type=Y";

			  //查詢得標者並顯示
				$.ajax({
						type: 'POST',
						url: WHOURLS,
						dataType: 'json',
						success: function(msg) {
								var winner=msg.retObj.data.nickname; //得標者
								show_close_bid_message('Y', product_name, winner);//顯示已結標訊息
								return;
						}
				})
		}


		//顯示已結標訊息
		function  show_close_bid_message(product_closed_state, product_name, winner) {

				//成功結標導向位置
				var success_url = "'<?php echo BASE_URL.APP_DIR;?>/bid/detail/?productid="+chkprodid+"'";

				//流標導向位置
				var nb_url = "'<?php echo BASE_URL.APP_DIR;?>'";

				if(product_closed_state == "Y"){
						$('.Congratulations').append(
								'<header>恭喜</header>'+
								'<div>'+product_name+'</div>'+
								'<footer><figure><img src="<?PHP echo APP_DIR; ?>/static/img/icon_Trophy.png"></figure><p>得標者</p>'+
								'<span>'+winner+'</span>'+
								'</footer>');
						$('.Congratulations_footer').append('<a class="btn_go" href="#" onclick="javascript:location.href='+success_url+'"><span>確定</span></a>');
						$(".th_finish").addClass('on');
				}else if(product_closed_state == "NB"){
						$('.Congratulations').append(
								'<header>流標</header>'+
								'<div>此商品流標</div>'
								);
						$('.Congratulations_footer').append('<a class="btn_go" href="#" onclick="javascript:location.href='+nb_url+'"><span>確定</span></a>');
						$(".th_finish").addClass('on');
				}

		}

    function WeiXinShareBtn() {
        if (typeof WeixinJSBridge == "undefined") {
            alert("請先通過微信搜索 殺價王 添加殺價王為好友，通過微信分享文章 :) ");
        } else {
            WeixinJSBridge.invoke('shareTimeline', {
                "title": "殺價王",
                "link": "https://www.saja.com.tw",
                "desc": "殺價王",
                "img_url": ""
            });
        }
    }

    function autoUpdate(showReloadIcon) {
        var URLs = "<?php echo BASE_URL.APP_DIR;?>/product/winner/?timestamp=" + new Date().getTime();
        if (showReloadIcon == 'Y') {
            $.mobile.loading('show', {
                text: 'reload..',
                textVisible: true,
                theme: 'b',
                html: ""
            });
        } else {
            $.mobile.loading('hide');
        }
        $.ajax({
            url: URLs,
            data: {
                productid: chkprodid
            },
            type: "GET",
            dataType: 'text',
            success: function(msg) {
                $.mobile.loading('hide');
                var json = JSON && JSON.parse(msg) || $.parseJSON(msg);
                if (json.productid == chkprodid) {
                    //假如無人得標或地址沒填寫，則地址(#cf)不顯示
                    //有人得標時增加class套用黑色(#bidded)
                    if (json.status == 1) {
                        $('#bidded').html(decodeURIComponent(json.name));
                        $("#bidded").addClass("color-black");
                        $("#cf").removeClass("d-flex");
                    }else{
                        $('#bidded').text("目前無人得標");
                        $("#bidded").removeClass("color-black");
                        if(json.comefrom){
                            $("#cf").addClass("d-flex");
                        }else{
                            $("#cf").removeClass("d-flex");
                        }

                    }
                    $('#bidfrom').html(json.src_ip);
                    $('#bidfrom').html(decodeURIComponent(json.comefrom));
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                //alert(xhr.status);
                //console.log(thrownError);
            }
        });
    }

    function ws_bidding(result) {
        var json = JSON.parse(result);
        if (json.actid == 'BID_WINNER' && json.productid == chkprodid) {
            //有人得標時增加class套用黑色(#bidded)
            if (json.status == 1) {
                $('#bidded').text(json.name);
                $("#bidded").addClass("color-black");
            }else{
                $('#bidded').text("目前無人得標");
                $("#bidded").removeClass("color-black");
            }
        }
    }

    $(document).ready(function() {
        clearInterval(timer);
        // autoUpdate('');
        var ua = navigator.userAgent.toLowerCase();
        var WS_URL = '<?php echo $config['WS_URL ']; ?>';
        WS_URL = "wss://www.saja.com.tw/";
        if (ua.match(/MicroMessenger/i) == "micromessenger" && ua.match(/Android/i) == "android") {
            WS_URL = "ws://www.saja.com.tw/";
        }


        // websocket最新得標測試
        socket = new WebSocket("wss://ws.saja.com.tw:3334");
        socket.onmessage = function(ret){
            ws_bidding(ret.data);
        }

        if (false) {
            $.mobile.loading('show', {
                text: '準備中.....',
                textVisible: true,
                theme: 'b',
                html: ""
            });
            $.ajaxSetup({
                cache: false
            });
            socket = new WebSocket(WS_URL);
            socket.onopen = function(evt) {
                autoUpdate('');
                socket.send('{"AGENT":"' + agent_type + '","PID":"<?php echo $product['
                                productid ']; ?>","UID":"<?php echo $_SESSION['
                                auth_id '];?>","PAGE":"saja","IP":"<?php echo $ip; ?>","MEMO":"' + navigator.appVersion + '"}');
            };
            socket.onmessage = function(ret) {
                var json;
                if ("object" == (typeof ret)) {
                    msg = $.trim(ret.data);
                    json = JSON && JSON.parse(msg) || $.parseJSON(msg);
                    if (json) {
                        if (json.ACTION == 'NOTIFY' && json.PRODID == chkprodid) {
                            $("#bidded").text(json.WINNER);
                            $("#src_ip").text(json.SRC_IP);
                        }
                    }
                }
            };
            socket.onclose = function() {
                if (event.code != 1000) {
                    /*
                      alert("連線異常！！"+event.code);
                      $.get("<?php echo UNIX_TS_URL; ?>",function(data) {
                              now_time = parseInt(data); //系統時間
                              autoUpdate();
                              timer=setInterval(timer_run,1000);
                      });
                      $.mobile.loading('hide');
                      */
                    // exit;
                } else {
                    //console.log("disconnect !!");
                }
                $.mobile.loading('hide');
            };
            socket.onerror = function(event) {
                // alert("您的微信流覽器不支持 ！");
                $.mobile.loading('hide');
                exit;
            };
        }
        $.get("https://ws.saja.com.tw/site/lib/cdntime.php", function(data) {
        // $.get("<?php echo UNIX_TS_URL; ?>", function(data) {
            now_time = parseInt(data); //系統時間
            timer = setInterval(timer_run, 59);
            $.mobile.loading('hide');
        });
    });

    // Wechat
    var dataForWeixin = {
        MsgImg: "<?php echo $img_src; ?>",
        TLImg: "<?php echo $img_src; ?>",
        url: "https://www.saja.com.tw" + APP_DIR + "/product/oscode/?productid=<?php echo $product['productid']; ?>",
        title: "<?php echo $product['name']; ?> 搶殺中-最低且唯一價中標，沒中標100%返還，還不快加入搶殺 ?",
        desc: "<?php if ($product['productid'] != 8015 ) { ?><?php echo $product['price_limit']; ?>元起 熱烈搶殺中，你還琢磨什麼? <?php } else { ?><?php echo $product['name']; ?>低價搶殺中最低且唯一中標，沒中標100%返還你的朋友正在搶殺還不快加入。<?php } ?>",
        fakeid: "",
        callback: function() {}
    };

    //產品圖幻燈片
    $(document).on('pageshow', function(){ //幻燈片banner
        $('.flexslider').flexslider({
            animation: "slide", //圖片切換方式 (滑動)
            slideshowSpeed: 3000, //自動播放速度 (毫秒)
            animationSpeed: 600, //切換速度 (毫秒)
            directionNav: false, //顯示左右控制按鈕
            controlNav: true, //隱藏下方控制按鈕
            prevText:"", //左邊控制按鈕顯示文字
            nextText:"", //右邊控制按鈕顯示文字
            pauseOnHover: false, //hover時停止播放
            animationLoop: false, //圖片循環
            slideshow: false, //自動播放 (debug用)
            start: function(slider){ //載入第一張圖片時觸發
                $('body').removeClass('loading');
            }
        });
    });

    function nologin() {
        // alert("殺友請先登入 !!");
        location.href="<?PHP echo APP_DIR; ?>/member/userlogin/?_to=<?php echo APP_DIR.'/product/saja/&productid='.$product['productid'];?>";
    }
</script>

<!-- 分享領殺價券 -->
<script>
    $(function(){
        $('.shareBtn').on('click', function(){
            //user_src 與 productid 沿用上面的參數
            var behav = 'b';
            window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('<?PHP echo BASE_URL.APP_DIR; ?>/shareoscode?user_src='+chkuserid+'&productid='+chkprodid+'&behav='+behav)) );
        })
    });
</script>
