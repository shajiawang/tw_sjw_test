<?php
$product = _v('product');
$sajabid = _v('sajabid');
$status = _v('status');
$meta=_v('meta');
$canbid=_v('canbid');
$cdnTime = date("YmdHis");
$options = _v('options');
$type=_v('type');
$paydata=_v('paydata');
$spoint = _v('spoint'); 
if(!isset($canbid)) {
    $canbid=$_REQUEST['canbid'];
}
if(empty($canbid)) {
    $canbid='Y';
}
//下標費用收取判斷
switch($product['totalfee_type']) {
	case 'A':
		 $totalfee_type = "下標金額 及 手續費";	
		 break;
	case 'B':
		 $totalfee_type = "下標金額";
		 break;
	case 'F':
		 $totalfee_type = "手續費";
		 break;
	case 'O':
		 $totalfee_type = "免費";
		 break;
}
//閃殺商品
//$flash_img = ($product['is_flash']=='Y') ? 'class="flash_img"' : '';
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
?>
<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/keyboard.min.css">
<!-- 此頁css -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/sajabid.css">

<div id="tobid" class="swipe-tab d-flex flex-column">
     
    <div class="swipe-navbar-content bgc_gary">
        <!-- 20190508 警告文字 -->
        <div class="warn_directions text-center w-100">
            <!-- 20190515使否需要手續費顯示 -->
            <div class="d-small sajabid-small">
                <?php if($product['totalfee_type'] == 'O'){ ?>
                    <span class="orange">本商品下標 <span class="txt_red"><?php echo $totalfee_type?></span></span>
                    <?php }else if($product['totalfee_type'] == 'A'){ ?>
                    <span class="txtfw">本商品下標收
                        <span class="txt_red">下標金額</span> 及 <span class="txt_red">手續費</span><span class="fee txt_red"><?php echo ' ($'.$product['saja_fee'].'元/標)';?></span></span>
                    </span>
                <?php }else if($product['totalfee_type'] == 'B'){ ?>
                <span class="txtfw">本商品下標只收
                    <span class="txt_red"><?php echo $totalfee_type?></span>
                </span>
                <?php }else{ ?>
                    <span>本商品下標只收 <span class="txt_red"><?php echo $totalfee_type; ?><span class="fee"><?php echo ' ($'.$product['saja_fee'].'元/標)';?></span>
                </span>
                <?php } ?>
            </div>
        </div>
		
        <!-- 20190508 單次下標 -->
        <div id="tab2" class="content-item sajabid_single on-active m25 under_chip">
            <div class="item-box">
                <div class="toptxt d-flex align-items-center ">
                    <div class="text-center w-100">請輸入下標金額</div>
                </div>
                <div class="text-center">
                    <div class="placeholder sajabid-text">
                        <div class="d-flex align-items-end justify-content-center">
                        <!-- 20190507 多加單字圖片 -->
                        <div class="sajabid-font1">下標金</div>
                            <input type="tel" name="price" id="price" class="keyboard allinput-type2" min="0" pattern="[0-9]*" step="1" onkeyup="sajabid_single()" autofocus placeholder=""/>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
   
    <form id="sajabid" name="sajabid" method="post" action="<?php echo BASE_URL.APP_DIR; ?>/dream_product/sajatopay/">
        <input type="hidden" name="type" id="type" value="single">
        <input type="hidden" name="count" id="count" value="1"/>
        <input type="hidden" name="fee_all" id="fee_all" value="<?php echo $product['saja_fee'];?>"/>
        <input type="hidden" name="total_all" id="total_all" value="0">
        <input type="hidden" name="bida" id="bida" value="0"/>
        <input type="hidden" name="bidb" id="bidb" value="0"/>
        <input type="hidden" name="totalfee_type" id="totalfee_type" value="<?php echo $product['totalfee_type']; ?>"/>
        <input type="hidden" name="saja_fee" id="saja_fee" value="<?php echo $product['saja_fee'];?>"/>
        <input type="hidden" name="autobid" id="autobid" value="<?php echo $paydata['autobid'];?>"/>
        <input type="hidden" name="productid" id="productid" value="<?php echo $product['productid']; ?>">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
        <input type="hidden" name="retail_price" id="retail_price" value="<?php echo $product['retail_price']; ?>">
		<input type="hidden" name="usereach_limit" id="usereach_limit" value="<?php echo $product['usereach_limit']; ?>">
        <input type="hidden" name="unix_offtime" id="unix_offtime" value="<?php echo $product['unix_offtime']; ?>">
        <input type="hidden" name="now" id="now" value="<?php echo $product['now']; ?>">
        <input type="hidden" name="totalpages" id="totalpages">
        <input type="hidden" name="totalcount" id="totalcount">
		
        <input type="hidden" name="dpname" id="dpname" value="<?php echo $product['dpname']; ?>">
        <input type="hidden" name="dpcontent" id="dpcontent" value="<?php echo $product['dpcontent']; ?>">
        <input type="hidden" name="dpaddress" id="dpaddress" value="<?php echo $product['dpaddress']; ?>">		
		<input type="hidden" name="dppic" id="dppic" value="<?php echo $product['dppic']; ?>">

        <div class="sajabid-footernav">
            <div class="footernav-button">
				<?php if (!empty($_SESSION['auth_id'])) { ?>
                <button type="button" id="topay" onClick="saja_gopay(this.form);">下一步</button>
				<?php }else{ ?>
				<button type="button" id="topay" onclick="nologin();">下一步</button>
				<?php } ?>  
            </div>
        </div>
    </form>
</div>


<!--  Bootstrap 4.1.1  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>

<!--  鍵盤-自訂義JS  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/js/keyboard_v1.2.js"></script>
    
<!--  小鍵盤動作  -->
<script>
    options.dom.caller = '.keyboard';                   //命名:呼叫小鍵盤的物件
    options.dom.keyidName = 'numkeyModal';              //命名:小鍵盤(無id符號)
    options.dom.keyModal = '#' + options.dom.keyidName; //命名:小鍵盤(有id符號)
    options.dom.callerId = '#price';                    //預設一開始的呼叫者,在keyboard.JS會做變更 點擊者
    options.btn.sure = '確定';                           //小鍵盤送出按鈕名稱
    
    $(function(){
        $(options.dom.caller).each(function(i){
            var $modalNum = options.dom.keyidName+i;
            var $that = $(this);
            $that.on("click",function(e){
                options.dom.callerId = $that;                           //記錄點擊哪個input的ID
                //開啟小鍵盤的動作
                //若預設開啟時，此段不會執行
                $('#'+$modalNum).on('show.bs.modal', function (e) {
                    $('.allinput-type2').css('border-bottom', '2px solid #d7d7d7');                           //執行onkeyup函數

                });

                //關閉小鍵盤的動作
                $('#'+$modalNum).on('hide.bs.modal', function (e) {
                    var $callerId = $(options.dom.callerId);            //取得按鈕ID
                    $callerId.keyup();   
                    $('.allinput-type2').css('border-bottom', '1px solid #E5E5E5');                           //執行onkeyup函數
                });
            })
        });
        setTimeout(function(){
            $(options.dom.callerId).click();                //小鍵盤預設開啟 (類似input的focus)
        },200);
    });
    
    //查看詳情
    // $('.more-details').on('click', function(){
    //     $('.detailsModal').show();
    //     $('.detailsModal-cover').show();
    //     $('body').addClass('modal-open');
    // })
    
    // $('.modal-details-close, .detailsModal-cover').on('click', function(){
    //     $('.detailsModal').hide();
    //     $('.detailsModal-cover').hide();
    //     $('body').removeClass('modal-open');
    // })
    
</script>
    

<script type="text/javascript">
    $(window).load(function(){
        var $winH = $(window).height();
        var $navbarH = $(".navbar").outerHeight(true);
        $("#tobid").css("min-height", $winH - $navbarH);
        $(".product").css("background","#FFF");


    });
	
	function nologin() {
		// alert("殺友請先登入 !!"); 
		location.href="/site/member/userlogin/";
	}	
</script>