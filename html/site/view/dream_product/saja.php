<?php
$product = _v('product');
$sajabid = _v('sajabid');
$status = _v('status');
$meta=_v('meta');
$canbid=_v('canbid');
$cdnTime = date("YmdHis");
$options = _v('options');
$type=_v('type');
$paydata=_v('paydata');
$latest_bid=_v('latest_bid');
$bid_winner=_v('bid_winner');
$bidget_count=_v('bidget_count');
$retCheck=_v('retCheck');
if(!isset($canbid)) {
    $canbid=$_REQUEST['canbid'];
}
if(empty($canbid)) {
    $canbid='Y';
}
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
if(empty($product) || empty($product['productid'])) {
	
?>
<p>
<div align="center" style="color:blue;font-size:30px;">無此商品 !!</div>
<script>
    window.location.href = "/site/product";
</script>
<?php
    exit;
}
?>
<!-- 此頁css -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/saja.css">
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/croppie.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/exif.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/megapix-image.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/weui_loading.js<?php echo "?_t=".date("YmdHis"); ?>"></script>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/croppie.css" rel="stylesheet"/>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/weui_loading.css" rel="stylesheet"/>

<div class="productSaja" id="dream_product" name="dream_product">
    <!-- <?php echo $_SESSION['auth_id']; ?> -->
    <div class="swipe-navbar-item" ></div>
    <div class="productSaja-imgbox">

        <?php
        if(!empty($bid_winner['shd_thumbnail_file'])) {
            $img_src = IMG_URL.'/site/images/dream/products/'.$bid_winner['shd_thumbnail_file'];
        } 
        ?>
        <div class="productSaja-imgbox imgbox-show">
            <!-- 圖片只有一張時 停用幻燈片滑動 -->
            <div class="sajaprod-img" style="border-radius: 50%;">
                <ul class="">
                    <li class="d-flex align-items-center ">
                        <img name="user-prod" id="user-prod" class="img-fluid dream_product_imgs" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>">
                    </li>
                </ul>
            </div>
        </div>
		<div class="productSaja-info">
			<div class="">
				<div class="info-item d-flex justify-content-between align-items-center">
					<div class="item-titleBox d-flex align-items-center">
						<div class="title" style="font-size: 1.55rem;">
							<p>目前得標者</p>
						</div>
					</div>
					<div class="dream_product_bid_winner_vertical-line"></div>
					<div class="dream_product_bid_winner_img">
							<!--img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rankman.png"-->
						<?php if (!empty($bid_winner['thumbnail_file'])) { ?>
							<img class="img-fluid" src="<?php echo BASE_URL.HEADIMGS_DIR;?>/<?php echo $bid_winner['thumbnail_file'];?>?t=<?php echo $cdnTime;?>" >
						<?php } elseif (!empty($bid_winner['thumbnail_url'])){ ?>
							<img class="img-fluid" src="<?php echo $bid_winner['thumbnail_url']; ?>" >
						<?php } else { ?>
							<img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rankman_1.png">
						<?php } ?>							
					</div>

					<?php if (empty($bid_winner['nickname']) && empty($bid_winner['userid'])) { ?>
					<div class="item-titleBox d-flex align-items-center" >
						<span style="color:#c3c3c3; font-size:1.3rem;">目前無人得標</span>
					</div>
					<?php } ?>
					
					<div class="d-inline-flex dream_product_bid_winner_name">
						<span id="bidded"><?php echo urldecode($bid_winner['nickname']); ?></span>
					</div>
				</div>
			</div>
		</div>
    <?php if (!empty($product['unix_offtime'])){ ?>
        <h2 class="overTime">
            <div class="countdown d-flex justify-content-center align-items-center" data-offtime="<?php echo $product['unix_offtime']; ?>" data-tag="">
                <div class="countdown_d_style countdown_d"><span class="data">00</span><span class="small">天</span></div>

                <div class="countdown_style countdown_h"><span class="data">0</span></div>
                <div class="countdown_style countdown_h"><span class="data">0</span></div>

                <div class="countdown_symbol countdown_symbol_h">:</div>

                <div class="countdown_style countdown_m"><span class="data">0</span></div>
                <div class="countdown_style countdown_m"><span class="data">0</span></div>

                <div class="countdown_symbol countdown_symbol_m">:</div>

                <div class="countdown_style countdown_s"><span class="data">0</span></div>
                <div class="countdown_style countdown_s"><span class="data">0</span></div>

                <div class="countdown_symbol countdown_symbol_s">:</div>

                <div class="countdown_style countdown_ms"><span class="data">0</span></div>
                <div class="countdown_style countdown_ms"><span class="data">0</span></div>
            <!-- 00天 00: 00: 00: 00 -->
            </div>
        </h2>
    <?php } ?>
    </div>
	
    <div class="productSaja-footernav2">
        <div class="productSaja-button2 d-flex">
            <!-- 網址 -->
			<?php if (!empty($_SESSION['auth_id'])) { ?>
					<button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:ScrollTo('dream_add')">我要許願</button>
			<?php }else{ ?>
				<button type="submit" class="ui-btn ui-corner-all ui-btn-a" onclick="nologin();">我要許願</button>
			<?php } ?>
		</div>
    </div>	
	
    <div class="productSaja-info">
        <div class="group">
            <div class="info-item d-flex justify-content-between align-items-center" onClick="ReverseDisplay('rule',this,'noblod','scroll');">
                <div class="item-titleBox d-flex align-items-center">
                    <div class="icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="title">
                        <p>規則</p>
                    </div>
                </div>
                <div class="rightTxt red d-inline-flex">
                    <span class="r-arrow">*請詳閱</span>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>					
                </div>
            </div>
            <div id="rule" class="linkBox" style="display:none">
                <div class="info-item-detailed">
                    <ul>
                        <?php if (!empty($product['rule'])){ ?>
                        <!-- 有自定義時 -->
                        <li>
                            <?php echo $product['rule']; ?>
                        </li>
                        <?php }else{ ?>
                        <!-- 否則使用預設 -->
                        <li>
                            <p class="detailed-title">下標方式：</p>
                            <p>每次下標只需支付手續費 $<?php echo $product['saja_fee']; ?>/標，如在時間截止後未得標，我們將贈送同等全部下標費用的鯊魚點給您並存入您的帳戶中，使用殺價券下標者則不贈送。</p>
                        </li>
                        <li>
                            <p class="detailed-title">下標規則：</p>
                            <p>1.包牌下標一次不可超過100標。</p>
                            <p>2.下標金額最低為1元。</p>
                            <p>3.下標時須自行確認是否重覆下標(可於殺價紀錄重中查看自己已下標的數字)。</p>

                        </li>					
                        <li>
                            <p class="detailed-title">得標方式：</p>
                            <p>在時間截止時，由出價最低且不與他人重覆者得標，<strong class="red">得標者需在結帳時支付得標處理費 $<?php echo ceil($product['process_fee']+$product['checkout_money']); ?></strong>。</p>
                        </li>
                        <li class="detailed-title">
                            <p>商品由協力廠商提供，得標並完成訂單後，配合活動廠商發貨排程出貨，隨機出貨，不可選色、選款。若協力廠商缺貨或因其他不可抗力原因無法出貨，得標者須同意接受等同於本商品市價的殺價幣為替代方案，下標本商品則默認為同意不可抗力原因下的替代方案。</p>
                        </li>
                        <?php } ?>
                    </ul>
                </div>
            </div>
        </div>

    </div>
	<?php if (!empty($latest_bid)){ ?>
	<!--div class="dream_productSaja">
		<hr >
    </div-->
	<div align="center" style="margin-top:1rem;font-size:1.55rem;">
		<img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/dream_prod/left.png" style="height: 1.2rem;" >
		<font class="dream_product_line_b">其他人的夢想</font>
		<img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/dream_prod/right.png" style="height: 1.2rem;">
	</div>
	
	<div class="article" >
		<ul class="user-lists-box" style="background-color: #f9f9f9;">
			<?php foreach($latest_bid as $rk => $rv){ ?>
			<li class="hsaja-list">
				<div class="hsaja-header2 d-flex align-items-center">
					<div class="saja-status d-flex align-items-center mr-auto" style="max-width:70%;" >
						<div class="status-img2 dream_product_img" style=" border-radius: 50%;">
							<img class="img-fluid" style="border-radius: 50%;" src="<?php echo IMG_URL.'/site/images/dream/products/'. $rv['thumbnail_filename']; ?>">
						</div>
						<div class="status-title d-flex align-items-center" style="max-width:70%; margin-left: 1rem;">
							<span class="dream_product_title"><?php echo $rv['title']; ?></span>
						</div>
					</div>
					<div class="bid-price" style="width:30%;">
						<div class="productSaja-footernav2" style="background-color: #f9f9f9; margin-right:1rem;">
							<div class="productSaja-button2">					
								<!--button type="submit" name="b<?php echo $rk+1; ?>" id="b<?php echo $rk+1; ?>" style="width:100%; font-size:1.25rem;" onclick="prddata(<?php echo $rk+1; ?>,'<?php echo $rv['thumbnail_filename']; ?>','<?php echo $rv['title']; ?>','<?php echo $rv['content']; ?>','<?php echo $rv['purchase_url']; ?>')" >我要 +1</button-->
								<button type="submit" name="b<?php echo $rk+1; ?>" id="b<?php echo $rk+1; ?>" style="width:100%; font-size:1.25rem;" onclick="prddata(<?php echo $rk+1; ?>,<?php echo $product['productid'];?>,<?php echo $rv['shdid']; ?>)" >我要 +1</button>
							</div>
						</div>								
					</div>
				</div>
			</li>
			<?php } ?>
		</ul>
	</div>
	<?php } ?>
	
	<!--div class="dream_productSaja_hr">
		<span class="word">許願單</span>
	</div-->
<form method="post" action="<?php echo BASE_URL.APP_DIR; ?>/dream_product/sajabid/" id="dpsaja" name="dpsaja">	
	
	<div class="dream_product_line_box">
		<div class="dream_product_line" >
			<font class="dream_product_line_a"><img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/dream_prod/left.png" style="height: 1.2rem;">
			<font class="dream_product_line_b">許願單</font> <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/dream_prod/right.png" style="height: 1.2rem;"></font>
		</div>
	</div>
	
	<div id="dream_add" class="">
		<!-- 圖檔內容-->	
		<div style="visibility:hidden;height:1px;background-color:#fff;">
			<input name="ispid" id="ispid" class="uploader__input" type="file" accept="image/*" placeholder="" >
		</div>		
		<!--  上傳圖片區  -->
		<div class="saja_lists_box" style="border-color: #fff;">
			<div class="list-group">
				<div class="upload-box">
					<div class="img-group d-flex justify-content-center" >
						<div id="pid1" class="img-list dream_product_img d-none" style="width:12rem;height:12rem;border-radius: 50%;" onclick="$('#ispid').trigger('click'); return false;">
							<img id="preview_logo1" name="preview_logo1" style="width:12rem;height:12rem;" src="http://www.saja.com.tw/site/images/site/product/121a8901e96cc3d806fa68b22b88c5b7.jpg" alt="">
						</div>
						<div id="pid-add" class="" onclick="$('#ispid').trigger('click'); return false;">
                            <div id="pid-but" class="upload_btn d-flex justify-content-center align-items-center"><span class="add_icon">+</span>上傳願望商品圖</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<!--  輸入發問內容  -->
		<div class="userEdit" style="background-color:#fff;">
			<div class="required" style="padding-top: 1.334rem;">
				<ul class="input_group_box">
					<li class="input_group d-flex align-items-center">
						<div class="label" for="name">商品名稱</div>
						<input name="dpname" id="dpname" style="background-color: #ddd;width: 73%;border-radius: 5px;" value="" type="text" placeholder="">
					</li>
					<li class="input_group d-flex align-items-center" style=" align-items: baseline;">
						<div class="label" for="zip" style="margin-top: 1rem;">商品敘述</div>
						<textarea class="textarea-box" style="background-color: #ddd;border-radius: 5px;" name="dpcontent" id="dpcontent" placeholder=""></textarea>
					</li>
					<li class="input_group d-flex align-items-center">
						<div class="label" for="address">出處網址</div>
						<input name="dpaddress" id="dpaddress" style="background-color: #ddd;width: 73%;border-radius: 5px;" value="" type="text" placeholder="">
					</li>

				</ul>
			</div>
		</div>
		<!-- 確認按鈕 -->
		<div class="productSaja-footernav2">
			<div class="productSaja-button2 d-flex">
				<!-- 網址 -->
				<?php if (!empty($_SESSION['auth_id'])) { ?>
                    <?php if ($retCheck['checkCode'] != 1){ ?>
                        <button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:alert('<?php echo $retCheck['checkMsg']; ?>')">送出願望</button>
                    <?php } else { ?>
                        <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="dp_product(this.form);">送出願望</button>
                    <?php } ?>
				<?php }else{ ?>
					<button type="button" class="ui-btn ui-corner-all ui-btn-a" onclick="nologin();">送出願望</button>
				<?php } ?>
			</div>
		</div>
		<input type="hidden" id="thumbnail" name="thumbnail">
		<input type="hidden" id="productid" name="productid" value="<?php echo $product['productid'];?>">
        <input type="hidden" name="autobid" value="<?php echo $paydata['autobid'];?>" />
        <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">		
	</div>
</form>


</div>

<!-- 結標&流標 -->
<div class="th_finish">
    <div class="finish2">
        <header>
            <figure></figure>
        </header>
        <div>
            <div class="Congratulations">

            </div>
            <footer class="Congratulations_footer">
            </footer>
        </div>
    </div>
</div>

<div id="actions" style="display:none;">
    <div class="pic-box">
        <div id="new_logo" style="text-align: center;"></div>
    </div>
    <canvas id="cvs" style="display:none"></canvas>
    <canvas id="cvs-wfp" style="display:none"></canvas>

    <div class="btn-box">
        <div class="rotate-group mx-auto d-flex">
            <button onclick="rotate_left()">向左旋轉90度</button>
            <button onclick="rotate_right()">向右旋轉90度</button>
        </div>
        <div class="submit-group mx-auto">
            <button onclick="save_pic()">儲存</button>
            <button onclick="cancel_edit()">取消</button>
        </div>
    </div>
</div>

<script type="text/javascript">
    var chkprodid = "<?php echo $product['productid']; ?>";
    var chkuserid = "<?php echo $_SESSION['auth_id']; ?>";
    var socket;
    var now_time;
    var timer,count=0,count2=0,count3=0,count4=0;
    var agent_type = (is_weixin() ? "WEIXIN" : "BROWSER");
    var URL0 = "<?php echo BASE_URL.APP_DIR.'/product/winner/?timestamp='; ?>";
    var timer_run = function() {
        if(now_time)
		   now_time = now_time + 1;
		var now = new Date();
		var end_time = $('.countdown').attr('data-offtime'); //截標時間
        var end_plus = $('.countdown').attr('data-tag'); //標籤
        var lost_time = new Date(end_time*1000).getTime() - now.getTime(); //剩餘時間
		var o = parseInt(lost_time/1000); //原始剩餘時間

		if (lost_time > 0) {
			
			var d = Math.floor(o / (24 * 60 * 60));
            if (d < 10) {
                d = "0" + d;
            };
            var h = Math.floor((o - (d * 24 * 3600)) / 3600); //37056
            if (h < 10) {
                h = "0" + h;
            };
            var m = Math.floor((o - (d * 24 * 3600) - (h * 3600)) / 60);
            if (m < 10) {
                m = "0" + m;
            };
            var s = Math.floor((o - (d * 24 * 3600) - (h * 3600) - (m * 60)));
            if (s < 10) {
                s = "0" + s;
            };
            var ms = Math.floor(lost_time%100);
			if (ms < 10 ) {
                ms = "0" + ms;
            }
            // if (d == "00") {
                // $('.countdown').html(end_plus + h + " 時 " + m + " 分 " + s + " 秒 " + ms );
            // } else {
                // $('.countdown').html(end_plus + d + " 天 " + h + " 時 " + m + " 分 " + s + " 秒 " + ms);
            // }
			
            if (d == "00") {
                // $('.countdown').html(end_plus + h + " : " + m + " : " + s + " : " + ms );
                
                // 隱藏天
                $('.countdown').find('.countdown_d').hide();
                
                // 時
                if($('.countdown_h')){
                    var $ele_h = $('.countdown_h');
                    $.map(String(h).split(''), function(item,index){
                        $($('.countdown_h')[index]).find('.data').text(item);
                    });
                }

                // 分
                if($('.countdown_m')){
                    var $ele_m = $('.countdown_m');
                    $.map(String(m).split(''), function(item,index){
                        $($('.countdown_m')[index]).find('.data').text(item);
                    });
                }

                // 秒
                if($('.countdown_s')){
                    var $ele_s = $('.countdown_s');
                    $.map(String(s).split(''), function(item,index){
                        $($('.countdown_s')[index]).find('.data').text(item);
                    });
                }

                // 毫秒
                if($('.countdown_ms')){
                    var $ele_ms = $('.countdown_ms');
                    $.map(String(ms).split(''), function(item,index){
                        $($('.countdown_ms')[index]).find('.data').text(item);
                    });
                }
            } else {
                // $('.countdown').html(end_plus + d + " 天 " + h + " : " + m + " : " + s + " : " + ms );
                
                // 天
                if($('.countdown_d')){
                    $('.countdown').find('.countdown_d').find('.data').text(d);
                }
                
                // 時
                if($('.countdown_h')){
                    var $ele_h = $('.countdown_h');
                    $.map(String(h).split(''), function(item,index){
                        $($('.countdown_h')[index]).find('.data').text(item);
                    });
                }

                // 分
                if($('.countdown_m')){
                    var $ele_m = $('.countdown_m');
                    $.map(String(m).split(''), function(item,index){
                        $($('.countdown_m')[index]).find('.data').text(item);
                    });
                }

                // 秒
                if($('.countdown_s')){
                    var $ele_s = $('.countdown_s');
                    $.map(String(s).split(''), function(item,index){
                        $($('.countdown_s')[index]).find('.data').text(item);
                    });
                }

                // 毫秒
                if($('.countdown_ms')){
                    var $ele_ms = $('.countdown_ms');
                    $.map(String(ms).split(''), function(item,index){
                        $($('.countdown_ms')[index]).find('.data').text(item);
                    });
                }
            }
            // 每10秒整更新一次(中標者和倒數計時的時間)
            if (s=="10" || s=="20" || s=="30" || s=="40"|| s=="50" || s=="00" ) {
                $.get("https://ws.saja.com.tw/site/lib/cdntime.php", function(data) {
				// $.get("<?php echo UNIX_TS_URL; ?>", function(data) {
                    now_time = parseInt(data); //系統時間
                });
                // 使否有得得標
                var PURLs2 = "<?php echo BASE_URL.APP_DIR;?>/dream_product/saja/?json=Y&productid="+chkprodid;
                var who_Win2;
                function  name_who2() {
                    var WHOURLS2 = "<?php echo BASE_URL.APP_DIR;?>/bid/bidlist/?json=Y&productid="+chkprodid+"&type=Y";
                    $.ajax({
                        type: 'POST',
                        url: WHOURLS2,
                        dataType: 'json',
                        success: function(msg) {
                            who_Win2=msg.retObj.data.nickname;
                            return who_Win2;
                        }
                    
                    })
                }
                name_who2();
                $.ajax({
                    type: 'POST',
                    url: 'http://test.shajiawang.com/site/dream_product/saja/?json=Y&productid='+chkprodid,
                    dataType: 'json',
                    success: function(msg) {
                    var timeout =msg.retObj.closed;
                    // console.log(timeout);

                    if(timeout == "Y"){
                        clearInterval(timer);
                        // var who = msg.retObj.final_winner_userid;
                        // alert('恭喜得標者~');
                        // console.log("srart1");
                        if(count3 == 0){
                            $('.Congratulations').append( 
                                '<header>恭喜</header>'+
                                '<div>'+msg.retObj.name+'</div>'+
                                '<footer><figure><img src="<?PHP echo APP_DIR; ?>/static/img/icon_Trophy.png"></figure><p>得標者</p>'+
                                '<span>'+who_Win2+'</span>'+
                                '</footer> ');
							var lk = "'<?php echo BASE_URL.APP_DIR;?>/bid/detail/?productid="+chkprodid+"'";	
							$('.Congratulations_footer').append('<a class="btn_go" href="#" onclick="javascript:location.href='+lk+'"><span>確定</span></a>');
                            $(".th_finish").addClass('on');
                        }
                        count3=2;
                    }else if(timeout == "NB"){
                            // alert('流標嚕~');
                           
                        //    console.log(count);
                        // console.log("end");
                            clearInterval(timer);
                            if(count == 0){
                                $('.Congratulations').append( 
                                '<header>流標</header>'+
                                '<div>此商品流標</div>'
                                );
                                $('.Congratulations_footer').append('<a class="btn_go" href="#" onclick="javascript:location.href='+nblink+'"><span>確定</span></a>');
                                $(".th_finish").addClass('on');
                            }
                            count=1;
                            
    
                    }
       
                        
                    }
                })
                autoUpdate('');
            }
        } else if (!lost_time || lost_time <= 0) {
            clearInterval(timer);
            $('.countdown').html(end_plus + " 已結標 ");
            // alert("本商品已結標 !!");
            // location.href = "/site/product/";
            var PURLs = "<?php echo BASE_URL.APP_DIR;?>/dream_product/saja/?json=Y&productid="+chkprodid;
            var who_Win;
            function  name_who() {
                    var WHOURLS = "<?php echo BASE_URL.APP_DIR;?>/bid/bidlist/?json=Y&productid="+chkprodid+"&type=Y";
                    $.ajax({
                        type: 'POST',
                        url: WHOURLS,
                        dataType: 'json',
                        success: function(msg) {
                            who_Win=msg.retObj.data.nickname;
                            return who_Win;
                        }
                    
                    })
                }
                name_who();
				 var nblink = "<?php echo BASE_URL.APP_DIR;?>";
 			

				$.ajax({
                    type: 'POST',
                    url: PURLs,
                    dataType: 'json',
                    success: function(msg) {
                     
                    var timeout =msg.retObj.closed;
                    // console.log(timeout);
                    if(timeout == "Y"){
                        clearInterval(timer);
                        // var who = msg.retObj.final_winner_userid;
                        // alert('恭喜得標者~');
                        console.log("srart2");
                        if(count4 == 0){
                            $('.Congratulations').append( 
                                '<header>恭喜</header>'+
                                '<div>'+msg.retObj.name+'</div>'+
                                '<footer><figure><img src="<?PHP echo APP_DIR; ?>/static/img/icon_Trophy.png"></figure><p>得標者</p>'+
                                '<span>'+who_Win+'</span>'+
                                '</footer>');
								var lk = "'<?php echo BASE_URL.APP_DIR;?>/bid/detail/?productid="+chkprodid+"'";
								$('.Congratulations_footer').append('<a class="btn_go" href="#" onclick="javascript:location.href='+lk+'"><span>確定</span></a>');
                                // $('.Congratulations_footer').append('<a class="btn_go" href="#" onclick="javascript:location.href=<?php echo BASE_URL.APP_DIR;?>/bid/detail/?productid='+ chkprodid +'"><span>確定</span></a>');
                            $(".th_finish").addClass('on');
                        }
                        count4=4;
                    }else if(timeout == "NB"){
                            // alert('流標嚕~');
                            clearInterval(timer);
                            if(count2 == 0){
                                $('.Congratulations').append( 
                                '<header>流標</header>'+
                                '<div>此商品流標</div>'
                                );
                                $('.Congratulations_footer').append('<a class="btn_go" href="#" onclick="javascript:location.href='+nblink+'"><span>確定</span></a>');
                                $(".th_finish").addClass('on');
                            }
                            count2=1;
                    }
       
                        
                    }
                })
          
        }
    };

    function WeiXinShareBtn() {
        if (typeof WeixinJSBridge == "undefined") {
            alert("請先通過微信搜索 殺價王 添加殺價王為好友，通過微信分享文章 :) ");
        } else {
            WeixinJSBridge.invoke('shareTimeline', {
                "title": "殺價王",
                "link": "https://www.saja.com.tw",
                "desc": "殺價王",
                "img_url": ""
            });
        }
    }
	
    function autoUpdate(showReloadIcon) {
        var URLs = "<?php echo BASE_URL.APP_DIR;?>/product/winner/?timestamp=" + new Date().getTime();
        if (showReloadIcon == 'Y') {
            $.mobile.loading('show', {
                text: 'reload..',
                textVisible: true,
                theme: 'b',
                html: ""
            });
        } else {
            $.mobile.loading('hide');
        }
        $.ajax({
            url: URLs,
            data: {
                productid: chkprodid
            },
            type: "GET",
            dataType: 'text',
            success: function(msg) {
                $.mobile.loading('hide');
                var json = JSON && JSON.parse(msg) || $.parseJSON(msg);
                if (json.productid == chkprodid) {
                    // $('#bidded').html(decodeURIComponent(json.name));
                    //假如無人得標或地址沒填寫，則地址(#cf)不顯示
                    //有人得標時增加class套用黑色(#bidded)
                    if (json.name != "目前沒有人得標") {
                        if(json.comefrom){
                            $("#cf").addClass("d-flex");
                        }else{
                            $("#cf").removeClass("d-flex");
                        }
                        $("#bidded").addClass("color-black");
                    }else{
						$("#cf").removeClass("d-flex");
                        $("#bidded").removeClass("color-black");
					}
                    $('#bidfrom').html(json.src_ip);
                    $('#bidfrom').html(decodeURIComponent(json.comefrom));
                }
            },
            error: function(xhr, ajaxOptions, thrownError) {
                //alert(xhr.status);
                //console.log(thrownError);
            }
        });
    }

    $(document).ready(function() {
        clearInterval(timer);
        autoUpdate('');
        var ua = navigator.userAgent.toLowerCase();
        var WS_URL = '<?php echo $config['WS_URL ']; ?>';
        WS_URL = "wss://www.saja.com.tw/";
        if (ua.match(/MicroMessenger/i) == "micromessenger" && ua.match(/Android/i) == "android") {
            WS_URL = "ws://www.saja.com.tw/";
        }
        if (false) {
            $.mobile.loading('show', {
                text: '準備中.....',
                textVisible: true,
                theme: 'b',
                html: ""
            });
            $.ajaxSetup({
                cache: false
            });
            socket = new WebSocket(WS_URL);
            socket.onopen = function(evt) {
                autoUpdate('');
                socket.send('{"AGENT":"' + agent_type + '","PID":"<?php echo $product['
                                productid ']; ?>","UID":"<?php echo $_SESSION['
                                auth_id '];?>","PAGE":"saja","IP":"<?php echo $ip; ?>","MEMO":"' + navigator.appVersion + '"}');
            };
            socket.onmessage = function(ret) {
                var json;
                if ("object" == (typeof ret)) {
                    msg = $.trim(ret.data); 
                    json = JSON && JSON.parse(msg) || $.parseJSON(msg);
                    if (json) {
                        if (json.ACTION == 'NOTIFY' && json.PRODID == chkprodid) {
                            $("#bidded").text(json.WINNER);
							console.log(json);
                            $("#src_ip").text(json.SRC_IP);
                        }
                    }
                }
            };
            socket.onclose = function() {
                if (event.code != 1000) {
                    /*
					  alert("連線異常！！"+event.code);
					  $.get("<?php echo UNIX_TS_URL; ?>",function(data) {
							  now_time = parseInt(data); //系統時間
							  autoUpdate();
							  timer=setInterval(timer_run,1000);
					  });
					  $.mobile.loading('hide');
					  */
                    // exit;
                } else {
                    //console.log("disconnect !!");
                }
                $.mobile.loading('hide');
            };
            socket.onerror = function(event) {
                // alert("您的微信流覽器不支持 ！");
                $.mobile.loading('hide');
                exit;
            };
        }
		$.get("https://ws.saja.com.tw/site/lib/cdntime.php", function(data) {
        // $.get("<?php echo UNIX_TS_URL; ?>", function(data) {
            now_time = parseInt(data); //系統時間
            timer = setInterval(timer_run, 59);
			$.mobile.loading('hide');
        });
    });

	//產品圖幻燈片
    $(document).on('pageshow', function(){ //幻燈片banner
        $('.flexslider').flexslider({
            animation: "slide", //圖片切換方式 (滑動)
            slideshowSpeed: 3000, //自動播放速度 (毫秒)
            animationSpeed: 600, //切換速度 (毫秒)
            directionNav: false, //顯示左右控制按鈕
            controlNav: true, //隱藏下方控制按鈕
            prevText:"", //左邊控制按鈕顯示文字
            nextText:"", //右邊控制按鈕顯示文字
            pauseOnHover: false, //hover時停止播放
            animationLoop: false, //圖片循環
            slideshow: false, //自動播放 (debug用)
            start: function(slider){ //載入第一張圖片時觸發
                $('body').removeClass('loading');
            }
        });
    });

	function nologin() {
		// alert("殺友請先登入 !!");
		location.href="<?PHP echo APP_DIR; ?>/member/userlogin/";
	}
	
    // Wechat
    var dataForWeixin = {
        MsgImg: "<?php echo $img_src; ?>",
        TLImg: "<?php echo $img_src; ?>",
        url: "https://www.saja.com.tw" + APP_DIR + "/product/oscode/?productid=<?php echo $product['productid']; ?>",
        title: "<?php echo $product['name']; ?> 搶殺中-最低且唯一價中標，沒中標100%返還，還不快加入搶殺 ?",
        desc: "<?php if ($product['productid'] != 8015 ) { ?><?php echo $product['price_limit']; ?>元起 熱烈搶殺中，你還琢磨什麼? <?php } else { ?><?php echo $product['name']; ?>低價搶殺中最低且唯一中標，沒中標100%返還你的朋友正在搶殺還不快加入。<?php } ?>",
        fakeid: "",
        callback: function() {}
    };


	var uri = "<?php echo 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
    $.ajax({
        type: "POST",
        url: "http://test.shajiawang.com"+APP_DIR+"/product/weixinSDK/",
        data: { wurl:uri },
        dataType: "text",
        success: function(data) {
            console.log(data);
            var configData = jQuery.parseJSON(data);
            console.log('sig is ' + configData.signature);
            wx.config({
				//debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                appId: configData.appId, // 必填，公众号的唯一标识
                timestamp: configData.timestamp, // 必填，生成签名的时间戳
                nonceStr: configData.nonceStr, // 必填，生成签名的随机串
                signature: configData.signature, // 必填，签名，见附录1
				jsApiList: [      //需要调用的接口
					'onMenuShareAppMessage',
					'onMenuShareTimeline'

				]
            });

			wx.error(function (res) {
				// alert('wx.error: '+JSON.stringify(res));
			});

			wx.ready(function () {
				wx.onMenuShareTimeline({
					title: dataForWeixin.title,     //分享后自定义标题
					link: dataForWeixin.url,   //分享后的URL
					imgUrl: dataForWeixin.MsgImg,  //分享的LOGO
				    desc: dataForWeixin.desc,	// 分享描述
					success: function (res) {
						alert('已分享');
					},
					cancel: function (res) {
						alert('已取消');
					},
					fail: function (res) {
						alert('wx.onMenuShareTimeline:fail: '+JSON.stringify(res));
					}
				});

				wx.onMenuShareAppMessage({
					title: dataForWeixin.title,     //分享后自定义标题
					link: dataForWeixin.url,   //分享后的URL
					imgUrl: dataForWeixin.MsgImg,  //分享的LOGO
				    desc: dataForWeixin.desc,	// 分享描述
					success: function (res) {
						alert('已分享');
					},
					cancel: function (res) {
						alert('已取消');
					},
					fail: function (res) {
						alert('wx.onMenuShareAppMessage:fail: '+JSON.stringify(res));
					}
				});
			
				//分享给朋友
				wx.updateAppMessageShareData({ 
					title: dataForWeixin.title, // 分享标题
					desc: dataForWeixin.desc, // 分享描述
					link: dataForWeixin.url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: dataForWeixin.MsgImg, // 分享图标
					success: function () {
					  // 设置成功
					  alert('已分享');
					}
				});
				
				//分享到朋友圈	
				wx.updateTimelineShareData({ 
					title: dataForWeixin.title, // 分享标题
					link: dataForWeixin.url, // 分享链接，该链接域名或路径必须与当前页面对应的公众号JS安全域名一致
					imgUrl: dataForWeixin.MsgImg, // 分享图标
					success: function () {
					  // 设置成功
					  alert('已分享');
					}
				});					
				
			});

        },
        error: function(data) {
        }

    });	


    //調整標題寬度一致
    var options = {
        dom: {
            ul: '.saja_lists_box',
            li: '.list-group',
            label: '.label',
            input: 'input',
            select: 'select'
        }
    }
    $(function(){
        function changeWidth(dom) {
            
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];
            
            $liW = parseInt($selfLi.width());                         //取得li總寬度   
            
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

            

            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                $selfInput.attr('style','');
                $selfSelect.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                    }
                })
                $selfSelect.each(function(){
                    if($selfSelect.length > 1){                 
                        $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                        
                    }else{
                        $(this).width($remain);
                    }
                })
            })
            
            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
        }
        $(options.dom.ul).each(function(){
            changeWidth($(this));
        })

        //尺寸改變重新撈取寬度
        $(window).resize(function(){
            $(options.dom.ul).each(function(){
                changeWidth($(this));
            })
        })

    })
	

	
	//绑定input change事件
	$("#ispid").unbind("change").on("change",function(){

		lnv.pageloading();
		var file = document.querySelector('#ispid').files[0];
		if(file){
			//验证图片文件类型
			if(file.type && !/image/i.test(file.type)){
				return false;
			}
			var reader = new FileReader();
			reader.onload = function(e){
				//readAsDataURL后执行onload，进入图片压缩逻辑
				//e.target.result得到的就是图片文件的base64 string
				render(file,e.target.result,1);
			};
			//以dataurl的形式读取图片文件
			reader.readAsDataURL(file);
		}
	});

	//定义照片的最大高度
	var MAX_HEIGHT = 480;
	var render = function(file,src,num){
		EXIF.getData(file,function(){
			//获取照片本身的Orientation
			var orientation = EXIF.getTag(this, "Orientation");
			var image = new Image();
			image.onload = function(){
				var cvs = document.getElementById("cvs");
				var w = image.width;
				var h = image.height;
				//计算压缩后的图片长和宽
				if(h>MAX_HEIGHT){
					w *= MAX_HEIGHT/h;
					h = MAX_HEIGHT;
				}
				//使用MegaPixImage封装照片数据
				var mpImg = new MegaPixImage(file);
				//按照Orientation来写入图片数据，回调函数是上传图片到服务器
				mpImg.render(cvs, {maxWidth:w,maxHeight:h,orientation:orientation}, sendImg);
			};
			image.src = src;
		});
	};

	//上传图片到服务器
	var sendImg = function(){
		var cvs = document.getElementById("cvs");
		//调用Canvas的toDataURL接口，得到的是照片文件的base64编码string
		var data = cvs.toDataURL("image/png");
		//base64 string过短显然就不是正常的图片数据了，过滤の。
		if(data.length<48){
			console.log("data error.");
			return;
		}

		var formData = new FormData();
		//formData.append('pic', reader.result );
		formData.append('pic', data );
		formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
		formData.append('pic_name','issuestemp');
		formData.append('filedir','site/product/');

		$.ajax(
			{
				url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
				type : 'POST',
				data : formData,
				processData: false,  // tell jQuery not to process the data
				contentType: false,  // tell jQuery not to set contentType
				success : function(data)
				{
					load_img();
				}
			}
		);
	};


	function load_img()
	{
	  destory_img();

	  var el = document.getElementById('new_logo');
		vanilla = new Croppie(el, {
		viewport: { width: 196, height: 196 },
		boundary: { width: 300, height: 300 },
		showZoomer: true,
		enableOrientation: true
	  });

	  vanilla.bind({
		  url: '<?php echo BASE_URL.APP_DIR;?>/images/site/product/_'+<?php echo $_SESSION['auth_id'];?>+'_issuestemp.png?t=' + new Date().getTime(),
	  });

	  document.getElementById('dream_product').style.display = "none";
	  document.getElementById('actions').style.display = "inline";
	  lnv.destroyloading();
	}

	function destory_img()
	{
	  document.getElementById('new_logo').innerHTML="";
	}

	function cancel_edit()
	{
	  location.reload();
	}

	function page_reload()
	{
	  location.reload();
	}

	function rotate_right()
	{
	   vanilla.rotate(90);
	}

	function rotate_left()
	{
	   vanilla.rotate(-90);
	}

	function save_pic()
	{
		var pic = vanilla.result('canvas','viewport');
		console.log(pic);

		pic.then(function(value){
		console.log(value);
		
		var count = 1;
		var formData = new FormData();
		formData.append('pic',value);
		formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
		formData.append('pic_name','<?php echo md5($cdnTime."issues");?>_'+count);
		formData.append('filedir','dream/products/');
		//console.log(count);

		$.ajax({
			url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c2.php',
			type : 'POST',
			data : formData,
			processData: false,  // tell jQuery not to process the data
			contentType: false,  // tell jQuery not to set contentType
			success : function(data)
				{
					var tf = document.getElementById('thumbnail').value="_<?php echo $_SESSION['auth_id'];?>_<?php echo md5($cdnTime."issues");?>_"+count+".png";
					document.getElementById('dream_product').style.display = "block";
					document.getElementById('actions').style.display = "none";
					document.getElementById("pid1").classList.remove('d-none');
					document.getElementById("preview_logo"+count).style.display = "";
					document.getElementById("preview_logo"+count).src = "<?php echo BASE_URL.APP_DIR;?>/images/dream/products/_<?php echo $_SESSION['auth_id'];?>_<?php echo md5($cdnTime."issues");?>_"+count+".png?t=" + new Date().getTime();
					if (parseInt(count) < 1){
						document.getElementById('pidcount').value = 1;
					}else{
						document.getElementById('pid-add').style.display = "none";
						document.getElementById('pid-add').classList.remove('d-flex');
						document.getElementById('pid-add').classList.add('d-none');
					}
					$('html,body').animate({
					scrollTop: $('#dream_add').offset().top}, 1000);
				}
			});
		});
	}

	
	function ScrollTo(id_name) {
		$('html,body').animate({
		scrollTop: $('#'+id_name).offset().top}, 1000);
	}
	
	// function prddata(nid,pic,pic_name,content,purchase_url) {	
	function prddata(nid,chkprodid,chkshdid) {	
		
		$('html,body').animate({
		scrollTop: $('#dream_add').offset().top}, 1000);
		
		for(var i = 1; i < 6; i++) {
			if(document.getElementById('b'+i)) {
			   document.getElementById('b'+i).style.opacity = '1';
		    }
		}
		document.getElementById('b'+nid).style.opacity = '0.5';

		document.getElementById("pid1").classList.remove('d-none');
		document.getElementById('pid-add').style.display = "none";
		
		$.ajax({
			type: 'GET',
			url: '<?php echo BASE_URL.APP_DIR;?>/dream_product/get_history_detail_info/?json=Y&productid='+chkprodid+'&shdid='+chkshdid,
			dataType: 'json',
            success: function(msg) {
                // $.mobile.loading('hide');
				document.getElementById("preview_logo1").src = '<?php echo BASE_URL.APP_DIR;?>/images/dream/products/'+msg.thumbnail_filename; 
				document.getElementById('thumbnail').value= msg.thumbnail_filename;
				$('#dpname').val(msg.title); 
				$('#dpcontent').val(msg.content);
				$('#dpaddress').val(msg.purchase_url);
            }
        });
		
		// document.getElementById("preview_logo1").src = '<?php echo BASE_URL.APP_DIR;?>/images/dream/products/'+pic; 
		// document.getElementById('thumbnail').value= pic;
		// $('#dpname').val(pic_name); 
		// $('#dpcontent').val(content);
		// $('#dpaddress').val(purchase_url);
	}	
</script>