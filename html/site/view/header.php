<?php
    $header = _v('header');
    if($header=='N') {
        return;
    }
    $noback = _v('noback');
	$download_app = _v('download_app');
    $adctrl = _v('adctrl');
?>
<?php if (!is_weixin()) { ?>
    <div class="navbar sajaNavBar d-flex align-items-center">
		<div class="sajaNavBarIcon back">
            <?php if (empty($noback) && $noback !== "Y") { ?>
            <a href="javascript:history.back();" >
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
            </a>
            <?php } ?>
        </div>
		<div class="navbar-brand">
			<h3><?php if (!empty($tpl->title)){ echo $tpl->title; } else { echo $tpl->page_header;} ?></h3>
		</div>

		<?php if (!empty($download_app) && $download_app=="Y") { ?>
		<div class="sajaNavBarIcon ">
			<a href="javascript:;" onclick="createMsgModal();" data-iconpos="right">
				<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_download-417.png" height="30px">
			</a>
        </div>
		<?php } ?>
        <?php if (!empty($adctrl) && $adctrl=="Y") { ?>
        <div class="sajaNavBarIcon ">
            <a href="javascript:;" onclick="show_history();" data-iconpos="right">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_download-417.png" height="30px">
            </a>
        </div>
        <?php } ?>
		<?php /*
		<div class="sajaNavBarIcon qrcode">
			<a href="#left-panel<?php get_panel_id(); ?>" data-iconpos="right">
				<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/hamburger.png">
			</a>
        </div>
		*/?>

	</div>
    <script>
        // 不是在微信  也不是在appbsl的app裏  才顯示Navigator bar

        $(document).ready(function() {
            // if(is_kingkr_obj()) {
            //     // controllNavigateLayout(0);
            // }
            //if(!is_kingkr_obj()) {
            $('#navibar').show();
            //}
        });
    </script>
    <!-- 往下捲動後 header 增加陰影 -->
    <script>
        $(document).ready(function() {
            var $header = $(".sajaNavBar"),
                $lastScrollY = $header.height();
            $(window).scroll(function(){
                var st = this.scrollY;
                // 捲動超過 header 才加陰影
                if( st < $lastScrollY) {
                    $header.removeClass("navShadow");
                }else{
                    $header.addClass("navShadow");
                }
            });
        });
        $(window).on('load',function(){
        //側邊攔
            var $panelBtn = $('.sajaNavBarIcon.Burger').find('a[href^="#left-panel"]');
            var $panelBox = $('.sajaPanel');
            var $panelClose = $('.sajaPanel').find('[data-rel="close"]');
            var $coverClose = $('.ui-panel-dismiss');
            $panelBtn.on('click', function(){
                if(!$panelBox.hasClass('.ui-panel-open')){
                    $('body').addClass('modal-open');
                    $('[data-role="page"]').find('[class$="-content"]').addClass('modal-open');
                }
            });
            $panelClose.on('click', function(){
                if(!$panelBox.hasClass('.ui-panel-closed')){
                    $('body').removeClass('modal-open');
                    $('[data-role="page"]').find('[class*="modal-open"]').removeClass('modal-open');
                }
            });
            $coverClose.on('click', function(){
                if(!$panelBox.hasClass('.ui-ui-panel-closed')){
                    $('body').removeClass('modal-open');
                    $('[data-role="page"]').find('[class*="modal-open"]').removeClass('modal-open');
                }
            })
        })
    </script>

	<?php if (!empty($download_app) && $download_app=="Y") { ?>
	<script>
	//判斷使用者裝置
    var $device;        //android, ios, other
	(function whDevice() {
        //判斷使用者裝置是否支援觸控功能，判斷是否為行動版 (有觸控功能的筆電也判斷為行動版)
        function isMobile() {
            try{ document.createEvent("TouchEvent"); return true; }
            catch(e){ return false;}
        }

        //判斷使用者裝置為Android或iOS
        function isDevice() {
            var u = navigator.userAgent;
            var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android終端
            var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios終端
            if(isAndroid){
                return 'Android';
            }else if(isiOS){
                return 'iOS';
            }
        }

        if(isMobile()){
            var $mobile = isDevice();
            switch($mobile){
                case 'Android':
                    $device = 'android';
                    break;
                case 'iOS':
                    $device = 'ios';
                    break;
            }
        }else{
            $device = 'other';
        }

    }());

    function toAndroid() {
        location.href='https://play.google.com/store/apps/details?id=tw.com.saja.userapp';
    }
	function toiOS() {
		location.href='https://itunes.apple.com/tw/app/id1441402090';
    }
	function createMsgModal(){
        console.log(1);
		if($device == 'android'){
			toAndroid();
		}else if($device == 'ios'){
			toiOS();
		}else{
			$('body').addClass('modal-open');
			$('body').append(
				$('<div class="downloadModalBox d-flex justify-content-center align-items-center"/>')
				.append(
				$('<div class="modal-content"/>')
					.append(
					$('<div class="modal-body"/>')
						.append(
							$('<div class="qrcodebox"><img src="https://www.saja.com.tw/site/phpqrcode/?data=https://www.saja.com.tw/site/mall/qrpage" ></div>')
						)
						.append(
							$('<div class="modal-title"/>')
							.append(
								$('<p>為了安全起見</p>')
							)
							.append(
								$('<p>本功能僅能在APP使用</p>')
							)
							.append(
								$('<p>請用手機掃描QRcode</p>')
							)
						)
					)
				)
			)
		}
    }
	</script>
	<?php } ?>

<?php } ?>
