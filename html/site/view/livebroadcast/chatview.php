<?php
// echo "<pre>".print_r($_SESSION,1)."</pre>";
// exit();
?>

<html>
<head>
	<title>主持人專用聊天室</title>
	<meta name="viewport" content="width=device-width, initial-scale=1" >
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<style>
		body{
			background-color: #343a40;
			font-family: "微軟正黑體";
		}
		.toast{
			font-size: 2rem;
			min-width: 600px;
		}
		.chat_room{
			overflow-y: auto;
			overflow-x: hidden;
			max-height: calc(100vh - 50px); 
		}
		.fixed-bottom{
			bottom: 10px;
		}
		.btn-warning{
			background-color: #df901a;
		    border-color: #df901a;
			color: #fff;
		}
	</style>
</head>
<body>
	<div class="container" >
		<div class="row justify-content-md-center">
			<div class="chat_room"></div>
		</div>
	</div>

	<div class="container">
		<div class="row justify-content-md-center">
			<div class="fixed-bottom col-12" >
				<div class="row">
					<div class="input-group col-2">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i class="material-icons">person</i>
							</span>
						</div>
						<input type="text" class="form-control name-input" value="殺小編">
					</div>
					<div class="input-group col">
						<div class="input-group-prepend">
							<span class="input-group-text">
								<i class="material-icons">chat_bubble_outline</i>
							</span>
						</div>
						<input type="text" class="form-control chatbox-input" value="">
						<div class="input-group-append">
							<button class="btn btn-warning chatbox-btn" type="button">發送</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

<script>
	$(document).ready(function($) {
		ws_chat();
	});
	// 聊天室 websocket
	function ws_chat() {
		var wsUri = "wss://ws.saja.com.tw:3334";
		websocket = new WebSocket(wsUri);
		websocket.onmessage = function(evt) { 
			onMessage(evt) 
		};
		websocket.onclose = function() { 
			setTimeout(function() {ws_chat()}, 100);
		}; 
		websocket.onerror = function() {
			setTimeout(function() {ws_chat()}, 100);
		};

		function onMessage(evt){
			var today = new Date();
			var year = today.getFullYear();
			var mon = (today.getMonth()+1<10) ? "0"+(today.getMonth()+1) : (today.getMonth()+1);
			var day = (today.getDate()<10) ? "0"+(today.getDate()) : today.getDate();
			var date = year+'-'+mon+'-'+day;

			var hour = (today.getHours()<10) ? "0"+(today.getHours()) : today.getHours();
			var min = (today.getMinutes()<10) ? "0"+(today.getMinutes()) : today.getMinutes();
			var sec = (today.getSeconds()<10) ? "0"+(today.getSeconds()) : today.getSeconds();
			var time = hour + ":" + min + ":" + sec;

			var dateTime = date+' '+time

			var json_data = JSON.parse(evt.data);
			if(json_data.actid == "CHAT_MSG"){
				var chat_text = '\
				<div class="d-flex align-items-center col-12" style="min-height: 150px;">\
					<div class="toast fade show" role="alert" aria-live="assertive" aria-atomic="true">\
						<div class="toast-header">\
								<strong class="mr-auto">'+json_data.name+'</strong>\
								<small>'+dateTime+'</small>\
						</div>\
						<div class="toast-body">\
							'+json_data.msg+'\
						</div>\
					</div>\
				</div>'
				$(".chat_room").append(chat_text);
				$('.chat_room').scrollTop($('.chat_room')[0].scrollHeight);
			}
		}
	}

	function doSend(message){
		nickname = $(".name-input").val();
		nickname = (nickname.trim() == "") ? "殺小編" : nickname.trim();
		var json_msg = '{"actid":"CHAT_MSG", "name":"'+nickname+'", "msg":"'+message+'"}';
		websocket.send(json_msg);
	}

	// enter送出
	$(".chatbox-input").keypress(function(e){
		code = (e.keyCode ? e.keyCode : e.which);
		if (code == 13){
			$(".chatbox-btn").click();
		}
	});

	/* 發送 */
	$(".chatbox-btn").click(function(event) {
		var message = $(".chatbox-input").val();
		if (message!="") {
			doSend(message);
		}
		$(".chatbox-input").val("");
	});

	
</script>