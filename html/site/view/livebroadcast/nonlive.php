<html>
    <head>
        <meta charset="utf-8">

        <meta property="og:url"           content="<?php echo BASE_URL.$_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="新殺價王" />
		<meta property="og:description"   content="創新殺價式購物，突破傳統，出價最低唯一者可獲得商品，免費送下標券" />

		<meta property="og:image"         content="<?PHP echo APP_DIR; ?>/static/img/activity-share-imgcat.png" />
		<meta property="og:image:type"    content="image/png">
        <meta property="og:image:width"   content="1201">
        <meta property="og:image:height"  content="630">
        <title>殺價王</title>
        <!-- RWD -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <meta name="apple-mobile-web-app-capable" content="yes">
        <!--Web，Android，Microsoft和iOS（iPhone和iPad）應用程式圖標-->
        <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?PHP echo APP_DIR; ?>/static/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?PHP echo APP_DIR; ?>/static/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?PHP echo APP_DIR; ?>/static/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

		<!-- 瀏覽器初始化 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/reset.css">

        <!-- flexbox 框架 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/flexbox.css">

        <!-- shareoscode.CSS -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/shareoscode.min.css">

        <script src="<?PHP echo APP_DIR; ?>/static/js/jquery-2.1.4.js"></script>

        <script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/vendor/base64/base64.js"></script>

        <!-- 頁面loading -->
        <script type="text/javascript" src="<?php echo APP_DIR;?>/static/js/es-page-loading.js"></script>
	</head>
	<style>
    body{
        height: 100vh;
        overflow-y: hidden;
    }
    #nonlive{
        display: flex;
        align-items: flex-start;
        justify-content: center;
    }
    .nonlive_Img{
        width: 80%;
        position: relative;
        background-size: cover;
        height: 100vh;
        background-position: center;
    }
    .nonlive_Img img{
        width: 100%;
        max-height: 100%;
    }
    .top-btn{
        position: absolute;
        z-index: 999;
        top: 20px;
        text-align: center;
        right: 20px;
    }
    .close-btn img{
        width: 30px;
    }

    .nonlive_btn{
        position: absolute;
        z-index: 997;
        bottom: 15%;
        font-weight: bold;
        cursor: pointer;
        width: 100%;
        height:0;
        text-align: center;
    }
    .free-btn{
        background-color: #fff;
        color: #ff7110;
        font-size: 1.25rem;
        display: inline-block;
        padding: 1rem 4rem;
        margin: 1rem .5rem 0;
        border-radius: 100vh;
        cursor: pointer;
    }


    .free-btn:hover{
        background-color: #ff7110;
        color: #0b144c;
    }
    .btn{
        text-align: center;
        display: block;
        width: 30%;
        background-color: hsla(0, 0%, 0%, 0.34);
        padding: 10px;
        margin: 0 auto;
        border-radius: 6px;
    }
    .btn img {
        width: 50px;
        margin-bottom: 10px;
    }
    .btn span{
        display: block;
        font-size: 14px;
        color:#FFFFFF;
        font-weight:bold;
    }
    @media (max-width: 992px){
        #nonlive{
            height: 100vh;
            overflow-y: hidden;
        }
        .nonlive_Img{
            width: 100vh;
        }

    }
	</style>

    <body >
        <?php
            $json = _v('json');

        ?>
        <div id="mask">
        </div>
        <div id="nonlive">
            <div class="nonlive_Img" style="background-image: url('<?php echo BASE_URL.IMG_DIR.'/nonlive.jpg';?>')">
            <?php if($json!='Y'){?>
            <div class="top-btn">
                <a href="<?php echo BASE_URL.APP_DIR;?>" class="close-btn" id="close">
                    <img src="<?php echo BASE_URL.IMG_DIR.'/close.png';?>" alt="close">
                </a>
            </div>
            <?php } ?>
                <!-- <img src="<?php echo BASE_URL.IMG_DIR.'/nonlive.jpg';?>" alt="nonlive"> -->
            <div class="nonlive_btn">
				<div id="free"  class="free-btn">領取免費殺價卷</div>
			</div>
            </div>

        </div>
		<script>
        

        $('#free').click(function(){
			$.ajax({
				url: "<?php echo BASE_URL.APP_DIR;?>/broadcast/lb_room/",
				type:"POST",
				dataType:'JSON',

				success: function(msg){

					var obj ;
					if((typeof msg)=="string") {
						obj = JSON.parse(trim(msg));
					} else if((typeof msg)=="object") {
						obj = msg;
					}
					lbroom = obj["retObj"]["data"];
                    <?php if($json!='Y'){?>
                        document.location.href= lbroom['promote_link'];
                    <?php } ?>
                    <?php if($json=='Y'){?>
                        document.location.href= lbroom['promote_link'] + '&json=Y';
                    <?php } ?>         

				},
			});
		});
		</script>
    </body>
</html>
