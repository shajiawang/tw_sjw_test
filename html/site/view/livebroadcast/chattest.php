<html>
	<head>
		<meta charset="utf-8">

		<meta property="og:url"           content="<?php echo BASE_URL.$_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="新殺價王" />
		<meta property="og:description"   content="創新殺價式購物，突破傳統，出價最低唯一者可獲得商品，免費送下標券" />

		<meta property="og:image"         content="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/activity-share-img-live.png" />
		<meta property="og:image:type"    content="image/png">
		<meta property="og:image:width"   content="1201">
		<meta property="og:image:height"  content="630">
		<title>殺價王 直播活動</title>
		<!-- RWD -->
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
		<!--Web，Android，Microsoft和iOS（iPhone和iPad）應用程式圖標-->
		<link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192" href="<?PHP echo APP_DIR; ?>/static/img/favicon/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-16x16.png">
		<link rel="manifest" href="<?PHP echo APP_DIR; ?>/static/img/favicon/manifest.json">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="<?PHP echo APP_DIR; ?>/static/img/favicon/ms-icon-144x144.png">
		<meta name="theme-color" content="#ffffff">

		<!-- 瀏覽器初始化 -->
		<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/reset.css">

		<!-- flexbox 框架 -->
		<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/flexbox.css">

		<!-- shareoscode.CSS -->
		<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/shareoscode.min.css">

		<script src="<?PHP echo APP_DIR; ?>/static/js/jquery-2.1.4.js"></script>

		<script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/vendor/base64/base64.js"></script>

		<!-- 頁面loading -->
		<script type="text/javascript" src="<?php echo APP_DIR;?>/static/js/es-page-loading.js"></script>

		<script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
	</head>
	<style>
		body{
			/* background: #eee; */
		}
		.bt-btn{
			width: 100%;
			position: absolute;
			z-index: 997;
			bottom:3%;
			text-align: center;
		}
		.btn{
			text-align: center;
			display: block;
			width: 30%;
			background-color: hsla(0, 0%, 0%, 0.5);
			padding: 10px;
			margin: 0 auto;
			border-radius: 6px;
		}
		.btn img {
			width: 50px;
			margin-bottom: 10px;
		}
		.btn span{
			display: block;
			font-size: 14px;
			color:#FFFFFF;
			font-weight:bold;
		}
		
		.top-btn{
			position: absolute;
			z-index: 999;
			top: 20px;
			text-align: center;
			right: 20px;
		}

		.close-btn{

			display: block;

		}
		.chat-btn,
		.share-btn{
			position: absolute;
			bottom: 3%;
			background-color: hsla(0, 0%, 0%, 0.34);
			padding: 10px;
			border-radius: 100%;
			z-index: 998;
			height: 20px;
			align-items: center;
			display: flex;
		}
		.share-btn{
			right: 40px;
		}
		.chat-btn{
			left: 40px;
		}
		.ticket-btn{
			position: absolute;
			bottom: 10%;
			right: 20px;
			z-index: 997;
		}
		.ticket-btn img{
			width: 70px;
		}
		.chat-btn img,
		.share-btn img{
			width: 20px;
		}

		.close-btn img{
			width: 30px;
			margin-bottom: 0px;
		}

		.video-pic{
			background: #000000; 
			position: absolute; 
			z-index: 998; 
			width: 100%; 
			height: 100%;
		}

		.video-pic img{
			width:100%; 
			padding-top:30%
		}

		.chatbox{
			position: fixed;
			bottom: 0;
			width: 100%;
			max-width: 500px;
			display: inline-flex;
			margin-bottom: 10px;
			justify-content: center;
			z-index: 999;
			display: none;
		}
		.chatbox-input{
			width: 76%;
			outline: none;
			border-radius: 20px;
			padding-left: 10px;
			padding-right: 10px;
			height: 30px;
			border: 1px solid #DEDEDE;
		}
		.chatbox-btn{
			width: 16%;
			height: 30px;
			border-radius: 17.5px;
			border:0;
			background-color:#F9A823;
			color:#fff;
			padding: 5px 16px;
			margin-left: 6px;
			font-size: 15px;
		}

		.chat_room{
			z-index: 997;
			position: fixed;
			width: 100%;
			max-width: 500px;
			height: 140px;
			padding: 15px;
			bottom: 17%;
			color: #ffffff;
			overflow-y: auto;
			background-color: rgba(0,0,0,0.01);
			left: 50%;
			transform: translateX(-50%);
			box-sizing: border-box;
		}
		.chat_room p{
			margin: 5px;
			font-size: 17px;
			word-break: break-all;
		}
		.chat_mask{
			display: none;
			z-index: 996;
			width: 100%;
			height: 35%;
			position: absolute;
			bottom: 0;
			background: -moz-linear-gradient(bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.7) 15%,rgba(0,0,0,0.5) 11%,rgba(0,0,0,0.1) 100%); /* FF3.6-15 */
			background: -webkit-linear-gradient(bottom, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.7) 15%,rgba(0,0,0,0.5) 11%,rgba(0,0,0,0.1) 100%); /* Chrome10-25,Safari5.1-6 */
			background: linear-gradient(to top, rgba(0,0,0,0.7) 0%,rgba(0,0,0,0.7) 15%,rgba(0,0,0,0.5) 11%,rgba(0,0,0,0.1) 100%) /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
		}
		#loading{
			width: 100vw;
			height: 100vh;
			z-index: 990;
			background-color: #000;
			position: absolute;
			top: 0;
			justify-content: center;
			align-items: center;
			display: none;
		}
		#loading img{
			width: 50px;
		}
		#mask{
			width: 100vw;
			height: 100vh;
			z-index: 990;
			background-color: #000;
			opacity: 0.2;
			position: absolute;
			display: none;
		}
		.video-container{
			width: 100vw;
			height: 100vh;
		}
		#lb_player{
			position: absolute;
			left: 50%;
			transform: translateX(-50%);
		}
		.user_name{
			color: #dddddd;
		}

	</style>

	<body id="sajabody" style="overflow: hidden;" >
		<div id="mask">
		</div>
		<div id="shareoscode-activityhome" style="overflow: hidden;">
			<div class="video-container">
				<div id="lb_player"></div>
				<!-- JS載入youtube ifram框架-->
				<!-- JS設定youtube 播放器-->
			</div>		
			<div class="top-btn">
				<a href="<?php echo BASE_URL.APP_DIR;?>" class="close-btn" id="close">
					<img src="<?php echo BASE_URL.IMG_DIR;?>/close.png" alt="">
				</a>
			</div>


			<div class="ticket-btn">
				<a href="javascript:void();" id='ticket'>
					<img src="<?php echo BASE_URL.IMG_DIR;?>/free_ticket.png" alt="">
				</a>
			</div>

			<div id="bottom-btn">
			
				<div class="bt-btn">
					<a href="javascript:void();" class="btn" id='bidnow'>
						<img src="<?php echo BASE_URL.IMG_DIR;?>/hammer.png" alt="">
						<span>立即下標</span>
					</a>
				</div>
				
				<div class="share-btn">
					<a href="javascript:void();"  id='share' target="_blank">
						<img src="<?php echo BASE_URL.IMG_DIR;?>/free_share.png" alt="">
					</a>
				</div>
				
				<div class="chat-btn" id="chat">
					<a href="javascript:void();" >
						<img src="<?php echo BASE_URL.IMG_DIR;?>/free_chat.png" alt="">
					</a>
				</div>
			</div>

			<div id="chatbox" class="chatbox">
				<input type="text" class="chatbox-input">
				<input type="button" value="發送" class="chatbox-btn">
			</div>

			<div class="getOscodeModal modalstyle" id="pop_msg" style="top:30%; display:none;">
				<div class="modal-overtime" id="msg_content"></div>
				<div class="modal-btn-box d-flex">
					<div class="then-btn go-btn" id="close_pop" style="margin:0 auto;width:50%">關閉</div>
				</div>
			</div>
		</div>
		<div class="chat_mask">
			
		</div>
		<div class="chat_room"></div>
		
		<div id="loading">
			<img src="<?php echo BASE_URL.IMG_DIR;?>/loading.svg" alt="">
		</div>

		<script>

		$(document).ready(function () {

			ws_chat();

			//取裝置長寬
			w= $(window).width();
			

			if(isMobile()){
				h= $(window).height() + 100;
				$("#lb_player").css("top","-5px"); /* 影片上移遮住黑邊 */
			}else{
				h= $(window).height();
			}
			console.log(w, h);

			get_lbroom();


		});

		//取直播間資料
		function get_lbroom(){
			$.ajax({
				url: "<?php echo BASE_URL.APP_DIR;?>/broadcast/lb_room/",
				type:"POST",
				dataType:'JSON',
				
				success: function(msg){

					var obj ;
					if((typeof msg)=="string") {
						obj = JSON.parse(trim(msg));
					} else if((typeof msg)=="object") {
						obj = msg;
					}
					lbroom = obj["retObj"]["data"];
					if(obj["retObj"]["data"] == false) {
						document.location.href= lbroom['adpage_link'];
					}
					// lbroom['ontime'] = '2019-06-13 12:13:00';
					ontime = new Date(lbroom['ontime']).getTime();
					console.log(obj["retObj"]["data"]);
					now = new Date().getTime();
					if(ontime > now){
						document.location.href= lbroom['adpage_link'];

					}else{
						$("#videonotyet").css("display","none");

						//載入 youtube 框架
						var tag = document.createElement('script');
						tag.src = "https://www.youtube.com/iframe_api";
						var firstScriptTag = document.getElementsByTagName('script')[0];
						firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

						// $("#lb_player").css("position","absolute");
						// left_value = (((h/720)*1280) - w) / 2;
						// // $("#lb_player").css("left","-" + left_value + "px");

						

						var url_path_arr = new URL(lbroom['lb_url']).pathname.split("/");
						//設定youtube vedioID
						yt_videoid = url_path_arr[1];
						//分享
						var share = "http://www.facebook.com/share.php?u=" + lbroom['share_link']
						$("#share").attr("href", share );

						//領卷
						var promote = lbroom['promote_link']
						$("#ticket").attr("href", promote );


					}
				},
			});
		}

		//點擊立即殺價
		$('#bidnow').click(function(){
			$.ajax({
				url: "<?php echo BASE_URL.APP_DIR;?>/product/get_lb_product_now/",
				type:"POST",
				dataType:'JSON',

				success: function(msg){
					var obj ;
					if((typeof msg)=="string") {
						obj = JSON.parse(trim(msg));
					} else if((typeof msg)=="object") {
						obj = msg;
					}
					//console.log(lbproduct[0]['name']);
					//obj["retCode"] = 1;
					if(obj["retCode"] == 1){
						//alert('今日商品已全數殺價完畢！');
						$("#msg_content").text(obj["retMsg"]);
						$("#pop_msg").css("display","");
					}else if(obj["retCode"] == 2){
						document.location.href="<?php echo BASE_URL.APP_DIR;?>/product/saja/?channelid=1&productid=" + obj['retObj']['product_data']['productid'];
					}else if(obj["retCode"] == 3){
						//alert('下一檔商品將於' + obj["retObj"]['next_start_in'] + '秒後開始殺價');
						$("#msg_content").text(obj["retMsg"]);
						$("#pop_msg").css("display","");
					}
				},
			});
		});
	
		$('#close_pop').click(function(){
			$("#pop_msg").css("display","none");
		});

		/* 聊天室輸入框開啟 */
		$('#chat').click(function(){
			$("#bottom-btn").css("display","none");
			$("#chatbox").css("display","inline-flex");
			$(".chatbox-input").focus();

			/* 點擊輸入框範圍以外關閉輸入框 */
			$('#mask').click(function(){
				$("#bottom-btn").css("display","block");
				$("#chatbox").css("display","none");
			})
		});

		$('#loading').click(function(){
			alert('loading')
		});

		// 聊天室 websocket
		var nickname = (Cookies.get('auth_nickname')==undefined)? "訪客" : Cookies.get('auth_nickname');
		function ws_chat() {
			var wsUri = "wss://ws.saja.com.tw:3334";
			websocket = new WebSocket(wsUri);
			websocket.onmessage = function(evt) { onMessage(evt) };

			function onMessage(evt){
				var json_data = JSON.parse(evt.data);
				console.log('json_data', json_data);
				if(json_data.actid == "CHAT_MSG"){
					var chat_text = "<p>"+"<span class='user_name'>"+json_data.name+"</span>"+": "+json_data.msg+"</p>";
					$(".chat_room").append(chat_text);
					$('.chat_room').scrollTop($('.chat_room')[0].scrollHeight);
				}
			}
		}

		function doSend(message){
			var json_msg = '{"actid":"CHAT_MSG", "name":"'+nickname+'", "msg":"'+message+'"}';
			websocket.send(json_msg);
		}

		$(".chatbox-input").keypress(function(e){
			code = (e.keyCode ? e.keyCode : e.which);
			if (code == 13){
				$(".chatbox-btn").click();
			}
		});


		/* 發送 */
		$(".chatbox-btn").click(function(event) {
			var message = $(".chatbox-input").val();
			if (message!="") {
				doSend(message);
			}
			$(".chatbox-input").val("");
			$("#bottom-btn").css("display","block");
			$("#chatbox").css("display","none");
			$(".chat_room").show();
		});

		var player;
		function onYouTubeIframeAPIReady() {
				player = new YT.Player('lb_player', {
				height: h +'px',
				width: (h/720)*1280 + 'px',
				// height: 100%,
				// width: 100%,
				videoId: '',
				//videoId: '7zEAJbKeG64',
				playerVars : {
					'playsinline':1,
					'autoplay': 1,
					'autohide': 0,
					'showinfo': 0,
					'modestbranding': 1,
					'origin':'https://www.saja.com.tw',
					'controls':0,
					'color':'red',
					'disablekb':1,
					'iv_load_policy': 3, //影片註釋，1:顯示(默認)，3:不顯示
					'rel': 0, //顯示相關視頻，0:不顯示，1:顯示(默認)
					'modestbranding': 1, //YouTube標籤，0:顯示(默認)，1:不顯示    
					'fs':0
				},
				events: {
					'onReady': onPlayerReady,
					'onStateChange': onPlayerStateChange
				}
				});
		}

		// 4. The API will call this function when the video player is ready.
		function onPlayerReady(event) {				
				//event.target.setPlaybackQuality('highres');
				//event.target.loadVideoById('wUPPkSANpyo');
				event.target.loadVideoById(yt_videoid);
				event.target.playVideo();
				//$(".html5-main-video").css("left","-444.5px");
		}

		// 5. The API calls this function when the player's state changes.
		//    The function indicates that when playing a video (state=1),
		//    the player should play for six seconds and then stop.
		var done = false;
		function onPlayerStateChange(event) {
			console.log(event.data);

			/* 判斷影片撥放後才開啟遮罩 */
			if(player.getPlayerState() == 1){
				$("#mask").css("display","block");
			}
			// alert("onPlayerStateChange");
			/*
			if (event.data == YT.PlayerState.PLAYING && !done) {
			setTimeout(stopVideo, 15000);
			done = true;
			}*/
		}
		function stopVideo() {
			player.stopVideo();
		}

		/* 判斷是否為手機裝置 */
		function isMobile() {

			try{ document.createEvent("TouchEvent"); return true; }
			catch(e){ return false;}

		}
		</script>
	</body>
</html>
