<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list');
$status = _v('status');
//$frozenspoint = _v('frozenspoint'); 
$onbid = 0;
$getbid = 0;
foreach($row_list['table']['record'] as $rk => $rv) {
	if($rv['closed'] == 'N') { 
		$onbid = $onbid + 1;
	}else{
		if ($rv['final_winner_userid'] == $_SESSION['auth_id']){
			$getbid = $getbid + 1;
		}
	}
}	
?>	
<?php //殺價紀錄 ?>
	
<div id="tobid" class="swipe-tab">
    <div class="sajaProduct-select d-flex">
        <!-- 橫向menu -->
        <div class="horscroll">
            <div class="menu-wrapper d-flex flex-nowrap">
                <a class="menu-a on-active" href="#tab1">全部</a>
                <a class="menu-a" href="#tab2">競標中</a>
                <a class="menu-a" href="#tab3">已得標</a>
                <div class="sideline"><!-- menu底線 --></div>
            </div>
        </div>
        <!-- 下拉menu -->
        <!--
        <div id="sajaProduct-select-btn" class="menu-btn"></div>
        -->
    </div>
<!--
    <div class="history-freeze">
        <div class="title">凍結殺價幣共</div>
        <div class="point"><span class="currency">NT</span><span class="frozen"></span></div>
    </div>
-->
    <div class="swipe-navbar-content history-saja">
        <!-- ajax生成 -->
    </div>
</div>

<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };
    
    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現  
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }
    
    $(function(){
        //有footerBar時，回頂按鈕 位置往上移動
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
    })
</script>

<!-- 觸底加載 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/es-bottom-scroll.js"></script>

<!-- 客製化加載 -->
<script>
    $(function(){
        //各元件
        esOptions.nopicUrl = '<?php echo APP_DIR;?>/static/img/nohistory-img.png';        //無資料時顯示
        esOptions.loadingUrl = '<?php echo APP_DIR;?>/static/img/bottomLoading.gif';        //資料loading顯示
        
        //存入 json 開頭
        esOptions.jsonInfo.dir = '<?php echo BASE_URL.APP_DIR;?>';
        esOptions.jsonInfo.hpage = '/dream_history/history_list/';                //網頁page識別碼，抓取json用
        esOptions.isTab = 'true';                                   //是否為多頁tab
        
        esOptions.contentdom.ulClass = 'hsaja-lists';
        createDom();
        
        //追加 tabItem : kind & jsonUrl & nopicText
        $.each(esOptions.tabItem,function(i,item){
            //添加特性 kind & jsonUrl
            switch(item['id']){
                    case 'tab1' :
                        item['kind'] = 'all';
                        item['nopicText'] = '目前無任何記錄哦！';
                        break;
                    case 'tab2' :
                        item['kind'] = 'onbid';
                        item['nopicText'] = '目前無競標中的商品哦！';
                        break;
                    case 'tab3' :
                        item['kind'] = 'getbid';
                        item['nopicText'] = '目前沒有得標的商品哦！';
                        break;
                }
            item['jsonUrl'] = esOptions.jsonInfo.dir + esOptions.jsonInfo.hpage +'?json=Y&kind='+item['kind'];
        });
    })
    
    //載入json(第一次與其他頁共用同一個)
    esOptions.show = function($id) {
        var $ajaxUrl,$nowPage,$endPage,$total;
        var $wrapper = $id;
        var $note,$textcolor;
        $.map(esOptions.tabItem,function(item, index){
            if(item['id'] === $id){
                $endPage = item['endpage'];
                $total = item['total'];
                if(item['page'] < $total){                          //與總頁數比對，若已達總頁數不再累加，避免重複加載
                    item['page']++;                                 //這次要加載的頁數
                }
                $nowPage = item['page'];
                $ajaxUrl = item['jsonUrl']+'&p='+$nowPage;      //json路徑
            }
        });
        if ($endPage == $nowPage){                                  //比對是否是這次要加載的頁數
            $.ajax({  
                url: $ajaxUrl,
                contentType: esOptions.jsonInfo.contentType,
                type: esOptions.jsonInfo.type,  
                dataType: esOptions.jsonInfo.dataType,
                async: esOptions.jsonInfo.async === true,
                cache: esOptions.jsonInfo.cache === true,
                timeout: esOptions.jsonInfo.timeout,
                processData: esOptions.jsonInfo.processData,
                contentType: esOptions.jsonInfo.contentType
            }).then(
                showResponse,                       //加載成功
                showFailure                         //加載失敗
            ).always(
                endLoading                          //加載後 不管成功失敗
            )

            function showResponse(data){                                              //生成div的function
                var $itemWrapper = $('#'+$wrapper);
                var $arr = data["retObj"]["data"];
                if(!($arr == null || $arr == 'undefined' || $arr == '')){
//                    var $frozenspoint = data["retObj"]["frozenspoint"]["frozen"];         //凍結殺價幣
//                    $(".history-freeze").find(".frozen").text($frozenspoint);             //凍結殺價幣(顯示於畫面)
                    if($nowPage <= $total){
                        $.each(data["retObj"]["data"],function(i,item){
                            if(item['final_winner_userid']=='<?php echo $_SESSION['auth_id'] ?>' && item['complete']=='N'){              //得標者 且 未結帳 計算是否超時
                                var $nowTime = new Date();                                    //現在時間
                                var $orderTime = new Date(getDate(item['pofftime']));       //結標時間
                                var $limitedTime = 3*24*60*60;                                //7天後未結帳顯示為到期(秒)
                                var $nowTime_to_second = Math.floor($nowTime / 1000);         //轉換uniTime 比較用(秒)
                                var $orderTime_to_second = Math.floor($orderTime / 1000);     //轉換uniTime 比較用(秒)
                                var $difference = $nowTime_to_second - $orderTime_to_second;  //現在時間大於結標時間多久 
                                var $timeFinish;        //儲存時間狀態

                                if($difference >= $limitedTime){
                                    $timeFinish = 'Y';          //已過時間
                                }else{
                                    $timeFinish = 'N';          //時間還沒到
                                }
                            }
                            
                            var statusImg = '<?php echo APP_DIR;?>/static/img/'+ item['bid_status']; 
                            var productImg = '<?php echo BASE_URL.APP_DIR;?>/images/site/product/'+ item['filename'];
                            var lazyloadImg = '<?php echo APP_DIR;?>/static/img/loading.gif';
                            
                            if(item['sajago'] != ''){
                                var sajaHref = item['sajago'];
                            }
                            
                            var checkoutHref = '<?php echo BASE_URL.APP_DIR;?>/history/bid_checkout/?productid=' +item['productid']+ '&<?php echo $cdnTime; ?>'
                            var orderLink = '<?php echo BASE_URL.APP_DIR;?>/member/order/?<?php echo $cdnTime; ?>'

                            $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                            .append(
                                $('<li class="hsaja-list"/>')
                                .append(
                                    $('<div class="hsaja-header d-flex align-items-center"/>')
                                    .append(
                                        $('<div class="saja-status d-flex align-items-center mr-auto align-self-end"/>')
                                        .append(
                                            $('<div class="status-img"/>')
                                            .append(
                                                $('<img class="img-fluid" src="'+statusImg+'"/>')
                                            )
                                        )
                                        .append(
                                            $('<div class="status-title d-flex align-items-center"/>')
                                            .append(
                                                $('<span>'+item['bid_mod']+'</span>')
                                            )
                                            .append(
                                                $('<span class="num">(出價次數：'+item['count']+')</span>')
                                            )
                                        )
                                    )
                                    .append(
                                        $('<div class="bid-price d-flex flex-column text-right"/>')
                                        .append(
                                            $('<span class="small">總下標費用</span>')
                                        )
                                        .append(
                                            $('<span>NT '+item['alltotal']+' 元</span>')
                                        )
                                    )
                                )
                                .append(
                                    $('<div class="hsaja-contant d-flex align-items-start"/>')
                                    .append(
                                        $('<a/>')
                                        .attr('onclick','javascript:location.href="'+item['href']+'"')
                                        .append(
                                            $('<div class="hsaja-img"/>')
                                            .append(
                                                $('<i class="d-flex align-items-center"/>')
                                                .append(
                                                    $('<img src="'+productImg+'"/>')
                                                )
                                            )
                                        )
                                    )
                                    .append(
                                        $('<div class="hsaja-info align-self-center"/>')
                                        .append(
                                            $('<a class="hsaja-info-a"/>')
                                            .attr('onclick','javascript:location.href="'+item['href']+'"')
                                            .append(
                                                $('<p class="hsaja-name">'+item['name']+'</p>')
                                            )
                                            .append(
                                                $('<p class="hsaja-retasil">官方售價：NT '+item['retail_price']+' 元</p>')
                                            )
                                            .append(
                                                $('<p class="hsaja-time">結標時間：<span>'+item['pofftime']+'</span></p>')
                                            )
                                        )
                                        .append(
                                            (item['sajago'] != ''?
                                             '<a class="sajalink red" onclick="javascript:location.href=\''+sajaHref+'\'"><span>殺價去</span></a>'
                                            :'')
                                        )
                                        .append(
                                            (item['final_winner_userid']=='<?php echo $_SESSION['auth_id'] ?>'?
                                                (item['complete']=='N'?
                                                    ($timeFinish == 'Y')?
                                                    '<a class="checkout gray" onclick="javascript:void(0);"><span>超時未結帳</span></a>'
                                                    :
                                                    '<a class="checkout red" onclick="javascript:location.href=\''+checkoutHref+'\'"><span>結帳去</span></a>'
                                                 :
                                                 '<a class="checkout red" onclick="javascript:location.href=\''+orderLink+'\'"><span>查詢訂單</span></a>'
                                                )
                                            :'')
                                        )
                                    )
                                )
                            )
                        }) 
                    }else{
                        return false;
                    }
                }else{
                    //不同頁簽顯示不同文字 (先前存進物件的資料)
                    var $nopicText = ''
                    var $nopicgroup = $.map(esOptions.tabItem, function(item, index){
                        if(item['id']===$wrapper){
                            $nopicText = item['nopicText'];
                        }
                    });
                    $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                    .after(
                        $('<div class="nohistory-box"/>')
                        .append(
                            $('<div class="nohistory-img"/>')
                            .append(
                                $('<img class="img-fluid" src="'+esOptions.nopicUrl+'"/>')
                            )
                        )
                        .append(
                            $('<div class="nohistory-txt">'+$nopicText+'</div>')
                        )
                    )
                }
            } 
            //加載動作完成 (不論結果success或error)
            function endLoading() {
                $.map(esOptions.tabItem,function(item, index){
                    if(item['id'] === $id){
                        $totalPage = item['total'];              //找到對應ID的總頁數
                        item['endpage']++;                       //預計下次要加載的頁數,無論如何載完都加1，事後比對用
                    }
                });
                var $loadingDom = $('#'+$id).find('.esloading');
                if($totalPage == 1){
                    $loadingDom.hide();
                }else if($nowPage == $totalPage){
                    $loadingDom.html('<span>沒有其他資料了</span>'); 
                }else if($nowPage < $totalPage){
                    $loadingDom.show();
                }else{
                    $loadingDom.hide();
                }
            }
            //加載錯誤訊息
            function showFailure() {
            }
        }
    };
    
    var $menuParent = $(esOptions.menudom.parent),                        //按鈕的顯示窗
        $wrapper = $(esOptions.menudom.wrapper),                          //包住所有按鈕的外框
        $allmenu = $menuParent.find(esOptions.menudom.menuItem);          //按鈕群
    
    var $showParent = $(esOptions.contentdom.parent);                    //翻頁物件的顯示窗
    
    $(window).on('load', function(){
        //撈取各頁籤總頁數後，生成畫面
        $.when.apply($, esOptions.tabItem.map(function(item) {
            getTotalPages(item);
        })).then(function() {
            //開始生成
            esOptions.tabItem.map(function(item) {
                esOptions.show(item['id']);
            })
            
            if(esOptions.tabItem.length > 1){                 //menu數量大於1，才開放滑動效果
                //向左滑
                $showParent.bind('swipeleft', function(e){
                    menuGoNext();
                    e.stopPropagation();
                });
                $showParent.bind('swipe taphold', function(e){
                    e.stopPropagation();
                })
                //向右滑
                $showParent.bind('swiperight', function(e){
                    menuGoPrevious();
                    e.stopPropagation();
                });
            }
            
            $allmenu.bind('click', function() {
                var $that = $(this);
                menuGoNumber($that.index() + 1);
            });
        });
    });
    $(window).on('resize', function(){
        getPageHeight();
        menuLine();
    });

    //指定跳頁
    function menuGoNumber(number) {
        var $that = $menuParent.find(esOptions.menudom.menuItem + ':nth-child(' + number + ')'),                  //按鈕active
            $thatItem = $showParent.find(esOptions.contentdom.showItem + ':nth-child(' + number + ')'),           //對應的翻頁物件
            $menuLeft = 0,                                                                                      //按鈕定位
            $showLeft = 0;
        var $item = $(esOptions.contentdom.showItem);                                                               //翻頁物件群
        //翻頁物件
        if($thatItem.length > 0){
            //按鈕
            if($that.length > 0){
                $menuLeft = $that.position().left;
                var $maxW = $wrapper.width();               //總寬度
                var $minW = $menuParent.width();            //螢幕寬度
                if ($menuLeft > $minW / 2) {
                    if($menuLeft < $maxW - $minW / 2) {
                        $menuLeft = ($that.position().left - ($menuParent.width() * 0.5) + $that.width());
                        $menuParent.stop().animate({scrollLeft:$menuLeft},300)    //移動位置，讓按鈕置中
                    }else{
                        $menuParent.stop().animate({scrollLeft:$maxW},300)       //最尾段，如果點選的按鈕未超過螢幕一半時，保持在最後
                    }
                }else{
                    $menuParent.stop().animate({scrollLeft:0},300)               //最前段，如果點選的按鈕未超過螢幕一半時，保持在最前
                }
                $allmenu.removeClass(esOptions.class.active);
                $that.addClass(esOptions.class.active);
                menuLine();
            }
            //翻頁物件active切換
            $item.removeClass(esOptions.class.active);
            $thatItem.addClass(esOptions.class.active);
            return true;
        }
        return false;
    }
    
    //上一個(右滑)
    function menuGoPrevious() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        $('body,html').stop().animate({scrollTop:0},500);
        if ($that.index() > 0) {
            $that.prev().click();
            return true;
        }
        return false;
    }
    
    //下一個(左滑)
    function menuGoNext() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var menuLength = $(esOptions.menudom.wrapper).find(esOptions.menudom.menuItem).length;
        $('body,html').stop().animate({scrollTop:0},500);
        if ($that.index() < menuLength - 1) {
            $that.next().click();
            return true;
        }
        return false;
    }
    
    //menu底線變換
    function menuLine() {
        var $that = $(esOptions.menudom.line);
        var $actMenu = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var $actMenuW = $actMenu.innerWidth();
        $that.stop().animate({left:$actMenu.position().left},200).animate({width:$actMenuW},200);
    }
    
    //ios 時間問題轉換
    function getDate(time) {
        //將xxxx-xx-xx的時間格式，轉換為 xxxx/xx/xx的格式
        time = time.replace(/\-/g, "\/");
        return time;
    };
    
    //ajax用 取得總頁數
    function getTotalPages(item){
        var $getTotalPagesId = item['id'];
        var $ajaxUrl;
        $.map(esOptions.tabItem,function(item){
            if(item['id'] == $getTotalPagesId){
                $ajaxUrl = item['jsonUrl'];      //json路徑      
            }
        });
        return $.ajax({
            url: $ajaxUrl,
            contentType: esOptions.jsonInfo.contentType,
            type: esOptions.jsonInfo.type,  
            dataType: esOptions.jsonInfo.dataType,
            async: esOptions.jsonInfo.async === true,
            cache: esOptions.jsonInfo.cache === true,
            timeout: esOptions.jsonInfo.timeout,
            processData: esOptions.jsonInfo.processData,
            contentType: esOptions.jsonInfo.contentType
        }).then(
            function(data) {             //取得成功
                //將總頁數 寫入 tabItem
                $.map(esOptions.tabItem,function(item){
                    if(item['id'] == $getTotalPagesId){
                        item['total'] = data['retObj']['page']['totalpages'] || 1;
                    }
                });
            },
            function(data) {            //取得失敗
                console.log(data);
            }
        );
    };
</script>