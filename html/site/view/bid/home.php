<?php
$status = _v('status');
$product_list = _v('product_list');
$ctype=_v('ctype');
if(empty($ctype))  
   $ctype=0;

$arr_ctype=array("","");
if($ctype==1) {
   $arr_ctype[0]="";
   $arr_ctype[1]="selected";
} else if($ctype==0) {
   $arr_ctype[0]="selected";
   $arr_ctype[1]="";
} 

if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') >0 ) {
	$browser = 1;
}else{
	$browser = 1;
}
if (isset($_GET['ctype'])&&($_GET['ctype']==1)){
    $ct=array("","on-active");
}else{
    $ct=array("on-active","");
}
?>
<script>
function ctypesel(pt){
    if(pt==1) {
			   window.location.href='/site/bid/?ctype=1';	
			} else {
			   window.location.href='/site/bid/?ctype=0';	
			}    
}
</script>
<div class="sajaProduct-select d-flex">
            <!-- 橫向menu -->
            <div class="horscroll">
                <div class="menu-wrapper d-flex flex-nowrap">
                
                <a class="menu-a ui-link <?php echo $ct[0];?>" href="#" style="padding-left: 2.667rem; padding-right: 2.292rem; width: calc(33.3333vw);"><span onclick="javascript:ctypesel(0);">按成交時間</span></a>
                <a class="menu-a ui-link <?php echo $ct[1];?>" href="#" style="padding-left: 2.292rem; padding-right: 2.292rem; width: calc(33.3333vw);"><span onclick="javascript:ctypesel(1);">按成交金額</span></a>
                   <!-- 生成按鈕 -->
                
                </div>
            </div>
        </div>
<div class="swipe-navbar-content footernav-h">
    <!-- ajax生成 -->
</div>

<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };

　　// 數字格式化(千分位以 ',' 間隔)　
　　function numberWithCommas(x) {
		var parts = x.toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return parts.join(".");
	}

    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }

    $(function(){
        //有footerBar時，回頂按鈕 位置往上移動
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
		
		$("#ctype").change(function(){
			if($("#ctype").val()==1) {
			   window.location.href='https://www.saja.com.tw/site/bid/?ctype=1';	
			} else {
			   window.location.href='https://www.saja.com.tw/site/bid/?ctype=0';	
			}
		});
    })
</script>

<!-- 觸底加載 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/es-bottom-scroll.js"></script>

<!-- 客製化加載 -->
<script>
	//比較時間
	function MillisecondToDate(msd) {
		var time = parseFloat(msd) / 1000;
		if (null != time && "" != time) {
			if (time >= 3600.0 ) {
				time = [parseInt(time / 3600.0),
					parseInt((parseFloat(time / 3600.0) - parseInt(time / 3600.0)) * 60),
					parseInt((parseFloat((parseFloat(time / 3600.0) - parseInt(time / 3600.0)) * 60) -
					parseInt((parseFloat(time / 3600.0) - parseInt(time / 3600.0)) * 60)) * 60)];
			}
			else if (time >= 60.0 && time < 3600.0) {
				time = [0, parseInt(time / 60.0),
				parseInt((parseFloat(time / 60.0) - parseInt(time / 60.0)) * 60) ];
			}
			else {
				time = [0, 0, parseInt(time)];
			}
		}
		return time;
	}
	function check24Hours(startTime, endTime){
		var strtime_s = startTime;
		strtime_s = strtime_s.replace(/-/g, "/");
		var start = (new Date(strtime_s)).getTime();

		var strtime_n = endTime;
		strtime_n = strtime_n.replace(/-/g, "/");
		var end = (new Date(strtime_n)).getTime();

		var rs = MillisecondToDate(end - start);
		//console.log(end+' = '+ start+' = '+rs );

		if(rs[0]>=24){
			return true
		}
		return false;
	}


	//各元件
    esOptions.nopicUrl = '<?php echo APP_DIR;?>/static/img/nohistory-img.png';        //無資料時顯示
    esOptions.loadingUrl = '<?php echo APP_DIR;?>/static/img/bottomLoading.gif';        //資料loading顯示

    //存入 json 開頭
    esOptions.jsonInfo.dir = '<?php echo BASE_URL.APP_DIR;?>';
    esOptions.jsonInfo.hpage = '/bid/';                //網頁page識別碼，抓取json用
    esOptions.contentdom.ulClass = 'listsimg-box-group';
    createDom();
    //追加 tabItem : kind & jsonUrl & nopicText
    $.each(esOptions.tabItem,function(i,item){
        item['nopicText'] = '目前無相關紀錄';
        item['jsonUrl'] = esOptions.jsonInfo.dir + esOptions.jsonInfo.hpage +'?json=Y&ctype=<?php echo $_GET['ctype'];?>';
    });

    //載入json(第一次與其他頁共用同一個)
    esOptions.show = function() {
        var $ajaxUrl,$nowPage,$endPage,$total;
        var $note,$textcolor;
        $.map(esOptions.tabItem,function(item, index){
            $endPage = item['endpage'];
            $total = item['total'];
            if(item['page'] < $total){                          //與總頁數比對，若已達總頁數不再累加，避免重複加載
                item['page']++;                                 //這次要加載的頁數
            }
            $nowPage = item['page'];
            $ajaxUrl = item['jsonUrl']+'&p='+$nowPage;      //json路徑
        });
        if ($endPage == $nowPage){                                  //比對是否是這次要加載的頁數
            $.ajax({
                url: $ajaxUrl,
                contentType: esOptions.jsonInfo.contentType,
                type: esOptions.jsonInfo.type,
                dataType: esOptions.jsonInfo.dataType,
                async: esOptions.jsonInfo.async === true,
                cache: esOptions.jsonInfo.cache === true,
                timeout: esOptions.jsonInfo.timeout,
                processData: esOptions.jsonInfo.processData,
                contentType: esOptions.jsonInfo.contentType
            }).then(
                showResponse,                       //加載成功
                showFailure                         //加載失敗
            ).always(
                endLoading                          //加載後 不管成功失敗
            )
            function openlnk(url){
                window.open(url,"_blank");
            }
            function showResponse(data){                                              //生成div的function
                var $arr = data["retObj"]["data"];
                if(!($arr == null || $arr == 'undefined' || $arr == '')){
                    if($nowPage <= $total){
                        $.each(data["retObj"]["data"],function(i,item){

							//是否 > 24HR
							var chkTime = check24Hours(item['insertt'], '<?php echo(date("Y-m-d H:i:s", time()));?>');

							var statusImg = '<?php echo APP_DIR;?>/static/img/bid-member.jpg';
                            if(item['thumbnail']!==''){
                                var productImg = esOptions.jsonInfo.dir + '/images/site/product/'+ item['thumbnail'];
                            }else if(item['thumbnail_url']!==''){
                                var productImg = item['thumbnail_url'];
                            }else{
                                var productImg = esOptions.nopicUrl;
                            }
                            if (item['tx_hash']!='')item['tx_hash']="<div style='line-height:3rem;' onclick='javascript:window.open(\""+item['tx_hash']+"\",\"_blank\");'>Ethereum 區塊鏈記錄</div>";
							if(item['m_thumbnail_file']!=='' && item['m_thumbnail_file']!==null){
                                var userImg = '<?php echo BASE_URL;?>/site/images/headimgs/'+ item['m_thumbnail_file'];
                            }else if(item['m_thumbnail_url']!=='' && item['m_thumbnail_url']!==null){
                                var userImg = item['m_thumbnail_url'];
                            }else{
                                var userImg = statusImg;
                            }



							if(!chkTime){
									$('.' + esOptions.contentdom.ulClass)
									.append(
										$('<li class="listsimg-box"/>')

										.append(
										$('<div class="listsimg-box-solid"/>')
										.append(
											$('<div class="listsimg-header active d-flex align-items-center mt-auto"/>')
											.append(
												$('<div class="bid-user d-inlineflex align-items-center mr-auto"/>')
												.append(
													$('<div class="bid-img"/>')
													.append(
														$('<img class="img-fluid" src="'+userImg+'" />')
													)
												)
												.append(
													$('<div class="bid-userName active"/>')
													.text(item['nickname'])
												)
											)
											.append(
												$('<div class="bid-price active"/>')
												.append(
													$('<span>得標金額 NT '+numberWithCommas(item['price'])+' 元</span>')
												)
											)
										)
										.append(
											$('<div class="listsimg-contant d-flex align-items-start"/>')
											.append(
												$('<div class="listimg_img"/>')
												.append(
													$('<i class="d-flex align-items-center"/>')
                                                    .attr('onclick',"javascript:location.href='"+item['link_url']+"'")
                                                    .attr('target','_blank')
													.append(
														$('<img src="'+productImg+'" />')
													)
												)
											)
											.append(
												$('<div class="listimg-info"/>')
												.append(
													$('<p class="pro-name"/>')
                                                    .attr('onclick',"javascript:location.href='"+item['link_url']+"'")
                                                    .attr('target','_blank')
													.text(item['name'])
												)
												.append(
													$('<p class="pro-price"/>')
													.append(
														$('<span>市價:NT '+numberWithCommas(item['retail_price'])+' 元</span>'+item['tx_hash'])
													)
												)
											)
										)
										.append(
											$('<div class="listimg-time d-flex"/>')
											.append(
												$('<div class="ml-auto"/>')
												.append(
													$('<span>'+item['insertt']+'</span>')
												)
											)
										)
										)
									)

							} else {

								$('.' + esOptions.contentdom.ulClass)
								.append(
									$('<li class="listsimg-box"/>')

									.append(
									$('<div class="listsimg-box-solid"/>')
									.append(
										$('<div class="listsimg-header active d-flex align-items-center mt-auto"/>')
										.append(
											$('<div class="bid-user d-inlineflex align-items-center mr-auto"/>')
											.append(
												$('<div class="bid-img"/>')
												.append(
													$('<img class="img-fluid" src="'+userImg+'" />')
												)
											)
											.append(
												$('<div class="bid-userName"/>')
												.text(item['nickname'])
											)
										)
										.append(
											$('<div class="bid-price"/>')
											.append(
												$('<span>得標金額 NT '+numberWithCommas(item['price'])+' 元</span>')
											)
										)
									)
									.append(
										$('<div class="listsimg-contant d-flex align-items-start"/>')
										.append(
											$('<div class="listimg_img"/>')
											.append(
												$('<i class="d-flex align-items-center"/>')
                                                .attr('onclick',"javascript:location.href='"+item['link_url']+"'")
                                                .attr('target','_blank')
												.append(
													$('<img src="'+productImg+'" />')
												)
											)
										)
										.append(
											$('<div class="listimg-info"/>')
											.append(
												$('<p class="pro-name"/>')
                                                .attr('onclick',"javascript:location.href='"+item['link_url']+"'")
                                                .attr('target','_blank')
												.text(item['name'])
											)
											.append(
												$('<p class="pro-price"/>')
												.append(
													$('<span>市價:NT '+numberWithCommas(item['retail_price'])+' 元</span>'+item['tx_hash'])
												)
											)
										)
									)
									.append(
										$('<div class="listimg-time d-flex"/>')
										.append(
											$('<div class="ml-auto"/>')
											.append(
												$('<span>'+item['insertt']+'</span>')
											)
										)
									)
									)
								)
							}
                        })
                    }else{
                        return false;
                    }
                }else{
                    //不同頁簽顯示不同文字 (先前存進物件的資料)
                    var $nopicText = ''
                    var $nopicgroup = $.map(esOptions.tabItem, function(item, index){
                        $nopicText = item['nopicText'];
                    });
                    $('.' + esOptions.contentdom.ulClass)
                    .after(
                        $('<div class="nohistory-box"/>')
                        .append(
                            $('<div class="nohistory-img"/>')
                            .append(
                                $('<img class="img-fluid" src="'+esOptions.nopicUrl+'"/>')
                            )
                        )
                        .append(
                            $('<div class="nohistory-txt"/>')
                            .text($nopicText)
                        )
                    )
                }
            }
            //加載動作完成 (不論結果success或error)
            function endLoading() {
                $.map(esOptions.tabItem,function(item, index){
                    item['endpage']++;                          //預計下次要加載的頁數
                    $totalPage = item['total'];                 //找到對應ID的總頁數
                });
                var $loadingDom = $('.esloading');
                if($totalPage == 1){
                    $loadingDom.hide();
                }else if($nowPage == $totalPage){
                    $loadingDom.html('<span>沒有其他資料了</span>');
                }else if($nowPage < $totalPage){
                    $loadingDom.show();
                }else{
                    $loadingDom.hide();
                }
                //強制顯示
                $('.swipe-navbar-content .content-item').css('display','block');
            }
            //加載錯誤訊息
            function showFailure() {}
        }
    };

    $(window).on('load', function(){
        //撈取各頁籤總頁數後，生成畫面
        $.when.apply($, esOptions.tabItem.map(function(item) {
            getTotalPages(item);
        })).then(function() {
            //開始生成
            esOptions.show();
           

        });
    });
    $(window).on('resize', function(){
        
        getPageHeight();
        //$('.menu-wrapper').html('<a class="menu-a ui-link <?php echo $ct[0];?>" href="/site/bid/?ctype=0" style="padding-left: 2.667rem; padding-right: 2.292rem; width: calc(33.3333vw);">按成交時間</a><a class="menu-a ui-link <?php echo $ct[1];?>" href="/site/bid/?ctype=1" style="padding-left: 2.292rem; padding-right: 2.292rem; width: calc(33.3333vw);">按成交金額</a>');

    });

    //ajax用 取得總頁數
    function getTotalPages(item){
        var $ajaxUrl;
        $.map(esOptions.tabItem,function(item){
            $ajaxUrl = item['jsonUrl']+'&p=1';      //json路徑
        });
        return $.ajax({
            url: $ajaxUrl,
            contentType: esOptions.jsonInfo.contentType,
            type: esOptions.jsonInfo.type,
            dataType: esOptions.jsonInfo.dataType,
            async: esOptions.jsonInfo.async === true,
            cache: esOptions.jsonInfo.cache === true,
            timeout: esOptions.jsonInfo.timeout,
            processData: esOptions.jsonInfo.processData,
            contentType: esOptions.jsonInfo.contentType
        }).then(
            function(data) {             //取得成功
                //將總頁數 寫入 tabItem
                $.map(esOptions.tabItem,function(item){
                    item['total'] = data['retObj']['page']['totalpages'] || 1;
                });
            },
            function(data) {            //取得失敗
                console.log(data);
            }
        );
    };
</script>