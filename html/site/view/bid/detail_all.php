<!--
<?php
if (_v('type') != '' && _v('type') != 'deal') {
?>
	<script>
		alert('偷看是不道德的行為！');
		location.href='/site/';
	</script>
<?php }  ?>
-->
<?php
if (_v('user_bid_count') == '' && _v('user_bid_count') < 1) {
?>
	<script>
		location.href='/site/';
	</script>
<?php }  ?>
<?php
$product = _v('product');
$type=_v('type');
$channelid=_v('channelid');
$productid=_v('productid');

$bid_info=_v('bid_info');
$page_content = _v('page_content');

//會員下標總標數
$user_bid_count=_v('user_bid_count');
//會員下標最大金額
$user_bid_max=_v('user_bid_max');
//會員最後下標時間
$user_bid_last=_v('user_bid_last');

if(!empty($product['thumbnail'])) {
	$img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
} elseif (!empty($product['thumbnail_url'])) {
	$img_src = $product['thumbnail_url'];
}
?>
<div class="article">
    <!--  競標資訊  -->
    <ul class="listsimg-box-group bidSelfDetail">
        <li class="listsimg-box">
            <div class="listsimg-header d-flex align-items-center">
                <div class="bid-user d-inlineflex align-items-center mr-auto">
                    <div class="bid-img">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/bid-success.png">
                    </div>
                    <div class="bid-userName">
                        <?php if (_v('type') == '' || _v('type') == 'deal') { ?>
                            <?php
                                if (!empty($product['nickname'])) {
                                    echo $product['nickname'];
                                } else {
                                    echo "無人得標";
                                }
                            ?>
                        <?php } else { ?>
                            <span>流標原因：<?php echo $product['memo']; ?></span>
                        <?php } ?>
                    </div>
                </div>
                <div class="bid-price">
                    <?php if (_v('type') == '' || _v('type') == 'deal') { ?>
                        <span>得標金額 NT <?php echo $product['price']; ?> 元</span>
                    <?php } else { ?>
                    <?php } ?>
                </div>
            </div>

            <div class="listsimg-contant d-flex align-items-start">
                <div class="listimg_img">
                    <i class="d-flex align-items-center">
                       <img src="<?php echo $img_src; ?>">
                    </i>
                </div>
                <div class="listimg-info align-self-center">
                    <p class="pro-name"><?php echo $product['name']; ?></p>
                    <p class="pro-price">商品市價：NT <span><?php echo $product['retail_price']; ?></span> 元</p>
                </div>
            </div>
            <div class="listimg-time d-flex">
                <div class="ml-auto"><span><?php echo $product['insertt'] ?></span></div>
            </div>
        </li>
    </ul>

    <!-- 欲查看的範圍 -->
    <div class="bidSelf-selectBox">
        <div class="showTxt showTxt d-flex align-items-center">
            <div class="showTitle mr-auto">請選擇下標金額區間</div>
            <img class="showIcon" src="<?php echo BASE_URL.APP_DIR;?>/static/img/arrow.png" alt="">
        </div>
        <div class="optionBox">
            <!-- 生成選項 -->
        </div>
    </div>

    <!--  競標資訊  -->
    <div class="bidSelfList">
        <!-- 生成List -->
    </div>

<!--
	<ul data-role="listview" data-inset="true" data-icon="false">
		<li>
			<h2><?php //echo $product['name']; ?></h2>
			<h2>商品市價：RMB <?php //echo $product['retail_price'];?> 元</h2>

			<?php //if (_v('type') == '' || _v('type') == 'deal') { ?>
				  <h2>中標金額：RMB <?php //echo $product['price']; ?> 元</h2>
					<h2>中標者：
					<?php
						/*
                        if (!empty($product['nickname'])) {
							echo $product['nickname'];
						} else {
							echo "無";
						}
                        */
					?>
					</h2>
			<?php //} else { ?>
				  <h2>流標原因：<?php //echo $product['memo']; ?></h2>
			<?php //} ?>
				<div style="overflow: hidden;">
					<div style="float:right;width:100px;height:45px;text-align:center;line-height:45px;">
						 <h2> / 共 <?php //echo $page_content['lastpage']; ?> 頁</h2>
					</div>
					<div style="float:left;width:0px;height:45px;line-height:45px;">
						<h2></h2>
					</div>
					<div style="margin: 0 5px;">
						<select name="change-page" id="change-page" onchange="page(this.value);">
							<?php //echo $page_content['change']; ?>
						</select>
					</div>
				</div>
			<?php //if (is_array($bid_info)) { ?>
				<h2 style="float:left;width:70px;text-align:center;">出價</h2>&nbsp;<h2 style="float:left;width:70px;text-align:center;">下標人數</h2><h2></h2>
				 h2 style="clear:both;">(每页50笔)</h2
				<?php
                    /*
					foreach ($bid_info as $rk => $rv) {
						if ($product['price'] == $rv['price']) {
                    */
				?>
						<h2 style="float:left;width:70px;text-align:center;color:red;"><?php //echo sprintf("%.2f",$rv['price']); ?></h2>&nbsp;
						<h2 style="float:left;width:70px;text-align:center;color:red;"><?php //echo $rv['count']; ?></h2>
						<h2 style="clear:both;"></h2>
					<?php //} else { ?>
						<h2 style="float:left;width:70px;text-align:center;"><?php //echo sprintf("%.2f",$rv['price']); ?></h2>&nbsp;
						<h2 style="float:left;width:70px;text-align:center;"><?php //echo $rv['count']; ?></h2>
						<h2 style="clear:both;"></h2>
				<?php
                    /*
						}
					}
                    */
				?>
			<?php //} ?>
		</li>
	</ul>
-->

<!--
	<?php
//	if(!empty($bid_info) ) {
//	$page_content = _v('page_content');
//
//	$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : '';
//	$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
	?>
	<div>
		<div class="ui-grid-a">
			<div class="ui-block-a <?php //echo $arrow_l_able; ?>">
			<a href="<?php //echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
			</div>
			<div class="ui-block-b <?php //echo $arrow_r_able; ?>">
			<a href="<?php //echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
			</div>
		</div>

	</div>
	<?php //} ?>
-->
</div><!-- /article -->

<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };

    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }

    $(function(){
        //有footerBar時，回頂按鈕 位置往上移動
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
    })
</script>

<script>
//selector Bar
    var esSelectBar = {
        class: {
            active: 'active',
            create: 'option',
            orange: 'orange'
        },
        dom: {
            parent: '.bidSelf-selectBox',
            showBox: '.showTxt',
            showTitle: '.showTitle',
            optionBox: '.optionBox'
        },
        contentdom: {
            parent: '.bidSelfList'
        },
        data: {
            startNum: 1,
            range: 500,
        }
    }

    esSelectBar.data.totalNum = '<?php echo $user_bid_max; ?>';       //總筆數

    var $totalNum = esSelectBar.data.totalNum,
        $range = esSelectBar.data.range,
        $count = Math.ceil($totalNum / $range),                     //生成option用的迴圈
        $output = [],                                               //產生range區間
        $start,$end,
        $selectBox = $(esSelectBar.dom.parent),
        $showBox = $selectBox.find(esSelectBar.dom.showBox),
        $showTitle = $showBox.find(esSelectBar.dom.showTitle),
        $optionBox = $selectBox.find(esSelectBar.dom.optionBox),
        $bidSelfList = $(esSelectBar.contentdom.parent),
        $orange = esSelectBar.class.orange,
        $active = esSelectBar.class.active,
        $create = esSelectBar.class.create;
    //console.log($totalNum);
    esSelectBar.nopicText = '此區間沒有下標紀錄';
    esSelectBar.nodataText = '沒有下標紀錄';
    esSelectBar.nopicUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';          //無資料時顯示
    esSelectBar.loadingUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/bottomLoading.gif';        //資料loading顯示

    if($totalNum > 0){
        for(var i=0; i<$count; i++){                                        //儲存各區間
            if(i==0){
                $start = esSelectBar.data.startNum;
            }else{
                $start = (i*$range)+1;
            }
            $end = (i+1)*$range;
            $output.push({start:$start,end:$end});
        };
        //console.dir($output);

        $.each($output,function(i,item){                                    //生成option
            var $text = item['start'] + '元 - ' + item['end'] + '元';
            var $createName = esSelectBar.class.create;
            $optionBox.append(
                $('<div/>')
                .addClass($createName)
                .attr('data-start',item['start'])
                .attr('data-end',item['end'])
                .text($text)
            );
        });

        $showBox.on('click',function(){                                     //點擊showBox展開option
            var $that = $(this);
            $selectBox.toggleClass($orange);
            $optionBox.toggleClass($active);
            if(!($selectBox.siblings().hasClass('cover'))){
                $selectBox.after(
                    $('<div/>')
                    .addClass('cover')
                );
                $('.cover').on('click',function() {
                    $selectBox.removeClass($orange);
                    $optionBox.removeClass($active);
                    $(this).remove();
                });
            }else{
                if($selectBox.siblings().hasClass('cover')){
                    $('.cover').remove();
                };
            };
        });

        $('.'+$create).on('click',function(){                           //點擊option 更換showtitle 收合option
            var $that = $(this);
            if (!$that.hasClass($active)){
                var $startNum = $that.data('start');
                var $endNum = $that.data('end');
                $showTitle.text($startNum +' - '+ $endNum);
                $('.'+$create).removeClass($active);
                $that.addClass($active);
                showList($startNum);                                        //帶入ajax 撈取資料
            };
            $selectBox.toggleClass($orange);
            $optionBox.toggleClass($active);
            if($selectBox.siblings().hasClass('cover')){
                $('.cover').remove();
            };

        });

        function openSelect() {
            $showBox.click();
            $optionBox.find('.'+$create+':first-child').click();
        }
        //預設開啟第一個選項
        $(window).on('load',function(){
            setTimeout(openSelect(),200);
        })
    }else{
        $showTitle.text('沒有項目可選取');
        //不同頁簽顯示不同文字 (先前存進物件的資料)
        var nodataText = esSelectBar.nodataText;
        $bidSelfList.html(
            $('<div/>')
            .addClass('nohistory-box')
            .append(
                $('<div/>')
                .addClass('nohistory-img')
                .append(
                    $('<img/>')
                    .addClass('img-fluid')
                    .attr('src',esSelectBar.nopicUrl)
                )
            )
            .append(
                $('<div/>')
                .addClass('nohistory-txt')
                .text(nodataText)
            )
        );
    }

    function showList($priceNum) {                                      //ajax function
        //console.log($priceNum)
        if ($priceNum){
            var $productid = '<?php echo $product['productid']; ?>'
            var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/bid/getBidList/?json=Y&productid='+$productid+'&startprice='+$priceNum;

            //console.log($ajaxUrl)
            $.ajax({
                url: $ajaxUrl,
                contentType: 'application/json; charset=utf-8',
                type: 'POST',
                dataType: 'json',
                async: 'false',
                cache: 'false',
                timeout: '4000',
                complete: endLoading,
                error: showFailure,
                success: showResponse
            });

            function showResponse(data){                                              //生成div的function
                var $arr = data["retObj"]["data"];
                //console.dir($arr);
                if(!($arr == null || $arr == 'undefined' || $arr == '')){
                    $bidSelfList.html(
                        $('<ul/>')
                        .addClass('bidList-group')
                        .append(
                            $('<div/>')
                            .addClass('bidList-header d-flex')
                            .append(
                                $('<div/>')
                                .addClass('header-title')
                                .text('下標金額 (元)')
                            )
                            .append(
                                $('<div/>')
                                .addClass('header-title')
                                .text('下標人數 (人)')
                            )
                        )
                    );

                    var $ulClass = $bidSelfList.find('.bidList-group');
                    $.each(data["retObj"]["data"],function(i,item){
                        $ulClass.append(
                            $('<li>')
                            .addClass('bidList d-flex')
                            .append(
                                $('<div/>')
                                .addClass('list-data')
                                .text(parseInt(item['price']))
                            )
                            .append(
                                $('<div/>')
                                .addClass('list-data')
                                .text(item['count'])
                            )
                        )
                    });
                }else{
                    //不同頁簽顯示不同文字 (先前存進物件的資料)
                    var $nopicText = esSelectBar.nopicText;
                    $bidSelfList.html(
                        $('<div/>')
                        .addClass('nohistory-box')
                        .append(
                            $('<div/>')
                            .addClass('nohistory-img')
                            .append(
                                $('<img/>')
                                .addClass('img-fluid')
                                .attr('src',esSelectBar.nopicUrl)
                            )
                        )
                        .append(
                            $('<div/>')
                            .addClass('nohistory-txt')
                            .text($nopicText)
                        )
                    );
                }
            }
            //加載動作完成 (不論結果success或error)
            function endLoading() {
//                $.map(esOptions.tabItem,function(item, index){
//                    if(item['id'] === $id){
//                        item['endpage']++;                          //預計下次要加載的頁數
//                        $totalPage = item['total'];                 //找到對應ID的總頁數
//                    }
//                });
//                var $loadingDom = $('#'+$id).find('.esloading');
//                if($totalPage == 1){
//                    $loadingDom.hide();
//                }else if($nowPage == $totalPage){
//                    $loadingDom.html('<span>沒有其他資料了</span>');
//                }else if($nowPage < $totalPage){
//                    $loadingDom.show();
//                }else{
//                    $loadingDom.hide();
//                }
            }
            //加載錯誤訊息
            function showFailure() {
//                $.map(esOptions.tabItem,function(item, index){
//                    if(item['id'] === $id){
//                        item['page']--;                                 //處理未加載的頁數扣回來重新run
//                    }
//                });
            }
        }
    }

</script>

<!--
<script type="text/javascript">

	// Wechat
	var dataForWeixin={
			MsgImg:"<?php echo $product['img_src'];?>",
			TLImg:"<?php echo $product['img_src'];?>",
			url:"<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>",
			title:"最新成交 - <?php echo $product['name'];?> (<?php echo $product['price'];?> 元成交)",
			desc:"最新成交 - <?php echo $product['name'];?> (<?php echo $product['price'];?> 元成交)",
			fakeid:"",
			callback:function(){}
	};


    //var uri = encodeURIComponent(location.href.split('#')[0]);
	var uri = "<?php echo 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];?>";
    $.ajax({
        type: "POST",
        url: "http://www.shajiawang.com/site/product/weixinSDK/",
        data: { wurl:uri },
        dataType: "text",
        success: function(data) {
            console.log(data);
            var configData = jQuery.parseJSON(data);
            console.log('sig is ' + configData.signature);
            wx.config({
				//debug: true, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参數，可以在pc端打开，参數信息会通过log打出，仅在pc端时才会打印。
                appId: configData.appId, // 必填，公众號的唯一標识
                timestamp: configData.timestamp, // 必填，生成签名的時間戳
                nonceStr: configData.nonceStr, // 必填，生成签名的随機串
                signature: configData.signature, // 必填，签名，见附录1
				jsApiList: [      //需要调用的接口
					'onMenuShareTimeline',
					'onMenuShareAppMessage',
					'onMenuShareQQ',
					'onMenuShareWeibo'
				]
            });

			wx.error(function (res) {
				// alert('wx.error: '+JSON.stringify(res));
			});

			wx.ready(function () {
				wx.onMenuShareTimeline({
					title: dataForWeixin.title,     //分享后自定义標题
					link: dataForWeixin.url,   //分享后的URL
					imgUrl: dataForWeixin.MsgImg,  //分享的LOGO
				    desc: dataForWeixin.desc,	// 分享描述
					success: function (res) {
						alert('已分享');
					},
					cancel: function (res) {
						alert('已取消');
					},
					fail: function (res) {
						alert('wx.onMenuShareTimeline:fail: '+JSON.stringify(res));
					}
				});

				wx.onMenuShareAppMessage({
					title: dataForWeixin.title,     //分享后自定义標题
					link: dataForWeixin.url,   //分享后的URL
					imgUrl: dataForWeixin.MsgImg,  //分享的LOGO
				    desc: dataForWeixin.desc,	// 分享描述
					success: function (res) {
						alert('已分享');
					},
					cancel: function (res) {
						alert('已取消');
					},
					fail: function (res) {
						alert('wx.onMenuShareAppMessage:fail: '+JSON.stringify(res));
					}
				});
				wx.onMenuShareQQ({
					title: dataForWeixin.title,     //分享后自定义標题
					link: dataForWeixin.url,   //分享后的URL
					imgUrl: dataForWeixin.MsgImg,  //分享的LOGO
				    desc: dataForWeixin.desc,	// 分享描述
					success: function (res) {
						alert('已分享');
					},
					cancel: function (res) {
						alert('已取消');
					},
					fail: function (res) {
						alert('wx.onMenuShareQQ:fail: '+JSON.stringify(res));
					}
				});
				wx.onMenuShareWeibo({
					title: dataForWeixin.title,     //分享后自定义標题
					link: dataForWeixin.url,   //分享后的URL
					imgUrl: dataForWeixin.MsgImg,  //分享的LOGO
				    desc: dataForWeixin.desc,	// 分享描述
					success: function (res) {
						alert('已分享');
					},
					cancel: function (res) {
						alert('已取消');
					},
					fail: function (res) {
						alert('wx.onMenuShareWeibo:fail: '+JSON.stringify(res));
					}
				});
			});

        },
        error: function(data) {

        }

    });

	function page(pe){
		location.href='/site/bid/bidlist/?channelid=<?php echo $channelid;?>&productid=<?php echo $productid; ?>&perpage=50&p='+pe+'&type=<?php echo $type;?>';
	}
</script>
-->
