<?php
if (_v('user_bid_count') == '' && _v('user_bid_count') < 1) { 
?>
	<script>
		location.href='/site/';
	</script>
<?php }  ?>
<?php
$product = _v('product');
$type=_v('type');
$channelid=_v('channelid');
$productid=_v('productid');

$bid_info=_v('bid_info'); 
$page_content = _v('page_content'); 

//會員下標總標數
$user_bid_count=_v('user_bid_count');
//會員下標最大金額
$user_bid_max=_v('user_bid_max');
//會員最後下標時間
$user_bid_last=_v('user_bid_last');

if(!empty($product['thumbnail'])) {
	$img_src = APP_DIR.'/images/site/product/'.$product['thumbnail'];
} elseif (!empty($product['thumbnail_url'])) {
	$img_src = $product['thumbnail_url']; 
}    
?>
<div class="article">
    <!--  競標資訊  -->
    <ul class="listsimg-box-group bidSelfDetail">
        <li class="bidSelf-list">
            <div class="bidSelf-header d-flex align-items-center">
                <div class="bidSelf-status d-flex align-items-center mr-auto">
                    <div class="status-img">
                        <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/<?php if ((_v('type') == '' || _v('type') == 'deal') && ($product['userid'] == $_SESSION['auth_id'])){ ?>saja-status-img2.png<?php }else{ ?>saja-status-img3.png<?php } ?>">
                    </div>
                    <div class="status-title d-flex align-items-center">
                        <?php if ((_v('type') == '' || _v('type') == 'deal') && ($product['userid'] == $_SESSION['auth_id'])) { ?>
                            <?php 
                                if (!empty($product['nickname'])) { 
                                    echo $product['nickname']; 
                                } else { 
                                    echo "無人得標";
                                } 
                            ?>
                        <?php } elseif ($type == 'NB') { ?>
                            <span>流標原因：<?php echo $product['memo']; ?></span>
                        <?php } else { ?>
							<span>已結標</span>	
						<?php } ?>
                    </div>
                </div>
                <div class="bid-price">
                    <?php if ((_v('type') == '' || _v('type') == 'deal') && ($product['userid'] == $_SESSION['auth_id'])) { ?>
                        <span>得標金額 NT <?php echo $product['price']; ?> 元</span>
                    <?php } ?>
                </div>
            </div>

            <div class="bidSelf-contant d-flex align-items-start">
                <div class="product-img">
                    <i class="d-flex align-items-center">
                       <img src="<?php echo $img_src; ?>">
                    </i>
                </div>
                <div class="product-info align-self-center">
                    <p class="pro-name"><?php echo $product['name']; ?></p>
                    <p class="pro-price">官方售價：NT <span><?php echo $product['retail_price']; ?></span> 元</p>
                </div>
            </div>
            <div class="bidSelf-time d-flex">
                <div class="ml-auto">結標時間：<span><?php echo $product['insertt'] ?></span></div>
            </div>
        </li>
    </ul>
    
    <!-- 欲查看的範圍 -->
    <div class="bidSelf-selectBox">
        <div class="showTxt showTxt d-flex align-items-center">
            <div class="showTitle mr-auto">請選擇下標金額區間</div>
            <img class="showIcon" src="<?php echo APP_DIR;?>/static/img/arrow.png" alt="">
        </div>
        <div class="optionBox">
            <!-- Ajax生成選項 -->
        </div>
    </div>
    <!--  競標資訊  -->
    <div class="bidSelfList">
        <!-- Ajax生成List -->
    </div>
</div>
    
<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };
    
    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現  
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }
    
    $(function(){
        //有footerBar時，回頂按鈕 位置往上移動
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
    })
</script>
<script>
//selector Bar
    var esSelectBar = {
        class: {
            active: 'active',
            create: 'option',
            orange: 'orange'
        },
        dom: {
            parent: '.bidSelf-selectBox',
            showBox: '.showTxt',
            showTitle: '.showTitle',
            optionBox: '.optionBox'
        },
        contentdom: {
            parent: '.bidSelfList'
        },
        data: {
            startNum: 1,
            range: 500,
        }
    }
    
    esSelectBar.data.totalNum = '<?php echo $user_bid_max; ?>';       //總筆數
    
    var $totalNum = esSelectBar.data.totalNum,
        $range = esSelectBar.data.range,
        $count = Math.ceil($totalNum / $range),                     //生成option用的迴圈
        $output = [],                                               //產生range區間
        $start,$end,
        $selectBox = $(esSelectBar.dom.parent),
        $showBox = $selectBox.find(esSelectBar.dom.showBox),
        $showTitle = $showBox.find(esSelectBar.dom.showTitle),
        $optionBox = $selectBox.find(esSelectBar.dom.optionBox),
        $bidSelfList = $(esSelectBar.contentdom.parent),
        $orange = esSelectBar.class.orange,
        $active = esSelectBar.class.active,
        $create = esSelectBar.class.create;
    
    esSelectBar.nopicText = '此區間沒有下標紀錄';
    esSelectBar.nodataText = '沒有下標紀錄';
    esSelectBar.nopicUrl = '<?PHP echo APP_DIR; ?>/static/img/nohistory-img.png';          //無資料時顯示
    esSelectBar.loadingUrl = '<?PHP echo APP_DIR; ?>/static/img/bottomLoading.gif';        //資料loading顯示

    if($totalNum > 0){
        for(var i=0; i<$count; i++){                                        //儲存各區間
            if(i==0){
                $start = esSelectBar.data.startNum;
            }else{
                $start = (i*$range)+1;
            }
            $end = (i+1)*$range;
            $output.push({start:$start,end:$end});
        };

        $.each($output,function(i,item){                                    //生成option
            var $text = item['start'] + '元 - ' + item['end'] + '元';
            var $createName = esSelectBar.class.create;
            $optionBox.append(
                $('<div/>')
                .addClass($createName)
                .attr('data-start',item['start'])
                .attr('data-end',item['end'])
                .text($text)
            );
        });

        $showBox.on('click',function(){                                     //點擊showBox展開option
            var $that = $(this);
            $selectBox.toggleClass($orange);
            $optionBox.toggleClass($active);
            if(!($selectBox.siblings().hasClass('cover'))){
                $selectBox.after(
                    $('<div class="cover"/>')
                );
                $('.cover').on('click',function() {
                    $selectBox.removeClass($orange);
                    $optionBox.removeClass($active);
                    $(this).remove();
                });
            }else{
                if($selectBox.siblings().hasClass('cover')){
                    $('.cover').remove();
                };
            };
        });

        $('.'+$create).on('click',function(){                           //點擊option 更換showtitle 收合option
            var $that = $(this);
            if (!$that.hasClass($active)){
                var $startNum = $that.data('start');
                var $endNum = $that.data('end');
                $showTitle.text($startNum +' - '+ $endNum);
                $('.'+$create).removeClass($active);
                $that.addClass($active);
                showList($startNum);                                        //帶入ajax 撈取資料
            };
            $selectBox.toggleClass($orange);
            $optionBox.toggleClass($active);
            if($selectBox.siblings().hasClass('cover')){
                $('.cover').remove();
            };
        });
        
        function openSelect() {
            $showBox.click();
            $optionBox.find('.'+$create+':first-child').click();
        }
        //預設開啟選單
        $(window).on('load',function(){
            setTimeout(openSelect(),200);
        })
    }else{
        $showTitle.text('沒有項目可選取');
        //不同頁簽顯示不同文字 (先前存進物件的資料)
        var nodataText = esSelectBar.nodataText;
        $bidSelfList.html(
            $('<div class="nohistory-box"/>')
            .append(
                $('<div class="nohistory-img"/>')
                .append(
                    $('<img class="img-fluid"/>')
                    .attr('src',esSelectBar.nopicUrl)
                )
            )
            .append(
                $('<div class="nohistory-txt"/>')
                .text(nodataText)
            )
        );
    }
    
    function showList($priceNum) {                               //ajax function
        if ($priceNum){
            var $productid = '<?php echo $product['productid']; ?>'
            var $ajaxUrl = '<?php echo BASE_URL.APP_DIR;?>/bid/getUserBidList/?json=Y&productid='+$productid+'&startprice='+$priceNum;
            $.ajax({  
                url: $ajaxUrl,
                contentType: 'application/json; charset=utf-8',
                type: 'POST',  
                dataType: 'json',
                async: 'false',
                cache: 'false',
                timeout: '4000',
                complete: endLoading,
                error: showFailure,  
                success: showResponse
            });
            
            function showResponse(data){                                              //生成div的function
                var $arr = data["retObj"]["data"];
				var $getprice = <?php echo $product['price'];?>;
				var $userid = <?php if (!empty($product['userid'])){ echo $product['userid']; }else{ echo 0; } ?>;
				var $memeberid = <?php echo $_SESSION['auth_id'];?>;
                if(!($arr == null || $arr == 'undefined' || $arr == '')){
                    $bidSelfList.html(
                        $('<ul class="bidList-group"/>')
                        .append(
                            $('<div class="bidList-header d-flex"/>')
                            .append(
                                $('<div class="header-title">下標金額 (元)</div>')
                            )
                            .append(
                                $('<div class="header-title">下標次數 (次)</div>')
                            )
                        )
                    ); 
                    var $ulClass = $bidSelfList.find('.bidList-group');
                    $.each(data["retObj"]["data"],function(i,item){
                        //假如有得標者，Mark得標金額
                        var $li_class = (($getprice == item['price']) && ($memeberid == $userid))? 'bidList d-flex listMark':'bidList d-flex';
                        $ulClass.append(	
                            $('<li class="'+$li_class+'"/>')
                            .append(
                                $('<div class="list-data"/>')
                                .text(parseInt(item['price']))
                            )
                            .append(
                                $('<div class="list-data"/>')
                                .text(item['count'])
                            )
                        )
                    });
                }else{
                    //不同頁簽顯示不同文字 (先前存進物件的資料)
                    var $nopicText = esSelectBar.nopicText;
                    $bidSelfList.html(
                        $('<div class="nohistory-box"/>')
                        .append(
                            $('<div class="nohistory-img"/>')
                            .append(
                                $('<img class="img-fluid"/>')
                                .attr('src',esSelectBar.nopicUrl)
                            )
                        )
                        .append(
                            $('<div class="nohistory-txt"/>')
                            .text($nopicText)
                        )
                    );
                }
            } 
            
            //加載動作完成 (不論結果success或error)
            function endLoading() {}
            //加載錯誤訊息
            function showFailure() {}
        }
    }
    
</script>
