<?php
   $type = _v('type');
   $channelid = _v('channelid');
   $product = _v('product');
   $productid=_v('productid');
   $user_bid_count=_v('user_bid_count');
     
   if(file_exists('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html')) {
      include_once('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html');
   }
?>
<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>
<!-- 指定位置按鈕 -->
<div id="godom">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gobid.png" alt="得標位置">
</div>

<!-- 回頂與指定位置JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            godomid: '#godom',      //指定移動按鈕名稱
            goto: '.listMark',      //指定移動到
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };

    $(function(){
        var $godomMove,             //指定按鈕上移位置
            $btnH,                  //按鈕高度
            $btnSpacing;            //按鈕間距
        
            $(options.dom.gotopid).hide(); //隱藏回頂按鈕
            //偵測平台高度
            var $windowH = $(window).height();
            $(options.dom.godomid).hide();
            if($(options.dom.goto).length>0){
                //偵測Mark位置
                var $toPosition = $(options.dom.goto).offset().top;
                function godomIsShow() {
                    if($toPosition < $windowH) {
                        $(options.dom.godomid).hide();  //指定移動按鈕(Mark小於原始視窗高度時隱藏)
                    }else{
                        $(options.dom.godomid).show();
                    }
                };
                godomIsShow();
            }
        
		$(window).on('load',function(){
            $btnH = parseInt($(options.dom.gotopid).height());                //抓取按鈕高度
            $btnSpacing = parseInt($(options.dom.gotopid).css('bottom'));     //抓取按鈕間距
            $godomMove = $btnH+$btnSpacing*2;                                 //計算需要上移的距離
            
            function changeShow() {                                           //偵測視窗捲軸動作
                if ($(window).scrollTop()>options.scroll){                    //當捲軸向下移動多少時，按鈕顯示
                    $(options.dom.godomid).css('bottom',$godomMove);          //將指定移動按鈕上移，下方顯示回頂按鈕
                    $(options.dom.gotopid).show();                            //回頂按鈕出現  
                }else{
                    $(options.dom.godomid).css('bottom',$btnSpacing);         //指定按鈕回到原始位置
                    $(options.dom.gotopid).hide();                            //回頂按鈕隱藏
                }
                if ($(window).scrollTop()>$toPosition+$windowH || $(window).scrollTop()<$toPosition-$windowH){                       
                    $(options.dom.godomid).show();                            //指定位置按鈕出現
                }else{
                    $(options.dom.godomid).hide();                            //指定位置按鈕隱藏
                }
            };
            
			$(window).on('scroll resize',function(){
                changeShow();
            });
			
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed); //單擊按鈕，捲軸捲動到頂部
            })
			
            $(options.dom.godomid).click(function(){
                $('body,html').stop().animate({scrollTop:$toPosition},options.scrollspeed); //單擊按鈕，捲軸捲動到指定位置
            })
        });
    })
</script>

<!-- 查看全部按鈕 -->
<script>
/* 若已顯示筆數等於全部筆數，則按鈕不顯示 */
    var esOptions = {
        dom: {
            parent: ".bidList-group",
            bidList: ".bidList"
        }
    };
    function moreBtn () {
        var $parent = $(esOptions.dom.parent),
            $lists = $parent.find(esOptions.dom.bidList),
            $listCount = $lists.length,                                 //目前顯示的筆數
            $allCount = $('#allbidcount').val(),
            
            $link = "location.href='<?PHP echo BASE_URL.APP_DIR; ?>/bid/bidlist/?type=<?php echo $type;?>&channelid=<?php echo $channelid;?>&productid=<?php echo $productid;?>&perpage=50';",                           //查看更多 路徑
            $linkTxt = '看更多出價紀錄';                                  //查看更多 文字
        
            $parent.append(
                $('<div class="moreBtn"/>')
                .append(
                    $('<a onclick="'+$link+'"/>')
                    .text($linkTxt)
                )
            )
    }
    $(window).on('load',function(){
        moreBtn();
    });
</script>
