<html>
    <head>
        <meta charset="utf-8">

        <meta property="og:url"           content="<?php echo BASE_URL.$_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="新殺價王" />
		<meta property="og:description"   content="創新殺價式購物，突破傳統，出價最低唯一者可獲得商品，免費送下標券" />

		<meta property="og:image"         content="<?PHP echo APP_DIR; ?>/static/img/activity-share-imgcat.png" />
		<meta property="og:image:type"    content="image/png">
        <meta property="og:image:width"   content="1201">
        <meta property="og:image:height"  content="630">
        <title>殺價王 商家聯合行銷</title>
        <!-- RWD -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        <!--Web，Android，Microsoft和iOS（iPhone和iPad）應用程式圖標-->
        <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo APP_DIR; ?>/static/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?PHP echo APP_DIR; ?>/static/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo APP_DIR; ?>/static/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?PHP echo APP_DIR; ?>/static/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?PHP echo APP_DIR; ?>/static/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

		<!-- 瀏覽器初始化 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/reset.css">

        <!-- flexbox 框架 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/flexbox.css">

        <!-- shareoscode.CSS -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/shareoscode.min.css">

        <script src="<?PHP echo APP_DIR; ?>/static/js/jquery-2.1.4.js"></script>

        <script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/vendor/base64/base64.js"></script>

        <!-- 頁面loading -->
        <script type="text/javascript" src="<?php echo APP_DIR;?>/static/js/es-page-loading.js"></script>
    </head>
    <body id="sajabody">
        <?php
            $open_fb_share = _v('open_fb_share');
            $user_auth_id = _v('user_auth_id');
            $user_src = _v('user_src');
			$json = _v('json');
            $behav = _v('behav');
            $lbuserid = _v('lbuserid');
            $promote_menu = _v('promote_menu');
            $user_already_taken_oscode = _v('user_already_taken_oscode');
            $cdnTime = date("YmdHis");
        ?>

        <div id="shareoscode-activityhome">
			<?php if($json!='Y'){?>
            <div class="sajaNavBar">
                <div class="sajaNavBarIcon back">
                    <a href="<?PHP echo APP_DIR; ?>" class="ui-link">
                        <img src="<?PHP echo APP_DIR; ?>/static/img/back.png">
                    </a>
                </div>
                <div class="share-title">殺價王 商家聯合行銷</div>
            </div>
			<?php } ?>
            <div class="share-flexbox">
                <div class="activityhome-solgan-box">
                    <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-bg.jpg?<?php echo $cdnTime; ?>" alt="">
<!-- 舊banner 的遊戲規則按鈕 & 連結FB粉專按鈕 -->
<!--
                    <div class="header-info-box">
                        <div class="header-rule-btn">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-rule-btn.png" alt="">
                        </div>
                        <div class="header-txt">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-txt.png" alt="">
                        </div>
                        <div class="header-fb-bar d-flex align-items-center justify-content-center">
                            <div class="header-fb-btn">
                                <a href="https://www.facebook.com/newsajawang/" target="_blank">
                                    <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/activityhome-fb-btn.png" alt="">
                                </a>
                            </div>
                            <div class="header-gofb-btn">
                                <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/header-gofb-btn.png" alt="">
                            </div>
                        </div>

                    </div>
-->
<!-- / 舊banner 的遊戲規則按鈕 & 連結FB粉專按鈕 -->
                </div>
                <div class="activityhome-box">
                    <ul class="activityhome-ul">
                        <!-- 生成list -->
                    </ul>
                </div>
                <!-- 遊戲規則彈窗(有自己的遮罩) -->
                <div class="headerRuleModal" style="display:none">
                    <div class="img-box">
                        <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/open-game-rule.png" alt=""/>
                        <div class="headerRuleModal-close">關  閉</div>
                    </div>
                    <div class="headerRuleModal-cover"></div>
                </div>
            </div>
        </div>
        <script>
            var esActivity = {
                class:{
                    isOver: 'isover',
                },
                dom: {
                    ul: '.activityhome-ul',
                    li: '.activityhome-li'
                }
            }
            var $APP_DIR = '<?PHP echo APP_DIR; ?>';
			var $IMG_DIR = '<?PHP echo IMG_URL.APP_DIR; ?>';
            var b = new Base64();
            var user_already_taken_oscode = <?php echo $user_already_taken_oscode; ?>;      //使用者已領券的資訊
            var $arr_uato = $.parseJSON('<?php echo $user_already_taken_oscode;?>');

//            var productid = 8645;
//            if($arr_uato !== false){                                //使用者未登入
//                $.each($arr_uato, function( i, val ) {
//                    //console.log(val.productid);
//                    if(val.productid == productid ){
//                        console.log('hit');
//                    }
//                    return;
//                });
//            }
            $(function(){
                //header-規則按鈕
                $('.activityhome-solgan-box .header-rule-btn').on('click', function(){
                    $('.headerRuleModal').show();
                    $('#shareoscode-activityhome').addClass('modal-open');
                    $('.headerRuleModal-cover, .headerRuleModal-close').on('click', function(){
                        $('.headerRuleModal').hide();
                        $('#shareoscode-activityhome').removeClass('modal-open');
                        event.stopPropagation();
                    })
                });

                //生成list-group
                createLi();
                
                var $islogin = '<?php echo $user_auth_id ?>' || false;   //使用者登入判斷
                if(!$islogin){
                    loginModal();                   //登入視窗
                }

                //定期執行偵測結標商品
                var isTimeOut = setInterval(isTimeOutFun, 1000);
                
                //header - FB分享按鈕
                $('.header-gofb-btn').on('click', function(){
                    var user_auth_id = "<?php echo $user_auth_id; ?>";
                    var user_src = "<?php echo $user_src; ?>";
                    var behav = "<?php echo $behav; ?>";
                    var lbuserid = "<?php echo $lbuserid;?>";
                    if(user_auth_id !="" || user_auth_id != 0 ){
                        window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('<?php echo BASE_URL.APP_DIR; ?>/shareoscodestore/activityhome/?user_src='+user_auth_id+'&behav='+behav+'&storeid='+lbuserid)) );
                    }else{
                        window.location = '<?php echo BASE_URL.APP_DIR; ?>/shareoscodestore/share_login/?user_src='+user_src+'&behav='+behav+'&lbuserid='+lbuserid;
                    }
                })
            })

            //載入畫面
                //生成list
            function createLi(){
                var $noproImg = '<?php echo APP_DIR;?>/static/img/nohistory-img.png';
                var $arrLi = $.parseJSON('<?php echo $promote_menu?>');
                if($arrLi.length > 0){
                    $.each($arrLi, function(i,item){
                        var $isgot = false;                             //已領取狀態預設
                        var $thatPid = item['productid'];
                        if($arr_uato !== false){                        //修改領取狀態
                            $.map($arr_uato,function(item, index){
                                if(item['productid'] == $thatPid) {
                                    $isgot = true;
                                }
                            })
                        }
                        var $nowTime = new Date();                                  //現在時間
                        var $proOffTime = new Date(getDate(item['offtime']));       //商品結標時間
                        var $proOffTime_to_second = Math.floor($proOffTime / 1000);     //轉換uniTime 比較時用
                        var $isOverTime = $proOffTime - $nowTime;                   //計算是否結標 (小於0代表結標)
                        $(esActivity.dom.ul).append(
                            $('<li/>')
                            .addClass('activityhome-li d-flex align-items-stretch'+($isOverTime < 0 ?' isover':'')+($isgot == true ?' isgot':''))
                            .attr('id',item['productid'])
                            .attr('data-offtime',$proOffTime_to_second)
                            .append(
                                $('<div class="activityhome-img"/>')
                                .append(
                                    $('<img class="img-fluid"/>')
                                    .attr('src',$IMG_DIR+'/images/site/product/'+item['thumbnail'])
                                )
                            )
                            .append(
                                $('<div class="activityhome-infobox d-flex flex-column justify-content-around"/>')
                                .append(
                                    $('<div class="product-name d-flex text-left"/>')
                                    .append(
                                        $('<div class="content">'+item['name']+'</div>')
                                    )
                                )
                                .append(
                                    $('<div class="overtime d-inlineflex text-left flex-wrap"/>')
                                    .append(
                                        $('<div class="title">結標時間：</div>')
                                    )
                                    .append(
                                        $('<div class="title">'+item['offtime']+'</div>')
                                    )
                                )
                                .append(
                                    $('<div class="btn-box d-flex ml-auto flex-wrap"/>')
    //                                .append(
    //                                     $('<div class="btn-item description-btn">商品說明</div>')
    //                                )
    //                                .append(
    //                                     $('<div class="btn-item rule-btn">下標規則</div>')
    //                                )
    //                                .append(
    //                                     $('<div class="btn-item gosaja-btn">殺價去</div>')
    //                                )
                                    .append(
                                        //未領取 顯示 '領券去'
                                        //已領取 顯示 '已領取'
                                         $('<div class="btn-item getoscode-btn"/>')
                                        .text(($isgot == true ?' 已領取':'領券去'))
                                    )
                                    .append(
                                        //已領券 顯示分享按鈕
                                        ($isgot == true ?'<div class="btn-item fbshare-btn">分享領券</div>':'')
                                    )
                                    .append(
                                        //已領券 顯示殺價去
                                        ($isgot == true ?'<div class="btn-item gosaja-btn">殺價去</div>':'')
                                    )
                                )
                            )
                            .append(
                                //商品詳情，彈窗用
                                $('<input type="hidden" name="rule"/>')
                                .attr({'value':(item['rule'] == null || item['rule'] == '')?'':item['rule']})
                            )
                        )

                        //event - 領券去
                        $('#'+item['productid']+' .getoscode-btn').on('click', function(){
                            animate();                   //轉場
                            function animate($time){
                                esloading(true);
                                location.href = "<?php echo BASE_URL.APP_DIR; ?>/shareoscodestore/getActivityPromoteScode/?user_src=<?php echo $user_src ?>&productid="+item['productid']+"&behav=lb&lbuserid=<?php echo $lbuserid; ?>";
                            }
                        })
                        
                        //event - 殺價去
                        $('#'+item['productid']+' .gosaja-btn').on('click', function(){
                            location.href = '<?php echo BASE_URL.APP_DIR; ?>/product/saja/?channelid=1&productid='+item['productid']+'&type=';
                        });

                        //event - 商品說明
                        $('#'+item['productid']+' .activityhome-img').on('click', function(){
                            var $imgsrc = $IMG_DIR+'/images/site/product/'+item['thumbnail'];
                            var $proname = item['name'];
                            //rule 沒有值的時候顯示預設
                            var $proinfo_rule_default = '<ul>'+
                                    '<li>'+
                                        '<p class="detailed-title">下標方式：</p>'+
                                        '<p>每次下標只需支付手續費 $10/標 ， 如在時間截止後未得標，全部支出的手續費將轉為鯊魚點，存入您的帳戶中。</p>'+
                                    '</li>'+
                                    '<li>'+
                                        '<p class="detailed-title">得標方式：</p>'+
                                        '<p>在時間截止時，由出價最低且唯一者得標，得標者需在結帳時支付得標處理費 $299，若商品流標，下標時支付的手續費將退回殺價幣。</p>'+
                                    '</li>'+
                                    '<li class="detailed-title">'+
                                        '<p>商品由協力廠商提供，得標並完成訂單後，配合活動廠商發貨排程出貨，隨機出貨，不可選色、選款。若協力廠商缺貨或因其他不可抗力原因無法出貨，得標者須同意接受等同於本商品市價的殺價幣為替代方案，下標本商品則默認為同意不可抗力原因下的替代方案。</p>'+
                                    '</li>'+
                               '</ul>'
                            var $proinfo = item['description']==null || item['description']=='' ? '' : b.decode(item['description']);       //商品資訊
    // var $proinfo_rule = item['rule']==null || item['rule']=='' ? $proinfo_rule_default : b.decode(item['rule']);                //下標規則
                            //彈窗 - 商品詳情
                            createGetProductModal($imgsrc,$proname,$proinfo);
                        })
                        
                        //event - FB分享
                        $('#'+item['productid']+' .fbshare-btn').on('click',function(){
                            var user_auth_id = "<?php echo $user_auth_id; ?>";
                            var user_src = "<?php echo $user_src; ?>";
                            var behav = "<?php echo $behav; ?>";
                            var lbuserid = "<?php echo $lbuserid;?>";
                            if(user_auth_id !="" || user_auth_id != 0 ){
                                window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('<?php echo BASE_URL.APP_DIR; ?>/shareoscodestore/activityhome/?user_src='+user_auth_id+'&behav='+behav+'&storeid='+lbuserid)) );
                            }else{
                                window.location = '<?php echo BASE_URL.APP_DIR; ?>/shareoscodestore/share_login/?user_src='+user_src+'&behav='+behav+'&lbuserid='+lbuserid;
                            }
                        })
                    })
                }else{
                     $(esActivity.dom.ul).append(
                        $('<div class="nopro"/>')
                        .append(
                            $('<div class="nopro-img"/>')
                            .append(
                                $('<img class="img-fluid" src="'+$noproImg+'"/>')
                            )
                        )
                        .append(
                            $('<div class="nopro-text">目前無相關活動</div>')
                        )
                     )
                }
            }

            //開啟領券modal
            function createGetOscodeModal($date,$imgsrc,$proname,$oscodenum) {
                $('#shareoscode-activityhome').append(
                    $('<div class="getOscodeModal modalstyle"/>')
                    .append(
                        $('<div class="modal-overtime">結標時間：'+$date+'</div>')
                    )
                    .append(
                        $('<div class="modal-pro-box"/>')
                        .append(
                            $('<div class="pro-img-box"/>')
                            .append(
                                $('<img class="img-fluid" src="'+$imgsrc+'"/>')
                            )
                        )
                        .append(
                            $('<div class="pro-title">'+$proname+'</div>')
                        )
                        .append(
                            $('<div class="oscode-info"/>')
                            .append(
                                $('<div class="p1">成功獲取 殺價券 x '+$oscodenum+'</div>')
                            )
                        )
                    )
                    .append(
                        $('<div class="modal-btn-box d-flex"/>')
                        .append(
                            $('<div class="then-btn get-btn">繼續領券</div>')
                        )
                        .append(
                            $('<div class="then-btn go-btn">殺價去</div>')
                        )
                    )
                )
                .append(
                    $('<div class="modalcover"/>')
                )
                $('#shareoscode-activityhome').addClass('modal-open');
                //event - 關閉
                $('.modal-btn-box .then-btn').on('click', function(){
                    $('.getOscodeModal').remove();
                    $('.modalcover').remove();
                    $('#shareoscode-activityhome').removeClass('modal-open');
                });
            };

            //開啟商品資訊modal
            function createGetProductModal($imgsrc,$proname,$proinfo) {
                $('#shareoscode-activityhome').append(
                    $('<div class="getProductModal modalstyle"/>')
                    .append(
                        $('<div class="modal-proinfo-box"/>')
                        .append(
                            $('<div class="proinfo-img-box"/>')
                            .append(
                                $('<img class="img-fluid" src="'+$imgsrc+'"/>')
                            )
                        )
                        .append(
                            $('<div class="proinfo-title">'+$proname+'</div>')
                        )
                        .append(
                            $('<div class="proinfo-info"/>')
                            .append(
                                $('<div class="p1"/>')
                                .html($proinfo)
                            )
                        )
                    )
                    .append(
                        $('<div class="modal-proinfo-close"/>')
                        .append(
                            $('<img class="img-fluid"/>')
                            .attr('src','<?PHP echo APP_DIR; ?>/static/img/close-circle.png')
                        )
                    )
                )
                .append(
                    $('<div class="modalcover"/>')
                )
                $('#shareoscode-activityhome').addClass('modal-open');
                $('.modalcover').on('click', function(e){
                    e.preventDefault();
                    e.stopPropagation();
                })
                
                //event - 關閉按鈕
                $('.modal-proinfo-close').on('click', function(){
                    $('.getProductModal').remove();
                    $('.modalcover').remove();
                    $('#shareoscode-activityhome').removeClass('modal-open');
                })
                
                //event - 點遮罩關閉
                $('.modalcover').on('click', function(){
                    $('.getProductModal').remove();
                    $('.modalcover').remove();
                    $('#shareoscode-activityhome').removeClass('modal-open');
                })
            }
            
            //請求登入modal
            function loginModal() {
                $('#shareoscode-activityhome').append(
                    $('<div class="loginModal modalstyle"/>')
                    .append(
                        $('<div class="login-modalTitle">登入提醒</div>')
                    )
                    .append(
                        $('<div class="login-modalBody"/>')
                        .append(
                            $('<div class="login-info text-center">請先登入或註冊會員!</div>')
                        )
                    )
                    .append(
                        $('<div class="login-modalFooter"/>')
                        .append(
                            $('<div class="login-btn">前往登入或註冊</div>')
                        )
                    )
                )
                .append(
                    $('<div class="modalcover"/>')
                )
                $('#shareoscode-activityhome').addClass('modal-open');
                
                //event - 前往登入 按鈕
                $('.login-btn').on('click', function(){
                    location.href = $APP_DIR+'/shareoscodestore/share_login/?user_src=<?php echo $user_src ?>&behav=lb&lbuserid=<?php echo $lbuserid; ?>';
                })
            }

            //ios 時間問題轉換
            function getDate(time) {
                //將xxxx-xx-xx的時間格式，轉換為 xxxx/xx/xx的格式
                time = time.replace(/\-/g, "\/");
                return time;
            };

            //持續判斷商品是否結標
            function isTimeOutFun() {
                var $nowT = Date.now();                                  //現在時間
                var $nowTS = Math.floor($nowT / 1000);                   //轉換uniTime 比較用
                var $ligroup = $(esActivity.dom.ul).find(esActivity.dom.li+':not(".isover")');      //抓取未結標
                $ligroup.each(function(){
                    var $that = $(this);
                    var $offTS = $that.attr('data-offtime');
                    if ($nowTS > $offTS) {          //結標
                        $that.addClass('isover');
                    }
                })
            }

            //FB登入後自動另開FB分享頁面
            window.onload = function() {
              var open_fb_share = "<?php echo $open_fb_share; ?>";
              var user_auth_id = "<?php echo $user_auth_id; ?>";
              var behav = "<?php echo $behav; ?>";
              var lbuserid = "<?php echo $lbuserid;?>";
              if(open_fb_share == 'Y'){
                window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('<?php echo BASE_URL.APP_DIR; ?>/shareoscodestore/activityhome/?user_src='+user_auth_id+'&behav='+behav+'&storeid='+lbuserid)));
              }
            }
        </script>
    </body>
</html>
