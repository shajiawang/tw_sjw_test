<html>
    <head>
        <meta property="og:url"           content="<?php echo BASE_URL.$_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="新殺價王" />
		<meta property="og:description"   content="創新殺價式購物，突破傳統，出價最低唯一者可獲得商品，免費送下標券"/>
		
		<meta property="og:image"         content="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/share-breaks-img.png" />
		<meta property="og:image:type"    content="image/png">
        <meta property="og:image:width"   content="1200">
        <meta property="og:image:height"  content="630">
        
        <!-- RWD -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        
        <!--Web，Android，Microsoft和iOS（iPhone和iPad）應用程式圖標-->
        <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		  
		<!-- 瀏覽器初始化 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/reset.css">

        <!-- flexbox 框架 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/flexbox.css">

        <!-- shareoscode.CSS -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/shareoscode.min.css">

        <script src="<?PHP echo APP_DIR; ?>/static/js/jquery-2.1.4.js"></script>
    </head>
    <body>
        <?php
            $retCode = _v('retCode');
            $retMsg = _v('retMsg');
            $oscode_num = _v('oscode_num');
            $product_id = _v('product_id');
            $product_name = _v('product_name');
            $product_pic = _v('product_pic');
            $activity_name = _v('activity_name');
            $behav =  _v('behav');
            $lbuserid = _v('lbuserid');
            $userid = _v('userid');
        ?>
    <div id="shareoscode-endpage">
        <div class="share-title">我是殺價王live show 看直播送下標券</div>
        <div class="shareend-flexbox">     
        <?php if($behav == 'b'){?>
            <?php if($retCode == '1'){?>
<!--
                <div class="title-box">
                    <p>恭喜您</p>
                    <p>獲得<span class="proName"><?php echo $product_name ?></span></p>
                    <p><span class="num"><?php echo $oscode_num ?></span>張 殺價券</p>
                </div>
                <div class="gosaja-btn">殺價去</div>
-->
            <?php }else{ ?>
                <!-- 無法領取顯示頁 -->
<!--
                <div class="no-oscode">
                    <div class="img-box">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/bid-checkout-img2.png" alt="">
                    </div>
                    <div class="msgbox">
                        <div class="msg"><?php echo $retMsg ?></div>
                    </div>
                    <div class="gohome">回首頁</div>
                </div>
-->
            <?php } ?>

        <?php }else if($behav == 'lb'){ ?>
            <?php if($retCode == '1'){?>
                <div class="sharetxt-box">
                    <div class="pro-img">
                        <img class="img-fluid" src="<?php echo $product_pic ?>" alt="">
                    </div>
                    <div class="info-box">
                        <div class="text-box">
                            <p>恭喜您獲得</p>
                            <p><span class="proName"><?php echo $product_name ?></span></p>
                            <p>殺價券 <span class="num"><?php echo $oscode_num ?></span>張</p>
                        </div>
                        <div class="info-img">
                            <img class="info-img-left" src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/trophy-left.png" alt="">
                            <img class="info-img-right" src="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/trophy-right.png" alt="">
                        </div>
                    </div>
                    
                </div>
                <div class="footer-btn-box">
<!--                    <div class="fbfan-btn">FB粉絲專頁 觀看直播</div>-->
                    <div class="gosaja-btn">殺價去</div>
<!--
                    <div class="getmoreoscode-btn">FB 分享送更多</div>
                    
                    <div class="list-item d-flex align-items-start">
                        <i class="icon">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/share-img002.png" alt="">
                        </i>
                        <div class="icon-box text-left">
                            <div class="txt">FB分享朋友領免費下標券，你也能領免費下標券,分享越多領更多。</div>
                        </div>
                    </div>
-->
                </div>
                
            <?php }else if($retCode == '0'){ ?>
                <div class="title-box">
                    <p>本檔活動已結束!!</p>
<!--                    <p>其他活動時間如下</p>-->
                </div>
                <div class="info">
<!--
                    <div class="d1">
                        <p class="p1">我是殺價王直播活動</p>
                        <p class="p2">12/6-12/9日</p>
                    </div>
                    <div class="d2">
                        <p class="title">本次殺品：</p>
                        <p class="p1">無敵破壞王電影票（西門今日秀泰）</p>
                        <p class="p2">包場電影12/12日 晚上7-8點放映</p>
                    </div>
                    <div class="d3">
                        <p class="title">殺價直播時間：</p>
                        <p class="p2">12/6-12/9 晚上8:00-10:00</p>
                    </div>
                    <div class="ps">
                        <p class="small">點擊下方 新殺價王 粉絲專頁 按鈕</p>
                        <p class="small">按讚 並 觀賞直播，領取免費下標券</p>
                        <div class="gofanpage">
                            <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/fbfanpage.png" alt="">
                        </div>
                    </div>
-->
                    <div class="activity-img">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/share-activity.png" alt="">
                    </div>
                    
                </div>
<!--                <div class="gomore">看更多商品</div>-->
<!--                <div class="getmoreoscode-btn">分享送更多</div>-->
            <?php }else if($retCode == '-1'){ ?>
                <div class="title-box">
                    <p>您已領過本檔殺價券!!</p>
<!--                    <p>其他活動時間如下</p>-->
                </div>
                <div class="info">
<!--
                    <div class="d1">
                        <p class="p1">我是殺價王直播活動</p>
                        <p class="p2">12/6-12/9日</p>
                    </div>
                    <div class="d2">
                        <p class="title">本次殺品：</p>
                        <p class="p1">無敵破壞王電影票（西門今日秀泰）</p>
                        <p class="p2">包場電影12/12日 晚上7-8點放映</p>
                    </div>
                    <div class="d3">
                        <p class="title">殺價直播時間：</p>
                        <p class="p2">12/6-12/9 晚上8:00-10:00</p>
                    </div>
                    <div class="ps">
                        <p class="small">點選下方 新殺價王 粉絲專頁 按鈕</p>
                        <p class="small">按讚 並 觀賞直播，領取免費下標券</p>
                        <div class="gofanpage">
                            <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/fbfanpage.png" alt="">
                        </div>
                    </div>
-->
                    <div class="activity-img">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/share-activity.png" alt="">
                    </div>
                </div>
                <div class="footer-btn-box">
                    <div class="gosaja-btn">殺價去</div>
    <!--                <div class="gomore">看更多商品</div>-->
<!--
                    <div class="getmoreoscode-btn">FB 分享送更多</div>
                    
                    <div class="list-item d-flex align-items-start">
                        <i class="icon">
                            <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/share-img002.png" alt="">
                        </i>
                        <div class="icon-box text-left">
                            <div class="txt">FB分享朋友領免費下標券，你也能領免費下標券,分享越多領更多。</div>
                        </div>
                    </div>
-->
                </div>
            <?php }else{ ?>
                <!-- 無法領取顯示頁 -->
                <div class="title-box top5r">
                    <p>資料錯誤!!</p>
                    <p>請重新操作</p>
                </div>
                <div class="no-oscode">
                    <div class="img-box">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/share-error-icon.png" alt="">
                    </div>
<!--
                    <div class="msgbox">
                        <div class="msg"><?php echo $retMsg ?></div>
                    </div>
-->
                    <div class="gohome" style="margin-top:2rem;">返回「新殺價王」首頁</div>
                </div>
            <?php } ?>
        <?php } ?>

        </div>
    </div>

    <script>
//        console.log('$retCode = <?php echo $retCode?>');
//        console.log('$retMsg = <?php echo $retMsg?>');
//        console.log('$oscode_num = <?php echo $oscode_num?>');
//        console.log('$product_id = <?php echo $product_id?>');
//        console.log('$product_name = <?php echo $product_name?>');
//        console.log('$product_pic = <?php echo $product_pic?>');
//        console.log('$activity_name = <?php echo $activity_name?>');
//        console.log('$behav = <?php echo $behav?>');
//        console.log('$lbuserid = <?php echo $lbuserid?>');
//        console.log('$userid = <?php echo $userid?>');
        
        $('.gosaja-btn').on('click', function(){
            location.href = '<?php echo BASE_URL.APP_DIR;?>/product/saja/?channelid=1&productid=<?php echo $product_id; ?>&type=&t=20181205092845';
        });
        $('.gohome').on('click', function(){
            location.href = '<?php echo BASE_URL.APP_DIR;?>';
        })
        $('.gomore').on('click', function(){
            location.href = '<?php echo BASE_URL.APP_DIR;?>';
        })
        $('.getmoreoscode-btn').on('click', function(){
            //user_src 與 productid 沿用上面的參數
            window.open('http://www.facebook.com/share.php?u='.concat(encodeURIComponent('<?PHP echo BASE_URL.APP_DIR; ?>/shareoscodestore/?user_src=<?php echo $userid ?>&productid=<?php echo $product_id ?>&behav=lb&lbuserid=<?php echo $lbuserid ?>')) );
        })
        $('.activity-img').on('click', function(){
            location.href = 'https://www.facebook.com/newsajawang/';
        })
        $('.fbfan-btn').on('click', function(){
            location.href = 'https://www.facebook.com/newsajawang/';
        })
    </script>
    </body>
</html>
