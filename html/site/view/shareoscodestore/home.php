<html>
    <head>
        <meta property="og:url"           content="<?php echo BASE_URL.$_SERVER['REQUEST_URI']; ?>" />
		<meta property="og:type"          content="website" />
		<meta property="og:title"         content="新殺價王" />
		<meta property="og:description"   content="創新殺價式購物，突破傳統，出價最低唯一者可獲得商品，免費送下標券" />
		
		<meta property="og:image"         content="<?php echo $product_pic ?>" />
		<meta property="og:image:type"    content="image/png">
        <meta property="og:image:width"   content="1200">
        <meta property="og:image:height"  content="630">
        
        <!-- RWD -->
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />
        
        <!--Web，Android，Microsoft和iOS（iPhone和iPad）應用程式圖標-->
        <link rel="apple-touch-icon" sizes="57x57" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/favicon-16x16.png">
        <link rel="manifest" href="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="<?PHP echo BASE_URL.APP_DIR; ?>/static/img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
		  
		<!-- 瀏覽器初始化 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/reset.css">

        <!-- flexbox 框架 -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/flexbox.css">
        
        <!-- shareoscode.CSS -->
        <link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/shareoscode.min.css">

        <script src="<?PHP echo APP_DIR; ?>/static/js/jquery-2.1.4.js"></script>
    </head>
    <body>
        <?php
            $to = _v('to');
            $state =  _v('state');
            $send_url = BASE_URL.APP_DIR."/oauthapi/r/?_to=".$to."&state=".$state;
            $product_pic = _v('product_pic');
            $product_name = _v('product_name');
            $oscode_num = _v('oscode_num');
            $product_rule = _v('product_rule');
        ?>
        
        <div id="shareoscode-home">
            <div class="share-title">我是殺價王live show 看直播送下標券</div>
            <div class="share-flexbox">
               
<!--  判斷 (有商品)-->
               
                <div class="pro-head">
                    <div class="pro-img">
                        <img class="img-fluid" src="<?php echo $product_pic ?>" alt="">
                    </div>

                    <div class="pro-name">
                        <div class="title"><?php echo $product_name ?></div>
                    </div>
                </div>
                <div class="getoscode-btn">領取免費殺價券</div>
                <div class="pro-list">
                    <div class="list-item d-flex align-items-start">
                        <i class="icon">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/share-img0.png" alt="">
                        </i>
                        <div class="icon-box text-left">
                            <div class="title">使用規則</div>
                            <div class="txt"><?php echo $product_rule ?></div>
                        </div>
                    </div>
                </div>
                <div class="oscode d-flex">
                    <div class="title mr-auto">免費下標券：</div>
                    <div class="info"><span class="num"><?php echo $oscode_num ?></span>張</div>
                </div>
                
<!--  判斷 (無商品)-->    
<!--
                <div class="nohistory-box">
                    <div class="nohistory-img">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png" alt="">
                    </div>
                    <div class="nohistory-txt">查無此商品</div>
                </div>
-->
                
            </div>
        </div>
        <script>
//            console.log('$to = <?php echo $to ?>');
//            console.log('$state = <?php echo $state ?>');
//            console.log('$send_url = <?php echo $send_url ?>');
//            console.log('$product_pic = <?php echo $product_pic ?>');
//            console.log('$product_name = <?php echo $product_name ?>');
//            console.log('$oscode_num = <?php echo $oscode_num ?>');
            
            
            $('.getoscode-btn').on('click', function(){
                location.href = '<?php echo $send_url ?>';
            })
        </script>
    </body>
</html>

