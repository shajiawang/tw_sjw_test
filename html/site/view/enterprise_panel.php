<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
?>
    <div data-role="panel" id="rpanel<?php get_panel_id(); ?>" data-display="overlay" data-position="right">
        <ul data-role="listview" data-icon="false">            
<?php if(!empty($_SESSION['sajamanagement']['enterprise']['enterpriseid'])){ ?>         
			<li><a href="<?php echo APP_DIR; ?>/enterprise/member/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-user ui-btn-icon-right">商户信息</a></li>
			<li><a href="<?php echo APP_DIR; ?>/enterprise/member_update/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-user ui-btn-icon-right">商户信息修改</a></li>
            <li><a href="<?php echo APP_DIR; ?>/enterprise/storebonuslist/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-mail ui-btn-icon-right">交易纪录</a></li>
			<li><a href="<?php echo APP_DIR; ?>/enterprise/storeproduct/?flash=Y&<?php echo $cdnTime; ?>" class="ui-btn ui-icon-plus ui-btn-icon-right">闪殺商品</a></li>
	<?php if($_SESSION['sajamanagement']['enterprise']['sms'] == 'Y'){ ?>         
			<li><a href="<?php echo APP_DIR; ?>/enterprise/cash/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-plus ui-btn-icon-right">商户提現</a></li>
	<?php } else { ?>
			<li><a href="javascript:alert('請先至商户信息中心，進行手機驗證!!')" class="ui-btn ui-icon-plus ui-btn-icon-right">商户提現</a></li>
	<?php } ?>
			<li><a  href="<?php echo APP_DIR; ?>/enterprise/logout/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-power ui-btn-icon-right" >退出</a></li>
<?php }  ?>
			<!-- li><a href="#" class="ui-btn ui-icon-mail ui-btn-icon-right">站内公告</a></li -->
			<li data-icon="star"><a href="<?php echo APP_DIR; ?>/enterprise/home/?<?php echo $cdnTime; ?>">回首页</a></li>
		</ul>
    </div><!-- /panel -->

</div><!--data-role="page"-->
</body>
</html>