<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$gmap = _v('gmap');
$maplocation = _v('maplocation');
$mapstate = _v('mapstate');
?>	
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="<?PHP echo APP_DIR; ?>/javascript/jquery.tinyMap.min.js"></script>
		<div class="article">
            <div data-role="collapsible" data-collapsed="false" data-iconpos="right" data-collapsed-icon="heart" data-expanded-icon="heart">
                <h4><?php echo $gmap; ?></h4>
                <p>
                	<div id="onlyContent"><?php echo $gmap; ?></div>
                	<div id="map"></div>
                </p>
            </div>
        </div><!-- /article -->
<script>
$(window).resize(function() {
	var w = $(".article").width() * 0.9;
	var h = w * 0.75;
	$("#map").css({"width" : w, "height" : h});
});

$(window).load(function() {
	//var w = $(".article").width() * 0.9;
	//var h = w * 0.75;
	//$("#map").css({"width" : w, "height" : h});
	$(window).resize();
	
	var state = "<?php echo $mapstate; ?>";	
	if (state == "loc") {
		$('#map').tinyMap({
			center: "<?php if($maplocation['from'] == '這裡' || $maplocation['from'] == '这里') { echo '台北市八德路三段2號'; } else if ($maplocation['from'] == '最近711' || $maplocation['from'] == '最近合作商家') { echo '台北市松山區敦化南路一段5號'; } else if ($maplocation['from'] == '最近的阮的肉干' || $maplocation['from'] == '最近的软的肉干') { echo '台北市復興南路一段39號B1'; } else if ($maplocation['from'] == '最近的御之饌' || $maplocation['from'] == '最近的御之馔' || $maplocation['from'] == '最近的御之转') { echo '台北市信義區東興路45號B1'; } else if ($maplocation['from'] == '最近的桃源製茶所' || $maplocation['from'] == '最近的桃源制茶所' || $maplocation['from'] == '最近的桃元至察素' || $maplocation['from'] == '最近的桃源至察素五' || $maplocation['from'] == '最近的桃源至察素我') { echo '雲林縣古坑鄉桂林村桃源路3號'; } else { echo $maplocation['from']; } ?>",
			zoom: 13,
			marker: [{
		    	addr: "<?php if($maplocation['from'] == '這裡' || $maplocation['from'] == '这里') { echo '台北市八德路三段2號'; } else if ($maplocation['from'] == '最近711' || $maplocation['from'] == '最近合作商家') { echo '台北市松山區敦化南路一段5號'; } else if ($maplocation['from'] == '最近的阮的肉干' || $maplocation['from'] == '最近的软的肉干') { echo '台北市復興南路一段39號B1'; } else if ($maplocation['from'] == '最近的御之饌' || $maplocation['from'] == '最近的御之馔' || $maplocation['from'] == '最近的御之转') { echo '台北市信義區東興路45號B1'; } else if ($maplocation['from'] == '最近的桃源製茶所' || $maplocation['from'] == '最近的桃源制茶所' || $maplocation['from'] == '最近的桃元至察素' || $maplocation['from'] == '最近的桃源至察素五' || $maplocation['from'] == '最近的桃源至察素我') { echo '雲林縣古坑鄉桂林村桃源路3號'; } else { echo $maplocation['from']; } ?>",
		        text: "<?php echo $maplocation['from']; ?>"
		    }]
		    
		});
	}
	else if (state == "nav") {
		$('#map').tinyMap({
			center: "<?php if($maplocation['from'] == '這裡' || $maplocation['from'] == '这里') { echo '台北市八德路三段2號'; } else if ($maplocation['from'] == '最近711' || $maplocation['from'] == '最近合作商家') { echo '台北市松山區敦化南路一段5號'; } else if ($maplocation['from'] == '最近的阮的肉干' || $maplocation['from'] == '最近的软的肉干') { echo '台北市復興南路一段39號B1'; } else if ($maplocation['from'] == '最近的御之饌' || $maplocation['from'] == '最近的御之馔' || $maplocation['from'] == '最近的御之转') { echo '台北市信義區東興路45號B1'; } else if ($maplocation['from'] == '最近的桃源製茶所' || $maplocation['from'] == '最近的桃源制茶所' || $maplocation['from'] == '最近的桃元至察素' || $maplocation['from'] == '最近的桃源至察素五' || $maplocation['from'] == '最近的桃源至察素我') { echo '雲林縣古坑鄉桂林村桃源路3號'; } else { echo $maplocation['from']; } ?>",
		    zoom: 13,
		    direction: [{
		        from: "<?php if($maplocation['from'] == '這裡' || $maplocation['from'] == '这里') { echo '台北市八德路三段2號'; } else if ($maplocation['from'] == '最近711' || $maplocation['from'] == '最近合作商家') { echo '台北市松山區敦化南路一段5號'; } else if ($maplocation['from'] == '最近的阮的肉干' || $maplocation['from'] == '最近的软的肉干') { echo '台北市復興南路一段39號B1'; } else if ($maplocation['from'] == '最近的御之饌' || $maplocation['from'] == '最近的御之馔' || $maplocation['from'] == '最近的御之转') { echo '台北市信義區東興路45號B1'; } else if ($maplocation['from'] == '最近的桃源製茶所' || $maplocation['from'] == '最近的桃源制茶所' || $maplocation['from'] == '最近的桃元至察素' || $maplocation['from'] == '最近的桃源至察素五' || $maplocation['from'] == '最近的桃源至察素我') { echo '雲林縣古坑鄉桂林村桃源路3號'; } else { echo $maplocation['from']; } ?>",
		        <?php echo $maplocation['waypoint']; ?>
		        to: "<?php if($maplocation['to'] == '這裡' || $maplocation['to'] == '这里') { echo '台北市八德路三段2號'; } else if ($maplocation['to'] == '最近711' || $maplocation['to'] == '最近合作商家') { echo '台北市松山區敦化南路一段5號'; } else if ($maplocation['to'] == '最近的阮的肉干' || $maplocation['to'] == '最近的软的肉干') { echo '台北市復興南路一段39號B1'; } else if ($maplocation['to'] == '最近的御之饌' || $maplocation['to'] == '最近的御之馔' || $maplocation['to'] == '最近的御之转') { echo '台北市信義區東興路45號B1'; } else if ($maplocation['to'] == '最近的桃源製茶所' || $maplocation['to'] == '最近的桃源制茶所' || $maplocation['to'] == '最近的桃元至察素' || $maplocation['to'] == '最近的桃源至察素五' || $maplocation['to'] == '最近的桃源至察素我') { echo '雲林縣古坑鄉桂林村桃源路3號'; } else { echo $maplocation['to']; } ?>",
		        travel: "driving",
		        panel: "#direction-panel" // 導航資訊 DOM 容器
		    }]
		});
	}
});

var dataForWeixin={
    appId:"",
    MsgImg:"http://farm1.staticflickr.com/12/68349395_3d0c98ff8b.jpg",
    TLImg:"http://farm1.staticflickr.com/12/68349395_3d0c98ff8b.jpg",
    url:location.href,
    title:"GoogleMap",
    desc:"<?php echo $gmap; ?>",
    fakeid:"",
    callback:function(){}
};
(function(){
    var onBridgeReady=function(){
        WeixinJSBridge.on('menu:share:appmessage', function(argv){
            WeixinJSBridge.invoke('sendAppMessage',{
                "appid":dataForWeixin.appId,
                "img_url":dataForWeixin.MsgImg,
                "img_width":"120",
                "img_height":"120",
                "link":dataForWeixin.url,
                "desc":dataForWeixin.desc,
                "title":dataForWeixin.title
            }, function(res){(dataForWeixin.callback)();});
        });
        WeixinJSBridge.on('menu:share:timeline', function(argv){
            (dataForWeixin.callback)();
            WeixinJSBridge.invoke('shareTimeline',{
                "img_url":dataForWeixin.TLImg,
                "img_width":"120",
                "img_height":"120",
                "link":dataForWeixin.url,
                "desc":dataForWeixin.desc,
                "title":dataForWeixin.title
            }, function(res){});
        });
        WeixinJSBridge.on('menu:share:weibo', function(argv){
            WeixinJSBridge.invoke('shareWeibo',{
                "content":dataForWeixin.title,
                "url":dataForWeixin.url
            }, function(res){(dataForWeixin.callback)();});
        });
        WeixinJSBridge.on('menu:share:facebook', function(argv){
            (dataForWeixin.callback)();
            WeixinJSBridge.invoke('shareFB',{
                "img_url":dataForWeixin.TLImg,
                "img_width":"120",
                "img_height":"120",
                "link":dataForWeixin.url,
                "desc":dataForWeixin.desc,
                "title":dataForWeixin.title
            }, function(res){});
        });
    };
    if(document.addEventListener){
        document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
    }else if(document.attachEvent){
        document.attachEvent('WeixinJSBridgeReady'   , onBridgeReady);
        document.attachEvent('onWeixinJSBridgeReady' , onBridgeReady);
    }
})();
</script>