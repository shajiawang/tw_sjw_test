<?php 
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$deposit_rule = _v('deposit_rule'); 
$get_deposit = _v('get_deposit');
$get_scode_promote = _v('get_scode_promote');
$bank_list=_v('bank_list');
$spmemo = _v('spmemo');
?>
		
		<div class="article">
            <form id="deposit-<?php echo $cdnTime; ?>" name="deposit-rule-item" method="post" action="<?php echo $config[$deposit_rule[0]['act']]['htmlurl']; ?>">
			<ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                	<div id="tagBox">
                        <img src="<?php echo $config['path_image'];?>/<?php echo $deposit_rule[0]['logo']; ?>">
                    </div>
                </li>
		    <?php if ($get_deposit['drid'] == 4) { //銀聯 ?>
						<li>发卡行 : 
							<select name="issBankNo" >
								<option value="">(請選擇)</option>
					            <?php foreach($bank_list as $key=>$value) {	?>
									<option value="<?php echo $key; ?>" ><?php echo $value; ?></option> 
								<?php }			?>
							</select>
						</li>
            <?php  }  ?> 
			<?php if ($get_deposit['drid'] == 6){ // ibonpay ?>
                        <li> 
					    <PRE>储值流程：
								1.按下方 "確定充值"，系统将产生缴費代碼并显示于页面 (如需多组代碼缴費，請务必每组间隔三分钟后，再取得新代碼)。
								2.至超商機台(殺價王合作超商 : FamilyMart(全家)、7-ELEVEN(统一)、Hi-Life(莱尔富))操作输入缴費代碼后打印缴費单。
								3.打印成功后，持缴費单至柜台缴費。
								4.缴費完成后3-30分钟，殺價幣就会汇入您的帳户中。
								5.缴費收据請妥善保存备查，以保障您的权益。
						</PRE>
					       <br>
					    </li>
					    	        
               
			<?php  }   ?>
			<?php  if($get_deposit['drid']!= 7) {?>
						<li>充值點數：<?php echo $get_deposit['name']; ?></li>
						<li>手續費  ：2％ </li>
						<?php  
								$currency="RMB";
								if($get_deposit['drid']==6 || $get_deposit['drid']==8) {
								   $currency="NTD";
								}
						?>
						<li>交易金額：<b><?php echo $currency." ".sprintf ("%u", $get_deposit['amount']); ?> 元</b></li>
						<li>备    注：<h2><?php echo nl2br($spmemo); ?></h2></li>
						<?php if($get_deposit['drid']==6 || $get_deposit['drid']==8) { ?>
						          * 殺價幣1點為NTD 5.5元								  
						<?php } ?>
						<li>
							<button id="Submit" type="button" class="ui-btn ui-corner-all ui-btn-a" 
									onClick="chkBankForDeposit(this.form);">確定充值</button>
						</li>
						
					</ul>
            <?php } else if ($get_deposit['drid']==7) { // Hinet點數兌換 	?>
                    <table width="45%" border="0" align="center" cellpadding="0" cellspacing="0">
						<tr>
							<td height="30" width="225" align="right" valign="middle">
								<font color="#FF5D00" size="4" style="font-family: 標楷體;"><b>序號:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font>
							</td>
							<td height="30" align="left" valign="middle">
								<input type="text" name="serial_no" size="20">
							</td>
						</tr>
						<tr>
							<td height="30" width="225" align="right" valign="middle">
								<font color="#FF5D00" size="4" style="font-family: 標楷體;"><b>密碼:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font>
							</td>
							<td height="30" align="left" valign="middle">
								<input type="text" name="passw" size="20">
							</td>
						</tr>
						<tr>
							<td height="30" width="225" align="right" valign="middle">
								<font color="#FF5D00" size="4" style="font-family: 標楷體;"><b>驗證碼:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</b></font>
							</td>
							<td height="30" align="left" valign="middle">
								<input id="txtInput" name="txtInput" type="text" style="border: 1px solid #C4C4C4; width:100px; height:20px;" />
								<span title="點选图档更换驗證碼" id = "show_img" onClick="DrawCaptcha();" style="cursor:pointer;"></span>
								<input size="8" type="hidden" id="txtCaptcha" style="background-image:url('<?php echo BASE_URL.$config["path_image"]; ?>/captcha/captcha.jpg'); text-align:center; border:none; font-weight:bold; font-family:Modern; font-color:0000FF;" />
							</td>
						</tr>	
						</table>
						<table width="50%" border="0" align="center">
						<tr>
							<td align="center" style="padding: 15px 15px 0px 15px; ">
								<input type="button" onClick="send_data(this.form);" value="兌換殺價幣">
							</td>
						</tr>
					</table>
			<?php } ?>
            <?php if ($get_deposit['drid'] == 1) { //支付寶 ?>
					<!--卖家支付宝帳户-->
					<input type="hidden" name="WIDseller_email" value="<?php echo $config['alipay']['sellemail']; ?>" />
					<!--商户订单號-->
					<input type="hidden" name="WIDout_trade_no" value="<?php echo $get_deposit['ordernumber']; ?>" />
					<!--订单名称-->
					<input type="hidden" name="WIDsubject" value="<?php echo $get_deposit['name']; ?>" />
					<!--付款金額-->
					<?php if ($_SESSION['auth_id'] == 28 || $_SESSION['auth_id'] == 1  || $_SESSION['auth_id'] == 116 ) { ?>
						<input type="hidden" name="WIDtotal_fee" value="0.01" />
					<?php } else { ?>
						<input type="hidden" name="WIDtotal_fee" value="<?php echo $get_deposit['amount']; ?>" />
					<?php } ?>
            <?php } else if ($get_deposit['drid'] == 4) { //銀聯 ?>
					<input type="hidden" name="drid" value="<?php echo $get_deposit['drid']; ?>">
					<input type="hidden" name="merCert" value="">
					<!--消息版本號-->
					<input type = "hidden" name = "interfaceVersion" value = "<?php echo($config['bankcomm']['interfaceVersion']); ?>">
					<!--商户號-->
					<input type = "hidden" name = "merID" value = "<?php echo($config['bankcomm']['merchID']); ?>">
					<!--订单號-->
					<input type = "hidden" name = "orderid" value = "<?php echo $get_deposit['ordernumber']; ?>">
					<!--交易時間-->
					<?php
						$Ymd=date("Ymd");
						$His=date("His");
					?>
					<input type = "hidden" name = "orderDate" value = "<?php echo($Ymd); ?>">
					<input type = "hidden" name = "orderTime" value = "<?php echo($His); ?>">
					<!--交易类别-->
					<input type = "hidden" name = "tranType" value = "<?php echo($config['bankcomm']['tranType']); ?>">
					<!--交易金額-->
					<?php if ($_SESSION['auth_id'] == 28 || $_SESSION['auth_id'] == 1 || $_SESSION['auth_id'] == 116) { ?>
					<input type = "hidden" name = "amount" value = "0.01">
					<?php } else { ?>
					<input type = "hidden" name = "amount" value = "<?php echo sprintf ("%01.2f", $get_deposit['amount']); ?>">
					<?php } ?>
					<!--交易幣种-->
					<input type = "hidden" name = "curType" value = "<?php echo($config['bankcomm']['curType']); ?>">
					<!--订单内容-->
					<input type = "hidden" name = "orderContent" value = "<?php echo "Spts:".sprintf ("%01.2f",$get_deposit['spoint']); ?>" >
					<!--商家备注-->
					<input type = "hidden" name = "orderMono" value = "<?php echo "OdrID:".$get_deposit['ordernumber']
																				 ."/OdrTime:".$Ymd.$His
																				 ."/Pay:".sprintf("%01.2f", $get_deposit['amount'])
																				 ."/Spts:".sprintf ("%01.2f",$get_deposit['spoint']);
																		?>">          
					<!--通知方式 0 不通知 1 通知 2 抓取页面 -->
					<input type="hidden" name="notifyType" value="2">
					<input type="hidden" name="goodsURL" value="<?php echo ($config['bankcomm']['goodsURL']); ?>" >
					<input type="hidden" name="merURL" value="<?php echo ($config['bankcomm']['merURL']); ?>" >
					<input type="hidden" name = "jumpSeconds" value = "0">
					<!--渠道编號 0:html渠道 -->
					<input type="hidden" name="netType" value="0">
					<?php } else if ($get_deposit['drid'] == 5) { //微信支付 ?>
					<!--订单號-->
					<input type="hidden" name="orderid" value="<?php echo $get_deposit['ordernumber']; ?>">
					<!--交易金額-->
					<?php if ($_SESSION['auth_id'] == 28 || $_SESSION['auth_id'] == 1  || $_SESSION['auth_id'] == 116 ) { ?>
						<input type="hidden" name="amount" value="0.01">
					<?php } else { ?>
						<input type="hidden" name="amount" value="<?php echo $get_deposit['amount']; ?>">
					<?php } ?>
			<?php } else if($get_deposit['drid'] == 6) { // ibonpay ?>
					<input type="hidden" name="pay_type" value="1">
					<input type="hidden" name="amount"  value="<?php echo sprintf ("%u", $get_deposit['amount']); ?>">
					<input type="hidden" name="ordernumber" value="<?php echo $get_deposit['ordernumber']; ?>" >
			<?php } else if ($get_deposit['drid'] == 7) { //Hinet pts ?>
					<input type = "hidden" name = "orderid" value = "<?php echo $get_deposit['ordernumber']; ?>">
					<input type = "hidden" name = "drid" value = "<?php echo $get_deposit['drid']; ?>">
			<?php } ?>
	</form>	
</div>
<script language="javascript">
		    <?php if($get_deposit['drid'] != 7) { ?>
				   function chkBankForDeposit(frm) {
						if(typeof frm.drid !== 'undefined') {
							if(frm.drid.value=='4') {
								idx=frm.elements["issBankNo"].selectedIndex;
								if(frm.elements["issBankNo"].options[idx].value=='') {
									alert('請選擇发卡行 !!');
									return false;
								}
							}
						}
						document.getElementById("Submit").innerText = "充值中";
						document.getElementById("Submit").setAttribute("disabled","disabled");
						document.getElementById("deposit-<?php echo $cdnTime; ?>").submit();
					}
            <?php } else if($get_deposit['drid'] == 7) { ?>
							function send_data(theForm)
							{
								var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
								var str2 = removeSpaces(document.getElementById('txtInput').value);
								if(theForm.serial_no.value=="" )
								{
									alert("請填写欢乐點序號!");
									theForm.serial_no.focus();
									return ;
								}
								if(theForm.passw.value=="" )
								{
									alert("請填写欢乐點密碼!");
									theForm.passw.focus();
									return ;
								}
								if(theForm.txtInput.value=="" )
								{
									alert("請輸入驗證碼!");
									theForm.txtInput.focus();
									return ;
								}
								if (str1 != str2)
								{
									alert('您输入的驗證碼不正確!');
									return ;        
								} else
								{
									theForm.submit();
								}
							}

							//Created / Generates the captcha function    
							function DrawCaptcha()
							{
								var a = Math.ceil(Math.random() * 9)+ '';
								var b = Math.ceil(Math.random() * 9)+ '';       
								var c = Math.ceil(Math.random() * 9)+ '';  
								var d = Math.ceil(Math.random() * 9)+ '';  
								var e = Math.ceil(Math.random() * 9)+ '';  
								var code = a + '' + b + '' + '' + c + '' + d + '' + e;
								
								//把code更換圖檔
								
								var url = "<?php echo BASE_URL.$config["path_image"]."/captcha/";?>";
								var r = "<img src='"+url+a+".gif'>";
								r += "<img src='"+url+b+".gif'>";
								r += "<img src='"+url+c+".gif'>";
								r += "<img src='"+url+d+".gif'>";
								r += "<img src='"+url+e+".gif'>";
								
								document.getElementById("show_img").innerHTML = r;
								document.getElementById("txtCaptcha").value = code;
							}

							// Validate the Entered input aganist the generated security code function   
							function ValidCaptcha(){
								var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
								var str2 = removeSpaces(document.getElementById('txtInput').value);
								if (str1 == str2) return true;        
								return false;
								
							}

							// Remove the spaces from the entered and generated code
							function removeSpaces(string)
							{
								return string.split(' ').join('');
							}
							
							$(function() { DrawCaptcha();});
		    <?php } ?>
</script>
		<!-- button id="Submit" type="button" class="ui-btn ui-corner-all ui-btn-a" 
				            onClick='document.getElementById("Submit").innerText = "充值中";
							document.getElementById("Submit").setAttribute("disabled","disabled");
							document.getElementById("deposit-<?php echo $cdnTime; ?>").submit();'>確定充值</button -->
		<!-- /article -->
		
