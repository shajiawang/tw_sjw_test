<?php
   date_default_timezone_set('Asia/Taipei');
   $cdnTime = date("YmdHis");
   $pay_result=_v('pay_result');
   $pay_info=_v('pay_info');
?>	
		<div class="article" > 
			<ul data-role="listview" data-inset="true" data-icon="false">
                <li>充值方式：超商缴費</li>
				<li>充值金額： <b><font color="red">NT$ <?php echo $pay_result['amount']; ?> </b>元</font></li>
                <li>缴費代碼 : <b><font color="red"><?php echo $pay_result['paycode']; ?></font></b></li>
				<li><font class="autowrap">
				    1.請至统一(7-ELEVEN),全家(Family Mart)或莱尔富(Hi-Life)超商機台输入缴費代碼打印缴費单。<br/>
				    2.打印成功后，即可持缴費单至柜台缴費。<br/>
					3.缴費后3-30分钟，殺價幣就会汇入您的帳户中。<br/>
					4.缴費成功后，收据請妥善保存备查，以保障您的权益。<br/>
					</font>
				</li>
				<li>
				   <input type="button" id="btn_ShowOper" name="btn_ShowOper" data-icon="info" data-mini="true" value="各機台操作说明"/>
				</li>
				<li>
				  <div id="oper_example" style="display:none">
					  <image width="95%" src="<?php echo $config["path_image"]; ?>/deposit/7_11_0424_02.jpg"/>
					  <br>
					  <image width="95%" src="<?php echo $config["path_image"]; ?>/deposit/famiport_pay_L.jpg"/>
					  <br>
					  <image width="95%" src="<?php echo $config["path_image"]; ?>/deposit/life_et_pay_L.jpg"/>
				  </div>
			    </li>
            </ul>
			<script>
			$("#btn_ShowOper").on("click",function(){$("#oper_example").toggle();});
		</script>
        </div><!-- /article -->
		