<?php 
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$deposit_rule = _v('deposit_rule'); 
$deposit_rule_item = _v('row_list');
$get_deposit = _v('get_deposit');
$get_scode_promote = _v('get_scode_promote');
$spmemo = _v('spmemo');
$spmemototal = _v('spmemototal');
$user_extrainfo = _v('user_extrainfo');

//一銀銷帳編號
//echo $user_extrainfo[8]['field1name'];

if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
	$browser = 1;
}else{
	$browser = 2;
} 
  
$currency=$config['currency'];
// $curr_format="%01.2f";
if($get_deposit['drid']=='6' || $get_deposit['drid']=='8' || $get_deposit['drid']=='10') {
    $currency="NTD";
    // $curr_format="%u";
}
           
?>
<div class="article deposit-payfaq">
    <form id="deposit-<?php echo $cdnTime; ?>" name="deposit-rule-item" method="post" action="<?php echo $config[$deposit_rule[0]['act']]['htmlurl']; ?>">
        <input type="hidden" name="runAt" id="runAt" value="WEB">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
        <input type="hidden" name="chkStr" id="chkStr" value="<?php echo $chkStr; ?>">
        <input type="hidden" name="drid" id="drid" value="<?php echo $get_deposit['drid']; ?>">
        <input type="hidden" name="driid" id="driid" value="<?php echo $get_deposit['driid']; ?>">
        
        <ul class="deposit_confirm_ul d-flex">
            <li class="d-inlineflex flex-column mx-auto">
                <div class="d-inlineflex align-items-center">
                    <div class="bank-logo">
                        <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/<?php echo $user_extrainfo['bankpic']?>" alt="">
                    </div>
                    <div class="bank-name"><?php echo $user_extrainfo['bankname']?></div>
                </div>
                <div class="bank_title d-inlineflex flex-column">
                    <p><?php echo $user_extrainfo['text'][0]?></p>
                    <p><span class="d-inlineflex"></span><span class="d-inlineflex"><?php echo $user_extrainfo['text'][1]?></span></p>
                </div>
            </li>
        </ul>
        <ul class="deposit_confirm_ul">
            <li>
                <div class="deposit-box d-flex">
                    <div class="de_title mr-auto">選擇項目</div>
                    <div class="de_point"><span class="selector"><?php echo $get_deposit['name']; ?></span></div>
                </div>
            </li>
	<?php if($get_deposit['drid']!= '7' && $get_deposit['drid']!= '9' ) {    ?>
            <li class="d-flex">
                <div class="de_title mr-auto">商品總金額</div>
                <div class="re_de_dollar">
                    <div class="red_font">			
                        <?php 
                            // echo $currency." ".sprintf ($curr_format, $get_deposit['amount']); 
                            echo "<span class=\"symbol\">".$get_deposit['mark']."</span><span>".$get_deposit['amount']."</span>";
                        ?>
                    </div>
                </div>
            </li>
        </ul>
	
	
        <!--- 有無贈品 --->
        <ul class="deposit_confirm_ul">
            <?php if(!empty($spmemo) && $spmemo != '<li>無贈送任何東西</li>') {	?>
            
                <?php echo nl2br($spmemo); ?>
            
            <?php }else{ ?>
                <li>無贈送任何東西</li>
            <?php } ?>
        </ul>
        <?php echo $user_extrainfo['remark']?>
	<?php } ?>

    </form>	
</div>

<style>
div p{
    margin: 10px;
}
</style>