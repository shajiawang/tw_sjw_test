<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$get_product_info = _v('get_product_info'); 
$paydata = _v('paydata');
?>
	<form id="contactform" name="contactform" method="post" action="">
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
                    <li>
                        <div id="pay" class="ui-grid-b">
                                <input type="radio" name="pay" id="radio-choicepay-1" value="home/" onClick="javascript:changAction('home');"> 
                                <label for="radio-choicepay-1" style="font-size:0.9em; width:220px; background-color:#ffffff; border-color:#fff;">
								我要充值，給我殺價礼包                                </label>
                        </div><!-- /grid-b -->
                    </li>
					<li>
                        <div id="pay" class="ui-grid-b">
                                <input type="radio" name="pay" id="radio-choicepay-2" value="onlinpay/" onClick="javascript:changAction('onlinpay');"> 
                                <label for="radio-choicepay-2" style="font-size:0.9em; width:220px; background-color:#ffffff; border-color:#fff;">
								我只要支付本次下標：RMB ¥<?php echo round($paydata['total'], 2); ?> 元
                                </label>
                        </div><!-- /grid-b -->
                    </li>					
					<input type="hidden" name="bida" id="bida" value="<?php echo $paydata['bida']; ?>">
					<input type="hidden" name="bidb" id="bidb" value="<?php echo $paydata['bidb']; ?>">
					<input type="hidden" name="type" id="type" value="<?php echo $paydata['type']; ?>">
					<input type="hidden" name="productid" id="productid" value="<?php echo $paydata['productid']; ?>">
					<input type="hidden" name="total" id="total" value="<?php echo $paydata['total']; ?>">

 					<button type="button" class="ui-btn ui-corner-all ui-btn-a" onclick="chkonlinepay(this.form);" >確定</button>
            </ul>
        </div>
	</form>		
		<!-- /article -->
<script type="text/javascript" language="javascript">

		function changAction(n){
			if( n=="home" ) {
				$("#contactform").attr("action", "<?php echo BASE_URL.APP_DIR; ?>/deposit/home/");
			} else {
				$("#contactform").attr("action", "<?php echo BASE_URL.APP_DIR; ?>/deposit/onlinepay/");
			}
		}

		function chkonlinepay(frm) {
			
			var kind = $('input:radio[name="pay"]:checked').val();		
			if (kind == undefined){
				alert('請指定支付方式 !!');
				return;
			} else {
				document.getElementById("contactform").submit();
			}
		}
		
</script>