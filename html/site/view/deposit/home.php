<?php 
$deposit_rule = _v('deposit_rule'); 
$spoint = _v('spoint'); 
$row_list = _v('row_list');
$get_drid_list = _v('get_drid_list');
$scode_promote_memo = _v('scode_promote_memo');
$get_deposit = _v('get_deposit');
$pay_account_list=_v("pay_account_list");
$user_extrainfo=_v("user_extrainfo");
$firstdeposit=_v("firstdeposit");
$user_carrier=_v("user_carrier");
$app=_v("app");
$times=_v("times");
$donate=_v("donate");
//一銀銷帳編號
//echo $user_extrainfo[8]['field1name'];
$writeoffid="";
if(!empty($user_extrainfo[8]['field1name'])) {
   $tmp=$user_extrainfo[8]['field1name'];
   $writeoffid= substr($tmp,0,4)."-".substr($tmp,4,4)."-".substr($tmp,8,4)."-".substr($tmp,12,4);  
}

//統計支付方式數量
$dridlistcount = count($get_drid_list);

?>
<script>
    $(function(){
        var $dridList = $.parseJSON('<?php echo json_encode($get_drid_list); ?>');
        //console.log($dridList.length);
//        console.log('<?php echo json_encode($dridlistcount); ?>');
        //console.log('<?php echo json_encode($get_drid_list); ?>');
    })
</script>

<?php
//首儲帳號顯示banner圖
if($firstdeposit == 'Y'){
	?>
	<?php if (empty($times['thumbnail'])){?>
	<div>
		<img style="width:100%" src="/site/static/img/firstdeposit.jpg">
	</div>
	<?php }else{ ?>
	<div>
		<img style="width:100%" src="<?php echo IMG_URL.'/site/images/site/deposit/'. $times['thumbnail']; ?>">
	</div>	
	<?php } ?>
	<?php
}
?>
<!-- 選擇儲值金額 -->
<div class="deposit-select-box">
    <div class="title">請選擇欲購買的額度</div>
    <div id="driid" class="d-flex flex-wrap">
        <!--  生成充值選項  -->
    </div>
    <input type="hidden" name = "user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
</div>
<ul class="deposit-lists-box">
    <li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">付款方式</div>
            </div>
            <div class="ui-field-contain deposit-list-rtxtbox deposit-select-box">
                <!-- 支付方式2種以上 使用 select, 只有1種時，使用div -->
                <?php if($dridlistcount > 1) {?>
                    
                    <select name="deposit-list-select" class="deposit-list-select" id="deposit-list-select" onchange="row_list(this.value);">
                        <?php 
                        if (!empty($get_drid_list)) {
                            foreach ($get_drid_list as $key => $value) {
                        ?>
                        <?php
                            //預設為銀行支付
                            if ($value['drid'] == '8') {
                                echo '<option value="'.$value['drid'].'" paytype="'.$value['type'].'" selected>'.$value['name'].'</option>';
                            } else {
                                if($pay_account_list[$value['act']])
                                    echo '<option value="'.$value['drid'].'" paytype="'.$value['type'].'">'.$value['name'].'</option>';
                                else
                                    echo '<option value="'.$value['drid'].'" act="bind" paytype="'.$value['type'].'">'.$value['name'].'</option>';
                            }   
                        ?>
                        <?php } }//endforeach; ?>
                    </select>
                <?php }else{ ?>
                    <div class="text-right"><?php echo $get_drid_list[0]['name'] ?></div>
                    <input type="hidden" name="deposit-list-select" id="deposit-list-select" value="<?php echo $get_drid_list[0]['drid'] ?>">
                <?php } ?>
            </div>
        </div>
    </li>
</ul>
<ul class="deposit-lists-box">
    <li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">發票稅5%＋金流手續費</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon proccess-box"><span class="proccess">&nbsp;</span></div>
            </div>
        </div>		
    </li>
    
    <li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">商品總金額</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon price-box"><span class="symbol">$</span><span class="price">&nbsp;</span></div>
            </div>
        </div>
    </li>
</ul>

<ul class="deposit-lists-box memo_box">
    
        

        
    
</ul>

<?php if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='925' || $_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='597' || $_SESSION['auth_id']=='6' || $_SESSION['auth_id']=='1705' ) { ?>
<!--- 發票載具類型 --->
<ul class="deposit-lists-box">
    <li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title" style="font-weight: bolder;">發票選項</div>
            </div>
        </div>
	</li>
	<li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">索取紙本電子發票 
					<input type="radio" name="invoice_paper" id="invoice_paper" value="Y" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_carrier['invoice_paper']=='Y'){echo "checked";}?> onClick="invoice_paper_check('Y');"> 是 
					<input type="radio" name="invoice_paper" id="invoice_paper" value="N" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_carrier['invoice_paper']=='N'){echo "checked";}?> onClick="invoice_paper_check('N');"> 否 
				</div>
			 </div>
			 <div class="deposit-list-rtxtbox">
				<div class="deposit-list-ricon proccess-box"> </div>
			</div>
        </div> 
		<div name="address_item" id="address_item" style="display:none">
        <div class="deposit-box d-flex align-items-center" >
			 <div class="deposit-list-rtxtbox">
				<input type="text" name="addressee" id="addressee" value="" placeholder="請輸入收件人" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;width: 25rem;margin: 0.5rem 0.5rem 0rem 2.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;">
				<input type="text" name="address" id="address" value="" placeholder="請輸入發票寄送地址" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;width: 25rem;margin: 0rem 0.5rem 0rem 2.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;">
			</div>
        </div>	
		</div>	
	</li>
	<li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">電子發票類型
					<input type="radio" name="invoice_type" id="invoice_type" value="P" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_carrier['invoice_type']=='P'){echo "checked";}?> onClick="invoice_type_check('P');"> 個人 
					<input type="radio" name="invoice_type" id="invoice_type" value="C" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_carrier['invoice_type']=='C'){echo "checked";}?> onClick="invoice_type_check('C');"> 公司 
					<div id="typed" style="display: inline-block;" ><input type="radio" name="invoice_type" id="invoice_type" value="D" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($user_profile['invoice_type']=='D'){echo "checked";}?> onClick="invoice_type_check('D');"> 捐贈 </div>
				</div>
            </div>
        </div>
	</li> 
	<div name="company_item" id="company_item" style="display:none">
	<li class="deposit-list">
		<div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">買方統編 (不可捐贈)</div>
            </div>
			<div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon proccess-box" ><input type="number" name="buyer_id" id="buyer_id" value="" placeholder="如需統編請在此填寫" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 1.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" ></div>
            </div>
        </div>	
	</li>
	<li class="deposit-list">
		<div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">發票抬頭 (不可捐贈)</div>
            </div>
			<div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon proccess-box" ><input type="number" name="buyer_name" id="buyer_name" value="" placeholder="如需發票抬頭請在此填寫" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 1.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" ></div>
            </div>
        </div>	
	</li>
	</div>
	<div name="donate_item" id="donate_item" style="display:none">
    <li class="deposit-list" >
		 <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">捐贈 (不可填寫統編)</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon proccess-box">
				<select name="donatecode" id="donatecode" >
					<option value="">(請選擇)</option>
					<?php
						foreach ($donate as $key => $value) {
							echo '<option value="'.$value['code'].'">'.$value['name'].'</option>';
						}
					?>
				</select> 				
				</div>
            </div>
        </div>
	</li> 	
	</div>
	<div name="carrier_item" id="carrier_item" <?php if ($user_profile['invoice_paper']=='Y'){echo "style='display:none;'"; } ?>>
	<li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title" style="font-weight: bolder;">載具</div>
            </div>
        </div>
	</li>
	<div name="carrier_saja" id="carrier_saja" >
	<li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title"><input type="radio" name="carrier" id="carrier" value="saja" style="-webkit-appearance:radio;width:1.5rem;" checked>殺價王會員載具</div>
            </div>
        </div>
	</li>
	</div>
	<li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title"><input type="radio" name="carrier" id="carrier" value="phone" style="-webkit-appearance:radio;width:1.5rem;">手機條碼</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon proccess-box"><input type="text" name="phonecode" id="phonecode" value="" placeholder="'/'開頭+8碼編號" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 1.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" maxlength="8" onkeyup="check_phonecode(this.value);"></div>
            </div>
        </div>	
	</li>
	<div name="carrier_saja" id="carrier_npcode" >
	<li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title"><input type="radio" name="carrier" id="carrier" value="np" style="-webkit-appearance:radio;width:1.5rem;">自然人憑證條碼</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon proccess-box"><input type="text" name="npcode" id="npcode" value="" placeholder="2碼大寫英文字母+14碼數字" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 1.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" maxlength="16" onkeyup="this.value = this.value.toUpperCase();"></div>
            </div>
        </div>	
	</li>
	</div>
	</div>
</ul>
<?php } ?>
<div class="oscodePs" style="display:none;">*本活動截止時間 : <span class="offtime"></span></div>

<!--  彈窗-匯款訊息  -->
<div class="remindModal justify-content-center align-items-center">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-title d-flex align-items-center justify-content-center">
                <div class="slogn">
                    <p>使用轉帳匯款</p>
                </div>
            </div>
            <div class="remittance-box d-flex align-items-center justify-content-center">
                <div class="info mr-auto">
                    <p><span>第一銀行 興雅分行</span></p>
                    <p><span>銀行總代號：</span><span>007</span></p>
                    <p class="d-flex flex-wrap"><span class="d-inlineflex">個人專屬匯款帳號：</span><span class="d-inlineflex"><?php echo $writeoffid; ?></span></p>
                </div>
                <div class="img">
                    <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/deposit-atm.png" alt="">
                </div>
            </div>
            <div class="remittance-option-box">
                <div class="remittance-title d-flex align-items-center justify-content-center">
                    <i class="esfas ycolor exclamation" style="width: 2.48rem;height: 2.15rem;margin-right: .5rem;"></i>
                    <div>
                        <p>非以下限定金額，須人工處理</p>
                        <p>無法即時到帳、並無贈送殺價券</p>
                    </div>
                </div>
                <div class="option-title">付款金額 / 送券數量：</div>
                <ul class="remittance-options d-flex flex-wrap">
                    <li class="options d-inlineflex align-items-center flex-wrap mr-auto">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 105 </span>
                        <span class="d-inlineflex">(無送券)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 525 </span>
                        <span class="d-inlineflex">(無送券)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap mr-auto">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 2100 </span>
                        <span class="d-inlineflex">(送2張)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 5250 </span>
                        <span class="d-inlineflex">(送6張)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap mr-auto">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 10500 </span>
                        <span class="d-inlineflex">(送15張)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 31500 </span>
                        <span class="d-inlineflex">(送50張)</span>
                    </li>
                </ul>
            </div>
            <ul class="remarks-box d-flex flex-wrap">
                <li class="item d-flex">
                    <div class="mini d-flex">
                        <p>本活動截止時間 : 2019/01/13 (日) 23:00:00</p>
                    </div>
                </li>
                <li class="item red d-flex">
                    <div class="mini d-flex">
                        <p>匯款金額內含5%發票稅</p>
                    </div>
                </li>
                <li class="item d-flex">
                    <div class="mini d-flex flex-wrap">
                        <p class="d-inlineflex">匯款成功後約一個工作天內可收到殺價幣/殺價券</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="modal-proinfo-close"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/close-circle.png"></div>
    </div>
    <div class="remindModalBg"></div>
</div>
   				
    <div class="footernav">
        <div class="footernav-button">
            <button type="button" id="topay" onclick="deposit();" class="ui-btn ui-shadow ui-corner-all">確定</button>
        </div>
    </div>
<input type="hidden" id="app" name="app" value="<?php echo $app; ?>">		
<!-- /article -->
<script type="text/javascript">
    var $default = 8;       //預設支付方式
	$(document).ready(function() {
        
		// row_list(1);							//預設銀聯支付
		row_list($default);							//信用卡支付
        
        //將支付選項調整為預設
        $('#deposit-list-select').val($default);
        var $option_text = $('#deposit-list-select').find('option[value='+ $default +']').text();
        $('span.deposit-list-select').text($option_text);

	});
    $(function(){
        //預設開啟匯款彈窗
        // $openPopModal();
        $('.modal-link-bar .link-item.remittance').on('click', function(){
            $openPopModal();
        })
        $('.remindModalBg , .modal-proinfo-close').on('click', function(){
            $closePopModal();
        })
        
    })
	// Add By Thomas 20141111
	// var row_list_Arr2 = new Array();

//	if ($("input[name='radio-drid']").is(":checked")) {
//	    // $("input[name='radio-drid'][value='1']").prop('checked',true).checkboxradio("refresh");
//		$("select option[value='8']").prop('selected',true).checkboxradio("refresh");     // 預設改為微信支付
//	} else {
//	    // $("input[name='radio-drid'][value='1']").attr('checked',true).addClass("refresh");
//		$("select option[value='8']").attr('selected',true).addClass("refresh");          // 預設改為微信支付
//	}
	function row_list(drid) {
		if(drid=='7' || drid=='9' ) {
		   $("#note").hide();
		} else {
		  $("#driid").show();
		  $("#memo").show();
		  $("#note").show();
		}
		var html = '';
		var i = 0;
		//var pageid = $.mobile.activePage.attr('id');
        var row_list_Arr = <?php echo json_encode($row_list); ?>;
        //購買額度
        for (var i = 0; i < row_list_Arr[drid].length; i++) {
            var k = row_list_Arr[drid][i]['driid'];
            html += '<div class="payment_option moneymenu-radio">';
            html += '    <input type="radio" name="radio-driid" id="radio-choice-' + k + '" value="' + k + '" onclick="deposit_memo(' + k + ',\'' + row_list_Arr[drid][i]['name'] + '\',' + parseFloat(row_list_Arr[drid][i]['amount']) +');">';
            html += '    <label for="radio-choice-' + k + '" class="payment_option_title radio-label d-flex align-items-center justify-content-center">'+row_list_Arr[drid][i]['name'] + '</label>';
            html += '</div>';
            html += '<input type="hidden" name="radio-driid-count" id="radio-choice-count-' + k + '" value="' + parseFloat(row_list_Arr[drid][i]['amount']) + '">';          
            html += '<input type="hidden" name="radio-driid-name" id="radio-choice-name-' + k + '" value="' + row_list_Arr[drid][i]['name'] + '">';         
        }
        var val = $("input[name='radio-driid']:checked").index("input[name='radio-driid']");

		//刪除id = driid內容
		//$("#driid .weui-grid3").remove();
		document.getElementById('driid').innerHTML="";
		
		//$("#driid").append(html).trigger('create');
		document.getElementById('driid').innerHTML = html;

		//$("#" + pageid + " #driid");
        
        //預設選取第一個金額
		//$("input[name='radio-driid']:eq(0)").prop('checked',true);
        
		if (val > -1) {
			$("input[name='radio-driid']:eq(" + val + ")").prop('checked',true);
			var v = $("input[name='radio-driid']:checked").val();
			deposit_memo($("input[name='radio-driid'][value=" + v + "]").val(),$("input[id='radio-choice-name-"+v+"']").val(),$("input[id='radio-choice-count-"+v+"']").val());
			$("input[name='radio-driid'][value=" + v + "]").prop('checked',true);
            
		}else {
			if(drid=='1' || drid=='5' ) {
				$("input[name='radio-driid']:eq(3)").prop('checked',true);
				deposit_memo($("input[name='radio-driid']:eq(3)").val(),$("input[name='radio-driid-name']:eq(3)").val(),$("input[name='radio-driid-count']:eq(3)").val());	//預設第4筆資料
			} else {
			   $("input[name='radio-driid']:eq(1)").prop('checked',true);
			   deposit_memo($("input[name='radio-driid']:eq(1)").val(),$("input[name='radio-driid-name']:eq(1)").val(),$("input[name='radio-driid-count']:eq(1)").val());	//預設第2筆資料
			}
		}
    }
    
    function deposit_memo(driid,dname,price){

    	var scode_promote_memo_Arr = new Array();
        scode_promote_memo_Arr =JSON.parse('<?php echo json_encode($scode_promote_memo); ?>');

    	$("#memo h4").html(scode_promote_memo_Arr[driid]);
        
        // console.log(scode_promote_memo_Arr);
        //贈送殺價券 樣式
        var $freeOscode;
        if(scode_promote_memo_Arr[driid][0]['num'].length > 0){
            if(scode_promote_memo_Arr[driid][0]['num'].indexOf('無') < 0){
                var memo_Arr = scode_promote_memo_Arr[driid]
                $(".memo_box").empty();
                for (var i = 0; i < memo_Arr.length; i++) {
                    $freeOscode = memo_Arr[i]['num'];
                    $freeOscodeName = memo_Arr[i]['name'];
                    $firstdeposit_text = memo_Arr[i]['firstdeposit_text'];

                    $(".memo_box").append('<li class="deposit-list">\
                        <div class="deposit-box d-flex align-items-center ">\
                            <div class="deposit-list-titlebox">\
                                <div class="deposit-list-title"><span class="memo_title">送 殺價券'+$freeOscodeName+'</span></div>\
                            </div>\
                        <div class="deposit-list-rtxtbox">\
                            <div class="deposit-list-ricon oscode-box"><span class="memo">'+ $freeOscode +'張'+$firstdeposit_text+'</span></div>\
                            </div>\
                        </div>\
                    </li>')
                }

            }else{
                $freeOscode = 0;

                $(".memo_box").empty();
                $(".memo_box").append('<li class="deposit-list">\
                    <div class="deposit-box d-flex align-items-center ">\
                        <div class="deposit-list-titlebox">\
                            <div class="deposit-list-title"><span class="memo_title">無贈送任何東西</span></div>\
                        </div>\
                        <div class="deposit-list-rtxtbox">\
                            <div class="deposit-list-ricon oscode-box"><span class="memo gray">無贈送殺價券</span></div>\
                            </div>\
                        </div>\
                    </li>')
            }
        }
        var offtime = scode_promote_memo_Arr[driid][0]['offtime'] || '';
        $changeShow(dname,price,offtime,$freeOscode);
    }	
    
    //更改切換文字
    function $changeShow(dname,price,offtime,$freeOscode) {
        var $selector = $(".deposit-list-rtxtbox .selector-box .selector");
        var $price = $(".deposit-list-rtxtbox .price-box .price");                  //商品總金額
		var $proccess = $(".deposit-list-rtxtbox .proccess-box .proccess");         //處理費用
        var $offtime_parent = $('.oscodePs');                                       //活動截止時間父層
        var $offtime = $offtime_parent.find('.offtime');                            //活動截止時間
		//console.log(offtime);
		var $ps = price - dname.replace(/枚|點|点|元|匯款/, "");                          //替換成 元
        
        if ($ps <=0) {
            $proccess.parents("li").hide();
        }else{
            $proccess.parents("li").show();
        }

        $selector.html(dname);
        $price.html(price);
		$proccess.html($ps+'元');
        
        if((offtime !== '') && ($freeOscode !== 0)){ 
            $offtime_parent.show();
            $offtime_parent.find('.offtime').html(offtime);
        }else{
            $offtime_parent.hide();
            $offtime_parent.find('.offtime').html('');
        }
    }
    
    //開啟匯款彈框
    function $openPopModal() {
        $('body').addClass('modal-open');
        $('.remindModal').addClass('d-flex').show();
        $('.remindModalBg').show();
    }
    //關閉匯款彈框
    function $closePopModal() {
        $('body').removeClass('modal-open');
        $('.remindModal').removeClass('d-flex').hide();
        $('.remindModalBg').hide();
    }
	
	
	//檢查手機條碼	
	function check_phonecode(value){
		
		var appid = '<?php echo $config['invoice']['appId']; ?>';
		var txid = new Date().getTime();
		var code = value;
		
		if (value.length == 8){
			
			if (value.substr(0,1) !='/') {
				alert('手機條碼格式錯誤');
				return false;
			}
			
			$.ajax({
				url: '<?php echo BASE_URL.APP_DIR;?>/invoice/check_phonecode/',
				data: {version:"1.0",action:"bcv",barCode:code,TxID:txid,appId:appid},
				type: 'POST',
				dataType:'json',
				success: function(result) {
					console.log(result); 
					// var ret = jQuery.parseJSON(result); 
					if(result.code == 200) { 
						if (result.isExist == 'Y'){
							alert("手機條碼存在 ");
						}else{
							alert("手機條碼不存在 ");
						}
					} else { 
						alert(result.msg);
					}
				}
			});
		}else{
			if(value.length > 8){
				alert('手機條碼長度為8碼');
				return false;
			}
		}
	}	
	
	function invoice_paper_check(value) {
		$("#donate_item").hide();
		$("#typed").hide();
		$("#carrier_item").hide();
		$("#address_item").hide();
		
		if(value=='N') {
			// $("#donate_item").show();
			$("#typed").show();
			$("#carrier_item").show();
			$("#address_item").hide();
		} else {
			$("#donate_item").hide();
			$("#typed").hide();
			$("#carrier_item").hide();
			$("#donate_mark").val('N');
			$("#address_item").show();
		}
	}
	
	function invoice_type_check(value) {
		if(value=='C') {
			$("#company_item").show();
			$("#donate_item").hide();
			$("#donate_mark").val('N');
			$("#carrier_item").show();
			$("#carrier_saja").hide();
			$("#carrier_npcode").hide();
		} else if(value=='P') {
			$("#company_item").hide();
			$("#donate_item").hide();
			$("#donate_mark").val('N');
		} else {
			$("#company_item").hide();
			$("#donate_item").show();
			$("#donate_mark").val('Y');
		} 
	}
</script>
