<?php 
$deposit_rule = _v('deposit_rule'); 
$row_list = _v('row_list');
$get_drid_list = _v('get_drid_list');
$scode_promote_memo = _v('scode_promote_memo');
?>
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                	<div id="tagBox">
                        <img src="<?php echo $config['path_image'];?>/deposit/deposit_title.png">
                    </div>
                    
                    <li>
                        <div id="driid" class="ui-grid-b">
                        </div><!-- /grid-b -->
                    </li>
                    <li>
                        <div id="drid" class="ui-grid-b">
                        <?php 
                        	if (!empty($get_drid_list)) {
                        		foreach ($get_drid_list as $key => $value) {
                        			if ($key % 3 == 0) {
                        				echo '
                        	<div class="ui-block-a" style="width: 33% !important;">';
                        			}
                        			else if ($key % 3 == 1) {
                        				echo '
                        	<div class="ui-block-b" style="width: 33% !important;">';
                        			}
                        			else if ($key % 3 == 2) {
                        				echo '
                        	<div class="ui-block-b" style="width: 33% !important;">';
                        			}
                        ?>
                                <input type="radio" name="radio-drid" id="radio-choicepay-<?php echo $value['drid']; ?>" value="<?php echo $value['drid']; ?>" onclick="row_list(<?php echo $value['drid']; ?>);">
                                <label for="radio-choicepay-<?php echo $value['drid']; ?>" style="width:130px;background-color:#ffffff;border-color:#fff;">
                                	<img src="<?php echo $config['path_image']."/deposit/".$value['logo'];?>" />
                                </label>

         					<!-- for ibon end -->
                            </div>
                        <?php } 
						
						     }//endforeach; 
						?>
                            <!-- div class="ui-block-a" style="width: 33% !important;">
						     <input type="radio" name="radio-drid" id="radio-choicepay-6" value="6" onclick="row_list(6);">
                                <label for="radio-choicepay-6" style="width:130px;background-color:#ffffff;border-color:#fff;">
                                	<img src="/site/images/site/deposit/ibon.gif" />
                                </label>
                            </div>	
                             <div class="ui-block-b" style="width: 33% !important;"> 
						     <input type="radio" name="radio-drid" id="radio-choicepay-7" value="7" onclick="row_list(7);">
                                <label for="radio-choicepay-7" style="width:130px;background-color:#ffffff;border-color:#fff;">
                                	<img src="/site/images/site/deposit/hinet.png" />
                                </label>
                            </div -->							
                        </div><!-- /grid-b -->
                    </li>
                    <li>
                        <div class="ui-grid-a">
                            <div class="ui-block-a" style="width: 30% !important;">
                                <img src="<?php echo $config['path_image'];?>/deposit/deposit_free.png">
                            </div>
                            <div id="memo" class="ui-block-b" style="width: 70% !important;">
                            </div>
                        </div><!-- /grid-a -->
                    </li>
					
					  <button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="deposit2()">充值</button>

                </li>
            </ul>
        </div><!-- /article -->
<script>
	$(document).ready(function() {
		row_list(4);												//預設銀聯支付
	});
	
	if ($("input[name='radio-drid']").is(":checked")) {
		$("input[name='radio-drid'][value='4']").prop('checked',true).checkboxradio("refresh");
	} else {
		$("input[name='radio-drid'][value='4']").attr('checked',true).addClass("refresh");
	}
	
	function row_list(drid) {
	    if(drid=='7') {
	        $("#driid").hide();
			$("#memo").html('');
     		return;
		} else {
		    $("#driid").show();
		}
		var row_list_Arr = new Array();
		var html = '';
		var i = 0;
		//var pageid = $.mobile.activePage.attr('id');
		
		<?php 
		foreach($row_list as $rk => $rvArr) {
			echo "row_list_Arr[$rk] = [];\n";
			if(!$rvArr)
			   continue;
			foreach ($rvArr as $key =>  $value) {
        		echo "row_list_Arr[$rk][$value[driid]] = $value[spoint];\n";
        	}
    	}
    	?>
    	
		// Test End
		for (var k in row_list_Arr[drid]) {
			if (i % 3 == 0) {
				html += '<div class="ui-block-a" style="width: 33% !important;">';
			}
			if (i % 3 == 1) {
				html += '<div class="ui-block-b" style="width: 33% !important;">';
			}
			if (i % 3 == 2) {
				html += '<div class="ui-block-c" style="width: 33% !important;">';
			}
			html += '<input type="radio" name="radio-driid" id="radio-choice-' + k + '" value="' + k + '" onclick="deposit_memo(' + k + ');">';
            html += '<label for="radio-choice-' + k + '" style="width:130px;background-color:#ffffff;border-color:#fff;">' + row_list_Arr[drid][k] + '點</label>';
            html += '</div>';
            
			i++;
		}
		
		//刪除id = driid內容
		$("#driid .ui-block-a").remove();
		$("#driid .ui-block-b").remove();
		$("#driid .ui-block-c").remove();
		
		$("#driid").append(html).trigger('create');
		//$("#" + pageid + " #driid");
		$("input[name='radio-driid']:eq(0)").prop('checked',true).checkboxradio();
		$("input[name='radio-driid']:eq(0)").prop('checked',true).checkboxradio("refresh");
		deposit_memo($("input[name='radio-driid']:eq(0)").val());	//預設第1筆資料
    }
    
    function deposit_memo(driid){
    	var scode_promote_memo_Arr = new Array();
    	
    	<?php 
    	foreach ($scode_promote_memo as $sk => $sv) {
    		echo "scode_promote_memo_Arr[$sk] = '".$sv."';\n";
    	}
    	?>
    	$("#memo").html(scode_promote_memo_Arr[driid]);
    }
</script>