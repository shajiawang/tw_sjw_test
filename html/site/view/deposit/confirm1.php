<?php 
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$deposit_rule = _v('deposit_rule'); 
$get_deposit = _v('get_deposit');
$get_scode_promote = _v('get_scode_promote');
$bank_list=_v('bank_list');
$spmemo = _v('spmemo');
?>
		
		<div class="article">
            <form id="deposit-<?php echo $cdnTime; ?>" name="deposit-rule-item" method="post" action="<?php echo $config[$deposit_rule[0]['act']]['htmlurl']; ?>">
			<ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                	<div id="tagBox">
                        <img src="<?php echo $config['path_image'];?>/<?php echo $deposit_rule[0]['logo']; ?>">
                    </div>
                </li>
		<?php if ($get_deposit['drid'] == 4) { //銀聯 ?>
			
                        <li>发卡行 : <select name="issBankNo" >
                            <option value="">(請選擇)</option>
                <?php
                        foreach($bank_list as $key=>$value) {
                ?>
                          <option value="<?php echo $key; ?>" ><?php echo $value; ?></option> 
                <?php
                        };              
                    } 
				?>
                </select></li>
                <li>充值點數：<?php echo $get_deposit['name']; ?></li>
                <li>手續費  ：2％ </li>
                <li>交易金額：RMB <?php echo sprintf ("%01.2f", $get_deposit['amount']); ?> 元</li>
                <li>备    注：<?php echo nl2br($spmemo); ?></li>
                <li>
				<!-- button id="Submit" type="button" class="ui-btn ui-corner-all ui-btn-a" 
				            onClick='document.getElementById("Submit").innerText = "充值中";
							document.getElementById("Submit").setAttribute("disabled","disabled");
							document.getElementById("deposit-<?php echo $cdnTime; ?>").submit();'>確定充值</button -->
				<button id="Submit" type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="chkBankForDeposit(this.form);">確定充值</button>
				</li>
            </ul>
            
            <?php if ($get_deposit['drid'] == 1) { //支付寶 ?>
            <!--卖家支付宝帳户-->
            <input type="hidden" name="WIDseller_email" value="<?php echo $config['alipay']['sellemail']; ?>" />
            <!--商户订单號-->
            <input type="hidden" name="WIDout_trade_no" value="<?php echo $get_deposit['ordernumber']; ?>" />
            <!--订单名称-->
            <input type="hidden" name="WIDsubject" value="<?php echo $get_deposit['name']; ?>" />
            <!--付款金額-->
            <?php if ($_SESSION['auth_id'] == 28 || $_SESSION['auth_id'] == 1  || $_SESSION['auth_id'] == 116 ) { ?>
            <input type="hidden" name="WIDtotal_fee" value="0.01" />
        	<?php } else { ?>
            <input type="hidden" name="WIDtotal_fee" value="<?php echo $get_deposit['amount']; ?>" />
            <?php } ?>
            <?php } else if ($get_deposit['drid'] == 4) { //銀聯 ?>
			<input type="hidden" name="drid" value="<?php echo $get_deposit['drid']; ?>">
            <input type="hidden" name="merCert" value="">
            <!--消息版本號-->
            <input type = "hidden" name = "interfaceVersion" value = "<?php echo($config['bankcomm']['interfaceVersion']); ?>">
            <!--商户號-->
            <input type = "hidden" name = "merID" value = "<?php echo($config['bankcomm']['merchID']); ?>">
            <!--订单號-->
            <input type = "hidden" name = "orderid" value = "<?php echo $get_deposit['ordernumber']; ?>">
            <!--交易時間-->
			<?php
			    $Ymd=date("Ymd");
				$His=date("His");
			?>
			<input type = "hidden" name = "orderDate" value = "<?php echo($Ymd); ?>">
            <input type = "hidden" name = "orderTime" value = "<?php echo($His); ?>">
            <!--交易类别-->
            <input type = "hidden" name = "tranType" value = "<?php echo($config['bankcomm']['tranType']); ?>">
            <!--交易金額-->
            <?php if ($_SESSION['auth_id'] == 28 || $_SESSION['auth_id'] == 1 || $_SESSION['auth_id'] == 116) { ?>
            <input type = "hidden" name = "amount" value = "0.01">
            <?php } else { ?>
            <input type = "hidden" name = "amount" value = "<?php echo sprintf ("%01.2f", $get_deposit['amount']); ?>">
            <?php } ?>
            <!--交易幣种-->
            <input type = "hidden" name = "curType" value = "<?php echo($config['bankcomm']['curType']); ?>">
            <!--订单内容-->
            <input type = "hidden" name = "orderContent" value = "<?php echo "Spts:".sprintf ("%01.2f",$get_deposit['spoint']); ?>" >
            <!--商家备注-->
            <input type = "hidden" name = "orderMono" value = "<?php echo "OdrID:".$get_deposit['ordernumber']
			                                                             ."/OdrTime:".$Ymd.$His
																		 ."/Pay:".sprintf("%01.2f", $get_deposit['amount'])
																		 ."/Spts:".sprintf ("%01.2f",$get_deposit['spoint']);
																?>">          
			<!--通知方式 0 不通知 1 通知 2 抓取页面 -->
            <input type="hidden" name="notifyType" value="2">
            <input type="hidden" name="goodsURL" value="<?php echo ($config['bankcomm']['goodsURL']); ?>" >
            <input type="hidden" name="merURL" value="<?php echo ($config['bankcomm']['merURL']); ?>" >
			<input type="hidden" name = "jumpSeconds" value = "0">
            <!--渠道编號 0:html渠道 -->
            <input type="hidden" name="netType" value="0">
            <?php } else if ($get_deposit['drid'] == 5) { //微信支付 ?>
            <!--订单號-->
            <input type="hidden" name="orderid" value="<?php echo $get_deposit['ordernumber']; ?>">
            <!--交易金額-->
            <?php if ($_SESSION['auth_id'] == 28 || $_SESSION['auth_id'] == 1  || $_SESSION['auth_id'] == 116 ) { ?>
            <input type="hidden" name="amount" value="0.01">
            <?php } else { ?>
            <input type="hidden" name="amount" value="<?php echo $get_deposit['amount']; ?>">
            <?php } ?>
            <?php } else if($get_deposit['drid'] == 6) { // 超商支付 ?>
			    <input type="hidden" name="pay_type" value="1">
                <input type="hidden" name="deposit_value"  value="">
				<input type="hidden" name="invoices" value="">
			
			<?php } ?>
	</form>	
        </div>
		<script language="javascript">
		   function chkBankForDeposit(frm) {
				if(typeof frm.drid !== 'undefined') {
					if(frm.drid.value=='4') {
						idx=frm.elements["issBankNo"].selectedIndex;
						if(frm.elements["issBankNo"].options[idx].value=='') {
							alert('請選擇发卡行 !!');
							return false;
						}
					}
				}
				document.getElementById("Submit").innerText = "充值中";
				document.getElementById("Submit").setAttribute("disabled","disabled");
				document.getElementById("deposit-<?php echo $cdnTime; ?>").submit();
			}
		</script>
		<!-- /article -->
		
