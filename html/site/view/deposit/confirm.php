<?php 
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$deposit_rule = _v('deposit_rule'); 
$deposit_rule_item = _v('row_list'); 
$vendor_pay=_v('vendor_pay');
$get_deposit = _v('get_deposit');
$get_scode_promote = _v('get_scode_promote');
$bank_list=_v('bank_list');
$spmemo = _v('spmemo');
$chkStr=_v('chkStr');
$spmemototal = _v('spmemototal');
$times = _v('times');
$carrierdata=_v('carrierdata');
$donate=_v("donate");

if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
	$browser = 1;
}else{
	$browser = 2;
} 
  
$currency=$config['currency'];
// $curr_format="%01.2f";
if($get_deposit['drid']=='6' || $get_deposit['drid']=='8' || $get_deposit['drid']=='10') {
    $currency="NTD";
    // $curr_format="%u";
}
           
?>

<div class="article">
    <form id="deposit-<?php echo $cdnTime; ?>" name="deposit-rule-item" method="post" action="<?php echo $config[$deposit_rule[0]['act']]['htmlurl']; ?>">
        <input type="hidden" name="runAt" id="runAt" value="WEB">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
        <input type="hidden" name="chkStr" id="chkStr" value="<?php echo $chkStr; ?>">
        <input type="hidden" name="drid" id="drid" value="<?php echo $get_deposit['drid']; ?>">
        <input type="hidden" name="driid" id="driid" value="<?php echo $get_deposit['driid']; ?>">
		<input type="hidden" name="amount" id="amount" value="<?php echo $get_deposit['amount']; ?>">
		<input type="hidden" name="ordernumber" id="ordernumber" value="<?php echo $get_deposit['ordernumber']; ?>" />
        
        <ul class="deposit_confirm_ul">
            <li class="d-flex">
                <div class="de_title mr-auto"><?php echo $deposit_rule[0]['name']; ?></div>
            </li>
        </ul>
        <ul class="deposit_confirm_ul">
            <li>
                <div class="deposit-box d-flex">
                    <div class="de_title mr-auto">選擇項目</div>
                    <div class="de_point"><span class="selector"><?php echo $get_deposit['name']; ?></span></div>
                </div>
            </li>
            
        <?php if ($get_deposit['drid'] == 4) {                                                      //銀聯 ?>
            <li>發卡行 :
                <select name="issBankNo">
                    <option value="">(請選擇)</option>
                        <?php foreach($bank_list as $key=>$value) {	?>
                            <option value="<?php echo $key; ?>" ><?php echo $value; ?></option> 
                        <?php } ?>
				</select>
            </li>
        <?php  }  ?>
        <?php if ($get_deposit['drid'] == 6){                                                       // ibonpay ?>
            <li>
                <h2>
                    <font class="autowrap">
                        儲值流程：
                        <br/>1.按下方 "確定充值"，系統將產生繳費代碼並顯示於頁面
                        <br/>2.至統一(7-ELEVEN),全家(Family Mart)或萊爾富(Hi-Life)超商機台輸入繳費代碼打印繳費單。
                        <br/>3.打印成功後，持繳費單至櫃檯繳費。
                        <br/>4.繳費完成後3-30分鐘，殺價幣就會匯入您的帳戶中。
                        <br/>5.繳費收據請妥善保存備查，以保障您的權益。
                        <br/>6.如需多組代碼繳費，請務必間隔三分鐘後，再取得新代碼。
                    </font>
                </h2>
            </li>
        <?php  }   ?>
        
	<?php if($get_deposit['drid']!= '7' && $get_deposit['drid']!= '9' ) {    ?>
            <li class="d-flex">
                <div class="de_title mr-auto">商品總金額</div>
                <div class="re_de_dollar">
                    <div class="red_font">
                        <?php //if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='925' || $_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='597' || $_SESSION['auth_id']=='6' || $_SESSION['auth_id']=='1476' ) { ?>
							<?php 
								// echo $currency." ".sprintf ($curr_format, $get_deposit['amount']); 
								//echo $currency." 0.01"; 
							?>							
				        <?php //}else{ ?>			
							<?php 
								// echo $currency." ".sprintf ($curr_format, $get_deposit['amount']); 
								echo "<span class=\"symbol\">$</span><span>".$get_deposit['amount']."</span>";
							?>
				        <?php //} ?>
                    </div>
                </div>
            </li>
        </ul>

        <!--- 有無贈品 --->
        <ul class="deposit_confirm_ul">
            <?php if(!empty($spmemo) && $spmemo != '<li>無贈送任何東西</li>') {	?>
                <?php echo nl2br($spmemo); ?>
            <?php }else{ ?>
                <li>無贈送任何東西</li>
            <?php } ?>
        </ul>
		<?php if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='925' || $_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='597' || $_SESSION['auth_id']=='6' || $_SESSION['auth_id']=='1705' ) { ?>
		<!--- 發票載具類型 --->
		<ul class="deposit_confirm_ul">
		
			<li class="deposit-list">
				<div class="deposit-box d-flex align-items-center">
					<div class="deposit-list-titlebox">
						<div class="deposit-list-title">發票統編</div>
					</div>
					<div class="deposit-list-rtxtbox">
						<div class="deposit-list-ricon proccess-box"><input type="text" name="buyer_id" id="buyer_id" value="<?php echo $carrierdata['buyer_id'];?>" placeholder="如需統編請在此填寫" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" ></div>
					</div>
				</div>	
			</li> 
			<li class="deposit-list">
				<div class="deposit-box d-flex align-items-center">
					<div class="deposit-list-titlebox">
						<div class="deposit-list-title">紙本發票</div>
					 </div>
					 <div class="deposit-list-rtxtbox">
						<div class="deposit-list-ricon proccess-box">　</div>
					</div>
				</div> 
				<div class="deposit-box d-flex align-items-center">
					 <div class="deposit-list-rtxtbox">
						<input type="text" name="addressee" id="addressee" value="<?php echo $carrierdata['addressee'];?>" placeholder="請輸入收件人" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;width: 26rem;margin: 0.5rem 0.5rem 0rem 2.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" disabled>
						<input type="text" name="address" id="address" value="<?php echo $carrierdata['address'];?>" placeholder="請輸入發票寄送地址" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;width: 26rem;margin: 0rem 0.5rem 0rem 2.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" disabled>
					</div>
				</div>	
			</li>
			<li class="deposit-list" >
				<div class="deposit-box d-flex align-items-center">
					<div class="deposit-list-titlebox">
						<div class="deposit-list-title">捐贈</div>
					</div>
					<div class="deposit-list-rtxtbox">
						<div class="deposit-list-ricon proccess-box">
						<!--input type="number" name="donatecode" id="donatecode" value="<?php echo $carrierdata['donatecode'];?>" maxlength="7" placeholder="請輸入捐贈碼(3~7碼數字)" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" disabled-->
						<select name="donatecode" id="donatecode" disabled>
							<option value="">(請選擇)</option>
								<?php 
									foreach ($donate as $key => $value) {
										if ($carrierdata['donatecode'] == $value['code']) {
											echo '<option value="'.$value['code'].'" selected>'.$value['name'].'</option>';
										} else {
											echo '<option value="'.$value['code'].'">'.$value['name'].'</option>';
										}
									}
								?>
						</select>  
						</div>
					</div>
				</div> 			
			</li> 

			<li class="deposit-list">
				<div class="deposit-box d-flex align-items-center">
					<div class="deposit-list-titlebox">
						<div class="deposit-list-title" style="font-weight: bolder;">載具</div>
					</div>
				</div>
			</li>
			<li class="deposit-list">
				<div class="deposit-box d-flex align-items-center">
					<div class="deposit-list-titlebox">
						<div class="deposit-list-title"><input type="radio" name="carrier" id="carrier-saja" value="saja" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($carrierdata['carrier']== "saja"){echo "checked";}?> disabled>殺價王會員載具(預設)</div>
					</div>
				</div>
			</li>
			<li class="deposit-list">
				<div class="deposit-box d-flex align-items-center">
					<div class="deposit-list-titlebox">
						<div class="deposit-list-title"><input type="radio" name="carrier" id="carrier-phone" value="phone" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($carrierdata['carrier']== "phone"){echo "checked";}?> disabled>手機條碼</div>
					</div>
					<div class="deposit-list-rtxtbox">
						<div class="deposit-list-ricon proccess-box"><input type="text" name="phonecode" id="phonecode" value="<?php echo $carrierdata['phonecode'];?>" placeholder="請輸入/開頭8碼編號" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" disabled></div>
					</div>
				</div>	

			</li>
			<li class="deposit-list">
				<div class="deposit-box d-flex align-items-center">
					<div class="deposit-list-titlebox">
						<div class="deposit-list-title"><input type="radio" name="carrier" id="carrier-phone" value="np" style="-webkit-appearance:radio;width:1.5rem;" <?php if ($carrierdata['carrier']== "np"){echo "checked";}?> disabled>自然人憑證條碼</div>
					</div>
					<div class="deposit-list-rtxtbox">
						<div class="deposit-list-ricon proccess-box"><input type="text" name="npcode" id="npcode" value="<?php echo $carrierdata['npcode'];?>" placeholder="請輸入2碼文大寫+14碼數字序號" style="height: 1.8rem;padding: 0.2rem 0.2rem 0.2rem 0.5rem;border-color: #ffffff;border-style:none none solid none;background-color: #f3f3f3;" disabled></div>
					</div>
				</div>	
			</li>

		</ul>
		<?php } ?>
        <div class="footernav">
            <div class="footernav-button">
                <?php if (($get_deposit['drid'] == 1) && ($browser == 1) && true) { ?>
                    <button id="Submit" type="button" onClick="alert('微信內無法調用支付寶，請在網頁瀏覽器操作!!');document.location = '/site/deposit/';">確定支付</button>
                <?php } else { ?>
                    <button id="Submit" type="button" onClick="chkDataForDeposit(this.form);">確定支付</button>
                <?php } ?>
            </div>
        </div>
	<?php } else if ($get_deposit['drid']=='7' || $get_deposit['drid']=='9') {              // 點數兌換 	?>
        <ul>
            <li>
                <label for="serial_no">序號 : </label>
                <input name="serial_no" id="serial_no" value="" data-clear-btn="true" type="text" placeholder="兌換卡的序號">
            </li>
            <li>
                <label for="passw">密碼 : </label>
                <input name="passw" id="passw" value="" data-clear-btn="true" type="password" placeholder="兌換卡的密碼">
            </li>
            <li>
                <label for="txtInput">驗證碼 : (如無法辨识, 可點选數字图档更换驗證碼)</label>
                <input id="txtInput" name="txtInput" type="text" />
                <span title="點选图档更换驗證碼" id="show_img" onClick="DrawCaptcha();" style="cursor:pointer;"></span>
                <input size="8" type="hidden" id="txtCaptcha" style="background-image:url('<?php echo BASE_URL.$config[" path_image "]; ?>/captcha/captcha.jpg');" />
            </li>
        </ul>
        <div class="footernav">
            <div class="footernav-button">
                <button type="button" id="hinetconfirm" onclick="send_data(this.form);">兌換殺價幣</button>
            </div>
        </div>
	<?php } ?>
	<?php if ($get_deposit['drid'] == 1) {                                                          // 支付宝                ?>
		<!--卖家支付宝帳户-->
		<input type="hidden" name="WIDseller_email" value="<?php echo $config['alipay']['sellemail']; ?>" />
		<!--商户订单號-->
		<input type="hidden" name="WIDout_trade_no" value="<?php echo $get_deposit['ordernumber']; ?>" />
		<!--订单名称-->
		<input type="hidden" name="WIDsubject" value="<?php echo $get_deposit['name']; ?>" />
		<!--付款金額-->

		<?php if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='925' || $_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='597' || $_SESSION['auth_id']=='6' || $_SESSION['auth_id']=='1476' ) { ?>
			<input type="hidden" name="WIDtotal_fee" value="0.01" />
		<?php }else{ ?>
			<input type="hidden" name="WIDtotal_fee" value="<?php echo $get_deposit['amount']; ?>" />
		<?php } ?>

	<?php } else if ($get_deposit['drid'] == 4) { 												    //银联                      ?>
		<input type="hidden" name="drid" value="<?php echo $get_deposit['drid']; ?>">
		<input type="hidden" name="merCert" value="">
		<!--消息版本號-->
		<input type="hidden" name="interfaceVersion" value="<?php echo($config['bankcomm']['interfaceVersion']); ?>">
		<!--商户號-->
		<input type="hidden" name="merID" value="<?php echo($config['bankcomm']['merchID']); ?>">
		<!--订单號-->
		<input type="hidden" name="orderid" value="<?php echo $get_deposit['ordernumber']; ?>">
		<!--交易時間-->
		<?php
				$Ymd=date("Ymd");
				$His=date("His");
			?>
		<input type="hidden" name="orderDate" value="<?php echo($Ymd); ?>">
		<input type="hidden" name="orderTime" value="<?php echo($His); ?>">
		<!--交易类别-->
		<input type="hidden" name="tranType" value="<?php echo($config['bankcomm']['tranType']); ?>">
		<!--交易金額-->
		<input type="hidden" name="amount" value="<?php echo sprintf ("%d", $get_deposit['amount']); ?>">
		<!--交易幣种-->
		<input type="hidden" name="curType" value="<?php echo($config['bankcomm']['curType']); ?>">
		<!--订单内容-->
		<input type="hidden" name="orderContent" value="<?php echo " Spts: ".sprintf ("%d ",$get_deposit['spoint']); ?>">
		<!--商家备注-->
		<input type="hidden" name="orderMono" value="<?php echo (" ID: ".$get_deposit['ordernumber']."/Time: ".$Ymd.$His."/Pay: ".sprintf("%01.2f ", $get_deposit['amount'])); ?>">
		<!--通知方式 0 不通知 1 通知 2 抓取页面 -->
		<input type="hidden" name="notifyType" value="2">
		<input type="hidden" name="goodsURL" value="<?php echo ($config['bankcomm']['goodsURL']); ?>">
		<input type="hidden" name="merURL" value="<?php echo ($config['bankcomm']['merURL']); ?>">
		<input type="hidden" name="jumpSeconds" value="0">
		<!--渠道编號 0:html渠道 -->
		<input type="hidden" name="netType" value="0">
	<?php } else if ($get_deposit['drid'] == 5) {                                                 //微信支付 ?>
		<!--订单號-->
		<input type="hidden" id="orderid" name="orderid" value="<?php echo $get_deposit['ordernumber']; ?>">
		<!--交易金額-->

		<?php if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='925' || $_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='597' || $_SESSION['auth_id']=='6' || $_SESSION['auth_id']=='1476' ) { ?>
		<input type="hidden" id="amount" name="amount" value="0.01">
		<?php }else{ ?>
		<input type="hidden" id="amount" name="amount" value="<?php echo $get_deposit['amount']; ?>">
		<?php } ?>

	<?php } else if($get_deposit['drid'] =='6') {                                                          // ibonpay ?>
		<input type="hidden" name="pay_type" value="1">
		<input type="hidden" name="amount" value="<?php echo sprintf (" %u ", $get_deposit['amount']); ?>">
		<input type="hidden" name="ordernumber" value="<?php echo $get_deposit['ordernumber']; ?>">
	<?php } else if ($get_deposit['drid'] =='7' || $get_deposit['drid'] =='9') { 															//Hinet , alading pts ?>
		<input type="hidden" name="orderid" value="<?php echo $get_deposit['ordernumber']; ?>">
		<input type="hidden" name="drid" value="<?php echo $get_deposit['drid']; ?>">
	<?php } else if ($get_deposit['drid'] == '8') { 														//Creditcard  ?>
		<input type="hidden" name="amount" value="<?php echo sprintf (" %u ", $get_deposit['amount']); ?>">
		<input type="hidden" name="ordernumber" value="<?php echo $get_deposit['ordernumber']; ?>">
		<input type="hidden" name="spoint" value="<?php echo intval($get_deposit['spoint']); ?>">
	<?php } else if ($get_deposit['drid'] == '10') {   // Gamapay  
			  $GamaPayAccount="";
			  if($vendor_pay) {
				  foreach($vendor_pay as $item) {
					  if($item['name']=="gama") {
						 $GamaPayAccount=$item['uid'];
					  }                              
				  }
			  }
	?>
		  <input type="hidden" id="TransAmount" name="TransAmount" value="<?php echo $get_deposit['amount']; ?>">
		  <input type="hidden" id="MerchantOrderID" name="MerchantOrderID" value="<?php echo $get_deposit['ordernumber']; ?>">
		  <!-- 橘子支付的帳號  -->
		  <input type="hidden" id="GamaPayAccount" name="GamaPayAccount" value="<?php echo $GamaPayAccount; ?>">
		  <!-- 我方綁定橘子支付的帳號 -->
		  <input type="hidden" id="MerchantAccount" name="MerchantAccount" value="<?php echo $_SESSION['user']['name']; ?>">
	<?php }else if($get_deposit['drid'] == '11'){   ?> 
		<input type="hidden" name="ordernumber" id="ordernumber" value="<?php echo $get_deposit['ordernumber']; ?>">
		<input type="hidden" name="amount" id="amount" value="<?php echo $get_deposit['amount']; ?>">	
	<?php } ?>	
    </form>	
</div>

<?php if($get_deposit['drid']=='26'){ ?>
<form name="twp_form" id="twp_form" method="POST">
  <input type="hidden" id="acqBank" name="acqBank" value="" />
  <input type="hidden" id="encQRCode" name="encQRCode" value="" />
  <input type="hidden" id="encRetURL" name="encRetURL" value="" />
  <input type="hidden" id="merchantId" name="merchantId" value="" />
  <input type="hidden" id="orderNumber" name="orderNumber" value="" />
  <input type="hidden" id="terminalId" name="terminalId" value="" />
  <input type="hidden" id="txnType" name="txnType" value="" />
  <input type="hidden" id="verifyCode" name="verifyCode" value="">
</form>
<?php } ?>

<div class="esPageLoadBox">
    <div class="espage-loader">
        <span class="es-icon-loading"></span>
    </div>
    <div class="es-panel-dismiss"></div>
</div>

<script language="javascript">
    $(document).ready(function() {
        // alert("is_weixin:"+is_weixin()+" | is_kingkr_obj:"+is_kingkr_obj());
        if (is_kingkr_obj()) {
            $('#runAt').val('WEBAPP');
        } else if (is_weixin()) {
            $('#runAt').val('WEIXIN');
        } else {
            $('#runAt').val('WEB');
        }
        $('.esPageLoadBox').hide();      //Pageloading hide
    });
    $('select[name="issBankNo"]').change(function() {
        <?php if ($_GET['driid'] >= 8 && $_GET['driid'] <= 12) { ?>
        if ($(this).val() == 'BOCOM') {
            $('#comm').show();
        } else {
            $('#comm').hide();
        }
        <?php } else {?>
        $('#comm').hide();
        <?php } ?>
    });

    <?php if($get_deposit['drid'] != 7 && $get_deposit['drid'] != 9) { ?>
    var payRetObj = '';

    function chkDataForDeposit(frm) {
        if (typeof frm.drid !== 'undefined') {
            //alert(frm.drid.value +"--"+frm.runAt.value);
            if (frm.drid.value == '4') {
                idx = frm.elements["issBankNo"].selectedIndex;
                if (frm.elements["issBankNo"].options[idx].value == '') {
                    alert('請選擇发卡行 !!');
                    return false;
                }
            } else if (frm.drid.value == '5' && frm.runAt.value == 'WEBAPP') {
                // appbsl APP for Weixin Pay
                $('#Submit').attr('disabled', true);
                $('#Submit').attr('value', '充值中');　
                $.ajax({
                    type: "post",
                    url: APP_DIR + "/deposit/getWxUnifiedOrderForWebApp",
                            data: {
                        drid: frm.drid.value,
                        driid: frm.driid.value,
                        orderid: frm.orderid.value,
                        total_fee: frm.amount.value,
                        amount: frm.amount.value,
                        chkStr: frm.chkStr.value
                    },
                            error: function(jqXHR, textStatus, errorThrown) {
                        /*错误信息处理*/
                        alert("充值失敗, 請刷新後再試！");
                        location.href = APP_DIR + '/member?t=' + new Date().getTime();
                        return false;
                    },
                    success: function(ret) {　　　　　　　　　　 // alert("[success] : "+ret+"=>"+(typeof ret));
                        try {
                            var data = JSON.parse($.trim(ret));
                            if (data)
                                payRetObj = data.retObj;
                            var wxpayData = '{"appid":"' + payRetObj.appid + '",' + 　　　　　　　　　　　'"noncestr":"' + payRetObj.nonce_str + '",' + 　　　　　　　　　　　　'"package":"Sign=WXPay",' + 　　　　　　　　　　　　'"partnerid":"' + payRetObj.mch_id + '",' +
                                '"prepayid":"' + payRetObj.prepay_id + '",' + 　　　　　　　　　　　　　　'"timestamp":' + payRetObj.timestamp + ',' + 　　　　　　　　　　　'"sign":"' + payRetObj.sign + '"}';
                            if (payRetObj.result_code == 'SUCCESS') {
                                payType(wxpayData, 'WEIXIN', 'wxpayResult');
                            } else {
                                alert("充值失敗, 請刷新後再試！");
                                location.href = APP_DIR + '/member?t=' + new Date().getTime();
                                return false;
                            }
                        } catch (err) {
                            alert("充值失敗 : " + err.message);
                        }　　　　　　　　
                    },

                });
                return false;
            } else if (frm.drid.value == '1' && frm.runAt.value == 'WEBAPP') {
                $('#Submit').attr('disabled', true);
                $('#Submit').attr('value', '充值中');
                $.ajax({　　　　　　　　
                    type: "post",
                    url: APP_DIR + "/deposit/getAliPayOrderForWebApp",
                            data: {
                        WIDout_trade_no: frm.WIDout_trade_no.value,
                        WIDtotal_fee: frm.WIDtotal_fee.value,
                        WIDsubject: frm.WIDsubject.value,
                        WIDseller_email: frm.WIDseller_email.value,
                        chkStr: frm.chkStr.value,
                        drid: frm.drid.value,
                        driid: frm.driid.value,
                        userid: frm.userid.value
                    },
                            async: false,
                            timeout: 5000,
                    error: function(xhr) {
                        alert(xhr);
                    },
                    success: function(ret) {
                        // alert(JSON.stringify(ret));
                        var data = JSON.parse($.trim(ret))
                        if (data.retObj)
                            payRetObj = data.retObj;
                        //alert(payRetObj);
                        if (data.retCode == '1' && data.retMsg == 'OK') {
                            payType(payRetObj, 'ALIPAY', 'alipayResult');
                        } else {
                            alert("充值失敗, 請刷新後再試！");
                            location.href = APP_DIR + '/member?t=' + new Date().getTime();
                            return false;
                        }
                    }
                });
                return false;
            } else if (frm.drid.value == '10') {
                $('.esPageLoadBox').show();                 //Pageloading show
                // 橘子支付
                $.ajax({
                    type:'post',
                    url:'<?php echo BASE_URL."/pay/gamapay/linkPay.php" ?>',
                    data:{
                        MerchantAccount:$('#MerchantAccount').val(),
                        GamaPayAccount:$('#GamaPayAccount').val(),
                        TransAmount:$('#TransAmount').val(),
                        MerchantOrderID:$('#MerchantOrderID').val()
                    },
                    success: function(r) {
                         // alert(r);
                         var obj="";
                         if((typeof r)=="string") {
                             obj=JSON.parse(r);
                         } else if((typeof r)=="object") {
                             obj=r;   
                         }
                         if(obj.ResultCode=='0000') {
                              $.ajax({
                                   type:'post',
                                   url:'<?php echo BASE_URL.APP_DIR; ?>/deposit/gama_pay_result',
                                   data:obj,
                                   success:function(r2){
                                        var obj2="";
                                         if((typeof r2)=="string") {
                                             obj2=JSON.parse(r2);
                                         } else if((typeof r2)=="object") {
                                             obj2=r2;   
                                         }
                                        if(obj2.retCode==1) {
                                           alert("儲值成功 !!");                                                    
                                        } else {
                                           alert("儲值失敗 : "+obj2.retMsg);  
                                        }
                                        window.location.href="<?php echo APP_DIR; ?>/history/";

                                    }
                                });
                         } else {
                            alert("儲值失敗 : "+obj.ResultMessage);
                         }
                    },
                    complete: function() {
                        $('.esPageLoadBox').hide();         //載入完成&失敗後 Pageloading hide
                    }
                }); 
                return false;
            } else if (frm.drid.value == '26') {
                $('.esPageLoadBox').show();                 //Pageloading show
                // 台灣Pay支付
				$.ajax({
                    type:'post',
                    url:"<?php echo BASE_URL.APP_DIR; ?>/deposit/taiwanpay",
                    data:{
                        userid:$('#userid').val(),
                        json:'N',
						drid:$('#drid').val(),
						driid:$('#driid').val(),
						amount:$('#amount').val(),
						chkStr:$('#chkStr').val(),
                        ordernumber:$('#ordernumber').val()
                    },
                    success:function(r){
                        //alert(r);
						var obj="";
                        if((typeof r)=="string") {
                            obj=JSON.parse(r);
                        } else if((typeof r)=="object") {
                            obj=r;   
                        }
						if(obj.status==1) {
                            //console.log(obj.action_data);
							$('#acqBank').val(obj.action_data.acqBank);
							$('#encQRCode').val(obj.action_data.encQRCode);
							$('#encRetURL').val(obj.action_data.encRetURL);
							$('#merchantId').val(obj.action_data.merchantId);
							$('#orderNumber').val(obj.action_data.orderNumber);
							$('#terminalId').val(obj.action_data.terminalId);
							$('#txnType').val(obj.action_data.txnType);
							$('#verifyCode').val(obj.action_data.verifyCode);
							$('#twp_form').attr("action", obj.action_data.post_action);
							$('#twp_form').submit();
                        }
                    },
                    complete: function() {
                        $('.esPageLoadBox').hide();         //載入完成&失敗後 Pageloading hide
                    }
                });
                return false;
            }
        }
        document.getElementById("Submit").innerText = "充值中";
        document.getElementById("Submit").setAttribute("disabled", "disabled");
        document.getElementById("deposit-<?php echo $cdnTime; ?>").submit();
    }

    function alipayResult(r) {
        // alert("[alipay]:"+r);
        if (r == 9000) {　　　　
            alert('支付成功！');
            setTimeout(function() {　　　　
                location.href = APP_DIR + '/member?t=' + new Date().getTime();　　　　
            }, 1000);　　　　
        } else if (r == 6001) {
            alert('您已取消支付！');
            setTimeout(function() {　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);
        } else if (r == 6002) {
            alert('網路異常, 請刷新後再試！');
            setTimeout(function() {　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);
        } else if (r == 5000) {
            alert('重複支付！');
            setTimeout(function() {　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);
        } else if (r == 8000 || r == 6004) {
            alert('支付結果未知, 請洽商戶查詢！');
            setTimeout(function() {　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);
        } else {　　　　　　
            alert('支付失敗, 請刷新後再試！');
            setTimeout(function() {　　　　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);　　
        }
    }

    function wxpayResult(r) {　　　　 // alert("[wxpay]:"+r);
        if (r == 0) {　　　　
            alert('支付成功！');
            setTimeout(function() {　　　　
                location.href = APP_DIR + '/member?t=' + new Date().getTime();　　　　
            }, 1000);　　　　
        } else if (r == -2) {
            alert('您已取消支付！');
            setTimeout(function() {　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);
        } else if (r == -3) {
            alert('發送失敗, 請刷新後再試！');
            setTimeout(function() {　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);
        } else if (r == -4) {
            alert('授權失敗, 請刷新後再試！');
            setTimeout(function() {　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);
        } else {　　　　　　
            alert('支付失敗, 請刷新後再試！');
            setTimeout(function() {　　　　　
                location.href = APP_DIR + '/deposit';　　　　
            }, 1000);　　
        }
    }
    <?php } else if($get_deposit['drid'] == 7 || $get_deposit['drid'] == 9) { ?>

    function send_data(theForm) {
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (theForm.serial_no.value == "") {
            alert("請填写兌換卡序號!");
            theForm.serial_no.focus();
            return;
        }
        if (theForm.passw.value == "") {
            alert("請填写兌換卡密碼!");
            theForm.passw.focus();
            return;
        }
        if (theForm.txtInput.value == "") {
            alert("請輸入驗證碼!");
            theForm.txtInput.focus();
            return;
        }
        if (str1 != str2) {
            alert('驗證碼不正確!');
            return;
        } else {
            theForm.submit();
        }
    }

    //Created / Generates the captcha function    
    function DrawCaptcha() {
        var a = Math.ceil(Math.random() * 9) + '';
        var b = Math.ceil(Math.random() * 9) + '';
        var c = Math.ceil(Math.random() * 9) + '';
        var d = Math.ceil(Math.random() * 9) + '';
        var e = Math.ceil(Math.random() * 9) + '';
        var code = a + '' + b + '' + '' + c + '' + d + '' + e;

        //把code更換圖檔

        var url = "<?php echo BASE_URL.$config["
        path_image "]." / captcha / ";?>";
        var r = "<img src='" + url + a + ".gif'>";
        r += "<img src='" + url + b + ".gif'>";
        r += "<img src='" + url + c + ".gif'>";
        r += "<img src='" + url + d + ".gif'>";
        r += "<img src='" + url + e + ".gif'>";

        document.getElementById("show_img").innerHTML = r;
        document.getElementById("txtCaptcha").value = code;
    }

    // Validate the Entered input aganist the generated security code function   
    function ValidCaptcha() {
        var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
        var str2 = removeSpaces(document.getElementById('txtInput').value);
        if (str1 == str2) return true;
        return false;

    }

    // Remove the spaces from the entered and generated code
    function removeSpaces(string) {
        return string.split(' ').join('');
    }

    $(function() {
        DrawCaptcha();
    });
    <?php } ?>

</script>
