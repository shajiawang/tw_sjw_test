<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$get_product_info = _v('get_product_info'); 
$get_drid_list = _v('get_drid_list');
$paydata = _v('paydata');
?>

	<form id="deposit-<?php echo $cdnTime; ?>" name="deposit-rule-item" method="post" action="<?php echo BASE_URL.APP_DIR; ?>/deposit/onlinpay_confirm/">
		<div >
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                	<div id="tagBox">
                        <img src="<?php echo $config['path_image'];?>/deposit/deposit_title2.jpg?a=1" width="300px" >
					</div>
                                        <li>
                        <div id="driid" class="ui-grid-b">
							<div style="float:left;">下標出價：</div>
							<?php if ($paydata['type'] == "single"){ ?>
								<div style="float:right;"><?php echo round($paydata['bida'], 2); ?> </div>						
							<?php }else{ ?>
								<div style="float:right;"><?php echo round($paydata['bida'], 2); ?> 至 <?php echo round($paydata['bidb'], 2); ?></div>						
							<?php } ?>
						</div><!-- /grid-b -->
                    </li>
                    <li>
                        <div id="driid" class="ui-grid-b">
							<div style="float:left;">下標數：</div>
							<div style="float:right;"><?php echo round($paydata['count']); ?> 標</div>						
                        </div><!-- /grid-b -->
                    </li>
					<?php if (($get_product_info['totalfee_type	'] == "O" ) || ($get_product_info['totalfee_type'] == "F")){?>
                    <li>
                        <div id="driid" class="ui-grid-b">
							<div style="float:left;">下標手續費：</div>
							<div style="float:right;"> RMB ¥<?php echo round($get_product_info['saja_fee'], 2); ?> 元</div>						
                        </div><!-- /grid-b -->
                    </li>
					<?php } ?>
                    <li>
                        <div id="driid" class="ui-grid-b">
							<div style="float:left;">金額：</div>
							<div style="float:right;"> RMB ¥<?php echo round($paydata['total'], 2); ?> 元</div>						
                        </div><!-- /grid-b -->
                    </li>					
                    <li>
                        <div id="drid" >
                        <?php 
                        	if (!empty($get_drid_list)) {
                        		foreach ($get_drid_list as $key => $value) {
							
							if ($value['drid'] <= "5"){

							if ($key % 2 == 0) {
                        		echo '<div class="ui-block-a" style="font-size:0.8em; width: 50% !important;">';
							}
							else if ($key % 2 == 1) {
								echo '<div class="ui-block-b" style="font-size:0.8em; width: 50% !important;">';
							}
                        ?>
                                <input type="radio" name="drid" id="radio-choicepay-<?php echo $value['drid']; ?>" value="<?php echo $value['drid']; ?>" checked> 
                                <label for="radio-choicepay-<?php echo $value['drid']; ?>" style="font-size:0.9em; width:220px; background-color:#ffffff; border-color:#fff;">
                                	<img src="<?php echo $config['path_image']."/deposit/".$value['logo'];?>" height='25px' />
									<?php echo $value['name']; ?>
                                </label>
                            </div>
							<?php } } }//endforeach; ?>
                        </div><!-- /grid-b -->
                    </li>
					<input type="hidden" name="autobid" id="autobid" value="Y">			
					<input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
					<input type="hidden" name="bida" id="bida" value="<?php echo $paydata['bida']; ?>">
					<input type="hidden" name="bidb" id="bidb" value="<?php echo $paydata['bidb']; ?>">
					<input type="hidden" name="type" id="type" value="<?php echo $paydata['type']; ?>">
					<input type="hidden" name="productid" id="productid" value="<?php echo $paydata['productid']; ?>">
					<input type="hidden" name="total" id="total" value="<?php echo $paydata['total']; ?>">
                </li>
            </ul>
        </div>
	<div class="nav">
		<div  class="weui-tabbar" style="background-color:#FFFFFF;width:100%;">
			<div style="width:3%;"> </div>
			<div style="width:94%;" >
				<button type="button" id="topay" onclick="chkonlinepay(this.form);" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0px 0 ">確定</button>
			<div style="width:3%;"> </div>
		</div>			
	</div>		
		<!-- /article -->
<script type="text/javascript" language="javascript">

		function chkonlinepay(frm) {
			
			var kind = $('input:radio[name="drid"]:checked').val();		
			if (kind == undefined){
				alert('請指定支付方式 !!');
				return;
			} else {
				document.getElementById("deposit-<?php echo $cdnTime; ?>").submit();
			}
		}
		
</script>
</form>