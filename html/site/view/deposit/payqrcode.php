<?php 
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$deposit_rule = _v('deposit_rule'); 
$deposit_rule_item = _v('row_list');
$get_deposit = _v('get_deposit');
$get_scode_promote = _v('get_scode_promote');
$spmemo = _v('spmemo');
$spmemototal = _v('spmemototal');
$user_extrainfo = _v('user_extrainfo');
?>
<script src="<?PHP echo APP_DIR; ?>/static/js/qrcode.min.js"></script>

<style type="text/css">
    .a_line{
        color: #2676af;
        text-decoration: underline;
    }
</style>

<div class="article deposit-payfaq">
    <form id="deposit-<?php echo $cdnTime; ?>" name="deposit-rule-item" method="post" action="<?php echo $config[$deposit_rule[0]['act']]['htmlurl']; ?>">
        <input type="hidden" name="runAt" id="runAt" value="WEB">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
        <input type="hidden" name="chkStr" id="chkStr" value="<?php echo $chkStr; ?>">
        <input type="hidden" name="drid" id="drid" value="<?php echo $get_deposit['drid']; ?>">
        <input type="hidden" name="driid" id="driid" value="<?php echo $get_deposit['driid']; ?>">
        
        <ul class="deposit_confirm_ul d-flex">
            <li class="d-inlineflex flex-column" style="width:100%">
                <div class="d-inlineflex bank_title">
                    <div class="bank-logo">
                        <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img<?php echo $user_extrainfo['bankpic']?>" alt="">
                    </div>
                    <div class="bank-name"><?php echo $user_extrainfo['bankname']?></div>
                </div>
                <div class="d-inlineflex align-items-center" style="margin: 10px auto;">
                    <div id="qrcode"></div>
                </div>
            </li>
        </ul>
        <ul class="deposit_confirm_ul">
            <li>
                <div class="deposit-box d-flex">
                    <div class="de_title mr-auto">選擇項目</div>
                    <div class="de_point"><span class="selector"><?php echo $get_deposit['name']; ?></span></div>
                </div>
            </li>
	<?php if($get_deposit['drid']!= '7' && $get_deposit['drid']!= '9' ) {    ?>
            <li class="d-flex">
                <div class="de_title mr-auto">總計</div>
                <div class="re_de_dollar">
                    <div class="red_font">			
                        <?php 
                            echo "<span class=\"symbol\">".$get_deposit['mark']."</span><span>".$get_deposit['amount']."</span>";
                        ?>
                    </div>
                </div>
            </li>
        </ul>
	
	
        <!--- 有無贈品 --->
        <ul class="deposit_confirm_ul">
            <?php if(!empty($spmemo) && $spmemo != '<li>無贈送任何東西</li>') {	?>
            
                <?php echo nl2br($spmemo); ?>
            
            <?php }else{ ?>
                <li>無贈送任何東西</li>
            <?php } ?>
        </ul>
       <?php echo $user_extrainfo['remark']?>
	<?php } ?>

    </form>	
</div>

<script>

var qrcode = new QRCode(document.getElementById("qrcode"), {
    text: "<?php echo $get_deposit['qrcodeformat'][0]['code'];?>",
    width: 200,
    height: 200,
    colorDark : "#000000",
    colorLight : "#ffffff",
    correctLevel : QRCode.CorrectLevel.H
});

</script>