<?php 
$deposit_rule = _v('deposit_rule'); 
$spoint = _v('spoint'); 
$row_list = _v('row_list');
$get_drid_list = _v('get_drid_list');
$scode_promote_memo = _v('scode_promote_memo');
$get_deposit = _v('get_deposit');
$pay_account_list=_v("pay_account_list");
$user_extrainfo=_v("user_extrainfo");
//一銀銷帳編號
//echo $user_extrainfo[8]['field1name'];
$writeoffid="";
if(!empty($user_extrainfo[8]['field1name'])) {
   $tmp=$user_extrainfo[8]['field1name'];
   $writeoffid= substr($tmp,0,4)."-".substr($tmp,4,4)."-".substr($tmp,8,4)."-".substr($tmp,12,4);  
}

//統計支付方式數量
$dridlistcount = count($get_drid_list);

?>
<script>
    $(function(){
        var $dridList = $.parseJSON('<?php echo json_encode($get_drid_list); ?>');
        //console.log($dridList.length);
//        console.log('<?php echo json_encode($dridlistcount); ?>');
        //console.log('<?php echo json_encode($get_drid_list); ?>');
    })
</script>
<!-- 選擇儲值金額 -->
<div class="deposit-select-box">
    <div class="title">請選擇欲購買的額度55555</div>
    <div id="driid" class="d-flex flex-wrap">
        <!--  生成充值選項  -->
    </div>
    <input type="hidden" name = "user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
</div>
<ul class="deposit-lists-box">
    <li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">付款方式</div>
            </div>
            <div class="ui-field-contain deposit-list-rtxtbox deposit-select-box">
                <!-- 支付方式2種以上 使用 select, 只有1種時，使用div -->
                <?php if($dridlistcount > 1) {?>
                    <select name="deposit-list-select" class="deposit-list-select" id="deposit-list-select" onchange="row_list(this.value);">
                        <?php 
                        if (!empty($get_drid_list)) {
                            foreach ($get_drid_list as $key => $value) {
                                $value['drid'] = '7';
                                echo $value['drid'];
                        ?>
                        <?php
                                echo '<option value="'.$value['drid'].'" selected>'.$value['name'].'</option>';
                                ?>
                        <?php } }//endforeach; ?>
                    </select>
                <?php }
                 ?>
                    <div class="text-right"><?php echo $get_drid_list[0]['name'] ?></div>
                    <input type="hidden" name="deposit-list-select" id="deposit-list-select" value="7">
                <?php ?>
            </div>
        </div>
    </li>
</ul>
<ul class="deposit-lists-box">
    <li class="deposit-list">
<!--
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">原剩餘殺價幣</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon">
                  <span><?php echo sprintf("%1\$u", $spoint['amount']); ?>點</span>
                </div>
            </div>
        </div>
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">選擇項目</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon selector-box"><span class="selector">50</span></div>
            </div>
        </div>
-->
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">發票稅5%＋金流手續費</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon proccess-box"><span class="proccess">3</span></div>
            </div>
        </div>		
    </li>
    
    <li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">商品總金額</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon price-box"><span class="symbol">$</span><span class="price">50</span></div>
            </div>
        </div>
    </li>
</ul>
<ul class="deposit-lists-box">
    <li class="deposit-list">
        <div class="deposit-box d-flex align-items-center">
            <div class="deposit-list-titlebox">
                <div class="deposit-list-title">活動送券</div>
            </div>
            <div class="deposit-list-rtxtbox">
                <div class="deposit-list-ricon oscode-box"><span class="memo"></span></div>
            </div>
        </div>
    </li>
</ul>
<div class="oscodePs" style="display:none;">*本活動截止時間 : <span class="offtime"></span></div>

<!--  彈窗-匯款訊息  -->
<div class="remindModal justify-content-center align-items-center">
    <div class="modal-content">
        <div class="modal-body">
            <div class="modal-title d-flex align-items-center justify-content-center">
                <div class="slogn">
                    <p>使用轉帳匯款</p>
                </div>
            </div>
            <div class="remittance-box d-flex align-items-center justify-content-center">
                <div class="info mr-auto">
                    <p><span>第一銀行 興雅分行</span></p>
                    <p><span>銀行總代號：</span><span>007</span></p>
                    <p class="d-flex flex-wrap"><span class="d-inlineflex">個人專屬匯款帳號：</span><span class="d-inlineflex"><?php echo $writeoffid; ?></span></p>
                </div>
                <div class="img">
                    <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/deposit-atm.png" alt="">
                </div>
            </div>
            <div class="remittance-option-box">
                <div class="remittance-title d-flex align-items-center justify-content-center">
                    <i class="esfas ycolor exclamation" style="width: 2.48rem;height: 2.15rem;margin-right: .5rem;"></i>
                    <div>
                        <p>非以下限定金額，須人工處理</p>
                        <p>無法即時到帳、並無贈送殺價券</p>
                    </div>
                </div>
                <div class="option-title">付款金額 / 送券數量：</div>
                <ul class="remittance-options d-flex flex-wrap">
                    <li class="options d-inlineflex align-items-center flex-wrap mr-auto">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 105 </span>
                        <span class="d-inlineflex">(無送券)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 525 </span>
                        <span class="d-inlineflex">(無送券)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap mr-auto">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 2100 </span>
                        <span class="d-inlineflex">(送2張)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 5250 </span>
                        <span class="d-inlineflex">(送6張)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap mr-auto">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 10500 </span>
                        <span class="d-inlineflex">(送15張)</span>
                    </li>
                    <li class="options d-inlineflex align-items-center flex-wrap">
                        <span class="d-inlineflex" style="padding-right:.5rem;">$ 31500 </span>
                        <span class="d-inlineflex">(送50張)</span>
                    </li>
                </ul>
            </div>
            <ul class="remarks-box d-flex flex-wrap">
                <li class="item d-flex">
                    <div class="mini d-flex">
                        <p>本活動截止時間 : 2019/01/13 (日) 23:00:00</p>
                    </div>
                </li>
                <li class="item red d-flex">
                    <div class="mini d-flex">
                        <p>匯款金額內含5%發票稅</p>
                    </div>
                </li>
                <li class="item d-flex">
                    <div class="mini d-flex flex-wrap">
                        <p class="d-inlineflex">匯款成功後約一個工作天內可收到殺價幣/殺價券</p>
                    </div>
                </li>
            </ul>
        </div>
        <div class="modal-proinfo-close"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/close-circle.png"></div>
    </div>
    <div class="remindModalBg"></div>
</div>
   				
    <div class="footernav">
        <div class="footernav-button">
            <button type="button" id="topay" onclick="deposit();" class="ui-btn ui-shadow ui-corner-all">確定</button>
        </div>
    </div>		
<!-- /article -->
<script type="text/javascript">
    var $default = 8;       //預設支付方式
	$(document).ready(function() {
        
		// row_list(1);							//預設銀聯支付
		row_list($default);							//信用卡支付
        
        //將支付選項調整為預設
        $('#deposit-list-select').val($default);
        var $option_text = $('#deposit-list-select').find('option[value='+ $default +']').text();
        $('span.deposit-list-select').text($option_text);

	});
    $(function(){
        //預設開啟匯款彈窗
        // $openPopModal();
        $('.modal-link-bar .link-item.remittance').on('click', function(){
            $openPopModal();
        })
        $('.remindModalBg , .modal-proinfo-close').on('click', function(){
            $closePopModal();
        })
        
    })
	// Add By Thomas 20141111
	// var row_list_Arr2 = new Array();

//	if ($("input[name='radio-drid']").is(":checked")) {
//	    // $("input[name='radio-drid'][value='1']").prop('checked',true).checkboxradio("refresh");
//		$("select option[value='8']").prop('selected',true).checkboxradio("refresh");     // 預設改為微信支付
//	} else {
//	    // $("input[name='radio-drid'][value='1']").attr('checked',true).addClass("refresh");
//		$("select option[value='8']").attr('selected',true).addClass("refresh");          // 預設改為微信支付
//	}
	function row_list(drid) {
		if(drid=='7' || drid=='9' ) {
		   $("#note").hide();
		} else {
		  $("#driid").show();
		  $("#memo").show();
		  $("#note").show();
		}
		var html = '';
		var i = 0;
		//var pageid = $.mobile.activePage.attr('id');
		var row_list_Arr = new Array();
		var row_list_Arr2 = new Array();

		<?php 
			foreach($row_list as $rk => $rvArr) {
				echo "row_list_Arr[$rk] = [];\n";
				echo "row_list_Arr2[$rk] = [];\n";
				foreach ($rvArr as $key =>  $value) {
					echo "row_list_Arr[$rk][$value[driid]] = '".$value[name]."';\n";
					//echo "row_list_Arr[$rk][$value[driid]] = '".intval($value[amount])."元 礼包';\n";
					echo "row_list_Arr2[$rk][$value[driid]] = '".round($value[amount])."';\n";
				}
			}
    	?>
        //購買額度
		for (var k in row_list_Arr[drid]) {
            html += '<div class="payment_option moneymenu-radio">';
			html += '    <input type="radio" name="radio-driid" id="radio-choice-' + k + '" value="' + k + '" onclick="deposit_memo(' + k + ',\'' + row_list_Arr[drid][k] + '\',' + row_list_Arr2[drid][k] +');">';
			html += '    <label for="radio-choice-' + k + '" class="payment_option_title radio-label d-flex align-items-center justify-content-center">'+row_list_Arr[drid][k] + '</label>';
            html += '</div>';
			html += '<input type="hidden" name="radio-driid-count" id="radio-choice-count-' + k + '" value="' + row_list_Arr2[drid][k] + '">';			
			html += '<input type="hidden" name="radio-driid-name" id="radio-choice-name-' + k + '" value="' + row_list_Arr[drid][k] + '">';			
			i++;
		}
		var val = $("input[name='radio-driid']:checked").index("input[name='radio-driid']");

		//刪除id = driid內容
		//$("#driid .weui-grid3").remove();
		document.getElementById('driid').innerHTML="";
		
		//$("#driid").append(html).trigger('create');
		document.getElementById('driid').innerHTML = html;

		//$("#" + pageid + " #driid");
        
        //預設選取第一個金額
		//$("input[name='radio-driid']:eq(0)").prop('checked',true);
        
		if (val > -1) {
			$("input[name='radio-driid']:eq(" + val + ")").prop('checked',true);
			var v = $("input[name='radio-driid']:checked").val();
			deposit_memo($("input[name='radio-driid'][value=" + v + "]").val(),$("input[id='radio-choice-name-"+v+"']").val(),$("input[id='radio-choice-count-"+v+"']").val());
			$("input[name='radio-driid'][value=" + v + "]").prop('checked',true);
            
		}else {
			if(drid=='1' || drid=='5' ) {
				$("input[name='radio-driid']:eq(3)").prop('checked',true);
				deposit_memo($("input[name='radio-driid']:eq(3)").val(),$("input[name='radio-driid-name']:eq(3)").val(),$("input[name='radio-driid-count']:eq(3)").val());	//預設第4筆資料
			} else {
			   $("input[name='radio-driid']:eq(1)").prop('checked',true);
			   deposit_memo($("input[name='radio-driid']:eq(1)").val(),$("input[name='radio-driid-name']:eq(1)").val(),$("input[name='radio-driid-count']:eq(1)").val());	//預設第2筆資料
			}
		}
    }
    
    function deposit_memo(driid,dname,price){
    	var scode_promote_memo_Arr = new Array();
        scode_promote_memo_Arr =JSON.parse('<?php echo json_encode($scode_promote_memo); ?>');

    	$("#memo h4").html(scode_promote_memo_Arr[driid]);
        
        console.log(scode_promote_memo_Arr);
        //贈送殺價券 樣式
        var $freeOscode;
        if(scode_promote_memo_Arr[driid][0]['num'].length > 0){
            if(scode_promote_memo_Arr[driid][0]['num'].indexOf('無') < 0){
                $freeOscode = scode_promote_memo_Arr[driid][0]['num'];
                $('.memo').removeClass('gray');
                $('.memo').text('送大檔殺價券 '+scode_promote_memo_Arr[driid][0]['num']+' 張');
            }else{
                $freeOscode = 0;
                $('.memo').addClass('gray');
                $('.memo').text('無贈送殺價券');
            }
        }
        var offtime = scode_promote_memo_Arr[driid][0]['offtime'] || '';
        $changeShow(dname,price,offtime,$freeOscode);
    }	
    
    //更改切換文字
    function $changeShow(dname,price,offtime,$freeOscode) {
        var $selector = $(".deposit-list-rtxtbox .selector-box .selector");
        var $price = $(".deposit-list-rtxtbox .price-box .price");                  //商品總金額
		var $proccess = $(".deposit-list-rtxtbox .proccess-box .proccess");         //處理費用
        var $offtime_parent = $('.oscodePs');                                       //活動截止時間父層
        var $offtime = $offtime_parent.find('.offtime');                            //活動截止時間
		//console.log(offtime);
		var $ps = price - dname.replace(/點|点|元|匯款/, "");                          //替換成 元

        $selector.html(dname);
        $price.html(price);
		$proccess.html($ps+'元');
        
        if((offtime !== '') && ($freeOscode !== 0)){ 
            $offtime_parent.show();
            $offtime_parent.find('.offtime').html(offtime);
        }else{
            $offtime_parent.hide();
            $offtime_parent.find('.offtime').html('');
        }
    }
    
    //開啟匯款彈框
    function $openPopModal() {
        $('body').addClass('modal-open');
        $('.remindModal').addClass('d-flex').show();
        $('.remindModalBg').show();
    }
    //關閉匯款彈框
    function $closePopModal() {
        $('body').removeClass('modal-open');
        $('.remindModal').removeClass('d-flex').hide();
        $('.remindModalBg').hide();
    }
</script>
