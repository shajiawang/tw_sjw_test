<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$redata=_v('redata');
?>
<div class="article">
    <div class="codeBox">
        <?php if(!empty($redata)){ ?>
        <div id="barcodeBox">
            <img src="<?php echo APP_DIR; ?>/barcodegen/genbarcode.php?text=<?php echo $redata['BarcodeA']; ?>">
        </div>
        <div id="barcodeBox">
            <img src="<?php echo APP_DIR; ?>/barcodegen/genbarcode.php?text=<?php echo $redata['BarcodeB']; ?>">
        </div>
        <div id="barcodeBox">
            <img src="<?php echo APP_DIR; ?>/barcodegen/genbarcode.php?text=<?php echo $redata['BarcodeC']; ?>">
        </div>		
		
        <div id="bonus" class="d-flex align-items-center justify-content-center">
            <img class="bonusIcon" src="<?php echo APP_DIR;?>/static/img/member/depositspoint.png">
            <p class="title">支付金額：</p>
            <div class="bonusPoint">NT $<?php echo $redata['MN'];?> 元</div>
        </div>
		<?php } ?>
    </div>
</div>