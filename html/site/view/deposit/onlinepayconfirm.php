<?php 
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$deposit_rule = _v('deposit_rule'); 
$get_deposit = _v('get_deposit');
$bank_list=_v('bank_list');
$chkStr=_v('chkStr');
$paydata=_v('paydata');
$bida=_v('bida');
$bidb=_v('bidb');
$bidtype=_v('bidtype');
$userid=_v('user');
$autobid=_v('autobid');
$productid=_v('productid');
$total=_v('total');
error_log("[v/onlinepayconfirm]:".json_encode($paydata));
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
	$browser = 1;
}else{
	$browser = 2;
}  
?>
<form id="deposit-<?php echo $cdnTime; ?>" name="deposit-rule-item" method="post" action="<?php echo $config[$deposit_rule[0]['act']]['htmlurl']; ?>">
		
	<div class="article">
			<input type="hidden" name="chkStr" id="chkStr" value="<?php echo $chkStr; ?>">
			<ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                	<div id="tagBox">
                        <img src="<?php echo $config['path_image'];?>/deposit/<?php echo $deposit_rule[0]['banner']; ?>">
                    </div>
                </li>
		    <?php if ($get_deposit['drid'] == 4) {                                                      //銀聯 ?>
						<li>发卡行 : 
							<select name="issBankNo" >
								<option value="">(請選擇)</option>
					            <?php foreach($bank_list as $key=>$value) {	?>
									<option value="<?php echo $key; ?>" ><?php echo $value; ?></option> 
								<?php }			?>
							</select>
						</li>
            <?php  }  ?> 
			<?php if ($get_deposit['drid'] == 6){                                                       // ibonpay ?>
                        <li> 
						   <h2>
						   <font class="autowrap"> 
					        储值流程：
								<br/>1.按下方 "確定充值"，系统将产生缴費代碼并显示于页面
								<br/>2.至统一(7-ELEVEN),全家(Family Mart)或莱尔富(Hi-Life)超商機台输入缴費代碼打印缴費单。
								<br/>3.打印成功后，持缴費单至柜台缴費。
								<br/>4.缴費完成后3-30分钟，殺價幣就会汇入您的帳户中。
								<br/>5.缴費收据請妥善保存备查，以保障您的权益。
								<br/>6.如需多组代碼缴費，請务必间隔三分钟后，再取得新代碼。
						    </font>
							</h2>
					    </li>        
			<?php  }   ?>
			<?php  if($get_deposit['drid']!= '7' && $get_deposit['drid']!= '9' ) {    ?>
		
						<?php  
							$currency="RMB ¥";
							if($get_deposit['drid']=='6' || $get_deposit['drid']=='8') {
							   $currency="NT $";
							}
						?>

						<li>
							<div style="float:left;">金額：</div>
							<div style="float:right;"><b><font color="red">
								<?php 
									echo $currency.$get_deposit['amount']; 
								?>
							</font></b>元</div>
						</li>
					</ul>
					
            <?php } else if ($get_deposit['drid']=='7' || $get_deposit['drid']=='9') {              // 點數兌換 	?>
							<li>
								<label for="serial_no">序號 : </label>
								<input name="serial_no" id="serial_no" value="" data-clear-btn="true" type="text" placeholder="兌換卡的序號">
							</li>
							<li>
								<label for="passw">密碼 : </label>
								<input name="passw" id="passw" value="" data-clear-btn="true" type="password" placeholder="兌換卡的密碼" >
							</li>
							<li>
								<label for="txtInput">驗證碼 : (如無法辨识, 可點选數字图档更换驗證碼)</label>
								<input id="txtInput" name="txtInput" type="text" />
								<span title="點选图档更换驗證碼" id="show_img" onClick="DrawCaptcha();" style="cursor:pointer;"></span>
								<input size="8" type="hidden" id="txtCaptcha" style="background-image:url('<?php echo BASE_URL.$config["path_image"]; ?>/captcha/captcha.jpg');" />
							</li>
					   </ul>
					   
			<?php } ?>
            <?php if ($get_deposit['drid'] == 1) {                                                          // 支付宝                ?>
					<!--卖家支付宝帳户-->
					<input type="hidden" name="WIDseller_email" id="WIDseller_email" value="<?php echo $config['alipay']['sellemail']; ?>" />
					<!--商户订单號-->
					<input type="hidden" name="WIDout_trade_no" id="WIDout_trade_no" value="<?php echo $get_deposit['ordernumber']; ?>" />
					<!--订单名称-->
					<input type="hidden" name="WIDsubject" id="WIDsubject" value="<?php echo $get_deposit['name']; ?>" />
					<!--付款金額-->
					<input type="hidden" name="WIDtotal_fee" id="WIDtotal_fee" value="<?php echo $get_deposit['amount']; ?>" />
					
					
            <?php } else if ($get_deposit['drid'] == 4) { 												    //银联                      ?>
					<input type="hidden" name="drid" value="<?php echo $get_deposit['drid']; ?>">
					<input type="hidden" name="merCert" value="">
					<!--消息版本號-->
					<input type = "hidden" name = "interfaceVersion" value = "<?php echo($config['bankcomm']['interfaceVersion']); ?>">
					<!--商户號-->
					<input type = "hidden" name = "merID" value = "<?php echo($config['bankcomm']['merchID']); ?>">
					<!--订单號-->
					<input type = "hidden" name = "orderid" value = "<?php echo $get_deposit['ordernumber']; ?>">
					<!--交易時間-->
					<?php
						$Ymd=date("Ymd");
						$His=date("His");
					?>
					<input type = "hidden" name = "orderDate" value = "<?php echo($Ymd); ?>">
					<input type = "hidden" name = "orderTime" value = "<?php echo($His); ?>">
					<!--交易类别-->
					<input type = "hidden" name = "tranType" value = "<?php echo($config['bankcomm']['tranType']); ?>">
					<!--交易金額-->
					<input type = "hidden" name = "amount" value = "<?php echo sprintf ("%01.2f", $get_deposit['amount']); ?>">
					<!--交易幣种-->
					<input type = "hidden" name = "curType" value = "<?php echo($config['bankcomm']['curType']); ?>">
					<!--订单内容-->
					<input type = "hidden" name = "orderContent" value = "<?php echo "Spts:".sprintf ("%01.2f",$get_deposit['spoint']); ?>" >
					<!--商家备注-->
					<input type = "hidden" name = "orderMono" value = "<?php echo ("ID:".$get_deposit['ordernumber']."/Time:".$Ymd.$His."/Pay:".sprintf("%01.2f", $get_deposit['amount'])); ?>">          
					<!--通知方式 0 不通知 1 通知 2 抓取页面 -->
					<input type="hidden" name="notifyType" value="2">
					<input type="hidden" name="goodsURL" value="<?php echo ($config['bankcomm']['goodsURL']); ?>" >
					<input type="hidden" name="merURL" value="<?php echo ($config['bankcomm']['merURL']); ?>" >
					<input type="hidden" name = "jumpSeconds" value = "0">
					<!--渠道编號 0:html渠道 -->
					<input type="hidden" name="netType" value="0">
			<?php } else if ($get_deposit['drid'] == 5) {                                                 //微信支付 ?>
					<!--订单號-->
					<input type="hidden" name="orderid" value="<?php echo $get_deposit['ordernumber']; ?>">
					<!--交易金額-->
					
   					<?php if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='1777' || $_SESSION['auth_id']=='2525' ) { ?>
					<input type="hidden" name="amount" value="0.01">
					<?php }else{ ?>
					<input type="hidden" name="amount" value="<?php echo $get_deposit['amount']; ?>">
					<?php } ?>
					
			<?php } else if($get_deposit['drid'] =='6') {                                                          // ibonpay ?>
					<input type="hidden" name="pay_type" value="1">
					<input type="hidden" name="amount"  value="<?php echo sprintf ("%u", $get_deposit['amount']); ?>">
					<input type="hidden" name="ordernumber" value="<?php echo $get_deposit['ordernumber']; ?>" >
			<?php } else if ($get_deposit['drid'] =='7' || $get_deposit['drid'] =='9') { 						//Hinet , alading pts ?>                                      
					<input type = "hidden" name = "orderid" value = "<?php echo $get_deposit['ordernumber']; ?>">
					<input type = "hidden" name = "drid" value = "<?php echo $get_deposit['drid']; ?>">
			<?php } else if ($get_deposit['drid'] == '8') { 														//Creditcard  ?>
					<input type = "hidden" name = "amount" value = "<?php echo sprintf ("%u", $get_deposit['amount']); ?>">
					<input type = "hidden" name = "ordernumber" value = "<?php echo $get_deposit['ordernumber']; ?>">
					<input type = "hidden" name = "spoint" value="<?php echo intval($get_deposit['spoint']); ?>" >
			<?php } ?>
			<input type="hidden" name="autobid" id="autobid" value="<?php echo $autobid; ?>" >			
			<input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>" >
			<input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>" >
			<input type="hidden" name="bida" id="bida" value="<?php echo $bida; ?>" >
			<input type="hidden" name="bidb" id="bidb" value="<?php echo $bidb; ?>" >
			<input type="hidden" name="type" id="type" value="<?php echo $bidtype; ?>" >
			<input type="hidden" name="productid" id="productid" value="<?php echo $productid; ?>" >
            <input type="hidden" name="runAt" id="runAt" value="WEB" >
			<input type="hidden" name="drid" id="drid" value="<?php echo $get_deposit['drid']; ?>" >			
	</div>
	<?php  if($get_deposit['drid']!= '7' && $get_deposit['drid']!= '9' ) {    ?>

		<div class="nav">
			<div  class="weui-tabbar" style="background-color:#FFFFFF;width:100%;">
				<div style="width:3%;"> </div>
				<div style="width:94%;" >
					<?php if (($get_deposit['drid'] == 1) && ($browser == 1) && true) { ?>
						<button id="Submit" type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="alert('微信內無法调用支付宝，請在网页浏览器操作!!');document.location = '/site/deposit/';">確定支付</button>
					<?php } else { ?>
						<button id="Submit" type="button" class="ui-btn ui-corner-all ui-btn-a" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0px 0 " onClick="chkDataForDeposit(this.form);">確定支付</button>
					<?php } ?>				
				<div style="width:3%;"> </div>
			</div>			
		</div>		

	<?php } else if ($get_deposit['drid']=='7' || $get_deposit['drid']=='9') {              // 點數兌換 	?>

		<div class="nav">
			<div  class="weui-tabbar" style="background-color:#FFFFFF;width:100%;">
				<div style="width:3%;"> </div>
				<div style="width:94%;" >
					<button type="button" id="hinetconfirm" class="ui-btn ui-corner-all ui-btn-a" onClick="send_data(this.form);">兌換殺價幣</button>	
				<div style="width:3%;"> </div>
			</div>			
		</div>	
	<?php } ?>
</form>		
<script language="javascript">
    var payRetObj;
	var oproductid;
	var obida;
	var obidb;
	var oautobid;
	var obidtype;
	var qrystr;
			   
	$(document).ready(function(){
		if(is_kingkr_obj()) {
		   $('#runAt').val('WEBAPP');
		} else if(is_weixin()) {
		   $('#runAt').val('WEIXIN');
		} else {
		   $('#runAt').val('WEB');
		}
        oproductid='<?php echo $productid; ?>';
	    obida='<?php echo $bida; ?>';
	    obidb='<?php echo $bidb; ?>';
	    oautobid='<?php echo $autobid; ?>';
	    obidtype='<?php echo $bidtype; ?>';
	    qrystr='productid='+oproductid+'&bida='+obida+'&bidb='+obidb+'&auto='+oautobid+'&type='+obidtype;		
		// alert($('#runAt').val());
	});
	$('select[name="issBankNo"]').change(function() {
	<?php if ($_GET['driid'] >= 8 && $_GET['driid'] <= 12) { ?>
		if ($(this).val() == 'BOCOM') {
			$('#comm').show();
		} else {
			$('#comm').hide();
		}
	<?php } else {?>
		$('#comm').hide();
	<?php } ?>
	});
	
	<?php if($get_deposit['drid'] != 7 && $get_deposit['drid'] != 9) { ?>
		
		function chkDataForDeposit(frm) {
			if(typeof frm.drid !== 'undefined') {
				if(frm.drid.value=='4') {
					idx=frm.elements["issBankNo"].selectedIndex;
					if(frm.elements["issBankNo"].options[idx].value=='') {
						alert('請選擇发卡行 !!');
						return false;
					}
				} else if(frm.drid.value=='5' && frm.runAt.value=='WEBAPP' ) {
				    $('#Submit').attr('disabled', true);
					$('#Submit').attr('value','充值中');
					$.ajax({
			　　　　　　　　type:"post",
							url: APP_DIR+"/deposit/getWxUnifiedOrderForWebApp", 
			　　　　　　　　data: {orderid:frm.orderid.value
								  ,total_fee:frm.amount.value
								  ,amount:frm.amount.value
								  ,chkStr:frm.chkStr.value
							},
			　　　　　　　　async: false,
			　　　　　　　　timeout: 5000,
							error: function (xhr) {
								  alert(xhr);
							},   
			　　　　　　　　success: function(ret){
			　　　　　　　　　　　//成功回访　
			　　　　　　　　　　  var data = JSON.parse($.trim(ret))
			                      if(data.retObj)
									 payRetObj=data.retObj;
								  var wxpayData='{"appid":"'+payRetObj.appid+'",'+　　
				　　　　　　　　　   '"noncestr":"'+payRetObj.nonce_str+'",'+　　　
				　　　　　　　　　   '"package":"Sign=WXPay",'+　　　   
				　　　　　　　　　   '"partnerid":"'+payRetObj.mch_id+'",'+
									 '"prepayid":"'+payRetObj.prepay_id+'",'+　　 　　　    
				　　　　　　　　　   '"timestamp":'+payRetObj.timestamp+','+　　   
				　　　　　　　　　   '"sign":"'+payRetObj.sign+'"}'; 
								  if(payRetObj.result_code=='SUCCESS') {
									 payType(wxpayData,'WEIXIN','wxpayResult');
								  } else { 
									 alert("充值失敗, 請刷新后再试！");
									 location.href=APP_DIR+'/member?t='+new Date().getTime();
									 return false;  
								  }
			　　　　　　　　}
			　　　　　});
					  return false;
				} else if(frm.drid.value=='1' && frm.runAt.value=='WEBAPP' ) {
				      $('#Submit').attr('disabled', true);
					  $('#Submit').attr('value','充值中');
					  $.ajax({
			　　　　　　　　type:"post",
							url: APP_DIR+"/deposit/getAliPayOrderForWebApp", 
			　　　　　　　　data: {WIDout_trade_no:frm.WIDout_trade_no.value
								  ,WIDtotal_fee:frm.WIDtotal_fee.value
								  ,WIDsubject:frm.WIDsubject.value
								  ,WIDseller_email:frm.WIDseller_email.value
								  ,chkStr:frm.chkStr.value
								  ,bida:frm.bida.value
								  ,bidb:frm.bidb.value
								  ,type:frm.type.value
								  ,autobid:frm.autobid.value
								  ,productid:frm.productid.value
								  ,userid:frm.user.value
							},
			　　　　　　　　async: false,
			　　　　　　　　timeout: 5000,
							error: function (xhr) {
								  alert(xhr);
							}, 
                            success: function(ret) {
                                  // alert(JSON.stringify(ret));
								  var data = JSON.parse($.trim(ret))
			                      if(data.retObj)
									 payRetObj=data.retObj;
								  if(data.retCode=='1' && data.retMsg=='OK') {
									 payType(payRetObj,'ALIPAY','alipayResult');
								  } else { 
									 alert("充值失敗, 請刷新后再试！");
									 location.href=APP_DIR+'/member?t='+new Date().getTime();
									 return false;  
								  }
                            }							
				      });
					  return false;
				}
			}
			document.getElementById("Submit").innerText = "充值中";
			document.getElementById("Submit").setAttribute("disabled","disabled");
			document.getElementById("deposit-<?php echo $cdnTime; ?>").submit();
		}
		
		function alipayResult(r) {
                // alert("[alipay]:"+qrystr);
				if(r==9000){
				    alert('支付成功！');
			　　　　setTimeout(function(){
			  　　　　 location.href=APP_DIR+'/product/sajatopay?'+qrystr;
			　　　　}, 1000);
		　　　　} else if(r==6001) {
		            alert('您已取消支付！');
					setTimeout(function(){
			  　　　   location.href=APP_DIR+'/deposit';
			　　　　}, 1000); 
		        } else if(r==6002) {
		            alert('网络异常, 請刷新后再试！');
					setTimeout(function(){
			  　　　	location.href=APP_DIR+'/deposit';
			　　　　}, 1000);
				} else if(r==5000) {
		            alert('重复支付！');
					setTimeout(function(){
			  　　　   location.href=APP_DIR+'/deposit';
			　　　　}, 1000);
				} else if(r==8000 || r==6004) {
		            alert('支付结果請洽商户查询！');
					setTimeout(function(){
			  　　　	location.href=APP_DIR+'/deposit';
			　　　　}, 1000);
				} else {
		　　　　　　alert('支付失败, 請刷新后再试！');
		            setTimeout(function(){
			　　　　　　location.href=APP_DIR+'/deposit';
			　　　　}, 1000);
		　　    }
            }
		
			function wxpayResult(r) {
		　　　　// alert("[wxpay]:"+qrystr);
		        if(r==0){
			　　　　alert('支付成功！');
			        setTimeout(function(){
			  　　　　 location.href=APP_DIR+'/product/sajatopay?'+qrystr;
				    }, 1000);
		　　　　} else if(r==-2) {
		            alert('您已取消支付！');
					setTimeout(function(){
			  　　　   location.href=APP_DIR+'/deposit';
			　　　　}, 1000); 
		        } else if(r==-3) {
		            alert('发送失败, 請刷新后再试！');
					setTimeout(function(){
			  　　　   location.href=APP_DIR+'/deposit';
			　　　　}, 1000);
				} else if(r==-4) {
		            alert('授权失败, 請刷新后再试！');
					setTimeout(function(){
			  　　　   location.href=APP_DIR+'/deposit';
			　　　　}, 1000);
				} else {
		　　　　　　alert('支付失败, 請刷新后再试！');
		            setTimeout(function(){
			　　　　　 location.href=APP_DIR+'/deposit';
			　　　　}, 1000);
		　　    }
		    }
	<?php } else if($get_deposit['drid'] == 7 || $get_deposit['drid'] == 9) { ?>
		function send_data(theForm)
		{
			var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
			var str2 = removeSpaces(document.getElementById('txtInput').value);
			if(theForm.serial_no.value=="" )
			{
				alert("請填写兌換卡序號!");
				theForm.serial_no.focus();
				return ;
			}
			if(theForm.passw.value=="" )
			{
				alert("請填写兌換卡密碼!");
				theForm.passw.focus();
				return ;
			}
			if(theForm.txtInput.value=="" )
			{
				alert("請輸入驗證碼!");
				theForm.txtInput.focus();
				return ;
			}
			if (str1 != str2)
			{
				alert('驗證碼不正確!');
				return ;        
			} else
			{
				theForm.submit();
			}
		}

		//Created / Generates the captcha function    
		function DrawCaptcha()
		{
			var a = Math.ceil(Math.random() * 9)+ '';
			var b = Math.ceil(Math.random() * 9)+ '';       
			var c = Math.ceil(Math.random() * 9)+ '';  
			var d = Math.ceil(Math.random() * 9)+ '';  
			var e = Math.ceil(Math.random() * 9)+ '';  
			var code = a + '' + b + '' + '' + c + '' + d + '' + e;
			
			//把code更換圖檔
			
			var url = "<?php echo BASE_URL.$config["path_image"]."/captcha/";?>";
			var r = "<img src='"+url+a+".gif'>";
			r += "<img src='"+url+b+".gif'>";
			r += "<img src='"+url+c+".gif'>";
			r += "<img src='"+url+d+".gif'>";
			r += "<img src='"+url+e+".gif'>";
			
			document.getElementById("show_img").innerHTML = r;
			document.getElementById("txtCaptcha").value = code;
		}

		// Validate the Entered input aganist the generated security code function   
		function ValidCaptcha(){
			var str1 = removeSpaces(document.getElementById('txtCaptcha').value);
			var str2 = removeSpaces(document.getElementById('txtInput').value);
			if (str1 == str2) return true;        
			return false;
			
		}

		// Remove the spaces from the entered and generated code
		function removeSpaces(string)
		{
			return string.split(' ').join('');
		}
		
		$(function() { DrawCaptcha();});
	<?php } ?>
</script>
