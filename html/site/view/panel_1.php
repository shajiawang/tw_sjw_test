<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') >0 ) {
    $browser = 1;
}else{
    $browser = 2;
}
?>
<div class="sajaPanel" data-role="panel" id="left-panel<?php get_panel_id(); ?>" data-display="overlay" data-position="right">
    <div class="panel_close" data-rel="close">
        <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/hamburger.png">
    </div>
    <div class="panel_lists">
        <div class="panelList">
            <a href="<?php echo BASE_URL.APP_DIR; ?>/" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                   <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-home.png">
                </div>
                <div class="panelList-label">
                    <span>回首页</span>
                </div>
            </a>
        </div>
        <div class="panelList">
            <a href="<?php echo BASE_URL.APP_DIR; ?>/bid/?channelid=<?php echo $_GET['channelid']; ?>&<?php echo $cdnTime; ?>"  <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-bid.png">
                </div>
                <div class="panelList-label">
                    <span>最新成交</span>
                </div>
            </a>
        </div>
        <div class="panelList">
            <a href="<?php echo BASE_URL.APP_DIR; ?>/product/?<?php echo $cdnTime; ?>" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-saja.png">
                </div>
                <div class="panelList-label">
                    <span>殺價列表</span>
                </div>		
            </a>
        </div>
        <div class="panelList">
            <a href="<?php echo BASE_URL.APP_DIR; ?>/mall/?channelid=<?php echo $_GET['channelid']; ?>&epcid=1&<?php echo $cdnTime; ?>" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-mall.png">
                </div>
                <div class="panelList-label">
                    <span>兌換商城</span>
                </div>
            </a>
        </div>
        <div class="panelList">
            <a href="<?php echo BASE_URL.APP_DIR; ?>/product/today?<?php echo $cdnTime; ?>" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-today.png">
                </div>
                <div class="panelList-label">
                    <span>今日必殺</span>
                </div>
            </a>
        </div>

    <?php if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='925' || $_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='597' || $_SESSION['auth_id']=='1861' || $_SESSION['auth_id']=='1476') { ?>
        <div class="panelList">
            <a href="<?php echo BASE_URL.APP_DIR; ?>/mall/genUserTxQrcode2/?<?php echo $cdnTime; ?>" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-qrcode.png">
                </div>
                <div class="panelList-label">
                    <span>兌換碼</span>
                </div>	
            </a>
        </div>
    <?php } ?>

    <?php if ($_SESSION['auth_id']=='28' && $_GET['fun'] != 'deposit') { ?>
        <div id="deposititem1" style="display:none">
            <div class="panelList">
                <a onClick="awakeOtherBrowser('<?php echo BASE_URL.APP_DIR; ?>/deposit/')">
                    <div class="panelList-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-deposit.png">
                    </div>
                    <div class="panelList-label">
                        <span>充值殺價幣</span>
                    </div>
                </a>
            </div>
        </div>
        <div id="deposititem2" style="display:none">
            <div class="panelList">
                <a href="<?php echo BASE_URL.APP_DIR; ?>/deposit/"  <?php if ($browser == 1){ ?>target="_blank"<?php } ?> >
                    <div class="panelList-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-deposit.png">
                    </div>
                    <div class="panelList-label">
                        <span>充值殺價幣</span>
                    </div>	
                </a>
            </div>
        </div>
    <?php } ?>
        <div class="panelList">
           <a href="<?php echo BASE_URL.APP_DIR; ?>/faq/?<?php echo $cdnTime; ?>" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
               <div class="panelList-icon">
                   <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-faq.png">
                </div>
               <div class="panelList-label">
                   <span>新手教学</span>
               </div>
           </a>
       </div>
        <div class="panelList">
            <a href="<?php echo BASE_URL.APP_DIR; ?>/tech/?<?php echo $cdnTime; ?>" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-tech.png">
                </div>
                <div class="panelList-label">
                    <span>殺價密技</span>
                </div>
            </a>
        </div>
        <div class="panelList panelListPlace">
    <?php if($user->is_logged || !empty($_SESSION['auth_id'])){ ?>   
            <a href="#" onClick="javascript:logout()">
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-loginout.png">
                </div>
                <div class="panelList-label">
                    <span>退出</span>
                </div>
            </a>
    <?php }else{ ?>
            <a href="<?php echo BASE_URL.APP_DIR; ?>/member/userlogin/?<?php echo $cdnTime; ?>" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="panelList-icon">
                    <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/panel-loginout.png">
                </div>
                <div class="panelList-label">
                    <span>殺友登录</span>
                </div>
            </a>
        <?php } ?>
        </div>
    </div>
</div><!-- /panel -->

<div data-role="panel" id="right-panel<?php get_panel_id(); ?>" data-display="overlay" data-position="right">
    <ul data-role="listview" data-icon="false">
        <?php /*<li data-icon="delete"><a href="javascript:void(0);" data-rel="close">关闭选单</a></li>
			<li data-role="list-divider"></li>*/?>

        <?php if($user->is_logged || !empty($_SESSION['auth_id'])){ ?>         
        <li><a href="<?php echo APP_DIR; ?>/member/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-user ui-btn-icon-right">会员中心</a></li>
        <li><a href="<?php echo APP_DIR; ?>/user/profile/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-user ui-btn-icon-right">会员修改</a></li>
        <li><a href="<?php echo APP_DIR; ?>/member/news/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-mail ui-btn-icon-right">站内公告</a></li>
        <?php if($_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='925' || $_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='597' || $_SESSION['auth_id']=='612' || $_SESSION['auth_id']=='6' || $_SESSION['auth_id']=='1777' || $_SESSION['auth_id']=='1795' || $_SESSION['auth_id']=='1861') { ?>
        <li><a href="<?php echo APP_DIR; ?>/mall/genUserTxQrcode2/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-plus ui-btn-icon-right">兌換碼</a></li>
        <?php }  ?>
        <?php if ($_GET['fun'] != 'deposit') { ?>

        <li id="deposititem1" style="display:none"><a onClick="awakeOtherBrowser('<?php echo APP_DIR; ?>/deposit/')" >充值殺價幣</a></li>
        <li id="deposititem2" style="display:none"><a href="<?php echo APP_DIR; ?>/deposit/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-plus ui-btn-icon-right">充值殺價幣</a></li>
        <?php } ?>
        <li data-role="collapsible" data-iconpos="right" data-inset="false" data-collapsed-icon="user" data-expanded-icon="user">
            <h2>会员绑定</h2>
            <ul data-role="listview" data-theme="b">
                <?php if (is_weixin()) { ?>
                <li><a href="<?php echo APP_DIR; ?>/user/weixinbindingapi/?<?php echo $cdnTime; ?>" rel="external">微信绑定</a></li>
                <?php } else { ?>
                <li><a href="#" onclick="alert('从殺價王微信公众號绑定');" rel="external">微信绑定</a></li>
                <?php } ?>
            </ul>
        </li>

        <li data-role="collapsible" data-iconpos="right" data-inset="false" data-collapsed-icon="search" data-expanded-icon="search">
            <h2>交易纪录</h2>
            <ul data-role="listview" data-theme="b">
                <li><a href="<?php echo APP_DIR; ?>/history/?<?php echo $cdnTime; ?>">殺價幣纪录</a></li>
                <li><a href="<?php echo APP_DIR; ?>/history/saja/?<?php echo $cdnTime; ?>">殺價纪录</a></li>
                <li><a href="<?php echo APP_DIR; ?>/history/bid/?<?php echo $cdnTime; ?>">中標纪录</a></li>
                <li><a href="<?php echo APP_DIR; ?>/history/bonus/?<?php echo $cdnTime; ?>">鯊魚點數</a></li>
                <?php /*
					<li><a href="<?php echo APP_DIR; ?>/history/store/?<?php echo $cdnTime; ?>">店點纪录</a></li>
					<li><a href="<?php echo APP_DIR; ?>/history/coupon/?<?php echo $cdnTime; ?>">殺價礼券</a></li>
					*/?>
            </ul>
        </li>
        <!-- li data-role="collapsible" data-iconpos="right" data-inset="false" data-collapsed-icon="search" data-expanded-icon="search">
<h2>S碼纪录</h2>
<ul data-role="listview" data-theme="b">
<li><a href="<?php echo APP_DIR; ?>/scode/?<?php echo $cdnTime; ?>">S碼纪录</a></li>
<li><a href="<?php echo APP_DIR; ?>/scode/accept/?<?php echo $cdnTime; ?>">收取明细</a></li>
<li><a href="<?php echo APP_DIR; ?>/scode/used/?<?php echo $cdnTime; ?>">使用明细</a></li>
</ul>
</li -->
        <li data-role="collapsible" data-iconpos="right" data-inset="false" data-collapsed-icon="search" data-expanded-icon="search">
            <h2>殺價券</h2>
            <ul data-role="listview" data-theme="b">
                <li><a href="<?php echo APP_DIR; ?>/oscode/?<?php echo $cdnTime; ?>">殺價券纪录</a></li>
                <li><a href="<?php echo APP_DIR; ?>/oscode/serial/?<?php echo $cdnTime; ?>">序號激活明细</a></li>
                <li><a href="<?php echo APP_DIR; ?>/oscode/accept/?<?php echo $cdnTime; ?>">收取明细</a></li>
                <li><a href="<?php echo APP_DIR; ?>/oscode/used/?<?php echo $cdnTime; ?>">使用明细</a></li>
            </ul>
        </li>
        <li><a href="<?php echo APP_DIR; ?>/member/order/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-tag ui-btn-icon-right">订单纪录</a></li>
        <li><a class="ui-btn ui-icon-power ui-btn-icon-right" onclick="logout()">退出</a></li>
        <?php } else { ?>          
        <li class="ui-field-contain">
            <input name="lphone" id="lphone" value="" data-clear-btn="true" type="text" placeholder="帳號/手機">
            <input name="lpw" id="lpw" value="" data-clear-btn="true" autocomplete="off" type="password" placeholder="密碼">
            <button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="login()">殺友登录</button>
        </li>
        <?php if (is_weixin()) { ?>
        <li><a href="<?php echo APP_DIR; ?>/user/weixinapi/?<?php echo $cdnTime; ?>" rel="external" class="ui-btn ui-icon-location ui-btn-icon-right">微信登录</a></li>
        <!--<li>
<a href="<?php echo APP_DIR; ?>/user/weixinapi/?<?php echo $cdnTime; ?>" rel="external" class="ui-btn ui-icon-eye ui-btn-icon-right">
<div style="position:relative;"><img src="<?php echo BASE_URL . APP_DIR .'/static/img/wechatlogin1.png'; ?>" style="position:relative; top:3px;"></div></a><?php /*微信登录*/?>
</li>-->
        <?php } else { ?>
        <li><a href="#" onClick="alert('請从殺價王微信公众號進入');" class="ui-btn ui-icon-location ui-btn-icon-right">微信登录</a></li>
        <?php }  ?>
        <!--<li>
<a href="<?php echo APP_DIR; ?>/user/qqapi/?<?php echo $cdnTime; ?>" rel="external" class="ui-btn ui-icon-eye ui-btn-icon-right">
<div style="position:relative;"><img src="http://qzonestyle.gtimg.cn/qzone/vas/opensns/res/img/bt_blue_76X24.png" style="position:relative; top:3px;"></div></a><?php /*QQ登录*/?>
</li>-->
        <li><a href="<?php echo APP_DIR; ?>/user/forget/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-lock ui-btn-icon-right">忘記密碼</a></li>
        <li data-role="list-divider"></li>
        <li><a href="<?php echo APP_DIR; ?>/user/register/?<?php echo $cdnTime; ?>" class="ui-btn ui-icon-edit ui-btn-icon-right">一般註冊</a></li>
        <?php if (is_weixin()) { ?>
        <li><a href="<?php echo APP_DIR; ?>/user/weixinapi/?<?php echo $cdnTime; ?>" rel="external" class="ui-btn ui-icon-location ui-btn-icon-right">微信註冊</a></li>
        <?php } else { ?>
        <li><a href="#" onclick="alert('从殺價王微信公众號註冊');" rel="external" class="ui-btn ui-icon-location ui-btn-icon-right">微信註冊</a></li>
        <!--<li>
<a href="<?php echo APP_DIR; ?>/user/weixinapi/?<?php echo $cdnTime; ?>" rel="external" class="ui-btn ui-icon-eye ui-btn-icon-right">
<div style="position:relative;"><img src="<?php echo BASE_URL . APP_DIR .'/static/img/wechatregister.png'; ?>" style="position:relative; top:3px;"></div></a><?php /*微信註冊*/?>
</li>-->
        <?php } ?>
        <!--<li><a href="<?php echo APP_DIR; ?>/user/qqapi/?<?php echo $cdnTime; ?>" rel="external" class="ui-btn ui-icon-location ui-btn-icon-right">QQ註冊</a></li>-->
        <?php } ?>
        <li data-icon="star"><a href="<?php echo APP_DIR; ?>/?<?php echo $cdnTime; ?>"  <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>回首页</a></li>
    </ul>
</div><!-- /panel -->

</div><!--data-role="page"-->
</body>
</html>
<script>
    // 不是在微信  也不是在appbsl的app裏  才顯示Navigator bar

    $(document).ready(function() {
        if(is_kingkr_obj()) {
            $('#deposititem1').show();
        }else {
            $('#deposititem2').show();
        }
    });
</script>