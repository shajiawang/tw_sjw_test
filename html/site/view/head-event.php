<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");

function is_weixin() {
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
                // error_log("Weixin!");
		return true;
	}
        // error_log("Not Weixin!");
	return false;
}
$header = _v('header');
$style = "";
if ($tpl->page_id == "site_oscode_home" ){
	$style = "background-color:#EFEFEF;"; 
}
?>		
<!DOCTYPE html>
<html prefix="og:http://ogp.me/ns# fb:http://ogp.me/ns/fb#" >
<head>
<?php include_once 'meta.php'; ?>
<title><?php echo $tpl->page_header; ?></title>
<?php include_once 'cssjs.php'; ?>

<style>
.content {
    width:100%;
	height:100%;
    border:0px; 
    margin:0 auto;
}
</style>
<!–[if IE]>
    <script src="<?PHP echo BASE_URL.APP_DIR; ?>/javascript/html5.js"></script>
<![endif]–>
</head>
<body style="height:100%" ontouchstart>

<div class="" >
<!--  class="first-content"開頭，footer結尾  -->
<!--  first-content 有預留footerBar高度，other-content 內頁無footerBar無預留高度 -->
<div class="content" >
