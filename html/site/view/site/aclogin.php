<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') >0 ) {
	$browser = 1;
}else{
	$browser = 2;
}
?>
<!-- 此頁css -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/aclogin.css">

<div class="login-container">
    <div class="login-titlebox">2019 殺價王會閃殺活動</div>
    <figure class="login-pric">
        <!-- <img src="<?PHP echo APP_DIR; ?>/static/img/icon_login.png"> -->
    </figure>
	<div class="login_box2">
    <div class="login-contentbox">
        <div class="login-account d-flex align-items-center">
            <img class="icon" src="<?php echo APP_DIR;?>/static/img/account.png" alt="帳號">
            <input type="text" name="text" id="lphone" class="login-input" autocomplete="off" placeholder="請輸入帳號"/>
        </div>
        <div class="login-password d-flex align-items-center">
            <img class="icon" src="<?php echo APP_DIR;?>/static/img/password.png" alt="密碼"  >
            <input type="password" name="lpw" id="lpw" class="login-input" autocomplete="off" placeholder="請輸入密碼"/>
        </div>
        <p class="login-forgotpassword" onClick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/aforget/?<?php echo $cdnTime; ?>'">忘記密碼?</p>

        <div class="login-btn-login" onClick="sajalogin()">登入</div>
        </div>
        <div class="login-footer">
            <p><span class="login-footer-txt">還不是會員嗎?</span><a href="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/user/aregister/?<?php echo $cdnTime; ?>'">按此註冊</a></p>
        </div>
        
        <div class="login-other">
            <p class="login-login">使用其他登入</p>
        </div>
        <div class="login-kind" >
            <img id="facebook_login" name="facebook_login" onclick="fb_oauth();" src="<?php echo APP_DIR;?>/static/img/facebook.png" alt="fb login">
        
            <img id="line_login" name="line_login_icon" onclick="line_oauth();" src="<?php echo APP_DIR;?>/static/img/line.png" alt="line login">
        </div>			
    </div>
	
	
</div>
<script>
    $(document).ready(function(){
        if(is_kingkr_obj()) {
            $('#login-other').show();
            $('#line_login').show();
        }
    });
   //line 註冊登入
    function line_oauth() {
		window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/r/?_authby=line&_to=/site/shareoscode/one_click_promote_scode_oauth_return_data_process/';
        return;
    }  
   
    function lineLoginResult(r) {
        if((typeof r)=="string") {
            var obj=JSON.parse(r);
            if(obj.retcode=="1") {
                var oauthData = {
                    json:"Y",
                    type:"sso",
                    nickname:obj.displayName,
                    sso_uid:obj.userId,
                    headimgurl:obj.pictureUrl,
                    sso_name:"line",
                    gender:"male",
                    sso_data:r
                };
                $.post(
                    '<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                    oauthData,
                    function(r2) {
                        if((typeof r2)=="string") {
                            var ret=JSON.parse(r2);
                            if(ret.retCode==-16) {
                                // 帳號已註冊 => 跳到會員中心頁
                                window.location.href="<?php echo BASE_URL.APP_DIR;?>/shareoscode/one_click_promote_scode_oauth_return_data_process/";
                            } else if (ret.retCode==1) {
                                // alert("ToDo : 新帳號登入程序 !!");
                                alert("註冊完成 !!");
                                window.location.href="<?php echo BASE_URL.APP_DIR;?>/shareoscode/one_click_promote_scode_oauth_return_data_process/";
                            }
                        }                          
                    }
                );
            }
        }
    }
    //fb 登入 註冊
	function fb_oauth() {
        window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/r/?_to=/site/shareoscode/one_click_promote_scode_oauth_return_data_process/';
		return ;      
	}
   
	function fbLoginResult(r) {
        var obj;
        if((typeof r)=="string") {
            alert(r);
            obj=JSON.parse(r);
        } else if((typeof r)=="object") {
            obj=r;
            alert(JSON.stringify(obj));
        }
        if(obj.retcode=="1") {
            var oauthData = {
                json:"Y",
                type:"sso",
                nickname:obj.name,
                sso_uid:obj.id,
                headimgurl:obj.picture.url,
                sso_name:"fb"
            };
            $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                oauthData,
                function(r2) {
                    var ret;
                    if((typeof r2)=="string") {
                        ret=JSON.parse(r2);
                    } else if((typeof r2)=="object") {
                        ret=r2;
                    }
                    if(ret.retCode==-16) {
                        // 帳號已註冊 => 跳到會員中心頁
                        window.location.href="<?php echo BASE_URL.APP_DIR;?>/shareoscode/one_click_promote_scode_oauth_return_data_process/";
                    } else if (ret.retCode==1) {
                        // alert("ToDo : 新帳號登入程序 !!");
                        alert("註冊完成 !!");
                        window.location.href="<?php echo BASE_URL.APP_DIR;?>/shareoscode/one_click_promote_scode_oauth_return_data_process/";
                    }                         
                }
            );
        }
    }

//	<!-- Facebook JS SDK -->
	
    function checkFBLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }
   
    function statusChangeCallback(response) {
        FB.login(function(response) {
            if (response.status==='connected') {
                // Logged into your app and Facebook.
                FB.api('/me','GET',{"fields" : "id,name,gender,email,picture"}, 
                    function(response) {
                        var oauthData = {
                            json:"Y",
                            type:"sso",
                            nickname:response.name,
                            sso_uid:response.id,
                            headimgurl:response.picture.url,
                            sso_name:"fb",
                            gender:response.gender,
                            sso_data:JSON.stringify(response)
                        };
                        $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                            oauthData,
                            function(r2) {
                                var ret;
                                if((typeof r2)=="string") {
                                    ret = JSON.parse(r2);
                                } else if((typeof r2)=="object") {
                                    ret = r2;
                                }
                                if(ret.retCode==-16) {
                                    // 帳號已註冊 => 跳到會員中心頁 
                                    window.location.href="<?php echo BASE_URL.APP_DIR;?>/shareoscode/one_click_promote_scode_oauth_return_data_process/";                              
                                } else if (ret.retCode==1) {
                                    // alert("ToDo : 新帳號登入程序 !!");
                                    alert("註冊完成 !!");
                                    window.location.href="<?php echo BASE_URL.APP_DIR;?>/shareoscode/one_click_promote_scode_oauth_return_data_process/";       
                                }                       
                            }
                        );
                    }
                );
            } else {
            // The person is not logged into this app or we are unable to tell. 
            }
        });
        return;
    }
    //明暗碼切換
    var $parent = $('login-password'),
        $inpt = $('input[type^="password"]');
    $inpt.each(function(){
        var $that = $(this);
        $that.parent($parent)
            .append(
                $('<div/>')
                .addClass('password-btn d-flex align-items-center')
                .append(
                    $('<img/>')
                    .addClass('eye-icon')
                    .attr('src','<?php echo APP_DIR;?>/static/img/pass_eye-slash.png')
                )
            );

        var $thisEyeBtn = $that.parents($parent).find('.password-btn');
        $thisEyeBtn.removeClass('d-flex').hide();

        //明暗碼切換
        function toggleBtn(who,$thisBtn){
            if(who.val()==''){
                $thisEyeBtn.removeClass('d-flex').hide();
            }else{
                $thisEyeBtn.addClass('d-flex').show();
            }
        }
        //切到明碼
        function passwordEye() {
            $thisEyeBtn.find(".eye-icon")
                .attr('src','<?php echo APP_DIR;?>/static/img/pass_eye.png');
            $inpt.attr("type","text");
            $thisEyeBtn.one("click",passwordEyeSlash);
        }
        //切到暗碼
        function passwordEyeSlash() {
            $thisEyeBtn.find(".eye-icon")
                .attr('src','<?php echo APP_DIR;?>/static/img/pass_eye-slash.png');
            $inpt.attr("type","password");
            $thisEyeBtn.one("click",passwordEye);
        }

        $that.on("keyup blur",function(){
            toggleBtn($(this),$thisEyeBtn);
        })
        $thisEyeBtn.one("click",passwordEye);
    });
    
    //輸入資料時icon變化
    $(function(){
        var $loginAccount = $('.login-account');
        var $loginPassword = $('.login-password');
        
        accountIconChange();
        passwordIconChange();
        
        $loginAccount.find('input').on('keyup change', function(){
            accountIconChange();
        });
        $loginPassword.find('input').on('keyup change', function(){
            passwordIconChange();
        })
    })
    
    //帳號icon變化
    function accountIconChange(){
        var $loginAccount = $('.login-account');
        if($loginAccount.find('input').val() !== null){
            if($loginAccount.find('input').val() !== ''){
                $loginAccount.find('.icon').attr('src','<?php echo APP_DIR;?>/static/img/account_old.png')
            }else{
                $loginAccount.find('.icon').attr('src','<?php echo APP_DIR;?>/static/img/account.png')
            }
        }else{
            $loginAccount.find('.icon').attr('src','<?php echo APP_DIR;?>/static/img/account.png')
        }
    }
    
    //密碼icon變化
    function passwordIconChange(){
        var $loginPassword = $('.login-password');
        if($loginPassword.find('input').val() !== null){
            if($loginPassword.find('input').val() !== ''){
                $loginPassword.find('.icon').attr('src','<?php echo APP_DIR;?>/static/img/password_old.png')
            }else{
                $loginPassword.find('.icon').attr('src','<?php echo APP_DIR;?>/static/img/password.png')
            }
        }else{
            $loginPassword.find('.icon').attr('src','<?php echo APP_DIR;?>/static/img/password.png')
        }
    }
</script>
