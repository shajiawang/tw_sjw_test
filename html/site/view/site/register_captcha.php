<?php
$cdnTime = date("YmdHis");
$json = _v('json');
$phone=_v('phone');
?>
<p>
<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo APP_DIR;?>/static/css/keyboard.min.css">

<style>
    .es-content {
        background: #FFF;
    }
</style>
<form method="post" action="" id="contactform" name="contactform">
    <div class="login-title login-captcha-title">
	    <h1>輸入驗證碼</h1>
	    <div class="small">
            <span>驗證碼已發送至</span><span class="telNum"><?php echo $phone; ?></span>
        </div>
	</div>
    <div class="captcha-box captcha_keyboard">
        <ul class="num-box d-flex justify-content-center">
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
            <li class="d-flex justify-content-center align-items-center"><span></span></li>
        </ul>
        <input type="hidden" class="input-field numHideBox" id="captcha" name="captcha" value="">
    </div>
    
    <!--  驗證訊息  -->
    <ul class="captcha-msgError"></ul>
    
    <div class="resend-box">
        <div class="resend-text">
        </div>
    </div>
    
    <!--  彈窗-驗證訊息  -->
    <div class="errorModal">
        <div class="modal-content">
            <div class="modal-body">
                <ul></ul>
            </div>
            <div class="modal-footer">
                <a class="close_btn">確認</a>
            </div>
        </div>
    </div>
    <div class="errorModalBg"></div>
</form>	
<!-- ============================= JS 控制 ============================= -->

<!--  CDN Bootstrap 4.1.1  -->
    <script src="<?php echo APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--  鍵盤-自訂義JS  -->
    <script src="<?php echo APP_DIR;?>/static/js/_captcha_keyboard.js"></script>

<script type="text/javascript">
	//判斷使用者裝置 
    var $device;        //android, ios, other
	(function whDevice() {
        //判斷使用者裝置是否支援觸控功能，判斷是否為行動版 (有觸控功能的筆電也判斷為行動版)
        function isMobile() {
            try{ document.createEvent("TouchEvent"); return true; }
            catch(e){ return false;}
        }
        
        //判斷使用者裝置為Android或iOS
        function isDevice() {
            var u = navigator.userAgent;
            var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android終端
            var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios終端
            if(isAndroid){
                return 'Android';
            }else if(isiOS){
                return 'iOS';
            }
        }
        
        if(isMobile()){
            var $mobile = isDevice();
            switch($mobile){
                case 'Android':
                    $device = 'android';
                    break;
                case 'iOS':
                    $device = 'ios';
                    break;
            }
        }else{
            $device = 'other';
        }
		
    }());
	
    function toAndroid() {
        location.href='https://play.google.com/store/apps/details?id=tw.com.saja.userapp';
    }
	function toiOS() {
		location.href='https://itunes.apple.com/tw/app/id1441402090';
    }
	function createMsgModal(){
        console.log(1);
		if($device == 'android'){
			toAndroid();
		}else if($device == 'ios'){
			toiOS();
		}else{
			$('body').addClass('modal-open');
			$('body').append(
				$('<div class="downloadModalBox d-flex justify-content-center align-items-center"/>')
				.append(
				$('<div class="modal-content"/>')
					.append(
					$('<div class="modal-body"/>')
						.append(
							$('<div class="qrcodebox"><img src="https://www.saja.com.tw/site/phpqrcode/?data=https://www.saja.com.tw/site/mall/qrpage" ></div>')
						)
						.append(
							$('<div class="modal-title"/>')
							.append(
								$('<p>為了安全起見</p>')
							)
							.append(
								$('<p>本功能僅能在APP使用</p>')
							)
							.append(
								$('<p>請用手機掃描QRcode</p>')
							)
						)
					)
				)
			)
		}
    }
</script>
    
<!--  倒數重發驗證碼  -->
<script>
    $(function(){
        var $wait = 99,
            $resend = $(".resend-box .resend-text");
        
        function createBtn() {
            $resend.html('');
            $resendText = '<a class="resend-btn" href="">重新獲取驗證碼</a>';
            $resend.append($resendText);
            $wait = 99;
            $(".resend-btn").on("click",function(e){
                getSms();
				time($(this));
                e.preventDefault();
            })
        }
        function createText() {
            $resend.html('');
            $resendText = '<span class="time">'+$wait+'</span><span>秒後可重新獲取驗證碼</span>';
            $resend.append($resendText);
            $wait--;
        }
        function time(resend) { 
            if ($wait == 0) {
                createBtn();
            } else {
                createText();
                setTimeout(function() {
                    time(resend)
                },1000) 
            } 
        }
        createBtn();
    })
</script>

<!--  驗證訊息  -->
<script type="text/javascript">
    var array;
    function setError(msg){
        array.push('<li>'+msg+'</li>');
    };
    
	//送出表單	
	function getSms(){
		var ophone = '<?php echo $phone; ?>';
		$.post(
			'<?php echo BASE_URL.APP_DIR;?>/ajax/auth.php',
			{phone:ophone,type:"verify_phone"}, 
			function(r2) {
				if((typeof r2)=="string") { 
					var ret=JSON.parse(r2);
					if(ret.status == 200) {
						alert('手機驗證碼發送成功');
					} 
					else if(json.status==1){ alert('殺友請先登入 !'); }
					else if(json.status==2){ alert('手機號碼不能空白 !'); } 
					else if(json.status==3){ alert('手機號碼不正確'); } 
					else if(json.status==4){ alert('手機號碼已驗證'); }
					else if(json.status==9){ alert('手機號碼必需為數字'); }
					else if (json.status < 1) {
						if(json.status==-2)
						  alert('此手機號碼已被使用 !!');
						else
						  alert('手機簡訊發送失敗 !!');
					}
				}
			}
		);	
	}
	
    //驗證錯誤訊息彈窗
    var $ErrorModal = $(".errorModal .modal-content"),
        $ModalBg = $(".errorModalBg"),
        $ErrorBtn = $(".close_btn");
    function openErrorModal() {
        $ErrorModal.show();
        $ModalBg.show();
    }
    function closeErrorModal() {
        $ErrorModal.hide();
        $ModalBg.hide();
    }
    $ErrorBtn.on("click",function(){
        closeErrorModal();
    })
    
</script>

<!--  小鍵盤動作  -->
<script>
    var $wherekey,
        $open,
        $thisBtn,
        $much = $(".captcha_keyboard li").length;
    $(".captcha_keyboard").each(function(i,e){
        $(this)
            .attr("data-toggle","modal")
            .attr("data-target","#captcha_numkeyModal");
        $(this).on("click",function(e){
            //生成按鈕
            if(!$("#captcha_numkeyModal").length > 0){
                keyModal();
            }else{
                $('#captcha_numkeyModal').remove();
            }
            var $domEle=$("#passport");

            //先儲存點選哪個輸入框
            $wherekey = $(".numHideBox");
            $open = $(this).data("target");

            e.stopPropagation;
            e.preventDefault;

            //將原先輸入框裡的值帶入鍵盤的暫存區
            if ($wherekey.val()!=''){
                $domEle.val(String($wherekey.val()));
            }else{
                $domEle.val('');
            }

            //計算
            _captcha_Calculation($wherekey,$much);
            
            //Event-關閉小鍵盤後的動作
            $('#captcha_numkeyModal').on('hide.bs.modal', function (e) {
                $('#captcha_numkeyModal').remove();
                $(".num-box li").find(".pic").remove();
                
                //驗證
                var $tabNum = String($wherekey.val()).length,
                    $error = $(".captcha-msgError"),
                    $errorBox = $(".errorModal .modal-body ul");
                
                array = [];
                
                //判斷是否填滿所有格數
                if($tabNum == $much){
                    if(array.length !== 0){
                        $error.html(array.join(''));
                        $errorBox.html(array.join(''));
                        e.preventDefault;
                        openErrorModal();
                    }else{
                        $error.html('');
						var osmscode = $('#captcha').val();
                        $.ajax({
							url: "<?php echo BASE_URL.APP_DIR;?>/ajax/auth.php",
                            data: {phone:"<?php echo $phone;?>",type:"check_sms",smscode:osmscode},
                            type:"POST",
                            dataType:'JSON',
    
                            success: function(msg){
                                console.log(msg);
								var obj ;
                                if((typeof msg)=="string") {
                                    obj = JSON.parse(trim(msg));
                                } else if((typeof msg)=="object") {
                                    obj = msg;
                                }
							   
                                if(obj.status == 200) {	msg = obj.retMsg; /*'手機驗證成功'*/ } 
								else if(obj.status==2){ msg = '手機號碼不正確 !'; } 
								else if(obj.status==3){ msg = '驗證碼不正確'; } 
								else if(obj.status==4){ msg = '身份驗證失敗'; }
								
								setError(msg);
								$error.css("display","block").html(array.join(''));	
								if (obj.status == 200){
									//document.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";
									alert(obj.retMsg);
									createMsgModal();
								}								
                            },
                            error:function(){
                                var obj ;
                                if((typeof msg)=="string") {
                                   obj = JSON.parse(trim(msg));
                                } else if((typeof msg)=="object") {
                                   obj = msg;
                                }
                                setError("身份驗證失敗");
                                $error.html(array.join(''));
                                $errorBox.html(array.join(''));
                                openErrorModal();
                            }
                        });
                    }
                }else{
                    setError("請填寫完整驗證碼");
                    $error.html(array.join(''));
                }
            })
        });
    })
    //防止小鍵盤沒吊起，每600毫秒判斷未吊起時重新點擊開啟
    var interval = null;
    $(function(){
        interval = setInterval(Continuous,600);
    })
    //持續判斷小鍵盤
    function Continuous() {
        var $modal = $("#captcha_numkeyModal");
        if($modal.hasClass("show")){
            //已吊起，要移除持續判斷
            clearInterval(interval);
        }else{
            $(".captcha_keyboard").click();
        }
    }
</script>

