<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");

$browser = browserType();

$_to=_v('_to');
$_params=_v('_params');
if(empty($_to))
   $_to="_to=".APP_DIR."/member/";
error_log("[v/site/login] _to : ".$_to);
?>
<div class="login-container">
	<div class="login-title">
	    <h1>歡迎登入殺價王</h1>
	</div>
	<ul class="es-login-kind">
	    <li id="line_login" name="line_login_icon" onclick="line_oauth();" class="item item-line">
	        <div class="icon"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/login-icon/Line.png" alt=""></div>
	        <div class="title">使用 Line 登入</div>
        </li>
	    <li id="facebook_login" name="facebook_login_icon" onclick="fb_oauth();" class="item item-facebook" >
	        <div class="icon"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/login-icon/FB.png" alt=""></div>
	        <div class="title">使用 Facebook 登入</div>
	    </li>
        <?php 
           if($browser=="weixin") {  
        ?>
            <li id="twitter_login" name="wechat_login_icon"  onclick="wx_oauth();" class="item item-wechat" >
	        <div class="icon"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/login-icon/wechat.png" alt=""></div>
	        <div class="title">使用 Wechat 登入</div>
	         </li>
        <?php   
            }
        ?>
	    <li id="twitter_login" name="twitter_login_icon" class="item item-twitter" style="display:none;">
	        <div class="icon"><img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/login-icon/twitter.png" alt=""></div>
	        <div class="title">使用 Twitter 登入</div>
	    </li>
    </ul>
</div>
<script>
    window.fbAsyncInit = function() {
        FB.init({
            appId      : '202083143845729',
            cookie     : true,
            xfbml      : true,
            version    : 'v3.1'
        });
        FB.AppEvents.logPageView();
    };

    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));

    $(document).ready(function(){
        if(is_kingkr_obj()) {
            $('#login-other').show();
            $('#line_login').show();
            $('#fb_login').show();
        }
    });
   
    function wx_oauth() {
        window.location.href="<?php echo BASE_URL.APP_DIR; ?>/oauthapi/r/?_authby=weixin&<?php echo $_to; ?>";
        return;
    }
   
    function line_oauth() {
        window.location.href="<?php echo BASE_URL.APP_DIR; ?>/oauthapi/r/?_authby=line&<?php echo $_to; ?>";
        // window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/use_line_login/';
        return;
    }
   
    function fb_oauth() {
        window.location.href="<?php echo BASE_URL.APP_DIR; ?>/oauthapi/r/?_authby=fb&<?php echo $_to; ?>";
        return;
    }
   
    function lineLoginResult(r) {
        if((typeof r)=="string") {
            var obj=JSON.parse(r);
            if(obj.retcode=="1") {
                var oauthData = {
                    json:"Y",
                    type:"sso",
                    nickname:obj.displayName,
                    sso_uid:obj.userId,
                    headimgurl:obj.pictureUrl,
                    sso_name:"line",
                    gender:"male",
                    sso_data:r};
                $.post(
                    '<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                    oauthData,
                    function(r2) {
                        if((typeof r2)=="string") {
                            var ret=JSON.parse(r2);
                            if(ret.retCode==-16) {
                                // 帳號已註冊 => 跳到會員中心頁
                                window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";
                            } else if (ret.retCode==1) {
                                // alert("註冊完成 !!");
                                window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";
                            }
                        }
                    }
                );
            } 
        }
    }
   
    function fbLoginResult(r) {
        var obj;
        if((typeof r)=="string") {
            alert(r);
            obj=JSON.parse(r);
        } else if((typeof r)=="object") {
            obj=r;
            alert(JSON.stringify(obj));
        }
        if(obj.retcode=="1") {
            var oauthData = {
                json:"Y",
                type:"sso",
                nickname:obj.name,
                sso_uid:obj.id,
                headimgurl:obj.picture.url,
                sso_name:"fb"
            };
            $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                oauthData,
                function(r2) {
                    var ret;
                    if((typeof r2)=="string") {g
                        ret=JSON.parse(r2);
                    } else if((typeof r2)=="object") {
                        ret=r2;
                    }
                    if(ret.retCode==-16) {
                        // 帳號已註冊 => 跳到會員中心頁 
                        window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";                              
                    } else if (ret.retCode==1) {
                        // alert("註冊完成 !!");
                        window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";
                    }                         
                }
            );
        }    
   }


//Facebook JS SDK
    function checkFBLoginState() {
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });
    }
   
    function statusChangeCallback(response) {
        FB.login(function(response) {
            if (response.status==='connected') {
                // Logged into your app and Facebook.
                FB.api('/me','GET',{"fields" : "id,name,gender,email,picture"}, 
                    function(response) {
                        var oauthData = {
                            json:"Y",
                            type:"sso",
                            nickname:response.name,
                            sso_uid:response.id,
                            headimgurl:response.picture.url,
                            sso_name:"fb",
                            gender:response.gender,
                            sso_data:JSON.stringify(response)
                        };
                        $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                            oauthData,
                            function(r2) {
                                var ret;
                                if((typeof r2)=="string") {
                                    ret = JSON.parse(r2);
                                } else if((typeof r2)=="object") {
                                    ret = r2;
                                }
                                if(ret.retCode==-16) {
                                    // 帳號已註冊 => 跳到會員中心頁 
                                    window.location.href='<?php echo BASE_URL.$gotourl; ?>';                              
                               
                                } else if (ret.retCode==1) {
                                    // alert("註冊完成 !!");       
                                    window.location.href='<?php echo BASE_URL.$gotourl; ?>'; 
                                }                       
                            }
                        );
                    }
                );
            }
       });
       return;
    }
</script>