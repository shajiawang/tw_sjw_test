<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') >0 ) {
	$browser = 1;
}else{
	$browser = 2;
}
?>
<div class="login-container">

<!-- 舊登入頁 -->

	<div class="login-logo">
		<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/login-logo-2.png" alt="殺價王">
	</div>
	<div class="login-content">
		<div class="login-account d-flex align-items-center">
			<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/account.png" alt="帳號">
<!--			<input type="number" name="lphone" id="lphone" class="login-input"  pattern="[0-9]*" placeholder="請輸入帳號" focus/>-->
			<input type="text" name="lphone" id="lphone" class="login-input" placeholder="請輸入帳號" focus/>
		</div>
		<div class="login-password d-flex align-items-center">
			<img src="<?php echo BASE_URL.APP_DIR;?>/static/img/password.png" alt="密碼"  >
			<input type="password" name="lpw" id="lpw" class="login-input" placeholder="請輸入密碼" focus/>
		</div>
		<div class="login-btn-login" onClick="sajalogin()">登入</div>
		<p class="login-forgotpassword" onClick="javascript:location.href='<?php echo APP_DIR; ?>/user/forget/?<?php echo $cdnTime; ?>'">忘記密碼?</p>
	</div>
	<div class="login-other" style="display:none">
		<p class="login-login">或由以下方式註冊/登入</p>
	</div>
	<div class="login-kind" >
		<img id="facebook_login" name="facebook_login" onclick="fb_oauth();" src="<?php echo BASE_URL.APP_DIR;?>/static/img/facebook.png" alt="fb login">
	
		<img id="line_login" name="line_login_icon" onclick="line_oauth();" src="<?php echo BASE_URL.APP_DIR;?>/static/img/line.png" alt="line login">
    </div>			
	<div class="login-footer">
		<p><span class="login-footer-txt">還不是會員嗎?</span><a href="javascript:location.href='<?php echo APP_DIR; ?>/user/register/?<?php echo $cdnTime; ?>'">按此註冊</a></p>
	</div>
	
	
	

    
    
</div>
<script>
/*
    window.fbAsyncInit = function() {
    FB.init({
      appId      : '202083143845729',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.0'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   */
   $(document).ready(function(){
      if(is_kingkr_obj()) {
          $('#login-other').show(); 
          $('#line_login').show();
      } 
       
   });
   
   function line_oauth() {
         window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/r/?_to=/site/member/';
         return;
         /*
         if(is_kingkr_obj()) {
             //login('LINE','','lineLoginResult');
			  window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/use_line_login/';
              return; 
         } else {
              // alert("瀏覽器中無法啟用Line登入 !!");
			  window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/use_line_login/';
              return; 
         } 
         */         
   }  
   
   function lineLoginResult(r) {
         // alert(r); 
         // alert((typeof r));         
         if((typeof r)=="string") {
             var obj=JSON.parse(r);
             if(obj.retcode=="1") {
                var oauthData = {
                                 json:"Y",
                                 type:"sso",
                                 nickname:obj.displayName,
                                 sso_uid:obj.userId,
                                 headimgurl:obj.pictureUrl,
                                 sso_name:"line",
                                 gender:"male",
                                 sso_data:r};
                 $.post(
                    '<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                    oauthData,
                    function(r2) {
                          if((typeof r2)=="string") { 
                             // alert(r2);
                             var ret=JSON.parse(r2);
                             if(ret.retCode==-16) {
                                  // 帳號已註冊 => 跳到會員中心頁 
                                  window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";                              
                             } else if (ret.retCode==1) {
                                  // alert("ToDo : 新帳號登入程序 !!");
                                  alert("註冊完成 !!");
                                  window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";       
                             }
                          }                          
                    }
                );
             }    
         } 
         
   }
   
	function fb_oauth() {
        window.location.href='<?php echo BASE_URL.APP_DIR;?>/oauthapi/r/?_to=/site/member/';
		return ;
        /*
        if(is_kingkr_obj()) {
			login('FB','','fbLoginResult');
		} else {
			checkFBLoginState();
			return; 
		} */        
	}
   
	function fbLoginResult(r) {
         var obj;
         if((typeof r)=="string") {
             alert(r);
             obj=JSON.parse(r);
         } else if((typeof r)=="object") {
             obj=r;
             alert(JSON.stringify(obj));
         }
         // alert("000000");
         if(obj.retcode=="1") {
            // alert("11111");
            var oauthData = {
                     json:"Y",
                     type:"sso",
                     nickname:obj.name,
                     sso_uid:obj.id,
                     headimgurl:obj.picture.url,
                     sso_name:"fb"                    
                };
             // sso_data:JSON.stringify(obj)
             // alert("22222");
             $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                oauthData,
                function(r2) {
                     // alert("33333");
                     var ret;
                      if((typeof r2)=="string") {
                          // alert("44444");
                          ret=JSON.parse(r2);
                      } else if((typeof r2)=="object") {
                          // alert("55555");
                          ret=r2;
                      }                      
                      // alert(r2);
                      if(ret.retCode==-16) {
                          // 帳號已註冊 => 跳到會員中心頁 
                          window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";                              
                      } else if (ret.retCode==1) {
                          // alert("ToDo : 新帳號登入程序 !!");
                          alert("註冊完成 !!");
                          window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";       
                      }                         
                }
            );
         }    
   }


	<!-- Facebook JS SDK -->
	
   function checkFBLoginState() {
      // console.log("rrr");
      FB.getLoginStatus(function(response) {
          statusChangeCallback(response);
      });
   }
   
   function statusChangeCallback(response) {
       FB.login(function(response) {
          if (response.status==='connected') {
                // Logged into your app and Facebook.
                FB.api('/me','GET',{"fields" : "id,name,gender,email,picture"}, 
                    function(response) {
                        var oauthData = {
                                 json:"Y",
                                 type:"sso",
                                 nickname:response.name,
                                 sso_uid:response.id,
                                 headimgurl:response.picture.url,
                                 sso_name:"fb",
                                 gender:response.gender,
                                 sso_data:JSON.stringify(response)
                        };
                        $.post('<?php echo BASE_URL.APP_DIR;?>/ajax/user_register.php',
                            oauthData,
                            function(r2) {
                                var ret;
                                if((typeof r2)=="string") { 
                                     ret = JSON.parse(r2);
                                } else if((typeof r2)=="object") {
                                     ret = r2;
                                }
                                if(ret.retCode==-16) {
                                    // 帳號已註冊 => 跳到會員中心頁 
                                    window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";                              
                                } else if (ret.retCode==1) {
                                    // alert("ToDo : 新帳號登入程序 !!");
                                    alert("註冊完成 !!");
                                    window.location.href="<?php echo BASE_URL.APP_DIR;?>/member/";       
                                }                       
                            }
                        );
                    }
                );
          } else {
            // The person is not logged into this app or we are unable to tell. 
          }
       });
       return;
   }
    
        //明暗碼切換
        var $parent = $('login-password'),
            $inpt = $('input[type^="password"]');
        $inpt.each(function(){
            var $that = $(this);
            $that.parent($parent)
                .append(
                    $('<div/>')
                    .addClass('password-btn d-flex align-items-center')
                    .append(
                        $('<i/>')
                        .addClass('esfas ycolor eye-slash')
                    )
                );
            
            var $thisEyeBtn = $that.parents($parent).find('.password-btn');
            $thisEyeBtn.removeClass('d-flex').hide();
            
            //明暗碼切換
            function toggleBtn(who,$thisBtn){
                if(who.val()==''){
                    $thisEyeBtn.removeClass('d-flex').hide();
                }else{
                    $thisEyeBtn.addClass('d-flex').show();
                }
            }
            //切到明碼
            function passwordEye() {
                $thisEyeBtn.find("i")
                    .removeClass('eye-slash')
                    .addClass('eye');
                $inpt.attr("type","text");
                $thisEyeBtn.one("click",passwordEyeSlash);
            }
            //切到暗碼
            function passwordEyeSlash() {
                $thisEyeBtn.find("i")
                    .removeClass('eye')
                    .addClass('eye-slash');
                $inpt.attr("type","password");
                $thisEyeBtn.one("click",passwordEye);
            }
            
            $that.on("keyup blur",function(){
                toggleBtn($(this),$thisEyeBtn);
            })
            $thisEyeBtn.one("click",passwordEye);
        });
</script>
