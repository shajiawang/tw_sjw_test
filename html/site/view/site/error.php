		<div class="content text-center ErrorPage">
			<h1>404 Error</h1>
            <small><p>Sorry, we couldn't find that page.</p><p>Please check the URL or try again later.</p></small>
            <div class="back-btn"><a href="<?php echo BASE_URL.APP_DIR;?>">回到首頁</a></div>
		</div>
	