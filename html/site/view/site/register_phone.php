<style>
    .es-content {
        background: #FFF;
    }
</style>
   
    <div class="login-title login-tele-title">
	    <h1>手機驗證</h1>
	</div>
    <div class="phone-box">
        <div class="phone-icon">
            <div class="iconBox">
                <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/register-phone1.png" alt="">
            </div>
            <small>輸入手機號碼獲取驗證碼</small>
        </div>
		<form method="post" action="" id="contactform" name="contactform">
            <input type="tel" class="input-field phone-input" id="phone" name="phone" maxlength="10" value="">
			<input type="hidden" id="userid" name="userid" value="<?php echo $_SESSION['auth_id']; ?>">
            <input type="hidden" id="user_type" name="user_type" value="P">
        </form>
    </div>
    <a class="send-btn disabled" onclick="getSms();" >獲取簡訊驗證碼</a>

<!-- ============================= JS 控制 ============================= -->

<!--  CDN Bootstrap 4.1.1  -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.bundle.min.js"></script>

<!--  手機號碼加空格  -->
<script>
    var $phoneInput = $('.phone-input');
    var $phoneIcon = $('.phone-box .phone-icon');
    
    //加空白
    function addBlank(){
        var Value = $phoneInput.val().replace(/\D/g, '').replace(/(.{4})(.{3})(.{3})/g, "$1 $2 $3"); //替換空格前4位數字為4位數字加空格
        $phoneInput.val(Value);
    }
    
    //計算去掉空白的字串長度,來判斷按鈕的功能是否作用
    function cutBlank(){
        var $cut = $phoneInput.val().replace(/\s/g, '');
        var $cutLength = $cut.length;
        if ($cutLength == 10) {
            $(".send-btn").removeClass("disabled");
			checkPhone();
        }else{
            $(".send-btn").addClass("disabled");
        }
    }
    var open = "<?php echo APP_DIR;?>/static/img/register-phone1.png";
    var close = "<?php echo APP_DIR;?>/static/img/register-phone2.png"
    function addUp() {
        $phoneIcon
        .addClass("up")
        .find(".iconBox img").attr("src",close)
        .end()
        .find("small").hide();
    }
    function removeUp() {
        $phoneIcon
        .removeClass("up")
        .find(".iconBox img").attr("src",open)
        .end()
        .find("small").show();
    }
    $phoneInput.on("keyup change",function(e){
        cutBlank();
        addUp();
    });
    $phoneInput.on("click focus",function(e){
        addUp();
    });
    $phoneInput.on("blur",function(e){
        if ($phoneInput.val()=='') {
            removeUp();
        }else {
            addUp();
        }
    });
    
    //預設移除UP
    removeUp();

	function checkPhone() {		
        var $cphone = $phoneInput.val().replace(/\s/g, '');
        var $cutLength = $cphone.length;
		var ok=true;
		var ophone=$cphone;
		
		if(ophone == "") {
			alert('手機號碼不能為空白 !!');
			ok=false;
		} else if(isDigit(ophone)==false) {
			alert('手機號碼必需為數字 !'); 
			ok=false;
		}
    }
	
	//送出表單	
	function getSms(){
        var $cphone = $phoneInput.val().replace(/\s/g, '');
		var ophone = $cphone;
		$.post(
			'<?php echo BASE_URL.APP_DIR;?>/ajax/auth.php',
			{phone:ophone,type:"verify_phone"}, 
			function(r2) {
				if((typeof r2)=="string") { 
					var ret=JSON.parse(r2);
					if(ret.status == 200) {
						alert('手機驗證碼發送成功');
						window.location.href="<?php echo BASE_URL.APP_DIR;?>/verified/register_captcha/?phone="+ophone;
					} 
					else if(ret.status==1){ alert('殺友請先登入 !'); }
					else if(ret.status==2){ alert('手機號碼不能空白 !'); } 
					else if(ret.status==3){ alert('手機號碼不正確'); } 
					else if(ret.status==4){ alert('手機號碼已驗證'); }
					else if(ret.status==9){ alert('手機號碼必需為數字'); }
					else if (ret.status < 1) {
						if(ret.status==-2)
						  alert('此手機號碼已被使用 !!');
						else
						  alert('手機簡訊發送失敗 !!');
					}
				}
			}
		);
	}
    
</script>
    
<!--  驗證訊息  -->
<script type="text/javascript">
    var array;
    function setError(msg){
        array.push('<li>'+msg+'</li>');
    };
    //驗證錯誤訊息彈窗
    var $ErrorModal = $(".errorModal .modal-content"),
        $ModalBg = $(".errorModalBg"),
        $ErrorBtn = $(".close_btn");
    function openErrorModal() {
        $ErrorModal.show();
        $ModalBg.show();
    }
    function closeErrorModal() {
        $ErrorModal.hide();
        $ModalBg.hide();
    }
    $ErrorBtn.on("click",function(){
        closeErrorModal();
    })
</script>




