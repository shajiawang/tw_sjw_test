<?php 
$status = _v('status');
$product_list = _v('today_product'); 
?>
<script> $(window).stopTime('losttime');</script>

<div id="container">
    <div class="sajalist-wrapper d-flex flex-wrap">
        <?php 
        if(!empty($product_list) ) {
            foreach($product_list as $rk => $rv){ 
        ?>
        <div class="sajalist-item align-items-center justify-content-center">
            <a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR;?>/product/saja/?<?php echo $status['status']['args'];?>&productid=<?php echo $rv['productid']; ?>&type=<?php echo $_GET['type']; ?>&t=<?php echo $cdnTime; ?>'" >
                <div class="sajalist-img d-flex flex-column">
                    <i>
                        <?php if(!empty($rv['thumbnail'])){ ?>
                            <img <?php echo $flash_img; ?> class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif" data-src="<?php echo $status['path_image'] .'/product/'. $rv['thumbnail']; ?>">
                        <?php }elseif (!empty($rv['thumbnail_url'])){ ?>
                            <img <?php echo $flash_img; ?> class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif" data-src="<?php echo $rv['thumbnail_url']; ?>">
                        <?php }else{ ?>
                            <img <?php echo $flash_img; ?> class="lazyload" src="<?PHP echo APP_DIR; ?>/static/img/loading.gif" data-src="http://howard.sajar.com.tw/site/images/site/product/1ee1b3f296827f55d52783119f036316.jpg">
                        <?php }//endif; ?>
                    </i>
                    <p class="sajalist-label"><?php echo $rv['name']; ?></p>
                    <div class="sajalist-overtime mt-auto">
                        <span id="ot-<?php echo $rv['productid']; ?>" class="overtime" data-offtime="<?php echo $rv['unix_offtime']; ?>" data-tag="">0天 00:00:00</span>
                    </div>
                </div>
            </a>
        </div>
		<script type="text/javascript">
            var interval=59;
            window.setInterval(function () { ShowTimer(<?php echo $rv['unix_offtime'] ?>, '', 'ot-<?php echo $rv['productid']; ?>'); }, interval);
        </script>
        <?php } }else{//endforeach; ?>
            <div class="nohistory-box">
                <div class="nohistory-img">
                    <img class="img-fluid" src="<?PHP echo APP_DIR; ?>/static/img/nohistory-img.png" alt="">
                </div>
                <div class="nohistory-txt">本日無必殺商品</div>
            </div>
        <?php }?>

    </div>
</div>

<script type="text/javascript">

    //倒數計時
    function ShowTimer(otime,ntime,divId) {
        var now = new Date();
        //getTime()獲取的值/1000=秒數
        var leftTime=(otime*1000) - now.getTime();
        var leftSecond=parseInt(leftTime/1000);
		if (leftTime > 0) {
			var day=Math.floor(leftSecond/(60*60*24));
			var hour=Math.floor((leftSecond-day*24*60*60)/3600);
			if (hour < 10) {
				hour = "0" + hour;
			};
			var minute=Math.floor((leftSecond - day * 24 * 60 * 60 - hour * 3600) / 60);
			if (minute < 10) {
				minute = "0" + minute;
			};
			var second = Math.floor(leftSecond - day * 24 * 60 * 60 - hour * 3600 - minute * 60);
			if (second < 10) {
				second = "0" + second;
			};  
			var msecond = Math.floor(leftTime % 100);	
			if (msecond < 10) {
				msecond = "0" + msecond;
			};  
			var htmlElement=document.getElementById(divId);
			if (day == "00") {
				$('.'+divId).html(hour + ":" + minute + ":" + second + ":" + msecond);
				htmlElement.innerHTML = "" + hour + ":" + minute + ":" + second + ":"+ msecond;
			} else {
				htmlElement.innerHTML = "" + day + " 天 "  + hour + ":" + minute + ":" + second + ":"+ msecond;				
			}
		} else if (leftTime < 0) {
            var htmlElement=document.getElementById(divId);
			htmlElement.innerHTML = "已結標";
			window.clearInterval('intervalId'+divId);
						
            var $location = <?php echo json_encode(BASE_URL.APP_DIR) ?>;
            var $cdnTime = <?php echo json_encode($cdnTime) ?>;
            var $that = $('#'+divId),
                $alert = 'alert("此商品已結標")',
                $href = 'location.href="'+$location+'/product/?'+$cdnTime+'"';
            $that.parents(".sajalist-item").find("a").attr('onclick',$alert+','+$href);	
		}
    }
    
    $(function(){
        /*懶載入初始化*/
        $(".sajalist-img .lazyload").lazyload({
            threshold : 50                     //提前加載(像素)
        });
    })
    
    //懶加載漏網之魚
    $(window).on('load',function(){
        //loader漏網之魚
        var interval = null;
        var options = {
            dom:{
                item: '.sajalist-item',
                img: '.lazyload'
            }
        }
       
        function $reload() {
            var $winH = $(window).height();                                 //螢幕高度
            var $winPos = $(window).scrollTop();                            //目前捲軸位置
            var $item = $(options.dom.item);
            var $img = $item.find(options.dom.img);                         //抓取所有lazyload img
            //讓作用中頁面的圖片都跑一次迴圈，判斷是否加載
            $img.each(function(){
                var $that = $(this);
                var $imgPos = $(this).offset().top;                                                     //圖片位置
                if($imgPos > $winPos && $imgPos < $winPos + $winH){
                    var $showSrc = $that.attr('src');                                                   //loading圖片
                    var $showDsrc = $that.attr('data-src');                                             //產品圖片
                    //判斷兩者不相同時 表示未載入,這張圖 重新lazyload
                    if($showSrc !== $showDsrc){
                        $(this).lazyload();
                    }
                }
                
            })
        }   
        //判斷loader漏網之魚，2秒判斷一次
        //reloadimg = setInterval($reload,2000);
        //因為只有單頁，改為 滾動時判斷
        $(window).on('scroll', function(){
            $reload();
        })
    })
    
</script>