<?php 
$product = _v('product'); 
$status = _v('status');
//閃殺商品
$flash_img = ($product['is_flash']=='Y') ? 'class="flash_img"' : '';
?>
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                    <div id="tagBox">
                        
						<div id="tagBoxPlus">
							<?php if(!empty($product['bonus'])){ ?>
							<div id="tagBox_bonus" title="鯊魚點回馈%"><?php echo $product['bonus']; ?></div>
							<?php }//endif; ?>
							
							<?php /* if(!empty($product['gift'])){ ?>
							<div id="tagBox_store" title="店點回馈%"><?php echo $product['gift']; ?></div>
							<?php }//endif; */?>
							
							<?php if (!empty($product['srid']) ){
							if($product['srid']=='any_saja'){
							}elseif($product['srid']=='never_win_saja'){ ?>
								<div id="tagBox_qualifications" title="新手限定(未中標过者)" >限新手</div>
							<?php } elseif($product['srid']=='le_win_saja'){ ?>
								<div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以下)" >限新手</div>
							<?php } elseif($product['srid']=='ge_win_saja'){ ?>
								<div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以上)" >限老手</div>
							<?php 
							} }//endif; ?>
							
							<?php /*if ($product['loc_type'] == 'G'){ ?>
							<div id="tagBox_area" title="區域">全国</div>
							<?php }//endif;*/ ?>
							
							<?php if ((float)$product['saja_fee'] == 0.00){ ?>
								<div id="tagBox_free" title="免費(出價免扣殺價幣)">免費</div>
							<?php }//endif; ?>
						</div>
						
						<?php 
						if(!empty($product['thumbnail'])) { //$status['path_image'] 
							$img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail'];
						} elseif (!empty($product['thumbnail_url'])) {
							$img_src = $product['thumbnail_url']; 
						} else { 
							$img_src = "http://cn.sajar.com.tw/site/images/site/product/1ee1b3f296827f55d52783119f036316.jpg";
						}?>
						
						<img <?php echo $flash_img; ?> src="<?php echo $img_src; ?>" >
                    </div>
					
					<h2 style="white-space:pre-wrap;"><?php echo $product['name']; ?></h2>
					<?php if (!empty($product['offtime'])){ ?>
					<p class="countdown" data-offtime="<?php echo $product['offtime']; ?>" data-tag="">00天00时00分00秒</p>
					<?php }//endif; ?>
					<h2 style="color:red;line-height:23px;">目前中標：<span id="bidded"></span>&nbsp;<span id="loadingIMG" style="display:none;"><img src="<?php echo BASE_URL.APP_DIR.'/images/site/loading.gif'; ?>"/></span>&nbsp;
					    <a href="#" onclick="autoUpdate();" data-role="button" data-icon="refresh" data-iconpos="notext" style="display:inline;">
					        <span class="ui-btn-inner">
					        	<span class="ui-btn-text">&nbsp;</span>
					        	<span class="ui-icon ui-icon-refresh ui-icon-shadow" />&nbsp;</span>
					        </span>
						</a>
						<span style="color:black;display:block;white-space:normal;">（每分钟自動更新一次）</span>
					</h2>
                    
                </li>
            </ul>
			
            <div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">
                <?php if($product['is_flash']=='Y'){ }else{ ?>
				<div data-role="collapsible">
                    <h2>连續出價</h2>
                    <ul data-role="listview" data-theme="a">
						<?php /* if(!empty( _v('sgift_num') ) ){ ?>
						<div class="button">
							<input type="radio" name="use_sgift" value="N" checked/>使用殺價幣
							<input type="radio" name="use_sgift" value="Y"/>使用下標礼券
						</div>
						<?php } else { ?>
						<input type="hidden" name="use_sgift" value="N"/>
						<input type="hidden" name="use_sgift" value="<?php echo _v('sgift_num'); ?>"/>
						<?php }//endif; */?>
						<li class="ui-field-contain">
                            <input name="bidb" id="bidb" value="" data-clear-btn="true" type="text" placeholder="例:1.00">
                            至
                            <input name="bide" id="bide" value="" data-clear-btn="true" type="text" placeholder="例:2.05">
                        </li>
						
						<!--<li class="ui-field-contain">
							<input name="radio-saja" id="pay_type" value="spoint" type="radio" checked >
							<label for="pay_type">支付殺價幣</label>
						</li>-->
						
						<li class="ui-field-contain">   
							<button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="bid_n(<?php echo $product['productid']; ?>)">殺價幣连續出價</button>
							<h4 style="color: red;white-space:normal;">连續出價不提示下標顺位，結標前最后一分钟禁用连續下標</h4>
                        </li>
                    </ul>
                </div>
				<?php } ?>
				
                <div data-role="collapsible">
                    <h2>单次出價</h2>
                    <ul data-role="listview" data-theme="a">
                        <?php /* if(!empty( _v('sgift_num') ) ){ ?>
						<div class="button">
							<input type="radio" name="use_sgift" value="N" checked/>使用殺價幣
							<input type="radio" name="use_sgift" value="Y"/>使用下標礼券
						</div>
						<?php } else { ?>
						<input type="hidden" name="use_sgift" value="N"/>
						<input type="hidden" name="use_sgift" value="<?php echo _v('sgift_num'); ?>"/>
						<?php }//endif; */?>
						
						<li class="ui-field-contain">
                            <input name="bid" id="bid" value="" data-clear-btn="true" type="text" placeholder="例:5.02">
                        </li>
						
						<li class="ui-field-contain">
							<fieldset data-role="controlgroup" id="pay_type">
								<?php if($product['pay_type'] !=='a'){ ?>
								<input name="radio-saja" id="radio-1" value="oscode" type="radio" >
								<label for="radio-1">支付限定S碼</label>
								<?php } ?>
								
								<?php if($product['pay_type']=='a' || $product['pay_type']=='b' || $product['pay_type']=='c'){ ?>
								<input name="radio-saja" id="radio-2" value="spoint" type="radio" checked >
								<label for="radio-2">支付殺價幣</label>
								<?php } ?>
								
								<?php 
									//if($product['use_scode'] && ($product['pay_type']=='b' || $product['pay_type']=='d') ){ 
									if($product['pay_type']=='b' || $product['pay_type']=='d'){ 
								?>
								<input name="radio-saja" id="radio-3" value="scode" type="radio" <?php echo $chk_3; ?> >
								<label for="radio-3">支付S碼</label>
								<?php } ?>
							</fieldset>
						</li>
						
						<li class="ui-field-contain"> 
							<button type="submit" class="ui-btn ui-corner-all ui-btn-a" onClick="bid_1(<?php echo $product['productid']; ?>)">单次出價</button>
							<h4 style="color: red;white-space:normal;">結標前最后一分钟不提示下標顺位</h4>
                        </li>
                    </ul>
                </div>
                <div data-role="collapsible">
                    <h2>詳细资料</h2>
                    <ul data-role="listview" data-theme="b">
                        <li>市價：RMB <?php echo $product['retail_price']; ?> 元</li>
                        <li>底價：RMB <?php echo $product['price_limit']; ?> 元</li>
                        
						<?php if($product['is_flash']=='Y'){ }else{ ?>
						<li>下標手續費：殺價幣 <?php echo $product['saja_fee']; ?> 點</li>
						<?php } ?>
                        
						<li>中標处理費：殺價幣 <?php echo $product['process_fee']; ?> 點</li>
						
						<?php if(!empty($product['saja_limit']) ){ ?>
						<li>未达 <?php echo $product['saja_limit']; ?> 標 流標退點</li>
						<?php } ?>
                        <?php /*<li>目前中標：<span id="bidded"><?php echo _v('bidded'); ?></span></li>*/?>
                    </ul>
                </div>
                <div data-role="collapsible">
                    <h2>商品简介</h2>
					<p><div id="onlyContent"><?php echo $product['description']; ?></div></p>
                </div>
            </div>
        </div><!-- /article -->
		
		<script type="text/javascript">
			$(function(){
				autoUpdate();
			});
			
			var autoUpdate = function(){
                var URLs = "<?php echo BASE_URL . APP_DIR .'/product/test01/'; ?>";
                var now;
                
                $.get("/site/lib/cdntime.php?"+getNowTime(), function(data) {
					now = data;
				});
				
                $.ajax({
                    url: URLs,
                    data: { productid: <?php echo $product['productid']; ?>},
                    type: "POST",
                    dataType: 'text',
                    success: function(str_json){
                        var json = $.parseJSON(str_json);
                        var second = 60;
                        $('#bidded').html(json.name);
                        
                        var d = parseInt(now /24 / 60 / 60);
                        var h = parseInt((now - parseInt(d * 24 * 60 * 60)) / 60 / 60);
                        var m = parseInt((now - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60)) / 60);
                        var s = parseInt((now - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60) - parseInt(m * 60)));
                        
                        setTimeout(autoUpdate, (second - s) * 1000);
                    },
                    beforeSend:function(){
                        $('#loadingIMG').show();
                    },
                    complete:function(){
                        $('#loadingIMG').hide();
                    },
                    error:function(xhr, ajaxOptions, thrownError){ 
                        //alert(xhr.status); 
                        //alert(thrownError); 
                    }
                });
            }
			
			var dataForWeixin={
               appId:"",
               MsgImg:"<?php echo $img_src; ?>",
               TLImg:"",
               url:location.href,
               title:"<?php echo $product['name']; ?> 低價抢殺中!",
               desc:"<?php echo $product['price_limit']; ?>元起 热烈抢殺中，你还琢磨什么?",
               fakeid:"",
               callback:function(){}
            };
            (function(){
               var onBridgeReady=function(){
               WeixinJSBridge.on('menu:share:appmessage', function(argv){
                  WeixinJSBridge.invoke('sendAppMessage',{
                     "appid":dataForWeixin.appId,
                     "img_url":dataForWeixin.MsgImg,
                     "img_width":"120",
                     "img_height":"120",
                     "link":dataForWeixin.url,
                     "desc":dataForWeixin.desc,
                     "title":dataForWeixin.title
                  }, function(res){(dataForWeixin.callback)();});
               });
               WeixinJSBridge.on('menu:share:timeline', function(argv){
                  (dataForWeixin.callback)();
                  WeixinJSBridge.invoke('shareTimeline',{
                     "img_url":dataForWeixin.TLImg,
                     "img_width":"120",
                     "img_height":"120",
                     "link":dataForWeixin.url,
                     "desc":dataForWeixin.desc,
                     "title":dataForWeixin.title
                  }, function(res){});
               });
               WeixinJSBridge.on('menu:share:weibo', function(argv){
                  WeixinJSBridge.invoke('shareWeibo',{
                     "content":dataForWeixin.title,
                     "url":dataForWeixin.url
                  }, function(res){(dataForWeixin.callback)();});
               });
               WeixinJSBridge.on('menu:share:facebook', function(argv){
                  (dataForWeixin.callback)();
                  WeixinJSBridge.invoke('shareFB',{
                     "img_url":dataForWeixin.TLImg,
                     "img_width":"120",
                     "img_height":"120",
                     "link":dataForWeixin.url,
                     "desc":dataForWeixin.desc,
                     "title":dataForWeixin.title
                  }, function(res){});
               });
            };
            if(document.addEventListener){
               document.addEventListener('WeixinJSBridgeReady', onBridgeReady, false);
            }else if(document.attachEvent){
               document.attachEvent('WeixinJSBridgeReady'   , onBridgeReady);
               document.attachEvent('onWeixinJSBridgeReady' , onBridgeReady);
            }
            })();
		</script>
