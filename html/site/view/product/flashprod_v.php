<?php 
$product = _v('product'); 
$status = _v('status');
$meta=_v('meta');
$cdnTime = date("YmdHis");
$qrcode=_v('qrcode');
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
?>
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/css/colorbox.css" />
<script src="<?PHP echo APP_DIR; ?>/javascript/jquery.colorbox-min.js"></script>
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/css/colorbox.css" />
<script src="<?PHP echo APP_DIR; ?>/javascript/jquery.colorbox-min.js"></script>
<script src="<?PHP echo APP_DIR; ?>/static/js/socket.io-1.3.7.js"></script>
  	<div class="article">
		  <?php if(!empty($product['productid'])) { ?>
            <ul data-role="listview" data-inset="true" data-icon="false">
                <li>
                    <div id="tagBox">
                        
						<div id="tagBoxPlus">
							
						
							<?php if (!empty($product['srid']) ){
										if($product['srid']=='any_saja'){
										
										}elseif($product['srid']=='never_win_saja'){ ?>
											<div id="tagBox_qualifications" title="新手限定(未中標过者)" >限新手</div>
										<?php } elseif($product['srid']=='le_win_saja'){ ?>
											<div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以下)" >限新手</div>
										<?php } elseif($product['srid']=='ge_win_saja'){ ?>
											<div id="tagBox_qualifications" title="(限中標次數 <?php echo $product['value'];?> 次以上)" >限老手</div>
										<?php 
										} 
								  }//endif; 
							?>
							
							<?php if ((float)$product['saja_fee'] == 0.00){ ?>
								       <div id="tagBox_free" title="免費(出價免扣殺價幣)">免費</div>
							<?php } //endif; ?>
						</div>
						
						<?php 
						if(!empty($product['thumbnail'])) { //$status['path_image'] 
							$img_src = BASE_URL . APP_DIR .'/images/site/product/'. $product['thumbnail'];
						} elseif (!empty($product['thumbnail_url'])) {
							$img_src = $product['thumbnail_url']; 
						} 
						?>
						<img <?php echo $flash_img; ?> width="100%" src="<?php echo $img_src; ?>"  >
                    </div>
					
					<div style="white-space:pre-wrap;font-size:30px;" align="center"><?php echo $product['name']; ?></div>
					<?php if (!empty($product['offtime'])){ ?>
					           <div align="center"><p class="countdown" style="font-size:30px;color:red" data-offtime="<?php echo $product['offtime']; ?>" data-tag=" "> 00时00分00秒</p></div>
					<?php }//endif; ?>
					
					     <!-- div align="center" id="final_bidded" style="color:blue;font-size:30px;" -->
						 <div align="center" id="final_bidded" style="display:none">
						   <span id="bidded"><?php echo _v('bidded'); ?></span>&nbsp;
                    				
					      <!-- a href="#" onclick="autoUpdate();" data-role="button" data-icon="refresh" data-iconpos="notext" style="display:inline;">
					        <span class="ui-btn-inner">
					        	<span class="ui-btn-text">&nbsp;</span>
					        	<span class="ui-icon ui-icon-refresh ui-icon-shadow" />&nbsp;</span>
					        </span>
						</a>
						<span style="color:black;display:block;white-space:normal;">（每分钟自動更新一次）</span -->
					   
					     </div>
					<p>
						<?php if($qrcode=='Y') { ?>
							<div align="center" width="60%">
							   <img width="270" height="270" src="http://www.shajiawang.com/site/phpqrcode/?data=<?php  echo urlencode("http://www.shajiawang.com/site/product/oscode/?productid=".$product['productid']); ?>"/>
							</div>
						<?php  }   ?>
					
					<?php  if($_SESSION['auth_id']=='995') {  ?>
							<span id="line-share-button"  style="display:none" >
							   <a href="http://line.me/R/msg/text/?<?php echo $meta['title']."  ".BASE_URL.$_SERVER['REQUEST_URI']; ?>"><img src="<?php echo BASE_URL.APP_DIR; ?>/images<?php echo APP_DIR; ?>/linebutton_en/linebutton_78x20_en.png" width="78px" height="20px" alt="LINE it!" /></a>
							</span>
							<div class="fb-share-button" id="fb-share-button"  style="display:none"
								 data-href="http://www.shajiawang.com/site/product/saja/?productid=<?php echo $product['productid']; ?>" data-layout="button_count">
							</div>
					<?php  } //endif; ?>
                </li>
            </ul>
		<?php  } else { ?>
		               <div align="center">
					     <p style="font-size:40px;color:blue" ><b>~~敬請期待闪殺活動~~</b></p>
					   </div>
		<?php  }   ?>
        </div>
		<!-- /article -->
<?php if(!empty($product['productid'])) { ?>		
		
		<script type="text/javascript">		
        var chkprodid="<?php echo $product['productid']; ?>";
		var now_time;
		var socket;
		var timer;
		var timer_run = function(){
			  // $(this).everyTime('1s','losttime', function(event) {
			now_time = now_time + 1;
			var end_time = $('.countdown').attr('data-offtime'); //截標時間
			var end_plus = $('.countdown').attr('data-tag'); //標籤		
			var lost_time = parseInt(end_time) - now_time; //剩餘時間
			if(lost_time >= 0) {
				var o = lost_time; //原始剩餘時間
				var d = parseInt(o /24 / 60 / 60);
				if(d<10){d = "0" + d;};
				var h = parseInt((o - parseInt(d * 24 * 60 * 60)) / 60 / 60); //37056
				if(h<10){h = "0" + h;};
				var m = parseInt((o - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60)) / 60);
				if(m<10){m = "0" + m;};
				var s = parseInt((o - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60) - parseInt(m * 60)));
				if(s<10){s = "0" + s;};
				
				// $('.countdown:eq('+i+')').html(end_plus+d+"天"+h+"时"+m+"分"+s+"秒");
				$('.countdown').html(end_plus+" "+h+"时"+m+"分"+s+"秒");
				if(now_time%5==0) {
					$.get("/site/lib/cdntime.php?"+getNowTime(), function(data) {
						now_time = parseInt(data); //系統時間
					});
					autoUpdate();
				}
			} else if(lost_time < 0) {
				if(lost_time>=-4) {
				   $('.countdown').html(end_plus+" <b>.. 结標作业中 ..</b>");
				   // $('.countdown').html(end_plus+" <b>~ 時間到 ~</b>");
				   // $('#final_bidded').html('<b><span id="bidded">..结標作业中..</span></b>');
				} else {
				   $.get("/site/product/getBidWinnerInfo/?productid="+chkprodid, function(msg){
					   var json = JSON && JSON.parse(msg) || $.parseJSON(msg);
					   if(json.productid==chkprodid) {
						  var wname=json.nickname;
						  var wprice=json.price;
						  var wuserid=json.userid;
						  $('.countdown').html(end_plus+" <b>~ 已结標 ~</b>");
						  /*
						  if(wuserid=='') {
							 $('#final_bidded').html('<b><span id="bidded">..無人中標..</span></b>');
						  } else {
							 $('#final_bidded').html('恭喜!! 中標者為 : <b><span id="bidded"> '+wname+' ('+wprice+'元)'+'</span></b>');
						  }
						  */
					   }
				   });
				   clearInterval(timer);
				}
				/*
				// $('.countdown:eq('+i+')').html(end_plus+"00分00秒");
				$('.countdown').html(end_plus+" <b>已结標</b>");
				if(lost_time<=-2 && lost_time>=-5) {
				   autoUpdate();
				   $('#final_bidded').html('恭喜 !! 得標者為 : <b><span id="bidded">'+$('#bidded').val()+'</span></b>');
				}
				if(lost_time<-30) {
				   // $(this).stopTime('losttime');
				   location.href="/site/product/adview2/";
				}
				*/
			}
		};
		
		function autoUpdate() {
			var URLs = "<?php echo BASE_URL.APP_DIR.'/product/winner/?timestamp='.$cdnTime; ?>";
			// var URLs = "http://mbr.shajiawang.com:3344/winner";
			// $.mobile.loading('show', {text: '更新中', textVisible: true, theme: 'b', html: ""});
			$.mobile.loading('hide');
			$.ajax({
				url: URLs,
				data: { productid: "<?php echo $product['productid']; ?>"},
				type: "GET",
				dataType: 'text',
				success: function(str_json){
					$.mobile.loading('hide');
					var json = $.parseJSON(str_json);
					// console && console.log($.parseJSON(str_json));
					// $('#bidded').html(json.name);
					if(json.productid == chkprodid) {
						$('#bidded').html(json.name);
						// t = window.setTimeout(autoUpdate, second*1000);
					}
				},
				error:function(xhr, ajaxOptions, thrownError){ 

				}
			});
		}
		
		$(document).ready(function(){
		    $.mobile.loading('show', {text: '準備中..', textVisible: true, theme: 'b', html: ""});
		    socket = io.connect('http://mbr.shajiawang.com:3333/');
			socket.on('connect', function(){
				console.log("connect !!");
				autoUpdate();
				socket.emit('notify','{"PID":"<?php echo $product['productid']; ?>","PAGE":"flashprod_v.php","IP":"<?php echo $ip; ?>"}');
			});
			socket.on('message', function(msg){
				  console.log(msg);
				  // var json = JSON && JSON.parse(msg) || $.parseJSON(msg);
				  var json = msg ;
				  if(typeof(msg)=="string") {
					  json = JSON && JSON.parse(msg) || $.parseJSON(msg);
				  } else {
				     json = msg;
				  }
				  // console.log(json);
				  if(json.PRODID==chkprodid) {
					 if(json.ACTION=='reload') {
					   socket.close();
					   window.location.reload();
					 } else if(json.ACTION=='end_saja') {
					   $('.countdown').attr('data-offtime',json.END_TIME);
					   clearInterval(timer);
					   timer=setInterval(timer_run,1000);
					   // window.location.reload(); 
					 } else {
					   document.getElementById("bidded").innerHTML=json.WINNER;
					 }
				  }
			});
			socket.on('disconnect', function(){
				  console.log("disconnect !!");
				  socket.connect();
			});
			
			$.ajaxSetup({ cache: false });
			clearInterval(timer);
			$(".inline").colorbox({inline:true, width:"80%"});
			$(".example").colorbox({iframe:true, width:$(window).width() * 0.8, height: $(window).width() * 0.8 * 0.75});
			var ts=new Date().getTime();
			$.get("http://mbr.shajiawang.com:3344/ts",function(data) {
			// $.get("/site/lib/cdntime.php?"+getNowTime(), function(data) {
			      // alert(new Date().getTime()-ts);
				  now_time = parseInt(data); //系統時間
				  autoUpdate();
				  timer=setInterval(timer_run,1000);
				  $.mobile.loading('hide');
				  /*
				  timer=setInterval(function(){
				  // $(this).everyTime('1s','losttime', function(event) {
							now_time = now_time + 1;
							var end_time = $('.countdown').attr('data-offtime'); //截標時間
							var end_plus = $('.countdown').attr('data-tag'); //標籤		
							var lost_time = parseInt(end_time) - now_time; //剩餘時間
							if(lost_time >= 0) {
								var o = lost_time; //原始剩餘時間
								var d = parseInt(o /24 / 60 / 60);
								if(d<10){d = "0" + d;};
								var h = parseInt((o - parseInt(d * 24 * 60 * 60)) / 60 / 60); //37056
								if(h<10){h = "0" + h;};
								var m = parseInt((o - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60)) / 60);
								if(m<10){m = "0" + m;};
								var s = parseInt((o - parseInt(d * 24 * 60 * 60) - parseInt(h * 60 * 60) - parseInt(m * 60)));
								if(s<10){s = "0" + s;};
								
								// $('.countdown:eq('+i+')').html(end_plus+d+"天"+h+"时"+m+"分"+s+"秒");
								$('.countdown').html(end_plus+" "+h+"时"+m+"分"+s+"秒");
								if(now_time%5==0) {
									$.get("/site/lib/cdntime.php?"+getNowTime(), function(data) {
										now_time = parseInt(data); //系統時間
									});
									autoUpdate();
								}
							} else if(lost_time < 0) {
								if(lost_time>=-3) {
								   $('.countdown').html(end_plus+" <b>~時間到~</b>");
								   $('#final_bidded').html('<b><span id="bidded">..结標作业中..</span></b>');
								} else {
								   clearInterval(timer);
								   $.get("/site/product/getBidWinnerInfo/?productid="+chkprodid, function(msg){
									   var json = JSON && JSON.parse(msg) || $.parseJSON(msg);
									   if(json.productid==chkprodid) {
										  var wname=json.nickname;
										  $('.countdown').html(end_plus+" <b>~已结標~</b>");
										  if(wname=='') {
										     $('#final_bidded').html('<b><span id="bidded">..無人中標..</span></b>');
										  } else {
										     $('#final_bidded').html('恭喜!!中標者為 : <b><span id="bidded"> '+wname+'</span></b>');
										  }
									   }
								   });
								}
								// $('.countdown:eq('+i+')').html(end_plus+"00分00秒");
								$('.countdown').html(end_plus+" <b>已结標</b>");
								if(lost_time<=-2 && lost_time>=-5) {
								   autoUpdate();
								   $('#final_bidded').html('恭喜 !! 得標者為 : <b><span id="bidded">'+$('#bidded').val()+'</span></b>');
								}
								if(lost_time<-30) {
								   // $(this).stopTime('losttime');
								   location.href="/site/product/adview2/";
								}
								
							}
					},1000);*/
    		 });
		 });
		</script>
<?php  } else {  ?>
		<script>
		    // var now_sec=new Date().getUTCSeconds();
			//$(this).everyTime('1s','losttime', 
			//      function(i) {
			var now_sec=0;
            setInterval(function(){			
				   now_sec = now_sec + 1;
				   // console.log('time :'+now_sec);
				   if(now_sec==30) {
					  // $(this).stopTime('losttime');
					  window.location.reload();
				   }
				},1000);
		</script>
<?php  }  ?>
