<?php
$product = _v('product');
$sajabid = _v('sajabid');
$status = _v('status');
$meta = _v('meta');
$canbid = _v('canbid');
$cdnTime = date("YmdHis");
$options = _v('options');
$type = _v('type');
$paydata = _v('paydata');
$topay = _v('topay');
$spoint = _v('spoint');
$oscode_num=_v('oscode_num');
$scode_num=_v('scode_num');
$scode_use_num=_v('scode_use_num');
if(!isset($canbid)) {
    $canbid=$_REQUEST['canbid'];
}
if(empty($canbid)) {
    $canbid='Y';
}
//閃殺商品
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
?>
<div class="sajatopay">
    <div class="sajatopay-box">
        <?php
        if(!empty($product['thumbnail'])) {
            $img_src = $product['thumbnail'];
        } elseif (!empty($product['thumbnail_url'])) {
            $img_src = $product['thumbnail_url'];
        }
        ?>
        <div class="sajatopay-imgbox d-flex align-items-center justify-content-center">
            <div class="imgbox-img d-flex align-items-center">
                <img class="img-fluid" <?php echo $flash_img; ?> src="<?php echo $img_src; ?>">
            </div>
            <div class="imgbox-info d-inflex">
                <h4 class="sajatopay-name"><?php echo $product['name']; ?></h4>
                <p class="sajalist-oprice"><span class="oprice-title">官方售價：NT </span><span class="oprice-num"><?php echo $product['retail_price']; ?> 元</span></p>
                <div class="sajatopay-overtime">
                    <span class="overtime-title d-inlineflex">結標日期</span>
                    <span id="ot-<?php echo $product['productid']; ?>" class="overtime d-inlineflex" data-offtime="<?php echo date('Y-m-d H:i',$product['offtime']); ?>" data-tag=""><?php echo date('Y-m-d H:i',$product['offtime']); ?></span>
                </div>
            </div>
        </div>
        <div class="sajatopay-payinfo">
            <div class="payinfo-item d-flex align-items-center">
                <div class="pay-title mr-auto">下標數量</div>
                <div class="pay-rightTxt">
                    <span id="bidded"><?php echo $topay['count']; ?> 標</span>
                </div>
            </div>
            <div class="payinfo-item d-flex align-items-center">
                <div class="pay-title mr-auto">下標金額<?php if ($product['totalfee_type'] == 'F' || $product['totalfee_type'] == 'O') {?><span class="small"> (免收) </span><?php } ?></div>
                <div class="pay-rightTxt">
                    <!-- 單次下標 -->
                    <?php if($topay['type'] == 'single'){ ?>
                        <span>$ <?php echo $topay['bida']; ?></span>
                        
                    <!-- 包牌 or 隨機下標 -->
                    <?php } else if($topay['type'] == 'range' || $topay['type'] == 'lazy'){ ?>
                        <span>$ <?php echo $topay['bida']; ?> ~ <?php echo $topay['bidb']; ?></span>
                        
                    <?php } ?>
                </div>
            </div>
            <div class="payinfo-item d-flex align-items-center">
                <div class="pay-title mr-auto">手續費<span class="small"> (<?php if ($product['totalfee_type'] == 'B' || $product['totalfee_type'] == 'O') { echo "免收"; }else{ echo $product['saja_fee'].'元/標';} ?>) </span></div>
                <div class="pay-rightTxt">
                <?php if ($topay['fee_all'] > 0 & $product['totalfee_type'] !== 'B' & $product['totalfee_type'] !== 'O'){ ?>
                    <span>NT <?php echo $topay['fee_all']; ?> 元</span>
                <?php } else { ?>
                    <span>NT 0 元</span>
                <?php } ?>
                </div>
            </div>
        </div>
        <div class="sajatopay-paytotal d-flex align-items-center">
            <div class="pay-title mr-auto">總計</div>
            <div class="pay-rightTxt red">NT <span><?php echo sprintf("%1\$u",$topay['payall']); ?></span> 元</div>
        </div>
    </div>

    <div class="sajatopay-select">

    <?php if($canbid!='N') { ?>
        <div class="sele-items">
            <!-- 殺價券顯示 -->
            <div class="sele-item sele-oscode d-flex align-items-center disabled">
                <div class="all-radio">
                    <input name="radio1" id="oradio" value="oscode" type="radio">
                    <label for="oradio" class="radio-label"> </label>
                </div>
                <div class="radio-title-box d-flex align-items-center">
                    <div class="radio-title mr-auto">殺價券抵扣<small></small></div>
                    <div class="Txt"></div>
<!--
                    <?php if($oscode_num>0) { ?>
                        <div class="radio-input d-inlineflex align-items-center">
                            <?php 
                                if($oscode_num >= $topay['count']){
                                    $oscode_value = $topay['count'];
                                }else{
                                    $oscode_value = $oscode_num;
                                }
                            ?>
                            <input type="number" name="oscode_num" id="oscode_num" value="<?php echo $oscode_value ?>" placeholder="輸入折抵張數">
                            <span>張</span>
                        </div>
                    <?php }else{ ?>
                        <div class="Txt">無殺價券</div>
                    <?php } ?>
-->
                </div>
            </div>
            
            <!-- 殺幣顯示 -->		
            <div class="sele-item sele-spoint d-flex align-items-center disabled">
                <div class="all-radio">
                    <input name="radio" id="sradio" value="spoint" type="radio">
                    <label for="sradio" class="radio-label"> </label>
                </div>

                <div class="radio-title-box d-flex align-items-center">
                    <div class="radio-title mr-auto">殺價幣下標<small></small></div>
                    <div class="Txt"></div>
                </div>
            </div>
            
            <!-- S券顯示 -->
            <?php if ($scode_num + $scode_use_num > 1){ ?>
                <div class="sele-item sele-scode d-flex align-items-center disabled">
                    <div class="all-radio">
                        <input name="radio3" id="kradio" value="scode" type="radio">
                        <label for="kradio" class="radio-label"> </label>
                    </div>
                    <div class="radio-title-box d-flex align-items-center">
                        <div class="radio-title mr-auto">超級殺價券抵扣<small></small></div>
                        <div class="Txt"></div>
<!--
                        <?php if($scode_num>0) { 
                            if($topay['count'] <= $scode_num){
                        ?>
                            <div class="radio-input d-inlineflex align-items-center">
                                <?php 
                                    if($scode_num >= $topay['count']){
                                        $scode_value = $topay['count'];
                                    }else{
                                        $scode_value = $scode_num;
                                    }
                                ?>
                                <input type="hidden" name="scode_num" id="scode_num" value="<?php echo $scode_value ?>" placeholder="輸入折抵張數"><?php echo $scode_value ?>
                                <span>張</span>
                            </div>
                        <?php }else{ ?>
                            <div class="Txt">張數不足</div>
                        <?php }}else{ ?>
                        <div class="Txt">無超級殺價券</div>
                        <?php } ?>
-->
                    </div>
                </div>	
            <?php } ?>
    <!--
            <div class="sele-item d-flex justify-content-between align-items-center disabled">
                <div class="all-radio">
                    <input name="radio" id="cradio" value="cash" type="radio" disabled>
                    <label for="cradio" class="radio-label"> </label>
                </div>
                <div class="radio-title">線上支付下標</div>
                <div class="Txt sajapay-font3">-<?php echo sprintf("%1\$u",$topay['payall']); ?> 元</div>
            </div>
    -->
        </div>
    <?php } ?>
    </div>	
    <input type="hidden" name="pay_type" id="pay_type" value="<?php echo $product['pay_type']; ?>">
    <input type="hidden" name="type" id="type" value="<?php echo $topay['type']; ?>">
    <input type="hidden" name="count" id="count" value="<?php echo $topay['count']; ?>"/>
    <input type="hidden" name="fee_all" id="fee_all" value="<?php echo $topay['fee_all']; ?>"/>
    <input type="hidden" name="total_all" id="total_all" value="<?php echo $topay['total_all']; ?>">
    <input type="hidden" name="payall" id="payall" value="<?php echo $topay['payall']; ?>">

    <input type="hidden" name="oscode_num" id="oscode_num" value="<?php echo $oscode_value ?>">
    <input type="hidden" name="scode_num" id="scode_num" value="<?php echo $scode_value ?>">
    
    <input type="hidden" name="bida" id="bida" value="<?php echo $topay['bida']; ?>"/>
    <input type="hidden" name="bidb" id="bidb" value="<?php echo $topay['bidb']; ?>"/>
    <input type="hidden" name="saja_fee" id="saja_fee" value="<?php echo $product['saja_fee'];?>"/>
    <input type="hidden" name="autobid" id="autobid" value="<?php echo $paydata['autobid'];?>"/>
    <input type="hidden" name="productid" id="productid" value="<?php echo $product['productid']; ?>">
    <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
    <input type="hidden" name="user" id="user" value="<?php echo $_SESSION['auth_id']; ?>">
    <input type="hidden" name="prod_le" id="prod_le" value="<?php echo $config['prod_le']; ?>">

    <div class="productSaja-footernav">
        <div class="productSaja-button">
<!--
           	<?php if (($spoint['amount'] < sprintf("%1\$u",$topay['payall'])) && ($topay['count'] > $oscode_num) && ($topay['count'] > $scode_num) ){ ?>
                <button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/deposit/?<?php echo $cdnTime; ?>'">餘額不足 點我購買</button>            
			<?php }else{ ?>
                <button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a"
                    <?php if ($topay['type'] == 'single'){ ?>
                         onClick="bid_1(<?php echo $product['productid']; ?>);"
                    <?php }else{ ?>
                         onClick="bid_n(<?php echo $product['productid']; ?>);"
                    <?php } ?>>確定結算
                </button>
            <?php } ?>	
-->
        </div>
    </div>
</div>

<script type="text/javascript">

    <?php if (!empty($paydata['autobid']) && $paydata['autobid'] == 'Y') { ?>
    $(window).load(function() {
        <?php if ($paydata['type'] == "single"){ ?>
        bid_1(<?php echo $product['productid']; ?>);
        <?php }else{ ?>
        bid_n(<?php echo $product['productid']; ?>);
        <?php } ?>
    });
    <?php } ?>
    
    $(function(){
        var sajatopayOption = {
            dom:{
                $seleOscode: '.sele-item.sele-oscode',
                $seleSpoint: '.sele-item.sele-spoint',
                $seleScode: '.sele-item.sele-scode'
            }
        }
        
        var $data_totalfee_type = '<?php echo $product['totalfee_type'];?>',      //收款類型
            $data_bid_type = '<?php echo $topay['type']; ?>',                     //下標方式(單次、包牌)
            $data_count = Number('<?php echo $topay['count']; ?>'),               //總標數
            $data_bid_a = parseInt('<?php echo $topay['bida']; ?>'),              //起標
            $data_bid_b = parseInt('<?php echo $topay['bidb']; ?>'),              //尾標
            $data_bidArray = [],                                                  //標陣列 (包牌用)
            $data_saja_fee = parseInt('<?php echo $product['saja_fee']; ?>'),     //手續費/標
            $data_range = parseInt('<?php echo $config['prod_le']; ?>'),          //包牌range
            $data_pay_all = parseInt('<?php echo $topay['payall']; ?>'),          //總計:標金+手續費
            $data_pay_type = '<?php echo $product['pay_type']; ?>',               //支付方式限制
            $data_spoint = parseInt('<?php echo $spoint['amount']; ?>'),          //殺幣餘額
            $data_oscode_num = Number('<?php echo $oscode_num; ?>'),                      //殺價券
            $data_scode_num = Number('<?php echo $scode_num; ?>'),                        //超級殺價券
            $data_scode_used = Number('<?php echo $scode_use_num; ?>');                   //超級殺價券使用過的張數
        
        var $isScode_show = ($data_scode_num + $data_scode_used) > 0;        //超級殺價券(現有張數 + 使用過張數，若大於0表示有見過，可顯示)
        
        // $data_totalfee_type 收款類型: 1.收下標金+手續費(A) 2.只收下標金(B) 3.只收手續費(F) 4.免費(O)
        // $data_bid_type 下標方式: 1.單次(single) 2.包牌(range) 3.隨機(lazy)
        // $data_pay_type 下標支付方式: 1.殺幣(a) 2.殺幣+S碼+殺價券(b) 3.殺幣+殺價券(c) 4.S碼+殺價券(d) 5.殺價券(e)
        
    //殺幣
        var $spoint = $(sajatopayOption.dom.$seleSpoint);
        var $spradio = $spoint.find('#sradio');
        var $sptbox_title = $spoint.find('.radio-title-box .radio-title small');
        var $sptbox_txt = $spoint.find('.radio-title-box .Txt');
    //殺券
        var $oscode = $(sajatopayOption.dom.$seleOscode);
        var $osradio = $oscode.find('#oradio');
        var $oscbox_title = $oscode.find('.radio-title-box .radio-title small');
        var $oscbox_txt = $oscode.find('.radio-title-box .Txt');
    //超券
        var $scode = $(sajatopayOption.dom.$seleScode);
        var $sradio = $scode.find('#kradio');
        var $scbox_title = $scode.find('.radio-title-box .radio-title small');
        var $scbox_txt = $scode.find('.radio-title-box .Txt');
        
        $oscbox_title.text('(折抵前殺價券張數：'+$data_oscode_num+' 張)');
        $sptbox_title.text('(折抵前殺價幣餘額：'+$data_spoint+')');
        $scbox_title.text('(折抵前超級殺價券張數：'+$data_scode_num+' 張)');
        
        function bidArray() {                       
            var $start = $data_bid_a;
            for(var i = 0; i < $data_count ;i++){
                var $num = $start + (i * $data_range);
                if($data_totalfee_type == 'A'){
                    $data_bidArray.push(Number($num + $data_saja_fee));       //標金+手續費
                }else if($data_totalfee_type == 'B'){
                    $data_bidArray.push(Number($num));                        //標金
                }else{
                    return;
                }
            }
            //console.log($data_bidArray);
            //console.log('總價:'+sumData($data_bidArray));
        }           //下標詳細陣列(標金+手續費)
        
        function creatSubmitBtn(goto,paytype){
            var $box = $('.productSaja-footernav .productSaja-button');
            $box.empty();                                       //清空內容
            //goto: deposit (購買殺幣), bid (單次&包牌下標), loset (一般券張數不足), losest (超級殺價券張數不足)
            switch(goto){
                case 'deposit':
                    $box.append(
                        $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a side">')
                        .attr('onClick',"javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/deposit/?<?php echo $cdnTime; ?>'")
                        .text('點我購買')
                    )
                    break;
                case 'bid':
                    if($data_bid_type == 'single'){
                        $box.append(
                            $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')
                            .attr('onClick',"bid_1(<?php echo $product['productid']; ?>,'"+paytype+"');")
                            .text('確定結算')
                        )
                    }else if($data_bid_type == 'range'){
                        $box.append(
                            $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')
                            .attr('onClick',"bid_n(<?php echo $product['productid']; ?>,'"+paytype+"');")
                            .text('確定結算')
                        )
                    }
                    break;
                case 'loset':
                    $box.append(
                        $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')
                        .text('確定結算')
                    )
                    //彈窗 (一般券)
                    $('#topay').on('click', function(){
                        createMsgModal('殺價券');
                    })
                    break;
                case 'losest':
                    $box.append(
                        $('<button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a">')        
                        .text('確定結算')
                    )
                    //彈窗 (超券)
                    $('#topay').on('click', function(){
                        createMsgModal('超級殺價券');
                    })
                    break;
            }
        }       //submit按鈕設定
        
        function sumData(arr){
            var sum = 0;
            for(var i = 0; i < arr.length; i++){
                sum += arr[i];
            };
            return sum;
        }                       //陣列加總
        
        function createMsgModal(title){
            $('body').addClass('modal-open');
            $('.sajatopay').append(
                $('<div class="msgModalBox d-flex justify-content-center align-items-center"/>')
                .append(
                $('<div class="modal-content"/>')
                    .append(
                    $('<div class="modal-body"/>')
                        .append(
                            $('<div class="modal-title d-flex align-items-center justify-content-center"/>')
                            .text(title)
                        )
                        .append(
                            $('<div class="modal-txt d-flex align-items-center justify-content-center"/>')
                            .text('張數不足')
                        )
                    )
                    .append(
                        $('<div class="modal-footer"/>')
                        .append(
                            $('<div class="footer-btn">')
                            .attr('onClick',"javascript:location.href='<?php echo APP_DIR; ?>/product/'")
                            .text('回殺戮戰場')
                        )
                    )
                )
                .append(
                    $('<div class="msgModalBg">')
                )
            )
            $('.modal-close').on('click', function(){
                $('body').removeClass('modal-open');
                $('.msgModalBox').remove();
            })
        }              //彈窗
        
        function deductionScode(num) {
            var $reverseArray,$deduction,$sundata,$less;
            $deduction = [];
            if(num >= 1){                                                          //殺券使用數量
                if($data_bidArray.length > 1){                                     //下標數大於 1標
                    for(var i = 0;i < num;i++){
                        $reverseArray = $data_bidArray.sort(function (a,b) {       //反過來排序
                            return b - a;
                        })
                        var $add_num = $reverseArray.splice(0, 1)
                        $deduction.push($add_num[0]);                   //使用殺價券扣抵的金額
                    }
//                    console.log('幣應扣:'+$reverseArray);
//                    console.log('券扣除額:'+$deduction);
                }else{                                                             //否則，不用跑回圈 直接取1張殺券的扣除額
                    $deduction = $data_bidArray.splice(0, 1);
                }
                $sundata = sumData($deduction);                                    //殺券總扣除額
                $less = $data_pay_all - $sundata;                                  //減掉殺價券折抵後 應扣的殺幣
            }else{
                $less = $data_pay_all;                                             //殺幣應扣 = 總金額
            }
            return $less;
        }               //計算殺價券扣抵
        
        var $type = $data_pay_type;                                  //debug用  $data_pay_type
        var $sp_less;
        (function changeRadioTitle($type) {
            bidArray();
            if($data_totalfee_type == 'O'){
                $('.sajatopay-select').append(
                    $('<div class="free_imgBox d-flex justify-content-center align-items-center"/>')
                    .append(
                        $('<div class="imgBox"/>')
                        .append(
                            $('<img class="free_img" src="<?PHP echo APP_DIR; ?>/static/img/free_img.svg">')
                        )
                    )
                )
                creatSubmitBtn('bid','spoint');
            }else{
                switch($type){
                    case 'a':       //幣
                        $spoint.removeClass('disabled');
                        $spradio.attr('checked','checked');
                        $oscbox_txt.text('不可使用');
                        $scbox_txt.text('不可使用');

                        //靜態顯示
                        if($data_spoint >= $data_pay_all){                     //幣全抵
                            $sptbox_txt.text('- '+$data_pay_all+' 點');
                        }else{                                                 //幣不足
                            $sptbox_txt.addClass('red').text('餘額不足');
                        }

                        //預設按鈕
                        if($data_spoint >= $data_pay_all){                     //幣全抵
                            creatSubmitBtn('bid','spoint');
                        }else{                                                 //幣不足
                            creatSubmitBtn('deposit');
                        }
                        break;
                    case 'b':       //幣+券+S
                        $spoint.removeClass('disabled');
                        $oscode.removeClass('disabled');
                        $scode.removeClass('disabled');

                        //預設選取
                        $osradio.attr('checked','checked');
                        $spradio.attr('checked','checked');
                        if(Number($data_scode_num) >= Number($data_count)){
                            $scbox_txt.text($data_count+' 張');
                        }else{
                            $scbox_txt.addClass('red').text('張數不足');
                        }

                        if($data_oscode_num > 0){
                            $sp_less = deductionScode($data_oscode_num);
                            $sp_less_text = $sp_less > 0 ? '- '+$sp_less+' 點' : '0';
                            if($data_oscode_num >= $data_count){                   //券全抵
                                $oscbox_txt.text($data_count+' 張');
                                $sptbox_txt.text($sp_less_text);
                                creatSubmitBtn('bid','oscode');
                            }else if($data_spoint >= $sp_less){                    //幣+券
                                $oscbox_txt.text($data_oscode_num+' 張');
                                $sptbox_txt.text($sp_less_text);
                                creatSubmitBtn('bid','oscode');
                            }else{                                                 //幣不足
                                $oscbox_txt.text($data_oscode_num+' 張');
                                $sptbox_txt.addClass('red').text('餘額不足');
                                creatSubmitBtn('deposit');
                            }
                        }else{                                                      //無券
                            $oscbox_txt.text('0 張');
                            $sp_less = deductionScode(0);
                            if($data_spoint >= $sp_less){                           //幣全抵
                                $sptbox_txt.text('- '+$sp_less+' 點');
                                creatSubmitBtn('bid','spoint');
                            }else{                                                  //幣不足
                                $sptbox_txt.addClass('red').text('餘額不足');
                                creatSubmitBtn('deposit');
                            }
                        }

                        break;
                    case 'c':       //幣+券
                        $spoint.removeClass('disabled');
                        $oscode.removeClass('disabled');
                        $scbox_txt.text('不可使用');

                        //預設選取
                        $osradio.attr('checked','checked');
                        $spradio.attr('checked','checked');

                        if($data_oscode_num > 0){
                            $sp_less = deductionScode($data_oscode_num);
                            $sp_less_text = $sp_less > 0 ? '- '+$sp_less+' 點' : '0';
                            if($data_oscode_num >= $data_count){                   //券全抵
                                $oscbox_txt.text($data_count+' 張');
                                $sptbox_txt.text($sp_less_text);
                                creatSubmitBtn('bid','oscode');
                            }else if($data_spoint >= $sp_less){                    //幣+券
                                $oscbox_txt.text($data_oscode_num+' 張');
                                $sptbox_txt.text($sp_less_text);
                                creatSubmitBtn('bid','oscode');
                            }else{                                                 //幣不足
                                $oscbox_txt.text($data_oscode_num+' 張');
                                $sptbox_txt.addClass('red').text('餘額不足');
                                creatSubmitBtn('deposit');
                            }
                        }else{                                                      //無券
                            $oscbox_txt.text('0 張');
                            $sp_less = deductionScode(0);
                            if($data_spoint >= $sp_less){                           //幣全抵
                                $sptbox_txt.text('- '+$sp_less+' 點');
                                creatSubmitBtn('bid','spoint');
                            }else{                                                  //幣不足
                                $sptbox_txt.addClass('red').text('餘額不足');
                                creatSubmitBtn('deposit');
                            }
                        }
                        break;
                    case 'd':       //S+券
                        $oscode.removeClass('disabled');
                        $scode.removeClass('disabled');
                        $sptbox_txt.text('不可使用');

                        //靜態顯示
                        if($data_oscode_num >= $data_count){
                            $oscbox_txt.text($data_count+' 張');
                        }else{
                            $oscbox_txt.addClass('red').text('張數不足');
                        }

                        if($data_scode_num >= $data_count){
                            $scbox_txt.text($data_count+' 張');
                        }else{
                            $scbox_txt.addClass('red').text('張數不足');
                        }

                        //預設選取
                        if($data_oscode_num >= $data_count){                //券全抵
                            $osradio.attr('checked','checked');
                            creatSubmitBtn('bid','oscode');
                        }else{
                            if($data_scode_num >= $data_count){             //S全抵
                                $sradio.attr('checked','checked');
                                creatSubmitBtn('bid','scode');
                            }else{                                          //券 & S 都不足
                                $osradio.attr('checked','checked');
                                creatSubmitBtn('loset');
                            }
                        }
                        break;
                    case 'e':       //券
                        $oscode.removeClass('disabled');
                        $osradio.attr('checked','checked');
                        $sptbox_txt.text('不可使用');
                        $scbox_txt.text('不可使用');

                        //靜態顯示
                        if($data_oscode_num >= $data_count){
                            $oscbox_txt.text($data_count+' 張');
                        }else{
                            $oscbox_txt.addClass('red').text('張數不足');
                        }

                        //預設按鈕
                        if($data_oscode_num >= $data_count){                    //券全抵
                            creatSubmitBtn('bid','oscode');
                        } else {                                                //券不足
                            creatSubmitBtn('loset');
                        }
                        break;
                }
            }
        }($type));
        
        (function changeBtnEvent($type){
            $('.sele-item input[type=radio]').on('change',function(e){
                var $change = $(this).attr('id');
                //console.log($change);
                switch($type){
                    case 'b':
                        switch($change){
                            case 'oradio':
                            case 'sradio':
                                $osradio.prop('checked', true);
                                $spradio.prop('checked', true);
                                $sradio.prop('checked', false);
                                if($data_oscode_num > 0){
                                    if($data_oscode_num >= $data_count){                   //券全抵
                                        creatSubmitBtn('bid','oscode');
                                    }else if($data_spoint >= $sp_less){                    //幣+券
                                        creatSubmitBtn('bid','oscode');
                                    }else{                                                 //幣不足
                                        creatSubmitBtn('deposit');
                                    }
                                }else{                                                      //無券
                                    if($data_spoint >= $sp_less){                           //幣全抵
                                        creatSubmitBtn('bid','spoint');
                                    }else{                                                  //幣不足
                                        creatSubmitBtn('deposit');
                                    }
                                }
                                break;
                            case 'kradio':
                                $osradio.prop('checked', false);
                                $spradio.prop('checked', false);
                                $sradio.prop('checked', true);
                                if($data_scode_num >= $data_count){                     //S全底
                                    creatSubmitBtn('bid','scode');
                                }else{
                                    creatSubmitBtn('losest');
                                }
                                break;
                        }
                        break;
                    case 'd':
                        switch($change){
                            case 'oradio':
                                $osradio.prop('checked', true);
                                $sradio.prop('checked', false);
                                if($data_oscode_num >= $data_count){                    //券全抵
                                    creatSubmitBtn('bid','oscode');
                                }else{
                                    creatSubmitBtn('loset');
                                }
                                break;
                            case 'kradio':
                                $osradio.prop('checked', false);
                                $sradio.prop('checked', true);
                                if($data_scode_num >= $data_count){                     //S全底
                                    creatSubmitBtn('bid','scode');
                                }else{
                                    creatSubmitBtn('losest');
                                }
                                break;
                        }
                        break;
                }
            })
        }($type));                                              //切換按鈕導向  
    
        $('#oscode_num').on('change keyup', function(){
            var $that = $(this);
            if($that.val() == ''){
                $that.addClass('empty');
            }else{
                $that.removeClass('empty');
            }
        })
    })
</script>
