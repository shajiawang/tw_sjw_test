<?php
$product = _v('product'); 
$status = _v('status');
$errMsg=_v('errMsg');
$errAct=_v('errAct');
$meta=_v('meta');
$cdnTime = date("YmdHis");
$bid_count=_v('bid_count');
$saja_record=_v('saja_record');
$user_limit = _v('user_limit');
$hostid = _v('hostid');

if(empty($bid_count)) {
   $bid_count=0;	
}

if(!empty($errMsg) && strlen(trim($errMsg))>1) {
	echo '<script>window.alert("'.$errMsg.'");'.$errAct.'</script>';
	unset($errMsg);
	unset($errAct);
}


if(!empty($product['thumbnail2'])) { 
	$img_src = BASE_URL.APP_DIR.'/images/site/product/'. $product['thumbnail2'];
} elseif (!empty($product['thumbnail_url'])) {
	$img_src = $product['thumbnail_url']; 
} 
error_log("[v/kuso_saja] user_limit:".$user_limit);
error_log("[v/kuso_saja] bid_count:".$bid_count);

?>
<!-- <?php echo $_SESSION['auth_id']; //var_dump($product);
?> -->
<input type="hidden" name="bid_count" id="bid_count" value="<?php echo $bid_count; ?>">
<input type="hidden" name="user_limit" id="user_limit" value="<?php echo $user_limit; ?>">

<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/keyboard.css">
<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">

<style>
    .swipe-navbar-content .content-item .sajabid-text {
        padding: 2rem 0 .1rem;
    }
    .productSaja-imgbox {
        width: 100%;
        background: #f6f6f6;
    }
    .sajaprod-img {
        max-width: 600px;
        margin: 0 auto;
    }
    #view_block {
        padding: 1.75rem 2.917rem 1.167rem;
        background: #FFF;
        border-bottom: .417rem solid rgb(249,249,249);
    }
    
    #bid_block {
        padding-bottom: 0;
    }
    
    .user_img {
        height: 3rem;
        width: auto;
        margin-right: 1rem;
        border-radius: 100vh;
    }
    .history-li {
        padding-bottom: .8rem;
        font-size: 1.2rem;
        line-height: 1.3rem;
    }
    .history-li + .history-li {
        border-top: 1px solid #DDD;
        padding: .8rem 0;
    }
/*
    .user-info {
        text-align: left;
        font-size: 1.2rem;
        word-break: break-all;
        line-height: 1.3rem;
    }
    .user-name {
        
    }
*/
    .bind-time {
        
    }
    .bind-price {
        
    }
    #bid_result {
        
    }
    .footernav.nopadding {
        padding: 0;
    }
    .footernav.nopadding .footernav-button button {
        margin: 3.334rem auto;
    }
    @media (min-width:630px) {
        .sajaprod-img {
            padding: 2rem 0;
        }
    }
    
    #actions {
        position: fixed;
        z-index: 1040;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
    }
    #actions .captchaBox {
        background: #FFF;
        z-index: 1042;
        border-radius: 1rem;
        padding: 0 1rem 2rem 1rem;
        margin-bottom: 3rem;
    }
    #actions .login-captcha-title {
        padding: 2.667rem 3.167rem 3.084rem;
    }
    #actions .login-captcha-title h1 {
        font-size: 1.667rem;
    }
    #actions .captcha-cover {
        position: fixed;
        z-index: 1041;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgb(0,0,0);
        background-color: rgba(0,0,0,.3);
        pointer-events: none;
    }
	.productSaja-imgbox .sajaprod-name {
		text-align: center;
		padding: .917rem 0rem 1rem;
		color: rgb(42, 42, 42);
		font-size: 1.5rem;
		font-weight: bold;
		line-height: 2rem;
	}
</style>

<?php
// 無商品時
if(empty($product) || empty($product['productid'])) {
?>
	<div class="nohistory-box"/>
		<div class="text-center">
			<div class="nohistory-img"/>
			   <img class="img-fluid" src="<?php echo APP_DIR;?>/static/img/nohistory-img.png" />
			<div class="nohistory-txt">目前無閃殺商品 !!</div>
			<p>&nbsp;</p>
			
			<div class="footernav">
				<div class="footernav-button">         
				<button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="window.location.href='<?php echo BASE_URL.APP_DIR;?>/index.php';">殺價王首頁</button>
				</div>
			</div>
		</div>
	</div>
<?php 
} else {
	
// 有商品時	
?>
		<div>
        <div id="actions" class="d-flex justify-content-center align-items-center">
            <div class="captchaBox d-flex flex-column justify-content-center align-items-center">
                <div class="login-title login-captcha-title">
                    <h1>輸入通關密碼</h1>
                </div>
				
				<div class="text-center">
					<div class=" sajabid-text">
						<div class="d-flex align-items-end justify-content-center">
							<input type="password" name="bidpass" id="bidpass" class=" allinput-type2" min="1" pattern="[0-9]*" step="1" autofocus placeholder=""/>
						</div>
					</div>
					<div class="footernav">
						<div id="bid_btn" class="footernav-button">
							<button type="submit" class="ui-btn ui-btn-a ui-corner-all" onClick="do_bid_pass('<?php echo $product['productid']; ?>','<?php echo $hostid; ?>')">提交</button>
						</div>
					</div>
				</div>
				
                <?php /*
				<div class="captcha-box captcha_keyboard">
                    <ul class="num-box d-flex justify-content-center">
                        <li class="d-flex justify-content-center align-items-center"><span></span></li>
                        <li class="d-flex justify-content-center align-items-center"><span></span></li>
                        <li class="d-flex justify-content-center align-items-center"><span></span></li>
                        <li class="d-flex justify-content-center align-items-center"><span></span></li>
                        <li class="d-flex justify-content-center align-items-center"><span></span></li>
                        <li class="d-flex justify-content-center align-items-center"><span></span></li>
                    </ul>
                    <input type="hidden" class="input-field numHideBox" id="captcha" name="captcha" value="">
                </div> 

                <!--  驗證訊息  -->
                 <ul class="captcha-msgError"></ul>
				 */?>
            </div>
            <div class="captcha-cover"></div>
        </div>    
    
        <div id="prod_info" class="productSaja-imgbox">
			<div class="productSaja-imgbox">
				<div class="sajaprod-img" >
					<ul class="d-flex">
						<li class="mx-auto">
							<img class="img-fluid"  src="<?php echo $img_src; ?>">
						</li>
				    </ul>
				</div>
			</div>
        	<!-- h2 class="overTime">
				<?php if (!empty($product['offtime'])){ ?>
					<p class="countdown" data-offtime="<?php echo $product['offtime']; ?>" data-tag="">00天 00時 00分 00秒 00</p>
				<?php }  //endif; ?>
			</h2 -->
            <!-- h4 class="sajaprod-name">目前商品</h4 -->
			<h4 class="sajaprod-name"><?php echo $product['name']; ?></h4>
			<h4 class="sajaprod-name">官方售價 : <?php echo intVal($product['retail_price']); ?> 元</h4>
		</div>
        <div class="swipe-navbar-content">    
			<div class="content-item on-active">
				<?php   if($_SESSION['auth_id']>0) {  ?>
				<div id="bid_block" class="item-box">
					<div  class="toptxt">
					   <span>
						 <?php echo urldecode($_SESSION['user']['profile']['nickname']); ?> , 請出價 : 
					   </span>
					</div>
					<div class="text-center">
						<div class="placeholder sajabid-text">
							<div class="d-flex align-items-end justify-content-center">
								<input type="tel" name="bid" id="bid" class="keyboard allinput-type2" min="1" pattern="[0-9]*" step="1" autofocus placeholder="請輸入要下標的金額"/>
							</div>
						</div>
						<div class="footernav">
						    <div id="bid_btn" class="footernav-button">
							     <button type="submit" class="ui-btn ui-btn-a ui-corner-all" onClick="kuso_saja('<?php echo $product['productid']; ?>')">確定出價</button>
						    </div>
                        </div>
					</div>
				</div>
               
				<?php  }  ?>
			</div>
		</div>
       
        <!-- 下標紀錄 -->
         <?php if(count($saja_record)>0) {  ?>
            <div id="view_block" class="item-box" >
                <div class="toptxt">
                <?php foreach($saja_record as $k=>$v) { ?>

                    <div class="history-li d-flex justify-content-center">
<!--
                        <?php if($_SESSION['user']['profile']['thumbnail_url']){ ?>
                            <img class="user_img" src="<?php echo $_SESSION['user']['profile']['thumbnail_url']; ?>" />
                        <?php }else{ ?>
                            <img class="user_img" src="<?php echo BASE_URL.APP_DIR;?>/static/img/order-default.png"/>
                        <?php } ?>
-->
<!--                        <div class="user-name"><?php echo urldecode($_SESSION['user']['profile']['nickname']); ?></div>-->
                        <div class="bind-time mr-auto"><span id="bid_time" ><?php echo $v['insertt']; ?></span></div>
                        <div class="bind-price">出價: <span id="bid_result" ><?php echo round($v['price']); ?></span>元</div>
                    </div>


                <?php  }   ?>
                </div>
            </div>
        <?php  }   ?>
         
         <div class="footernav nopadding">
            <div id="reload_btn" class="footernav-button">
                 <button type="button" class="ui-btn ui-btn-a ui-corner-all" onClick="window.location.reload();">更新</button>
            </div>
        </div>
		</div>
	
		<!--  CDN Bootstrap 4.1.1  -->
		<script src="<?php echo BASE_URL.APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
		<!--  鍵盤-自訂義JS  -->
		<script src="<?php echo BASE_URL.APP_DIR;?>/static/js/keyboard_v1.2.js"></script>

		<script src="<?php echo BASE_URL.APP_DIR;?>/static/js/_captcha_keyboard.js"></script>
		<!--auth_pass: <?php echo isset($_COOKIE['auth_pass'])?$_COOKIE['auth_pass']: 'N/A';?>-->
		<?php if($_SESSION['auth_id']>0 && (isset($_COOKIE['auth_pass']) && $_COOKIE['auth_pass']==$hostid) ) {  ?>
		<script>
		$(function(){
			$('#actions').remove();
		});
		</script>
		<?php } ?>

		<?php /*
		<script>
			var $wherekey,
				$open,
				$thisBtn,
				$much = $(".captcha_keyboard li").length;
			$(".captcha_keyboard").each(function(i,e){
				$(this)
					.attr("data-toggle","modal")
					.attr("data-target","#captcha_numkeyModal");
				$(this).on("click",function(e){
					//生成按鈕(畫面沒有小鍵盤才生成)
					if(!$("#captcha_numkeyModal").length > 0){
						keyModal();
					}else{
						$('#captcha_numkeyModal').remove();
					}
					var $domEle=$("#passport");

					//先儲存點選哪個輸入框
					$wherekey = $(".numHideBox");
					$open = $(this).data("target");

					e.stopPropagation;
					e.preventDefault;

					//將原先輸入框裡的值帶入鍵盤的暫存區
					if ($wherekey.val()!=''){
						$domEle.val(String($wherekey.val()));
					}else{
						$domEle.val('');
					}
					//控制輸入框位置
					//controlSize($(this));

					//計算
					_captcha_Calculation($wherekey,$much);

					//Event-關閉小鍵盤後的動作
					$('#captcha_numkeyModal').on('hide.bs.modal', function (e) {
						$('#captcha_numkeyModal').remove();
						$(".num-box li").find(".pic").remove();

						//removeBlank();

						//驗證
						var $tabNum = String($wherekey.val()).length,
							$error = $(".captcha-msgError"),
							$errorBox = $(".errorModal .modal-body ul");

						array = [];

						//判斷是否填滿所有格數
						if($tabNum == $much){
							if(array.length !== 0){
								$error.html(array.join(''));
								$errorBox.html(array.join(''));
								e.preventDefault;
								openErrorModal();
							}else{
								$error.html('');
								var osmscode = $('#captcha').val();
								//$_SESSION['user']['profile']['phone']
								$.ajax({
									url: "<?php echo BASE_URL.APP_DIR;?>/ajax/mall.php",
									data: {phone:"<?php echo $_SESSION['user']['profile']['phone'];?>",type:"checkexpwd", expw:osmscode, num:0},
									type:"POST",
									dataType:'JSON',

									success: function(msg){
									   var obj ;
									   // var obj = $.parseJSON(msg);
									   if((typeof msg)=="string") {
										  obj = JSON.parse(trim(msg));
									   } else if((typeof msg)=="object") {
										  obj = msg;
									   }
										//console.log("retCode : "+obj.retCode);
										if(obj.retCode == 1) {
											msg = '兌換密碼確認成功 !!';
											$('#actions').fadeOut(1000,'linear',function(){
												$(this).removeClass('d-flex').hide()
											});
										} else if(obj.retCode==-6){ 
											msg = '兌換密碼錯誤，請重新輸入'; 
										} 
										setError(msg);
										$error.css("display","block").html(array.join(''));
		//                                if(obj.retCode == 1) {
		//                                    mall_checkout();
		//                                }
									},
									error:function(){
										var obj ;
										// var obj = $.parseJSON(msg);
										if((typeof msg)=="string") {
										   obj = JSON.parse(trim(msg));
										} else if((typeof msg)=="object") {
										   obj = msg;
										}
										setError("兌換密碼確認失敗");
										$error.html(array.join(''));
										$errorBox.html(array.join(''));
										openErrorModal();
									}
								});
							}
						}else{
							setError("請填寫完整兌換密碼");
							$error.html(array.join(''));
						}
					})
					
				});
			})
			//防止小鍵盤沒吊起，每600毫秒判斷未吊起時重新點擊開啟
			var interval = null;
			$(function(){
				//$(".captcha_keyboard").click();
				interval = setInterval(Continuous,600);
			})
			//持續判斷小鍵盤
			function Continuous() {
				var $modal = $("#captcha_numkeyModal");
				if($modal.hasClass("show")){
					//已吊起，要移除持續判斷
					clearInterval(interval);
				}else{
					$(".captcha_keyboard").click();
				}
			}
			var array;
			function setError(msg){
				array.push('<li>'+msg+'</li>');
			};
		</script>
		*/?>

		<!--  小鍵盤動作  -->
		<script>
			options.dom.caller = '.keyboard';                   //命名:呼叫小鍵盤的物件
			options.dom.keyidName = 'numkeyModal';              //命名:小鍵盤(無id符號)
			options.dom.keyModal = '#' + options.dom.keyidName; //命名:小鍵盤(有id符號)
			options.dom.callerId = '#bid';                      //預設一開始的呼叫者,在keyboard.JS會做變更 點擊者
			options.btn.sure = '確定';                           //小鍵盤送出按鈕名稱
			
			$(function(){
				$(options.dom.caller).each(function(i){
					var $modalNum = options.dom.keyidName+i;
					//console.log($modalNum);
					var $that = $(this);
					$that.on("click",function(e){
						options.dom.callerId = $that;                           //記錄點擊哪個input的ID
						//開啟小鍵盤的動作
						//若預設開啟時，此段不會執行
						$('#'+$modalNum).on('show.bs.modal', function (e) {
							controlSize($that);
						});
						
						//關閉小鍵盤的動作
						$('#'+$modalNum).on('hide.bs.modal', function (e) {
							removeBlank();
						});
					})
				});
			})
		</script>
	
		<script type="text/javascript">
			var chkprodid="<?php echo $product['productid']; ?>";
			var timer;
           	$(document).ready(function() {
			    $.mobile.loading('show', {text: '準備中..', textVisible: true, theme: 'b', html: ""});
				clearInterval(timer);
				$.ajaxSetup({ cache: false });
								
				// 下標次數
				var bid_count=parseInt($('#bid_count').val());
				
                // 標數限制
				var user_limit=parseInt($('#user_limit').val());
                
				$('#prod_info').show();
                if(bid_count<0) {
                    $('#view_block').hide();
                    $('#bid_block').hide();                   
				} else if(bid_count>=0 && bid_count<user_limit) {
                    $('#view_block').show();
                    $('#bid_block').show();
                    $('#reload_btn').hide();
                } else if(bid_count>=user_limit) {
					$('#view_block').show();
                    $('#bid_block').hide();
                    $('#reload_btn').show();
				} 
                $.mobile.loading('hide');
            });
            
            $(function(){
                $('.hasFooter').removeClass('hasFooter');
            })
        
        function kuso_saja(pid) {
	          kuso_bid_1(pid);
        }
        
        
        function kuso_bid_1(pid) {
            var app_url = "/site/kusosaja/?hostid=<?php echo $hostid;?>&_t="+(new Date()).getTime();
			var rightid;
            for(i=0; i<$('[data-role="panel"]').length; i++)	{
                var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
                if(rtemp.match("right-panel-") == "right-panel-") {
                    rightid = rtemp;
                }
            }
            
            var pageid = $.mobile.activePage.attr('id');
            // var opaytype = $('#'+pageid+' #pay_type :radio:checked').val();
            var opaytype = 'spoint';
            var obid = $('#'+pageid+' #bid').val();
            
            if(obid == "") {
                alert('單次出價不能空白');
                return false;
            }
            else if(isNaN(Number(obid))==true) {
                alert('出價必須為數字'); 
                 window.location.href=app_url;               
            }
            else if (Number(obid) < 1) {
                alert('單次出價不能小於1');
                 window.location.href=app_url;
            }
            else if (!obid.match("^[0-9]*[1-9][0-9]*$")) {
                alert('價格最小單位為整數');
                 window.location.href=app_url;
            }
            else
            {
                $.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});
                obid=parseInt(obid);
                var bidData = {
                    type: "single",
                    productid: pid,
                    price: obid,
                    pay_type: opaytype,
                    use_sgift: 'N'
                };
                // alert(JSON.stringify(bidData));
                $.post(APP_DIR+"/ajax/product.php", 
                       bidData, 
                       function(str_json){
                            if(str_json)
                            {
                                $.mobile.loading('hide');
                                // console && console.log(str_json);
                                
                                var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
                                                        
                                if (json.status==1) {
                                    alert('請先登入');
                                    location.href=APP_DIR+"/member/userlogin/";
                                } else if (json.status < 1) {
                                    alert('單次出價失敗');
                                } else {
                                    var msg = '';
                                    var msg2 = '';
                                    if (json.status=='200'){ 
                                        if (json.rank) {
                                            if (json.rank==-1) {
                                                msg="很抱歉！這個出價與其他人重複了！\n請您試試其他出價或許會有好運喔！";
                                            } else if(json.rank==0) {
                                                //msg = "恭喜！您是目前的得標者！\n記得在時間到之前要多布局！";
                                                msg = "恭喜！您是目前的得標者！";
                                            } else if (json.rank>=1 && json.rank<=10) {
                                                // msg = ' , 本次下標順位為第 ' + json.rank + ' 名'; 
                                                msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的還有 "+(json.rank)+" 位。";
                                            } else if (json.rank>10) {
                                                // msg = ' , 本次下標順位超過第 12 名或與他人重覆下標';
                                                msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
                                            }
                                        }
                                        alert("出價 "+obid+"元 成功\n"+ msg);                                 
                                    } 
                                    else if(json.status==2){ alert('這檔商品已結標囉'); } 
                                    else if(json.status==3){ alert('下標間隔過短，請稍後再下'); } 
                                    else if(json.status==4){ alert('金額記得填!'); } 
                                    else if(json.status==5){ alert('金額格式錯誤'); } 
                                    else if(json.status==6){ alert('金額低於底價'); } 
                                    else if(json.status==7){ alert('已經超過連續下標時間囉!'); }
                                    else if(json.status==8){ alert('超過下標次數限制'); }
                                    else if(json.status==9){ alert('價格請由低填到高!'); }
                                    else if(json.status==10){ alert('金額超過商品市價'); }
                                    else if(json.status==11){ alert('超過可下標次數限制!'); }
                                    else if(json.status==12){ alert('殺價券不足!'); }
                                    else if(json.status==13){ alert('殺價幣不足，請充值!'); }
                                    else if(json.status==14){ alert('殺價券不足!'); }
                                    else if(json.status==15){ alert('限新 手(未得標過者)'); }
                                    else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
                                    else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
                                    
                                }
                            }
                            $.mobile.loading('hide');
                            window.location.href=app_url;
                        });
            }
            
        }
		
		function do_bid_pass(pid,hid) {
            var app_url = '/site/kusosaja/?_t='+(new Date()).getTime();
			var obid = $('#bidpass').val();
            
            if(obid == "") {
                alert('密碼不能空白');
                return false;
            }
            else if(isNaN(Number(obid))==true) {
                alert('密碼必須為數字'); 
				$('#bidpass').val('');
                return false; //window.location.href= app_url +'&hostid='+hid;               
            }
            else
            {
                $.mobile.loading('show', {text: '處理中', textVisible: true, theme: 'b', html: ""});
                
                var bidData = {
                    type: "single",
                    productid: pid,
                    hostid: hid,
					pass: obid
                };
                
				//alert(JSON.stringify(bidData));
				$.post(APP_DIR+"/kusosaja/check_pass/", 
                    bidData, 
                    function(str_json){
                        if(str_json){
                                $.mobile.loading('hide');
                                console && console.log(str_json);
                                var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
                                
                                if (json.retCode==0) {
                                    alert('帶入的資料錯誤 !');
									$('#bidpass').val('');
                                    return false;
                                } else if (json.retCode < 1) {
                                    alert('密碼錯誤 !');
									$('#bidpass').val('');
									return false;
                                } else if (json.retCode=='1' && json.retMsg=='OK') {
                                    $('#actions').remove();
									// alert("驗證成功 !");                                 
                                }
                            }
                            $.mobile.loading('hide');
                    });
				
            }
            
        }
		</script>

<?php } ?>