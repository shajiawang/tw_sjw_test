<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
?>
<style>
    body {
        background: #FFF;
        height: 100vmax;
        min-height: unset;
    }
    /*取消隱藏FooterBar*/
    .sajaFooterNavBar {
        display: unset;
    }
</style>

<?php if(!empty($_SESSION['sajamanagement']['enterprise']['enterpriseid'])){ ?>
<div class="article">
	<ul class="user-lists-box">
        <!-- 商戶資料 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/member/?<?php echo $cdnTime; ?>'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png">
                    </div>
                    <div class="list-title">商戶資料</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li>

        <!-- 商戶資料修改 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/member_update/?<?php echo $cdnTime; ?>'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png">
                    </div>
                    <div class="list-title">商戶資料修改</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li>

        <!-- 交易紀錄 -->
        <!-- <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/storebonuslist/?<?php echo $cdnTime; ?>'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/all list_r.png">
                    </div>
                    <div class="list-title">交易紀錄</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li> -->

        <!-- 閃殺商品 -->
        <!-- <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/storeproduct/?flash=Y&<?php echo $cdnTime; ?>'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/prodList_0.png">
                    </div>
                    <div class="list-title">閃殺商品</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li> -->

        <!-- 商戶提現 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center"
               <?php if($_SESSION['sajamanagement']['enterprise']['sms'] == 'Y'){ ?>
                   onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/cash/?<?php echo $cdnTime; ?>'"
               <?php } else { ?>
                   onclick="javascript:alert('請先至商戶資料中心，進行手機驗證!!')"
               <?php } ?>>
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/spoints.png">
                    </div>
                    <div class="list-title">商戶提現</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li>

        <!-- 退出 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/logout/?<?php echo $cdnTime; ?>'" <?php if ($browser == 1){ ?>target="_blank"<?php } ?>>
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/exit.png">
                    </div>
                    <div class="list-title">退出</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li>

    </ul>
</div><!-- /article -->
<?php }  ?>



<!-- 2018/10/15 舊檔 -->
<!--
	<div class="weui-cells2">

		<?php if(!empty($_SESSION['sajamanagement']['enterprise']['enterpriseid'])){ ?>
		<a class="weui-cell weui-cell_access" href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/member/?<?php echo $cdnTime; ?>" >
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png" style="width:20px;height:20px" ></div>
			<div class="weui-cell__bd">
			  <h5><p style="font-weight:normal;color:#696969">商户信息</p></h5>
			</div>
			<div class="weui-cell__ft"> </div>
		</a>

		<a class="weui-cell weui-cell_access" href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/member_update/?<?php echo $cdnTime; ?>" >
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png" style="width:20px;height:20px"></div>
			<div class="weui-cell__bd">
			  <h5><p style="font-weight:normal;color:#696969">商户信息修改</p></h5>
			</div>
			<div class="weui-cell__ft"> </div>
		</a>

		<a class="weui-cell weui-cell_access" href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/storebonuslist/?<?php echo $cdnTime; ?>" >
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/all list_r.png" style="width:20px;height:20px"></div>
			<div class="weui-cell__bd">
			  <h5><p style="font-weight:normal;color:#696969">交易纪录</p></h5>
			</div>
			<div class="weui-cell__ft"> </div>
		</a>

		<a class="weui-cell weui-cell_access" href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/storeproduct/?flash=Y&<?php echo $cdnTime; ?>">
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/prodList_0.png" style="width:20px;height:20px"></div>
			<div class="weui-cell__bd">
			  <h5><p style="font-weight:normal;color:#696969">闪殺商品</p></h5>
			</div>
			<div class="weui-cell__ft"> </div>
		</a>

		<?php if($_SESSION['sajamanagement']['enterprise']['sms'] == 'Y'){ ?>
		<a class="weui-cell weui-cell_access" href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/cash/?<?php echo $cdnTime; ?>">
		<?php } else { ?>
		<a class="weui-cell weui-cell_access" href="#" onclick="javascript:alert('請先至商户信息中心，進行手機驗證!!')">
		<?php } ?>
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/spoints.png" style="width:20px;height:20px"></div>
			<div class="weui-cell__bd">
			  <h5><p style="font-weight:normal;color:#696969">商户提現</p></h5>
			</div>
			<div class="weui-cell__ft"> </div>
		</a>

		<a class="weui-cell weui-cell_access" href="<?php echo BASE_URL.APP_DIR; ?>/enterprise/logout/?<?php echo $cdnTime; ?>"  <?php if ($browser == 1){ ?>target="_blank"<?php } ?>" >
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/exit.png" style="width:20px;height:20px"></div>
			<div class="weui-cell__bd">
			  <h5><p style="font-weight:normal;color:#696969">退出</p></h5>
			</div>
			<div class="weui-cell__ft"> </div>
			</a>
		</a>
		<?php }  ?>

	</div>
-->
<!-- 2018/10/15 舊檔/ -->
