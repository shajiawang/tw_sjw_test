<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$enterprise=$_SESSION['sajamanagement']['enterprise'];
$share_url = _v('share_url');
$total_net_bonus=_v('total_net_bonus');
?>
<div class="article">
	<div class="user-header">
        <div class="userinfo">
            <div class="user-img">
<!--                <a href="#" onclick="$('#logo').trigger('click'); return false;" >-->
                <img class="userinfo-avatar" src="<?php echo $enterprise['thumbnail']; ?>" background-size="cover">
<!--                </a>-->
            </div>
            <div class="user-name">
                <p><?php echo $enterprise['exchange_store_name']; ?></p>
            </div>
		</div>
    </div>
	<ul class="user-lists-box">
        <!-- 支付專屬二維碼 -->
        <!--li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/shareQrcode/?kind=pay&<?php echo $cdnTime; ?>'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/qrcode2.png">
                    </div>
                    <div class="list-title">支付專屬二維碼</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li-->

        <!-- 分享專屬二維碼 -->
        <!--li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/shareQrcode/?kind=share&<?php echo $cdnTime; ?>'">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/qrcode2.png">
                    </div>
                    <div class="list-title">分享專屬二維碼</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="r-arrow"></div>
                </div>
            </a>
        </li-->

        <!-- 商戶ID -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0)">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">商戶ID</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['enterpriseid']; ?></div>
                    </div>
                </div>
            </a>
        </li>

        <!-- 店家名稱 -->
        <!-- li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0)">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">店家名稱</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['exchange_store_name']; ?></div>
                    </div>
                </div>
            </a>
        </li -->

        <!-- 鯊魚點數 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0)">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">鯊魚點數</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo sprintf("%.2f", $total_net_bonus); ?> 點</div>
                    </div>
                </div>
            </a>
        </li>

        <!-- 負責人 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0)">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">負責人</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['owner']; ?></div>
                    </div>
                </div>
            </a>
        </li>

        <!-- 聯絡電話 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0)">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">聯絡電話</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['phone']; ?></div>
                    </div>
                </div>
            </a>
        </li>

        <!-- URL -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">URL</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['url']; ?></div>
                    </div>
                </div>
            </a>
        </li>

        <!-- email -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0);">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">Email</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['email']; ?></div>
                    </div>
                </div>
            </a>
        </li>

        <!-- Fax -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0);">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">FAX</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['fax']; ?></div>
                    </div>
                </div>
            </a>
        </li>

        <!-- 地址 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onclick="javascript:void(0);">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png">
                    </div>
                    <div class="list-title">地址</div>
                </div>
                <div class="list-rtxt d-inline-flex">
                    <div class="nor-arrow">
                        <div class="rtxt-num"><?php echo $enterprise['address']; ?></div>
                    </div>
                </div>
            </a>
        </li>
    </ul>

		<div data-role="popup" id="weixinpopupParis" data-overlay-theme="b" data-theme="b" data-corners="false">
			<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
			<img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=http://www.shajiawang.com/site/mall/topay/?esid=<?php echo $enterprise['esid'];?>" style="max-height:290px;"
				  alt="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=http://www.shajiawang.com/site/mall/topay/?esid=<?php echo $enterprise['esid'];?>">
		</div>
		<div data-role="popup" id="weixinpopupParis2" data-overlay-theme="b" data-theme="b" data-corners="false">
			<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
			<img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo $share_url; ?>" style="max-height:290px;"
				 alt="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo $share_url; ?>">
		</div>

</div><!-- /article -->


<!-- 2018/10/15 舊檔 -->
<!--
    <div class="article">
        <div class="userinfo">
			<div style="text-align:center;">
				<img class="userinfo-avatar" src="<?php echo $enterprise['thumbnail']; ?>"  background-size="cover" width="120px" height="120px">
			</div>
		</div>

		<div class="weui-cells">
			<a class="weui-cell weui-cell_access" href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/shareQrcode/?kind=pay&<?php echo $cdnTime; ?>'" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/qrcode2.png" style="height:20px;"></div>
				<div class="weui-cell__bd">
				   <h5><p style="font-weight:normal;color:#696969">支付專屬二維碼</p></h5>
				</div>
				<div class="weui-cell__ft"> </div>
			</a>

			<a class="weui-cell weui-cell_access" href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/enterprise/shareQrcode/?kind=share&<?php echo $cdnTime; ?>'" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/qrcode2.png" style="height:20px;"></div>
				<div class="weui-cell__bd">
				   <h5><p style="font-weight:normal;color:#696969">分享专属二维碼</p></h5>
				</div>
				<div class="weui-cell__ft"> </div>
			</a>

			<a class="weui-cell" href="#" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px" ></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">商户ID：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['enterpriseid']; ?></div>
			</a>

			<a class="weui-cell" href="#"  >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">店家名称：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['exchange_store_name']; ?></div>
			</a>

			<a class="weui-cell" href="#" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">鯊魚點數：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo sprintf("%.2f", $total_net_bonus); ?> 點</div>
			</a>

			<a class="weui-cell" href="#" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">负责人：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['owner']; ?></div>
			</a>

			<a class="weui-cell" href="#" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">联系电话：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['phone']; ?></div>
			</a>

			<a class="weui-cell" href="#" onclick="javascript:location.href='<?php echo $enterprise['url']; ?>'" target="_blank" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">URL：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['url']; ?></div>
				</a>
			</a>

			<a class="weui-cell" href="#" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">Email：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['email']; ?></div>
				</a>
			</a>

			<a class="weui-cell" href="#" >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">FAX：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['fax']; ?></div>
				</a>
			</a>

			<a class="weui-cell" href="#"  >
				<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/rule.png" style="width:20px;height:20px"></div>
				<div class="weui-cell__bd">
				  <h5><p style="font-weight:normal;color:#696969">地址：</p></h5>
				</div>
				<div class="weui-cell__ft mall-font2"><?php echo $enterprise['address']; ?></div>
				</a>
			</a>

		</div>

		<div data-role="popup" id="weixinpopupParis" data-overlay-theme="b" data-theme="b" data-corners="false">
			<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
			<img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=http://www.shajiawang.com/site/mall/topay/?esid=<?php echo $enterprise['esid'];?>" style="max-height:290px;"
				  alt="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=http://www.shajiawang.com/site/mall/topay/?esid=<?php echo $enterprise['esid'];?>">
		</div>
		<div data-role="popup" id="weixinpopupParis2" data-overlay-theme="b" data-theme="b" data-corners="false">
			<a href="#" data-rel="back" class="ui-btn ui-corner-all ui-shadow ui-btn-a ui-icon-delete ui-btn-icon-notext ui-btn-right">Close</a>
			<img class="popphoto" src="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo $share_url; ?>" style="max-height:290px;"
				 alt="<?php echo BASE_URL . APP_DIR;?>/phpqrcode/?data=<?php echo $share_url; ?>">
		</div>

    </div>
-->
<!-- 2018/10/15 舊檔/ -->
