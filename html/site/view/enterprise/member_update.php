<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$profile=_v('enterprise_profile');
$account=_v('enterprise_account');
$shop=_v('enterprise_shop');
$enterprise=_v('enterprise');
?>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/croppie.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/exif.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/megapix-image.js"></script>
<script type="text/javascript" src="<?php echo BASE_URL.APP_DIR;?>/js/weui_loading.js"></script>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/croppie.css" rel="stylesheet"/>
<link href="<?php echo BASE_URL.APP_DIR;?>/css/weui_loading.css" rel="stylesheet"/>
<style >
    span.infotext {
        line-height: 1.6rem !important;
    }

</style>
<div id="pcd1">
<form method="post" action="" id="contactform" name="contactform">
    <ul class="user-lists-box">
        <!-- 修改登入密碼 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('m1');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png">
                    </div>
                    <div class="list-title">修改登入密碼</div>
                </div>
            </a>
        </li>
        <div id="m1" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center required">
                    <div class="title">登入密碼</div>
                    <input class="infotxt" name="passwd" id="passwd" value="" type="password">
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">確認登入密碼</div>
                    <input class="infotxt" name="confrim_passwd" id="confrim_passwd" value="" type="password" placeholder="以4~12个英文字母或數字為限" >
                </div>
            </div>
            <div class="switch-btn">
                <button type="button" id="u1" onClick="userenterprisepw();">提交</button>
            </div>
        </div>

        <!-- 修改提現密碼 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('m2');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png">
                    </div>
                    <div class="list-title">修改提現密碼</div>
                </div>
            </a>
        </li>
        <div id="m2" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center required">
                    <div class="title">提現密碼</div>
                    <input class="infotxt" name="expasswd" id="expasswd" value="" type="password">
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">確認提現密碼</div>
                    <input class="infotxt" name="confrim_expasswd" id="confrim_expasswd" value="" type="password" placeholder="以4~12个英文字母或數字為限" >
                </div>
            </div>
            <div class="switch-btn">
                <button type="button" id="u2" onClick="userenterpriseexpw();">提交</button>
            </div>
        </div>

        <!-- 營業信息 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('m3');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png">
                    </div>
                    <div class="list-title">營業信息</div>
                </div>
            </a>
        </li>
        <div id="m3" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center required">
                    <div class="title">店家名稱</div><span class="infotext"><?php echo $shop['name']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">負責人</div><span class="infotext"><?php echo $shop['owner']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">店家電話</div>
                    <input class="infotxt" name="contact_phone" id="contact_phone" value="<?php echo $shop['contact_phone'];?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">店家手機</div>
                    <input class="infotxt" name="contact_mobile" id="contact_mobile" value="<?php echo $shop['contact_mobile'];?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">分潤比</div><span class="infotext"><?php echo $shop['profit_ratio'];?>%</span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">店家網址</div>
                    <input class="infotxt" name="url" id="url" value="<?php echo $shop['url'];?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">聯絡信箱</div>
                    <input class="infotxt" name="contact_email" id="contact_email" value="<?php echo $shop['contact_email'];?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">營業開始時間</div>
                    <input class="infotxt" name="businessontime" id="businessontime" value="<?php echo $shop['businessontime'];?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">營業結束時間</div>
                    <input class="infotxt" name="businessofftime" id="businessofftime" value="<?php echo $shop['businessofftime'];?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">客服信箱</div>
                    <input class="infotxt" name="service_email" id="service_email" value="<?php echo $shop['service_email'];?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">營業年資</div>
                    <span class="infotext"><?php echo $shop['seniority'];?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">員工人數</div>
                    <span class="infotext"><?php echo $shop['employees'];?></span>
                </div>
            </div>
        </div>

        <!-- 企業信息 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('m4');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png">
                    </div>
                    <div class="list-title">企業信息</div>
                </div>
            </a>
        </li>
        <div id="m4" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center required">
                    <div class="title">企業名稱</div>
                    <span class="infotext"><?php echo $profile['companyname']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">對外名稱</div>
                    <span class="infotext"><?php echo $profile['marketingname']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">登記證號</div>
                    <span class="infotext"><?php echo $profile['uniform']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">負責人</div>
                    <span class="infotext"><?php echo $profile['name']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">聯絡電話</div>
                    <span class="infotext"><?php echo $profile['phone']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">Email</div>
                    <input class="infotxt" name="email" id="email" value="<?php echo $profile['email']; ?>" type="text">
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">FAX</div>
                    <span class="infotext"><?php echo $profile['fax']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">公司地址</div>
                    <span class="infotext"><?php echo $profile['address']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">公司圖片</div>
                    <div class="infotxt">
                        <a href="#" onclick="$('#logo').trigger('click'); return false;" >
                        <?php if (!empty($profile['thumbnail_file'])) { ?>
                            <img name="lpic" id="lpic" height="50px" width="50px" src="<?php echo BASE_URL;?>/management/images/headimgs/<?php echo $profile['thumbnail_file'];?>?t=<?php echo $cdnTime;?>" background-size="cover">
                        <?php } else { ?>
                            <img class="userinfo-avatar" src="<?php echo BASE_URL;?>/management/images/headimgs/_DefaultShop.jpg" background-size="cover">
                        <?php } ?>
                        </a>
                    </div>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">公司圖片連結</div>
                    <input class="infotxt" name="picurl" id="picurl" value="<?php echo $profile['thumbnail_url']; ?>" type="text">
                </div>
            </div>
        </div>

        <!-- 帳務信息 (提現必填) -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('m5');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png">
                    </div>
                    <div class="list-title">帳務信息 (提現必填)</div>
                </div>
            </a>
        </li>
        <div id="m5" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center required">
                    <div class="title">帳務負責人</div>
                    <span class="infotext"><?php echo $account['account_contactor']; ?></span>

                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">帳務負責人電話</div>
                    <span class="infotext"><?php echo $account['account_phone']; ?></span>

                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">營運人</div>
                    <span class="infotext"><?php echo $account['execute_contactor']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">營運人電話</div>
                    <span class="infotext"><?php echo $account['execute_phone']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">撥款帳戶名</div>
                    <span class="infotext"><?php echo $account['accountname']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">撥款帳號</div>
                    <span class="infotext"><?php echo $account['accountnumber']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">帳戶銀行名稱</div>
                    <span class="infotext"><?php echo $account['bankname']; ?></span>
                </div>
                <div class="infobox d-flex align-items-center required">
                    <div class="title">帳戶銀行分行名稱</div>
                    <span class="infotext"><?php echo $account['branchname']; ?></span>
                </div>
            </div>
        </div>

    </ul>
    <input name="enterpriseid" id="enterpriseid" value="<?php echo $profile['enterpriseid']; ?>" type="hidden">
    <div style="visibility:hidden">
        <input name="logo" id="logo" class="uploader__input" type="file" accept="image/*" placeholder="" >
    </div>
    <input type="hidden" name="spic" id="spic" value="<?php echo $profile['thumbnail_file']; ?>">
    <input type="hidden" name="oldpic" id="oldpic" value="<?php echo $profile['thumbnail_file']; ?>">
    <div class="footernav">
        <div class="switch-btn">
            <button type="button" onClick="userenterprise();">提交</button>
        </div>
    </div>
</form>
</div>
<div id="actions" style="display:none;">
    <div class="pic-box">
        <div id="new_logo" style="text-align: center;"></div>
    </div>
    <canvas id="cvs" style="display:none"></canvas>

    <div class="btn-box">
        <div class="rotate-group mx-auto d-flex">
            <button onclick="rotate_left()">向左旋轉90度</button>
            <button onclick="rotate_right()">向右旋轉90度</button>
        </div>
        <div class="submit-group mx-auto">
            <button onclick="save_pic()">儲存</button>
            <button onclick="cancel_edit()">取消</button>
        </div>
    </div>
</div>

<!-- 2018/10/15 舊檔/ -->
<script type="text/javascript">
    $(function(){
        //更改標題
        $('.navbar-brand h3').text('商戶資料修改');
    });

    //調整標題寬度一致
    var options = {
        dom: {
            parent: '.switch-box',
            ul: '.switch-body',
            li: '.infobox',
            label: '.title',
            input: 'input.infotxt'
        }
    };
    $(window).on('load',function(){

    })
    $(function(){
        //目前顯示的ul區塊
        var $isShow = $(options.dom.parent);

        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));

            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];

            $liW = $('.user-list').width();                         //取得li總寬度
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                var $fs = parseFloat($(this).css('font-size'));
                $arr.push($(this).text().length * ($fs + 1.5) + 14);    //+2為字距空間 , +14 預留冒號空間
                //console.log($arr);
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值
//console.log($liW)
            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                $selfInput.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                        $(this).css('width',$remain);

                    }
                })
            })
        }
        $isShow.each(function(){
            changeWidth($(this));
        })
        //尺寸改變重新撈取寬度
        $(window).on('load resize',function(){
            $isShow.each(function(){
                changeWidth($(this));
            })
        })

    })

</script>


<script type="text/javascript">

// function uploadFile1(n){
    // var formName = "contactform";                        //表單名稱
    // var prevImg = "preUpLoadImg";                        //顯示圖片ID
    // var upFloder = "../../management/images/headimgs/";      //上傳目錄名稱
    // var rePicName = n;                               //回傳圖片上傳名稱
    // var subName ="jpg,png,gif";                  //可上傳副檔名
    // var maxSize = 1024;                          //檔案限制大小
    // var winTitle = "fileUpload";                     //視窗名稱
    // var winWidth = 400;                          //視窗寬
    // var winHeight = 150;　                            //視窗高

    // window.open('<?php echo BASE_URL . APP_DIR;?>/ImgUp/upload_c.php?formName='+formName+'&prevImg='+prevImg+'&url=http://www.shajiawang.com/management/images/headimgs/&enterpriseid=<?php echo $profile['enterpriseid']; ?>&upFloder='+upFloder+'&rePicIpt='+rePicName+'&subName='+subName+"&maxSize="+maxSize,winTitle,'width='+winWidth+',height='+winHeight+'');
// }

 // $("#pic").change(function(){
      // readImage( this );

        // var formData = new FormData();
        // formData.append('pic', $('#pic')[0].files[0]);
        // formData.append('formName', 'contactform');                      //表單名稱
        // formData.append('prevImg', 'preUpLoadImg');                      //顯示圖片ID
        // formData.append('upFloder', '../../management/images/headimgs/');        //上傳目錄名稱
        // formData.append('rePicName', 'pic');                                 //回傳圖片上傳名稱
        // formData.append('subName', 'jpg,png,gif,jpeg');                  //可上傳副檔名
        // formData.append('maxSize', '1024');                          //檔案限制大小
        // formData.append('url', '<?php echo BASE_URL; ?>/management/images/headimgs/');
        // formData.append('rePic', '400');
        // formData.append('allowSubName', 'jpg,png,gif,jpeg');　

        // $.ajax({
            // url : '/site/ImgUp/upload_c2.php?upload=true',
            // type : 'POST',
            // data : formData,
            // processData: false,  // tell jQuery not to process the data
            // contentType: false,  // tell jQuery not to set contentType
            // success : function(data) {
                // console.log(data);
                // var json = $.parseJSON(data);
                // console && console.log($.parseJSON(data));

                // if (json.status == 1){
                    // $('#spic').attr("value", json.pic);
                    // $('#pic').attr("value", json.pic);
                    // document.getElementById('spic').value = json.pic;
                // } else {
                    // alert(json.msg);
                    // return false;
                // }
            // }
        // });
    // });

    // function readImage(input) {
      // if ( input.files && input.files[0] ) {
        // var FR= new FileReader();
        // FR.onload = function(e) {
          // $('#preUpLoadImg').attr( "src", e.target.result );
        // };
        // FR.readAsDataURL( input.files[0] );
      // }
    // }


$("#logo").unbind("change").on("change",function()  {
    lnv.pageloading();
    var file = document.querySelector('#logo').files[0];
    if(file){
        //验证图片文件类型
        if(file.type && !/image/i.test(file.type)){
            return false;
        }
        var reader = new FileReader();
        reader.onload = function(e){
            //readAsDataURL后执行onload，进入图片压缩逻辑
            //e.target.result得到的就是图片文件的base64 string
            render(file,e.target.result);
        };
        //以dataurl的形式读取图片文件
        reader.readAsDataURL(file);
    }
});

//定义照片的最大高度
var MAX_HEIGHT = 480;
var render = function(file,src){
    EXIF.getData(file,function(){
        //获取照片本身的Orientation
        var orientation = EXIF.getTag(this, "Orientation");
        var image = new Image();
        image.onload = function(){
            var cvs = document.getElementById("cvs");
            var w = image.width;
            var h = image.height;
            //计算压缩后的图片长和宽
            if(h>MAX_HEIGHT){
                w *= MAX_HEIGHT/h;
                h = MAX_HEIGHT;
            }
            //使用MegaPixImage封装照片数据
            var mpImg = new MegaPixImage(file);
            //按照Orientation来写入图片数据，回调函数是上传图片到服务器
            mpImg.render(cvs, {maxWidth:w,maxHeight:h,orientation:orientation}, sendImg);
        };
        image.src = src;
    });
};

//上传图片到服务器
var sendImg = function(){
    var cvs = document.getElementById("cvs");
    //调用Canvas的toDataURL接口，得到的是照片文件的base64编码string
    var data = cvs.toDataURL("image/png");
    //base64 string过短显然就不是正常的图片数据了，过滤の。
    if(data.length<48){
        console.log("data error.");
        return;
    }

    var formData = new FormData();
    //formData.append('pic', reader.result );
    formData.append('pic', data );
    formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
    formData.append('pic_name','logotemp');
    formData.append('filedir','management/images/headimgs/sp/');

    $.ajax(
      {
      url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c3.php',
      type : 'POST',
      data : formData,
      processData: false,  // tell jQuery not to process the data
      contentType: false,  // tell jQuery not to set contentType
      success : function(data)
        {
          load_img();
        }
      }
    );
};


function load_img()
{
  destory_img();

  var el = document.getElementById('new_logo');
    vanilla = new Croppie(el, {
    viewport: { width: 196, height: 196 },
    boundary: { width: 300, height: 300 },
    showZoomer: true,
    enableOrientation: true
  });

  vanilla.bind({
      url: '<?php echo BASE_URL;?>/management/images/headimgs/sp/_'+<?php echo $_SESSION['auth_id'];?>+'_logotemp.png?t=' + new Date().getTime(),
  });

  document.getElementById('pcd1').style.display = "none";
  document.getElementById('actions').style.display = "inline";
  lnv.destroyloading();
}

function destory_img()
{
  document.getElementById('new_logo').innerHTML="";
}

function cancel_edit()
{
  location.reload();
}

function page_reload()
{
  location.reload();
}

function rotate_right()
{
   vanilla.rotate(90);
}

function rotate_left()
{
   vanilla.rotate(-90);
}

function save_pic()
{
    var pic = vanilla.result('canvas','viewport');
    console.log(pic);

    pic.then(function(value){

    console.log(value);

    var formData = new FormData();
    formData.append('pic',value);
    formData.append('userid','<?php echo $_SESSION['auth_id'];?>');
    formData.append('pic_name','logo');
    formData.append('filedir','management/images/headimgs/');

    $.ajax({
        url : '<?php echo BASE_URL.APP_DIR;?>/ImgUp/upload_c3.php',
        type : 'POST',
        data : formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,  // tell jQuery not to set contentType
        success : function(data)
            {
                var tf = document.getElementById('spic').value="_<?php echo $_SESSION['auth_id'];?>_logo.png";
                //console.log(tf);
                // change_db_data();
                // page_reload();


                $('#lpic').attr( "src", '<?php echo BASE_URL;?>/management/images/headimgs/_'+<?php echo $_SESSION['auth_id'];?>+'_logo.png?t=' + new Date().getTime());
                  document.getElementById('pcd1').style.display = "inline";
                  document.getElementById('actions').style.display = "none";
                  lnv.destroyloading();
            }
        });
    });
}

function change_db_data() {
  $.ajax({
    // url: "<?php echo BASE_URL.APP_DIR;?>/user/edit_user_logo/?web=Y",
    url: "<?php echo BASE_URL.APP_DIR;?>/user/edit_user_logo/?web=Y",
    data: $('#contactform4').serialize(),
    type:"POST",
    dataType:'text',
    success: function(msg){
      var obj = jQuery.parseJSON(msg);
      alert( obj.retMsg );
    },
    error:function(){
      var obj = jQuery.parseJSON();
      alert( obj.retMsg );
    }
  });
}
</script>