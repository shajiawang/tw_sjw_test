<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$member_data = _v('member_data');
$product_data = _v('product_data');
?>
<style>
    .product-edit-box {
        background: #FFF;
        margin: .8rem auto 0;
    }
    .product-edit-box .label {
        margin-right: 10px;
    }
    .product-edit-box .label::after {
        content: ' : ';
    }
    .input_group .infotxt.txt {
        padding: .667rem;
        margin: .667rem 0;
        height: 1.25rem;
    }
    .input_group input.infotxt {
        flex: 1;
        border: 1px solid #ccc;
        padding: 10px;
        border-radius: 5px;
        box-sizing: border-box;
        margin: .667rem 0;
    }
    .input_group .infotxt.file-box {
        flex: 1;
        margin: .667rem 0;
    }
    .file-box * {
        display: block;
    }
    .file-box img {
        width: 100px;
        margin-bottom: 10px;
    }
</style>

<form method="post" action="" id="contactform" name="contactform" enctype="multipart/form-data">
    <div class="product-edit-box">
        <div class="input_group d-flex align-items-center">
            <div class="label">店家名稱</div>
            <div class="infotxt txt"><?php echo $member_data['store_name'];?></div>
        </div>

        <div class="input_group d-flex align-items-center">
            <div class="label" for="name">產品名稱</div>
            <input class="infotxt" name="name" id="name" value="<?php echo $product_data['name']; ?>" type="text" placeholder="請填產品名稱">
        </div>

        <div class="input_group d-flex align-items-center">
            <div class="label" for="retail_price">市價</div>
            <input class="infotxt" name="retail_price" id="retail_price" value="<?php echo $product_data['retail_price']; ?>" type="number" placeholder="請填巿價" onchange="if(this.value < 0){alert('最小需填0.01');this.value='';}">
        </div>

        <div class="input_group d-flex align-items-center">
            <div class="label">產品圖片</div>
            <div class="infotxt file-box">
                <img name="preUpLoadImg" border="0" id="preUpLoadImg" src="<?php echo BASE_URL."/site/images/site/product/"; ?><?php echo $product_data['thumbnail']; ?>">
                <input accept="image/*" name="pic" id="pic" type="file" value="<?php echo $product_data['thumbnail']; ?>">
            </div>
        </div>

        <div class="input_group d-flex align-items-center">
            <div class="label" for="ontime">起標時間</div>
            <input name="ontime" id="ontime" value="<?php echo date("Y-m-d H:i:s",$product_data['unix_ontime']); ?>" type="text" placeholder="請填起標時間" class="infotxt some_class">
        </div>

        <div class="input_group d-flex align-items-center">
            <div class="label" for="offtime">結標時間</div>
            <input name="offtime" id="offtime" value="<?php echo date("Y-m-d H:i:s", $product_data['unix_offtime']); ?>" type="text" placeholder="請填結標時間" class="infotxt some_class">
        </div>

        <div class="input_group d-flex align-items-center">
            <div class="label" for="flash_loc">閃殺地點</div>
            <input class="infotxt" name="flash_loc" id="flash_loc" value="<?php echo $product_data['flash_loc']; ?>" type="text" placeholder="請填閃殺地點">
        </div>

    <!--
        <div class="input_group d-flex align-items-center">
            <div class="label" for="channel">閃殺地區</div>
            <input class="infotxt" name="channel" id="channel" value="<?php echo $product_data['channel']; ?>" type="text" placeholder="請填閃殺地區">
        </div>
    -->

        <div class="input_group d-flex align-items-center">
            <div class="label" for="contact_info">聯絡方式</div>
            <input class="infotxt" name="contact_info" id="contact_info" value="<?php echo $product_data['contact_info']; ?>" type="text" placeholder="請填聯絡方式">
        </div>

<!--
        <div class="input_group d-flex align-items-center">
            <div class="label" for="channel">產品類別</div>
            <select name="channel" id="channel" >
                <option value="flash">閃殺</option>
                <option value="exchange">兌換</option>
                <option value="saja">殺價</option>
            </select>
        </div>
-->

        <input name="spic" id="spic" type="hidden" value="<?php echo $product_data['thumbnail']; ?>" >
        <input name="opic" id="opic" type="hidden" value="<?php echo $product_data['thumbnail']; ?>" >
        <input name="totalfee_type" id="totalfee_type" type="hidden" value="B" >
        <input name="productid" id="productid" type="hidden" value="<?php echo $product_data['productid']; ?>" >

        <div class="footernav">
            <div class="switch-btn">
                <button type="button" name="putdata" id="putdata" <?php if ($product_data['now'] >= $product_data['unix_ontime']){ ?>onClick="javascript:alert('產品已達起標時間，無法修改!!');"<?php } else { ?>onClick="eppodduct_update()" <?php } ?>>確認提交</button>
            </div>
        </div>
    </div>
</form>

<!-- 2018/10/17 舊檔 -->
<!--
   <form method="post" action="" id="contactform" name="contactform" enctype="multipart/form-data">

		<div >
            <ul data-role="listview" data-inset="true" data-icon="false">

				<li>
					<label for="esname">店家名称 : <?php echo $member_data['store_name'];?></label>
					
                </li>
				<li>
					<label for="name"><font style="color:red">*</font>产品名称</label>
					<input name="name" id="name" value="<?php echo $product_data['name']; ?>" data-clear-btn="true" type="text" placeholder="請填产品名称">
                </li>
				<li>
					<label for="retail_price"><font style="color:red">*</font>巿價</label>
					<input name="retail_price" id="retail_price" value="<?php echo $product_data['retail_price']; ?>" data-clear-btn="true" type="number" placeholder="請填巿價" onchange="if(this.value < 0){alert('最小需填0.01');this.value='';}">
                </li>
				
				<li><h2><font style="color:red">*</font>产品图片：</h2>
					<ul data-role="listview" data-inset="true" data-icon="false">
						<li>
							<img name="preUpLoadImg" border="0" id="preUpLoadImg" src="<?php echo BASE_URL."/site/images/site/product/"; ?><?php echo $product_data['thumbnail']; ?>" height="150px" width="150px">
							<input accept="image/*" name="pic" id="pic" type="file" value="<?php echo $product_data['thumbnail']; ?>">
						</li>
					</ul>	
				</li>				
				<li>
					<label for="ontime"><font style="color:red">*</font>起標時間</label>
					<input name="ontime" id="ontime" value="<?php echo date("Y-m-d H:i:s",$product_data['unix_ontime']); ?>" data-clear-btn="true" type="text" placeholder="請填起標時間"  class="some_class">
                </li>				
				<li>
					<label for="offtime"><font style="color:red">*</font>结標時間</label>
					<input name="offtime" id="offtime" value="<?php echo date("Y-m-d H:i:s", $product_data['unix_offtime']); ?>" data-clear-btn="true" type="text" placeholder="請填结標時間"  class="some_class">
                </li>				
				<li>
					<label for="flash_loc"><font style="color:red">*</font>闪殺地點</label>
					<input name="flash_loc" id="flash_loc" value="<?php echo $product_data['flash_loc']; ?>" data-clear-btn="true" type="text" placeholder="請填闪殺地點">
                </li>
				<li>
					<label for="contact_info"><font style="color:red">*</font>联络方式</label>
					<input name="contact_info" id="contact_info" value="<?php echo $product_data['contact_info']; ?>" data-clear-btn="true" type="text" placeholder="請填联络方式">
                </li>			
				
				<input name="spic" id="spic" type="hidden" value="<?php echo $product_data['thumbnail']; ?>" >
				<input name="opic" id="opic" type="hidden" value="<?php echo $product_data['thumbnail']; ?>" >
				<input name="totalfee_type" id="totalfee_type" type="hidden" value="B" >
				<input name="productid" id="productid" type="hidden" value="<?php echo $product_data['productid']; ?>" >
                
            </ul>
        </div>
		<div style="background-color:#FFFFFF;height:50px;margin:10px;">  </div>
		<div class="nav">	
			<div class="weui-tabbar" style="background-color:#FFFFFF">
				<div style="width:3%;"> </div>
				<div style="width:94%; margin:0 auto;" >
					<button type="button" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0" name="putdata" id="putdata" <?php if ($product_data['now'] >= $product_data['unix_ontime']){ ?>onClick="javascript:alert('产品已达起標時間，無法修改!!');"<?php } else { ?>onClick="eppodduct_update()" <?php } ?>>確認提交</button>
				</div>	
				<div style="width:3%;"> </div>
			</div>
		</div>				
</form>
-->
<!-- 2018/10/17 舊檔/ -->


<script type="text/javascript" src="<?PHP echo APP_DIR; ?>/static/js/jquery.datetimepicker.full.js"></script>
<link rel="stylesheet" href="<?PHP echo APP_DIR; ?>/static/css/jquery.datetimepicker.css" />

<script type="text/javascript">
    $(function(){
        //更改標題
        $('.navbar-brand h3').text('更改商品資料');
    });
    
    //調整標題寬度一致
    var options = {
        dom: {
            ul: '.product-edit-box',
            li: '.input_group',
            label: '.label',
            input: '.infotxt'
        }
    };
    $(function(){
        
        //目前顯示的ul區塊
        var $ul = $(options.dom.ul);

        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));
            
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];

            $liW = $selfLi.width();                         //取得li總寬度   
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                var $fs = parseFloat($(this).css('font-size'));
                $arr.push($(this).text().length * ($fs + 1.5) + 14);    //+2為字距空間 , +14 預留冒號空間
                //console.log($arr);
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值
//console.log($liW);
            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                $selfInput.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                        $(this).css('width',$remain);
                        
                    }
                })
            })
        }
        changeWidth($ul);
        //尺寸改變重新撈取寬度
        $(window).on('load resize',function(){
            $ul.each(function(){
                changeWidth($(this));
            })
        })
        
    })
    
</script>

<script type="text/javascript">

	function likename() {
		document.getElementById('companyname').value = document.getElementById('esname').value;
	}
		
    $("#pic").change(function(){
      readImage( this );
	  
		var formData = new FormData();
		formData.append('pic', $('#pic')[0].files[0]);
		formData.append('formName', 'contactform'); 						//表單名稱
		formData.append('prevImg', 'preUpLoadImg'); 						//顯示圖片ID
		formData.append('upFloder', '../../site/images/site/product/');		//上傳目錄名稱
		formData.append('rePicName', 'pic'); 								//回傳圖片上傳名稱
		formData.append('subName', 'jpg,png,gif,jpeg'); 					//可上傳副檔名
		formData.append('maxSize', '2048'); 							//檔案限制大小
		formData.append('url', '<?php echo BASE_URL; ?>/site/images/site/product'); 					
		formData.append('rePic', '400'); 							
		formData.append('allowSubName', 'jpg,png,gif,jpeg');　							
		
		$.ajax({
			url : '/site/ImgUp/upload_c1.php?upload=true',
			type : 'POST',
			data : formData,
			processData: false,  // tell jQuery not to process the data
			contentType: false,  // tell jQuery not to set contentType
			success : function(data) {
				console.log(data);
				//alert(data.pic);
				var json = $.parseJSON(data);
				console && console.log($.parseJSON(data));
				if (json.status == 1){
					$('#spic').attr("value", json.pic);
					$('#pic').attr("value", json.pic);
					document.getElementById('spic').value = json.pic;
				} else {
					alert(json.msg);
					return false;
				}
			}
		});	  
    });

    function readImage(input) {
      if ( input.files && input.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
          $('#preUpLoadImg').attr( "src", e.target.result );
        };       
        FR.readAsDataURL( input.files[0] );
      }
    }

	$('.some_class').datetimepicker();	
</script>
