<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$profile=_v('enterprise_profile');
$account=_v('enterprise_account');
$shop=_v('enterprise_shop');
$enterprise=_v('enterprise');
?>
   <form method="post" action="" id="contactform" name="contactform">
	<div class="article">
		<div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">
		    <div data-role="collapsible">	
			    <h2>修改登入密碼</h2>
				<ul data-role="listview" data-inset="true" data-icon="false">
				    <li><h2>*登入密碼：</h2><input name="passwd" id="passwd" data-clear-btn="true" value="" type="password"></li>
					<li><h2>*確認登入密碼：</h2><input name="confrim_passwd" id="confrim_passwd" data-clear-btn="true" value="" type="password"></li>	
				</ul>
				<button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="userenterprisepw()">提交</button>
		    </div>
			<div data-role="collapsible">	
			    <h2>修改提现密碼</h2>
				<ul data-role="listview" data-inset="true" data-icon="false">
				    <li><h2>*提现密碼：</h2><input name="expasswd" id="expasswd" data-clear-btn="true" value="" type="password"></li>
					<li><h2>*確認提現密碼：</h2><input name="confrim_expasswd" data-clear-btn="true" id="confrim_expasswd" value="" type="password"></li>	
				</ul>
				<button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="userenterpriseexpw()">提交</button>
		    </div>
		</div>	
		<div data-role="collapsible-set" data-theme="a" data-iconpos="right" data-collapsed-icon="carat-d" data-expanded-icon="carat-u">
			<div data-role="collapsible">	
			   <h2>营业信息</h2>
				<ul data-role="listview" data-inset="true" data-icon="false">
				    <li><h2>*店家名称：</h2><input name="name" id="name" value="<?php echo $shop['name']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>*负责人</h2><input name="owner" id="owner" value="<?php echo $shop['owner']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>*店家电话：</h2><input name="contact_phone" id="contact_phone" value="<?php echo $shop['contact_phone']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>*店家移動电话：</h2><input name="contact_mobile" id="contact_mobile" value="<?php echo $shop['contact_mobile']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>*分润比：</h2><input name="profit_ratio" id="profit_ratio" value="<?php echo $shop['profit_ratio']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>店家网址：</h2><input name="url" id="url" value="<?php echo $shop['url']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>联系信箱：</h2><input name="contact_email" id="contact_email" value="<?php echo $shop['contact_email']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>营业开始時間</h2><input name="businessontime" id="businessontime" value="<?php echo $shop['businessontime']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>营业结束時間</h2><input name="businessofftime" id="businessofftime" value="<?php echo $shop['businessofftime']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>客服信箱：</h2><input name="service_email" id="service_email" value="<?php echo $shop['service_email']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>营业年资：</h2><input name="seniority" id="seniority" value="<?php echo $shop['seniority']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>员工人數：</h2><input name="employees" id="employees" value="<?php echo $shop['employees']; ?>" data-clear-btn="true" type="text" ></li>
				</ul>
			</div>
			<div data-role="collapsible">	
			    <h2>企业信息</h2>
				<ul data-role="listview" data-inset="true" data-icon="false">
				    <li><h2>企业名称：</h2><input name="companyname" id="companyname" value="<?php echo $profile['companyname']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>对外名称：</h2><input name="marketingname" id="marketingname" value="<?php echo $profile['marketingname']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>登記证號：</h2><input name="uniform" id="uniform" value="<?php echo $profile['uniform']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>负责人：</h2><input name="name" id="name" value="<?php echo $profile['name']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>联系电话：</h2><input name="phone" id="phone" value="<?php echo $profile['phone']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>Email：</h2><input name="email" id="email" value="<?php echo $profile['email']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>FAX：</h2><input name="fax" id="fax" value="<?php echo $profile['fax']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>公司地址：</h2><input name="address" id="address" value="<?php echo $profile['address']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>公司图片：</h2>
						<ul data-role="listview" data-inset="true" data-icon="false">
							<li>
								<img src="<?php if (empty($profile['thumbnail_file'])) { echo "/management/images/headimgs/_DefaultShop.jpg"; }else{ echo "http://test.shajiawang.com/management/images/headimgs/".$profile['thumbnail_file'];} ?>" alt="顯示上傳預覽圖片" name="preUpLoadImg" border="0" id="preUpLoadImg" height="150px" width="150px" onclick="javascript:uploadFile1('pic');">
								<input name="pic" id="pic" value="<?php if (empty($profile['thumbnail_file'])){ echo "_DefaultShop.jpg" ; }else{ echo $profile['thumbnail_file']; } ?>" data-clear-btn="true" type="text" onclick="javascript:uploadFile1('pic');">
							</li>
						</ul>	
					</li>
					<li><h2>公司图片連結：</h2><input name="picurl" id="picurl" value="<?php echo $profile['thumbnail_url']; ?>" data-clear-btn="true" type="text" ></li>
				</ul>
			</div>
			<div data-role="collapsible">	
				<h2>帳务信息 (提现必填)</h2>
				<ul data-role="listview" data-inset="true" data-icon="false">
					<li><h2>帳务负责人：</h2><input name="account_contactor" id="account_contactor" value="<?php echo $account['account_contactor']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>帳务负责人电话：</h2><input name="account_phone" id="account_phone" value="<?php echo $account['account_phone']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>营运人</h2><input name="execute_contactor" id="execute_contactor" value="<?php echo $account['execute_contactor']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>营运人电话</h2><input name="execute_phone" id="execute_phone" value="<?php echo $account['execute_phone']; ?>" data-clear-btn="true" type="text" ></li>	
					<li><h2>拨款帳户名：</h2><input name="accountname" id="accountname" value="<?php echo $account['accountname']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>拨款帳號：</h2><input name="accountnumber" id="accountnumber" value="<?php echo $account['accountnumber']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>帳户银行名称：</h2><input name="bankname" id="bankname" value="<?php echo $account['bankname']; ?>" data-clear-btn="true" type="text" ></li>
					<li><h2>帳户银行分行名称：</h2><input name="branchname" id="branchname" value="<?php echo $account['branchname']; ?>" data-clear-btn="true" type="text" ></li>
				</ul>
			</div>
			<input name="enterpriseid" id="enterpriseid" value="<?php echo $profile['enterpriseid']; ?>" type="hidden">
			<input name="oldpic" id="oldpic" value="<?php echo $profile['thumbnail_file']; ?>" type="hidden">
			<button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="userenterprise()">提交</button>			   
		</div>
    </div>
</form>
<!-- /article -->
<script type="text/javascript">

function uploadFile1(n){
	var formName = "contactform"; 						//表單名稱
	var prevImg = "preUpLoadImg"; 						//顯示圖片ID
	var upFloder = "../../management/images/headimgs/";		//上傳目錄名稱
	var rePicName = n; 								//回傳圖片上傳名稱
	var subName ="jpg,png,gif"; 					//可上傳副檔名
	var maxSize = 1024; 							//檔案限制大小
	var winTitle = "fileUpload"; 					//視窗名稱
	var winWidth = 400; 							//視窗寬
	var winHeight = 150;　							//視窗高
	
	window.open('<?php echo BASE_URL . APP_DIR;?>/ImgUp/upload_c.php?formName='+formName+'&prevImg='+prevImg+'&url=http://test.shajiawang.com/management/images/headimgs/&enterpriseid=<?php echo $profile['enterpriseid']; ?>&upFloder='+upFloder+'&rePicIpt='+rePicName+'&subName='+subName+"&maxSize="+maxSize,winTitle,'width='+winWidth+',height='+winHeight+'');	
}
</script>
