<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$REQUEST_DATA = _v('REQUEST_DATA');
?>
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/enterprise2018.css">
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">

<div class="enterprise-login">
    <form name="login" id="login" action="/site/enterprise/login_action" method="post" data-ajax="false">
        <div class="login-logo">
            <img src="<?php echo IMAGES_DIR."/site/sajapos.jpg"?>">
        </div>
    <div class="login-content">
        <div class="login-account d-flex align-items-center">
            <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/account.png" alt="帳號">
            <input type="text" name="loginname" id="loginname" class="login-input" placeholder="商家帳號">
            <?php
				if (is_array($REQUEST_DATA)) {
					foreach ($REQUEST_DATA as $key => $value) {
						echo '<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
					}
				}
			?>
        </div>
        <div class="login-password d-flex align-items-center">
            <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/password.png" alt="密碼">
            <input type="password" name="passwd" id="pw" class="login-input" placeholder="請輸入密碼" focus="">
        </div>
        <div class="login-btn-login" onclick="enterprise_login()">商家登入</div>
<!--        <div class="login-btn-login" onclick="javascript:this.form.submit();">商家登入</div>-->
<!--        <p class="login-forgotpassword" onclick="javascript:location.href='#'">忘記密碼?</p>-->
    </div>
    <div class="login-footer">
		<p><span class="login-footer-txt">還不是會員嗎?</span><a href="javascript:location.href='/site/enterprise/member_register'" class="ui-link">按此註冊</a></p>
	</div>
	</form>
</div>
<!-- 2018-10-12 舊版面 -->
<!--
		<div class="article">
			<form name="login" id="login" action="/site/enterprise/login_action" method="post" data-ajax="false">
			<ul data-role="listview" data-inset="true" data-icon="false">
				<li style="text-align:center"  >
					<h4><img src="<?php echo IMAGES_DIR."/site/sajapos.jpg?a=33" ?>" /></h4>
				</li>
				<li>	
                    <label for="loginname">商家帳號</label>
					<input type="text" name="loginname" id="loginname" placeholder="商家帳號" />
					<?php
						if (is_array($REQUEST_DATA)) {
							foreach ($REQUEST_DATA as $key => $value) {
								echo '<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
							}
						}
					?>
				</li>
				<li>
					<label for="pw">密碼</label>
					<input type="password" name="passwd" id="pw" placeholder="密碼" />
				</li>
				<li>
					<input type="submit" value="登录" class="ui-btn ui-corner-all ui-btn-a" />
					button type="submit" class="ui-btn ui-corner-all ui-btn-a">登录</button
				</li>
            </ul>
            </form>
        </div>
-->
<!-- 2018-10-12 舊版面 -->
