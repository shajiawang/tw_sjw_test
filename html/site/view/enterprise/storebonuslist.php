<?php //交易明细
$store_bonus_list = _v('store_bonus_list');
?>
<script>
    function del(evrid){
		var delitem = {
                    evrid:evrid,
                    enterpriseid:<?php echo $_SESSION['sajamanagement']['enterprise']['enterpriseid']?>,
                };

                //alert(JSON.stringify(bidData));
                if (confirm("是否確認取消此訂單")){
                $.post(APP_DIR+"/enterprise/rollBackTx/",
                    delitem,
                    function(str_json){
                        if(str_json){
                            $.mobile.loading('show');
                                console && console.log(str_json);
                                var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);

                                if (json.retCode==1) {
                                    //$('#del_'+json.evrid).html('[退單完成]');
                                    $('#del_'+json.evrid).html('['+json.retMsg+']');
//return false;
                                } else{
                                    $('#del_'+json.evrid).html('['+json.retMsg+']');
                                    //
                                }
                            }
                            $.mobile.loading('hide');
                            return false;
                    });
        		}
	}
	function changeview(){
		if ($('#toggleview').html()=='[展開全部記錄]'){
			var collection = $(".switch-box");
			collection.each(function() {
				console.log(this.id);
				console.log(this.attributes['style']);
				console.log(this.attributes['style']=='style="display:none"');

			    if (this.style['display']=='none'){
			    	ReverseDisplay(this.id);
			    }
			});
			$('#toggleview').html('[收合全部記錄]');
		}else{
			var collection = $(".switch-box");
			collection.each(function() {
			    if (this.style['display']=='block') ReverseDisplay(this.id);
			});
			$('#toggleview').html('[展開全部記錄]');
		}
	}
</script>
<style>
    .history-list {
        padding: .834rem 1.084rem .667rem;
    }
    .history-list ~ .history-list {
        border-top: 1px solid #F2F2F2;
    }
    .history-list .peo {
        color: #FF5722;
        font-size: 1.25rem;
    }
    .history-list .num {
        font-size: 1.334rem;
        color: #FF5722;
    }
</style>
<span class='button' onclick='changeview()' id='toggleview' >[展開全部記錄]</span>
<ul class="saja_listsimg_box">
    <?php
		if(!empty($store_bonus_list['table']['record']) ) {
        foreach($store_bonus_list['table']['record'] as $rk => $rv){
    ?>
        <li class="history-list d-flex align-items-center" onClick="ReverseDisplay('<?php echo $rv['evrid']; ?>');">
            <div class="history-list-info d-flex flex-column mr-auto">
                <div class="peo"><?php echo $rv['nickname']; ?>:<?php echo round($rv['total_bonus']); ?><span id ="del_<?php echo $rv['evrid']; ?>" onclick="del(<?php echo $rv['evrid']; ?>);" <?php if ((time()-strtotime(date($rv['commit_time'])))>300) echo "style='display:none'";?> >[Del]</span></div>
                <div class="time" ><?php echo $rv['commit_time']; ?></div>
            </div>
<!--            <div class="num">1</div>-->
        </li>
        <div id="<?php echo $rv['evrid']; ?>" class="switch-box" style="display:none">
                <div class="switch-table d-flex flex-column align-items-center">
                    <div class="switch-tr d-flex justify-content-around w-100">
                        <div class="switch-td">交易編號</div>
                        <div class="switch-td">交易點數</div>
<!--                        <div class="switch-td">分潤</div>-->
                        <div class="switch-td">實收點數</div>
                    </div>
                    <div class="switch-tr d-flex justify-content-around w-100">
                        <div class="switch-td"><?php echo $rv['evrid']; ?></div>
                        <div class="switch-td t-rcolor"><?php echo sprintf("%.2f",$rv['total_bonus']); ?></div>
<!--                        <div class="switch-td"><?php echo sprintf("%.2f",$rv['sys_profit']); ?></div>-->
                        <div class="switch-td t-gcolor"><?php echo sprintf("%.2f",$rv['net_bonus']); ?></div>
                    </div>
                </div>
        </div>
    <?php } } ?>

</ul>
<?php
    if(!empty($row_list['table']['record']) ) {
    $page_content = _v('page_content');

    $arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : '';
    $arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
?>
<!--div>
    <div class="ui-grid-a">
        <div class="ui-block-a <?php echo $arrow_l_able; ?>">
        <a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
        </div>
        <div class="ui-block-b <?php echo $arrow_r_able; ?>">
        <a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
        </div>
    </div>
    <?php /*<form>
        <select name="select-native-1" id="change-page">
        <?php echo $page_content['change']; ?>
        </select>
    </form>*/?>
</div-->
<?php } ?>









<!-- 2018/10/16 舊檔 -->
<!--
	<div>
		<div class="weui-cells2">
		<?php
			if(!empty($store_bonus_list['table']['record']) ) {
			foreach($store_bonus_list['table']['record'] as $rk => $rv){
		?>
			<div class="weui-cell weui-cell_access enterprise-font1" onClick="ReverseDisplay('<?php echo $rv['evrid']; ?>');">
				<div class="weui-cell__hd" style="width:45%"><?php echo $rv['commit_time']; ?></div>
				<div class="weui-cell__bd"><?php echo $rv['prod_name']; ?></div>
				<div class="weui-cell__ft"> </div>
			</div>
			<div id="<?php echo $rv['evrid']; ?>" class="sajapay-content enterprise-font1" style="display:none">
				<div style="background-color:#FFFFFF;padding:10px;">
					<div class="weui-cell2">
						<div class="weui-cell__bd" style="width:30%" >交易编號</div>
						<div class="weui-cell__ft"><?php echo $rv['evrid']; ?></div>
					</div>
					<div class="weui-cell2">
						<div class="weui-cell__bd" style="width:30%" >交易點數</div>
						<div class="weui-cell__ft"><font class="enterprise-font2"><?php echo sprintf("%.2f",$rv['total_bonus']); ?></font></div>
					</div>
					<div class="weui-cell2">
						<div class="weui-cell__bd" style="width:30%" >分润</div>
						<div class="weui-cell__ft"><?php //echo sprintf("%.2f",$rv['sys_profit']); ?></div>
					</div>
					<div class="weui-cell2">
						<div class="weui-cell__bd" style="width:30%" >实收點數</div>
						<div class="weui-cell__ft"><font class="enterprise-font3"><?php echo sprintf("%.2f",$rv['net_bonus']); ?></font></div>
					</div>
				</div>
			</div>
		<?php } } //endif; ?>
		</div>
		<?php
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content');

			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : '';
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
		?>
		<div>
			<div class="ui-grid-a">
				<div class="ui-block-a <?php echo $arrow_l_able; ?>">
				<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
				</div>
				<div class="ui-block-b <?php echo $arrow_r_able; ?>">
				<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
				</div>
			</div>
			<?php /*<form>
				<select name="select-native-1" id="change-page">
				<?php echo $page_content['change']; ?>
				</select>
			</form>*/?>
		</div>
		<?php } ?>

	</div>
-->
<!-- 2018/10/16 舊檔/ -->
