<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$total_net_bonus = _v('total_net_bonus');
$member_data = _v('member_data');
?>
<style>
    .cash-edit-box {
        background: #FFF;
        margin: .8rem auto 0;
    }
    .cash-edit-box .label {
        margin-right: 10px;
    }
    .cash-edit-box .label::after {
        content: ' : ';
    }
    .input_group .infotxt.txt {
        padding: .667rem;
        margin: .667rem 0;
        height: 1.25rem;
    }
    .input_group input.infotxt {
        flex: 1;
        border: 1px solid #ccc;
        padding: 10px;
        border-radius: 5px;
        box-sizing: border-box;
        margin: .667rem 0;
    }
    .input_group .infotxt.file-box {
        flex: 1;
        margin: .667rem 0;
    }
    .file-box * {
        display: block;
    }
    .file-box img {
        width: 100px;
        margin-bottom: 10px;
    }
</style>

<form method="post" action="" id="contactform" name="contactform" enctype="multipart/form-data">
    <div class="cash-edit-box">
        <div class="input_group d-flex align-items-center">
            <div class="label" for="esname">目前鯊魚點數</div>
            <div class="infotxt txt"><?php echo $total_net_bonus;?></div>
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="cash">提現</div>
            <input class="infotxt" name="cash" id="cash" value="" type="number" placeholder="請填數字 " onkeyup="value=value.replace(/[^\d]/g,'')" onchange="if(this.value < 1){alert('最小需填1');this.value='';}">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="accountname">銀行開戶名</div>
            <input class="infotxt" name="accountname" id="accountname" value="" type="text" placeholder="銀行開戶名">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="accountnumber">銀行帳號</div>
            <input class="infotxt" name="accountnumber" id="accountnumber" value="" type="text" placeholder="银行帳號">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="accountnumber">開戶銀行</div>
            <input class="infotxt" name="bankname" id="bankname" value="" type="text" placeholder="開戶銀行">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="bankarea">開戶銀行所在地區</div>
            <input class="infotxt" name="bankarea" id="bankarea" value="" type="text" placeholder="開戶銀行所在地區">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="bankarea">開戶銀行支行名稱</div>
            <input class="infotxt" name="branchname" id="branchname" value="" type="text" placeholder="開戶銀行支行名稱">
        </div>
        
        <input type="hidden" name="bonus" id="bonus" value="<?php echo $total_net_bonus; ?>">
        
        <div class="footernav">
            <div class="switch-btn">
                <button type="button" name="putdata" id="putdata" onClick="enterprise_cash()">確認提交</button>
            </div>
        </div>

    </div>		
</form>

  
<!-- 2018/10/17 舊檔 -->
<!--
   <form method="post" action="" id="contactform" name="contactform" enctype="multipart/form-data">

		<div >
            <ul data-role="listview" data-inset="true" data-icon="false">

				<li>
					<label for="esname">目前鯊魚點數：<?php echo $total_net_bonus;?></label>
				</li>
				<li>
					<label for="cash"><font style="color:red">*</font>提現 </label>
					<input name="cash" id="cash" value="" data-clear-btn="true" type="number" placeholder="請填數字 " onkeyup="value=value.replace(/[^\d]/g,'')" onchange="if(this.value < 1){alert('最小需填1');this.value='';}">
                </li>

				<li>
					<label for="accountname"><font style="color:red">*</font>银行开户名</label>
					<input name="accountname" id="accountname" value="" data-clear-btn="true" type="text" placeholder="银行开户名">
                </li>
				<li>
					<label for="accountnumber"><font style="color:red">*</font>银行帳號</label>
					<input name="accountnumber" id="accountnumber" value="" data-clear-btn="true" type="text" placeholder="银行帳號">
                </li>
				<li>
					<label for="bankname"><font style="color:red">*</font>开户银行</label>
					<input name="bankname" id="bankname" value="" data-clear-btn="true" type="text" placeholder="开户银行">
                </li>
				<li>
					<label for="bankarea"><font style="color:red">*</font>开户银行所在地區</label>
					<input name="bankarea" id="bankarea" value="" data-clear-btn="true" type="text" placeholder="开户银行所在地區">
                </li>				
				<li>
					<label for="branchname"><font style="color:red">*</font>开户银行支行名称</label>
					<input name="branchname" id="branchname" value="" data-clear-btn="true" type="text" placeholder="开户银行支行名称">
                </li>
				
					<input type="hidden" name="bonus" id="bonus" value="<?php echo $total_net_bonus; ?>">

            </ul>
        </div>
		<div style="background-color:#FFFFFF;height:50px;margin:10px;">  </div>
		<div class="nav">	
			<div class="weui-tabbar" style="background-color:#FFFFFF">
				<div style="width:3%;"> </div>
				<div style="width:94%; margin:0 auto;" >
					<button type="button" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0" onClick="enterprise_cash()">確認提交</button>
				</div>	
				<div style="width:3%;"> </div>
			</div>
		</div>		
</form>
-->
<!-- 2018/10/17 舊檔/ -->


<script type="text/javascript">
    $(function(){
        //更改標題
        $('.navbar-brand h3').text('提現');
    });
    
    //調整標題寬度一致
    var options = {
        dom: {
            ul: '.cash-edit-box',
            li: '.input_group',
            label: '.label',
            input: '.infotxt'
        }
    };
    $(function(){
        
        //目前顯示的ul區塊
        var $ul = $(options.dom.ul);

        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));
            
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];

            $liW = $selfLi.width();                         //取得li總寬度   
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                var $fs = parseFloat($(this).css('font-size'));
                $arr.push($(this).text().length * ($fs + 1.5) + 14);    //+2為字距空間 , +14 預留冒號空間
                //console.log($arr);
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值
//console.log($liW);
            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                $selfInput.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                        $(this).css('width',$remain);
                        
                    }
                })
            })
        }
        changeWidth($ul);
        //尺寸改變重新撈取寬度
        $(window).on('load resize',function(){
            $ul.each(function(){
                changeWidth($(this));
            })
        })
        
    })
    
</script>