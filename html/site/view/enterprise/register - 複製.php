<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$nickname = _v('nickname');

?>
   <form method="post" action="" id="contactform" name="contactform">
		
		<?php if (!empty($user_src)){ ?>
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">
				<li>
					<label for="esname">推荐人：　<?php echo $nickname; ?></label>
				</li>
            </ul>
        </div>
		<?php } ?>
		<div class="article">
            <ul data-role="listview" data-inset="true" data-icon="false">

				<li>
					<label for="esname"><font style="color:red">*</font>店家名称</label>
					<input name="esname" id="esname" value="" data-clear-btn="true" type="text" placeholder="請填店家名称">
                </li>
				<li>
					<label for="ephone"><font style="color:red">*</font>联络手機</label>
					<input name="ephone" id="ephone" value="" data-clear-btn="true" type="text" placeholder="可接收短信的手機號">
                </li>
				<li>					
					<label for="loginname"><font style="color:red">*</font>商户登入帳號</label>
					<input name="loginname" id="loginname" value="" data-clear-btn="true" type="text" placeholder="請填商户登入帳號">
                </li>
				<li>
					<label for="passwd"><font style="color:red">*</font>登入密碼</label>
					<input name="passwd" id="passwd" value="" data-clear-btn="true" type="password" placeholder="請填登入密碼">
                </li>
				<li>
					<label for="checkpasswd"><font style="color:red">*</font>密碼確認</label>
					<input name="checkpasswd" id="checkpasswd" value="" data-clear-btn="true" type="password" placeholder="請填密碼確認">					
				</li>				
				<li>
					<label for="epname"><font style="color:red">*</font>负责人</label>
					<input name="epname" id="epname" value="" data-clear-btn="true" type="text" placeholder="請填负责人">
                </li>
				<li>
					<label for="profit_ratio"><font style="color:red">*</font>分润比</label>
					<input name="ratio" id="ratio" value="" data-clear-btn="true" type="number" placeholder="請填分润比">
                </li>				
				<li>
					<label for="email">联络信箱</label>
					<input name="email" id="email" value="" data-clear-btn="true" type="email" placeholder="請填联络信箱">
                </li>
				<li><h2>公司图片：</h2>
					<ul data-role="listview" data-inset="true" data-icon="false">
						<li>
							<img src="<?php echo BASE_URL."/management/images/headimgs/_DefaultShop.jpg"; ?>" alt="顯示上傳預覽圖片" name="preUpLoadImg" border="0" id="preUpLoadImg" height="150px" width="150px" onclick="javascript:uploadFile1('pic');">
							<input name="pic" id="pic" value="" data-clear-btn="true" type="text" onclick="javascript:uploadFile1('pic');">
						</li>
					</ul>	
				</li>
				<li><h2>公司图片連結：</h2><input name="picurl" id="picurl" value="" data-clear-btn="true" type="text" ></li>				
				<li>
					<input type="hidden" name="usersrc" id="usersrc" value="<?php echo $user_src; ?>">
					<button type="button" class="ui-btn ui-corner-all ui-btn-a" onClick="enterprise()">確認提交</button>
                </li>

				<!--li>					
					<input type="checkbox" name="rule-1" id="rule-1" onclick="likename();" /><label for="rule-1">同店家名称</label>
                </li>				
				<li>					
					<label for="companyname">企业名称</label>
					<input name="companyname" id="companyname" value="" data-clear-btn="true" type="text" placeholder="請填企业名称">
                </li-->				
            </ul>
        </div><!-- /article -->
</form>
		
<script type="text/javascript">

	function uploadFile1(n){
		var formName = "contactform"; 						//表單名稱
		var prevImg = "preUpLoadImg"; 						//顯示圖片ID
		var upFloder = "../../management/images/headimgs/";		//上傳目錄名稱
		var rePicName = n; 								//回傳圖片上傳名稱
		var subName ="jpg,png,gif"; 					//可上傳副檔名
		var maxSize = 1024; 							//檔案限制大小
		var winTitle = "fileUpload"; 					//視窗名稱
		var winWidth = 400; 							//視窗寬
		var winHeight = 150;　							//視窗高
		
		window.open('<?php echo BASE_URL . APP_DIR;?>/ImgUp/upload_c.php?formName='+formName+'&prevImg='+prevImg+'&url=http://test.shajiawang.com/management/images/headimgs/&upFloder='+upFloder+'&rePicIpt='+rePicName+'&subName='+subName+"&maxSize="+maxSize,winTitle,'width='+winWidth+',height='+winHeight+'');	
	}
	
	function likename() {
		document.getElementById('companyname').value = document.getElementById('esname').value;
	}
</script>		