<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$nickname = _v('nickname');
$channel = _v('channel');

?>
<style>
    .register-box {
        background: #FFF;
        margin: .8rem auto 0;
    }
    .register-box .label {
        margin-right: 10px;
    }
    .register-box .label:not(.hasSmall)::after {
        content: ' : ';
    }
    .register-box .label.hasSmall .label-title::after {
        content: ' : ';
    }
    .input_group .infotxt.txt {
        padding: .667rem;
        margin: .667rem 0;
        height: 1.25rem;
    }
    .input_group input.infotxt {
        flex: 1;
        border: 1px solid #ccc;
        padding: 10px;
        border-radius: 5px;
        box-sizing: border-box;
        margin: .667rem 0;
    }
    .input_group .infotxt.file-box {
        flex: 1;
        margin: .667rem 0;
    }
    .file-box * {
        display: block;
    }
    .file-box img {
        width: 100px;
        margin-bottom: 10px;
    }
    .file-box input[type^='file'] {
        width: 100%;
    }
    .small {
        color: #aaa;
        margin-top: 10px;
        font-size: 12px;
        display: block;
    }
</style>

<form method="post" action="" id="contactform" name="contactform" enctype="multipart/form-data">
    <div class="register-box">
		<?php if (!empty($user_src)){ ?>
		<div class="input_group d-flex align-items-center">
            <div class="label" for="esname">推薦人</div>
            <div class="infotxt txt"><?php echo $nickname; ?></div>
        </div>
		<?php } ?>
		
		<div class="input_group d-flex align-items-center">
            <div class="label" for="esname">店家名稱</div>
            <input class="infotxt" name="esname" id="esname" value="" type="text" placeholder="請填店家名稱">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="ephone">聯絡手機</div>
            <input class="infotxt" name="ephone" id="ephone" value="" type="number" onkeyup="value=value.replace(/[^\d]/g,'')" placeholder="可接收短訊的手機號碼">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="loginname">商戶登入帳號</div>
            <input class="infotxt" name="loginname" id="loginname" value="" type="text" onkeyup="value=value.replace(/[^\a-\z\A-\Z0-9\_]/g,'')" placeholder="請填商户登入帳號">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="passwd">登入密碼</div>
            <input class="infotxt" name="passwd" id="passwd" value="" type="password" placeholder="請填登入密碼">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="checkpasswd">密碼確認</div>
            <input class="infotxt" name="checkpasswd" id="checkpasswd" value="" type="password" placeholder="請填密碼確認">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="epname">負責人</div>
            <input class="infotxt" name="epname" id="epname" value="" type="text" placeholder="請填負責人">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label hasSmall" for="epname">
                <div class="label-title">分潤比</div>
                <p class="small">(單位:最少需千分之十)</p>
            </div>
            <input class="infotxt" name="ratio" id="ratio" value="" type="number" placeholder="千分之十五請填 “15”" onkeyup="value=value.replace(/[^\d]/g,'')" onchange="if(this.value < 10){alert('最小需填10');this.value='';}">
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label" for="channel">地區</div>
            <div class="infotxt">
                <select name="channel" id="channel" >
                    <option value="">(請選擇)</option>
                    <?php 
                        foreach($channel as $key=>$value) {
                        if ($value['channelid'] > 0 && !empty($value['name'])){
                    ?>
                        <option value="<?php echo $value['channelid']; ?>" ><?php echo $value['name']; ?></option> 
                    <?php
                        } 
                        }
                    ?>
                </select>
            </div>
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label hasSmall">
                <div class="label-title">公司圖片</div>
                <p class="small">(公司圖片大小限1MB內)</p>
            </div>
            <div class="infotxt file-box">
                <img name="preUpLoadImg" border="0" id="preUpLoadImg" src="<?php echo BASE_URL."/management/images/headimgs/_DefaultShop.jpg"; ?>">
                <input accept="image/*" name="pic" id="pic" type="file" value="" placeholder="公司圖片大小限1MB內">
            </div>
        </div>
        
        <div class="input_group d-flex align-items-center">
            <div class="label">公司圖片連結</div>
            <input class="infotxt" name="picurl" id="picurl" value="" type="text">
        </div>
        
		<input type="hidden" name="usersrc" id="usersrc" value="<?php echo $user_src; ?>">
        <input name="spic" id="spic" type="hidden" value="" >
        
        <div class="footernav">
            <div class="switch-btn">
                <button type="button" onClick="enterprise()">確認提交</button>
            </div>
        </div>
    </div>	
</form>




<!-- 2018/10/17 舊檔 -->
<!--
   <form method="post" action="" id="contactform" name="contactform" enctype="multipart/form-data">
		<?php if (!empty($user_src)){ ?>
		<div >
            <ul data-role="listview" data-inset="true" data-icon="false">
				<li>
					<label for="esname">推荐人：　<?php echo $nickname; ?></label>
				</li>
            </ul>
        </div>
		<?php } ?>
		<div >
            <ul data-role="listview" data-inset="true" data-icon="false">

				<li>
					<label for="esname"><font style="color:red">*</font>店家名称</label>
					<input name="esname" id="esname" value="" data-clear-btn="true" type="text" placeholder="請填店家名称">
                </li>
				<li>
					<label for="ephone"><font style="color:red">*</font>联络手機</label>
					<input name="ephone" id="ephone" value="" data-clear-btn="true" type="number" onkeyup="value=value.replace(/[^\d]/g,'')" placeholder="可接收短信的手機號">
                </li>
				<li>					
					<label for="loginname"><font style="color:red">*</font>商户登入帳號</label>
					<input name="loginname" id="loginname" value="" data-clear-btn="true" type="text" onkeyup="value=value.replace(/[^\a-\z\A-\Z0-9\_]/g,'')" placeholder="請填商户登入帳號">
                </li>
				<li>
					<label for="passwd"><font style="color:red">*</font>登入密碼</label>
					<input name="passwd" id="passwd" value="" data-clear-btn="true" type="password" placeholder="請填登入密碼">
                </li>
				<li>
					<label for="checkpasswd"><font style="color:red">*</font>密碼確認</label>
					<input name="checkpasswd" id="checkpasswd" value="" data-clear-btn="true" type="password" placeholder="請填密碼確認">					
				</li>				
				<li>
					<label for="epname"><font style="color:red">*</font>负责人</label>
					<input name="epname" id="epname" value="" data-clear-btn="true" type="text" placeholder="請填负责人">
                </li>
				<li>
					<label for="profit_ratio"><font style="color:red">*</font>分润比（單位:最少需千分之十）</label>
					<input name="ratio" id="ratio" value="" data-clear-btn="true" type="number" placeholder="千分之十五請填 “15”" onkeyup="value=value.replace(/[^\d]/g,'')" onchange="if(this.value < 10){alert('最小需填10');this.value='';}">
                </li>				
				<li>
					<label for="channel"><font style="color:red">*</font>地區</label>
					<select name="channel" id="channel" >
						<option value="">(請選擇)</option>
						<?php 
							foreach($channel as $key=>$value) {
							if ($value['channelid'] > 0 && !empty($value['name'])){
						?>
							<option value="<?php echo $value['channelid']; ?>" ><?php echo $value['name']; ?></option> 
						<?php
							} 
							}
						?>
					</select>
				</li>
				<li><h2>公司图片：</h2>
					<ul data-role="listview" data-inset="true" data-icon="false">
						<li>
							<img name="preUpLoadImg" border="0" id="preUpLoadImg" src="<?php echo BASE_URL."/management/images/headimgs/_DefaultShop.jpg"; ?>" height="150px" width="150px">
							<input accept="image/*" name="pic" id="pic" type="file" value="" placeholder="公司图片大小限1MB內"> 公司图片大小限1MB內!!
						</li>
					</ul>	
				</li>
				<li><h2>公司图片連結：</h2><input name="picurl" id="picurl" value="" data-clear-btn="true" type="text" ></li>				
					<input type="hidden" name="usersrc" id="usersrc" value="<?php echo $user_src; ?>">
					<input name="spic" id="spic" type="hidden" value="" >
            </ul>
        </div> /article 
		<div style="background-color:#FFFFFF;height:50px;margin:10px;">  </div>
		<div class="nav">	
			<div class="weui-tabbar" style="background-color:#FFFFFF">
				<div style="width:3%;"> </div>
				<div style="width:94%; margin:0 auto;" >
					<button type="button" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0" onClick="enterprise()">確認提交</button>
				</div>	
				<div style="width:3%;"> </div>
			</div>
		</div>		
</form>
-->
<!-- 2018/10/17 舊檔/ -->

<script type="text/javascript">
    $(function(){
        //更改標題
        $('.navbar-brand h3').text('更改商品資料');
    });
    
    //調整標題寬度一致
    var options = {
        dom: {
            ul: '.register-box',
            li: '.input_group',
            label: '.label',
            input: '.infotxt'
        }
    };
    $(function(){
        
        //目前顯示的ul區塊
        var $ul = $(options.dom.ul);

        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));
            
            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];

            $liW = $selfLi.width();                         //取得li總寬度   
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                if($(this).find('.small').length > 0) {
                    
                    //比較大字與小字寬度，取最寬者
                    var $up = $(this).find('.label-title');
                    var $us = parseFloat($up.css('font-size'));
                    
                    var $down = $(this).find('.small');
                    var $ds = parseFloat($down.css('font-size'));
                    
                    var $upW = $up.text().length * ($us + 1.5) + 14;                //+1.5為字距空間 , +14 預留冒號空間
                    var $downW = $down.text().length * ($ds + 1.5) + 14;
                    console.log($upW+'_'+$downW);
                    if($upW > $downW){
                        $arr.push($upW);    
                    }else{
                        $arr.push($downW);
                    }
                    
                }else{
                    var $fs = parseFloat($(this).css('font-size'));
                    $arr.push($(this).text().length * ($fs + 1.5) + 14);    //+2為字距空間 , +14 預留冒號空間
                }
                //console.log($arr);
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            $maxW = ($maxW > 130) ? 130 : $maxW;
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值
             
//console.log($liW);
//console.log($arr);
//console.log($maxW);
            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                $selfInput.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                        $(this).css('width',$remain);
                        
                    }
                })
            })
        }
        changeWidth($ul);
        //尺寸改變重新撈取寬度
        $(window).on('load resize',function(){
            $ul.each(function(){
                changeWidth($(this));
            })
        })
        
    })
    
</script>

<script type="text/javascript">

	function likename() {
		document.getElementById('companyname').value = document.getElementById('esname').value;
	}
		
    $("#pic").change(function(){
      readImage( this );
	  
		var formData = new FormData();
		formData.append('pic', $('#pic')[0].files[0]);
		formData.append('formName', 'contactform'); 						//表單名稱
		formData.append('prevImg', 'preUpLoadImg'); 						//顯示圖片ID
		formData.append('upFloder', '../../management/images/headimgs/');		//上傳目錄名稱
		formData.append('rePicName', 'pic'); 								//回傳圖片上傳名稱
		formData.append('subName', 'jpg,png,gif,jpeg'); 					//可上傳副檔名
		formData.append('maxSize', '2048'); 							//檔案限制大小
		formData.append('url', '<?php echo BASE_URL; ?>/management/images/headimgs/'); 					
		formData.append('rePic', '400'); 							
		formData.append('allowSubName', 'jpg,png,gif,jpeg');　							
		
		$.ajax({
			// url : '/site/ajax/test3.php',
			url : '/site/ImgUp/upload_c1.php?upload=true',
			type : 'POST',
			data : formData,
			processData: false,  // tell jQuery not to process the data
			contentType: false,  // tell jQuery not to set contentType
			success : function(data) {
				console.log(data);
				//alert(data.pic);
				var json = $.parseJSON(data);
				console && console.log($.parseJSON(data));
				if (json.status == 1){
					$('#spic').attr("value", json.pic);
					$('#pic').attr("value", json.pic);
					document.getElementById('spic').value = json.pic;
				} else {
					alert(json.msg);
					return false;
				}				
			}
		});	  
    });

    function readImage(input) {
      if ( input.files && input.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
          $('#preUpLoadImg').attr( "src", e.target.result );
        };       
        FR.readAsDataURL( input.files[0] );
      }
    }

</script>		