<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$status = _v('status');
$product = _v('product_data');
$enterprise = _v('enterprise');
if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
	$browser = 1;
}else{
	$browser = 2;
}  
?>

<style>
    .store-grids {
        padding: 5px;
        box-sizing: border-box;
    }
    .product-box {
        width: calc((100% / 2) - 15px);
        padding: 10px 5px 5px;
        margin: 15px 2.5px 0;
        background: #FFF;
        box-shadow: 0 1px 5px -3px #000;
    }
    .product-box:nth-of-type(1),
    .product-box:nth-of-type(2) {
        margin-top: 0;
    }
    .product-img {
        border: 1px solid #eee;
        box-sizing: border-box;
    }
    .product-label {
        margin-top: 5px;
    }
</style>


<div class="switch-btn" style="margin-top: 10px;">
    <button type="button" onClick="javascript:location.href='<?php echo APP_DIR; ?>/enterprise/storeproduct_add/?<?php echo $cdnTime; ?>'">新增產品</button>
</div>
<div class="store-grids d-flex flex-wrap">
    <?php 
		if(!empty($product['table']['record']) ) {
		foreach($product['table']['record'] as $rk => $rv){ 
    ?>
        <a href="#" class="product-box d-flex flex-column" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR;?>/enterprise/storeproduct_update/?pid=<?php echo $rv['productid']; ?>&t=<?php echo $cdnTime; ?>'">
            <div class="product-img my-auto">
                <?php if(!empty($rv['thumbnail'])){ ?>
					<img class="img-fluid" src="<?php echo $status['path_image'] .'/product/'. $rv['thumbnail']; ?>">
				<?php }elseif (!empty($rv['thumbnail_url'])){ ?>
					<img class="img-fluid" src="<?php echo $rv['thumbnail_url']; ?>">
				<?php }else{ ?>
					<img class="img-fluid" src="<?php echo BASE_URL;?>/site/images/site/product/1ee1b3f296827f55d52783119f036316.jpg">
				<?php }//endif; ?>
            </div>
            <p class="product-label text-center"><?php echo $rv['name']; ?></p>
        </a>
    <?php } }//endforeach; ?>
</div>



<!-- 2018/10/17 舊檔 -->
<!--
	<div class="" style="background-color:#FFFFFF">
		<div style="width:3%;"> </div>
		<div style="width:94%; margin:0 auto;" >
			<button type="button" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0" onClick="javascript:location.href='<?php echo APP_DIR; ?>/enterprise/storeproduct_add/?<?php echo $cdnTime; ?>'">新增產品</button>
		</div>	
		<div style="width:3%;"> </div>
	</div>
	<div class="weui-grids" style="float:left;text-align:center;background-color: #FFFFFF;">

		<?php 
		if(!empty($product['table']['record']) ) {
		foreach($product['table']['record'] as $rk => $rv){ 
		?>
			<a href="#" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR;?>/enterprise/storeproduct_update/?pid=<?php echo $rv['productid']; ?>&t=<?php echo $cdnTime; ?>'" class="weui-grid3 js_grid">
				<div class="" >
					<?php if(!empty($rv['thumbnail'])){ ?>
					<img src="<?php echo $status['path_image'] .'/product/'. $rv['thumbnail']; ?>" width="95%"  height="92px">
					<?php }elseif (!empty($rv['thumbnail_url'])){ ?>
					<img src="<?php echo $rv['thumbnail_url']; ?>" width="95%" height="92px" >
					<?php }else{ ?>
					<img src="<?php echo BASE_URL;?>/site/images/site/product/1ee1b3f296827f55d52783119f036316.jpg"  width="95%" height="92px">
					<?php }//endif; ?>
				</div>
				<p class="weui-grid3__label">
				  <?php echo $rv['name']; ?>
				</p>
			</a>
		<?php } }//endforeach; ?>
	</div>
-->
<!-- 2018/10/17 舊檔/ -->


<script type="text/javascript">
    $(function(){
        //更改標題
        $('.navbar-brand h3').text('閃殺商品');
    });
</script>