<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$user_src = _v('user_src');
$status = _v('status'); 
?>

<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/enterprise2018.css">
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">

<div class="enterprise-login">
    <div class="login-logo">
        <img src="<?php echo IMAGES_DIR."/site/sajapos.jpg"?>">
    </div>
    <div class="login-content">
        <div class="login-account d-flex align-items-center">
            <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/account.png" alt="帳號">
            <input type="text" name="loginname" id="loginname" class="login-input" placeholder="商家帳號">
        </div>
        <div class="login-password d-flex align-items-center">
            <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/password.png" alt="密碼">
            <input type="password" name="passwd" id="pw" class="login-input" placeholder="請輸入密碼" focus="">
        </div>
        <div class="login-btn-login" onclick="enterprise_login()">商家登入</div>
<!--        <p class="login-forgotpassword" onclick="javascript:location.href='#'">忘記密碼?</p>-->
    </div>
    <div class="login-footer">
		<p><span class="login-footer-txt">還不是會員嗎?</span><a href="javascript:location.href='/site/enterprise/member_register'" class="ui-link">按此註冊</a></p>
	</div>
</div>

<!-- 2018-10-12 舊版面 -->
<!--
	<div style="text-align:center;padding:20px">
		<img src="<?php echo IMAGES_DIR."/site/sajapos.jpg"?>" style="width:40%;">
	</div>

	<div class="weui-cells3">
		<div style="text-align:center;padding:10px">
			<div class="weui-cell2" >
				<div class="weui-cell__hd" style="padding:0 2px;"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/people2.png" style="height:20px;"></div>	
				<div class="weui-cell__bd">
					<input name="loginname" id="loginname" value="" type="text" style="border-color:#FFFFFF" placeholder="商家帳號">
				</div>
			</div>	  
			<div class="weui-cell2" >
				<div class="weui-cell__hd"  style="padding:0 2px;"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/lock.png" style="height:20px;"></div> 
				<div class="weui-cell__bd">
					<input name="passwd" id="pw" value="" type="password" style="border-color:#FFFFFF;border:0px" placeholder="請輸入密碼">
				</div>
			</div>
		</div>			
	</div>
	<div style="background-color:#FFFFFF;height:50px;margin:30px;">  </div>
	<div class="nav">	
		<div class="weui-tabbar" style="background-color:#FFFFFF">
			<div style="width:3%;"> </div>
			<div style="width:94%; margin:0 auto;" >
				<button type="button" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0" onClick="enterprise_login()">登录</button>
				<button type="button" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0 0" onClick="javascript:location.href='<?php echo APP_DIR; ?>/enterprise/member_register/?<?php echo $cdnTime; ?>'">註冊</button>
			</div>	
			<div style="width:3%;"> </div>
		</div>
	</div>
-->
<!-- 2018-10-12 舊版面 /-->


<script>
    $(function(){
        $('.hasHeader').css({'paddingTop':'0'});

        //生成明暗碼切換按鈕
        var $parent = $('login-password'),
            $inpt = $('input[type^="password"]');
        $inpt.each(function(){
            var $that = $(this);
            $that.parent($parent)
                .append(
                    $('<div/>')
                    .addClass('password-btn d-flex align-items-center')
                    .append(
                        $('<i/>')
                        .addClass('esfas ycolor eye-slash')
                    )
                );
            
            var $thisEyeBtn = $that.parents($parent).find('.password-btn');
            $thisEyeBtn.removeClass('d-flex').hide();
            
            //明暗碼切換
            function toggleBtn(who,$thisBtn){
                if(who.val()==''){
                    $thisEyeBtn.removeClass('d-flex').hide();
                }else{
                    $thisEyeBtn.addClass('d-flex').show();
                }
            }
            //切到明碼
            function passwordEye() {
                $thisEyeBtn.find("i")
                    .removeClass('eye-slash')
                    .addClass('eye');
                $inpt.attr("type","text");
                $thisEyeBtn.one("click",passwordEyeSlash);
            }
            //切到暗碼
            function passwordEyeSlash() {
                $thisEyeBtn.find("i")
                    .removeClass('eye')
                    .addClass('eye-slash');
                $inpt.attr("type","password");
                $thisEyeBtn.one("click",passwordEye);
            }
            
            $that.on("keyup blur",function(){
                toggleBtn($(this),$thisEyeBtn);
            })
            $thisEyeBtn.one("click",passwordEye);
        });
    })
    
</script>