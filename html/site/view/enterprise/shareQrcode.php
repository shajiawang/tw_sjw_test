<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$enterprise=$_SESSION['sajamanagement']['enterprise'];
$share_url = _v('share_url');
$kind = _v('kind');  
?>
<style>
    body {
        background: #F9A823;
        height: 100vmax;
        min-height: unset;
    }
    .enterprise-content {
        background: unset;
        padding-bottom: 0;
        min-height: 100vh;
    }
    .enterprise-content .article {
        padding: 2.167rem .834rem;
        box-sizing: border-box;
        height: inherit;
    }
    .codeBox {
        padding: 2.334rem 2.834rem;
    }
    #qrcodeBox h5 {
        font-size: .8rem;
        color: #797979;
        margin-bottom: 4rem;
    }
    
    /*隱藏FooterBar*/
    .sajaFooterNavBar {
        display: none;
    }
</style>

<div class="article">
    <div class="codeBox">
        <div id="qrcodeBox">
            <?php if ($kind == "share"){ ?>
                <img src="<?php echo APP_DIR; ?>/phpqrcode/?data=<?php echo $share_url; ?>">
                <h5><p>分享專屬二維碼</p></h5>
            <?php }else{ ?>
                <img src="<?php echo APP_DIR; ?>/phpqrcode/?data=http://www.shajiawang.com/site/mall/topay/?esid=<?php echo $enterprise['esid'];?>">
                <h5><p>支付專屬二維碼</p></h5>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function(){
        //更改標題
        <?php if ($kind == "share"){ ?>
            $('.navbar-brand h3').text('分享專屬二維碼');
        <?php }else{ ?>
            $('.navbar-brand h3').text('支付專屬二維碼');
        <?php } ?>
    })
</script>




<!-- 2018/10/15 舊檔 -->
<!--
<style type="text/css">
    .codeBox {
	    background-color:#ffffff;
		padding-top:8%;
		padding-left:5%;
		padding-bottom:8%;
		padding-right:5%;
        border:#FF0000 20px solid;
	};
</style>
<div class="article" style="background-color:#FF0000" >

	<div class="codeBox">
		<div id="qrcodeBox" align="center" >
		<?php if ($kind == "share"){ ?>
			<img style="-webkit-user-select: none" width="85%" src="<?php echo APP_DIR; ?>/phpqrcode/?data=<?php echo $share_url; ?>" >
		<?php }else{ ?>
			<img style="-webkit-user-select: none" width="85%" src="<?php echo APP_DIR; ?>/phpqrcode/?data=http://www.shajiawang.com/site/mall/topay/?esid=<?php echo $enterprise['esid'];?>" >		
		<?php } ?>
		</div>
	</div>

	<div class="member-sqrcode">
		<?php if ($kind == "share"){ ?>
		<p style="font-weight:normal;text-shadow: 0 0px 0">分享专属二维碼</p>
		<?php }else{ ?>
		<p style="font-weight:normal;text-shadow: 0 0px 0">支付专属二维碼</p>
		<?php } ?>
	</div>
	
</div>
-->
<!-- 2018/10/15 舊檔/ -->
