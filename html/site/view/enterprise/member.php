<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$enterprise=_v('member_data');
?>
<div class="article">
	<ul class="user-lists-box">
        <?php if($_SESSION['sajamanagement']['enterprise']['sms'] != 'Y'){ ?>
            <!-- 綁定手機 -->
            <li class="user-list">
                <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('du');">
                    <div class="list-titlebox d-flex align-items-center">
                        <div class="list-icon">
                            <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png">
                        </div>
                        <div class="list-title">綁定手機</div>
                    </div>
<!--
                    <div class="list-rtxt d-inline-flex">
                        <div class="r-arrow"></div>
                    </div>
-->
                </a>
            </li>          
            <div id="du" class="switch-box" style="display:none">
                <div class="switch-body">
                    <div class="input_group d-flex align-items-center required">
                        <div class="label" for="nickname">手機號碼:</div>
                        <?php if(ctype_digit($enterprise['phone'])==true){ ?>
                            <input id="phone" name="phone" type="number" readonly value="<?php echo $enterprise['phone']?>">
                            <input type='hidden' id='act' name='act' value='verify_phone'>
                        <?php }else{ ?>
                            <input id="phone" name="phone" value="" type="number" min=0>
                            <input type='hidden' id='act' name='act' value='upd_verify_phone'>
                        <?php } ?>
                        <input type='hidden' id='ep' name='ep' value='Y'>
                    </div>
                </div>
                <?php if(ctype_digit($enterprise['phone'])!==true){ ?>
                    <div class="switch-btn">
                        <button type="button" id="topay" onClick="verify_phone();">確認修改</button>
                    </div>
                    <p class="small text-center">※殺友們請注意，如無法收到驗證短訊，煩請 <a class="link" href="/site/contact/">聯繫我們</a>。</p>
                <?php } ?>
            </div>
        <?php } ?>
        
        <!-- 聯絡信息 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('d1');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png">
                    </div>
                    <div class="list-title">聯絡信息</div>
                </div>
            </a>
        </li>
        <div id="d1" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center">
                    <div class="title">公司名稱</div>
                    <p class="infotxt"><?php echo $enterprise['companyname']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">對外營銷名稱</div>
                    <p class="infotxt"><?php echo $enterprise['marketingname']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">登記證號</div>
                    <p class="infotxt"><?php echo $enterprise['uniform']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">負責人</div>
                    <p class="infotxt"><?php echo $enterprise['name']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">聯絡電話</div>
                    <p class="infotxt"><?php echo $enterprise['phone']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">Email</div>
                    <p class="infotxt"><?php echo $enterprise['email']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">FAX</div>
                    <p class="infotxt"><?php echo $enterprise['fax']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">公司地址</div>
                    <p class="infotxt"><?php echo $enterprise['address']; ?></p>
                </div>
            </div>
        </div>
        
        <!-- 帳務信息 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('d2');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png">
                    </div>
                    <div class="list-title">帳務信息</div>
                </div>
            </a>
        </li>
        <div id="d2" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center">
                    <div class="title">帳務負責人</div>
                    <p class="infotxt"><?php echo $enterprise['account_contactor']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">帳務負責人電話</div>
                    <p class="infotxt"><?php echo $enterprise['account_phone']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">營運人</div>
                    <p class="infotxt"><?php echo $enterprise['execute_contactor']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">營運人電話</div>
                    <p class="infotxt"><?php echo $enterprise['execute_phone']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">撥款帳戶名</div>
                    <p class="infotxt"><?php echo $enterprise['accountname']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">撥款帳號</div>
                    <p class="infotxt"><?php echo $enterprise['accountnumber']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">帳戶銀行</div>
                    <p class="infotxt"><?php echo $enterprise['bankname']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">分行名稱</div>
                    <p class="infotxt"><?php echo $enterprise['branchname']; ?></p>
                </div>
            </div>
        </div>
		        
        <!-- 營業信息 -->
        <li class="user-list">
            <a href="#" class="user-list-link d-flex justify-content-between align-items-center" onClick="ReverseDisplay('d3');">
                <div class="list-titlebox d-flex align-items-center">
                    <div class="list-icon">
                        <img class="img-fluid" src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png">
                    </div>
                    <div class="list-title">營業信息</div>
                </div>
            </a>
        </li>
        <div id="d3" class="switch-box" style="display:none">
            <div class="switch-body">
                <div class="infobox d-flex align-items-center">
                    <div class="title">店家名稱</div>
                    <p class="infotxt"><?php echo $enterprise['exchange_store_name']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">店家網址</div>
                    <p class="infotxt"><?php echo $enterprise['url']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">營業開始時間</div>
                    <p class="infotxt"><?php echo $enterprise['businessontime']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">營業結束時間</div>
                    <p class="infotxt"><?php echo $enterprise['businessofftime']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">聯絡信箱</div>
                    <p class="infotxt"><?php echo $enterprise['contact_email']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">客服信箱</div>
                    <p class="infotxt"><?php echo $enterprise['service_email']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">營業年資</div>
                    <p class="infotxt"><?php echo $enterprise['seniority']; ?></p>
                </div>
                <div class="infobox d-flex align-items-center">
                    <div class="title">員工人數</div>
                    <p class="infotxt"><?php echo $enterprise['employees']; ?></p>
                </div>
            </div>
        </div>
    </ul>
</div>
<script type="text/javascript">
    $(function(){
        //更改標題
        $('.navbar-brand h3').text('商戶資料');
    });
    
    //調整標題寬度一致
    var options = {
        dom: {
            parent: '.switch-box',
            ul: '.switch-body',
            li: '.infobox',
            label: '.title',
            input: '.infotxt'
        }
    };
    $(function(){
        //目前顯示的ul區塊
        var $isShow = $(options.dom.parent);

        //取得應該顯示的寬度
        function changeWidth(dom) {
            var $selfLi = dom.find($(options.dom.li));
            var $selfLabel = $selfLi.find($(options.dom.label));

            //初始化
            $selfLabel.attr('style','');

            var $liW = '';
            var $arr = [];

            $liW = $(window).width();                         //取得li總寬度   
            
            $selfLabel.each(function(){                     //取得label寬度並存入陣列
                var $fs = parseFloat($(this).css('font-size'));
                $arr.push($(this).text().length * ($fs + 1.5) + 14);    //+2為字距空間 , +14 預留冒號空間
                console.log($arr);
            });
            var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
            var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

            //改變寬度
            $selfLabel.each(function(){
                $(this).css('width',$maxW);
            })
            $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                var $selfInput = $(this).find($(options.dom.input));
                $selfInput.attr('style','');
                $selfInput.each(function(){
                    if($selfInput.length > 1){
                        $(this).width(($remain / $selfInput.length));
                    }else{
                        $(this).width($remain);
                    }
                })
            })
        }

        //尺寸改變重新撈取寬度
        $(window).on('load resize',function(){
            $isShow.each(function(){
                changeWidth($(this));
            })
        })
        
    })
    
</script>

<!-- 2018/10/15 舊檔 -->
<!--
		<div class="article">

	<div class="weui-cells2">
	
		<?php if($_SESSION['sajamanagement']['enterprise']['sms'] != 'Y'){ ?>	
		<div class="weui-cell weui-cell_access" onClick="ReverseDisplay('du');">
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/penIcon.png" style="width:20px;height:20px;"></div>
			<div class="weui-cell__bd">
			  <p>绑定手機</p>
			</div>
			<div class="weui-cell__ft"><font style="font-size:12px;"> </font></div>
		</div>
		<div id="du" class="sajapay-content" style="display:none">
			<div style="background-color:#FFFFFF;padding:10px;">
				<div class="weui-cell2">
					<div class="weui-cell__hd" style="width:25%" ><span style="color:red;">*</span>手機號：</div>
					<div class="weui-cell__bd">
						<?php 
							 if(ctype_digit($enterprise['phone'])==true)
								echo "<input type='number' name='phone' id='phone' readonly='readonly' value='".$enterprise["phone"]."'>
										<input type='hidden' id='act' name='act' value='verify_phone' >"; 
							  else  
								echo "<input type='number' name='phone' id='phone' min=0 />
										<input type='hidden' id='act' name='act' value='upd_verify_phone' >";
						?>
						<input type='hidden' id='ep' name='ep' value='Y' >						
					</div>
				</div>
			</div>
			<div class="" style="background-color:#FFFFFF">
				<div style="width:3%;"> </div>
				<div style="width:94%; margin:0 auto;" >
					<button type="button" id="topay" onClick="verify_phone();" style="background-color:#F9A823;color:#FFFFFF;font-weight:normal;text-shadow: 0 0px 0 ">確認修改</button>
					<p style="font-size:75%;text-align:center;">※殺友们請注意，如無法收到驗證短信，烦請<a href="/site/contact/"> 联系我们</a>。</p>
				</div>
				<div style="width:3%;"> </div>
			</div>
		</div>			
		<?php } ?>
		
		<div class="weui-cell weui-cell_access" onClick="ReverseDisplay('d1');">
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png" style="width:20px;height:20px;"></div>
			<div class="weui-cell__bd">
			  <p>联系信息</p>
			</div>
			<div class="weui-cell__ft"><font style="font-size:12px;"> </font></div>
		</div>
		<div id="d1" class="sajapay-content enterprise-font1" style="display:none">
			<div style="background-color:#FFFFFF;margin:10px;border: 1px solid #D9D9D9;">
	
				<div class="weui-cell">
					<div class="weui-cell__bd">
						<p class="">公司名称：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['companyname']; ?></div>
				</div>					
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>对外营销名称：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['marketingname']; ?></div>
				</div>	
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>登記证號：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['uniform']; ?></div>
				</div>	
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>负责人：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['name']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>联系电话：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['phone']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>Email：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['email']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>FAX：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['fax']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>公司地址：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['address']; ?></div>
				</div>					
			</div>
		</div>
		
		<div class="weui-cell weui-cell_access" onClick="ReverseDisplay('d2');">
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png" style="width:20px;height:20px;"></div>
			<div class="weui-cell__bd">
			  <p>帳务信息</p>
			</div>
			<div class="weui-cell__ft"><font style="font-size:12px;"> </font></div>
		</div>
		<div id="d2" class="sajapay-content enterprise-font1" style="display:none">
			<div style="background-color:#FFFFFF;margin:10px;border: 1px solid #D9D9D9;">
	
				<div class="weui-cell">
					<div class="weui-cell__bd">
						<p>帳务负责人：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['account_contactor']; ?></div>
				</div>					
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>帳务负责人电话：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['account_phone']; ?></div>
				</div>	
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>营运人：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['execute_contactor']; ?></div>
				</div>	
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>营运人电话：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['execute_phone']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>拨款帳户名：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['accountname']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>拨款帳號：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['accountnumber']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>帳户银行：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['bankname']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>分行名称：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['branchname']; ?></div>
				</div>					
			</div>
		</div>
		
		<div class="weui-cell weui-cell_access" onClick="ReverseDisplay('d3');">
			<div class="weui-cell__hd"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/userdata.png" style="width:20px;height:20px;"></div>
			<div class="weui-cell__bd">
			  <p>营业信息</p>
			</div>
			<div class="weui-cell__ft"><font style="font-size:12px;"> </font></div>
		</div>
		<div id="d3" class="sajapay-content enterprise-font1" style="display:none">
			<div style="background-color:#FFFFFF;margin:10px;border: 1px solid #D9D9D9;">
	
				<div class="weui-cell">
					<div class="weui-cell__bd">
						<p>店家名称：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['exchange_store_name']; ?></div>
				</div>					
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>店家网址：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['url']; ?></div>
				</div>	
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>营业开始時間：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['businessontime']; ?></div>
				</div>	
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>营业结束時間：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['businessofftime']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>联系信箱：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['contact_email']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>客服信箱：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['service_email']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>营业年资：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['seniority']; ?></div>
				</div>
				<div class="weui-cell">
					<div class="weui-cell__bd">
					  <p>员工人數：</p>
					</div>
					<div class="weui-cell__ft"><?php echo $enterprise['employees']; ?></div>
				</div>					
			</div>
		</div>
		
	</div>
-->
<!-- 2018/10/15 舊檔/ -->
