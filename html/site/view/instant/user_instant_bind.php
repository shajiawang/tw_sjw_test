<style>
    .instant-content {
        
    }
    #instant-box {
        width: 100vw;
        background: #fefefe;
    }
    #instant-box .container {
        padding: 0;
        
    }
    .bind-container {
        margin: 0 3rem;
    }
    .user-bind-history {
        
    }
    .user-bind-history .bind-history-title {
        font-size: 1.5rem;
        font-weight: 600;
        padding-bottom: .5rem;
        border-bottom: 1px solid #d9d9d9;
    }
    .user-bind-history .bind-history-table {
        border: 1px solid #ededed;
        max-height: 5.5rem;
        margin: .8rem;
        overflow: scroll;
    }
    .bind-history-table .bind-history-tr {
        font-size: 1.2rem;
        padding: .5rem;
    }
    .bind-history-table .bind-history-tr:nth-child(2n) {
        background: #e5e5e5;
    }
    .bind-history-tr .bind-time {
        flex: 1
    }
    .bind-history-tr .bind-price {
        max-width: 10rem;
        white-space: nowrap;
        text-align: right;
        text-overflow: ellipsis;
        overflow: hidden;
    }
    
    .user-bind-title {
        text-align: center;
        font-size: 1.5rem;
        font-weight: 600;
        margin: 1.5rem 0;
    }
    
    .bind-inputBox {
        margin: 1rem;
        text-align: center;
    }
    .bind-inputBox input {
        font-size: 3rem;
    }
    .bind-inputBox .bind-unit {
        display: inline-block;
        font-size: 1.5rem;
        font-weight: 600;
    }
    
    /* 控制按鈕 */
    .footer-btn {
        margin: 2rem 0;
    }
    .btn-item {
        
    }
    .btn-item + .btn-item {
        margin-top: 1rem;
    }
    .btn-item-a {
        background: #f9a823;
        color: #FFF;
        font-size: 1.5rem;
        letter-spacing: .2rem;
        text-indent: .5rem;
        margin: .5rem;
        padding: 1rem;
        text-align: center;
        border-radius: 100vh;
        display: block;
        font-family: -apple-system-font, arial,"Microsoft JhengHei";
        border: none;
        outline: none;
    }
    .btn-item-a:active {
        background: #fed38d;
    }
    .footer-btn .bind-btn .btn-item-a.disabled,
    .footer-btn .bind-btn .btn-item-a:disabled {
        background: #ddd;
        pointer-events: none;
    }
    
    /* 驗證訊息 */
    .feedback {
       text-align: center;
        color: #FF0000;
        font-size: 1.2rem; 
    }
    
    /* 確定下標彈跳視窗 */
    .check-modal {
        
    }
    .check-modal .check-body {
        position: fixed;
        top: calc((100% - 80vmin) / 2);
        left: 0;
        right: 0;
        width: 80vmin;
        max-width: 500px;
        margin: 0 auto;
        text-align: center;
        z-index: 1070;
        background: #FFF;
        box-shadow: 0 1px 10px -1px #000;
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s;
        font-size: 20px;
        border-radius: 10px;
        overflow: hidden;
    }
    .check-body .check-title {
        color: #959595;
        padding: 3rem 0 .5rem;
    }
    .check-body .check-price {
        margin: .5rem 0 3rem;
        font-size: 2rem;
    }
    .check-body .check-btn-group {
        margin: 0;
    }
    .check-btn-group .close-btn,
    .check-btn-group .ok-btn {
        flex: 1;
        background: #f9a823;
        color: #FFF;
        font-size: 1.3rem;
        border: none;
        padding: 1rem;
        margin: 0;
        box-sizing: border-box;
        font-family: -apple-system-font, arial,"Microsoft JhengHei";
        letter-spacing: .5rem;
        text-indent: .5rem;
    }
    .check-btn-group .close-btn:active,
    .check-btn-group .ok-btn:active {
        background: #ffd185;
    }
    .check-btn-group .close-btn {
/*        background: #fd5d5d;*/
    }
    .check-btn-group .ok-btn {
        border-left: 1px solid #FFF;
/*        background: #57d2c4;*/
    }
    .check-modal .check-bg {
        position: fixed;
        z-index: 1060;
        left: 0;
        top: 0;
        width: 100%;
        height: 100%;
        background-color: rgba(0, 0, 0, 0.3);
    }
</style>
<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/keyboard.css">
<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">
   
<div id="instant-box">
    <div class="container">
        <!-- 商品資訊 -->
        <div class="productSaja-imgbox">
            <div class="productSaja-imgbox">
                <div class="clearfix sajaprod-img">
                    <ul class="slides d-flex">
                        <li class="d-flex align-items-center">
                            <img class="img-fluid product-img" src="">
                        </li>
                    </ul>
                </div>
                
                <h4 class="sajaprod-name"></h4>
            </div>
        </div>
        
        <div class="bind-container">
            <!-- 下標紀錄 -->
            <div class="user-bind-history">
                <div class="bind-history-title">下標紀錄</div>
                <div class="bind-history-table">
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">555555555555</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                    <div class="bind-history-tr d-flex">
                        <div class="bind-time"><span class="data-date">2018-11-27</span><span class="data-time"> 10:02:30</span></div>
                        <div class="bind-price"><span class="data-price">5</span> 元</div>
                    </div>
                </div>
            </div>

            <div class="user-bind-title"><span class="user-name">肥仔</span>，請出價</div>

            <div class="bind-inputBox">
                <input type="text" name="price" id="price" class="keyboard allinput-type2" min="1" pattern="[0-9]*" step="1" placeholder="請輸入要下標的金額">
                <div class="bind-unit">元</div>
            </div>
            <div class="feedback"></div>
            
            
            <div class="footer-btn">
                <div class="btn-item bind-btn">
                    <button class="btn-item-a" disabled>下標</button>
                </div>
                <div class="btn-item next-btn">
                    <button class="btn-item-a">下一檔</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!--  CDN Bootstrap 4.1.1  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
<!--  鍵盤-自訂義JS  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/js/keyboard.js"></script>

<!--  小鍵盤動作  -->
<script>
    options.dom.caller = '.keyboard';                   //命名:呼叫小鍵盤的物件
    options.dom.keyidName = 'numkeyModal';              //命名:小鍵盤(無id符號)
    options.dom.keyModal = '#' + options.dom.keyidName; //命名:小鍵盤(有id符號)
    options.dom.callerId = '#price';                    //預設一開始的呼叫者,在keyboard.JS會做變更 點擊者
    options.btn.sure = '確定';                           //小鍵盤送出按鈕名稱
    
    $(function(){
        $(options.dom.caller).each(function(i){
            var $modalNum = options.dom.keyidName+i;
            //console.log($modalNum);
            var $that = $(this);
            $that.on("click",function(e){
                options.dom.callerId = $that;                           //記錄點擊哪個input的ID
                //開啟小鍵盤的動作
                //若預設開啟時，此段不會執行
                $('#'+$modalNum).on('show.bs.modal', function (e) {
                    controlSize($that);
                });
                
                //關閉小鍵盤的動作
                $('#'+$modalNum).on('hide.bs.modal', function (e) {
                    var $callerId = $(options.dom.callerId);            //取得按鈕ID
                    var $feedback = $('.feedback');                     //驗證msg
                    var $callerVL = $callerId.val().length;
                    
                    
                    console.log($callerVL)
                    if($callerVL < 1) {
                        $(esinstantOptions.btn.bind).attr('disabled',true);
                    }
                    else if($callerVL > 10){
                        $callerId.val($callerId.val().substr(0,10));
                        $feedback.text('限制 最多10個字元');
                        $(esinstantOptions.btn.bind).attr('disabled',true);
                    }else{
                        $feedback.text('');
                        $(esinstantOptions.btn.bind).attr('disabled',false);
                    }
                    
                    //移除 將輸入框上推的方塊
                    removeBlank();
                });
            })
        });
//        setTimeout(function(){
//            $(options.dom.callerId).click();                //小鍵盤預設開啟 (類似input的focus)
//        },200);
    })

    
    
    
</script>

<script>
    var esinstantOptions = {
        dom: {
            productImg: '.product-img',
            productName: '.sajaprod-name',
            productPrice: '.market-price',
            downtime: '.countdown-num',
        },
        getData: {
            productImgUrl: '',
            productName: '',
            productPrice: '',
            QRcodeUrl: '',
        },
        input: {
            bind: '#price'
        },
        btn: {
            bind: '.footer-btn .bind-btn .btn-item-a',
            next: '.footer-btn .next-btn .btn-item-a'
        }
    }
    $(function(){
        esinstantOptions.nopicText = '暫無閃殺商品';
        esinstantOptions.nopicUrl = '<?php echo BASE_URL.APP_DIR;?>/static/img/nohistory-img.png';          //無資料時顯示
        
        $('.sajaNavBar').addClass('d-flex').show();
        $('.sajaNavBar').find('h3').text('閃殺下標');
        $('.sajaNavBar').find('.sajaNavBarIcon').hide();
        $('.other-content').removeClass('instant-content').addClass('other-content');
        
        //撈取資料
        getInstantData_user(function($hasInstant){
            if($hasInstant){         //有殺品時，執行此行

            }else{                  //無殺品時，執行此行
                var $nodataText = esinstantOptions.nopicText;
                $('#instant-box').html(
                    $('<div/>')
                    .addClass('nohistory-box')
                    .append(
                        $('<div/>')
                        .addClass('nohistory-img')
                        .append(
                            $('<img/>')
                            .addClass('img-fluid')
                            .attr('src',esinstantOptions.nopicUrl)
                        )
                    )
                    .append(
                        $('<div/>')
                        .addClass('nohistory-txt')
                        .text($nodataText)
                    )
                )
                
            }
        });
        
        //按鈕群
        $(esinstantOptions.btn.bind).on('click', function(){
            //開啟確認框
            if($('body').find('.check-modal').length <= 0){
                $('body').append(
                    $('<div/>')
                    .addClass('check-modal')
                    .append(
                        $('<div/>')
                        .addClass('check-body')
                        .append(
                            $('<div/>')
                            .addClass('check-title')
                            .text('下標金額')
                        )
                        .append(
                            $('<div/>')
                            .addClass('check-price')
                            .append(
                                $('<span/>')
                                .text('100')
                            )
                            .append(
                                $('<span/>')
                                .text('元')
                            )
                        )
                        .append(
                            $('<div/>')
                            .addClass('check-btn-group d-flex')
                            .append(
                                $('<button/>')
                                .addClass('close-btn')
                                .text('取消')
                            )
                            .append(
                                $('<button/>')
                                .addClass('ok-btn')
                                .text('確定')
                            )
                        )
                    )
                    .append(
                        $('<div/>')
                        .addClass('check-bg')
                    )
                )
                $('body').addClass('modal-open');
                
                $('.close-btn').on('click', function(){
                    $('body').find('.check-modal').remove();
                    $('body').removeClass('modal-open');
                    $('#price').val('');
                    $(esinstantOptions.btn.bind).attr('disabled',true);
                });
                
                $('.ok-btn').on('click', function(){
                    $('body').find('.check-modal').remove();
                    $('body').removeClass('modal-open');
                    $('#price').val('');
                    $(esinstantOptions.btn.bind).attr('disabled',true);
                });
                
            }
        })
        $(esinstantOptions.btn.next).on('click', function(){
            //判斷目前閃殺商品是否換檔
        })
    })
    
    //畫面一開啟 取得商品圖、商品名稱、倒數時間
    function getInstantData_user(callback) {
        esinstantOptions.getData.productImgUrl = 'https://www.saja.com.tw/site/images/site/product/ab3459dbf98ce54375cb49c30df8cb6c.png';
        esinstantOptions.getData.productName = 'IPhone 7plus';
        esinstantOptions.getData.productPrice = '32000';

        var $downtimeMin = 3;                             //秒
        var $timeToMSecond = $downtimeMin * 1000;      //轉換毫秒
        
        esinstantOptions.getData.downtime = $timeToMSecond;
        
        $hasInstant = true;  //有無下標商品
        
        callback($hasInstant);
//        esinstantOptions.getData.downtime = new Date();
//        console.log(esinstantOptions.getData.downtime)
//        var $ajaxUrl = '';
//        
//        $.ajax({  
//            url: $ajaxUrl,
//            contentType: 'application/json; charset=utf-8',
//            type: 'GET',
//            dataType: 'json',
//            async: 'false',
//            cache: 'false',
//            timeout: '4000',
//            processData: false,
//            contentType: false,
//            complete: endLoading,
//            error: showFailure,  
//            success: showResponse
//        });
//
//        function showResponse(data){
//        }
        
//        //加載動作完成 (不論結果success或error)
//        function endLoading() {
//        }
        
//        //加載錯誤訊息
//        function showFailure() {
//        }
        
        showInstantData_user();
    }
    
    //更改DOM資訊，之後用 ajax的success取代
    function showInstantData_user() {
        $(esinstantOptions.dom.productImg).attr('src',esinstantOptions.getData.productImgUrl);

        $(esinstantOptions.dom.productName).text(esinstantOptions.getData.productName);
        $(esinstantOptions.dom.productPrice).text(esinstantOptions.getData.productPrice);

        var $downtime = esinstantOptions.getData.downtime;                              
        $(esinstantOptions.dom.downtime).attr('data-countdowm',$downtime);                          //儲存data 毫秒數
        mSecondeChange($downtime,function($minute,$second,$msecond){                                //換算分秒
            $(esinstantOptions.dom.downtime).text($minute+':'+$second);
        })
    }
    var $timeCountDown;
    
    //毫秒 > 換算
    function mSecondeChange(time,callback) {
        var $downtime = time;
        var $minute = parseInt($downtime / 60 / 1000);
        var $second = parseInt(($downtime - $minute * 60 * 1000) / 1000);
        var $msecond = $downtime - $minute * 60 * 1000 - $second * 1000;
        
        //秒--補零
        if($second < 10){
            $second = '0'+String($second);
        }
        
        //毫秒--補零
        if($msecond < 10){
            $msecond = '00'+String($msecond);
        }else if($msecond < 100){
            $msecond = '0'+String($msecond);
        }
        
        callback($minute,$second,$msecond);
    }
</script>
