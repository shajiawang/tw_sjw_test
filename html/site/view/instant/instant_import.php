<style>
    .instant-content {

    }
    #instant-box {
        width: 100vw;
        min-height: 100vh;
        background: #dbdbdb;
    }
    #instant-box .container {
        padding: 3rem 2rem;
    }
    #instant-box .instant-header {
        background: #eee;
        padding: 1rem;
        border-radius: 1rem;
    }
        .instant-header .countdown-box {
            text-align: center;
            font-size: 3rem;
            margin: 1rem;
        }
        .instant-header .header-box {
            margin-bottom: 1rem;
        }

            .header-box .instant-product {
                max-width: 50%;
            }
            .header-box .instant-qrcode {
                max-width: 35%;
                padding: 1rem;
                background: #FFF;
                border-radius: 1rem;
            }

    #instant-box .instant-footer {
        margin: 1rem auto;
    }
        .instant-footer .product-info {
            margin-left: 2rem;
            font-size: 2rem;
            text-align: center;
        }
            .product-info .product-name {

            }
            .product-info .market-price-box {
                margin-top: .5rem;
            }
        .instant-footer .footer-btn {
            width: 50%;
        }
            .footer-btn .instant-go-btn {
                font-family: -apple-system-font, arial,"Microsoft JhengHei";
                border: none;
                width: 100%;
                text-align: center;
                background: #FFC107;
                padding: 1rem;
                border-radius: 1rem;
                font-size: 1.5rem;
                box-shadow: 0 1px 10px #414141, inset 2px 2px 20px #fff, inset -2px -2px 20px #909090;
                box-sizing: border-box;
                font-weight: bold;
                color: #000;
                text-shadow: -1px -1px 1px #FFF, 1px 1px 1px #7a7a7a;
            }
            .footer-btn .instant-go-btn:hover {
                width: 100%;
                text-align: center;
                background: #FFC107;
                padding: 1rem;
                border-radius: 1rem;
                font-size: 1.5rem;
                box-shadow: 0 1px 10px #414141, inset 2px 2px 20px #fff, inset -2px -2px 20px #909090;
                box-sizing: border-box;
                font-weight: bold;
                color: #000;
                text-shadow: -1px -1px 1px #FFF, 1px 1px 1px #7a7a7a;
            }
</style>

<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="http://test.shajiawang.com/site/static/css/flexbox.css">


<div id="instant-box">
    <div class="container">
        <div class="instant-header">
            <div class="countdown-box">
                <span>02:50:55</span>
            </div>
            <div class="header-box d-flex align-items-center justify-content-around">
                <div class="instant-product">
                    <img class="img-fluid" src="https://www.saja.com.tw/site/images/site/product/ab3459dbf98ce54375cb49c30df8cb6c.png" alt="">
                </div>
                <div class="instant-qrcode">
                    <img class="img-fluid" src="/site/phpqrcode/?data=https://www.saja.com.tw/management/webtx/vendorConfirm2?tx_key=NzkyOHwxZTE5YzM0NGJiODA2MWVjMWIwODc5Y2FkODY5MzBkMQ==" alt="">
                </div>
            </div>
        </div>

        <div class="instant-footer d-flex">
            <div class="product-info">
                <div class="product-name">IPhone 7plus</div>
                <div class="market-price-box">市價:<span class="market-price">32000</span></div>
            </div>

            <div class="footer-btn ml-auto">
                <button class="instant-go-btn">一起搶殺去</button>
            </div>
        </div>
    </div>
</div>

<?php echo _v('test'); ?>
<script>
    $(function(){
        $('.sajaNavBar').removeClass('d-flex').hide();
        $('.other-content').removeClass('other-content').addClass('instant-content');
    })
</script>
