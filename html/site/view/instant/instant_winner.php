<style>
    #instant-box {
        width: 100vw;
        min-height: 100vh;
        background: #dbdbdb;
    }
    #instant-box .container {
        padding: 5rem 0;
    }
    
    .winner_img {
        width: 30vmin;
        height: 30vmin;
        border-radius: 100%;
        overflow: hidden;
        margin: 0 auto;
    }
    .winner_img img {
        width: 100%;
        height: auto;
    }
    
    .winner_txt {
        margin: 2rem auto 0;
        text-align: center;
        font-size: 2rem;
        line-height: 3rem;
    }
    .winner_txt .winner_name {
        margin-left: 1rem;
        font-weight: 600;
        color: #E91E63;
    }
    .winner_txt .winner_price {
        margin: 0 1rem;
        font-weight: 600;
        color: #E91E63;
    }
    
    .footer_btn {
        margin: 2rem auto;
        text-align: center;
        font-size: 2rem;
    }
    .footer_btn .next_btn {
        display: block;
        font-size: 1.5rem;
        color: #FFF;
        background: #FF9800;
        width: 40vmin;
        margin: 0 auto;
        padding: 1rem 0rem;
        border-radius: 10rem;
    }
    .footer_btn .next_btn:hover {
        background: #de8500;
        opacity: .7;
    }
</style>

<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">
   
<div id="instant-box">
    <div class="container">
        <div class="winner_img">
            <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/instant/test_user_img.jpg" alt="">
        </div>
        <div class="winner_txt">
            <div class="txt_1">恭喜<span class="winner_name">殺價小天使</span></div>
            <div class="txt_2">以<span class="winner_price">400</span>元</div>
            <div class="txt_3">得標</div>            
        </div>
        <div class="footer_btn">
            <a class="next_btn" href="<?php echo BASE_URL.APP_DIR;?>/testeason/instant_import">下一檔</a>
        </div>
    </div>
    
</div>


<script>
    $(function(){
        $('.sajaNavBar').removeClass('d-flex').hide();
        $('.other-content').removeClass('other-content').addClass('instant-content');
    })
</script>
