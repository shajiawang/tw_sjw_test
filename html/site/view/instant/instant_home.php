<style>
    .instant-content {
        
    }
    #instant-box {
        width: 100vw;
        min-height: 100vh;
        background: #fefefe;
    }
    #instant-box .container {
        padding: 1.667rem 1.7rem;
    }
    #instant-box .title {
        font-size: 1.3rem;
        margin: 1rem 0;
        color: #a5a5a5;
        font-weight: 600;
    }
    .instant-select-box {
        
    }
    .instant-select-box .select-item,
    .instant-select-box .select-item:active,
    .instant-select-box .select-item:visited,
    .instant-select-box .select-item:focus {
        display: block;
        background: #ffe6b1;
        padding: 1.25rem;
        font-size: 1.584rem;
        border-radius: 1rem;
        /* box-shadow: 0 1px 10px #e0a901, inset 0 -5px 10px #fae198, inset 0 5px 10px #ffffff; */
        text-align: center;
        color: #F9A823;
        cursor: pointer;
    }
    .instant-select-box .select-item:hover {
        background: #fdd175;
        box-shadow: 0 0 10px #ff8d6a;
    }
    .instant-select-box .select-item + .select-item {
        margin-top: 1.25rem;
    }
    .instant-select-box .select-item small {
        font-size: 1.2rem;
        margin-left: 1rem;
    }
    
</style>

<!-- flexbox 框架架構 -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/flexbox.css">
   
<div id="instant-box">
    <div class="container">
<!--        <div class="title">場次</div>-->
        <div class="instant-select-box">
            <a class="select-item" href="<?php echo BASE_URL.APP_DIR;?>/testeason/instant_import">台中場</a>
            <a class="select-item" href="<?php echo BASE_URL.APP_DIR;?>/testeason/instant_import">高雄場</a>
        </div>
    </div>
</div>


<script>
    $(function(){
        $('.sajaNavBar').addClass('d-flex').show();
        $('.sajaNavBar').find('h3').text('主持閃殺')
        $('.other-content').removeClass('instant-content').addClass('other-content');
    })
</script>
