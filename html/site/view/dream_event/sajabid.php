<?php
$product = _v('product');
$sajabid = _v('sajabid');
$status = _v('status');
$meta=_v('meta');
$canbid=_v('canbid');
$cdnTime = date("YmdHis");
$options = _v('options');
$type=_v('type');
$paydata=_v('paydata');
$spoint = _v('spoint'); 
if(!isset($canbid)) {
    $canbid=$_REQUEST['canbid'];
}
if(empty($canbid)) {
    $canbid='Y';
}
//下標費用收取判斷
switch($product['totalfee_type']) {
	case 'A':
		 $totalfee_type = "下標金額 及 手續費";	
		 break;
	case 'B':
		 $totalfee_type = "下標金額";
		 break;
	case 'F':
		 $totalfee_type = "手續費";
		 break;
	case 'O':
		 $totalfee_type = "免費";
		 break;
}
//閃殺商品
//$flash_img = ($product['is_flash']=='Y') ? 'class="flash_img"' : '';
$ip="";
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
?>
<!--  鍵盤-自訂義CSS  -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/keyboard.min.css">
<!-- 此頁css -->
<link rel="stylesheet" href="<?php echo BASE_URL.APP_DIR;?>/static/css/sajabid.css">

<div id="tobid" class="swipe-tab d-flex flex-column">
    <div class="swipe-navbar d-flex">
        <!--20181210 隨機移除 檔案sajabid181210-->
        <div class="swipe-navbar-item on-active" href="#tab2" onClick="sajabid_single()">
            <span>單次下標</span>
        </div>
        <div class="swipe-navbar-item" href="#tab1" onClick="sajabid_range()">
            <span>連續下標</span>
        </div>
    </div>
      
    <div class="swipe-navbar-content bgc_gary">
        <!-- 20190508 警告文字 -->
        <div class="warn_directions text-center w-100 psr">
            <!-- 20190515使否需要手續費顯示 -->
            <div class="d-small sajabid-small">
                <!-- <p>可重複下標，請自行確認數字是否重覆</p> -->
                <!-- <p class="orange">*每次包牌總數不得超過 <?php echo $product['usereach_limit']; ?> 標</p> -->
                <?php if($product['totalfee_type'] == 'O'){ ?>
                    <span class="orange">本商品下標 <span class="txt_red"><?php echo $totalfee_type?></span></span>
                    <?php }else if($product['totalfee_type'] == 'A'){ ?>
                    <span class="txtfw">本商品下標收
                        <span class="txt_red">下標金額</span> 及 <span class="txt_red">手續費</span><span class="fee txt_red"><?php echo ' ($'.$product['saja_fee'].'元/標)';?></span></span>
                    </span>
                <?php }else if($product['totalfee_type'] == 'B'){ ?>
                <span class="txtfw">本商品下標只收
                    <span class="txt_red"><?php echo $totalfee_type?></span>
                </span>
                <?php }else{ ?>
                    <span>本商品下標只收 <span class="txt_red"><?php echo $totalfee_type; ?><span class="fee"><?php echo ' ($'.$product['saja_fee'].'元/標)';?></span>
                </span>
                <?php } ?>
            </div>
            <!-- <span>本商品下標只 <span class="txt_red txtfw">收手續費</span> <span class="txt_red">（＄50/標 ）</span></span> -->
        </div>
        <!-- 20190508 連續下標 -->
        <div id="tab1" class="content-item sajabid_range m25 under_chip">
            <div class="item-box">
                <div class="toptxt d-flex align-items-center">
                    <div class="text-center w-100">下標金額</div>
                     <!-- 20190508 隱藏  移除class d-flex -->
                     <div class="toptxt-small  flex-column  d-none">
                        <div>（官方售價：<?php echo $product['retail_price']; ?> 元）</div>
                        <div>（得標處理費：<?php echo $product['process_fee']+$product['checkout_money']; ?> 元）</div>
                    </div>
                </div>
                <div class="text-center">
                    <div class="placeholder sajabid-text">
                        <div class="d-flex align-items-end justify-content-center">
                            <div class="sajabid-font1"> <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_start.png"></div>
                            <input type="tel" name="price_start" id="price_start" class="keyboard allinput-type2" min="1" pattern="[0-9]*" step="1" onkeyup="sajabid_range()" focus placeholder="請輸入要下標的金額"/>
                        </div>
                        <div class="d-flex align-items-end justify-content-center">
                            <div class="sajabid-font1"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_last.png"></div>
                            <input type="tel" name="price_stop" id="price_stop" class="keyboard allinput-type2" min="1" pattern="[0-9]*" step="1" onkeyup="sajabid_range()" placeholder="請輸入要下標的金額"/>
                        </div>
                    </div>
                </div>
            </div>
 
            <div class="warn_txt">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_mark.png">
                <span>此平台機制可重複下標，請自行確認數字是否重複!!!</span>          
            </div>
        </div>
        <div id="bid_count"></div>
        <!-- 20190508 單次下標 -->
        <div id="tab2" class="content-item sajabid_single on-active m25 under_chip">
            <div class="item-box">
                <div class="toptxt d-flex align-items-center ">
                    <div class="text-center w-100">下標金額</div>
                     <!-- 20190508 隱藏  移除class d-flex -->
                    <div class="toptxt-small  flex-column  d-none">
                        <div>（官方售價：<?php echo $product['retail_price']; ?> 元）</div>
                        <div>（得標處理費：<?php echo $product['process_fee']+$product['checkout_money']; ?> 元）</div>
                    </div>
                </div>
                <div class="text-center">
                    <div class="placeholder sajabid-text">
                        <div class="d-flex align-items-end justify-content-center">
                        <!-- 20190507 多加單字圖片 -->
                        <div class="sajabid-font1"><img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_single.png"></div>
                            <input type="tel" name="price" id="price" class="keyboard allinput-type2" min="0" pattern="[0-9]*" step="1" onkeyup="sajabid_single()" autofocus placeholder="請輸入要下標的金額"/>
                            <!-- <input type="tel"  min="0" pattern="[0-9]*" step="1" onkeyup="sajabid_single()" autofocus placeholder="請輸入要下標的金額"/> -->
                            <!-- <div name="price" id="price" class="keyboard allinput-type2" onkeyup="sajabid_single()"></div> -->
                        </div>
                    </div>
                     <!-- 20190508 隱藏 small sajabid-small -->
                    <div class="d-none">
                        <p>可重複下標，請自行確認數字是否重覆</p>
                        <?php if($product['totalfee_type'] == 'O'){ ?>
                            <p class="orange">*本商品下標不收任何金額</p>
                        <?php }else if($product['totalfee_type'] == 'A' || $product['totalfee_type'] == 'F'){ ?>
                            <p class="orange">*本商品下標<?php echo $totalfee_type.'($'.$product['saja_fee'].' /標)'; ?> </p>
                        <?php }else{ ?>
                            <p class="orange">*本商品下標<?php echo $totalfee_type; ?></p>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="warn_txt">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_mark.png">
                <span>此平台機制可重複下標，請自行確認數字是否重複!!!</span>          
            </div>
        </div>
        <div id="bid_single" class="on-active"></div>
    </div>

    <!-- 下標紀錄 -->
    <div class="th_ecords bgc_gary d-none hasHeader">

        
        <div class="navbar sajaNavBar d-flex align-items-center">
        <div class="icon_back">
            <a href="javascript:;" class="ui-link">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
            </a>
        </div>
        <div class="navbar-brand">
            <h3>下標記錄</h3>
        </div>
        <div class="sajaNavBarIcon qrcode d-none">
            <a href="#left-panel-c21d6f1c58eb4e0fcd6860bdcdbec098" data-iconpos="right" class="ui-link">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/hamburger.png">
            </a>
        </div>
        <div>
            <div class="th_recordbet_index">
                <a href="javascript:;" class="ui-link">
                <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/Group@2x.png" class="icon_close">
                </a>
            </div>
            </div>
        </div>
        <div id="record" style="overflow-y: scroll; height: calc(100% - 64px);;">
            <div class="frequency">
                <header>
                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/icon_record.png">
                </header>
                <div>
                    <table>
                        <thead>
                            <tr>
                                <th>下標金額</th>
                                <th>下標次數</th>
                            </tr>
                        </thead>
                        <tbody>
                                
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
    <div class="th_index hasHeader">
        <div class="index_navbar">下標區間</div>
        <div class="index_list">
            <li class="index_box" id="1" >1</li>
        </div>
    </div>
    
    <form id="sajabid" name="sajabid" method="post" action="<?php echo BASE_URL.APP_DIR; ?>/dream_product/sajatopay/">
        <input type="hidden" name="type" id="type" value="single">
        <input type="hidden" name="count" id="count" value="1"/>
        <input type="hidden" name="fee_all" id="fee_all" value="<?php echo $product['saja_fee'];?>"/>
        <input type="hidden" name="total_all" id="total_all" value="0">
        <input type="hidden" name="bida" id="bida" value="0"/>
        <input type="hidden" name="bidb" id="bidb" value="0"/>
        <input type="hidden" name="totalfee_type" id="totalfee_type" value="<?php echo $product['totalfee_type']; ?>"/>
        <input type="hidden" name="saja_fee" id="saja_fee" value="<?php echo $product['saja_fee'];?>"/>
        <input type="hidden" name="autobid" id="autobid" value="<?php echo $paydata['autobid'];?>"/>
        <input type="hidden" name="productid" id="productid" value="<?php echo $product['productid']; ?>">
        <input type="hidden" name="userid" id="userid" value="<?php echo $_SESSION['auth_id']; ?>">
        <input type="hidden" name="retail_price" id="retail_price" value="<?php echo $product['retail_price']; ?>">
		<input type="hidden" name="usereach_limit" id="usereach_limit" value="<?php echo $product['usereach_limit']; ?>">
        <input type="hidden" name="unix_offtime" id="unix_offtime" value="<?php echo $product['unix_offtime']; ?>">
        <input type="hidden" name="now" id="now" value="<?php echo $product['now']; ?>">
        <input type="hidden" name="totalpages" id="totalpages">
        <input type="hidden" name="totalcount" id="totalcount">
		
        <input type="hidden" name="dpname" id="dpname" value="<?php echo $product['dpname']; ?>">
        <input type="hidden" name="dpcontent" id="dpcontent" value="<?php echo $product['dpcontent']; ?>">
        <input type="hidden" name="dpaddress" id="dpaddress" value="<?php echo $product['dpaddress']; ?>">		
		<input type="hidden" name="dppic" id="dppic" value="<?php echo $product['dppic']; ?>">

        <div class="sajabid-footernav">
            <div class="footernav-button">
				<?php if (!empty($_SESSION['auth_id'])) { ?>
                <button type="button" id="topay" onClick="saja_gopay(this.form);">下一步</button>
				<?php }else{ ?>
				<button type="button" id="topay" onclick="nologin();">下一步</button>
				<?php } ?>  
            </div>
        </div>
    </form>
</div>


<!--  Bootstrap 4.1.1  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/vendor/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>

<!--  鍵盤-自訂義JS  -->
<script src="<?php echo BASE_URL.APP_DIR;?>/static/js/keyboard_v1.2.js"></script>
    
<!--  小鍵盤動作  -->
<script>
    options.dom.caller = '.keyboard';                   //命名:呼叫小鍵盤的物件
    options.dom.keyidName = 'numkeyModal';              //命名:小鍵盤(無id符號)
    options.dom.keyModal = '#' + options.dom.keyidName; //命名:小鍵盤(有id符號)
    options.dom.callerId = '#price';                    //預設一開始的呼叫者,在keyboard.JS會做變更 點擊者
    options.btn.sure = '確定';                           //小鍵盤送出按鈕名稱
    
    $(function(){
        $(options.dom.caller).each(function(i){
            var $modalNum = options.dom.keyidName+i;
            var $that = $(this);
            $that.on("click",function(e){
                options.dom.callerId = $that;                           //記錄點擊哪個input的ID
                //開啟小鍵盤的動作
                //若預設開啟時，此段不會執行
                $('#'+$modalNum).on('show.bs.modal', function (e) {
                    $('.allinput-type2').css('border-bottom', '2px solid #d7d7d7');                           //執行onkeyup函數

                });

                //關閉小鍵盤的動作
                $('#'+$modalNum).on('hide.bs.modal', function (e) {
                    var $callerId = $(options.dom.callerId);            //取得按鈕ID
                    $callerId.keyup();   
                    $('.allinput-type2').css('border-bottom', '1px solid #E5E5E5');                           //執行onkeyup函數
                });
            })
        });
        setTimeout(function(){
            $(options.dom.callerId).click();                //小鍵盤預設開啟 (類似input的focus)
        },200);
    });
    
    //查看詳情
    // $('.more-details').on('click', function(){
    //     $('.detailsModal').show();
    //     $('.detailsModal-cover').show();
    //     $('body').addClass('modal-open');
    // })
    
    // $('.modal-details-close, .detailsModal-cover').on('click', function(){
    //     $('.detailsModal').hide();
    //     $('.detailsModal-cover').hide();
    //     $('body').removeClass('modal-open');
    // })
    
</script>
    

<script type="text/javascript">
    //滿版
    // $("#record").scroll(function () {
    //     var scrollTop = $(this).scrollTop();
    //     var scrollHeight = $('.frequency').height();
    //     var windowHeight = $(this).height();
    //     var p = 2
    //     console.log('scrollTop',scrollTop,'scrollHeight',scrollHeight,'windowHeight',windowHeight)
    //     if (scrollTop + scrollHeight + 38 == windowHeight) {
    //         console.log("p", p)
    //         loading_record(p)
    //     } else if(scrollTop + scrollHeight  >= windowHeight * 2){
    //         p = p + 1
    //         console.log("p", p)
    //         loading_record(p)
    //         return
    //     }else if(scrollTop + scrollHeight  >= windowHeight * 3){
    //         p = p + 1
    //         console.log("p", p)
    //         loading_record(p)
    //         return
    //     }
    //     return
    // });
    $(window).load(function(){
        var $winH = $(window).height();
        var $navbarH = $(".navbar").outerHeight(true);
        $("#tobid").css("min-height", $winH - $navbarH);
        $(".product").css("background","#FFF");


    });
	
	function nologin() {
		// alert("殺友請先登入 !!"); 
		location.href="/site/member/userlogin/";
	}	
</script>
<script type="text/javascript">

/*     $(".icon_back").click(function () { 
        $('.th_ecords').toggleClass("d-block");

        if($('.th_index').css('right') == "0px"){
            $('.th_index').css('right', '-65px');
            $('.frequency').css('transform', 'translateX(0px)');
        }
    });

    $(function () {
        var productid = document.getElementById("productid").value;
        var perpage = 12;
        var page = 1;
        var list = []
        function loading_total(total) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'<?php echo BASE_URL.APP_DIR;?>/product/get_user_onbid_list/?json=Y&productid='+productid+'&p='+1+'&perpage='+total,
                success: function (msg) {
                    var up_record=msg.retObj.data.record;
                    list = up_record.map(item => Object.values(item)[0]);
                    // console.log('li',list)
                    return list
                }
            }); 
        }
        function loading_record(p, c, id) {
            $.ajax({
                type: 'POST',
                dataType: 'json',
                url:'<?php echo BASE_URL.APP_DIR;?>/product/get_user_onbid_list/?json=Y&productid='+productid+'&p='+p+'&perpage='+c,
                success: function (msg) {
                    console.log(msg)
                    var up_record=msg.retObj.data.record;
                    var totalcount =msg.retObj.data.page.totalcount;
                    var totalpages =msg.retObj.data.page.totalpages;
                    var item =msg.retObj.data.page.item;
                    console.log('up_record',up_record)

                    $('#totalpages').attr('value', totalpages)
                    $('#totalcount').attr('value', totalcount)


                    loading_total(totalcount)
                    if(p > msg.retObj.data.page.totalpages){
                        return
                    }else if(p <= msg.retObj.data.page.totalpages){
                        insertDiv(up_record, totalcount);
                    }

                    if(c == totalcount.toString( )){
                        test(id);
                    } 
                    // return list



                }
            }); 
        }

        loading_record(page, perpage);

        function insertDiv(up_record, totalcount) {

            console.log('totalcount',totalcount)
            for(var key =0;key<totalcount;key++ ){
                console.log(key);
                $(".frequency tbody").append('<tr id="i_' + up_record[key].price + '"><td class='+ Math.floor(up_record[key].price) +'>'+Math.floor(up_record[key].price)+'</td><td>'+up_record[key].count+'</td></tr>');
            }
        }
        $("#record").scroll(function () {
            var scrollT = $(this).scrollTop();  
            var thisH = $(this).height()
            var totalp = $('#totalpages').val()
            var totalH = thisH + scrollT
            for(var n =0; n< totalp ; n++ ){
                if ( totalH == 689 + (456 * n) ) {
                    loading_record(n+2, 12);
                }
            }
        });

        var index_range = parseInt(<?php echo $product['retail_price']; ?>/100);
        for(var n =1; n<10 ;n++ ){
            $(".index_list").append("<li class='index_box' id='"+index_range*n*10+"'>"+ index_range*n*10 +"</li>");
        };
        $(".index_box").click(function() {
            $('#'+this.id).addClass('active');
            $('#'+this.id).siblings().removeClass('active');

            $(".frequency tbody").empty(); 
        
            var totalp = $('#totalpages').val()
            var totalc = $('#totalcount').val()


            loading_record(1, totalc, this.id);


        })

        function test(id){
            var index_id = "i_" + id
            if($("#" + index_id).size() > 0) {
                document.getElementById("i_" + id).scrollIntoView();
            } else {

                var this_arr=id;
                list.sort(function(a,b){
                    return Math.abs(a-this_arr)-Math.abs(b-this_arr);
                })
                document.getElementById("i_" + list[0]).scrollIntoView({ behavior: 'auto' });
            }
        }
    });

    /* 開啟索引 */
   /* $(".th_recordbet_index").click(function () { 
        
        if($('.th_index').css('right') == "-65px"){
            $('.th_index').css('right', '0px');
            $('.frequency').css('transform', 'translateX(-32.5px)');


        }else if($('.th_index').css('right') == "0px"){
            $('.th_index').css('right', '-65px');
            $('.frequency').css('transform', 'translateX(0px)');

        }
    }); */


</script>
