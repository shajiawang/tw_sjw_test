<?php
$status = _v('status');
$product_list = _v('product_list');
$canbid=_v('canbid');
$cdnTime = date("YmdHis");
$prod_type=$_GET['type'] ;
$category_list = _v('category_list');
if(!isset($canbid)) {
    $canbid=$_REQUEST['canbid'];
}

if ($prod_type=='flash') {
    $canbid='N';
} else {
    $canbid='Y';
}
$_GET['channelid']=10;

if ( strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false ) {
    $browser = 1;
}else{
    $browser = 2;
}  
?>
<script> $(window).stopTime('losttime');</script>

<script type="text/javascript">
    function openPage(url) {
		if(url=='') {
		   return false;
		}
		if(is_kingkr_obj()){
		   awakeOtherBrowser(url);
		} else {
		   window.location.href=url;
		}
    }
</script>
<div class="product-home">
    <div id="container">
        <div class="sajaProduct-select d-flex">
            <!-- 橫向menu -->
            <div class="horscroll">
                <div class="menu-wrapper d-flex flex-nowrap">

                    <!-- 生成按鈕 -->

                    <div class="sideline"><!-- menu底線 --></div>
                </div>
            </div>
            <!-- 下拉menu (未開發，保留樣式) -->
            <!--
            <div id="sajaProduct-select-btn" class="menu-btn"></div>
            -->
        </div>
        <div class="sajaWrapper-show-box">
            <div class="sajaWrapper-boxscroll">
                <!-- 生成滑動內容 -->
            </div>
        </div>
    </div>
</div>

<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };
    
    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現  
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }
    
    $(function(){
        //有footerBar時，回頂按鈕 位置往上偏移
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
    })
</script>

<!--創建menu-->
<script>
    //儲存所有項目ID及按鈕名稱
    var $tabArr = [];
    <?php foreach ($category_list as $key => $value) { ?>
        var $pcid = '<?php echo $value['pcid']; ?>';
        var $name = '<?php echo $value['name']; ?>';
        $tabArr.push({
            pcid: $pcid,
            name: $name
        });
    <?php } ?>
</script>

<!-- 觸底加載(商品) -->
<script src="<?PHP echo APP_DIR; ?>/static/js/es-productbottom-scroll-v2.0.js"></script>

<!-- 滑動事件 -->
<script>
    //esOptions物件 取自 觸底加載的預設參數(es-productbottom-scroll-v2.0.js);
    
    //各元件
    esOptions.nopicUrl = '<?php echo APP_DIR;?>/static/img/nohistory-img.png';        //無資料時顯示
    esOptions.loadingUrl = '<?php echo APP_DIR;?>/static/img/bottomLoading.gif';        //資料loading顯示
    
    //存入 json 開頭
    esOptions.jsonInfo.dir = '<?php echo BASE_URL.APP_DIR;?>';
    // esOptions.jsonInfo.hpage = '/product/getCategoryProduct/';               //網頁page識別碼，抓取json用
    esOptions.jsonInfo.hpage = '/dream_product/product_list/';               //網頁page識別碼，抓取json用

    //    esOptions.actItemId = 63;                                          //設定預設active位置

    createDom();
    if($tabArr.length == 1){
        $('.sajaProduct-select').removeClass('d-flex').hide();
    }
    
    //追加 tabItem : kind & jsonUrl & nopicText
    $.each(esOptions.tabItem,function(i,item){
        //添加特性
        item['nopicText'] = '此分類目前無殺價商品';
        item['jsonUrl'] = esOptions.jsonInfo.dir + esOptions.jsonInfo.hpage +'?json=N&pcid='+item['pcid'];
    });
    
    //載入json 的 function
    esOptions.show = function($pcid) {
        var $ajaxUrl,$nowPage,$endPage,$total;
        var $wrapper = $pcid;
        
        //對應id 存入頁數計數器 & 總頁數 & json路徑
        $.map(esOptions.tabItem,function(item, index){
            if(item['pcid'] === $pcid){
                $endPage = item['endpage'];
                $total = item['total'];
                if(item['page'] < $total){                          //與總頁數比對，若已達總頁數不再累加，避免重複加載
                    item['page']++;                                 //這次要加載的頁數
                }
                $nowPage = item['page'];
                $ajaxUrl = item['jsonUrl']+'&p='+$nowPage;          //json路徑
            }
        });
        
        if ($endPage == $nowPage){                                  //比對是否是這次要加載的頁數
            $.ajax({  
                url: $ajaxUrl,
                contentType: esOptions.jsonInfo.contentType,
                type: esOptions.jsonInfo.type,  
                dataType: esOptions.jsonInfo.dataType,
                async: esOptions.jsonInfo.async === true,
                cache: esOptions.jsonInfo.cache === true,
                timeout: esOptions.jsonInfo.timeout,
                processData: esOptions.jsonInfo.processData === true,
                contentType: esOptions.jsonInfo.contentType === true
            }).then(
                showResponse,                       //加載成功
                showFailure                         //加載失敗
            ).always(
                endLoading                          //加載後 不管成功失敗
            )

            function showResponse(data){            //生成div的function
                var $pcArr = data["retObj"]["data"];
                var $pageArr = data["retObj"]["page"];
                var $showItem = $('#show-'+$wrapper),
                    $itemWrapper = $showItem.find(esOptions.contentdom.boxWrapper),
                    $canbid;
                var $app_dir = esOptions.jsonInfo.dir;
                if(!($pcArr == null || $pcArr == 'undefined' || $pcArr == '')){
                    if($nowPage <= $total){
                        $.each(data["retObj"]["data"],function(i,item){
                            //判斷是否結標
                            $canbid = item["is_flash"]=='Y'? 'Y':'N';
                            //判斷商品分類ID
                            if(item["pcid"]==$wrapper || $wrapper == '0'){
                                
								var $itemLink;
								if(item["lb_used"] == 'Y'){
									$itemLink = "javascript:location.href='<?php echo BASE_URL.APP_DIR;?>/livebroadcast'";
								}else{
									$itemLink = "javascript:location.href='<?php echo BASE_URL.APP_DIR;?>/dream_product/saja/?<?php echo $status['status']['args'];?>&productid="+item["productid"]+"&type=<?php echo $_GET['type']; ?>&t=<?php echo $cdnTime; ?>'";
								}
                                
                                //圖片路徑
                                var $loadImg = $app_dir+"/static/img/loading.gif",               //loading圖
                                    // $pcImg = $app_dir+"/images/site/product/"+item["thumbnail"], //商品圖
									$pcImg = "<?PHP echo IMG_URL.APP_DIR; ?>/images/site/product/"+item["thumbnail"], //商品圖
                                    $timerId = 'ot-'+item["productid"]+'-'+$pcid;

                                //開始生成
                                $itemWrapper
                                    .append(
                                        $('<div class="sajalist-item align-items-center justify-content-center"/>')
                                        .append(
                                            $('<a href="#" onclick="'+$itemLink+'"/>')
                                            .append(
                                                $('<div class="sajalist-img d-flex flex-column psr"/>')
                                                .append(
                                                    $('<i/>')
                                                    .append(
                                                        $('<img class="lazyload" src="'+$loadImg+'" data-src="'+$pcImg+'"/>')
                                                    )
                                                )
                                                .append(
                                                    $('<p class="sajalist-label">'+item["name"]+'</p>')
                                                )
                                                .append(
                                                    $('<div class="sajalist-overtime mt-auto"/>')
                                                    .append(
                                                        $('<span id="'+$timerId+'" class="overtime"/>')
                                                        .attr('data-offtime',item["unix_offtime"])
                                                        .attr('data-tag','')
                                                        .text('00 天 00:00:00:00')
                                                    )
                                                )
                                            )
                                        )
                                    )
                            }
                        })
                    }else{
                        return false;
                    }
                }else{
                    var $nopicText = ''
                    var $nopicgroup = $.map(esOptions.tabItem, function(item){
                        if(item['pcid']===$wrapper){
                            $nopicText = item['nopicText'];
                        }
                    });
                    $itemWrapper.append(
                        $('<div class="nohistory-box"/>')
                        .append(
                            $('<div class="nohistory-img"/>')
                            .append(
                                $('<img class="img-fluid" src="'+esOptions.nopicUrl+'"/>')
                            )
                        )
                        .append(
                            $('<div class="nohistory-txt">'+$nopicText+'</div>')
                        )
                    )
                }   
            }
            
            //加載動作完成 (不論結果success或error)
            function endLoading() {
                $.map(esOptions.tabItem,function(item, index){
                    if(item['pcid'] === $wrapper){
                        $totalPage = item['total'];                 //找到對應ID的總頁數
                        item['endpage']++;                          //預計下次要加載的頁數,無論如何載完都加1，事後比對用
                    }
                });
                //執行倒數
                timerEven('show-'+$wrapper);
                var $loadingDom = $('#show-'+$wrapper).find('.esloading');
                if($totalPage == 1){
                    $loadingDom.hide();
                }else if($nowPage == $totalPage){
                    $loadingDom.html('<span>沒有其他資料了</span>'); 
                }else if($nowPage < $totalPage){
                    $loadingDom.show();
                }else{
                    $loadingDom.hide();
                }
            }
            //加載錯誤訊息
            function showFailure() {
            }
        }
    }
      
    var $menuParent = $(esOptions.menudom.parent),                        //按鈕的顯示窗
        $wrapper = $(esOptions.menudom.wrapper),                          //包住所有按鈕的外框
        $allmenu = $menuParent.find(esOptions.menudom.menuItem);          //按鈕群
    
    var $showParent = $(esOptions.contentdom.showBox),                    //翻頁物件的顯示窗
        $touchItem = $(esOptions.contentdom.boxScroll),                   //包住翻頁物件的外框
        $item = $(esOptions.contentdom.showItem);                         //翻頁物件群

    //執行倒數
    function timerEven(ele){
        var $timerPageId = ele;
        var $selfTime = $('#'+ele).find('.sajalist-overtime span');
        
        $selfTime.each(function(){
            var $selfOfftime = $(this).attr('data-offtime');
            var $selfId = $(this).attr('id');
            $timer($selfOfftime,$selfId);
        })
    }
    
    //menubar 指定跳頁
    function menuGoNumber(number) {
        var $that = $menuParent.find(esOptions.menudom.menuItem + ':nth-child(' + number + ')'),                  //按鈕active
            $thatItem = $showParent.find(esOptions.contentdom.showItem + ':nth-child(' + number + ')'),           //對應的翻頁物件
            $menuLeft = 0,                                                                                      //按鈕定位
            $showLeft = 0;
        var $loadingPageId = $thatItem.attr('id');
        //執行倒數
        timerEven($loadingPageId);
        
        //loader
        var $loadingPageId = $thatItem.attr('id');
        toload($loadingPageId);
        
        //翻頁物件
        if($thatItem.length > 0){
            //按鈕
            if($that.length > 0){
                $menuLeft = $that.position().left;
                var $maxW = $wrapper.width();               //總寬度
                var $minW = $menuParent.width();            //螢幕寬度
                if ($menuLeft > $minW / 2) {
                    if($menuLeft < $maxW - $minW / 2) {
                        $menuLeft = ($that.position().left - ($menuParent.width() * 0.5) + $that.width());
                        $menuParent.stop().animate({scrollLeft:$menuLeft},300)    //移動位置，讓按鈕置中
                    }else{
                        $menuParent.stop().animate({scrollLeft:$maxW},300)       //最尾段，如果點選的按鈕未超過螢幕一半時，保持在最後
                    }
                }else{
                    $menuParent.stop().animate({scrollLeft:0},300)               //最前段，如果點選的按鈕未超過螢幕一半時，保持在最前
                }
                $allmenu.removeClass(esOptions.class.active);
                $that.addClass(esOptions.class.active);
                menuLine();
            }
            //翻頁物件active切換
            $item.removeClass(esOptions.class.active);
            $thatItem.addClass(esOptions.class.active);
            
            //翻頁物件移動
            $showLeft = $thatItem.position().left;
            $touchItem.stop().animate({left:-$showLeft},300);
            return true;
        }
        return false;
    }

    //上一個(右滑)
    function menuGoPrevious() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        if ($that.index() > 0) {
            $that.prev().click();
            return true;
        }
        return false;
    }
    
    //下一個(左滑)
    function menuGoNext() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var menuLength = $(esOptions.menudom.wrapper).find(esOptions.menudom.menuItem).length;
        if ($that.index() < menuLength - 1) {
            $that.next().click();
            return true;
        }
        return false;
    }
    
    //menu底線變換
    function menuLine() {
        var $that = $(esOptions.menudom.line);
        var $actMenu = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var $actMenuW = $actMenu.innerWidth();
        $that.stop().animate({left:$actMenu.position().left},200).animate({width:$actMenuW},200);
    }
    
    /*懶載入初始化*/
    function toload($id) {
        $("#"+$id+" .sajalist-img .lazyload").lazyload();
    }
    
    //倒數計時
    function ShowTimer(otime,ntime,divId) {
        var $now = new Date();

        //getTime()獲取的值/1000=秒数
        var leftTime = (otime*1000) - $now.getTime();

        var leftSecond=parseInt(leftTime/1000);
        if (leftTime > 0) {

            var day=Math.floor(leftSecond/(60*60*24));

            var hour=Math.floor((leftSecond-day*24*60*60)/3600);
            if (hour < 10) {
                hour = "0" + hour;
            };

            var minute=Math.floor((leftSecond - day * 24 * 60 * 60 - hour * 3600) / 60);
            if (minute < 10) {
                minute = "0" + minute;
            };

            var second = Math.floor(leftSecond - day * 24 * 60 * 60 - hour * 3600 - minute * 60);
            if (second < 10) {
                second = "0" + second;
            };  
            var msecond = Math.floor(leftTime % 100);	
            if (msecond < 10) {
                msecond = "0" + msecond;
            };  

            var htmlElement=document.getElementById(divId);

            if (day == "00") {
                $('.'+divId).html(hour + ":" + minute + ":" + second + ":" + msecond);
                htmlElement.innerHTML = "" + hour + ":" + minute + ":" + second + ":"+ msecond;
            } else {
                htmlElement.innerHTML = "" + day + " 天 "  + hour + ":" + minute + ":" + second + ":"+ msecond;				
            }

        } else if (leftTime < 0) {
            var htmlElement=document.getElementById(divId);
            htmlElement.innerHTML = "已結標";
            window.clearInterval('intervalId'+divId);

            var $location = <?php echo json_encode(BASE_URL.APP_DIR) ?>;
            var $cdnTime = <?php echo json_encode($cdnTime) ?>;
            var $that = $('#'+divId),
                $alert = 'alert("此商品已結標")',
                $href = 'location.href="'+$location+'/product/?'+$cdnTime+'"';
            $that.parents(".sajalist-item").find("a").attr('onclick',$alert+','+$href);
        }
    }
    
    //重複執行倒數計時
    function $timer(offtime,timerId) {
        var interval=59;
        window.setInterval(function () {
            ShowTimer(offtime, '', timerId); 
        }, interval);
    }

    //loader漏網之魚
    var interval = null;
    function $reload() {
        var $showBox = $(esOptions.contentdom.showItem+'.' + esOptions.class.active);               //作用中的頁面
        var $showPos = $showBox.offset().top;                                                       //目前顯示位置(顯示區上邊緣)
        var $showH = $showBox.height();                                                             //顯示區高度
        var $showTo = $showPos + $showH;                                                            //(顯示區下邊緣)
        var $img = $showBox.find('.lazyload');                                                      //抓取所有lazyload img
        
        //讓作用中頁面的圖片都跑一次迴圈，判斷是否加載
        $img.each(function(){
            var $that = $(this);
            var $imgPos = $(this).offset().top;                                                     //圖片位置
            if($imgPos > $showPos && $imgPos < $showTo){
                var $showSrc = $that.attr('src');                                                   //loading圖片
                var $showDsrc = $that.attr('data-src');                                             //產品圖片
                //判斷兩者不相同時 表示未載入,這張圖 重新lazyload
                if($showSrc !== $showDsrc){
                    $(this).lazyload();
                }
            }
        })
    }    
    
    //翻頁物件移動
    function showMove(){
        $showBox = $(esOptions.contentdom.showBox);
        $showItem = $(esOptions.contentdom.showBox).find('.'+esOptions.class.active);
        $showLeft = $showItem.position().left;
        $touchItem.stop().animate({left:-$showLeft},300);
    }

    $(window).on('load',function(){
        //撈取各頁籤總頁數後，生成畫面
        $.when.apply($, esOptions.tabItem.map(function(item) {
            getTotalPages(item);
        })).then(function() {
            //開始生成
            esOptions.tabItem.map(function(item) {
                esOptions.show(item['pcid']);
            })
            //預設頁面
            menuGoNumber(esOptions.actItemIndex);
            //移動線段到作用中的menu
            menuLine();

            //判斷loader漏網之魚
            reloadimg = setInterval($reload,2000);
            
            if($tabArr.length > 1){                 //menu數量大於1，才開放滑動效果
                //向左滑
                $touchItem.bind('swipeleft', function(e){
                    menuGoNext();
                    e.stopPropagation();
                });
                $touchItem.bind('swipe taphold', function(e){
                    e.stopPropagation();
                })
                //向右滑
                $touchItem.bind('swiperight', function(e){
                    menuGoPrevious();
                    e.stopPropagation();
                });
            }

            //event - 切換頁面
            $allmenu.bind('click', function() {
                var $that = $(this);
                menuGoNumber($that.index() + 1);
                reloadimg = setInterval($reload,1000);
            });
        });
    });
    
    //調整顯示區寬度
    $(window).on('resize',function(){
        changeW();
        menuLine();
        getPageHeight();
        //showMove();
        var $showBox = $(esOptions.contentdom.showBox);
        var $showItem = $(esOptions.contentdom.showBox).find(esOptions.contentdom.showItem);
        var $showAct = $(esOptions.contentdom.showBox).find('.'+esOptions.class.active);
        var $number = parseInt($showItem.index($showAct))+1;
        menuGoNumber($number);
    });

    //ajax用 取得總頁數
    function getTotalPages(item){
        var $getTotalPagesId = item['pcid'];
        var $ajaxUrl;
        $.map(esOptions.tabItem,function(item){
            if(item['pcid'] == $getTotalPagesId){
                $ajaxUrl = item['jsonUrl'];      //json路徑      
            }
        });
        return $.ajax({
            url: $ajaxUrl,
            contentType: esOptions.jsonInfo.contentType,
            type: esOptions.jsonInfo.type,  
            dataType: esOptions.jsonInfo.dataType,
            async: esOptions.jsonInfo.async === true,
            cache: esOptions.jsonInfo.cache === true,
            timeout: esOptions.jsonInfo.timeout,
            processData: esOptions.jsonInfo.processData === true,
            contentType: esOptions.jsonInfo.contentType === true
        }).then(
            function(data) {             //取得成功
                //將總頁數 寫入 tabItem
                $.map(esOptions.tabItem,function(item){
                    if(item['pcid'] == $getTotalPagesId){
                        item['total'] =  1;
                    }
                });
            },
            function(data) {            //取得失敗
                console.log(data);
            }
        );
    };

    window.onload = function() { 
        // $('#ot-9684-0').addClass('test');
        $('#ot-8397-0').parents('div.sajalist-item').addClass('test'); 

    };


</script>