<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list');
$status = _v('status'); 
?>	
		<?php //殺價紀錄明細 ?>
		<div class="article">
            <table data-role="table" id="table-custom" data-mode="reflow" class="dable-list ui-responsive">
              <thead>
                <tr>
                  <th data-priority="1">出價金額</th>
				  <th data-priority="2">次數</th>
                  <th data-priority="3">手續費</th>
				  <th data-priority="4">殺價時間</th>
                  <th data-priority="5">方式</th>
                </tr>
              </thead>
              
			  <tbody>
                <?php 
				if(!empty($row_list['table']['record']) ) {
				foreach($row_list['table']['record'] as $rk => $rv) { ?>
				<tr>
                  <td><?php if ($rv['type'] != '殺價幣 流標退點') { echo $rv['price']; } ?></td>
				  <td><?php echo $rv['count']; ?></td>
                  <td><?php echo $rv['saja_fee']; ?></td>
				  <td><?php echo $rv['insertt']; ?></td>
                  <td><?php echo $rv['type']; ?></td>
                </tr>
				<?php } } else { ?>
				<tr>
                  <td> </td>
                  <td> </td>
                  <td> </td>
				  <td> </td>
                  <td> </td>
                </tr>
				<?php } ?>
              </tbody>
            </table>
			
			<?php 
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content'); 
			
			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : ''; 
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
			?>
			<div>
				<div class="ui-grid-a">
                    <div class="ui-block-a <?php echo $arrow_l_able; ?>">
					<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
					</div>
                    <div class="ui-block-b <?php echo $arrow_r_able; ?>">
					<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
					</div>
                </div>
                <?php /*<form>
					<select name="select-native-1" id="change-page">
					<?php echo $page_content['change']; ?>
                    </select>
                </form>*/?>
            </div>
			<?php } ?>
			
        </div><!-- /article -->
		