<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$product = _v('product');
$status = _v('status'); 
?>
    <?php //中標結帳確認 ?>
<div class="bidConfirm">
    <input name="productid" id="productid" type="hidden" value="<?php echo $product['productid'];?>">
    <input name="pgpid" id="pgpid" type="hidden" value="<?php echo $product['pgpid']; ?>">
    
    <ul class="bidCheck-lists">
        <li class="check-list d-flex align-items-center">
            <div class="check-img align-self-start">
                <i class="d-flex align-items-center">
                    <img src="/site/images/site/product/<?php echo $product['thumbnail'];?>">
                </i>
            </div>
            <div class="check-title">
                <p class="check-title-name"><?php echo $product['name'];?></p>
                <p class="check-title-time"><?php echo date('Y-m-d H:i', strtotime($product['insertt']));?></p>
            </div>
        </li>
        <li class="check-list">
            <div class="check-info">
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">商品數量</div>
                    <div class="info-item-rtxt"><span>1</span> 個</div>
                </div>
            <?php
                if ($product['checkout_type'] != 'pf') {
            ?>
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">得標金額</div>
                    <div class="info-item-rtxt">NT <span><?php echo ceil($product['price']);?></span> 元</div>
                </div>
            <?php
                }
                if ($product['checkout_type'] != 'bid') {
            ?>
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">處理費用</div>
                    <div class="info-item-rtxt">NT <span><?php echo ceil($product['real_process_fee']);?></span> 元</div>
                </div>
            <?php
                }
                if ($product['is_discount'] == 'Y') {
            ?>
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">折抵金額</div>
                    <div class="info-item-rtxt">-NT <span><?php echo ceil($product['discount']);?></span> 元</div>
                </div>
            <?php
                }
            ?>
            </div>
        </li>
        <li class="check-list">
            <div class="check-info">
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">總計</div>
                    <div class="info-item-rtxt red">NT <span><?php echo ceil($product['total']);?></span> 元</div>
                </div>
            </div>
        </li>
    </ul>
    <div class="recipient-box">
        <div class="recipient-header">
            <div>收件人資訊</div>
        </div>
        <div>
            <div class="recipient-item d-flex align-items-center">
                <div class="title mr-auto">姓名</div>
                <div id="liname" class="info"></div>
                <input name="name" id="name" value="" type="hidden">
            </div>
            <div class="recipient-item d-flex align-items-center">
                <div class="title mr-auto">電話</div>
                <div id="liphone" class="info"></div>
                <input name="phone" id="phone" value="" type="hidden">
            </div>
            <div class="recipient-item d-flex align-items-center">
                <div class="title mr-auto">郵政編號</div>
                <div id="lizip" class="info"></div>
                <input name="zip" id="zip" value="" type="hidden">
            </div>
            <div class="recipient-item d-flex align-items-center">
                <div class="title mr-auto">地址</div>
                <div id="liaddress" class="info"></div>
                <input name="address" id="address" value="" type="hidden">
            </div>
        </div>
    </div>
		
	<div class="footernav">
        <div class="footernav-button">
            <button type="button" onClick="history.back()">
                <div>修改訂單</div>
            </button>
            <button type="button" onClick="bid_confirm()">
                <div>確認結帳</div>
                <div class="arrow">
				    <img src="<?PHP echo APP_DIR; ?>/static/img/sysMsg-arrow.png" alt="">
				</div>
           </button>
        </div>
    </div>
</div>		