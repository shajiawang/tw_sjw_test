<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list');
$status = _v('status'); 
?>		
		<?php //店點紀錄 ?>
		<div class="article">
            <table data-role="table" id="table-custom" data-mode="reflow" class="dable-list ui-responsive">
              <thead>
                <tr>
                  <th data-priority="1">商品名称</th>
                  <th data-priority="2">時間</th>
                  <th data-priority="3">點數</th>
                  <th data-priority="4">状态</th>
                </tr>
              </thead>
              <tbody>
                <?php if(!empty($row_list['table']['record']) ) {
				foreach($row_list['table']['record'] as $rk => $rv) { 
				?>
				<tr>
                  <td><?php echo $rv['name']; ?></td>
				  <td><?php echo $rv['insertt']; ?></td>
                  <td><?php echo sprintf('%0.2f', $rv['amount']); ?></td>
                  <td><?php if ($rv['behav'] == 'product_close') { ?>结標回馈店點
					<?php } elseif ($rv['behav'] == 'used') { ?>店點序號已使用
					<?php } else { ?>取得店點
					<?php }//endif; ?>
				  </td>
                </tr>
				<?php } } else { ?>
				<tr>
                  <td> </td>
				  <td> </td>
                  <td> </td>
                  <td> </td>
                </tr>
				<?php }//endif; ?>
              </tbody>
            </table>
			
			<?php 
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content'); 
			
			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : ''; 
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
			?>
			<div>
				<div class="ui-grid-a">
                    <div class="ui-block-a <?php echo $arrow_l_able; ?>">
					<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
					</div>
                    <div class="ui-block-b <?php echo $arrow_r_able; ?>">
					<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
					</div>
                </div>
                <?php /*<form>
					<select name="select-native-1" id="change-page">
					<?php echo $page_content['change']; ?>
                    </select>
                </form>*/?>
            </div>
			<?php } ?>
			
        </div><!-- /article -->
<?php /*
//gift.php
<div class="content_data_table">
                <div class="content_data_captionA content_data_title">商品名称</div>
                <div class="content_data_tr content_data_title">
                    <div class="content_data_td1">時間</div>
					<div class="content_data_td2">點數</div>
					<div class="content_data_td3">状态</div>
                </div>
            </div>
            
            <div>
			<?php if (!empty($this->tplVar['table']['record'])) : ?>
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
			<div class="content_data_table">
                <div class="content_data_captionB"><a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>" target="_blank"><span><?php echo $rv['name']; ?></span></a></div>
                <div class="content_data_tr">
                    <div class="content_data_td1"><?php echo $rv['insertt']; ?></div>
					<div class="content_data_td2"><?php echo sprintf('%0.2f', $rv['amount']); ?></div>
					<div class="content_data_td3">
						<span>
						<?php if ($rv['behav'] == 'product_close') : ?>
						结標回馈店點
						<?php elseif ($rv['behav'] == 'used') : ?>
						店點序號已使用
						<?php else : ?>
						取得店點
						<?php endif; ?>
						</span>
					</div>
                </div>
            </div>
			<?php endforeach; ?>
			<?php endif; ?>
			</div>
*/?>		