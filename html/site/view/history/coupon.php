<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list');
$status = _v('status'); 
?>		
		<?php //殺價禮券 ?>
		<div class="article">
			<table data-role="table" id="table-custom" data-mode="reflow" class="dable-list ui-responsive">
              <thead>
                <tr>
                  <th data-priority="1">礼券名称</th>
                  <th data-priority="2">數量</th>
                  <th data-priority="3">编號</th>
                  <th data-priority="4">获得時間</th>
                  <th data-priority="5">使用期限</th>
                </tr>
              </thead>
              
			  <tbody>
                <?php if(!empty($row_list['table']['record']) ) {
				foreach($row_list['table']['record'] as $rk => $rv) { ?>
				<tr>
                  <td><?php echo $rv['name']; ?></td>
                  <td><?php echo $rv['count']; ?></td>
                  <td><?php echo $rv['productid']; ?></td>
                  <td><?php echo $rv['insertt']; ?></td>
                  <td>
					<?php if ($rv['offtime']) { ?><?php echo date("Y-m-d H:i:s", $rv['offtime']); ?>
					<?php } else { ?>****-**-** --:--:--
					<?php }//endif; ?>
				</td>
                </tr>
				<?php } } ?>
              </tbody>
            </table>
			
			<?php 
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content'); 
			
			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : ''; 
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
			?>
			<div>
				<div class="ui-grid-a">
                    <div class="ui-block-a <?php echo $arrow_l_able; ?>">
					<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
					</div>
                    <div class="ui-block-b <?php echo $arrow_r_able; ?>">
					<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
					</div>
                </div>
                <?php /*<form>
					<select name="select-native-1" id="change-page">
					<?php echo $page_content['change']; ?>
                    </select>
                </form>*/?>
            </div>
			<?php } ?>
			
		</div><!-- /article -->
<?php /*
//sgift.php
<div class="content_data_table">
                <div class="content_data_captionA content_data_title">礼券名称 × 數量</div>
                <div class="content_data_tr content_data_title">
                    <div class="content_data_td1">编號</div>
					<div class="content_data_td2">获得時間</div>
					<div class="content_data_td3">使用期限</div>
                </div>
            </div>
            
            <div>
			<?php if (!empty($this->tplVar['table']['record'])) : ?>
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<div class="content_data_table content_gray">
					<div class="content_data_captionA"><?php echo $rv['name']; ?> × <?php echo $rv['count']; ?> 张</div>
					<div class="content_data_tr">
						<div class="content_data_td1"><?php echo $rv['productid']; ?></div>
						<div class="content_data_td2"><?php echo $rv['insertt']; ?></div>
						<div class="content_data_td3">
							<?php if ($rv['offtime']) : ?>
							<?php echo date("Y-m-d H:i:s", $rv['offtime']); ?>
							<?php else : ?>
							****-**-** --:--:--
							<?php endif; ?>
						</div>
		
					</div>
				</div>
			<?php endforeach; ?>	
			<?php endif; ?>
			</div>
*/?>		
		