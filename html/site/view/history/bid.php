<?php
date_default_timezone_set('Asia/Taipei');
$cdnTime = date("YmdHis");
$row_list = _v('row_list');
$status = _v('status'); 
$errMsg=$_REQUEST['errMsg'];
$msgType=$_REQUEST['msgType'];
$yesURL=$_REQUEST['yesURL'];
?>	
<?php 
      if(!empty($errMsg)) {
		if($msgType=='confirm') {
?>
           <script>
		   if(confirm('<?php echo $errMsg; ?>')){
				location.href='<?php echo $yesURL; ?>';
		   }
		   </script>
<?php		
		} else {
?>
	      <script>alert('<?php echo $errMsg; ?>');</script>
<?php 
		} 
	 }
?>
		<?php //中標紀錄 ?>
		<div class="article">
            <table data-role="table" id="table-custom" data-mode="reflow" class="dable-list ui-responsive">
              <thead>
                <tr>
                  <th data-priority="1">商品名称</th>
                  <th data-priority="2">编號</th>
                  <th data-priority="3">结標時間</th>
                  <th data-priority="4">结帳状态</th>
                  <th data-priority="5">结帳</th>
                </tr>
              </thead>
              <tbody>
                <?php 
				if(!empty($row_list['table']['record']) ) {
				foreach($row_list['table']['record'] as $rk => $rv) { 
				?>
				<tr>
                  <td><?php echo $rv['name']; ?></td>
                  <td><?php echo $rv['productid']; ?></td>
                  <td><?php echo date("Y-m-d H:i:s", $rv['insertt']); ?></td>
                  <td>
				  <?php if($rv['complete'] == 'Y'){ ?>
						<?php if((int)$product['bid_oscode'] > 0){ ?>已结帳,一般发送 限定S碼
						<?php }elseif((int)$product['bid_scode'] > 0){ ?>已结帳,一般发送S碼
						<?php }elseif((float)$product['bid_spoint'] > 0){ ?>已结帳,并发送殺價幣
						<?php }else{ ?>已结帳<?php } ?>
				  <?php }else{ ?>未结帳<?php } ?>
				  </td>
                  <td>
				  <?php if ($rv['complete'] !== 'Y') { ?>
				  <a href="<?php echo APP_DIR; ?>/history/bid_checkout/?productid=<?php echo $rv['productid']; ?>&<?php echo $cdnTime; ?>">前往结帳</a>
				  <?php } ?>
				  </td>
                </tr>
				<?php } } else { ?>
				<tr>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                  <td> </td>
                </tr>
				<?php }//endif; ?>
              </tbody>
            </table>
			
			<?php 
			if(!empty($row_list['table']['record']) ) {
			$page_content = _v('page_content'); 
			
			$arrow_l_able = ($page_content['thispage'] <= 1) ? 'ui-state-disabled' : ''; 
			$arrow_r_able = ($page_content['thispage'] >= $page_content['lastpage']) ? 'ui-state-disabled' : '';
			?>
			<div>
				<div class="ui-grid-a">
                    <div class="ui-block-a <?php echo $arrow_l_able; ?>">
					<a href="<?php echo $page_content['prevhref']; ?>" data-role="button" data-icon="arrow-l">上一页</a>
					</div>
                    <div class="ui-block-b <?php echo $arrow_r_able; ?>">
					<a href="<?php echo $page_content['nexthref']; ?>" data-role="button" data-icon="arrow-r" data-iconpos="right">下一页</a>
					</div>
                </div>
                <?php /*<form>
					<select name="select-native-1" id="change-page">
					<?php echo $page_content['change']; ?>
                    </select>
                </form>*/?>
            </div>
			<?php } ?>
			
        </div><!-- /article -->
<?php /*
//pay_get_product.php
<div class="content_data_table">
                <div class="content_data_captionA content_data_title">商品名称　<span style="color:#F00; font-weight:bold;">※ 未结帳點选商品名称前往结帳</span></div>
                <div class="content_data_tr content_data_title">
                    <div class="content_data_td1">编號</div>
					<div class="content_data_td2">结標時間</div>
					<div class="content_data_td3">结帳手續</div>
					<div class="content_data_td4">结帳</div>
                </div>
            </div>
            
            <div>
			<?php if (!empty($this->tplVar['table']['record'])) : ?>
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<div class="content_data_table">
					<div class="content_data_captionA"><a href="<?php echo $this->config->default_main.'/product/saja?productid='.$rv['productid']; ?>"><span><?php echo $rv['name']; ?></span></a></div>
					<div class="content_data_tr">
						<div class="content_data_td1"><?php echo $rv['productid']; ?></div>
						<div class="content_data_td2"><?php echo date("Y-m-d H:i:s", $rv['insertt']); ?></div>
						<div class="content_data_td3">
							<?php if ($rv['complete'] == 'Y') : ?>
							<span>已结帳</span>
							<?php else : ?>
							<a href="<?php echo $this->config->default_main.'/pay_get_product?productid='.$rv['productid']; ?>">
								<span>未结帳</span>
							</a>
							<?php endif; ?>
						</div>
						<div class="content_data_td4"><?php echo $rv['status']; ?></div>
					</div>
				</div>
			<?php endforeach; ?>
			<?php endif; ?>
			</div>
*/?>		
