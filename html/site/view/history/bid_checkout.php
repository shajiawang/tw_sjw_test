<?php
date_default_timezone_set('Asia/Shanghai');
$cdnTime = date("YmdHis");
$product = _v('product');
$status = _v('status');
$auth = _v('auth');
$user_profile = _v('user_profile');

$getuser['name'] = $user_profile['addressee']; 
$getuser['zip'] = $user_profile['area'];
$getuser['address'] = $user_profile['address'];
$getuser['phone'] = $user_profile['rphone'];

// $member = _v('member');
?>	
    <?php //中標結帳 ?>
    <div class="navbar sajaNavBar d-flex align-items-center">
		<div class="sajaNavBarIcon back">
            <a href="javascript:history.back();" class="ui-link">
                <img src="http://test.shajiawang.com/site/static/img/back.png">
            </a>
        </div>
		<div class="navbar-brand">
            <h3>得標結帳</h3>
        </div>
        <div>
        </div>
    </div>
<div class="bidCheckout">
    <input name="productid" id="productid" type="hidden" value="<?php echo $product['productid'];?>">
	<input name="pgpid" id="pgpid" type="hidden" value="<?php echo $product['pgpid']; ?>">
   
    <ul class="bidCheck-lists">
        <li class="check-list d-flex align-items-center">
            <div class="check-img align-self-start">
                <i class="d-flex align-items-center">
                    <img src="/site/images/site/product/<?php echo $product['thumbnail'];?>">
                </i>
            </div>
            <div class="check-title">
                <p class="check-title-name"><?php echo $product['name'];?></p>
                <p class="check-title-time">結標日期　<?php echo date('Y-m-d H:i',$product['offtime']); ?></p>
            </div>
        </li>
    </ul>
    <ul class="bidCheck-lists">

        <li class="check-list">
            <div class="check-info">
                <!-- <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">商品數量</div>
                    <div class="info-item-rtxt"><span>1</span> 個</div>
                </div> -->
            <?php
                if ($product['checkout_type'] != 'pf') {
            ?>
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">得標金額</div>
                    <div class="info-item-rtxt">NT <span><?php echo ceil($product['price']);?></span> 元</div>
                </div>
            <?php
                }
                if ($product['checkout_type'] != 'bid') {
            ?>
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto">得標處理費</div>
                    <div class="info-item-rtxt">NT <span><?php echo ceil($product['real_process_fee']);?></span> 元</div>
                </div>
            <?php
                }
                if ($product['is_discount'] == 'Y') {
            ?>
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto red">*折抵</div>
                    <div class="info-item-rtxt red">-NT <span><?php echo ceil($product['discount']);?></span> 元</div>
                </div>
            <?php
                }
            ?>
            </div>
        </li>

    </ul>
    <ul class="bidCheck-lists total">
        <li class="check-list">
            <div class="check-info">
                <div class="info-item d-flex align-items-center">
                    <div class="info-item-title mr-auto red bold">總付款金額</div>
                    <div class="info-item-rtxt red bold">NT <span><?php echo ceil($product['total']);?></span> 元</div>
                </div>
            </div>
        </li>
    </ul>
    <?php
        if ($product['is_discount'] == 'Y' && $product['discount']>0) {
    ?>
    <ul class="bidCheck-lists detail">
        <li>
        <a href="javascript:;" class="d_detail">*折抵詳情</a>
        </li>
    </ul>
    <?php
        }
    ?>
	<ul class="input_group_box">
        <a href="javascript:;" class="m_detail">
            <li class="input_group d-flex align-items-center info_detail">
                <div class="label" for="name">收件人資訊</div>
                <img src="/site/static/img/info_right_icon.png" alt="" >
            </li>
        </a>
        <?php
            if ($getuser['name'] != "" || $getuser['phone'] != "" || $getuser['zip'] != "" || $getuser['address'] != "") {
        ?>
            <li class="input_group_msg">
                <p id="msg_name"><?php echo $getuser['name'];?></p>
                <p id="msg_phone"><?php echo $getuser['phone'];?></p>
                <p id="msg_zip"><?php echo $getuser['zip'];?></p>
                <p id="msg_address"><?php echo $getuser['address'];?></p>
            </li>
        <?php
            }
        ?>
        <!-- <li class="input_group d-flex align-items-center">
            <div class="label" for="name"><span class="required">*</span>收件人姓名</div>
            <input id="name" name="name" value="<?php echo $getuser['name'];?>" type="text" placeholder="郵寄需要，請輸入真實姓名">
        </li>
        <li class="input_group d-flex align-items-center">
            <div class="label" for="phone"><span class="required">*</span>手機號碼</div>
            <input id="phone" name="phone" value="<?php echo $getuser['phone'];?>" type="number" pattern="[0-9.]*" placeholder="請輸入正確的收件人手機號碼">
        </li>
        <li class="input_group d-flex align-items-center">
            <div class="label" for="zip"><span class="required">*</span>郵遞區號</div>
            <input id="zip" name="zip" value="<?php echo $getuser['zip'];?>" type="number" pattern="[0-9.]*" placeholder="請輸入正確完整的郵遞區號">
        </li>
        <li class="input_group d-flex align-items-center">
            <div class="label" for="address"><span class="required">*</span>詳細地址</div>
            <input id="address" name="address" value="<?php echo $getuser['address'];?>" type="text" placeholder="請輸入正確的詳細地址">
        </li> -->
    </ul>
    <!-- <div class="precautions text-right">若因地址填寫錯誤造成商品寄送遺失，須自行負責後續責任</div> -->
    
    <div class="footernav">
        <div class="footernav-button">
            <?php if (empty($auth['verified']) || $auth['verified']!=='Y'){ ?>
                <button type="button" onClick="checkphone()">確定結帳</button>
            <?php } else { ?>
                <!-- <button type="button" onClick="bid_checkout()">確認結帳</button> -->
                <button type="button" onClick="bid_confirm()">
                <div>確定結帳</div>
                <div class="arrow">
				    <img src="<?PHP echo APP_DIR; ?>/static/img/sysMsg-arrow.png" alt="">
				</div>
           </button>
            <?php } ?>
        </div>
    </div>	
    
    <div class="discount_detail">
        <div class="navbar sajaNavBar d-flex align-items-center">
            <div class="sajaNavBarIcon back icon_back">
                <a href="javascript:;" >
                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
                </a>
            </div>
            <div class="navbar-brand">
                <h3>折抵詳情</h3>
            </div>
        </div>
        <div class="hasHeader">
            <img src="/site/static/img/discount_detail.png" alt="">
        </div>
        <footer>
            <div class="productSaja-footernav">
                <div class="productSaja-button">
        <!--    
                    <?php if (($spoint['amount'] < sprintf("%1\$u",$topay['payall'])) && ($topay['count'] > $oscode_num) && ($topay['count'] > $scode_num) ){ ?>
                        <button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a" onClick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/deposit/?<?php echo $cdnTime; ?>'">餘額不足 點我購買</button>            
                    <?php }else{ ?>
                        <button type="submit" id="topay" class="ui-btn ui-corner-all ui-btn-a"
                            <?php if ($topay['type'] == 'single'){ ?>
                                onClick="bid_1(<?php echo $product['productid']; ?>);"
                            <?php }else{ ?>
                                onClick="bid_n(<?php echo $product['productid']; ?>);"
                            <?php } ?>>確定結算
                        </button>
                    <?php } ?>	
        -->
                </div>
            </div>
        </footer>                          
    </div>

    <div class="member_detail">
        <div class="navbar sajaNavBar d-flex align-items-center">
            <div class="sajaNavBarIcon back icon_back">
                <a href="javascript:;" >
                    <img src="<?php echo BASE_URL.APP_DIR;?>/static/img/back.png">
                </a>
            </div>
            <div class="navbar-brand">
                <h3>修改收件人資訊</h3>
            </div>
        </div>
        <div class="bidConfirm hasHeader">
            <input name="productid" id="productid" type="hidden" value="<?php echo $product['productid'];?>">
            <input name="pgpid" id="pgpid" type="hidden" value="<?php echo $product['pgpid']; ?>">
            
            <ul class="input_group_box">
                <li class="input_group d-flex align-items-center">
                    <div class="required">*</div><div class="label" for="name">收件人姓名</div>
                    <input id="name" name="name" value="<?php echo $getuser['name'];?>" type="text" placeholder="郵寄需要，請輸入真實姓名" readonly="readonly">
                </li>
                <li class="input_group d-flex align-items-center">
                    <div class="required">*</div><div class="label" for="phone">手機號碼</div>
                    <input id="phone" name="phone" value="<?php echo $getuser['phone'];?>" type="number" pattern="[0-9.]*" placeholder="請輸入正確的收件人手機號碼" readonly="readonly">
                </li>
                <li class="input_group d-flex align-items-center">
                    <div class="required">*</div><div class="label" for="zip">郵遞區號</div>
                    <input id="zip" name="zip" value="<?php echo $getuser['zip'];?>" type="number" pattern="[0-9.]*" placeholder="請輸入正確完整的郵遞區號" readonly="readonly">
                </li>
                <li class="input_group d-flex align-items-center">
                    <div class="required">*</div><div class="label" for="address">詳細地址</div>
                    <input id="address" name="address" value="<?php echo $getuser['address'];?>" type="text" placeholder="請輸入正確的詳細地址" readonly="readonly">
                </li>
            </ul>
            <div class="precautions text-center">若因地址填寫錯誤造成商品寄送遺失，須自行負責後續責任</div>

            <div class="footernav">
                <div class="footernav-button">
                    <button type="button" onClick="bid_checkout()">
                        <div>確認</div>
                    </button>
                </div>
            </div>
        </div>                        
    </div>
</div>
		
	

		
		<!-- /article -->
		
		
        <?php // if (!is_weixin()) { ?>
		
        <?php if (false) { ?>
        <style>
        #BG {
            position: fixed;
            top: 0;
            left: 0;
            z-index: 2;
            width: 100%;
            height: 100%;
            background: #000;
            filter: alpha(opacity=50);
            opacity: 0.5;
        }
        </style>
        <!--<div id="BG">
        	<img src="<?php echo BASE_URL . APP_DIR;?>/images/site/qr/weixin_share.png" width="100%" height="100%" original="<?php echo BASE_URL . APP_DIR;?>/images/site/qr/weixin_share.png">
        </div>-->
        <!-- script src="/site/javascript/WeixinApi.js"></script -->
        <script type="text/javascript">
		WeixinApi.ready(function (Api) {
                    Api.showOptionMenu();
                
                <?php if(!empty($product['thumbnail']) ){ ?>
				var imgUrl = "<?php echo BASE_URL . APP_DIR;?>/images/site/product/<?php echo $product['thumbnail']; ?>";
				<?php } elseif(!empty($product['thumbnail_url']) ){ ?>
				var imgUrl = "<img src="<?php echo $product['thumbnail_url']; ?>";
				<?php } else { ?>
				var imgUrl = "<?php echo BASE_URL . APP_DIR;?>/images/site/product/f3faf3bb9cc297f31521432ab72b2ce2.png";
				<?php } ?>
                
                var wxData = {
                    "appId": "",
                    "imgUrl": imgUrl,
                    "link": "<?php echo BASE_URL . APP_DIR;?>/bid/detail/?productid=<?php echo $product['productid'];?>",
                    "title": "下一个“殺價王”会是你吗？",
                    "desc": "我刚刚在殺價王以<?php echo sprintf("%1\$u", $product['price']);?>元購得<?php echo $product['name'];?>，亲你还不加入吗？"
                };
                var wxCallbacks = {
                    ready: function () {
                        wxData['desc'] = wxData['desc'] + new Date().getTime(); 
                        wxData['title'] = wxData['title'] + new Date().getTime(); 
                        wxData['link'] = wxData['link'] + '&uid=' + new Date().getTime();
                    },
                    cancel: function (resp) {
                        alert("好东西要和朋友分享的嘛！分享后会有免費下標次數赠送！");
                    },
                    fail: function (resp) {
                        alert("分享失败，可能是网络问题，一会再试试？");
                    },
                    confirm: function (resp) {
                    	//alert(1111);
                        //$("#BG").hide();
                    },
                };
                Api.shareToFriend(wxData, wxCallbacks);
                Api.shareToTimeline(wxData, wxCallbacks);
                Api.shareToWeibo(wxData, wxCallbacks);
            });
            
        </script>
        <?php } ?>
        
       <script>
        //調整標題寬度一致
        var options = {
            dom: {
                ul: 'ul.input_group_box',
                li: 'li.input_group',
                label: '.label',
                input: 'input',
                select: 'select'
            }
        }

        // var user_name = '<?php echo $getuser['name'];?>',
        //     user_zip = '<?php echo $getuser['zip'];?>',
        //     user_address = '<?php echo $getuser['address'];?>',
        //     user_phone = '<?php echo $getuser['phone'];?>'

        $(function(){
            //目前顯示的ul區塊
            var $isShow = $(options.dom.ul).has(':visible');
            
            //取得應該顯示的寬度
            function changeWidth(dom) {
                var $selfLi = dom.find($(options.dom.li));
                var $selfLabel = $selfLi.find($(options.dom.label));
                
                //初始化
                $selfLabel.attr('style','');
                
                var $liW = '';
                var $arr = [];

                $liW = $selfLi.width();                         //取得li總寬度   

                $selfLabel.each(function(){                     //取得label寬度並存入陣列
                    $arr.push($(this).text().length * (parseInt($(this).css('font-size')) + 3));    //+3防止文字本身寬度不一造成斷行
                });
                var $maxW = Math.max.apply(this, $arr);         //取得label群中最大值
                var $remain = $liW - $maxW;                     //(剩餘寬度)input或select的寬度等於li總寬度-label寬度最大值

                //改變寬度
                $selfLabel.each(function(){
                    $(this).css('width',$maxW);
                })
                
                $selfLi.each(function(){                        //每個li都重新判斷是否有兩個欄位
                    var $selfInput = $(this).find($(options.dom.input));
                    var $selfSelect = $(this).find($(options.dom.select)).parents(".ui-select");
                    $selfInput.attr('style','');
                    $selfSelect.attr('style','');
                    $selfInput.each(function(){
                        if($selfInput.length > 1){
                            $(this).width(($remain / $selfInput.length));
                        }else{
                            $(this).width($remain);
                        }
                    })
                    $selfSelect.each(function(){
                        if($selfSelect.length > 1){                 
                            $(this).width(($remain / $selfSelect.length));          //出現1個以上的輸入框時，將剩餘寬度均分
                        }else{
                            $(this).width($remain);
                        }
                    })
                })
            }
            $isShow.each(function(){
                changeWidth($(this));
            })
            
            //尺寸改變重新撈取寬度
            $(window).resize(function(){
                $isShow.each(function(){
                    changeWidth($(this));
                })
            })

        })

        // 開啟確認
        $(".d_detail").click(function () { 
           $(".discount_detail").addClass('on');
           var sumprice=$(".sumprice").find('span').text();
           $(".sum_payment").text(sumprice+"枚");
        });
        // 關閉付款確認
        $(".discount_detail").find('.icon_back').click(function () { 
           $(".discount_detail").removeClass('on');
        });

        // 開啟修改收件人資訊
        $(".m_detail").click(function () { 
           $(".member_detail").addClass('on');
        });
        // 關閉收件人資訊
        $(".member_detail").find('.icon_back').click(function () { 
           $(".member_detail").removeClass('on');
        });

    </script>