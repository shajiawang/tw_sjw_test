<?php //殺幣紀錄 
$row_list = _v('row_list'); 
$row_in_list = _v('row_in_list'); 
$row_out_list = _v('row_out_list'); 
$frozenspoint = _v('frozenspoint');
?>
<!-- 
    * 獲得類 ： 正常色 (不用其他設定)
    * 使用類 ： 在li.history-list 增加class "red"
    * "+"跟"-"符號，使用CSS增加，判斷條件為父層li是否有 "red",
      所以輸出price時，使用絕對值
    
    * 若無資料，整個"ul"都隱藏 切換顯示class="nohistory"
    * #tab1 ： 全部顯示
      #tab2 ： 獲得
      #tab3 ： 使用
 -->
<div id="tobid" class="swipe-tab">
    <div class="sajaProduct-select d-flex">
        <!-- 橫向menu -->
        <div class="horscroll">
            <div class="menu-wrapper d-flex flex-nowrap">
                <a class="menu-a on-active" href="#tab1">全部</a>
                <a class="menu-a" href="#tab2">獲得</a>
                <a class="menu-a" href="#tab3">使用</a>
                <div class="sideline"><!-- menu底線 --></div>
            </div>
        </div>
        <!-- 下拉menu -->
        <!--
        <div id="sajaProduct-select-btn" class="menu-btn"></div>
        -->
    </div>
<!--
    <div class="history-freeze">
        <div class="title">凍結殺價幣共</div>
        <div class="point"><span class="currency">NT</span><span class="frozen"></span></div>
    </div>
-->
    <div class="swipe-navbar-content history-bid">
        <!-- ajax生成 -->
    </div>
    
</div>

<!-- 回頂按鈕 -->
<div id="gotop">
    <img src="<?PHP echo APP_DIR; ?>/static/img/gotop.png" alt="回頂">
</div>

<!-- 回頂JS -->
<script>
    var options = {
        dom: {
            gotopid: '#gotop',      //回頂按鈕名稱
            debug: '.debug'
        },
        scroll: '200',              //按鈕在捲動多少時出現
        scrollspeed: '500'          //捲動毫秒
    };
    
    function changeShow() {
        if ($(window).scrollTop()>options.scroll){                              //當捲軸向下移動多少時，按鈕顯示
            $(options.dom.gotopid).show();                                      //回頂按鈕出現  
        }else{
            $(options.dom.gotopid).hide();                                      //回頂按鈕隱藏
        }
    }
    
    $(function(){
        //有footerBar時，回頂按鈕 位置往上移動
        if($(".sajaFooterNavBar").length > 0){
            var $old = parseInt($(options.dom.gotopid).css('bottom'));
            var $add = $(".sajaFooterNavBar").outerHeight();
            $(options.dom.gotopid).css('bottom',$old+$add);
        }
        var $btnH;                                                                      //按鈕高度
        var $btnSpacing;                                                                //按鈕間距
        $(options.dom.gotopid).hide();                                                  //隱藏回頂按鈕
        $(window).on('load',function(){
            //偵測平台高度
            var $windowH = $(window).height();
            $(window).on('scroll resize',function(){                                           //偵測視窗捲軸動作
                changeShow();
            });
            $(options.dom.gotopid).click(function(){
                $('body,html').stop().animate({scrollTop:0},options.scrollspeed);              //單擊按鈕，捲軸捲動到頂部
            })
        });
    })
</script>

<!-- 觸底加載 -->
<script src="<?PHP echo APP_DIR; ?>/static/js/es-bottom-scroll.js"></script>

<!-- 客製化加載 -->
<script>
    //各元件
    esOptions.nopicUrl = '<?php echo APP_DIR;?>/static/img/nohistory-img.png';        //無資料時顯示
    esOptions.loadingUrl = '<?php echo APP_DIR;?>/static/img/bottomLoading.gif';        //資料loading顯示
    
    //存入 json 開頭
    esOptions.jsonInfo.dir = '<?php echo BASE_URL.APP_DIR;?>';
    esOptions.jsonInfo.hpage = '/history/';                     //網頁page識別碼，抓取json用
    esOptions.isTab = 'true';                                   //是否為多頁tab

    esOptions.invoiceUrl = '<?php echo BASE_URL.APP_DIR;?>/invoice/InvoiceDataOut/?invoiceno='
    createDom();
    //追加 tabItem : kind & jsonUrl & nopicText
    $.each(esOptions.tabItem,function(i,item){
        //添加特性 kind & jsonUrl
        switch(item['id']){
                case 'tab1' :
                    item['kind'] = 'all';
                    item['nopicText'] = '目前沒有任何紀錄';
                    break;
                case 'tab2' :
                    item['kind'] = 'get';
                    item['nopicText'] = '目前沒有獲得紀錄';
                    break;
                case 'tab3' :
                    item['kind'] = 'use';
                    item['nopicText'] = '目前沒有使用紀錄';
                    break;
            }
        item['jsonUrl'] = esOptions.jsonInfo.dir + esOptions.jsonInfo.hpage +'?json=Y&kind='+item['kind'];
    });
    
    //載入json(第一次與其他頁共用同一個)
    esOptions.show = function($id) {
        var $ajaxUrl,$nowPage,$endPage,$total;
        var $wrapper = $id;
        var $note,$textcolor;
        $.map(esOptions.tabItem,function(item, index){
            if(item['id'] === $id){
                $endPage = item['endpage'];
                $total = item['total'];
                if(item['page'] < $total){                          //與總頁數比對，若已達總頁數不再累加，避免重複加載
                    item['page']++;                                 //這次要加載的頁數
                }
                $nowPage = item['page'];
                $ajaxUrl = item['jsonUrl']+'&p='+$nowPage;      //json路徑
            }
        });
        if ($endPage == $nowPage){                                  //比對是否是這次要加載的頁數
            $.ajax({  
                url: $ajaxUrl,
                contentType: esOptions.jsonInfo.contentType,
                type: esOptions.jsonInfo.type,  
                dataType: esOptions.jsonInfo.dataType,
                async: esOptions.jsonInfo.async === true,
                cache: esOptions.jsonInfo.cache === true,
                timeout: esOptions.jsonInfo.timeout,
                processData: esOptions.jsonInfo.processData,
                contentType: esOptions.jsonInfo.contentType
            }).then(
                showResponse,                       //加載成功
                showFailure                         //加載失敗
            ).always(
                endLoading                          //加載後 不管成功失敗
            )

            function showResponse(data){       //生成div的function
                var $itemWrapper = $('#'+$wrapper);
                var $arr = data["retObj"]["data"];
                if(!($arr == null || $arr == 'undefined' || $arr == '')){
//                    var $frozenspoint = data["retObj"]["frozenspoint"]["frozen"];         //凍結殺價幣
//                    $(".history-freeze").find(".frozen").text($frozenspoint);             //凍結殺價幣(顯示於畫面)
                    if($nowPage <= $total){
                        $.each(data["retObj"]["data"],function(i,item){
                            switch(item['behav']){
                                case 'bid_by_saja' :
                                    $note = '得標贈送';
                                    $textcolor = '';
                                    break;
                                case 'prod_sell' :
                                    $note = '得標商品兌換';
                                    $textcolor = 'red';
                                    break;
                                case 'user_deposit' :
                                    $note = '購買成功';
                                    $textcolor = '';
                                    break;
                                case 'process_fee' :
                                    $note = '得標處理費';
                                    $textcolor = 'red';
                                    break;
                                case 'gift' :
                                    $note = '贈送';
                                    $textcolor = '';
                                    break;
                                case 'bid_refund' :
                                    $note = '流標退款';
                                    $textcolor = '';
                                    break;
                                case 'sajabonus' :
                                    $note = '鯊魚點兌換';
                                    $textcolor = '';
                                    break;
                                case 'user_saja' :
                                    $note = '下標手續費';
                                    $textcolor = 'red';
                                    break;
                                case 'prod_buy' :
                                    $note = '商品購買';
                                    $textcolor = 'red';
                                    break;									
                                case 'feedback' :
                                    if(item['title'] !== null || item['title'] == ''){
                                        $note = item['title']+' 商家回饋';
                                    }else{
                                        $note = '商家回饋';
                                    }
                                    $textcolor = '';
                                    break;
                                default :
                                    $note = '其他';
                                    if(item['amount'] < 0){
                                        $textcolor = 'red';
                                    }else{
                                        $textcolor = '';
                                    }
                            }
                            
                            if(item['invoiceno'] == '' || item['invoiceno'] == null) {
                                var $invoiceUrlSelf = 'javascript:void(0)';                               //發票連結 (無發票)
                            }else{
                                var $invoiceUrlSelf = esOptions.invoiceUrl + item['invoiceno'];           //發票連結
                            }
                            if(item['is_winprize'] == 'N' || item['is_winprize'] == null) {
                                var $is_winprize = '';                               //發票連結 (無發票)
                            }else{
                                var $is_winprize = '(已中獎)';           //發票連結
                            }
							
                            $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                            .append(
                                $('<li/>')
                                .addClass('history-list d-flex align-items-center' +($textcolor !== ''?' '+$textcolor:''))
                                .append(
                                    $('<div class="history-list-info d-flex flex-column mr-auto"/>')
                                    .append(
                                        item['behav'] == 'user_deposit' ?
                                            $('<div class="status d-flex"/>')
                                            .append(
                                                $('<div>'+$note+'</div>')
                                            )
                                            .append(
												$('<a onclick="javascript:location.href=\''+$invoiceUrlSelf+'\'" target="_blank">發票 '+$is_winprize+'</a>' )
													.addClass('invoice-btn'+(item['invoiceno'] == '' || item['invoiceno'] == null ? ' disabled':''))
                                            )
                                        :
                                            $('<div class="status">'+$note+'</div>')
                                    )
                                    .append(
                                        $('<div class="time">'+item['insertt']+'</div>')
                                    )
                                )
                                .append(
                                    $('<div class="price"/>')
                                    .append(
                                        $('<span class="currency">NT</span>')
                                    )
                                    .append(
                                        $('<span>'+Math.abs(item['amount'])+'</span>')
                                    )
                                )
                            )
                        })
                    }else{
                        return false;
                    }
                }else{
                    //不同頁簽顯示不同文字 (先前存進物件的資料)
                    var $nopicText = ''
                    var $nopicgroup = $.map(esOptions.tabItem, function(item, index){
                        if(item['id']===$wrapper){
                            $nopicText = item['nopicText'];
                        }
                    });
                    $itemWrapper.find('.' + esOptions.contentdom.ulClass)
                    .after(
                        $('<div class="nohistory-box"/>')
                        .append(
                            $('<div class="nohistory-img"/>')
                            .append(
                                $('<img class="img-fluid" src="'+esOptions.nopicUrl+'"/>')
                            )
                        )
                        .append(
                            $('<div class="nohistory-txt">'+$nopicText+'</div>')
                        )
                    )
                }   
            } 
            //加載動作完成 (不論結果success或error)
            function endLoading() {
                $.map(esOptions.tabItem,function(item, index){
                    if(item['id'] === $id){
                        item['endpage']++;                          //預計下次要加載的頁數
                        $totalPage = item['total'];                 //找到對應ID的總頁數
                    }
                });
                var $loadingDom = $('#'+$id).find('.esloading');
                if($totalPage == 1){
                    $loadingDom.hide();
                }else if($nowPage == $totalPage){
                    $loadingDom.html('<span>沒有其他資料了</span>'); 
                }else if($nowPage < $totalPage){
                    $loadingDom.show();
                }else{
                    $loadingDom.hide();
                }
            }
            //加載錯誤訊息
            function showFailure() {

            }
        }
    };
    
    var $menuParent = $(esOptions.menudom.parent),                        //按鈕的顯示窗
        $wrapper = $(esOptions.menudom.wrapper),                          //包住所有按鈕的外框
        $allmenu = $menuParent.find(esOptions.menudom.menuItem);          //按鈕群
    
    var $showParent = $(esOptions.contentdom.parent);                    //翻頁物件的顯示窗
    
    $(window).on('load', function(){
        //撈取各頁籤總頁數後，生成畫面
        $.when.apply($, esOptions.tabItem.map(function(item) {
            getTotalPages(item);
        })).then(function() {
            //開始生成
            esOptions.tabItem.map(function(item) {
                esOptions.show(item['id']);
            })
            
            if(esOptions.tabItem.length > 1){                 //menu數量大於1，才開放滑動效果
                //向左滑
                $showParent.bind('swipeleft', function(e){
                    menuGoNext();
                    e.stopPropagation();
                });
                $showParent.bind('swipe taphold', function(e){
                    e.stopPropagation();
                })
                //向右滑
                $showParent.bind('swiperight', function(e){
                    menuGoPrevious();
                    e.stopPropagation();
                });
            }
            
            $allmenu.bind('click', function() {
                var $that = $(this);
                menuGoNumber($that.index() + 1);
            });
        });
    });
    $(window).on('resize', function(){
        getPageHeight();
        menuLine();
    });
    
    //指定跳頁
    function menuGoNumber(number) {
        var $that = $menuParent.find(esOptions.menudom.menuItem + ':nth-child(' + number + ')'),                  //按鈕active
            $thatItem = $showParent.find(esOptions.contentdom.showItem + ':nth-child(' + number + ')'),           //對應的翻頁物件
            $menuLeft = 0,                                                                                      //按鈕定位
            $showLeft = 0;
        var $item = $(esOptions.contentdom.showItem);                                                               //翻頁物件群
        //翻頁物件
        if($thatItem.length > 0){
            //按鈕
            if($that.length > 0){
                $menuLeft = $that.position().left;
                var $maxW = $wrapper.width();               //總寬度
                var $minW = $menuParent.width();            //螢幕寬度
                if ($menuLeft > $minW / 2) {
                    if($menuLeft < $maxW - $minW / 2) {
                        $menuLeft = ($that.position().left - ($menuParent.width() * 0.5) + $that.width());
                        $menuParent.stop().animate({scrollLeft:$menuLeft},300)    //移動位置，讓按鈕置中
                    }else{
                        $menuParent.stop().animate({scrollLeft:$maxW},300)       //最尾段，如果點選的按鈕未超過螢幕一半時，保持在最後
                    }
                }else{
                    $menuParent.stop().animate({scrollLeft:0},300)               //最前段，如果點選的按鈕未超過螢幕一半時，保持在最前
                }
                $allmenu.removeClass(esOptions.class.active);
                $that.addClass(esOptions.class.active);
                menuLine();
            }
            //翻頁物件active切換
            $item.removeClass(esOptions.class.active);
            $thatItem.addClass(esOptions.class.active);
            
            return true;
        }
        return false;
    }
    
    //上一個(右滑)
    function menuGoPrevious() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        $('body,html').stop().animate({scrollTop:0},500);
        if ($that.index() > 0) {
            $that.prev().click();
            return true;
        }
        return false;
    }
    
    //下一個(左滑)
    function menuGoNext() {
        var $that = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var menuLength = $(esOptions.menudom.wrapper).find(esOptions.menudom.menuItem).length;
        $('body,html').stop().animate({scrollTop:0},500);
        if ($that.index() < menuLength - 1) {
            $that.next().click();
            return true;
        }
        return false;
    }
    
    //menu底線變換
    function menuLine() {
        var $that = $(esOptions.menudom.line);
        var $actMenu = $menuParent.find(esOptions.menudom.menuItem + '.' + esOptions.class.active);
        var $actMenuW = $actMenu.innerWidth();
        $that.stop().animate({left:$actMenu.position().left},200).animate({width:$actMenuW},200);
    }
    
    //ajax用 取得總頁數
    function getTotalPages(item){
        var $getTotalPagesId = item['id'];
        var $ajaxUrl;
        $.map(esOptions.tabItem,function(item){
            if(item['id'] == $getTotalPagesId){
                $ajaxUrl = item['jsonUrl'];      //json路徑      
            }
        });
        return $.ajax({
            url: $ajaxUrl,
            contentType: esOptions.jsonInfo.contentType,
            type: esOptions.jsonInfo.type,  
            dataType: esOptions.jsonInfo.dataType,
            async: esOptions.jsonInfo.async === true,
            cache: esOptions.jsonInfo.cache === true,
            timeout: esOptions.jsonInfo.timeout,
            processData: esOptions.jsonInfo.processData,
            contentType: esOptions.jsonInfo.contentType
        }).then(
            function(data) {             //取得成功
                //將總頁數 寫入 tabItem
                $.map(esOptions.tabItem,function(item){
                    if(item['id'] == $getTotalPagesId){
                        item['total'] = data['retObj']['page']['totalpages'] || 1;
                    }
                });
            },
            function(data) {            //取得失敗
                console.log(data);
            }
        );
    };
</script>

