<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");	
include_once(LIB_DIR ."/ini.php");

$currency = 'RMB';
$code               = $config['alipay']['code'];
$merchantnumber     = $_POST['merchantnumber'];// ='456025';
$paymenttype        = $_POST['paymenttype'];// = 'ALIPAY_WAP'; //ALIPAY';
$ordernumber        = $_POST['ordernumber'];// =79;
$amount             = $_POST['amount'];// = 204;

$serialnumber       = $_POST['serialnumber'];// = 91573;
$writeoffnumber     = $_POST['writeoffnumber'];// ='456025_91573'; 
$timepaid           = $_POST['timepaid'];// =20140315133807;
$tel                = $_POST['tel'];// ='';
$hash               = $_POST['hash'];// ='';

 $verify = md5("merchantnumber=".$merchantnumber.
               "&ordernumber=".$ordernumber.
               "&serialnumber=".$serialnumber.
               "&writeoffnumber=".$writeoffnumber.
               "&timepaid=".$timepaid.
               "&paymenttype=".$paymenttype.
               "&amount=".$amount.
               "&tel=".$tel.
               $code);
 
 //print "verify=".$verify;
 
 //if($hash === $verify)
 {
 	if($merchantnumber != $config['alipay']['merchantnumber']){ die(); }

// file_put_contents('alipay_log', json_encode($_POST));

	// 初始化資料庫連結介面
	$db = new mysql($config["db"]);
	$db->connect();

	//充值記錄
	$query ="SELECT dh.*, unix_timestamp(dh.insertt) as insertt, unix_timestamp(dh.modifyt) as modifyt, dri.amount, dri.spoint
	FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh 
	LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` dri ON 
	     dh.prefixid = dri.prefixid
	     AND dh.driid = dri.driid 
	     AND dri.switch = 'Y'
	WHERE 
		dh.prefixid   = '{$config['default_prefix_id']}' 
		AND dh.dhid   = '{$ordernumber}'
		AND dh.status = 'order'
		AND dh.switch = 'Y'
	" ;
	$table = $db->getQueryRecord($query);
	if(empty($table['table']['record'])){ die(); }
	
	$spoint = (int)$table['table']['record'][0]['spoint'];
	
	//S碼贈點活動
	$query ="SELECT spr . * , sp.name spname
	FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` spr 
	LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp ON 
	    spr.prefixid = sp.prefixid 
		AND spr.spid = sp.spid 
		AND unix_timestamp( sp.offtime ) >0 
		AND unix_timestamp() >= unix_timestamp( sp.ontime ) 
		AND unix_timestamp() <= unix_timestamp( sp.offtime )  
		AND sp.switch = 'Y' 
	WHERE 
		spr.prefixid = '{$config['default_prefix_id']}' 
		AND spr.behav = 'c' 
		AND spr.amount = '{$spoint}' 
		AND spr.switch = 'Y' 
		AND sp.spid IS NOT NULL 
	";
	$row = $db->getQueryRecord($query); 
	if(empty($row['table']['record']) ) { 
		$row_scode = '';
	} else {
		$row_scode = $row['table']['record'][0];
	}
	
	
	//查驗藍新支付寶銷帳狀態
	$order = queryOrder($config, $table['table']['record']); 
	if( empty($order) 
		|| $order['status'] != 1 
		|| $order['merchantnumber'] != $config['alipay']['merchantnumber']
		|| $order['ordernumber'] != $table['table']['record'][0]['dhid']
	){ 
		die(); 
	}
	else 
	{
		if($row_scode)
		{
			//會員充值滿額送S碼
			$scodeModel = new ScodeModel; 
			
			$scode_promote['spid'] = $row_scode['spid'];
			$scode_promote['behav'] = 'c';
			$scode_promote['promote_amount'] = $row_scode['num'];
			$scode_promote['num'] = $row_scode['amount']; 
				
			//插入S碼收取記錄
			$scodeModel->insert_scode($scode_promote, $table['table']['record'][0]['userid'], $row_scode['spname']);
		}
		
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
		SET
			 `prefixid`='{$config['default_prefix_id']}',
			 `userid`='{$table['table']['record'][0]['userid']}', 
			 `countryid`='{$config['country']}', 
			 `behav`='user_deposit', 
			 `currency`='{$currency}',
			 `amount`='{$table['table']['record'][0]['amount']}', 
			 `insertt`=NOW()
		";
		$db->query($query);
		$depositid = $db->_con->insert_id;

		if (!empty($table['table']['record'][0]['spointid']) ) {
		  $spointid = $table['table']['record'][0]['spointid'];
		}
		else {
		  $query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint`
		  SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$table['table']['record'][0]['userid']}', 
			   `countryid`='{$config['country']}',
			   `behav`='user_deposit', 
			   `amount`='{$table['table']['record'][0]['spoint']}', 
			   `insertt`=NOW()
		  ";
		  $db->query($query);
		  $spointid = $db->_con->insert_id;
		}

		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history`
		SET 
			data      = '".json_encode($_POST)."',
			status    = 'deposit',
			spointid  = '{$spointid}',
			depositid = '{$depositid}'
		WHERE 
			prefixid   = '{$config['default_prefix_id']}' 
			AND dhid   = '{$ordernumber}'
			AND status = 'order'
			AND switch = 'Y'
		";
		$db->query($query);
	}
 }

 
//查驗藍新支付寶銷帳狀態
function queryOrder($config, $record) 
{
	$adminurl 			= $config['alipay']['url_query'];
	$code 				= $config['alipay']['code'];
	$merchantnumber		= $_POST['merchantnumber'];
	$ordernumber		= $_POST['ordernumber'];
	$writeoffnumber		= $_POST['writeoffnumber'];
	$paymenttype		= $config['alipay']['paymenttype'];
	$timecreateds		= date("Ymd", $record[0]['modifyt']);
	$timecreatede		= date("Ymd", $record[0]['modifyt']);
	$timepaids			= date("Ymd", $record[0]['modifyt']);
	$timepaide			= date("Ymd", $record[0]['modifyt']);     
	$status				= 1;
	$operation			= 'queryorders';

     $time = date('YmdHis');

     //hash = md5(operation+code+time)
	 $hash = md5($operation . $code . $time);

     $postdata = "merchantnumber=".$merchantnumber.
				"&ordernumber=".$ordernumber.
				"&writeoffnumber=".$writeoffnumber.
				"&paymenttype=".$paymenttype.
				"&timecreateds=".$timecreateds.
				"&timecreatede=".$timecreatede.
                "&timepaids=".$timepaids.
				"&timepaide=".$timepaide.
				"&status=".$status.
                "&operation=".$operation.
				"&time=".$time.
				"&hash=".$hash;

     $url = parse_url($adminurl); 
	 
     if (empty($url['port'])) {
     	if ($url['scheme'] == 'http') {
     		$url['port'] = 80;
     	}
     	else if ($url['scheme'] == 'https') {
     		$url['port'] = 443;
     	}
     }
 //echo '<pre>';print_r($url);exit;
     $postdatalen = strlen($postdata);
     $postdata = "POST ". $url['path'] ." HTTP/1.0\r\n".
                "Content-Type: application/x-www-form-urlencoded\r\n".
                "Host: ". $url['host'] .":". $url['port'] ."\r\n".
                "Content-Length: ". $postdatalen ."\r\n".
                "\r\n".
                $postdata;

     $receivedata = "";

     //$fp = fsockopen ($hostip, $hostport, &$errno, &$errstr, 90);
	 //-- 若不用SSL(https)連接，則改為 $fp = fsockopen ($url['host'], $url['port'], $errno, $errstr, 90);
     $fp = fsockopen ($url['host'], $url['port'], $errno, $errstr, 90);
     
	 if(!$fp) { 
          echo "$errstr ($errno)<br>\n";
     }else{ 
          fputs ($fp, $postdata);

          do{ 
               if(feof($fp)){
                    //echo "connect is break\n";
                 	break;
               }
               $tmpstr = fgets($fp,128);
               $receivedata = $receivedata.$tmpstr;
          } while(true); //!($tmpstr=="0")
          fclose ($fp);
     }

     $receivedata = str_replace("\r","",trim($receivedata));
     $isbody = false;
     $httpcode = null;
     $httpmessage = null;
     $result = "";
     $array1 = explode("\n",$receivedata);
	 //var_dump($array1);exit;
     
	 for($i=0;$i<count($array1);$i++)
	 {
          if($i==0){
               $array2 = explode(" ",$array1[$i]);
               $httpcode = $array2[1];
               $httpmessage = $array2[2];
          }else if(!$isbody){
               if(strlen($array1[$i])==0) $isbody = true;
          }else{
               $result = $result.$array1[$i];
          }
     }

     if($httpcode!="200"){
          if($httpcode=="404") echo "网址错误，无法找到网页!";
          else if($httpcode=="500") echo "服务器错误!";
          else echo $httpmessage;
          return;
     }
	 
     $result = iconv("BIG5","UTF8",$result);
 //print_r( iconv("BIG5","UTF8",$result) );exit;
     parse_str($result, $args);

     return $args;
}
