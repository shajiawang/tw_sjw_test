<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title></title>
<script src="jquery-2.1.0.min.js"></script>
<script src="jquery.qrcode-0.7.0.js"></script>
<script src="jquery-barcode.js"></script>
</head>

<body>
<?php
if((int)substr($_GET['code'], 3, 2)<=10) {
	$code_month = "0".((int)substr($_GET['code'], 3, 2) - 1);
}
else {
	$code_month = (int)substr($_GET['code'], 3, 2) - 1;
}
//102 12 QE 24267204 AAAA

$qrcode = explode(":", $_GET['qrcode']);
//QE242672041021125AAAA00000000000002350000000025089889 XXzIr9xh5ziZD9WYyruilg==:**********:1:1:1:系統使用費:1:235
?>
<div style="width:673px; height:1063px; left:0px; position:absolute; font-size:37px; background-color:#FFF; border:#000 solid 1px; margin:10px;">
    <div style="position:absolute; left:0px; top:132px; width:100%; height:86px; text-align:center; font-size:86px; font-weight:bold;">殺價王</div>
    <div style="position:absolute; left:0px; top:259px; width:100%; height:74px; text-align:center; font-size:74px; font-weight:bold;">電子發票證明聯</div>
    <div style="position:absolute; left:0px; top:339px; width:100%; height:74px; text-align:center; font-size:74px; font-weight:bold;"><?php echo substr($_GET['code'], 0, 3);?>年<?php echo $code_month;?>-<?php echo substr($_GET['code'], 3, 2);?>月</div>
    <div style="position:absolute; left:0px; top:419px; width:100%; height:74px; text-align:center; font-size:74px; font-weight:bold;"><?php echo substr($_GET['qrcode'], 0, 2)."-".substr($_GET['qrcode'], 2, 8);?></div>
    
    <div style="position:absolute; left:69px; top:499px; width:604px; height:37px;"><?php echo $_GET['adate'];?></div>
    <div style="position:absolute; left:69px; top:541px; width:604px; height:37px;">隨機碼：<?php echo substr($_GET['qrcode'], 17, 4);?>　總計：<?php echo $qrcode[7] ;?></div>
    <div style="position:absolute; left:69px; top:583px; width:604px; height:37px;">賣方<?php echo substr($_GET['qrcode'], 45, 8);?></div>
    
    <div style="position:absolute; left:44px; top:628px; width:433px; height:74px;"><div id="barcodeTarget" class="barcodeTarget"></div></div>
    <div style="position:absolute; left:91px; top:711px; width:200px; height:200px;"><div id="qrcode_left"></div></div>
    <div style="position:absolute; left:396px; top:711px; width:200px; height:200px;"><div id="qrcode_right"></div></div>
    
    <div style="position:absolute; left:69px; top:923px; width:604px; height:37px; display:none;">安和　　000000　序000000　機0</div>
    <div style="position:absolute; left:69px; top:965px; width:604px; height:37px; display:none;">退貨憑電子發票證明聯正本辦理</div>
</div>
<div style="width:673px; height:1063px; left:693px; position:absolute; font-size:26px; background-color:#FFF; border:#000 solid 1px; margin:10px;">
    <div style="position:absolute; left:0px; top:132px; width:100%; height:38px; text-align:center; font-size:38px; font-weight:bold;">紙本電子發票使用注意事項</div>
    
    <div style="position:absolute; left:58px; top:189px; width:615px; height:52px; font-size:22px;">中獎兌領：依財政部規定期間內兌領完畢。<br />(如1-2月發票於3/25開獎，4/6-7/5日為領獎期間)</div>
    <div style="position:absolute; left:58px; top:260px; width:615px; height:22px; font-size:22px;">財政部電子發票客服中心：0800-521-988</div>
    
    <div style="position:absolute; left:59px; top:301px; width:614px; height:26px;">領獎收據　新台幣<span style="text-decoration:underline;">　　　　　　　　　　</span>元整</div>
    <div style="position:absolute; left:59px; top:353px; width:614px; height:26px;">中&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;獎&nbsp;&nbsp;&nbsp;&nbsp;人(簽名或簽章)<span style="text-decoration:underline;">　　　　　　　　　</span>&nbsp;</div>
    <div style="position:absolute; left:59px; top:405px; width:614px; height:26px;">身分證號<span style="text-decoration:underline;">　　　　　　　　　　　　　　　　</span>&nbsp;</div>
    <div style="position:absolute; left:59px; top:457px; width:614px; height:26px;">電　　話<span style="text-decoration:underline;">　　　　　　　　　　　　　　　　</span>&nbsp;</div>
    <div style="position:absolute; left:59px; top:509px; width:614px; height:26px;">戶籍地址<span style="text-decoration:underline;">　　　　　　　　　　　　　　　　</span>&nbsp;</div>
</div>
<script type="text/javascript">
function generateBarcode() {
    var value = "<?php
	echo $_GET['code'];
	//10206AB112233449999
	//10212QE24267204AAAA
	?>";
    var btype = "code39";
    var renderer = "bmp";
    var settings = {
        output: renderer,
        bgColor: "#FFFFFF",
        color: "#000000",
        barWidth: "2",
        barHeight: "74",
        moduleSize: "5",
        posX: "0",
        posY: "0",
        addQuietZone: "1"
    };
	$("#barcodeTarget").html("").show().barcode(value, btype, settings);
}
$(function () {
    generateBarcode();
});

$("#qrcode_left").qrcode({
	render: 'canvas',	// render method: `'canvas'`, `'image'` or `'div'`
	minVersion: 10,		// version range somewhere in 1 .. 40
	maxVersion: 40,
	ecLevel: 'L',		// error correction level: `'L'`, `'M'`, `'Q'` or `'H'`
	left: 0,			// offset in pixel if drawn onto existing canvas
	top: 0,
	size: 200,			// size in pixel
	fill: '#000',		// code color or image element
	background: null,	// background color or image element, `null` for transparent background
	text: utf16to8('<?php
	echo $_GET['qrcode'];
	//AB112233441020523999900000145000001540000000001234567ydXZt4LAN1UHN/j1juVcRA==:**********:3:3:0:乾電池:1:105:
	//QE242672041021125AAAA00000000000002350000000025089889XXzIr9xh5ziZD9WYyruilg==:**********:1:1:1:系統使用費:1:235
	?>'),	// content
	radius: 0.0,			// corner radius relative to module width: 0.0 .. 0.5
	quiet: 0,			// quiet zone in modules
	mode: 0,			// 0:normal  1:label strip  2:label box  3:image strip  4:image box
	mSize: 0.3,
	mPosX: 0.5,
	mPosY: 0.5,
	label: 'no label',
	fontname: 'sans',
	fontcolor: '#000',
	image: null//$("#img-buffer")[0]
});
$("#qrcode_right").qrcode({
	render: 'canvas',	// render method: `'canvas'`, `'image'` or `'div'`
	minVersion: 10,		// version range somewhere in 1 .. 40
	maxVersion: 40,
	ecLevel: 'L',		// error correction level: `'L'`, `'M'`, `'Q'` or `'H'`
	left: 0,			// offset in pixel if drawn onto existing canvas
	top: 0,
	size: 200,			// size in pixel
	fill: '#000',		// code color or image element
	background: null,	// background color or image element, `null` for transparent background
	text: '**',	// content
	radius: 0.0,			// corner radius relative to module width: 0.0 .. 0.5
	quiet: 0,			// quiet zone in modules
	mode: 0,			// 0:normal  1:label strip  2:label box  3:image strip  4:image box
	mSize: 0.3,
	mPosX: 0.5,
	mPosY: 0.5,
	label: 'no label',
	fontname: 'sans',
	fontcolor: '#000',
	image: null//$("#img-buffer")[0]
});

function utf16to8(str) {
    var out, i, len, c;
    out = "";
    len = str.length;
    for (i = 0; i < len; i++) {
        c = str.charCodeAt(i);
        if ((c >= 0x0001) && (c <= 0x007F)) {
            out += str.charAt(i);
        } else if (c > 0x07FF) {
            out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
            out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
        } else {
            out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));
            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
        }
    }
    return out;
}
</script>
</body>
</html>
