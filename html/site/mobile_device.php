<?php
//Detect special conditions devices
$iPod = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
$iPhone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
$iPad = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
if(stripos($_SERVER['HTTP_USER_AGENT'],"Android") && stripos($_SERVER['HTTP_USER_AGENT'],"mobile")){
        $Android = true;
}else if(stripos($_SERVER['HTTP_USER_AGENT'],"Android")){
        $Android = false;
        $AndroidTablet = true;
}else{
        $Android = false;
        $AndroidTablet = false;
}
$webOS = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
$BlackBerry = stripos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
$RimTablet= stripos($_SERVER['HTTP_USER_AGENT'],"RIM Tablet");


$isMobile = false;

//do something with this information

if( $iPod || $iPhone ){
    $isMobile = true; //were an iPhone/iPod touch
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if($iPad){
    $isMobile = true; //were an iPad
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if($Android){
    $isMobile = true; //we're an Android Phone
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if($AndroidTablet){
    $isMobile = true; //we're an Android Phone
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if($webOS){
    $isMobile = true; //we're a webOS device
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if($BlackBerry){
    $isMobile = true; //we're a BlackBerry phone
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if($RimTablet){
    $isMobile = true; //we're a RIM/BlackBerry Tablet
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if(isset($_SERVER['HTTP_X_WAP_PROFILE'])){
	$isMobile = true; //"移动设备";
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
}else if(isset($_SERVER['HTTP_VIA'])){
  if(stristr($_SERVER['HTTP_VIA'], "wap")){
	$isMobile = true; //"移动设备";
	$_SESSION['m_prefix'] = 'm';
	$_GET['tpl'] = 'm';
  }
} else {
    //it`s not a mobile device.
	$isMobile = false;
	$_SESSION['m_prefix'] = '';
	$_GET['tpl'] = '';
}
