<?php

	if (!session_id()) {
		session_start();
	}

	include("Facebook/autoload.php");

	$fb = new Facebook\Facebook([
		'app_id' => '202083143845729', // Replace {app-id} with your app id
		'app_secret' => 'f035b34e307c0b8dc347c0fcc87a5b94',
		'default_graph_version' => 'v2.9',
	]);

	$helper = $fb->getRedirectLoginHelper();

	$state_str = $_REQUEST['state'];
	error_log("[getFbOauthData] state : ".($state_str));
	$code = $_REQUEST['code'];
	error_log("[getFbOauthData] code : ".$code);



	try {

	  $accessToken = $helper->getAccessToken();

	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  // When Graph returns an error
	  echo 'Graph returned an error: ' . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  // When validation fails or other local issues
	  echo 'Facebook SDK returned an error: ' . $e->getMessage();
	  exit;
	}

	if (! isset($accessToken)) {
	  if ($helper->getError()) {
		header('HTTP/1.0 401 Unauthorized');
		echo "Error: " . $helper->getError() . "\n";
		echo "Error Code: " . $helper->getErrorCode() . "\n";
		echo "Error Reason: " . $helper->getErrorReason() . "\n";
		echo "Error Description: " . $helper->getErrorDescription() . "\n";
	  } else {
		header('HTTP/1.0 400 Bad Request');
		echo 'Bad request';
	  }
	  exit;
	}

	// Logged in
	// echo '<h3>Access Token</h3>';
	// echo print_r($accessToken->getValue());

	// The OAuth 2.0 client handler helps us manage access tokens
	$oAuth2Client = $fb->getOAuth2Client();

	// Get the access token metadata from /debug_token
	$tokenMetadata = $oAuth2Client->debugToken($accessToken);
	// echo '<br><h3>Metadata</h3>';
	// print_r($tokenMetadata);

	// Validation (these will throw FacebookSDKException's when they fail)
	$tokenMetadata->validateAppId('202083143845729');
	// If you know the user ID this access token belongs to, you can validate it here
	//$tokenMetadata->validateUserId('123');
	$tokenMetadata->validateExpiration();

	if (! $accessToken->isLongLived()) {
	  // Exchanges a short-lived access token for a long-lived one
	  try {
		$accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
	  } catch (Facebook\Exceptions\FacebookSDKException $e) {
		// echo "<p>Error getting long-lived access token: " . $e->getMessage() . "</p>\n\n";
		error_log("[getFbOauthData] Error getting long-lived access token: " . $e->getMessage());
		exit;
	  }

	  // echo '<h3>Long-lived</h3>';
	  // print_r($accessToken->getValue());
	}

	$_SESSION['fb_access_token'] = (string) $accessToken;

	try {
	  // Returns a `Facebook\FacebookResponse` object
	  $response = $fb->get('/me?fields=id,name,gender,locale,picture,timezone', $_SESSION['fb_access_token']);
	} catch(Facebook\Exceptions\FacebookResponseException $e) {
	  echo 'Graph returned an error: ' . $e->getMessage();
	  exit;
	} catch(Facebook\Exceptions\FacebookSDKException $e) {
	  echo '1 Facebook SDK returned an error: ' . $e->getMessage();
	  exit;
	}

	$user = $response->getGraphUser();
	$sso_uid=$user->getId();
	/*
	echo '<br><h3>id</h3>';
	echo $user->getId();
	*/
	$callback_url="https://www.sharebonus.tw/get_login/getUserData/";

	if($state_str) {
	   $state=json_decode(base64_decode($state_str),TRUE);
	   if(is_array($state) && $state["callback_url"]) {
	       $callback_url=$state["callback_url"];
	   }
	}
	error_log("[getFbOauthData] user data : ".$user->asJson());
    $url=$callback_url."?auth_by=fb&sso_uid=".$sso_uid."&state=".($state_str)."&data=".base64_encode(urlencode($user->asJson()));
	error_log("[getFbOauthData] callback url : ".$url);
	header("Location:".$url);
?>
