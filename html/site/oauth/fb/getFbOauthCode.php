<?php
	if (!session_id()) {
		session_start();
	}

	include("./Facebook/autoload.php");

	$fb = new Facebook\Facebook([
		'app_id' => '202083143845729', // Replace {app-id} with your app id
		'app_secret' => 'f035b34e307c0b8dc347c0fcc87a5b94',
		'default_graph_version' => 'v2.9'
	]);

	$helper = $fb->getRedirectLoginHelper();

	$state_str = $_REQUEST['state'];
	error_log("[getFbOauthCode] state_str : ".($state_str));
	error_log("[getFbOauthCode] state : ".base64_decode($state_str));

	$state = json_decode(base64_decode($state_str));
	$permissions = ['email']; // Optional permissions
	$loginUrl = $helper->getLoginUrl('https://www.sharebonus.tw/oauth/fb/getFbOauthData.php', $permissions);

	$helper->getPersistentDataHandler()->set('state',$state_str);
	$loginUrl.="&state=".$state_str;

	error_log("[getFbOauthCode] loginUrl : ".$loginUrl);

	header("Location:".$loginUrl);
	exit;
?>
