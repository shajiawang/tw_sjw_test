﻿<?php
	$_client_id="1584069353";
	$_client_secret="a10fd06edfb8630b1f265ccb159993b7";

	$_redirect_uri="http://test.shajiawang.com/site/oauth/line/getLineOauthData.php";

	function requestCode($response_type='code'
						 ,$client_id
						 ,$redirect_uri="http://test.shajiawang.com/site/oauth/line/getLineOauthData.php"
						 ,$scope="openid%20profile"
						 ,$state=''
						 ,$nonce='') {
		if(empty($nonce)) {
			$nonce=MD5(date('YmdHissi'));
		}
		$url = "https://access.line.me/oauth2/v2.1/authorize".
		"?response_type=".$response_type.
		"&client_id=".$client_id.
		"&redirect_uri=".$redirect_uri.
		"&scope=".$scope.
		"&state=".$state.
		"&nonce=".$nonce;
		error_log("[oauth/line/helper] request Line code : ".$url);
		header("Location:".$url);
		return;
	}

	function requestToken($code,
						$client_id="1584069353",
						$client_secret="a10fd06edfb8630b1f265ccb159993b7",
						$redirect_uri="http://test.shajiawang.com/oauth/line/getLineOauthData.php"
						)
	{
		$url ="https://api.line.me/oauth2/v2.1/token";
		$arrPostData = array(
			'grant_type' => "authorization_code",
			'code' => $code,
			'client_id' => $client_id,
			'client_secret' => $client_secret,
			'redirect_uri' => $redirect_uri
		);
		$postData=http_build_query($arrPostData, '', '&');
		$ch = curl_init();
		$arrHeader[]="Content-Type: application/x-www-form-urlencoded";
		curl_setopt($ch, CURLOPT_HTTPHEADER,$arrHeader);
		//等待時間
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 2);
		curl_setopt($ch, CURLOPT_TIMEOUT, 10);

		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		// 設定referer
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$resultStr = curl_exec($ch);
		curl_close($ch);
		error_log("[oauth/line/helper] token str : ".$resultStr);
		return $resultStr;
	}



	function requestProfile($access_token) {
		$url = 'https://api.line.me/v2/profile';
		$context = array(
			'http' => array(
				'method'  => 'GET',
				'header'  => 'Authorization: Bearer '.$access_token
			)
		);
		$resultStr = file_get_contents($url, false, stream_context_create($context));
		error_log("[oauth/line/helper] profile str ".$resultStr);
		if(!empty($resultStr)) {
			return $resultStr;
		} else {
			return "";
		}
	}
?>
