﻿<?php
     include_once('./helper.php');
?>
<?php

  date_default_timezone_set('Asia/Taipei');
   $code = $_REQUEST['code'];
   error_log("[oauth/line/getLineOauthData.php] code : ".$code);

   $state_str = $_REQUEST['state'];
   $state=base64_decode($state_str);
   error_log("[oauth/line/getLineOauthData.php] state : ".$state);

   if(!empty($code)) {
	   //用code取得用戶授權資訊
	   	$resultStr = requestToken(
		     $code,
		     $_client_id,
			 $_client_secret,
			 $_redirect_uri
		);
		error_log("[oauth/line/getLineOauthData] resultStr : ".$resultStr);

		/*
	    Line Oauth V2.1 回傳的格式,
        見https://developers.line.me/en/docs/line-login/web/integrate-line-login/
		V2.1 版的User profileString ,已埋在id_token中
		放在id_token 的第二段(Payload)中, 用base64_encode編碼
		用code申請就可以抓到
		但是每次都要先申請code才能拿到
		{
 			"access_token":"eyJhbGciOiJIUzI1NiJ9.MeZIq2-SNv_zBUoj0tPZgrIJQwCVpTXM99M7l2OZ0lglRnS9NzyJ6kfzYZxBVpXPHl7PX5LlO_ODJK1RnJnWyq-pb8wSyTwNiWiyPc1UemzuiAwoPQqoytj-vEPKM4_rGt2FeKxbibwg6lNuWV2JF7-Ezm1N8JRTLwP-GjZVKao.0ewuygCAY2MJ2Krm46BTiG2CeZmsb6zzIXCSU4NDNQ8",
			"token_type":"Bearer",
			"refresh_token":"pRt8iPYdsWtFu8f7KPbI",
			"expires_in":2592000,
			"scope":"profile openid",
			"id_token":"eyJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL2FjY2Vzcy5saW5lLm1lIiwic3ViIjoiVWU1NDE2Mzk5YmE2MzVkNzM5Y2ZlYjVhYTViYmM1YzdkIiwiYXVkIjoiMTU2NTI5MTk3MiIsImV4cCI6MTUxOTcyMjE0MiwiaWF0IjoxNTE5NzE4NTQyLCJub25jZSI6IjliNzJmMzY1ZWYyZjQ1YTdjZDhlNzU0ZWFjNjUxZGYzIiwibmFtZSI6IuWKieWGoOengCIsInBpY3R1cmUiOiJodHRwczovL3Byb2ZpbGUubGluZS1zY2RuLm5ldC8wbTAwY2Q5ZTQ0NzI1MTZiNzdkNDYxZmJlOGM3ZmRjZDA4Y2RhY2RkZTg2NjJhIn0.rvE3ppeHQ32D7vKOPFFnvvcjK64o5OpX1AOAjgxy4Vo"
		}
		*/
		$arrInfo = json_decode($resultStr, true);

		// 儲存到cookie, 效期是25天,
		setCookie("saja_line_access_token",$resultStr,time()+$arrInfo['expires_in']-60*60*24*5,"test.shajiawang.com") ;

		$access_token=$arrInfo['access_token'];

		/*
		 V2.1版 user profileString
		{
			"iss":"https://access.line.me",
			"sub":"Ue5416399ba635d739cfeb5aa5bbc5c7d",    // userId
			"aud":"1565291972",                           // Channel Id
			"exp":1519723273,
			"iat":1519719673,
			"nonce":"xxxxxx",
			"name":"xxxxx",                               //display name
			"picture":"https://profile.line-scdn.net/0m00cd9e4472516b77d461fbe8c7fdcd08cdacdde8662a"  // Picture
		}
		*/
		// Todo : 驗簽
		error_log("[oauth/line/getLineOauthData] accessToken : ".$access_token);

		/*
		 V2.0版的 user profileString
		 要用code拿到accessToken
		 再用accessToken 放在header裡去戳 https://api.line.me/v2/profile  才拿得到
		 但好處是accessToken可以存在cookie
		 之後可重複使用30天, 不用每次都去拿code
		{
		 "userId":"Ue5416399ba635d739cfeb5aa5bbc5c7d",
		 "displayName":"xxxxx",
		 "pictureUrl":"https://profile.line-scdn.net/0m00cd9e4472516b77d461fbe8c7fdcd08cdacdde8662a",
		 "statusMessage":"xxxxx"
		 }
		*/

		$profileString = $profileString = requestProfile($access_token);
		error_log("[oauth/line/getLineOauthData.php] line v2 profileString : ".$profileString);

		$arrProfile="";
		if(!empty($profileString))
		    $arrProfile=json_decode($profileString,TRUE);

		$sso_uid="";
		if(is_array($arrProfile))
		   $sso_uid=$arrProfile['userId'];
	   
	    $arr_state=json_decode($state,TRUE);
		if(is_array($arr_state) && !empty($arr_state['callback_url'])) {
			$callback_url=$arr_state['callback_url'];
		} else {	
		    $callback_url="https://www.saja.com.tw/oauthapi/linecallback/";
		}
		$callback_url.="?auth_by=line&sso_uid=".$sso_uid.
					   "&data=".base64_encode(urlencode($profileString)).
					   "&state=".$state_str;
		error_log("[oauth/line/getLineOauthData] callback : ".$callback_url);
		header("Location:".$callback_url);
   }

?>
