﻿<?php
     include_once('./helper.php');
?>
<?php

    // $state是json格式的string
	// Line驗證完成後, $state會由Line系統連同Line產生的user data一起傳給 getLineOauthData.php
	// 再由 getLineOauthData.php 參考$state中的 {"callback_url":"....."} 來決定要把Line的user info傳給哪一隻程式

	date_default_timezone_set('Asia/Taipei');

	$url = "";
	$state_str=$_REQUEST['state'];
	$state_str_decode=base64_decode($state_str);
	error_log("[oauth/line/getLineOauthCode.php] state : ".$state_str_decode);

    //　找cookie內是否存有效的acccess token
	$cookie_json=$_COOKIE['access_token'];
	error_log("[oauth/line/getLineOauthCode.php] cookie : ".$cookie_json);


	if(!empty($cookie_json)){
	    //　有的話就用access token 去取得user profile string
		$cookie = json_decode(stripslashes($cookie_json), true);
		if(is_array($cookie)) {
			$access_token=$cookie['access_token'];
			$arr_state=json_decode($state_str_decode,TRUE);
			error_log("[oauth/line/getLineOauthCode] got Token from cookie : ".$access_token);
			if(!empty($access_token)) {
			   $profileString = requestProfile($access_token);
			   error_log("[oauth/line/getLineOauthCode] (cookie) line v2 profileString : ".$profileString);
			   if(!empty($profileString))
				  $arrProfile=json_decode($profileString,TRUE);

			   $sso_uid="";
			   if(is_array($arrProfile))
				   $sso_uid=$arrProfile['userId'];

			   // $callback_url="http://qc.xuebona.com/LinepayTx/getSsoData/";
			   if(is_array($arr_state) && $arr_state['callback_url']) {
				   $callback_url=$arr_state['callback_url'];
			   }
			   $callback_url.="?auth_by=line&sso_uid=".$sso_uid.
							   "&data=".base64_encode(urlencode($profileString)).
							   "&state=".$state_str;
			   error_log("[oauth/line/getLineOauthCode] (cookie) callback : ".$callback_url);
			   	print_r($arr_state);
				exit;
			   header("Location:".$callback_url);
           }
		} else {
		   //　沒有的話　=> 申請code, 取得token,  再去取得user profile string
		   requestCode(
			   'code',
			   $_client_id,
			   $_redirect_uri,
			   "openid%20profile",
			   $state_str,
			   MD5(date('mdHisis'))
		   );
		}
	} else {
		//　沒有的話　=> 申請code, 取得token,  再去取得user profile string
		requestCode(
			   'code',
			   $_client_id,
			   $_redirect_uri,
			   "openid%20profile",
			   $state_str,
			   MD5(date('mdHisis'))
		);
    }
?>
