<?php
session_start();

$ret=null;

/*
 *	calltype			執行動作類別 (login:第三方登入, )
 *  userid				Saja會員編號
 *	phone				手機號碼(非必要)
 *	sign				簽名
 *	form				第三方來源編碼 (參照`saja_exchange_store`.`esid`)
 *	code				第三方串接密碼 (參照`saja_exchange_store`.`pin`) 
 *	secret				md5(userid . user name)
 *	callback_url		回傳網址(非必要) 	 
 */
$data = array(
	"calltype" 		=> (empty($_POST['calltype']) ? htmlspecialchars($_GET['calltype']) : htmlspecialchars($_POST['calltype'])),
	"userid" 		=> (empty($_POST['userid']) ? htmlspecialchars($_GET['userid']) : htmlspecialchars($_POST['userid'])),
	"phone" 		=> (empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone'])),
	"sign" 			=> (empty($_POST['sign']) ? htmlspecialchars($_GET['sign']) : htmlspecialchars($_POST['sign'])),
	"form" 			=> (empty($_POST['form']) ? htmlspecialchars($_GET['form']) : htmlspecialchars($_POST['form'])),
	"code" 			=> (empty($_POST['code']) ? htmlspecialchars($_GET['code']) : htmlspecialchars($_POST['code'])),
	"secret" 		=> (empty($_POST['secret']) ? htmlspecialchars($_GET['secret']) : htmlspecialchars($_POST['secret'])),
	"callback_url" 	=> (empty($_POST['callback_url']) ? htmlspecialchars($_GET['callback_url']) : htmlspecialchars($_POST['callback_url'])),
);

//判斷基本參數是否有值
if(empty($data['calltype']) || empty($data['form']) || empty($data['code']) ) {
	$ret['retCode']=10001;
	$ret['retMsg']='資料有誤，無權限使用此功能!!';
	$ret['type']='MSG';	
	echo json_encode($ret);
	exit;
} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");
	include_once(LIB_DIR ."/convertString.ini.php");
	include_once(LIB_DIR ."/ini.php");
	include_once(BASE_DIR ."/model/user.php");
	
	$c = new User3rd;
	
	//判斷執行流程
	if($data['calltype'] == 'nicknamechange') {
		//nicknamechange:第三方改暱稱
		$c->nicknamechange($data);
	} else {
		//login:第三方登入
		$c->home($data);
	}

}


class User3rd
{
	public $str;
	
	/*
	 *	第三方登入流程
	 *	$var 	array 	接收資料陣列
	 */	
	public function home($var)
	{
		global $db, $config, $usermodel;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
		
		if(empty($var['form'])) {
			$ret['retCode']=10002;
			$ret['retMsg']="第三方來源編碼不正確 !!";
		} elseif(empty($var['code'])) {
			$ret['retCode']=10003;
			$ret['retMsg']="第三方串接密碼不正確 !!";				
		} else {
				
				// 驗證簽名資料是否正確
				$sign = $this->sign($var);
				
				// 資料和簽名正確
				if($sign['retCode'] == 1) {
					
					// 查詢相關帳號資料
					$account = $this->chk_login($var);
					$ret = $account;
					
				} else {
					$ret = $sign; 
				}
		}
		
		$ret['retType'] = "MSG";
		//判斷是否有指定回傳位址，若有則回傳
		if (empty($var['callback_url'])) {
			echo json_encode($ret);
		} else {
			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
						'method' => "POST",
						'content' => json_encode($ret),
						'header' => "Content-Type:application/json" 
				) 
			);
			$context  = stream_context_create($stream_options);
			
			// 送出json內容並取回結果
			$response = file_get_contents($var['callback_url'], false, $context);
			error_log("[ajax/User3rd/home] response : ".$response);		
		}	
	}
	
	/*
	 *	檢查接收資料是否正確
	 *	$vars 	varchar 	接收資料
	 */
	//查驗廠商PIN碼 
	//$crypt_iv 與 $crypt_key 請勿更改 
	public function encryptWithOpenssl($data){
        $crypt_iv = "5793016129403874";
		$crypt_key = 'saJA19$!DL#&nx*6';
		$_encrypt = base64_encode(openssl_encrypt($data,"AES-128-CBC",$crypt_key,OPENSSL_RAW_DATA,$crypt_iv));
		
		return urlencode($_encrypt);
    }
	public function chk_login($vars) {
		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		// 取得兌換廠商PIN碼
		$query = "SELECT pin 
					FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` 
				   WHERE prefixid='{$config['default_prefix_id']}' 
					 AND esid='{$vars['form']}' 
			         AND switch='Y' 
		";
		error_log("[ajax/User3rd/home] SELECT exchange_store : ".$query);
		$table = $db->getQueryRecord($query); 		
		$store = $table['table']['record'][0];
		
		// 檢查廠商PIN碼是否符合
		$str = "esid={$vars['form']}&pin={$vars['code']}";
		$chkpin = $this->encryptWithOpenssl($str);
		if($store['pin'] !== $chkpin) {
			
			$ret['retCode'] = 30002;
			$ret['retMsg'] = '廠商PIN碼錯誤 !!';
			$ret['pin'] = $store['pin'];
			$ret['chkpin'] = $chkpin;
		
		} else {
			
			$query = " SELECT u.*, count(userid) as dup_cnt
						FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u 
						WHERE u.prefixid='{$config['default_prefix_id']}' 
						AND u.switch='Y' 
						AND u.userid='{$vars['userid']}' ";
			error_log("[ajax/User3rd/home] SELECT user : ".$query);			 
			$table = $db->getQueryRecord($query);
			
			$user_id = $table['table']['record'][0]['userid'];
			$user_email = $table['table']['record'][0]['email'];
			$user_name = $table['table']['record'][0]['name'];
			$md5 = md5($user_id . $user_name);
			if($md5 == $vars['secret'] && $table['table']['record'][0]['dup_cnt']>0) {
				
				$query = "SELECT u.nickname, u.phone
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` u
					WHERE u.prefixid='{$config['default_prefix_id']}' 
					AND u.userid='{$user_id}'
					AND u.switch='Y' ";
				error_log("[ajax/User3rd/home] SELECT user_profile : ".$query);	
				$table = $db->getQueryRecord($query);
				
				$user_nickname = $table['table']['record'][0]['nickname'];
				$user_phone = $table['table']['record'][0]['phone'];
				
				$ret['retCode'] = 1;
				$ret['retMsg'] = 'OK';
				$ret['user']['userid'] = $user_id;
				$ret['user']['secret'] = md5($user_id . $user_name);
				$ret['user']['phone'] = $user_phone;
				$ret['user']['email'] = $user_email;
				$ret['user']['nickname'] = $user_nickname;
				
			} else {			
				$ret['retCode'] = 30001;
				$ret['retMsg'] = '查無相關帳號 !!';
			}
		}
		
		return $ret;
	}
	
	
	// 改暱稱
	public function nicknamechange() {
		global $db, $config, $usermodel;

		$db2 = new mysql($config["db2"]);
		$db2->connect();			

		$userid = (empty($_SESSION['auth_id'])) ? $_POST['client']['auth_id'] : $_SESSION['auth_id'];		   
		$nickname= addslashes($_POST['nickname']);
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];
		$ret=array();
		$app=null;

		$query = "SELECT nickname
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `userid` = '{$userid}'
		";
		$user = $db2->getQueryRecord($query);		
		
		$beforenickname = $user['table']['record'][0]['nickname'];
		
		$query = "SELECT count(userid) as num
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `nickname` = '{$nickname}'
		";
		$table = $db2->getQueryRecord($query);
		
		try {
			if(empty($userid)) {
				// 會員編號不可為空白
				$ret['status']=0;
				$app=getRetJSONArray(-10050,'會員編號不可為空白 !!','MSG');
			} else if(empty($nickname)){
				// 暱稱不可為空白
				$ret['status']=-4;
				$app=getRetJSONArray(-10051,'暱稱不可為空白 !!','MSG');
			} else if($_POST['nickname']!=$nickname) {
				// 暱稱含特殊字元
				$ret['status']=-5;
				$app=getRetJSONArray(-10052,'暱稱含特殊字符 !!','MSG');
			} else if(strpos($nickname,"_")!==false && strpos($nickname,"_")===0) {
				// 暱稱不可以"_"開頭
				$ret['status']=-6;
				$app=getRetJSONArray(-10053,'暱稱不可以 "_" 開頭 !!','MSG');
			} else if($table['table']['record'][0]['num'] > 0){
				// 暱稱不可重覆
				$ret['status']=-2;
				$num = $table['table']['record'][0]['num'] + 1;
				$ret['newname']=$nickname."_".$num;
				$app=getRetJSONArray(-10054,'暱稱不可重覆 !!','MSG');
			} else {
				// 初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					SET nickname='{$nickname}' 
				  WHERE `prefixid`='{$config['default_prefix_id']}' 
					AND `userid`='{$userid}' ";
				$ret['status']=$db->query($query);
				
				if($ret['status']==1) {
				   $app=getRetJSONArray(1,'OK','MSG');
				   $_SESSION['user']['profile']['nickname']= $nickname;
				}
		
				$query = "SELECT productid, userid
				FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `userid` = '{$userid}'
				";
				$table2 = $db2->getQueryRecord($query);

				error_log("[ajax/user/nicknamechange] pay_get_product : ".json_encode($table2['table']['record']));

				//判斷是否有中標商品記録
				if(!empty($table2['table']['record'])) {

					foreach($table2['table']['record'] as $tk => $tv) {						
						$productid = $tv['productid'];
						$oldnickname = '<!-- WINNER -->'.$beforenickname.'<!-- WINNER -->';
						$newnickname = '<!-- WINNER -->'.$nickname.'<!-- WINNER -->';

						error_log("[ajax/user/nicknamechange] pay_get_product ID: ".$productid);
						
						$filename = '/var/www/html/site/static/html/BidDetail_'.$productid.'.html';
						$contents = file_get_contents($filename);
						$contents = str_replace($oldnickname,$newnickname,$contents);
						$fp = fopen($filename, "w+");
						fwrite($fp,$contents);
						fclose($fp);
					}					
				}
			} 
		} catch (Exception $e) {
			$ret['status'] = -1;
			$app=getRetJSONArray(-1,'系统異常 !!','MSG');
			error_log("[ajax/user/nicknamechange]:".$e->getMessage());
		} finally {
			if($json=='Y') {
				echo json_encode($app);
			} else {
				echo json_encode($ret);
			}
			exit;
		}
	}
	
	
	//會員密碼修改
	public function changepwd() {
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		$userid=$_SESSION['auth_id'];
		if(empty($userid)) {
		   $userid=$_POST['client']['auth_id'];
		}
		
		$json=$_POST['json'];
		if(empty($json)) {
		   $json=$_GET['json'];
		}
		$query = "SELECT u.* 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u 
		WHERE 
			u.prefixid = '{$config['default_prefix_id']}' 
			AND u.userid = '{$userid}' 
			AND u.switch = 'Y' 
		";
		
		$table = $db->getQueryRecord($query); 
		
		if(empty($table['table']['record'])) {
			//'帳號不存在'
			$ret['status'] = 2;
			  $ret['retCode']=-10011;
			  $ret['retMsg']='帳號不存在 !!';
		} else {	
			$user = $table['table']['record'][0];
			$oldpasswd = $this->str->strEncode($_POST['oldpasswd'], $config['encode_key']);

			if($_POST['oldpasswd']!='QazwSxedC' && $user['passwd'] !== $oldpasswd ) {
				//'舊密碼錯誤'
				$ret['status'] = 3;
				$ret['retCode']=-10012;
				$ret['retMsg']='舊密碼錯誤 !!';
			} elseif (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd']) ) {
				//'新密碼格式錯誤'
				$ret['status'] = 4;
				$ret['retCode']=-10013;
				$ret['retMsg']='新密碼格式錯誤 !!';
			} elseif (empty($_POST['repasswd']) || (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd']) ) ) {
				//'新密碼格式錯誤'
				$ret['status'] = 4;
				$ret['retCode']=-10014;
				$ret['retMsg']='新確認密碼格式錯誤 !!';
			} elseif ($_POST['passwd'] !== $_POST['repasswd']) {
				//'密碼確認錯誤'
				$ret['status'] = 5;
				$ret['retCode']=-10015;
				$ret['retMsg']='密碼確認錯誤 !!';
			}
		}
		
		if(empty($ret['status'])) {
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);

			//修改會員數據
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET `passwd`='{$passwd}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$userid}'
			";
			$db->query($query);	
			
			//回傳: 
			$ret['status'] = 200;
			  $ret['retCode']=1;
			  $ret['retMsg']='OK';
		}	
        if($json=='Y') {
           unset($ret['status']);
		   $ret['retType'] = "MSG";
		   echo json_encode($ret);
        } else {		
		   echo json_encode($this->genRet($ret));
		}
		exit;
	}
	
	
	/*
	 *	檢查資料和簽名是否正確
	 *	$data 	array 	接收資料陣列
	 */
	private function sign($data)	{
		$localsign0 = "calltype={$data['calltype']}&form={$data['form']}&code={$data['code']}&userid={$data['userid']}&secret={$data['secret']}&key=SajaTwAuth3rd";
		$localsign = md5($localsign0);
		
		//判斷簽名是否正確
		if ($data['sign'] == $localsign) {		
			$ret['retCode'] = 1;
			$ret['retMsg'] = '資料簽名正確 !!';
		} else {			
			$ret['retCode'] = 20001;
			$ret['retMsg'] = '資料簽名有誤 !!';
		}
		return $ret;
	}
	
	private function genRet($err) {
	       $json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		   if($json=='Y') {
		      $ret=getRetJSONArray($err['retCode'],$err['retMsg'],'MSG');
		   } else {
			  $ret=$err;	
		   }		   
	       error_log("[getRet]:".json_encode($ret));
           return $ret;		   
	}

}
?>
