<?php
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
	foreach($_POST2 as $k=> $v) {
		$_POST[$k]=$v;
		if($_POST['client']['json']) {
			$_POST['json']=$_POST['client']['json'];
		}
		if($_POST['client']['auth_id']) {
			$_POST['userid']=$_POST['client']['auth_id'];
		}
	}
}

session_start();
// 1. 要接收 $json, 
$ret=null;
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
error_log("[ajax/auth] auth_id :".$_POST['auth_id']);

if(empty($_SESSION['auth_id']) && empty($_POST['auth_id'])) { //'請先登入會員帳號'
	
	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';	
	} else {
	   $ret['status'] = 1;
	}
	echo json_encode($ret);
	exit;
} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");
	include_once(LIB_DIR ."/convertString.ini.php");
	include_once(LIB_DIR ."/ini.php");
	include_once(BASE_DIR ."/model/user.php");

	error_log("[ajax/auth] type:".$_POST['type']."|phone:".$_POST['phone']);
	$c = new UserAuth;

	if($_POST['type']=='check_sms') {
		$c->check_sms();
	} else if($_POST['type']=='verify_phone') {
		$c->verify_phone();
	} else if($_POST['type']=='upd_verify_phone') {
	    $r=$c->update_phone();
		error_log("--".$r['status']);
		if(!empty($r['status'])) {
		  echo json_encode($r);
		} else {
		  $c->verify_phone();
		}
	} else {
		$c->home();
	}
}


class UserAuth 
{
	public $str;
	
	public function home() {
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$ret['status'] = 0;
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		
		echo json_encode($ret);
	}

	//检查簡訊驗證碼
	public function verify_phone() {
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		$usermodel = new UserModel;
		$ret['status'] = 0;
		$chk1 = $this->chk_phone();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
			$ret['retCode'] = $chk1['err'];
			$ret['retMsg'] =  $chk1['errMsg'];
		}
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
			$ret['retCode'] =  1;
			$ret['retMsg'] =  "手機驗證碼發送成功";
		}
		
		echo json_encode($ret);
	}
	
	//手機检查
	/*
	   手機號碼09開頭=>臺灣
	   手機號碼1開頭=>大陸
	*/
	public function chk_phone()	{
		global $db, $config, $usermodel;
	
		$r['err'] = '';
		$r['errMsg'] = '';
		
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		error_log("[ajax/auth/chk_phone] phone : ".$_POST['phone']);
		
		if (empty($_POST['phone'])) {
			//'手機號碼不能空白' 
			$r['err'] = 2;
			$r['errMsg'] = "手機號碼不能空白";
		} else if (ctype_digit($_POST['phone'])==false) {
		    //'手機號碼非數字'
			$r['err'] = 9;
			$r['errMsg'] = "手機號碼非數字";
		} else {
			$user_auth = $usermodel->validUserAuth($userid, $_POST['phone']);
			if (false) {
				//'手機號碼不正確' 
				$r['err'] = 3;
				$r['errMsg'] = "手機號碼不正確";
			} else {
				if($user_auth['verified']=='Y') {
					//('手機號碼已驗證!!', $this->config->default_main);
					$r['err'] = 4;
					$r['errMsg'] = "手機號碼已驗證";
				} else {
					$uid = $userid; 
					$code = $user_auth['code']; 
					$user_phone = $_POST['phone'];
					$user_phone_0 = substr($user_phone, 0, 1);
					$user_phone_1 = substr($user_phone, 1, 1);

					if($user_phone_0 == '0' && $user_phone_1 == '9') {
						$phone = $user_phone;
						$area = 1;
					} else if($user_phone_0 == '1') {
						$phone = $user_phone;
						$area = 2;
					} else {
						//'手機號碼只提供台灣及大陸驗證' 
						$r['err'] = 5;
						$r['errMsg'] = "手機號碼只提供台灣及大陸驗證";
					}
					error_log("area:".$area.",code:".$code);
					if(! $this->mk_sms($uid, $_POST['phone'], $code, $area) ) {
						//'手機號碼不正確' 
						$r['err'] = 3;
						$r['errMsg'] = "手機號碼不正確";
					}
				}
			}
		}
		return $r;
	}
	
	//SMS驗證碼
	/*
	   手機號碼09開頭=>臺灣, 呼叫簡訊王傳送驗證碼
	   手機號碼1開頭=>大陸, 呼叫中國短信網傳送驗證碼
	*/
	public function mk_sms($uid, $phone, $code, $area) {
		global $db, $config;
		
		if ($area == 1) {
			//簡訊王 SMS-API
			$message = "殺價王驗證碼: ".$code;
			$sMessage = urlencode(mb_convert_encoding($message, 'BIG5', 'UTF-8'));			
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
		        $getfile = file($to_url);
				error_log("[ajax/auth] getfile : ".json_encode($getfile)); 
			if(!$getfile) {
				//('ERROR: SMS-API 無法连接 !', $this->config->default_main ."/user/register");
				return false;
			} else {
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				if($kmsgid < 0) {
					//('手機號碼錯誤!!', $this->config->default_main ."/user/register");
					return false;
				}
				else {
					return true;
				}
			}
		} else if ($area == 2) {
			//中國短信網
			$url='http://dxhttp.c123.cn/tx/';
			$data = array
			(
				'uid' => '501091960002',
				'pwd' => strtolower(md5('sjw25089889')),
				'mobile' => $phone,
				'content' => ($code.'（手機認證驗證碼，三十分鐘内有效）'),
				'encode' => 'utf8'
			);				
			
			$xml = $this->postSMS($url,$data);
			if (trim($xml) == 100) {
			    return true;
		    } else {
				return false;
			}
		}
	}
	
	// 以HTTP Post 方式傳送發簡訊的request (中國短信網用)
	public function postSMS($url, $data='') {
		$row = parse_url($url);
		$host = $row['host'];
		$port = $row['port'] ? $row['port'] : 80;
		$file = $row['path'];
		while (list($k,$v) = each($data)) {
			$post .= rawurlencode($k)."=".rawurlencode($v)."&";
		}
		$post = substr($post , 0 , -1);
		$len = strlen($post);
		$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
		if (!$fp) {
			return "$errstr ($errno)\n";
		} else {
			$receive = '';
			$out = "POST $file HTTP/1.0\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Content-type: application/x-www-form-urlencoded\r\n";
			$out .= "Connection: Close\r\n";
			$out .= "Content-Length: $len\r\n\r\n";
			$out .= $post;		
			fwrite($fp, $out);
			while (!feof($fp)) {
				$receive .= fgets($fp, 128);
			}
			fclose($fp);
			$receive = explode("\r\n\r\n",$receive);
			unset($receive[0]);
			return implode("",$receive);
		}
	}
	
	//SMS完成驗證
	/*
	   檢查session中的userid和($_POST)phone no是否有對應資料(saja_user_profile & saja_user_sms_auth)
	   No->回傳錯誤訊息
	   Yes->修改saja_user_sms_auth中該userid的check code
	        檢查是否有介紹人(saja_scode_history)
	        No->直接回傳完成訊息
			Yes->介紹人贈送S碼(saja_scode)
			     介紹人S碼領取狀態enable(saja_scode_history)
			     回傳完成訊息
	*/
	public function check_sms() {
		global $db, $config, $usermodel;
	
		$userid = empty($_SESSION['auth_id']) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		$phone = $_POST['phone'];
		$smscode = $_POST['smscode'];
		$mid = $_POST['mid'];
		$ret['status'] = 0;
		
		// 接收值驗證失敗就直接回傳 不用耗費資源連DB
		if(empty($phone)) {
		   $ret['status'] = 2;
		   $ret['retCode']= 2;
		   $ret['retMsg']='手機號碼不能空白!!';
		  echo json_encode($ret);
		} else if(empty($smscode)) {
		   $ret['status'] = 3;
		   $ret['retCode']= 3;
		   $ret['retMsg']='手機號碼不正確!!';
		  echo json_encode($ret);
		} else if(empty($userid)) {
		   $ret['status'] = 4;
		   $ret['retCode']= 4;
		   $ret['retMsg']='手機號碼已驗證!!';
           echo json_encode($ret);		   
		}
		error_log("[ajax/auth/check_sms] ret1 : ".$ret['status']);
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		$usermodel = new UserModel;
		$user_auth = $usermodel->validSMSAuth($userid);
		
		error_log("[ajax/auth/check_sms] user_auth : ".json_encode($user_auth));
		error_log("[ajax/auth/check_sms] smscode : ".$smscode);
		error_log("[ajax/auth/check_sms] userid : ".$userid);
		
		if (empty($user_auth) ) {
			//'無此用戶驗證資料' 
			$ret['status'] = 2;
			$ret['retCode']= 2;
			$ret['retMsg']='手機號碼不能空白!!';
		} else if ($smscode !== $user_auth['code']) {
			//'驗證碼不正確' 
			$ret['status'] = 3;
			$ret['retCode']= 3;
			$ret['retMsg']='手機號碼不正確!!';
		 }
		if($ret['status']===0) {
			//修改SMS check code
			$shuffle = get_shuffle();
			$this->str=new convertString();	
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
						 SET `verified`='Y', 
							 `phone`='{$phone}'
					   WHERE `prefixid` = '{$config['default_prefix_id']}'
						 AND `userid` = '{$userid}' ";
			$db->query($query);

			$query = " SELECT count(userid) as cnt
						FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` 
						WHERE `userid`='{$userid}'
						AND `switch`='Y' ";				   
			$chk_sso = $db->getQueryRecord($query);
			
			// 修改 saja_user的name 
			$passwd = $this->str->strEncode(substr($phone, -6), $config['encode_key']);

			//判斷是第三方會員，才更新登入密碼
			if($chk_sso['table']['record'][0]['cnt'] > 0){
				$update_sso_passwd = " ,passwd='{$passwd}'";
			}else{
				$update_sso_passwd = "";
			}
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
						SET  exchangepasswd='{$passwd}' ";

			$query .= $update_sso_passwd;

			$query .= " WHERE `prefixid`='{$config['default_prefix_id']}' 
						AND `userid`='{$userid}'";

			$db->query($query);
			

			$query2 = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							SET phone='{$phone}' 
						WHERE `prefixid`='{$config['default_prefix_id']}' 
							AND `userid`='{$userid}' ";
			$db->query($query2);				

			
			//檢查是否有介紹人(送scode)
			$query = "select * from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history`
				where `prefixid` = '{$config['default_prefix_id']}' 
				and memo = '{$userid}'
				and switch = 'N'";
			$table = $db->getQueryRecord($query);
			
			if(!empty($table['table']['record'])) {
				$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` 
				SET switch = 'Y', modifyt = NOW() 
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `scodeid` = '{$table['table']['record'][0]['scodeid']}' 
					and `userid` = '{$table['table']['record'][0]['userid']}'
				";
				$db->query($query);
				
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}scode_history` 
				SET switch = 'Y', modifyt = NOW() 
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}'
					AND `sphid` = '{$table['table']['record'][0]['sphid']}'
				";
				$db->query($query);
			}

			//老人回娘家活動，以手機號判斷是否為舊會員 20190709
			$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}old_user_list`
				WHERE `phone` = '{$phone}' 
				AND is_given = 'N'
				AND switch = 'Y'";
			$table1 = $db->getQueryRecord($query);

			if(!empty($table1['table']['record'])){
				
				/*原程式碼
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$userid}',
					`behav`='gift',
					`amount`='100',
					`remark`='1',
					`insertt`=NOW()
				";
				$db->query($query); 
				$spointid = $db->_con->insert_id;

				if(!empty($spointid)){
					$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}old_user_list` 
					SET is_given = 'Y'  
					WHERE 
						`phone` = '{$phone}'
						AND `switch` = 'Y'
					";
					$db->query($query);
				}
				*/
				// update By Thomas 
				// 延遲一點時間, 0.05 ~ 0.2 秒不等
				$sleep = 50000 * (getRandomNum()%4+1);
				error_log("[ajax/auth] sleep : ".($sleep)."  micro seconds ...");
				usleep($sleep);
				
				/*
				原程式碼
				// 寫入saja_spoints前再檢查一次 remark=1的spoints紀錄 
				$query = " SELECT count(spointid) as gift_cnt 
				              FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
				             WHERE `userid`='{$userid}'
                               AND `behav`='gift'
                               AND `remark`='1'							   
                               AND `switch`='Y' ";				   
				$chk = $db->getQueryRecord($query);
				error_log("[ajax/auth] chk : ".$query." ==> ".$chk['table']['record'][0]['gift_cnt']);	
				*/

				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}old_user_list` 
				SET is_given = 'Y'  
				WHERE 
					`phone` = '{$phone}'
					AND `switch` = 'Y'
				";
				$db->query($query);
				//update的資料筆數
				$affected_num = $db->_con->affected_rows;
				error_log("[ajax/auth] update : ".$query." ==> ".$affected_num);	

				if($affected_num > 0) {  // 有更新資料，才送幣

					// 寫入saja_spoints前再檢查一次 remark=1的spoints紀錄 
					$query = " SELECT count(spointid) as gift_cnt 
								FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
								WHERE `userid`='{$userid}'
								AND `behav`='gift'
								AND `remark`='1'							   
								AND `switch`='Y' ";				   
					$chk = $db->getQueryRecord($query);
					error_log("[ajax/auth] chk : ".$query." ==> ".$chk['table']['record'][0]['gift_cnt']);

					//確認無送幣紀錄
					if($chk['table']['record'][0]['gift_cnt'] == 0){
						//新增送幣紀錄
						$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
						SET
							`prefixid`='{$config['default_prefix_id']}',
							`userid`='{$userid}',
							`behav`='gift',
							`amount`='100',
							`remark`='1',
							`insertt`=NOW()
						";
						$db->query($query);
						$spointid = $db->_con->insert_id;

						//新增活動送幣使用紀錄
						$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` 
						SET
							`userid`='{$userid}',
							`behav`='gift',
							`activity_type` = '1',
							`amount_type` = '1',
							`free_amount` = '100',
							`total_amount`= '100',
							`spointid`= '{$spointid}',
							`insertt`=NOW()
						";
						$db->query($query); 
						
					}
				} 
			}

			// 2019/09/04 的新會員手機驗證送 20190910
			$activity_type = 1001;//活動代碼

			if ( $today<'2019-09-11 22:03:00' ) {
				$query = "SELECT count(*) u_cnt
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
					WHERE
						u.switch = 'Y'
						AND u.userid = '{$userid}'
						AND u.insertt > '2019-09-04'";
				$table2 = $db->getQueryRecord($query);

				if ( $table2['table']['record'][0]['u_cnt']!=0 ) {
					$query = "SELECT count(*) ah_cnt
						FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` ah
						WHERE
							ah.switch = 'Y'
							AND ah.activity_type= {$activity_type}
							AND ah.phone = '{$phone}'";
					$table3 = $db->getQueryRecord($query);

					if ( $table3['table']['record'][0]['ah_cnt']==0 ) {
						$today = date('Y-m-d H:i:s');
						// 延遲一點時間, 0.05 ~ 0.2 秒不等
						$sleep = 50000 * (getRandomNum()%4+1);
						error_log("[ajax/auth] sleep : ".($sleep)."  micro seconds ...");
						usleep($sleep);
				
						//新增送幣紀錄
						$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
						SET
							`prefixid`='{$config['default_prefix_id']}',
							`userid`='{$userid}',
							`behav`='gift',
							`amount`='100',
							`remark`='{$activity_type}',
							`insertt`=NOW()
						";
						$db->query($query);
						$spointid = $db->_con->insert_id;

						//新增活動送幣使用紀錄
						$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` 
						SET
							`userid`='{$userid}',
							`behav`='gift',
							`activity_type` = '{$activity_type}',
							`amount_type` = '1',
							`free_amount` = '100',
							`total_amount`= '100',
							`spointid`= '{$spointid}',
							`insertt`=NOW()
						";
						$db->query($query); 

						for ($i=0; $i < 3; $i++) { 
							//新增送券紀錄
							$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
								SET 
									prefixid='saja',
									userid='{$userid}',
									productid='10044',
									spid=0,
									behav='gift',
									used='N',
									used_time='0000-00-00 00:00:00',
									amount=1,
									serial='',
									verified='N',
									switch='Y',
									seq=0,
									remark = '{$activity_type}',
									insertt=NOW() ";
							
							$db->query($query);
							$oscode = $db->_con->insert_id;
						}

						// 寫入活動紀錄
						$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
							SET
								`userid`='{$userid}',
								`phone`='{$phone}',
								`activity_type`= {$activity_type},
								`insertt`=NOW()";
						$db->query($query); 
						error_log("[ajax/auth] insert : ".$query);
						
						$_SESSION['user']['profile']['phone'] = $phone;
						//回傳:
						$ret['status'] = 200;
						$ret['retCode']= 1;
						$ret['retMsg']="手機驗證成功，"."\n"."恭喜獲得重機殺價券3張及100殺價幣！";
						error_log("[ajax/auth/check_sms] ret : ".$ret['status']);
						echo json_encode($ret);
						exit();
					}
				}
			}

			$_SESSION['user']['profile']['phone'] = $phone;
			//回傳: 
			$ret['status'] = 200;
			$ret['retCode']= 1;
			$ret['retMsg']='手機驗證成功!!';
		 }
		error_log("[ajax/auth/check_sms] ret : ".$ret['status']);
		echo json_encode($ret);
	}
	
	public function update_phone() {

		global $db, $config, $usermodel;

		$userid = (empty($_SESSION['auth_id']) ) ? $_POST['client']['auth_id'] : $_SESSION['auth_id'];	
		$phone= $_POST['phone'];
		$ret= array();
		$ret['status']='';
		
		try {
			if(empty($userid)) {
			  $ret['status']=-1;
			} else if(empty($phone)) {
			  $ret['status']=-4;
			} else if(ctype_digit($phone)==false) {
			  $ret['status']=-5;
			} else {
				// 初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();

				// 檢查重複
				$query = " SELECT count(userid) as dup_cnt 
							FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
						   WHERE prefixid='{$config['default_prefix_id']}' 
							 AND userid!='{$userid}' 
							 AND name='{$phone}' ";
				$table = $db->getQueryRecord($query);
				
				if($table['table']['record'][0]['dup_cnt']>0) {
					$ret['status']=-2;
				} else {
					$_SESSION['user']['profile']['phone']=$phone;
				}
			} 
		} catch (Exception $e) {
			$ret['status'] = -99;		 
		} finally {
			return $ret;
		}
	}
	
}
?>
