<?php
session_start();

$time_start = microtime(true);

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
   // error_log($k."=>".$v);
	foreach($_POST2 as $k=> $v) {
		$_POST[$k]=$v;
		if($_POST['client']['json']) {
			$_POST['json']=$_POST['client']['json'];
		}
		if($_POST['client']['auth_id']) {
			$_POST['userid']=$_POST['client']['auth_id'];
		}	  
	}
}

$ret=null;
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
error_log("[ajax/product] POST : ".json_encode($_POST));

if(empty($_SESSION['auth_id']) && empty($_POST['client']['auth_id']) && empty($_POST['root'])) { //'請先登入會員帳號'
	
	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';	
		
	} else {
	   
	   $ret['status'] = 1;
	}
	
	echo json_encode($ret);
	exit;

} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/jpush.php");	
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/ini.php");
 	include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");
    require_once("/var/www/lib/vendor/autoload.php");   
	//$app = new AppIni; 
	
	$c = new ProductBid;
	$c->saja();
}

use WebSocket\Client;

class ProductBid {
	public $userid = '';
	public $countryid = 1;//國家ID
	public $currency_unit = 1;//貨幣最小單位
	public $last_rank = 11;//預設順位提示（順位排名最後一位）
	public $saja_time_limit = 0;//每次下標時間間隔限制
	public $display_rank_time_limit = 60;//（秒）下標後離結標還有n秒才顯示順位
	public $range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標
	public $tickets = 1;
	public $price;
	public $username='';	
	
	//下標
	public function saja() {	
		$time_start = microtime(true);
				
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		// if($db) {
		  error_log("[ajax/product] connect DB ...");
  		  $db = new mysql($config["db"]);
		  $db->connect();
		// }
		
		$JSON=$_POST['json'];
		
		$router = new Router();
		
		$this->userid = (empty($_SESSION['auth_id']) ) ? $_POST['client']['auth_id'] : $_SESSION['auth_id'];	
		$this->username= (empty($_SESSION['user']) ) ? $_POST['client']['auth_nickname'] : $_SESSION['user']['profile']['nickname'];	
		
		if(empty($this->userid) && $_POST['root'] == 'weixin' && !empty($_POST['userid'])) {
			$this->userid = $_POST['userid'];
		}		
		
		if(empty($this->username)) {
			$query ="SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`  
			WHERE prefixid = '{$config['default_prefix_id']}'
			  AND userid = '{$this->userid}'
			  AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$this->username = $table['table']['record'][0]['nickname']; 		
		}
		
		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['status'] = 0;
		$ret['winner'] = '';
		$ret['rank'] = '';
		$ret['value'] = 0;
		
		//讀取商品資料
		$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
		WHERE  p.prefixid = '{$config['default_prefix_id']}'
		  AND p.productid = '{$_POST['productid']}'
		  AND p.switch = 'Y'
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		unset($table['table']['record'][0]['description']);
		$product = $table['table']['record'][0]; 

		if(($product['offtime'] == 0 && $product['locked'] == 'Y') || ($product['offtime'] > 0 && $product['now'] > $product['offtime'])) {
			//回傳: 本商品已結標
			$ret['status'] = 2;
			if($JSON=='Y') {
				$ret=getRetJSONArray(-2,'本商品已結標!!','MSG');
				echo json_encode($ret);
				exit;
			}
		}

        //下標資格
        // 閃殺不檢測 By Thomas 20190102
		if($product['is_flash']!='Y') {
            if ($this->userid != 1705) {
                $chk = $this->chk_saja_rule($product);
                if ($chk['err'] && $chk['err']>0) {
                    if($JSON=='Y') {
                        $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],$chk['retType']);
                        echo json_encode($ret);
                        exit;
                    } else {
                        $ret['status'] = $chk['err'];
                        $ret['value'] = $chk['value'];
                    }
                }
            }
        }
		
		if($_POST['type']=='single') { //單次下標
			$chk = $this->chk_single($product);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
			    }
			} 
		} else if($_POST['type']=='range' ) { //區間連續下標
			$chk = $this->chk_range($product);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
			       echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
				}
			}
		} else if($_POST['type']=='lazy') { //懶人下標
			$chk = $this->chk_range($product);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
				}
			}
		}
		
		if(empty($ret['status']) || $ret['status']==0 ) {
			
			//產生下標歷史記錄
			$mk = $this->mk_history($product);
			
			//回傳: 下標完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;			
			if($mk['err']) {
				// 有異常
				if($JSON=='Y') {
				   $ret=getRetJSONArray($mk['retCode'],$mk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				}   
			}
			
			// 正常下標
			if($JSON=='Y') {
			    $ret['retObj']=array();
			    $ret['retObj']['winner'] = $mk['winner'];
				$ret['retObj']['rank'] = $mk['rank'];
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				//單次下標
				if($_POST['type']=='single') {
					if ($mk['rank']==-1) {
						$msg = '很抱歉！這個價錢與其他人重複了！請您試試其他價錢或許會有好運喔！';
					} else if($mk['rank']==0) {
						$msg = '恭喜！您是目前的中標者！記得在時間到之前要多布局！';
					} else if ($mk['rank']>=1 && $mk['rank']<=10) {
						$msg = '恭喜！這個價錢是「唯一」但不是「最低」目前是「唯一」又比您「低」的還有 '.($mk['rank']).' 位。';
					} else if ($mk['rank']>10) {
						$msg = '恭喜！這個價錢是「唯一」但不是「最低」目前是「唯一」又比您「低」的超過 10 位。';
					}
					$ret['retMsg'] = '單次出價 '.$_POST['price'].'元 成功 '. $msg;
				} else if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['retObj']['total_num'] = $mk['total_num'];
					$ret['retObj']['num_uniprice'] = $mk['num_uniprice'];
					$ret['retObj']['lowest_uniprice'] = $mk['lowest_uniprice'];
					if($mk['total_num']>=2) {
						if($mk['num_uniprice']>0) {
						   $msg ='本次有 '.$mk['num_uniprice'].' 個出價是唯一價, 其中最低價格是 '.$mk['lowest_uniprice'].' 元 !';
						} else {
						   $msg ='本次下標的出價都與其他人重複了 !';
						}
					}
					$ret['retMsg'] = '連續出價 '.$_POST['price_start'].'至'.$_POST['price_stop'].'元 成功 !'.$msg;
				}
			} else {
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['total_num'] = $mk['total_num'];
					$ret['num_uniprice'] = $mk['num_uniprice'];
					$ret['lowest_uniprice'] = $mk['lowest_uniprice'];
				}
			}
		}
		error_log("[ajax/product] saja : ".json_encode($ret));
		echo json_encode($ret);
	}
	
	//下標資格
	public function chk_saja_rule($product)	{
		global $db, $config;
	
		$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt` 
		where 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND productid = '{$product['productid']}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		$r['err'] = '';
		$r['value'] = 0;
		$count = $this->pay_get_product_count($this->userid);
		if($table['table']['record'][0]['srid'] == 'never_win_saja') {		//新手(未曾得標)
			if ($count > 0) {
				$r['err'] = 15;
				$r['retCode']=-100215;
				$r['retMsg']='未符合(未得標新手)資格!!';
			}
		} else if($table['table']['record'][0]['srid'] == 'le_win_saja') {	//得標次數n次以內
			if($count > $table['table']['record'][0]['value']) {
				$r['err'] = 16;
				$r['value'] = $table['table']['record'][0]['value'];
				$r['retCode']=-100216;
				$r['retMsg']='未符合下標資格!!';
			}
		} else if($table['table']['record'][0]['srid'] == 'ge_win_saja') {	//得標次數n次以上
			if($count < $table['table']['record'][0]['value']) {
				$r['err'] = 17;
				$r['value'] = $table['table']['record'][0]['value'];
				$r['retCode']=-100217;
				$r['retMsg']='未符合老手資格!!';
			}
		}
		return $r;
	}
	
	//得標次數限制
	public function pay_get_product_count($userid) {
		global $db, $config;
		
		$query = "SELECT count(*) as count 
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`  
		where 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND userid = '{$userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		return $table['table']['record'][0]['count'];
	}
	
	// 連續下標
	public function chk_range($product) {
		$time_start = microtime(true);
		
		global $db, $config;
		
		$r['err'] = '';
		
		if(empty($_POST['price_start']) || empty($_POST['price_stop'])) {
		    //'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($_POST['price_start']) || !is_numeric($_POST['price_stop'])) {
		    //'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((int)$_POST['price_start'] < (int)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif($product['offtime'] > 0 && ($product['offtime'] - $product['now'] < $this->range_saja_time_limit)) {
		    //'超過可連續下標時間'
			$r['err'] = 7;
			$r['retCode']=-100227;
			$r['retMsg']='超過可連續下標時間!';
		} elseif((int)$_POST['price_start'] > (int)$_POST['price_stop']) {
		    //'起始價格不可超過結束價格'
			$r['err'] = 9;
			$r['retCode']=-100229;
			$r['retMsg']='起始價格不可超過結束價格!';
		} elseif((int)$_POST['price_stop'] > (int)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif(((int)$product['user_limit'] > 0) && ($this->chk_user_limit($product['user_limit'], $product['productid'])) ) {
		    //'超過可下標次數限制'
			$r['err'] = 11;
			$r['retCode']=-100231;
			$r['retMsg']='超過可下標次數限制!';
		} elseif((int)$_POST['price_start'] == (int)$_POST['price_stop']) {
		    //'起始價格不可超過結束價格'
			$r['err'] = 12;
			$r['retCode']=-100232;
			$r['retMsg']='出價起訖區間不可相同!';
		}

        //本次下標數
		$this->tickets = ((int)$_POST['price_stop'] - (int)$_POST['price_start']) / $this->currency_unit + 1;
		error_log("[ajax/product] 本次下標數：".$this->tickets);
		//起始價格
		$this->price = $_POST['price_start'];
		
		if($this->tickets > (int)$product['usereach_limit']) {
		    //'超過可連續下標次數限制'
			$r['err'] = 8;
			$r['retCode']=-100228;
			$r['retMsg']='超過可連續下標次數限制!';
		}
		return $r;
	}
	
	// 單次下標
	public function chk_single($product) {
		$time_start = microtime(true);
		
		global $db, $config;
		
		//殺價記錄數量
		$this->tickets = 1;
		
		//殺價起始價格
		$this->price = $_POST['price'];
		
		$r['err'] = '';
		
		if(empty($this->price)) {
		    //'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($this->price)) {
		    //'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((float)$this->price < (float)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif((float)$this->price > (float)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid']) ) {
		    //'超過可下標次數限制'
			$r['err'] = 11;
			$r['retCode']=-100231;
			$r['retMsg']='超過可下標次數限制!';
		}

		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "單次下標 $time 秒<br>";
		return $r;
	}
	
	//檢查使用者下標次數限制
	public function chk_user_limit($user_limit, $pid) {
		$time_start = microtime(true);
		
		global $db, $config;
	
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND productid = '{$pid}'
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		$tickets_avaiable = (int)$user_limit - (int)$table['table']['record'][0]['count'];

		if((int)$this->tickets > $tickets_avaiable) {
			return 1;
		}
		return 0;
	}
	
	//檢查最後下標時間
	public function chk_time_limit() {
		$time_start = microtime(true);
		
		global $db, $config;
		
		$query = "
		SELECT unix_timestamp(MAX(insertt)) as insertt, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			if( (int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $this->saja_time_limit) {
				return 1;
			}
		}
		return 0;
	}
	
	//得標者資訊
	public function getBidWinner($productid) {
		global $db, $config;
		// 得標者
		$ret='';
		$query="SELECT h.userid, h.productid, h.nickname, h.price, h.src_ip ".
			  ",(SELECT name from `{$config['db'][2]["dbname"]}`.`{$config['default_prefix']}province` WHERE switch='Y' AND provinceid=up.provinceid LIMIT 1) as src_province ".
			  ",(SELECT name from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` WHERE switch = 'Y' AND provinceid=up.provinceid) as comefrom ".
			  ",(SELECT name from `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}product` WHERE productid=h.productid LIMIT 1) as prodname ".
			  ",(SELECT uid from `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}sso` WHERE ssoid=(SELECT ssoid FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sso_rt` WHERE userid=h.userid and switch='Y' LIMIT 1) and switch='Y' and name='weixin' LIMIT 1) as openid ".
			  " FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h ".
			  " JOIN `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_profile` up ON h.userid=up.userid ".
			  " WHERE (h.productid, h.price) =  ".
			  "       (SELECT productid, min_price FROM  `{$config['db'][4]["dbname"]}`.`v_min_unique_price` WHERE productid='{$productid}' LIMIT 1) ";
		// error_log("[ajax/product/getBidWinner] : ".$query);

		$table = $db->getQueryRecord($query);
		if($table['table']['record']) {
			$ret=array();
			$ret['userid'] = $table['table']['record'][0]['userid']; 
			$ret['nickname'] = $table['table']['record'][0]['nickname'];
			$ret['productid'] = $table['table']['record'][0]['productid']; 
			$ret['prodname'] = $table['table']['record'][0]['prodname']; 
			$ret['src_province'] = $table['table']['record'][0]['src_province'];  			  
			$ret['price'] = $table['table']['record'][0]['price'];			  
			$ret['openid'] = $table['table']['record'][0]['openid'];
			$ret['src_ip'] = $table['table']['record'][0]['src_ip'];
			$ret['comefrom'] = $table['table']['record'][0]['comefrom'];			  
		}
		error_log("[ajax/product/getBidWinner] : ".json_encode($ret));
		return $ret;		   
	}
	
	//產生下標歷史記錄
	public function mk_history($product) {
		$time_start = microtime(true);
		
		global $db, $config, $jpush;
		
		// $jp = new jpush;
		
		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = '';
		$values = array();
		$ip = GetIP();
		if($_POST['use_sgift'] == 'Y') {
			/****************************
			* 使用禮券
			****************************/
			
			//檢查禮券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}sgift`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}' 
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
			ORDER BY seq
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$sgift = 0;
			} else {
				$sgift = count($table['table']['record']); //禮券數量
			}

			if($this->tickets > $sgift) {
				//'禮券不足'
				$r['err'] = 15;
			} else {
				//使用禮券
				for($i = 0; $i < round($this->tickets); $i++) 
				{
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}sgift` 
					SET 
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND sgiftid = '{$table['table']['record'][$i]['sgiftid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);
					
					//殺價下標歷史記錄（對應下標記錄與下標禮券id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'0',
						'{$table['table']['record'][$i]['sgiftid']}',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						NOW(),
						'{$ip}'
					)";
				}
				
				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retMsg']='下標失敗!';
				}
			}
		} elseif($_POST['pay_type'] == 'oscode') {
			/****************************
			* 使用殺價券 : 為針對某特定商品免費的「下標次數」， 1張殺價券可以下標乙次
			****************************/
			
			//檢查殺價券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`  
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}' 
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
			    ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']); 
			}
			error_log("[ajax/product/mk_history]: oscode needed ==>".round($this->tickets));
			error_log("[ajax/product/mk_history]: oscode available : ".$query);
			
			if ($osq > $_POST['oscode_num']) {
				$osq = $_POST['oscode_num']; 
			} 
			error_log("[ajax/product/mk_history]: osq  : ".$osq);
			
			if(round($this->tickets) > $osq) {
				
					/****************************
					* 使用殺幣
					****************************/
					$scode_fee = 0;
					$needspoint = 0;
					for($i = 0 ; $i < round($this->tickets-$osq); $i++) 
					{
						$needspoint = $needspoint + ($_POST['price_start']+$i);
					}
					
					$saja_fee_amount= getBidTotalFee($product,
													 $_POST['type'],
													 $_POST['price'],
													 $_POST['price_start'],
													 $_POST['price_stop'],
													 $this->currency_unit);
					
					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$needspoint = $needspoint+($product['saja_fee']*round($this->tickets-$_POST['oscode_num']));break;
						case 'B':$needspoint = $needspoint;break;			  
						case 'O':$needspoint = 0;break;
						case 'F':$needspoint = $product['saja_fee']*round($this->tickets-$osq);break;
						default:$needspoint = $product['saja_fee']*round($this->tickets-$osq);break;			  
					}

					error_log("[ajax/product/mk_history]: needspoint : ".$needspoint);

					// 檢查殺幣餘額
					$query = "SELECT SUM(amount) as amount
								FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
							   WHERE userid = '{$this->userid}'
								 AND prefixid = '{$config['default_prefix_id']}' 
								 AND switch = 'Y'
					";			
					$spointtable = $db->getQueryRecord($query);
					error_log("[ajax/product/mk_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

					if($needspoint > (float)$spointtable['table']['record'][0]['amount']) {
						//'殺價幣不足'
						$r['err'] = 13;
						$r['retCode']=-100193;
						$r['retMsg']='殺價幣不足!';
					} else {
						
						//使用殺幣
						$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
						SET
							`prefixid`='{$config['default_prefix_id']}',
							`userid`='{$this->userid}',
							`countryid`='{$this->countryid}',
							`behav`='user_saja',
							`amount`='".- $needspoint."',
							`insertt`=NOW()
						";
						
						$res = $db->query($query);
						$spointid = $db->_con->insert_id;
						error_log("[ajax/product/mk_history]: spointid : ".$spointid);

						//使用殺價券
						for($i = 0 ; $i < round($osq); $i++) {						
							$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode` 
							SET 
								used = 'Y',
								used_time = NOW()
							WHERE
								`prefixid` = '{$config['default_prefix_id']}'
								AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
								AND switch = 'Y'
							";
							$res = $db->query($query);
							
							error_log("[ajax/product/mk_history]: oscode res ".$i." : ".$res);								
						}
						
						//使用殺價幣
						for($i = 0 ; $i < round($this->tickets-$osq); $i++) {
							//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'{$this->userid}',
								'{$this->username}',
								'{$spointid}',
								'0',
								'0',
								'0',
								'".($this->price + $i * $this->currency_unit)."',
								NOW(),
								'{$ip}'
							)";	
						}
						
						$k=0;
						//使用殺價券
						for($i = $this->tickets-$osq; $i < round($this->tickets); $i++) {
							//殺價下標歷史記錄（對應下標記錄與 oscodeid）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'{$this->userid}',
								'{$this->username}',
								'0',
								'0',
								'0',
								'{$table['table']['record'][$k]['oscodeid']}',
								'".($this->price + $i * $this->currency_unit)."',
								NOW(),
								'{$ip}'
							)";
							$k++;
							error_log("[ajax/product/mk_history]: oscode values : ".$table['table']['record'][($k-1)]['oscodeid']);

						}
						
						if(empty($values)) {
							//'下標失敗'
							$r['err'] = -1;
							$r['retCode']=-1;
							$r['retMsg']='下標失敗!';
						}
					}				

			} else {
				//使用殺價券
				for($i = 0; $i < round($this->tickets); $i++) {
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode` 
					SET 						
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);
					
					//殺價下標歷史記錄（對應下標記錄與 oscodeid）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'0',
						'0',
						'{$table['table']['record'][$i]['oscodeid']}',
						'".($this->price + $i * $this->currency_unit)."',
						NOW(),
						'{$ip}'
					)";
				}
				
				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retCode']=-1;
					$r['retMsg']='下標失敗!';
				}
			}
		} else if($_POST['pay_type'] == 'scode') {
			/****************************
			* 使用S碼 : 為免費的「下標次數」，1組S碼可以下標乙次
			* 有效時間為30天(發送當天開始算30天)，逾期失效不補回。
			****************************/
			
			$scodeModel = new ScodeModel;
			$scode = $scodeModel->get_scode($this->userid);
			
			if(empty($scode)) {
				$sq = 0;
			} else {
				$sq = $scode; 
			}
			error_log("[ajax/product/mk_history]: scode needed ==>".round($this->tickets));
			
			if($sq > $_POST['scode_num']) {
				$sq = $_POST['scode_num']; 
			} 
			error_log("[ajax/product/mk_history]: sq  : ".$sq);
			
			
			//檢查S碼數量
			if(round($this->tickets) > $sq) {
				//'S碼不足'
				$r['err'] = 19;
				$r['retCode']=-100199;
				$r['retMsg']='超級殺價券不足!';
			} else {
				for($i = 0 ; $i < round($this->tickets); $i++) {	
					//確認S碼
					$query = "select scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode` 
						where 
						`prefixid`='{$config['default_prefix_id']}' 
						and `userid` = '{$this->userid}' 
						and `behav` != 'a' 
						and `closed` = 'N'
						and `remainder` > 0
						and `switch` = 'Y'
						order by insertt asc
						limit 0, 1
					";
					$table = $db->getQueryRecord($query);
		
					//使用S碼
					$query = "update `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode` 
					SET
						`remainder`=`remainder` - 1,
						`modifyt`=NOW()
					where
						`prefixid`='{$config['default_prefix_id']}' 
						and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
						and `userid` = '{$this->userid}' 
						and `behav` != 'a' 
						and `closed` = 'N' 
						and `remainder` > 0
						and `switch` = 'Y'
					";
					$res = $db->query($query);
				}
				
				// 新增扣除組數紀錄
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`behav`='a',
					`productid`='{$product['productid']}',
					`amount`='". - $this->tickets ."',
					`remainder`='0',
					`insertt`=NOW()
				";
				$res = $db->query($query);
				$scodeid = $db->_con->insert_id;
				for($i = 0 ; $i < round($this->tickets); $i++) {
					//殺價下標歷史記錄（對應下標記錄與使用S碼id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'{$scodeid}',
						'0',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						NOW(),
						'{$ip}'
					)";
				}
				
				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retCode']=-1;
					$r['retMsg']='下標失敗!';
				}
			}
		} else {
			/****************************
			* 使用殺幣
			****************************/
			$scode_fee = 0;
			
			//檢查殺幣餘額
			$query = "SELECT SUM(amount) as amount
			            FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
			           WHERE userid = '{$this->userid}'
				         AND prefixid = '{$config['default_prefix_id']}' 
				         AND switch = 'Y'
			";			
			$table = $db->getQueryRecord($query);

			$saja_fee_amount= getBidTotalFee($product,
			                                 $_POST['type'],
											 $_POST['price'],
											 $_POST['price_start'],
											 $_POST['price_stop'],
											 $this->currency_unit);
			error_log("[ajax/product/mk_history] saja_fee_amount <==> table amount : ".$saja_fee_amount."<==>".(float)$table['table']['record'][0]['amount']);
			if($saja_fee_amount > (float)$table['table']['record'][0]['amount']) {
				//'殺價幣不足'
				$r['err'] = 13;
				$r['retCode']=-100193;
				$r['retMsg']='殺價幣不足!';
			} else {
				//使用殺幣
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`countryid`='{$this->countryid}',
					`behav`='user_saja',
					`amount`='". - $saja_fee_amount ."',
					`insertt`=NOW()
				";
				
				$res = $db->query($query);
				$spointid = $db->_con->insert_id;
				
				for($i = 0 ; $i < round($this->tickets); $i++) 
				{
					//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'{$spointid}',
						'0',
						'0',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						NOW(),
						'{$ip}'
					)";
					
				}
				
				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retCode']=-1;
				    $r['retMsg']='下標失敗!';
				}
			}
		}
		
		
		if(empty($r['err']) && !empty($values) ) {
			// 取得目前winner
			$arr_ori_winner=array();
			$arr_ori_winner=$this->getBidWinner($product['productid']);
			if(!empty($arr_ori_winner)) {
			   error_log("[ajax/product/mk_history]: ori Winner of ".$arr_ori_winner['prodname']." is :".$arr_ori_winner['nickname']);
			} else {
			   error_log("[ajax/product/mk_history]: ori Winner is Empty !");
			}
			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
			(
				prefixid,
				productid,
				userid,
				nickname,
				spointid,
				scodeid,
				sgiftid,
				oscodeid,
				price,
				insertt,
				src_ip		
			)
			VALUES
			".implode(',', $values)."
			";
			
			$res = $db->query($query);
			error_log("[ajax/product]:".$query."-->".$res);
			$r['rank'] = $last_rank;
			
			// 非閃殺商品下標, 結標一分鐘以前, 單次下標將提示順位
			if($_POST['type'] == 'single' && $product['is_flash']!='Y') {
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
					// 檢查重複
					$query= "SELECT count(userid) as cnt 
							   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
							  WHERE productid='{$product['productid']}' 
								AND price={$_POST['price']} 
								AND switch='Y' ";
					
					$table = $db->getQueryRecord($query);
					$dup_cnt=$table['table']['record'][0]['cnt'];
					if($dup_cnt>1) {
						// 重複出價
						$r['rank']=-1;
					} else if($dup_cnt<=1) {
						// 無重複出價
						$query1 = "SELECT count(*) as cnt FROM  `{$config['db'][4]["dbname"]}`.`v_unique_price` 
								   WHERE productid='{$product['productid']}' AND price<{$_POST['price']} ";
						$table1 = $db->getQueryRecord($query1);	
						$r['rank'] =$table1['table']['record'][0]['cnt'];
						error_log("[ajax/product]rank : ".$r['rank']);
					}
				} else {
				    // 不提示
					$r['total_num']=-1;    
				}
			}  
			
			// 非閃殺商品下標, 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出價個數, 及該次下標的唯一最低價
			if($product['is_flash']!='Y') {
				if($_POST['type'] == 'range') {
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						$r['total_num']=$this->tickets;
						// 顯示提示
						// 取得該區間內唯一出價的個數
						if($this->tickets>1) {
							$query="SELECT * FROM saja_shop.saja_history sh JOIN saja_shop.v_unique_price up 
									   ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y' 
									WHERE sh.switch='Y'  
									  AND sh.productid='{$product['productid']}' 
									  AND sh.price between {$_POST['price_start']} AND {$_POST['price_stop']}
									  AND sh.userid='{$this->userid}' ORDER BY sh.price asc ";
									  
							// error_log("[ajax/product]range chk: ".$query);
							$table = $db->getQueryRecord($query);
							error_log("[ajax/product]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);	
							
							if(empty($table['table']['record'])) {
							   // 無唯一出價
							   $r['num_uniprice'] = 0;	
							   $r['lowest_uniprice'] = 0;					   
							} else {
							   // 有唯一出價
							   $r['num_uniprice'] = count($table['table']['record']);	
							   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
							}
						} else {
							// 不提示
							$r['total_num']=-1;
						}
					} else {
						// 不提示
						$r['total_num']=-1;
					}
				}
			}
			
			$arr_new_winner=array();
			try {
				$arr_new_winner=$this->getBidWinner($product['productid']);
				if(is_array($arr_ip)) {
					$ip2=strlen($arr_ip[1]);
					$arr_ip[1]='';
					for($i=0;$i<$ip2;++$i) {
						$arr_ip[1].='*';        
					}

					$ip3=strlen($arr_ip[2]);
					$arr_ip[2]='';
					for($i=0;$i<$ip3;++$i) {
						$arr_ip[2].='*';        
					}
					$arr_new_winner['src_ip']=implode(".",$arr_ip);
				}
				
				error_log("[ajax/product/mk_history] new_winner :".json_encode($arr_new_winner));
				$r['winner']=$arr_new_winner['nickname'];
				if(empty($r['winner'])) {
				   $r['winner']="_無人中標_";
				}	
				$sjq['winner'] = $r['winner'];
				$sjq['src_ip'] = $arr_new_winner['src_ip'];
				$sjq['comefrom'] = $arr_new_winner['comefrom'];
			} catch(Exception $e) {
			   error_log("[ajax/product] exception : ".$e->getMessage());
			}
		}
		error_log("[ajax/mk_history] r : ".json_encode($r));
		return $r;
	}

}
?>