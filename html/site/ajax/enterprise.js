// JavaScript Document

/* login.php */
function enterprise_login() {
	var pageid = $.mobile.activePage.attr('id');
	var ologinname = $.trim($('#'+pageid+' #loginname').val());
	var opw = $('#'+pageid+' #pw').val();

	if(ologinname=="" && opw=="") {
		alert('商家帳號及密碼不能空白');
	}
	else
	{
		$.post(APP_DIR+"/ajax/enterprise.php?t="+getNowTime(),
		{type:'enterprise_login', loginname:ologinname, passwd:opw},
		function(str_json)
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));

				if (json.status < 1) {
					alert('登入失敗');
				}
				else {
					var msg = '';
					if (json.status==1){
						alert('殺價王商家歡迎您!');
						location.href = APP_DIR+'/enterprise/home';
					}
					else if (json.status==9){
						location.reload();
					}
					else if(json.status==2){ alert('請填寫商家帳號'); }
					else if(json.status==3){ alert('商家帳號不存在'); }
					else if(json.status==4){ alert('請填寫密碼'); }
					else if(json.status==5){ alert('密碼不正確'); }
				}
			}
		});
	}
}

function userenterprise() {

	var pageid = $.mobile.activePage.attr('id');
	//企業營業資料
	var oenterpriseid = $('#'+pageid+' #enterpriseid').val();

	var ocontact_phone = $('#'+pageid+' #contact_phone').val();
	var ocontact_mobile = $('#'+pageid+' #contact_mobile').val();
	var ourl = $('#'+pageid+' #url').val();
	var ocontact_email = $('#'+pageid+' #contact_email').val();
	var obusinessontime = $('#'+pageid+' #businessontime').val();
	var obusinessofftime = $('#'+pageid+' #businessofftime').val();
	var oservice_email = $('#'+pageid+' #service_email').val();

	//企業詳細資料

	var oemail = $('#'+pageid+' #email').val();
	var opic = $('#'+pageid+' #pic').val();
	var opicurl = $('#'+pageid+' #picurl').val();
	//企業帳戶資料
	if(ocontact_phone != "" && ocontact_mobile != "" ) {



		$.post(APP_DIR+"/ajax/enterprise.php?t="+getNowTime(),
		{type:'enterpriseupdate',  contact_phone:ocontact_phone, contact_mobile:ocontact_mobile, profit_ratio:oprofit_ratio, url:ourl, contact_email:ocontact_email, businessontime:obusinessontime,
		businessofftime:obusinessofftime, service_email:oservice_email,  enterpriseid:oenterpriseid ,spic:ospic},
		function(str_json)
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));

				if (json.status==0) {
					alert('商家請先登入!!');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status==-1) {
					alert('商家資料修改失敗!!');
				}
				else {
					var msg = '';
					if (json.status==1){
						alert('商家資料修改完成');
						location.href=APP_DIR+"/enterprise/member_update/";
					}
					else if(json.status<=-2){ alert(json.kind + '!!'); }
				}
			}
		});

	} else {
		alert('請將必填資料填寫完整，不能空白');
	}
}

function userenterprisepw() {

	var pageid = $.mobile.activePage.attr('id');
	//企業帳密相關
	var oenterpriseid = $('#'+pageid+' #enterpriseid').val();
	var opasswd = $('#'+pageid+' #passwd').val();
	var ocheckpasswd = $('#'+pageid+' #confrim_passwd').val();

	if(opasswd != "" && ocheckpasswd != "") {

		if(opasswd !== ocheckpasswd) {
			alert('登入密碼與再次確認密碼確認不同!!');
		} else {
			$.post(APP_DIR+"/ajax/enterprise.php?t="+getNowTime(),
			{type:'enterprisepwupdate', passwd:opasswd, checkpasswd:ocheckpasswd, enterpriseid:oenterpriseid},
			function(str_json)
			{
				if(str_json)
				{
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));

					if (json.status==0) {
						alert('商家請先登入');
						$('#'+pageid+' #'+rightid).panel("open");
					}
					else if (json.status==-1) {
						alert('修改商家登入密碼失敗');
					}
					else {
						var msg = '';
						if (json.status==1){
							alert('修改商家登入密碼完成');
							location.href=APP_DIR+"/enterprise/member_update/";
						}
						else if(json.status<=-2){ alert(json.kind + '!!'); }
					}
				}
			});
		}
	} else {
		alert('登入密碼欄位請填寫完整，不能空白');
	}
}


function userenterpriseexpw() {

	var pageid = $.mobile.activePage.attr('id');
	//企業帳密相關
	var oenterpriseid = $('#'+pageid+' #enterpriseid').val();
	var oexpasswd = $('#'+pageid+' #expasswd').val();
	var oexcheckpasswd = $('#'+pageid+' #confrim_expasswd').val();


	if(oexpasswd != "" && oexcheckpasswd != "") {

		if(oexpasswd !== oexcheckpasswd) {
			alert('提现密碼與確認提现密碼不同!!');
		} else {
			$.post(APP_DIR+"/ajax/enterprise.php?t="+getNowTime(),
			{type:'enterpriseexpwupdate', expasswd:oexpasswd, excheckpasswd:oexcheckpasswd, enterpriseid:oenterpriseid},
			function(str_json)
			{
				if(str_json)
				{
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));

					if (json.status==0) {
						alert('商家請先登入 !!');
						$('#'+pageid+' #'+rightid).panel("open");
					}
					else if (json.status==-1) {
						alert('修改商家提现密碼失敗');
					}
					else {
						var msg = '';
						if (json.status==1){
							alert('修改商家提现密碼完成');
							location.href=APP_DIR+"/enterprise/member_update/";
						}
						else if(json.status<=-2){ alert(json.kind + '!!'); }
					}
				}
			});
		}
	} else {
		alert('提现密碼欄位請填寫完整，不能空白');
	}
}


function enterprise_cash() {

	//企業帳戶資料
	var pageid = $.mobile.activePage.attr('id');

	var oaccountname = $('#'+pageid+' #accountname').val();
	var oaccountnumber = $('#'+pageid+' #accountnumber').val();
	var obankname = $('#'+pageid+' #bankname').val();
	var obranchname = $('#'+pageid+' #branchname').val();
	var ocash = $('#'+pageid+' #cash').val();
	var oenterpriseid = $('#'+pageid+' #enterpriseid').val();
	var obonus = $('#'+pageid+' #bonus').val();
	var obankarea = $('#'+pageid+' #bankarea').val();

	if(ocash == "") {
		alert('提現金額不能為空白 !!');
		return;
	} else if (isNaN(Number(ocash))==true) {
		alert('提現金額必需為數字!');
		return;
	} else if (Number(ocash) > Number(obonus)) {
		alert('鯊魚點點數不足!');
		return;
	}

	if (oaccountname != "" && oaccountnumber != "" && obankname != "" && obranchname != "" && ocash != "" && oenterpriseid != "" && obankarea != "") {

		$.post(APP_DIR+"/ajax/enterprise.php?t="+getNowTime(),
		{type:'enterprisecash', cash:ocash, accountname:oaccountname, accountnumber:oaccountnumber, bankname:obankname, branchname:obranchname, bankarea:obankarea,
		enterpriseid:oenterpriseid},
		function(str_json)
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));

				if (json.status==0) {
					alert('商家請先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				}
				else if (json.status==-1) {
					alert('商家提現申請失敗');
				}
				else {
					var msg = '';
					if (json.status==1){
						alert('商家提現申請完成');
						location.href=APP_DIR+"/enterprise/";
					} else if(json.status<=-2){
						alert(json.kind + '!!');
					}
				}
			}
		});

	} else {
		alert('請將必填資料填寫完整，不能空白');
	}

}

function eppodduct_add() {

	var pageid = $.mobile.activePage.attr('id');
	var osname = $('#'+pageid+' #name').val();
	var oretail_price = $('#'+pageid+' #retail_price').val();
	var oontime = $('#'+pageid+' #ontime').val();
	var oofftime = $('#'+pageid+' #offtime').val();
	var oflash_loc = $('#'+pageid+' #flash_loc').val();
	var ois_flash = $('#'+pageid+' #is_flash').val();
	var obid_type = $('#'+pageid+' #bid_type').val();
	var ocontact_info = $('#'+pageid+' #contact_info').val();
	var opic = $('#'+pageid+' #pic').val();
	var ospic = $('#'+pageid+' #spic').val();
	var ostoreid = $('#'+pageid+' #storeid').val();

	if(osname != "" && oretail_price != "" && oontime != "" && oofftime != "" && oflash_loc != "" && ois_flash != "" &&  obid_type != "" && ocontact_info != "" && ostoreid != "") {

		var s1 = new Date(oontime);
		var s2 = new Date(oofftime);
		var s3 = s2.getTime() - s1.getTime();
		var s4 = new Date();
		var tianshu = s3 / (60*60*1000);

		if(isNaN(oretail_price)) {
			alert('巿價比必需為數字 !!');
			return false;
		}
		if (!oretail_price.match("^[0-9]*[1-9][0-9]*$")) {
			alert('巿價比最小單位為整數!');
			return false;
		}
		if(oontime == "") {
			alert('起標時間不能為空白!');
			return false;
		}
		if (oofftime == "") {
			alert('結標時間不能為空白!');
			return false;
		}
		if (s2.getTime() < s1.getTime()){
			alert('結標時間不能小於起標時間!');
			return false;
		}else if (s2.getTime() < s4.getTime()) {
			alert('結標時間不能小於當前時間!');
			return false;
		}

		if (oflash_loc == "") {
			alert('閃殺地點不能為空白!');
			return false;
		}
		if(ocontact_info == "") {
			alert('聯絡方式不能為空白 !!');
			return false;
		}

		$('#'+pageid+' #putdata').disabled = true;

		$.post(APP_DIR+"/ajax/enterprise.php?t="+getNowTime(),
		{type:'enterpriseproductadd', sname:osname, retail_price:oretail_price, ontime:oontime, offtime:oofftime, flash_loc:oflash_loc, is_flash:ois_flash, bid_type:obid_type, pic:ospic, spic:ospic, contact_info:ocontact_info, storeid:ostoreid},
		function(str_json)
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));

				if (json.status==0) {
					alert('商家請先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				} else if (json.status==-1) {
					alert('新增產品失敗');
				} else {
					var msg = '';
					if (json.status==1){
						alert('新增產品完成');
						location.href=APP_DIR+"/enterprise/storeproduct/";
					} else if(json.status==-2) {
						alert(json.kind + '!!');
					}

				}
			}
		});

	} else {
		alert('請將資料填寫完整，不能空白');
	}

}

function eppodduct_update() {

	var pageid = $.mobile.activePage.attr('id');
	var osname = $('#'+pageid+' #name').val();
	var oretail_price = $('#'+pageid+' #retail_price').val();
	var oontime = $('#'+pageid+' #ontime').val();
	var oofftime = $('#'+pageid+' #offtime').val();
	var oflash_loc = $('#'+pageid+' #flash_loc').val();
	var ocontact_info = $('#'+pageid+' #contact_info').val();
	var opic = $('#'+pageid+' #pic').val();
	var ospic = $('#'+pageid+' #spic').val();
	var oopic = $('#'+pageid+' #opic').val();
	var oproductid = $('#'+pageid+' #productid').val();

	if(osname != "" && oretail_price != "" && oontime != "" && oofftime != "" && oflash_loc != "" && ocontact_info != "" && oproductid != "") {

		var s1 = new Date(oontime);
		var s2 = new Date(oofftime);
		var s3 = s2.getTime() - s1.getTime();
		var s4 = new Date();
		var tianshu = s3 / (60*60*1000);

		if(isNaN(oretail_price)) {
			alert('巿價比必需為數字 !!');
			return false;
		}
		if(!oretail_price.match("^[0-9]*[1-9][0-9]*$")) {
			alert('巿價比最小單位為整數!');
			return false;
		}
		if(oontime == "") {
			alert('起標時間不能為空白!');
			return false;
		}
		if(oofftime == "") {
			alert('結標時間不能為空白!');
			return false;
		}
		if(s2.getTime() < s1.getTime()) {
			alert('結標時間不能小於起標時間!');
			return false;
		} else if (s2.getTime() < s4.getTime()) {
			alert('結標時間不能小於當前時間!');
			return false;
		}
		if(oflash_loc == "") {
			alert('閃殺地點不能為空白!');
			return false;
		}
		if(ocontact_info == "") {
			alert('聯絡方式不能為空白 !!');
			return false;
		}

		$('#'+pageid+' #putdata').disabled = true;

		$.post(APP_DIR+"/ajax/enterprise.php?t="+getNowTime(),
		{type:'enterpriseproductupdate', sname:osname, retail_price:oretail_price, ontime:oontime, offtime:oofftime, flash_loc:oflash_loc, pic:ospic, spic:ospic, opic:oopic, contact_info:ocontact_info, productid:oproductid},
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));

				if (json.status==0) {
					alert('商家請先登入');
					$('#'+pageid+' #'+rightid).panel("open");
				} else if (json.status==-1) {
					alert('修改產品失敗');
				} else {
					var msg = '';
					if (json.status==1) {
						alert('修改產品完成');
						location.href=APP_DIR+"/enterprise/storeproduct/";
					} else if(json.status==-2) {
						alert(json.kind + '!!');
					}

				}
			}
		});

	} else {
		alert('請將資料填寫完整，不能空白');
	}

}
