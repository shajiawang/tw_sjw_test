<?php
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
	foreach($_POST2 as $k=> $v) {
		// error_log($k."=>".$v);
		$_POST[$k]=$v;
		if($_POST['client']['json']) {
			$_POST['json']=$_POST['client']['json'];
		}
		if($_POST['client']['auth_id']) {
			$_POST['userid']=$_POST['client']['auth_id'];
		}
	}
}

session_start();
ini_set('display_errors','0');
// error_reporting(E_ALL);
// 1. 要接收 $json,
$ret=null;
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json'];

error_log("[ajax/bid] POST : ".json_encode($_POST));
error_log("[ajax/bid] GET : ".json_encode($_GET));
if(empty($_SESSION['auth_id']) && empty($_POST['auth_id'])) { //'請先登入会員帳号'

	if($json=='Y') {
		$ret['retCode']='-1';
		$ret['retMsg']='請先登入會員 !!';
	} else {
		$ret['retCode']='-1';
		$ret['retMsg']='請先登入會員 !!';
		$ret['status'] = '-1';
	}

	echo json_encode($ret);
	exit;
} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");
	include_once(LIB_DIR ."/ini.php");
	//$app = new AppIni;

    
	$c = new Bid;
	$c->home();
}


class Bid {
	public $userid = '';

	//中標结帳
	public function home() {
		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$router = new Router();
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json'];
		$this->userid = (empty($_SESSION['auth_id']) ) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		$pgpid = (empty($_POST['pgpid']) ) ? $_GET['pgpid'] : $_POST['pgpid'];
		$name = (empty($_POST['name']) ) ? $_GET['name'] : $_POST['name'];
		if(empty($name)) {
			$name = (empty($_POST['addressee']) ) ? $_GET['addressee'] : $_POST['addressee'];
		}
		$zip = (empty($_POST['zip']) ) ? $_GET['zip'] : $_POST['zip'];
		$address = (empty($_POST['address']) ) ? $_GET['address'] : $_POST['address'];
		$phone = (empty($_POST['phone']) ) ? $_GET['phone'] : $_POST['phone'];

		$ret = getRetJSONArray(1,'OK','MSG');
		

		$ret['status'] = '';

		// Check Variable Start

		if(empty($pgpid)) {
			$ret['status'] = '-8';
		} elseif(empty($name)) {
			//('收件人姓名錯誤!!');
			$ret['status'] = '-2';
		} elseif(empty($zip)) {
			//('收件人邮编錯誤!!');
			$ret['status'] = '-3';
		} elseif(empty($address)) {
			//('收件人地址錯誤!!');
			$ret['status'] = '-4';
		} elseif(empty($phone)) {
			//('收件人電話錯誤!!');
			$ret['status'] = '-7';
		} else {
			$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now`, pgp.userid, pgp.pgpid, pgp.price, pgp.complete
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
				pgp.prefixid = p.prefixid
				AND pgp.productid = p.productid
				AND p.switch = 'Y'
			WHERE
				pgp.`prefixid` = '{$config['default_prefix_id']}'
				AND pgp.pgpid  = '{$pgpid}'
				AND pgp.userid = '{$this->userid}'
				AND pgp.switch = 'Y'
			" ;
			$table = $db->getQueryRecord($query);

			$product = $table['table']['record'][0];
			$product['price'] = round($product['price'] * $config['sjb_rate']);
			$product['real_process_fee'] = round($product['retail_price'] * $product['process_fee']/100 * $config['sjb_rate'])+ $product['checkout_money'];
			$product['total'] = $product['real_process_fee'] + $product['price'] ;

			if ($product['checkout_type'] == 'pf') {
				$product['total'] = $product['real_process_fee'] ;
			} elseif($product['checkout_type'] == 'bid') {
				$product['total'] = $product['price'];
			} else {
				$product['total'] = $product['real_process_fee'] + $product['price'] ;
			}

			if ($product['is_discount']=='Y') {
				// 計算折抵金額 justice_lee 20190603
				$productid = $product['productid'];
				$query ="SELECT sum(price) saja_total_price, count(*) saja_cnt
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
					WHERE productid = '{$productid}'
					AND userid = '{$this->userid}'
					AND spointid > 0
					group by userid,productid";
				$table = $db->getQueryRecord($query);
				$discount = $table['table']['record'][0];

				// 計算折抵金額
				switch ($product['totalfee_type']) {
					case 'A'://手續費+出價金額
						$product['discount'] = 0;
						$product['discount'] += (int)$discount['saja_total_price'];
						$product['discount'] += (int)$product['saja_fee']*(int)$discount['saja_cnt'];
						break;
					case 'B'://出價金額
						$product['discount'] = (int)$discount['saja_total_price'];
						break;
					case 'F'://手續費
						$product['discount'] = (int)$product['saja_fee']*(int)$discount['saja_cnt'];
						break;
					default://免費
						$product['discount'] = 0;
						break;
				}
				$product['total'] -= $product['discount'];
				$product['total'] = ($product['total']<0)? 0 : $product['total'];
			}else{
				$product['discount'] = 0;
			}

			$query = "SELECT sum(amount) as total_amount
			from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint`
			where
				userid = '{$this->userid}'
				and switch = 'Y'
			GROUP BY userid
			";
			$table = $db->getQueryRecord($query);

			if ($table['table']['record'][0]['total_amount'] < $product['total']) {
				// 餘額不足
				$ret['status'] = '-6';
			}

			if($product['complete']=='Y') {
				//'已結帳'
				$ret['status'] = '-5';
			}

			if (!$this->check_limit($product)) {
				// 不符合限定商品資格
				$ret['status'] = '-10';
			}


		}

		if(empty($ret['status'])) {
/* 			// 開立折讓
            $allowanceData = $this->mkSalesAllowance($product['userid'], $product['total']);

            // 開立商品購買費用發票
            $prodItem=array();
            $prodItem[0]=array("seq_no"=>1, "quantity"=>1, "description"=>$product['name'], "amount"=>$product['price'],"utype"=>"B" );
            $prodInvData = $this->mkInvoice($product['userid'], $product['price'], "", $prodItem, "Y");
            error_log("[ajax/bid] Product Invoice : ".json_encode($prodInvData));

            // 開立得標處理費發票
            if($product['real_process_fee']>0) {
                $feeItem=array();
                $feeItem[0]=array("seq_no"=>1, "quantity"=>1, "description"=>"商品:".$product['name']."得標處理費", "amount"=>$product['real_process_fee'],"utype"=>"B" );
                $feeInvData = $this->mkInvoice($product['userid'], $product['real_process_fee'], "", $feeItem, "Y");
                error_log("[ajax/bid] Fee Invoice : ".json_encode($feeInvData));
            } */
            //產生訂單記錄
			$mk = $this->mk_order($product);

			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 1;
			
			//關閉用戶下標功能 AARONFU
			//$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` SET
			//		`bid_enable` = 'N', `dream_enable` = 'N'
			//		WHERE `userid` = '{$this->userid}'";
			//$res = $db->query($query);
		}

		switch($ret['status']) {
			case '0':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "結帳失敗";
				break;
			case '-1':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "殺友請先登入";
				break;
			case '-2':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "請填寫收件人姓名 !!";
				break;
			case '-3':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "請填寫收件人郵編";
				break;
			case '-4':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "請填寫收件人地址";
				break;
			case '-5':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "已完成結帳";
				break;
			case '-6':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "殺價幣餘額不足，請先充值後再結帳";
				break;
			case '-7':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "請填寫收件人電話";
				break;
			case '-8':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "資料有誤，無法結帳";
				break;
			case '-9':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "商品卡庫存不足";
				break;
			case '-10':
				$ret['retCode'] = $ret['status']*-1;
				$ret['retMsg'] = "不符合限定商品資格";
				break;
			case '1':
				$ret['retCode'] = $ret['status'];
				$ret['retMsg'] = "結帳完成";
				// Add By Thomas 2020/01/07 結帳成功的提醒訊息
				$action_list=array();
				if($product['ordertype']=="2" && $product['orderbonus']>0) {
				   $action_list['type']	="5";
                   $action_list['page']="21";	
                   $action_list['msg']="結帳完成 !\n等值鯊魚點 ".$product['orderbonus']." 點 已匯入您的殺價王帳戶, 請至鯊魚點明細查看!\n您可至7-11超商以ibon印出現金券購買該商品 ~";
                   // $action_list['msg']="結帳完成2 !";				   
				} else {
                   $action_list['type']="5";
                   $action_list['page']="5";	
                   $action_list['msg']='結帳完成 !';
				}
                $ret['action_list']=$action_list;
				break;
		}

		error_log("[ajax/bid/home] ret : ".json_encode($ret));
		echo trim(json_encode($ret));
	}


	//產生訂單記錄
	public function mk_order($product) {

		$time_start = microtime(true);

		global $db, $config,$oscode;

		$r['err'] = '';
		$this->userid = (empty($_SESSION['auth_id']) ) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		$pgpid=empty($_GET["pgpid"])?$_POST['pgpid']:$_GET['pgpid'];
		$name = (empty($_POST['name']) ) ? $_GET['name'] : $_POST['name'];
		if(empty($name)) {
		   $name = (empty($_POST['addressee']) ) ? $_GET['addressee'] : $_POST['addressee'];
		}
		$zip = (empty($_POST['zip']) ) ? $_GET['zip'] : $_POST['zip'];
		$address = (empty($_POST['address']) ) ? $_GET['address'] : $_POST['address'];
		$phone = (empty($_POST['phone']) ) ? $_GET['phone'] : $_POST['phone'];

		if(empty($r['err'])) {
			//新增訂單
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order`
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`status`='0',
				`pgpid`='{$pgpid}',
				`type`='saja',
				`num`='1',
				`point_price`='{$product['price']}',
				`process_fee`='{$product['real_process_fee']}',
				`total_fee`='{$product['total']}',
				`discount_fee`='{$product['discount']}',
				`totalfee_type`='{$product['totalfee_type']}',
				`checkout_type`='{$product['checkout_type']}',
				`confirm`='Y',
				`insertt`=NOW()
			";
			$res = $db->query($query);
			$orderid = $db->_con->insert_id;

			if($product['is_exchange'] == 1){
				//取兌換商品資訊
				$query ="SELECT *
				FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product`
				WHERE
					epid = '{$product['epid']}'
					AND switch = 'Y'
				";

				$ep_info = $db->getQueryRecord($query);

				//無序號商品卡
				if($ep_info['table']['record'][0]['eptype'] == 4){
					$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` set
					`epid` = '{$product['epid']}'
					WHERE `orderid` = '{$orderid}'";
					$res = $db->query($query);
				}else{  //有序號商品卡
					//取商品卡資訊
					$query ="SELECT cid
					FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card`
					WHERE
						epid = '{$product['epid']}'
						AND userid = '0'
						AND orderid = '0'
						AND used = 'N'
						AND switch = 'Y'
						ORDER BY cid ASC,insertt ASC
						LIMIT {$ep_info['table']['record'][0]['set_qty']}
					";

					$ec_info = $db->getQueryRecord($query);

					//商品卡庫存
					$card_num = count($ec_info['table']['record']);
					if($card_num >= $ep_info['table']['record'][0]['set_qty']){		//庫存足夠
						$cid_value = '';
						foreach($ec_info['table']['record'] as $row){
							$cid_value .= $row['cid'].',';
						}
						//將cid組合成字串
						$cid_value = rtrim($cid_value,',');

						$db->query('start transaction');
						$affected_num = 0;
						$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card`
						set
							`userid` = '{$this->userid}',
							`orderid` = '{$orderid}',
							`used` = 'Y'
						WHERE `cid` IN ({$cid_value}) AND `userid` = 0 AND `orderid` = 0 AND `used` = 'N'";

						$res = $db->query($query);
						//update的資料筆數
						$affected_num = $db->_con->affected_rows;

						if($affected_num == $ep_info['table']['record'][0]['set_qty']){		//更新比數與兌換數量符合
							$db->query('COMMIT');

							//商品卡訂單直接更新為已到貨狀態
							$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` set
							`status` = '4',
							`epid` = '{$product['epid']}'
							WHERE `orderid` = '{$orderid}'";
							$res = $db->query($query);

							//調整預留庫存
							if($ep_info['table']['record'][0]['reserved_stock'] > 0){
								$reserved_stock = $ep_info['table']['record'][0]['reserved_stock'] - $ep_info['table']['record'][0]['set_qty'];
								if($reserved_stock <= 0){
									$reserved_stock = 0;
								}
								$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` set
								`reserved_stock` = '{$reserved_stock}'
								WHERE `epid` = '{$product['epid']}'";
								$res = $db->query($query);
							}

						}else{											//更新比數與兌換數量不符合
							$db->query('ROLLBACK');

							//刪除訂單
							$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` set
							`switch` = 'N'
							WHERE `orderid` = '{$orderid}'";
							$res = $db->query($query);

							error_log("[ajax/bid/mk_order]:stock_not_enough，delete_order:".$orderid);
							$r['err'] = '-9';

							return $r;
						}
					}else{     //庫存不足
						//刪除訂單
						$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` set
						`switch` = 'N'
						WHERE `orderid` = '{$orderid}'";
						$res = $db->query($query);

						error_log("[ajax/bid/mk_order]:stock_not_enough，delete_order:".$orderid);
						$r['err'] = '-9';
						return $r;
					}
				}
			}

			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee`
			SET
				`userid`='{$this->userid}',
				`orderid`='{$orderid}',
				`name`='{$name}',
				`phone`='{$phone}',
				`zip`='{$zip}',
				`address`='{$address}',
				`gender`=(SELECT gender FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` WHERE userid='{$this->userid}' ),
				`prefixid`='{$config['default_prefix_id']}',
				`insertt`=now()
			";
			$db->query($query);
			//更新 pay_get_product
			$query = "UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
			SET `complete`='Y', `modifyt`=now()
			WHERE `prefixid`='{$config['default_prefix_id']}'
				AND `pgpid`='{$pgpid}'
			";
			$db->query($query);

			//扣除處理費
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint`
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`countryid` = '{$config['country']}',
				`behav` = 'process_fee',
				`amount` = '-{$product['total']}',
				`seq` = '0',
				`switch` = 'Y',
				`insertt` = now()
			";
			$db->query($query);
			$spointid = $db->_con->insert_id;

			// 計算免費殺價幣餘額並更新
			$query = "SELECT sum(free_amount) total_free_amount
				FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history`
				WHERE `userid`='{$this->userid}'
				AND   `switch` = 'Y'
				AND   `amount_type` = 1
				";
			$table = $db->getQueryRecord($query);

			if ($table['table']['record'][0]['total_free_amount'] > 0) {

				$free_amount = ($table['table']['record'][0]['total_free_amount'] >= $product['total']) ? $product['total'] : $table['table']['record'][0]['total_free_amount'];
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history`
				SET
					`userid`='{$this->userid}',
					`behav` = 'process_fee',
					`amount_type` = 1,
					`free_amount` = '-{$free_amount}',
					`total_amount` = '-{$product['total']}',
					`spointid` = '{$spointid}',
					`orderid` = '{$orderid}',
					`productid` = '{$product['productid']}',
					`switch` = 'Y',
					`insertt` = now()
				";
				$db->query($query);
			}


			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			SET
				`addressee`='{$name}',
				`area`='{$zip}',
				`address`='{$address}',
				`rphone`='{$phone}',
				`modifyt`=now()
			WHERE `prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$this->userid}'
			";
			$db->query($query);

			if((int)$product['bid_oscode'] > 0) {
				//中標取得殺價券
				//殺價券贈送數量定義於saja_scode_promote
				//saja_product.bid_oscode 紀錄串到saja_scode_promote的Pk
				$this->oscode_by_bid($orderid, $this->userid, $product['bid_oscode'], $product['productid']);
			} elseif((int)$product['bid_scode'] > 0) {
				//中標取得S碼
				// S碼贈送數量定義於saja_scode_promote_rt,
				// saja_product.bid_scode中紀錄串到saja_scode_promote_rt的pk
				$this->scode_by_bid($orderid, $this->userid, $product['bid_scode']);
			} elseif((float)$product['bid_spoint'] > 0) {
				//中標取得殺價幣
				//殺幣的數量直接定義於saja_product.bid_spoint
				$this->spoint_by_bid($orderid, $this->userid, $product['bid_spoint']);
			}
			//1是商品出貨 2 是鯊魚點出貨 這裡只處理type=saja的單
			if ($product['ordertype']=='2'){
				// $query="SELECT o.orderid,o.userid,p.productid,p.name,p.orderbonus,o.status FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ,`{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o, `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp WHERE o.orderid = ({$orderid}) and o.status<2 and o.pgpid=pgp.pgpid and pgp.productid=p.productid";
				/*
				$query="SELECT o.orderid, o.userid, p.productid, p.name, 
				         (CASE o.orderbonus WHEN 0 THEN p.retail_price ELSE o.orderbonus) as orderbonus ,
						       o.status 
				          FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ,`{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o, `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp 
						 WHERE o.type='saja'
						   AND o.orderid = '{$orderid}' 
						   AND o.status IN (0,1) 
						   AND o.pgpid=pgp.pgpid 
						   AND pgp.productid=p.productid ";
				*/
				$query="SELECT o.orderid, o.userid, p.productid, p.name, p.orderbonus, o.status 
				          FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ,`{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o, `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp 
						 WHERE o.type='saja'
						   AND o.orderid = '{$orderid}' 
						   AND o.status IN (0,1) 
						   AND o.pgpid=pgp.pgpid 
						   AND pgp.productid=p.productid ";
				error_log("[ajax/bid] query : ".$query);
				$table = $db->getQueryRecord($query);
				$insQ="INSERT `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`
				       SET
					       `userid`='{$this->userid}',
					       `behav`='order_close',
					       `amount`='{$table["table"]["record"][0]["orderbonus"]}',
						   `insertt`=now()
				       ";
			    error_log("[ajax/bid] insQ : ".$insQ);
				$inschk=$db->query($insQ);
				
				if ($inschk){
					$bonusid=$db->_con->insert_id;
					$insQ1="INSERT `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}settlement_history`
					       SET
						       `prefixid`='saja',
						       `userid`='{$this->userid}',
						       `productid`='{$table["table"]["record"][0]["productid"]}',
						       `bonusid`='{$bonusid}',
							   `insertt`=now()
					       ";
					error_log("[ajax/bid] insQ1 : ".$insQ1);
					$inschk1=$db->query($insQ1);
					if ($inschk1){
						error_log("line 544:".$table["table"]["record"][0]["productid"]);
						$query ="UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` SET status='4' where type='saja' and orderid='{$orderid}'" ;
						$upd1 = $db->query($query);
						
						$push_info['title']="殺價王得標商品出貨通知";
						$push_info['body']="得標商品:".$table["table"]["record"][0]["name"]." 的等值鯊魚點".$table["table"]["record"][0]["orderbonus"]."點已轉入您的帳戶, 您可至7-Eleven使用ibon印出現金券購買該商品 ~";
						$push_info['productid']=$table["table"]["record"][0]["productid"];
						$push_info['action']="0";
						$push_info['url_title']="";
						$push_info['url']="";
						$push_info['sendto']=$this->userid;
						//error_log("[push after_order_close_frontend ]: real:sendtoid".$this->userid);
						$push_info['page']="9";
						$push_info['groupset']="";
						error_log("[push after_order_close_frontend ]: ".json_encode($push_info));
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_URL, $_SERVER['HTTP_ORIGIN']."/site/push/goods_arrival");
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($push_info));
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						error_log("[push after_order_close_frontend ]: ".$_SERVER['HTTP_ORIGIN']."/site/push/goods_arrival");
						$result = curl_exec($ch);
						error_log("[push after_order_close_frontend ]:".json_encode($result));
						curl_close($ch);
					}
				}
			}elseif ($product['ordertype']=='3'){
				$query = "SELECT switch
				FROM `saja_shop`.`saja_product`
				WHERE `productid`='{$product['codepid']}'
				AND   `switch` = 'Y'";
				$productcheck = $db->Record($query);
				if (($product['codenum']>0)&&(sizeof(productcheck)>0)){
					$oscode->insert_bid_oscode($product['productid'],$product['codepid'],$product['codenum'],$this->userid);
				}
			}
		}
		return $r;
	}


	/*
	* S碼、限定S碼 、殺幣  的訂單紀錄 改成 "已發送" (saja_order.status=3)
	*/
	private function set_order_status($orderid) {
		global $db, $config;

		$query ="UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` SET
			`status` ='3',
			`modifyt`=now()
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND `orderid`='{$orderid}'
		" ;
		$db->query($query);
	}

	//中標取得 殺價券(一般發送活動編號)
	/*
	    活動資料紀錄於 saja_scode_promote (贈送數量:onum)
		限定S碼明細記錄於 saja_oscode
		在saja_oscode中增加 onum 筆資料 => 一筆saja_oscode表示使用一次的scode
	*/
	public function oscode_by_bid($orderid, $uid, $spid, $productid) {
		global $db, $config;

		//送殺價券活動
		$query = "SELECT s.*
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` s
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.spid = '{$spid}'
			AND s.behav = 'h'
			AND s.switch = 'Y'
		";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record'][0])) {
			$sp_productid = $rs['table']['record'][0]['productid'];

			//殺價券發送組數
			$onum = (empty($rs['table']['record'][0]['onum']) ) ? 1 : (int)$rs['table']['record'][0]['onum'];

			for($i=1; $i<=$onum; $i++) {
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
				SET
				   `prefixid`='{$config['default_prefix_id']}',
				   `userid`='{$uid}',
				   `spid`='{$spid}',
				   `productid`='{$sp_productid}',
				   `behav` = 'f',
				   `amount`='1',
				   `insertt`=NOW()

				";
				$db->query($query);
			}

			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` SET
			   `scode_sum`=`scode_sum` + {$onum},
			   `amount`=`amount` + {$onum}
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND spid = '{$spid}'
				AND switch = 'Y'
			";
			$db->query($query);

			//訂單紀錄 改成 "已發送"
			$this->set_order_status($orderid);

			return true;
		}

		return false;
	}

	//中標取得 S碼(一般發送活動編號)
	/*
	    活動資料紀錄於 saja_scode_promote & saja_scode_promote_rt (贈送數量: saja_scode_promote_rt.num )
		S碼明細記錄於 saja_scode
		在saja_scode中增加1筆資料, 以amount欄紀錄所贈送的數量
		在saja_scode_history中記錄該次贈送資料(活動代號, 受贈者userid, 該活動被使用1次promote_amount)
		scode_promote記錄該活動被使用的累積次數以及累積S碼贈送數量
	*/
	public function scode_by_bid($orderid, $uid, $spid)
	{
		global $db, $config;

		//取得 S碼活動資訊
		$query = "SELECT s.name, sp.name spname, sp.num num
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` s
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` sp ON
			sp.prefixid = s.prefixid
			AND sp.spid = s.spid
			AND sp.switch = 'Y'
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.spid = '{$spid}'
			AND s.behav = 'd'
			AND unix_timestamp( s.offtime ) >0
			AND unix_timestamp() >= unix_timestamp( s.ontime )
			AND unix_timestamp() <= unix_timestamp( s.offtime )
			AND s.switch = 'Y'
			AND sp.spid IS NOT NULL
		";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record'][0])) {
			$info = $rs['table']['record'][0];
			$batch = time();
			$off = date('Y-m-d H:i:s',strtotime('+180 day'));

			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$uid}',
			   `spid`='{$spid}',
			   `behav`='d',
			   `amount`='{$info['num']}',
			   `offtime`='{$off}',
			   `remainder`='{$info['num']}',
			   `insertt`=NOW()
			";
			$db->query($query);
			$scodeid = $db->_con->insert_id;

			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history`
			SET `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$uid}',
			   `scodeid`='{$scodeid}',
			   `spid`='{$spid}',
			   `promote_amount`='1',
			   `num`='{$info['num']}',
			   `memo`='{$info['name']}',
			   `batch`='{$batch}',
			   `insertt`=NOW()
			";
			$db->query($query);

			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
			SET
			   `scode_sum`=`scode_sum` + '{$info['num']}',
			   `amount`=`amount` + 1
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND spid = '{$spid}'
				AND switch = 'Y'
			";
			$db->query($query);

			//訂單紀錄 改成 "已發送"
			$this->set_order_status($orderid);

			return true;
		}

		return false;
	}

	//中標取得 殺價幣
	/*
	   新增一筆殺幣紀錄(saja_deposit), behav='bid_by_saja'
	   新增一筆儲值紀錄(saja_deposit), behav='bid_deposit'
	   新增一筆儲值history紀錄(saja_deposit_history),
	        driid	= '0',
			data    = '中標發送',
			status  = 'bid'
	*/
	public function spoint_by_bid($orderid, $userid, $spoint) {
		global $db, $config;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint`
		SET
		   `prefixid`='{$config['default_prefix_id']}',
		   `userid`='{$userid}',
		   `countryid`='{$config['country']}',
		   `behav`='bid_by_saja',
		   `amount`='{$spoint}',
		   `insertt`=NOW()
		";
		$db->query($query);
		$spointid = $db->_con->insert_id;
		error_log("[ajax/bid/spoint_by_bid] spointid : ".$spointid);

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
		SET
			 `prefixid`='{$config['default_prefix_id']}',
			 `userid`='{$userid}',
			 `countryid`='{$config['country']}',
			 `behav`='bid_deposit',
			 `currency`='RMB',
			 `amount`='{$spoint}',
			 `insertt`=NOW()
		";
		$db->query($query);
		$depositid = $db->_con->insert_id;
		error_log("[ajax/bid/spoint_by_bid] depositit : ".$depositid);

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history`
		SET prefixid = '{$config['default_prefix_id']}',
			userid = '{$userid}',
			driid = '0',
			data = '中標發送',
			status = 'bid',
			spointid = '{$spointid}',
			depositid = '{$depositid}',
			insertt=NOW()
		";
		error_log("[ajax/bid/spoint_by_bid] : ".$query);
		$res = $db->query($query);
		error_log("[ajax/bid/spoint_by_bid] : res->".$res);

		//訂單紀錄 改成 "已發送"
		$this->set_order_status($orderid);

		return true;
	}

    // 開立折讓
    public function mkSalesAllowance($userid, $amount) {
		global $db, $config;

		$arrPost=array();
		$arrPost['userid']=$userid;
		$arrPost['amount']=$amount;
		$arrPost['json']='Y';
		$ret = postReq(BASE_URL.APP_DIR."/invoice/createAllowance/",$arrPost);
		error_log("[ajax/bid/mkSalesAllowance] ret : ".json_encode($ret));
		return $ret;
    }

    // 開立發票
    public function mkInvoice($userid="", $total_amt="", $sales_amt="", $arrItems, $json="Y" ) {
		global $db, $config;

		$arrPost=array();
		$ret = array("retCode"=>0,"retMsg"=>"");

		$arrPost['json']=$json;
		$arrPost['userid']=$userid;
		$arrPost['utype']="B";
		$arrPost['is_winprize']="N";
		// 金額內含稅
		$arrPost['total_amt']=$total_amt;

		// 金額不含稅
		$arrPost['sales_amt']=$sales_amt;

		$arrPost['items']=array();
		for($i=0; $i<count($arrItems); ++$i) {
			$quantity=($arrItems[$i]['quantity'])?$arrItems[$i]['quantity']:1;
			$arrPost['items'][$i]=array();
			$arrPost['items'][$i]["seq_no"]=($i+1);
			$arrPost['items'][$i]["quantity"]=$quantity;
			$arrPost['items'][$i]["description"]=$arrItems[$i]['description'];
			$arrPost['items'][$i]["amount"]=round($arrItems[$i]['amount']);
			$arrPost['items'][$i]["unitprice"]=round($arrItems[$i]['amount']/$quantity,2);
		}

		$ret = postReq("https://www.saja.com.tw/site/invoice/createSalesInvoice/",$arrPost);
		error_log("[ajax/bid/mkInvoice] ret : ".json_encode($ret));
		return $ret;
    }

    public function check_limit($product){
		global $db, $config;
    	$r =true;
    	$limitid = $product['limitid'];

    	$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited`
		where
			plid = '{$limitid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		$kind=$table['table']['record'][0]['kind'];
		$mvalue=$table['table']['record'][0]['mvalue'];
		$svalue=$table['table']['record'][0]['svalue'];
		$limit_group=$table['table']['record'][0]['limit_group'];
		$plname=$table['table']['record'][0]['name'];

		$count = $this->pay_get_product_count($this->userid);

		if( $kind=='small') {		//小於
			if ($count >= $mvalue) {
				$r = false;
			}
		} else if($kind=='big') {	//大於
			if($count <= $svalue) {
				$r = false;
			}
		}

		// 新手限定 一次一標
		if ($limit_group > 0) {
			$query = "SELECT
						h.productid,
						p.name,
						h.userid,
						h.nickname,
						pl.name as pl_name,
						pl.limit_group
					FROM
						`{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON h.productid = p.productid
						AND p.switch = 'Y'
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited` pl
						ON p.limitid = pl.plid
						AND pl.switch = 'Y'
					WHERE
						h.switch = 'Y'
					AND (
						p.offtime > now()
						OR p.closed = 'N'
					)
					AND h.userid = '{$this->userid}'
					AND pl.limit_group = '{$limit_group}'
					AND p.productid <> '{$product['productid']}'
					AND p.ptype = '{$product['ptype']}'
					GROUP BY
						h.productid";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])) {
				$r = false;
			}
		}
		return $r;
    }

    //得標次數限制
	public function pay_get_product_count($userid) {
		global $db, $config;

		$query = "SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
		where
			`prefixid` = '{$config['default_prefix_id']}'
			AND userid = '{$userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		return $table['table']['record'][0]['count'];
	}
}
?>