<?php
session_start();
// 1. 要接收 $json, 
$ret=null;

/*
 *	phone				手機號碼
 *	currency			幣別 (TWD:台幣, RMB:人民幣)
 *	sign				簽名
 *	calltype			執行動作類別 (order_cancel:取消扣點, order:扣點, checkbonus:查詢鯊魚點餘額, checkphone:確認手機關聯帳號)
 *	form				第三方來源編碼 (參照`saja_exchange_store`.`esid`)
 *	code				第三方串接密碼 (參照`saja_exchange_store`.`pin`) 
 *	orderid				第三方訂單編號
 *	pay_code			第三方支付編碼
 *	productid			第三方商品編號(非必要)
 *	total_amount		扣點金額
 *	callback_url		回傳網址(非必要) 	 
 *  userid				Saja會員編號
 */
$data = array(
	"phone" 		=> (empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone'])),
	"currency" 		=> (empty($_POST['currency']) ? htmlspecialchars($_GET['currency']) : htmlspecialchars($_POST['currency'])),
	"sign" 			=> (empty($_POST['sign']) ? htmlspecialchars($_GET['sign']) : htmlspecialchars($_POST['sign'])),
	"code" 			=> (empty($_POST['code']) ? htmlspecialchars($_GET['code']) : htmlspecialchars($_POST['code'])),
	"form" 			=> (empty($_POST['form']) ? htmlspecialchars($_GET['form']) : htmlspecialchars($_POST['form'])),
	"calltype" 		=> (empty($_POST['calltype']) ? htmlspecialchars($_GET['calltype']) : htmlspecialchars($_POST['calltype'])),
	"orderid" 		=> (empty($_POST['orderid']) ? htmlspecialchars($_GET['orderid']) : htmlspecialchars($_POST['orderid'])),
	"pay_code" 		=> (empty($_POST['pay_code']) ? htmlspecialchars($_GET['pay_code']) : htmlspecialchars($_POST['pay_code'])),
	"productid" 	=> (empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid'])),
	"total_amount" 	=> (empty($_POST['total_amount']) ? htmlspecialchars($_GET['total_amount']) : htmlspecialchars($_POST['total_amount'])),
	"callback_url" 	=> (empty($_POST['callback_url']) ? htmlspecialchars($_GET['callback_url']) : htmlspecialchars($_POST['callback_url'])),
	"userid" 		=> (empty($_POST['userid']) ? htmlspecialchars($_GET['userid']) : htmlspecialchars($_POST['userid']))
);

//判斷基本參數是否有值
if(empty($data['phone']) || empty($data['sign']) || empty($data['calltype']) || empty($data['form']) || empty($data['userid']) ) {	
	$ret['retCode']=10001;
	$ret['retMsg']='資料有誤，無權限使用此功能!!';
	$ret['type']='MSG';	
	echo json_encode($ret);
	exit;
} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/ini.php");
	
	$c = new Bonus;
	
	//判斷執行流程 (order_cancel:取消扣點, order:扣除鯊魚點, checkbonus:鯊魚點查詢, checkphone:確認手機關聯帳號)
	if($data['calltype'] == "order" ) {
		$c->home($data);
	}
	elseif($data['calltype'] == "order_cancel") {
		$c->cancel_order($data);
	}
	elseif($data['calltype'] == "checkbonus") {
		$c->check_bonus($data);
	} 
	elseif($data['calltype'] == "checkphone") {
		$c->check_phone($data);
	}	
}

/*
 *	第三方扣除鯊魚點專用 API
 */
class Bonus {

	/*
	 *	扣除鯊魚點流程
	 *	$data					array				接收資料陣列
	 */	
	public function home($data)
	{
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		//empty($data['productid']) ||
		
		if(empty($data['phone']) || empty($data['currency']) || empty($data['sign']) || empty($data['code']) || empty($data['form']) || empty($data['orderid']) || empty($data['total_amount']) || empty($data['calltype'])) {	
			$ret['retCode']=10001;
			$ret['retMsg']='資料有誤，無權限使用此功能!!';
		} else if(!is_numeric($data['phone'])) {
			$ret['retCode']=10002;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(strpos($data['phone'],".")!=false || strpos($data['phone'],"+")!=false ||  strpos($data['phone'],"-")!=false) {
			$ret['retCode']=10003;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(empty($data['orderid'])) {
			$ret['retCode']=10004;
			$ret['retMsg']="第三方訂單編號格式不正確 !!";
		} 
		/*
		else if(empty($data['productid'])) {
			$ret['retCode']=10005;
			$ret['retMsg']="第三方商品編號格式不正確 !!";
		} */
		else if(empty($data['total_amount'])) {
			$ret['retCode']=10006;
			$ret['retMsg']="扣點金額格式不正確 !!";
		} else if(empty($data['currency'])) {
			$ret['retCode']=10007;
			$ret['retMsg']="幣別格式不正確 !!";		
		} else if(!empty($data['currency']) && ($data['currency'] != "TWD") && ($data['currency'] != "RMB")) {
			$ret['retCode']=10008;
			$ret['retMsg']="目前不支此種幣別 !!";
		} else if(empty($data['form'])) {
			$ret['retCode']=10009;
			$ret['retMsg']="第三方來源編碼不正確 !!";				
		} else {
			
			// 幣別點數換算
			if($data['currency'] == "TWD") {
				$data['total_bonus'] = $data['total_amount'];
			} else {
				$data['total_bonus'] = ($data['total_amount'] / 4);		
			}
			
			// 驗證簽名資料是否正確
			$sign = $this->sign($data);
			
			// 資料和簽名正確
			if($sign['retCode'] == 1) {
				// 查詢相關帳號資料
				$account = $this->phone($data);
				
				// 查到相關帳號
				if($account['retCode'] == 1){
					// 指定會員編號 $data['userid'] = $account['userid'];
					// 確認帳號點數是否足夠
					$bonus = $this->bonus($account['userid'], $data['total_bonus']);
					
					// 鯊魚點數足夠
					if($bonus['retCode'] == 1){
						// 指定鯊魚點餘額
						$data['amount'] = $bonus['amount'];
						
						// 產生訂單記錄並扣除鯊魚點
						$mk = $this->mk_order($data);
						
						$ret = $mk;
					} else {
						$ret = $bonus; 
					}
				} else {
					$ret = $account; 
				}
			} else {
				$ret = $sign; 
			}
		}
		
		$ret['retType'] = "MSG";
		//判斷是否有指定回傳位址，若有則回傳
		if (empty($data['callback_url'])) {
			echo json_encode($ret);
		} else {
			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
						'method' => "POST",
						'content' => json_encode($ret),
						'header' => "Content-Type:application/json" 
				) 
			);
			$context  = stream_context_create($stream_options);
			// 送出json內容並取回結果
			$response = file_get_contents($data['callback_url'], false, $context);
			error_log("[ajax/bonuspay/home] response : ".$response);		
		}	
	}

	/*
	 *	查詢鯊魚點流程
	 *	$data					array				接收資料陣列
	 */
	public function check_bonus($data) {
		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		if(empty($data['phone']) || empty($data['form']) || empty($data['sign']) || empty($data['calltype'])) {	
			$ret['retCode']=10001;
			$ret['retMsg']='資料有誤，無權限使用此功能!!';
		} else if(!is_numeric($data['phone'])) {
			$ret['retCode']=10002;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(strpos($data['phone'],".")!=false || strpos($data['phone'],"+")!=false ||  strpos($data['phone'],"-")!=false) {
			$ret['retCode']=10003;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(empty($data['form'])) {
			$ret['retCode']=10004;
			$ret['retMsg']="第三方來源編碼不正確 !!";				
		} else {
			
			// 驗證簽名資料是否正確
			$sign = $this->sign($data);
			
			// 資料和簽名正確
			if($sign['retCode'] == 1) {
				// 查詢相關帳號資料
				$account = $this->phone($data);
				
				// 查到相關帳號
				if($account['retCode'] == 1) {
					// 指定會員編號
					$data['userid'] = $account['userid'];
					// 確認帳號點數是否足夠
					$bonus = $this->bonus($account['userid'], 0);
					$ret = $bonus;
				} else {
					$ret = $account; 
				}
				
			} else {
				$ret = $sign; 
			}
		}
		
		$ret['retType'] = "MSG";

		//判斷是否有指定回傳位址，若有則回傳
		if (empty($data['callback_url'])) {
			echo json_encode($ret);
		} else {
			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
						'method' => "POST",
						'content' => json_encode($ret),
						'header' => "Content-Type:application/json" 
				) 
			);
			$context  = stream_context_create($stream_options);
			// 送出json內容並取回結果
			$response = file_get_contents($data['callback_url'], false, $context);
			error_log("[ajax/bonuspay/home] response : ".$response);		
		}
	}

	/*
	 *	查詢手機關聯帳號流程
	 *	$data					array				接收資料陣列
	 */
	public function check_phone($data) {
		global $db, $config, $router;
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		if(empty($data['phone']) || empty($data['form']) || empty($data['sign']) || empty($data['calltype'])) {	
			$ret['retCode']=10001;
			$ret['retMsg']='資料有誤，無權限使用此功能!!';
		} else if(!is_numeric($data['phone'])) {
			$ret['retCode']=10002;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(strpos($data['phone'],".")!=false || strpos($data['phone'],"+")!=false ||  strpos($data['phone'],"-")!=false) {
			$ret['retCode']=10003;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(empty($data['form'])) {
			$ret['retCode']=10004;
			$ret['retMsg']="第三方來源編碼不正確 !!";				
		} else {
			
			// 證簽名資料是否正確
			$sign = $this->sign($data);
			
			// 資料和簽名正確
			if($sign['retCode'] == 1) {
				// 查詢相關帳號資料
				$account = $this->phone($data);
				
				$ret = $account; 
			} else {
				$ret = $sign; 
			}
		}
		
		$ret['retType'] = "MSG";

		//判斷是否有指定回傳位址，若有則回傳
		if (empty($data['callback_url'])) {
			echo json_encode($ret);
		} else {
			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
						'method' => "POST",
						'content' => json_encode($ret),
						'header' => "Content-Type:application/json" 
				) 
			);
			$context  = stream_context_create($stream_options);
			// 送出json內容並取回結果
			$response = file_get_contents($data['callback_url'], false, $context);
			error_log("[ajax/bonuspay/home] response : ".$response);		
		}	
	}
	
	/*
	 *	取消扣點流程
	 *	$data					array				接收資料陣列
	 */	
	public function cancel_order($data)	{
		global $db, $config, $router;
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		if(empty($data['phone']) || empty($data['sign']) || empty($data['code']) || empty($data['form']) || empty($data['orderid']) || empty($data['calltype'])) {	
			$ret['retCode']=10021;
			$ret['retMsg']='資料有誤，無權限使用此功能!!';
		} else if(!is_numeric($data['phone'])) {
			$ret['retCode']=10022;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(strpos($data['phone'],".")!=false || strpos($data['phone'],"+")!=false ||  strpos($data['phone'],"-")!=false) {
			$ret['retCode']=10023;
			$ret['retMsg']="手機號碼格式不正確 !!";
		} else if(empty($data['orderid'])) {
			$ret['retCode']=10024;
			$ret['retMsg']="第三方訂單編號格式不正確 !!";
		} else if(empty($data['pay_code'])) {
			$ret['retCode']=10025;
			$ret['retMsg']="第三方支付編碼不正確 !!";		
		} else if(empty($data['form'])) {
			$ret['retCode']=10026;
			$ret['retMsg']="第三方來源編碼不正確 !!";				
		} else {
			
			// 驗證簽名資料是否正確
			$sign = $this->sign($data);
			
			// 資料和簽名正確
			if($sign['retCode'] == 1) {
				// 查詢相關帳號資料
				$account = $this->phone($data);
				
				// 查到相關帳號
				if($account['retCode'] == 1) {
						
						// 取消訂單記錄並返回鯊魚點
						$ret = $this->back_order($data);
					
				} else {
					$ret = $account; 
				}
			} else {
				$ret = $sign; 
			}
		}
		
		$ret['retType'] = "MSG";
		//判斷是否有指定回傳位址，若有則回傳
		if (empty($data['callback_url'])) {
			echo json_encode($ret);
		} else {
			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
						'method' => "POST",
						'content' => json_encode($ret),
						'header' => "Content-Type:application/json" 
				) 
			);
			$context  = stream_context_create($stream_options);
			// 送出json內容並取回結果
			$response = file_get_contents($data['callback_url'], false, $context);
			error_log("[ajax/bonuspay/home] response : ".$response);		
		}	
	}
	
	/*
	 *	檢查是手機號是否正確
	 *	$phone				varchar				手機號
	 */
	public function phone($vars) {
		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		// 取得兌換廠商PIN碼
		$query = "SELECT pin 
					FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` 
				   WHERE prefixid='{$config['default_prefix_id']}' 
					 AND esid='{$vars['form']}' 
			         AND switch='Y' 
		";
		$table = $db->getQueryRecord($query); 		
		$store = $table['table']['record'][0];
		
		// 檢查廠商PIN碼是否符合
		$str = "esid={$vars['form']}&pin={$vars['code']}";
		$chkpin = $this->encryptWithOpenssl($str);
		if($store['pin'] !== $chkpin) {
			
			$ret['retCode'] = 30002;
			$ret['retMsg'] = '廠商PIN碼錯誤 !!';
			$ret['pin'] = $store['pin'];
			$ret['chkpin'] = $chkpin;
		
		} else {

			$query = " SELECT u.userid, count(u.userid) as dup_cnt, usa.verified 
						FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u 
						INNER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa 
						ON u.prefixid = usa.prefixid AND u.userid = usa.userid AND usa.switch = 'Y' 
					   WHERE u.prefixid='{$config['default_prefix_id']}' 
						 AND usa.verified='Y' 
						 AND usa.userid='{$vars['userid']}'
						 AND usa.phone='{$vars['phone']}' ";
						 
			$table = $db->getQueryRecord($query);
			
			$userid = $table['table']['record'][0]['userid'];
			if($table['table']['record'][0]['dup_cnt']>0) {
				$ret['retCode'] = 1;
				$ret['retMsg'] = '已確認相關帳號 !!';
				$ret['userid'] = $userid;
			} else {			
				$ret['retCode'] = 30001;
				$ret['retMsg'] = '查無可扣點相關帳號 !!';
			}
		}
		
		return $ret;
	}

	/*
	 *	檢查是鯊魚點是否足夠
	 *	$userid				int					會員編號
	 *	$total_bonus		int					總計扣除鯊魚點	 
	 */
	public function bonus($userid, $total_bonus) {
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();		
	
		$query = "SELECT IFNULL(sum(amount),0) as amount 
		FROM `{$config["db"][1]["dbname"]}`.`{$config['default_prefix']}bonus`  
		WHERE 
			prefixid = '{$config["default_prefix_id"]}' 
			AND userid = '{$userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		if($table['table']['record'][0]['amount'] >= $total_bonus) {
			$ret['retCode'] = 1;
			$ret['retMsg'] = '點數查詢成功 !!';
			$ret['amount'] = $table['table']['record'][0]['amount'];	
		} else {			
			$ret['retCode'] = 40001;
			$ret['retMsg'] = '剩餘點數不足 !!';
			$ret['amount'] = $table['table']['record'][0]['amount'];
		}
		return $ret;
	}
	
	/*
	 *	產生訂單記錄並扣點
	 *	$data					array				接收資料陣列	 
	 */ 
	public function mk_order($data) {
		$time_start = microtime(true);
		
		global $db, $config;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();	
		
		// 取得兌換密碼
		$query = "SELECT exchangepasswd 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
				   WHERE 	prefixid = '{$config['default_prefix_id']}' 
					 AND userid = '{$data['userid']}' 
			         AND switch = 'Y' 
		";
		//$table = $db->getQueryRecord($query); 		
		//$user = $table['table']['record'][0];
		// 檢查兌換密碼是否符合
		//if($user['exchangepasswd']!== $oldexpasswd) {
		//	$ret['retCode'] = 50001;
		//	$ret['retMsg'] = '兌換密碼錯誤 !!';		
		//}		
			// 確認會員資料
			$query = "SELECT * 
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
			   WHERE prefixid = '{$config['default_prefix_id']}' 
				 AND userid = '{$data['userid']}' 
				 AND switch = 'Y' 
			";
			$table = $db->getQueryRecord($query); 		
			$updata = $table['table']['record'][0];
			
			if (!empty($updata['userid'])) {
				
				$tx_data = json_encode($data);
				
				//新增訂單
				$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$data['userid']}',
					`status`='1',
					`pgpid`='0',
					`epid`='0',
					`esid`='{$data['form']}',
					`type`='exchange',
					`num`='1',
					`point_price`='{$data['total_bonus']}',
					`process_fee`='0',
					`total_fee`='{$data['total_bonus']}',
					`memo`='第三方扣點:{$data['form']}_單號:{$data['orderid']}',
					`tx_data`='{$tx_data}',
					`confirm`='Y',
					`insertt`=NOW()
				";
				$res = $db->query($query);
				$orderid = $db->_con->insert_id;
				error_log("[ajax/bonuspay/mk_order] INSERT order : ".$query);
				
				//新增訂單明細
				//`gender`=(SELECT gender FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` WHERE userid='{$data['userid']}' ),
				$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` 
				SET 
					`userid`='{$data['userid']}',
					`orderid`='{$orderid}',
					`name`='{$updata['addressee']}',
					`phone`='{$data['phone']}',
					`zip`=0,
					`address`='{$updata['address']}',
					`prefixid`='{$config['default_prefix_id']}',
					`insertt`=NOW()
				";
				$db->query($query);
				error_log("[ajax/bonuspay/mk_order] INSERT order_consignee : ".$query);
				
				//扣除鯊魚點
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$data['userid']}',
					`countryid`='{$config['country']}', 
					`behav`='other_exchange', 
					`amount`='-{$data['total_bonus']}', 
					`switch`='Y', 
					`insertt`=NOW()
				";
				$db->query($query); 
				$bonusid = $db->_con->insert_id;
				error_log("[ajax/bonuspay/mk_order] INSERT bonus : ".$query);
				
				//鯊魚點和訂單關聯紀錄
				$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$data['userid']}', 
					`orderid`='{$orderid}',
					`bonusid`='{$bonusid}',
					`amount`='-{$data['total_bonus']}',
					`seq`=0, 
					`switch`='Y', 
					`insertt`=NOW()
				";
				$db->query($query); 
				$evrid = $db->_con->insert_id;
				error_log("[ajax/bonuspay/mk_order] INSERT exchange_bonus_history : ".$query);
				
				if (!empty($orderid) && !empty($bonusid) ) {
					// 發送簡訊通知
					$msg = iconv("UTF-8","big5","已扣除鯊魚點:".$data['total_bonus']." [台灣殺價王].");
					// 發送簡訊 (09XXXXXXXX 台灣, 其他->中國) //sendSMS($data['phone'], $msg);	
					
					$ret['retCode'] = 1;
					$ret['retMsg'] = '扣點成功';
					$ret['orderid'] = $orderid;
					$ret['bonusid'] = $bonusid;
				} else {
					$ret['retCode'] = 50003;
					$ret['retMsg'] = '扣點失敗 !!';
				}
			} else {
				$ret['retCode'] = 50002;
				$ret['retMsg'] = '會員資料有誤 !!';
			}
		
		return $ret;
	}
	
	/*
	 *	取消訂單記錄, 並返回鯊魚點
	 *	$data					array				接收資料陣列
	 */ 
	public function back_order($data) {
		$time_start = microtime(true);
		
		global $db, $config;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();	
			
			$pay_code = explode("_", $data['pay_code']);
			$saja_orderid = $pay_code[1];
			$saja_bonusid = $pay_code[2];
			
			//鯊魚點和訂單關聯紀錄
			$query = "SELECT count(ebh.userid) as cnt, ebh.amount, o.memo
					FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` ebh 
					INNER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o 
					ON o.orderid = ebh.orderid AND o.prefixid = ebh.prefixid 
				WHERE ebh.prefixid='{$config['default_prefix_id']}' 
					AND ebh.`switch` = 'Y' 
					AND o.`status` NOT IN (2,5)
					AND ebh.`userid`='{$data['userid']}'
					AND ebh.`orderid`='{$saja_orderid}' 
			";
			$table = $db->getQueryRecord($query);
			error_log("[ajax/bonuspay/mk_order] SELECT exchange_bonus_history : ".$query);
			
			if($table['table']['record'][0]['cnt']==1) {
				
				$edit_memo = $table['table']['record'][0]['memo'] .'_訂單已取消';
				$edit_amount = abs($table['table']['record'][0]['amount']);
				
				//變更訂單
				$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
				SET `status`='5',
					`memo`='{$edit_memo}'
				WHERE `userid`='{$data['userid']}' AND `orderid`='{$saja_orderid}'
				";
				$res = $db->query($query);
				error_log("[ajax/bonuspay/mk_order] UPDATE order : ".$query);
				
				//返回鯊魚點
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$data['userid']}',
					`countryid`='{$config['country']}', 
					`behav`='other_exchange', 
					`amount`='{$edit_amount}', 
					`switch`='Y', 
					`insertt`=NOW()
				";
				$db->query($query); 
				$bonusid = $db->_con->insert_id;
				error_log("[ajax/bonuspay/mk_order] INSERT bonus : ".$query);
				
				//鯊魚點和訂單關聯紀錄
				$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$data['userid']}', 
					`orderid`='{$saja_orderid}',
					`bonusid`='{$bonusid}',
					`amount`='{$edit_amount}',
					`seq`=0, 
					`switch`='Y', 
					`insertt`=NOW()
				";
				$db->query($query); 
				error_log("[ajax/bonuspay/mk_order] INSERT exchange_bonus_history : ".$query);
			
				if (!empty($saja_orderid) && !empty($bonusid) ) {
					// 發送簡訊通知
					$msg = iconv("UTF-8","big5","已返回鯊魚點:".$edit_amount." [台灣殺價王].");
					// 發送簡訊 (09XXXXXXXX 台灣, 其他->中國) //sendSMS($data['phone'], $msg);	
					
					$ret['retCode'] = 1;
					$ret['retMsg'] = '返點成功';
					$ret['orderid'] = $saja_orderid;
					$ret['bonusid'] = $bonusid;
				} else {
					$ret['retCode'] = 50021;
					$ret['retMsg'] = '返點失敗 !!';
				}
			
			} else {
				$ret['retCode'] = 50022;
				$ret['retMsg'] = '查無相關訂單關聯紀錄 !!';
			}
		
		return $ret;
	}
	
	
	/*
	 *	檢查資料和簽名是否正確
	 *	$data					array				接收資料陣列
	 */
	private function sign($data)	{
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();		
		
		if ($data['calltype'] == "order_cancel"){
			$localsign0 = "calltype={$data['calltype']}&form={$data['form']}&code={$data['code']}&phone={$data['phone']}&orderid={$data['orderid']}&pay_code={$data['pay_code']}&key=SajaTwBonus";
		} elseif ($data['calltype'] == "order"){
			//&productid={$data['productid']}
			$localsign0 = "calltype={$data['calltype']}&form={$data['form']}&code={$data['code']}&phone={$data['phone']}&currency={$data['currency']}&orderid={$data['orderid']}&total_amount={$data['total_amount']}&key=SajaTwBonus";
		} else {
			$localsign0 = "calltype={$data['calltype']}&form={$data['form']}&phone={$data['phone']}&key=SajaTwBonus";
		}
		$localsign = md5($localsign0);
		
		//判斷簽名是否正確
		if ($data['sign'] == $localsign) {		
			$ret['retCode'] = 1;
			$ret['retMsg'] = '資料簽名正確 !!';
		} else {			
			$ret['retCode'] = 20001;
			$ret['retMsg'] = '資料簽名有誤 !!';
		}
		return $ret;
	}
	
	//查驗廠商PIN碼 
	//$crypt_iv 與 $crypt_key 請勿更改 
	public function encryptWithOpenssl($data){
        $crypt_iv = "5793016129403874";
		$crypt_key = 'saJA19$!DL#&nx*6';
		$_encrypt = base64_encode(openssl_encrypt($data,"AES-128-CBC",$crypt_key,OPENSSL_RAW_DATA,$crypt_iv));
		
		return urlencode($_encrypt);
    }
	
	// Sign 解碼器
	//$crypt_iv 與 $crypt_key 請勿更改
	public function opensslDecrypt($data){
        $crypt_iv = "5793016129403874";
		$crypt_key = "saJA19$!DL#&nx*6";
		$_decrypt = urldecode($data);
		
		return openssl_decrypt(base64_decode($_decrypt),"AES-128-CBC",$crypt_key,OPENSSL_RAW_DATA,$crypt_iv);
    }
	
}
?>