// JavaScript Document

function checkphone() {
    console.log($().length);
    var rightid;
    for (i = 0; i < $('[data-role="panel"]').length; i++) {
        var rtemp = $('[data-role="panel"]:eq(' + i + ')').attr('id');
        if (rtemp.match("right-panel-") == "right-panel-") {
            rightid = rtemp;
        }
    }
    var pageid = $.mobile.activePage.attr('id');
    alert('手機未驗證');
    location.href = APP_DIR + "/verified/register_phone/";
}

function bid_confirm() {
    var rightid;
    for (i = 0; i < $('[data-role="panel"]').length; i++) {
        var rtemp = $('[data-role="panel"]:eq(' + i + ')').attr('id');
        if (rtemp.match("right-panel-") == "right-panel-") {
            rightid = rtemp;
        }
    }

    var pageid = $.mobile.activePage.attr('id');
    var opgpid = $('#' + pageid + ' #pgpid').val();
    var oproductid = $('#' + pageid + ' #productid').val();
    // var onum = $('#' + pageid + ' #num').val();
    var oname = $('#' + pageid + ' #name').val();
    var ozip = $('#' + pageid + ' #zip').val();
    var oaddress = $('#' + pageid + ' #address').val();
    var ophone = $('#' + pageid + ' #phone').val();

    if (oname == "" && oaddress == "" && ophone == "") {
        alert('數量、收件人姓名、電話及地址不能空白');
    } else {
        $.post(APP_DIR + "/ajax/bid.php?productid=" + oproductid + "&pgpid=" + opgpid + "&t=" + getNowTime(), {
                name: oname,
                zip: ozip,
                address: oaddress,
                phone: ophone
            },
            function(str_json) {
                if (str_json) {
                    var json = $.parseJSON(str_json);
                    console && console.log($.parseJSON(str_json));

                    if (json.status == 200 || json.status == 1 || json.retCode == 1) {
                        location.href = encodeURI(APP_DIR + "/sys/showMsg/?code=" + json.status + "&msg=結帳成功&kind=bid&kid=" + oproductid);
                    } else{
                        alert(json.retMsg);
					}
                }

            });
    }
}

/* record.php */
function bid_checkout() {
    var pageid = $.mobile.activePage.attr('id');
    var opgpid = $('#' + pageid + ' #pgpid').val();
    var oproductid = $('#' + pageid + ' #productid').val();
    var oname = $('#' + pageid + ' #name').val();
    var ozip = $('#' + pageid + ' #zip').val();
    var oaddress = $('#' + pageid + ' #address').val();
    var ophone = $('#' + pageid + ' #phone').val();
    var auth_id = "<?php echo $_SESSION['auth_id']?>";

    if (oname == "" || ozip == "" || oaddress == "" || ophone == "") {
        alert('收件人姓名、郵政編號、電話及地址不能空白');
        //location.href=APP_DIR+"/user/profile/";
        return;
    } else {
        $.post(APP_DIR + "/ajax/user.php", {
                json: "Y",
                auth_id: auth_id,
                type: "profile",
                name: oname,
                zip: ozip,
                address: oaddress,
                rphone: ophone
            },
            function(str_json) {
                if (str_json) {
                    var json = $.parseJSON(str_json);
                    console.log(json);
                    if (json.retCode == 1) {
                        $(".member_detail").removeClass('on');
                        $("#msg_name").replaceWith("<p id='msg_name'>" + oname + "</p>");
                        $("#msg_phone").replaceWith("<p id='msg_phone'>" + ophone + "</p>");
                        $("#msg_zip").replaceWith("<p id='msg_zip'>" + ozip + "</p>");
                        $("#msg_address").replaceWith("<p id='msg_address'>" + oaddress + "</p>");
                    } else if (json.retCode == -1) {
                        alert('請先登入會員');
                    } else if (json.retCode == -10008) {
                        alert('收件人姓名不可為空白');
                    } else if (json.retCode == -10009) {
                        alert('收件人郵編不可為空白');
                    } else if (json.retCode == -10010) {
                        alert('收件人地址不可為空白');
                    } else if (json.retCode == -10011) {
                        alert('收件人姓名格式錯誤');
                    } else if (json.retCode == -10012) {
                        alert('收件人郵編格式錯誤');
                    } else if (json.retCode == -10013) {
                        alert('收件人地址格式錯誤');
                    } else if (json.retCode == -10014) {
                        alert('數據修改失敗');
                    } else if (json.retCode == -10015) {
                        alert('收件人電話格式錯誤');
                    } else if (json.retCode == -10016) {
                        alert('收件人電話不可為空白');
                    }
                }


            });




    }
}