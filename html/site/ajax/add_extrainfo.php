<?php
include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");


$c = new Addextrainfo;
$c->home();
class Addextrainfo 
{
	public $str;
	
	public function home() {
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		$query = "SELECT u.userid 
				FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user` u
				LEFT OUTER JOIN `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_profile` up ON 
					u.prefixid = up.prefixid 
					AND u.userid = up.userid 
					AND up.switch = 'Y'
				WHERE
					u.prefixid = '{$config['default_prefix_id']}' 
					AND u.switch = 'Y'
				";
		$table = $db->getQueryRecord($query);
		
		
		foreach($table['table']['record'] as $rk => $rv) {
			
			$usernum = str_pad($rv['userid'],10,'0',STR_PAD_LEFT);
			
			$id2 = $config['firstbank_companyid'].$config['firstbank_kindnum_mall'].$usernum;
			
			$query2 = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo`
			SET
				`prefixid` = '{$config['default_prefix_id']}'
				, `userid` = '{$rv['userid']}'
				, `uecid` = '11'
				, `field1name` = '{$id2}'
				, `seq` = '0'
				, `switch` = 'Y'
				, `insertt` = NOW()
			";
			$ue2 = $db->query($query2);
		}	
	}
}
?>
