<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");

$c = new App_act;

if(!empty($HTTP_RAW_POST_DATA)){
	foreach(json_decode($HTTP_RAW_POST_DATA,true) as $k=>$v ) {
		$_POST[$k]=$v;
	}
}
error_log("[ajax/app_act.php] ".json_encode($_REQUEST));

if($_REQUEST['adid']=='19') {
    // 2019 圓夢碼 掃碼送券
	$c->gift_dream_event();
}


class App_act 
{
	public $str;
	
	public function genRet($err) {
	       $json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		   if($json=='Y') {
		      $ret=getRetJSONArray($err['retCode'],$err['retMsg'],'MSG');
		   } else {
			  $ret=$err;	
		   }		   
	       error_log("[getRet]:".json_encode($ret));
           return $ret;		   
	}
	
	// 2019/11/14 圓夢碼 掃碼送券
	public function gift_dream_event() {
		global $db, $config, $usermodel;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$b_day = date("Y-m-d 00:00:00");
		$e_day = date("Y-m-d 23:59:59");
		$now_day = date("Y-m-d H:i:s");
		
		$activity_type = 19;//活動代碼
		$ary_productid = array('0');//商品代碼
		//$insertt = '2019-11-14 12:00:00';//新手註冊時間
		$oscode_cnt = 1;//券數量
		$spoint_cnt = 0;//幣數量
		
		$userid = empty($_REQUEST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_REQUEST['auth_id']);
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];
		$ret=array();
		$app=null;
		
		
		if (empty($userid)){
			$ret['status'] = -1;
			
			//回傳:
			$app['retCode']=1;
			$app['retMsg']="OK";
			$app['retType']="LIST";
			$app['action_list']=array(
				"type"=>5,
				"page"=>9,
				"msg"=>"會員資料錯誤, 請重新登入！"
			);
			
		} elseif (MD5($userid .'|'. $_REQUEST['ts']) !== $_REQUEST['tk']){
			$ret['status'] = -3;
			
			//回傳:
			$app['retCode']=1;
			$app['retMsg']="OK";
			$app['retType']="LIST";
			$app['action_list']=array(
				"type"=>5,
				"page"=>4,
				"msg"=>"驗證碼錯誤！"
			);
			
		} else {
		
			$query = "SELECT insertt
				FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` ah
				WHERE
					ah.switch = 'Y'
					AND ah.activity_type= {$activity_type}
					AND ah.userid = '{$userid}' ";
			$table1 = $db->getQueryRecord($query);
			
			if (!empty($table1['table']['record']) ) {
				$ah_insertt = $table1['table']['record'][0]['insertt'];
				
				$query = "SELECT scodeid, offtime
				FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
				WHERE
					switch = 'Y'
					AND behav='d'
					AND spid= {$activity_type}
					AND userid = '{$userid}' 
					ORDER BY scodeid desc LIMIT 1";
				$table2 = $db->getQueryRecord($query);
				
				//前次到期時間
				$ex_offtime = $table2['table']['record'][0]['offtime'];
				
				//贈送條件: 前次到期後,才可再次贈送
				if($now_day < $ex_offtime){
					$ret_status = 2;
				} else {
					//2019/11/14 AARONFU 送券
					$productid = $ary_productid[0];
					$ret_status = $this->scode_19($userid, $oscode_cnt, $productid, $activity_type);
				}
				
			} else {
				
				// 寫入活動紀錄
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
					SET
						`userid`='{$userid}',
						`activity_type`= {$activity_type},
						`insertt`=NOW()";
				$db->query($query);
				
				//2019/11/14 AARONFU 送券
				$productid = $ary_productid[0];
				$ret_status = $this->scode_19($userid, $oscode_cnt, $productid, $activity_type);
			}
		
			if($ret_status==1) {
				$ret['status'] = 1;
				
				//回傳:
				$app['retCode']=1;
				$app['retMsg']="OK";
				$app['retType']="LIST";
				$app['action_list']=array(
					"type"=>5,
					"page"=>9,
					"msg"=>"恭喜獲得殺價券！"
				);
			
			} else {
				$ret['status'] = -2;
				
				//回傳:
				$app['retCode']=1;
				$app['retMsg']="OK";
				$app['retType']="LIST";
				$app['action_list']=array(
					"type"=>5,
					"page"=>4,
					"msg"=>"殺價券已領取！"
				);
			}
		}
		
		if($json=='Y') {
			echo json_encode($app);
		} else {
			echo json_encode($ret);
		}
		exit;
	}
	
	//2019/11/14 AARONFU 送券
	public function scode_19($userid, $oscode_cnt, $productid=0, $activity_type=19) {
		global $db, $config, $usermodel;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		//備用贈送活動表
		$query_action = "SELECT `ontime`,`offtime`
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}action`
			WHERE `activity_type` = '{$activity_type}' ";
		//$table_action = $db->getQueryRecord($query_action);
			
		// 延遲一點時間, 0.05 ~ 0.2 秒不等
		$sleep = 50000 * (getRandomNum()%4+1);
		usleep($sleep);
		
		// 有效期限: 此時 + 24小時
		$now = time() + 86400;
		$offtime = date("Y-m-d H:i:s", $now);
		
		for ($i=0; $i < $oscode_cnt; $i++) {
			
			//新增送券紀錄
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
				SET 
					prefixid='saja',
					userid='{$userid}',
					productid='0',
					spid='{$activity_type}',
					behav='d',
					amount=1,
					remainder=1,
					offtime='{$offtime}',
					insertt=NOW() ";
			$db->query($query);
			$scodeid = $db->_con->insert_id;
		}

		// 寫入活動紀錄
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
			SET `insertt`=NOW(), `switch`='Y'
			WHERE `userid`='{$userid}'
			AND `activity_type`= {$activity_type}";
		$db->query($query);

		$log = date('Ymd H:i:s') .' - '. $userid .'_'. $activity_type .'_'. $oscode_cnt;
		// 2019/11/14 麻煩不要修改 AARONFU
		error_log("[ajax/app_act/oscode_19] ". $log);
		
		return 1;
	}
	
	//2019/11/14 AARONFU 送幣
	/*
	public function spoint_19($userid, $intro_by, $activity_type=19) {
		global $db, $config, $usermodel;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
			
		//備用贈送活動表
		$query_action = "SELECT `ontime`,`offtime`
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}action`
			WHERE `activity_type` = '{$activity_type}' ";
		//$table_action = $db->getQueryRecord($query_action);
			
		// 延遲一點時間, 0.05 ~ 0.2 秒不等
		$sleep = 50000 * (getRandomNum()%4+1);
		usleep($sleep);
		
		//新增送幣紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`behav`='gift',
			`amount`='{$spoint_cnt}',
			`remark`='{$activity_type}',
			`insertt`=NOW()
		";
		$db->query($query);
		$spointid = $db->_con->insert_id;

		// 新增活動送幣使用紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` 
		SET
			`userid`='{$userid}',
			`behav`='gift',
			`activity_type` = '{$activity_type}',
			`amount_type` = '1',
			`free_amount` = '{$spoint_cnt}',
			`total_amount`= '{$spoint_cnt}',
			`spointid`= '{$spointid}',
			`insertt`=NOW()
		";
		$db->query($query);
		
		// 寫入活動紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
			SET
				`userid`='{$userid}',
				`phone`='{$phone}',
				`activity_type`= {$activity_type},
				`insertt`=NOW()";
		$db->query($query); 
		

		$log = date('Ymd H:i:s') .' - '. $userid .'_'. $intro_by .'_'. $spoint_cnt_intro;
		// 2019/11/14 麻煩不要修改 AARONFU
		error_log("[ajax/user/spoint_8] ". $log);
		
		return 1;
	}
	*/
	
}
?>
