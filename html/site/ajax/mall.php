<?php
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
   foreach($_POST2 as $k=> $v) {
      $_POST[$k]=$v;
	  // error_log("[ajax/mall] ".$k."=>".$v);
	  if($_POST['client']['json']) {
	     $_POST['json']=$_POST['client']['json'];
	  }
	  if($_POST['client']['auth_id']) {
	     $_POST['userid']=$_POST['client']['auth_id'];
	  }
   }
}

session_start();
// 1. 要接收 $json, 
$ret=null;
$json=(empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
$type=(empty($_POST['type'])) ? $_GET['type'] : $_POST['type']; 
if(empty($_SESSION['auth_id']) && empty($_POST['auth_id'])) { //'請先登入會員帳號'
	
	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';	
	} else {
	   $ret['status'] = 1;
	}
	echo json_encode($ret);
	exit;
} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/convertString.ini.php");
	include_once(LIB_DIR ."/ini.php");
	//$app = new AppIni; 
	
	$c = new Mall;
	if ($type=="qrcode") {
		$c->qrcode();
	} elseif ($type=="exchange") {
		// 2020/03/12 AARONFU 重大變革轉由 /site/controller/mall/checkout() 處理 
		// 要接收 type="exchange"
		$c->home();
	} elseif ($type=="checkexpwd") {
		// 要接收 type="checkexpwd"
		$c->checkpwd();
	}
}
	
class Mall {
	public $userid = '';
	public $json = '';
	public $epid = '';
	public $esid = '';

	//下標
	public function home() {
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		$json 	= (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$userid = (empty($_SESSION['auth_id'])) ? $_POST['auth_id']:$_SESSION['auth_id']; 
		$epid 	= (empty($_POST['epid'])) ? $_GET['epid'] : $_POST['epid'];
		$esid 	= (empty($_POST['esid'])) ? $_GET['esid'] : $_POST['esid'];	
		$note 	= (empty($_POST['note'])) ? $_GET['note'] : $_POST['note'];		
		$order_info['note'] = $note;
		$this->userid = $userid;
		$ret['status'] = 0;
		
		//查詢兌換 狀態 AARONFU
		$bid_able = query_bid_able($this->userid);
		if($bid_able["exchange_enable"] !=='Y') {
			if($json=='Y') {
				$ret['retCode']=10023;
				$ret['retMsg']='兌換功能鎖定中,請洽客服';
				echo json_encode($ret);
				exit;
			} else {
				$ret['status'] = 23;
			}
		}
	
		//兌換數量
		$num = isset($_POST['num']) ? (int)$_POST['num'] : (int)$_GET['num'];
		$order_info['phone'] = isset($_POST['phone']) ? $_POST['phone'] : $_GET['phone'];
		$total = isset($_POST['total']) ? (int)$_POST['total'] : (int)$_GET['total'];
		
		// Check Variable Start
		
		if(empty($num) || empty($epid)) {
			$ret['status'] = 0;
			if($json=='Y') {
				$ret['retCode']=-7;
				$ret['retMsg']='兌換商品或數量錯誤 !!';	
				echo json_encode($ret);	
				exit;
			}
		}
		
		/* 20160624*/
		$chk = $this->expw_check();
		if($chk['err']) {
			$ret['status'] = $chk['err'];
			//$json==Y, 回傳$ret 然後exit 
			if($json=='Y') {
				$ret['retCode']=-6;
				$ret['retMsg']='兌換密碼錯誤 !!';	
				echo json_encode($ret);	
				exit;
			}
		}
		
		//判斷鯊魚點來源是否正常
		$mall_check = $this->deposit_bonus_check($this->userid);
		error_log("[ajax/mall/home] mall_check : ".json_encode($mall_check));

		if(!empty($mall_check['err'])) {
			if($json=='Y') {
				$ret['retCode']=10026;
				$ret['retMsg']='鯊魚點資料異常 請洽客服!!';	
				echo json_encode($ret);	
				exit;
			} else {
				$ret['status'] = 26;
			}
		}
		
		/* 
		// 未使用  暫時disable
		$query ="SELECT pc.epcid 
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` pc 
		left outer join `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category_rt` rt on 
			pc.prefixid = rt.prefixid 
			AND pc.epcid = rt.epcid
			AND rt.switch = 'Y'
		WHERE 
			pc.prefixid = '{$config['default_prefix_id']}' 
			AND rt.epid = '{$epid}' 
			AND pc.switch = 'Y' 
			ORDER BY pc.seq
		";
		$epcArr = $db->getQueryRecord($query); 
		*/
		
		// 20200113檢查用戶是否有異常下標(出價為商品市價8成以上)現象
/*		$chk = $this->chk_strange_bid_history($this->userid);
		if($chk['err']) {
			$ret['status'] = $chk['err'];
			//$json==Y, 回傳$ret 然後exit 
			if($json=='Y') {
				$ret['retCode']=-26;
				$ret['retMsg']='兌換資格異常  請洽客服 !!';	
				echo json_encode($ret);	
				exit;
			}
		} */
		
		if(empty($ret['status'])) {
			$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
			WHERE 
				p.`prefixid` = '{$config['default_prefix_id']}'  
				AND p.epid = '{$epid}'
				AND p.switch = 'Y'
				LIMIT 1
			" ;
			$table = $db->getQueryRecord($query); 

			$retail_price = ($table['table']['record'][0]['retail_price']) ? (float)$table['table']['record'][0]['retail_price'] : 0;
			$cost_price = ($table['table']['record'][0]['cost_price']) ? (float)$table['table']['record'][0]['cost_price'] : 0;
			$process_fee = ($table['table']['record'][0]['process_fee']) ? (float)$table['table']['record'][0]['process_fee'] : 0;
			$order_info['point_price'] = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;
			$order_info['eptype'] = ($table['table']['record'][0]['eptype']) ? $table['table']['record'][0]['eptype'] : 0;
			$order_info['num'] = $num * $table['table']['record'][0]['set_qty'];

			if(empty($esid)) {
				$esid = $table['table']['record'][0]['esid'];				
			}
			//分潤=(市價-進貨價)*數量
			$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

			//商品兌換總點數
			$used_point = $order_info['point_price'] * $order_info['num'];

			//商品處理費總數 $process_fee * $num;
			$order_info['used_process'] = $process_fee;

			//總費用
			$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			
			if($epid == 525) {
			
				$retail_price = $total;
				$cost_price = $total;
				$process_fee = 0;
				$order_info['point_price'] = $total;

				//分潤=(市價-進貨價)*數量
				$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

				//商品兌換總點數
				$used_point = $order_info['point_price'] * $order_info['num'];

				//商品處理費總數 $process_fee * $num;
				$order_info['used_process'] = $process_fee;

				//總費用
				$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			}
			
			if($epid == 689) {
			
				$retail_price = $total;
				$cost_price = $total;
				$process_fee = $total - ($order_info['point_price'] * $num);

				//分潤=(市價-進貨價)*數量
				$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

				//商品兌換總點數
				$used_point = $order_info['point_price'] * $order_info['num'];

				//商品處理費總數 $process_fee;
				$order_info['used_process'] = $process_fee;

				//總費用
				$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			}
		
			//檢查使用者的紅利點數
			$query ="SELECT SUM(amount) bonus 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `switch` = 'Y'
				AND `userid` = '{$this->userid}' 
			";
			$recArr = $db->getQueryRecord($query); 
			$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;

			if($user_bonus < $order_info['total_fee']) {
				//('红利點數不足');	
				if($json=='Y') {
					$ret['retCode']=-5;
					$ret['retMsg']='红利點數不足 !!';
					echo json_encode($ret);
					exit;
				} else {
					$ret['status'] = 5;
				}
			}
			
			//查詢企業id
			$query = "SELECT enterpriseid 
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` 
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `esid` = '{$esid}' 
				AND `switch` = 'Y'
			";
			$recArr = $db->getQueryRecord($query); 
			$order_info['enterpriseid'] = $recArr['table']['record'][0]['enterpriseid'];
		}
		
		
		if(empty($ret['status'])) {
			//產生訂單記錄
			$mk = $this->mk_order($order_info);

			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			
			if($this->userid == 1777 || $this->userid == 116 || $this->userid == 28) {
				
				//查詢企業id
				$query = "SELECT profit_ratio 
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` 
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `enterpriseid` = '{$order_info['enterpriseid']}' 
					AND `switch` = 'Y'
				";
				$recArr = $db->getQueryRecord($query); 
				$profit_ratio = ($recArr['table']['record'][0]['profit_ratio']) ? (float)$recArr['table']['record'][0]['profit_ratio'] : 0;

				$postdata['epid'] = $epid;
				$postdata['orderid'] = $mk['orderid'];
				$postdata['userid'] = $this->userid;

				if (($order_info['profit'] > 0) && ($cost_price > 0)) {
					$postdata['amount'] = $order_info['profit'];
				} else {
					$postdata['amount'] = $order_info['total_fee'] * ($profit_ratio/1000);
				}
				
				$url = "https://www.saja.com.tw/site/mall/giftDistributionProfits";
				//設定送出方式-POST
				$stream_options = array(
					'http' => array (
							'method' => "POST",
							'content' => json_encode($postdata),
							'header' => "Content-Type:application/json" 
					) 
				);
				$context  = stream_context_create($stream_options);
				// 送出json內容並取回結果
				$response = file_get_contents($url, false, $context);
				error_log("[ajax/mall/home] response : ".$response);
				
				// 讀取json內容
				$arr = json_decode($response,true);	
				error_log("[ajax/mall/home] arr : ".json_encode($arr));
			
			}
			
		}

		if ($json=='Y') {
			if (empty($mk['orderid'])) {
				$ret['retCode']=-1;
                $ret['retType']='MSG';
				$ret['retMsg']='兌換失敗 !!';
			} else {
				$ret['retCode']=1;
				$ret['retMsg']='OK';
                $ret['retType']='OBJ';
				$ret['retObj']['orderid']=$mk['orderid'];		
			}
			unset($ret['status']);
			error_log("[ajax/mall/home] exchange OK:".json_encode($ret));
			echo json_encode($ret);
			exit;
		} else {
		    error_log("[ajax/mall/home]:".json_encode($ret));
			echo json_encode($ret);
			exit;
		}
	}
	
	//qrcode兌換商品
	public function qrcode() {
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
		
		//兌換數量
		$order_info['num'] = 1;
		
		// Check Variable Start
		
		if(empty($order_info['num']) || empty($_POST['epid'])) {
			$ret['status'] = 0;
		}
		
		//查詢兌換密碼
		$chk = $this->expw_check();
		
		if($chk['err']) {
			$ret['status'] = $chk['err'];
		}
		error_log("chk[err]:".$chk['err']);
		if(empty($ret['status'])) {
			if ($_POST['epid'] > 0) {
				$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
				FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
				WHERE 
					p.`prefixid` = '{$config['default_prefix_id']}'  
					AND p.epid = '{$_POST['epid']}'
					AND p.switch = 'Y'
					LIMIT 1
				" ;
				$table = $db->getQueryRecord($query); 

				$retail_price = ($table['table']['record'][0]['retail_price']) ? (float)$table['table']['record'][0]['retail_price'] : 0;
				$cost_price = ($table['table']['record'][0]['cost_price']) ? (float)$table['table']['record'][0]['cost_price'] : 0;
				$process_fee = ($table['table']['record'][0]['process_fee']) ? (float)$table['table']['record'][0]['process_fee'] : 0;
				$order_info['point_price'] = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;
	            
				//分潤=(市價-進貨價)*數量
				$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];
	            
				//商品兌換總點數
				$used_point = $order_info['point_price'] * $order_info['num'];
	            
				//商品處理費總數 $process_fee * $num;
				$order_info['used_process'] = $process_fee;
	            
				//總費用
				$order_info['total_fee'] = $order_info['used_process'] + $used_point;
				
				$esid = $table['table']['record'][0]['esid'];
			} else {
            	$query ="select * from `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
            	WHERE 
					`prefixid` = '{$config['default_prefix_id']}'  
					AND orderid = '{$_POST['orderid']}'
					AND switch = 'Y'
					LIMIT 1
				";
				$table = $db->getQueryRecord($query); 
				
				$order_info['point_price'] = $table['table']['record'][0]['point_price'];
				$order_info['used_process'] = 0;
				$order_info['total_fee'] = $table['table']['record'][0]['total_fee'];
				$order_info['profit'] = 0;
            }
            
			//檢查使用者的紅利點數
			$query ="SELECT SUM(amount) bonus 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `switch` = 'Y'
				AND `userid` = '{$this->userid}' 
			";
			$recArr = $db->getQueryRecord($query); 
			$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;
            
			if($user_bonus < $order_info['total_fee']) {
				//('红利點數不足');	
				$ret['status'] = 5;
			}
		}
		
		if(empty($ret['status'])) {
			//產生訂單記錄
			$mk = $this->set_order($order_info);
				
			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;

			if ($this->userid == 1777 || $this->userid == 116 || $this->userid == 28) {
				

				//查詢企業id
				$query = "SELECT enterpriseid 
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` 
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `esid` = '{$esid}' 
					AND `switch` = 'Y'
				";
				$recArr = $db->getQueryRecord($query); 
				$enterpriseid = $recArr['table']['record'][0]['enterpriseid'];
			
				//查詢企業id
				$query = "SELECT profit_ratio 
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` 
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `enterpriseid` = '{$enterpriseid}' 
					AND `switch` = 'Y'
				";
				$recArr = $db->getQueryRecord($query); 
				$profit_ratio = ($recArr['table']['record'][0]['profit_ratio']) ? (float)$recArr['table']['record'][0]['profit_ratio'] : 0;

				$postdata['epid'] = $_POST['epid'];
				$postdata['orderid'] = $_POST['orderid'];
				$postdata['userid'] = $this->userid;
				
				if (($order_info['profit'] > 0) && ($cost_price > 0)) {
					$postdata['amount'] = $order_info['profit'];
				} else {
					$postdata['amount'] = $order_info['total_fee'] * ($profit_ratio/1000);
				}
				
				$url = "https://www.saja.com.tw/site/mall/giftDistributionProfits";
				//設定送出方式-POST
				$stream_options = array(
					'http' => array (
							'method' => "POST",
							'content' => json_encode($postdata),
							'header' => "Content-Type:application/json" 
					) 
				);
				$context  = stream_context_create($stream_options);
				// 送出json內容並取回結果
				$response = file_get_contents($url, false, $context);
				error_log("[ajax/mall/home] response : ".$response);
				
				// 讀取json內容
				$arr = json_decode($response,true);	
				error_log("[ajax/mall/home] arr : ".json_encode($arr));
			}			
		}
		echo json_encode($ret);
	}
	
	//產生訂單記錄
	public function mk_order($order_info) {
		$time_start = microtime(true);

		global $db, $config;
		$userid = (empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		$epid 	= (empty($_POST['epid'])) ? $_GET['epid'] : $_POST['epid'];
		$esid 	= (empty($_POST['esid'])) ? $_GET['esid'] : $_POST['esid'];	
		$r['err'] = '';
		
		if($epid == 525) {
			$memo = 'tel:'.$order_info['phone'];
		} else {
			$memo = '';
		}
		
		$query ="SELECT p.*  
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
		WHERE 
			p.`prefixid` = '{$config['default_prefix_id']}'  
			AND p.epid = '{$epid}'
			AND p.switch = 'Y'
			LIMIT 1
		" ;
		$table2 = $db->getQueryRecord($query); 		
		if(empty($esid)) {
			$esid = $table2['table']['record'][0]['esid'];				
		}
		
		if(empty($r['err'])) {
			//新增訂單
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`status`='0',
				`epid`='{$epid}',
				`esid`='{$esid}',
				`num`='{$order_info['num']}',
				`point_price`='{$order_info['point_price']}',
				`process_fee`='{$order_info['used_process']}',
				`total_fee`='{$order_info['total_fee']}',
				`profit`='{$order_info['profit']}',
				`memo` = '{$memo}',
				`note` = '{$order_info['note']}',  
				`confirm`='Y',
				`insertt`=NOW()
			";
			$res = $db->query($query); 
			$orderid = $db->_con->insert_id;

			//商品卡
			if($order_info['eptype'] == 2 || $order_info['eptype'] == 3){

				$query ="SELECT cid
				FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card` 
				WHERE 
					epid = '{$epid}'
					AND userid = '0'
					AND orderid = '0'
					AND used = 'N'
					AND switch = 'Y'
					ORDER BY cid ASC,insertt ASC
					LIMIT {$order_info['num']}
				";

				$table3 = $db->getQueryRecord($query);
				//商品卡數量
				$card_num = count($table3['table']['record']);
				//商品卡庫存 = 商品卡數量 - 預留庫存
				$stock = $card_num - $order_info['reserved_stock'];

				if($stock >= $order_info['num']){		//庫存足夠

					$cid_value = '';
					foreach($table3['table']['record'] as $row){
						$cid_value .= $row['cid'].',';
					}
					//將cid組合成字串
					$cid_value = rtrim($cid_value,',');

					$db->query('start transaction');
					$affected_num = 0;
					$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card` 
					set 
						`userid` = '{$this->userid}',
						`orderid` = '{$orderid}',
						`used` = 'Y'
					WHERE `cid` IN ({$cid_value}) AND `userid` = 0 AND `orderid` = 0 AND `used` = 'N'";

					$res = $db->query($query);
					//update的資料筆數
					$affected_num = $db->_con->affected_rows;

					if($affected_num == $order_info['num']){		//更新比數與兌換數量符合
						$db->query('COMMIT');

						//商品卡訂單直接更新為已到貨狀態
						$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` set 
						`status` = '4'
						WHERE `orderid` = '{$orderid}'";
						$res = $db->query($query);
						
					}else{											//更新比數與兌換數量不符合
						$db->query('ROLLBACK');

						//刪除訂單
						$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` set 
						`switch` = 'N'
						WHERE `orderid` = '{$orderid}'";
						$res = $db->query($query);

						error_log("[ajax/mall/mk_order]:stock_not_enough，delete_order:".$orderid);
						$r['err'] = -2;
						return $r;
					}
			
				}else{		//庫存不足
					
					//刪除訂單
					$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` set 
					`switch` = 'N'
					WHERE `orderid` = '{$orderid}'";
					$res = $db->query($query);

					error_log("[ajax/mall/mk_order]:stock_not_enough，delete_order:".$orderid);
					$r['err'] = -2;
					return $r;
				}
			}

			$r['orderid'] = (empty($orderid)) ? '' : $orderid;
			
			//查詢使用者收件相關資料
			$query = "SELECT addressee, area, address, phone, gender
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
			WHERE prefixid = '{$config['default_prefix_id']}' AND userid = '{$this->userid}' ";
			$consignee = $db->getQueryRecord($query);			
			
			//判定是否傳值,無傳值則使用查詢資料替代
			$name = (empty($_POST['name'])) ? $consignee['table']['record'][0]['addressee'] : $_POST['name'];
			$zip = (empty($_POST['zip'])) ? $consignee['table']['record'][0]['area'] : $_POST['zip'];
			$address = (empty($_POST['address'])) ? $consignee['table']['record'][0]['address'] : $_POST['address'];
			$phone = (empty($_POST['phone'])) ? $consignee['table']['record'][0]['phone'] : $_POST['phone'];
			$gender = (empty($_POST['gender'])) ? $consignee['table']['record'][0]['gender'] : $_POST['gender'];
			
			//新增訂單收件人
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` 
			SET 
				`userid`='{$this->userid}',
				`orderid`='{$orderid}',
				`name`='{$name}',
				`phone`='{$phone}',
				`zip`='{$zip}',
				`address`='{$address}',
				`gender`='{$gender}',
				`prefixid`='{$config['default_prefix_id']}',
				`insertt`=now()
			";
			$db->query($query); 
			
			//扣除點數
			$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$this->userid}', 
			          `countryid` = '{$config['country']}', 
					  `orderid`='{$orderid}',
			          `behav` = 'user_exchange', 
			          `amount` = '-{$order_info['total_fee']}', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bonusid = $db->_con->insert_id;
			error_log("[ajax/mall/mk_order] bonus:".json_encode($query));
			//商家增加點數(Frank-14/12/16)
			$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `bonusid` = '{$bonusid}', 
			          `esid` = '{$esid}', 
			          `enterpriseid` = '{$order_info['enterpriseid']}', 
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', 
			          `amount` = '{$order_info['total_fee']}', 
					  `bonus_pay`='0',
					  `cash_pay`='0',
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bsid = $db->_con->insert_id;
			error_log("[ajax/mall/mk_order] bonus_store:".json_encode($query));
			//新增紅利點數歷史資料(Frank-14/12/16)
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_store_history` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `esid` = '{$esid}', 
			          `orderid` = '{$orderid}',
			          `bsid` = '{$bsid}',
			          `enterpriseid` = '{$order_info['enterpriseid']}', 
			          `amount` = '{$order_info['total_fee']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			error_log("[ajax/mall/mk_order] exchange_bonus_store_history:".json_encode($query));
			//新增商家紅利點數歷史資料
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$this->userid}', 
			          `orderid` = '{$orderid}',
			          `bonusid` = '{$bonusid}',
			          `amount` = '-{$order_info['total_fee']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			error_log("[ajax/mall/mk_order] exchange_bonus_history:".json_encode($query));
			
			//圓夢券
			if ($order_info['eptype'] == 5){
				for($i = 0; $i < $order_info['num']; $i++){ 
					$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode` SET
						`prefixid`='saja',
						`userid`='{$this->userid}',
						`orderid`='{$orderid}',
						`used`='N',
						`amount`='1',
						`behav`='user_exchange',
						`feedback`='N',
						`switch`='Y' ,
						`insertt`=NOW()";
					$res = $db->query($query); 
					$dscode = $db->_con->insert_id;
				}
			}
			
			if($order_info['eptype'] != 2 && $order_info['eptype'] != 3 && $order_info['eptype'] != 4){
				//扣除庫存
				$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}stock` set 
						`prefixid` = '{$config['default_prefix_id']}', 
						`behav` = 'user_exchange', 
						`epid` = '{$epid}',
						`num` = '-{$order_info['num']}', 
						`orderid` = '{$orderid}',
						`seq` = '0', 
						`switch` = 'Y', 
						`insertt` = now()";
				$db->query($query);
			}

			// 計算免費鯊魚點餘額並更新
			$query = "SELECT sum(free_amount) total_free_amount
				FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history`
				WHERE `userid`='{$this->userid}'
				AND   `switch` = 'Y'
				AND   `amount_type` = 2
				";
			$table = $db->getQueryRecord($query); 
			
			if ($table['table']['record'][0]['total_free_amount'] > 0) {

				$free_amount = ($table['table']['record'][0]['total_free_amount'] >= $order_info['total_fee']) ? $order_info['total_fee'] : $table['table']['record'][0]['total_free_amount'];
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` 
				SET
					`userid`='{$this->userid}',
					`behav` = 'user_exchange', 
					`amount_type` = 2, 
					`free_amount` = '-{$free_amount}', 
					`total_amount` = '-{$order_info['total_fee']}', 
					`bonusid` = '{$bonusid}', 
					`orderid` = '{$orderid}',
					`switch` = 'Y', 
					`insertt` = now()
				";
				$db->query($query); 
			}
		}
		
		return $r;
	}
	
	//兌換密碼確認
	public function expw_check() {
		$time_start = microtime(true);
		
		global $db, $config;
		
		$this->str = new convertString();
		$exchange_passwd = htmlspecialchars($_REQUEST['expw']);
		$exchangepasswd = $this->str->strEncode($exchange_passwd, $config['encode_key']);
		$userid = empty($_REQUEST['userid']) ? $_SESSION['auth_id'] : htmlspecialchars($_REQUEST['userid']); 
		if(empty($userid)) {
			$userid=$_POST['auth_id'];
		}	
		$r['err'] = '';
		
		$query = "SELECT * 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$userid}' 
			AND exchangepasswd = '{$exchangepasswd}' 
			AND switch = 'Y' 
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			//'兌換密碼錯誤'
			$r['retCode']=-6;
			$r['retMsg']='兌換密碼錯誤 !!';				
			$r['err'] = 6;
		} else {
			$r['retCode']=1;
			$r['retMsg']='兌換密碼正確 !!';	
		}
        return $r;
	}
	
	//更改訂單記錄
	public function set_order($order_info) {
		$time_start = microtime(true);
		
		global $db, $config;
		
		$r['err'] = '';
		
		if(empty($r['err'])) {
			//更新訂單
			$query = "update `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			SET
				confirm = 'Y', 
				modifyt = NOW()
			where 
				orderid = '{$_POST['orderid']}'
				and userid = '{$_SESSION['auth_id']}'
				and confirm = 'N'
				and switch = 'Y'
			";
			
			$res = $db->query($query); 
			
			//扣除點數
			$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$_SESSION['auth_id']}', 
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', 
			          `amount` = '-{$order_info['total_fee']}', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bonusid = $db->_con->insert_id;
			
			//新增紅利點數歷史資料
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$_SESSION['auth_id']}', 
			          `orderid` = '{$_POST['orderid']}',
			          `bonusid` = '{$bonusid}',
			          `amount`='-{$order_info['total_fee']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			
			//扣除庫存
			if($_POST['epid'] > 0) {
				$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}stock` set 
				          `prefixid` = '{$config['default_prefix_id']}', 
				          `behav` = 'user_exchange', 
				          `epid` = '{$_POST['epid']}',
				          `num` = '-1', 
				          `orderid` = '{$_POST['orderid']}',
				          `seq` = '0', 
				          `switch` = 'Y', 
				          `insertt` = now()";
				$db->query($query);
			}
		}
		
		return $r;
	}
	
	//商城確認兌換密碼
	public function checkpwd() {
		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		$userid = (empty($_SESSION['auth_id'])) ? $_POST['client']['auth_id']:$_SESSION['auth_id']; 
		
		/* 20160624*/
		$chk = $this->expw_check();

		echo json_encode($chk);	
		exit;				
	}
	
	// add by Thomas 2020/01/13 檢查用戶是否有異常下標紀錄(ex: 出了3次以上超過市價8折的價格 )
	// 有異常的下標紀錄, 可能是故意不得標要換成鯊魚點 & ibon
	public function chk_strange_bid_history($userid) {
		   global $db, $config, $router;

		   // 初始化資料庫連結介面
		   $db = new mysql($config["db"]);
		   $db->connect();
		
		   $query = "SELECT count(shid) cnt 
		               FROM saja_shop.saja_strange_history 
					  WHERE switch='Y' 
					    AND userid='${userid}' ";
		   $table = $db->getQueryRecord($query);
		   error_log("[ajax/mall/chk_strange_bid_history] sql : ".$query."-->".$table['table']['record'][0]['cnt']);
		   $r=array("err"=>"");
		   if($table['table']['record'][0]['cnt']>=3) {
			  $r['err'] =26;
		   } 
           return $r;

	}
	
	// 檢查點數是否有符合正常範圍
	public function deposit_bonus_check($userid) {
		
		global $db, $config;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$query ="SELECT SUM(amount) bonus 
		FROM `saja_cash_flow`.`saja_bonus`  
		WHERE 
			`prefixid` = 'saja' 
			AND `switch` = 'Y'
			AND `userid` = '{$userid}' 
			AND behav in ('product_close')
		";
		$table = $db->getQueryRecord($query); 
		error_log("[ajax/mall/deposit_bonus_check] bonus sql : ".$query."-->".$table['table']['record'][0]['bonus']);
		
		$query2 ="SELECT SUM(amount) spoint 
		FROM `saja_cash_flow`.`saja_spoint`
		WHERE 
			`prefixid` = 'saja' 
			AND `switch` = 'Y'
			AND `userid` = '{$userid}' 
			AND behav in ('user_deposit','gift','system')
		";
		$table2 = $db->getQueryRecord($query2); 
		error_log("[ajax/mall/deposit_bonus_check] spoint sql : ".$query2."-->".$table2['table']['record'][0]['spoint']);
		
		$r=array("err"=>"");
		
		if($table['table']['record'][0]['bonus'] > $table2['table']['record'][0]['spoint']) {
		  $r['err'] =9;
		} 
		return $r;

	}
}
?>