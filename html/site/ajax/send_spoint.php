<?php
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
   foreach($_POST2 as $k=> $v) {
      $_POST[$k]=$v;
	  // error_log("[ajax/mall] ".$k."=>".$v);
	  if($_POST['client']['json']) {
	     $_POST['json']=$_POST['client']['json'];
	  }
	  if($_POST['client']['auth_id']) {
	     $_POST['userid']=$_POST['client']['auth_id'];
	  }
   }
}

session_start();
// 1. 要接收 $json, 
$ret=null;
$json=(empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
$type=(empty($_POST['type'])) ? $_GET['type'] : $_POST['type'];
$userid = (empty($_POST['auth_id'])) ? $_GET['auth_id'] : $_POST['auth_id'];

if(empty($userid)) { //'請先登入會員帳號'
	$ret['retCode']=1;
	$ret['retMsg']='請先登入會員 !';	
	$action_list['type'] = 5;
	$action_list['page'] = 9;
	$action_list['msg'] = "請先登入會員 !";
	$ret['action_list']=$action_list;
	echo json_encode($ret);
	exit;
} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/convertString.ini.php");
	include_once(LIB_DIR ."/ini.php");
	//$app = new AppIni; 
	
	$c = new Sendspoint;
	$c->home();
}

class Sendspoint 
{
	public $str;
	
	public function home() {
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$json 	= (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$userid = (empty($_POST['auth_id'])) ? $_GET['auth_id'] : $_POST['auth_id'];
		$activity_type = (empty($_POST['activity_type'])) ? $_GET['activity_type'] : $_POST['activity_type'];

		switch ($activity_type) {
			case '2':
				$ret['retCode']=-4;
				$ret['retMsg']='活動已結束 !';
				echo json_encode($ret);
				exit;

				$productid = array('10074','10071','10068','10065','10044');
				$oscode_cnt = 5;
				$this->oscode_gift($productid,$oscode_cnt);
				break;

			case '5':
				$ret['retCode']=-4;
				$ret['retMsg']='活動已結束 !';
				echo json_encode($ret);
				exit;

				$productid = '10044';
				$oscode_cnt = 10;
				$spoint_cnt = 500;
				$this->oscode_gift($productid,$oscode_cnt,false);
				$this->spoint_gift($spoint_cnt);
				break;

			case '7':
				$ret['retCode']=-4;
				$ret['retMsg']='活動已結束 !';
				echo json_encode($ret);
				exit;

				$productid = '10473';
				$oscode_cnt = 10;
				$spoint_cnt = 500;
				$this->oscode_gift($productid,$oscode_cnt,false);
				$this->spoint_gift($spoint_cnt);
				break;

			case '9':
				$ret['retCode']=-4;
				$ret['retMsg']='活動已結束 !';
				echo json_encode($ret);
				exit;

				$productid = array('11877','11874','11871');
				$oscode_cnt = 3;
				$this->oscode_gift($productid,$oscode_cnt);
				break;

			case '10':
				if(date('Y-m-d H:i:s') < '2019-10-25 15:00:00' || date('Y-m-d H:i:s') > '2019-10-26 21:30:00'){
					$ret['retCode']=-4;
					$ret['retMsg']='活動已結束 !';
					echo json_encode($ret);
					exit;
				}

				if (!$this->check_verify()) {
					$ret['retCode']=1;
					$ret['retMsg']='請先完成手機驗證';

					$action_list['type'] = 5;
					$action_list['page'] = 10;
					$action_list['msg'] = "請先完成手機驗證";
					$ret['action_list']=$action_list;
					echo json_encode($ret);
					exit;
				}

				$productid = array('12171','12180','12183','12186','12192','12195','12198','12204');
				$oscode_cnt = 3;
				$this->oscode_gift($productid,$oscode_cnt);
				break;

			case '11':
				if(date('Y-m-d H:i:s') < '2019-10-25 15:00:00' || date('Y-m-d H:i:s') > '2019-10-26 21:30:00'){
					$ret['retCode']=-4;
					$ret['retMsg']='活動已結束 !';
					echo json_encode($ret);
					exit;
				}

				if (!$this->check_verify()) {
					$ret['retCode']=1;
					$ret['retMsg']='請先完成手機驗證';

					$action_list['type'] = 5;
					$action_list['page'] = 10;
					$action_list['msg'] = "請先完成手機驗證";
					$ret['action_list']=$action_list;
					echo json_encode($ret);
					exit;
				}

				$productid = array('12171','12180','12183','12186','12192','12195','12198','12204');
				$oscode_cnt = 1;
				$this->oscode_gift($productid,$oscode_cnt);
				break;

			default:
				$ret['retCode']= -2;
				$ret['retMsg']='活動類型錯誤!';
				echo json_encode($ret);	
				exit;
				break;
		} 
	}

	// $productid 商品id 
	// $oscode_cnt 送卷數量
	// $end 是否結束
	public function oscode_gift($productid,$oscode_cnt,$end=true){
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$userid = (empty($_POST['auth_id'])) ? $_GET['auth_id'] : $_POST['auth_id'];
		$activity_type = (empty($_POST['activity_type'])) ? $_GET['activity_type'] : $_POST['activity_type'];

		//檢查userid是否存在
		$query = " SELECT count(userid) as user_cnt 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE `userid`='{$userid}'
		AND `switch`='Y' ";				   
		$chk_user = $db->getQueryRecord($query);

		if($chk_user['table']['record'][0]['user_cnt'] > 0){

			//檢查是否送過殺價卷
			$query = " SELECT count(ahid) as activity_cnt 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
			WHERE `userid`='{$userid}'
			AND `activity_type`='{$activity_type}'
			AND `switch`='Y' ";				   
			$chk = $db->getQueryRecord($query);
			error_log("[ajax/auth] chk : ".$query." ==> ".$chk['table']['record'][0]['activity_cnt']);

			//確認無送劵紀錄
			if($chk['table']['record'][0]['activity_cnt'] == 0){
				if (is_array($productid)) {
					foreach($productid as $row){
						for($i=0; $i < $oscode_cnt; $i++){
							$query = " INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
							SET 
								prefixid='saja',
								userid='{$userid}',
								productid='{$row}',
								spid=0,
								behav='gift',
								used='N',
								used_time='0000-00-00 00:00:00',
								amount=1,
								serial='',
								verified='N',
								switch='Y',
								seq=0,
								remark ='{$activity_type}',
								insertt=NOW() 
							";
							$db->query($query);
						}
					}
				}else{
					for ($i=0; $i < $oscode_cnt; $i++) { 
						$query = " INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
						SET 
							prefixid='saja',
							userid='{$userid}',
							productid='{$productid}',
							spid=0,
							behav='gift',
							used='N',
							used_time='0000-00-00 00:00:00',
							amount=1,
							serial='',
							verified='N',
							switch='Y',
							seq=0,
							remark ='{$activity_type}',
							insertt=NOW() 
						";
						$db->query($query);
					}
				}

				if ($end) {
					//新增活動贈送紀錄
					$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
					SET
						`userid`='{$userid}',
						`activity_type` = '{$activity_type}',
						`insertt`=NOW()
					";
					$db->query($query);
					
					$ret['retCode']= 1;
					$ret['retMsg']='貴賓您好，歡迎來到殺價王~'."\n".'恭喜獲得殺價券！';

					$action_list['type'] = 5;
					$action_list['page'] = 0;
					$action_list['msg'] = '貴賓您好，歡迎來到殺價王~'."\n".'恭喜獲得殺價券！';
					$ret['action_list']=$action_list;
				}
				
			}else{
				//已有活動送幣紀錄
				$ret['retCode']= -3;
				$ret['retMsg']='您已領取過殺價券!';
			}
		}else{
			$ret['retCode']= -5;
			$ret['retMsg']='無此會員編號!';
		}
		if ($end) {
			echo json_encode($ret);
			exit();
		}
	}

	// $spoint_cnt送幣數量
	// $end 是否結束
	public function spoint_gift($spoint_cnt=0, $end=true){
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$userid = (empty($_POST['auth_id'])) ? $_GET['auth_id'] : $_POST['auth_id'];
		$activity_type = (empty($_POST['activity_type'])) ? $_GET['activity_type'] : $_POST['activity_type'];


		//檢查userid是否存在
		$query = " SELECT count(userid) as user_cnt 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE `userid`='{$userid}'
		AND `switch`='Y' ";				   
		$chk_user = $db->getQueryRecord($query);

		if($chk_user['table']['record'][0]['user_cnt'] > 0){

			//檢查是否送過殺價幣
			$query = " SELECT count(ahid) as activity_cnt 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
			WHERE `userid`='{$userid}'
			AND `activity_type`='{$activity_type}'
			AND `switch`='Y' ";				   
			$chk = $db->getQueryRecord($query);

			if($chk['table']['record'][0]['activity_cnt'] == 0){
				//新增送幣紀錄
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$userid}',
					`behav`='gift',
					`amount`='{$spoint_cnt}',
					`remark`='{$activity_type}',
					`insertt`=NOW()
				";
				$db->query($query);
				$spointid = $db->_con->insert_id;

				//新增活動送幣使用紀錄
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` 
				SET
					`userid`='{$userid}',
					`behav`='gift',
					`activity_type` = '{$activity_type}',
					`amount_type` = '1',
					`free_amount` = '{$spoint_cnt}',
					`total_amount`= '{$spoint_cnt}',
					`spointid`= '{$spointid}',
					`insertt`=NOW()
				";
				$db->query($query);

				if ($end) {
					//新增活動贈送紀錄
					$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` 
					SET
						`userid`='{$userid}',
						`activity_type` = '{$activity_type}',
						`insertt`=NOW()
					";
					$db->query($query);
					
					$ret['retCode']= 1;
					$ret['retMsg']='貴賓您好，歡迎來到殺價王~'."\n".'恭喜獲得殺價幣！';

					$action_list['type'] = 5;
					$action_list['page'] = 0;
					$action_list['msg'] = '貴賓您好，歡迎來到殺價王~'."\n".'恭喜獲得殺價幣！';
					$ret['action_list']=$action_list;
				}				
			}else{
				//已有活動送幣紀錄
				$ret['retCode']= -3;
				$ret['retMsg']='您已領取過殺價幣!';
			}
		}else{
			$ret['retCode']= -5;
			$ret['retMsg']='無此會員!';
		}
		if ($end) {
			echo json_encode($ret);
			exit();
		}
	}

	public function check_verify(){
		global $db, $config;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$userid = (empty($_POST['auth_id'])) ? $_GET['auth_id'] : $_POST['auth_id'];

		//檢查userid是否存在
		$query = " SELECT verified
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
			WHERE `userid`='{$userid}'
			AND `switch`='Y' ";				   
		$chk = $db->getQueryRecord($query);
		if($chk['table']['record'][0]['verified'] == 'Y'){
			return true;
		}else{
			return false;
		}
	}
}
?>
