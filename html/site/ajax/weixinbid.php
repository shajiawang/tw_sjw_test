<?php
session_start();
/*
$time_start = microtime(true);

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
*/
$_POST2 = json_decode($HTTP_RAW_POST_DATA,true);

if($_POST2) {
   // error_log($k."=>".$v);
	foreach($_POST2 as $k=> $v) {
		$_POST[$k]=$v;
		if($_POST['client']['json']) {
			$_POST['json']=$_POST['client']['json'];
		}
		if($_POST['client']['auth_id']) {
			$_POST['userid']=$_POST['client']['auth_id'];
		}	  
	}
}

$ret=null;
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 

error_log("[ajax/weixinbid] POST : ".json_encode($_POST));

if(empty($_POST['userid']) && empty($_POST['root'])) { //'請先登入會員帳號'
	
	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';	
	}
	echo json_encode($ret);
	exit;
} 
else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/ini.php");
 	include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");
	require_once("/var/www/lib/vendor/autoload.php");  
	//$app = new AppIni; 
	
	$c = new ProductBid;		
	$c->saja();
}

use WebSocket\Client;

class ProductBid 
{
	public $userid = '';
	public $countryid = 2;//國家ID
	public $currency_unit = 0.01;//貨幣最小單位
	public $last_rank = 11;//預設順位提示（順位排名最後一位）
	public $saja_time_limit = 0;//每次下標時間間隔限制
	public $display_rank_time_limit = 60;//（秒）下標後離結標還有n秒才顯示順位
	public $range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標
	public $tickets = 1;
	public $price;
	public $username='';
	
	//下標
	public function saja()
	{	
		$time_start = microtime(true);
				
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$JSON = $_POST['json'];
		
		$router = new Router();
		
		$this->userid = (empty($_POST['userid']) ) ? $_SESSION['auth_id'] : $_POST['userid'];	
		$this->username= (empty($_SESSION['user']) ) ? $_POST['nickname'] : $_SESSION['user']['profile']['nickname'];	
		
		if (empty($this->username)){

			$query ="SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`  
			WHERE prefixid = '{$config['default_prefix_id']}'
			  AND userid = '{$this->userid}'
			  AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$this->username = $table['table']['record'][0]['nickname']; 		

		}
		
		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['status'] = 0;
		$ret['winner'] = '';
		$ret['rank'] = '';
		$ret['value'] = 0;
		
		//讀取商品資料
		$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
		WHERE  p.prefixid = '{$config['default_prefix_id']}'
		  AND p.productid = '{$_POST['productid']}'
		  AND p.switch = 'Y'
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		// error_log("111111");
		unset($table['table']['record'][0]['description']);
		$product = $table['table']['record'][0]; 

		if( ($product['offtime'] == 0 && $product['locked'] == 'Y') || ($product['offtime'] > 0 && $product['now'] > $product['offtime']) ) {
			//回傳: 本商品已结標
			$ret['status'] = 2;
			if($JSON=='Y') {
				$ret=getRetJSONArray(-2,'本商品已结標!!','MSG');
				echo json_encode($ret);
				exit;
			}
		}
		
		if ($product['is_flash']!="Y"){
			//下標資格
			$chk = $this->chk_saja_rule($product);
			if ($chk['err'] && $chk['err']>0) {
				if($JSON=='Y') {
					$ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],$chk['retType']);
					echo json_encode($ret);
					exit;
				} else {
					$ret['status'] = $chk['err'];
					$ret['value'] = $chk['value'];
				}
			}
		}
		
		if($_POST['type']=='single') { //單次下標
			$chk = $this->chk_single($product);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
			    }
			} 
			
		} else if($_POST['type']=='range' ) { //區間連續下標
			$chk = $this->chk_range($product);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
			       echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
				}
			}
			
		} 
		
		if ($_POST['pre_check']=='Y'){
			echo json_encode($ret);
			exit;	
		}
		error_log("6666666");

		if(empty($ret['status']) || $ret['status']==0 ) {
			error_log("7777777");
			//產生下標歷史記錄
			$mk = $this->mk_history($product);
			
			//回傳: 下標完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;			
			if($mk['err']) {
				// 有異常
				if($JSON=='Y') {
				   $ret=getRetJSONArray($mk['retCode'],$mk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				}   
			}
			error_log("888888");
			// 正常下標
			if($JSON=='Y') {
			    error_log("999999");
				$ret['retObj']=array();
			    $ret['retObj']['winner'] = $mk['winner'];
				$ret['retObj']['rank'] = $mk['rank'];
				
				//單次下標
				if($_POST['type']=='single') {
                     error_log("single");
					if ($mk['rank']==-1) {
						$msg = '很抱歉！这个價钱与其他人重复了！請您试试其他價钱或许会有好运喔！';
					} else if($mk['rank']==0) {
						$msg = '恭喜！您是目前的中標者！记得在時间到之前要多布局！';
					} else if ($mk['rank']>=1 && $mk['rank']<=10) {
						$msg = '恭喜！这个價钱是「唯一」但不是「最低」目前是「唯一」又比您「低」的还有 '.($mk['rank']).' 位。';
					} else if ($mk['rank']>10) {
						$msg = '恭喜！这个價钱是「唯一」但不是「最低」目前是「唯一」又比您「低」的超過 10 位。';
					}

					$ret['retMsg'] = '單次出價 '.$_POST['price'].'元 成功 '. $msg;
						

					
				} else if($_POST['type']=='range' || $_POST['type']=='lazy') {
					error_log("range");
					$ret['retObj']['total_num'] = $mk['total_num'];
					$ret['retObj']['num_uniprice'] = $mk['num_uniprice'];
					$ret['retObj']['lowest_uniprice'] = $mk['lowest_uniprice'];
					
					if($mk['total_num']>=2) {
						if($mk['num_uniprice']>0) {
						   $msg ='本次有 '.$mk['num_uniprice'].' 个出價是唯一價, 其中最低價格是 '.$mk['lowest_uniprice'].' 元 !';
						} else {
						   $msg ='本次下標的出價都与其他人重复了 !';
						}
					}
					$ret['retMsg'] = '连续出價 '.$_POST['price_start'].'至'.$_POST['price_stop'].'元 成功 !'.$msg;
					
				}
				
			} else {
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['total_num'] = $mk['total_num'];
					$ret['num_uniprice'] = $mk['num_uniprice'];
					$ret['lowest_uniprice'] = $mk['lowest_uniprice'];
				}
			}
		}
		error_log("[ajax/weixinbid] saja : ".json_encode($ret));
		echo json_encode($ret);
	}
	
	//下標資格
	public function chk_saja_rule($product, $userid='')
	{
		global $db, $config;
		
		if(empty($userid))
		   $userid=$this->userid;
		
		$db = new mysql($config["db"]);
		$db->connect();
		$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt` 
		where 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND productid = '{$product['productid']}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		$r['err'] = '';
		$r['value'] = 0;
		$count = $this->pay_get_product_count($userid);
		
		if ($table['table']['record'][0]['srid'] == 'never_win_saja') {		//新手(未曾得標)
			if ($count > 0) {
				$r['err'] = 15;
				$r['retCode']=-100215;
				$r['retMsg']='未符合(未得標新手)資格!!';
			}
		}
		else if ($table['table']['record'][0]['srid'] == 'le_win_saja') {	//得標次數n次以內
			if ($count > $table['table']['record'][0]['value']) {
				$r['err'] = 16;
				$r['value'] = $table['table']['record'][0]['value'];
				$r['retCode']=-100216;
				$r['retMsg']='未符合下標資格!!';
			}
		}
		else if ($table['table']['record'][0]['srid'] == 'ge_win_saja') {	//得標次數n次以上
			if ($count < $table['table']['record'][0]['value']) {
				$r['err'] = 17;
				$r['value'] = $table['table']['record'][0]['value'];
				$r['retCode']=-100217;
				$r['retMsg']='未符合老手資格!!';
			}
		}
		return $r;
			
	}
	
	//得標次數限制
	public function pay_get_product_count($userid)
	{
		global $db, $config;
		
		$query = "SELECT count(*) as count 
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`  
		where 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND userid = '{$userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		return $table['table']['record'][0]['count'];
	}
	
	// 連續下標
	public function chk_range($product)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		$r['err'] = '';
		
		if (empty($_POST['price_start']) || empty($_POST['price_stop']) ) {
		    //'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		}
		elseif (!is_numeric($_POST['price_start']) || !is_numeric($_POST['price_stop']) ) {
		    //'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		}
		elseif ((float)$_POST['price_start'] < (float)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		}elseif ($product['offtime'] > 0 && 
		       ($product['offtime'] - $product['now']<$this->range_saja_time_limit) ) {
		    //'超過可连续下標時间'
			$r['err'] = 7;
			$r['retCode']=-100227;
			$r['retMsg']='超過可连续下標時间!';
		}
		elseif ((float)$_POST['price_start'] > (float)$_POST['price_stop']) {
		    //'起始價格不可超過结束價格'
			$r['err'] = 9;
			$r['retCode']=-100229;
			$r['retMsg']='起始價格不可超過结束價格!';
		}
		elseif ((float)$_POST['price_start'] == (float)$_POST['price_stop']) {
		    //'出價起讫區间不可相同'
			$r['err'] = 12;
			$r['retCode']=-100232;
			$r['retMsg']='出價起讫區间不可相同!';
		}	
		elseif ((float)$_POST['price_stop'] > (float)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		}
		elseif ((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid']) ) {
		    //'超過可下標次數限制'
			$r['err'] = 11;
			$r['retCode']=-100231;
			$r['retMsg']='超過可下標次數限制!';
		}
				
		//計算本次下標數
		$this->tickets = ((float)$_POST['price_stop']-(float)$_POST['price_start'])/$this->currency_unit + 1;
		error_log("本次下標數：".$this->tickets);
		$this->price = $_POST['price_start'];
		
		if ($this->tickets > (int)$product['usereach_limit']) {
		    //'超過可连续下標次數限制'
			$r['err'] = 8;
			$r['retCode']=-100228;
			$r['retMsg']='超過可連續下標次數限制!';
		}
		return $r;
	}
	
	// 單次下標
	public function chk_single($product)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		//殺價記錄數量
		$this->tickets = 1;
		
		//殺價起始價格
		$this->price = $_POST['price'];
		
		$r['err'] = '';
		
		if (empty($this->price)) {
		    //'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		}
		elseif (!is_numeric($this->price) ) {
		    //'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		}
		elseif ((float)$this->price < (float)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		}
		elseif ((float)$this->price > (float)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		}
		elseif ((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid']) ) {
		    //'超過可下標次數限制'
			$r['err'] = 11;
			$r['retCode']=-100231;
			$r['retMsg']='超過可下標次數限制!';
		}

		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "單次下標 $time 秒<br>";
		return $r;
	}
	
	//檢查使用者下標次數限制
	public function chk_user_limit($user_limit, $pid)
	{
		$time_start = microtime(true);
		
		global $db, $config;
	
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND productid = '{$pid}'
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		$tickets_avaiable = (int)$user_limit - (int)$table['table']['record'][0]['count'];

		if((int)$this->tickets > $tickets_avaiable) {
			return 1;
		}
		return 0;
	}
	
	//檢查最後下標時間
	public function chk_time_limit()
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$query = "
		SELECT unix_timestamp(MAX(insertt)) as insertt, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			if( (int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $this->saja_time_limit) {
				return 1;
			}
		}
	
		return 0;
	}
	
	public function getBidWinner($productid) {
	       global $db, $config;
		   // 得標者
		   $ret='';
		   $query="SELECT h.userid, h.productid, h.nickname, h.price, h.src_ip ".
		          ",(SELECT name from `{$config['db'][2]["dbname"]}`.`{$config['default_prefix']}province` WHERE switch='Y' AND provinceid=up.provinceid) as src_province ".
				  ",(SELECT name from `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}product` WHERE productid=h.productid) as prodname ".
				  ",(SELECT uid from `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}sso` where ssoid=(SELECT ssoid FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sso_rt` WHERE userid=h.userid and switch='Y' ) and switch='Y' and name='weixin') as openid ".
				  " FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h ".
				  " JOIN `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_profile` up ON h.userid=up.userid ".
			      " WHERE (h.productid, h.price) =  ".
				  "       (SELECT productid, min_price FROM  `{$config['db'][4]["dbname"]}`.`v_min_unique_price` WHERE productid='{$productid}') ";
					
		   error_log("[ajax/weixinbid/getBidWinner] : ".$query);
		   $table = $db->getQueryRecord($query);
		   if($table['table']['record']) {
			  $ret=array();
			  $ret['userid'] = $table['table']['record'][0]['userid']; 
			  $ret['nickname'] = $table['table']['record'][0]['nickname'];
              $ret['productid'] = $table['table']['record'][0]['productid']; 
              $ret['prodname'] = $table['table']['record'][0]['prodname']; 
              $ret['src_province'] = $table['table']['record'][0]['src_province'];  			  
              $ret['price'] = $table['table']['record'][0]['price'];			  
              $ret['openid'] = $table['table']['record'][0]['openid'];
			  $ret['src_ip'] = $table['table']['record'][0]['src_ip'];			  
           }
		   error_log("[ajax/weixinbid/getBidWinner] : ".json_encode($ret));
           return $ret;		   
	}

	//產生下標歷史記錄
	public function mk_history($product)
	{
		$time_start = microtime(true);
		
		global $db, $config;
		
		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = '';
		$values = array();
		$ip = GetIP();

		$scode_fee = 0;
		
		//檢查殺幣餘額
		$query = "SELECT SUM(amount) as amount
		FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
		WHERE userid = '{$this->userid}'
			AND prefixid = '{$config['default_prefix_id']}' 
			AND switch = 'Y'
		";			
		$table = $db->getQueryRecord($query);
		
		$saja_fee_amount=getBidTotalFee($product,$_POST['type'],
										$_POST['price'],
										$_POST['price_start'],
										$_POST['price_stop']);

		if($saja_fee_amount > (float)$table['table']['record'][0]['amount']) {
			//'殺價幣不足'
			$r['err'] = 13;
			$r['retCode']=-100193;
			$r['retMsg']='殺價幣不足!';
		}
		else
		{
			//使用殺幣
			$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`countryid`='{$this->countryid}',
				`behav`='user_saja',
				`amount`='". - $saja_fee_amount ."',
				`insertt`=NOW()
			";
			
			$res = $db->query($query);
			$spointid = $db->_con->insert_id;
			
			for($i = 0 ; $i < round($this->tickets); $i++) 
			{
				//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
				$values[$i] = "
				(
					'{$config['default_prefix_id']}',
					'{$product['productid']}',
					'{$this->userid}',
					'{$this->username}',
					'{$spointid}',
					'0',
					'0',
					'0',
					'".($this->price + $i * $this->currency_unit)."',
					NOW(),
					'{$ip}'
				)";
				
			}
			
			if(empty($values)) {
				//'下標失敗'
				$r['err'] = -1;
				$r['retCode']=-1;
				$r['retMsg']='下標失敗!';
			}
		}
		
		
		if(empty($r['err']) && !empty($values) )
		{
			// 取得目前winner
			$arr_ori_winner=array();
			$arr_ori_winner=$this->getBidWinner($product['productid']);
			if(!empty($arr_ori_winner)) {
			   error_log("[ajax/weixinbid/mk_history]: ori Winner of ".$arr_ori_winner['prodname']." is :".$arr_ori_winner['nickname']);
			} else {
			   error_log("[ajax/weixinbid/mk_history]: ori Winner is Empty !");
			}
			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
			(
				prefixid,
				productid,
				userid,
				nickname,
				spointid,
				scodeid,
				sgiftid,
				oscodeid,
				price,
				insertt,
				src_ip		
			)
			VALUES
			".implode(',', $values)."
			";
			
			$res = $db->query($query);
			$r['rank'] = $last_rank;
			error_log("[ajax/weixinbid/mk_history] r :".json_encode($r));
			
			// 使用順位
			$rank = bidrank($product, $_POST['price'], $_POST['type'], $_POST['price_start'], $_POST['price_stop'], $this->tickets, $this->userid);			
			error_log("[ajax/weixinbid/mk_history] r2 :".json_encode($rank));
			foreach($rank as $k=> $v) {
				$r[$k]=$v;
			}
			
			$arr_new_winner=array();
			try {
				$arr_new_winner=$this->getBidWinner($product['productid']);
				if(!empty($arr_new_winner['src_ip'])) {
				   $arr_ip=explode(".",$arr_new_winner['src_ip']);
				   if(is_array($arr_ip)) {
					  $ip2=strlen($arr_ip[1]);
					  $arr_ip[1]='';
					  for($i=0;$i<$ip2;++$i) {
						  $arr_ip[1].='*';        
					  }
					  
					  $ip3=strlen($arr_ip[2]);
					  $arr_ip[2]='';
					  for($i=0;$i<$ip3;++$i) {
						  $arr_ip[2].='*';        
					  }
					  $arr_new_winner['src_ip']=implode(".",$arr_ip);
					  error_log($arr_new_winner['src_ip']);
				   }
				}
				error_log("[ajax/weixinbid/mk_history] new_winner :".json_encode($arr_new_winner));
				$r['winner']=$arr_new_winner['nickname'];
				if(empty($r['winner']))
				   $r['winner']="_無_";
			
			// 更新redis中的中標者暱稱資訊
			
				if(!empty($r['winner']) && !empty($product['productid'])) {
					$redis = new Redis();
					$redis->connect('127.0.0.1');
					$redis->set('PROD:'.$product['productid'].':BIDDER',$r['winner']);
					error_log("[ajax/weixinbid] set to redis : ".'PROD:'.$product['productid'].':BIDDER'.":".$r['winner']);
				}	
				
				// Socket通知
				$from=MD5("NOTIFY|".$product['productid']);
				$json_str='{"ACTION":"NOTIFY","FROM":"'.$from.
						 '","PRODID":"'.$product['productid'].
						 '","WINNER":"'.$r['winner'].
						 '","SRC_IP":"'.$arr_new_winner['src_ip'].
						 '","SRC_PROVINCE":"'.$arr_new_winner['src_province'].
						 '"}';
			    error_log("[ajax/weixinbid] socket notify :".$json_str);
			
				$client = new Client("wss://www.saja.com.tw.cn/");
				$client->send($json_str);
				$client = new Client("ws://www.saja.com.tw.cn/");
				$client->send($json_str);
	        } catch (Exception $e) {
			   error_log("[weixinbid] exception : ".$e->getMessage());
			}
						
			if($product['is_flash']!='Y') {
				// 非閃殺的商品, 結標一分鐘以前, 得標者改變會發微信通知(openid)
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit) ) {
					if($_POST['pay_type']!='saja_test') {					
						if($arr_ori_winner['userid']!=$arr_new_winner['userid']) {
						   $options['appid'] = APP_ID;
						   $options['appsecret'] = APP_SECRET;
						   $wx=new WeixinChat($options);
						
						   if(!empty($arr_new_winner['openid'])) {		
							  $msg='【目前中標通知】 恭喜你！刚刚干掉之前<a href="http://www.saja.com.tw/wx_auth.php?jdata=gotourl:/site/product/saja/|productid:'.$arr_new_winner['productid'].'|canbid:Y">《'.$arr_new_winner['prodname'].'》</a>的中標者。請持续关注，以確保领先地位。';
							  $wx->sendCustomMessage($arr_new_winner['openid'],$msg,'text');
							  $wx->sendCustomMessage('oNmoZtxOu0pKnlUjZjO0beFCSfEY',$msg,'text');
						   }
						   if(!empty($arr_ori_winner['openid'])) {			   
							  if(!empty($arr_new_winner['userid'])) {
								 $msg='【紧急通知】原本你中標的<a href="http://www.saja.com.tw/wx_auth.php?jdata=gotourl:/site/product/saja/|productid:'.$arr_ori_winner['productid'].'|canbid:Y">《'.$arr_ori_winner['prodname'].'》</a>在刚刚被某个小伙伴给抢走了！赶紧去看看是哪个家伙！再把它抢回来吧！';
							  } else if (empty($arr_new_winner['userid'])) {
								 $msg='【紧急通知】原本你中標的<a href="http://www.saja.com.tw/wx_auth.php?jdata=gotourl:/site/product/saja/|productid:'.$arr_ori_winner['productid'].'|canbid:Y">《'.$arr_ori_winner['prodname'].'》</a>目前無人中標 ! 赶紧再去把它抢回来吧！';
							  }
							  $wx->sendCustomMessage($arr_ori_winner['openid'],$msg,'text');
							  $wx->sendCustomMessage('oNmoZtxOu0pKnlUjZjO0beFCSfEY',$msg,'text');
						   }
						} 
					}
				}
			}
			// 得標者
		}
		error_log("[ajax/weixinbid/mk_history] r : ".json_encode($r));
		return $r;
	}

}
?>
