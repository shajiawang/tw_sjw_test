<?php
session_start();
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Methods: GET, POST, DELETE');
header('Access-Control-Allow-Headers: *');
include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/enterprise.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR ."/wechat.class.php");
include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");

$c = new Enterprise;

if($_POST['type']=='enterprise_login') {

	$c->home();

} elseif($_POST['type']=='enterpriseupdate') {

	$c->enterpriseupdate();

} elseif($_POST['type']=='enterprisepwupdate') {

	$c->enterprisepwupdate();

} elseif($_POST['type']=='enterpriseexpwupdate') {

	$c->enterpriseexpwupdate();

} elseif($_POST['type']=='enterpriseproductadd') {

	$c->enterpriseproductadd();

} elseif($_POST['type']=='enterpriseproductupdate') {

	$c->enterpriseproductupdate();

} elseif($_POST['type']=='enterprisecash') {

	$c->enterprisecash();

}


class Enterprise {
	public $str;
	public $salt='sjW333-_@';
	public function home(){

		global $db, $config, $enterprisemodel;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		//載入lib
		$enterprisemodel = new EnterpriseModel;//商家model
		$this->str = new convertString();//字串處理

		//app輸出設定
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];

		//設定web回傳初始狀態
		$ret['status'] = 0;

		//是否已登入
		if(!empty($_SESSION['sajamanagement']['enterprise']['enterpriseid'])) {

			//已登入
			$ret['status'] = 9;
			$app['retCode']= 9;
			$app['retObj']['companyname']= $_SESSION['sajamanagement']['enterprise']['companyname'];
			$app['retObj']['marketingname']= $_SESSION['sajamanagement']['enterprise']['marketingname'];
			$app['retObj']['thumbnail']= $_SESSION['sajamanagement']['enterprise']['thumbnail'];
			$app['retObj']['enterpriseid']= $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
			$app['retObj']['loginname']= $_SESSION['sajamanagement']['enterprise']['loginname'];
			$app['retObj']['verified']= $_SESSION['sajamanagement']['enterprise']['verified'];			
			$app['retMsg']= '已登入';
			$app['retType']= 'JSON';

			//輸出結果
			if($json=='Y') {
				echo json_encode($app, JSON_UNESCAPED_UNICODE);
			} else {
				echo json_encode($ret);
			}
			exit;

		}

		//檢查帳密是否已填寫
		if(empty($_POST['loginname'])) {//帳號未填寫

			$ret['status'] = 2;
			$app=getRetJSONArray(2,'帳號未填寫','MSG');

		} elseif (empty($_POST['passwd'])) {//密碼未填寫

			$ret['status'] = 4;
			$app=getRetJSONArray(4,'密碼未填寫','MSG');

		}

		//檢查帳密是否正確
		if(empty($ret['status'])) {

			//檢查帳密
			$chk = $this->chk_login();

			//檢查回傳
			if(empty($chk['err'])) {//帳密正確

				$ret['status'] = 1;
				$app['retCode']= 1;
				$app['retMsg']= '帳密正確';
				$app['retType']= 'JSON';
				$app['retObj']['companyname']= $_SESSION['sajamanagement']['enterprise']['companyname'];
				$app['retObj']['marketingname']= $_SESSION['sajamanagement']['enterprise']['marketingname'];
				$app['retObj']['thumbnail']= $_SESSION['sajamanagement']['enterprise']['thumbnail'];
				$app['retObj']['enterpriseid']= $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
				$app['retObj']['loginname']= $_SESSION['sajamanagement']['enterprise']['loginname'];
				$app['retObj']['verified']= $_SESSION['sajamanagement']['enterprise']['verified'];
				$app['retObj']['ts']=time();
				//function enterprise_log($user,$module,$act,$target='',$memo=''){
				enterprise_log($_SESSION['sajamanagement']['enterprise']['enterpriseid'],'user','login ok');

			} elseif($chk['err'] == 3) {//帳號不存在

				$ret['status'] = 3;
				$app=getRetJSONArray(3,'帳號不存在','MSG');

			} elseif($chk['err'] == 5) {//密碼錯誤

				$ret['status'] = 5;
				$app=getRetJSONArray(5,'密碼錯誤','MSG');

			}

		}

		//輸出結果
		if($json=='Y') {
			echo json_encode($app, JSON_UNESCAPED_UNICODE);
		} else {
			echo json_encode($ret);
		}

		exit;
	}

	// 查驗帳號密碼
	public function chk_login() {

		global $db, $config, $enterprisemodel;

		//設定回傳預設值
		$r['err'] = '';
		//初始化資料庫連結介面
		$db2 = new mysql($config["db2"]);
		$db2->connect();
		//載入lib
		$this->str = new convertString();//字串處理

		//查詢商家資料
		$query = "SELECT *,est.name as exchange_store_name FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` e
		            JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` ep
				             ON e.enterpriseid=ep.enterpriseid
				        JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` es
				             ON e.enterpriseid=es.enterpriseid
				        LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` est
				             ON e.esid=est.esid
		            WHERE e.prefixid = '{$config['default_prefix_id']}'
			            AND e.loginname = '{$_POST['loginname']}'
			            AND e.switch = 'Y'
		            LIMIT 1
		         ";

		error_log("[ajax/enterprise.chk_login] query:".$query);
		$table = $db2->getQueryRecord($query);

		//檢查商家帳號是否存在
		if(empty($table['table']['record'])) {//'帳號不存在'

			$r['err'] = 3;

		} else {//帳號存在

			//清除session內商家資料
			$_SESSION['sajamanagement']['enterprise'] = '';

			//取得商家資料
			$enterprise = $table['table']['record'][0];

			//查詢商家密碼
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);

			//檢查密碼是否正確
			if($enterprise['passwd'] === $passwd) {

				//查詢是否驗證
				$query2 = "SELECT u.userid, usa.verified FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
							       LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt` uer
								          ON u.userid=uer.userid
							       LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa
								          ON u.userid=usa.userid
						        WHERE u.prefixid = '{$config['default_prefix_id']}'
							        AND uer.enterpriseid = '{$enterprise['enterpriseid']}'
							        AND u.switch = 'Y'
						        LIMIT 1
				          ";

				error_log("[ajax/enterprise.chk_login] query2:".$query2);
				$table2 = $db2->getQueryRecord($query2);

				//取得商家驗證資料
				$enterprise['sms'] = $table2['table']['record'][0]['verified'];

				//商家頭像輸出處理
				if(!empty($enterprise['thumbnail_file'])) {//上傳頭像
				  $enterprise['thumbnail']=BASE_URL."/management/images/headimgs/".$enterprise['thumbnail_file'];
				}else{//第三方頭像
					$enterprise['thumbnail']=$enterprise['thumbnail_url'];
				}

				if(empty($enterprise['thumbnail'])) {//預設頭像
					$enterprise['thumbnail']=BASE_URL."/management/images/headimgs/_DefaultShop.jpg";
				}

				//商家資料寫入session
				$_SESSION['sajamanagement']['enterprise'] = $enterprise;

				//商家userid寫入session
				$_SESSION['auth_id'] = $table2['table']['record'][0]['userid'];
				//商家id寫入log
				error_log("[ajax/enterprise.chk_login] enterprise id : ".$enterprise['enterpriseid']);

				//商家userid與商家id寫入cookie
				setcookie("auth_id", $table2['table']['record'][0]['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("enterpriseid", $enterprise['enterpriseid'], time() + 86400, "/", COOKIE_DOMAIN);
				$r['err'] = '';

			} else {
				//密碼錯誤
				$r['err'] = 5;
			}
		}
		return $r;
	}

	//修改商家資料
	public function enterpriseupdate() {

		global $db, $config, $enterprisemodel;

		//商家id
		$enterpriseid = $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
		//app輸出設定
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];

		//商家營業資料輸入
		//$owner=(empty($_POST['owner']))?$_GET['owner']:$_POST['owner'];
		$contact_phone=(empty($_POST['contact_phone']))?$_GET['contact_phone']:$_POST['contact_phone'];
		$contact_mobile=(empty($_POST['contact_mobile']))?$_GET['contact_mobile']:$_POST['contact_mobile'];
		$url=(empty($_POST['url']))?$_GET['url']:$_POST['url'];
		$contact_email=(empty($_POST['contact_email']))?$_GET['contact_email']:$_POST['contact_email'];
		$businessontime=(empty($_POST['businessontime']))?$_GET['businessontime']:$_POST['businessontime'];
		$businessofftime=(empty($_POST['businessofftime']))?$_GET['businessofftime']:$_POST['businessofftime'];
		$service_email=(empty($_POST['service_email']))?$_GET['service_email']:$_POST['service_email'];
		//$seniority=(empty($_POST['seniority']))?$_GET['seniority']:$_POST['seniority'];
		//$employees=(empty($_POST['employees']))?$_GET['employees']:$_POST['employees'];
		//$profit_ratio=(empty($_POST['profit_ratio']))?$_GET['profit_ratio']:$_POST['profit_ratio'];

		//商家詳細資料輸入
		//$companyname=(empty($_POST['companyname']))?$_GET['companyname']:$_POST['companyname'];
		//$marketingname=(empty($_POST['marketingname']))?$_GET['marketingname']:$_POST['marketingname'];
		//$uniform=(empty($_POST['uniform']))?$_GET['uniform']:$_POST['uniform'];
		//$name=(empty($_POST['name']))?$_GET['name']:$_POST['name'];
		//$phone=(empty($_POST['phone']))?$_GET['phone']:$_POST['phone'];
		$email=(empty($_POST['email']))?$_GET['email']:$_POST['email'];
		//$fax=(empty($_POST['fax']))?$_GET['fax']:$_POST['fax'];
		$address=(empty($_POST['address']))?$_GET['address']:$_POST['address'];
		$pic=(empty($_POST['pic']))?$_GET['pic']:$_POST['pic'];
		$picurl=(empty($_POST['picurl']))?$_GET['picurl']:$_POST['picurl'];
		$opic=(empty($_POST['oldpic']))?$_GET['oldpic']:$_POST['oldpic'];
		$spic=(empty($_POST['spic']))?$_GET['spic']:$_POST['spic'];

		//商家帳戶資料輸入
		//$account_contactor=(empty($_POST['account_contactor']))?$_GET['account_contactor']:$_POST['account_contactor'];
		//$account_phone=(empty($_POST['account_phone']))?$_GET['account_phone']:$_POST['account_phone'];
		//$execute_contactor=(empty($_POST['execute_contactor']))?$_GET['execute_contactor']:$_POST['execute_contactor'];
		//$execute_phone=(empty($_POST['execute_phone']))?$_GET['execute_phone']:$_POST['execute_phone'];
		//$accountname=(empty($_POST['accountname']))?$_GET['accountname']:$_POST['accountname'];
		//$accountnumber=(empty($_POST['accountnumber']))?$_GET['accountnumber']:$_POST['accountnumber'];
		//$bankname=(empty($_POST['bankname']))?$_GET['bankname']:$_POST['bankname'];
		//$branchname=(empty($_POST['branchname']))?$_GET['branchname']:$_POST['branchname'];

		//post輸入資料寫入log
		error_log("[ajax/enterprise/enterpriseupdate] POST : ".json_encode($_POST));

		//web回傳參數初始設定
		$ret=array();
		enterprise_login_required();
		$opdevice=($json=='Y')?'app':'web';
		try{

			//輸入資料檢查
			if(empty($enterpriseid)) {// 會員編號不可為空白

				$ret['status']=0;
				$app=getRetJSONArray(0,'商家請先登入 !!','MSG');

/*			} elseif(empty($owner)) {// 負責人不可為空白

				$ret['status']=-2;
				$ret['kind']='負責人不可為空白!!';
				$app=getRetJSONArray(-2,'負責人不可為空白 !!','MSG');
*/
			} elseif(empty($contact_phone)) {// 店家電話不可為空白

				$ret['status']=-3;
				$ret['kind']='商家電話不可為空白!!';
				$app=getRetJSONArray(-3,'商家電話不可為空白 !!','MSG');

			} elseif(empty($contact_mobile)) {// 店家移動電話不可為空白

				$ret['status']=-4;
				$ret['kind']='商家行動電話不可為空白!!';
				$app=getRetJSONArray(-4,'商家行動電話不可為空白 !!','MSG');

/*			} elseif(empty($profit_ratio)) {// 店家分潤比不可為空白

				$ret['status']=-5;
				$ret['kind']='商家分潤比不可為空白!!';
				$app=getRetJSONArray(-5,'商家分潤比不可為空白 !!','MSG');
*/
			} else {//輸入資料檢查成功

				// 初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();

				//新頭像更新sql碼
				$new_thumbnail_file = '';

				//判斷新頭像資料是否存在
				if(!empty($spic) && ($spic != $opic)) {
					$image = base64_decode($spic);
					$new_thumbnail_file = ", `thumbnail_file` = '{$spic}'";
				}

				//更新商家詳細資料
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile`
										 SET
												  `email` = '{$email}'
												 {$new_thumbnail_file}
												 , `thumbnail_url` = '{$picurl}'
									 WHERE `prefixid` = '{$config['default_prefix_id']}'
										 AND enterpriseid = '{$enterpriseid}'
										 AND switch = 'Y'
								 ";
				$db->query($query);

				//更新商家營業資料
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop`
										 SET  `contact_phone` = '{$contact_phone}'
												 , `contact_mobile` = '{$contact_mobile}'

												 , `url` = '{$url}'
												 , `contact_email` = '{$contact_email}'
												 , `businessontime` = '{$businessontime}'
												 , `businessofftime` = '{$businessofftime}'
												 , `service_email` = '{$service_email}'
									 WHERE `prefixid` = '{$config['default_prefix_id']}'
										 AND enterpriseid = '{$enterpriseid}'
										 AND switch = 'Y'
								 ";
				$db->query($query);

				//更新商家帳戶資料
				/*$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_account`
										 SET `account_contactor` = '{$account_contactor}'
												 , `account_phone` = '{$account_phone}'
												 , `execute_contactor` = '{$execute_contactor}'
												 , `execute_phone` = '{$execute_phone}'
												 , `accountname` = '{$accountname}'
												 , `accountnumber` = '{$accountnumber}'
												 , `bankname` = '{$bankname}'
												 , `branchname` = '{$branchname}'
									 WHERE `prefixid` = '{$config['default_prefix_id']}'
										 AND enterpriseid = '{$enterpriseid}'
										 AND switch = 'Y'
								 ";
				$db->query($query);*/

				//更新成功返回狀態設定
				$ret['status']=1;
				$app = getRetJSONArray(1,'OK','MSG');
				enterprise_log($enterpriseid,'user','changeprofile',$opdevice);
			}

		//寫入失敗處理
		} catch(Exception $e) {

			$ret['status'] = -1;
			$app=getRetJSONArray(-1,'商家資料修改失敗 !!','MSG');
			error_log("[ajax/enterprise/enterpriseupdate]:".$e->getMessage());

		//執行完成結果
		} finally {
			if ($ret['status']!=1)enterprise_log($enterpriseid,'user','changeprofile fail',$opdevice,json_encode($app, JSON_UNESCAPED_UNICODE));
			//輸出結果
			if($json=='Y') {
				echo json_encode($app, JSON_UNESCAPED_UNICODE);
			} else {
				echo json_encode($ret);
			}

		}
		exit;
	}

	//修改商家登入密碼
	public function enterprisepwupdate() {

		global $db, $config, $enterprisemodel;

		//商家id
		$enterpriseid=$_SESSION['sajamanagement']['enterprise']['enterpriseid'];
		enterprise_login_required();
		//商家帳密相關
		$passwd=(empty($_POST['passwd']))?$_GET['passwd']:$_POST['passwd'];
		$checkpasswd=(empty($_POST['checkpasswd']))?$_GET['checkpasswd']:$_POST['checkpasswd'];

		//app輸出設定
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];

		//post輸入資料寫入log
		error_log("[ajax/enterprise/enterprisepwupdate] POST : ".json_encode($_POST));

		//web回傳參數初始設定
		$ret=array();
		$opdevice=($json=='Y')?'app':'web';
		try {
			//輸入資料檢查
			if(empty($enterpriseid)) {//商家未登入

				$ret['status']=0;
				$app=getRetJSONArray(0,'商家請先登入 !!','MSG');

			} elseif(empty($passwd) || empty($checkpasswd)) {//登入密碼與登入密碼確認不可為空白

				$ret['status']=-12;
				$ret['kind']='登入密碼與再次確認密碼不可為空白!!';
				$app=getRetJSONArray(-12,'登入密碼與再次確認密碼不可為空白!!','MSG');

			} elseif($passwd !== $checkpasswd) {//登入密碼與登入密碼確認不相同，請再確認

				$ret['status']=-13;
				$ret['kind']='登入密碼與再次確認密碼確認不同!!';
				$app=getRetJSONArray(-13,'登入密碼與再次確認密碼確認不同!!','MSG');

			} elseif(!preg_match("/^[A-Za-z0-9]{4,12}$/", $passwd)) {//登入密碼格式錯誤

				$ret['status']=-14;
				$ret['kind']='登入密碼格式錯誤!!';
				$app=getRetJSONArray(-14,'登入密碼格式錯誤!!','MSG');

			} else {//輸入資料檢查成功

				//初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();

				//載入lib
				$this->str = new convertString();//字串處理

				//檢查是否更新密碼
				if (!empty($passwd) && ($passwd == $checkpasswd)){

					$newpasswd = $this->str->strEncode($passwd, $config['encode_key']);//密碼加密
					$sql .= " , `passwd` = '{$newpasswd}' ";//產生更新密碼sql

				}

				//更新商家登入密碼
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise`
										 SET `prefixid` = '{$config['default_prefix_id']}'
											   {$sql}
									 WHERE `prefixid` = '{$config['default_prefix_id']}'
										 AND enterpriseid = '{$enterpriseid}'
										 AND switch = 'Y'
									";
				$db->query($query);
				enterprise_log($enterpriseid,'user','changepasswd',$opdevice);
				//返回狀態設定
				$ret['status']=1;
				$app = getRetJSONArray(1,'OK','MSG');

			}

		//寫入失敗處理
		} catch(Exception $e) {

			$ret['status'] = -1;
			$app=getRetJSONArray(-1,'修改商家登入密碼失敗 !!','MSG');
			error_log("[ajax/enterprise/enterprisepwupdate]:".$e->getMessage());

		//執行完成結果
		} finally {
			if ($ret['status']!=1)enterprise_log($enterpriseid,'user','changepasswd fail',$opdevice,json_encode($app, JSON_UNESCAPED_UNICODE));
			//輸出結果
			if($json=='Y') {
				echo json_encode($app, JSON_UNESCAPED_UNICODE);
			} else {
				echo json_encode($ret);
			}

		}
		exit;
	}

	//修改商家提現密碼
	public function enterpriseexpwupdate() {

		global $db, $config, $enterprisemodel;
		enterprise_login_required();
		//商家id
		$enterpriseid = $_SESSION['sajamanagement']['enterprise']['enterpriseid'];

		//商家帳密相關
		$expasswd=(empty($_POST['expasswd']))?$_GET['expasswd']:$_POST['expasswd'];
		$excheckpasswd=(empty($_POST['excheckpasswd']))?$_GET['excheckpasswd']:$_POST['excheckpasswd'];

		//app輸出設定
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];

		//post輸入資料寫入log
		error_log("[ajax/enterprise/enterpriseexpwupdate] POST : ".json_encode($_POST));

		//web回傳參數初始設定
		$ret=array();
		$opdevice=($json=='Y')?'app':'web';
		try {
			//輸入資料檢查
			if(empty($enterpriseid)) {//商家未登入

				$ret['status']=0;
				$app=getRetJSONArray(0,'商家請先登入 !!','MSG');

			} elseif(empty($expasswd) || empty($excheckpasswd)) {//提現密碼與確認提現密碼不可為空白

				$ret['status']=-2;
				$ret['kind']='提現密碼與確認提現密碼不可為空白!!';
				$app=getRetJSONArray(-2,'提現密碼與確認提現密碼不可為空白!!','MSG');

			} elseif($expasswd !== $excheckpasswd) {//提現密碼與確認提現密碼不同

				$ret['status']=-5;
				$ret['kind']='提現密碼與確認提現密碼不同!!';
				$app=getRetJSONArray(-5,'提現密碼與確認提現密碼不同!!','MSG');

			} elseif((!empty($expasswd))&&(!preg_match("/^[A-Za-z0-9]{4,12}$/", $expasswd))) {// 提現密碼格式錯誤

				$ret['status']=-6;
				$ret['kind']='提現密碼格式錯誤!!';
				$app=getRetJSONArray(-6,'提現密碼格式錯誤!!','MSG');

			} else {//輸入資料檢查成功

				//初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();
				//載入lib
				$this->str = new convertString();//字串處理

				//檢查是否更新密碼
				if (!empty($expasswd) && ($expasswd == $excheckpasswd)) {

					$newexpasswd = $this->str->strEncode($expasswd, $config['encode_key']);//密碼加密
					$sql .= " , `exchangepasswd` = '{$newexpasswd}' ";//產生更新密碼sql

				}

				//更新商家提現密碼
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise`
										 SET `prefixid` = '{$config['default_prefix_id']}'
												 {$sql}
									 WHERE `prefixid` = '{$config['default_prefix_id']}'
										 AND enterpriseid = '{$enterpriseid}'
										 AND switch = 'Y'
								 ";
				$db->query($query);
				enterprise_log($enterpriseid,'user','change_ex_passwd',$opdevice);
				//返回狀態設定
				$ret['status']=1;
				$app = getRetJSONArray(1,'OK','MSG');
			}

		//寫入失敗處理
		} catch(Exception $e) {

			$ret['status'] = -1;
			$app=getRetJSONArray(-1,'修改商家提現密碼失敗 !!','MSG');
			error_log("[ajax/enterprise/enterpriseex[wupdate]:".$e->getMessage());

		//執行完成結果
		} finally {
			if ($ret['status']!=1)enterprise_log($enterpriseid,'user','change_ex_passwd fail',$opdevice,json_encode($app, JSON_UNESCAPED_UNICODE));
			//輸出結果
			if($json=='Y'){
				echo json_encode($app, JSON_UNESCAPED_UNICODE);
			}else{
				echo json_encode($ret);
			}

		}

		exit;
	}

	//商家提現
	public function enterprisecash() {

		global $db, $config, $enterprisemodel;

		//商家id
		$enterpriseid = $_SESSION['sajamanagement']['enterprise']['enterpriseid'];

		//商家帳戶資料
		$accountname	= (empty($_POST['accountname'])) ? $_GET['accountname'] : $_POST['accountname'];
		$accountnumber	= (empty($_POST['accountnumber'])) ? $_GET['accountnumber'] : $_POST['accountnumber'];
		$bankname		= (empty($_POST['bankname'])) ? $_GET['bankname'] : $_POST['bankname'];
		$bankarea		= (empty($_POST['bankarea'])) ? $_GET['bankarea'] : $_POST['bankarea'];
		$branchname		= (empty($_POST['branchname'])) ? $_GET['branchname'] : $_POST['branchname'];
		$cash			= (empty($_POST['cash'])) ? $_GET['cash'] : $_POST['cash'];
		$bonus			= (empty($_POST['bonus'])) ? $_GET['bonus'] : $_POST['bonus'];
		enterprise_login_required();
		//app輸出設定
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];

		//post輸入資料寫入log
		error_log("[ajax/enterprise/enterprisecash] POST : ".json_encode($_POST));
		$target=($json=='Y')?'APP':'web';
		//web回傳參數初始設定
		$ret=array();

		try {
			//輸入資料檢查
			if(empty($enterpriseid)) {//商家未登入

				$ret['status']=0;
				$app=getRetJSONArray(0,'商家請先登入 !!','MSG');

			} elseif(empty($accountname)) {//銀行開戶名不可為空白

				$ret['status']=-12;
				$ret['kind']='銀行開戶名不可為空白!!';
				$app=getRetJSONArray(-12,'銀行開戶名不可為空白!!','MSG');

			} elseif(empty($accountnumber)) {//銀行帳號不可為空白

				$ret['status']=-13;
				$ret['kind']='銀行帳號不可為空白!!';
				$app=getRetJSONArray(-13,'銀行帳號不可為空白!!','MSG');

			} elseif(empty($bankname)) {//開戶銀行名稱不可為空白

				$ret['status']=-14;
				$ret['kind']='開戶銀行名稱不可為空白!!';
				$app=getRetJSONArray(-14,'開戶銀行名稱不可為空白!!','MSG');

			} elseif(empty($bankarea)) {//開戶銀行所在地區不可為空白

				$ret['status']=-15;
				$ret['kind']='開戶銀行所在地區不可為空白!!';
				$app=getRetJSONArray(-15,'開戶銀行所在地區不可為空白!!','MSG');

			} else if(empty($branchname)) {//開戶銀行支行名稱不可為空白

				$ret['status']=-16;
				$ret['kind']='開戶銀行支行名稱不可為空白!!';
				$app=getRetJSONArray(-16,'開戶銀行支行名稱不可為空白!!','MSG');

			} else if(empty($cash)) {//提現金額不可為空白

				$ret['status']=-17;
				$ret['kind']='提現金額不可為空白!!';
				$app=getRetJSONArray(-17,'提現金額不可為空白!!','MSG');

			} else {

				//初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();

				//載入lib
				$this->str = new convertString();//字串處理

				//查詢商家資料
				$query = "SELECT *
				            FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise`
				           WHERE prefixid = '{$config['default_prefix_id']}'
					           AND enterpriseid = '{$enterpriseid}'
					           AND switch = 'Y'
				         ";

				error_log("[ajax/enterprise/enterpriseproductadd]".$query);
				$table = $db->getQueryRecord($query);

				//取得商家資料
				$enterprise = $table['table']['record'][0];

				$esid = $enterprise['esid'];//兌換商品店家id
				$storeid = $enterprise['storeid'];//殺價商品店家id

				//更新商家帳戶資料
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_account`
										 SET `accountname` = '{$accountname}'
												 , `accountnumber` = '{$accountnumber}'
												 , `bankname` = '{$bankname}'
												 , `branchname` = '{$branchname}'
									 WHERE `prefixid` = '{$config['default_prefix_id']}'
										 AND enterpriseid = '{$enterpriseid}'
										 AND switch = 'Y'
								 ";
				$db->query($query);

				//新增商家提現資料
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store`
				                  SET `prefixid` = '{$config['default_prefix_id']}'
															,`bonusid` = '0'
															, `esid` = '{$esid}'
															, `enterpriseid` = '{$enterpriseid}'
															, `countryid` = '{$config['country']}'
															, `behav` = 'store_exchange_cash'
															, `amount` = '{$cash}'
															, `sys_profit` = '0'
															, `switch` = 'Y'
															, `insertt` = NOW()
							   ";
				$db->query($query);
				enterprise_log($enterpriseid,'bonus','withdraw',$target,json_encode($_POST));
				//返回狀態設定
				$ret['status']=1;
				$app = getRetJSONArray(1,'OK','MSG');

			}

		//寫入失敗處理
		} catch (Exception $e) {

			$ret['status'] = -1;
			enterprise_log($enterpriseid,'bonus','withdraw fail',$target,json_encode($_POST));
			$app=getRetJSONArray(-1,'商家提現失敗!!','MSG');
			error_log("[ajax/enterprise/enterprisecash]:".$e->getMessage());

		//執行完成結果
		} finally {

			//輸出結果
			if($json=='Y') {
				echo json_encode($app, JSON_UNESCAPED_UNICODE);
			} else {
				echo json_encode($ret);
			}

		}

		exit;
	}

	//新增閃殺商品
	public function enterpriseproductadd() {

		global $db, $config, $enterprisemodel;

		$enterpriseid = $_SESSION['sajamanagement']['enterprise']['enterpriseid'];

		//企業帳密相關
		$sname=(empty($_POST['sname']))?$_GET['sname']:$_POST['sname'];
		$retail_price=(empty($_POST['retail_price']))?$_GET['retail_price']:$_POST['retail_price'];
		$ontime=(empty($_POST['ontime']))?$_GET['ontime']:$_POST['ontime'];
		$offtime=(empty($_POST['offtime']))?$_GET['offtime']:$_POST['offtime'];
		$flash_loc=(empty($_POST['flash_loc']))?$_GET['flash_loc']:$_POST['flash_loc'];
		$is_flash=(empty($_POST['is_flash']))?$_GET['is_flash']:$_POST['is_flash'];
		$bid_type=(empty($_POST['bid_type']))?$_GET['bid_type']:$_POST['bid_type'];
		$contact_info=(empty($_POST['contact_info']))?$_GET['contact_info']:$_POST['contact_info'];
		$pic=(empty($_POST['pic']))?$_GET['pic']:$_POST['pic'];
		$spic=(empty($_POST['spic']))?$_GET['spic']:$_POST['spic'];
		$storeid=(empty($_POST['storeid']))?$_GET['storeid']:$_POST['storeid'];

		$saja_limit=(empty($_POST['saja_limit']))?$_GET['saja_limit']:$_POST['saja_limit'];
		if(empty($saja_limit))
		   $saja_limit='1';

		$usereach_limit=(empty($_POST['usereach_limit']))?$_GET['usereach_limit']:$_POST['usereach_limit'];
		if(empty($usereach_limit))
		   $usereach_limit='100';

		$user_limit=(empty($_POST['user_limit']))?$_GET['user_limit']:$_POST['user_limit'];
		if(empty($user_limit))
		   $user_limit='99999';

		$totalfee_type=(empty($_POST['totalfee_type']))?$_GET['totalfee_type']:$_POST['totalfee_type'];
		if(empty($totalfee_type))
		   $totalfee_type='B';


		$ret=array();
		error_log("[ajax/enterprise/enterpriseproductadd] POST : ".json_encode($_POST));

		$db2 = new mysql($config["db2"]);
		$db2->connect();

		$query = "SELECT * FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt`
		          WHERE prefixid = '{$config['default_prefix_id']}'
			        AND storeid = '{$storeid}'
			        AND switch = 'Y'
		";
		error_log("[ajax/enterprise/enterpriseproductadd]".$query);
		$table = $db2->getQueryRecord($query);

		try {
			if(empty($enterpriseid)) {
				// 商户編號不可為空白
				$ret['status']=0;
				$app=getRetJSONArray(-10050,'商户請先登入 !!','MSG');
			} else if(empty($sname)) {
				// 产品名稱不可為空白
				$ret['status']=-2;
				$ret['kind']='产品名稱不可為空白!!';
				$app=getRetJSONArray(-10051,'产品名稱不可為空白 !!','MSG');
			} else if(empty($retail_price)) {
				// 提現密碼不可為空白
				$ret['status']=-2;
				$ret['kind']='巿價不可為空白!!';
				$app=getRetJSONArray(-10051,'巿價不可為空白 !!','MSG');
			} else if(empty($ontime)) {
				// 起標时间不能為空白
				$ret['status']=-2;
				$ret['kind']='起標时间不能為空白!!';
				$app=getRetJSONArray(-10051,'起標时间不能為空白 !!','MSG');
			} else if(empty($offtime)) {
				// 结標时间不能為空白
				$ret['status']=-2;
				$ret['kind']='结標时间不能為空白!!';
				$app=getRetJSONArray(-10051,'结標时间不能為空白 !!','MSG');
			} else if(empty($flash_loc)) {
				// 閃殺地奌不能為空白
				$ret['status']=-2;
				$ret['kind']='閃殺地奌不能為空白!!';
				$app=getRetJSONArray(-10051,'閃殺地奌不能為空白 !!','MSG');
			} else if(empty($contact_info)) {
				// 聯络方式不能為空白
				$ret['status']=-2;
				$ret['kind']='聯络方式不能為空白!!';
				$app=getRetJSONArray(-10051,'聯络方式不能為空白 !!','MSG');
			} else {
				// 初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();

				//新增商品主圖資料
				$query = "INSERT INTO `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail`
				SET
					`prefixid` = '{$config['default_prefix_id']}'
					, `name` = '{$sname}'
					, `original` = '{$pic}'
					, `filename` = '{$pic}'
					, `insertt` = NOW()
				";
				$res = $db->query($query);
				$ptid = $db->_con->insert_id;
				error_log("[ajax/enterprise/enterpriseproductadd] ptid : ".$ptid);

				//新增商品資料
				$query2 = "INSERT INTO `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
				SET
					`prefixid` = '{$config['default_prefix_id']}',
					`name`			= '{$sname}',
					`description`	= '{$sname}',
					`retail_price`	= '{$retail_price}',
					`ptid`			= '{$ptid}',
					`ontime`		= '{$ontime}',
					`offtime`		= '{$offtime}',
					`saja_limit`	= '{$saja_limit}',
					`user_limit`	= '{$user_limit}',
					`usereach_limit`= '{$usereach_limit}',
					`totalfee_type`	= '{$totalfee_type}',
					`process_fee`	= '0',
					`gift`			= '0',
					`vendorid`		= 'saja',
					`split_rate`	= '0',
					`base_value`	= '0',
					`display`		= 'N',
					`bid_type`		= '{$bid_type}',
					`mob_type`		= 'all',
					`loc_type`		= 'L',
					`is_flash`		= '{$is_flash}',
					`flash_loc`     = '{$flash_loc}',
					`seq`			= '0',
					`description2`	= '{$sname}',
					`contact_info`  = '{$contact_info}',
					`insertt`		= now()
				";
				$res2 = $db->query($query2);
				$productid = $db->_con->insert_id;
				error_log("[ajax/enterprise/enterpriseproductadd] productid : ".$productid);

				//新增商品分類關聯
				$query3 = "INSERT INTO `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt`
				SET
					`prefixid` = '{$config['default_prefix_id']}'
					, `productid` = '{$productid}'
					, `pcid` = '0'
					, `insertt` = NOW()
				";
				$res3 = $db->query($query3);
				error_log("[ajax/enterprise/enterpriseproductadd] pcr : ".$query3);

				$db = new mysql($config["db"]);
				$db->connect();
				//新增商家商品關聯
				$query4 = "INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt`
				SET
					`prefixid` = '{$config['default_prefix_id']}'
					, `storeid` = '{$storeid}'
					, `productid` = '{$productid}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$res4 = $db->query($query4);
				error_log("[ajax/enterprise/enterpriseproductadd] spr : ".$query4);

				//新增商品下標資格
				$query5 = "INSERT INTO `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt`
				SET
					`prefixid` = '{$config['default_prefix_id']}'
					, `srid` = 'any_saja'
					, `productid` = '{$productid}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$res5 = $db->query($query5);
				error_log("[ajax/enterprise/enterpriseproductadd] prt : ".$query5);

				$ret['status']=1;
				$app = getRetJSONArray(1,'OK','MSG');

			}
		} catch (Exception $e) {
			$ret['status'] = -1;
			$app=getRetJSONArray(-1,'新增产品失敗 !!','MSG');
			error_log("[ajax/enterprise/enterpriseproductadd] : ".$e->getMessage());
		} finally {
			if($json=='Y') {
				echo json_encode($app);
			} else {
				echo json_encode($ret);
			}
			exit;
		}
	}

	//修改閃殺商品
	public function enterpriseproductupdate() {

		global $db, $config, $enterprisemodel;

		$enterpriseid = $_SESSION['sajamanagement']['enterprise']['enterpriseid'];

		//企業帳密相關
		$sname=(empty($_POST['sname']))?$_GET['sname']:$_POST['sname'];
		$retail_price=(empty($_POST['retail_price']))?$_GET['retail_price']:$_POST['retail_price'];
		$ontime=(empty($_POST['ontime']))?$_GET['ontime']:$_POST['ontime'];
		$offtime=(empty($_POST['offtime']))?$_GET['offtime']:$_POST['offtime'];
		$flash_loc=(empty($_POST['flash_loc']))?$_GET['flash_loc']:$_POST['flash_loc'];
		$contact_info=(empty($_POST['contact_info']))?$_GET['contact_info']:$_POST['contact_info'];
		$pic=(empty($_POST['pic']))?$_GET['pic']:$_POST['pic'];
		$spic=(empty($_POST['spic']))?$_GET['spic']:$_POST['spic'];
		$opic=(empty($_POST['opic']))?$_GET['opic']:$_POST['opic'];
		$productid=(empty($_POST['productid']))?$_GET['productid']:$_POST['productid'];

		$ret=array();
		error_log("[ajax/enterprise/enterpriseproductupdate] POST : ".json_encode($_POST));

		$db2 = new mysql($config["db2"]);
		$db2->connect();

		$query = "SELECT ptid FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
		          WHERE prefixid = '{$config['default_prefix_id']}'
			        AND productid = '{$productid}'
			        AND switch = 'Y'
		";
		$table = $db2->getQueryRecord($query);
		$ptid = $table['table']['record'][0]['ptid'];
		error_log("[ajax/enterprise/enterpriseproductupdate] ptid: ".$ptid);

		try {
			if(empty($enterpriseid)) {
				// 商户編號不可為空白
				$ret['status']=0;
				$app=getRetJSONArray(-10050,'商户請先登入 !!','MSG');
			} else if(empty($sname)) {
				// 产品名稱不可為空白
				$ret['status']=-2;
				$ret['kind']='产品名稱不可為空白!!';
				$app=getRetJSONArray(-10051,'产品名稱不可為空白 !!','MSG');
			} else if(empty($retail_price)) {
				// 提現密碼不可為空白
				$ret['status']=-2;
				$ret['kind']='巿價不可為空白!!';
				$app=getRetJSONArray(-10051,'巿價不可為空白 !!','MSG');
			} else if(empty($ontime)) {
				// 起標时间不能為空白
				$ret['status']=-2;
				$ret['kind']='起標时间不能為空白!!';
				$app=getRetJSONArray(-10051,'起標时间不能為空白 !!','MSG');
			} else if(empty($offtime)) {
				// 结標时间不能為空白
				$ret['status']=-2;
				$ret['kind']='结標时间不能為空白!!';
				$app=getRetJSONArray(-10051,'结標时间不能為空白 !!','MSG');
			} else if(empty($flash_loc)) {
				// 閃殺地奌不能為空白
				$ret['status']=-2;
				$ret['kind']='閃殺地奌不能為空白!!';
				$app=getRetJSONArray(-10051,'閃殺地奌不能為空白 !!','MSG');
			} else if(empty($contact_info)) {
				// 聯络方式不能為空白
				$ret['status']=-2;
				$ret['kind']='聯络方式不能為空白!!';
				$app=getRetJSONArray(-10051,'聯络方式不能為空白 !!','MSG');
			} else {

				// 初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();

				error_log("[ajax/enterprise/enterpriseproductupdate] pic: ".$pic);

				if ((!empty($pic)) && ($pic != $opic)) {
					error_log("[ajax/enterprise/enterpriseproductupdate] opic: ".$opic);

					//修改商品主圖資料
					$query2 = " UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail`
					SET `name` = '{$sname}'
						, `original` = '{$pic}'
						, `filename` = '{$pic}'
						, `modifyt` = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND ptid = '{$ptid}'
						AND switch = 'Y'
					";
					error_log("[ajax/enterprise/enterpriseproductupdate] pt: ".$query2);
					$res = $db->query($query2);
					error_log("[ajax/enterprise/enterpriseproductupdate] pt res: ".json_encode($res));
				}

				//修改商品資料
				$query3 = " UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
				SET
					`prefixid` = '{$config['default_prefix_id']}',
					`name`			= '{$sname}',
					`description`	= '{$sname}',
					`retail_price`	= '{$retail_price}',
					`ontime`		= '{$ontime}',
					`offtime`		= '{$offtime}',
					`flash_loc`		= '{$flash_loc}',
					`description2`	= '{$sname}',
					`contact_info`  = '{$contact_info}',
					`modifyt`		= now()
				WHERE
					`prefixid` = '{$config['default_prefix_id']}'
					AND productid = '{$productid}'
					AND switch = 'Y'
				";
				error_log("[ajax/enterprise/enterpriseproductupdate] pt: ".$query3);

				$res2 = $db->query($query3);
				error_log("[ajax/enterprise/enterpriseproductupdate] pro: ".json_encode($res2));

				$ret['status']=1;
				$app = getRetJSONArray(1,'OK','MSG');

			}
		} catch (Exception $e) {
			$ret['status'] = -1;
			$app=getRetJSONArray(-1,'修改产品失敗 !!','MSG');
			error_log("[ajax/enterprise/enterpriseproductadd] : ".$e->getMessage());
		} finally {
			if($json=='Y') {
				echo json_encode($app);
			} else {
				echo json_encode($ret);
			}
			exit;
		}
	}

}
?>