// JavaScript Document
// 閃殺連續出價
function kuso_bid_n(pid) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var obida = $('#'+pageid+' #bidb').val();
	var obidb = $('#'+pageid+' #bide').val();
	var oscode_num=$('#'+pageid+' #oscode_num').text();
	var oscode_needed = 0;
	
	if (obida == "" || obidb == "") {
		alert('出價起訖區間不能空白!');
	} 
	else if(isNaN(Number(obida))==true || isNaN(Number(obidb))==true) {
	    alert('出價必須為數字!');   
	}
	else if (Number(obida) < 1 || Number(obidb) < 1) {
		alert('連續出價不能小於1!');
	}
	else if (!obida.match("^[0-9]+(.[0-9]{1,2})?$") || !obidb.match("^[0-9]+(.[0-9]{1,2})?$")) {
		alert('價格最小單位為小數點兩位!');
	}
	else if(Number(obida)==Number(obidb)) {
	    alert('出價起訖區間不能相同!');
	} 
	else if(Number(obida)>Number(obidb)) {
	    alert('價格請由低填到高!');
	}	
	else
	{
		oscode_needed = Math.round((Number(obidb)-Number(obida))/1)+1;
		if(oscode_needed>parseInt(oscode_num)) {
		   alert('現有殺價券數量不足!');
		   return ;
		}
		$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});
		$.post(APP_DIR+"/ajax/house_product.php?t="+getNowTime(),
		{
			type: "range",
			productid: pid,
			price_start: obida,
			price_stop: obidb,
			pay_type: 'oscode',
			use_sgift: 'N'
		}, 
		function(str_json) {
			if(str_json) {
				$.mobile.loading('hide');
				var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
				
				if(json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 1) {
					alert('連續出價失敗');
				} else {
					var msg = '';
					var msg2 = '';
					var total_num=json.total_num;     // 下標次數
					var num_uprice=json.num_uniprice;  // 唯一出價的個數
					var lowest_uprice=json.lowest_uniprice; // 最低的唯一出價
					if (json.status==200){ 
						if (json.winner) { 
							//msg2 = ' , 本次得標者 ' + json.winner; 
							$('#'+pageid+' #bidded').html(json.winner);
						}
						if(total_num>=2) {
							if(num_uprice>0) {
							   msg='本次有 '+num_uprice+' 個出價是唯一價, 其中最低價格是 '+lowest_uprice+ ' 元 !\n';
							} else {
							   msg='本次下標的出價都與其他人重複了 !\n';
							}
						}
						alert('連續出價 '+obida+'至'+obidb+'元 成功 !\n'+ msg);
						$('#'+pageid+' #bidb').val("");
						$('#'+pageid+' #bide').val("");
						$('#'+pageid+' #oscode_num').text(oscode_num-oscode_needed);
					} 
					else if(json.status==2){ alert('親，這檔商品已結標囉 !'); } 
					else if(json.status==3){ alert('下標間隔過短，請稍後再下 !'); } 
					else if(json.status==4){ alert('金額記得填 !'); } 
					else if(json.status==5){ alert('金額格式錯誤 !'); } 
					else if(json.status==6){ alert('金額低於底價 !'); } 
					else if(json.status==7){ alert('已經超過連續下標時間囉 !'); }
					else if(json.status==8){ alert('超過下標次數限制 !'); }
					else if(json.status==9){ alert('價格請由低填到高 !'); }
					else if(json.status==10){ alert('出價金額超過市價，太貴囉 !'); }
					else if(json.status==11){ alert('超過下標次數限制 !'); }
					else if(json.status==12){ alert('殺價券不足 !'); }
					else if(json.status==13){ alert('殺價幣不足，請充值 !'); }
					else if(json.status==14){ alert('殺價券不足 !'); }
					else if(json.status==15){ alert('限新 手(未得標過者)'); }
					else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
					else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
					else if(json.status==18){ 
					     alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !'); 
					     location.href='/evt/2854.php';                     
					}
					else{ alert(json.retMsg); }
				}
			}
		});
	}
}

//下標連續出價
function bid_n(pid,paytype) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var count = $('#'+pageid+' #count').val();	
	var obida = $('#'+pageid+' #bida').val();
	var obidb = $('#'+pageid+' #bidb').val();
	var opaytype = paytype;
	var ouserid = $('#'+pageid+' #user').val();
	var oautobid = $('#'+pageid+' #autobid').val();	
	var oscode_num = $('#'+pageid+' #oscode_num').val();
	var scode_num = $('#'+pageid+' #scode_num').val();
	var dscode_num = $('#'+pageid+' #oscode_num').val();
	var dscode_num2 = $('#'+pageid+' #scode_num').val();
	var oscode_needed = 0;
	if(opaytype=='') { 	  
	   alert('請指定下標方式 !!');
	} else if (obida == "" || obidb == "") {
		alert('連續出價不能空白 !!');
	} else if(isNaN(Number(obida))==true || isNaN(Number(obidb))==true) {
	    alert('出價必須為數字 !!');   
	} else if (Number(obida) < 1 || Number(obidb) < 1) {
		alert('連續出價不能小於1 !!');
	} else if (!obida.match("^[0-9]*[1-9][0-9]*$") || !obidb.match("^[0-9]*[1-9][0-9]*$")) {
		alert('價格最小單位為整數點 !!');
	} else if(Number(obida)==Number(obidb)) {
	    alert('出價起訖區間不能相同 !!');
	} else if(Number(obida) > Number(obidb)) {
	    alert('起始價格不可超過結束價格 !!');
	} else {
		if((ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861') && (opaytype == 'cash') && ( oautobid != 'Y')) {
			location.href=APP_DIR+'/deposit/onlinepay/?bida='+obida+'&bidb='+obidb+'&productid='+pid+'&type=range&count='+count;
		} else {
			$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});
			$.post(APP_DIR+"/ajax/product.php?t="+getNowTime(), 
			{
				type: "range",
				productid: pid,
				price_start: obida,
				price_stop: obidb,
				pay_type: opaytype,
				oscode_num: oscode_num,
				scode_num: scode_num,
				dscode_num: dscode_num,
				dscode_num2: dscode_num2,
				use_sgift: 'N'
			}, 
			function(str_json) {
				if(str_json) {
					$.mobile.loading('hide');
					var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
	
					if (json.status==1) {
						alert('殺友請先登入 !!');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('連續出價失敗 !!');
					} else {
						var msg = '';
						var msg2 = '';
						var total_num=json.total_num;     // 下標次數
						var num_uprice=json.num_uniprice;  // 唯一出價的個數
						var lowest_uprice=json.lowest_uniprice; // 最低的唯一出價
						if (json.status==200){ 
							if(total_num>=2) {
								if(num_uprice>0) {
								   msg='本次有 '+num_uprice+' 個出價是唯一價,\n 其中最低價格是 '+lowest_uprice+ ' 元 !!\n';
								} else {
								   msg='本次下標的出價都與其他人重複了 !!\n';
								}
							}
							$('#'+pageid+' #bidb').val("");
							$('#'+pageid+' #bide').val("");
							sendmsg = '連續出價 xxx 至 yyy, 成功!! \n '+ msg; 
							location.href=encodeURI(APP_DIR+"/sys/showMsg/?code="+json.status+"&msg="+sendmsg+"&kind=product&kid="+pid+"&obida="+obida+"&obidb="+obidb);
						} 
						else if(json.status==2){
                            alert("本商品已結標 !!");
                            location.href = "/site/product/";
                        } 
						else if(json.status==3){ alert('下標間隔過短，請稍後再下 !!'); } 
						else if(json.status==4){ alert('金額記得填 !!'); } 
						else if(json.status==5){ alert('金額格式錯誤 !!'); } 
						else if(json.status==6){ alert('金額低於底價 !!'); } 
						else if(json.status==7){ alert('已經超過連續下標時間囉 !!'); }
						else if(json.status==8){ alert('超過下標次數限制 !!'); }
						else if(json.status==9){ alert('價格請由低填到高 !!'); }
						else if(json.status==10){ alert('出價金額超過市價，太貴囉 !!'); }
						else if(json.status==11){ alert('超過下標次數限制 !!'); }					
						else if(json.status==12){ alert('出價起訖區間不可相同 !!'); }
						else if(json.status==13){ 
							alert('殺價幣不足，請充值 !!');
							if (ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861'){
								location.href=APP_DIR+'/deposit/onlinepay/?bida='+obida+'&bidb='+obidb+'&productid='+pid+'&type=range&count='+count;
							}
						} 
						else if(json.status==14){ alert('殺價券不足 !!'); }
						else if(json.status==15){ alert('限新 手(未得標過者)'); }
						else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
						else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
						else if(json.status==18){ 
							 alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !!'); 
							 location.href='/evt/2854.php';                     
						}
						else if(json.status==19){ alert('超級殺價券不足 !!'); }
						else if(json.status==20){ alert('下標總金額超過市價 !!'); }
						else{ alert(json.retMsg); }
					}
				}
			});
		}
	}
}

function kuso_bid_1(pid) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var opaytype = 'oscode';
	var obid = $('#'+pageid+' #bid').val();
	
	if(obid == "") {
		alert('單次出價不能空白');
	} else if(isNaN(Number(obid))==true) {
	    alert('出價必須為數字');  
	} else if (Number(obid) < 1) {
		alert('單次出價不能小於1');
	} else if (!obid.match("^[0-9]*[1-9][0-9]*$")) {
		alert('價格最小單位為整數');
	} else {
		$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});
		obid=parseFloat(obid);
		$.post(APP_DIR+"/ajax/product.php?t="+getNowTime(), 
		{
			type: "single",
			productid: pid,
			price: obid,
			pay_type: opaytype,
			use_sgift: 'N'
		}, 
		function(str_json) {
			if(str_json) {
				$.mobile.loading('hide');
				
				var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
				console && console.log(json);
				
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 1) {
					alert('單次出價失敗');
				} else {
					var msg = '';
					var msg2 = '';
					if (json.status=='200') { 
						var oscode_num=parseInt($('#oscode_num').val())-1;
						$('#oscode_num').val(oscode_num);
						if(oscode_num>1) {
							if(json.rank) {
								if(json.rank==-1) {
									msg="很抱歉！這個出價與其他人重複了！\n請您試試其他出價或許會有好運喔！";
								} else if(json.rank==0) {
									//msg = "恭喜！您是目前的得標者！\n記得在時間到之前要多布局！";
                                    msg = "恭喜！您是目前的得標者！";
								} else if (json.rank>=1 && json.rank<=10) {
									// msg = ' , 本次下標順位為第 ' + json.rank + ' 名'; 
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的還有 "+(json.rank)+" 位。";
								} else if (json.rank>10) {
									// msg = ' , 本次下標順位超過第 12 名或與他人重覆下標';
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
								}
							}
							if(json.winner) { 
								//msg2 = ' , 本次得標者 ' + json.winner; 
								$('#bidded').html(json.winner);
							}
							alert("單次出價 "+obid+"元 成功\n"+ msg); 
						}
						
						
						if(oscode_num<=0) {
						    $('#bid_result').text(obid+" 元");
							$('#bid_block').hide();
							$('#view_block').show();
							$('#prod_info').hide();
						} else {
						    $('#bid_block').show();
							$('#view_block').hide();
							$('#prod_info').show();
						}
					} 
					else if(json.status==2){ alert('親，這檔商品已結標囉'); } 
					else if(json.status==3){ alert('下標間隔過短，請稍後再下'); } 
					else if(json.status==4){ alert('金額記得填!'); } 
					else if(json.status==5){ alert('金額格式錯誤'); } 
					else if(json.status==6){ alert('金額低於底價'); } 
					else if(json.status==7){ alert('已經超過連續下標時間囉!'); }
					else if(json.status==8){ alert('超過下標次數限制'); }
					else if(json.status==9){ alert('價格請由低填到高!'); }
					else if(json.status==10){ alert('金額超過商品市價'); }
					else if(json.status==11){ alert('超過可下標次數限制!'); }
					else if(json.status==12){ alert('殺價券不足!'); }
					else if(json.status==13){ alert('殺價幣不足，請充值!'); }
					else if(json.status==14){ alert('殺價券不足!'); }
					else if(json.status==15){ alert('限新 手(未得標過者)'); }
					else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
					else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
					else if(json.status==18){ 
					     alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !'); 
					     location.href='/evt/2854.php';                     
					}
					else if(json.status==20){ alert('下標總金額超過市價 !!'); }
					else{ alert(json.retMsg); }
				}
			}
		});
	}
}

//單次出價
function bid_1(pid,paytype) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var count = $('#'+pageid+' #count').val();	
	var obid = $('#'+pageid+' #bida').val();
	var ouserid = $('#'+pageid+' #user').val();
	var oautobid = $('#'+pageid+' #autobid').val();
	var oscode_num = $('#'+pageid+' #oscode_num').val();
	var scode_num = $('#'+pageid+' #scode_num').val();
	var oscode_needed = 0;
	var opaytype = paytype;
	
	if(obid == "") {
		alert('單次出價不能空白');
	} else if(isNaN(Number(obid))==true) {
	    alert('出價必須為數字');  
	} else if (Number(obid) < 1) {
		alert('單次出價不能小於1');
	} else if (!obid.match("^[0-9]*[1-9][0-9]*$")) {
		alert('價格最小單位為整數');
	} else {
		$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});

		if ((ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861') && (opaytype == 'cash') && ( oautobid != 'Y')) {
			location.href=APP_DIR+'/deposit/onlinepay/?bida='+obid+'&bidb=0&productid='+pid+'&type=single&count='+count;
		} else {
			$.post(APP_DIR+"/ajax/product.php?t="+getNowTime(), 
			{
				type: "single",
				productid: pid,
				price: obid,
				pay_type: opaytype,
				oscode_num: oscode_num,
				scode_num:scode_num,
				use_sgift: 'N'
			}, 
			function(str_json) {
				if(str_json) {
					$.mobile.loading('hide');
					var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
					if (json.status==1) {
						alert('殺友請先登入');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('單次出價失敗');
					} else {
						var msg = '';
						var msg2 = '';
						if (json.status=='200'){ 
							// if (json.rank) {
								if (json.rank==-1) {
									msg="很抱歉！這個出價與其他人重複了！\n請您試試其他出價或許會有好運喔！";
								} else if(json.rank==0) {
									//msg = "恭喜！您是目前的得標者！\n記得在時間到之前要多布局！";
                                    msg = "恭喜！您是目前的得標者！";
								} else if (json.rank>=1 && json.rank<=10) {
									// msg = ' , 本次下標順位為第 ' + json.rank + ' 名'; 
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的還有 "+(json.rank)+" 位。";
								} else if (json.rank>10) {
									// msg = ' , 本次下標順位超過第 12 名或與他人重覆下標';
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
								} else {
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
								}
							// }
							$('#'+pageid+' #bid').val("");
							sendmsg = "單次出價 xxx 成功 !!\n "+ msg; 
							location.href=encodeURI(APP_DIR+"/sys/showMsg/?code="+json.status+"&msg="+sendmsg+"&kind=product&kid="+pid+"&obid="+obid);
						} else if(json.status==2) {
                            alert("本商品已結標 !!");
                            location.href = "/site/product/";
                        } 
						else if(json.status==3){ alert('下標間隔過短，請稍後再下'); } 
						else if(json.status==4){ alert('金額記得填!'); } 
						else if(json.status==5){ alert('金額格式錯誤'); } 
						else if(json.status==6){ alert('金額低於底價'); } 
						else if(json.status==7){ alert('已經超過連續下標時間囉!'); }
						else if(json.status==8){ alert('超過下標次數限制'); }
						else if(json.status==9){ alert('價格請由低填到高!'); }
						else if(json.status==10){ alert('金額超過商品市價'); }
						else if(json.status==11){ alert('超過可下標次數限制!'); }
						else if(json.status==12){ alert('殺價券不足!');	}
						else if(json.status==13){ 
							alert('殺價幣不足，請充值!'); 
							if (ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861'){
								location.href=APP_DIR+'/deposit/onlinepay/?bida='+obid+'&bidb=0&productid='+pid+'&type=single&count='+count;
							}
						}
						else if(json.status==14){ alert('殺價券不足!'); }
						else if(json.status==15){ alert('限新 手(未得標過者)'); }
						else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
						else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
						else if(json.status==18){ 
							 alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !'); 
							 location.href='/evt/2854.php';                     
						}
						else if(json.status==19){ alert('超級殺價券不足 !!'); }
						else if(json.status==20){ alert('下標總金額超過市價 !!'); }
						else{ alert(json.retMsg); }
					}
				}
			});
		}
	}
}

function sajabid_single() {
	var pageid = $.mobile.activePage.attr('id');
	var obida = $('#'+pageid+' #price').val();
	var osaja_fee = $('#'+pageid+' #saja_fee').val();
	var totalfee_type = $('#'+pageid+' #totalfee_type').val();	
	var oretail_price =  $('#'+pageid+' #retail_price').val();	
	
	$('#'+pageid+' #bid_single').html('');
    $('#'+pageid+' #font').html('');
	$('#'+pageid+' #bid_count').html('');
	$('#'+pageid+' #fee_total').html('');
	
	$('#topay').attr('disabled',true);
	
	if (obida == "") {
	} else if(isNaN(Number(obida))==true) {
        $('#'+pageid+' #bid_single').html('<h2>標價只能輸入數字</h2>');
	} else if (Number(obida) < 1) {
        $('#'+pageid+' #bid_single').html('<h2>標價不得小於1</h2>');
	} else if (!obida.match("^[0-9]*[1-9][0-9]*$")) {
		$('#'+pageid+' #bid_single').html('<h2>標價最小單位為整數</h2>');
	} else if ((Number(obida) > Number(oretail_price)) && totalfee_type !== "O") {
        $('#'+pageid+' #bid_single').html('<h2>標價大於商品巿價</h2>');		
	} else {	
		var oprice_count = 1;
		var oprice_all = (parseFloat(obida));
		var ofee_all = parseFloat(osaja_fee) * parseFloat(oprice_count);
		
		if(totalfee_type == "F"){
			var all_total = parseFloat(ofee_all);
		} else if (totalfee_type == "B") {
			var all_total = parseFloat(oprice_all);
		} else if (totalfee_type == "A") {
			var all_total = parseFloat(oprice_all) + parseFloat(ofee_all);
		} else if (totalfee_type == "O") {
            var all_total = 0;
        }
		
		$('#'+pageid+' #bida').val(parseFloat(obida).toFixed(0));		
		$('#'+pageid+' #bidb').val(0);						
		$('#'+pageid+' #count').val(oprice_count);	
		$('#'+pageid+' #fee_all').val(parseFloat(ofee_all).toFixed(0));
        $('#'+pageid+' #font').html('<h3>此次下標費用如下</h3>');
        $('#'+pageid+' #bid_single').html('<h1><small>共 </small><span class="num">' + parseFloat(all_total).toFixed(0) + '</span><small> 元 </small></h1>');
		$('#topay').attr('disabled',false);
	}
}

function sajabid_range() {
	var pageid = $.mobile.activePage.attr('id');
	var obida = $('#'+pageid+' #price_start').val();
	var obidb = $('#'+pageid+' #price_stop').val();
	var osaja_fee = $('#'+pageid+' #saja_fee').val();
	var oretail_price =  $('#'+pageid+' #retail_price').val();	
	var ousereach_limit = $('#'+pageid+' #usereach_limit').val();

    $('#'+pageid+' #font').html('');
	$('#'+pageid+' #bid_count').html('');
	$('#'+pageid+' #fee_total').html('');
	$('#topay').attr('disabled',true);

    if(isNaN(Number(obida))==true || isNaN(Number(obidb))==true) {
        $('#'+pageid+' #bid_count').html('<h2>標價只能輸入數字</h2>');
	} else if (Number(obida) < 1 || Number(obidb) < 1) {
        $('#'+pageid+' #bid_count').html('<h2>標價不得小於1</h2>');
	} else if (!obida.match("^[0-9]*[1-9][0-9]*$") || !obidb.match("^[0-9]*[1-9][0-9]*$")) {
		$('#'+pageid+' #bid_count').html('<h2>標價最小單位為整數</h2>');
	} else if(Number(obida) == Number(obidb)) { 
        $('#'+pageid+' #bid_count').html('<h2>起始價格不可等於結束價格 !!</h2>');
	} else if(Number(obida) > Number(obidb)) {
		$('#'+pageid+' #bid_count').html('<h2>起始價格不可超過結束價格 !!</h2>');	
	} else if(Number(obidb) > Number(oretail_price)) {
		$('#'+pageid+' #bid_count').html('<h2>結束價格不可超過商品巿價 !!</h2>');		
	} else {	
		var oprice_count = ((parseFloat(obidb) - parseFloat(obida)) / 1) + 1;
		var oprice_all = ((parseFloat(obidb) + parseFloat(obida)) * oprice_count) / 2;
		var ofee_all = parseFloat(osaja_fee) * parseFloat(oprice_count);
		var all_total = parseFloat(oprice_all) + parseFloat(ofee_all);
		
		if (Number(oprice_count) > ousereach_limit) {
			$('#'+pageid+' #bid_count').html('<h2>下標數不能大於'+ousereach_limit+' !!</h2>');
		} else {
			$('#'+pageid+' #bida').val(parseFloat(obida).toFixed(0));		
			$('#'+pageid+' #bidb').val(parseFloat(obidb).toFixed(0));						
			$('#'+pageid+' #fee_all').val(parseFloat(ofee_all).toFixed(0));
			$('#'+pageid+' #total_all').val(parseFloat(all_total).toFixed(0));		
			$('#'+pageid+' #count').val(parseFloat(oprice_count).toFixed(0));
			$('#'+pageid+' #font').html('<h3>此次下標標數如下</h3>');
			$('#'+pageid+' #bid_count').html('<h1><small>共 </small><span class="num">' + parseFloat(oprice_count).toFixed(0) + '</span><small> 標 </small></h1>');
			$('#topay').attr('disabled',false);
		}
	}
}

function sajabid_lazy() {
	var pageid = $.mobile.activePage.attr('id');
	var olazy_count = $('#'+pageid+' #lazy_count').val();
	var osaja_fee = $('#'+pageid+' #saja_fee').val();
	var oretail_price =  $('#'+pageid+' #retail_price').val();
	var ousereach_limit = $('#'+pageid+' #usereach_limit').val();

	$('#'+pageid+' #font').html('');
	$('#'+pageid+' #start_stop').html('');
	$('#'+pageid+' #fee_total').html('');
	$('#topay').attr('disabled',true);
	if(olazy_count == "") {
	    
	} else if(isNaN(Number(olazy_count))==true) {
        $('#'+pageid+' #start_stop').html('<h2>標價只能輸入數字</h2>');
	} else if (Number(olazy_count) < 1) {
        $('#'+pageid+' #start_stop').html('<h2>下標數不能小於1 !!</h2>');
	} else if (Number(olazy_count) > ousereach_limit) {
        $('#'+pageid+' #start_stop').html('<h2>下標數不能大於'+ousereach_limit+' !!</h2>');
    } else if (olazy_count.length > 3) {
        $('#'+pageid+' #start_stop').html('<h2>下標數不能超過3位數 !!</h2>');
	} else if (!olazy_count.match("^[0-9]*[1-9][0-9]*$")) {
        $('#'+pageid+' #start_stop').html('<h2>下標數請輸入整數!</h2>');
	} else {	
		var oprice_count = olazy_count;
		var rnd=0.0;
		do {
		  rnd=Math.random();  
		} while(rnd<0.01); 
		
		var obida = rnd * parseFloat(oretail_price)*0.35;   
		var obidb = parseFloat(obida + (1 * (olazy_count-1))); 	
		var oprice_all = ((parseFloat(obidb) + parseFloat(obida)) * oprice_count) / 2;
		var ofee_all = parseFloat(osaja_fee) * parseFloat(oprice_count);
		var all_total = parseFloat(oprice_all) + parseFloat(ofee_all);
		
		$('#'+pageid+' #bida').val(parseFloat(obida).toFixed(0));		
		$('#'+pageid+' #bidb').val(parseFloat(obidb).toFixed(0)); 
		$('#'+pageid+' #count').val(parseFloat(oprice_count).toFixed(0));		
		$('#'+pageid+' #fee_all').val(parseFloat(ofee_all).toFixed(0));
		$('#'+pageid+' #total_all').val(parseFloat(all_total).toFixed(0));
		$('#'+pageid+' #font').html('<h3>此次隨機下標費用如下</h3>');
		$('#'+pageid+' #start_stop').html('<h1>' +parseFloat(obida).toFixed(0)+ ' - '+parseFloat(obidb).toFixed(0) + '</h1>' );		
		$('#topay').attr('disabled',false);
	}
}

function saja_gopay() {
	
	var pageid = $.mobile.activePage.attr('id');
	var olazy_count = $('#'+pageid+' #lazy_count').val();
	var obid = $('#'+pageid+' #price').val();
	var obida = $('#'+pageid+' #price_start').val();
	var obidb = $('#'+pageid+' #price_stop').val();
	var ocount = $('#'+pageid+' #count').val();
	var ofee_all = $('#'+pageid+' #fee_all').val();
	var ototal_all = $('#'+pageid+' #total_all').val();		
	var opaytype = $('#'+pageid+' #type').val();
	var ouserid = $('#'+pageid+' #user').val();
	var oautobid = $('#'+pageid+' #autobid').val();
	var oretail_price =  $('#'+pageid+' #retail_price').val();
	
    $('#topay').attr('disabled',true);
	if(opaytype == "single") {
        if(obid == "") {
			alert('單次出價不能空白');
			$('#price').val('');
			$('#price').focus();
		} else if (isNaN(Number(obid))==true) {
			alert('出價必須為數字'); 
            $('#price').val('');
            $('#price').focus();			
		} else if (Number(obid) < 1) {
			alert('單次出價不能小於1');
			$('#price').val('');
			$('#price').focus();
		} else if (!obid.match("^[0-9]*[1-9][0-9]*$")) {
			alert('價格最小單位為整數');
			$('#price').focus();
			$('#price').val('');
		} else if (Number(obid) > Number(oretail_price)) {
			alert('出價金額不能大於商品巿價');
			$('#price').val('');
			$('#price').focus();			
		} else {
			document.getElementById("sajabid").submit();
		}
	} else if (opaytype == "range") {
	    if(obida == "") {
			alert('連續出價不能空白 !');
			$('#price_start').focus();
		} else if(obidb == "") {
		    alert('連續出價不能空白 !!');
			$('#price_stop').focus();
		} else if(isNaN(Number(obida))==true) {
			alert('出價必須為數字 !');
			$('#price_start').val('');
            $('#price_start').focus();			
        } else if(isNaN(Number(obidb))==true) {
		    alert('出價必須為數字 !');
			$('#price_stop').val('');
            $('#price_stop').focus();	
		} else if (Number(obida) < 1) {
			alert('連續出價不能小於1 !');
			$('#price_start').val('');
            $('#price_start').focus();	
		} else if (Number(obidb) < 1) {
			alert('連續出價不能小於1 !!');
			$('#price_stop').val('');
            $('#price_stop').focus();	
		} else if (!obida.match("^[0-9]*[1-9][0-9]*$") ) {
			alert('起始價格最小單位為整數 !');
			$('#price_start').val('');
            $('#price_start').focus();	
		} else if (!obidb.match("^[0-9]*[1-9][0-9]*$")) {
			alert('結束價格最小單位為整數 !!');
			$('#price_stop').val('');
            $('#price_stop').focus();
		} else if(Number(obida)==Number(obidb)) {
			alert('出價起訖區間不能相同 !!');
			$('#price_start').focus();
		} else if(Number(obida) > Number(obidb)) {
			alert('起始價格不可超過結束價格 !!');
			$('#price_stop').focus();
		} else if(Number(obidb) > Number(oretail_price)) {
			alert('結束價格不可超過商品巿價 !!');
			$('#price_stop').focus();			
		} else {
			document.getElementById("sajabid").submit();
		}
	} else {
		if(olazy_count == "") {
			alert('下標數不能空白 !!');	
            $('#lazy_count').focus();			
		} else if(isNaN(Number(olazy_count))==true) {
			alert('請指定下標數 !!');
            $('#lazy_count').val('');
			$('#lazy_count').focus();				
		} else if (Number(olazy_count) < 1) {
			alert('下標數不能小於1 !!');
			$('#lazy_count').val('');
			$('#lazy_count').focus();	
		} else if (Number(olazy_count) > 100) {
			alert('下標數不能大於100 !!');
			$('#lazy_count').val('');
			$('#lazy_count').focus();	
		} else if (!olazy_count.match("^[0-9]*[1-9][0-9]*$")) {
			alert('下標數須為整數!');
			$('#lazy_count').val('');
			$('#lazy_count').focus();	
        } else if(olazy_count.length > 3){    
            alert('下標數不可超過3位數!');
			$('#lazy_count').val('');
			$('#lazy_count').focus();
		} else if(Number(obidb) > Number(oretail_price)) {
			alert('結束價格不可超過商品巿價 !!');
			$('#lazy_count').val('');
			$('#price_stop').focus();
		} else {
			document.getElementById("sajabid").submit();
		}
	}
}

function ReverseDisplay(d,click,blod,scroll) {
    /*
        d : 欲展開的div "id";
        click : 使用"this"傳入，判斷點擊哪一個DOM;
        blod : 若要展開時，標題變粗體，參數輸入 "blod"
        scroll : 若要展開時，捲動到標題處，參數輸入 "scroll"
    */
    var $linkBox = $('#'+d);
    var $click = $(click);
    var $selfLinkBox = $(".linkBox.more");
    var $selfArrow = $(".r-arrow");
	if ($linkBox.css("display") == "none") {
        //展開時，先初始化
        $selfLinkBox.css("display","none");
        $(".fontBlod").removeClass("fontBlod");
        $(".down").removeClass("down");
        //箭頭向下
        if(!$click.find(".r-arrow").hasClass("down")){
            $click.find(".r-arrow").addClass("down");
        }
        //捲動到標題處
        if(scroll=="scroll") {
            var $scrollTop = $click.offset().top;       //標題位置
            var $titile = $click.outerHeight(true);     //標題物件高度
            $('body,html').stop().animate({scrollTop:$scrollTop - $titile},500,'swing');       //捲動到標題處
        }
        //標題變粗
        if(blod=="blod") {
            $click.find("[class$='title']").addClass("fontBlod");
        }
        $linkBox.css("display","block");
	} else {
        $linkBox.css("display","none");
        //箭頭向右
        if($click.find(".r-arrow").hasClass("down")){
            $click.find(".r-arrow").removeClass("down");
        }
        if(blod=="blod") {
            $(".fontBlod").removeClass("fontBlod");
        }
	}

}

//原夢商品
function dp_product() {
	
	var pageid = $.mobile.activePage.attr('id');
	var othumbnail = $('#'+pageid+' #thumbnail1').val();
	var odpname = $('#'+pageid+' #dpname').val();
	var odpcontent = $('#'+pageid+' #dpcontent').val();
	var odpaddress = $('#'+pageid+' #dpaddress').val();

	if(othumbnail == "") {
		alert('圓夢商品圖不能空白 !');
		$('#pid-but').focus();
	} else if(odpname == "") {
		alert('圓夢商品名稱不能空白 !!');
		$('#dpname').focus();
	} else if(odpcontent == "") {
		alert('圓夢商品內容不能空白 !!');
		$('#dpcontent').focus();
	} else if(odpaddress == "") {
		alert('圓夢商品出處不能空白 !!');
		$('#dpaddress').focus();
	} else {
		document.getElementById("dpsaja").submit();
	}
}

//圓夢下標連續出價
function dp_bid_n(pid,paytype) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var count = $('#'+pageid+' #count').val();	
	var obida = $('#'+pageid+' #bida').val();
	var obidb = $('#'+pageid+' #bidb').val();
	var opaytype = paytype;
	var ouserid = $('#'+pageid+' #user').val();
	var oautobid = $('#'+pageid+' #autobid').val();	
	var oscode_num = $('#'+pageid+' #oscode_num').val();
	var scode_num = $('#'+pageid+' #scode_num').val();
	
	var odpname = $('#'+pageid+' #dpname').val();	
	var odpcontent = $('#'+pageid+' #dpcontent').val();
	var odpaddress = $('#'+pageid+' #dpaddress').val();
	var odppic = $('#'+pageid+' #dppic').val();
	
	var oscode_needed = 0;
	if(odpname=='') { 	  
	   alert('圓夢商品名稱不能空白 !!');
	} else if(odpcontent=='') { 	  
	   alert('圓夢商品內容不能空白 !!');
	} else if(odpaddress=='') { 	  
	   alert('圓夢商品出處不能空白 !!');
	} else if(odppic=='') { 	  
	   alert('圓夢商品圖不能空白 !!');
	} else if(opaytype=='') { 	  
	   alert('請指定下標方式 !!');
	} else if (obida == "" || obidb == "") {
		alert('連續出價不能空白 !!');
	} else if(isNaN(Number(obida))==true || isNaN(Number(obidb))==true) {
	    alert('出價必須為數字 !!');   
	} else if (Number(obida) < 1 || Number(obidb) < 1) {
		alert('連續出價不能小於1 !!');
	} else if (!obida.match("^[0-9]*[1-9][0-9]*$") || !obidb.match("^[0-9]*[1-9][0-9]*$")) {
		alert('價格最小單位為整數點 !!');
	} else if(Number(obida)==Number(obidb)) {
	    alert('出價起訖區間不能相同 !!');
	} else if(Number(obida) > Number(obidb)) {
	    alert('起始價格不可超過結束價格 !!');
	} else {
		// if((ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861') && (opaytype == 'cash') && ( oautobid != 'Y')) {
			// location.href=APP_DIR+'/deposit/onlinepay/?bida='+obida+'&bidb='+obidb+'&productid='+pid+'&type=range&count='+count;
		// } else {
			$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});
			$.post(APP_DIR+"/ajax/dream_product.php?t="+getNowTime(), 
			{
				type: "range",
				productid: pid,
				price_start: obida,
				price_stop: obidb,
				pay_type: opaytype,
				oscode_num: oscode_num,
				scode_num: scode_num,
				use_sgift: 'N',
				title: odpname,
				content: odpcontent,
				purchase_url: odpaddress,
				pic_name: odppic,
				pic_type:'1',
				filedir:'dream/products/',
				pic:odppic
			}, 
			function(str_json) {
				if(str_json) {
					$.mobile.loading('hide');
					var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
	
					if (json.status==1) {
						alert('殺友請先登入 !!');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('連續出價失敗 !!');
					} else {
						var msg = '';
						var msg2 = '';
						var total_num=json.total_num;     // 下標次數
						var num_uprice=json.num_uniprice;  // 唯一出價的個數
						var lowest_uprice=json.lowest_uniprice; // 最低的唯一出價
						if (json.status==200){ 
							if(total_num>=2) {
								if(num_uprice>0) {
								   msg='本次有 '+num_uprice+' 個出價是唯一價,\n 其中最低價格是 '+lowest_uprice+ ' 元 !!\n';
								} else {
								   msg='本次下標的出價都與其他人重複了 !!\n';
								}
							}
							$('#'+pageid+' #bidb').val("");
							$('#'+pageid+' #bide').val("");
							sendmsg = '連續出價 xxx 至 yyy, 成功!! \n '+ msg; 
							location.href=encodeURI(APP_DIR+"/sys/showMsg/?code="+json.status+"&msg="+sendmsg+"&kind=product&kid="+pid+"&obida="+obida+"&obidb="+obidb);
						} 
						else if(json.status==2){
                            alert("本商品已結標 !!");
                            location.href = "/site/dream_product/";
                        } 
						else if(json.status==3){ alert('下標間隔過短，請稍後再下 !!'); } 
						else if(json.status==4){ alert('金額記得填 !!'); } 
						else if(json.status==5){ alert('金額格式錯誤 !!'); } 
						else if(json.status==6){ alert('金額低於底價 !!'); } 
						else if(json.status==7){ alert('已經超過連續下標時間囉 !!'); }
						else if(json.status==8){ alert('超過下標次數限制 !!'); }
						else if(json.status==9){ alert('價格請由低填到高 !!'); }
						else if(json.status==10){ alert('出價金額超過市價，太貴囉 !!'); }
						else if(json.status==11){ alert('超過下標次數限制 !!'); }					
						else if(json.status==12){ alert('出價起訖區間不可相同 !!'); }
						else if(json.status==13){ 
							alert('殺價幣不足，請充值 !!');
							if (ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861'){
								location.href=APP_DIR+'/deposit/onlinepay/?bida='+obida+'&bidb='+obidb+'&productid='+pid+'&type=range&count='+count;
							}
						} 
						else if(json.status==14){ alert('殺價券不足 !!'); }
						else if(json.status==15){ alert('限新 手(未得標過者)'); }
						else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
						else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
						else if(json.status==18){ 
							 alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !!'); 
							 location.href='/evt/2854.php';                     
						}
						else if(json.status==19){ alert('超級殺價券不足 !!'); }
						else if(json.status==20){ alert('下標總金額超過市價 !!'); }
						else{ alert(json.retMsg); }
					}
				}
			});
		// }
	}
}

//圓夢下標單次出價
function dp_bid_1(pid,paytype) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var count = $('#'+pageid+' #count').val();	
	var obid = $('#'+pageid+' #bida').val();
	var ouserid = $('#'+pageid+' #user').val();
	var oautobid = $('#'+pageid+' #autobid').val();
	var oscode_num = $('#'+pageid+' #oscode_num').val();
	var scode_num = $('#'+pageid+' #scode_num').val();
	var oscode_needed = 0;
	var opaytype = paytype;
	
	var odpname = $('#'+pageid+' #dpname').val();	
	var odpcontent = $('#'+pageid+' #dpcontent').val();
	var odpaddress = $('#'+pageid+' #dpaddress').val();
	var odppic = $('#'+pageid+' #dppic').val();
	
	if(odpname=='') { 	  
	   alert('圓夢商品名稱不能空白 !!');
	} else if(odpcontent=='') { 	  
	   alert('圓夢商品內容不能空白 !!');
	} else if(odpaddress=='') { 	  
	   alert('圓夢商品出處不能空白 !!');
	} else if(odppic=='') { 	  
	   alert('圓夢商品圖不能空白 !!');
	} else if(obid == "") {
		alert('單次出價不能空白');
	} else if(isNaN(Number(obid))==true) {
	    alert('出價必須為數字');  
	} else if (Number(obid) < 1) {
		alert('單次出價不能小於1');
	} else if (!obid.match("^[0-9]*[1-9][0-9]*$")) {
		alert('價格最小單位為整數');
	} else {
		$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});

		// if ((ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861') && (opaytype == 'cash') && ( oautobid != 'Y')) {
			// location.href=APP_DIR+'/deposit/onlinepay/?bida='+obid+'&bidb=0&productid='+pid+'&type=single&count='+count;
		// } else {
			$.post(APP_DIR+"/ajax/dream_product.php?t="+getNowTime(), 
			{
				type: "single",
				productid: pid,
				price: obid,
				pay_type: opaytype,
				oscode_num: oscode_num,
				scode_num:scode_num,
				use_sgift: 'N',
				title: odpname,
				content: odpcontent,
				purchase_url: odpaddress,
				pic_name: odppic,
				pic_type:'1',
				filedir:'dream/products/',
				pic:odppic
			}, 
			function(str_json) {
				if(str_json) {
					$.mobile.loading('hide');
					var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
					if (json.status==1) {
						alert('殺友請先登入');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('單次出價失敗');
					} else {
						var msg = '';
						var msg2 = '';
						if (json.status==200){ 
							// if (json.rank) {
								if (json.rank==-1) {
									msg="很抱歉！這個出價與其他人重複了！\n請您試試其他出價或許會有好運喔！";
								} else if(json.rank==0) {
									//msg = "恭喜！您是目前的得標者！\n記得在時間到之前要多布局！";
                                    msg = "恭喜！您是目前的得標者！";
								} else if (json.rank>=1 && json.rank<=10) {
									// msg = ' , 本次下標順位為第 ' + json.rank + ' 名'; 
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的還有 "+(json.rank)+" 位。";
								} else if (json.rank>10) {
									// msg = ' , 本次下標順位超過第 12 名或與他人重覆下標';
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
								} else {
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
								}
							// }
							$('#'+pageid+' #bid').val("");
							sendmsg = "單次出價 xxx 成功 !!\n "+ msg; 
							location.href=encodeURI(APP_DIR+"/sys/showMsg/?code="+json.status+"&msg="+sendmsg+"&kind=product&kid="+pid+"&obid="+obid);
						} else if(json.status==2) {
                            alert("本商品已結標 !!");
                            location.href = "/site/dream_product/";
                        } 
						else if(json.status==3){ alert('下標間隔過短，請稍後再下'); } 
						else if(json.status==4){ alert('金額記得填!'); } 
						else if(json.status==5){ alert('金額格式錯誤'); } 
						else if(json.status==6){ alert('金額低於底價'); } 
						else if(json.status==7){ alert('已經超過連續下標時間囉!'); }
						else if(json.status==8){ alert('超過下標次數限制'); }
						else if(json.status==9){ alert('價格請由低填到高!'); }
						else if(json.status==10){ alert('金額超過商品市價'); }
						else if(json.status==11){ alert('超過可下標次數限制!'); }
						else if(json.status==12){ alert('殺價券不足!');	}
						else if(json.status==13){ 
							alert('殺價幣不足，請充值!'); 
							if (ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861'){
								location.href=APP_DIR+'/deposit/onlinepay/?bida='+obid+'&bidb=0&productid='+pid+'&type=single&count='+count;
							}
						}
						else if(json.status==14){ alert('殺價券不足!'); }
						else if(json.status==15){ alert('限新 手(未得標過者)'); }
						else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
						else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
						else if(json.status==18){ 
							 alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !'); 
							 location.href='/evt/2854.php';                     
						}
						else if(json.status==19){ alert('超級殺價券不足 !!'); }
						else if(json.status==20){ alert('下標總金額超過市價 !!'); }
						else{ alert(json.retMsg); }
					}
				}
			});
		// }
	}
}


//下標連續出價
function hp_bid_n(pid,paytype) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var count = $('#'+pageid+' #count').val();	
	var obida = $('#'+pageid+' #bida').val();
	var obidb = $('#'+pageid+' #bidb').val();
	var opaytype = paytype;
	var ouserid = $('#'+pageid+' #user').val();
	var oautobid = $('#'+pageid+' #autobid').val();	
	var oscode_num = $('#'+pageid+' #oscode_num').val();
	var scode_num = $('#'+pageid+' #scode_num').val();
	var dscode_num = $('#'+pageid+' #oscode_num').val();
	var dscode_num2 = $('#'+pageid+' #scode_num').val();
	var oscode_needed = 0;
	if(opaytype=='') { 	  
	   alert('請指定下標方式 !!');
	} else if (obida == "" || obidb == "") {
		alert('連續出價不能空白 !!');
	} else if(isNaN(Number(obida))==true || isNaN(Number(obidb))==true) {
	    alert('出價必須為數字 !!');   
	} else if (Number(obida) < 1 || Number(obidb) < 1) {
		alert('連續出價不能小於1 !!');
	} else if (!obida.match("^[0-9]*[1-9][0-9]*$") || !obidb.match("^[0-9]*[1-9][0-9]*$")) {
		alert('價格最小單位為整數點 !!');
	} else if(Number(obida)==Number(obidb)) {
	    alert('出價起訖區間不能相同 !!');
	} else if(Number(obida) > Number(obidb)) {
	    alert('起始價格不可超過結束價格 !!');
	} else {
		if((ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861') && (opaytype == 'cash') && ( oautobid != 'Y')) {
			location.href=APP_DIR+'/deposit/onlinepay/?bida='+obida+'&bidb='+obidb+'&productid='+pid+'&type=range&count='+count;
		} else {
			$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});
			$.post(APP_DIR+"/ajax/house_product.php?t="+getNowTime(), 
			{
				type: "range",
				productid: pid,
				price_start: obida,
				price_stop: obidb,
				pay_type: opaytype,
				oscode_num: oscode_num,
				scode_num: scode_num,
				dscode_num: dscode_num,
				dscode_num2: dscode_num2,
				use_sgift: 'N'
			}, 
			function(str_json) {
				if(str_json) {
					$.mobile.loading('hide');
					var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
	
					if (json.status==1) {
						alert('殺友請先登入 !!');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('連續出價失敗 !!');
					} else {
						var msg = '';
						var msg2 = '';
						var total_num=json.total_num;     // 下標次數
						var num_uprice=json.num_uniprice;  // 唯一出價的個數
						var lowest_uprice=json.lowest_uniprice; // 最低的唯一出價
						if (json.status==200){ 
							if(total_num>=2) {
								if(num_uprice>0) {
								   msg='本次有 '+num_uprice+' 個出價是唯一價,\n 其中最低價格是 '+lowest_uprice+ ' 元 !!\n';
								} else {
								   msg='本次下標的出價都與其他人重複了 !!\n';
								}
							}
							$('#'+pageid+' #bidb').val("");
							$('#'+pageid+' #bide').val("");
							sendmsg = '連續出價 xxx 至 yyy, 成功!! \n '+ msg; 
							location.href=encodeURI(APP_DIR+"/sys/showMsg/?code="+json.status+"&msg="+sendmsg+"&kind=product&kid="+pid+"&obida="+obida+"&obidb="+obidb);
						} 
						else if(json.status==2){
                            alert("本商品已結標 !!");
                            location.href = "/site/product/";
                        } 
						else if(json.status==3){ alert('下標間隔過短，請稍後再下 !!'); } 
						else if(json.status==4){ alert('金額記得填 !!'); } 
						else if(json.status==5){ alert('金額格式錯誤 !!'); } 
						else if(json.status==6){ alert('金額低於底價 !!'); } 
						else if(json.status==7){ alert('已經超過連續下標時間囉 !!'); }
						else if(json.status==8){ alert('超過下標次數限制 !!'); }
						else if(json.status==9){ alert('價格請由低填到高 !!'); }
						else if(json.status==10){ alert('出價金額超過市價，太貴囉 !!'); }
						else if(json.status==11){ alert('超過下標次數限制 !!'); }					
						else if(json.status==12){ alert('出價起訖區間不可相同 !!'); }
						else if(json.status==13){ 
							alert('殺價幣不足，請充值 !!');
							if (ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861'){
								location.href=APP_DIR+'/deposit/onlinepay/?bida='+obida+'&bidb='+obidb+'&productid='+pid+'&type=range&count='+count;
							}
						} 
						else if(json.status==14){ alert('圓夢券不足 !!'); }
						else if(json.status==15){ alert('限新 手(未得標過者)'); }
						else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
						else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
						else if(json.status==18){ 
							 alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !!'); 
							 location.href='/evt/2854.php';                     
						}
						else if(json.status==19){ alert('圓夢券不足 !!'); }
						else if(json.status==20){ alert('下標總金額超過市價 !!'); }
						else{ alert(json.retMsg); }
					}
				}
			});
		}
	}
}


//單次出價
function hp_bid_1(pid,paytype) {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var count = $('#'+pageid+' #count').val();	
	var obid = $('#'+pageid+' #bida').val();
	var ouserid = $('#'+pageid+' #user').val();
	var oautobid = $('#'+pageid+' #autobid').val();
	var oscode_num = $('#'+pageid+' #oscode_num').val();
	var scode_num = $('#'+pageid+' #scode_num').val();
	var oscode_needed = 0;
	var opaytype = paytype;
	
	if(obid == "") {
		alert('單次出價不能空白');
	} else if(isNaN(Number(obid))==true) {
	    alert('出價必須為數字');  
	} else if (Number(obid) < 1) {
		alert('單次出價不能小於1');
	} else if (!obid.match("^[0-9]*[1-9][0-9]*$")) {
		alert('價格最小單位為整數');
	} else {
		$.mobile.loading('show', {text: '下標處理中', textVisible: true, theme: 'b', html: ""});

		if ((ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861') && (opaytype == 'cash') && ( oautobid != 'Y')) {
			location.href=APP_DIR+'/deposit/onlinepay/?bida='+obid+'&bidb=0&productid='+pid+'&type=single&count='+count;
		} else {
			$.post(APP_DIR+"/ajax/house_product.php?t="+getNowTime(), 
			{
				type: "single",
				productid: pid,
				price: obid,
				pay_type: opaytype,
				oscode_num: oscode_num,
				scode_num:scode_num,
				use_sgift: 'N'
			}, 
			function(str_json) {
				if(str_json) {
					$.mobile.loading('hide');
					var json = JSON && JSON.parse(str_json) || $.parseJSON(str_json);
					if (json.status==1) {
						alert('殺友請先登入');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('單次出價失敗');
					} else {
						var msg = '';
						var msg2 = '';
						if (json.status=='200'){ 
							// if (json.rank) {
								if (json.rank==-1) {
									msg="很抱歉！這個出價與其他人重複了！\n請您試試其他出價或許會有好運喔！";
								} else if(json.rank==0) {
									//msg = "恭喜！您是目前的得標者！\n記得在時間到之前要多布局！";
                                    msg = "恭喜！您是目前的得標者！";
								} else if (json.rank>=1 && json.rank<=10) {
									// msg = ' , 本次下標順位為第 ' + json.rank + ' 名'; 
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的還有 "+(json.rank)+" 位。";
								} else if (json.rank>10) {
									// msg = ' , 本次下標順位超過第 12 名或與他人重覆下標';
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
								} else {
									msg = "恭喜！這個出價是「唯一」但不是「最低」\n目前是「唯一」又比您「低」的超過 10 位。";
								}
							// }
							$('#'+pageid+' #bid').val("");
							sendmsg = "單次出價 xxx 成功 !!\n "+ msg; 
							location.href=encodeURI(APP_DIR+"/sys/showMsg/?code="+json.status+"&msg="+sendmsg+"&kind=product&kid="+pid+"&obid="+obid);
						} else if(json.status==2) {
                            alert("本商品已結標 !!");
                            location.href = "/site/product/";
                        } 
						else if(json.status==3){ alert('下標間隔過短，請稍後再下'); } 
						else if(json.status==4){ alert('金額記得填!'); } 
						else if(json.status==5){ alert('金額格式錯誤'); } 
						else if(json.status==6){ alert('金額低於底價'); } 
						else if(json.status==7){ alert('已經超過連續下標時間囉!'); }
						else if(json.status==8){ alert('超過下標次數限制'); }
						else if(json.status==9){ alert('價格請由低填到高!'); }
						else if(json.status==10){ alert('金額超過商品市價'); }
						else if(json.status==11){ alert('超過可下標次數限制!'); }
						else if(json.status==12){ alert('圓夢券不足!');	}
						else if(json.status==13){ 
							alert('殺價幣不足，請充值!'); 
							if (ouserid == '1777' || ouserid == '116' || ouserid == '28' || ouserid == '1861'){
								location.href=APP_DIR+'/deposit/onlinepay/?bida='+obid+'&bidb=0&productid='+pid+'&type=single&count='+count;
							}
						}
						else if(json.status==14){ alert('圓夢券不足!'); }
						else if(json.status==15){ alert('限新 手(未得標過者)'); }
						else if(json.status==16){ alert('限新 手(限得標次數 ' + json.value +' 次以下)'); }
						else if(json.status==17){ alert('限老 手(限得標次數 ' + json.value +' 次以上)'); }
						else if(json.status==18){ 
							 alert('您還沒有《萬人殺房》的下標資格, 將跳轉至活動頁激活資格 !'); 
							 location.href='/evt/2854.php';                     
						}
						else if(json.status==19){ alert('圓夢券不足 !!'); }
						else if(json.status==20){ alert('下標總金額超過市價 !!'); }
						else{ alert(json.retMsg); }
					}
				}
			});
		}
	}
}
