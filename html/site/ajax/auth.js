// JavaScript Document
function check_sms() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#'+pageid+' #phone').val();
	var osmscode = $('#'+pageid+' #smscode').val();
	var omid=$('#'+pageid+' #mid').val();
	var oep=$('#'+pageid+' #ep').val();
	
	if(osmscode == "") {
		alert('驗證碼不能空白');
	} else  {
		$.post(APP_DIR+"/ajax/auth.php?t="+getNowTime(), 
		{type:'check_sms', phone:ophone, smscode:osmscode, mid:omid}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if(json.retCode==1){
					var msg = '';
					alert(json.retMsg);
					if (oep == 'Y'){
						location.href=APP_DIR+"/enterprise/member";
					}else{	
						location.href=APP_DIR+"/member/";
					}	
				}else if(json.retCode==-1){
					alert(json.retMsg);
					location.href=APP_DIR+"/member/userlogin/";
				}else{
					alert(json.retMsg);
				}
			}
		});
	}
}

function verify_phone() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#phone').val();
	var act=$('#act').val();
	var omid=$('#mid').val();
	var oep=$('#ep').val();
	
	var ok=true;
    if(ophone == "") {
		alert('手機號碼不能為空白 !!');
		ok=false;
	} else if(isDigit(ophone)==false) {
	   alert('手機號碼必需為數字 !'); 
	   ok=false;
	}

	if(ok==true) {
		$.post(APP_DIR+"/ajax/auth.php?t="+getNowTime(), 
		{type:act, phone:ophone, mid:omid}, 
		function(str_json) {
			if(str_json) {
				console.log(str_json);
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if(json.retCode==1){
					alert(json.retMsg);
					if (oep != 'Y'){
						location.href=APP_DIR+"/member/check_sms/";
					}else{
						location.href=APP_DIR+"/member/check_sms/?ep=Y&phone="+ophone;
					}
				}else if(json.retCode==-1){
					alert(json.retMsg);
					location.href=APP_DIR+"/member/userlogin/";
				}else{
					alert(json.retMsg);
				}
			}
		});
	} else {
	     return false;
	}
}

function isDigit(value) {
	var str=value.trim();
	if(str==''){
		return false;
	}	
	if (str.length<1)  return false;
	var ret = true;
	for(var i=0;i<str.length;++i) {
		myCharCode = str.charCodeAt(0);
		if((myCharCode > 47) && (myCharCode <  58)) {
			ret=true;
		} else {
			ret=false;
			break;			
		}
	}
	return ret;
}
