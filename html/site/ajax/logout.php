<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/dbconnect.php");	
include_once(LIB_DIR ."/ini.php");
//$app = new AppIni; 

$c = new Logout;
$c->home();

class Logout {
	public function home() {
		global $db, $config;

		$ret['status'] = 0;
		
		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_secret'] = '';
		$_SESSION['auth_email'] = '';
		
		$_SESSION = [];
		
		session_unset($_SESSION);
        session_destroy();
		
		setcookie("auth_id", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_email", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", "", time() - 3600, "/", COOKIE_DOMAIN);

		setcookie("auth_loginid", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_nickname", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_type", "", time() - 3600, "/", COOKIE_DOMAIN);		
		
		//回傳:
		$ret['status'] = 200;
		$ret['retCode'] = 1;

		if (!isset($_SESSION)) {
			exit(json_encode(array('code' => 0, 'message' => 'Logout successful')));
		} else {
			$user = $_SESSION['auth_id']; 
			if ($shit_happens)
			{
				exit(json_encode(array('code' => 1, 'message' => 'Shit happens')));
			}
			unset($_SESSION['auth_id']); 
		}

		echo json_encode($ret);
	}
	
}
?>