<?php

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");	
include_once(LIB_DIR ."/ini.php");

$c = new Device_token;
$c->home();
/**
 * 
 */
class Device_token {
	
	public function home(){

		if (empty($_POST['device_tk']) && empty($_GET['device_tk'])) {
			$ret['retCode'] = -1;
			$ret['retMsg' ]='沒有裝置token !!';

			echo json_encode($ret);
			exit;
		}

		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$router = new Router();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$auth_id = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$device_tk = empty($_POST['device_tk']) ? htmlspecialchars($_GET['device_tk']) : htmlspecialchars($_POST['device_tk']);
		$device_type = empty($_POST['device_type']) ? htmlspecialchars($_GET['device_type']) : htmlspecialchars($_POST['device_type']);

		if (empty($auth_id)) {
			$auth_id = 'null';
		}else{
			$auth_id = "'".$auth_id."'";
		}

		$query = "	INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_device_token` (
						token,
						userid,
						device_type,
						insertt
					)
					VALUES
						(
							'{$device_tk}',
							{$auth_id},
							'{$device_type}',
							now()
						) ON DUPLICATE KEY UPDATE userid = {$auth_id},
						device_type = '{$device_type}',
						modifyt = now()";

		$db->query($query);

		$ret['retCode'] = 1;
		$ret['retMsg'] = "OK";
		echo json_encode($ret);
	}

	public function enterprise(){

		if (empty($_POST['device_tk']) && empty($_GET['device_tk'])) {
			$ret['retCode'] = -1;
			$ret['retMsg' ]='沒有裝置token !!';

			echo json_encode($ret);
			exit;
		}

		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$router = new Router();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$enterpriseid = empty($_POST['enterpriseid']) ? htmlspecialchars($_GET['enterpriseid']) : htmlspecialchars($_POST['enterpriseid']);
		$device_tk = empty($_POST['device_tk']) ? htmlspecialchars($_GET['device_tk']) : htmlspecialchars($_POST['device_tk']);
		$device_type = empty($_POST['device_type']) ? htmlspecialchars($_GET['device_type']) : htmlspecialchars($_POST['device_type']);

		if (empty($auth_id)) {
			$enterpriseid = 'null';
		}else{
			$enterpriseid = "'".$enterpriseid."'";
		}

		$query = "	INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_device_token` (
						token,
						userid,
						device_type,
						insertt
					)
					VALUES
						(
							'{$device_tk}',
							{$enterpriseid},
							'{$device_type}',
							now()
						) ON DUPLICATE KEY UPDATE enterpriseid = {$enterpriseid},
						device_type = '{$device_type}',
						modifyt = now()";

		$db->query($query);

		$ret['retCode'] = 1;
		$ret['retMsg'] = "OK";
		echo json_encode($ret);
	}	
}
