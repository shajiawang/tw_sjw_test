<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
//$app = new AppIni;

$c = new Register;

foreach(json_decode($HTTP_RAW_POST_DATA,true) as $k=>$v ) {
    $_POST[$k]=$v;
}
error_log("[ajax/user_register.php] : ".json_encode($_POST));
$c->home();

class Register {
	public $str;
	public $json='';
	public $type='';

	public function home() {
		global $db, $config, $json, $type;

		$this->str = new convertString();
		$json=$_POST['json'];
		$type=$_POST['type'];


		$ret=array();
		if(empty($type)) {
			if($json=='Y') {
				echo json_encode(getRetJSONArray(-1,'EMPTY ACTION TYPE !!','MSG'));
				exit;
			}
		}

		$ret['status'] = "";
		$ret['pid'] = 0;

        $db = new mysql($config["db"]);
		$db->connect();

		$chk1 = $this->chk_nickname();
		$chk2 = $this->chk_phone();
		$chk6= $this->chk_idx6();
        $chk3 = $this->chk_passwd();
		$chk4 = $this->chk_product();

		if($_POST['type'] == 'flash') {
			if(!empty($chk4['err'])) {
				$ret['status'] = $chk4['err'];
				if($json=='Y') {
					echo json_encode(getRetJSONArray($chk4['retCode'],$chk4['retMsg'],'MSG'));
					exit;
				}
			}
		}

		if ($_POST['type'] != 'flash') {
			if(!empty($chk1['err'])) {
				$ret['status'] = $chk1['err'];
				if($json=='Y') {
					echo json_encode(getRetJSONArray($chk1['retCode'],$chk1['retMsg'],'MSG'));
					exit;
				}
			}
			if(!empty($chk3['err'])) {
				$ret['status'] = $chk3['err'];
				if($json=='Y') {
					echo json_encode(getRetJSONArray($chk3['retCode'],$chk3['retMsg'],'MSG'));
					exit;
				}
			}
		}

		if(!empty($chk2['err'])) {
			$ret['status'] = $chk2['err'];
			if($json=='Y') {
				echo json_encode(getRetJSONArray($chk2['retCode'],$chk2['retMsg'],'MSG'));
				exit;
			}
		}

		if(!empty($_POST['sso_uid']) || !empty($_POST['sso_uid2'])) {
			$auth_by=strtolower($_POST['sso_name']);
			$chk_sso=$this->chk_sso($auth_by, $_POST['sso_uid'],$_POST['sso_uid2']);
			if(!empty($chk_sso['err']) && $chk_sso['err']>0) {
				$ret['status']=$chk_sso['err'];
				if($json=='Y') {
					if($chk_sso['userid']>0 && $chk_sso['ssoid']>0) {
						$this->mk_login($chk_sso['userid']);
						$user = $_SESSION['user'];
						$arrAuth=array(
						   'auth_id'=>$_SESSION['auth_id'],
						   'auth_email'=>'',
						   'auth_secret'=>$_SESSION['auth_secret'],
						   'auth_loginid'=>$_SESSION['auth_loginid'],
						   'auth_nickname'=>$_SESSION['auth_nickname'],
						   'auth_type'=>$_SESSION['auth_type']
						);
						$ret=getRetJSONArray(1,'OK','JSON');
						$ret['retObj']=array();
						$ret['retObj']=$arrAuth;
						$retStr = json_encode($ret);
						error_log("[ajax/user_register] auth json : ".$retStr);
						echo $retStr;
					}
					exit;
				}
			}
		}

		error_log("[ajax/user_register] ret[status] : ".$ret['status']."|");
		if(empty($ret['status'])) {
			//建立會員帳號
			if($json=='Y') {
				error_log("[ajax/user_register] mk_appuser ");
				$userdata=$this->mk_appuser();
				if($userdata) {
					$ret=getRetJSONArray(1,'OK','JSON');
					$ret['retObj']=array();
					$ret['retObj']=$userdata;
				} else {
					$ret=getRetJSONArray(-999,'Reg ERROR !!','MSG');
				}
			} else {
				error_log("[ajax/user_register] mk_user ");
				$this->mk_user();
				if ($_POST['type'] == 'flash') {
					$ret['pid'] = $_POST['productid'];
				}
				$ret['sessid'] = session_id();
				$ret['status'] = 200;
				$ret['retCode']=1;
			}
		}
		error_log("[ajax/user_register] home : ".json_encode($ret));
	    echo json_encode($ret);
	}

	// 檢測SSO
	public function chk_sso($auth_by, $uid, $uid2='') {
		global $db, $config, $json, $type;
	    $r['err'] = '';

        $query= "SELECT r.userid,s.ssoid FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso` s
                        JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` r
                          ON  r.ssoid=s.ssoid
		         WHERE s.prefixid='{$config['default_prefix_id']}'
    			   AND s.switch='Y'
                   AND r.switch='Y'
                   AND r.prefixid='{$config['default_prefix_id']}'
                   AND s.name='{$auth_by}' ";
        if(!empty($uid) && !empty($uid2)) {
           $query.=" AND (s.uid='{$uid}' OR s.uid2='{$uid2}' ) ";
        } else if(empty($uid2)) {
            $query.=" AND (s.uid='{$uid}' OR s.uid2='{$uid}' ) ";
        }
        $table = $db->getQueryRecord($query);
        if(!empty($table['table']['record'])) {
           if($table['table']['record'][0]['userid']>0 && $table['table']['record'][0]['ssoid']>0) {
               error_log("[ajax/user_register] uid : ".$uid."( uid2:".$uid2.") is existed and will not create sso mapping !!");
			   $r['err']=16;
               $r['retCode']=-16;
               $r['ssoid']=$table['table']['record'][0]['ssoid'];
               $r['userid']=$table['table']['record'][0]['userid'];
               $r['retMsg']="此帳號已綁定過 !";
           }
        }

		error_log("[chk_sso]:".json_encode($r));
		return $r;
	}

	//暱稱检查
	public function chk_nickname() {
		global $db, $config, $json, $type;

		$r['err'] = '';
		$_POST['nickname'] = addslashes($_POST['nickname']);

        $nickname = $_POST['nickname'];
		if (empty($nickname)) {
			//'請填寫暱稱'
			$r['err'] = 2;
			$r['retCode']=-2;
			$r['retMsg']='請填寫暱稱 !!';
		} else {
            // 檢察特殊符號
            if(emoji_exists($nickname)) {
               $nickname = emoji_remove($nickname);
			}
			// 暱稱存在就在後方加 "__序號" 不彈回了
			$query = " SELECT count(*) as cnt
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND (`nickname` = '{$nickname}' OR `nickname` like '{$nickname}\_\_%')
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record']) && $table['table']['record'][0]['cnt']>0) {
			    $_POST['nickname'] = $nickname."__".($table['table']['record'][0]['cnt']);
			} else {
                $_POST['nickname'] = $nickname;
            }
		}
        error_log("[chk_nickname] nickname : ".$nickname);
		error_log("[chk_nickname] : ".json_encode($r));
		return $r;
	}

	//checkidx
	public function chk_idx6(){
		global $db, $config, $json, $type;

		$r['err'] = '';
        $idsix = $_POST['idsix'];
		if (empty($idsix)) {
			//'請填寫暱稱'
			$r['err'] = 5;
			$r['retCode']=-5;
			$r['retMsg']='請填寫身份証/末六碼數字!!';
		} else {
            // 檢察特殊符號
            if(emoji_exists($idsix)) {
               $idsix = emoji_remove($idsix);
			}
			if ((is_numeric($idsix))&&str_len($idsix)!=6){
			}else{
				//'請填寫暱稱'
				$r['err'] = 6;
				$r['retCode']=-6;
				$r['retMsg']='請填寫身份証/末六碼數字!!';
			}
		}
        error_log("[chk_idx6] chk_idx6 : ".$idsix);
		error_log("[chk_idx6] : ".json_encode($r));
		return $r;
	}
	//手機號碼检查
	public function chk_phone() {
		global $db, $config, $json, $type;

		$r['err'] = '';

		if($type=='sso') {
			$ts=str_replace(".","",microtime(true));
			$_POST['phone'] = $_POST['sso_name']."_".$ts;
			switch($_POST['sso_name']) {
				case 'line'  :  $_POST['phone'] = 'ln'.$ts; break;
				case 'fb'    :  $_POST['phone'] = 'fb'.$ts; break;
				case 'weixin':  $_POST['phone'] = 'wx'.$ts; break;
			}
			return $r;
		}

		// 如有sso_name就不檢查了 ??
		if(!empty($_POST['sso_name'])) {
		   // return $r['err'];
		   return $r;
		}

		if (empty($_POST['phone'])) {
			//'請填寫手機號碼'
			$r['err'] = 4;
			$r['retCode']=-4;
			$r['retMsg']="請填寫手機號碼 !!";
		} else if (!is_numeric($_POST['phone'])) {
			//'手機號碼格式不正確'
			$r['err'] = 5;
			$r['retCode']=-5;
			$r['retMsg']="手機號碼格式不正確 !";
		} else if(strpos($_POST['phone'],".")!=false || strpos($_POST['phone'],"+")!=false ||  strpos($_POST['phone'],"-")!=false) {
		    $r['err'] = 5;
			$r['retCode']=-5;
			$r['retMsg']="手機號碼格式不正確 !!";
		} else {
			$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
			           WHERE `prefixid` = '{$config['default_prefix_id']}'
					     AND `name` = '{$_POST['phone']}'
						 AND `user_type` = 'P' ";
			$table = $db->getQueryRecord($query);

			if (!empty($table['table']['record'])) {
				//'此手機號碼已存在'
				$r['err'] = 6;
				$r['retCode']=-6;
				$r['retMsg']="此手機號碼已存在 !!";
			}
		}
		error_log("[chk_phone]:".json_encode($r));
		return $r;
	}

	//检查密碼
	public function chk_passwd() {
		global $db, $config, $json, $type;

		$r['err'] = '';
		if($type=='sso') {
		   // sso註冊就不檢查密碼
		   return $r;
		}

		// 如有sso_name就不檢查密碼
		if(!empty($_POST['sso_name'])) {
		   return $r;
		}

		if (empty($_POST['passwd'])) {
			//'請填寫密碼'
			$r['err'] = 7;
			$r['retCode']=-7;
			$r['retMsg']="請填寫密碼 !!";
		} elseif (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd'])) {
			//'密碼格式不正確'
			$r['err'] = 8;
			$r['retCode']=-8;
			$r['retMsg']="密碼格式不正確 !!";
		} elseif (empty($_POST['repasswd']) ) {
			//'請填寫確認密碼'
			$r['err'] = 9;
			$r['retCode']=-9;
			$r['retMsg']="請填寫確認密碼 !!";
		} elseif ($_POST['passwd'] != $_POST['repasswd']) {
			//'密碼確認不正確'
			$r['err'] = 10;
			$r['retCode']=-10;
			$r['retMsg']="密碼確認不正確 !!";
		} elseif (!empty($_POST['expasswd']) && !preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['expasswd'])) {
			//'兌換密碼格式不正確'
			$r['err'] = 11;
			$r['retCode']=-11;
			$r['retMsg']="兌換密碼格式不正確 !!";
		} elseif (!empty($_POST['expasswd']) && empty($_POST['exrepasswd'])) {
			//'請填寫兌換密碼確認'
			$r['err'] = 12;
			$r['retCode']=-12;
			$r['retMsg']="請填寫確認兌換密碼 !!";
		} elseif ($_POST['expasswd'] != $_POST['exrepasswd']) {
			//'兌換密碼確認不正確'
			$r['err'] = 13;
			$r['retCode']=-13;
			$r['retMsg']="兌換密碼確認不正確 !!";
		}
        error_log("[chk_passwd]:".json_encode($r));
		return $r;
	}

	//檢查閃殺活動
	public function chk_product() {
		global $db, $config, $json, $type;

		$r['err'] = '';
		$productid = trim($_POST['productid']);

		if(empty($productid)) {
			//無此闪殺活動或一般註冊
			$r['err'] = "";
			$r['retCode']=1;
			$r['retMsg']="OK";
		} else {
			$query = "SELECT *
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND `productid` = '{$_POST['productid']}'
				AND `is_flash` = 'Y'
				AND `display` = 'Y'
				AND `closed` = 'N'
				and now() between `ontime` and `offtime`
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);

			if (empty($table['table']['record'])) {
				//闪殺活動已结束
				$r['err'] = 15;
				$r['retCode']=-15;
			    $r['retMsg']="此闪殺活動已结束 !!";
			}
		}
		error_log("[chk_product]:".json_encode($r));
		return $r;
	}

	//建立會員帳號
	public function mk_user() {
		global $db, $config, $json, $type;

		//自動產生密碼
		$shuffle = get_shuffle();
		$pwcode = substr($shuffle, 0, 6);
		$reg_type=$_POST['type'];
        $nickname = $_POST['nickname'];

		if($reg_type== 'flash') {
			$passwd = $this->str->strEncode($pwcode, $config['encode_key']);
		} else {
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);
		}
		$exchangepasswd = $this->str->strEncode(substr($_POST['phone'], -6), $config['encode_key']);

		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`name`='{$_POST['phone']}',
			`passwd`='{$passwd}',
			`exchangepasswd`='{$exchangepasswd}',
			`email`='',
			`user_type`='P',
			`insertt`=NOW()
		";

		$ret=$db->query($query);
		$userid = $db->_con->insert_id;
        error_log("[ajax/user_register] ".$query."-->".$userid);

		// initial default
		$countryid = $config['country'];
		$regionid = $config['region'];
		$provinceid = $config['province'];
		$channelid = $config['channel'];

		//
		$country=$_REQUEST['country'];

		if(!empty($country)) {
		   $encoding=mb_detect_encoding($country);
		   $country=mb_convert_encoding($country,"UTF-8",$encoding);
		   error_log("[ajax/mk_user] country:".$country);
		   if($country=='中国台湾') {
		      $countryid='1';
              $regionid='0';
		   } else {
			   $countryData=getCountryData(array("name"=>$country));
			   if(!$countryData) {
				  $countryData=getCountryData(array("isocountrycode"=>$country));
			   }
			   if($countryData)
				  $countryid=$countryData['table']['record'][0]['countryid'];
		   }
		}
		error_log("[ajax/mk_user] countryid:".$countryid);

		$province=$_REQUEST['province'];
		if(!empty($province)) {
			$encoding=mb_detect_encoding($province);
			$province=mb_convert_encoding($province,"UTF-8",$encoding);
			error_log("[ajax/mk_user] province:".$province);
			$provinceData=getProvinceData(array("name"=>$province));

			if($provinceData){
				$provinceid=$provinceData['table']['record'][0]['provinceid'];
			}
		}
		error_log("[ajax/mk_user] provinceid:".$provinceid);

		$city=$_REQUEST['city'];
		if(!empty($city)) {
			$encoding = mb_detect_encoding($city);
			$city = mb_convert_encoding($city,"UTF-8",$encoding);
			error_log("[ajax/mk_user] city:".$city);
			$channelData=getChannelData(array("name"=>$city));
			if($channelData) {
				$channelid = $channelData['table']['record'][0]['channelid'];
			}
		}
		error_log("[ajax/mk_user] channelid:".$channelid);

		$language=$_REQUEST['language'];
		if($language=='zh_CN') {
		   // Nothing to do so far....
		}
		$gender="male";
        $sex=$_REQUEST['gender'];
        switch($sex) {
            case '1' :
			case 'male':
				$gender="male";
			    break;
			case '0':
			case 'female':
				$gender="female";
			    break;
        }

		if($reg_type=='flash') {
		    if(empty($_POST['nickname']))
			   $nickname = '_guest_'.$userid;
			   $src_from = 'FLASH';
		} else if($reg_type=='sso') {
			$src_from = strtoupper($_POST['sso_name']);
		}
		$rep_patten[]="'";
		$rep_patten[]="'";
		$nickname=str_replace($rep_patten,"",$nickname);
		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`nickname`='{$nickname}',
			`gender`='{$gender}',
			`countryid`='{$countryid}',
			`regionid`='{$regionid}',
			`provinceid`='{$provinceid}',
			`channelid`='{$channelid}',
			`thumbnail_url`='{$_POST['thumbnail_url']}',
			`area`='',
			`address`='',
			`addressee`='',
			`phone`='{$_POST['phone']}',
			`src_from` = '{$src_from}|{$productid}',
			`insertt`=NOW()
		";
		error_log("[ajax/user_register]:".$query);
		$ret=$db->query($query);
        error_log("[ajax/user_register]:".$ret);

		//新增SMS check code
		$shuffle = get_shuffle();
		$checkcode = substr($shuffle, 0, 6);

		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`code`='{$checkcode}',
			`phone`='{$_POST['phone']}',
			`verified`='N',
			`user_type`='P',
			`insertt`=NOW()
		";
        error_log("[ajax/user_register]:".$query);
		$ret=$db->query($query);
        error_log("[ajax/user_register]:".$ret);

		// 建立會員銷帳帳號
		$this->mk_extrainfo($userid);

		$query = "SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE  prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$userid}'
				AND switch = 'Y'
			LIMIT 1
		";
		$table = $db->getQueryRecord($query);
     	$user = $table['table']['record'][0];

		if($user['nickname'] == '') {
			// 修改 saja_user_profile
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				SET	`nickname`='_{$_POST['phone']}',
				WHERE 1=1 AND `userid`='{$userid}'
			";
			$db->query($query);
		}

		if($reg_type=='flash') {
			//閃殺活動
			$this->spoint_by_flash($userid);

			$user_phone_0 = substr($_POST['phone'], 0, 1);
			$user_phone_1 = substr($_POST['phone'], 1, 1);

			if($user_phone_0 == '0' && $user_phone_1 == '9') {
				$area = 1; // TW
			} else if($user_phone_0 == '1') {
				$area = 2; // CN
			} else {
				//'手機號碼只提供台湾及大陆驗證'
				$r['err'] = 5;
				$r['retCode']=-16;
			    $r['retMsg']="手機號碼仅限台湾及中国大陆地区 !!";
			}
			//傳簡訊
			if($area==1) {
			   $this->mk_sms($userid, $_POST['phone'], $pwcode, $area);
            }
		}

		if(!empty($_POST['sso_uid']) && !empty($userid)) {
			$arr_sso=$_SESSION['sso']['sso_data'];

			//建立會員SSO帳號
			$this->mk_sso($userid,$_POST['sso_uid'],$_POST['sso_uid2'],$_POST['sso_name'],json_encode($arr_sso));
		}

		//註冊送殺價券活動
		$this->give_oscode_by_promote($userid);

		// 設成已登入
		$this->mk_login($userid);

		// 記錄連線來源
		// 抓來源IP
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
			$ip = $temp_ip[0];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		// 抓User Agent
		$memo=$_POST['client'];
		if(!empty($memo)) {
		   $user_agent='APP';
		} else {
		   $memo=$_SERVER['HTTP_USER_AGENT'];
		   if(strpos($memo, 'MicroMessenger')>0) {
			  $user_agent='WEIXIN';
		   } else {
			  $user_agent='BROWSER';
		   }
		}

		$productid=$_REQUEST['productid'];
		$act='REG';
		$goto='';
		$intro_by='';
		$encoded_user_src=trim($_REQUEST['user_src']);
		error_log("[ajax/user_register] encoded user_src : ".$encoded_user_src);
		if(!empty($encoded_user_src)) {
			// 拉人進來的會員
    		$this->str = new convertString();
			if(!is_numeric($encoded_user_src)) {
				$encoded_user_src=str_replace(" ","+",$encoded_user_src);
				$intro = $this->str->decryptAES128($config['encode_key'], base64_decode($encoded_user_src));
				$arr = explode("&&", $intro);
				if(is_array($arr)) {
					$user_src = $arr[0];
				}
			} else {
				$user_src = $encoded_user_src;
			}
			error_log("[ajax/user_register] decoded user_src : ".$user_src);

			$intro_by=$user_src;

		}
		// 記錄連線及註冊來源
		$this->logAffiliate($ip, $intro_by, $act, $productid, $userid, $goto, $memo, $user_agent );
		error_log("[ajax/user_register] mk_user : {type:".$reg_type.",userid:".$userid.",user_src:".$intro_by.",pwd:".$_POST['passwd'].",expwd:".substr($_POST['phone'], -6)."}");
	}

	//建立會員帳號(for  APP)
	public function mk_appuser() {
		global $db, $config;

        $phone=$_POST['phone'];
		$passwd = $_POST['passwd'];
		$sso_uid=$_POST['sso_uid'];
		$sso_uid2=$_POST['sso_uid2'];
		$sso_name=strtolower($_POST['sso_name']);
		$nickname = $_POST['nickname'];
		$idsix = $_POST['idsix'];

        $user_src = $_POST['user_src'];

		if(!empty($passwd)) {
		    $passwd=$this->str->strEncode($passwd, $config['encode_key']);
		} else {
			if(empty($sso_name)){
				$passwd=$this->str->strEncode(substr($phone, -6), $config['encode_key']);
			}else{
				$passwd=$this->str->strEncode('123456', $config['encode_key']);
			}
        }

		$expasswd = $_POST['expasswd'];
		if(!empty($expasswd)){
		   $expasswd=$this->str->strEncode($expasswd, $config['encode_key']);
		} else {
			if (empty($sso_name)){
				$expasswd=$this->str->strEncode(substr($phone, -6), $config['encode_key']);
			}else{
				$expasswd=$this->str->strEncode('123456', $config['encode_key']);
			}
		}

		$headimgurl=$_POST['headimgurl'];
		$gender='';
		switch($_POST['gender']) {
		    case 1 : $gender='male';
			         break;
			case 2 : $gender='female';
			         break;
			default: $gender='male';
		}

		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`name`='{$phone}',
			`passwd`='{$passwd}',
			`exchangepasswd`='{$expasswd}',
			`email`='',
			`user_type`='P',
			`insertt`=NOW()
		";

		$db->query($query);
		$userid = $db->_con->insert_id;
        $countryid = $config['country'];
		$regionid = $config['region'];
		$provinceid = $config['province'];
		$channelid = $config['channel'];
		// 判斷帳號是否為數字
		if(is_numeric($phone)) {
			error_log("[ajax/user_register] 1 phone:".$phone." , sso_name:".$sso_name);
			//取得該電話所在區域的相關資料
			$phonedata = getPhoneAddress($phone);
		} else {
		    error_log("[ajax/user_register] 2 phone:".$phone." , sso_name:".$sso_name);
			switch($sso_name) {
			    case "weixin":
					$countryid = $config['country'];
					$regionid = $config['region'];
					$provinceid = $config['province'];
					$channelid = $config['channel'];

					$country=$_POST['country'];
					if(!empty($country)) {
						$encoding=mb_detect_encoding($country);
						$country=strtoupper(mb_convert_encoding($country,"UTF-8",$encoding));
						error_log("[ajax/mk_appuser] country:".$country);
						if($country=='中国台湾' || $country=='TW') {
							$countryid='1';
							$regionid='0';
						} else {
							$countryData=getCountryData(array("name"=>$country));
							if(!$countryData) {
								$countryData=$this->getCountryData(array("isocountrycode"=>$country));
							}
							if($countryData) {
								$countryid=$countryData['table']['record'][0]['countryid'];
							}
						}
					}

					$province = $_POST['province'];
					if(!empty($province)) {
						$encoding=mb_detect_encoding($province);
						$province=strtoupper(mb_convert_encoding($province,"UTF-8",$encoding));
						error_log("[ajax/mk_appuser] province:".$province);
						$provinceData=getProvinceData(array("name"=>$province));
						if($provinceData){
							$provinceid=$provinceData['table']['record'][0]['provinceid'];
						}
					}

					$city=$_POST['city'];;
					if(!empty($city)) {
						$encoding=mb_detect_encoding($city);
						$city=strtoupper(mb_convert_encoding($city,"UTF-8",$encoding));
						error_log("[ajax/mk_appuser] city:".$city);
						$channelData=getChannelData(array("name"=>$city));
						if($channelData) {
							$channelid=$channelData['table']['record'][0]['channelid'];
						}
					}
					$language=strtoupper($_POST['language']);
					if($language=='ZH_CN') {
						// Nothing to do so far....
					}

					$gender="male";
					$sex = $_REQUEST['gender'];
					switch($sex) {
						case '1' :
							$gender="male";
							break;
						case 'male':
							$gender="male";
							break;
						case '0':
							$gender="female";
							break;
						case 'female':
							$gender="female";
							break;
						default:
							$gender="female";
							break;
					}
					break;
                case "line": break;
				case "qq": break;
				case "weibo": break;
				default: break;
			}
		}
        error_log("[ajax/user_register/mk_appuser] provinceid:".$provinceid." ,countryid:".$countryid.", channelid:".$channelid);

		$rep_patten[]="'";
		$rep_patten[]="'";
		$nickname=str_replace($rep_patten,"",$nickname);
		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		SET	`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`nickname`='{$nickname}',
			`countryid`='{$countryid}',
			`regionid`='{$regionid}',
			`provinceid`='{$provinceid}',
			`channelid`='{$channelid}',
			`thumbnail_url`='{$headimgurl}',
			`idsix`='{$idsix}',
			`gender`='{$gender}',
			`phone`='{$phone}',
			`insertt`=NOW()
		";
		$ret = $db->query($query);

		// 修改 saja_user_sms_auth
		$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
		SET	`userid`='{$userid}',
			`verified`='Y',
			`modifyt`=NOW()
		    WHERE 1=1 AND `phone`='{$phone}' AND `user_type`='P'
		";
		$ret = $db->query($query);

		$query = "SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE  prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$userid}'
				AND switch = 'Y'
			LIMIT 1
		";
		$table = $db->getQueryRecord($query);
     	$user = $table['table']['record'][0];

		// 建立會員銷帳帳號
		$this->mk_extrainfo($userid);

		if($user['nickname'] == '') {
			// 修改 saja_user_profile
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				SET	`nickname`='_{$phone}',
				WHERE 1=1 AND `userid`='{$userid}'
			";
			$ret = $db->query($query);
		}
		error_log("821 -------");
		error_log($_POST['sso_data']);
		error_log("822 -------");
        if(!empty($userid)) {
			//建立會員SSO帳號
			if(!empty($sso_uid) || !empty($sso_uid2)) {
			   $sso_data=(strpos($_POST['sso_data'],'}')>0)?$_POST['sso_data']:urldecode($_POST['sso_data']);
			   $this->mk_sso($userid, $sso_uid, $sso_uid2, $sso_name,$sso_data);
			}
		}

		error_log("[ajax/user_register] user_src : ".$user_src);
		if(!empty($user_src)) {
			// 拉人進來的會員
    		if (!is_numeric($user_src)) {
				$this->str = new convertString();
                $encoded_user_src=str_replace(" ","+",$user_src);
				$temp = $this->str->decryptAES128($config['encode_key'], base64_decode($user_src));
				$arr = explode("&&", $temp);
				if(is_array($arr)) {
					$user_src = $arr[0];
				}
			}
			error_log("[ajax/user_register] decoded user_src : ".$user_src);
		}
        // 記錄連線來源
		// 抓來源IP
		if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
			$ip = $temp_ip[0];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		// 抓User Agent
		$memo=$_POST['client'];
		if(!empty($memo)) {
			$user_agent='APP';
		} else {
			$memo=$_SERVER['HTTP_USER_AGENT'];
			if(strpos($memo, 'MicroMessenger')>0) {
				$user_agent = 'WEIXIN';
			} else {
				$user_agent = 'BROWSER';
			}
		}
		$productid = $_POST['productid'];
		$act='REG';
		$goto='';
		// 記錄連線及註冊來源
		$this->logAffiliate($ip, $user_src, $act, $productid, $userid, $goto, $memo, $user_agent );

		//註冊送殺價券活動
		$this->give_oscode_by_promote($userid);

        // 設定為已登入
        $this->mk_login($userid);

		$query = "SELECT u.*, up.nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
			JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up
			  ON   u.userid=up.userid
			WHERE  u.prefixid = '{$config['default_prefix_id']}'
				AND u.userid = '{$userid}'
				AND u.switch = 'Y'
			LIMIT 1
		";
		$table = $db->getQueryRecord($query);
     	$user = $table['table']['record'][0];
        $user['nickname'] = urldecode($user['nickname']);

		// 回傳JSON資料
		$r = array();
		$r['auth_id'] = $userid;
		$r['auth_email'] = '';
		$r['auth_secret'] = md5($userid . $user['name']);
		$r['auth_loginid'] = $user['name'];
		$r['auth_nickname'] = $user['nickname'];
		error_log("[ajax/user_register] mk_appuser : ".json_encode($r));
        return $r;
	}

	// 記錄連線及註冊來源
	public function logAffiliate($ip, $intro_by, $act, $productid, $userid, $goto, $memo, $user_agent ) {
		global $db, $config, $json, $type,$usermodel;

		if($act=='PROD|VIEW') {
			error_log(">userid : ".$userid." ".$act." productid :".$productid);
			return 1 ;
		}
		if(empty($productid)){
			$productid = 0;
		}
		//CHECK IF EXIST
		if ($usermodel->get_intro_by_id($userid)<0){
			$insert = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` SET
						`come_from`='{$ip}',
						`intro_by`='{$intro_by}',
						`act`='{$act}',
						`productid`='{$productid}',
						`userid`='{$userid}',
						`goto`='{$goto}',
						`memo`='{$memo}',
						`user_agent`='{$user_agent}',
						`insertt`=NOW(),
						`modifyt`=NOW() ";
			$db->query($insert);

			$suaid = $db->_con->insert_id;
			error_log("[ajax/user_register] ".$insert."-->".$suaid);

			//推薦送活動
			if($act=='REG' && (!empty($intro_by) || $intro_by !==0)) {
				$activity_type = 8; //活動代碼

				$update = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
							SET promoteid='{$activity_type}'
							WHERE `userid`='{$userid}'
							AND `suaid`='{$suaid}'";
				$ret=$db->query($update);
				error_log("[ajax/logAffiliate] update : ".$update."-->".$ret);
			}
		}
	}

	//註冊送殺價券活動
	public function give_oscode_by_promote($userid) {
    	global $db, $config, $json, $type;

		$query = "SELECT *
		FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		WHERE 	`prefixid` = '{$config['default_prefix_id']}'
			AND `switch`='Y'
			AND `behav`='b'
			AND NOW() between `ontime` and `offtime`
			AND `productid` > 0 ";

		$table = $db->getQueryRecord($query);
		$total = count($table['table']['record']);

		error_log("[give_oscode_by_promoite] ".$query."-->".$total);

		if($total > 0) {
			for($i=0;$i<$total;++$i) {
				$spid = $table['table']['record'][$i]['spid'];
				$onum = $table['table']['record'][$i]['onum'];
				$prodid=$table['table']['record'][$i]['productid'];
				if($onum>0) {
					for($j=0;$j<$onum;++$j) {
						$insert = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		                SET prefixid = '{$config['default_prefix_id']}',
                            userid = '{$userid}',
							spid = '{$spid}',
							productid = '{$prodid}',
							behav = 'user_register',
							amount = 1,
							insertt = NOW()";
						if($j==0) {
							error_log($insert);
						}
						$db->query($insert);
					}
					if($spid>0) {
						$update = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
						              SET amount=amount+1 ,scode_sum=scode_sum+".$onum."
						            WHERE spid='".$spid."'";
						$ret=$db->query($update);
						error_log("[oscode_by_promoite] update : ".$update."-->".$ret);
					}
				}
            }

        } else {
			error_log("[give_oscode_by_promoite] No oscode by promote to give..");
		}
    }

	//建立閃殺活動
	public function spoint_by_flash($userid) {
		global $db, $config, $json, $type;

		$query = "SELECT userid, by_flash
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND userid = '{$userid}'
			AND user_type = 'P'
			AND switch = 'Y'
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);

		if(isset($table['table']['record'][0]) && $table['table']['record'][0]['by_flash'] !=='Y') {
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			SET
			   `by_flash`='Y'
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$userid}'
				AND user_type = 'P'
				AND switch = 'Y'
			";
			$db->query($query);
		}
	}

	// 建立會員SSO帳號
	// POST 傳入四參數: userid, uid, uid2, sso_name
	// POST 傳入5參數: userid, uid, uid2, sso_name,sso_data
	public function mk_sso($auth_id='', $uid='', $uid2='', $sname='', $auth_data='') {
		global $db, $config;

		$userid=$_POST['userid'];
		if(!empty($auth_id)) {
		   $userid=$auth_id;
		}

		$sso_name = $_POST['sso_name']; //$_SESSION['sso']['provider'];
		if(!empty($sname)) {
		   $sso_name = $sname;
		}

		$sso_uid = $_POST['sso_uid']; //$_SESSION['sso']['identifier'];
     	if(!empty($uid)) {
           $sso_uid=$uid;
        }

		$sso_uid2 = $_POST['sso_uid2'];
        if(!empty($uid2)) {
           $sso_uid2=$uid2;
        }

        if(empty($sso_data)) {
		   $sso_data=$auth_data;
		}

		$sso_data=addslashes($sso_data);
        error_log("[ajax/user_register/mk_appuser] mk_sso => userid:".$userid." ,sso_uid:".$sso_uid.", sso_uid2:".$sso_uid2.", sso_name:".$sso_name.", sso_data:".$sso_data);

		//加入SSO資料
		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
		SET	`prefixid`='{$config['default_prefix_id']}',
			`name`='{$sso_name}',
			`uid`='{$sso_uid}',
			`uid2`='{$sso_uid2}',
			`sso_data`='{$sso_data}',
            `seq`=0,
			`insertt`=NOW()
		";
		$db->query($query);
		error_log("[ajax/user_register] mk_sso : ".$query);

		$ssoid = $db->_con->insert_id;
        error_log("[ajax/user_register] mk_sso : ".$query."-->".$ssoid);

		$query = "
		INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt`
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$userid}',
			`ssoid`='{$ssoid}',
			`insertt`=NOW()
		";
		$db->query($query);
		$sso_rtid = $db->_con->insert_id;
		error_log("[ajax/user_register] mk_sso_rt : ".$query."-->".$sso_rtid);
	}

	// 設成已登入
	public function mk_login($userid) {
		global $db, $config, $usermodel, $json, $type;

		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';
		$_SESSION['auth_loginid'] = '';
		$_SESSION['auth_nickname'] = '';

		$query = "SELECT u.*
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
		WHERE
			u.prefixid = '{$config['default_prefix_id']}'
			AND u.userid = '{$userid}'
			AND u.switch = 'Y'
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);

		$user = $table['table']['record'][0];

		$headimgurl = $_POST['headimgurl'];
		if (!empty($headimgurl)){
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			SET	`thumbnail_url`='{$headimgurl}',
				`modifyt`=NOW()
			WHERE
				`userid` = '{$userid}'
				AND `switch` =  'Y'
			";
			$ret = $db->query($query);
		}

		$query = "SELECT *
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND `userid` = '{$userid}'
			AND `switch` =  'Y'
		";
		$table = $db->getQueryRecord($query);

		unset($user['passwd']);
		$user['profile'] = $table['table']['record'][0];
		$_SESSION['user'] = $user;

		// Set session and cookie information.
		$_SESSION['auth_id'] = $userid;
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = md5($userid . $user['name']);
		$_SESSION['auth_loginid'] = $user['name'];
		$_SESSION['auth_nickname'] = $user['profile']['nickname'];
		$_SESSION['auth_type'] = $user['user_type'];

		setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", md5($userid . $user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_loginid", $user['name'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_nickname", $user['profile']['nickname'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_type", $user['user_type'], time()+60*60*24*30, "/", COOKIE_DOMAIN);

		// Set local publiciables with the user's info.
		$usermodel->userid = $userid;
		$usermodel->name = $user['profile']['nickname'];
		$usermodel->email = '';
		$usermodel->ok = true;
		$usermodel->is_logged = true;
	}

	//SMS驗證碼
	public function mk_sms($uid, $phone, $code, $area, $message='')	{
		global $db, $config, $json, $type;

		if($area == 1) {
			//簡訊王 SMS-API
			if(empty($message))
			   $message = "殺價王密碼: ".$code.",兌換密碼：".substr($phone, -6);
			$sMessage = urlencode(mb_convert_encoding($message, 'BIG5', 'UTF-8'));
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;

			if(!$getfile=file($to_url)) {
				error_log('ERROR: SMS-API 無法连接 !', $this->config->default_main ."/user/register");
				return false;
			} else {
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				if($kmsgid < 0) {
					error_log('手機號碼错误!!', $this->config->default_main ."/user/register");
					return false;
				} else {
					return true;
				}
			}
		} else if ($area == 2) {
			//中國短信網
			$url='http://dxhttp.c123.cn/tx/';
			if(empty($message))
			   $message=$code.'（手機密碼）'.substr($phone,-6).'（兌換密碼）';
			$data = array
			(
				'uid' => '501091960002',
				'pwd' => strtolower(md5('sjw25089889')),
				'mobile' => $phone,
				'content' => ($sMessage),
				'encode' => 'utf8'
			);
			$xml = $this->postSMS($url,$data);
			if (trim($xml) == 100) {
			    return true;
		    } else {
				return false;
			}
		}
	}

	//查詢電話的地域相關資料
	public function phonedata($province, $city) {
		global $db, $config, $json, $type;

		$query = "SELECT pr.name, pr.ename, pr.provinceid, pr.regionid, re.countryid
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` pr
		left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}region` re
				on pr.prefixid = re.prefixid
				and pr.regionid = re.regionid
				and pr.switch = 'Y'
		WHERE
			pr.prefixid = '{$config['default_prefix_id']}'
			AND pr.switch = 'Y'
			AND (pr.name = '{$province}' OR pr.ename = '{$province}')
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table)) {
		    $ret['province_name']=$table['table']['record'][0]['name'];
			$ret['province_ename']=$table['table']['record'][0]['ename'];
			$ret['provinceid'] = $table['table']['record'][0]['provinceid'];
			$ret['regionid'] = $table['table']['record'][0]['regionid'];
			$ret['countryid'] = $table['table']['record'][0]['countryid'];
		} else {
			$ret['countryid'] = $config['country'];
			$ret['regionid'] = $config['region'];
			$ret['provinceid'] = $config['province'];
			$ret['channelid'] = $config['channel'];

			return $ret;
			exit;
		}

		$query = "SELECT *
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND provinceid = '{$ret['provinceid']}'
			AND ( name='{$city}' OR ename='{$city}' )
			AND switch = 'Y'
		";
		$table2 = $db->getQueryRecord($query);

		if(empty($table2['table']['record'][0]['channelid'])) {
			$query = "
			INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel`
			SET	`prefixid`='{$config['default_prefix_id']}',
				`countryid`='{$ret['countryid']}',
				`provinceid`='{$ret['provinceid']}',
				`name`='{$city}',
				`description`='{$province}全区',
				`seq`=0,
				`switch`='Y',
				`insertt`=NOW()
			";

			$db->query($query);
			$channelid = $db->_con->insert_id;
		} else {
			$ret['channelid'] = $table2['table']['record'][0]['channelid'];
		}
		return $ret;
	}

	//建立會員銷帳帳號
	public function mk_extrainfo($userid) {
		global $db, $config;

		$usernum = str_pad($userid,10,'0',STR_PAD_LEFT);
		$id = $config['firstbank_companyid'].$config['firstbank_kindnum'].$usernum;

		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo`
		SET
			`prefixid` = '{$config['default_prefix_id']}'
			, `userid` = '{$userid}'
			, `uecid` = '8'
			, `field1name` = '{$id}'
			, `seq` = '0'
			, `switch` = 'Y'
			, `insertt` = NOW()
		";
		$ue = $db->query($query);
		return $ue;
	}

}
?>