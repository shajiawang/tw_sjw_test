<?php
session_start();

$time_start = microtime(true);

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
echo "php 開始 $time 秒<br>";

if(empty($_SESSION['auth_id']) ) { //'請先登入會員帳號'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/ini.php");
	//$app = new AppIni; 

	$c = new ProductBid;
	$c->time_start = $time_start;
	$c->saja();
}


class ProductBid 
{
	public $userid = '';
	public $countryid = 2;//國家ID
	public $currency_unit = 0.01;//貨幣最小單位
	public $last_rank = 12;//預設順位提示（順位排名最後一位）
	public $saja_time_limit = 3;//每次下標時間間隔限制
	public $display_rank_time_limit = 1;//（秒）下標後離結標還有n秒才顯示順位
	public $range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標
	public $tickets = 1;
	public $price;
	
	//下標
	public function saja()
	{	
		$time_start = $this->time_start;
				
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = $this->time_start;
		
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
		$ret['status'] = 0;
		$ret['winner'] = '';
		$ret['rank'] = '';
		
		//讀取商品資料
		$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.productid = '{$_POST['productid']}'
			AND p.switch = 'Y'
		LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		unset($table['table']['record'][0]['description']);
		$product = $table['table']['record'][0];
		
		if( ($product['offtime'] == 0 && $product['locked'] == 'Y') || ($product['offtime'] > 0 && $product['now'] > $product['offtime']) ) {
			//回傳: 本商品已结標
			$ret['status'] = 2;
		}
		/*
		elseif($this->chk_time_limit() ) {
			//回傳: 下標时间间隔過短，請稍后再下標
			$ret['status'] = 3;
		}*/
		
		if($_POST['type']=='single') { //單次下標
			$chk = $this->chk_single($product);
			if($chk['err']) {
				$ret['status'] = $chk['err'];
			}
		}
		elseif($_POST['type']=='range') { //連續下標
			$chk = $this->chk_range($product);
			if($chk['err']) {
				$ret['status'] = $chk['err'];
			}
		}
		
		if(empty($ret['status']) ) {
			
			//產生下標歷史記錄
			$mk = $this->mk_history($product);
			
			//回傳: 下標完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			$ret['winner'] = $mk['winner'];
			$ret['rank'] = $mk['rank'];
		}
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		echo "下標 $time 秒<br>";
		
		echo json_encode($ret);
	}
	
	// 連續下標
	public function chk_range($product)
	{
		$time_start = $this->time_start;
		
		global $db, $config;
		
		//殺價記錄數量
		$this->tickets = ((float)$_POST['price_stop'] - (float)$_POST['price_start']) / $this->currency_unit + 1;
		
		//殺價起始價格
		$this->price = $_POST['price_start'];
		
		$r['err'] = '';
		
		if (empty($_POST['price_start']) || empty($_POST['price_stop']) ) {
		    //'金額不可空白'
			$r['err'] = 4;
		}
		elseif (!is_numeric($_POST['price_start']) || !is_numeric($_POST['price_stop']) ) {
		    //'金額格式錯誤'
			$r['err'] = 5;
		}
		elseif ((float)$_POST['price_start'] < (float)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
		}
		elseif ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] < $this->range_saja_time_limit) ) {
		    //'超過可连续下標时间'
			$r['err'] = 7;
		}
		elseif ($this->tickets > (int)$product['usereach_limit']) {
		    //'超過可连续下標次數限制'
			$r['err'] = 8;
		}
		elseif ((float)$_POST['price_start'] > (float)$_POST['price_stop']) {
		    //'起始價格不可超過结束價格'
			$r['err'] = 9;
		}
		elseif ((float)$_POST['price_stop'] > (float)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
		}
		/*
		elseif ((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid']) ) {
		    //'超過可下標次數限制'
			$r['err'] = 11;
		}
		*/
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		echo "連續下標 $time 秒<br>";
		
		return $r;
	}
	
	// 單次下標
	public function chk_single($product)
	{
		$time_start = $this->time_start;
		
		global $db, $config;
		
		//殺價記錄數量
		$this->tickets = 1;
		
		//殺價起始價格
		$this->price = $_POST['price'];
		
		$r['err'] = '';
		
		if (empty($this->price)) {
		    //'金額不可空白'
			$r['err'] = 4;
		}
		elseif (!is_numeric($this->price) ) {
		    //'金額格式錯誤'
			$r['err'] = 5;
		}
		elseif ((float)$this->price < (float)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
		}
		elseif ((float)$this->price > (float)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
		}
		/*
		elseif ((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid']) ) {
		    //'超過可下標次數限制'
			$r['err'] = 11;
		}*/
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		echo "單次下標 $time 秒<br>";
		
		return $r;
	}
	
	//檢查使用者下標次數限制
	/*
	public function chk_user_limit($user_limit, $pid)
	{
		$time_start = $this->time_start;
		
		global $db, $config;
	
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND productid = '{$pid}'
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		$tickets_avaiable = (int)$user_limit - (int)$table['table']['record'][0]['count'];
		
		if($this->tickets > $tickets_avaiable) {
			//'超過可下標次數限制'
			$time_end = microtime(true);
			$time = number_format(($time_end - $time_start),10);
			echo "檢查使用者下標次數限制 $time 秒<br>";
		
			return 1;
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		echo "檢查使用者下標次數限制 $time 秒<br>";
		
		return 0;
	}
	*/
	
	//檢查最後下標時間
	/*
	public function chk_time_limit()
	{
		$time_start = $this->time_start;
		
		global $db, $config;
		
		$query = "
		SELECT unix_timestamp(MAX(insertt)) as insertt, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$this->userid}' 
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			if( (int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $this->saja_time_limit) {
				//'下標时间间隔過短，請稍后再下標!'
				$time_end = microtime(true);
				$time = number_format(($time_end - $time_start),10);
				echo "檢查最後下標時間 $time 秒<br>";
				
				return 1;
			}
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		echo "檢查最後下標時間 $time 秒<br>";
		
		return 0;
	}
	*/
	
	//產生下標歷史記錄
	public function mk_history($product)
	{
		$time_start = $this->time_start;
		
		global $db, $config;
		
		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = '';
		$values = array();
		
		if($_POST['use_sgift'] == 'Y') 
		{
			//檢查禮券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}sgift`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}' 
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
			ORDER BY seq
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$sgift = 0;
			}
			else {
				$sgift = count($table['table']['record']); //殺價禮券數量
			}

			if($this->tickets > $sgift) {
				//'下標礼券不足'
				$r['err'] = 12;
			}
			else 
			{
				//使用禮券
				for($i = 0; $i < $this->tickets; $i++) {
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}sgift` 
					SET 
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND sgiftid = '{$table['table']['record'][$i]['sgiftid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);
					
					//殺價下標歷史記錄（對應下標記錄與下標禮券id）
					$values[] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'0',
						'{$table['table']['record'][$i]['sgiftid']}',
						'".($this->price + $i * $this->currency_unit)."',
						NOW()
					)";
				}
			}
			
			$time_end = microtime(true);
			$time = number_format(($time_end - $time_start),10);
			echo "→檢查禮券數量 $time 秒<br>";
		}
		else 
		{
			//檢查殺幣餘額
			$query = "SELECT SUM(amount) as amount
			FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
			WHERE 
				userid = '{$this->userid}'
			";
			/*
				prefixid = '{$config['default_prefix_id']}' 
				AND userid = '{$this->userid}'
				AND countryid = '{$this->countryid}'
				AND switch = 'Y'
			*/
			$table = $db->getQueryRecord($query);
			
			$saja_fee = (int)$this->tickets * (float)$product['saja_fee'];//殺價手續費
			
			$time_end = microtime(true);
			$time = number_format(($time_end - $time_start),10);
			echo "→檢查殺幣餘額 $time 秒<br>";
			
			if($saja_fee > (float)$table['table']['record'][0]['amount']) {
				//'殺價幣不足'
				$r['err'] = 13;
			}
			else
			{
				//使用殺幣
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint` 
				(
					`prefixid`,
					`userid`,
					`countryid`,
					`behav`,
					`amount`,
					`insertt`
				)
				VALUE
				(
					'{$config['default_prefix_id']}',
					'{$this->userid}',
					'{$this->countryid}',
					'user_saja',
					'". - $saja_fee ."',
					NOW()
				)
				";
				$res = $db->query($query);
				$spointid = $db->_con->insert_id;

				for($i = 0 ; $i < $this->tickets ; $i++) {
					//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
					$values[] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$spointid}',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						NOW()
					)";
				}
				
				$time_end = microtime(true);
				$time = number_format(($time_end - $time_start),10);
				echo "→使用殺幣 $time 秒<br>";
			}
		}

		if(empty($values)) {
			//'下標失敗'
			$r['err'] = -1;
		}
		
		if(empty($r['err']) && !empty($values) )
		{
			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
			(
				prefixid,
				productid,
				userid,
				spointid,
				sgiftid,
				price,
				insertt
			)
			VALUES
			".implode(',', $values)."
			";
			$res = $db->query($query);
			
			$time_end = microtime(true);
			$time = number_format(($time_end - $time_start),10);
			echo "→產生下標歷史記錄 $time 秒<br>";

			//單次下標而且離結標還有十分鐘以上，回傳目前順位
			if($_POST['type'] == 'single') 
			{
				$rank = $this->last_rank;//最大顯示排名為第12位 _".substr($product['productid'],-1)."
				echo "<br>";
				echo $query = "SELECT h.*, up.nickname
				FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h 
				LEFT OUTER JOIN `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_profile` up ON 
					h.prefixid = up.prefixid
					AND h.userid = up.userid
					AND up.switch = 'Y'
				WHERE
					h.prefixid = '{$config['default_prefix_id']}'
					AND h.productid = '{$product['productid']}'
					AND h.switch = 'Y'
				GROUP BY h.price
				HAVING COUNT(*) = 1
				ORDER BY h.price ASC
				LIMIT {$this->last_rank}
				";
				echo "<br><br>";
				$table = $db->getQueryRecord($query);

				if (!empty($table['table']['record'])) 
				{
					//回傳中標者
					$r['winner'] = $table['table']['record'][0]['nickname']; 

					foreach($table['table']['record'] as $rk => $rv) {
						if ($rv['price'] == $_POST['price']) {
							$rank = $rk + 1;
							break;
						}
					}
				}

				//回傳下標顺位
				$r['rank'] = $rank;
				
				$time_end = microtime(true);
				$time = number_format(($time_end - $time_start),10);
				echo "→單次下標順位提示 $time 秒<br>";
			}
			
			/*
			if((int)$product['saja_limit'] > 0) 
			{
				//如果有下標次數限制，則檢查目前已下標次數
				$query = "SELECT COUNT(*) as count 
				FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` 
				WHERE 
					prefixid = '{$config['default_prefix_id']}'
					AND productid = '{$product['productid']}'
					AND switch = 'Y'
				";
				$table = $db->getQueryRecord($query);

				//若下標次數大於標數限制，則鎖定殺價商品
				if ((int)$table['table']['record'][0]['count'] >= (int)$product['saja_limit']) {
					$query = "UPDATE `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}product` 
					SET locked = 'Y'
					WHERE 
						prefixid = '{$config['default_prefix_id']}' 
						AND productid = '{$product['productid']}'
						AND switch = 'Y'		
					";
					$res = $db->query($query);
				}
				
				$time_end = microtime(true);
				$time = number_format(($time_end - $time_start),10);
				echo "→檢查下標次數限制 $time 秒<br>";
			}*/
		}
		
		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		echo "產生下標歷史記錄 $time 秒<br>";
		
		return $r;
	}
	
}

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
echo "<br>php 結束 $time 秒";
?>
