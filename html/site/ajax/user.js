// JavaScript Document
// alert("user.js : "+APP_DIR);
// APP_DIR is defined in cssjs.php 

/* login.php */
function sajalogin(_to) {

//function login() {
	var pageid = $.mobile.activePage.attr('id');
	var olphone = $.trim($('#'+pageid+' #lphone').val());
	var olpw = $('#'+pageid+' #lpw').val();
	var _redir_to = _to || '/member/';
	if(olphone=="" && olpw=="") {
		alert('帳號及密碼不能空白');
	}
	else 
	{
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'login', username:olphone, passwd:olpw}, 
		function(str_json) 
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status < 1) {
					alert('登錄失敗');
				}
				else {
					var msg = json.retMsg;
					if (json.status==202){ 
						alert('請立即更新密碼 !');
						location.href=APP_DIR+'/user/upwd/';
					}
					else if(json.status==200 || json.status==201){ 
						 location.href=APP_DIR+_redir_to;
						// location.href=APP_DIR+'/member/';
					}
					else{
						alert(msg);
					}
					/*
					else if(json.status==2){ alert('請填寫帳號'); }
					else if(json.status==3){ alert('帳號不存在'); }
					else if(json.status==4){ alert('請填寫密碼'); }
					else if(json.status==5){ alert('密碼不正確'); }
					else if(json.status==6){ alert('臨時密碼逾時或錯誤'); }
					*/
				}
			}
		});
	}
}

function webapp_wxlogin(){
	if(!is_weixin() && !is_kingkr_obj()) {
		alert('請在App內使用此功能 ！！');
		return;
	} else if(is_kingkr_obj()){
		login('WEIXIN','','wxloginResult');
	} else if(is_weixin()) {
		window.location.href=APP_DIR+"/user/weixinapi/";
	}
}

function wxloginResult(jstr){
	if(jstr) {
		var j=JSON.parse(jstr);
		$.ajax({
			type:"POST",
			url:APP_DIR+"/oauthapi/WebAppLogin",
			data:{
				thumbnail_url:j.head_imgurl,
				nickname:j.nickname,
				sex:j.sex,
				uid:j.openid,
				uid2:j.unionid,
				city:j.city,
				type:"weixin"
			},
			dataType:"json",
			success:function(data) {
				   
				if(j.nickname)
					alert(j.nickname+", 歡迎來到殺價王!!");

					location.href=APP_DIR+'/product';
					return;
			}
		});
	}
}

function profile_confirm() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var oname = $('#'+pageid+' #name').val();
	var ozip = $('#'+pageid+' #zip').val();
	var oaddress = $('#'+pageid+' #address').val();
	var orphone = $('#'+pageid+' #rphone').val();
	
	if(oname == "" || ozip == "" || oaddress == "" || oaddress == "" || orphone == "") {
		alert('收件人姓名、郵編、電話及地址不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'profile', name:oname, zip:ozip, address:oaddress, rphone:orphone}, 
		function(str_json) {
			if(str_json) {
				var msg = '';
				var json = $.parseJSON(str_json);
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				}
				else if (json.status < 1) { alert('收件人信息註冊失敗'); }
				else if(json.status==2){ alert('請填寫收件人姓名'); }
				else if(json.status==3){ alert('請填寫收件人郵遞區號'); }
				else if(json.status==4){ alert('請填寫收件人地址'); }
				else if(json.status==5){ alert('請填寫收件人電話'); }
				else if (json.status==8) { alert('郵遞區號格式錯誤'); }
				else if (json.status==200){
					alert('收件人資訊完成修改');
					location.href=APP_DIR+"/member/";
				}
			}
		});
	}
}

function profile() {
	var pageid = $.mobile.activePage.attr('id');
	var oname = $('#'+pageid+' #name').val();
	var ozip = $('#'+pageid+' #zip').val();
	var oaddress = $('#'+pageid+' #address').val();
	var orphone = $('#'+pageid+' #rphone').val();
	
	if(oname == "" && ozip == "" && oaddress == "" && oaddress == "" && orphone == "") {
		alert('收件人姓名、郵編、電話及地址不能空白');
	} else {
		$.mobile.changePage(APP_DIR+"/user/profile_confirm/", {transition:"slide"} );
		
		$(this).everyTime('1s','profile',
		function() {
			var pageid = $.mobile.activePage.attr('id');
			if ($('#'+pageid+' #liname').length > 0) {
				$('#'+pageid+' #liname').html('');
				$('#'+pageid+' #lizip').html('');
				$('#'+pageid+' #liaddress').html('');
				$('#'+pageid+' #lirphone').html('');
				$('#'+pageid+' #name').val('');
				$('#'+pageid+' #zip').val('');
				$('#'+pageid+' #address').val('');
				$('#'+pageid+' #rphone').val('');
				$('#'+pageid+' #liname').html($('#'+pageid+' #liname').html()+oname);
				$('#'+pageid+' #lizip').html($('#'+pageid+' #lizip').html()+ozip);
				$('#'+pageid+' #liaddress').html($('#'+pageid+' #liaddress').html()+oaddress);
				$('#'+pageid+' #lirphone').html($('#'+pageid+' #lirphone').html()+orphone);
				$('#'+pageid+' #name').val(oname);
				$('#'+pageid+' #zip').val(ozip);
				$('#'+pageid+' #address').val(oaddress);
				$('#'+pageid+' #rphone').val(orphone);
				$(this).stopTime('profile');
			}
		});
	}
}

function forget() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}

	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#'+pageid+' #phone').val();
	
	if(ophone == "") {
		alert('手機號碼不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'forget', phone:ophone}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				}
				else if (json.status < 1) {
					alert('新密碼發送失敗');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('新密碼已發送至您的手機，請注意查收');
						location.href=APP_DIR+"/";
					} 
					else if(json.status==2){ alert('請填寫暱稱'); } 
					else if(json.status==3){ alert('請填寫手機號碼'); } 
					else if(json.status==4){ alert('查無此手機號'); }
					else if(json.status==5){ alert('手機號碼不正確'); }
				}
			}
		});
	}
}

/* forgetnickname.php */
function forgetnickname() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}

	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#'+pageid+' #phone').val();
	
	if($.trim(ophone) == "") {
		alert('手機號碼不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'forgetnickname', phone:ophone}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 1) {
					alert('新密碼發送失敗');
				} else {
					var msg = '';
					if (json.status==200){ 
						alert('暱稱查詢發送成功');
						location.href=APP_DIR+"/";
					} 
					else if(json.status==3){ alert('請填寫手機號碼'); } 
					else if(json.status==4){ alert('手機不正確'); }
				}
			}
		});
	}
}

/* cdnforget.php */
function forgetexchange() {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#'+pageid+' #phone').val();
	var opw = $('#'+pageid+' #passwd').val();

	if($.trim(ophone) == "") {
		alert('手機號碼不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'forgetexchange', phone:ophone, pw:opw}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 1) {
					alert('新兌換密碼發送失敗');
				} else {
					var msg = '';
					if (json.status==200){ 
						alert('新兌換密碼發送成功');
						location.href=APP_DIR+"/";
					}
					else if(json.status==2){ alert('請填寫完整手機號碼'); }
					else if(json.status==4){ alert('手機號碼不正確'); }
					else if(json.status==5){ alert('手機號碼不正確'); }
				}
			}
		});
	}
}

/* profile.php */
function user_pw() {
	var pageid = $.mobile.activePage.attr('id');
	var ooldpw = $('#'+pageid+' #oldpw').val();
	var onewpw = $('#'+pageid+' #newpw').val();
	var onewrepw = $('#'+pageid+' #newrepw').val();
	
	if(ooldpw == "" && onewpw == "" && onewrepw == "") {
		alert('舊密碼、新密碼及密碼確認不能空白');
	} else {
		if(onewpw !== onewrepw) {
			alert('新密碼與密碼確認不相同，請再確認');
		} else {
			$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
			{type:'pw', oldpasswd:ooldpw, passwd:onewpw, repasswd:onewrepw}, 
			function(str_json) {
				if(str_json) {
					var json = $.parseJSON(str_json);
					if (json.status==1) {
						alert('殺友請先登入');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('密碼修改失敗');
					} else {
						var msg = '';
						if (json.status==200){ 
							alert('密碼修改成功');
							location.reload();
						}
						else if(json.status==2){ alert('帳號不存在'); }
						else if(json.status==3){ alert('舊密碼錯誤'); }
						else if(json.status==4){ alert('新密碼格式錯誤'); }
						else if(json.status==5){ alert('密碼確認錯誤'); }
					}
				}
			});
		}
	}
}

/* profile.php */
function user_expw() {
	var pageid = $.mobile.activePage.attr('id');
	var ooldexpw = $('#'+pageid+' #oldexpw').val();
	var onewexpw = $('#'+pageid+' #newexpw').val();
	var onewexrepw = $('#'+pageid+' #newexrepw').val();
	
	if(ooldexpw == "" && onewexpw == "" && onewexrepw == "") {
		alert('兌換密碼：舊密碼、新密碼及密碼確認不能空白');
	} else {
		if(onewexpw !== onewexrepw) {
			alert('兌換密碼：新密碼與密碼確認不相同，請再確認');
		} else {
			$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
			{type:'expw', oldexpasswd:ooldexpw, expasswd:onewexpw, exrepasswd:onewexrepw}, 
			function(str_json) {
				if(str_json) {
					var json = $.parseJSON(str_json);
					if (json.status==1) {
						alert('殺友請先登入');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert('兌換密碼修改失敗');
					} else {
						var msg = '';
						if (json.status==200){ 
							alert('兌換密碼修改成功');
							location.reload();
						}
						else if(json.status==2){ alert('帳號不存在'); }
						else if(json.status==3){ alert('兌換密碼舊密碼錯誤'); }
						else if(json.status==4){ alert('兌換密碼新密碼格式錯誤,或是密碼長度不足6碼'); }
						else if(json.status==5){ alert('兌換密碼確認錯誤'); }
					}
				}
			});
		}
	}
}

/* user.php */
function user_qqno() {
	var pageid = $.mobile.activePage.attr('id');
	var qq = $('#'+pageid+' #qqno').val();
	if(qq=="") {
		alert('qq帳號不能為空白 !!');
		return false;
	} else {
		if(confirm('請再確認您輸入的qq帳號: '+qq+' 是否正確 ??\n一旦綁定QQ帳號之後即無法修改。')==false) {
		   return false;
		}
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(),
		{type:'updateqqno',qqno:qq},
		function(req_str) {
			if(req_str) { 
			   var json = $.parseJSON(req_str);
				if (json.status<0) {
					alert(json.retmsg);
					if(json.status==-1) {
					   // redir to 登入页面
					  $('#'+pageid+' #'+rightid).panel("open");
					}
				} else if(json.status==200) {
					alert(json.retmsg);
					location.reload();
				}
			}
		});
	} 
}

/* profile.php */
function extrainfo() {
	console.log($().length);
	var pageid = $.mobile.activePage.attr('id');
	var uecArr = {};
	
	$('#' + pageid + ' input[name^="uecid"]').each(function() {
		var uecid = $(this).val();
		var uecname = $('#' + pageid + ' input[name="uecname[' + uecid + ']"]').val();
		var sum = 0;
		var total = 0;
		uecArr = new Object();
		
		$('#' + pageid + ' input[name^="uecfield[' + uecid + ']"]').each(function() {
			var fieldname = $(this).attr("id").replace('uecfield_' + uecid + '_', '');
			uecArr[fieldname] = $(this).val();
			
			if ($(this).val() != '') {
				sum++;
			}
			
			total++;
		});
		
		if(uecid == 2) {
			uecArr['field2name'] = $("select[name='city']").val() + "_" + $("select[name='school']").val();
		}
		
		if((sum > 0) && (sum != total)) {
			alert('請將 ' + uecname + ' 資料，填寫完整');
			return false;
		} else if ((sum > 0) && (sum == total)) {
			$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
			{type:'extrainfo',  uecid:uecid, uecArr:uecArr}, 
			function(str_json) {
				if(str_json) {
					var json = $.parseJSON(str_json);	
					if(json.status==1) {
						alert('殺友請先登入');
						location.href=APP_DIR+"/member/userlogin/";
					} else if (json.status < 1) {
						alert(uecname + ' 資料修改失敗');
					} else {
						var msg = '';
						if (json.status==200){ 
							alert(uecname + '修改成功');
							location.reload();
						}
						else if(json.status==2){ alert('帳號不存在'); }
					}
				}
			});
		}
	});
}

/* profile.php */
function bonus_noexpw() {

	var pageid = $.mobile.activePage.attr('id');
	var bonus = $('#'+pageid+' #bonus_noexpw').val();

	if(isNaN(bonus) || bonus == '') {
		$('#bonus_noexpw').val('0');
		alert('額度必須為數字 !!');
		return false;
	} 
	if(bonus<0) {
		alert('額度必須為正數 !!');
		return false;
	}
	$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(),
	{'type':'bonus_noexpw','bonus_noexpw':bonus},
	function(req_str) {
		if(req_str) { 
			var json = $.parseJSON(req_str);
			if (json.status<0) {
				alert(json.retmsg);
				if(json.status==-1) {
				   // redir to 登入页面
				  $('#'+pageid+' #'+rightid).panel("open"); 
				}
			} else if(json.status==200) {
				alert('兌換額度修改成功');
				location.reload();
			}
		}
	});
}

/* profile_edit.php */
function gift_affiliate() {
	var pageid = $.mobile.activePage.attr('id');
	var introby = $('#'+pageid+' #introby').val();
	
	if(introby == '' || introby == 0) {
		alert('推薦人ID 不能空白 !');
		return false;
	} else if(isNaN(introby) ) {
		$('#introby').val('0');
		alert('推薦人ID 必須為數字 !');
		return false;
	}  else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'giftAff', intro_by:introby}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if(json.status==0) {
					alert('殺友請先登入 !!');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 0) {
					if(json.status==-1){ alert('會員資料錯誤 !'); }
					else if(json.status==-2){ alert('推薦人ID 不能空白 !'); }
					else if(json.status==-3){ alert('推薦人ID 必須為數字 !'); }
					else if(json.status==-4){ alert('推薦人ID 設定失敗 !'); }
				} else if (json.status==1) {
					$('#affiliate_btn').addClass('disabled');
					alert('推薦人ID 設定成功 !');
					location.reload();
				}
			}
		});
	}
}

/* profile.php */
function user_nickname() {
	var pageid = $.mobile.activePage.attr('id');
	var onickname = $('#'+pageid+' #nickname').val();
	
	if(onickname == "") {
		alert('暱稱不能空白');
	} else if(onickname.substr(0,1)=="_") {
	    alert('暱稱不能以 "_" 開頭 !!');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'nicknamechange', nickname:onickname}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if(json.status==0) {
					alert('殺友請先登入 !!');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 0) {
					if(json.status==-1){ alert('暱稱修改失敗'); }
					else if(json.status==-2){ 
						alert('暱稱重覆');
						$('#'+pageid+' #nickname').val(json.newname);
						$('#'+pageid+' #handling').html('推薦使用暱稱 !!');
					}
					else if(json.status==-3){ alert('暱稱錯誤'); }
					else if(json.status==-4){ alert('暱稱不能空白'); }
					else if(json.status==-5){ alert('暱稱不能含特殊字符'); }
				} else if (json.status==1) {
					alert('暱稱修改成功 !!');
					location.reload();
				}
			}
		});
	}
}

/* profile.php */
function channel_modify() {
	var pageid = $.mobile.activePage.attr('id');
	var ocountryid = $('#'+pageid+' #countryid').val();
	var oregionid = $('#'+pageid+' #regionid').val();
	var oprovinceid = $('#'+pageid+' #provinceid').val();
	var ochannelid = $('#'+pageid+' #channelid').val();
	
	if(ocountryid == '' || oregionid == '' || oprovinceid == '' || ochannelid == '') {
		alert('國家、區域、省/直轄市及分區不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'channelmodify', countryid:ocountryid, regionid:oregionid, provinceid:oprovinceid, channelid:ochannelid}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if(json.status == 1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				} else if(json.status < 1) {
					alert('資料修改失敗');
				} else {
					var msg = '';
					if (json.status==200){ 
						alert('資料修改成功');
						location.reload();
					}
					else if(json.status == 2) { alert('國家、區域、省/直轄市及分區不能空白'); }
					else if(json.status == 3) { alert('請勿選擇與預設值相同'); }
				}
			}
		});
	}
}

function enterprise() {
	var pageid = $.mobile.activePage.attr('id');
	var oesname = $('#'+pageid+' #esname').val();
	var ocompanyname = $('#'+pageid+' #esname').val();
	var ologinname = $('#'+pageid+' #loginname').val();
	var oepname = $('#'+pageid+' #epname').val();
	var oephone = $('#'+pageid+' #ephone').val();
	var opasswd = $('#'+pageid+' #passwd').val();
	var ocheckpasswd = $('#'+pageid+' #checkpasswd').val();
	var ousersrc = $('#'+pageid+' #usersrc').val();
	var oratio = $('#'+pageid+' #ratio').val();
	var opic = $('#'+pageid+' #pic').val();
	var opicurl = $('#'+pageid+' #picurl').val();
	var ospic = $('#'+pageid+' #spic').val();
	var ochannel = $('#'+pageid+' #channel').val();
	
	if(oesname != "" && ologinname != "" && oepname != "" && oephone != "" && opasswd != "" && ocheckpasswd != "" &&  oratio != "" &&  ochannel != "") {

		if(isNaN(oratio)) {
			alert('分潤比必須為數字 !!');
			return false;
		}
		if (!oratio.match("^[0-9]*[1-9][0-9]*$")) {
			alert('分潤比最小單位為整數!');
			return false;
		}
		if (ologinname.length<4) {
			alert('您的登入帳號必須大於四位');
			return false;
		}		
		if (opasswd.length<4) {
			alert('您的密碼必須大於四位');
			return false;
		}
		if(oephone == "") {
			alert('手機號不能為空白 !!');
			return false;
		} else if(isNaN(Number(oephone))==true) {
			alert('手機號必須為數字!');
			return false;
		}
		
		if(opasswd !== ocheckpasswd) {
			alert('密碼與密碼確認不相同，請再確認');
		} else {		
			$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
			{type:'tobeenterprise', esname:oesname, companyname:ocompanyname, loginname:ologinname, epname:oepname, ephone:oephone, passwd:opasswd, checkpasswd:ocheckpasswd, usersrc:ousersrc, ratio:oratio, pic:ospic, picurl:opicurl, spic:ospic, channel:ochannel}, 
			function(str_json) {
				if(str_json) {
					var json = $.parseJSON(str_json);
					if (json.status==0) {
						alert('殺友請先登入');
						location.href=APP_DIR+"/member/userlogin/";
					} else if(json.status==-1) {
						alert('申請商戶失敗');
					} else {
						var msg = '';
						if (json.status==1){ 
							alert('申請商戶完成');
							location.href=APP_DIR+"/enterprise/";
						}
						else if(json.status==-2){ alert(json.kind + '!!'); }
						else if(json.status==-3){ alert('商戶登入帳號錯誤'); }
						else if(json.status==-4){ 
							alert('商戶登入帳號重覆');
							$('#'+pageid+' #loginname').val(json.newname);
							$('#'+pageid+' #handling').html('推薦使用商戶登入帳號 !!');
						}
						else if(json.status==-5){ alert('聯絡信箱格式錯誤'); }
						else if(json.status==-6){ alert('密碼與密碼確認不相同，請再確認'); }
						else if(json.status==-7){ alert('密碼格式錯誤'); }
					}
				}
			});
		}
	} else {
		alert('請將資料填寫完整，不能空白');
	}
}

function collectprod(prod,kind,ptype) {
	var pageid = $.mobile.activePage.attr('id');
	var ouserid = $('#'+pageid+' #userid').val();
	var user = {'auth_id':ouserid};
	
	if(prod != "" && kind != "" && ptype != "" && ouserid != "") {

		$.post(APP_DIR+"/user/setCollectProd?t="+getNowTime(), 
		{client:user, productid:prod, prod_type:kind, switch:ptype}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if(json.retCode==-1) {
					alert('收藏商品失敗');
				} else {
					if (json.retCode==1) { 
						alert('收藏商品完成');
						window.location.reload();
					}
				}
			}
		});

	} else {
		alert('收藏資料不完整 !!');
	}
}


function aforget() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}

	var pageid = $.mobile.activePage.attr('id');
	var ophone = $('#'+pageid+' #phone').val();
	
	if(ophone == "") {
		alert('手機號碼不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'forget', phone:ophone}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/auserlogin/";
				}
				else if (json.status < 1) {
					alert('新密碼發送失敗');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('新密碼已發送至您的手機，請注意查收');
						location.href=APP_DIR+"/member/auserlogin/";
					} 
					else if(json.status==2){ alert('請填寫暱稱'); } 
					else if(json.status==3){ alert('請填寫手機號碼'); } 
					else if(json.status==4){ alert('查無此手機號'); }
					else if(json.status==5){ alert('手機號碼不正確'); }
				}
			}
		});
	}
}


/* login.php */
function asajalogin() {

//function login() {
	var pageid = $.mobile.activePage.attr('id');
	var olphone = $.trim($('#'+pageid+' #lphone').val());
	var olpw = $('#'+pageid+' #lpw').val();
	
	if(olphone=="" && olpw=="") {
		alert('帳號及密碼不能空白');
	}
	else 
	{
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'login', username:olphone, passwd:olpw}, 
		function(str_json) 
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status < 1) {
					alert('登錄失敗');
				}
				else {
					var msg = '';
					if(json.status==200){ 
						location.href=APP_DIR+'/shareoscode/one_click_promote_scode_oauth_return_data_process/';
					} else if (json.status==201){ 
						location.href=APP_DIR+'/shareoscode/one_click_promote_scode_oauth_return_data_process/';
					} 
					else if(json.status==2){ alert('請填寫帳號'); }
					else if(json.status==3){ alert('帳號不存在'); }
					else if(json.status==4){ alert('請填寫密碼'); }
					else if(json.status==5){ alert('密碼不正確'); }
				}
			}
		});
	}
}


/* login.php */
function login3rd(_to) {

	var pageid = $.mobile.activePage.attr('id');
	var olphone = $.trim($('#'+pageid+' #lphone').val());
	var olpw = $('#'+pageid+' #lpw').val();
	
	if(olphone=="" && olpw=="") {
		alert('帳號及密碼不能空白');
	}
	else 
	{
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'login', username:olphone, passwd:olpw}, 
		function(str_json) 
		{
			if(str_json)
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status < 1) {
					alert('登錄失敗');
				}
				else {
					var msg = '';
					var to = _to +'?phone='+ json.retObj.auth_phone +'&saja_auth='+ json.retObj.auth_id +'&saja_secret='+ json.retObj.auth_secret;
					console.log(to); // exit;
					
					if(json.status==200){ 
						location.href = to;
					} else if (json.status==201){ 
						location.href = to;
					} 
					else if(json.status==2){ alert('請填寫帳號'); }
					else if(json.status==3){ alert('帳號不存在'); }
					else if(json.status==4){ alert('請填寫密碼'); }
					else if(json.status==5){ alert('密碼不正確'); }
				}
			}
		});
	}
}


function carrier_confirm() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var phonecode = $('#'+pageid+' #phonecode').val();
	
	if(phonecode == "" ) {
		alert('手機條碼不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user.php?t="+getNowTime(), 
		{type:'phonecode', phonecode:phonecode}, 
		function(str_json) {
			if(str_json) {
				var msg = '';
				var json = $.parseJSON(str_json);
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				}
				else if (json.status < 1) { alert('收手機條碼修改失敗'); }
				else if(json.status==2){ alert('請填寫手機條碼'); }
				else if (json.status==200){
					alert('手機條碼資訊完成修改');
					location.href=APP_DIR+"/member/";
				}
			}
		});
	}
}
