function alipay(){
	$.ajax({
		type: 'post',
		url: "{:U('AlipayCore/index')}", //(method path)
		data: '',
		dataType:'json',
		async: true,
		timeout: 200000,
		success: function(data){
			//成功回访
			payType(data,'ALIPAY','payResult');
		}
	});
}

function wxpay(){//按钮出發的交易事件
	$.ajax({//異步發送請求至后台拼接order串
		type: 'post',
		url: "{:U('WxPayHelper/index')}", //(method path)
		data: '',
		dataType:'json',
		async: true,
		timeout: 200000,
		success: function(data){
			//成功回访　
			payType(data,'WEIXIN','payResult');
		}
	});
}

function payResult(r) {
　　if(r == 0){
		setTimeout(function(){
			alert('支付成功！');
		}, 2000);
	}else{
		setTimeout(function(){
			alert('支付失敗請刷新后再试！');
		}, 2000);
　　}
}