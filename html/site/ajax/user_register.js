// JavaScript Document
function sso_register() {
	 var onickname = $('#nickname').val();
	 var ophone = $('#phone').val();
	 var osex = $('#gender').val();
	 var opw = $('#pw').val();
	 var orepw = $('#repw').val();
	 var ouser_src = $('#user_src').val();
	 var osso_name = $('#sso_name').val();
	 var osso_uid = $('#sso_uid').val();
	 var ogotourl = $('#gotourl').val();
	 var oproductid = $('#productid').val();
	 var othumbnail_url = $('#thumbnail_url').val();
	 var osubscribe = $('#subscribe').val();
	
	if(ophone=="") {
		alert('手機號碼不能為空白 !!');
		return false;
	} 
	if(isNaN(ophone) || ophone.indexOf('.')>=0 || ophone.indexOf('-')>=0 || ophone.indexOf('+')>=0) {
	    alert('手機號碼必需為數字 !!');
		return false;
	} else {
	    $.post(APP_DIR+"/ajax/user_register.php?t="+getNowTime(), 
		{
			type:'sso', 
			thumbnail_url:othumbnail_url ,
			user_src:ouser_src, 
			nickname:onickname, 
			gender:osex, 
			phone:ophone, 
			sso_name:osso_name, 
			sso_uid:osso_uid, 
			passwd:opw, 
			repasswd:orepw, 
			productid:oproductid,
			subscribe:osubscribe
		}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if(json.status < 1) {
					alert('註冊失敗');
				} else {
					var msg = '';
					if(json.status==200) { 
						alert('註冊成功!歡迎加入殺友行列!');
						if(ogotourl!='') {
						   location.href = ogotourl;
						} else {
						   location.href = APP_DIR+"/";
						}
					} 
					else if(json.status==2){ alert('請填寫暱稱'); } 
					else if(json.status==3){ alert('此暱稱已經有一樣的了!'); } 
					else if(json.status==4){ alert('請填寫手機號碼'); } 
					else if(json.status==5){ alert('手機號碼格式不正確'); } 
					else if(json.status==6){ alert('此手機號碼已存在'); }
					else if(json.status==7){ alert('請填寫密碼'); }
					else if(json.status==8){ alert('密碼格式不正確'); }
					else if(json.status==9){ alert('請填寫確認密碼'); }
					else if(json.status==10){ alert('密碼確認不正確'); }
					else if(json.status==11){ alert('兌換密碼格式不正確'); }
					else if(json.status==12){ alert('請填寫兌換密碼確認'); }
					else if(json.status==13){ alert('兌換密碼確認不正確'); }
					else if(json.status==15){ alert('活動已结束!'); }
				}
			}
		});
	}
}

/* cdnregistration.php */
function registration() {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var onickname = $.trim($('#'+pageid+' #nickname').val());
	var osex = $('#'+pageid+' #sex :radio:checked').val();
	var ophone = $.trim($('#'+pageid+' #phone').val());
	var opw = $('#'+pageid+' #pw').val();
	var orepw = $('#'+pageid+' #repw').val();
	var oexpw = $('#'+pageid+' #expw').val();
	var oexrepw = $('#'+pageid+' #exrepw').val();
	var ouser_src = $('#'+pageid+' #user_src').val();
	
	if(ophone.length != 10){
		alert('手機號長度為10碼');
	} else if (ophone.substr(0,2) !='09' ) {
		alert('手機號格式錯誤');
	} else if(onickname == "" && ophone && opw == "" && orepw == "") {
		alert('暱稱、手機、密碼及密碼確認不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user_register.php?t="+getNowTime(), 
		{type:'default', user_src:ouser_src, nickname:onickname, gender:osex, phone:ophone, passwd:opw, repasswd:orepw, expasswd:oexpw, exrepasswd:oexrepw}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if (json.status < 1) {
					alert('註冊失敗');
				} else {
					var msg = '';
					if (json.status==200) { 
						alert('註冊成功!歡迎加入殺友行列!');
						location.href = APP_DIR+"/member/";
					} 
					else if(json.status==2){ alert('請填寫暱稱'); } 
					else if(json.status==3){ alert('此暱稱已经有一樣的了!'); } 
					else if(json.status==4){ alert('請填寫手機號碼'); } 
					else if(json.status==5){ alert('手機號碼格式不正確'); } 
					else if(json.status==6){ alert('此手機號碼已存在'); } 
					else if(json.status==7){ alert('請填寫密碼'); }
					else if(json.status==8){ alert('密碼格式不正確'); }
					else if(json.status==9){ alert('請填寫確認密碼'); }
					else if(json.status==10){ alert('密碼確認不正確'); }
					else if(json.status==11){ alert('兌換密碼格式不正確'); }
					else if(json.status==12){ alert('請填寫兌換密碼確認'); }
					else if(json.status==13){ alert('兌換密碼確認不正確'); }
                    else if(json.status==15){ alert('活動已结束!'); }
				}
			}
		});
	}
}

function oscode_registration() {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var ophone = $.trim($('#'+pageid+' #phone').val());
	var oproductid = $('#'+pageid+' #productid').val();
	var ochannelid = $('#'+pageid+' #channelid').val();
	var ousersrc = $('#'+pageid+' #user_src').val();
	if(ophone == "") {
		alert('手機號碼為空或格式不正確 !!');
	} else {
		$.post(APP_DIR+"/ajax/user_register.php?t="+getNowTime(), 
		{type:'flash', phone:ophone, productid:oproductid, channelid:'', user_src:ousersrc}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if(json.status < 1) {
					alert('註冊失敗');
				} else {
					var msg = '';
					if(json.status==200) { 
						alert('註冊成功!歡迎加入殺友行列!');
						location.href = APP_DIR+"/product/saja/?canbid=Y&productid=" + json.pid;
					}  
					else if(json.status==4){ alert('請填寫手機號碼'); } 
					else if(json.status==5){ alert('手機號碼格式不正確'); } 
					else if(json.status==6){ alert('此手機號碼已存在'); } 
					else if(json.status==14){ alert('無此閃殺活動'); }
					else if(json.status==15){ alert('閃殺活動已结束 !!'); }
				}
			}
		});
	}
}


/* cdnregistration.php */
function aregistration() {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	
	var pageid = $.mobile.activePage.attr('id');
	var onickname = $.trim($('#'+pageid+' #nickname').val());
	var osex = $('#'+pageid+' #sex :radio:checked').val();
	var ophone = $.trim($('#'+pageid+' #phone').val());
	var opw = $('#'+pageid+' #pw').val();
	var orepw = $('#'+pageid+' #repw').val();
	var oexpw = $('#'+pageid+' #expw').val();
	var oexrepw = $('#'+pageid+' #exrepw').val();
	var ouser_src = $('#'+pageid+' #user_src').val();
	
	if(onickname == "" && ophone && opw == "" && orepw == "") {
		alert('暱稱、手機、密碼及密碼確認不能空白');
	} else {
		$.post(APP_DIR+"/ajax/user_register.php?t="+getNowTime(), 
		{type:'default', user_src:ouser_src, nickname:onickname, gender:osex, phone:ophone, passwd:opw, repasswd:orepw, expasswd:oexpw, exrepasswd:oexrepw}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if (json.status < 1) {
					alert('註冊失敗');
				} else {
					var msg = '';
					if (json.status==200) { 
						alert('註冊成功!歡迎加入殺友行列!');
						location.href = APP_DIR+"/shareoscode/one_click_promote_scode_oauth_return_data_process/";
					} 
					else if(json.status==2){ alert('請填寫暱稱'); } 
					else if(json.status==3){ alert('此暱稱已经有一樣的了!'); } 
					else if(json.status==4){ alert('請填寫手機號碼'); } 
					else if(json.status==5){ alert('手機號碼格式不正確'); } 
					else if(json.status==6){ alert('此手機號碼已存在'); } 
					else if(json.status==7){ alert('請填寫密碼'); }
					else if(json.status==8){ alert('密碼格式不正確'); }
					else if(json.status==9){ alert('請填寫確認密碼'); }
					else if(json.status==10){ alert('密碼確認不正確'); }
					else if(json.status==11){ alert('兌換密碼格式不正確'); }
					else if(json.status==12){ alert('請填寫兌換密碼確認'); }
					else if(json.status==13){ alert('兌換密碼確認不正確'); }
                    else if(json.status==15){ alert('活動已结束!'); }
				}
			}
		});
	}
}