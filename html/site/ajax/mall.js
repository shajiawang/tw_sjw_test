// JavaScript Document
function mall_checkout() {
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++)	{
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}	
	/*var pageid = $.mobile.activePage.attr('id');
	var oesid = $('#'+pageid+' #esid').val();
	var oepcid = $('#'+pageid+' #epcid').val();
	var oepid = $('#'+pageid+' #epid').val();
	var ototal = $('#'+pageid+' #total').val();
	var onum = $('#'+pageid+' #num').val();
	//var oname = $('#'+pageid+' #name').val();
	//var ozip = $('#'+pageid+' #zip').val();
	//var oaddress = $('#'+pageid+' #address').val();
	var oexpw = $('#'+pageid+' #expw').val();
	var ophone = $('#'+pageid+' #phone').val();
	var oamount = $('#'+pageid+' #amount').val();
	*/
	
	var oesid = $('#esid').val();
	var oepcid = $('#epcid').val();
	var oepid = $('#epid').val();
	var ototal = $('#total').val();
	var onum = $('#num').val();
	//var oname = $('#name').val();
	//var ozip = $('#zip').val();
	//var oaddress = $('#address').val();
	var oexpw = $('#captcha').val();
	var ophone = $('#phone').val();
	var oamount = $('#amount').val();	

	//if(onum == "" && oname == "" && ozip == "" && oaddress == "" && oepcid != "38")	{
	if ( (onum == null) || (oexpw == null) ){
		alert('數量及兌換密碼不能空白');
		return;
	} else {
		$.post(APP_DIR+"/ajax/mall.php?epcid="+ oepcid +"&epid="+ oepid +"&esid="+ oesid +"&t="+ getNowTime(), 
		//{type:'home', total:ototal, num:onum, name:oname, zip:ozip, address:oaddress}, 
		{type:'exchange', total:ototal, num:onum, expw:oexpw, phone:ophone}, 
		function(str_json) 
		{
			if(str_json) 
			{
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 1) {
					if(json.status == -2){
						alert('庫存不足');
					}else{
						alert('兌換失敗');
					}
				} else {
					var msg = '';
					if (json.status==200){ 
						//alert('兌換成功');
						location.href=encodeURI(APP_DIR+"/sys/showMsg/?code="+json.status+"&msg=兌換成功&kind=mall&kid="+oepid+"&total="+ototal);
					} 
					else if(json.status==2){ alert('請填寫收件人姓名'); } 
					else if(json.status==3){ alert('請填寫收件人郵編'); }
					else if(json.status==4){ alert('請填寫收件人地址'); }
					else if(json.status==5){ alert('红利點數不足'); }
					else if(json.status==6){ alert('兌換密碼不正確'); }
				}
			}
		});
	}
}

function mall_product() {
	var pageid = $.mobile.activePage.attr('id');
	var oepcid = $('#'+pageid+' #epcid').val();
	var oepid = $('#'+pageid+' #epid').val();
	var onum = $('#'+pageid+' #num').val();
	var oname = $('#'+pageid+' #name').val();
	var ozip = $('#'+pageid+' #zip').val();
	var oaddress = $('#'+pageid+' #address').val();
	var oeimoney = $('#'+pageid+' #eimoney').val();
	if (oepid == 525) {
	
		var oamount = $('#'+pageid+' #amount').val();
		var ouse = $('#'+pageid+' #use').val();
		var ophone = $('#'+pageid+' #phone').val();
	
		if(ophone == "") {
			alert('手機號不能為空白 !!');
			return;
		} else if (isNaN(Number(ophone))==true) {
			alert('手機號必需為數字!');
			return;	
		} else if (Number(oamount) < 50) {
			alert('兌換不能為小於50!');
			return;	
		} else if (Number(oamount) > Number(ouse)) {
			alert('鲨鱼點數不足!');		
			return;	
		} else if (!oamount.match("^[0-9]*[1-9][0-9]*$")) {
			alert('兌換最小單位為整數!');
			return;	
		}	
	}
	
	$.mobile.changePage(APP_DIR+"/mall/check/?epcid="+ oepcid +"&epid="+ oepid +"&num="+ onum + "&eimoney=" + oeimoney, {transition:"slide"} );
	$(this).everyTime('1s','mall_product',
	function() {
		var pageid = $.mobile.activePage.attr('id');

		$('#'+pageid+' #litotal').html('');
		$('#'+pageid+' #linum').html('');
		$('#'+pageid+' #liname').html('');
		$('#'+pageid+' #lizip').html('');
		$('#'+pageid+' #liaddress').html('');
		$('#'+pageid+' #total').val('');
		$('#'+pageid+' #num').val('');

		if (oepid != 525) {
			var total = parseFloat($('#'+pageid+' #bonus').val()) * parseFloat(onum) + parseFloat($('#'+pageid+' #handling').val());
			$('#'+pageid+' #litotal').html($('#'+pageid+' #litotal').html()+total-parseFloat($('#'+pageid+' #handling').val()));
			$('#'+pageid+' #linum').html($('#'+pageid+' #linum').html()+onum);
			$('#'+pageid+' #malltotal').html($('#'+pageid+' #malltotal').html()+total);
			$('#'+pageid+' #total').val(total);
			$('#'+pageid+' #num').val(onum);
		}else{
			$('#'+pageid+' #num').val(onum);
			$('#'+pageid+' #amount').val(oamount);
			$('#'+pageid+' #phone').val(ophone);
			$('#'+pageid+' #use').val(ouse);
			$('#'+pageid+' #total').val(oamount);
			var total = oamount;
			$('#'+pageid+' #liphone').html($('#'+pageid+' #liphone').html()+ophone+'');
			$('#'+pageid+' #litotal').html($('#'+pageid+' #litotal').html()+total);
			$('#'+pageid+' #liamount').html($('#'+pageid+' #liamount').html()+total+' 元');
		}
		$('#'+pageid+' #name').val(oname);
		$('#'+pageid+' #zip').val(ozip);
		$('#'+pageid+' #address').val(oaddress);
		$('#'+pageid+' #havebonus').html($('#'+pageid+' #all_bonus').val()-total);
		
		$(this).stopTime('mall_product');
	});
}

function mall_ecoupon() {
	var pageid = $.mobile.activePage.attr('id');
	var oepcid = $('#'+pageid+' #epcid').val();
	var oepid = $('#'+pageid+' #epid').val();
	var onum = $('#'+pageid+' #num').val();
	
	$.mobile.changePage(APP_DIR+"/mall/check/?epcid="+ oepcid +"&epid="+ oepid +"&num="+ onum, {transition:"slide"} );
	$(this).everyTime('1s','mall_ecoupon',
	function() {
		var pageid = $.mobile.activePage.attr('id');
		if ($('#'+pageid+' #liname').length > 0) {
			$('#'+pageid+' #litotal').html('');
			$('#'+pageid+' #linum').html('');
			$('#'+pageid+' #total').val('');
			$('#'+pageid+' #num').val('');
			
			var total = parseFloat($('#'+pageid+' #bonus').val()) * parseFloat(onum) + parseFloat($('#'+pageid+' #handling').val());
			$('#'+pageid+' #litotal').html($('#'+pageid+' #litotal').html()+total+' 點');
			
			$('#'+pageid+' #linum').html($('#'+pageid+' #linum').html()+onum);
			
			$('#'+pageid+' #total').val(total);
			$('#'+pageid+' #num').val(onum);
			
			$(this).stopTime('mall_ecoupon');
		}
	});
}

function mk_totalpoint() {
	var pageid = $.mobile.activePage.attr('id');
	var onum = $('#'+pageid+' #num').val();
	var obonus = $('#'+pageid+' #bonus').val();
	var ohandling = $('#'+pageid+' #handling').val();
	
	var total = parseFloat(obonus) * onum + parseFloat(ohandling);
	$('#'+pageid+' #totalpoint').html(parseFloat(total).toFixed(2));
}

function checkphone() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	alert('手機未驗證');
	location.href=APP_DIR+"/member/";
}

function mall_login() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	alert('殺友請先登入 !');
	location.href=APP_DIR+"/member/userlogin/";
}

function qrcode_exchange() {
	console.log($().length);
	var rightid;
	for(i=0; i<$('[data-role="panel"]').length; i++) {
		var rtemp = $('[data-role="panel"]:eq('+i+')').attr('id');
		if(rtemp.match("right-panel-") == "right-panel-") {
			rightid = rtemp;
		}
	}
	var pageid = $.mobile.activePage.attr('id');
	var oorderid = $('#'+pageid+' #orderid').val();
	var oepid = $('#'+pageid+' #epid').val();
	var oexpw = $('#'+pageid+' #expw').val();
	
	if(oexpw == "")	{
		alert('兌換密碼不能空白');
	} else if (oorderid == "" || oepid == "") {
		alert('訂單編號或產品編號不能空白');
	} else {
		$.post(APP_DIR+"/ajax/mall.php?t="+ getNowTime(), 
		{type:'qrcode', orderid:oorderid, epid:oepid, expw:oexpw}, 
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if (json.status==1) {
					alert('殺友請先登入 !');
					location.href=APP_DIR+"/member/userlogin/";
				}
				else if (json.status < 1) {
					alert('查無訂單编號');
				}
				else {
					var msg = '';
					if (json.status==200){ 
						alert('交易成功');
						location.href=APP_DIR+"/member/order/";
					} 
					else if(json.status==2){ alert('超過交易時間'); }
					else if(json.status==5){ alert('红利點數不足'); }
					else if(json.status==6){ alert('兌換密碼不正確'); }
				}
			}
		});
	}
}

//Add by Thomas 20141225
function userCancelTx() {
	if(confirm('確定取消本次交易 ?!')) {
		var pageid = $.mobile.activePage.attr('id');
		var oevrid=$('#'+pageid+' #evrid').val();
		var ouserid=$('#'+pageid+' #userid').val();
		$.post(APP_DIR+"/mall/userCancelTx/",
			{'evrid':oevrid,'userid':ouserid},
			function(str_json){
				if(str_json) {
					var json = $.parseJSON(str_json);
					if(json.retCode==1) {
						alert('交易已取消 !!');
						location.href=APP_DIR+"/member/";
					} 
				}
			}
		); 
	} 
}

function userConfirmTx() {
	var pageid = $.mobile.activePage.attr('id');
	var oexpw = $('#'+pageid+' #expw').val();
	var oevrid=$('#'+pageid+' #evrid').val();
	var ouserid=$('#'+pageid+' #userid').val();
	if(oexpw == "") {
		alert('兌換密碼不能為空白 !!');
		return;
	}
	$.post(APP_DIR+"/mall/userCommitTx/", 
		{'expw':oexpw,'evrid':oevrid,'userid':ouserid},
		function(str_json){
			if(str_json) {
				var json = $.parseJSON(str_json);
				alert(decodeURIComponent(json.retMsg));
				if(json.retCode=='1') {
				   location.href=APP_DIR+"/history/bonus/";
				}					
			}
		}
	);
}
// Add End

function mall_submit() {
	var pageid = $.mobile.activePage.attr('id');
	var oexpw = $('#'+pageid+' #expw').val();
	if(oexpw == "") {
		alert('兌換密碼不能為空白 !!');
		return;
	}
	$("#contactform").submit();
}


function mk_extotalpoint() {
	var pageid = $.mobile.activePage.attr('id');
	var onum = $('#'+pageid+' #amount').val();
	if (onum >= 100) {
		var handling = parseFloat(0.1 * parseFloat(onum));
		var total = parseFloat(1.1 * parseFloat(onum));
		$('#'+pageid+' #handling').html(parseFloat(handling).toFixed(2));
		$('#'+pageid+' #totalbonus').html(parseFloat(total).toFixed(2));
	} else if (Number(onum) < 100) {
		alert('兌換不能為小於100!');
		return;	
	} else if (!onum.match("^[0-9]*[1-9][0-9]*$")) {
		alert('兌換最小單位為整數!');
		return;	
	}	
}


function exchangespoint() {
	var pageid = $.mobile.activePage.attr('id');
	var amount = $('#'+pageid+' #amount').val();
	var json = $('#'+pageid+' #json').val();
	var use	= $('#'+pageid+' #use').val();
	if(amount == "") {
		alert('兌換不能為空白 !!');
		return;
	} else if (isNaN(Number(amount))==true) {
		alert('兌換必需為數字!');
		return;	
	} else if (Number(amount) < 100) {
		alert('兌換不能為小於100!');
		return;	
	} else if (Number(amount) > Number(use)) {
		alert('鲨鱼點數不足!');		
		return;	
	} else if (!amount.match("^[0-9]*[1-9][0-9]*$")) {
		alert('兌換最小單位為整數!');
		return;	
	}

	$("#contactform").submit();
}

function topay() {
	var pageid 	= $.mobile.activePage.attr('id');
	var ototal 	= $('#'+pageid+' #total').val();
	var ojson 	= $('#'+pageid+' #json').val();
	var oesid	= $('#'+pageid+' #esid').val();
	var ouserid	= $('#'+pageid+' #userid').val();
	var ospoint	= $('#'+pageid+' #spoint').val();
	var obonus	= $('#'+pageid+' #bonus').val();
	
	if(ototal == "") {
		alert('兌換點數不能為空白 !!');
		return;
	} else if (isNaN(Number(ototal))==true) {
		alert('兌換點數必需為數字!');
		return;	
	} else if (Number(ototal) > Number(ospoint)) {
		alert('鲨鱼點數不足!');		
		return;	
	}		 
	
	if(Number(ototal) > Number(obonus)) {
		var oexpw = prompt("請輸入兌換密碼","");
		if(oexpw == "")	{
			alert('兌換密碼不能空白');
			return;
		}		
	}
	
	$.post(APP_DIR+"/mall/topayBonus/?t="+ getNowTime(), 
	{json:ojson, total:ototal, esid:oesid, expw:oexpw, userid:ouserid, spoint:ospoint, bonus:obonus}, 
	function(str_json) 
	{
		if(str_json) 
		{
			var json = $.parseJSON(str_json);
			console && console.log($.parseJSON(str_json));
			
			var msg = '';
			if (json.status==1){ 
				alert('兌換成功');
				location.href=APP_DIR+"/mall/topayend/?esid="+oesid+"&orderid="+json.orderid;
			} else if(json.status<1) { 
				alert(json.kind + '!!');
			}				
		}
	});
}

function mall_ddcar() {
	var pageid = $.mobile.activePage.attr('id');
	var onum = $('#'+pageid+' #amount').val();
	var ouse = $('#'+pageid+' #use').val();
	
	if (onum >= 50) {
		var handling = parseFloat(parseFloat(onum));
		var total = parseFloat(parseFloat(onum));
		
		$('#'+pageid+' #handling').html(parseFloat(handling).toFixed(2));
		$('#'+pageid+' #totalbonus').html(parseFloat(total).toFixed(2));
	} else if (Number(onum) < 50) {
		alert('兌換不能為小於50!');
		return;
	} else if (Number(onum) > Number(ouse)) {
		alert('兌換不能為大於!' + Number(ouse));
		return;			
	} else if (!onum.match("^[0-9]*[1-9][0-9]*$")) {
		alert('兌換最小單位為整數!');
		return;	
	}	
}
// Add End
