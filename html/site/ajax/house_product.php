<?php
session_start();

$time_start = microtime(true);

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
	foreach($_POST2 as $k=> $v) {
		$_POST[$k]=$v;
		if($_POST['json']) {
			$_POST['json']=$_POST['json'];
		}
		if($_POST['auth_id']) {
			$_POST['userid']=$_POST['auth_id'];
		}
	}
}

$ret=null;
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json'];
error_log("[ajax/house_product] post bid data : ".json_encode($_POST));
error_log("[ajax/house_product] get bid data : ".json_encode($_GET));

if(empty($_SESSION['auth_id']) && empty($_POST['auth_id']) && empty($_POST['root'])) { //'請先登入會員帳號'

	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';

	} else {

	   $ret['status'] = 1;
	}

	echo json_encode($ret);
	exit;

} else {

	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");
	include_once(LIB_DIR ."/ini.php");
	include_once(BASE_DIR ."/model/user.php");
	require(LIB_DIR."/vendor/autoload.php"); // 載入wss client 套件(composer)

	// 延遲一點時間, 0.03 ~ 0.15 秒不等
	$sleep = getRandomNum()%5+1;
	// error_log("sleep: ".$sleep*500000);
	usleep($sleep*30000);


	$c = new ProductBid;
	$c->saja();
}

// use WebSocket\Client;

class ProductBid {
	public $userid = '';
	public $countryid = 1;//國家ID
	public $currency_unit = 1;//貨幣最小單位
	public $last_rank = 11;//預設順位提示（順位排名最後一位）
	public $saja_time_limit = 0;//每次下標時間間隔限制
	// public $display_rank_time_limit = 60;//（秒）下標後離結標還有n秒才顯示順位
	public $display_rank_time_limit = 0;//（秒）下標後離結標還有n秒才顯示順位
	public $range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標
	public $tickets = 1;
	public $price;
	public $username='';

	//下標
	public function saja() {
		$time_start = microtime(true);

		global $db, $config, $router, $user;

        
		$JSON=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$os_type=(empty($_POST['os_type'])) ? htmlspecialchars($_GET['os_type']) : htmlspecialchars($_POST['os_type']);

        $user = new UserModel;
		// $router = new Router();
		login_required();

		if ($JSON == 'Y') {
			$this->userid = htmlspecialchars($_POST['auth_id']);
			$this->username=  htmlspecialchars($_POST['auth_nickname']);
		}else{
			$this->userid = $_SESSION['auth_id'];
			$this->username= $_SESSION['user']['profile']['nickname'];
		}


		if(empty($this->userid) && $_POST['root'] == 'weixin' && !empty($_POST['userid'])) {
			$this->userid = $_POST['userid'];
		}

		// 如果沒有用戶編號 不給下標
		if(empty($this->userid)) {
		   if($JSON=='Y') {
			  echo json_encode(getRetJSONArray(-1,'NO_USER_DATA','MSG'));
		   } else {
			  // ToDo Web Error Page
		   }
		   return ;
		}
		 
		// 如果沒有商品編號  不給下標
		if(empty($_POST['productid'])) {
		   if($JSON=='Y') {
			  echo json_encode(getRetJSONArray(-3,'NO_PRODUCT_DATA','MSG'));
		   } else {
			  // ToDo Web Error Page
		   }
		   return ;
		}

		// 新版驗證簽名 20191021
		// update By Thomas 20191030
		$uts = (empty($_POST['uts'])) ? htmlspecialchars($_GET['uts']) : htmlspecialchars($_POST['uts']);
		$first_price  = (empty($_POST['first_price'])) ? htmlspecialchars($_GET['first_price']) : htmlspecialchars($_POST['first_price']);
		$sign  = (empty($_POST['sign'])) ? htmlspecialchars($_GET['sign']) : htmlspecialchars($_POST['sign']);
		$mysign = MD5($this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts.'|'.'sjW333-_@');
		error_log("[ajax/house_product] APP sign: ".$sign);
		error_log("[ajax/house_product] SERVER sign decode: ".$this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts);
		error_log("[ajax/house_product] SERVER sign : ".$mysign);
		
		// APP 才驗簽
		if($JSON=='Y') {
			if (empty($sign) || empty($mysign) || $sign!=$mysign) {
				error_log("[ajax/house_product] : Sign Check Failed !!");
				echo json_encode(getRetJSONArray(-20,'下標驗簽失敗','MSG'));
				exit();
			}
		}



		$db = new mysql(getDbConfig());
		$product = "";

		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['status'] = 0;
		$ret['winner'] = '';
		$ret['rank'] = '';
		$ret['value'] = 0;

		// 先從Cache中找商品資料
		$cache = getRedis('','');
		if($cache) {
		   $jsonStr = $cache->get('getProductById:'.$_POST['productid'].'|Y|Y');
		   if($jsonStr) {
			  $product = json_decode($jsonStr,TRUE);
		   }
		}
		// 如果Cache中沒有資料  則撈DB
		if(empty($product) || !is_array($product) || empty($product['productid'])) {
			//讀取商品資料
			$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			WHERE  p.prefixid = '{$config['default_prefix_id']}'
			  AND p.productid = '{$_POST['productid']}'
			  AND p.switch = 'Y'
			LIMIT 1	";
			$table = $db->getQueryRecord($query);
			unset($table['table']['record'][0]['description']);
			$product = $table['table']['record'][0];
		}
		
		// 如果沒有商品資料不給下標
		if(!is_array($product) || empty($product['productid'])) {
			if($JSON=='Y') {
			  echo json_encode(getRetJSONArray(-5,'NO_PRODUCT_DATA','MSG'));
		   } else {
			  // ToDo Web Error Page
		   }
		   return ;
		}

		$user_profile = $user->get_user_profile($this->userid);	
		// 如果沒有會員資料不給下標
		if($user_profile && $user_profile['userid']>0) {	
		   // error_log("[ajax/product/saja] user_profile:".json_encode($user_profile));
		} else {
		   if($JSON=='Y') {
			  echo json_encode(getRetJSONArray(-2,'NO_USER_DATA','MSG'));
		   } else {
			  // ToDo Web Error Page
		   }
		   return ;
		}
			
		error_log("[ajax/house_product] user_profile2 : ".json_encode($user_profile));
		// 如果沒有會員填寫身分證號末六碼不給下標
		if(empty($user_profile['idsix'])) {
			if($JSON=='Y') {
				 echo json_encode(getRetJSONArray(-15,"請先填寫身分證號末六碼 !",'MSG'));
			} else {
				 // ToDo Web Error Page
			}	
			return;	
		}
		
		$user_sms = $user->validSMSAuth($this->userid);	 
		error_log("[ajax/house_product] user_sms : ".json_encode($user_sms));
		 
		// 如果沒有會員手機驗證不給下標
		if(empty($user_sms['verified'])) {
			if($JSON=='Y') {
				 echo json_encode(getRetJSONArray(-14,"請先進行手機驗證 !",'MSG'));
			} else {
				 // ToDo Web Error Page
			}	
			return;	 
		}
		
		switch ($product['pay_type']) {
			case 'f':
				if ($_POST['pay_type'] != 'dscode-h' && $_POST['pay_type'] != 'dscode-n') {
					$r['err'] = 22;
					$r['retCode']=-100235;
					$r['retMsg']='請使用圓夢卷下標';
				}
				break;
		}
		if ($r['err'] && $r['err']>0) {
			if($JSON=='Y') {
				$ret=getRetJSONArray($r['retCode'],$r['retMsg'],'MSG');
				echo json_encode($ret);
				exit;
			} else {
				$ret['status'] = $r['err'];
			}
		}

		// 更新檢查的時間
		$product['now']=time();
		if(  $product['closed']!='N' ||
		    ($product['offtime'] == 0 && $product['locked'] == 'Y') ||
			($product['offtime'] > 0 && $product['now'] > $product['offtime'])) {
			//回傳: 本商品已結標
			$ret['status'] = 2;
			if($JSON=='Y') {
				$ret=getRetJSONArray(-2,'本商品已結標!!','MSG');
				echo json_encode($ret);
				exit;
			}
		}

		// 沒有用戶暱稱, 去DB撈 ?
		if(empty($this->username) && !empty($this->userid)) {
			$query ="SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE prefixid = '{$config['default_prefix_id']}'
			  AND userid = '{$this->userid}'
			  AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$this->username = $table['table']['record'][0]['nickname'];
		}

		// 下標總金額超過市價時 要擋
		$query ="SELECT sum(price) saja_total_price, count(*) saja_cnt
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
					WHERE productid = '{$product['productid']}'
					AND userid = '{$this->userid}'
					AND spointid > 0
					group by userid,productid";
		$table = $db->getQueryRecord($query);
		$discount = $table['table']['record'][0];

		if ($_POST['type']=='single') {
			$add_saja_fee = (int)$product['saja_fee'];
			$add_total_price = (int)$_POST['price'];
			$bidprice=$_POST['price'];
		}else if($_POST['type']=='range'){
			$times = (int)$_POST['price_stop']-(int)$_POST['price_start']+1;
			$add_saja_fee = (int)$times*(int)$product['saja_fee'];
			$add_total_price = (((int)$_POST['price_start']+(int)$_POST['price_stop'])*(int)$times)/2;
			$bidprice=$_POST['price_start']." ~ ".$_POST['price_stop'];
		}
		switch ($product['totalfee_type']) {
			case 'A'://手續費+出價金額
				$product['discount'] = 0;
				$product['discount'] += (int)$discount['saja_total_price'];
				$product['discount'] += (int)$product['saja_fee']*(int)$discount['saja_cnt'];
				$product['discount'] += (int)$add_saja_fee+(int)$add_total_price;
				break;
			case 'B'://出價金額
				$product['discount'] = (int)$discount['saja_total_price']+(int)$add_total_price;;
				break;
			case 'F'://手續費
				$product['discount'] = (int)$product['saja_fee']*(int)$discount['saja_cnt'];
				$product['discount'] += (int)$add_saja_fee;
				break;
			default://免費
				$product['discount'] = 0;
				break;
		}

		if($product['discount'] > $product['retail_price']) {
			$ret = getRetJSONArray(-100233,'下標總金額超過市價','MSG');
			if($JSON=='Y') {
				echo json_encode($ret);
			} else {
				$ret['status'] = 20;
				echo json_encode($ret);
			}
			return ;
		}

		// 檢查用卷數量
		// 因改為一天2標, 此限制取消
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$product['productid']}'
			AND spointid=0
			AND sgiftid=0
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		$xscode_count=(int)$table['table']['record'][0]['count'];
		if ($_POST['pay_type'] == 'dscode-h' || $_POST['pay_type'] == 'dscode-n') {
				$product['xcode_count'] = (1000000-$xscode_count);
				$product['xcode_use'] = ($xscode_count);
				error_log("[ajax/product] xcode_count : ".$product['xcode_count']);
		}
 
		//
		//下標資格
		// 閃殺不檢測 By Thomas 20190102
		if($product['is_flash']!='Y') {
			//if ($this->userid != 1705) {
				$chk = $this->chk_saja_rule($product);
				if ($chk['err'] && $chk['err']>0) {
					if($JSON=='Y') {
						$ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],$chk['retType']);
						echo json_encode($ret);
						exit;
					} else {
						$ret['status'] = $chk['err'];
						$ret['value'] = $chk['value'];
						$ret['retMsg'] = isset($chk['retMsg']) ? $chk['retMsg'] : '';
					}
				}
			//}
		}

		if($_POST['type']=='single') { //單次下標
			$chk = $this->chk_single($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				} else { 
				   $ret['status'] = $chk['err'];
				}
			}
		} else if($_POST['type']=='range' ) { //區間連續下標
			$chk = $this->chk_range($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				} else {
				   $ret['status'] = $chk['err'];
				}
			}
		} else if($_POST['type']=='lazy') { //懶人下標
			$chk = $this->chk_range($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				} else {
				   $ret['status'] = $chk['err'];
				}
			}
		}

		// 隨機產生出價(僅供非正式機的壓力測試用)
		if(BASE_URL!="https://www.saja.com.tw" && $_REQUEST['random_price']=='Y') {
		   $this->price = getRandomNum(1,$product['retail_price']);
		   $this->username.= $this->price;
		}

		if(empty($ret['status']) || $ret['status']==0 ) {
			$rep_patten[]="'";
			$rep_patten[]="'";
			$this->username=str_replace($rep_patten,"",$this->username);
			//產生下標歷史記錄
			$mk = $this->mk_history($product);
			error_log("[ajax/house_product] mk : ".json_encode($mk));
			//回傳: 下標完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			if($mk['err']) {
				// 有異常
				if($JSON=='Y') {
				   $ret=getRetJSONArray($mk['retCode'],$mk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				}
			}

			// 正常下標
			if($JSON=='Y') {
				$ret['retObj']=array();
				$ret['retObj']['winner'] = $mk['winner'];
				$ret['retObj']['rank'] = $mk['rank'];
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				//單次下標
				if($_POST['type']=='single') {
					if ($mk['rank']==-2) {
						$msg = '最後一分鐘下標不提示排名或重複 ！';
					} else if ($mk['rank']==-1) {
						$msg = '很抱歉！這個價錢與其他人重複了！請您試試其他價錢或許會有好運喔！';
					} else if($mk['rank']==0) {
						$msg = '恭喜！您是目前的得標者！記得在時間到之前要多佈局！';
					} else if ($mk['rank']>=1 && $mk['rank']<=10) {
						$msg = '恭喜！這個價錢是「唯一」但不是「最低」目前是「唯一」又比您「低」的還有 '.($mk['rank']).' 位。';
					} else if ($mk['rank']>10) {
						$msg = '恭喜！這個價錢是「唯一」但不是「最低」目前是「唯一」又比您「低」的超過 10 位。';
					}
					$ret['retMsg'] = '單次出價 '.$_POST['price'].'元 成功 '. $msg;
				} else if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['retObj']['total_num'] = $mk['total_num'];
					$ret['retObj']['num_uniprice'] = $mk['num_uniprice'];
					$ret['retObj']['lowest_uniprice'] = $mk['lowest_uniprice'];
					if($mk['total_num']>=2) {
						if($mk['num_uniprice']>0) {
						   $msg ='本次有 '.$mk['num_uniprice'].' 個出價是唯一價, 其中最低價格是 '.$mk['lowest_uniprice'].' 元 !';
						} else {
						   $msg ='本次下標的出價都與其他人重複了 !';
						}
					}
					$ret['retMsg'] = '連續出價 '.$_POST['price_start'].'至'.$_POST['price_stop'].'元 成功 !'.$msg;
				}
			} else {
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['total_num'] = $mk['total_num'];
					$ret['num_uniprice'] = $mk['num_uniprice'];
					$ret['lowest_uniprice'] = $mk['lowest_uniprice'];
				}
			}
		}
		// error_log("[ajax/house_product] saja : ".json_encode($ret));
		$db=null;

		echo json_encode($ret);
	}

	//檢查是屬於特定分類
	public function getProductCatetory($productid,$pcid){
		global $db, $config;
		$query = "SELECT pcid FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` where productid='{$productid}' and pcid='{$pcid}'";
		error_log($query);
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['pcid'];
        }else{
        	return "-1";
        }
		
	}
	
	//下標資格
	public function chk_saja_rule($product)	{
		global $db, $config;

		$limitid = $product['limitid'];

		$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited`
		where
			plid = '{$limitid}'
			AND switch = 'Y'
		";
		error_log("chk_saja_rule query: ".$query);
		$table = $db->getQueryRecord($query);
		$kind=$table['table']['record'][0]['kind'];
		$mvalue=$table['table']['record'][0]['mvalue'];
		$svalue=$table['table']['record'][0]['svalue'];

		$r['err'] = '';
		$r['value'] = 0;
		//賀歲處理
		$productCategory=$this->getProductCatetory($product['productid'],147);
		// error_log("[ajax/house_product] productCategory : ".json_encode($productCategory));
		
 		if ($productCategory=='147'){
			$count = $this->pay_get_product_count($this->userid,$productCategory);
			error_log("557 ".$count);
			if ($count>0){
				$r['err'] = 24;
				$r['retCode']=-100324;
				$r['retMsg']='您已在賀歲商品得過標';
			}else{
				//競標中商品同時間只能有一檔
				// $count = $this->product_bid_count($this->userid,$product['productid'],$productCategory,"",1,'Y');
				$count = $this->product_bid_count($this->userid,$product['productid'],$productCategory,"",1,'N');
				if ($count>0){
					$r['err'] = 25;
					$r['retCode']=-100325;
					$r['retMsg']='您在賀歲商品尚有其它競標中的項目';
				}
			}
		} 

		if( $kind=='small') {		//小於
		   	if($mvalue==1) {
				if ($count >= $mvalue) {
					$r['err'] = 16;
					$r['retCode']=-100216;
					$r['retMsg']='未符合下標資格: '.$table['table']['record'][0]['name'];
					$r['value'] = $mvalue;
				}
			} else {
				if ($count > $mvalue) {
					$r['err'] = 16;
					$r['retCode']=-100216;
					$r['retMsg']='未符合下標資格: '.$table['table']['record'][0]['name'];
					$r['value'] = $mvalue;
				}
			}
		} else if($kind=='big') {	//大於
			if($count < $svalue) {
				$r['err'] = 16;
				$r['retCode']=-100216;
				$r['retMsg']='未符合下標資格:'.$table['table']['record'][0]['name'];
				$r['value'] = $svalue;
			}
		}

		// 新手限定 一次一標
		if ($limitid == 7 || $limitid ==21 || $limitid ==22 ) {
			$query = "SELECT
						h.productid,
						p.name,
						h.userid,
						h.nickname
					FROM
						`{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON h.productid = p.productid
						AND p.switch = 'Y'
					WHERE
						h.switch = 'Y'
					AND p.limitid = '{$limitid}'
					AND (
						p.offtime > now()
						OR p.closed = 'N'
					)
					AND h.userid = '{$this->userid}'
					AND p.productid <> '{$product['productid']}'
					AND p.ptype = '{$product['ptype']}'
					GROUP BY
						h.productid";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])) {
				$r['err'] = 21;
				$r['retCode']=-100234;
				$r['retMsg']='已有下標其他新手限定商品';
			}
		}

		//查詢下標 狀態 AARONFU
		$bid_able = query_bid_able($this->userid);
		if($bid_able["bid_enable"] !=='Y') {
			$r['err'] = 23;
			$r['retCode']=-100236;
			$r['retMsg']='下標功能鎖定中,請洽客服';
		}

		return $r;
	}


	// 計算用戶得標次數
	// Add By Thomas 2020-01-17
	public function pay_get_product_count($userid, $pcid='', $plid='') {
		global $db, $config;
        if(empty($userid)) {
			   return false;
		}
        if(empty($plid) && empty($pcid)) {
			$query = "SELECT count(*) as count
						FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
					   WHERE `prefixid` = '{$config['default_prefix_id']}'
						 AND userid = '${userid}'
						 AND switch = 'Y' ";
		} else if($pcid>0) {
		    // 某商品分類下的商品 得標次數
			$query =  "SELECT count(*) as count
						 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
						 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						   ON pgp.productid = p.productid
						 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr
						   ON p.productid = pcr.productid
						WHERE pgp.`prefixid` = '{$config['default_prefix_id']}'
						  AND pgp.switch = 'Y'
						  AND pgp.userid = '${userid}'
						  AND pcr.pcid= '${pcid}' ";
		} else if($plid>0) {
			// 某下標限制的商品 得標次數
			$query =  "SELECT count(*) as count
						 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
						 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						   ON pgp.productid = p.productid
						WHERE pgp.`prefixid` = '{$config['default_prefix_id']}'
						  AND pgp.switch = 'Y'
						  AND pgp.userid = '${userid}'
						  AND p.limitid= '${plid}' ";
		}
		$table = $db->getQueryRecord($query);
		error_log("[ajax/house_product] pay_get_product_count(${userid},${pcid},${plid}) : ".$table['table']['record'][0]['count']);
		if (sizeof($table['table']['record'])>0){
			return $table['table']['record'][0]['count'];
		}else{
			return 0;
		}
	}

	// 計算用戶下標次數
	// Add By Thomas 2020-01-17
	// notIn時表示不含該物
	// bidstatus 表示針對特定狀態
	public function product_bid_count($userid, $productid, $pcid='', $plid='',$notIn='',$bidstatus='') {
		global $db, $config;

		    if(empty($userid) || empty($productid)) {
			   return false;
			}
			if(empty($plid) && empty($pcid)) {
			    $query = "SELECT count(*) as count
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
						   WHERE sh.`prefixid` = '{$config['default_prefix_id']}'
							 AND switch = 'Y'
							 AND userid = '${userid}'
							 AND productid = '${productid}' ";
			} else if($pcid>0) {
				// 某商品分類下的商品 下標數

				$query =  "SELECT count(*) as count
							 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
							 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
							   ON sh.productid = p.productid
							 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr
							   ON p.productid = pcr.productid
							WHERE sh.`prefixid` = '{$config['default_prefix_id']}'
							  AND sh.`userid` = '${userid}'
							  AND sh.`switch` = 'Y'
							  AND pcr.pcid= '${pcid}' ";

				if ($notIn==1) $query.=" AND p.productid<>'{$productid}' ";
				// if ($bidstatus!='') $query.=" AND (p.closed<>'".$bidstatus."' ) ";
				if ($bidstatus!='') $query.=" AND p.closed='".$bidstatus."' ";
			} else if($plid>0) {
				// 某下標限制的商品 下標數
				$query =  "SELECT count(*) as count
							 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
							 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
							   ON sh.productid = p.productid
							WHERE sh.`prefixid` = '{$config['default_prefix_id']}'
							  AND sh.`userid` = '${userid}'
							  AND sh.`switch` = 'Y'
							  AND p.`limitid`= '${plid}' ";
				if ($notIn==1) $query.=" AND p.productid<>'{$productid}' ";
				if ($bidstatus!='') $query.=" AND p.closed='".$bidstatus."' ";
			}
			$table = $db->getQueryRecord($query);
			//var_dump($query);
			error_log("[ajax/house_product] product_bid_count(${userid},${productid},${pcid},${plid}) : ".$table['table']['record'][0]['count']);

		if (sizeof($table['table']['record'])>0){
			return $table['table']['record'][0]['count'];
		}else{
			return 0;
		}
	}

	// 連續下標
	public function chk_range($product, $pay_type) {
		$time_start = microtime(true);

		global $db, $config;

		$r['err'] = '';
		// 檢查是否結標時, 不可用DB撈出的時間
		$osq = 0;
		//檢查圓夢券數量
		if ($pay_type=='dscode-h'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND feedback = 'Y' 
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY dscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
			error_log("[ajax/house_product] chk_range osq : ".$osq);
		} elseif ($pay_type=='dscode-n'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND feedback = 'N'  
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY dscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
		}
		
		if(empty($_POST['price_start']) || empty($_POST['price_stop'])) {
			//'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($_POST['price_start']) || !is_numeric($_POST['price_stop'])) {
			//'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((int)$_POST['price_start'] < (int)$product['price_limit']) {
			//'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif($product['offtime'] > 0 && ($product['offtime'] - time() < $this->range_saja_time_limit)) {
			//'超過可連續下標時間'
			$r['err'] = 7;
			$r['retCode']=-100227;
			$r['retMsg']='超過可連續下標時間!';
		} elseif((int)$_POST['price_start'] > (int)$_POST['price_stop']) {
			//'起始價格不可超過結束價格'
			$r['err'] = 9;
			$r['retCode']=-100229;
			$r['retMsg']='起始價格不可超過結束價格!';
		} elseif((int)$_POST['price_stop'] > (int)$product['retail_price']) {
			//'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif((int)$_POST['price_start'] == (int)$_POST['price_stop']) {
			//'起始價格不可超過結束價格'
			$r['err'] = 12;
			$r['retCode']=-100232;
			$r['retMsg']='出價起訖區間不可相同!';
		}

		//本次下標數
		$this->tickets = ((int)$_POST['price_stop'] - (int)$_POST['price_start']) / $this->currency_unit + 1;
		error_log("[ajax/house_product] 本次下標數：".$this->tickets);
		//單一用戶總下標次數限制檢查
		if($this->chk_user_limit((int)$product['user_limit'], $product['productid'],$pay_type,$osq)) {
			$r['err'] = 11;
			$r['retCode']=-100231;
			if((int)$product['user_limit']==0) {
  			   $r['retMsg']='本商品尚未開放競標 !';
			} else {
			   $r['retMsg']='超過可下標次數限制!';
			}

		}

		//起始價格
		$this->price = $_POST['price_start'];

		if($this->tickets > (int)$product['usereach_limit']) {
			//'超過可連續下標次數限制'
			$r['err'] = 8;
			$r['retCode']=-100228;
			if((int)$product['usereach_limit']==0) {
  			   $r['retMsg']='本商品尚未開放競標 !';
			} else {
			   $r['retMsg']='超過可連續下標次數限制 !';
			}
		}
		return $r;
	}

	// 單次下標
	public function chk_single($product,$pay_type) {
		$time_start = microtime(true);

		global $db, $config;

		//殺價記錄數量
		$this->tickets = 1;

		//殺價起始價格
		$this->price = $_POST['price'];
		$osq = 0;
		//檢查圓夢券數量
		//有返饋圓夢券
		if ($pay_type=='dscode-h'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND feedback = 'Y' 
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY dscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
		}
		//無返饋圓夢券
		if ($pay_type=='dscode-n'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND feedback = 'N'  
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY dscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
		}
		$r['err'] = '';

		if(empty($this->price)) {
			//'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($this->price)) {
			//'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((float)$this->price < (float)$product['price_limit']) {
			//'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif((float)$this->price > (float)$product['retail_price']) {
			//'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif($this->chk_user_limit((int)$product['user_limit'], $product['productid'],$pay_type,$osq)) {
			//'超過可下標次數限制'
			error_log("[ajax/house_product] 檢查結果：".$this->chk_user_limit($product['user_limit'], $product['productid'],$pay_type,$osq));
			$r['err'] = 11;
			$r['retCode']=-100231;
			if((int)$product['user_limit']==0) {
  			   $r['retMsg']='本商品尚未開放競標 !';
			} else {
			   $r['retMsg']='超過可下標次數限制!';
			}
		}

		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "單次下標 $time 秒<br>";
		return $r;
	}

	//檢查使用者下標次數限制
	public function chk_user_limit($user_limit, $pid, $pay_type, $osq) {
		$time_start = microtime(true);

		global $db, $config;
		
		// user_limit=0 就不用檢測了 必擋
		if($user_limit==0)
		   return 1;
	   
		//賀歲商品的$user_limit放大 百萬殺車的$user_limit放大
		if (($this->getProductCatetory($pid,147)>'0')||($this->getProductCatetory($pid,144)>'0')) $user_limit=999999;

		/*兩種卷合併記次*/
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$pid}'
			AND (spointid>0 or bonusid>0) 
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		$spoint_count=(int)$table['table']['record'][0]['count'];
		
		error_log("[ajax/house_product] 付費下標數：".$spoint_count);
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$pid}'
			AND spointid=0
			AND bonusid=0
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$code_limit=$user_limit;
		$table = $db->getQueryRecord($query);
		$oscode_count=(int)$table['table']['record'][0]['count'];
		error_log("[ajax/house_product] pay_type：".$pay_type);
		
		if ($pay_type=='spoint'){
			$tickets_avaiable = (int)$user_limit -$spoint_count;
			//下幣且幣的次數不夠
			error_log("[ajax/house_product] user_limit：".$tickets_avaiable." <====> tickets".$this->tickets);
			if((int)$this->tickets > (int)$tickets_avaiable) return 1;
		}elseif ($pay_type=='bonus'){
			$tickets_avaiable = (int)$user_limit -$spoint_count;
			//下幣且幣的次數不夠
			error_log("[ajax/house_product] user_limit：".$tickets_avaiable." <====> tickets".$this->tickets);
			if((int)$this->tickets > (int)$tickets_avaiable) return 1;			
		}else{
			//比如限制是 10次  已用圓夢券4次 尚有超沙7張
			if ($oscode_count==0){
				//取卷的剩餘數或下標卷次數較低者
				$oscode_limit=($osq<$user_limit)?$osq:$user_limit;
				$tickets_avaiable = (int)$oscode_limit - $oscode_count;
			}else{
				$user_limit=$user_limit-$oscode_count;
				$tickets_avaiable=($osq<$user_limit)?$osq:$user_limit;
			}
			//下卷且卷的次數不夠則看幣還有沒有剩($user_limit-$spoint_count);
			if((int)$this->tickets > $tickets_avaiable) {
				if((int)$this->tickets >($tickets_avaiable+$user_limit-$spoint_count)) return 1;
			}
		}
		return 0;
	}


	//檢查最後下標時間
	public function chk_time_limit() {
		$time_start = microtime(true);

		global $db, $config;

		$query = "
		SELECT unix_timestamp(MAX(insertt)) as insertt, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			if( (int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $this->saja_time_limit) {
				return 1;
			}
		}
		return 0;
	}

	public  function getBidWinner($productid) {
			global $db, $config;
			$ret=array(
			   'nickname'=>'',
			   'name'=>'',
			   'userid'=>'',
			   'productid'=>'',
			   'price'=>0,
			   'src_ip'=>''
			);
			$query="SELECT sh.userid, sh.productid, sh.nickname, sh.price
				   FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
				  WHERE sh.prefixid = '{$config['default_prefix_id']}'
					AND sh.productid = '{$productid}'
					AND sh.switch = 'Y'
					AND sh.type = 'bid'
					AND sh.userid IS NOT NULL
					GROUP BY sh.price
					HAVING COUNT(sh.price) = 1
					ORDER BY sh.price ASC
					LIMIT 1 ";
			$table = $db->getQueryRecord($query);

			// error_log("[ajax/house_product] sql : ".$query);

			if($table['table']['record'][0] && $table['table']['record'][0]['price']>0) {
				$ret=array();
				$ret['userid'] = $table['table']['record'][0]['userid'];
				$ret['nickname'] = $table['table']['record'][0]['nickname'];
				$ret['name'] = $table['table']['record'][0]['nickname'];
				$ret['productid'] = $table['table']['record'][0]['productid'];
				$ret['price'] = $table['table']['record'][0]['price'];
			 } else {
				$ret = false;
			}
			// error_log("[ajax/product/getBidWinner] : ".json_encode($ret));
			return $ret;
	}



	//產生下標歷史記錄
	public function mk_history($product) {
		$time_start = microtime(true);

		global $db, $config, $jpush;

		// $jp = new jpush;

		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = -2;
		$values = array();
		$ip = GetIP();
		
		if($_POST['pay_type'] == 'dscode-h') {
			/****************************
			* 使用返饋圓夢券 : 
			****************************/

			//檢查返饋圓夢券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND feedback = 'Y'
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY dscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
			error_log("[ajax/product/mk_history]: dscode-h needed ==>".round($this->tickets));
			error_log("[ajax/product/mk_history]: dscode-h available : ".$query);

			// if ($osq > $_POST['dscode_num']) {
				// $osq = $_POST['dscode_num'];
			// }
			error_log("[ajax/product/mk_history]: osq  : ".$osq);
			
			//判斷圓夢券可使用數量
			if ($osq <= $product['xcode_count']){
				$xosq = $osq;
			}else{
				$xosq = $product['xcode_count'];
			}
			
			//判斷取得每天免費可下標數
			$everyday_xcode_count = $this->everyday_xcode_count($product, round($this->tickets));
			
			if($product['everydaybid'] != 0 && $everyday_xcode_count == 0) {
				$r['err'] = 28;
				$r['retCode']=-100328;
				$r['retMsg']='已超過此商品每天免費可下標數 請用殺價幣下標!';
			}else{

				if(round($this->tickets) > $xosq) {

					$r['err'] = 19;
					$r['retCode']=-100199;
					$r['retMsg']='圓夢券不足!';
				} else {
					//使用圓夢券
					for($i = 0; $i < round($this->tickets); $i++) {
						
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}dscode`
						SET
							productid = '{$product['productid']}',
							used = 'Y',
							used_time = NOW()
						WHERE
							`prefixid` = '{$config['default_prefix_id']}'
							AND dscodeid = '{$table['table']['record'][$i]['dscodeid']}'
							AND switch = 'Y'
							AND feedback = 'Y'
						";
						$res = $db->query($query);

						//殺價下標歷史記錄（對應下標記錄與 oscodeid）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'0',
							'0',
							'0',
							'0',
							'{$table['table']['record'][$i]['dscodeid']}',
							'".($this->price + $i * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = -1;
						$r['retCode']=-1;
						$r['retMsg']='下標失敗!';
					}
				}
			
			}
			
		} else {
			/****************************
			* 使用不返饋圓夢券 : 
			****************************/

			//檢查不返饋圓夢券數量
			//檢查返饋圓夢券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND feedback = 'N'
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY dscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
			error_log("[ajax/product/mk_history]: dscode-n needed ==>".round($this->tickets));
			error_log("[ajax/product/mk_history]: dscode-n available : ".$query);

			// if ($osq > $_POST['dscode_num2']) {
				// $osq = $_POST['dscode_num2'];
			// }
			error_log("[ajax/product/mk_history]: osq  : ".$osq);
			
			//判斷圓夢券可使用數量
			if ($osq <= $product['xcode_count']){
				$xosq = $osq;
			}else{
				$xosq = $product['xcode_count'];
			}
			
			//判斷取得每天免費可下標數
			$everyday_xcode_count = $this->everyday_xcode_count($product, round($this->tickets));
			
			if($product['everydaybid'] != 0 && $everyday_xcode_count == 0) {
				$r['err'] = 28;
				$r['retCode']=-100328;
				$r['retMsg']='已超過此商品每天免費可下標數 請用殺價幣下標!';
			}else{

				if(round($this->tickets) > $xosq) {

					$r['err'] = 19;
					$r['retCode']=-100199;
					$r['retMsg']='圓夢券不足!';
				} else {
					//使用圓夢券
					for($i = 0; $i < round($this->tickets); $i++) {
						
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}dscode`
						SET
							productid = '{$product['productid']}',
							used = 'Y',
							used_time = NOW()
						WHERE
							`prefixid` = '{$config['default_prefix_id']}'
							AND dscodeid = '{$table['table']['record'][$i]['dscodeid']}'
							AND switch = 'Y'
							AND feedback = 'N'
						";
						$res = $db->query($query);

						//殺價下標歷史記錄（對應下標記錄與 oscodeid）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'0',
							'0',
							'0',
							'0',
							'{$table['table']['record'][$i]['dscodeid']}',
							'".($this->price + $i * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = -1;
						$r['retCode']=-1;
						$r['retMsg']='下標失敗!';
					}
				}
			
			}
		}


		if(empty($r['err']) && !empty($values) ) {
			// 取得目前winner
			// 20190420 暫時移除 20180816回復

			$arr_ori_winner=array();
			$arr_ori_winner=$this->getBidWinner($product['productid']);
			
			$db->query('start transaction');
			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
			(
				prefixid,
				productid,
				userid,
				nickname,
				spointid,
				scodeid,
				sgiftid,
				oscodeid,
				bonusid,
				dscodeid,
				price,
				bid_total,
				insertt,
				src_ip
			) VALUES
			".implode(',', $values)."
			";
 
			$res = $db->query($query);
			$affected_num = $db->_con->affected_rows;
			
			if($affected_num == round($this->tickets)){		//更新比數與兌換數量符合
				$db->query('COMMIT');				
			}else{
				$db->query('ROLLBACK');
				
				if($_POST['pay_type'] == 'dscode-h'){
					
					for($i = 0; $i < round($this->tickets); $i++) {
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}dscode`
						SET
							used = 'N',
							used_time = '0000-00-00 00:00:00'
						WHERE
							`prefixid` = '{$config['default_prefix_id']}'
							AND dscodeid = '{$table['table']['record'][$i]['dscodeid']}'
							AND userid = '{$this->userid}' 
							AND switch = 'Y'
							AND feedback = 'Y'
						";
						$res = $db->query($query);
					}
					
				} else {	
				
					for($i = 0; $i < round($this->tickets); $i++) {
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}dscode`
						SET
							used = 'N',
							used_time = '0000-00-00 00:00:00'
						WHERE
							`prefixid` = '{$config['default_prefix_id']}'
							AND dscodeid = '{$table['table']['record'][$i]['dscodeid']}'
							AND userid = '{$this->userid}' 
							AND switch = 'Y'
							AND feedback = 'Y'
						";
						$res = $db->query($query);
					}
					
				}
			}
			
			error_log("[ajax/house_product]:".$query."-->".$res);
			
			// $r['rank'] = $last_rank;
			

			// 20190420 加入以redis sortedset 判斷的機制
			// 非閃殺商品下標, 結標一分鐘以前, 單次下標將提示順位
			$updWinner=false;
			// if($_POST['type'] == 'single' && $product['is_flash']!='Y') {
			if($_POST['type'] == 'single') {
				// 寫入redis, 並使用 SortedSet來判斷順位
				$cache = getBidCache();
				// 如果設定要用redis判斷 且有redis -> 用redis 判斷
				if($config["rank_bid_by_redis"] && $cache) {
				   $str_price = str_pad($this->price,12,"0",STR_PAD_LEFT);
				   $dup_cnt = $cache->zIncrBy("PROD:RANK:".$product['productid'],1,$str_price) ;
				   if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
				  		// 檢查重複
						if($dup_cnt>2) {
							// 2筆以上重複出價  不用更新得標者
							$r['rank']=-1;
							$updWinner=false;
						} else if($dup_cnt==2) {
							// 剛好有2筆重複  可能是把得標者幹掉 要更新得標者
							$r['rank']=-1;
							$updWinner=true;
						} else if($dup_cnt==1) {
							// zRank直接回傳該出價的排名 以0為第1,
							// 所以可以直接表示前面有幾個唯一的出價
							// $updWinner=true;
							$r['rank']= $cache->zRank("PROD:RANK:".$product['productid'],$str_price);
							if($r['rank']<2)
							   $updWinner=true;
						}
						error_log("Check rank From redis=>".$r['rank']);
					} else {
						// 不提示
						$updWinner=false;
						$r['total_num']=-1;
					}
				} else {
					// 沒有redis或未設定由redis判斷 -> 就走原來程序進DB撈
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						// 檢查重複
						$query= "SELECT count(userid) as cnt
								   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
								  WHERE productid='{$product['productid']}'
									AND price={$_POST['price']}
									AND switch='Y' ";

						$table = $db->getQueryRecord($query);
						$dup_cnt=$table['table']['record'][0]['cnt'];
						if($dup_cnt>2) {
							// 2筆以上的重複出價
							$r['rank']=-1;
							$updWinner = false;
						} else if($dup_cnt==2) {
							// 剛好2筆重複出價
							$r['rank']=-1;
							$updWinner = true;
						} else if($dup_cnt<2) {
							// 無重複出價
							$query1 = "SELECT count(*) as cnt FROM  `saja_view`.`v_unique_price`
									   WHERE productid='{$product['productid']}' AND price<{$_POST['price']} ";
							$table1 = $db->getQueryRecord($query1);
							$r['rank'] =$table1['table']['record'][0]['cnt'];
							if($r['rank']<2)
							   $updWinner = true;
							error_log("[ajax/house_product] DB rank : ".$r['rank']);
						}
						error_log("Check rank From DB=>".$r['rank']);
					} else {
					// 不提示
						$r['total_num']=-1;
						$r['rank']=-2;
					}
				}
			}


			// 非閃殺商品下標, 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出價個數, 及該次下標的唯一最低價
			// if($product['is_flash']!='Y') {
				if($_POST['type'] == 'range') {
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						$r['total_num']=$this->tickets;
						// 顯示提示
						// 取得該區間內唯一出價的個數
						$cache = getBidCache();
						$start_price = $_POST['price_start'];
						$end_price = $_POST['price_stop'];
						// 如果設定要用redis判斷 且有redis -> 用redis 判斷
						if($config["rank_bid_by_redis"] && $cache) {
							$r['num_uniprice'] = 0;
							$r['lowest_uniprice'] = 0;
							$updWinner=false;
							for($p=$start_price ; $p<=$end_price ; ++$p) {
								$str_price = str_pad($p,12,"0",STR_PAD_LEFT);
								$dup_cnt = $cache->zIncrBy("PROD:RANK:".$product['productid'],1,$str_price);
								if($dup_cnt<=2) {
								   $updWinner=true;
								}
								if($dup_cnt==1) {
								   // 增加唯一價的個數
								   $r['num_uniprice']++;
								   // 把第一個碰到的價格記下來(就是該批次內最低的出價)
								   if($r['lowest_uniprice']==0) {
									  $r['lowest_uniprice']=$p;
								   }
								}
							}
						} else {
							// 沒有redis或沒有指定要用redis計算 -> 走原本程序進DB撈
							if($this->tickets>1) {
								$query="SELECT * FROM saja_shop.saja_history sh JOIN saja_view.v_unique_price up
										   ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y'
										WHERE sh.switch='Y'
										  AND sh.productid='{$product['productid']}'
										  AND sh.price between {$_POST['price_start']} AND {$_POST['price_stop']}
										  AND sh.userid='{$this->userid}' ORDER BY sh.price asc ";

								// error_log("[ajax/house_product]range chk: ".$query);
								$table = $db->getQueryRecord($query);
								// error_log("[ajax/house_product]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);

								if(empty($table['table']['record'])) {
								   // 無唯一出價
								   $updWinner=false;
								   $r['num_uniprice'] = 0;
								   $r['lowest_uniprice'] = 0;
								} else {
								   // 有唯一出價
								   $updWinner=true;
								   $r['num_uniprice'] = count($table['table']['record']);
								   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
								}
							} else {
								// 不提示
								$r['total_num']=-1;
								$r['rank']=-2;
							}
						}
					} else {
						// 不提示
						$r['total_num']=-1;
						$r['rank']=-2;
					}
				}
			// }

			// 未重複或剛好2筆重複出價者  需更新得標者
			if($updWinner) {
				error_log("[ajax/house_product] updWinner : ".$updWinner);
				$arr_new_winner=array();
				$status = 1; //websocket 狀態碼(1:更新得標者, 2:無人得標)
				try {
					$arr_new_winner=$this->getBidWinner($product['productid']);

					$query ="SELECT thumbnail_file, thumbnail_url
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					WHERE prefixid = '{$config['default_prefix_id']}'
					  AND userid = '{$arr_new_winner['userid']}'
					  AND switch = 'Y'
					";
					$table = $db->getQueryRecord($query);
					$r['thumbnail_file'] = $table['table']['record'][0]['thumbnail_file'];
					$r['thumbnail_url'] = $table['table']['record'][0]['thumbnail_url'];

					if($arr_new_winner['src_ip']) {
						$r['src_ip']=maskIP($arr_new_winner['src_ip']);
						$r['comefrom']=$r['src_ip'];
					}
					// error_log("[ajax/product/mk_history] new_winner :".json_encode($arr_new_winner));
					$r['winner']=$arr_new_winner['nickname'];
					$winner_name = $r['winner'];
					if(empty($r['winner'])) {
					   $r['winner']="";
					   $winner_name = "目前無人得標";
					   $status = 2; //websocket 狀態碼(1:更新得標者, 2:無人得標)
					}

					// $thumbnail_file = (empty($r['thumbnail_file'])) "":$r['thumbnail_file'];//空圖片 socket給空字串
					// $thumbnail_file = null;

					$r_bidded['name'] = $r['winner'];
					$r_bidded['status'] = $status;
					$r_bidded['thumbnail_file'] = $r['thumbnail_file'];
					$r_bidded['thumbnail_url'] = $r['thumbnail_url'];

					// 寫入redis
					$redis=getRedis('','','');
					if($redis) {
					   $redis->set("PROD:".$product['productid'].":BIDDER",json_encode($r_bidded));
					}
					// websocket更新得標人員
					$ts = time();
					$client = new WebSocket\Client("wss://ws.saja.com.tw:3334");
					$ary_bidder = array('actid'		=> 'BID_WINNER',
										'name' 		=> $winner_name,
										'productid' => "{$product['productid']}",
										'ts'		=> $ts,
										'status'	=> $status,
										'sign'		=> MD5($ts."|sjW333-_@"),
										'thumbnail_file' => $thumbnail_file ,
										'thumbnail_url'	=> $r['thumbnail_url']
								);
					$client->send(json_encode($ary_bidder));

					$diff_sec = $product['offtime']-time(); //大於30分鐘才執行推播
					if ($diff_sec>180) {
						$ori_userid = $arr_ori_winner['userid'];
						$new_userid = (empty($arr_new_winner['userid']))?"":$arr_new_winner['userid'];
						$post_array = array(
								"ori_userid" => $ori_userid,
								"new_userid" => $new_userid,
								"productid" => $product['productid']
							);
						// error_log("[ajax/house_product] push/after_saja : ".json_encode($post_array));
						// 更新得標者推播 20190819
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_URL, BASE_URL.APP_DIR."/push/after_saja");
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_array));
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						$result = curl_exec($ch);
						curl_close($ch);
					}

				} catch(Exception $e) {
					error_log("[ajax/house_product] updWinner exception : ".$e->getMessage());
					// 不提示
					// $r['total_num']=-1;
					// $r['rank']=-2;
				}
			}
		}
		error_log("[ajax/mk_history] r : ".json_encode($r));
		return $r;
	}

	public function saja_spoint_free_history($saja_fee, $spointid = 0, $productid = 0){

		global $db, $config;
		//確認是否有免費幣尚未用完
		$query="SELECT sum(free_amount) as free_amount
				FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint_free_history`
				WHERE userid = '{$this->userid}'
				AND amount_type ='1'
				AND switch =  'Y' ";

		// error_log("[ajax/house_product]range chk: ".$query);
		$chk = $db->getQueryRecord($query);

		//有未使用完的免費幣
		if($chk['table']['record'][0]['free_amount'] > 0){

			//免費幣餘額 大於等於 總下標費用，該筆金額全部視為使用免費幣
			if($chk['table']['record'][0]['free_amount'] >= $saja_fee){
				$free_amount = $saja_fee;
			}else{
				//免費幣餘額 小於 總下標費用，該筆金額部分視為免費幣
				$free_amount = $chk['table']['record'][0]['free_amount'];
			}

			//新增免費幣使用紀錄
			$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint_free_history`
			SET
				`userid`='{$this->userid}',
				`behav` = 'user_saja',
				`amount_type` = '1',
				`free_amount` = '".- $free_amount."',
				`total_amount` = '".- $saja_fee."',
				`productid` = '{$productid}',
				`spointid` = '{$spointid}',
				`insertt`=NOW()
			";
			$res = $db->query($query);

		}

	}
	
	//每天付費可下標數
	public function everyday_xspoint_count($product, $bid_count) {
		global $db, $config;
		if($product['everydaybid']!=0 ) {
			$day = date("Y-m-d"); 
			$query = "
			SELECT count(*) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND (spointid!=0 or bonusid!=0)
				AND userid = '{$this->userid}'
				AND switch = 'Y'
				AND insertt between '{$day} 00:00:00' AND '{$day} 23:59:59'
			";
			$table = $db->getQueryRecord($query);
			$xspoint_count=(int)$table['table']['record'][0]['count'];
			
			if ($_POST['pay_type'] == 'spoint') {
				error_log("[ajax/product/everyday_xcode_count] xspoint_count: ".$xspoint_count." everydaybid => ".$product['everydaybid']." bid_count =>".$bid_count);

				if (($xspoint_count >= $product['everydaybid']) || ($bid_count > $product['everydaybid']) || ($bid_count > ($product['everydaybid']-$xspoint_count)) ) {
					return 0;
				} else {
					return 1;
				}
			}else{
				return 0;
			}
			
		}else{ 
			return 1;
		}
	}	
	
	//每天免費可下標數
	public function everyday_xcode_count($product, $bid_count) {
		global $db, $config;
		if($product['freepaybid']!=0) {
			$day = date("Y-m-d");
			$query = "
			SELECT count(*) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}' 
				AND productid = '{$product['productid']}' 
				AND spointid=0 
				AND sgiftid=0 
				AND userid = '{$this->userid}' 
				AND switch = 'Y' 
				AND insertt between '{$day} 00:00:00' AND '{$day} 23:59:59'
			";
			$table = $db->getQueryRecord($query);
			$xscode_count=(int)$table['table']['record'][0]['count'];
		
			if ($_POST['pay_type'] == 'oscode' || $_POST['pay_type'] == 'scode') {
				error_log("[ajax/product/everyday_xcode_count] xscode_count: ".$xscode_count." freepaybid => ".$product['freepaybid']." bid_count =>".$bid_count);
				if ($xscode_count >= $product['freepaybid'] || ($bid_count > $product['freepaybid']) || ($bid_count > ($product['freepaybid']-$xscode_count)) ) {
					return 0;
				} else {
					return 1;
				}
			}else{
				return 0;
			}
			
		} else { 
			return 1;
		}
	}
}
?>