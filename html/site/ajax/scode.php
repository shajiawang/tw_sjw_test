<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");
include_once(BASE_DIR ."/model/oscode.php");
//$app = new AppIni;

//取得google 防機器人測試結果
function get_google_human_test_result($recaptcha_response){

  //echo $recaptcha_response."</br>";

  //傳送google驗證網址
	$URL= 'https://www.google.com/recaptcha/api/siteverify';

	//google驗證secret
  //$secret = '6LcECYsUAAAAAILe8_8ofC0Pzyswf2sAWHba2KAD';
	$secret = '6LfBr4sUAAAAAC4-eIgDi_XnxiyrrE0khJEhrFcS';

	//設定驗證傳送參數
	$data = array('secret' => $secret, 'response' => $recaptcha_response);

  //curl初始化設定
	$curl = curl_init($URL);
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_POST, true);
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($curl, CURLOPT_POSTFIELDS, $data);

	//取得curl回應結果
	$curl_result = curl_exec($curl);
	//echo $curl_result;

	//結束curl
	curl_close($curl);

	//轉換回傳json to array
	$result_arr = json_decode($curl_result, true);

	//轉換回傳success參數 boolean to string
	if($result_arr['success']){
		$success = 'true';
	}else{
		$success = 'false';
	}

	//echo "success :".$success;

  return $success;
}

$c = new s_code;

$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
	foreach($_POST2 as $k=> $v) {
		$_POST[$k]=$v;
		if($_POST['client']['json']) {
			$_POST['json']=$_POST['client']['json'];
		}
		if($_POST['client']['auth_id']) {
			$_POST['userid']=$_POST['client']['auth_id'];
		}
	}
}


// 1. 要接收 $json,
//$ret=null;
//回傳:
$ret['status']=0;
$ret['retCode']=1;
$ret['retMsg']="OK";
$ret['retType']="LIST";
$ret['action_list']=array(
		"type"=>5,
		"page"=>9,
		"msg"=>"NULL"
	);
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json'];

//if(empty($_SESSION['auth_id']) && empty($_POST['client']['auth_id'])) {} //'請先登入會員帳號'
if(empty($_SESSION['auth_id']) && empty($_POST['auth_id'])) { //'請先登入會員帳號'

	if($json=='Y') {
		//$ret['retCode']=-1;
		//$ret['retMsg']='請先登入會員 !!';
		$ret['status'] = -1;
		$ret['action_list']["msg"]="請先登入會員 !!";
	} else {
	   $ret['status'] = 1;
	}

	echo json_encode($ret);
	exit;
} else {

	if($_POST['type']=='act_osc') {
		$c->act_osc();
	} else {
		
		if($json !=='Y') {
			//取得google防機器人測試結果token
			$recaptcha_response	= htmlspecialchars($_POST['recaptcha_response']);

			//取得google 防機器人測試結果
			$google_human_test_result = get_google_human_test_result($recaptcha_response);

			//google 防機器人測試結果處理
			if($google_human_test_result != 'true') {//驗證失敗
				//if($json=='Y') {
				//	$ret['retCode']=-6;
				//	$ret['retMsg']='請重新驗證是否為機器人 !!';
				//} else {
				$ret['status'] = -6;
				//}
				echo json_encode($ret);
				exit;
			}
		}
		$c->home();
	}
}

class s_code {
	public $str;
	public $ret = array(
		'retCode'=>1,
		'retMsg'=>"OK",
		'retType'=>"LIST",
		'action_list'=>array(
				"type"=>5,
				"page"=>9,
				"msg"=>"NULL"
			)
		);

	public function valid_tk() {
		$this->ret['status'] = 1;
		
		if($_REQUEST['saja_id']=="dev"){
		} else {
			if (MD5($this->auth_id .'|'. $_REQUEST['ts']) !== $_REQUEST['tk']){
			
				//回傳:
				$this->ret['status'] = -2;
				$this->ret['action_list']["page"]='0';
				$this->ret['action_list']["msg"]="驗證碼錯誤 !";
			}
		}
		
		return $this->ret;
	}
	
	//S碼激活
	public function home() {
		global $db, $config, $user, $scodeModel;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$scodeModel = new ScodeModel;
		$this->str = new convertString();
		$user = new UserModel;
		
		$ret = $this->ret;
		$ret['status'] = 0;
		
		$this->auth_id = empty($_REQUEST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_REQUEST['auth_id']);
		$scsn = (empty($_POST['scsn'])) ? trim($_GET['scsn']) : trim($_POST['scsn']);
		$scpw = (empty($_POST['scpw'])) ? trim($_GET['scpw']) : trim($_POST['scpw']);
		
		//資安驗證 
		if($json=='Y'){
			$ret_tk = $this->valid_tk();
		} else {
			$ret_tk['status'] = 1;
		}
		
		//檢查用戶是否通過手機驗證
		$smsArr = $user->validSMSAuth($this->auth_id);
		
		
		if($ret_tk['status'] !==1){
			$ret = $ret_tk;
		}
		else if(empty($smsArr) || $smsArr["verified"] !=='Y') {
			$ret['status'] = -3;
			$ret['action_list']["msg"]="用戶手機未驗證 !";
		}
		else if (empty($scsn) || empty($scpw)) {
			//S碼序號、S碼密碼不能空白
			$ret['status'] = -4;
			//$ret['retCode']=-2;
			//$ret['retMsg']='序號或密碼錯誤 !!';
			$ret['action_list']["page"]='0';
			$ret['action_list']["msg"]="序號或密碼不能空白 !!";
		} 
		else 
		{
			$query = "SELECT *, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_item`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND `serial` = '{$scsn}'
				AND `switch` = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$scode_item = isset($table['table']['record'][0]) ? $table['table']['record'][0] : '';

			if(empty($scode_item)) {
				//'不存在'
				$ret['status'] = -5;
				//$ret['retCode']=-3;
				//$ret['retMsg']='序號或密碼錯誤 !!';
				$ret['action_list']["page"]='0';
				$ret['action_list']["msg"]="序號或密碼不存在 !!";
			} elseif($scpw !==$scode_item['pwd'] ) {
				//'密碼錯誤'
				$ret['status'] = -7;
				//$ret['retCode']=-4;
				//$ret['retMsg']='序號或密碼錯誤 !!';
				$ret['action_list']["page"]='0';
				$ret['action_list']["msg"]="序號或密碼錯誤 !!";
			} elseif($scode_item['verified'] == 'Y') {
				// 已激活
				$ret['status'] = -8;
				//$ret['retCode']=-5;
				//$ret['retMsg']='此組序號已被使用 !!';
				$ret['action_list']["page"]='0';
				$ret['action_list']["msg"]="此組序號已被使用 !!";
			} elseif($scode_item['offtime'] < $scode_item['now']) {
				//超時不可激活
				$ret['status'] = -9;
				//$ret['retCode']=-7;
				//$ret['retMsg']='此組序號已過期 !!';
				$ret['action_list']["page"]='0';
				$ret['action_list']["msg"]="此組序號已過期 !!";
			}
		}

		if(empty($ret['status'])) 
		{
			//S碼激活
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_item`
			SET `verified`='Y', userid='{$this->auth_id}'
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND `serial` = '{$scsn}'
			";
			$db->query($query);
			
			// 判斷`serial`格式, 是否為"有序號殺價券"
			$s1 = strchr($scsn, "%");
			if($s1){
				$ret = $this->active_osc($scsn, $scode_item);
			} else {

				//實體S碼序號
				$scode_promote['spid'] = $scode_item['spid'];
				$scode_promote['behav'] = 'e';
				$scode_promote['promote_amount'] = 1;
				$scode_promote['batch'] = $scode_item['batch'];
				$scode_promote['num'] = $scode_item['amount'];

				//插入S碼收取記錄
				$scodeModel->insert_scode($scode_promote, $this->auth_id, $scsn);
				
				//回傳:
				$ret['status'] = 200;
				//$ret['retCode']=200;
				//$ret['retMsg']='激活成功 !!';
				$ret['action_list']["msg"]="序號 ". $scsn ." 成功開通";
			}
			
		}
		
		if ($json=='Y') {
			$ret['retType'] = "MSG";
			unset($ret['status']);
		}
		echo json_encode($ret);
	}
	
	//激活 特殊有序號殺價券
	public function active_osc($scsn, $scode_item)
	{
		global $db, $config;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$oscModel = new OscodeModel;
		$this->str = new convertString();

		$ret['status'] = 0;
		
		$query = "SELECT *
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		WHERE `prefixid` = '{$config['default_prefix_id']}'
			AND `serial` = '{$scsn}'
			AND `switch` = 'Y'
		";
		$table_osc = $db->getQueryRecord($query);
		$osc_item = isset($table_osc['table']['record'][0]) ? $table_osc['table']['record'][0] : '';

		if(empty($osc_item)) {
			//'不存在'
			$ret['status'] = 0;
		} elseif($osc_item['verified'] == 'Y') {
			//已激活
			$ret['status'] = 1001;
			$ret['action_list']["page"]='0';
			$ret['action_list']["msg"]="此組序號已開通 !!";
		}

		if(empty($ret['status'])) 
		{
			//特定 OSCode 激活
			
			//券數量
			$oscode_cnt = $scode_item['amount'];
			
			//商品代碼
			$_scsn = substr($scsn, 2);
			$ary_productid = explode("N", $_scsn); 
			$productid = $ary_productid[0];
			
			if($scode_item['spid']=='9815' && $today >= '2020-01-01 02:33:00' ){
				$productid = '97086';
			}
			// Add By Thomas 2020-01-25 23:53:00 Mazda結標後 領券的都改為Mini Cooper的殺價券
			if($scode_item['spid']=='9815' && $today >= '2020-01-25 23:53:00' ){
				$productid = '97083';
			}
			
			$remark = 'batch:'. $scode_item['batch'];
			$offtime = date('Y-m-d H:i:s', $scode_item['offtime']);
			
			for ($i=0; $i < $oscode_cnt; $i++) 
			{ 
				//新增紀錄
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
					SET `prefixid`='saja',
						`userid`='{$this->auth_id}',
						`productid`='{$productid}',
						`spid`='{$scode_item['spid']}',
						`behav`='e',
						`used`='N',
						`used_time`='0000-00-00 00:00:00',
						`offtime`='{$offtime}',
						`amount`=1,
						`serial`='{$scsn}',
						`verified`='Y',
						`switch`='Y',
						`seq`=0,
						`remark`='{$remark}',
						`insertt`=NOW() ";
				
				$db->query($query);
				$oscode = $db->_con->insert_id;
			}

				//新增 `scode_promote` 紀錄
				$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
				SET
				   `scode_sum`=`scode_sum` + 1,
				   `amount`=`amount` + 1
				WHERE `prefixid` = '{$config['default_prefix_id']}'
					AND spid = '{$scode_item['spid']}'
				";
				$db->query($query);
		
			//回傳:
			$ret['status'] = 200;
			$ret['action_list']["msg"]="序號 ". $scsn ." 成功開通";
		}

		return $ret;
	}
	

	//限定S碼激活
	public function act_osc()
	{
		global $db, $config;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$oscModel = new OscodeModel;
		$this->str = new convertString();

		$ret['status'] = 0;

		if(empty($_POST['scsn']) || empty($_POST['scpw'])) {
			//S碼序號、S碼密碼不能空白
			$ret['status'] = 2;
		} else {
			$query = "SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND serial = '{$_POST['scsn']}'
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$scode_item = isset($table['table']['record'][0]) ? $table['table']['record'][0] : '';

			if(empty($scode_item)) {
				//'不存在'
				$ret['status'] = 3;
			} elseif($_POST['scpw'] !==$scode_item['pwd']) {
				//'密碼錯誤'
				$ret['status'] = 4;
			} elseif($scode_item['verified'] == 'Y') {
				//已激活
				$ret['status'] = 5;
			}
		}

		if(empty($ret['status'])) {
			//限定S碼激活
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			SET `verified`='Y', userid='{$_SESSION['auth_id']}'
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND serial = '{$_POST['scsn']}'
			";
			$db->query($query);

			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
			SET
			   `scode_sum`=`scode_sum` + 1,
			   `amount`=`amount` + 1
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND spid = '{$scode_item['spid']}'
			";
			$db->query($query);

			//回傳:
			$ret['status'] = 200;
		}

		echo json_encode($ret);
	}
}
?>