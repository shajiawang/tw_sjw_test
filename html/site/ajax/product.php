<?php
session_start();

$time_start = microtime(true);

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
   // error_log($k."=>".$v);
	foreach($_POST2 as $k=> $v) {
		$_POST[$k]=$v;
		if($_POST['json']) {
			$_POST['json']=$_POST['json'];
		}
		if($_POST['auth_id']) {
			$_POST['userid']=$_POST['auth_id'];
		}
	}
}

$ret=null;
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json'];
error_log("[ajax/product] post bid data : ".json_encode($_POST));
error_log("[ajax/product] get bid data : ".json_encode($_GET));

if(empty($_SESSION['auth_id']) && empty($_POST['auth_id']) && empty($_POST['root'])) { //'請先登入會員帳號'

	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';

	} else {

	   $ret['status'] = 1;
	}

	echo json_encode($ret);
	exit;

} else {

	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	// include_once(LIB_DIR ."/jpush.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");
	include_once(LIB_DIR ."/ini.php");
	include_once(BASE_DIR ."/model/user.php");

	require(LIB_DIR."/vendor/autoload.php"); // 載入wss client 套件(composer)

	// include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");
	// require_once("/var/www/lib/vendor/autoload.php");
	//$app = new AppIni;

	// 延遲一點時間, 0.03 ~ 0.15 秒不等
	$sleep = getRandomNum()%5+1;
	// error_log("sleep: ".$sleep*500000);
	usleep($sleep*30000);


	$c = new ProductBid;
	$c->saja();
}

// use WebSocket\Client;

class ProductBid {
	public $userid = '';
	public $countryid = 1;//國家ID
	public $currency_unit = 1;//貨幣最小單位
	public $last_rank = 11;//預設順位提示（順位排名最後一位）
	public $saja_time_limit = 0;//每次下標時間間隔限制
	// public $display_rank_time_limit = 60;//（秒）下標後離結標還有n秒才顯示順位
	public $display_rank_time_limit = 0;//（秒）下標後離結標還有n秒才顯示順位
	public $range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標
	public $tickets = 1;
	public $price;
	public $username='';

	//下標
	public function saja() {
		$time_start = microtime(true);

		global $db, $config, $router, $user;

		// 初始化資料庫連結介面
		// if($db) {
		// error_log("[ajax/product] connect DB ...");
		// $db = new mysql($config["db"]);
		  // $db->connect();
		// }
        
		$JSON=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$os_type=(empty($_POST['os_type'])) ? htmlspecialchars($_GET['os_type']) : htmlspecialchars($_POST['os_type']);

        $user = new UserModel;
		// $router = new Router();
		login_required();

		if ($JSON == 'Y') {
			$this->userid = htmlspecialchars($_POST['auth_id']);
			$this->username=  htmlspecialchars($_POST['auth_nickname']);
		}else{
			$this->userid = $_SESSION['auth_id'];
			$this->username= $_SESSION['user']['profile']['nickname'];
		}


		if(empty($this->userid) && $_POST['root'] == 'weixin' && !empty($_POST['userid'])) {
			$this->userid = $_POST['userid'];
		}

		// 如果沒有用戶編號 不給下標
		if(empty($this->userid)) {
		   if($JSON=='Y') {
			  echo json_encode(getRetJSONArray(-1,'NO_USER_DATA','MSG'));
		   } else {
			  // ToDo Web Error Page
		   }
		   return ;
		}
		
		$user_profile = $user->get_user_profile($this->userid);	
		// error_log("[ajax/product/saja] user_profile : ".json_encode($user_profile));
		 
		// 如果沒有商品編號  不給下標
		if(empty($_POST['productid'])) {
		   if($JSON=='Y') {
			  echo json_encode(getRetJSONArray(-3,'NO_PRODUCT_DATA','MSG'));
		   } else {
			  // ToDo Web Error Page
		   }
		   return ;
		}

        /*
		if($this->userid != '1705' && $_POST['productid'] == '147292') {
			if($JSON=='Y') {
				 echo json_encode(getRetJSONArray(-3,"無此商品資料 !",'MSG'));
			} else {
				 // ToDo Web Error Page
			}	
			return;	
		}
		*/

		// 新版驗證簽名 20191021
		// update By Thomas 20191030
		$uts = (empty($_POST['uts'])) ? htmlspecialchars($_GET['uts']) : htmlspecialchars($_POST['uts']);
		$first_price  = (empty($_POST['first_price'])) ? htmlspecialchars($_GET['first_price']) : htmlspecialchars($_POST['first_price']);
		$sign  = (empty($_POST['sign'])) ? htmlspecialchars($_GET['sign']) : htmlspecialchars($_POST['sign']);
		$mysign = MD5($this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts.'|'.'sjW333-_@');
		error_log("[ajax/product] APP sign: ".$sign);
		error_log("[ajax/product] SERVER sign decode: ".$this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts);
		error_log("[ajax/product] SERVER sign : ".$mysign);
		
		// APP 才驗簽
		if($JSON=='Y') {
			if (empty($sign) || empty($mysign) || $sign!=$mysign) {
				error_log("[ajax/product] : Sign Check Failed !!");
				echo json_encode(getRetJSONArray(-20,'下標驗簽失敗','MSG'));
				exit();
			}
		}

		// 如果沒有用戶暱稱, 不給下標
/*         if(empty($this->username)) {
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-4,'NO_USER_NICKNAME','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		} */

		$db = new mysql(getDbConfig());
		$product = "";

		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['status'] = 0;
		$ret['winner'] = '';
		$ret['rank'] = '';
		$ret['value'] = 0;

		// 先從Cache中找商品資料
		$cache = getRedis('','');
		if($cache) {
		   $jsonStr = $cache->get('getProductById:'.$_POST['productid'].'|Y|Y');
		   if($jsonStr) {
			  $product = json_decode($jsonStr,TRUE);
		   }
		}
		// 如果Cache中沒有資料  則撈DB
		if(empty($product) || !is_array($product) || empty($product['productid'])) {
			//讀取商品資料
			$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			WHERE  p.prefixid = '{$config['default_prefix_id']}'
			  AND p.productid = '{$_POST['productid']}'
			  AND p.switch = 'Y'
			LIMIT 1	";
			$table = $db->getQueryRecord($query);
			unset($table['table']['record'][0]['description']);
			$product = $table['table']['record'][0];
		}

		if(!is_array($product) || empty($product['productid'])) {
			if($JSON=='Y') {
			  echo json_encode(getRetJSONArray(-5,'NO_PRODUCT_DATA','MSG'));
		   } else {
			  // ToDo Web Error Page
		   }
		   return ;
		}

        
		if($product['ordertype']=='3') {
			// Add By Thomas 20201221
			// Jeff 2020/12/21 指定下列userid不能標票券類商品
	        error_log("[ajax/product.php] userid 1 : ".$userid." bid for ordertype : ".$product['ordertype']);
			error_log("[ajax/product.php] userid 2 : ".$this->userid." bid for ordertype : ".$product['ordertype']);
			$arr_black_list=array(
			    "129294"=>"X", 
			    "469138"=>"X",
				"188838"=>"X",
				"468985"=>"X",
				"469018"=>"X",
				"469009"=>"X",
				"105332"=>"X");
			/*
			if (array_key_exists($this->userid, $arr_black_list)) {
				echo json_encode(getRetJSONArray(-6,'無法下標 !','MSG'));
				return;
			}*/
		} else if($product['is_flash']=='Y' || $product['ordertype']=='2') {
			// 閃殺/鯊魚點商品 不檢查收件人地址
		} else if($product['is_big']=='Y' || $product['ptype']=='1') {
			// Add By Thomas 2020/02/05 (非閃殺及鯊魚點出貨)圓夢及大檔商品 須先填妥收件人資訊才能下標
			/*
			$query ="SELECT *
			          FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			         WHERE prefixid = '{$config['default_prefix_id']}'
			           AND userid = '{$this->userid}'
			           AND switch = 'Y'	";
			$table = $db->getQueryRecord($query); 		
			if(!empty($table['table']['record'][0])) {
				$user_profile = $table['table']['record'][0];
			}
			*/
            
            // $user_profile = $user->get_user_profile($this->userid);			
			if($user_profile && $user_profile['userid']>0) {	
			   // error_log("[ajax/product/saja] user_profile:".json_encode($user_profile));
			} else {
			   if($JSON=='Y') {
				  echo json_encode(getRetJSONArray(-2,'NO_USER_DATA','MSG'));
			   } else {
				  // ToDo Web Error Page
			   }
			   return ;
			}
				
			error_log("[ajax/product] user_profile : ".json_encode($user_profile));	
			
			if(!empty($table['table']['record'][0])) {
				$user_profile = $table['table']['record'][0];
			}
			
			// Add By Thomas 2020/12/10 檢查用戶是否有通過手機號驗證
			$query ="SELECT *
			          FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			         WHERE prefixid = '{$config['default_prefix_id']}'
			           AND userid = '{$this->userid}'
			           AND switch = 'Y'	";
			$sms = $db->getQueryRecord($query); 	
			
			if($sms['table']['record'][0]['verified']!='Y') {
			    if($JSON=='Y') {
					 echo json_encode(getRetJSONArray(-14,"請先通過手機號驗證 !",'MSG'));
				} else {
					 // ToDo Web Error Page
				}	
				return;	
			}
			/*
			if(empty($user_profile['idnum'])) {
			    if($JSON=='Y') {
					 echo json_encode(getRetJSONArray(-14,"請先通過身分證號驗證 !",'MSG'));
				} else {
					 // ToDo Web Error Page
				}	
				return;	
			}
			*/
			/*
			if(empty($user_profile['address']) || empty($user_profile['addressee']) || empty($user_profile['rphone'])) {
				 if($JSON=='Y') {
					 echo json_encode(getRetJSONArray(-14,"下標前需先詳填收件人資料 !",'MSG'));
				 } else {
					 // ToDo Web Error Page
				 }	
				 return;			 
			}
			*/
		}
		
		switch ($product['pay_type']) {
			case 'a':
				if ($_POST['pay_type'] != 'spoint') {
					$r['err'] = 22;
					$r['retCode']=-100235;
					$r['retMsg']='請使用殺價幣下標';
				}
				break;
			case 'b':
				// 都可以下
				break;
			case 'c':
				if ($_POST['pay_type'] == 'scode') {
					$r['err'] = 22;
					$r['retCode']=-100235;
					$r['retMsg']='請使用殺價幣或殺價券下標';
				}
				break;
			case 'd':
				if ($_POST['pay_type'] == 'spoint') {
					$r['err'] = 22;
					$r['retCode']=-100235;
					$r['retMsg']='請使用殺價券或超級殺價券下標';
				}
				break;
			case 'e':
				if ($_POST['pay_type'] != 'oscode') {
					$r['err'] = 22;
					$r['retCode']=-100235;
					$r['retMsg']='請使用殺價券下標';
				}
				break;
		}
		if ($r['err'] && $r['err']>0) {
			if($JSON=='Y') {
				$ret=getRetJSONArray($r['retCode'],$r['retMsg'],'MSG');
				echo json_encode($ret);
				exit;
			} else {
				$ret['status'] = $r['err'];
			}
		}

		// 更新檢查的時間
		$product['now']=time();
		if(  $product['closed']!='N' ||
		    ($product['offtime'] == 0 && $product['locked'] == 'Y') ||
			($product['offtime'] > 0 && $product['now'] > $product['offtime'])) {
			//回傳: 本商品已結標
			$ret['status'] = 2;
			if($JSON=='Y') {
				$ret=getRetJSONArray(-2,'本商品已結標!!','MSG');
				echo json_encode($ret);
				exit;
			}
		}

		// 沒有用戶暱稱, 去DB撈 ?
		if(empty($this->username) && !empty($this->userid)) {
			$query ="SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE prefixid = '{$config['default_prefix_id']}'
			  AND userid = '{$this->userid}'
			  AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$this->username = $table['table']['record'][0]['nickname'];
		}

		// 下標總金額超過市價時 要擋
		$query ="SELECT sum(price) saja_total_price, count(*) saja_cnt
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
					WHERE productid = '{$product['productid']}'
					AND userid = '{$this->userid}'
					AND spointid > 0
					group by userid,productid";
		$table = $db->getQueryRecord($query);
		$discount = $table['table']['record'][0];

		if ($_POST['type']=='single') {
			$add_saja_fee = (int)$product['saja_fee'];
			$add_total_price = (int)$_POST['price'];
			$bidprice=$_POST['price'];
		}else if($_POST['type']=='range'){
			$times = (int)$_POST['price_stop']-(int)$_POST['price_start']+1;
			$add_saja_fee = (int)$times*(int)$product['saja_fee'];
			$add_total_price = (((int)$_POST['price_start']+(int)$_POST['price_stop'])*(int)$times)/2;
			$bidprice=$_POST['price_start']." ~ ".$_POST['price_stop'];
		}
		//26958:leo 28:jeff 用卷的不理他
		/* 2020/02/24 Thomas disabled via. Renee's request 
		if ($_POST['pay_type'] == 'spoint') {
			if ($bidprice>($product['retail_price']*0.9)){
				$inp=array();
				$inp['title']="下標異常[".date("Y-m-d H:i:s")."]";
				$inp['body']="(".$this->userid.")".$this->username.",商品:".$product['name']."(".$product['productid'].")市價:".$product['retail_price'].",出價:".$bidprice;
				$inp['productid']=$product['productid'];
				$inp['action']="";
				$inp['url_title']="";
				$inp['url']="";
				$inp['sender']="99";
				$inp['sendto']="26958,28,2788,1705,4068,88419";
				$inp['page']=0;
				$inp['target']='saja';
				$rtn=sendFCM($inp);
			}
		}
		*/
		switch ($product['totalfee_type']) {
			case 'A'://手續費+出價金額
				$product['discount'] = 0;
				$product['discount'] += (int)$discount['saja_total_price'];
				$product['discount'] += (int)$product['saja_fee']*(int)$discount['saja_cnt'];
				$product['discount'] += (int)$add_saja_fee+(int)$add_total_price;
				break;
			case 'B'://出價金額
				$product['discount'] = (int)$discount['saja_total_price']+(int)$add_total_price;;
				break;
			case 'F'://手續費
				$product['discount'] = (int)$product['saja_fee']*(int)$discount['saja_cnt'];
				$product['discount'] += (int)$add_saja_fee;
				break;
			default://免費
				$product['discount'] = 0;
				break;
		}

		if($product['discount'] > $product['retail_price']) {
			$ret = getRetJSONArray(-100233,'下標總金額超過市價','MSG');
			if($JSON=='Y') {
				echo json_encode($ret);
			} else {
				$ret['status'] = 20;
				echo json_encode($ret);
			}
			// error_log("[ajax/product] discount : ".$product['discount']." ,retail_price : ".$product['retail_price']);
			// error_log("[ajax/product] saja : ".json_encode($ret));
			return ;
		}

		// 檢查用卷數量 
		// 因改為一天2標, 此限制取消 

		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$product['productid']}'
			AND spointid=0
			AND sgiftid=0
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		$xscode_count=(int)$table['table']['record'][0]['count'];
		if ($_POST['pay_type'] == 'oscode' || $_POST['pay_type'] == 'scode') {
			// if ($xscode_count >= 800000 ) {
				// $ret = getRetJSONArray(-100326,'下標卷用量已超過20張 請用殺價幣下標','MSG');
				// if($JSON=='Y') {
					// echo json_encode($ret);
				// } else {
					// $ret['status'] = 26;
					// echo json_encode($ret);
				// }
				// return ;
			// } else {
				$product['xcode_count'] = (1000000-$xscode_count);
				$product['xcode_use'] = ($xscode_count);
				error_log("[ajax/product] xcode_count : ".$product['xcode_count']);
			// }
		}

		//
		//下標資格
		// 閃殺不檢測 By Thomas 20190102
		if($product['is_flash']!='Y') {
			//if ($this->userid != 1705) {
				$chk = $this->chk_saja_rule($product);
				if ($chk['err'] && $chk['err']>0) {
					if($JSON=='Y') {
						$ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],$chk['retType']);
						echo json_encode($ret);
						exit;
					} else {
						$ret['status'] = $chk['err'];
						$ret['value'] = $chk['value'];
						$ret['retMsg'] = isset($chk['retMsg']) ? $chk['retMsg'] : '';
					}
				}
			//}
		}

		if($_POST['type']=='single') { //單次下標
			$chk = $this->chk_single($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				} else {
				   $ret['status'] = $chk['err'];
				}
			}
		} else if($_POST['type']=='range' ) { //區間連續下標
			$chk = $this->chk_range($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				} else {
				   $ret['status'] = $chk['err'];
				}
			}
		} else if($_POST['type']=='lazy') { //懶人下標
			$chk = $this->chk_range($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				} else {
				   $ret['status'] = $chk['err'];
				}
			}
		}

		/* 隨機產生出價(僅供非正式機的壓力測試用)
		if(BASE_URL!="https://www.saja.com.tw" && $_REQUEST['random_price']=='Y') {
		   $this->price = getRandomNum(1,$product['retail_price']);
		   $this->username.= $this->price;
		}
		*/

		if(empty($ret['status']) || $ret['status']==0 ) {
			$rep_patten[]="'";
			$rep_patten[]="'";
			$this->username=str_replace($rep_patten,"",$this->username);
			//產生下標歷史記錄
			$mk = $this->mk_history($product);

			//回傳: 下標完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			if($mk['err']) {
				// 有異常
				if($JSON=='Y') {
				   $ret=getRetJSONArray($mk['retCode'],$mk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				}
			}

			// 正常下標
			if($JSON=='Y') {
				$ret['retObj']=array();
				$ret['retObj']['winner'] = $mk['winner'];
				$ret['retObj']['rank'] = $mk['rank'];
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				//單次下標
				if($_POST['type']=='single') {
					if ($mk['rank']==-2) {
						$msg = '最後一分鐘下標不提示排名或重複 ！';
					} else if ($mk['rank']==-1) {
						$msg = '很抱歉！這個價錢與其他人重複了！請您試試其他價錢或許會有好運喔！';
					} else if($mk['rank']==0) {
						$msg = '恭喜！您是目前的得標者！記得在時間到之前要多佈局！';
					} else if ($mk['rank']>=1 && $mk['rank']<=10) {
						$msg = '恭喜！這個價錢是「唯一」但不是「最低」目前是「唯一」又比您「低」的還有 '.($mk['rank']).' 位。';
					} else if ($mk['rank']>10) {
						$msg = '恭喜！這個價錢是「唯一」但不是「最低」目前是「唯一」又比您「低」的超過 10 位。';
					}
					$ret['retMsg'] = '單次出價 '.$_POST['price'].'元 成功 '. $msg;
				} else if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['retObj']['total_num'] = $mk['total_num'];
					$ret['retObj']['num_uniprice'] = $mk['num_uniprice'];
					$ret['retObj']['lowest_uniprice'] = $mk['lowest_uniprice'];
					if($mk['total_num']>=2) {
						if($mk['num_uniprice']>0) {
						   $msg ='本次有 '.$mk['num_uniprice'].' 個出價是唯一價, 其中最低價格是 '.$mk['lowest_uniprice'].' 元 !';
						} else {
						   $msg ='本次下標的出價都與其他人重複了 !';
						}
					}
					$ret['retMsg'] = '連續出價 '.$_POST['price_start'].'至'.$_POST['price_stop'].'元 成功 !'.$msg;
				}
			} else {
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['total_num'] = $mk['total_num'];
					$ret['num_uniprice'] = $mk['num_uniprice'];
					$ret['lowest_uniprice'] = $mk['lowest_uniprice'];
				}
			}
		}
		// error_log("[ajax/product] saja : ".json_encode($ret));
		$db=null;

		echo json_encode($ret);
	}

	//下標資格
	// public function chk_saja_rule($product)	{
	// 	global $db, $config;

	// 	$srid="any_saja";
	// 	$srvalue=0;
	// 	if(is_array($product) && !empty($product['srid']) && !empty($product['srvalue'])) {
	// 		$srid=$product['srid'];
	// 		$srvalue=$product['srvalue'];
	// 	} else {
	// 		$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt`
	// 		where
	// 			`prefixid` = '{$config['default_prefix_id']}'
	// 			AND productid = '{$product['productid']}'
	// 			AND switch = 'Y'
	// 		";
	// 		$table = $db->getQueryRecord($query);
	// 		$srid=$table['table']['record'][0]['srid'];
	// 		$srvalue=$table['table']['record'][0]['value'];
	// 	}

	// 	$r['err'] = '';
	// 	$r['value'] = 0;
	// 	$count = $this->pay_get_product_count($this->userid);


	// 	if( $srid=='never_win_saja') {		//新手(未曾得標)
	// 		if ($count > 0) {
	// 			$r['err'] = 15;
	// 			$r['retCode']=-100215;
	// 			$r['retMsg']='未符合(未得標新手)資格!!';
	// 		}
	// 	} else if($srid=='le_win_saja') {	//得標次數n次以內
	// 		if($count > $srvalue) {
	// 			$r['err'] = 16;
	// 			$r['value'] = $srvalue;
	// 			$r['retCode']=-100216;
	// 			$r['retMsg']='未符合下標資格!!';
	// 		}
	// 	} else if($srid=='ge_win_saja') {	//得標次數n次以上
	// 		if($count <  $srvalue) {
	// 			$r['err'] = 17;
	// 			$r['value'] = $srvalue;
	// 			$r['retCode']=-100217;
	// 			$r['retMsg']='未符合老手資格!!';
	// 		}
	// 	}
	// 	return $r;
	// }

	//檢查是屬於特定分類
	public function getProductCatetory($productid,$pcid){
		global $db, $config;
		$query = "SELECT pcid FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` where productid='{$productid}' and pcid='{$pcid}'";
		error_log($query);
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['pcid'];
        }else{
        	return "-1";
        }
	}
	//下標資格
	public function chk_saja_rule($product)	{
		global $db, $config;

		$limitid = $product['limitid'];

		$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited`
		where
			plid = '{$limitid}'
			AND switch = 'Y'
		";
		// error_log("chk_saja_rule query: ".$query);
		$table = $db->getQueryRecord($query);
		$kind=$table['table']['record'][0]['kind'];
		$mvalue=$table['table']['record'][0]['mvalue'];
		$svalue=$table['table']['record'][0]['svalue'];

		$r['err'] = '';
		$r['value'] = 0;
		//賀歲處理
		$productCategory=$this->getProductCatetory($product['productid'],147);
		if ($productCategory=='147'){
			$count = $this->pay_get_product_count($this->userid,$productCategory);
			error_log("557 ".$count);
			if ($count>0){
				$r['err'] = 24;
				$r['retCode']=-100324;
				$r['retMsg']='您已在賀歲商品得過標';
			}else{
				//競標中商品同時間只能有一檔
				// $count = $this->product_bid_count($this->userid,$product['productid'],$productCategory,"",1,'Y');
				$count = $this->product_bid_count($this->userid,$product['productid'],$productCategory,"",1,'N');
				if ($count>0){
					$r['err'] = 25;
					$r['retCode']=-100325;
					$r['retMsg']='您在賀歲商品尚有其它競標中的項目';
				}
			}
		}

		if( $kind=='small') {		//小於
		   	if($mvalue==1) {
				if ($count >= $mvalue) {
					$r['err'] = 16;
					$r['retCode']=-100216;
					$r['retMsg']='未符合下標資格: '.$table['table']['record'][0]['name'];
					$r['value'] = $mvalue;
				}
			} else {
				if ($count > $mvalue) {
					$r['err'] = 16;
					$r['retCode']=-100216;
					$r['retMsg']='未符合下標資格: '.$table['table']['record'][0]['name'];
					$r['value'] = $mvalue;
				}
			}
		} else if($kind=='big') {	//大於
			if($count < $svalue) {
				$r['err'] = 16;
				$r['retCode']=-100216;
				$r['retMsg']='未符合下標資格:'.$table['table']['record'][0]['name'];
				$r['value'] = $svalue;
			}
		}

		// 新手限定 一次一標
		if ($limitid == 7 || $limitid ==21 || $limitid ==22 ) {
			$query = "SELECT
						h.productid,
						p.name,
						h.userid,
						h.nickname
					FROM
						`{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON h.productid = p.productid
						AND p.switch = 'Y'
					WHERE
						h.switch = 'Y'
					AND p.limitid = '{$limitid}'
					AND (
						p.offtime > now()
						OR p.closed = 'N'
					)
					AND h.userid = '{$this->userid}'
					AND p.productid <> '{$product['productid']}'
					AND p.ptype = '{$product['ptype']}'
					GROUP BY
						h.productid";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])) {
				$r['err'] = 21;
				$r['retCode']=-100234;
				$r['retMsg']='已有下標其他新手限定商品';
			}
		}

		//查詢下標 狀態 AARONFU
		$bid_able = query_bid_able($this->userid);
		if($bid_able["bid_enable"] !=='Y') {
			$r['err'] = 23;
			$r['retCode']=-100236;
			$r['retMsg']='下標功能鎖定中,請洽客服';
		}

		return $r;
	}

	//得標次數限制
	/*public function pay_get_product_count($userid) {
		global $db, $config;

		$query = "SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
		where
			`prefixid` = '{$config['default_prefix_id']}'
			AND userid = '{$userid}'
			AND switch = 'Y'
		";
		// error_log("pay_get_product_count query: ".$query);
		$table = $db->getQueryRecord($query);

		return $table['table']['record'][0]['count'];
	}*/

	// 計算用戶得標次數
	// Add By Thomas 2020-01-17
	public function pay_get_product_count($userid, $pcid='', $plid='') {
		global $db, $config;
        if(empty($userid)) {
			   return false;
		}
        if(empty($plid) && empty($pcid)) {
			$query = "SELECT count(*) as count
						FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
					   WHERE `prefixid` = '{$config['default_prefix_id']}'
						 AND userid = '${userid}'
						 AND switch = 'Y' ";
		} else if($pcid>0) {
		    // 某商品分類下的商品 得標次數
			$query =  "SELECT count(*) as count
						 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
						 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						   ON pgp.productid = p.productid
						 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr
						   ON p.productid = pcr.productid
						WHERE pgp.`prefixid` = '{$config['default_prefix_id']}'
						  AND pgp.switch = 'Y'
						  AND pgp.userid = '${userid}'
						  AND pcr.pcid= '${pcid}' ";
		} else if($plid>0) {
			// 某下標限制的商品 得標次數
			$query =  "SELECT count(*) as count
						 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
						 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						   ON pgp.productid = p.productid
						WHERE pgp.`prefixid` = '{$config['default_prefix_id']}'
						  AND pgp.switch = 'Y'
						  AND pgp.userid = '${userid}'
						  AND p.limitid= '${plid}' ";
		}
		$table = $db->getQueryRecord($query);
		error_log("[ajax/product] pay_get_product_count(${userid},${pcid},${plid}) : ".$table['table']['record'][0]['count']);
		if (sizeof($table['table']['record'])>0){
			return $table['table']['record'][0]['count'];
		}else{
			return 0;
		}
	}

	// 計算用戶下標次數
	// Add By Thomas 2020-01-17
	// notIn時表示不含該物
	// bidstatus 表示針對特定狀態
	public function product_bid_count($userid, $productid, $pcid='', $plid='',$notIn='',$bidstatus='') {
		global $db, $config;

		    if(empty($userid) || empty($productid)) {
			   return false;
			}
			if(empty($plid) && empty($pcid)) {
			    $query = "SELECT count(*) as count
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
						   WHERE sh.`prefixid` = '{$config['default_prefix_id']}'
							 AND switch = 'Y'
							 AND userid = '${userid}'
							 AND productid = '${productid}' ";
			} else if($pcid>0) {
				// 某商品分類下的商品 下標數

				$query =  "SELECT count(*) as count
							 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
							 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
							   ON sh.productid = p.productid
							 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr
							   ON p.productid = pcr.productid
							WHERE sh.`prefixid` = '{$config['default_prefix_id']}'
							  AND sh.`userid` = '${userid}'
							  AND sh.`switch` = 'Y'
							  AND pcr.pcid= '${pcid}' ";

				if ($notIn==1) $query.=" AND p.productid<>'{$productid}' ";
				// if ($bidstatus!='') $query.=" AND (p.closed<>'".$bidstatus."' ) ";
				if ($bidstatus!='') $query.=" AND p.closed='".$bidstatus."' ";
			} else if($plid>0) {
				// 某下標限制的商品 下標數
				$query =  "SELECT count(*) as count
							 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
							 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
							   ON sh.productid = p.productid
							WHERE sh.`prefixid` = '{$config['default_prefix_id']}'
							  AND sh.`userid` = '${userid}'
							  AND sh.`switch` = 'Y'
							  AND p.`limitid`= '${plid}' ";
				if ($notIn==1) $query.=" AND p.productid<>'{$productid}' ";
				if ($bidstatus!='') $query.=" AND p.closed='".$bidstatus."' ";
			}
			$table = $db->getQueryRecord($query);
			//var_dump($query);
			error_log("[ajax/product] product_bid_count(${userid},${productid},${pcid},${plid}) : ".$table['table']['record'][0]['count']);

		if (sizeof($table['table']['record'])>0){
			return $table['table']['record'][0]['count'];
		}else{
			return 0;
		}
	}

	// 連續下標
	public function chk_range($product, $pay_type) {
		$time_start = microtime(true);

		global $db, $config;

		$r['err'] = '';
		// 檢查是否結標時, 不可用DB撈出的時間
		$osq = 0;
		//檢查殺價券數量
		if ($pay_type=='oscode'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND offtime > NOW()
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
		}
		if(empty($_POST['price_start']) || empty($_POST['price_stop'])) {
			//'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($_POST['price_start']) || !is_numeric($_POST['price_stop'])) {
			//'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((int)$_POST['price_start'] < (int)$product['price_limit']) {
			//'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif($product['offtime'] > 0 && ($product['offtime'] - time() < $this->range_saja_time_limit)) {
			//'超過可連續下標時間'
			$r['err'] = 7;
			$r['retCode']=-100227;
			$r['retMsg']='超過可連續下標時間!';
		} elseif((int)$_POST['price_start'] > (int)$_POST['price_stop']) {
			//'起始價格不可超過結束價格'
			$r['err'] = 9;
			$r['retCode']=-100229;
			$r['retMsg']='起始價格不可超過結束價格!';
		} elseif((int)$_POST['price_stop'] > (int)$product['retail_price']) {
			//'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif((int)$_POST['price_start'] == (int)$_POST['price_stop']) {
			//'起始價格不可超過結束價格'
			$r['err'] = 12;
			$r['retCode']=-100232;
			$r['retMsg']='出價起訖區間不可相同!';
		}

		//本次下標數
		$this->tickets = ((int)$_POST['price_stop'] - (int)$_POST['price_start']) / $this->currency_unit + 1;
		error_log("[ajax/product] 本次下標數：".$this->tickets);
		//單一用戶總下標次數限制檢查
		/*
		if(((int)$product['user_limit']>=0) && ($this->chk_user_limit($product['user_limit'], $product['productid'],$pay_type,$osq))) {
			error_log("[ajax/product] 檢查結果：".$this->chk_user_limit($product['user_limit'], $product['productid'],$pay_type,$osq));
			$r['err'] = 11;
			$r['retCode']=-100231;
			$r['retMsg']='超過可下標次數限制!';

		}
		*/
		if($this->chk_user_limit((int)$product['user_limit'], $product['productid'],$pay_type,$osq)) {
			// error_log("[ajax/product] 檢查結果：".$this->chk_user_limit($product['user_limit'], $product['productid'],$pay_type,$osq));
			$r['err'] = 11;
			$r['retCode']=-100231;
			if((int)$product['user_limit']==0) {
  			   $r['retMsg']='本商品尚未開放競標 !';
			} else {
			   $r['retMsg']='超過可下標次數限制!';
			}

		}

		//起始價格
		$this->price = $_POST['price_start'];

		if($this->tickets > (int)$product['usereach_limit']) {
			//'超過可連續下標次數限制'
			$r['err'] = 8;
			$r['retCode']=-100228;
			if((int)$product['usereach_limit']==0) {
  			   $r['retMsg']='本商品尚未開放競標 !';
			} else {
			   $r['retMsg']='超過可連續下標次數限制 !';
			}
		}
		return $r;
	}

/* 取得目前可用的超殺卷數*/
	public function get_vaild_code($uid){
		global $db, $config;
		$query = "SELECT userid, sum(remainder) as amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			WHERE
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND behav != 'a'
				AND offtime > NOW()
				AND closed = 'N'
				AND switch = 'Y'
			";
		$table1 = $db->getQueryRecord($query);
		$get_scode=0;
		if(!empty($table1['table']['record'])) {
			$get_scode = (int)$table1['table']['record'][0]['amount'];
		}
		return $get_scode;//+$use_sum;
	}

	// 單次下標
	public function chk_single($product,$pay_type) {
		$time_start = microtime(true);

		global $db, $config;

		//殺價記錄數量
		$this->tickets = 1;

		//殺價起始價格
		$this->price = $_POST['price'];
		$osq = 0;
		//檢查殺價券數量
		if ($pay_type=='oscode'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND offtime > NOW()
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
		}
		$r['err'] = '';

		if(empty($this->price)) {
			//'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($this->price)) {
			//'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((float)$this->price < (float)$product['price_limit']) {
			//'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif((float)$this->price > (float)$product['retail_price']) {
			//'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif($this->chk_user_limit((int)$product['user_limit'], $product['productid'],$pay_type,$osq)) {
			//'超過可下標次數限制'
			error_log("[ajax/product] 檢查結果：".$this->chk_user_limit($product['user_limit'], $product['productid'],$pay_type,$osq));
			$r['err'] = 11;
			$r['retCode']=-100231;
			if((int)$product['user_limit']==0) {
  			   $r['retMsg']='本商品尚未開放競標 !';
			} else {
			   $r['retMsg']='超過可下標次數限制!';
			}
		}

		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "單次下標 $time 秒<br>";
		return $r;
	}

	//檢查使用者下標次數限制
	public function chk_user_limit($user_limit, $pid,$pay_type,$osq) {
		$time_start = microtime(true);

		global $db, $config;
		
		// user_limit=0 就不用檢測了 必擋
		if($user_limit==0)
		   return 1;
	   
		//賀歲商品的$user_limit放大 百萬殺車的$user_limit放大
		if (($this->getProductCatetory($pid,147)>'0')||($this->getProductCatetory($pid,144)>'0')) $user_limit=999999;

		/*兩種卷合併記次*/
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$pid}'
			AND spointid>0
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		$spoint_count=(int)$table['table']['record'][0]['count'];
		error_log("[ajax/product] 付費下標數：".$spoint_count);
		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$pid}'
			AND spointid=0
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$code_limit=$user_limit;
		$table = $db->getQueryRecord($query);
		$oscode_count=(int)$table['table']['record'][0]['count'];
		error_log("[ajax/product] pay_type：".$pay_type);
		if ($pay_type=='spoint'){
			$tickets_avaiable = (int)$user_limit -$spoint_count;
			//下幣且幣的次數不夠
			error_log("[ajax/product] user_limit：".$tickets_avaiable." <====> tickets".$this->tickets);
			if((int)$this->tickets > (int)$tickets_avaiable) return 1;
		}else{
			//如果下的是超殺則去拉超殺的次數
			if ($pay_type=='scode') $osq=$this->get_vaild_code($this->userid);
			//比如限制是 10次  已用殺價券4次 尚有超沙7張
			if ($oscode_count==0){
				//取卷的剩餘數或下標卷次數較低者
				$oscode_limit=($osq<$user_limit)?$osq:$user_limit;
				$tickets_avaiable = (int)$oscode_limit - $oscode_count;
			}else{
				$user_limit=$user_limit-$oscode_count;
				$tickets_avaiable=($osq<$user_limit)?$osq:$user_limit;
				//$tickets_avaiable = $oscode_limit;//(int)$oscode_limit - $oscode_count;
			}
			//下卷且卷的次數不夠則看幣還有沒有剩($user_limit-$spoint_count); 
			//當商品不等房子
			if ($pid != 97115){
				if((int)$this->tickets > $tickets_avaiable) {
					if((int)$this->tickets >($tickets_avaiable+$user_limit-$spoint_count)) return 1;
				}
			}
		}
		return 0;
	}


	//檢查最後下標時間
	public function chk_time_limit() {
		$time_start = microtime(true);

		global $db, $config;

		$query = "
		SELECT unix_timestamp(MAX(insertt)) as insertt, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			if( (int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $this->saja_time_limit) {
				return 1;
			}
		}
		return 0;
	}

	public  function getBidWinner($productid) {
			global $db, $config;
			$ret=array(
			   'nickname'=>'',
			   'name'=>'',
			   'userid'=>'',
			   'productid'=>'',
			   'price'=>0,
			   'src_ip'=>''
			);
			$query="SELECT sh.userid, sh.productid, sh.nickname, sh.price
				   FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
				  WHERE sh.prefixid = '{$config['default_prefix_id']}'
					AND sh.productid = '{$productid}'
					AND sh.switch = 'Y'
					AND sh.type = 'bid'
					AND sh.userid IS NOT NULL
					GROUP BY sh.price
					HAVING COUNT(sh.price) = 1
					ORDER BY sh.price ASC
					LIMIT 1 ";
			$table = $db->getQueryRecord($query);

			// error_log("[ajax/product] sql : ".$query);

			if($table['table']['record'][0] && $table['table']['record'][0]['price']>0) {
				$ret=array();
				$ret['userid'] = $table['table']['record'][0]['userid'];
				$ret['nickname'] = $table['table']['record'][0]['nickname'];
				$ret['name'] = $table['table']['record'][0]['nickname'];
				$ret['productid'] = $table['table']['record'][0]['productid'];
				$ret['price'] = $table['table']['record'][0]['price'];
			 } else {
				$ret = false;
			}
			// error_log("[ajax/product/getBidWinner] : ".json_encode($ret));
			return $ret;
	}

	/*
	//得標者資訊
	public function getBidWinner($productid) {
		global $db, $config;
		// 得標者
		$ret='';
		$query="SELECT h.userid, h.productid, h.nickname, h.price, h.src_ip ".
			  ",(SELECT name from `{$config['db'][2]["dbname"]}`.`{$config['default_prefix']}province` WHERE switch='Y' AND provinceid=up.provinceid LIMIT 1) as src_province ".
			  ",(SELECT name from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` WHERE switch = 'Y' AND provinceid=up.provinceid) as comefrom ".
			  ",(SELECT name from `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}product` WHERE productid=h.productid LIMIT 1) as prodname ".
			  ",(SELECT uid from `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}sso` WHERE ssoid=(SELECT ssoid FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sso_rt` WHERE userid=h.userid and switch='Y' LIMIT 1) and switch='Y' and name='weixin' LIMIT 1) as openid ".
			  " FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h ".
			  " JOIN `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_profile` up ON h.userid=up.userid ".
			  " WHERE (h.productid, h.price) =  ".
			  "       (SELECT productid, min_price FROM  `{$config['db'][4]["dbname"]}`.`v_min_unique_price` WHERE productid='{$productid}' LIMIT 1) ";
		// error_log("[ajax/product/getBidWinner] : ".$query);

		$table = $db->getQueryRecord($query);
		if($table['table']['record']) {
			$ret=array();
			$ret['userid'] = $table['table']['record'][0]['userid'];
			$ret['nickname'] = $table['table']['record'][0]['nickname'];
			$ret['productid'] = $table['table']['record'][0]['productid'];
			$ret['prodname'] = $table['table']['record'][0]['prodname'];
			$ret['src_province'] = $table['table']['record'][0]['src_province'];
			$ret['price'] = $table['table']['record'][0]['price'];
			$ret['openid'] = $table['table']['record'][0]['openid'];
			$ret['src_ip'] = $table['table']['record'][0]['src_ip'];
			$ret['comefrom'] = $table['table']['record'][0]['comefrom'];
		}
		error_log("[ajax/product/getBidWinner] : ".json_encode($ret));
		return $ret;
	}
	*/

	//產生下標歷史記錄
	public function mk_history($product) {
		$time_start = microtime(true);

		global $db, $config, $jpush;

		// $jp = new jpush;

		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = -2;
		$values = array();
		$ip = GetIP();
		if($_POST['pay_type'] == 'oscode') {
			/****************************
			* 使用殺價券 : 為針對某特定商品免費的「下標次數」， 1張殺價券可以下標乙次
			****************************/

			//檢查殺價券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND offtime > NOW()
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
			error_log("[ajax/product/mk_history]: oscode needed ==>".round($this->tickets));
			error_log("[ajax/product/mk_history]: oscode available : ".$query);

			if ($osq > $_POST['oscode_num']) {
				$osq = $_POST['oscode_num'];
			}
			error_log("[ajax/product/mk_history]: osq  : ".$osq);
			
			//判斷殺價券可使用數量
			if ($osq <= $product['xcode_count']){
				$xosq = $osq;
			}else{
				$xosq = $product['xcode_count'];
			}
			
			//判斷取得每天免費可下標數
			$everyday_xcode_count = $this->everyday_xcode_count($product, round($this->tickets));
			
			if($product['everydaybid'] != 0 && $everyday_xcode_count == 0) {
				$r['err'] = 28;
				$r['retCode']=-100328;
				$r['retMsg']='已超過此商品每天免費可下標數 請用殺價幣下標!';
			}else{

				if(round($this->tickets) > $xosq) {

					/****************************
					* 使用殺幣
					****************************/
					$scode_fee = 0;
					$needspoint = 0;
					for($i = 0 ; $i < round($this->tickets-$xosq); $i++)
					{
						$needspoint = $needspoint + ($_POST['price_start']+$i);
					}

					$saja_fee_amount= getBidTotalFee($product,
													 $_POST['type'],
													 $_POST['price'],
													 $_POST['price_start'],
													 $_POST['price_stop'],
													 $this->currency_unit);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$needspoint = $needspoint+($product['saja_fee']*round($this->tickets-$_POST['oscode_num']));break;
						case 'B':$needspoint = $needspoint;break;
						case 'O':$needspoint = 0;break;
						case 'F':$needspoint = $product['saja_fee']*round($this->tickets-$osq);break;
						default:$needspoint = $product['saja_fee']*round($this->tickets-$osq);break;
					}

					error_log("[ajax/product/mk_history]: oscode needspoint : ".$needspoint);
					$needspoint = abs($needspoint);
					error_log("[ajax/product/mk_history]: oscode needspoint2 : ".$needspoint);
					
					// 檢查殺幣餘額
					$query = "SELECT SUM(amount) as amount
								FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
							   WHERE userid = '{$this->userid}'
								 AND prefixid = '{$config['default_prefix_id']}'
								 AND switch = 'Y'
					";
					$spointtable = $db->getQueryRecord($query);
					error_log("[ajax/product/mk_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

					if ($product['pay_type'] == 'd' || $product['pay_type'] == 'e') {
						// 限定下標方式
						$r['err'] = 22;
						$r['retCode']=-100235;
						$r['retMsg']='請使用殺價券下標';
					}else if($needspoint > (float)$spointtable['table']['record'][0]['amount']) {
						//'殺價幣不足'
						$r['err'] = 13;
						$r['retCode']=-100193;
						$r['retMsg']='殺價幣不足!';
					} else {

						//使用殺幣
						$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
						SET
							`prefixid`='{$config['default_prefix_id']}',
							`userid`='{$this->userid}',
							`countryid`='{$this->countryid}',
							`behav`='user_saja',
							`amount`='".- $needspoint."',
							`insertt`=NOW()
						";

						$res = $db->query($query);
						$spointid = $db->_con->insert_id;
						error_log("[ajax/product/mk_history]: spointid : ".$spointid);

						//活動送幣使用紀錄
						$this->saja_spoint_free_history($needspoint , $spointid, $product['productid']);
						
						
						//使用殺價券
						for($i = 0 ; $i < round($xosq); $i++) {
							$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
							SET
								used = 'Y',
								used_time = NOW()
							WHERE
								`prefixid` = '{$config['default_prefix_id']}'
								AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
								AND switch = 'Y'
							";
							$res = $db->query($query);

							error_log("[ajax/product/mk_history]: oscode res ".$i." : ".$res);
						}

						//使用殺價幣
						for($i = 0 ; $i < round($this->tickets-$xosq); $i++) {
							
							$bid_total = 0;
							$bid_total = $bid_total + ($_POST['price_start']+$i);

							// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
							switch($product['totalfee_type']) {
								case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
								case 'B':$bid_total = $bid_total;break;
								case 'O':$bid_total = 0;break;
								case 'F':$bid_total = $product['saja_fee'];break;
								default:$bid_total = $product['saja_fee'];break;
							}
							
							//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'{$this->userid}',
								'{$this->username}',
								'{$spointid}',
								'0',
								'0',
								'0',
								'".($this->price + $i * $this->currency_unit)."',
								'{$bid_total}',
								NOW(),
								'{$ip}'
							)";
						}

						$k=0;
						//使用殺價券
						for($i = $this->tickets-$xosq; $i < round($this->tickets); $i++) {
							$bid_total = 0;
							$bid_total = $bid_total + ($_POST['price_start']+$i);

							// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
							switch($product['totalfee_type']) {
								case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
								case 'B':$bid_total = $bid_total;break;
								case 'O':$bid_total = 0;break;
								case 'F':$bid_total = $product['saja_fee'];break;
								default:$bid_total = $product['saja_fee'];break;
							}
							//殺價下標歷史記錄（對應下標記錄與 oscodeid）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'{$this->userid}',
								'{$this->username}',
								'0',
								'0',
								'0',
								'{$table['table']['record'][$k]['oscodeid']}',
								'".($this->price + $i * $this->currency_unit)."',
								'{$bid_total}',
								NOW(),
								'{$ip}'
							)";
							$k++;
							error_log("[ajax/product/mk_history]: oscode values : ".$table['table']['record'][($k-1)]['oscodeid']);

						}

						if(empty($values)) {
							//'下標失敗'
							$r['err'] = -1;
							$r['retCode']=-1;
							$r['retMsg']='下標失敗!';
						}
					}
				} else {
					//使用殺價券
					for($i = 0; $i < round($this->tickets); $i++) {
						
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
						SET
							used = 'Y',
							used_time = NOW()
						WHERE
							`prefixid` = '{$config['default_prefix_id']}'
							AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
							AND switch = 'Y'
						";
						$res = $db->query($query);

						//殺價下標歷史記錄（對應下標記錄與 oscodeid）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'0',
							'0',
							'{$table['table']['record'][$i]['oscodeid']}',
							'".($this->price + $i * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = -1;
						$r['retCode']=-1;
						$r['retMsg']='下標失敗!';
					}
				}
			
			}
			
		} else if($_POST['pay_type'] == 'scode') {
			/****************************
			* 使用S碼 : 為免費的「下標次數」，1組S碼可以下標乙次
			* 有效時間為30天(發送當天開始算30天)，逾期失效不補回。
			超級殺價卷
			****************************/

			$scodeModel = new ScodeModel;
			$scode = $scodeModel->get_scode($this->userid);

			if(empty($scode)) {
				$sq = 0;
			} else {
				$sq = $scode;
			}
			error_log("[ajax/product/mk_history]: scode needed ==>".round($this->tickets));

			if($sq > $_POST['scode_num']) {
				$sq = $_POST['scode_num'];
			}
			error_log("[ajax/product/mk_history]: sq  : ".$sq);
			
			//判斷殺價券可使用數量
			if ($sq <= $product['xcode_count']){
				$xsq = $sq;
			}else{
				$xsq = $product['xcode_count'];
			}
			
			//判斷取得每天免費可下標數
			$everyday_xcode_count = $this->everyday_xcode_count($product, round($this->tickets));

			if($product['everydaybid'] != 0 && $everyday_xcode_count == 0) {
				$r['err'] = 28;
				$r['retCode']=-100328;
				$r['retMsg']='已超過此商品每天免費可下標數 請用殺價幣下標!';
			}else{
				
				//檢查S碼數量
				if(round($this->tickets) > $xsq) {

					//'超級殺價卷不足'
					// $r['err'] = 19;
					// $r['retCode']=-100199;
					// $r['retMsg']='超級殺價券不足!';
					// 20190424

					/****************************
					* 使用殺幣
					****************************/
					$scode_fee = 0;
					$needspoint = 0;
					for($i = 0 ; $i < round($this->tickets-$xsq); $i++)
					{
						$needspoint = $needspoint + ($_POST['price_start']+$i);
					}

					$saja_fee_amount= getBidTotalFee($product,
													 $_POST['type'],
													 $_POST['price'],
													 $_POST['price_start'],
													 $_POST['price_stop'],
													 $this->currency_unit);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)

					switch($product['totalfee_type']) {
						case 'A':$needspoint = $needspoint+($product['saja_fee']*round($this->tickets-$_POST['scode_num']));break;
						case 'B':$needspoint = $needspoint;break;
						case 'O':$needspoint = 0;break;
						case 'F':$needspoint = $product['saja_fee']*round($this->tickets-$sq);break;
						default:$needspoint = $product['saja_fee']*round($this->tickets-$sq);break;
					}
					error_log("[ajax/product/mk_history]: needspoint : ".$needspoint);
					$needspoint = abs($needspoint);
					error_log("[ajax/product/mk_history]: needspoint2 : ".$needspoint);
					// 檢查殺幣餘額
					$query = "SELECT SUM(amount) as amount
								FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
								WHERE userid = '{$this->userid}'
								AND prefixid = '{$config['default_prefix_id']}'
								AND switch = 'Y'
					";
					$spointtable = $db->getQueryRecord($query);
					error_log("[ajax/product/mk_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

					if ($product['pay_type'] == 'd' || $product['pay_type'] == 'e') {
						// 限定下標方式
						$r['err'] = 22;
						$r['retCode']=-100235;
						$r['retMsg']='請使用超級殺價券下標';
					}else if($needspoint > (float)$spointtable['table']['record'][0]['amount']) {
						//'殺價幣不足'
						$r['err'] = 13;
						$r['retCode']=-100193;
						$r['retMsg']='殺價幣不足!';
					}else {

						//使用殺幣
						$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
						SET
							`prefixid`='{$config['default_prefix_id']}',
							`userid`='{$this->userid}',
							`countryid`='{$this->countryid}',
							`behav`='user_saja',
							`amount`='".- $needspoint."',
							`insertt`=NOW()
						";
						$res = $db->query($query);
						$spointid = $db->_con->insert_id;
						error_log("[ajax/product/mk_history]: spointid : ".$spointid);

						//活動送幣使用紀錄
						$this->saja_spoint_free_history($needspoint, $spointid, $product['productid']);

						//使用超級殺價券
						for($i = 0 ; $i < round($xsq); $i++) {

								//確認S碼
							$query = "SELECT scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
								where
								`prefixid`='{$config['default_prefix_id']}'
								and `userid` = '{$this->userid}'
								and `behav` != 'a'
								and `closed` = 'N'
								and `remainder` > 0
								and `switch` = 'Y'
								and `offtime` > NOW()
								order by `offtime` asc, `scodeid` asc
								limit 0, 1
							";
							$table = $db->getQueryRecord($query);

							//使用S碼
							$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
									SET
										`remainder`=`remainder` - 1,
										`modifyt`=NOW()
									where
										`prefixid`='{$config['default_prefix_id']}'
										and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
										and `userid` = '{$this->userid}'
										and `behav` != 'a'
										and `closed` = 'N'
										and `remainder` > 0
										and `switch` = 'Y'
							";
							$res = $db->query($query);
						}

						// 新增扣除組數紀錄
						$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
						SET
							`prefixid`='{$config['default_prefix_id']}',
							`userid`='{$this->userid}',
							`behav`='a',
							`productid`='{$product['productid']}',
							`amount`='". - $xsq ."',
							`remainder`='0',
							`insertt`=NOW()
						";
						$res = $db->query($query);
						$scodeid = $db->_con->insert_id;
						
						for($i = round($this->tickets-$xsq); $i < round($this->tickets); $i++) {
							$bid_total = 0;
							$bid_total = $bid_total + ($_POST['price_start']+$i);

							// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
							switch($product['totalfee_type']) {
								case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
								case 'B':$bid_total = $bid_total;break;
								case 'O':$bid_total = 0;break;
								case 'F':$bid_total = $product['saja_fee'];break;
								default:$bid_total = $product['saja_fee'];break;
							}
							
							//殺價下標歷史記錄（對應下標記錄與使用S碼id）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'{$this->userid}',
								'{$this->username}',
								'0',
								'{$scodeid}',
								'0',
								'0',
								'".($this->price + $i * $this->currency_unit)."',
								'{$bid_total}',
								NOW(),
								'{$ip}'
							)";
						}
						
						//使用殺價幣
						for($i = 0 ; $i < round($this->tickets-$xsq); $i++) {
							$bid_total = 0;
							$bid_total = $bid_total + ($_POST['price_start']+$i);

							// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
							switch($product['totalfee_type']) {
								case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
								case 'B':$bid_total = $bid_total;break;
								case 'O':$bid_total = 0;break;
								case 'F':$bid_total = $product['saja_fee'];break;
								default:$bid_total = $product['saja_fee'];break;
							}
							
							//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'{$this->userid}',
								'{$this->username}',
								'{$spointid}',
								'0',
								'0',
								'0',
								'".($this->price + $i * $this->currency_unit)."',
								'{$bid_total}',
								NOW(),
								'{$ip}'
							)";
						}
						

						
						if(empty($values)) {
							//'下標失敗'
							$r['err'] = -1;
							$r['retCode']=-1;
							$r['retMsg']='下標失敗!';
						}
					}

				} else {
					for($i = 0 ; $i < round($this->tickets); $i++) {
						//確認S碼
						$query = "SELECT scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
							where
							`prefixid`='{$config['default_prefix_id']}'
							and `userid` = '{$this->userid}'
							and `behav` != 'a'
							and `closed` = 'N'
							and `remainder` > 0
							and `switch` = 'Y'
							and `offtime` > NOW()
							order by insertt asc
							limit 0, 1
						";
						error_log("[ajax/product] chk scode : ".$query);
						$table = $db->getQueryRecord($query);
						error_log("[ajax/product] use scode : ".json_encode($table['table']['record']));

						//使用S碼
						$query = "update `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
						SET
							`remainder`=`remainder` - 1,
							`modifyt`=NOW()
						where
							`prefixid`='{$config['default_prefix_id']}'
							and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
							and `userid` = '{$this->userid}'
							and `behav` != 'a'
							and `closed` = 'N'
							and `remainder` > 0
							and `switch` = 'Y'
						";
						$res = $db->query($query);
						// error_log("[ajax/product] deduct scode : ".$query."=>".$res);
					}

					// 新增扣除組數紀錄
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`behav`='a',
						`productid`='{$product['productid']}',
						`amount`='". - $this->tickets ."',
						`remainder`='0',
						`insertt`=NOW()
					";
					$res = $db->query($query);
					$scodeid = $db->_con->insert_id;
					for($i = 0 ; $i < round($this->tickets); $i++) {
						
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						
						//殺價下標歷史記錄（對應下標記錄與使用S碼id）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'{$scodeid}',
							'0',
							'0',
							'".($this->price + $i * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = -1;
						$r['retCode']=-1;
						$r['retMsg']='下標失敗!';
					}
				}
			
			}
			
		} else {
			/****************************
			* 使用殺幣
			****************************/
			$scode_fee = 0;

			//檢查殺幣餘額
			$query = "SELECT SUM(amount) as amount
						FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
					   WHERE userid = '{$this->userid}'
						 AND prefixid = '{$config['default_prefix_id']}'
						 AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);

			$saja_fee_amount= getBidTotalFee($product,
											 $_POST['type'],
											 $_POST['price'],
											 $_POST['price_start'],
											 $_POST['price_stop'],
											 $this->currency_unit);
			// error_log("[ajax/product/mk_history] saja_fee_amount <==> table amount : ".$saja_fee_amount."<==>".(float)$table['table']['record'][0]['amount']);
			
			//判斷取得每天付費可下標數
			$everyday_xspoint_count = $this->everyday_xspoint_count($product, round($this->tickets));
			
			if($saja_fee_amount > (float)$table['table']['record'][0]['amount']) {
				//'殺價幣不足'
				$r['err'] = 13;
				$r['retCode']=-100193;
				$r['retMsg']='殺價幣不足!';
			} elseif($product['everydaybid'] != 0 && $everyday_xspoint_count == 0) {
				$r['err'] = 27;
				$r['retCode']=-100327;
				$r['retMsg']='已超過此商品每天付費可下標數 請用殺價卷下標!';
			} else {
				//使用殺幣
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`countryid`='{$this->countryid}',
					`behav`='user_saja',
					`amount`='". - $saja_fee_amount ."',
					`insertt`=NOW()
				";

				$res = $db->query($query);
				$spointid = $db->_con->insert_id;

				//活動送幣使用紀錄
				$this->saja_spoint_free_history($saja_fee_amount, $spointid, $product['productid']);
				
				for($i = 0 ; $i < round($this->tickets); $i++)
				{
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/product/mk_history]: bid_total : ".$bid_total);
					
					
					//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'{$this->userid}',
						'{$this->username}',
						'{$spointid}',
						'0',
						'0',
						'0',
						'".($this->price + $i * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";

				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retCode']=-1;
					$r['retMsg']='下標失敗!';
				}
			}
		}


		if(empty($r['err']) && !empty($values) ) {
			// 取得目前winner
			// 20190420 暫時移除 20180816回復

			$arr_ori_winner=array();
			$arr_ori_winner=$this->getBidWinner($product['productid']);
			// error_log("[ajax/product/mk_history] arr_ori_winner:".json_encode($arr_ori_winner));
			// if(!empty($arr_ori_winner)) {
			//    error_log("[ajax/product/mk_history]: ori Winner of ".$arr_ori_winner['prodname']." is :".$arr_ori_winner['nickname']);
			// } else {
			//    error_log("[ajax/product/mk_history]: ori Winner is Empty !");
			// }

			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
			(
				prefixid,
				productid,
				userid,
				nickname,
				spointid,
				scodeid,
				sgiftid,
				oscodeid,
				price,
				bid_total,
				insertt,
				src_ip
			)
			VALUES
			".implode(',', $values)."
			";

			$res = $db->query($query);
			// error_log("[ajax/product]:".$query."-->".$res);
			// $r['rank'] = $last_rank;

			/*
			// 非閃殺商品下標, 結標一分鐘以前, 單次下標將提示順位
			if($_POST['type'] == 'single' && $product['is_flash']!='Y') {
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
					// 檢查重複
					$query= "SELECT count(userid) as cnt
							   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
							  WHERE productid='{$product['productid']}'
								AND price={$_POST['price']}
								AND switch='Y' ";

					$table = $db->getQueryRecord($query);
					$dup_cnt=$table['table']['record'][0]['cnt'];
					if($dup_cnt>1) {
						// 重複出價
						$r['rank']=-1;
					} else if($dup_cnt<=1) {
						// 無重複出價
						$query1 = "SELECT count(*) as cnt FROM  `{$config['db'][4]["dbname"]}`.`v_unique_price`
								   WHERE productid='{$product['productid']}' AND price<{$_POST['price']} ";
						$table1 = $db->getQueryRecord($query1);
						$r['rank'] =$table1['table']['record'][0]['cnt'];
						error_log("[ajax/product]rank : ".$r['rank']);
					}
				} else {
					// 不提示
					$r['total_num']=-1;
				}
			}

			// 非閃殺商品下標, 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出價個數, 及該次下標的唯一最低價
			if($product['is_flash']!='Y') {
				if($_POST['type'] == 'range') {
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						$r['total_num']=$this->tickets;
						// 顯示提示
						// 取得該區間內唯一出價的個數
						if($this->tickets>1) {
							$query="SELECT * FROM saja_shop.saja_history sh JOIN saja_shop.v_unique_price up
									   ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y'
									WHERE sh.switch='Y'
									  AND sh.productid='{$product['productid']}'
									  AND sh.price between {$_POST['price_start']} AND {$_POST['price_stop']}
									  AND sh.userid='{$this->userid}' ORDER BY sh.price asc ";

							// error_log("[ajax/product]range chk: ".$query);
							$table = $db->getQueryRecord($query);
							error_log("[ajax/product]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);

							if(empty($table['table']['record'])) {
							   // 無唯一出價
							   $r['num_uniprice'] = 0;
							   $r['lowest_uniprice'] = 0;
							} else {
							   // 有唯一出價
							   $r['num_uniprice'] = count($table['table']['record']);
							   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
							}
						} else {
							// 不提示
							$r['total_num']=-1;
						}
					} else {
						// 不提示
						$r['total_num']=-1;
					}
				}
			}
			*/

			// 20190420 加入以redis sortedset 判斷的機制
			// 非閃殺商品下標, 結標一分鐘以前, 單次下標將提示順位
			$updWinner=false;
			// if($_POST['type'] == 'single' && $product['is_flash']!='Y') {
			if($_POST['type'] == 'single') {
				// 寫入redis, 並使用 SortedSet來判斷順位
				$cache = getBidCache();
				// 如果設定要用redis判斷 且有redis -> 用redis 判斷
				if($config["rank_bid_by_redis"] && $cache) {
				   $str_price = str_pad($this->price,12,"0",STR_PAD_LEFT);
				   $dup_cnt = $cache->zIncrBy("PROD:RANK:".$product['productid'],1,$str_price) ;
				   if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
				  		// 檢查重複
						if($dup_cnt>2) {
							// 2筆以上重複出價  不用更新得標者
							$r['rank']=-1;
							$updWinner=false;
						} else if($dup_cnt==2) {
							// 剛好有2筆重複  可能是把得標者幹掉 要更新得標者
							$r['rank']=-1;
							$updWinner=true;
						} else if($dup_cnt==1) {
							// zRank直接回傳該出價的排名 以0為第1,
							// 所以可以直接表示前面有幾個唯一的出價
							// $updWinner=true;
							$r['rank']= $cache->zRank("PROD:RANK:".$product['productid'],$str_price);
							if($r['rank']<2)
							   $updWinner=true;
						}
						error_log("Check rank From redis=>".$r['rank']);
					} else {
						// 不提示
						$updWinner=false;
						$r['total_num']=-1;
					}
				} else {
					// 沒有redis或未設定由redis判斷 -> 就走原來程序進DB撈
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						// 檢查重複
						$query= "SELECT count(userid) as cnt
								   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
								  WHERE productid='{$product['productid']}'
									AND price={$_POST['price']}
									AND switch='Y' ";

						$table = $db->getQueryRecord($query);
						$dup_cnt=$table['table']['record'][0]['cnt'];
						if($dup_cnt>2) {
							// 2筆以上的重複出價
							$r['rank']=-1;
							$updWinner = false;
						} else if($dup_cnt==2) {
							// 剛好2筆重複出價
							$r['rank']=-1;
							$updWinner = true;
						} else if($dup_cnt<2) {
							// 無重複出價
							$query1 = "SELECT count(*) as cnt FROM  `saja_view`.`v_unique_price`
									   WHERE productid='{$product['productid']}' AND price<{$_POST['price']} ";
							$table1 = $db->getQueryRecord($query1);
							$r['rank'] =$table1['table']['record'][0]['cnt'];
							if($r['rank']<2)
							   $updWinner = true;
							error_log("[ajax/product] DB rank : ".$r['rank']);
						}
						error_log("Check rank From DB=>".$r['rank']);
					} else {
					// 不提示
						$r['total_num']=-1;
						$r['rank']=-2;
					}
				}
			}


			// 非閃殺商品下標, 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出價個數, 及該次下標的唯一最低價
			// if($product['is_flash']!='Y') {
				if($_POST['type'] == 'range') {
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						$r['total_num']=$this->tickets;
						// 顯示提示
						// 取得該區間內唯一出價的個數
						$cache = getBidCache();
						$start_price = $_POST['price_start'];
						$end_price = $_POST['price_stop'];
						// 如果設定要用redis判斷 且有redis -> 用redis 判斷
						if($config["rank_bid_by_redis"] && $cache) {
							$r['num_uniprice'] = 0;
							$r['lowest_uniprice'] = 0;
							$updWinner=false;
							for($p=$start_price ; $p<=$end_price ; ++$p) {
								$str_price = str_pad($p,12,"0",STR_PAD_LEFT);
								$dup_cnt = $cache->zIncrBy("PROD:RANK:".$product['productid'],1,$str_price);
								if($dup_cnt<=2) {
								   $updWinner=true;
								}
								if($dup_cnt==1) {
								   // 增加唯一價的個數
								   $r['num_uniprice']++;
								   // 把第一個碰到的價格記下來(就是該批次內最低的出價)
								   if($r['lowest_uniprice']==0) {
									  $r['lowest_uniprice']=$p;
								   }
								}
							}
						} else {
							// 沒有redis或沒有指定要用redis計算 -> 走原本程序進DB撈
							if($this->tickets>1) {
								$query="SELECT * FROM saja_shop.saja_history sh JOIN saja_view.v_unique_price up
										   ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y'
										WHERE sh.switch='Y'
										  AND sh.productid='{$product['productid']}'
										  AND sh.price between {$_POST['price_start']} AND {$_POST['price_stop']}
										  AND sh.userid='{$this->userid}' ORDER BY sh.price asc ";

								// error_log("[ajax/product]range chk: ".$query);
								$table = $db->getQueryRecord($query);
								// error_log("[ajax/product]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);

								if(empty($table['table']['record'])) {
								   // 無唯一出價
								   $updWinner=false;
								   $r['num_uniprice'] = 0;
								   $r['lowest_uniprice'] = 0;
								} else {
								   // 有唯一出價
								   $updWinner=true;
								   $r['num_uniprice'] = count($table['table']['record']);
								   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
								}
							} else {
								// 不提示
								$r['total_num']=-1;
								$r['rank']=-2;
							}
						}
					} else {
						// 不提示
						$r['total_num']=-1;
						$r['rank']=-2;
					}
				}
			// }

			// 未重複或剛好2筆重複出價者  需更新得標者
			if($updWinner) {
				error_log("[ajax/product] updWinner : ".$updWinner);
				$arr_new_winner=array();
				$status = 1; //websocket 狀態碼(1:更新得標者, 2:無人得標)
				try {
					$arr_new_winner=$this->getBidWinner($product['productid']);

					$query ="SELECT thumbnail_file, thumbnail_url
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					WHERE prefixid = '{$config['default_prefix_id']}'
					  AND userid = '{$arr_new_winner['userid']}'
					  AND switch = 'Y'
					";
					$table = $db->getQueryRecord($query);
					$r['thumbnail_file'] = $table['table']['record'][0]['thumbnail_file'];
					$r['thumbnail_url'] = $table['table']['record'][0]['thumbnail_url'];

					if($arr_new_winner['src_ip']) {
						$r['src_ip']=maskIP($arr_new_winner['src_ip']);
						$r['comefrom']=$r['src_ip'];
					}
					// error_log("[ajax/product/mk_history] new_winner :".json_encode($arr_new_winner));
					$r['winner']=$arr_new_winner['nickname'];
					$winner_name = $r['winner'];
					if(empty($r['winner'])) {
					   $r['winner']="";
					   $winner_name = "目前無人得標";
					   $status = 2; //websocket 狀態碼(1:更新得標者, 2:無人得標)
					}

					// $thumbnail_file = (empty($r['thumbnail_file'])) "":$r['thumbnail_file'];//空圖片 socket給空字串
					// $thumbnail_file = null;

					$r_bidded['name'] = $r['winner'];
					$r_bidded['status'] = $status;
					$r_bidded['thumbnail_file'] = $r['thumbnail_file'];
					$r_bidded['thumbnail_url'] = $r['thumbnail_url'];

					// 寫入redis
					$redis=getRedis('','','');
					if($redis) {
					   $redis->set("PROD:".$product['productid'].":BIDDER",json_encode($r_bidded));
					}
					// websocket更新得標人員
					$ts = time();
					$client = new WebSocket\Client("wss://ws.saja.com.tw:3334");
					$ary_bidder = array('actid'		=> 'BID_WINNER',
										'name' 		=> $winner_name,
										'productid' => "{$product['productid']}",
										'ts'		=> $ts,
										'status'	=> $status,
										'sign'		=> MD5($ts."|sjW333-_@"),
										'thumbnail_file' => $thumbnail_file ,
										'thumbnail_url'	=> $r['thumbnail_url']
								);
					$client->send(json_encode($ary_bidder));

					$diff_sec = $product['offtime']-time(); //大於30分鐘才執行推播
					if ($diff_sec>180) {
						$ori_userid = $arr_ori_winner['userid'];
						$new_userid = (empty($arr_new_winner['userid']))?"":$arr_new_winner['userid'];
						$post_array = array(
								"ori_userid" => $ori_userid,
								"new_userid" => $new_userid,
								"productid" => $product['productid']
							);
						// error_log("[ajax/product] push/after_saja : ".json_encode($post_array));
						// 更新得標者推播 20190819
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_URL, BASE_URL.APP_DIR."/push/after_saja");
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_array));
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						$result = curl_exec($ch);
						curl_close($ch);
					}

				} catch(Exception $e) {
					error_log("[ajax/product] updWinner exception : ".$e->getMessage());
					// 不提示
					// $r['total_num']=-1;
					// $r['rank']=-2;
				}
			}
		}
		error_log("[ajax/mk_history] r : ".json_encode($r));
		return $r;
	}

	public function saja_spoint_free_history($saja_fee, $spointid = 0, $productid = 0){

		global $db, $config;
		//確認是否有免費幣尚未用完
		$query="SELECT sum(free_amount) as free_amount
				FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint_free_history`
				WHERE userid = '{$this->userid}'
				AND amount_type ='1'
				AND switch =  'Y' ";

		// error_log("[ajax/product]range chk: ".$query);
		$chk = $db->getQueryRecord($query);

		//有未使用完的免費幣
		if($chk['table']['record'][0]['free_amount'] > 0){

			//免費幣餘額 大於等於 總下標費用，該筆金額全部視為使用免費幣
			if($chk['table']['record'][0]['free_amount'] >= $saja_fee){
				$free_amount = $saja_fee;
			}else{
				//免費幣餘額 小於 總下標費用，該筆金額部分視為免費幣
				$free_amount = $chk['table']['record'][0]['free_amount'];
			}

			//新增免費幣使用紀錄
			$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint_free_history`
			SET
				`userid`='{$this->userid}',
				`behav` = 'user_saja',
				`amount_type` = '1',
				`free_amount` = '".- $free_amount."',
				`total_amount` = '".- $saja_fee."',
				`productid` = '{$productid}',
				`spointid` = '{$spointid}',
				`insertt`=NOW()
			";
			$res = $db->query($query);

		}

	}
	
	//每天付費可下標數
	public function everyday_xspoint_count($product, $bid_count) {
		global $db, $config;
		if($product['everydaybid']!=0 ) {
			$day = date("Y-m-d"); 
			$query = "
			SELECT count(*) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND spointid!=0
				AND userid = '{$this->userid}'
				AND switch = 'Y'
				AND insertt between '{$day} 00:00:00' AND '{$day} 23:59:59'
			";
			$table = $db->getQueryRecord($query);
			$xspoint_count=(int)$table['table']['record'][0]['count'];
			
			if ($_POST['pay_type'] == 'spoint') {
				error_log("[ajax/product/everyday_xcode_count] xspoint_count: ".$xspoint_count." everydaybid => ".$product['everydaybid']." bid_count =>".$bid_count);

				if (($xspoint_count >= $product['everydaybid']) || ($bid_count > $product['everydaybid']) || ($bid_count > ($product['everydaybid']-$xspoint_count)) ) {
					return 0;
				} else {
					return 1;
				}
			}else{
				return 0;
			}
			
		}else{ 
			return 1;
		}
	}	
	
	//每天免費可下標數
	public function everyday_xcode_count($product, $bid_count) {
		global $db, $config;
		if($product['freepaybid']!=0) {
			$day = date("Y-m-d");
			$query = "
			SELECT count(*) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}' 
				AND productid = '{$product['productid']}' 
				AND spointid=0 
				AND sgiftid=0 
				AND userid = '{$this->userid}' 
				AND switch = 'Y' 
				AND insertt between '{$day} 00:00:00' AND '{$day} 23:59:59'
			";
			$table = $db->getQueryRecord($query);
			$xscode_count=(int)$table['table']['record'][0]['count'];
		
			if ($_POST['pay_type'] == 'oscode' || $_POST['pay_type'] == 'scode') {
				error_log("[ajax/product/everyday_xcode_count] xscode_count: ".$xscode_count." freepaybid => ".$product['freepaybid']." bid_count =>".$bid_count);
				if ($xscode_count >= $product['freepaybid'] || ($bid_count > $product['freepaybid']) || ($bid_count > ($product['freepaybid']-$xscode_count)) ) {
					return 0;
				} else {
					return 1;
				}
			}else{
				return 0;
			}
			
		} else { 
			return 1;
		}
	}
}
?>
