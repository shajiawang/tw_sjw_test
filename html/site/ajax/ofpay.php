<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");

$c = new Paysms;

$c->home();

class Paysms 
{
	public $str;
	
	public function home()
	{
		
		global $db, $config, $router;
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$ret=array();
		error_log("[ajax/ofpay/SMS] POST : ".json_encode($_POST));
		
		
		$phone = empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone']);
		$msg = empty($_POST['msg']) ? $_GET['msg'] : $_POST['msg'];
		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$key = empty($_POST['key']) ? htmlspecialchars($_GET['key']) : htmlspecialchars($_POST['key']);

		
		if (!empty($type) && (!empty($key))) {
			//判斷資料是否存在
			if ((!empty($phone)) && (!empty($msg))) {
				sendSMS($phone, $msg);
				$ret = getRetJSONArray(1,"资料發送成功!!",'JSON');
				echo json_encode($ret);
				error_log("[ajax/ofpay/SMS] POST : ".json_encode($ret));
			} else {
				$ret = getRetJSONArray(-1,"资料異常，無法發送!!",'JSON');
				echo json_encode($ret);
				error_log("[ajax/ofpay/SMS] POST : ".json_encode($ret));
				exit;
			}
		} else {
			$ret = getRetJSONArray(-1,"资格不符，無法發送!!",'JSON');
			echo json_encode($ret);
			error_log("[ajax/ofpay/SMS] POST : ".json_encode($ret));
			exit;		
		}	
	}	
	
}
?>
