// JavaScript Document

//限定S碼激活
function act_oscode() {
	var pageid = $.mobile.activePage.attr('id');
	var oscsn = $.trim($('#'+pageid+' #scsn').val()).toUpperCase();
	var oscpw = $('#'+pageid+' #scpw').val();

	if(oscsn == "" && oscpw == "" ) {
		alert('序號、密碼不能空白');
	} else {
		$.post(APP_DIR+"/ajax/scode.php?t="+getNowTime(),
		{type:'act_osc', scsn:oscsn, scpw:oscpw},
		function(str_json) {
			if(str_json) {
				var json = $.parseJSON(str_json);
				if (json.status==1) {
					alert('殺友請先登入');
					location.href=APP_DIR+"/member/userlogin/";
				} else if (json.status < 1) {
					alert('序號激活失敗');
				} else {
					var msg = '';
					if (json.status==200){
						alert('序號激活成功');
						location.reload();
					}
					else if(json.status==2){ alert('序號、密碼不能空白'); }
					else if(json.status==3){ alert('序號不存在'); }
					else if(json.status==4){ alert('序號密碼錯誤'); }
					else if(json.status==5){ alert('序號已激活'); }
				}
			}
		});
	}
}

//S碼激活
function act_scode() {
    esloading(1);   //loading start
	var pageid = $.mobile.activePage.attr('id');
	var oscsn = $('#'+pageid+' #scsn').val();
	var oscpw = $('#'+pageid+' #scpw').val();
    var recaptcha_response = $('#'+pageid+' #g-recaptcha-response').val();
    $.post(APP_DIR+"/ajax/scode.php?t="+getNowTime(),
    {type:'act', scsn:oscsn, scpw:oscpw, recaptcha_response:recaptcha_response},
    function(str_json)
    {
        if(str_json)
        {
            var json = $.parseJSON(str_json);
            esloading(0);   //loading end
            if (json.status==1) {
                createMsgModal('殺友請先登入','location.href=APP_DIR+"/member/userlogin/"','前往登入');
            } else if (json.status < 1) {
                createMsgModal('超級殺價券激活失敗','location.reload()','重新輸入');
            } else {
                var msg = '';
                if (json.status==200){
                    location.href=APP_DIR+"/scode/?status="+json.status;
                }else if(json.status==2){
                    createMsgModal('序號或密碼錯誤','location.reload()','重新輸入');
                }else if(json.status==3){
                    createMsgModal('序號或密碼錯誤','location.reload()','重新輸入');
                }else if(json.status==4){
                    createMsgModal('序號或密碼錯誤','location.reload()','重新輸入');
                }else if(json.status==5){
                    createMsgModal('此組序號已被使用','location.reload()','重新輸入');
                }else if(json.status==6){
                    createMsgModal('驗證失敗，請重新驗證','grecaptcha.reset()','重新輸入');
                }else if(json.status==7){
                    createMsgModal('此組序號已過期','location.reload()','重新輸入');
                }

            }
        }
    });
}
