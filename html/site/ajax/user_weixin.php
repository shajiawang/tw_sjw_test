<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");	
include_once(LIB_DIR ."/ini.php");

$c = new Weixin;

$c->home();

class Weixin
{
	public $str;

	public function home()	{

		global $db, $config, $router;
			
		// 初始化資料庫連結介面

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();

		$this->str = new convertString();
		$json = $_POST['client']['json'];
		$type = $_POST['type'];
		$ret = null;
	
		$query= "SELECT ssoid, uid 
				 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
		         WHERE `prefixid`='{$config['default_prefix_id']}'
				 AND `uid2` IS NOT NULL
				 AND `switch`='Y'
				";

		$table = $db->getQueryRecord($query);
					
		if (!empty($table['table']['record'])){

			foreach ($table['table']['record'] as $key => $value){
				//取得weixin的token資料
				$tokendata = json_decode(file_get_contents("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxbd3bb123afa75e56&secret=367c4259039bf38a65af9b93d92740ae"), true);
				$token = $tokendata['access_token'];
				$lang = "zh_CN";
				$openid = $value['uid'];

				//取得用戶資料
				$weixindata = file_get_contents("https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$token."&openid=".$openid."&lang=".$lang);
				
				$userdata = json_decode($weixindata, true);
				if (!empty($userdata['openid'])){
					// 修改 sso
					$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso` 
								SET	`sso_data`='{$weixindata}',
									`uid2`='{$userdata['unionid']}', 
									`modifyt`=NOW()
								WHERE `ssoid`='{$value['ssoid']}'
					";
					//$res = $db->query($query); 
					//查詢該用戶資料
					$data = $this->phonedata(trim($userdata['province']), trim($userdata['city']));
					
					error_log("[Weixin sso] data :".json_encode($data));
					
					$query= "SELECT userid 
								FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt`
								WHERE `prefixid`='{$config['default_prefix_id']}' 
								AND  `ssoid`='{$value['ssoid']}' ";
					$user = $db->getQueryRecord($query);
					error_log("[Weixin sso] user :".json_encode($user));

					// 修改 user_profile
					$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
								SET	`countryid`='{$data['countryid']}',
									`regionid`='{$data['regionid']}',
									`provinceid`='{$data['provinceid']}',
									`channelid`='{$data['channelid']}',
									`modifyt`=NOW()
								WHERE `userid`='{$user['table']['record'][0]['userid']}'";

					$res = $db->query($query);
					error_log("[Weixin sso] res :".$user['table']['record'][0]['userid']);
					
				}else{
					error_log("[Weixin sso] uid ".$openid.": nodata");
				}
				
			}
		}
		
	}
	
	//查詢weixin用戶的相關資料
	public function phonedata($province, $city)
	{
		global $db, $config;
		$query = "SELECT pr.provinceid, pr.regionid, re.countryid
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` pr 
		left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}region` re 
				on pr.prefixid = re.prefixid 
				and pr.regionid = re.regionid
				and pr.switch = 'Y'
		WHERE
			pr.prefixid = '{$config['default_prefix_id']}'
			AND pr.switch = 'Y'
			AND pr.`name` = '{$province}'			
		";
		$table = $db->getQueryRecord($query);

		if (!empty($table)){
			$ret['provinceid'] = $table['table']['record'][0]['provinceid'];
			$ret['regionid'] = $table['table']['record'][0]['regionid'];
			$ret['countryid'] = $table['table']['record'][0]['countryid'];
		}else{
			$ret['countryid'] = $config['country'];
			$ret['regionid'] = $config['region'];
			$ret['provinceid'] = $config['province'];
			$ret['channelid'] = $config['channel'];
			
			return $ret;
			exit;	
		}
		
		$query = "SELECT *
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel` 
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND provinceid = '{$ret['provinceid']}'
			AND name = '{$city}'
			AND switch = 'Y'					
		";
		$table2 = $db->getQueryRecord($query);
		
		if (empty($table2['table']['record'][0]['channelid'])){
			$query = "
			INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel`
			SET	`prefixid`='{$config['default_prefix_id']}',
				`countryid`='{$ret['countryid']}',
				`provinceid`='{$ret['provinceid']}',
				`name`='{$city}',
				`description`='{$province}全區',
				`seq`=0,
				`switch`='Y', 
				`insertt`=NOW()
			";
			
			//$db->query($query);
			//$ret['channelid'] = $db->_con->insert_id;
			$ret['channelid'] = $config['channel'];

		}else{
			$ret['channelid'] = $table2['table']['record'][0]['channelid'];		
		}
		
		return $ret;
	}
}
?>
