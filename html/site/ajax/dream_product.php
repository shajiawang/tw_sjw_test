<?php
session_start();

$time_start = microtime(true);

$time_end = microtime(true);
$time = number_format(($time_end - $time_start),10);
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
   // error_log($k."=>".$v);
	foreach($_POST2 as $k=> $v) {
		$_POST[$k]=$v;
		if($_POST['json']) {
			$_POST['json']=$_POST['json'];
		}
		if($_POST['auth_id']) {
			$_POST['userid']=$_POST['auth_id'];
		}
	}
}

$ret=null;
$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json'];
error_log("[ajax/dream_product] POST : ".json_encode($_POST));
error_log("[ajax/dream_product] GET : ".json_encode($_GET));

if(empty($_SESSION['auth_id']) && empty($_POST['auth_id']) && empty($_POST['root'])) { //'請先登入會員帳號'

	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';

	} else {

	   $ret['status'] = 1;
	}

	echo json_encode($ret);
	exit;

} else {

	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	// include_once(LIB_DIR ."/jpush.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");
	include_once(LIB_DIR ."/ini.php");
	include_once(BASE_DIR ."/model/user.php");

	require(LIB_DIR."/vendor/autoload.php"); // 載入wss client 套件(composer)

 	// include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");
    // require_once("/var/www/lib/vendor/autoload.php");
	//$app = new AppIni;

    // 延遲一點時間, 0.05 ~ 0.3 秒不等
    $sleep = getRandomNum()%6+1;
    // error_log("sleep: ".$sleep*500000);
	usleep($sleep*50000);

	$c = new DreamProductBid;
	if($_POST['bid_type'] == 1){
		$c->dream_saja();
	}else{
		$c->saja();
	}
}

// use WebSocket\Client;

class DreamProductBid {
	public $userid = '';
	public $countryid = 1;//國家ID
	public $currency_unit = 1;//貨幣最小單位
	public $last_rank = 11;//預設順位提示（順位排名最後一位）
	public $saja_time_limit = 0;//每次下標時間間隔限制
	// public $display_rank_time_limit = 60;//（秒）下標後離結標還有n秒才顯示順位
	public $display_rank_time_limit = 0;//（秒）下標後離結標還有n秒才顯示順位
	public $range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標
	public $tickets = 1;
	public $price;
	public $username='';

	//圓夢商品下標流程(下標金額以array格式傳入)
	public function dream_saja(){

		$time_start = microtime(true);

		global $db, $config, $router, $user;

        error_log("[ajax/dream_product] dream_saja ");
		$user = new UserModel;

		$JSON=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$bid_list=(empty($_POST['bid_list'])) ? htmlspecialchars_decode($_GET['bid_list']) : htmlspecialchars_decode($_POST['bid_list']);
		$all_price=(empty($_POST['all_price'])) ? htmlspecialchars_decode($_GET['all_price']) : htmlspecialchars_decode($_POST['all_price']);

		$bid_list = json_decode($bid_list, true);
		$all_price = json_decode($all_price, true);

		//login_required();
		if ($JSON == 'Y') {
        	$this->userid = htmlspecialchars($_POST['auth_id']);
			$this->username=  htmlspecialchars($_POST['auth_nickname']);
        }else{
        	$this->userid = $_SESSION['auth_id'];
			$this->username= $_SESSION['user']['profile']['nickname'];
        }

		if(empty($this->userid) && $_POST['root'] == 'weixin' && !empty($_POST['userid'])) {
			$this->userid = $_POST['userid'];
		}

		// 如果沒有用戶編號 不給下標
		if(empty($this->userid)) {
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-2,'NO_USER_ID','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}
		
		// Add By Thomas 2020/01/31 
		// 未填寫收件人姓名/郵遞區號/地址/電話　不給下標
		/*
		$db = new mysql($config["db"]);
		$db->connect();
		$query ="SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND switch = 'Y' ";
		$table = $db->getQueryRecord($query);
		// error_log("[ajax/dream_product] dream_saja 111111");
		$user_profile = $table['table']['record'][0];
		*/
        $user_profile = $user->get_user_profile($this->userid);
		if($user_profile && $user_profile['nickname']) {	
		   $this->username = $user_profile['nickname'];
		} else {
		   if($JSON=='Y') {
			  error_log(json_encode(getRetJSONArray(-3,'NO_USER_DATA','MSG')));
              echo json_encode(getRetJSONArray(-3,'NO_USER_DATA','MSG'));
           } else {
              // ToDo Web Error Page
           }
           return ;
		}
		//error_log("[ajax/dream_product] dream_saja 222222");
		if(empty($user_profile['address']) || empty($user_profile['addressee']) || empty($user_profile['rphone'])) {
			 if($JSON=='Y') {
				 error_log(json_encode(getRetJSONArray(-14,"下標前需先詳填收件人資料 !",'MSG')));
				 echo json_encode(getRetJSONArray(-14,"下標前需先詳填收件人資料 !",'MSG'));
			 } else {
                 // ToDo Web Error Page
             }	
             return;			 
		}
		// error_log("[ajax/dream_product] dream_saja 333333");
		// 如果沒有商品編號  不給下標
		if(empty($_POST['productid'])) {
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-3,'NO_PRODUCT_DATA','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}

		//沒有圓夢商品標題 不給下標
		if(empty($_POST['title'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-4,'NO_TITLE','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}

		//沒有圓夢商品內文 不給下標
		if(empty($_POST['content'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-5,'NO_CONTENT','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}

		//沒有圓夢商品購買連結 不給下標
		if(empty($_POST['purchase_url'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-6,'NO_URL','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}
		// 新版驗證簽名 20191021
		$uts = (empty($_POST['uts'])) ? htmlspecialchars($_GET['uts']) : htmlspecialchars($_POST['uts']);
		$first_price  = (empty($_POST['first_price'])) ? htmlspecialchars($_GET['first_price']) : htmlspecialchars($_POST['first_price']);
		if(empty($first_price)) {
		   // bid_list : "[{\"price\":{\"end\":\"10000\",\"start\":\"9998\"},\"type\":\"range\"}]"
		   $bid_list = str_replace("]","",str_replace("[","",$_POST['bid_list']));
		   $arrBid=json_decode($bid_list,true);
		   $first_price=$arrBid['price']['start'];
		   error_log("[ajax/dream_product] first_price from all_price : ".$first_price);		   
		}
		$sign  = (empty($_POST['sign'])) ? htmlspecialchars($_GET['sign']) : htmlspecialchars($_POST['sign']);
		$mysign = MD5($this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts.'|'.'sjW333-_@');
		error_log("[ajax/dream_product/dream_saja] APP sign: ".$sign);
		error_log("[ajax/dream_product/dream_saja] SERVER sign decode: ".$this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts);
		error_log("[ajax/dream_product/dream_saja] SERVER sign : ".$mysign);
		if($JSON=='Y') {
			if (empty($sign) || empty($mysign) || $sign!=$mysign) {
				error_log("[ajax/product] : Sign Check Failed !!");
				echo json_encode(getRetJSONArray(-20,'下標驗簽失敗','MSG'));
				exit;
			}
		}

		//沒有圓夢商品圖檔 不給下標
		if(empty($_POST['pic']) || empty($_POST['filedir']) || empty($_POST['pic_name'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-7,'NO_PIC_DATA','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}else{
			$filedir = $_POST['filedir'];   //檔案子目錄路徑
			$img = $_POST['pic'];   //圖檔
			$pic_name = $_POST['pic_name'];   //圖檔名稱

			if($_POST['pic_type'] == 0){
				//尋找檔案內是否含png檔頭
				$img_format_check = strpos($img,'data:image/png;base64,');
				if ($img_format_check === false) {
					echo json_encode(getRetJSONArray(-8,'PIC_DATA_WRONG','MSG'));
				}
				//去除png檔頭取得純圖檔資料 需注意 data url 格式 與來源是否相符 ex:image/png
				$img = str_replace('data:image/png;base64,','', $img);
				//過濾圖檔資料裡的換行符號
				$img = preg_replace("/\s+/", '', $img);
				if (!(base64_encode(base64_decode($img, true)) === $img)){
					if($JSON=='Y') {
						echo json_encode(getRetJSONArray(-9,'PIC_DATA_WRONG','MSG'));
					}
				}
			}
		}

		$db = new mysql(getDbConfig());
        $product = "";

		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['status'] = 0;

		// 先從Cache中找商品資料
		$cache = getRedis('','');
		if($cache) {
			$jsonStr = $cache->get('getProductById:'.$_POST['productid'].'|Y|Y');
			if($jsonStr) {
				$product = json_decode($jsonStr,TRUE);
			}
		}
		// 如果Cache中沒有資料  則撈DB
		if(empty($product) || !is_array($product) || empty($product['productid'])) {
			//讀取商品資料
			$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			WHERE  p.prefixid = '{$config['default_prefix_id']}'
				AND p.productid = '{$_POST['productid']}'
				AND p.switch = 'Y'
			LIMIT 1
			";
			$table = $db->getQueryRecord($query);
			unset($table['table']['record'][0]['description']);
			$product = $table['table']['record'][0];
		}

		if(!is_array($product) || empty($product['productid'])) {
            if($JSON=='Y') {
              echo json_encode(getRetJSONArray(-10,'NO_PRODUCT_DATA','MSG'));
           } else {
              // ToDo Web Error Page
           }
           return ;
		}

        // 更新檢查的時間
        $product['now']=time();
		if(($product['offtime'] == 0 && $product['locked'] == 'Y') || ($product['offtime'] > 0 && $product['now'] > $product['offtime'])) {
			//回傳: 本商品已結標
			$ret['status'] = 2;
			if($JSON=='Y') {
				$ret=getRetJSONArray(-11,'本商品已結標!!','MSG');
				echo json_encode($ret);
				exit;
			}
		}

		/* 沒有用戶暱稱, 去DB撈 ?
		if(empty($this->username) && !empty($this->userid)) {
			$query ="SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$this->username = $table['table']['record'][0]['nickname'];
		}
		*/
		

		//下標資格
		$chk = $this->chk_saja_rule($product);
		if($chk['err'] && $chk['err']>0) {
			if($JSON=='Y') {
				$ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],"MSG");
				echo json_encode($ret);
				exit;
			}else {
				$ret['status'] = $chk['err'];
				$ret['value'] = $chk['value'];
				$ret['retCode'] = $chk['retCode'];
				$ret['retMsg'] = isset($chk['retMsg']) ? $chk['retMsg'] : '';
			}
		}

		//本次下標數
		$this->tickets = count($all_price);

		$chk_price = $this->chk_bid_price($product, $bid_list, $all_price);

		if($chk_price['err'] && $chk_price['err'] > 0) {
			if($JSON=='Y') {
				// $ret=getRetJSONArray($chk_price['retCode'],$chk_price['retMsg'],$chk_price['retType']);
				$ret=getRetJSONArray($chk_price['retCode'],$chk_price['retMsg'],"MSG");
				error_log("[ajax/dream_product] dream_saja : ".json_encode($ret));
				echo json_encode($ret);
				exit;
			}else {
				$ret['status'] = $chk_price['err'];
				$ret['value'] = $chk_price['value'];
				$ret['retCode'] = $chk_price['retCode'];
			}
		}

		if(empty($ret['status']) || $ret['status']==0 ) {

			//產生下標歷史記錄
			//$mk = $this->mk_history($product);
			$mk = $this->mk_bid_history($product, $all_price, $bid_list);

			//回傳: 下標完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 1;
			if($mk['err']) {
				// 有異常
				if($JSON=='Y') {
				   $ret=getRetJSONArray($mk['retCode'],$mk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				}
			}

			$ret['retCode'] = 1;
			$ret['retMsg'] = '出價成功';
			$ret['result_msg'] = $mk['result_msg'];
			$ret['result_msg2'] = "*因下標人數眾多，下標情況變化大，此提示只為當下結果，僅供參考。";
			$ret['result_type'] = $mk['result_type'];
			$ret['retType'] = "JSON";
		}
		error_log("[ajax/dream_product] saja : ".json_encode($ret));
		$db=null;
        echo json_encode($ret);
		exit;
		///////////////////////////////////////////////////////////////////////////////////////////
	}
	//下標
	public function saja() {
		$time_start = microtime(true);

		global $db, $config, $router, $user;

		// 初始化資料庫連結介面
		// if($db) {
		// error_log("[ajax/product] connect DB ...");
  		// $db = new mysql($config["db"]);
		  // $db->connect();
		// }
        error_log("[ajax/dream_product] saja ");
		$user = new UserModel;
		// $JSON=$_POST['json'];
        $JSON=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		// $router = new Router();

		login_required();
		if ($JSON == 'Y') {
        	$this->userid = htmlspecialchars($_POST['auth_id']);
			$this->username=  htmlspecialchars($_POST['auth_nickname']);
        }else{
        	$this->userid = $_SESSION['auth_id'];
			$this->username= $_SESSION['user']['profile']['nickname'];
        }

		if(empty($this->userid) && $_POST['root'] == 'weixin' && !empty($_POST['userid'])) {
			$this->userid = $_POST['userid'];
		}

        // 如果沒有用戶編號 不給下標
        if(empty($this->userid)) {
           if($JSON=='Y') {
              echo json_encode(getRetJSONArray(-1,'NO_USER_ID','MSG'));
           } else {
              // ToDo Web Error Page
           }
           return ;
		}
		
		// Add By Thomas 2020/01/31 
		// 未填寫收件人姓名/郵遞區號/地址/電話　不給下標
		$db = new mysql($config["db"]);
		$db->connect();
		$query ="SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$this->userid}'
				AND switch = 'Y' ";
		$table = $db->getQueryRecord($query);
		$user_profile = $table['table']['record'][0];
        if($user_profile && $user_profile['nickname']) {
		   $this->username = $user_profile['nickname'];
		} else {
		   if($JSON=='Y') {
              echo json_encode(getRetJSONArray(-2,'NO_USER_DATA','MSG'));
           } else {
              // ToDo Web Error Page
           }
           return ;
		}
        if(empty($user_profile['address']) || empty($user_profile['addressee']) || empty($user_profile['rphone'])) {
			 if($JSON=='Y') {
				 echo json_encode(getRetJSONArray(-14,"下標前需先詳填收件人資料 !",'MSG'));
			 } else {
                 // ToDo Web Error Page
             }	
             return;
		}

        // 如果沒有商品編號  不給下標
        if(empty($_POST['productid'])) {
           if($JSON=='Y') {
              echo json_encode(getRetJSONArray(-3,'NO_PRODUCT_DATA','MSG'));
           } else {
              // ToDo Web Error Page
           }
           return ;
		}


		//沒有圓夢商品標題 不給下標
		if(empty($_POST['title'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-6,'NO_TITLE','MSG'));
			 } else {
				// ToDo Web Error Page
			 }
			 return ;
		}

		//沒有圓夢商品內文 不給下標
		if(empty($_POST['content'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-7,'NO_CONTENT','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}
		//沒有圓夢商品購買連結 不給下標
		if(empty($_POST['purchase_url'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-8,'NO_URL','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}

		// 新版驗證簽名 20191021
		$uts = (empty($_POST['uts'])) ? htmlspecialchars($_GET['uts']) : htmlspecialchars($_POST['uts']);
		$first_price  = (empty($_POST['first_price'])) ? htmlspecialchars($_GET['first_price']) : htmlspecialchars($_POST['first_price']);
		$sign  = (empty($_POST['sign'])) ? htmlspecialchars($_GET['sign']) : htmlspecialchars($_POST['sign']);
		$mysign = MD5($this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts.'|'.'sjW333-_@');
		error_log("[ajax/dream_product/saja] APP sign: ".$sign);
		error_log("[ajax/dream_product/saja] SERVER sign decode: ".$this->userid.'|'.$_POST['productid'].'|'.$first_price.'|'.$uts);
		error_log("[ajax/dream_product/saja] SERVER sign : ".$mysign);
		if($JSON=='Y') {
			if (empty($sign) || empty($mysign) || $sign!=$mysign) {
				error_log("[ajax/product] : Sign Check Failed !!");
				echo json_encode(getRetJSONArray(-20,'下標驗簽失敗','MSG'));
				exit();
			}
		}

		//沒有圓夢商品圖檔 不給下標
		if(empty($_POST['pic']) || empty($_POST['filedir']) || empty($_POST['pic_name'])){
			if($JSON=='Y') {
				echo json_encode(getRetJSONArray(-9,'NO_PIC_DATA','MSG'));
			} else {
				// ToDo Web Error Page
			}
			return ;
		}else{
			$filedir = $_POST['filedir'];   //檔案子目錄路徑
			$img = $_POST['pic'];   //圖檔
			$pic_name = $_POST['pic_name'];   //圖檔名稱

			if($_POST['pic_type'] == 0){
				//尋找檔案內是否含png檔頭
				$img_format_check = strpos($img,'data:image/png;base64,');
				if ($img_format_check === false) {
					echo json_encode(getRetJSONArray(-10,'PIC_DATA_WRONG','MSG'));
				}
				//去除png檔頭取得純圖檔資料 需注意 data url 格式 與來源是否相符 ex:image/png
				$img = str_replace('data:image/png;base64,','', $img);
				//過濾圖檔資料裡的換行符號
				$img = preg_replace("/\s+/", '', $img);
				if (!(base64_encode(base64_decode($img, true)) === $img)){
					if($JSON=='Y') {
						echo json_encode(getRetJSONArray(-11,'PIC_DATA_WRONG','MSG'));
					}
				}
			}
		}

        $db = new mysql(getDbConfig());
        $product = "";

		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['status'] = 0;
		$ret['winner'] = '';
		$ret['rank'] = '';
		$ret['value'] = 0;

        // 先從Cache中找商品資料
        $cache = getRedis('','');
        if($cache) {
           $jsonStr = $cache->get('getProductById:'.$_POST['productid'].'|Y|Y');
           if($jsonStr) {
              $product = json_decode($jsonStr,TRUE);
           }
        }
        // 如果Cache中沒有資料  則撈DB
        if(empty($product) || !is_array($product) || empty($product['productid'])) {
            //讀取商品資料
            $query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
            FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
            WHERE  p.prefixid = '{$config['default_prefix_id']}'
              AND p.productid = '{$_POST['productid']}'
              AND p.switch = 'Y'
            LIMIT 1
            ";
            $table = $db->getQueryRecord($query);
            unset($table['table']['record'][0]['description']);
            $product = $table['table']['record'][0];
        }

        if(!is_array($product) || empty($product['productid'])) {
            if($JSON=='Y') {
              echo json_encode(getRetJSONArray(-5,'NO_PRODUCT_DATA','MSG'));
           } else {
              // ToDo Web Error Page
           }
           return ;
        }

        // 更新檢查的時間
        $product['now']=time();
		if(($product['offtime'] == 0 && $product['locked'] == 'Y') || ($product['offtime'] > 0 && $product['now'] > $product['offtime'])) {
			//回傳: 本商品已結標
			$ret['status'] = 2;
			if($JSON=='Y') {
				$ret=getRetJSONArray(-2,'本商品已結標!!','MSG');
				echo json_encode($ret);
				exit;
			}
		}

        // 沒有用戶暱稱, 去DB撈 ?
        if(empty($this->username) && !empty($this->userid)) {
			$query ="SELECT nickname
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			WHERE prefixid = '{$config['default_prefix_id']}'
			  AND userid = '{$this->userid}'
			  AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			$this->username = $table['table']['record'][0]['nickname'];
		}
        //
        //下標資格
        // 閃殺不檢測 By Thomas 20190102
		//if ($this->userid != 1705) {
			$chk = $this->chk_saja_rule($product);
			if ($chk['err'] && $chk['err']>0) {
				if($JSON=='Y') {
					$ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],$chk['retType']);
					echo json_encode($ret);
					exit;
				} else {
					$ret['status'] = $chk['err'];
					$ret['value'] = $chk['value'];
					$ret['retMsg'] = isset($chk['retMsg']) ? $chk['retMsg'] : '';
				}
			}
		//}
        

		if($_POST['type']=='single') { //單次下標
			$chk = $this->chk_single($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   error_log("[ajax/dream_product] saja chk_single : ".json_encode($ret));
				   echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
			    }
			}
		} else if($_POST['type']=='range' ) { //區間連續下標
			$chk = $this->chk_range($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   error_log("[ajax/dream_product] saja chk_range : ".json_encode($ret));
			       echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
				}
			}
		} else if($_POST['type']=='lazy') { //懶人下標
			$chk = $this->chk_range($product,$_POST['pay_type']);
			if($chk['err'] && $chk['err']>0 ) {
				if($JSON=='Y') {
				   $ret=getRetJSONArray($chk['retCode'],$chk['retMsg'],'MSG');
				   error_log("[ajax/dream_product] saja chk_lazy : ".json_encode($ret));
				   echo json_encode($ret);
				   exit;
			    } else {
				   $ret['status'] = $chk['err'];
				}
			}
		}

        // 隨機產生出價(僅供非正式機的壓力測試用)
        if(BASE_URL!="https://www.saja.com.tw" && $_REQUEST['random_price']=='Y') {
           $this->price = getRandomNum(1,$product['retail_price']);
           $this->username.= $this->price;
        }

		if(empty($ret['status']) || $ret['status']==0 ) {

			//產生下標歷史記錄
			$mk = $this->mk_history($product);

			//回傳: 下標完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			if($mk['err']) {
				// 有異常
				if($JSON=='Y') {
				   $ret=getRetJSONArray($mk['retCode'],$mk['retMsg'],'MSG');
				   echo json_encode($ret);
				   exit;
				}
			}

			// 正常下標
			if($JSON=='Y') {
			    $ret['retObj']=array();
			    $ret['retObj']['winner'] = $mk['winner'];
				$ret['retObj']['rank'] = $mk['rank'];
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				//單次下標
				if($_POST['type']=='single') {
					if ($mk['rank']==-2) {
						$msg = '最後一分鐘下標不提示排名或重複 ！';
					} else if ($mk['rank']==-1) {
						$msg = '很抱歉！這個出價跟其他人重複了！';
					} else if($mk['rank']==0) {
						$msg = '恭喜！您是目前的得標者！';
					} else if ($mk['rank']>=1 && $mk['rank']<=10) {
						$msg = '恭喜！此次出價是唯一但不是最低,目前是唯一又比你低的還有 '.($mk['rank']).' 位。';
					} else if ($mk['rank']>10) {
						$msg = '恭喜！此次出價是唯一但不是最低,目前是唯一又比你低的超過 10 位。';
					}
					$ret['retMsg'] = '單次出價 '.$_POST['price'].'元 成功 '. $msg;
				} else if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['retObj']['total_num'] = $mk['total_num'];
					$ret['retObj']['num_uniprice'] = $mk['num_uniprice'];
					$ret['retObj']['lowest_uniprice'] = $mk['lowest_uniprice'];
					if($mk['total_num']>=2) {
						if($mk['num_uniprice']>0) {
						   $msg ='此次出價有 '.$mk['num_uniprice'].' 個是唯一價, 其中最低的是 '.$mk['lowest_uniprice'].' ';
						} else {
						   $msg ='本次下標的出價都與其他人重複了 !';
						}
					}
					$ret['retMsg'] = '連續出價 '.$_POST['price_start'].'至'.$_POST['price_stop'].'元 成功 !'.$msg;
				}
			} else {
				$ret['winner'] = $mk['winner'];
				$ret['rank'] = $mk['rank'];
				if($_POST['type']=='range' || $_POST['type']=='lazy') {
					$ret['total_num'] = $mk['total_num'];
					$ret['num_uniprice'] = $mk['num_uniprice'];
					$ret['lowest_uniprice'] = $mk['lowest_uniprice'];
				}
			}
		}
		error_log("[ajax/product] saja : ".json_encode($ret));
		$db=null;
        echo json_encode($ret);
	}

	//下標資格
	public function chk_saja_rule($product)	{
		global $db, $config;

		$limitid = $product['limitid'];

		$query = "SELECT * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited`
		where
			plid = '{$limitid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		$kind=$table['table']['record'][0]['kind'];
		$mvalue=$table['table']['record'][0]['mvalue'];
		$svalue=$table['table']['record'][0]['svalue'];

		$r['err'] = '';
		$r['value'] = 0;
		$count = $this->pay_get_product_count($this->userid);

		if( $kind=='small') {		//小於
			if ($count >= $mvalue) {
				$r['err'] = 16;
				$r['retCode']=-100216;
				$r['retMsg']='未符合下標資格!!';
				$r['value'] = $mvalue;
			}
		} else if($kind=='big') {	//大於
			if($count <= $svalue) {
				$r['err'] = 16;
				$r['retCode']=-100216;
				$r['retMsg']='未符合下標資格!!';
				$r['value'] = $svalue;
			}
		}

		// 新手限定 一次一標
		if ($limitid == 7 || $limitid ==21) {
			$query = "SELECT
						h.productid,
						p.name,
						h.userid,
						h.nickname
					FROM
						`{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON h.productid = p.productid
						AND p.switch = 'Y'
					WHERE
						h.switch = 'Y'
					AND p.limitid = '{$limitid}'
					AND (
						p.offtime > now()
						OR p.closed = 'N'
					)
					AND h.userid = '{$this->userid}'
					AND p.productid <> '{$product['productid']}'
					AND p.ptype = '{$product['ptype']}'
					GROUP BY
						h.productid";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])) {
				$r['err'] = 21;
				$r['retCode']=-100234;
				$r['retMsg']='已有下標其他新手限定商品';
			}
		}
		
		//查詢下標 狀態 AARONFU
		$bid_able = query_bid_able($this->userid);
		if($bid_able["dream_enable"] !=='Y') {
			$r['err'] = 23;
			$r['retCode']=-100236;
			$r['retMsg']='下標功能鎖定中,請洽客服';
		}
		
		return $r;
	}

	//得標次數限制
	public function pay_get_product_count($userid) {
		global $db, $config;

		$query = "SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
		where
			`prefixid` = '{$config['default_prefix_id']}'
			AND userid = '{$userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		return $table['table']['record'][0]['count'];
	}

	// 連續下標
	public function chk_range($product,$pay_type) {
		$time_start = microtime(true);

		global $db, $config;

		$r['err'] = '';
        // 檢查是否結標時, 不可用DB撈出的時間
        $osq = 0;
		//檢查殺價券數量
		if ($pay_type=='oscode'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
		}
		if(empty($_POST['price_start']) || empty($_POST['price_stop'])) {
		    //'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($_POST['price_start']) || !is_numeric($_POST['price_stop'])) {
		    //'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((int)$_POST['price_start'] < (int)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif($product['offtime'] > 0 && ($product['offtime'] - time() < $this->range_saja_time_limit)) {
		    //'超過可連續下標時間'
			$r['err'] = 7;
			$r['retCode']=-100227;
			$r['retMsg']='超過可連續下標時間!';
		} elseif((int)$_POST['price_start'] > (int)$_POST['price_stop']) {
		    //'起始價格不可超過結束價格'
			$r['err'] = 9;
			$r['retCode']=-100229;
			$r['retMsg']='起始價格不可超過結束價格!';
		} elseif((int)$_POST['price_stop'] > (int)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif((int)$_POST['price_start'] == (int)$_POST['price_stop']) {
		    //'起始價格不可超過結束價格'
			$r['err'] = 12;
			$r['retCode']=-100232;
			$r['retMsg']='出價起訖區間不可相同!';
		}

        //本次下標數
		$this->tickets = ((int)$_POST['price_stop'] - (int)$_POST['price_start']) / $this->currency_unit + 1;
		error_log("[ajax/product] 本次下標數：".$this->tickets);
		if(((int)$product['user_limit'] > 0) && ($this->chk_user_limit($product['user_limit'], $product['productid'],$pay_type,$osq))) {
		    //超過可下標總次數限制
			$r['err'] = 11;
			$r['retCode']=-100231;
			$r['retMsg']='超過下標總次數限制!';
		}

		//起始價格
		$this->price = $_POST['price_start'];
		if($this->tickets > (int)$product['usereach_limit']) {
		    //'超過單次可連續下標次數限制'
			$r['err'] = 8;
			$r['retCode']=-100228;
			$r['retMsg']='超過可連續下標次數限制!';
		}
		return $r;
	}

	/* 取得目前可用的超殺卷數*/
	public function get_vaild_code($uid){
		global $db, $config;
		$query = "SELECT userid, sum(remainder) as amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			WHERE
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND behav != 'a'
				AND offtime > NOW()
				AND closed = 'N'
				AND switch = 'Y'
			";
		$table1 = $db->getQueryRecord($query);
		$get_scode=0;
		if(!empty($table1['table']['record'])) {
			$get_scode = (int)$table1['table']['record'][0]['amount'];
		}
		return $get_scode;//+$use_sum;
	}

	// 單次下標
	public function chk_single($product,$pay_type) {
		$time_start = microtime(true);

		global $db, $config;

		//殺價記錄數量
		$this->tickets = 1;

		//殺價起始價格
		$this->price = $_POST['price'];
        $osq = 0;
		//檢查殺價券數量
		if ($pay_type=='oscode'){
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
		}
        $r['err'] = '';

		if(empty($this->price)) {
		    //'金額不可空白'
			$r['err'] = 4;
			$r['retCode']=-100224;
			$r['retMsg']='金額不可空白!';
		} elseif(!is_numeric($this->price)) {
		    //'金額格式錯誤'
			$r['err'] = 5;
			$r['retCode']=-100225;
			$r['retMsg']='金額格式錯誤!';
		} elseif((float)$this->price < (float)$product['price_limit']) {
		    //'金額低於底價'
			$r['err'] = 6;
			$r['retCode']=-100226;
			$r['retMsg']='金額低於底價!';
		} elseif((float)$this->price > (float)$product['retail_price']) {
		    //'金額超過商品市價'
			$r['err'] = 10;
			$r['retCode']=-100230;
			$r['retMsg']='金額超過商品市價!';
		} elseif((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid'],$pay_type,$osq) ) {
		    //'超過可下標次數限制'
			$r['err'] = 11;
			$r['retCode']=-100231;
			$r['retMsg']='超過可下標次數限制!';
		}

		$time_end = microtime(true);
		$time = number_format(($time_end - $time_start),10);
		//echo "單次下標 $time 秒<br>";
		return $r;
	}

	//檢查使用者下標次數限制
	public function chk_user_limit($user_limit, $pid,$pay_type,$osq) {
		$time_start = microtime(true);

		global $db, $config;
		/*兩種卷合併記次*/

		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$pid}'
			AND spointid>0
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);
		$spoint_count=(int)$table['table']['record'][0]['count'];

		$query = "
		SELECT count(*) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND productid = '{$pid}'
			AND spointid=0
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$code_limit=$user_limit;
		$table = $db->getQueryRecord($query);
		$oscode_count=(int)$table['table']['record'][0]['count'];
		if ($pay_type=='spoint'){
			$tickets_avaiable = (int)$user_limit -$spoint_count;
			//下幣且幣的次數不夠
			if((int)$this->tickets > $tickets_avaiable) return 1;
		}else{
			//如果下的是超殺則去拉超殺的次數
			if ($pay_type=='scode') $osq=$this->get_vaild_code($this->userid);
			//比如限制是 10次  已用殺價券4次 尚有超沙7張
			if ($oscode_count==0){
				//取卷的剩餘數或下標卷次數較低者
				$oscode_limit=($osq<$user_limit)?$osq:$user_limit;
				$tickets_avaiable = (int)$oscode_limit - $oscode_count;
			}else{
				$user_limit=$user_limit-$oscode_count;
				$tickets_avaiable=($osq<$user_limit)?$osq:$user_limit;
			}
			//下卷且卷的次數不夠則看幣還有沒有剩($user_limit-$spoint_count);
			if((int)$this->tickets > $tickets_avaiable) {
				if((int)$this->tickets >($tickets_avaiable+$user_limit-$spoint_count)) return 1;
			}
		}
		return 0;
	}

	//檢查最後下標時間
	public function chk_time_limit() {
		$time_start = microtime(true);

		global $db, $config;

		$query = "
		SELECT unix_timestamp(MAX(insertt)) as insertt, unix_timestamp() as `now`
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND userid = '{$this->userid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			if( (int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $this->saja_time_limit) {
				return 1;
			}
		}
		return 0;
	}

    public  function getBidWinner($productid) {
            global $db, $config;
            $ret=array(
               'nickname'=>'',
               'name'=>'',
               'userid'=>'',
               'productid'=>'',
               'price'=>0,
               'src_ip'=>''
            );
            $query="SELECT sh.userid, sh.productid, sh.nickname, sh.price, shd.thumbnail_filename as shd_thumbnail_file
                   	FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` shd
					ON sh.shdid = shd.shdid
                  	WHERE sh.prefixid = '{$config['default_prefix_id']}'
                    AND sh.productid = '{$productid}'
                    AND sh.switch = 'Y'
                    AND sh.type = 'bid'
                    AND sh.userid IS NOT NULL
					AND sh.ptype = 1
                    GROUP BY sh.price
                    HAVING COUNT(sh.price) = 1
                    ORDER BY sh.price ASC
                    LIMIT 1 ";
            $table = $db->getQueryRecord($query);

            // error_log("[ajax/product] sql : ".$query);

            if($table['table']['record'][0] && $table['table']['record'][0]['price']>0) {
                $ret=array();
                $ret['userid'] = $table['table']['record'][0]['userid'];
                $ret['nickname'] = $table['table']['record'][0]['nickname'];
                $ret['name'] = $table['table']['record'][0]['nickname'];
                $ret['productid'] = $table['table']['record'][0]['productid'];
				$ret['price'] = $table['table']['record'][0]['price'];
				$ret['shd_thumbnail_file'] = $table['table']['record'][0]['shd_thumbnail_file'];
             } else {
                $ret = false;
            }
            error_log("[ajax/dream_product/getBidWinner] : ".json_encode($ret));
            return $ret;
	}

    /*
	//得標者資訊
	public function getBidWinner($productid) {
		global $db, $config;
		// 得標者
		$ret='';
		$query="SELECT h.userid, h.productid, h.nickname, h.price, h.src_ip ".
			  ",(SELECT name from `{$config['db'][2]["dbname"]}`.`{$config['default_prefix']}province` WHERE switch='Y' AND provinceid=up.provinceid LIMIT 1) as src_province ".
			  ",(SELECT name from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` WHERE switch = 'Y' AND provinceid=up.provinceid) as comefrom ".
			  ",(SELECT name from `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}product` WHERE productid=h.productid LIMIT 1) as prodname ".
			  ",(SELECT uid from `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}sso` WHERE ssoid=(SELECT ssoid FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sso_rt` WHERE userid=h.userid and switch='Y' LIMIT 1) and switch='Y' and name='weixin' LIMIT 1) as openid ".
			  " FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h ".
			  " JOIN `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_profile` up ON h.userid=up.userid ".
			  " WHERE (h.productid, h.price) =  ".
			  "       (SELECT productid, min_price FROM  `{$config['db'][4]["dbname"]}`.`v_min_unique_price` WHERE productid='{$productid}' LIMIT 1) ";
		// error_log("[ajax/product/getBidWinner] : ".$query);

		$table = $db->getQueryRecord($query);
		if($table['table']['record']) {
			$ret=array();
			$ret['userid'] = $table['table']['record'][0]['userid'];
			$ret['nickname'] = $table['table']['record'][0]['nickname'];
			$ret['productid'] = $table['table']['record'][0]['productid'];
			$ret['prodname'] = $table['table']['record'][0]['prodname'];
			$ret['src_province'] = $table['table']['record'][0]['src_province'];
			$ret['price'] = $table['table']['record'][0]['price'];
			$ret['openid'] = $table['table']['record'][0]['openid'];
			$ret['src_ip'] = $table['table']['record'][0]['src_ip'];
			$ret['comefrom'] = $table['table']['record'][0]['comefrom'];
		}
		error_log("[ajax/product/getBidWinner] : ".json_encode($ret));
		return $ret;
	}
    */

	//產生下標歷史記錄
	public function mk_history($product) {
		$time_start = microtime(true);

		global $db, $config, $jpush;

		// $jp = new jpush;

		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = '';
		$values = array();
		$ip = GetIP();

		if($_POST['pic_type'] == 0 || empty($_POST['pic_type'])){
			//上傳圓夢商品圖片
			$thumbnail_filename = $this->upload_pic();
			if($thumbnail_filename == false){
				$r['err'] = 34;
				$r['retCode']=-100234;
				$r['retMsg']='圖片上傳失敗!';
				return $r;
			}
		}else{
			//不須上傳圖檔
			$thumbnail_filename = $_POST['pic'];
		}
		//產生圓夢商品詳細記錄
		$shdid = $this->add_saja_history_detail($product, $thumbnail_filename);
		//紀錄圓夢殺價紀錄列表
		$this->add_saja_history_head($product, $shdid, $bid_list='');

		if($_POST['pay_type'] == 'oscode') {
			/****************************
			* 使用殺價券 : 為針對某特定商品免費的「下標次數」， 1張殺價券可以下標乙次
			****************************/

			//檢查殺價券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
			    ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
			error_log("[ajax/product/mk_history]: oscode needed ==>".round($this->tickets));
			error_log("[ajax/product/mk_history]: oscode available : ".$query);

			if ($osq > $_POST['oscode_num']) {
				$osq = $_POST['oscode_num'];
			}
			error_log("[ajax/product/mk_history]: osq  : ".$osq);

			if(round($this->tickets) > $osq) {

					/****************************
					* 使用殺幣
					****************************/
					$scode_fee = 0;
					$needspoint = 0;
					for($i = 0 ; $i < round($this->tickets-$osq); $i++)
					{
						$needspoint = $needspoint + ($_POST['price_start']+$i);
					}

					$saja_fee_amount= getBidTotalFee($product,
													 $_POST['type'],
													 $_POST['price'],
													 $_POST['price_start'],
													 $_POST['price_stop'],
													 $this->currency_unit);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$needspoint = $needspoint+($product['saja_fee']*round($this->tickets-$_POST['oscode_num']));break;
						case 'B':$needspoint = $needspoint;break;
						case 'O':$needspoint = 0;break;
						case 'F':$needspoint = $product['saja_fee']*round($this->tickets-$osq);break;
						default:$needspoint = $product['saja_fee']*round($this->tickets-$osq);break;
					}

					error_log("[ajax/product/mk_history]: needspoint : ".$needspoint);

					// 檢查殺幣餘額
					$query = "SELECT SUM(amount) as amount
								FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
							   WHERE userid = '{$this->userid}'
								 AND prefixid = '{$config['default_prefix_id']}'
								 AND switch = 'Y'
					";
					$spointtable = $db->getQueryRecord($query);
					error_log("[ajax/product/mk_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

					if($needspoint > (float)$spointtable['table']['record'][0]['amount']) {
						//'殺價幣不足'
						$r['err'] = 13;
						$r['retCode']=-100193;
						$r['retMsg']='殺價幣不足!';
					} else {

						//使用殺幣
						$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
						SET
							`prefixid`='{$config['default_prefix_id']}',
							`userid`='{$this->userid}',
							`countryid`='{$this->countryid}',
							`behav`='user_saja',
							`amount`='".- $needspoint."',
							`insertt`=NOW()
						";

						$res = $db->query($query);
						$spointid = $db->_con->insert_id;
						error_log("[ajax/product/mk_history]: spointid : ".$spointid);

						//活動送幣使用紀錄
						$this->saja_spoint_free_history($needspoint , $spointid, $product['productid']);

						//使用殺價券
						for($i = 0 ; $i < round($osq); $i++) {
							$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
							SET
								used = 'Y',
								used_time = NOW()
							WHERE
								`prefixid` = '{$config['default_prefix_id']}'
								AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
								AND switch = 'Y'
							";
							$res = $db->query($query);

							error_log("[ajax/product/mk_history]: oscode res ".$i." : ".$res);
						}

						//使用殺價幣
						for($i = 0 ; $i < round($this->tickets-$osq); $i++) {
							
							$bid_total = 0;
							$bid_total = $bid_total + ($_POST['price_start']+$i);

							// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
							switch($product['totalfee_type']) {
								case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
								case 'B':$bid_total = $bid_total;break;
								case 'O':$bid_total = 0;break;
								case 'F':$bid_total = $product['saja_fee'];break;
								default:$bid_total = $product['saja_fee'];break;
							}
							error_log("[ajax/dream_product/mk_history]: bid_total : ".$bid_total);
							
							//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'1',
								'{$this->userid}',
								'{$this->username}',
								'{$spointid}',
								'0',
								'0',
								'0',
								'{$shdid}',
								'".($this->price + $i * $this->currency_unit)."',
								'{$bid_total}',
								NOW(),
								'{$ip}'
							)";
						}

						$k=0;
						//使用殺價券
						for($i = $this->tickets-$osq; $i < round($this->tickets); $i++) {
							$bid_total = 0;
							$bid_total = $bid_total + ($_POST['price_start']+$i);

							// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
							switch($product['totalfee_type']) {
								case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
								case 'B':$bid_total = $bid_total;break;
								case 'O':$bid_total = 0;break;
								case 'F':$bid_total = $product['saja_fee'];break;
								default:$bid_total = $product['saja_fee'];break;
							}
							error_log("[ajax/dream_product/mk_history]: bid_total : ".$bid_total);
							
							//殺價下標歷史記錄（對應下標記錄與 oscodeid）
							$values[$i] = "
							(
								'{$config['default_prefix_id']}',
								'{$product['productid']}',
								'1',
								'{$this->userid}',
								'{$this->username}',
								'0',
								'0',
								'0',
								'{$table['table']['record'][$k]['oscodeid']}',
								'{$shdid}',
								'".($this->price + $i * $this->currency_unit)."',
								'{$bid_total}',
								NOW(),
								'{$ip}'
							)";
							$k++;
							error_log("[ajax/product/mk_history]: oscode values : ".$table['table']['record'][($k-1)]['oscodeid']);

						}

						if(empty($values)) {
							//'下標失敗'
							$r['err'] = -1;
							$r['retCode']=-1;
							$r['retMsg']='下標失敗!';
						}
					}

			} else {
				//使用殺價券
				for($i = 0; $i < round($this->tickets); $i++) {
					
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/dream_product/mk_history]: bid_total : ".$bid_total);
					
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
					SET
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);

					//殺價下標歷史記錄（對應下標記錄與 oscodeid）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'0',
						'0',
						'{$table['table']['record'][$i]['oscodeid']}',
						'{$shdid}',
						'".($this->price + $i * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";
				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retCode']=-1;
					$r['retMsg']='下標失敗!';
				}
			}
		} else if($_POST['pay_type'] == 'scode') {
			/****************************
			* 使用S碼 : 為免費的「下標次數」，1組S碼可以下標乙次
			* 有效時間為30天(發送當天開始算30天)，逾期失效不補回。
			超級殺價卷
			****************************/

			$scodeModel = new ScodeModel;
			$scode = $scodeModel->get_scode($this->userid);

			if(empty($scode)) {
				$sq = 0;
			} else {
				$sq = $scode;
			}
			error_log("[ajax/product/mk_history]: scode needed ==>".round($this->tickets));

			if($sq > $_POST['scode_num']) {
				$sq = $_POST['scode_num'];
			}
			error_log("[ajax/product/mk_history]: sq  : ".$sq);


			//檢查S碼數量
			if(round($this->tickets) > $sq) {

				//'超級殺價卷不足'
				// $r['err'] = 19;
				// $r['retCode']=-100199;
				// $r['retMsg']='超級殺價券不足!';
				// 20190424

				/****************************
				* 使用殺幣
				****************************/
				$scode_fee = 0;
				$needspoint = 0;
				for($i = 0 ; $i < round($this->tickets-$sq); $i++)
				{
					$needspoint = $needspoint + ($_POST['price_start']+$i);
				}

				$saja_fee_amount= getBidTotalFee($product,
												 $_POST['type'],
												 $_POST['price'],
												 $_POST['price_start'],
												 $_POST['price_stop'],
												 $this->currency_unit);

				// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)

				switch($product['totalfee_type']) {
					case 'A':$needspoint = $needspoint+($product['saja_fee']*round($this->tickets-$_POST['scode_num']));break;
					case 'B':$needspoint = $needspoint;break;
					case 'O':$needspoint = 0;break;
					case 'F':$needspoint = $product['saja_fee']*round($this->tickets-$sq);break;
					default:$needspoint = $product['saja_fee']*round($this->tickets-$sq);break;
				}
				error_log("[ajax/product/mk_history]: needspoint : ".$needspoint);

				// 檢查殺幣餘額
				$query = "SELECT SUM(amount) as amount
							FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
							WHERE userid = '{$this->userid}'
							AND prefixid = '{$config['default_prefix_id']}'
							AND switch = 'Y'
				";
				$spointtable = $db->getQueryRecord($query);
				error_log("[ajax/product/mk_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

				if($needspoint > (float)$spointtable['table']['record'][0]['amount']) {
					//'殺價幣不足'
					$r['err'] = 13;
					$r['retCode']=-100193;
					$r['retMsg']='殺價幣不足!';
				} else {

					//使用殺幣
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`countryid`='{$this->countryid}',
						`behav`='user_saja',
						`amount`='".- $needspoint."',
						`insertt`=NOW()
					";
					$res = $db->query($query);
					$spointid = $db->_con->insert_id;
					error_log("[ajax/product/mk_history]: spointid : ".$spointid);

					//活動送幣使用紀錄
					$this->saja_spoint_free_history($needspoint, $spointid, $product['productid']);

					//使用超級殺價券
					for($i = 0 ; $i < round($sq); $i++) {

							//確認S碼
						$query = "SELECT scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
							where
							`prefixid`='{$config['default_prefix_id']}'
							and `userid` = '{$this->userid}'
							and `behav` != 'a'
							and `closed` = 'N'
							and `remainder` > 0
							and `switch` = 'Y'
							and `offtime` > NOW()
							order by insertt asc
							limit 0, 1
						";
						$table = $db->getQueryRecord($query);

						//使用S碼
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
								SET
									`remainder`=`remainder` - 1,
									`modifyt`=NOW()
								where
									`prefixid`='{$config['default_prefix_id']}'
									and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
									and `userid` = '{$this->userid}'
									and `behav` != 'a'
									and `closed` = 'N'
									and `remainder` > 0
									and `switch` = 'Y'
						";
						$res = $db->query($query);
					}

					// 新增扣除組數紀錄
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`behav`='a',
						`productid`='{$product['productid']}',
						`amount`='". - $sq ."',
						`remainder`='0',
						`insertt`=NOW()
					";
					$res = $db->query($query);
					$scodeid = $db->_con->insert_id;

					for($i = 0 ; $i < round($sq); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/dream_product/mk_history]: bid_total : ".$bid_total);
						
						//殺價下標歷史記錄（對應下標記錄與使用S碼id）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'{$scodeid}',
							'0',
							'0',
							'{$shdid}',
							'".($this->price + $i * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					//使用殺價幣
					for($i = 0 ; $i < round($this->tickets-$sq); $i++) {

						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/dream_product/mk_history]: bid_total : ".$bid_total);
					
						//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'{$spointid}',
							'0',
							'0',
							'0',
							'{$shdid}',
							'".($this->price + $i * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = -1;
						$r['retCode']=-1;
						$r['retMsg']='下標失敗!';
					}
				}


			} else {
				for($i = 0 ; $i < round($this->tickets); $i++) {
					//確認S碼
					$query = "select scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
						where
						`prefixid`='{$config['default_prefix_id']}'
						and `userid` = '{$this->userid}'
						and `behav` != 'a'
						and `closed` = 'N'
						and `remainder` > 0
						and `switch` = 'Y'
						and `offtime` > NOW()
						order by insertt asc
						limit 0, 1
					";
					$table = $db->getQueryRecord($query);

					//使用S碼
					$query = "update `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
					SET
						`remainder`=`remainder` - 1,
						`modifyt`=NOW()
					where
						`prefixid`='{$config['default_prefix_id']}'
						and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
						and `userid` = '{$this->userid}'
						and `behav` != 'a'
						and `closed` = 'N'
						and `remainder` > 0
						and `switch` = 'Y'
					";
					$res = $db->query($query);
				}

				// 新增扣除組數紀錄
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`behav`='a',
					`productid`='{$product['productid']}',
					`amount`='". - $this->tickets ."',
					`remainder`='0',
					`insertt`=NOW()
				";
				$res = $db->query($query);
				$scodeid = $db->_con->insert_id;
				for($i = 0 ; $i < round($this->tickets); $i++) {

					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/dream_product/mk_history]: bid_total : ".$bid_total);
					
					//殺價下標歷史記錄（對應下標記錄與使用S碼id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'{$scodeid}',
						'0',
						'0',
						'{$shdid}',
						'".($this->price + $i * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";
				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retCode']=-1;
					$r['retMsg']='下標失敗!';
				}
			}
		} else {
			/****************************
			* 使用殺幣
			****************************/
			$scode_fee = 0;

            //檢查殺幣餘額
			$query = "SELECT SUM(amount) as amount
			            FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
			           WHERE userid = '{$this->userid}'
				         AND prefixid = '{$config['default_prefix_id']}'
				         AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);

			$saja_fee_amount= getBidTotalFee($product,
			                                 $_POST['type'],
											 $_POST['price'],
											 $_POST['price_start'],
											 $_POST['price_stop'],
											 $this->currency_unit);
			error_log("[ajax/dream_product/mk_history] saja_fee_amount <==> table amount : ".$saja_fee_amount."<==>".(float)$table['table']['record'][0]['amount']);
			if($saja_fee_amount > (float)$table['table']['record'][0]['amount']) {
				//'殺價幣不足'
				$r['err'] = 13;
				$r['retCode']=-100193;
				$r['retMsg']='殺價幣不足!';
			} else {
				//使用殺幣
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`countryid`='{$this->countryid}',
					`behav`='user_saja',
					`amount`='". - $saja_fee_amount ."',
					`insertt`=NOW()
				";

				$res = $db->query($query);
				$spointid = $db->_con->insert_id;

				//活動送幣使用紀錄
				$this->saja_spoint_free_history($saja_fee_amount, $spointid, $product['productid']);

				for($i = 0 ; $i < round($this->tickets); $i++)
				{
					
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/dream_product/mk_history]: bid_total : ".$bid_total);
					//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'{$spointid}',
						'0',
						'0',
						'0',
						'{$shdid}',
						'".($this->price + $i * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";

				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -1;
					$r['retCode']=-1;
				    $r['retMsg']='下標失敗!';
				}
			}
		}


		if(empty($r['err']) && !empty($values) ) {
			// 取得目前winner
			// 20190420 暫時移除 20180816回復

			$arr_ori_winner=array();
			$arr_ori_winner=$this->getBidWinner($product['productid']);
			// if(!empty($arr_ori_winner)) {
			//    error_log("[ajax/product/mk_history]: ori Winner of ".$arr_ori_winner['prodname']." is :".$arr_ori_winner['nickname']);
			// } else {
			//    error_log("[ajax/product/mk_history]: ori Winner is Empty !");
			// }


			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
			(
				prefixid,
				productid,
				ptype,
				userid,
				nickname,
				spointid,
				scodeid,
				sgiftid,
				oscodeid,
				shdid,
				price,
				bid_total,
				insertt,
				src_ip
			)
			VALUES
			".implode(',', $values)."
			";

			$res = $db->query($query);
			// error_log("[ajax/product]:".$query."-->".$res);

			$r['rank'] = $last_rank;

            /*
			// 非閃殺商品下標, 結標一分鐘以前, 單次下標將提示順位
			if($_POST['type'] == 'single' && $product['is_flash']!='Y') {
				if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
					// 檢查重複
					$query= "SELECT count(userid) as cnt
							   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
							  WHERE productid='{$product['productid']}'
								AND price={$_POST['price']}
								AND switch='Y' ";

					$table = $db->getQueryRecord($query);
					$dup_cnt=$table['table']['record'][0]['cnt'];
					if($dup_cnt>1) {
						// 重複出價
						$r['rank']=-1;
					} else if($dup_cnt<=1) {
						// 無重複出價
						$query1 = "SELECT count(*) as cnt FROM  `{$config['db'][4]["dbname"]}`.`v_unique_price`
								   WHERE productid='{$product['productid']}' AND price<{$_POST['price']} ";
						$table1 = $db->getQueryRecord($query1);
						$r['rank'] =$table1['table']['record'][0]['cnt'];
						error_log("[ajax/product]rank : ".$r['rank']);
					}
				} else {
				    // 不提示
					$r['total_num']=-1;
				}
			}

			// 非閃殺商品下標, 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出價個數, 及該次下標的唯一最低價
			if($product['is_flash']!='Y') {
				if($_POST['type'] == 'range') {
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						$r['total_num']=$this->tickets;
						// 顯示提示
						// 取得該區間內唯一出價的個數
						if($this->tickets>1) {
							$query="SELECT * FROM saja_shop.saja_history sh JOIN saja_shop.v_unique_price up
									   ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y'
									WHERE sh.switch='Y'
									  AND sh.productid='{$product['productid']}'
									  AND sh.price between {$_POST['price_start']} AND {$_POST['price_stop']}
									  AND sh.userid='{$this->userid}' ORDER BY sh.price asc ";

							// error_log("[ajax/product]range chk: ".$query);
							$table = $db->getQueryRecord($query);
							error_log("[ajax/product]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);

							if(empty($table['table']['record'])) {
							   // 無唯一出價
							   $r['num_uniprice'] = 0;
							   $r['lowest_uniprice'] = 0;
							} else {
							   // 有唯一出價
							   $r['num_uniprice'] = count($table['table']['record']);
							   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
							}
						} else {
							// 不提示
							$r['total_num']=-1;
						}
					} else {
						// 不提示
						$r['total_num']=-1;
					}
				}
			}
            */

			// 20190420 加入以redis sortedset 判斷的機制
			// 非閃殺商品下標, 結標一分鐘以前, 單次下標將提示順位
			$updWinner=false;
			// if($_POST['type'] == 'single' && $product['is_flash']!='Y') {
            if($_POST['type'] == 'single') {
				// 寫入redis, 並使用 SortedSet來判斷順位
				$cache = getBidCache();
                // 如果設定要用redis判斷 且有redis -> 用redis 判斷
                if($config["rank_bid_by_redis"] && $cache) {
                   $str_price = str_pad($this->price,12,"0",STR_PAD_LEFT);
                   $dup_cnt = $cache->zIncrBy("PROD:RANK:".$product['productid'],1,$str_price) ;
                   if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
                        // 檢查重複
                        if($dup_cnt>2) {
                            // 2筆以上重複出價  不用更新得標者
                            $r['rank']=-1;
							$updWinner=false;
                        } else if($dup_cnt==2) {
							// 剛好有2筆重複  可能是把得標者幹掉 要更新得標者
							$r['rank']=-1;
							$updWinner=true;
						} else if($dup_cnt==1) {
                            // zRank直接回傳該出價的排名 以0為第1,
                            // 所以可以直接表示前面有幾個唯一的出價
							// $updWinner=true;
                            $r['rank']= $cache->zRank("PROD:RANK:".$product['productid'],$str_price);
                            if($r['rank']<2)
							   $updWinner=true;
						}
                        error_log("Check rank From redis=>".$r['rank']);
                    } else {
                        // 不提示
                        $updWinner=false;
						$r['total_num']=-1;
                    }
                } else {
                    // 沒有redis或未設定由redis判斷 -> 就走原來程序進DB撈
                    if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
                        // 檢查重複
                        $query= "SELECT count(userid) as cnt
                                   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
                                  WHERE productid='{$product['productid']}'
                                    AND price={$_POST['price']}
                                    AND switch='Y' ";

                        $table = $db->getQueryRecord($query);
                        $dup_cnt=$table['table']['record'][0]['cnt'];
                        if($dup_cnt>2) {
                            // 2筆以上的重複出價
                            $r['rank']=-1;
							$updWinner = false;
						} else if($dup_cnt==2) {
							// 剛好2筆重複出價
							$r['rank']=-1;
							$updWinner = true;
                        } else if($dup_cnt<2) {
                            // 無重複出價
                            $query1 = "SELECT count(*) as cnt FROM  `{$config['db'][4]["dbname"]}`.`v_unique_price`
                                       WHERE productid='{$product['productid']}' AND price<{$_POST['price']} ";
                            $table1 = $db->getQueryRecord($query1);
                            $r['rank'] =$table1['table']['record'][0]['cnt'];
							if($r['rank']<2)
							   $updWinner = true;
                            error_log("[ajax/product] DB rank : ".$r['rank']);
                        }
                        error_log("Check rank From DB=>".$r['rank']);
                    } else {
				    // 不提示
                        $r['total_num']=-1;
                    }
                }
			}


			// 非閃殺商品下標, 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出價個數, 及該次下標的唯一最低價
			// if($product['is_flash']!='Y') {
				if($_POST['type'] == 'range') {
					if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)) {
						$r['total_num']=$this->tickets;
						// 顯示提示
						// 取得該區間內唯一出價的個數
                        $cache = getBidCache();
                        $start_price = $_POST['price_start'];
                        $end_price = $_POST['price_stop'];
						// 如果設定要用redis判斷 且有redis -> 用redis 判斷
                        if($config["rank_bid_by_redis"] && $cache) {
                            $r['num_uniprice'] = 0;
                            $r['lowest_uniprice'] = 0;
							$updWinner=false;
                            for($p=$start_price ; $p<=$end_price ; ++$p) {
                                $str_price = str_pad($p,12,"0",STR_PAD_LEFT);
                                $dup_cnt = $cache->zIncrBy("PROD:RANK:".$product['productid'],1,$str_price);
                                if($dup_cnt<=2) {
								   $updWinner=true;
								}
								if($dup_cnt==1) {
                                   // 增加唯一價的個數
                                   $r['num_uniprice']++;
                                   // 把第一個碰到的價格記下來(就是該批次內最低的出價)
                                   if($r['lowest_uniprice']==0) {
                                      $r['lowest_uniprice']=$p;
                                   }
                                }
                            }
                        } else {
                            // 沒有redis或沒有指定要用redis計算 -> 走原本程序進DB撈
                            if($this->tickets>1) {
                                $query="SELECT * FROM saja_shop.saja_history sh JOIN saja_shop.v_unique_price up
                                           ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y'
                                        WHERE sh.switch='Y'
                                          AND sh.productid='{$product['productid']}'
                                          AND sh.price between {$_POST['price_start']} AND {$_POST['price_stop']}
                                          AND sh.userid='{$this->userid}' ORDER BY sh.price asc ";

                                // error_log("[ajax/product]range chk: ".$query);
                                $table = $db->getQueryRecord($query);
                                // error_log("[ajax/product]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);

                                if(empty($table['table']['record'])) {
                                   // 無唯一出價
								   $updWinner=false;
                                   $r['num_uniprice'] = 0;
                                   $r['lowest_uniprice'] = 0;
                                } else {
                                   // 有唯一出價
                                   $updWinner=true;
								   $r['num_uniprice'] = count($table['table']['record']);
                                   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
                                }
                            } else {
                                // 不提示
                                $r['total_num']=-1;
                            }
                        }
					} else {
						// 不提示
						$r['total_num']=-1;
					}
				}
			// }

            // 未重複或剛好2筆重複出價者  需更新得標者
            if($updWinner) {
				error_log("[ajax/product] updWinner : ".$updWinner);
				$arr_new_winner=array();
				$status = 1; //websocket 狀態碼(1:更新得標者, 2:無人得標)
				try {
					$arr_new_winner=$this->getBidWinner($product['productid']);

					$query ="SELECT thumbnail_file, thumbnail_url
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					WHERE prefixid = '{$config['default_prefix_id']}'
					  AND userid = '{$arr_new_winner['userid']}'
					  AND switch = 'Y'
					";
					$table = $db->getQueryRecord($query);
					$r['thumbnail_file'] = $table['table']['record'][0]['thumbnail_file'];
					$r['thumbnail_url'] = $table['table']['record'][0]['thumbnail_url'];
					$r['shd_thumbnail_file'] = $arr_new_winner['shd_thumbnail_file'];
					if($arr_new_winner['src_ip']) {
						$r['src_ip']=maskIP($arr_new_winner['src_ip']);
						$r['comefrom']=$r['src_ip'];
					}
					error_log("[ajax/product/mk_history] new_winner :".json_encode($arr_new_winner));
					$r['winner']=$arr_new_winner['nickname'];
					$winner_name = $r['winner'];
					if(empty($r['winner'])) {
					   $r['winner']="";
					   $winner_name = "目前無人得標";
					   $status = 2; //websocket 狀態碼(1:更新得標者, 2:無人得標)
					}

					$query2 ="SELECT shdid, userid, productid, title, content, purchase_url, thumbnail_filename, thumbnail_name as pic_name, memo, insertt
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` as shd,
							(SELECT MAX(shdid) as max_shdid FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` GROUP BY userid) shd2
							WHERE shd.shdid = shd2.max_shdid
							AND productid = '{$product['productid']}'
							AND switch = 'Y'
							ORDER BY shd.insertt DESC LIMIT 5 ";
					$table2 = $db->getQueryRecord($query2);

					// $thumbnail_file = (empty($r['thumbnail_file'])) "":$r['thumbnail_file'];//空圖片 socket給空字串
					// $thumbnail_file = null;

					$r_bidded['name'] = $r['winner'];
					$r_bidded['status'] = $status;
					$r_bidded['thumbnail_file'] = $r['thumbnail_file'];
					$r_bidded['thumbnail_url'] = $r['thumbnail_url'];
					$r_bidded['shd_thumbnail_file'] = $r['shd_thumbnail_file'];
					// 寫入redis
					$redis=getRedis('','','');
					if($redis) {
					   $redis->set("PROD:".$product['productid'].":BIDDER",json_encode($r_bidded));
					}
					// websocket更新得標人員
					$ts = time();
					$client = new WebSocket\Client("wss://ws.saja.com.tw:3334");
					$ary_bidder = array('actid'		=> 'DREAM_BID_WINNER',
										'name' 		=> $winner_name,
										'productid' => "{$product['productid']}",
										'ts'		=> $ts,
										'status'	=> $status,
										'sign'		=> MD5($ts."|sjW333-_@"),
										'thumbnail_file' => $r['thumbnail_file'] ,
										'thumbnail_url'	=> $r['thumbnail_url'],
										'shd_thumbnail_file' => $r['shd_thumbnail_file'],
										'latest_bid' => $table2['table']['record']
								);
					$client->send(json_encode($ary_bidder));

					$diff_sec = $product['offtime']-time(); //大於30分鐘才執行推播
					if ($diff_sec>180) {
						$ori_userid = $arr_ori_winner['userid'];
						$new_userid = (empty($arr_new_winner['userid']))?"":$arr_new_winner['userid'];
						$post_array = array(
								"ori_userid" => $ori_userid,
								"new_userid" => $new_userid,
								"productid" => $product['productid']
							);
						// 更新得標者推播 20190819
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_URL, BASE_URL.APP_DIR."/push/after_saja");
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_array));
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						$result = curl_exec($ch);
						curl_close($ch);
						// error_log("[ajax/product] push/after_saja : ".print_r($post_array));
						error_log("[ajax/product] push/after_saja ori_userid: ".$ori_userid);
						error_log("[ajax/product] push/after_saja new_userid: ".$new_userid);
						error_log("[ajax/product] push/after_saja productid: ".$product['productid']);
						error_log("[ajax/product] push/after_saja url: ".BASE_URL.APP_DIR."/push/after_saja");
					}

				} catch(Exception $e) {
					error_log("[ajax/product] exception : ".$e->getMessage());
					// 不提示
					$r['total_num']=-1;
				}
			}
		}
		error_log("[ajax/mk_history] r : ".json_encode($r));
		return $r;
	}

	public function saja_spoint_free_history($saja_fee, $spointid = 0, $productid = 0){

		global $db, $config;
		//確認是否有免費幣尚未用完
		$query="SELECT sum(free_amount) as free_amount
				FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint_free_history`
				WHERE userid = '{$this->userid}'
				AND amount_type ='1'
				AND switch =  'Y' ";

		// error_log("[ajax/product]range chk: ".$query);
		$chk = $db->getQueryRecord($query);

		//有未使用完的免費幣
		if($chk['table']['record'][0]['free_amount'] > 0){

			//免費幣餘額 大於等於 總下標費用，該筆金額全部視為使用免費幣
			if($chk['table']['record'][0]['free_amount'] >= $saja_fee){
				$free_amount = $saja_fee;
			}else{
				//免費幣餘額 小於 總下標費用，該筆金額部分視為免費幣
				$free_amount = $chk['table']['record'][0]['free_amount'];
			}

			//新增免費幣使用紀錄
			$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint_free_history`
			SET
				`userid`='{$this->userid}',
				`behav` = 'user_saja',
				`amount_type` = '1',
				`free_amount` = '".- $free_amount."',
				`total_amount` = '".- $saja_fee."',
				`productid` = '{$productid}',
				`spointid` = '{$spointid}',
				`insertt`=NOW()
			";
			$res = $db->query($query);

		}

	}

	/*
	* 新增圓夢商品詳細記錄
	*/
	public function add_saja_history_detail($product, $thumbnail_filename){

		global $db, $config;

		$title=(empty($_POST['title'])) ? htmlspecialchars($_GET['title']) : htmlspecialchars($_POST['title']);
		$content=(empty($_POST['content'])) ? htmlspecialchars($_GET['content']) : htmlspecialchars($_POST['content']);
		$purchase_url=(empty($_POST['purchase_url'])) ? htmlspecialchars($_GET['purchase_url']) : htmlspecialchars($_POST['purchase_url']);

		//產生下標歷史記錄
		$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history_detail`
		SET
			`userid`='{$this->userid}',
			`productid` = '{$product['productid']}',
			`title` = '{$title}',
			`content` = '{$content}',
			`purchase_url` = '{$purchase_url}',
			`thumbnail_filename` = '{$thumbnail_filename}',
			`thumbnail_name` = '{$_POST['pic_name']}',
			`insertt`=NOW()
		";

		$res = $db->query($query);
		$shdid = $db->_con->insert_id;

		if(empty($shdid)){
			$shdid = '0';
		}
		return $shdid;
	}

	/*
	*上傳商品圖片
	*/
	public function upload_pic(){
		//參數接收設定
		$filedir = $_POST['filedir'];   //檔案子目錄路徑
		$img = $_POST['pic'];   //圖檔
		$pic_name = $_POST['pic_name'];   //圖檔名稱

		//尋找檔案內是否含png檔頭
		$img_format_check = strpos($img,'data:image/png;base64,');

		//去除png檔頭取得純圖檔資料 需注意 data url 格式 與來源是否相符 ex:image/png
		$img = str_replace('data:image/png;base64,','', $img);
		//過濾圖檔資料裡的換行符號
		$img = preg_replace("/\s+/", '', $img);

		$img_data = base64_decode($img);//解base64碼

		$filename = md5(date("YmdHis")."_".$pic_name).'.png';
		$file = '../images/'.$_POST['filedir'].$filename;
		//$file = '../images/'.$filedir.'_'.$filename;//檔名 包含資料夾路徑 請記得此資料夾需 777 權限 方可寫入圖檔

		$res = file_put_contents($file, $img_data);
		$res2 = syncToS3('/var/www/html/site/images/dream/products/'.$filename,'s3://img.saja.com.tw','/site/images/dream/products/');

		if($res2){
			return $filename;
		}else{
			return false;;
		}
	}

	/*
	*紀錄圓夢殺價紀錄
	*/
	public function add_saja_history_head($product, $shdid, $bid_list){

		global $db, $config;

		if($_POST['bid_type'] == 1){
			foreach($bid_list as $key => $row){
				if($row['type'] == 'single'){
					$paytype = 1;
				}else{
					$paytype = 2;
				}

				$values[$key] = "
				(
					'{$this->userid}',
					'{$product['productid']}',
					'{$shdid}',
					'{$paytype}',
					'{$row['price']['start']}',
					'{$row['price']['end']}',
					'Y',
					NOW()
				)";
			}

			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history_head`
			(
				`userid`,
				`productid`,
				`shdid`,
				`paytype`,
				`start`,
				`end`,
				`switch`,
				`insertt`
			)
			VALUES
			".implode(',', $values)."
			";
			$res = $db->query($query);
		}else{
			if($_POST['type'] == 'single'){
				$paytype = 1;
				$start_price = $_POST['price'];
				$end_price = 0;
			}else{
				$paytype = 2;
				$start_price = $_POST['price_start'];
				$end_price = $_POST['price_stop'];
			}

			//產生圓夢殺價紀錄列表
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history_head`
			SET
				`userid`='{$this->userid}',
				`productid` = '{$product['productid']}',
				`shdid` = '{$shdid}',
				`paytype` = '{$paytype}',
				`start` = '{$start_price}',
				`end` = '{$end_price}',
				`insertt`=NOW()
			";
			$res = $db->query($query);
		}

	}

	/*
	*檢查出價金額及記錄
	*/
	public function chk_bid_price($product, $bid_list, $all_price){

		$r['err'] = '';

		if($this->tickets <= 0){
			//'金額不可空白'
			$r['err'] = 13;
			$r['retCode'] = -13;
			$r['retMsg']='金額不可空白!';
			return $r;
		}
		if((int)$product['user_limit'] > 0 && $this->chk_user_limit($product['user_limit'], $product['productid'])){
			//'超過可下標次數限制'
			$r['err'] = 14;
			$r['retCode'] = -14;
			$r['retMsg']='超過可下標次數限制!';
			return $r;
		}
		if($this->tickets > (int)$product['usereach_limit']) {
			//'超過可連續下標次數限制'
			$r['err'] = 15;
			$r['retCode'] = -15;
			$r['retMsg']='超過可連續下標次數限制!';
			return $r;
		}
		if($product['offtime'] > 0 && ($product['offtime'] - time() < $this->range_saja_time_limit) && $this->tickets > 1) {
			//'超過可連續下標時間'
			$r['err'] = 16;
			$r['retCode'] = -16;
			$r['retMsg']='超過可連續下標時間!';
			return $r;
		}

		//檢查出價金額
		foreach($all_price as $row){
			if(!is_numeric((int)$row)) {
				//'金額格式錯誤'
				$r['err'] = 17;
				$r['retCode'] = -17;
				$r['retMsg']='金額格式錯誤!';
				return $r;
			}
			if((float)$row < (float)$product['price_limit']) {
				//'金額低於底價'
				$r['err'] = 18;
				$r['retCode'] = -18;
				$r['retMsg']='金額低於底價!';
				return $r;
			}
			if((float)$row > (float)$product['retail_price']) {
				//'金額超過商品市價'
				$r['err'] = 19;
				$r['retCode'] = -19;
				$r['retMsg']='金額超過商品市價!';
				return $r;
			}
		}

		//檢查出價紀錄
		foreach($bid_list as $row){

			if(!is_numeric($row['price']['start']) || !is_numeric($row['price']['end'])) {
				//'金額格式錯誤'
				$r['err'] = 20;
				$r['retCode'] = -20;
				$r['retMsg']='金額格式錯誤!';
				return $r;
			}

			if($row['type'] == 'range'){
				if((int)$row['price']['start'] > (int)$row['price']['end']) {
					//'起始價格不可超過結束價格'
					$r['err'] = 21;
					$r['retCode'] = -21;
					$r['retMsg']='起始價格不可超過結束價格!';
					return $r;
				}
				if((int)$row['price']['start'] == (int)$row['price']['end']) {
					//'起始價格不可超過結束價格'
					$r['err'] = 22;
					$r['retCode'] = -22;
					$r['retMsg']='出價起訖區間不可相同!';
					return $r;
				}
			}
		}

		return $r;
	}

	//產生下標歷史記錄
	public function mk_bid_history($product, $all_price, $bid_list){
		$time_start = microtime(true);

		global $db, $config, $jpush;

		// $jp = new jpush;

		$r['err'] = '';
		$r['winner'] = '';
		$r['rank'] = '';
		$values = array();
		$ip = GetIP();

		//檢查圓夢照片
		if($_POST['pic_type'] == 0 || empty($_POST['pic_type'])){
			//上傳圓夢商品圖片
			$thumbnail_filename = $this->upload_pic();
			if($thumbnail_filename == false){
				$r['err'] = 23;
				$r['retCode'] = -23;
				$r['retMsg']='圖片上傳失敗!';
				return $r;
			}
		}else{
			//不須上傳圖檔
			$thumbnail_filename = $_POST['pic'];
		}
		//產生圓夢商品詳細記錄
		$shdid = $this->add_saja_history_detail($product, $thumbnail_filename);
		//紀錄圓夢殺價紀錄列表
		$this->add_saja_history_head($product, $shdid, $bid_list);

		//pay_type(0.spoint 1.mix_scode 2.oscode 3.scode)
		if($_POST['pay_type'] == 1){
			/****************************
			* 使用S碼 : 為免費的「下標次數」，1組S碼可以下標乙次
			* 有效時間為30天(發送當天開始算30天)，逾期失效不補回。
			超級殺價卷
			****************************/

			$scodeModel = new ScodeModel;
			$scode = $scodeModel->get_scode($this->userid);

			if(empty($scode)) {
				$sq = 0;
			} else {
				$sq = $scode;
			}
			error_log("[ajax/dream_product/mk_bid_history]: scode needed ==>".round($this->tickets));

			if($sq > $_POST['scode_num']) {
				$sq = $_POST['scode_num'];
			}
			error_log("[ajax/dream_product/mk_bid_history]: sq  : ".$sq);

			//檢查殺價券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}

			if ($osq > $_POST['oscode_num']) {
				$osq = $_POST['oscode_num'];
			}
			error_log("[ajax/dream_product/mk_bid_history]: sq  : ".$sq.",osq : ".$osq);

			if($sq == 0 || $osq == 0){
				$r['err'] = 27;
				$r['retCode'] = -27;
				$r['retMsg']='所有殺價卷皆不可為零!';
				return $r;
			}

			if($product['pay_type'] != 'b' && $product['pay_type'] != 'd'){
				$r['err'] = 28;
				$r['retCode'] = -28;
				$r['retMsg']='支付方式錯誤!';
				return $r;
			}

			$mix_sq = $sq + $osq;
			//下標次數 大於 使用者擁有的所有殺價卷數量
			if(round($this->tickets) > $mix_sq) {

				//殺價卷不足時，判斷商品是否可以使用殺價幣(a.殺幣, b.殺幣+S碼+限S, c.殺幣+限S, d.S碼+限S, e.限S)
				if($product['pay_type'] == 'd'){
					$r['err'] = 24;
					$r['retCode'] = -24;
					$r['retMsg']='殺價卷不足!';
					return $r;
				}
				/****************************
				* 使用殺幣
				****************************/
				$scode_fee = 0;
				$needspoint = 0;
				for($i = 0 ; $i < round($this->tickets-$mix_sq); $i++)
				{
					$needspoint = $needspoint + ($all_price[$i]);
				}

				switch($product['totalfee_type']) {
					case 'A':
						$needspoint = $needspoint + ($product['saja_fee'] * round($this->tickets - $_POST['scode_num']));
					break;
					case 'B':
						$needspoint = $needspoint;
					break;
					case 'O':
						$needspoint = 0;
					break;
					case 'F':
						$needspoint = $product['saja_fee'] * round($this->tickets - $mix_sq);
					break;
					default:
						$needspoint = $product['saja_fee'] * round($this->tickets - $mix_sq);
					break;
				}
				error_log("[ajax/dream_product/mk_bid_history]: needspoint : ".$needspoint);

				// 檢查殺幣餘額
				$query = "SELECT SUM(amount) as amount
							FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
							WHERE userid = '{$this->userid}'
							AND prefixid = '{$config['default_prefix_id']}'
							AND switch = 'Y'
				";
				$spointtable = $db->getQueryRecord($query);
				error_log("[ajax/dream_product/mk_bid_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

				if($needspoint > (float)$spointtable['table']['record'][0]['amount']) {
					//'殺價幣不足'
					$r['err'] = 25;
					$r['retCode'] = -25;
					$r['retMsg']='殺價幣不足!';
					return $r;
				} else {

					//使用殺幣
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`countryid`='{$this->countryid}',
						`behav`='user_saja',
						`amount`='".- $needspoint."',
						`insertt`=NOW()
					";
					$res = $db->query($query);
					$spointid = $db->_con->insert_id;
					error_log("[ajax/dream_product/mk_bid_history]: spointid : ".$spointid);

					//活動送幣使用紀錄
					$this->saja_spoint_free_history($needspoint, $spointid, $product['productid']);

					//使用殺價券
					for($i = 0 ; $i < round($osq); $i++) {
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
						SET
							used = 'Y',
							used_time = NOW()
						WHERE
							`prefixid` = '{$config['default_prefix_id']}'
							AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
							AND switch = 'Y'
						";
						$res = $db->query($query);

						error_log("[ajax/dream_product/mk_bid_history]: oscode res ".$i." : ".$res);
					}

					//使用超級殺價券
					for($i = 0 ; $i < round($sq); $i++) {

						//確認S碼
						$query = "SELECT scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
							where
							`prefixid`='{$config['default_prefix_id']}'
							and `userid` = '{$this->userid}'
							and `behav` != 'a'
							and `closed` = 'N'
							and `remainder` > 0
							and `switch` = 'Y'
							and `offtime` > NOW()
							order by insertt asc
							limit 0, 1
						";
						$table = $db->getQueryRecord($query);

						//使用S碼
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
								SET
									`remainder`=`remainder` - 1,
									`modifyt`=NOW()
								where
									`prefixid`='{$config['default_prefix_id']}'
									and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
									and `userid` = '{$this->userid}'
									and `behav` != 'a'
									and `closed` = 'N'
									and `remainder` > 0
									and `switch` = 'Y'
						";
						$res = $db->query($query);
					}

					// 新增扣除組數紀錄
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`behav`='a',
						`productid`='{$product['productid']}',
						`amount`='". - $sq ."',
						`remainder`='0',
						`insertt`=NOW()
					";
					$res = $db->query($query);
					$scodeid = $db->_con->insert_id;

					//使用殺價幣
					for($i = 0 ; $i < round($this->tickets-$mix_sq); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
						//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'{$spointid}',
							'0',
							'0',
							'0',
							'{$shdid}',
							'".($all_price[$i] * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					$k=0;
					//使用殺價券
					for($i = round($this->tickets-$mix_sq); $i < round($this->tickets-$sq); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
						
						//殺價下標歷史記錄（對應下標記錄與 oscodeid）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'0',
							'0',
							'{$table['table']['record'][$k]['oscodeid']}',
							'{$shdid}',
							'".($all_price[$i] * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
						$k++;
						error_log("ajax/dream_product/mk_bid_history]: oscode values : ".$table['table']['record'][($k-1)]['oscodeid']);
					}

					//使用超級殺價卷
					for($i = round($this->tickets-$sq); $i < round($this->tickets); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
						
						//殺價下標歷史記錄（對應下標記錄與 oscodeid）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'{$scodeid}',
							'0',
							'0',
							'{$shdid}',
							'".($all_price[$i] * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}
					error_log("ajax/dream_product/mk_bid_history]: scode values : ".$scodeid);

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = -26;
						$r['retCode'] = -26;
						$r['retMsg']='下標失敗!';
						return $r;
					}
				}
			}else{ //下標次數 小於等於 使用者擁有的所有殺價卷數量
				//使用殺價券
				for($i = 0 ; $i < round($osq); $i++) {
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
					SET
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);

					error_log("[ajax/dream_product/mk_bid_history]: oscode res ".$i." : ".$res);
				}

				//使用超級殺價券
				for($i = 0 ; $i < round($sq); $i++) {

					//確認S碼
					$query = "SELECT scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
						where
						`prefixid`='{$config['default_prefix_id']}'
						and `userid` = '{$this->userid}'
						and `behav` != 'a'
						and `closed` = 'N'
						and `remainder` > 0
						and `switch` = 'Y'
						and `offtime` > NOW()
						order by insertt asc
						limit 0, 1
					";
					$table = $db->getQueryRecord($query);

					//使用S碼
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
							SET
								`remainder`=`remainder` - 1,
								`modifyt`=NOW()
							where
								`prefixid`='{$config['default_prefix_id']}'
								and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
								and `userid` = '{$this->userid}'
								and `behav` != 'a'
								and `closed` = 'N'
								and `remainder` > 0
								and `switch` = 'Y'
					";
					$res = $db->query($query);
				}

				// 新增扣除組數紀錄
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`behav`='a',
					`productid`='{$product['productid']}',
					`amount`='". - $sq ."',
					`remainder`='0',
					`insertt`=NOW()
				";
				$res = $db->query($query);
				$scodeid = $db->_con->insert_id;

				$k=0;
				//使用殺價券
				for($i = 0; $i < round($osq); $i++) {
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
					//殺價下標歷史記錄（對應下標記錄與 oscodeid）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'0',
						'0',
						'{$table['table']['record'][$k]['oscodeid']}',
						'{$shdid}',
						'".($all_price[$i] * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";
					$k++;
					error_log("ajax/dream_product/mk_bid_history]: oscode values : ".$table['table']['record'][($k-1)]['oscodeid']);
				}

				for($i = round($osq) ; $i < round($this->tickets); $i++) {
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
					
					//殺價下標歷史記錄（對應下標記錄與使用S碼id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'{$scodeid}',
						'0',
						'0',
						'{$shdid}',
						'".($all_price[$i] * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";
				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -26;
					$r['retCode'] = -26;
					$r['retMsg']='下標失敗!';
					return $r;
				}
			}

		}else if($_POST['pay_type'] == 2) {
			/****************************
			* 使用殺價券 : 為針對某特定商品免費的「下標次數」， 1張殺價券可以下標乙次
			****************************/

			//檢查殺價券數量
			$query ="SELECT *
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$product['productid']}'
				AND userid = '{$this->userid}'
				AND used = 'N'
				AND switch = 'Y'
				ORDER BY oscodeid
			";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'])) {
				$osq = 0;
			} else {
				$osq = count($table['table']['record']);
			}
			error_log("[ajax/dream_product/mk_bid_history]: oscode needed ==>".round($this->tickets));
			error_log("[ajax/dream_product/mk_bid_history]: oscode available : ".$query);

			if ($osq > $_POST['oscode_num']) {
				$osq = $_POST['oscode_num'];
			}
			error_log("[ajax/dream_product/mk_bid_history]: osq  : ".$osq);

			//下標次數 大於 使用者擁有的殺價卷數量
			if(round($this->tickets) > $osq){

				//殺價卷不足時，判斷商品是否可以使用殺價幣(a.殺幣, b.殺幣+S碼+限S, c.殺幣+限S, d.S碼+限S, e.限S)
				if($product['pay_type'] == 'd' || $product['pay_type'] == 'e'){
					$r['err'] = 24;
					$r['retCode'] = -24;
					$r['retMsg']='殺價卷不足!';
					return $r;
				}
				/****************************
				* 使用殺幣
				****************************/
				$scode_fee = 0;
				$needspoint = 0;

				for($i = 0 ; $i < round($this->tickets-$osq); $i++)
				{
					$needspoint = $needspoint + ($all_price[$i]);
				}

				// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
				switch($product['totalfee_type']) {
					case 'A':
						$needspoint = $needspoint+($product['saja_fee'] * round($this->tickets - $_POST['oscode_num']));
					break;
					case 'B':
						$needspoint = $needspoint;
					break;
					case 'O':
						$needspoint = 0;
					break;
					case 'F':
						$needspoint = $product['saja_fee'] * round($this->tickets - $osq);
					break;
					default:
						$needspoint = $product['saja_fee']*round($this->tickets - $osq);
					break;
				}

				error_log("[ajax/dream_product/mk_bid_history]: needspoint : ".$needspoint);

				// 檢查殺幣餘額
				$query = "SELECT SUM(amount) as amount
							FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
							WHERE userid = '{$this->userid}'
								AND prefixid = '{$config['default_prefix_id']}'
								AND switch = 'Y'
				";
				$spointtable = $db->getQueryRecord($query);
				error_log("[ajax/dream_product/mk_bid_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

				if($needspoint > (float)$spointtable['table']['record'][0]['amount']){

					//'殺價幣不足'
					$r['err'] = 25;
					$r['retCode'] = -25;
					$r['retMsg']='殺價幣不足!';
					return $r;
				}else{

					//使用殺幣
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`countryid`='{$this->countryid}',
						`behav`='user_saja',
						`amount`='".- $needspoint."',
						`insertt`=NOW()
					";

					$res = $db->query($query);
					$spointid = $db->_con->insert_id;
					error_log("[ajax/dream_product/mk_bid_history]: spointid : ".$spointid);

					//活動送幣使用紀錄
					$this->saja_spoint_free_history($needspoint , $spointid, $product['productid']);

					//使用殺價券
					for($i = 0 ; $i < round($osq); $i++) {
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
						SET
							used = 'Y',
							used_time = NOW()
						WHERE
							`prefixid` = '{$config['default_prefix_id']}'
							AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
							AND switch = 'Y'
						";
						$res = $db->query($query);

						error_log("[ajax/dream_product/mk_bid_history]: oscode res ".$i." : ".$res);
					}

					//使用殺價幣
					for($i = 0 ; $i < round($this->tickets-$osq); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
						//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'{$spointid}',
							'0',
							'0',
							'0',
							'{$shdid}',
							'".($all_price[$i] * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					$k=0;
					//使用殺價券
					for($i = $this->tickets-$osq; $i < round($this->tickets); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
						//殺價下標歷史記錄（對應下標記錄與 oscodeid）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'0',
							'0',
							'{$table['table']['record'][$k]['oscodeid']}',
							'{$shdid}',
							'".($all_price[$i] * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
						$k++;
						error_log("ajax/dream_product/mk_bid_history]: oscode values : ".$table['table']['record'][($k-1)]['oscodeid']);
					}

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = 26;
						$r['retCode'] = -26;
						$r['retMsg']='下標失敗!';
						return $r;
					}
				}
			}else{   //下標次數 小於等於 使用者擁有的殺價卷數量
				//使用殺價券
				for($i = 0; $i < round($this->tickets); $i++) {
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
					$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}oscode`
					SET
						used = 'Y',
						used_time = NOW()
					WHERE
						`prefixid` = '{$config['default_prefix_id']}'
						AND oscodeid = '{$table['table']['record'][$i]['oscodeid']}'
						AND switch = 'Y'
					";
					$res = $db->query($query);

					//殺價下標歷史記錄（對應下標記錄與 oscodeid）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'0',
						'0',
						'{$table['table']['record'][$i]['oscodeid']}',
						'{$shdid}',
						'".($all_price[$i] * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";
				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = 26;
					$r['retCode'] = -26;
					$r['retMsg']='下標失敗!';
					return $r;
				}
			}
		}else if($_POST['pay_type'] == 3){
			/****************************
			* 使用S碼 : 為免費的「下標次數」，1組S碼可以下標乙次
			* 有效時間為30天(發送當天開始算30天)，逾期失效不補回。
			超級殺價卷
			****************************/

			$scodeModel = new ScodeModel;
			$scode = $scodeModel->get_scode($this->userid);

			if(empty($scode)) {
				$sq = 0;
			} else {
				$sq = $scode;
			}
			error_log("[ajax/dream_product/mk_bid_history]: scode needed ==>".round($this->tickets));

			if($sq > $_POST['scode_num']) {
				$sq = $_POST['scode_num'];
			}
			error_log("[ajax/dream_product/mk_bid_history]: sq  : ".$sq);


			//下標次數 大於 使用者擁有的超級殺價卷數量
			if(round($this->tickets) > $sq) {

				//殺價卷不足時，判斷商品是否可以使用殺價幣(a.殺幣, b.殺幣+S碼+限S, c.殺幣+限S, d.S碼+限S, e.限S)
				if($product['pay_type'] == 'd' || $product['pay_type'] == 'e'){
					$r['err'] = 24;
					$r['retCode'] = -24;
					$r['retMsg']='殺價卷不足!';
					return $r;
				}
				/****************************
				* 使用殺幣
				****************************/
				$scode_fee = 0;
				$needspoint = 0;
				for($i = 0 ; $i < round($this->tickets-$sq); $i++)
				{
					$needspoint = $needspoint + ($all_price[$i]);
				}

				switch($product['totalfee_type']) {
					case 'A':
						$needspoint = $needspoint + ($product['saja_fee'] * round($this->tickets - $_POST['scode_num']));
					break;
					case 'B':
						$needspoint = $needspoint;
					break;
					case 'O':
						$needspoint = 0;
					break;
					case 'F':
						$needspoint = $product['saja_fee'] * round($this->tickets - $sq);
					break;
					default:
						$needspoint = $product['saja_fee'] * round($this->tickets - $sq);
					break;
				}
				error_log("[ajax/dream_product/mk_bid_history]: needspoint : ".$needspoint);

				// 檢查殺幣餘額
				$query = "SELECT SUM(amount) as amount
							FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
							WHERE userid = '{$this->userid}'
							AND prefixid = '{$config['default_prefix_id']}'
							AND switch = 'Y'
				";
				$spointtable = $db->getQueryRecord($query);
				error_log("[ajax/dream_product/mk_bid_history]: spointtable : ".$spointtable['table']['record'][0]['amount']);

				if($needspoint > (float)$spointtable['table']['record'][0]['amount']) {
					//'殺價幣不足'
					$r['err'] = 25;
					$r['retCode'] = -25;
					$r['retMsg']='殺價幣不足!';
					return $r;
				} else {

					//使用殺幣
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`countryid`='{$this->countryid}',
						`behav`='user_saja',
						`amount`='".- $needspoint."',
						`insertt`=NOW()
					";
					$res = $db->query($query);
					$spointid = $db->_con->insert_id;
					error_log("[ajax/dream_product/mk_bid_history]: spointid : ".$spointid);

					//活動送幣使用紀錄
					$this->saja_spoint_free_history($needspoint, $spointid, $product['productid']);

					//使用超級殺價券
					for($i = 0 ; $i < round($sq); $i++) {

						//確認S碼
						$query = "SELECT scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
							where
							`prefixid`='{$config['default_prefix_id']}'
							and `userid` = '{$this->userid}'
							and `behav` != 'a'
							and `closed` = 'N'
							and `remainder` > 0
							and `switch` = 'Y'
							and `offtime` > NOW()
							order by insertt asc
							limit 0, 1
						";
						$table = $db->getQueryRecord($query);

						//使用S碼
						$query = "UPDATE `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
								SET
									`remainder`=`remainder` - 1,
									`modifyt`=NOW()
								where
									`prefixid`='{$config['default_prefix_id']}'
									and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
									and `userid` = '{$this->userid}'
									and `behav` != 'a'
									and `closed` = 'N'
									and `remainder` > 0
									and `switch` = 'Y'
						";
						$res = $db->query($query);
					}

					// 新增扣除組數紀錄
					$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
					SET
						`prefixid`='{$config['default_prefix_id']}',
						`userid`='{$this->userid}',
						`behav`='a',
						`productid`='{$product['productid']}',
						`amount`='". - $sq ."',
						`remainder`='0',
						`insertt`=NOW()
					";
					$res = $db->query($query);
					$scodeid = $db->_con->insert_id;

					//使用殺價幣
					for($i = 0 ; $i < round($this->tickets-$sq); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
						//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'{$spointid}',
							'0',
							'0',
							'0',
							'{$shdid}',
							'".($all_price[$i] * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}

					//使用超級殺價卷
					for($i = $this->tickets - $sq; $i < round($this->tickets); $i++) {
						$bid_total = 0;
						$bid_total = $bid_total + ($_POST['price_start']+$i);

						// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
						switch($product['totalfee_type']) {
							case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
							case 'B':$bid_total = $bid_total;break;
							case 'O':$bid_total = 0;break;
							case 'F':$bid_total = $product['saja_fee'];break;
							default:$bid_total = $product['saja_fee'];break;
						}
						error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
						//殺價下標歷史記錄（對應下標記錄與 oscodeid）
						$values[$i] = "
						(
							'{$config['default_prefix_id']}',
							'{$product['productid']}',
							'1',
							'{$this->userid}',
							'{$this->username}',
							'0',
							'{$scodeid}',
							'0',
							'0',
							'{$shdid}',
							'".($all_price[$i] * $this->currency_unit)."',
							'{$bid_total}',
							NOW(),
							'{$ip}'
						)";
					}
					error_log("ajax/dream_product/mk_bid_history]: scode values : ".$scodeid);

					if(empty($values)) {
						//'下標失敗'
						$r['err'] = -26;
						$r['retCode'] = -26;
						$r['retMsg']='下標失敗!';
						return $r;
					}
				}
			}else{ //下標次數 小於等於 使用者擁有的超級殺價卷數量
				for($i = 0 ; $i < round($this->tickets); $i++) {
					//確認S碼
					$query = "select scodeid from `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
						where
						`prefixid`='{$config['default_prefix_id']}'
						and `userid` = '{$this->userid}'
						and `behav` != 'a'
						and `closed` = 'N'
						and `remainder` > 0
						and `switch` = 'Y'
						order by insertt asc
						limit 0, 1
					";
					$table = $db->getQueryRecord($query);

					//使用S碼
					$query = "update `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
					SET
						`remainder`=`remainder` - 1,
						`modifyt`=NOW()
					where
						`prefixid`='{$config['default_prefix_id']}'
						and `scodeid` =  '{$table['table']['record'][0]['scodeid']}'
						and `userid` = '{$this->userid}'
						and `behav` != 'a'
						and `closed` = 'N'
						and `remainder` > 0
						and `switch` = 'Y'
					";
					$res = $db->query($query);
				}

				// 新增扣除組數紀錄
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}scode`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`behav`='a',
					`productid`='{$product['productid']}',
					`amount`='". - $this->tickets ."',
					`remainder`='0',
					`insertt`=NOW()
				";
				$res = $db->query($query);
				$scodeid = $db->_con->insert_id;
				for($i = 0 ; $i < round($this->tickets); $i++) {
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
					//殺價下標歷史記錄（對應下標記錄與使用S碼id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'0',
						'{$scodeid}',
						'0',
						'0',
						'{$shdid}',
						'".($all_price[$i] * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";
				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -26;
					$r['retCode'] = -26;
					$r['retMsg']='下標失敗!';
					return $r;
				}
			}
		}else{
			/****************************
			* 使用殺幣
			****************************/
			$scode_fee = 0;

			//檢查殺幣餘額
			$query = "SELECT SUM(amount) as amount
						FROM `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
						WHERE userid = '{$this->userid}'
							AND prefixid = '{$config['default_prefix_id']}'
							AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);


			$needspoint = 0;
			for($i = 0 ; $i < round($this->tickets); $i++)
			{
				$needspoint = $needspoint + ($all_price[$i]);
			}

			switch($product['totalfee_type']) {
				case 'A':
					$needspoint = $needspoint + ($product['saja_fee'] * round($this->tickets));
				break;
				case 'B':
					$needspoint = $needspoint;
				break;
				case 'O':
					$needspoint = 0;
				break;
				case 'F':
					$needspoint = $product['saja_fee'] * round($this->tickets);
				break;
				default:
					$needspoint = $product['saja_fee'] * round($this->tickets);
				break;
			}


			error_log("[ajax/dream_product/mk_bid_history] saja_fee_amount <==> table amount : ".$needspoint."<==>".(float)$table['table']['record'][0]['amount']);
			if($needspoint > (float)$table['table']['record'][0]['amount']) {
				//'殺價幣不足'
				$r['err'] = 25;
				$r['retCode'] = -25;
				$r['retMsg']='殺價幣不足!';
				return $r;
			} else {
				//使用殺幣
				$query = "INSERT INTO `{$config['db'][1]["dbname"]}`.`{$config['default_prefix']}spoint`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$this->userid}',
					`countryid`='{$this->countryid}',
					`behav`='user_saja',
					`amount`='". - $needspoint ."',
					`insertt`=NOW()
				";

				$res = $db->query($query);
				$spointid = $db->_con->insert_id;

				//活動送幣使用紀錄
				$this->saja_spoint_free_history($needspoint, $spointid, $product['productid']);

				for($i = 0 ; $i < round($this->tickets); $i++)
				{
					$bid_total = 0;
					$bid_total = $bid_total + ($_POST['price_start']+$i);

					// $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
					switch($product['totalfee_type']) {
						case 'A':$bid_total = $bid_total+$product['saja_fee'];break;
						case 'B':$bid_total = $bid_total;break;
						case 'O':$bid_total = 0;break;
						case 'F':$bid_total = $product['saja_fee'];break;
						default:$bid_total = $product['saja_fee'];break;
					}
					error_log("[ajax/product/mk_bid_history]: bid_total : ".$bid_total);
					//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
					$values[$i] = "
					(
						'{$config['default_prefix_id']}',
						'{$product['productid']}',
						'1',
						'{$this->userid}',
						'{$this->username}',
						'{$spointid}',
						'0',
						'0',
						'0',
						'{$shdid}',
						'".($all_price[$i] * $this->currency_unit)."',
						'{$bid_total}',
						NOW(),
						'{$ip}'
					)";

				}

				if(empty($values)) {
					//'下標失敗'
					$r['err'] = -26;
					$r['retCode'] = -26;
					$r['retMsg']='下標失敗!';
					return $r;
				}
			}
		}

		if(empty($r['err']) && !empty($values) ) {

			$arr_ori_winner=array();
			// 取得目前winner
			$arr_ori_winner=$this->getBidWinner($product['productid']);

			//產生下標歷史記錄
			$query = "INSERT INTO `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
			(
				prefixid,
				productid,
				ptype,
				userid,
				nickname,
				spointid,
				scodeid,
				sgiftid,
				oscodeid,
				shdid,
				price,
				bid_total,
				insertt,
				src_ip
			)
			VALUES
			".implode(',', $values)."
			";
			$res = $db->query($query);

			/*
			*下標排序
			*/
			// 20190420 加入以redis sortedset 判斷的機制
			$updWinner=false;
			//唯一金額數量
			$unique_price = 0;
			//是否為得標金額
			$is_bid_winner = 0;
			//第一個碰到的價格記下來(就是該批次內最低的出價)
			$lowest_uniprice = 0;

			$cache = getBidCache();
			if($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > $this->display_rank_time_limit)){

				// 如果設定要用redis判斷 且有redis -> 用redis 判斷
				if($config["rank_bid_by_redis"] && $cache){

					foreach($all_price as $row){
						$str_price = str_pad($row,12,"0",STR_PAD_LEFT);
						$dup_cnt = $cache->zIncrBy("PROD:RANK:".$product['productid'],1,$str_price);
						//金額是唯一值
						if($dup_cnt == 1){
							$updWinner=true;
							//取得此金額排序順位
							$r['rank'] = $cache->zRank("PROD:RANK:".$product['productid'],$str_price);

							//統計唯一值有幾個
							$unique_price = $unique_price + 1;

							if($r['rank'] == 0){
								//此金額為得標金額
								$is_bid_winner = 1;
							}else{
								//把第一個碰到的價格記下來(就是該批次內最低的出價)
								if($lowest_uniprice == 0){
									$lowest_uniprice = $row;
								}
							}

						}else if($dup_cnt == 2){
							//金額剛好兩筆重複，可能幹掉得標者，須更新得標者資訊
							$updWinner=true;
						}else{
							//不做任何更新
						}
					}

					/*
					*出價結果訊息回傳
					*/
					//單一筆出價金額
					if(count($all_price) == 1){
						if($dup_cnt == 1){
							if($is_bid_winner == 1){
								$r['result_type'] = 1;
								$r['result_msg'] = "恭喜！\n您是目前的得標者！";
							}else{
								if($r['rank'] <= 10){
									$r['result_type'] = 2;
									$r['result_msg'] = "恭喜！\n此次出價\n是唯一但不是最低。\n目前是唯一又比你低的還有".($r['rank'])."位。";
								}else{
									$r['result_type'] = 2;
									$r['result_msg'] = "恭喜！\n此次出價\n是唯一但不是最低。\n目前是唯一又比你低的超過 10 位。";
								}
							}
						}else{
							$r['result_type'] = 3;
							$r['result_msg'] = "很抱歉！\n此次的出價都與別人重複!";
						}
					}else{
					//多筆出價金額
						if($is_bid_winner == 1){
							$r['result_type'] = 1;
							$r['result_msg'] = "恭喜！\n您是目前的得標者！";
						}else{
							if($unique_price > 0){
								$r['result_type'] = 2;
								$r['result_msg'] = "恭喜！\n此次出價\n有".$unique_price."個是唯一價。\n其中最低的是".$lowest_uniprice."。";
							}else{
								$r['result_type'] = 3;
								$r['result_msg'] = "很抱歉！\n此次的出價都與別人重複!";
							}
						}
					}

				}else{  //沒有redis或沒有指定要用redis計算 -> 走原本程序進DB撈
					$max_price = $all_price[count($all_price) - 1];
					//撈取出價紀錄是否為唯一值
					$query1 = "SELECT * FROM  `{$config['db'][4]["dbname"]}`.`v_unique_price`
								WHERE productid='{$product['productid']}'
								AND price <= {$max_price} ";
					$table1 = $db->getQueryRecord($query1);

					//有唯一出價金額紀錄
					if(count($table1['table']['record']) >= 1){
						//得標金額 跟 出價陣列比對
						$chk_bid_arr = in_array((int)$table1['table']['record'][0]['price'], $all_price);
						//出價金額是得標金額
						if($chk_bid_arr){

							$r['result_type'] = 1;
							$r['result_msg'] = "恭喜！\n您是目前的得標者！";

						}else{   //出價金額非得標金額
							//單一筆出價金額
							if(count($all_price) == 1){

								//最後一筆金額 等於 出價金額
								if((int)$table1['table']['record'][count($table1['table']['record']) - 1]['price'] == $all_price[0]){
									$r['rank'] = count($table1['table']['record']) - 1;
									if($r['rank'] <= 10){
										$r['result_type'] = 2;
										$r['result_msg'] = "恭喜！\n此次出價\n是唯一但不是最低。\n目前是唯一又比你低的還有".($r['rank'])."位。";
									}else{
										$r['result_type'] = 2;
										$r['result_msg'] = "恭喜！\n此次出價\n是唯一但不是最低。\n目前是唯一又比你低的超過 10 位。";
									}
								}else{    //最後一筆金額 不等於 出價金額
									$r['result_type'] = 3;
									$r['result_msg'] = "很抱歉！\n此次的出價都與別人重複!";
								}

							}else{   //多筆出價金額

								foreach($table1['table']['record'] as $row){
									//比對撈取出的唯一金額是否包含在出價紀錄內
									if(in_array($row['price'], $all_price)){
										$unique_price = $unique_price + 1;
										//把第一個碰到的價格記下來(就是該批次內最低的出價)
										if($lowest_uniprice == 0){
											$lowest_uniprice = (int)$row['price'];
										}
									}
								}

								if($unique_price > 0){
									$r['result_type'] = 2;
									$r['result_msg'] = "恭喜！\n此次出價\n有".$unique_price."個是唯一價。\n其中最低的是".$lowest_uniprice."。";
								}else{
									$r['result_type'] = 3;
									$r['result_msg'] = "很抱歉！\n此次的出價都與別人重複!";
								}
							}
						}
					}else{   //無唯一出價金額紀錄

						$r['result_type'] = 3;
						$r['result_msg'] = "很抱歉！\n此次的出價都與別人重複!";
					}

					$updWinner=true;
				}


			}else{
				//不跳提示，只回應下標成功
				$r['result_type'] = 2;
				$r['result_msg'] = "出價成功";
			}

			// 未重複或剛好2筆重複出價者  需更新得標者
			if($updWinner) {
				error_log("[ajax/product] updWinner : ".$updWinner);
				$arr_new_winner=array();
				$status = 1; //websocket 狀態碼(1:更新得標者, 2:無人得標)
				try {
					$arr_new_winner=$this->getBidWinner($product['productid']);

					$query ="SELECT thumbnail_file, thumbnail_url
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					WHERE prefixid = '{$config['default_prefix_id']}'
						AND userid = '{$arr_new_winner['userid']}'
						AND switch = 'Y'
					";
					$table = $db->getQueryRecord($query);
					$r['thumbnail_file'] = $table['table']['record'][0]['thumbnail_file'];
					$r['thumbnail_url'] = $table['table']['record'][0]['thumbnail_url'];
					$r['shd_thumbnail_file'] = $arr_new_winner['shd_thumbnail_file'];
					if($arr_new_winner['src_ip']) {
						$r['src_ip']=maskIP($arr_new_winner['src_ip']);
						$r['comefrom']=$r['src_ip'];
					}
					error_log("[ajax/product/mk_history] new_winner :".json_encode($arr_new_winner));
					$r['winner']=$arr_new_winner['nickname'];
					$winner_name = $r['winner'];
					if(empty($r['winner'])) {
						$r['winner']="";
						$winner_name = "目前無人得標";
						$status = 2; //websocket 狀態碼(1:更新得標者, 2:無人得標)
					}

					$query2 ="SELECT shdid, userid, productid, title, content, purchase_url, thumbnail_filename, thumbnail_name as pic_name, memo, insertt
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` as shd,
							(SELECT MAX(shdid) as max_shdid FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` GROUP BY userid) shd2
							WHERE shd.shdid = shd2.max_shdid
							AND productid = '{$product['productid']}'
							AND switch = 'Y'
							ORDER BY shd.insertt DESC LIMIT 5 ";
					$table2 = $db->getQueryRecord($query2);

					// $thumbnail_file = (empty($r['thumbnail_file'])) "":$r['thumbnail_file'];//空圖片 socket給空字串
					// $thumbnail_file = null;

					$r_bidded['name'] = $r['winner'];
					$r_bidded['status'] = $status;
					$r_bidded['thumbnail_file'] = $r['thumbnail_file'];
					$r_bidded['thumbnail_url'] = $r['thumbnail_url'];
					$r_bidded['shd_thumbnail_file'] = $r['shd_thumbnail_file'];
					// 寫入redis
					$redis=getRedis('','','');
					if($redis) {
						$redis->set("PROD:".$product['productid'].":BIDDER",json_encode($r_bidded));
					}
					// websocket更新得標人員
					$ts = time();
					$client = new WebSocket\Client("wss://ws.saja.com.tw:3334");
					$ary_bidder = array('actid'		=> 'DREAM_BID_WINNER',
										'name' 		=> $winner_name,
										'productid' => "{$product['productid']}",
										'ts'		=> $ts,
										'status'	=> $status,
										'sign'		=> MD5($ts."|sjW333-_@"),
										'thumbnail_file' => $r['thumbnail_file'] ,
										'thumbnail_url'	=> $r['thumbnail_url'],
										'shd_thumbnail_file' => $r['shd_thumbnail_file'],
										'latest_bid' => $table2['table']['record']
								);
					$client->send(json_encode($ary_bidder));

					$diff_sec = $product['offtime']-time(); //大於30分鐘才執行推播
					if ($diff_sec>180) {
						$ori_userid = $arr_ori_winner['userid'];
						$new_userid = (empty($arr_new_winner['userid']))?"":$arr_new_winner['userid'];
						$post_array = array(
								"ori_userid" => $ori_userid,
								"new_userid" => $new_userid,
								"productid" => $product['productid']
							);
						// 更新得標者推播 20190819
						$ch = curl_init();
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_URL, BASE_URL.APP_DIR."/push/after_saja");
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post_array));
						curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
						$result = curl_exec($ch);
						curl_close($ch);
						// error_log("[ajax/product] push/after_saja : ".print_r($post_array));
						error_log("[ajax/product] push/after_saja ori_userid: ".$ori_userid);
						error_log("[ajax/product] push/after_saja new_userid: ".$new_userid);
						error_log("[ajax/product] push/after_saja productid: ".$product['productid']);
						error_log("[ajax/product] push/after_saja url: ".BASE_URL.APP_DIR."/push/after_saja");
					}

				} catch(Exception $e) {
					error_log("[ajax/product] exception : ".$e->getMessage());
					// 不提示
					$r['total_num']=-1;
				}
			}
		}
		error_log("[ajax/dream_product/mk_bid_history] r : ".json_encode($r));
		return $r;
	}

}
?>
