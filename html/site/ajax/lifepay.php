<?php
$_POST2=json_decode($HTTP_RAW_POST_DATA,true);
if($_POST2) {
   foreach($_POST2 as $k=> $v) {
      $_POST[$k]=$v;
	  // error_log("[ajax/mall] ".$k."=>".$v);
	  if($_POST['client']['json']) {
	     $_POST['json']=$_POST['client']['json'];
	  }
	  if($_POST['client']['auth_id']) {
	     $_POST['userid']=$_POST['client']['auth_id'];
	  }
   }
}

session_start();
// 1. 要接收 $json, 
$ret=null;
$json 	= (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
$userid = (empty($_SESSION['auth_id'])) ? $_POST['auth_id']: $_SESSION['auth_id']; 
$calltype = empty($_POST['calltype']) ? $_GET['calltype'] : $_POST['calltype'];
$snid = empty($_POST['snid']) ? $_GET['snid'] : $_POST['snid'];

if(empty($_SESSION['auth_id']) && empty($_POST['auth_id'])) { //'請先登入會員帳號'
	
	if($json=='Y') {
		$ret['retCode']=-1;
		$ret['retMsg']='請先登入會員 !!';	
	} else {
	   $ret['status'] = 1;
	}

} else {
	include_once('/var/www/html/site/lib/config.php');
	include_once(LIB_DIR ."/helpers.php");
	include_once(LIB_DIR ."/router.php");
	include_once(LIB_DIR ."/dbconnect.php");	
	include_once(LIB_DIR ."/convertString.ini.php");
	include_once(LIB_DIR ."/ini.php");
	//$app = new AppIni; 
	
	$c = new LifePay;
	if($calltype == "addorder" ) {
		$c->home();
	} elseif($calltype == "checkexpwd") {
		$c->expw_check();
	} elseif($calltype == "savecommon") {
		$c->save_common();
	} elseif($calltype == "getcommon") {
		$c->get_common();
	} elseif($calltype == "totalcount") {
		$c->count_total();
	} elseif($calltype == "checkdate") {
		$c->check_date();
	} elseif($calltype == "delcommon") {
		$c->del_common();
	}		
}
	
class LifePay {
	
	public $userid = '';
	public $json = '';
	public $epid = '';
	public $esid = '';
	public $str;
	
	//生活繳費
	public function home() {
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		$userid = (empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		if(empty($userid)) {
			$userid=$_POST['auth_id'];
		}	
		
		$this->userid = $userid;
		$ret['status'] = 0;
		
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$data['bankid'] = empty($_POST['bankid']) ? $_GET['bankid'] : $_POST['bankid'];
		$data['toid'] = empty($_POST['toid']) ? $_GET['toid'] : $_POST['toid'];
		$data['tpid'] = empty($_POST['tpid']) ? $_GET['tpid'] : $_POST['tpid'];
		$data['pcode'] = empty($_POST['pcode']) ? $_GET['pcode'] : $_POST['pcode'];
		$data['number'] = empty($_POST['number']) ? $_GET['number'] : $_POST['number'];
		$data['total'] = empty($_POST['total']) ? (int)$_GET['total'] : (int)$_POST['total'];
		$data['idnumber'] = empty($_POST['idnumber']) ? (int)$_GET['idnumber'] : (int)$_POST['idnumber'];
		$data['fee'] = empty($_POST['fee']) ? (int)$_GET['fee'] : (int)$_POST['fee'];
		$data['endtime'] = empty($_POST['endtime']) ? $_GET['endtime'] : $_POST['endtime'];
		$data['expw'] = empty($_POST['expw']) ? $_GET['expw'] : $_POST['expw'];
		$data['type'] = empty($_POST['type']) ? $_GET['type'] : $_POST['type'];
		$data['save'] = empty($_POST['save']) ? $_GET['save'] : $_POST['save'];	
		
		error_log("[ajax/lifepay/home]:".json_encode($data));

		if((empty($data['bankid']) && empty($data['toid'])) || empty($data['number'])) {
			$ret['status'] = 0;
			if($json=='Y') {
				$ret['retCode']=20001;
				$ret['retMsg']='繳費機構代碼或帳號錯誤 !!';
				$ret['retType']='MSG';
				unset($ret['status']);				
				echo json_encode($ret);	
				exit;
			}
		}
		
		//20191122 AARONFU 支付電信市話費
		if( ($data['tpid']==2) && empty($data['pcode']) ) {
			$ret['status'] = 0;
			if($json=='Y') {
				$ret['retCode']=20002;
				$ret['retMsg']='繳市話費-區碼不可空白 !!';
				$ret['retType']='MSG';
				unset($ret['status']);				
				echo json_encode($ret);	
				exit;
			}
		}
		
		if(empty($data['total']) || is_null($data['fee'])) {
			$ret['status'] = 0;
			if($json=='Y') {
				$ret['retCode']=30001;
				$ret['retMsg']='繳費金額或手續費錯誤 !!';
				$ret['retType']='MSG';
				unset($ret['status']);				
				echo json_encode($ret);	
				exit;
			}
		}
		
		if(empty($data['endtime']) || empty($data['type'])) {
			$ret['status'] = 0;
			if($json=='Y') {
				$ret['retCode']=40001;
				$ret['retMsg']='繳費到期日或繳費類型錯誤 !!';
				$ret['retType']='MSG';
				unset($ret['status']);				
				echo json_encode($ret);	
				exit;
			}
		}
		
		$workday = array();
		for ($i=1; $i < 31; $i++) { 
			if ( date("w",strtotime('+'.$i.' day'))!=0 && date("w",strtotime('+'.$i.' day'))!=6) {
				$workday[] = date( "Y-m-d",strtotime('+'.$i.' day') );
			}
		}
		
		// 繳費期限 (1:信用卡,2:房貸,3:車貸,4:信貸,5:學貸,6:電信費,7:宅公益,8:停車費,9:水費,10:電費)
		switch ($data['type']) {
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':			
				$limitday = $workday[10];
				break;
			case '6':
				$limitday = $workday[7];
				break;
			default:
				$limitday = $workday[10];
				break;
		}

		if( $data['endtime'] < $limitday ) {
			$ret['status'] = 0;
			if($json=='Y') {
				$ret['retCode']=90001;
				$ret['retMsg']='繳費到期日低於作業時間 !!';
				$ret['retType']='MSG';
				unset($ret['status']);				
				echo json_encode($ret);	
				exit;
			}
		}			
		
		// $chk = $this->expw_check();
		// if($chk['err']) {
			// $ret['status'] = $chk['err'];
			// if($json=='Y') {
				// $ret['retCode']=50001;
				// $ret['retMsg']='兌換密碼錯誤 !!';	
				// unset($ret['status']);
				// echo json_encode($ret);	
				// exit;
			// }
		// }
		
		//查詢兌換 狀態 AARONFU
		$bid_able = query_bid_able($this->userid);
		if($bid_able["exchange_enable"] !=='Y') {
			if($json=='Y') {
				$ret['retCode']=100236;
				$ret['retMsg']='兌換功能鎖定中,請洽客服';
				echo json_encode($ret);
				exit;
			} else {
				$ret['status'] = 23;
			}
		}
		
		if(empty($ret['status'])) {
			
			//檢查使用者的紅利點數
			$query ="SELECT SUM(amount) bonus 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `switch` = 'Y'
				AND `userid` = '{$this->userid}' 
			";
			$recArr = $db->getQueryRecord($query); 
			$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;

			if($user_bonus < ($data['total']+$data['fee'])) {
				//('红利點數不足');	
				if($json=='Y') {
					$ret['retCode']=60001;
					$ret['retMsg']='鯊魚點數不足 !!';
					$ret['retType']='MSG';
					unset($ret['status']);
					echo json_encode($ret);
					exit;
				} else {
					$ret['status'] = 5;
				}
			}
		}
		
		// Add By Thomasd 2019/11/20
		//檢查使用者的可折讓金額
		if(empty($ret['status'])) {
			
			// 取得用戶有折讓中的發票 (依發票時間 asc & 可折讓金額 asc 排列)
			$invoice_list_in_allowance = $this->getInvoiceListInAllowance($userid, 'S', $db);
			if($invoice_list_in_allowance){
			   // error_log("invoice_list_in_allowance : ".json_encode($invoice_list_in_allowance));
			   foreach($invoice_list_in_allowance as $item){
				  $invoice_list[]=$item;
			   }
			}

			// 取得用戶未折讓的發票, (依發票時間 asc & 可折讓金額 asc 排列)
			$invoice_list_no_allowance  = $this->getInvoiceListNotInAllowance($userid, 'S', $db);
			if($invoice_list_no_allowance){
			   // error_log("invoice_list_no_allowance : ".json_encode($invoice_list_no_allowance));
			   foreach($invoice_list_no_allowance as $item){
				 $invoice_list[]=$item;
			   }
			}

			// 保留次序  將兩批資料合併(已折讓過的發票在前面)
			if(!$invoice_list || count($invoice_list)<1){
			   if($json=='Y') {
				   $ret['retCode']=70000;
				   $ret['retMsg']='可折讓金額不足 !';
				   $ret['retType']='MSG';
				   unset($ret['status']);
				   echo json_encode($ret);
				   exit;
			   } else {
				   $ret['status'] = 7;
			   }
			}
			
			$amount = $data['total'];
			$arrAllowanceItem=array();
			foreach($invoice_list as $item){
				if($amount>0){
					$allowanceItem=array();
					$allowanceItem['tax_type']='1';
					$allowanceItem['ori_invoice_date']=$item['invoice_datetime'];
					$allowanceItem['ori_invoiceno']=$item['invoiceno'];
					$allowanceItem['ori_allowance_amt']=$item['allowance_amt'];
					$allowanceItem['ori_sales_amt']=$item['sales_amt'];
					$allowanceItem['ori_tax_amt']=$item['tax_amt'];
					$allowanceItem['ori_total_amt']=$item['total_amt'];	
					// 折讓明細目前只有一項  先寫死
					$allowanceItem['quantity']=1;
					$allowanceItem['ori_description']=$item['description'];
					// 發票剩餘可折讓金額
					$allowable_allowance_amt = $item['total_amt']-$item['allowance_amt'];
					if($allowable_allowance_amt<=0)
					   continue;
					if($amount>=$allowable_allowance_amt){
					  // 當折讓金額>=發票可折讓金額時 發票全部折讓掉
					  $item['allowance_amt']=$allowable_allowance_amt;
					  $allowanceItem['amount']=$allowable_allowance_amt;
					  $allowance_without_tax=round($allowanceItem['amount']/(1.0+$item['tax_rate']));
					  $allowanceItem['tax']=$allowanceItem['amount']-$allowance_without_tax;
					  // $allowanceItem['tax']=round($allowable_allowance_amt*$item['tax_rate']);
					  $allowanceItem['unitprice']=$allowable_allowance_amt;
					  $amount-=$allowable_allowance_amt;
					}else if($amount<$allowable_allowance_amt){
					  // 當 剩餘折讓金額<發票可折讓金額時 發票僅部分折讓
					  $item['allowance_amt']=$amount;
					  $allowanceItem['amount']=$amount;
					  $allowanceItem['unitprice']=$amount;
					  $allowance_without_tax=round($amount/(1.0+$item['tax_rate']));
					  $allowanceItem['tax']=$allowanceItem['amount']-$allowance_without_tax;
					  // $allowanceItem['tax']=round($amount*$item['tax_rate']);
					  $amount=0;
					}
					array_push($arrAllowanceItem,$allowanceItem);
				}
			}			

            // 發票可折讓額度扣完了  amount仍大於零, 表示可折讓金額不足
			if($amount>0) {
				//('可折讓金額不足');	
				if($json=='Y') {
					$ret['retCode']=70001;
					$ret['retMsg']='可折讓金額不足 !!';
					$ret['retType']='MSG';
					unset($ret['status']);
					echo json_encode($ret);
					exit;
				} else {
					$ret['status'] = 7;
				}
			}
		}
		
		if(empty($ret['status'])) {
			//產生訂單記錄
			$mk = $this->mk_order($data);

			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
		}
		
		
		if (empty($mk['orderid'])) {
			$ret['retCode']=10001;
			$ret['retType']='MSG';
			$ret['retMsg']='繳費失敗 !!';
		} else {
			//檢查使用者的紅利點數
			$query ="SELECT SUM(amount) bonus 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`  
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `switch` = 'Y'
				AND `userid` = '{$this->userid}' 
			";
			$recArr = $db->getQueryRecord($query); 
			
			$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;
			
			$ret['retCode'] = 1;
			$ret['retMsg'] = '繳費成功';
			$ret['retType'] = 'OBJ';
			$ret['retObj']['orderid'] = $mk['orderid'];
			$ret['retObj']['bonus'] = $user_bonus;
		}
		unset($ret['status']);
		echo json_encode($ret);
		exit;
		
	}

	//產生訂單記錄
	public function mk_order($data) {
		$time_start = microtime(true);

		global $db, $config;
		$r['err'] = '';
		$memo = json_encode($_POST, JSON_UNESCAPED_UNICODE);
		
		if(empty($r['err'])) {
			
			$all = $data['total']+$data['fee'];
			
			//新增訂單
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$this->userid}',
				`status`='0',
				`epid`='{$data['type']}',
				`esid`='0',
				`num`='1',
				`type`='lifepay',
				`point_price`='{$data['total']}',
				`process_fee`='{$data['fee']}',
				`total_fee`='{$all}',
				`profit`='0',
				`memo` = '{$memo}', 
				`tx_data` = '{$memo}',
				`confirm`='Y',
				`insertt`=NOW()
			";
			$res = $db->query($query); 
			$orderid = $db->_con->insert_id;
			
			$r['orderid'] = (empty($orderid)) ? '' : $orderid;
			
			//查詢使用者收件相關資料
			$query = "SELECT addressee, area, address, phone, gender
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
			WHERE prefixid = '{$config['default_prefix_id']}' AND userid = '{$this->userid}' ";
			$consignee = $db->getQueryRecord($query);			
			
			//判定是否傳值,無傳值則使用查詢資料替代
			$name = (empty($_POST['name'])) ? $consignee['table']['record'][0]['addressee'] : $_POST['name'];
			$zip = (empty($_POST['zip'])) ? $consignee['table']['record'][0]['area'] : $_POST['zip'];
			$address = (empty($_POST['address'])) ? $consignee['table']['record'][0]['address'] : $_POST['address'];
			$phone = (empty($_POST['phone'])) ? $consignee['table']['record'][0]['phone'] : $_POST['phone'];
			$gender = (empty($_POST['gender'])) ? $consignee['table']['record'][0]['gender'] : $_POST['gender'];
			
			//新增訂單收件人
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` 
			SET 
				`userid`='{$this->userid}',
				`orderid`='{$orderid}',
				`name`='{$name}',
				`phone`='{$phone}',
				`zip`='{$zip}',
				`address`='{$address}',
				`gender`='{$gender}',
				`prefixid`='{$config['default_prefix_id']}',
				`insertt`=now()
			";
			$db->query($query); 
			
			//扣除點數
			$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` set 
			          `prefixid` = '{$config['default_prefix_id']}', 
			          `userid` = '{$this->userid}', 
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', 
			          `amount` = '-{$all}', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bonusid = $db->_con->insert_id;
			error_log("[ajax/lifepay/mk_order] bonus:".$query);
			
			$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` set 
					  `prefixid` = '{$config['default_prefix_id']}', 
					  `userid` = '{$this->userid}', 
					  `orderid` = '{$orderid}',
					  `bonusid` = '{$bonusid}',
					  `amount`='-{$all}',
					  `seq` = '0', 
					  `switch` = 'Y', 
					  `insertt` = now()";
			$db->query($query);
			$ebhid=$db->_con->insert_id;
			error_log("[ajax/lifepay/mk_order] exchange_bonus_history:".json_encode($query));
			
			// 計算免費鯊魚點餘額並更新
			$query = "SELECT sum(free_amount) total_free_amount
				FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history`
				WHERE `userid`='{$this->userid}'
				AND   `switch` = 'Y'
				AND   `amount_type` = 2
				";
			$table = $db->getQueryRecord($query); 
			error_log("[ajax/lifepay/mk_order] spoint_free_history:".json_encode($query));

			if ($table['table']['record'][0]['total_free_amount'] > 0) {

				$free_amount = ($table['table']['record'][0]['total_free_amount'] >= $all) ? $all : $table['table']['record'][0]['total_free_amount'];
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` 
				SET
					`userid`='{$this->userid}',
					`behav` = 'user_exchange', 
					`amount_type` = 2, 
					`free_amount` = '-{$free_amount}', 
					`total_amount` = '-{$all}', 
					`bonusid` = '{$bonusid}', 
					`orderid` = '{$orderid}',
					`switch` = 'Y', 
					`insertt` = now()
				";
				$db->query($query); 
				error_log("[ajax/lifepay/mk_order] spoint_free_history:".json_encode($query));
			}		
		
		}
		
		return $r;
	}
	
	//兌換密碼確認
	public function expw_check() {
	
		global $db, $config, $router;
		$db = new mysql($config["db"]);
		$db->connect();
		
		$this->str = new convertString();
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$data['expw'] = empty($_POST['expw']) ? $_GET['expw'] : $_POST['expw'];
		$data['calltype'] = empty($_POST['calltype']) ? $_GET['calltype'] : $_POST['calltype'];

		$exchangepasswd = $this->str->strEncode($data['expw'], $config['encode_key']);

		$userid = (empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		if(empty($userid)) {
			$userid=$_POST['auth_id'];
		}	

		$r['err'] = '';

		$query = "SELECT * 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$userid}' 
			AND exchangepasswd = '{$exchangepasswd}' 
			AND switch = 'Y' 
		LIMIT 1
		";

		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'][0])) {
			//'兌換密碼錯誤'
			$r['retCode']=50001;
			$r['retMsg']='兌換密碼錯誤 !!';				
			$r['err'] = 5;
		} else {
			$r['retCode']=1;
			$r['retMsg']='兌換密碼正確 !!';	
		}
        // return $r;

		$ret['status'] = $r['err'];
		$ret['retCode'] = $r['retCode'];
		$ret['retMsg'] = $r['retMsg'];
		$ret['retType'] = 'MSG';	

		
		if($json=='Y') {
			unset($ret['status']);
			echo json_encode($ret);	
			exit;
		}else{
			return $ret;
		}

	}

	//總金額&手續費計算
	public function count_total() {
		
		global $db, $config;
		
		$userid = (empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		if(empty($userid)) {
			$userid=$_POST['auth_id'];
		}	
		$json 	= (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 

		$data['total'] = empty($_POST['total']) ? (int)$_GET['total'] : (int)$_POST['total'];
		$data['calltype'] = empty($_POST['calltype']) ? $_GET['calltype'] : $_POST['calltype'];
		$data['type'] = empty($_POST['type']) ? $_GET['type'] : $_POST['type'];	
		
		error_log("[ajax/lifepay/count_total]:".json_encode($data));

		if (!empty($data['total']) && !empty($data['calltype'])){
			if (empty($data['type'])){
				if ((int)$data['total'] <= 3000){
					$fee = ceil((int)$data['total']*0.005) + 130;
					$alltotal =	(int)$data['total'] + $fee;					
				} elseif((int)$data['total'] > 3000 && (int)$data['total'] <= 30000) {
					$fee = ceil((int)$data['total']*0.005) + 130;
					$alltotal =	(int)$data['total'] + $fee;	
				} elseif((int)$data['total'] > 30000) { 
					$fee = ceil((int)$data['total']*0.005) + 130;
					$alltotal =	(int)$data['total'] + $fee;	
				}
			}else{
				switch($data['type']) {
					case 1:		// 信用卡繳費
						$fee = ceil((int)$data['total']*0.005) + 130;
						$alltotal =	(int)$data['total'] + $fee;	
						break;
					case 2:		// 房地產貸款支付
						$fee = ceil((int)$data['total']*0.005) + 130;
						$alltotal =	(int)$data['total'] + $fee;	
						break;
					case 3:		// 汽機車貸款支付
						$fee = ceil((int)$data['total']*0.005) + 130;
						$alltotal =	(int)$data['total'] + $fee;	
						break;
					case 4:		// 信用貸款支付
						$fee = ceil((int)$data['total']*0.005) + 130;
						$alltotal =	(int)$data['total'] + $fee;	
						break;
					case 5:		// 就學貸款支付
						$fee = ceil((int)$data['total']*0.005) + 130;
						$alltotal =	(int)$data['total'] + $fee;					
						break;
					case 6:		// 電信費支付
						if ((int)$data['total'] <= 3000){
							$fee = 35;
							$alltotal =	(int)$data['total'] + $fee;					
						} elseif((int)$data['total'] > 3000 && (int)$data['total'] <= 5000) {
							$fee = 50;
							$alltotal =	(int)$data['total'] + $fee;	
						} elseif((int)$data['total'] > 5000 && (int)$data['total'] <= 7500) {
							$fee = 100;
							$alltotal =	(int)$data['total'] + $fee;	
						} elseif((int)$data['total'] > 7501 && (int)$data['total'] <= 10000) {
							$fee = 200;
							$alltotal =	(int)$data['total'] + $fee;						
						} elseif((int)$data['total'] > 10000) { 
							$fee = 0;
							$alltotal =	$fee;	
						}
						break;					
					case 7:		// 宅公益
						break;							
					case 8:		// 停車費支付
						break;
					case 9:		// 水費支付
						break;
					case 10:	// 電費支付
						break;
							
				}			
			}
			if ($fee > 0 && $alltotal > 0 ){
				$ret['retCode'] = 1;
				$ret['retMsg'] = '金額計算完成 !!';	
				$ret['retType']='MSG';
				$ret['fee'] = $fee;
				$ret['alltotal'] = $alltotal;	
			}else{
				$ret['retCode'] = 10002;
				$ret['retMsg'] = '金額有誤不受理 !!';
				$ret['retType']='MSG';
			}

		} else {
			$ret['retCode'] = 10001;
			$ret['retMsg'] = '資料有錯誤 !!';
			$ret['retType']='MSG';			
		}

		if($json=='Y') {
			echo json_encode($ret);	
			exit;
		} else {
			return $ret;
		}		
	}

	//儲存常用資料
	public function save_common(){
		global $db, $config, $router;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$router = new Router();
		
		$userid = (empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		if(empty($userid)) {
			$userid = $_POST['auth_id'];
		}
		
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$data['bankid'] = empty($_POST['bankid']) ? $_GET['bankid'] : $_POST['bankid'];
		$data['number'] = empty($_POST['number']) ? $_GET['number'] : $_POST['number'];
		$data['tpid'] = empty($_POST['tpid']) ? $_GET['tpid'] : $_POST['tpid'];
		$data['toid'] = empty($_POST['toid']) ? $_GET['toid'] : $_POST['toid'];
		$data['pcode'] = empty($_POST['pcode']) ? $_GET['pcode'] : $_POST['pcode'];
		$data['save'] = empty($_POST['save']) ? $_GET['save'] : $_POST['save'];	
		$data['type'] = empty($_POST['type']) ? $_GET['type'] : $_POST['type'];	
		$data['account_name'] = empty($_POST['account_name']) ? $_GET['account_name'] : $_POST['account_name'];	
		$data['branch'] = empty($_POST['branch']) ? $_GET['branch'] : $_POST['branch'];	
		$data['userid'] = $userid;
		
		$this->str = new convertString();
		$number = $this->str->strEncode($data['number'], $config['encode_key']);

		if ($data['save'] == 'Y'){
			
			$query = "SELECT * 
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` 
			WHERE 
				`switch` = 'Y' 
				AND `type` = '{$data['type']}' 
				AND `userid` = '{$data['userid']}' 
				AND `number` = '{$data['number']}'  				
			LIMIT 1
			";

			$table = $db->getQueryRecord($query);

			if(empty($table['table']['record'][0])) {
				
				switch($data['type']) {
					case 1:		// 信用卡繳費
						$infosave = $this->creditcard_info($data);
						break;
					case 2:		// 房地產貸款支付
						$infosave = $this->realestate_loan_info($data);
						break;
					case 3:		// 汽機車貸款支付
						$infosave = $this->car_loan_info($data);
						break;
					case 4:		// 信用貸款支付
						$infosave = $this->credit_loan_info($data);
						break;
					case 5:		// 就學貸款支付
						$infosave = $this->student_loan_info($data);
						break;
					case 6:		// 電信費支付
						$infosave = $this->telphone_info($data);
						break;					
					case 7:		// 宅公益
						$infosave = $this->etag_info($data);
						break;							
					case 8:		// 停車費支付
						$infosave = $this->parking_info($data);
						break;
					case 9:		// 水費支付
						$infosave = $this->water_info($data);
						break;
					case 10:	// 電費支付
						$infosave = $this->electricity_info($data);
						break;	
				
				}
			} else {
				$infosave['retCode'] = 80001;
				$infosave['retMsg'] = '已經儲存過相關資料 !!';
			}
			
			$ret['retCode'] = $infosave['retCode'];
			$ret['retMsg'] = $infosave['retMsg'];
			$ret['retType'] = 'MSG';
			
			if($json=='Y') {
				echo json_encode($ret);	
				exit;
			} else {
				return $ret;
			}			
			
		}		
	}

	//取得常用資料
	public function get_common(){
		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
	
		$userid = (empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		if(empty($userid)) {
			$userid = $_POST['auth_id'];
		}
		
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$data['type'] = empty($_POST['type']) ? $_GET['type'] : $_POST['type'];	
		$data['userid'] = $userid;
		
		$this->str = new convertString();

		switch($data['type']) {
			case 1:		// 信用卡繳費
				$query = "SELECT usn.snid, usn.bankid, usn.number, b.bankname, usn.type, '信用卡' as lnname   
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` usn 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` b on usn.bankid = b.bankid 
				WHERE 
					usn.`switch` = 'Y' 
					AND usn.`type` = '1' 
					AND usn.`userid` = '{$data['userid']}' ";
				break;
			case 2:		// 房地產貸款支付
				$query = "SELECT usn.snid, usn.bankid, usn.number, b.bankname, usn.type, ln.name as lnname, usn.account_name, usn.branch
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` usn 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` b on usn.bankid = b.bankid 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}loan` ln on usn.type = ln.loanid 
				WHERE 
					usn.`switch` = 'Y' 
					AND usn.`type` > 1 
					AND usn.`type` < 6 
					AND usn.`userid` = '{$data['userid']}' ";
				break;
			case 3:		// 汽機車貸款支付
				$query = "SELECT usn.snid, usn.bankid, usn.number, b.bankname, usn.type, ln.name as lnname, usn.account_name, usn.branch
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` usn 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` b on usn.bankid = b.bankid 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}loan` ln on usn.type = ln.loanid 				
				WHERE 
					usn.`switch` = 'Y' 
					AND usn.`type` > 1 
					AND usn.`type` < 6 
					AND usn.`userid` = '{$data['userid']}' ";
				break;
			case 4:		// 信用貸款支付
				$query = "SELECT usn.snid, usn.bankid, usn.number, b.bankname, usn.type, ln.name as lnname, usn.account_name, usn.branch
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` usn 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` b on usn.bankid = b.bankid 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}loan` ln on usn.type = ln.loanid
				WHERE 
					usn.`switch` = 'Y' 
					AND usn.`type` > 1 
					AND usn.`type` < 6 
					AND usn.`userid` = '{$data['userid']}' ";
				break;
			case 5:		// 就學貸款支付
				$query = "SELECT usn.snid, usn.bankid, usn.number, b.bankname, usn.type, ln.name as lnname, usn.account_name, usn.branch
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` usn 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` b on usn.bankid = b.bankid 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}loan` ln on usn.type = ln.loanid
				WHERE 
					usn.`switch` = 'Y' 
					AND usn.`type` > 1 
					AND usn.`type` < 6 
					AND usn.`userid` = '{$data['userid']}' ";
				break;
			case 6:		// 電信費支付
				$query = "SELECT usn.snid, usn.toid, usn.tpid, usn.pcode, usn.number, usn.type, '電信費' as lnname, tco.name as toname, tp.name as tpname  
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` usn 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}telecom_op` tco on usn.toid = tco.toid 
				left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}telpaytype` tp on usn.tpid = tp.tpid 
				WHERE 
					usn.`switch` = 'Y' 
					AND usn.`type` = 6 
					AND usn.`userid` = '{$data['userid']}' ";
				break;					
			case 7:		// 宅公益
				break;							
			case 8:		// 停車費支付
				break;
			case 9:		// 水費支付
				break;
			case 10:	// 電費支付
				break;	
		}

		$table = $db->getQueryRecord($query);

		if($table['table']['record'][0]) {				
			$ret['retCode'] = 1;
			$ret['retMsg'] = '取得常用資料 !!';
			$ret['retType'] = 'LIST';
			$ret['retObj']['data'] = $table['table']['record'];	
		} else {
			$ret['retCode'] = 10002;
			$ret['retMsg'] = '無相關資料 !!';
			$ret['retType'] = 'MSG';				
		}
		
		if($json=='Y') {
			echo json_encode($ret);	
			exit;
		} else {
			return $ret;
		}			

	}


	//刪除常用資料
	public function del_common(){
		global $db, $config, $router;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
	
		$userid = (empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		if(empty($userid)) {
			$userid = $_POST['auth_id'];
		}
		
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$data['snid'] = empty($_POST['snid']) ? $_GET['snid'] : $_POST['snid'];	
		$data['userid'] = $userid;

		$query = "SELECT usn.snid  
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` usn 
		WHERE 
			usn.`switch` = 'Y' 
			AND usn.`snid` = '{$data['snid']}' 
			AND usn.`userid` = '{$data['userid']}' ";

		$table = $db->getQueryRecord($query);

		if($table['table']['record'][0]['snid']) {		
			$query = "UPDATE `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sales_number`
				SET 
					switch = 'N' 
				WHERE 
					`userid` = '{$data['userid']}' 
					AND `snid` = '{$data['snid']}' ";
			$db->query($query);

			$ret['retCode'] = 1;
			$ret['retMsg'] = '刪除常用資料 !!';
			$ret['retType'] = 'MSG';
		} else {
			$ret['retCode'] = 10002;
			$ret['retMsg'] = '無相關資料 !!';
			$ret['retType'] = 'MSG';				
		}
		
		if($json=='Y') {
			echo json_encode($ret);	
			exit;
		} else {
			return $ret;
		}			

	}
	

	//信用卡繳費
	public function creditcard_info($data) {
		global $db, $config, $router;
		if(!empty($data['number'])){
			
			// 初始化資料庫連結介面
			$db = new mysql($config["db"]);
			$db->connect();
			
			$this->str = new convertString();
			$number = $this->str->strEncode($data['number'], $config['encode_key']);

			$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` set 
			        `userid` = '{$data['userid']}',
					`type`	 = '1',
					`bankid` = '{$data['bankid']}',
					`number` = '{$data['number']}', 		
			        `switch` = 'Y', 
			        `insertt` = now()";
			$db->query($query);	
			$ucid = $db->_con->insert_id;
			
			$ret['retCode']=200;
			$ret['retMsg']='資料儲存成功 !!';	
		} else {
			$ret['retCode']=70001;
			$ret['retMsg']='儲存資料錯誤 !!';				
		}
		return $ret;		
	}

	//房地產貸款支付
	public function realestate_loan_info($data) {

		if(!empty($data['number'])){
			global $db, $config;
			$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` set 
			        `userid` = '{$data['userid']}',
					`type`	 = '2',	
					`bankid` = '{$data['bankid']}',
					`number` = '{$data['number']}',
					`account_name` = '{$data['account_name']}',
					`branch` = '{$data['branch']}',
			        `switch` = 'Y', 
			        `insertt` = now()";
			$db->query($query);	
			$ucid = $db->_con->insert_id;
			
			$ret['retCode']=200;
			$ret['retMsg']='資料儲存成功 !!';	
		} else {
			$ret['retCode']=70001;
			$ret['retMsg']='儲存資料錯誤 !!';				
		}
		return $ret;
	}

	//汽機車貸款支付
	public function car_loan_info($data) {
		
		if(!empty($data['number'])){
			global $db, $config;
			$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` set 
			        `userid` = '{$data['userid']}',
					`type`	 = '3',	
					`bankid` = '{$data['bankid']}',
					`number` = '{$data['number']}', 
					`account_name` = '{$data['account_name']}',
					`branch` = '{$data['branch']}',	
			        `switch` = 'Y', 
			        `insertt` = now()";
			$db->query($query);	
			$ucid = $db->_con->insert_id;
			
			$ret['retCode']=200;
			$ret['retMsg']='資料儲存成功 !!';	
		} else {
			$ret['retCode']=70001;
			$ret['retMsg']='儲存資料錯誤 !!';				
		}		
		return $ret;
	}
	
	//信用貸款支付
	public function credit_loan_info($data) {
		
		if(!empty($data['number'])){
			global $db, $config;
			$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` set 
			        `userid` = '{$data['userid']}',
					`type`	 = '4',	
					`bankid` = '{$data['bankid']}',
					`number` = '{$data['number']}', 
					`account_name` = '{$data['account_name']}',
					`branch` = '{$data['branch']}',	
			        `switch` = 'Y', 
			        `insertt` = now()";
			$db->query($query);	
			$ucid = $db->_con->insert_id;
			
			$ret['retCode']=200;
			$ret['retMsg']='資料儲存成功 !!';	
		} else {
			$ret['retCode']=70001;
			$ret['retMsg']='儲存資料錯誤 !!';				
		}	
		return $ret;
		
	}	
	
	//就學貸款支付
	public function student_loan_info($data) {
		
		if(!empty($data['number'])){
			global $db, $config;
			$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` set 
			        `userid` = '{$data['userid']}',
					`type`	 = '5',	
					`bankid` = '{$data['bankid']}',
					`number` = '{$data['number']}',
					`account_name` = '{$data['account_name']}',
					`branch` = '{$data['branch']}',		
			        `switch` = 'Y', 
			        `insertt` = now()";
			$db->query($query);	
			$ucid = $db->_con->insert_id;
			
			$ret['retCode']=200;
			$ret['retMsg']='資料儲存成功 !!';	
		} else {
			$ret['retCode']=70001;
			$ret['retMsg']='儲存資料錯誤 !!';				
		}	
		return $ret;		
	}

	//電信費支付
	public function telphone_info($data) {
		global $db, $config, $router;
		if(!empty($data['number'])){
			
			// 初始化資料庫連結介面
			$db = new mysql($config["db"]);
			$db->connect();
			
			$this->str = new convertString();
			$number = $data['number'];

			$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sales_number` set 
			        `userid` = '{$data['userid']}',
					`type`	 = '6',
					`toid` = '{$data['toid']}',
					`tpid` = '{$data['tpid']}',
					`pcode` = '{$data['pcode']}',
					`number` = '{$number}', 		
			        `switch` = 'Y', 
			        `insertt` = now()";
			$db->query($query);	
			$ucid = $db->_con->insert_id;
			
			$ret['retCode']=200;
			$ret['retMsg']='資料儲存成功 !!';	
		} else {
			$ret['retCode']=70001;
			$ret['retMsg']='儲存資料錯誤 !!';				
		}
		return $ret;	
	}
	
	//宅公益
	public function etag_info($data) {

	}

	//停車費支付
	public function parking_info($data) {
		
	}

	//水費支付
	public function water_info($data) {
		
	}
	
	//電費支付
	public function electricity_info($data) {
		
	}

	//繳費到期日檢查
	public function check_date() {
		
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 
		$data['endtime'] = empty($_POST['endtime']) ? $_GET['endtime'] : $_POST['endtime'];
		
		$now =  date('Y-m-d') ;
		// 過年期間暫停作業
		if($now<'2020-01-31') {
		   $ret['retCode']=90002;
		   $ret['retMsg']='生活代繳暫停服務至2020/01/30 !';
		   $ret['retType']='MSG';
           echo json_encode($ret);	
		   exit;		   
		}
		
		$allday = (strtotime($data['endtime']) - strtotime($now)) / (60*60*24);
		
		if(strtotime($now) > strtotime($data['endtime']) || $allday < 14 ) {
			$ret['retCode']=90001;
			$ret['retMsg']='繳費到期日低於作業時間 !!';
			$ret['retType']='MSG';
		} else {
			$ret['retCode']=1;
			$ret['retMsg']='繳費到期日高於作業時間 !!';
			$ret['retType']='MSG';
		}

		echo json_encode($ret);	
		exit;		
	}
	
	// 折讓相關資料撈取
	// 取得用戶折讓中發票
    public function getInvoiceListInAllowance($userid, $utype='', $db='') {
		if(empty($userid))
		   return false;
		global $db, $config; 

        // 初始化資料庫連結介面
		if(!$db || empty($db)) {
		   $db = new mysql($config["db"]);
		   $db->connect();	
        }		
		
		// 在allowance_items資料中編號有出現的發票, 並以可折讓金額小到大, 日期舊到新排列
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` 
				  WHERE prefixid ='{$config['default_prefix_id']}' 
				   AND userid = '{$userid}' 
				   AND invoiceno IN (
						  SELECT ai.ori_invoiceno  
							FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance` a
							JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items` ai
							  ON a.salid = ai.salid
						   WHERE 1=1
							 AND a.switch='Y' 
							 AND ai.switch='Y'
							 AND a.userid='{$userid}'
				   )
				AND allowance_amt>0 ";
		if(!empty($utype)) {
		   $query.= " AND utype='{$utype}' ";
		}	
		$query.=" ORDER BY (allowance_amt-sales_amt) asc, invoice_datetime asc,  siid asc ";

		//取得資料
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}
    }
    
    // 取得用戶未折讓過的發票
    public function getInvoiceListNotInAllowance($userid, $utype='', $db='') {
		if(empty($userid)) {
		   return false;
		}   
		global $db, $config;
		
		// 初始化資料庫連結介面
		if(!$db || empty($db)) {
		   $db = new mysql($config["db"]);
		   $db->connect();	
        }		
		
		// 在allowance_items資料中編號沒有出現的發票, 並以日期舊到新, 及可折讓金額小到大排列
		$query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` 
					WHERE prefixid ='{$config['default_prefix_id']}' 
					  AND userid = '{$userid}' 
					  AND invoiceno NOT IN 
					 (
						 SELECT ai.ori_invoiceno  
							FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance` a
							JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items` ai
							  ON a.salid = ai.salid
						   WHERE 1=1
							 AND a.switch='Y' 
							 AND ai.switch='Y'
							 AND ai.ori_invoiceno!=''
							 AND a.userid='{$userid}'
					  ) ";

		if(!empty($utype)) {
		   $query.= " AND utype='${utype}' ";
		}
		$query.=" ORDER BY invoice_datetime ASC, (allowance_amt-sales_amt) asc, siid asc ";         
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}
    }
	
}
?>