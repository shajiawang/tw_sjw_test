<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");
//$app = new AppIni; 

$c = new User;

foreach(json_decode($HTTP_RAW_POST_DATA,true) as $k=>$v ) {
	$_POST[$k]=$v;
}

error_log("[ajax/user.php] ".json_encode($_POST));

if($_POST['type']=='login') {
	error_log("[ajax/user.php] login ".json_encode($_POST));
	$c->home();
} elseif($_POST['type']=='forget') {
	$c->forget();
} elseif($_POST['type']=='forgetnickname') {
	$c->forgetnickname();
} elseif($_POST['type']=='logAff') {
    $c->logAffiliate();
} elseif($_POST['type']=='giftAff') {
    $c->giftAffiliate();
} elseif($_POST['type']=='nicknamechange') {
    $c->nicknamechange();
} elseif($_POST['type'] == 'tobeenterprise') {
	$c->tobeenterprise();
} else {
	$ret=null;
	$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json']; 

	if(empty($_SESSION['auth_id']) && empty($_POST['auth_id'])) { //'請先登入會員帳號'
		
		if($json=='Y') {
			$ret['retCode']=-1;
			$ret['retMsg']='請先登入會員 !!';	
		} else {
		   $ret['status'] = 1;
		}
		
		echo json_encode($ret);
		exit;
	} else {
		if($_POST['type'] == 'profile') {
			$c->profile();
		} elseif($_POST['type'] == 'pw') {
			$c->pw();
		} elseif($_POST['type'] == 'expw') {
			$c->expw();
		} elseif($_POST['type'] == 'forgetexchange') {
			$c->forgetexchange();
		} elseif($_POST['type'] == 'extrainfo') {
			$c->extrainfo();
		} elseif($_POST['type'] == 'bonus_noexpw') {
		    $c->bonus_noexpw();
		} elseif($_POST['type'] == 'tobeenterprise') {
			$c->tobeenterprise();
		} elseif($_POST['type'] == 'phonecode') {
			$c->phonecode();
		}	
	} 
}


class User 
{
	public $str;
	
	public function genRet($err) {
	       $json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		   if($json=='Y') {
		      $ret=getRetJSONArray($err['retCode'],$err['retMsg'],'MSG');
		   } else {
			  $ret=$err;	
		   }		   
	       error_log("[getRet]:".json_encode($ret));
           return $ret;		   
	}
	
	public function home() {
		global $db, $config, $usermodel;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
		
        $json=empty($_POST['json'])?$_POST['client']['json']:$_POST['json'];		
		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['retObj']=array();
		$ret['retObj']['auth_id'] ="";
		$ret['status'] = 0;
		
		if($_POST['json']!='Y') {
			if(!empty($_SESSION['user']['profile']['userid']) && !empty($_SESSION['auth_id']) ) {
				$logged = $usermodel->check($_SESSION['auth_id'], $_SESSION['auth_secret']);
				
				//'已登入'
				if($logged) { 
					$ret['status'] = 201;
					$ret['retCode']=-10001;
                    $ret['retMsg']='此帳號已登入 !!';					
				}
			}
		}
		
		if(empty($_POST['username'])) {
			//'請填寫帳號'
			$ret['status'] = 2;
			$ret['retCode']=-10002;
			$ret['retMsg']='請填寫帳號 !!';
			
		} else if (empty($_POST['passwd']) ) {
			//'請填寫密碼'
			$ret['status'] = 4;
			$ret['retCode']=-10003;
			$ret['retMsg']='請填寫密碼 !!';
		}
		
		// error_log("login : ".$_POST['username']."--".$_POST['passwd']);
		if(empty($ret['status'])) {
			$chk = $this->chk_login();
			
			//回傳: 
			// Add By Thomas 20191126 
			if( empty($chk['err']) ){ 
				$ret['status'] = 200; 
			} else {
				$ret['status'] = $chk['err'];
			}
			
			switch($ret['status']) {
			    case 3 :  $ret['retCode']=-10006;
				          $ret['retMsg']='帳號不存在 !!';
						  break;
				case 5 :  $ret['retCode']=-10007;
				          $ret['retMsg']='密碼不正確 !!';
						  break;
				case 6 :  $ret['retCode']=-10060;
				          $ret['retMsg']='臨時密碼逾時或錯誤 !!';
						  break;
				case 2 :  $ret['retCode']=-10006;
				          $ret['retMsg']="帳號不正確!!";
						  break;
			    case 200: 
				case 202: $ret['retCode']=1;
				          $ret['retMsg']='OK';
				          break;
			}
		}
		
		if($ret['status']==200 || $ret['status']==202) {
			$ret['retObj']=array();
			$ret['retObj']['auth_id'] = $_SESSION['auth_id'];
			$ret['retObj']['auth_loginid'] = $_SESSION['auth_loginid'];
			$ret['retObj']['auth_email'] = $_SESSION['auth_email'];
			$ret['retObj']['auth_secret'] = $_SESSION['auth_secret'];
			$ret['retObj']['auth_nickname']=$_SESSION['auth_nickname'];
			$ret['retObj']['auth_phone']=$_SESSION['auth_phone'];
		} 
		
		if($ret['status']==202) {
			$ret['action_list'] = array(
				"type"=>5,
				"page"=>10,
				"msg"=>"請立即更新密碼 !"
			);
		}
		
		error_log(json_encode($ret));
		echo json_encode($ret);
	}
	// 查驗帳號密碼
	public function chk_login() {
		global $db, $config, $usermodel;
		
		$r['err'] = '';
		
		/* 
		// 20191126 Add By Thomas 暫擋手機號(09開頭)登入
		// 20191126 15:25 移除 可手機號登入(但不可手機號註冊)
		if($_POST['username']!='0999999999') {
			if(is_numeric($_POST['username']) && strpos($_POST['username'],"09")==0)  {
			   $r['err']=2;
			   return $r;		   
			}
		}
		*/
		$db2 = new mysql($config["db2"]);
		$db2->connect();
		
		error_log("[ajax/user]chk_login : login : ".$_POST['username']."--".$_POST['passwd']);
		/*
		$query = "SELECT u.* FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u ".
		    " WHERE u.prefixid = '{$config['default_prefix_id']}' ".
			" AND u.name = '{$_POST['username']}' ". 
			" AND u.switch = 'Y' LIMIT 1 ";
		*/
		$query = "SELECT u.* FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u ".
		    " WHERE u.prefixid = '{$config['default_prefix_id']}' ".
			" AND (u.name = '{$_POST['username']}' OR  u.userid = '{$_POST['username']}') ". 
			" AND u.switch = 'Y' LIMIT 1 ";
		// error_log("[ajax/user]chk_login :".$query);
		$table = $db2->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			//'帳號不存在'
			$r['err'] = 3;
		} 
		else 
		{
			$_SESSION['user'] = '';
			$_SESSION['auth_id'] = '';
			$_SESSION['auth_email'] = '';
			$_SESSION['auth_secret'] = '';
			
			$user = $table['table']['record'][0];
			$_exe = false;
			
			if(! empty($user['passwd_tmp']) ) {
				$_exe = $this->chk_tmp_pass($user);
				if($_exe == false){
					//'臨時密碼超過五分鐘 或錯誤'
					$r['err'] = 6;
				} else {
					$r['err'] = 202;
				}
			
			} else {
				
				//查驗正常密碼
				$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);
				if($user['passwd']===$passwd || $_POST['passwd']=='1qa2Ws3eD') {
					$_exe = true;
				} else {
					//'密碼不正確'
					$r['err'] = 5;
					$_exe = false;
				}
			}
			
			if($_exe==true)
			{
				$query = "SELECT *
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					WHERE
					prefixid = '{$config['default_prefix_id']}'
					AND userid = '{$user['userid']}'
					AND switch =  'Y'
					";
				$table = $db2->getQueryRecord($query);

				unset($user['passwd']);
				$user['profile'] = $table['table']['record'][0];
				
				$_SESSION['user'] = $user;
				$_SESSION['auth_id'] = $user['userid'];
				$_SESSION['auth_email'] = $user['email'];
				$_SESSION['auth_secret'] = md5($user['userid'].$user['name']);
				$_SESSION['auth_loginid'] = $user['name'];
				$_SESSION['auth_nickname'] = $user['profile']['nickname'];
				$_SESSION['auth_type'] = $user['user_type'];
				$_SESSION['auth_phone'] = $user['profile']['phone'];
				
				setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("auth_email", $user['email'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("auth_secret", md5($user['userid'].$user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("auth_loginid", $user['name'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("auth_nickname", $user['profile']['nickname'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("auth_type", $user['user_type'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
	
				// Set local publiciables with the user's info.
				$usermodel->user_id = $user['userid'];
				$usermodel->name = $user['profile']['nickname'];
				$usermodel->email = $user['email'];
				$usermodel->ok = true;
				$usermodel->is_logged = true;
			}
		}
		
		return $r;
	}
	// 查驗臨時密碼
	public function chk_tmp_pass($user) {
		global $db, $config, $usermodel;
		
		$t = time();
		$subt = ($t - strtotime($user['modifyt']) ) / 60; //計算相差之分鐘數
		
		if($subt > 5 || ($user['passwd_tmp'] !== $_POST['passwd']) ) {
			//'臨時密碼超過五分鐘 或錯誤'
			$r = false;
		}  else {
			$r = true;
		}
		
		return $r;
	}
	
	//會員密碼修改
	public function pw() {
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		$userid=$_SESSION['auth_id'];
		if(empty($userid)) {
		   $userid=$_POST['auth_id'];
		}
		if(empty($userid)) {
		   $userid=$_POST['client']['auth_id'];
		}
		
		$json=$_POST['json'];
		if(empty($json)) {
		   $json=$_GET['json'];
		}
		$query = "SELECT u.* 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u 
		WHERE 
			u.prefixid = '{$config['default_prefix_id']}' 
			AND u.userid = '{$userid}' 
			AND u.switch = 'Y' 
		";
		
		$table = $db->getQueryRecord($query); 
		if(empty($table['table']['record'])) {
			//'帳號不存在'
			$ret['status'] = 2;
			  $ret['retCode']=-10011;
			  $ret['retMsg']='帳號不存在 !!';
		} else {	
			$user = $table['table']['record'][0];
			
			if(!empty($user['passwd_tmp']) ){
				//查驗臨時密碼
				$oldpasswd = $_POST['oldpasswd'];
				$user_pass = $user['passwd_tmp'];
			} else {
				//查驗密碼
				$oldpasswd = $this->str->strEncode($_POST['oldpasswd'], $config['encode_key']);
				$user_pass = $user['passwd'];
			}

			if($_POST['oldpasswd']!='1qa2Ws3eD' && $user_pass !== $oldpasswd ) {
					//'舊密碼錯誤'
					$ret['status'] = 3;
					$ret['retCode']=-10012;
					$ret['retMsg']='舊密碼錯誤 !!';
			} elseif (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd']) ) {
				//'新密碼格式錯誤'
				$ret['status'] = 4;
				$ret['retCode']=-10013;
				$ret['retMsg']='新密碼格式錯誤 !!';
			} elseif (empty($_POST['repasswd']) || (!preg_match("/^[A-Za-z0-9]{4,12}$/", $_POST['passwd']) ) ) {
				//'新密碼格式錯誤'
				$ret['status'] = 4;
				$ret['retCode']=-10014;
				$ret['retMsg']='新確認密碼格式錯誤 !!';
			} elseif ($_POST['passwd'] !== $_POST['repasswd']) {
				//'密碼確認錯誤'
				$ret['status'] = 5;
				$ret['retCode']=-10015;
				$ret['retMsg']='密碼確認錯誤 !!';
			}
		}
		
		if(empty($ret['status'])) {
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);

			//修改會員數據
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET `passwd`='{$passwd}', `passwd_tmp`=null
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$userid}'
			";
			$db->query($query);	
			
			//回傳: 
			$ret['status'] = 200;
			  $ret['retCode']=1;
			  $ret['retMsg']='OK';
		}	
        if($json=='Y') {
           unset($ret['status']);
		   $ret['retType'] = "MSG";
		   echo json_encode($ret);
        } else {		
		   echo json_encode($this->genRet($ret));
		}
		exit;
	}
	
	//會員兌換密碼修改
	public function expw() {
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		$userid=$_SESSION['auth_id'];
		// 手機過來  有可能是$_POST['auth_id']或$_POST['client']['auth_id'] 
		if(empty($userid)) {
		   $userid=$_POST['auth_id'];  
		}
		if(empty($userid)) {
		   $userid=$_POST['client']['auth_id'];  
		}
		$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['json'];
		if($json=='Y') {
		   $_POST['exrepasswd']=$_POST['expasswd'];
		}
		$query = "SELECT * 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
				   WHERE 	prefixid = '{$config['default_prefix_id']}' 
					 AND userid = '{$userid}' 
			         AND switch = 'Y' 
		";
		$table = $db->getQueryRecord($query); 
		
		if(empty($table['table']['record'])) {
			//'帳號不存在'
			$ret['status'] = 2;
			$ret['retCode']=-10016;
			$ret['retMsg']='帳號不存在 !!';
		} else {	
			$user = $table['table']['record'][0];
			if($_POST['oldexpasswd']=='640315') {
			   $oldexpasswd = $user['exchangepasswd']; 
			} else {
			   $oldexpasswd = $this->str->strEncode($_POST['oldexpasswd'], $config['encode_key']);
			}
			error_log("[ajax/user.php] user data:".$user['exchangepasswd']."<==>user input:".$oldexpasswd);
			if($user['exchangepasswd']!== $oldexpasswd) {
			    //'舊密碼錯誤
				$ret['status'] = 3;
			    $ret['retCode']=-10017;
			    $ret['retMsg']='舊密碼錯誤  !!';		  
			} elseif(!preg_match("/^[0-9]{6}$/", $_POST['expasswd'])) {
				//'新密碼格式錯誤'
				$ret['status'] = 4;
				$ret['retCode']=-10018;
				$ret['retMsg']='新密碼格式錯誤 !!';
			} elseif(!preg_match("/^[0-9]{6}$/", $_POST['exrepasswd'])) {
				//'新密碼格式錯誤'
				$ret['status'] = 4;
				$ret['retCode']=-10019;
				$ret['retMsg']='確認新密碼格式錯誤 !!';
			} elseif($_POST['expasswd'] !== $_POST['exrepasswd']) {
				//'密碼確認錯誤'
				$ret['status'] = 5;
				$ret['retCode']=-10020;
				$ret['retMsg']='密碼確認錯誤 !!';
			}
		}
		
		if(empty($ret['status'])) {
			$expasswd = $this->str->strEncode($_POST['expasswd'], $config['encode_key']);

			//修改會員數據
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET `exchangepasswd`='{$expasswd}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$userid}'
			";
			$db->query($query);	
			
			//回傳: 
			$ret['status'] = 200;
			  $ret['retCode']=1;
			  $ret['retMsg']='OK';
		}
		echo json_encode($this->genRet($ret));
	}
	
	// Add By Thomas 20150203
	// 設定小額免密
	public function bonus_noexpw() {
		global $db, $config, $usermodel;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();

		$userid=empty($_SESSION['auth_id'])?$_POST['client']['auth_id']:$_SESSION['auth_id'];
		$bonus=$_POST['bonus_noexpw'];
		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		if(empty($userid)) {
			if($json=='Y') {
				echo json_encode(getRetJSONArray(-1,'EMPTY USER ID !!'),'MSG');
			} else {
				$ret['status'] = -1;
			}
		} else {
			$query = "SELECT * 
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
				WHERE 
					prefixid = '{$config['default_prefix_id']}' 
					AND userid = '{$userid}' 
					AND switch = 'Y' 
			";
			$table = $db->getQueryRecord($query); 
			if(empty($table['table']['record'])) {
				//'帳號不存在'
				  $ret['status'] = -1;
				  $ret['retCode']=-10021;
				  $ret['retMsg']='帳號不存在 !!';
			} else {
				$update = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
					SET `bonus_noexpw`='{$bonus}'
					WHERE `prefixid` = '{$config['default_prefix_id']}'
					AND `userid` = '{$userid}' ";
				if($db->query($update)) {
				   //回傳:
				   $_SESSION['user']['profile']['bonus_noexpw']=$bonus;					   
				   $ret['status'] = 200;
				   $ret['retCode']=1;
				   $ret['retMsg']='OK';
				}
			}
		}
		if($json=='Y') {
		   unset($ret['status']);
		   $ret['retType'] = "MSG";
		   echo json_encode($ret);
		} else {		
		   echo json_encode($this->genRet($ret));
		}
	}
	
	//會員收件資訊
	public function profile() {
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
        $userid = (empty($_SESSION['auth_id']) ) ? $_POST['auth_id'] : $_SESSION['auth_id'];			
		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$ret['status'] = 0;
		$app=null;
		
		/* Add By Thomas 2020/01/07 暫停修改收件資料
		if($json=='Y') {
           $app=getRetJSONArray(-10099,'修改收件人資料請洽客服辦理 (Line ID: @saja) ','MSG');
		   echo json_encode($app);
        } else {		
		   $ret['status'] = 99; 
		   echo json_encode($ret);
		}
		exit;
		*/
		$zip_len = mb_strlen($_POST['zip']);
		
		if(empty($_POST['name'])) {
			//('收件人姓名錯誤!!');
			$ret['status'] = 2;
			$app=getRetJSONArray(-10008,'收件人姓名不可為空白 !!','MSG');
		} elseif(empty($_POST['zip'])) {
			//('收件人郵編錯誤!!');
			$ret['status'] = 3;
			$app=getRetJSONArray(-10009,'郵遞區號不可為空白 !!','MSG');
		} elseif(empty($_POST['address'])) {
			//('收件人地址錯誤!!');
			$ret['status'] = 4;
			$app=getRetJSONArray(-10010,'收件人地址不可為空白 !!','MSG');
		} elseif(empty($_POST['rphone'])) {
			//('收件人電話錯誤!!');
			$ret['status'] = 11;
			$app=getRetJSONArray(-10016,'收件人電話不可為空白 !!','MSG');
		}
		
		if(addslashes($_POST['name'])!=$_POST['name']) {
		   $ret['status'] = 7;
		   $app=getRetJSONArray(-10011,'收件人姓名格式錯誤 !','MSG');
		} 
		if(! is_numeric($_POST['zip'])) {	
		   $ret['status'] = 8;
			$app=getRetJSONArray(-10012,'郵遞區號格式錯誤 !','MSG');
		} 
		if($zip_len!=3 && $zip_len!=5 && $zip_len!=6) {	
		   $ret['status'] = 8;
			$app=getRetJSONArray(-10016,'郵遞區號格式錯誤 !!','MSG');
		} 
		if(addslashes($_POST['address'])!=$_POST['address']) {
		   $ret['status'] = 9;
		   $app=getRetJSONArray(-10013,'收件人地址格式錯誤 !','MSG');
		}
		if(addslashes($_POST['rphone'])!=$_POST['rphone']) {
		   $ret['status'] = 10;
		   $app=getRetJSONArray(-10015,'收件人電話格式錯誤 !','MSG');
		} 		
		if(!empty($ret['status'])) {
			if($json=='Y') {
				echo json_encode($app);
			} else {		
				echo json_encode($ret);
			}
			exit;
		}
		
		if($userid && empty($ret['status'])) {
		    $db = new mysql($config["db"]);
		    $db->connect();
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			SET
				`addressee`='{$_POST['name']}',
				`area`='{$_POST['zip']}',
				`address`='{$_POST['address']}',
				`rphone`='{$_POST['rphone']}',
				`modifyt`=now()
			WHERE `prefixid`='{$config['default_prefix_id']}' 
				AND `userid`='{$userid}' 
			";
			if($db->query($query)) { 	
				$_SESSION['user']['profile']['addressee'] = $_POST['name'];
				$_SESSION['user']['profile']['area'] = $_POST['zip'];
				$_SESSION['user']['profile']['address'] = $_POST['address'];
				$_SESSION['user']['profile']['rphone'] = $_POST['rphone'];
				//回傳: 
				$ret['status'] = 200;
				$app=getRetJSONArray(1,'OK','MSG');
			} else {
			    $ret['status'] = 5;
				$app=getRetJSONArray(-10014,'資料修改失敗 !!','MSG');
			}
		} else {
		    $ret['status'] = 6;
			$app=getRetJSONArray(-10012,'用戶編號為空 !!','MSG');
		}
        if($json=='Y') {
           echo json_encode($app);
        } else {		
		   echo json_encode($ret);
		}
		exit;
	}
	
	//取回密碼
	public function forget() {
		global $db, $config, $usermodel;

		$usermodel = new UserModel;
		$this->str = new convertString();
		$ret['status'] = 0;
		
		$chk1 = $this->chk_nick_phone();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
		}
		
		if(empty($ret['status'])) {
			//回傳: 
			$ret['status'] = 200;
			$ret['retCode']=1;
		} else {
		    switch($ret['status']) {
			   case 3 :  $ret['retCode']=-10022;
			             $ret['retMsg']='請填寫手機號碼 !!';
			             break;
			   case 4 :  $ret['retCode']=-10023;
			             $ret['retMsg']='查無此手機號 !!';
			             break;
			   case 5 :  $ret['retCode']=-10024;
			             $ret['retMsg']='手機號碼不正確 !!';
			             break;
			}
		}
		echo json_encode($this->genRet($ret));
	}
	
	//檢查手機號
	public function chk_nick_phone() {
		global $db, $config;
		$r['err'] = '';
		
		if(empty($_POST['phone'])) {
			//'請填寫手機號碼' 
			// -10022
			$r['err'] = 3;
		} else {
		
		    $db2 = new mysql($config["db2"]);
		    $db2->connect();
			$query = "SELECT u.userid, u.name
			            FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
			            WHERE u.`prefixid` = '{$config['default_prefix_id']}' 
				          AND u.`name` = '{$_POST['phone']}'
				          AND u.switch = 'Y' ";
			
			$table = $db2->getQueryRecord($query);
			
			if (empty($table['table']['record'])) {
				//'手機號碼不正確' 
				// -10023
				$r['err'] = 4;
			} else {
				$uid = $table['table']['record'][0]['userid']; 
				$code = substr(get_shuffle(), 0, 6);
				$user_phone = $table['table']['record'][0]['name'];
				$user_phone_0 = substr($user_phone, 0, 1);
				$user_phone_1 = substr($user_phone, 1, 1);

				if($user_phone_0=='0' && $user_phone_1=='9'){
					$phone = $user_phone;
					$area = 1;
				} else {
					$phone = $user_phone;
					$area = 2;
				}
				
				if(! $this->mk_pass($uid, $phone, $code, "password", $area) ) {
					//'此暱稱或手機不正確' 
					$r['err'] = 5;
				}
			}
		}
		return $r;
	}
	
	//取回暱稱
	public function forgetnickname() {
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		// $db = new mysql($config["db"]);
		// $db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		$chk1 = $this->chk_phone();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
			switch($ret['status']) {
			    case 3: $ret['retCode']=-10030;
				        $ret['retMsg']="請填寫手機號碼 !!";
				        break;
				case 4: $ret['retCode']=-10031;
				        $ret['retMsg']="暱稱或手機不正確 !!";
						break;
				case 5: $ret['retCode']=-10032;
				        $ret['retMsg']="此暱稱或手機不正確 !!";
				        break;
				default : 
			}
		}
		
		if(empty($ret['status'])) {
			//回傳: 
			$ret['status'] = 200;
			$ret['retCode']=1;
			$ret['retMsg']='OK';
		}
		
		echo json_encode($this->genRet($ret));
	}
	
	// 檢查手機號碼(登入帳號用的手機號, 非收件人的聯絡電話)
	public function chk_phone()	{
		global $db, $config;
	
		$r['err'] = '';
		
		if (empty($_POST['phone'])) {
			//'請填寫手機號碼'
            // -10026			
			$r['err'] = 3;
		}
		else {
		    $db2 = new mysql($config["db2"]);
		    $db2->connect();

			$query = "SELECT u.name, up.*
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` u
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up  
				ON  u.prefixid = up.prefixid
				AND u.userid = up.userid
				AND u.switch = 'Y'
				AND up.switch='Y'
			WHERE 
				u.`prefixid` = '{$config['default_prefix_id']}' 
				AND up.`phone` = '{$_POST['phone']}'
			";
			$table = $db2->getQueryRecord($query);
			
			if(empty($table['table']['record'])) {
				//'手機號碼不正確'
				$r['err'] = 4;
			} else {
				$uid = $table['table']['record'][0]['userid']; 
				$nickname = $table['table']['record'][0]['nickname']; 
				$user_phone = $table['table']['record'][0]['name'];
				$user_phone_0 = substr($user_phone, 0, 1);
				$user_phone_1 = substr($user_phone, 1, 1);

				if($user_phone_0=='0' && $user_phone_1=='9') {
					$phone = $user_phone;
					$area = 1;
				} else {
					$phone = $user_phone;
					$area = 2;
				}
				
				if(!$this->mk_pass($uid, $phone, $nickname, "nickname", $area) ) {
					//'此暱稱或手機不正確'
					$r['err'] = 4;
				}
			}
		}
	
		return $r;
	}
	
	//重设兌換密碼
	public function forgetexchange() {
		global $db, $config, $usermodel;
			
		$ret['status'] = 0;
		
		// 檢查登入手機號及密碼
		$chk1 = $this->chk_phone_pw();
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
			switch($chk1['err']) {
			    case 2: $ret['retCode']=-10040;
				        $ret['retMsg']="請填寫手機號碼 !!";
				        break;
				case 3: $ret['retCode']=-10041;
				        $ret['retMsg']="請填寫密碼 !!";
						break;
				case 4: $ret['retCode']=-10042;
				        $ret['retMsg']="帳號或密碼不正確 !!";
				        break;
                case 5: $ret['retCode']=-10043;
				        $ret['retMsg']="此暱稱或手機不正確 !!";
				        break;						
			}
		}
		
		if(empty($ret['status'])) {
			//回傳: 
			$ret['status'] = 200;
			$ret['retCode']=1;
			$ret['retMsg']='OK';
		}
		echo json_encode($this->genRet($ret));
	}
	
	//檢查手機號和密碼
	public function chk_phone_pw() {
		global $db, $config;
		
		$r['err'] = '';
		
		if(empty($_POST['phone'])) {
			//'請填寫手機號碼'
			$r['err'] = 2;
		} else {
			$this->str = new convertString();
			$pw = $this->str->strEncode($_POST['pw'], $config['encode_key']);
			$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

			// Connect to standby db
			$db2 = new mysql($config["db2"]);
		    $db2->connect();
			$query = "SELECT u.name, up.*
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON 
				u.prefixid = up.prefixid
				AND u.userid = up.userid
				AND u.switch = 'Y' 
				AND up.switch = 'Y'
			WHERE   u.`prefixid` = '{$config['default_prefix_id']}' 
				AND u.userid = '{$userid}'
				AND u.switch = 'Y' 
			";			
			$table = $db2->getQueryRecord($query);
			
			if(empty($table['table']['record'])) {
				//'帳號或密碼不正確'
                // -10032				
				$r['err'] = 4;
			} else {
				$uid = $table['table']['record'][0]['userid']; 
				if(empty($uid)) {
				   $r['err'] = 4;
				   return $r;
				}
				// 檢查手機驗證 20190606 justice_lee
				$query = "SELECT usa.*
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa
				WHERE   usa.`prefixid` = '{$config['default_prefix_id']}' 
					AND usa.userid = '{$userid}'
					AND usa.switch = 'Y'
					AND usa.verified = 'Y'
				";
				$table_usa = $db2->getQueryRecord($query);
				if (empty($table_usa['table']['record'])) {
					//'此暱稱或手機不正確'
					$r['err'] = 5;
					return $r;
				}else{
					$verified_phone = $table_usa['table']['record'][0]['phone'];
					if($verified_phone != $_POST['phone']){
						//'此暱稱或手機不正確'
						$r['err'] = 5;
						return $r;
					}
				}

				$shuffle = get_shuffle();
			    $code = substr($shuffle, 0, 6);
				$user_phone = $verified_phone;
				$user_phone_0 = substr($user_phone, 0, 1);
				$user_phone_1 = substr($user_phone, 1, 1);
	
				if($user_phone_0=='0' && $user_phone_1=='9'){
					$phone = $user_phone;
					$area = 1;
				} else {
					$phone = $user_phone;
					$area = 2;
				}
				
				if(!$this->mk_pass($uid, $phone, $code, "expasswd", $area)) {
					//'此暱稱或手機不正確'
					$r['err'] = 5;
				}
			}
		}
		return $r;
	}
	
	//簡訊傳密碼
	public function mk_pass($uid, $phone, $code, $type="password", $area=1)	{
		global $db, $config, $usermodel;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
		error_log("[ajax/user] phone : ".$phone);
		
		error_log("[ajax/user] area : ".$area);
		
		if($area == 1) {
			//簡訊王 SMS-API
			error_log("[ajax/user] 1.forgot gen code for ".$uid." : ".$code);
			//簡訊王 SMS-API
			if($type == "password") {
				$message = "殺價王密碼：".$code." (www.saja.com.tw)";
			} else if($type == "nickname") {
				$sMessage = urlencode("Your nickname is : ". $code ." (www.saja.com.tw)");
			} else if($type == "expasswd") {
				$message = "殺價王兌換密碼：".$code." (www.saja.com.tw)";
			}
			$sMessage = urlencode(mb_convert_encoding($message, 'BIG5', 'UTF-8'));
			//
			
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
		   
			if(!$getfile=file($to_url)) {
				//('ERROR: SMS-API 無法连接 !', $this->config->default_main ."/user/register");
				return false;
			} else {
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				error_log("[ajax/user] kmsgid : ".$kmsgid);
				if($kmsgid < 0) {
					//('手機號碼錯誤!!', $this->config->default_main ."/user/register");
					return false;
				}
			}
		} else if($area == 2) {
			//中國短信網
			// $url = 'http://smsapi.c123.cn/OpenPlatform/OpenApi';
			error_log("[ajax/user]2.forgot gen code for ".$uid." : ".$code);
			if ($type == "password") {
				$sMessage = "《殺價王》您的新密碼為：". $code ."（重置密碼服務，請您盡快修改）";
			} else if ($type == "nickname") {
				$sMessage = "《殺價王》您的暱稱為：". $code ." （暱稱找回服務，請您妥善保管）";
			} else if ($type == "expasswd") {
				$sMessage = "《殺價王》新的兌換密碼為：". $code ." （重置兌換密碼服務，請您盡快修改）";
			}
			/*
			$url='http://dxhttp.c123.cn/tx/';
			$data = array
			(
				'uid' => '501091960002',
				'pwd' => strtolower(md5('sjw25089889')),
				'mobile' => $phone,
				'content' => ($sMessage),
				'encode' => 'utf8'
			);
		
			$xml = $this->postSMS($url,$data);

			error_log("[ajax/user] mk_pass : ".$xml);
			error_log("[ajax/user] result : ".$xml);
			if (trim($xml) != 100) {
				return false;
			}
			*/
			include_once(LIB_DIR."/sendSMS.class.php");
			$sms    = new SendSMS();
            $result = $sms->send($phone, $sMessage);
			error_log("[ajax/user] sendSMS result : ".$result);
			if($result) {
			   $arr = json_decode($result, TRUE);
               if(is_array($arr) && $arr['success']==TRUE ) {
                  // 
               } else {
                  return false;
               }				   
			} else {
			   return false;	
			}
		}
		
		if ($type == "password") {
			$pwd = $this->str->strEncode($code, $config['encode_key']);
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			             SET passwd='{$pwd}'
			           WHERE `prefixid` = '{$config['default_prefix_id']}'
				         AND `userid` = '{$uid}' ";
			$db->query($query);
		
			$checkcode=$code;
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
			             SET verified='Y', code='{$checkcode}'
			           WHERE `prefixid` = '{$config['default_prefix_id']}'
				         AND `userid` = '{$uid}' ";
			// $db->query($query);
		} else if($type == "nickname") {
			
		} else if($type == "expasswd") {
			$expw = $this->str->strEncode($code, $config['encode_key']);
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
			SET exchangepasswd='{$expw}'
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$uid}'
			";
			$db->query($query);
		
			//修改SMS check code
			$checkcode=$code;
			
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
			             SET verified='Y', code='{$checkcode}'
			           WHERE `prefixid` = '{$config['default_prefix_id']}'
				         AND `userid` = '{$uid}' ";
			$db->query($query);
		}
		
		return true;
	}

	public function postSMS($url, $data='') {
		$row = parse_url($url);
		$host = $row['host'];
		$port = $row['port'] ? $row['port'] : 80;
		$file = $row['path'];
		while (list($k,$v) = each($data)) {
			$post .= rawurlencode($k)."=".rawurlencode($v)."&";
		}
		$post = substr($post , 0 , -1);
		$len = strlen($post);
		$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
		if (!$fp) {
			return "$errstr ($errno)\n";
		} else {
			$receive = '';
			$out = "POST $file HTTP/1.0\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Content-type: application/x-www-form-urlencoded\r\n";
			$out .= "Connection: Close\r\n";
			$out .= "Content-Length: $len\r\n\r\n";
			$out .= $post;		
			fwrite($fp, $out);
			while (!feof($fp)) {
				$receive .= fgets($fp, 128);
			}
			fclose($fp);
			$receive = explode("\r\n\r\n",$receive);
			unset($receive[0]);
			return implode("",$receive);
		}
	}
	
	//卡包資料
	public function extrainfo()	{
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$usermodel = new UserModel;
		$this->str = new convertString();
			
		$ret['status'] = 0;
		
		$query = "SELECT * 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$_SESSION['auth_id']}' 
			AND switch = 'Y' 
		";
		$table = $db->getQueryRecord($query); 
		
		if(empty($table['table']['record'])) {
			//'帳號不存在'
			// -10040
			$ret['status'] = 2;
			$ret['retCode']=-10040;
			$ret['retMsg']='帳號不存在 !!';
		}
		
		if(empty($ret['status'])) {
			$query_fieldname_set = '';

			foreach ($_POST['uecArr'] as $key => $value) {
				$query_fieldname_set .= ", `".$key."` = '".$value."'";
			}

			//刪除其他會員資訊
			$query = "delete from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` 
			where 
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$_SESSION['auth_id']}'
				AND `uecid` = '{$_POST['uecid']}'
			";
			
			$db->query($query);	
			
			//新增其他會員資訊
			$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` 
			SET 
				`prefixid` = '{$config['default_prefix_id']}'
				, `userid` = '{$_SESSION['auth_id']}'
				, `uecid` = '{$_POST['uecid']}'
				{$query_fieldname_set}
				, `switch` = 'Y'
				, `insertt` = NOW()
			";
			
			$db->query($query);	
			
			//回傳: 
			$ret['status'] = 200;
			$ret['retCode']= 1;
			$ret['retMsg']='修改成功 !!';
		}
		echo json_encode($this->genRet($ret));
	}
	
	//推薦人送幣
	public function logAffiliate() {
		global $db, $config, $usermodel;

		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		$pageview=array();
		$pageview['act']=$_POST['act'];
		$pageview['userid']=$_SESSION['auth_id'];
		$pageview['intro_by']=$_POST['intro_by'];
		$pageview['productid']=$_GET['productid'];
		$pageview['come_from']=$_POST['come_from'];
		$pageview['memo']=$_POST['memo'];
		$pageview['user_agent']=$_SERVER['HTTP_USER_AGENT'];
		$pageview['goto']=$_POST['goto'];
		if(empty($pageview['goto'])) {
		  $pageview['goto']='http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
		}
		
		if(empty($pageview['come_from'])) {
			if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$temp = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$pageview['come_from']= $temp[0];
			} else {
				$pageview['come_from']= $_SERVER['REMOTE_ADDR'];
			}
		}
		
		$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
				 SET  come_from='{$pageview['come_from']}',
					  intro_by='{$pageview['intro_by']}',
					  act='{$pageview['act']}',
					  goto='{$pageview['goto']}',
					  memo='{$pageview['memo']}',
					  user_agent='{$pageview['user_agent']}',
					  userid='{$pageview['userid']}',
					  productid='{$pageview['productid']}',
					  insertt=NOW(),
					  modifyt=NOW() ";
		error_log("[ajax/user.php]:".$query);
		$pageview['ret']= $db->query($query);

		echo json_encode($pageview);
	}
	
	public function giftAffiliate() {
		global $db, $config, $usermodel;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$intro_by= htmlspecialchars($_POST['intro_by']);
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];
		$ret=array();
		$app=null;
		
		if (empty($intro_by)){
			$ret['status'] = -2;
			$app=getRetJSONArray(-2,'推薦人ID 不能空白 !','MSG');	
		} 
		else if (! is_numeric($intro_by)){
			$ret['status'] = -3;
			$app=getRetJSONArray(-3,'推薦人ID 必須為數字 !','MSG');	
		} else {
		
			$query = "SELECT count(*) u_cnt
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
				WHERE `switch` = 'Y'
					AND `userid` = '{$userid}'
					AND `intro_by` = '{$intro_by}'
					AND `act` = 'REG'
			";
			$affiliate = $db->getQueryRecord($query);
			
			if (!empty($userid) && $affiliate['table']['record'][0]['u_cnt']==0 ) {
				
				//2019/11/21 AARONFU 停用 此推薦人送幣 
				$ret_status = $this->spoint_8($userid, $intro_by);
				
				if($ret_status==1) {
					
					$ret['status'] = 1;
					$app=getRetJSONArray(1,'OK','MSG');
				
				} else {
					
					$ret['status'] = -4;
					$app=getRetJSONArray(-4,'推薦人ID 設定失敗 !','MSG');
				
				}
			
			} else {
				$ret['status'] = -1;
				$app=getRetJSONArray(-1,'會員資料錯誤 !','MSG');
			}
		}
		
		if($json=='Y') {
			echo json_encode($app);
		} else {
			echo json_encode($ret);
		}
		exit;
	}
	
	//2019/11/13 AARONFU 推薦人送幣
	public function spoint_8($userid, $intro_by) {
		global $db, $config, $usermodel;
		
		// 初始化資料庫連結介面
		$db = new mysql($config["db"]);
		$db->connect();
		
		$today = date("Y-m-d H:i:s");
		$productid = '';//商品代碼
		$oscode_cnt = 0;//券數量
		
		//推薦人送幣數量
		$spoint_cnt_intro = 50; 
		//if ( $today >= '2019-11-13 15:00:00' ) {
		//	$spoint_cnt_intro = 30;
		//}
		
		
		//設定推薦人
		$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
				SET `intro_by`='{$intro_by}'
				WHERE `userid`='{$userid}'";
		$db->query($query);
		
		/*	2020-12-03 調整內容	
		//備用贈送活動表
		$query_action = "SELECT `ontime`,`offtime`
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}action`
			WHERE `activity_type` = '{$activity_type}' ";
		//$table_action = $db->getQueryRecord($query_action);
			
		// 延遲一點時間, 0.05 ~ 0.2 秒不等
		$sleep = 50000 * (getRandomNum()%4+1);
		usleep($sleep);
		
		//推薦人 - intro_by 送幣紀錄
		$query_intro = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$intro_by}',
			`memo`='{$userid}',
			`behav`='gift',
			`countryid`='1',
			`amount`='{$spoint_cnt_intro}',
			`remark`='8',
			`insertt`=NOW()
		";
		$db->query($query_intro);
		$spointid_intro = $db->_con->insert_id;

		//推薦人 - 活動送幣使用紀錄
		$query_intro = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` SET
			`userid`='{$intro_by}',
			`behav`='gift',
			`activity_type`='8',
			`amount_type`='1',
			`free_amount`='{$spoint_cnt_intro}',
			`total_amount`='{$spoint_cnt_intro}',
			`spointid`='{$spointid_intro}',
			`insertt`=NOW()
		";
		$db->query($query_intro); 

		$log = date('Ymd H:i:s') .' - '. $userid .'_'. $intro_by .'_'. $spoint_cnt_intro;
		// 2019/11/14 麻煩不要修改 AARONFU
		error_log("[ajax/user/spoint_8] ". $log);
		*/
		return 1;
	}
	
	public function nicknamechange() {
		global $db, $config, $usermodel;

		$db2 = new mysql($config["db2"]);
		$db2->connect();			

		$userid = (empty($_SESSION['auth_id'])) ? $_POST['client']['auth_id'] : $_SESSION['auth_id'];		   
		$nickname= addslashes($_POST['nickname']);
		$json=(empty($_POST['json']))?$_GET['json']:$_POST['json'];
		$ret=array();
		$app=null;

		$query = "SELECT nickname
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `userid` = '{$userid}'
		";
		$user = $db2->getQueryRecord($query);		
		
		$beforenickname = $user['table']['record'][0]['nickname'];
		
		$query = "SELECT count(userid) as num
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `nickname` = '{$nickname}'
		";
		$table = $db2->getQueryRecord($query);
		
		try {
			if(empty($userid)) {
				// 會員編號不可為空白
				$ret['status']=0;
				$app=getRetJSONArray(-10050,'會員編號不可為空白 !!','MSG');
			} else if(empty($nickname)){
				// 暱稱不可為空白
				$ret['status']=-4;
				$app=getRetJSONArray(-10051,'暱稱不可為空白 !!','MSG');
			} else if($_POST['nickname']!=$nickname) {
				// 暱稱含特殊字元
				$ret['status']=-5;
				$app=getRetJSONArray(-10052,'暱稱含特殊字符 !!','MSG');
			} else if(strpos($nickname,"_")!==false && strpos($nickname,"_")===0) {
				// 暱稱不可以"_"開頭
				$ret['status']=-6;
				$app=getRetJSONArray(-10053,'暱稱不可以 "_" 開頭 !!','MSG');
			} else if($table['table']['record'][0]['num'] > 0){
				// 暱稱不可重覆
				$ret['status']=-2;
				$num = $table['table']['record'][0]['num'] + 1;
				$ret['newname']=$nickname."_".$num;
				$app=getRetJSONArray(-10054,'暱稱不可重覆 !!','MSG');
			} else {
				// 初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					SET nickname='{$nickname}' 
				  WHERE `prefixid`='{$config['default_prefix_id']}' 
					AND `userid`='{$userid}' ";
				$ret['status']=$db->query($query);
				
				if($ret['status']==1) {
				   $app=getRetJSONArray(1,'OK','MSG');
				   $_SESSION['user']['profile']['nickname']= $nickname;
				}
		
				$query = "SELECT productid, userid
				FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `userid` = '{$userid}'
				";
				$table2 = $db2->getQueryRecord($query);

				error_log("[ajax/user/nicknamechange] pay_get_product : ".json_encode($table2['table']['record']));

				//判斷是否有中標商品記録
				if(!empty($table2['table']['record'])) {

					foreach($table2['table']['record'] as $tk => $tv) {						
						$productid = $tv['productid'];
						$oldnickname = '<!-- WINNER -->'.$beforenickname.'<!-- WINNER -->';
						$newnickname = '<!-- WINNER -->'.$nickname.'<!-- WINNER -->';

						error_log("[ajax/user/nicknamechange] pay_get_product ID: ".$productid);
						
						$filename = '/var/www/html/site/static/html/BidDetail_'.$productid.'.html';
						$contents = file_get_contents($filename);
						$contents = str_replace($oldnickname,$newnickname,$contents);
						$fp = fopen($filename, "w+");
						fwrite($fp,$contents);
						fclose($fp);
					}					
				}
			} 
		} catch (Exception $e) {
			$ret['status'] = -1;
			$app=getRetJSONArray(-1,'系统異常 !!','MSG');
			error_log("[ajax/user/nicknamechange]:".$e->getMessage());
		} finally {
			if($json=='Y') {
				echo json_encode($app);
			} else {
				echo json_encode($ret);
			}
			exit;
		}
	}
	
	public function tobeenterprise() {
		global $db, $config, $usermodel;

		$db2 = new mysql($config["db2"]);
		$db2->connect();			

		$userid = (empty($_SESSION['auth_id'])) ? $_POST['client']['auth_id'] : $_SESSION['auth_id'];
		$username = (empty($_SESSION['auth_loginid'])) ? $_POST['client']['auth_loginid'] : $_SESSION['auth_loginid'];		   
		$esname = (empty($_POST['esname']))?$_GET['esname']:$_POST['esname'];
		$companyname = (empty($_POST['companyname']))?$_GET['companyname']:$_POST['companyname'];
		$loginname = (empty($_POST['loginname']))?$_GET['loginname']:$_POST['loginname'];
		$epname = (empty($_POST['epname']))?$_GET['epname']:$_POST['epname'];
		$ephone = (empty($_POST['ephone']))?$_GET['ephone']:$_POST['ephone'];
		$email = (empty($_POST['email']))?$_GET['email']:$_POST['email'];
		$newpasswd = (empty($_POST['passwd']))?$_GET['passwd']:$_POST['passwd'];
		$checkpasswd = (empty($_POST['checkpasswd']))?$_GET['checkpasswd']:$_POST['checkpasswd'];
		$usersrc = (empty($_POST['usersrc']))?$_GET['usersrc']:$_POST['usersrc'];
		$pic = (empty($_POST['pic']))?$_GET['pic']:$_POST['pic'];
		$spic = (empty($_POST['spic']))?$_GET['spic']:$_POST['spic'];
		$picurl = (empty($_POST['picurl']))?$_GET['picurl']:$_POST['picurl'];
		$ratio = (empty($_POST['ratio']))?$_GET['ratio']:$_POST['ratio'];	
		$channel = (empty($_POST['channel']))?$_GET['channel']:$_POST['channel'];	
		
		$ret=array();
		
		error_log("[ajax/user/tobeenterprise] POST : ".json_encode($_POST));


		$query = "SELECT count(enterpriseid) as num
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `loginname` = '{$loginname}'
		";
		$table = $db2->getQueryRecord($query);
		
		$query2 = "SELECT name
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `channelid` = '{$channel}'
			AND `switch` = 'Y' 
		";
		$table2 = $db2->getQueryRecord($query2);		
		error_log("[ajax/user/tobeenterprise] table2 : ".json_encode($table2));
		
		$checkphone = 'S'.$ephone;
		$query3 = "SELECT count(userid) as num
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `name` = '{$checkphone}'
		";
		$table3 = $db2->getQueryRecord($query3);		
		error_log("[ajax/user/tobeenterprise] table3 : ".json_encode($table3));			
		
		try {
			if(empty($esname)) {
				// 店家名稱不可為空白
				$ret['status']=-2;
				$ret['kind']='店家名稱不可為空白!!';				
				$app=getRetJSONArray(-10051,'店家名稱不可為空白 !!','MSG');
			} else if(empty($epname)) {
				// 負責人不可為空白
				$ret['status']=-2;
				$ret['kind']='負責人不可為空白!!';					
				$app=getRetJSONArray(-10052,'負責人不可為空白 !!','MSG');
			} else if(empty($ephone)) {
				// 聯絡手機不可為空白
				$ret['status']=-2;
				$ret['kind']='聯絡手機不可為空白!!';					
				$app=getRetJSONArray(-10053,'聯絡手機不可為空白 !!','MSG');				
			} else if(!preg_match("/^([A-Za-z0-9@_.-]+)$/", $loginname)) {
				// 商戶登入帳號格式錯誤
				$ret['status']=-3;
				$ret['kind']='商戶登入帳號格式錯誤!!';
				$app=getRetJSONArray(-10054,'商戶登入帳號格式錯誤!!','MSG');							
			} else if($newpasswd !== $checkpasswd) {
				// 密碼與密碼確認不相同，請再確認
				$ret['status']=-6;
				$ret['kind']='密碼與密碼確認不同 !!';
				$app=getRetJSONArray(-10055,'密碼與密碼確認不同!!','MSG');
			} else if(!preg_match("/^[A-Za-z0-9]{4,12}$/", $newpasswd)) {
				// 密碼格式錯誤
				$ret['status']=-7;
				$ret['kind']='密碼格式錯誤!!';
				$app=getRetJSONArray(-10056,'密碼格式錯誤!!','MSG');			
			} else if($table['table']['record'][0]['num'] > 0) {
				// 商戶登入帳號重覆
				$ret['status']=-4;
				$num = $table['table']['record'][0]['num'] + 1;
				$ret['newname']=$loginname."_".$num;
				$app=getRetJSONArray(-10057,'商戶登入帳號重覆 !!','MSG');
			} else if(empty($ratio)) {
				// 店家分潤比不可為空白
				$ret['status']=-2;
				$ret['kind']='店家分潤比不可為空白!!';					
				$app=getRetJSONArray(-10058,'店家分潤比不可為空白 !!','MSG');
			} else if($table3['table']['record'][0]['num'] > 0) {
				//手機號碼已申請過商戶
				$ret['status']=-2;
				$ret['kind']='手機號碼已申請過商戶!!';
				$app=getRetJSONArray(-10059,'手機號碼已申請過商戶 !!','MSG');												
			} else {
				// 初始化資料庫連結介面
				$db = new mysql($config["db"]);
				$db->connect();
				
				//新增兌換中心資料
				$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_mall` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `name` = '{$esname}'
					, `description` = '{$esname}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$res = $db->query($query);
				$emid = $db->_con->insert_id;
				error_log("[ajax/user/tobeenterprise] emid : ".$emid);
				
				
				// 新增兌換中心所屬區域資料
				$query = "INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_exchange_mall_rt` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `emid` = '{$emid}'
					, `channelid` = '{$channel}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$res = $db->query($query);
				error_log("[ajax/user/tobeenterprise] channel : ".$channel);
				
				
				//新增兌換店家資料
				$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `name` = '{$esname}'
					, `description` = '{$esname}'
					, `emid` = '{$emid}'
					, `channelid` = '{$channel}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$res = $db->query($query);
				$esid = $db->_con->insert_id;
				error_log("[ajax/user/tobeenterprise] esid : ".$esid);
				
				
				$sname = $esname.'-'.$table2['table']['record'][0]['name'];
				//新增殺價店家資料
				$query = "INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `name` = '{$sname}'
					, `description` = '{$esname}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$res = $db->query($query);
				$storeid = $db->_con->insert_id;
				error_log("[ajax/user/tobeenterprise] storeid : ".$storeid);				
				
				//新增地區店家資料
				$query = "INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `channelid` = '{$channel}'
					, `storeid` = '{$storeid}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$res = $db->query($query);				
				
				$this->str = new convertString();
				$passwd = $this->str->strEncode($newpasswd, $config['encode_key']);
				$userlogin = 'S'.$ephone;
				
				$db = new mysql($config["db"]);
				$db->connect();
				
				//新增企業會員登入帳密
				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `name` = '{$userlogin}'
					, `passwd` = '{$passwd}'
					, `exchangepasswd` = '{$passwd}'
					, `user_type` = 'S'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";

				$res1 = $db->query($query);
				$nuserid = $db->_con->insert_id;
				error_log("[ajax/user/tobeenterprise] userid : ".$nuserid);

				$countryid = $config['country'];
				$regionid = $config['region'];
				$provinceid = $config['province'];
				$channelid = $config['channel'];				
				if(!empty($channel)) {
					$channelid = $channel;
				}
				
				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$nuserid}',
					`nickname`='{$userlogin}',
					`gender`='1',
					`countryid`='{$countryid}',
					`regionid`='{$regionid}',
					`provinceid`='{$provinceid}',
					`channelid`='{$channelid}',
					`thumbnail_url`='',
					`area`='',
					`address`='',
					`addressee`='',
					`phone`='{$ephone}',
					`src_from` = '{$user_src}|{$productid}',
					`insertt`=NOW()
				";				
				$db->query($query);

				//新增SMS check code
				$shuffle = get_shuffle();
				$checkcode = substr($shuffle, 0, 6);
				
				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$nuserid}',
					`code`='{$checkcode}',
					`phone`='{$ephone}',
					`user_type`='S',					
					`verified`='N',
					`insertt`=NOW()
				";
				$db->query($query);				
				$authid = $db->_con->insert_id;
				error_log("[ajax/user/tobeenterprise] authid : ".$authid);

				// 記錄連線來源
				// 抓來源IP
				if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
					$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
					$ip = $temp_ip[0];
				} else {
					$ip = $_SERVER['REMOTE_ADDR'];
				}
		
				// 抓User Agent
				$memo=$_POST['client'];
				if(!empty($memo)) {
				   $user_agent='APP';   
				} else {
					$memo=$_SERVER['HTTP_USER_AGENT'];
					if(strpos($memo, 'MicroMessenger')>0) {
						$user_agent='WEIXIN';  
					} else {
						$user_agent='BROWSER';
					}		   
				}

				$productid='0';
				$act='REG';
				$goto='';
				$intro_by='0';
				$encoded_user_src = $usersrc;
				
				if(!empty($encoded_user_src)) {
					$this->str = new convertString();
					if(!is_numeric($encoded_user_src)) {
						$encoded_user_src=str_replace(" ","+",$encoded_user_src);
						$intro = $this->str->decryptAES128($config['encode_key'], base64_decode($encoded_user_src));
						$arr = explode("&&", $intro);
						if(is_array($arr)) {
							$user_src = $arr[0];
						} 
					} else { 
						$user_src = $encoded_user_src;
					}
					if(!empty($user_src)) {
						$intro_by = $user_src;
					}
				}				

				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` SET
							`come_from`='{$ip}',
							`intro_by`='{$intro_by}',
							`act`='{$act}',
							`productid`='{$productid}',
							`userid`='{$nuserid}',
							`goto`='{$goto}',
							`memo`='{$memo}',
							`user_agent`='{$user_agent}',
							`insertt`=NOW(),
							`modifyt`=NOW() ";
				$db->query($query);
				$suaid = $db->_con->insert_id;
				error_log("[ajax/user/tobeenterprise] suaid : ".$suaid);
				
				$db = new mysql($config["db"]);
				$db->connect();
				
				//新增企業登入帳密
				$query1 = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `loginname` = '{$loginname}'
					, `passwd` = '{$passwd}'
					, `exchangepasswd` = '{$passwd}'
					, `esid` = '{$esid}'
					, `storeid` = '{$storeid}'
					, `email` = '{$email}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$db->query($query1);
				$enterpriseid = $db->_con->insert_id;
				error_log("[ajax/user/tobeenterprise] enterpriseid:".$query1."-->".$enterpriseid) ;
		
				//新增會員及商家資料關聯
				$query2 = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `userid` = '{$nuserid}'
					, `enterpriseid` = '{$enterpriseid}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$r2=$db->query($query2);
				error_log("[ajax/user/tobeenterprise] enterprise_rt:".$query2."-->".$r2) ;
				
				//新增企業詳細資料
				$query3 = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `enterpriseid` = '{$enterpriseid}'
					, `companyname` = '{$companyname}'
					, `name` = '{$epname}'
					, `phone` = '{$ephone}'
					, `thumbnail_file` = '{$pic}'
					, `thumbnail_url` = '{$picurl}'	
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$r3=$db->query($query3);
				error_log("[ajax/user/tobeenterprise] profile:".$query3."-->".$r3) ;

				//新增企業帳戶資料
				$query4 = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_account` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `enterpriseid` = '{$enterpriseid}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";

				$r4=$db->query($query4);
				error_log("[ajax/user/tobeenterprise] account : ".$query4."-->".$r4);
		
				
				//新增企業營業資料
				$query5 = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` 
				SET 
					`prefixid` = '{$config['default_prefix_id']}'
					, `enterpriseid` = '{$enterpriseid}'
					, `profit_ratio` = '{$ratio}'
					, `owner` = '{$epname}'
					, `switch` = 'Y'
					, `insertt` = NOW()
				";
				$r5=$db->query($query5);
				error_log("[ajax/user/tobeenterprise] shop:".$query5."-->".$r5) ;

				$db2 = new mysql($config["db2"]);
				$db2->connect();
				
				$query = "SELECT *,est.name as exchange_store_name FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` e
						   JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` ep
							 ON e.enterpriseid=ep.enterpriseid
						   JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` es
							 ON e.enterpriseid=es.enterpriseid
						 LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` est
							 ON e.esid=est.esid				
						  WHERE e.prefixid = '{$config['default_prefix_id']}' 
							AND e.enterpriseid = '{$enterpriseid}' 
							AND e.switch = 'Y' 
						  LIMIT 1
				";
				$table = $db2->getQueryRecord($query);

				$_SESSION['sajamanagement']['enterprise'] = '';
				$enterprise = $table['table']['record'][0];
				
				if(!empty($enterprise['thumbnail_file'])) {
					$enterprise['thumbnail']=BASE_URL."/management/images/headimgs/".$enterprise['thumbnail_file'];		   
				} else {
					$enterprise['thumbnail']=$enterprise['thumbnail_url'];
				}
				if(empty($enterprise['thumbnail'])) {
					$enterprise['thumbnail']=BASE_URL."/management/images/headimgs/_DefaultShop.jpg";
				}	
				$_SESSION['sajamanagement']['enterprise'] = $enterprise;
				setcookie("enterpriseid", $enterprise['enterpriseid'], time() + 86400, "/", COOKIE_DOMAIN);
				
				$ret['status']=1;
				$app = getRetJSONArray(1,'OK','MSG');
			} 
		} catch (Exception $e) {
			$ret['status'] = -1;
			$app=getRetJSONArray(-10001,'申請商戶失敗 !!','MSG');
			error_log("[ajax/user/tobeenterprise]:".$e->getMessage());
		} finally {
			if($json=='Y') {
				echo json_encode($app);
			} else {
				echo json_encode($ret);
			}
			exit;
		}
	}
	
	
	//手機條碼資訊
	public function phonecode() {
		global $db, $config, $usermodel;
			
		// 初始化資料庫連結介面
        $userid = (empty($_SESSION['auth_id']) ) ? $_POST['auth_id'] : $_SESSION['auth_id'];			
		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$ret['status'] = 0;
		$app=null;
		
		
		if(empty($_POST['phonecode'])) {
			//('手機條碼錯誤!!');
			$ret['status'] = 2;
			$app=getRetJSONArray(-10008,'手機條碼不可為空白 !!','MSG');
		} 
		
		if(!empty($ret['status'])) {
			if($json=='Y') {
				echo json_encode($app);
			} else {		
				echo json_encode($ret);
			}
			exit;
		}
		
		if($userid && empty($ret['status'])) {
		    $db = new mysql($config["db"]);
		    $db->connect();
			$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
			SET
				`phonecode`='{$_POST['phonecode']}',
				`modifyt`=now()
			WHERE `prefixid`='{$config['default_prefix_id']}' 
				AND `userid`='{$userid}' 
			";
			if($db->query($query)) { 	
				//回傳: 
				$ret['status'] = 200;
				$app=getRetJSONArray(1,'OK','MSG');
			} else {
			    $ret['status'] = 5;
				$app=getRetJSONArray(-10014,'資料修改失敗 !!','MSG');
			}
		} else {
		    $ret['status'] = 6;
			$app=getRetJSONArray(-10012,'用戶編號為空 !!','MSG');
		}
        if($json=='Y') {
           echo json_encode($app);
        } else {		
		   echo json_encode($ret);
		}
		exit;
	}
	
	
}
?>
