<?php

include_once("helpers.php");

/*資料連結物件*/
class mysql
{
	public $_con = '' ;
	protected $_config ;

	public function __construct($config)
	{
		if(!isset($config) && !is_array($config)){
			return false ;
		}
		$this->_config = $config ;
	}

	public function hostInfo() {
	       return ($this->_config[0]['host']."=>".$this->_config[0]['dbname']);
	}
	/* 資料連結 */
	/*
array(5) {
  [0]=&gt;  array(6) {["dbname"]=&gt;    string(9) "saja_user"  }
  [1]=&gt;  array(1) {["dbname"]=&gt;    string(14) "saja_cash_flow"  }
  [2]=&gt;  array(1) {["dbname"]=&gt;    string(12) "saja_channel"  }
  [3]=&gt;  array(1) {["dbname"]=&gt;    string(13) "saja_exchange"  }
  [4]=&gt;  array(1) {["dbname"]=&gt;    string(9) "saja_shop"  }
}*/
	public function connect()
	{
		// error_log("DB host:".$this->_config[0]['host']);
                // error_log("DB username:".$this->_config[0]['username']);
                // error_log("DB password:".$this->_config[0]['password']);
                global $config;

                $this->_config = getDbConfig();
		// error_log("mysql->connect()...");
		//DB密碼解密
		$db_password_decode = $this->strDecode_dbpassword($this->_config[0]['password'],'','');

		//$this->_con= new mysqli($this->_config[0]['host'], $this->_config[0]['username'], $this->_config[0]['password'], $this->_config[0]['dbname']);
		$this->_con= new mysqli($this->_config[0]['host'], $this->_config[0]['username'], $db_password_decode, $this->_config[0]['dbname']);

		if ($this->_con->connect_error) {
				echo 'mysql connect error:' . $this->_con->connect_error ; exit ;
		}

		if(!$this->_con->set_charset($this->_config[0]['charset'])){
			echo 'mysql connect error:' . $this->_con->connect_error; exit ;
		}
	}

	/**===============================================================**/
    /* 將字串加入引號及反斜線  */
    public function quote($var = null)
	{
        if(is_array($var)){
            foreach ($var as $key => $value) {
                $var[$key] = parent :: quote($value);
            }
            return join(',', $var);

        }elseif($var === null){
            return 'NULL';
        }else{
            return parent :: quote($var);
        }
    }

    /* 將 $query 字串與陣列做變數合併  */
    public function bind($query, array $bind = array ())
	{
        foreach($bind as $key => $value){
            if ($key[0] == '/' && $key[1] == '*') {
                continue;
            }

            $bind[$key] = $this->quote($value);
        }

        return strtr($query, $bind);
    }

	/**===============================================================**/
    /* 封裝 exec */
    public function exec($query, array $bind = array ())
	{
        $query = $this->bind($query, $bind);

        $stmt = parent :: query($query);
        $rowCount = $stmt->rowCount();
        $stmt->closeCursor();
        $stmt = null;
        return $rowCount;
    }

    /* 封裝 query */
    public function getquery($query, array $bind = array ())
	{
        $query = $this->bind($query, $bind);

        $stmt = parent :: query($query);
        $stmt->setFetchMode(self :: $_fetch_mode);
        return $stmt;
    }

    /* 僅取得筆數 */
    public function queryCount($query, array $bind = array ())
	{
        $query = $this->bind($query, $bind);
        $query = sprintf('SELECT COUNT(*) FROM (%s)AS QUERYCOUNT', $query);

        $stmt = parent :: query($query);
        $rowCount = $stmt->fetchColumn();
        $stmt->closeCursor();
        $stmt = null;
        return $rowCount;

    }


	/**===============================================================**/
	/**
	* Query
	* @access public
	* @link self()
	* @return boolean
	* @example $this->query();
	*/
	public function query($query)
	{

		if(!$query){
			echo '$query is empty!' ; exit ;
		}
        if(!$this->_con) {
            $this->connect();
        }

		if(!$this->_result = $this->_con->query($query)){
			echo '$this->_con->query fail! : '.$this->_con->error.": $query" ; exit ;
		}

		$this->queryAssign('query',$query);

		return true;
	}

	public function getQueryRecord($query)
	{
		$arr = "";
		$this->queryAssign('getQueryRecord',$query);

		if($this->query($query)===false){
			return false ;
		}

		$arr=array();
		while($raw = $this->_result->fetch_assoc()){
			$arr[] = $raw ;
		}

		$result["table"]["record"] =  $arr ;
		return  $result ;
	}

    public function getRecord($query)
	{
		$arr = "";
		$this->queryAssign('getQueryRecord',$query);

		if($this->query($query)===false){
			return false ;
		}

		$arr=array();
		while($raw = $this->_result->fetch_assoc()){
			$arr[] = $raw ;
		}
		return  $arr;
	}

   /* get field by index key leo
   @db database name
   @table table name
   @fileds request column value
   @pkey primay key name
   @value pkey value
    */
	public function getFields($db,$table,$fileds,$pkey,$value)
	{
		$cnt=explode(",",$fileds);
		if ($cnt >1){
			$query="Select $fileds from `$db`.`$table` where `$pkey`='{$value}'";
			$this->queryAssign('getQueryRecord',$query);
			if($this->query($query)===false){
				return false ;
			}
			$arr=array();
			while($raw = $this->_result->fetch_assoc()){
				$arr[] = $raw ;
			}
			return  $arr;
		}else{
			$query="Select $fileds from `$db`.`$table` where `$pkey`='{$value}'";
			$this->queryAssign('getQueryRecord',$query);

			if($this->query($query)===false){
				return false ;
			}

			$arr=array();
			while($raw = $this->_result->fetch_assoc()){
				$arr[] = $raw ;
			}
			return  $arr[0][$fileds];
		}
	}

	public function queryAssign($type , $query)
	{
		$this->queryArr[$type][] = $query ;
	}

    public function recordPage($rows,$obj)
	{
		global $config, $status;
		$p = (!empty($_GET['p'])) ? $_GET['p'] : $_POST['p'];
		$json = (!empty($_GET['json'])) ? $_GET['json'] : $_POST['json'];
		$perpage = 	(!empty($_GET['perpage'])) ? $_GET['perpage'] : $_POST['perpage'];
		$currpage =	(!empty($p)) ? $p : $_POST['page'];



		//echo "<pre>";         print_r($obj);          exit;
		$max_page = (!empty($perpage)) ? $perpage : $config['max_page'];
		$max_range = $config['max_range'];
		if(!isset($currpage)){ $currpage = "" ;}
		if(trim($currpage)=="" or $currpage < 1){ $currpage = 1  ; }
		$lastpage = ceil($rows/$max_page);
		if($currpage > $lastpage){ $currpage=1 ;}
		$rec_start= ($currpage-1)*$max_page +1;
		$rec_end  = $rec_start + $max_page -1;
		$ploops   = floor(($currpage-1)/$max_range)*$max_range + 1 ;
		$ploope   = $ploops + $max_range -1;
		if($ploope >= $lastpage){ $ploope=$lastpage; }
		$ppg      = $currpage - 1 ;
		$npg      = $currpage + 1 ;
		if($ppg<= 0) $ppg=$lastpage;
		if($npg > $lastpage) $npg=1;
		if($rec_end > $rows) $rec_end=$rows;
		$totalpages = $lastpage;
		if(ceil($rows/$max_page) < 2){ $totalpages = 1;}else{$totalpages = ceil($rows/$max_page);}

		if ((!empty($json)) && ($json == 'Y')){
			$page["rec_start"]       = $rec_start ;
			$page["totalcount"]      = $rows ;							//總共筆數
			$page["totalpages"]      = $totalpages;						//總共頁數
			$page["perpage"]      	 = $max_page ;						//每頁筆數
			$page["page"]       	 = $currpage ; 					//現在頁數
		}else{
			$page["rec_start"]       = $rec_start ;
			$page["rec_end"]         = $rec_end ;
			$page["firstpage"]       = 1 ;
			$page["lastpage"]        = $lastpage ;
			$page["previousrange"]   = $ploops - $max_range ;
			$page["nextrange"]       = $ploops + $max_range ;
			$page["previouspage"]    = $ppg ;
			$page["nextpage"]        = $npg ;
			$page["thispage"]        = $currpage ;
			$page["total"]           = $rows ;
			$page["loop"]            = $lastpage+1 ;
			$page["max_page"]         = $max_page ;
			$page["max_range"]        = $max_range ;
		}

		for($i=$ploops;$i <= $ploope;$i++){
			$page["item"][]["p"] = $i ;
		}

		if((!empty($currpage)) && ($json != 'Y')){
			if (empty($status["path"]["page"])) {
					$status["path"]["page"] = '&p='.$page["thispage"] ;
			} else {
					$status["path"]["page"] .= ('&'.'p='.$page["thispage"]) ;
			}
		}

		return $page;
	}

    /* DB密碼解密 */
	function strDecode_dbpassword($data,$key='' ,$encode_type = 'crypt'){
		global $config;
                return $data;
                /*
                if(empty($key))
		   $key = $config["encode_key"];
		if($encode_type == "crypt"){
				$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256 , MCRYPT_MODE_ECB);
				$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
				return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$key,urldecode($data),MCRYPT_MODE_ECB,$iv));
		}
		elseif($encode_type == "md5"){
				return urldecode($data);
		}
		else{
				echo "Please write \$encode_type type to config.ini.php file!!"; exit ;
		}
                */
	}

        public function __destruct() {
           if($this->_con) {
                // error_log("[dbconnect] closing connection ...");
                $this->_con->close();
                $this->_con=null;
           }
       }
}
?>
