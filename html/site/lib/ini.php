<?php
/**
 * App Controller
 * This is the main app controller, used for connecting to the database (via PHP PDO),
 * creating a new instance of both the Template and User model, and for routing the
 * current request to the correct action.
 */
// Load helper functions and the model classes.
include_once(LIB_DIR ."/helpers.php");

include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/template.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/site.php");
include_once(LIB_DIR ."/lib_language.php"); //語系物件


include_once(BASE_DIR ."/model/bid.php");
include_once(BASE_DIR ."/model/channel.php");
include_once(BASE_DIR ."/model/deposit.php");
include_once(BASE_DIR ."/model/faq.php");
include_once(BASE_DIR ."/model/history.php");
include_once(BASE_DIR ."/model/mall.php");
include_once(BASE_DIR ."/model/member.php");
include_once(BASE_DIR ."/model/product.php");
include_once(BASE_DIR ."/model/user.php");
include_once(BASE_DIR ."/model/scode.php");
include_once(BASE_DIR ."/model/oscode.php");
include_once(BASE_DIR ."/model/googlemap.php");
include_once(BASE_DIR ."/model/enterprise.php");
include_once(BASE_DIR ."/model/issues.php");
include_once(BASE_DIR ."/model/showoff.php");
include_once(BASE_DIR ."/model/broadcast.php");
include_once(BASE_DIR ."/model/invoice.php");
include_once(BASE_DIR ."/model/share.php");
include_once(BASE_DIR ."/model/newscode.php");
include_once(BASE_DIR ."/model/sys.php");
include_once(BASE_DIR ."/model/ad.php");
include_once(BASE_DIR ."/model/exchangecard.php");
include_once(BASE_DIR ."/model/statistics.php");
include_once(BASE_DIR ."/model/push.php");
include_once(BASE_DIR ."/model/dream_product.php");
include_once(BASE_DIR ."/model/dream_history.php");
include_once(BASE_DIR ."/model/dream.php");
include_once(BASE_DIR ."/model/app_exe.php");
include_once(BASE_DIR ."/model/bbc.php");

class AppIni
{
    public $routes = array();

	/**
     * Main constructor function, used to initialize the models, connect to the DB and
     * route the user to where they need to go.
     */
	public function __construct()
	{
		global $config, $db, $tpl, $bid, $deposit, $faq, $history, $mall, $member, $product, $user, $scodeModel,
		$oscode, $googlemap, $channel, $enterprise, $issues, $showoff, $broadcast, $invoice, $share, $newscode, $sys, $ad, $exchangecard, $statisticsModel,
		$push, $dream_product, $dream_history, $dream, $app_exe, $bbc;
		

		// Connect to the database.
		try {

			// 初始化資料庫連結介面
			$db = new mysql($config["db"]);
			// 20141128 disabled by Thomas, 因為進入首頁時不須連線DB, 暫時先移除
			// $db->connect();
		} catch (Exception $e) {
		    die($e);
		}

		// Create new models.
		$tpl 		= new TemplateModel;
		$bid 		= new BidModel;
		$channel 	= new ChannelModel;
		$deposit	= new DepositModel;
		$faq 		= new FaqModel;
		$history	= new HistoryModel;
		$mall 		= new MallModel;
		$member 	= new MemberModel;
		$product 	= new ProductModel; //var_dump($product);
		$user 		= new UserModel;
		$scodeModel = new ScodeModel;
		$googlemap 	= new GoogleMapModel;
		$enterprise = new EnterpriseModel;
		$issues 	= new IssuesModel;
		$showoff 	= new ShowoffModel;
		$oscode 	= new OscodeModel;
		$broadcast	= new BroadcastModel;
		$invoice	= new InvoiceModel;
		$share		= new ShareModel;
		$newscode	= new NewscodeModel;
		$sys	    = new SysModel;
		$ad	    	= new AdModel;
		$exchangecard = new ExchangecardModel;
		$statisticsModel = new StatisticsModel;
		$push 		= new PushModel;
		$dream_product = new DreamProductModel;
		//$dream = new DreamModel;
		$dream_history = new DreamHistoryModel;
		$dream = new DreamModel;
		$app_exe = new AppExeModel;
		$bbc = new BbcModel;
		// Figure out which controller the user is requesting and perform the correct actions.
		$this->router();
	}

    /**
     * Figure out where the user is trying to get to and route them to the appropriate controller/action.
	//剖析網址的規則, 取得 Controller/Action
	*/
	public function router()
	{
		$r = new Router();

		// Configure the routes, where the user should go when they access the
		// specified URL structures. Default controller and action is set in the config.php file.
		$r->map('/', array('controller' => ROUTER_DEFAULT_CONTROLLER, 'action' => ROUTER_DEFAULT_ACTION));

		// Load instructions for basic routing and send the user on their way!
		$r->default_routes();
		$r->execute();

		// Extracting info about where the user is headed, in order to match the
		// URL with the correct controllers/actions.
		$controller = $r->controller;
		$model = $r->controller_name;
		$action = $r->action;
		$id = $r->id;
		$params = $r->params; // Returns an array(...)
		$matched = $r->route_found; // Bool, where True is if a route was found.
		
		//error_log("[router]:". json_encode($r) .' controller:'. $controller .' matched:'. $matched);

		if($matched)
		{
			// If going to a site page, treat it in special manner, otherwise load
			// the appropriate controller/action and pass in the variables and
			// parameters specified by the URL.
			$controller_file = BASE_DIR ."/controller/{$controller}.php";

			if($controller == "site")
			{
				$site = new Site;
				$site->login();
				$site->home();
			}
			elseif(file_exists($controller_file))
			{
				include_once $controller_file;

				$site = new Site;
				$site->login();

				$$controller = new $model;
				if(method_exists($$controller, $action))
				{
					$$controller->controller['controller'] = $controller;
					$$controller->controller['action'] = $action;
					$$controller->id = $id;
					$$controller->params = $params;
					$$controller->$action();
				}
				else Site::load_page('error');
			}
			else Site::load_page('error');
		}
		else Site::home();
	}
}

?>
