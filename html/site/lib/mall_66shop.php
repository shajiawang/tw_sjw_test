<?php
/*
*	兌換66shop商品 (台1購)mall_66shop.php
*/
include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR ."/wechat.class.php");
include_once(LIB_DIR."/helpers.php");

class shop66_ext 
{
	//回傳:
	public $ret = array(
		'status' =>1,
		'retCode'=>1,
		'retMsg'=>"OK",
		'action_list'=>array(
				"type"=>2,
				"page"=>4,
				"msg"=>"NULL"
			)
		);
	
	//串接入口
	public function home()
	{
		global $tpl, $db, $config, $user, $member;
		
		//回傳:
		$ret = $this->ret;
		$check_var = false;
		$insert_exe = false;
		
		$act_type = (empty($_POST['b'])) ? htmlspecialchars($_GET['b']) : htmlspecialchars($_POST['b']);
		$this->activity_type = intval($act_type);
		
		error_log("[shop66_ext/home] : ".json_encode($_GET));
		error_log("[shop66_ext/home] : ".json_encode($_POST));
		
		//導入執行	
		switch ($this->activity_type)
		{
			case 5: //台1購訂單取消
					$ret = $this->bonus_back();
				break;
			
			case 4: //台1購訂單完成
					$ret = $this->bonus_pay();
				break;
				
			case 3: //回傳會員資料
					$ret = $this->userdata();
				break;
				
			default:
					$ret['status'] = -4;
					$ret["retMsg"]= "串接類型錯誤 !";
				break;
		}
		
		return $ret;
	}
	
	
	/**
     * 訂單完成, 扣除鯊魚點
     */
    protected function bonus_pay()
	{
		global $config, $user, $member;
		
		$ret = $this->ret;
		
		// 讀取json內容
		$getJsonData = empty($_POST['json']) ? stripslashes($_GET['json']) : $_POST['json'];
		$json_data = json_decode($getJsonData, true);
		
		//資安驗證
		$ret_tk = $this->valid_tk( urldecode($json_data['token']) );
		if($ret_tk['status'] !==1){
			$ret = $ret_tk;
		} else {
			$userdata = $ret_tk['userdata'];
			
			//Saja會員編號
			$data["userid"] = $userdata["userid"];
			//手機號碼
			$data["phone"] = $userdata["phone"];
			//幣別(TWD:台幣, RMB:人民幣)
			$data["currency"] = 'TWD';
			//第三方來源
			$data["esid"] = '59';
			//第三方訂單編號
			$data["orderid"] = $json_data["orderId"];
			//SAJA訂單號作為第三方支付編碼
			$data["pay_code"] = '';
			//第三方商品編號
			$data["productid"] = '';
			//扣點金額
			$data["total_amount"] = $json_data["sharkcoin"];
			
			// 幣別點數換算
			if($data['currency'] == "TWD") {
				$data['total_bonus'] = $data['total_amount'];
			} else {
				$data['total_bonus'] = ($data['total_amount'] / 4);		
			}
			
			//紅利積點數量
			$get_bonus = $member->get_bonus($data['userid']);
			
			if($get_bonus['amount'] < $data['total_bonus']) {
				$ret['status'] = 1001;
				$ret["retMsg"] = "剩餘點數不足 !";
			} else {			
				//產生訂單記錄並扣除鯊魚點
				
				if(!empty($data["orderid"]) && $json_data["type"]==1){
					$ret = $this->mk_order($data);
				} else {
					$ret['status'] = 10011;
					$ret["retMsg"] = "訂單編號錯誤 !";
				}
			}
			
			unset($ret['action_list']);
		}
		
		return $ret;
	}
	
	/*
	 *	產生訂單記錄並扣點
	 *	$data array 接收資料陣列	 
	 */ 
	protected function mk_order($data)
	{
		global $db, $config;
		
		$ret = $this->ret;

		// 確認會員資料
		$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
			WHERE prefixid = '{$config['default_prefix_id']}' 
			AND userid = '{$data['userid']}' 
			AND switch = 'Y'";
		$table = $db->getQueryRecord($query); 		
		$updata = $table['table']['record'][0];
		
		//新增訂單
		$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$data['userid']}',
			`status`='1',
			`pgpid`='0',
			`epid`='0',
			`esid`='{$data['esid']}',
			`type`='exchange',
			`num`='1',
			`point_price`='{$data['total_bonus']}',
			`process_fee`='0',
			`total_fee`='{$data['total_bonus']}',
			`memo`='第三方扣點:{$data['esid']}_txorderid:{$data['orderid']}',
			`tx_data`='". json_encode($data) ."',
			`confirm`='Y',
			`insertt`=NOW()";
		$res = $db->query($query);
		$order_id = $db->_con->insert_id;
			
		//新增訂單明細
		//`gender`=(SELECT gender FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` WHERE userid='{$data['userid']}' ),
		$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` SET 
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$data['userid']}',
			`orderid`='{$order_id}',
			`name`='{$updata['addressee']}',
			`phone`='{$data['phone']}',
			`zip`=0,
			`address`='{$updata['address']}',
			`insertt`=NOW()";
		$db->query($query);
		
		//扣除鯊魚點
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$data['userid']}',
			`orderid` = '{$order_id}',
			`countryid`='{$config['country']}', 
			`behav`='other_exchange', 
			`amount`='-{$data['total_bonus']}', 
			`switch`='Y', 
			`insertt`=NOW()";
		$db->query($query); 
		$bonus_id = $db->_con->insert_id;
		
		//鯊魚點和訂單關聯紀錄
		$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$data['userid']}', 
			`orderid`='{$order_id}',
			`bonusid`='{$bonus_id}',
			`amount`='-{$data['total_bonus']}',
			`seq`=0, 
			`switch`='Y', 
			`insertt`=NOW()
		";
		$db->query($query); 
		$evrid = $db->_con->insert_id;
		
		unset($ret['userdata']);
		if (!empty($order_id) && !empty($bonus_id) ) {
			//發送簡訊通知
			//$msg = iconv("UTF-8","big5","已扣除鯊魚點:".$data['total_bonus']." [台灣殺價王].");
			//發送簡訊 (09XXXXXXXX 台灣, 其他->中國) //sendSMS($data['phone'], $msg);	
			
			//$ret['bonusid'] = $bonus_id;
			$ret['status'] = 1;
			$ret['retMsg'] = '扣點成功';
			$ret['sajaorder'] = $order_id;
		
		} else {
			$ret['status'] = 1002;
			$ret['retMsg'] = '扣點失敗 !!';
		}
		
		return $ret;
	}
	
	/**
     * 訂單取消, 返還鯊魚點
     */
    protected function bonus_back()
	{
		global $config, $user, $member;
		
		$ret = $this->ret;
		
		// 讀取json內容
		$getJsonData = empty($_POST['json']) ? stripslashes($_GET['json']) : $_POST['json'];
		$json_data = json_decode($getJsonData, true);
		
		//資安驗證
		$ret_tk = $this->valid_tk( urldecode($json_data['token']) );
		if($ret_tk['status'] !==1){
			$ret = $ret_tk;
		} else {
			$userdata = $ret_tk['userdata'];
			
			//Saja會員編號
			$data["userid"] = $userdata["userid"];
			//手機號碼
			$data["phone"] = $userdata["phone"];
			//幣別(TWD:台幣, RMB:人民幣)
			$data["currency"] = 'TWD';
			//第三方來源
			$data["esid"] = '59';
			//第三方訂單編號
			$data["orderid"] = $json_data["orderId"];
			//SAJA訂單號作為第三方支付編碼
			$data["pay_code"] = $json_data["sajaorder"];
			//第三方商品編號
			$data["productid"] = '';
			//扣點金額
			$data["total_amount"] = $json_data["sharkcoin"];
			
			// 幣別點數換算
			if($data['currency'] == "TWD") {
				$data['total_bonus'] = $data['total_amount'];
			} else {
				$data['total_bonus'] = ($data['total_amount'] / 4);		
			}
			
			//取消訂單 並返還鯊魚點
			if(!empty($data["orderid"]) && !empty($data["pay_code"]) && $json_data["type"]==2){
				$ret = $this->back_order($data);
			} else {
				$ret['status'] = 10012;
				$ret["retMsg"] = "訂單編號錯誤 !";
			}
			
			unset($ret['action_list']);
		}
		
		return $ret;
	}
	
	/*
	 *	取消訂單記錄, 並返回鯊魚點
	 *	$data array 接收資料陣列
	 */ 
	protected function back_order($data) 
	{
		global $db, $config;
		
		$ret = $this->ret;
			
		//鯊魚點和訂單關聯紀錄
		$query = "SELECT count(ebh.userid) as cnt, ebh.amount, o.`orderid`, o.`memo`
					FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` ebh 
					INNER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o 
					ON o.orderid = ebh.orderid AND o.prefixid = ebh.prefixid 
				WHERE ebh.prefixid='{$config['default_prefix_id']}' 
					AND ebh.`switch` = 'Y'
					AND ebh.`userid`='{$data['userid']}'
					AND o.`orderid`='{$data['pay_code']}'
					AND o.`status` NOT IN (2,5)
					AND o.`memo` like '%txorderid:{$data["orderid"]}%'";
		$table = $db->getQueryRecord($query);
			
		if($table['table']['record'][0]['cnt']==1) 
		{
			$saja_orderid = $table['table']['record'][0]['orderid'];
			$edit_memo = $table['table']['record'][0]['memo'] .'_訂單已取消';
			$edit_amount = abs($table['table']['record'][0]['amount']);
			
			//變更訂單
			$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
				SET `status`='5',
					`memo`='{$edit_memo}'
				WHERE `userid`='{$data['userid']}' AND `orderid`='{$saja_orderid}'";
			$res = $db->query($query);
			
			//返回鯊魚點
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$data['userid']}',
				`countryid`='{$config['country']}', 
				`behav`='other_exchange', 
				`amount`='{$edit_amount}', 
				`switch`='Y', 
				`insertt`=NOW()";
			$db->query($query); 
			$bonusid = $db->_con->insert_id;
			
			//鯊魚點和訂單關聯紀錄
			$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` SET
				`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$data['userid']}', 
				`orderid`='{$saja_orderid}',
				`bonusid`='{$bonusid}',
				`amount`='{$edit_amount}',
				`seq`=0, 
				`switch`='Y', 
				`insertt`=NOW()";
			$db->query($query); 
		
			unset($ret['userdata']);
			if (!empty($saja_orderid) && !empty($bonusid) ) {
				// 發送簡訊通知
				//$msg = iconv("UTF-8","big5","已返回鯊魚點:".$edit_amount." [台灣殺價王].");
				// 發送簡訊 (09XXXXXXXX 台灣, 其他->中國) //sendSMS($data['phone'], $msg);	
				
				$ret['retCode'] = 1;
				$ret['retMsg'] = '返點成功';
				
			} else {
				$ret['retCode'] = 1021;
				$ret['retMsg'] = '返點失敗 !!';
			}
		
		} else {
			unset($ret['userdata']);
			$ret['retCode'] = 1022;
			$ret['retMsg'] = '查無訂單關聯紀錄 !!';
		}
		
		return $ret;
	}
	
	
	/**
     * 回傳會員資料
     */
    protected function userdata() 
	{
		global $config, $user, $member;
		
		$ret = $this->ret;
		$ret_tk = $this->valid_tk();
					
		if($ret_tk['status'] !==1){
			//資安驗證
			$ret = $ret_tk;
		} else {
			//$check_var = true;
			$ret['token'] = $this->mk_token($ret_tk['userdata']["phone"], $this->auth_id);
			$ret['name'] = $ret_tk['userdata']["nickname"];
			$ret['cellphone'] = $ret_tk['userdata']["phone"];
			$ret['address'] = $ret_tk['userdata']["address"];
			
			//紅利積點數量
			$get_bonus = $member->get_bonus($this->auth_id);
			$ret['sharkcoin'] = sprintf("%1\$d", $get_bonus['amount']);
			
			unset($ret['action_list']);
		}
		
		return $ret;
	}
	
	//台1購 - 導購網址
	public function urlto()
	{
		global $tpl, $db, $config, $user;
		
		//會員驗證
		$this->auth_id = empty($_SESSION['auth_id']) ? htmlspecialchars($_REQUEST['auth_id']) : $_SESSION['auth_id'];
		$_auth = $this->valid_auth();
		
		$ret = $this->ret;
		
		if($_auth['status'] !==1){
			$ret = $_auth;
		} else {
			$user_profile = $user->get_user_profile($this->auth_id);
			$user_tel = $user_profile["phone"];
			
			if(empty($user_tel) ){
				//回傳:
				$ret['status'] = -1;
				$ret['action_list']["page"]=9;
				$ret['action_list']["msg"]="會員手機資料錯誤 !";
			} else {
				//台1購 - 導購網址
				$_url = 'https://66shop.tw/saja/';
				$_url .= urlencode( $this->mk_token($user_tel, $this->auth_id) );
				
				unset($ret['action_list']["page"]);
				$ret['action_list']["type"] = 2;
				$ret['action_list']["url"] = $_url;
			}
		}
		
		return $ret;
	}
	
	
	/**
     * var string $method 加解密方法
     */
    protected $method = 'AES-128-CBC';
 
    /**
     * var string $secret_key 加解密的金鑰
     */
    protected $secret_key = 'sharkcoinuse2782';
	protected $secret_iv = 'abcdddddddffgghh'; //16長度
 
    /**
     * 加密方法，對資料進行加密，返回加密後的資料
     */
    protected function fixKey($key) 
	{
        if(strlen($key) < 16){
            //0 pad to len 16
            return str_pad($key, 16, 0);
        }
		else if(strlen($key) > 16 ){
			//truncate to 16 bytes
			return substr($key, 0, 16 );
		}
		
		return $key;
    }
	protected function os_encrypt($data)
    {
        $encodedEncryptedData = base64_encode(openssl_encrypt($data, $this->method, $this->fixKey($this->secret_key), 1, $this->secret_iv) );
        $encodedIV = base64_encode($this->secret_iv);
        return $encryptedPayload = $encodedEncryptedData .":". $encodedIV;
    }
 
    /**
     * 解密方法，對資料進行解密，返回解密後的資料
     */
    protected function os_decrypt($data)
    {
		$parts = explode(':', $data); //Separate Encrypted data from iv.
        $encrypted = $parts[0];
        $iv = $parts[1];
        $decryptedData = openssl_decrypt(base64_decode($encrypted), $this->method, $this->fixKey($this->secret_key), 1, base64_decode($iv) );
        return $decryptedData;
    }
	
	//生成 Token
	protected function mk_token($user_tel, $auth_id) 
	{
		$uid_encrypt = MD5($user_tel .':'. $auth_id);
				
		$_token = $this->os_encrypt($uid_encrypt ."||". $auth_id .";". date("YmdHi") );
				
		return $_token;
	}
	
	//驗證 Token
	protected function valid_tk($token='') {
		global $tpl, $db, $config, $user;
		
		$this->ret['status'] = 1;
		
		$decrypted = empty($token) ? $this->os_decrypt($_REQUEST['a']) : $this->os_decrypt($token);
		
		$decrypted = explode(";", $decrypted);
		$de_crypt = explode("||", $decrypted[0]);
		
		$this->auth_id = $de_crypt[1];
		
		$r = $this->valid_auth();
		if($r["status"] !==1){
			//回傳:
			unset($this->ret['action_list']);
			$this->ret['status'] = -1;
			$this->ret["retCode"]= -1;
			$this->ret["retMsg"]= "會員資料錯誤 !";
			
		} else {
			//會員資料
			$user_profile = $user->get_user_profile($this->auth_id);
			$user_tel = $user_profile["phone"];
			
			$md5id = MD5($user_tel .':'. $this->auth_id);
			
			if($_REQUEST['saja_id']=="dev"){
			} else {
				if($md5id !== $de_crypt[0]){
				
					//回傳:
					unset($this->ret['action_list']);
					$this->ret['status'] = -2;
					$this->ret["retCode"]= -2;
					$this->ret["retMsg"]= "驗證碼錯誤 !";
				}
			}
			
			$this->ret['userdata'] = $user_profile;
		}
		
		return $this->ret;
	}
	
	//驗證 會員資料
	protected function valid_auth() {
		global $tpl, $db, $config, $user;
		
		$this->ret['status'] = 1;
		
		if (empty($this->auth_id) || (! is_numeric($this->auth_id)) ){
			
			//回傳:
			$this->ret['status'] = -1;
			$this->ret['action_list']["page"]=9;
			$this->ret['action_list']["msg"]="會員資料錯誤, 請重新登入 !";
		} else {
			// 檢查新人是否通過手機驗證  沒過都不能給
			$recArr1 = $user->validSMSAuth($this->auth_id);
			
			if(empty($recArr1) || $recArr1["verified"] !=='Y') {
				$this->ret['status'] = -3;
				$this->ret['action_list']["page"]=9;
				$this->ret['action_list']["msg"]="用戶未通過手機驗證 !";
			}
		}
		
		return $this->ret;
	}
	
}
?>
