<?php
/*
*	信用卡支付後續處理(充值) creditcard_deposit.php
*/
include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR ."/wechat.class.php");
include_once(LIB_DIR."/helpers.php");

class creditcard_ext 
{
	public $_v = array();
	
	//串接入口
	public function home($userid, $var=array())
	{
		global $mall, $member, $config, $user, $deposit;
		
		//參數:
		$pay_info = $this->_v = $var;
		$this->userid = $userid;
		$check_var = true;
		$insert_exe = false;
		
		//Deposit 充值作業
		$get_deposit_history=$deposit->get_deposit_history($pay_info['Td']);

		if(!empty($get_deposit_history[0]['dhid'])){

			$arr_cond=array();
			$arr_cond['dhid']=$pay_info['Td'];
			
			$arr_data=array();
			$arr_data['out_trade_no']=$pay_info['Td'];
			$arr_data['userid']=$userid;
			$arr_data['amount']= intval($pay_info['MN']);
			$arr_data['timepaid']=date('YmdHis');
			$arr_data['phone']=$pay_info['sdt'];
			$arr_data['paymenttype']=$config['creditcard']['paymenttype'];
			$arr_data['ChkValue']=$pay_info['ChkValue'];

			$arr_update['data']=json_encode($arr_data);
			$arr_update['modifierid']=$userid;
			$arr_update['modifiername']=$_SESSION['user']['profile']['nickname'];
			$arr_update['modifiertype']='User';
					
			$carrierdata['carrier']=$pay_info['carrier'];
			$carrierdata['phonecode']=$pay_info['phonecode'];
			$carrierdata['npcode']=$pay_info['npcode'];
			$carrierdata['donatecode']=$pay_info['donatecode'];
			$arr_update['carrierdata']=json_encode($carrierdata);
					
			$deposit->update_deposit_history($arr_cond, $arr_update);
			
			$ret['status'] = 0;
		} else {
			$ret['retCode']=-6;
			$ret['retMsg']='支付程式異常 !!';
			$ret['status'] = 6;
		}
		
		return $ret;
	}
	
	//支付結果
	public function pay_result($result_array=array())
	{
		global $db, $config, $mall, $user, $deposit, $history;
		
		//參數:
		$this->_v = $result_array;
		$check_var = true;
		$insert_exe = false;
		$ret['userinfo']=array();
		
		//取得 deposit_history儲值記錄
		$get_deposit_history=$deposit->get_deposit_history($result_array['Td']);
		if($get_deposit_history){
				$userid=$get_deposit_history[0]['userid'];
				$orderid=$get_deposit_history[0]['dhid'];
				$depositid=$get_deposit_history[0]['depositid'];
				$dh_status=$get_deposit_history[0]['status'];
				$ret['userinfo']['userid'] = $userid;
				error_log("[creditcard_ext/twcreditcard_pay_success] userid:".$userid.", orderid:".$orderid." depositid:".$depositid.",status:".$dh_status);
                
				// Add By Thomas 2020/01/22 判斷儲值狀態, 若saja_deposit_history.status="deposit" 則不再跑後續流程  以避免重複加殺幣
				if($dh_status=="deposit") {
					error_log("[creditcard_ext/twcreditcard_pay_success] DH Status has been 'deposit' , do nothing !!");
					//echo "0000";
					$ret['msg']="00001";
                    return $ret;					
				}
				// Add End
				
				if(!empty($depositid)){
					$get_deposit=$deposit->get_deposit_id($depositid);
                  
					if($get_deposit){
						$switch=$get_deposit[0]['switch'];
						$amount= round(floatval($get_deposit[0]['amount']));
						error_log("[creditcard_ext/twcreditcard_pay_success]amount:".round($get_deposit[0]['amount'])."==>".$amount);
						$TransID=$result_array['buysafeno']; //紅陽交易編號
						if($TransID==''){
							$TransID=$result_array['BuySafeNo'];//（相容信用卡交易參數）
						}
						$my_chkvalue=$result_array['web'].
									$config['creditcard']['code'].
									$TransID.
									$amount.
									$result_array['errcode'];
						error_log("[creditcard_ext/twcreditcard_pay_success] My Ori ChkValue=".$my_chkvalue);
						$my_chkvalue=strtoupper(sha1($my_chkvalue));
						error_log("[creditcard_ext/twcreditcard_pay_success] My ChkValue=".$my_chkvalue);

						if($my_chkvalue==$result_array['ChkValue']){
							// 驗證ok, 修改訂單及加入贈送
							$switch=$get_deposit[0]['switch'];
							$status=$get_deposit_history[0]['status'];
							$spointid=$get_deposit_history[0]['spointid']; //殺幣id
							$rebateid=$get_deposit_history[0]['rebateid']; //購物金id
							$driid=$get_deposit_history[0]['driid'];
							$nickname=$get_deposit_history[0]['modifiername'];
							$carrier=json_decode($get_deposit_history[0]['carrierdata'], true);
							$ret['userinfo']['nickname'] = $nickname;
							error_log("[creditcard_ext/twcreditcard_pay_success] orderid: ".$orderid."/status: ".$status."/switch: ".$switch);

							//帳號與金鑰設定（用於信用卡代碼交易）
							$key = $config['creditcard']['key']; 					//金鑰
							$encIV = $config['creditcard']['encIV']; 				//IV
							//判斷信用卡是否有資料
							if ($tokenData != '') {

								try {
									$tokenData = $this->dataDecrypt($tokenData, $key, $encIV);
								} catch (Exception $ex) {
									exit('解密失敗');
								}
								error_log("[creditcard_ext/twcreditcard_pay_success] tokenData: ".$tokenData);

								//將tokenData物件化以便存取
								$tokenDataObj= $this->json_decode_fix($tokenData);
								$verification = json_decode($tokenData, true);
								//物件化後，$tokenDataObj->paymentToken為信用卡代碼，更多參數說明請參考技術手冊
								//其中信用卡代碼、代碼認證資料、信用卡代碼有效期限、信用卡卡號末四碼均為必須妥善儲存保管的資料（下次交易均會用到）
								//以下為debug用，正式環境應刪除
								// error_log("[twcreditcard_pay_success] verificationCode :".$tokenDataObj->verificationCode." <===> ".$verification['verificationCode']);
								// $tokenData_decoded = "<br>timestamp: " . $tokenDataObj->timestamp . "<br>userID: " . $tokenDataObj->userID . "<br>paymentToken: " . $tokenDataObj->paymentToken . "<br>verificationCode: " . $tokenDataObj->verificationCode . "<br>tokenExpiryDate: " . $tokenDataObj->tokenExpiryDate . "<br>last4Cardno: " . $tokenDataObj->last4Cardno;

								//寫入信用卡資料
								// $card = $user->updUserProfileCreditCardToken($userid, json_encode($tokenDataObj));
								if (!empty($verification['paymentToken']) && !empty($verification['last4Cardno'])){
									$card = $user->addUserCreditCardToken($userid, $verification);
								}
							}

							if($orderid && $switch == 'N'){
								// 將json形式的資訊轉為Associated array
								$arr_data=json_decode($get_deposit_history[0]['data'],true);
								// 添加資訊到data欄位
								$arr_data['out_trade_no']=$result_array['Td'];
								$arr_data['vendor_no']=$result_array['buysafeno'];
								$arr_data['merchant_id']=$result_array['web'];
								$arr_data['amount']=$result_array['MN'];
								$arr_data['ApproveCode']=$result_array['ApproveCode'];
								$arr_data['Card_NO']=$result_array['Card_NO'];
								$arr_data['Card_Type']=$result_array['Card_Type'];
								$arr_data['SendType']=$result_array['SendType'];
								$arr_data['errcode']=$result_array['errcode'];

								$arr_data['modifierid']=$userid;
								$arr_data['modifiername']=$nickname;
								$arr_data['modifiertype']="User";
								if($result_array['SendType']=="async"){
									$arr_data['modifierid']="0";
									$arr_data['modifiername']="twcreditcard_pay_success";
									$arr_data['modifiertype']="System";
								}

								//刷卡支付參數
								$payArr = $arr_data;
								
								//更新 deposit_history儲值記錄
								$deposit->set_deposit_history($payArr, $get_deposit_history[0], $config['creditcard']['paymenttype']);

								if($result_array['errcode']=='00'){  // 不管異步同步, 回傳errorcode='00' 才表示成功
									
									if(!empty($depositid) ){
										//deposit 儲值生效
										$deposit->set_deposit($depositid);
										
										//dscode 圓夢券生效 $deposit->set_dscode($orderid);
										
										//spoint 殺幣生效
										if(!empty($spointid) ){
											$deposit->set_spoint($spointid);
										}
										
										//20201212 oscode 殺房券生效 $deposit->set_ oscode($userid, '9907', $orderid, 'user_deposit');
										
										//TA充值送限定殺價券
										$spmemo_app = $deposit->oscode_promote($userid, $driid);
										error_log("[pay_result/oscode_promote]: ".json_encode($spmemo_app));
										
										if(!empty($spmemo_app) && !empty($orderid) ){
											//oscode 殺價券生效 
											$deposit->set_oscode($userid, '', $orderid, 'user_deposit');
										}
										
										//回饋上二代殺價幣
										$rtn = couponreward($userid, $depositid, $amount);
										
										//備用 rebate 購物金生效 
										//if(!empty($rebateid) ){ $history->set_rebate($rebateid); }
									}
									
									$get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
									$deposit_conut = $deposit->get_Deposit_Count($userid);
									error_log("[creditcard_ext/twcreditcard_pay_success] driid : ".$driid);
									
									if(!empty($get_deposit_rule_item[0]['driid'])){
										$get_scode_promote = $deposit->get_scode_promote_rt($driid);
										//重機首充儲值送活動，活動內容寫死 20190910
										if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00'  && $deposit_conut['num'] == 1){
											switch($driid){
												//300
												case 55:
												case 107:
													$cnt = 1;
												break;
												//2000
												case 56:
												case 173:
													$cnt = 7;
												break;
												//10000
												case 59:
												case 110:
													$cnt = 34;
												break;
												//30000
												case 60:
												case 111:
													$cnt = 101;
												break;
												//50000
												case 176:
												case 182:
													$cnt = 167;
												break;
												//100000
												case 179:
												case 185:
													$cnt = 334;
												break;
											}
											for($i = 0; $i < $cnt; $i++){
												$sv['productid'] = 10044;
												$sv['spid'] = 0;
												$deposit->add_oscode($userid, $sv);
											}
										}else{
											if(!empty($get_scode_promote)){
												foreach($get_scode_promote  as $sk => $sv){
													if(!empty($sv['productid']) && $sv['productid'] > 0){
														for($i = 0; $i < $sv['num']; $i++){
															$deposit->add_oscode($userid, $sv);
														}
													}
												}
											}
											$ret['msg']="0000";
											//判斷是否第一次充值
											if($deposit_conut['num'] == 1){

												$get_scode_promote = $deposit->get_scode_promote_rt($driid);
												if(!empty($get_scode_promote)){
													$deposit_times = $deposit->getDepositTimes();
													error_log("[creditcard_ext/twcreditcard_pay_success] times: ".json_encode($deposit_times));

													if ($deposit_times['times'] > 1) {
														//首儲送的倍數跑迴圈
														for($t = 0; $t < (int)$deposit_times['times']-1; $t++){
															if ($t < (int)$deposit_times['times']) {
																foreach($get_scode_promote  as $sk => $sv){
																	if(!empty($sv['productid']) && $sv['productid'] > 0){
																		for($i = 0; $i < $sv['num']; $i++){
																			$deposit->add_oscode($userid, $sv);
																		}
																	}
																}
															}
														}
													}
												}

											}
										}
										// 首次儲值送推薦人2塊
										// $this->add_deposit_to_user_src($userid);
									}

									// 開立電子發票
									$Invoice = $this->deposit_Invoice($depositid, $userid, $carrier);
									$ret['msg']="0000";
									
								}

							}else{
								error_log("[creditcard_ext/twcreditcard_pay_success] Duplicated Update, do nothing !!");
								$ret['msg']="0000";
							}

						}else{
							$ret['msg']="Data Consistency Check Error !!";
						}
					}

				}else{
					$ret['msg']="Empty deposit id of ".$orderid." !!";
				}

		} else {
			$ret['msg']="Cannot find the data of ".$orderid." !!";
		}
		
		return $ret;
	}
	
	/*
	 *	創建發票單（用途 : 兜成發票編號並回寫充值資料訂單）
	 *	@param int $depositid    未開發票的單號（儲值帳目id）
	 *  @param int $userid    會員編號
	 *  僅開放相同controller內部呼叫
	 *  遠端須透過api createInvoiceViaSign,通過驗簽後 才能內部呼叫此function
	 */
	public function deposit_Invoice($depositid='', $userid='', $carrier) {

		global $config, $deposit, $invoice;
		/*
		if(empty($userid))
		  $userid = empty($_SESSION['auth_id'])?htmlspecialchars($_REQUEST['userid']):$_SESSION['auth_id'];
		if(empty($depositid))
		  $depositid = htmlspecialchars($_REQUEST['depositid']);
		*/
		$ret = array();
		error_log("[creditcard_ext/deposit_Invoice] userid:${userid}, depositid:${depositid} ");

		if(empty($userid)) {
			$userid = $_SESSION['auth_id'];
		}
		if(empty($userid)) {
			$ret['retCode']=-7;
			$ret['retMsg']="NO_USERID";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		}
		if(empty($depositid)) {
			$ret['retCode']=-8;
			$ret['retMsg']="NO_DEPOSITID";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		}

		//判斷是否有發票編號
		$depositData= $deposit->getDepositHistoryForInvoice($depositid, $userid);

		if(!$depositData){
			//資料不存在
			$ret = array(
						"retCode" 	=> -1,
						"retMsg" 	=> "資料不存在"
			);
		}else{

			if($depositData['switch'] != 'Y'){
				// 充值是否完成
				$ret = array(
							  "retCode" 	=> -2,
							  "retMsg" 	=> "此筆充值尚未完成"
				);
			}else if ($depositData['invoiceno'] != ''){ //發票號碼是否為空
				//invoiceno不為空值
				$ret = array(
							  "retCode" 	=> -3,
							  "retMsg" 	=> "發票號碼已存在"
				);
			}else{
				//當儲存資料有值時，取modifyt欄位
				$year = date('Y', strtotime($depositData['insertt']));
				$date = date('m', strtotime($depositData['insertt']));

				//取得發票字軌資料
				$invoice_rail = $deposit->getInvoiceRail($year,$date);

				if(!$invoice_rail){
				  //發票字軌資料不存在
				  $ret = array(
								"retCode" 	=> -4,
								"retMsg" 	=> "發票字軌資料不存在"
				  );
			}else{

					// 發票號碼
					// 20181219 : 配號數前面有0的, 轉成integer時會被忽略掉  所以算完之後要再補零到8位
					$invoiceno = $invoice_rail['rail_prefix'] . str_pad(($invoice_rail['seq_start'] + $invoice_rail['used']),8,'0',STR_PAD_LEFT);

					/* 發票資料
					$arrInvoice = array();
					$arrInvoice['invoiceno']=$invoiceno;
					$arrInvoice['invoice_datetime']=date('Y-m-d H:i:s');
					$arrInvoice['buyer_name']=$userid;
					$arrInvoice['userid']=$userid;
					$arrInvoice['random_number']=$random_code ;
					$arrInvoice['total_amt'] =round($depositData['amount']);
					$arrInvoice['sales_amt']=round($depositData['amount']/1.05);
					$arrInvoice['tax_amt'] = round($arrInvoice['total_amt']-$arrInvoice['sales_amt']);
					$arrInvoice['railid']=$invoice_rail['railid'];
					$arrInvoice['donate_mark']="0";
					$arrInvoice['nopban']="";
					$arrInvoice['upload']="N";
					$arrInvoice['carrier_type']="";
					$arrInvoice['carrier_id1']="";
					$arrInvoice['carrier_id2']="";
					$arrInvoice['utype']="S";
					$arrInvoice['is_winprize']="N";
					*/

					$arrInvoice = $invoice->genDefaultInvoiceArr($userid, round($depositData['amount']), '','S');
					$arrInvoice['invoiceno']=$invoiceno;
					$arrInvoice['userid']=$userid;
					$arrInvoice['railid']=$invoice_rail['railid'];
					$arrInvoice['upload']="N";
					
					//組成載具OR捐贈資料
					switch ($carrier[0]['carrier'])
					{
						case 'phone': //手機條碼
							$arrInvoice['phonecode']=$carrier[0]['phonecode'];
							break;
						case 'np': //自然人憑證條碼
							$arrInvoice['npcode']=$carrier[0]['npcode'];
							break;
						// case 'donate': //捐贈
							// $arrInvoice['donate_mark']=1;
							// $arrInvoice['npoban']=$carrier[0]['donatecode'];
							// break;
						default: //殺價王會員載具
							$arrInvoice['carrier_type']='EG0544';
							$arrInvoice['carrier_id1']='SJ'.$userid;
							$arrInvoice['carrier_id2']='SJ'.date('Y-m-d').$userid;
							break;
					}
					
					if (!empty($carrier[0]['donatecode']) && $carrier[0]['buyer_id'] == ""){
						$arrInvoice['donate_mark']=1;
						$arrInvoice['npoban']=$carrier[0]['donatecode'];
					}
					if (!empty($carrier[0]['buyer_id'])){
						$arrInvoice['buyer_id'] = $carrier[0]['buyer_id'];
					}
					$siid=$invoice->addInvoiceData($arrInvoice);

					if($siid>0){
						$arrInvoiceItem = array();
						$arrInvoiceItem['siid']=$siid;
						$arrInvoiceItem['seq_no']=1;
						$arrInvoiceItem['unitprice']= round($depositData['amount']);
						$arrInvoiceItem['quantity']=1;
						$arrInvoiceItem['amount']=$arrInvoiceItem['unitprice']*$arrInvoiceItem['quantity'];
						$arrInvoiceItem['description']="系統使用費";
						$sipid = $invoice->addInvoiceProdItem($arrInvoiceItem);
					}


					error_log("[creditcard_ext/deposit_Invoice] invoiceno:".$invoiceno."--siid:".$siid."--sipid:".$sipid);
					//將 發票號碼 和 隨機號碼 回寫到表 saja_deposit

					$update_invoice = $deposit->updateDepositHistoryInvoice($userid,$depositid,$invoiceno);

					if($update_invoice != 1){
						//寫入表saja_deposit失敗
						$ret = array(
									  "retCode" 	=> -5,
									  "retMsg" 	=> "寫入失敗"
						);
					}else{
						//將used+1回寫到saja_invoice_rail中
						$update_invoice_rail = $deposit->updateInvoiceRail($invoice_rail['railid'],$invoice_rail['rail_prefix'],'');

						if($update_invoice_rail != 1){
						  //used累加 1失敗
						  $ret = array(
										"retCode" 	=> -6,
										"retMsg" 	=> "累加1失敗"
						  );
						}else{
							//取表invoice_rail中的資料
							$invoice_rail = $deposit->getInvoiceRail($year,$date);

							//如果加1之後檢查used是否等於total, 是則表示號碼用完,將switch改成"E"之後不再使用
							if($invoice_rail['used'] == $invoice_rail['total']){
								$update_invoice_rail_switch = $deposit->updateInvoiceRail($invoice_rail['railid'],$invoice_rail['rail_prefix'],1);
								$ret = array(
									"retCode" 	=> 1,
									"retMsg" 	=> "OK"
								);
							}else{
								$ret = array(
									"retCode" 	=> 1,
									"retMsg" 	=> "OK"
								);
							}
						}
					}
				}
			}
		}

		$ret['retObj']['depositid'] = (!is_null($depositid)) ? $depositid : "";
		$ret['retObj']['invoiceno'] = (!is_null($invoiceno)) ? $invoiceno : "";
		$ret['retObj']['random_code'] = (!is_null($random_code)) ? $random_code : "";
		error_log("[creditcard_ext/deposit_Invoice]".json_encode($ret));
		return $ret;
	}
	
	//JSON解碼
	public function json_decode_fix($json, $assoc = false) {
		$json = str_replace(array("\n","\r"),"",$json);
		$json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
		$json = preg_replace('/(,)\s*}$/','}',$json);
		return json_decode($json,$assoc);
	}
	
	// AES解密（資料加解密用）
	public function dataDecrypt($encryptedTextData, $key, $iv) {
		$outputData='';
		try {
			$encryptedTextData=base64_decode($encryptedTextData);
			$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$key=base64_decode($key);
			$iv=base64_decode($iv);

			$outputData=mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encryptedTextData, MCRYPT_MODE_CBC, $iv);
			if ($outputData=='') {return '';} //解密異常，回傳空值
			//padding處理
			$len=strlen($outputData);
			$pad = ord($outputData[$len - 1]);
			$outputData=substr($outputData, 0, $len-$pad);
		} catch (Exception $ex) {
			//解密異常
			throw new Exception('資料錯誤');
			//return $ex->getMessage(); //回傳錯誤訊息
		}
		return $outputData;
	}
	
}
?>
