<?php
/*
* 台灣 Pay 支付
*/
include_once(LIB_DIR."/config.php");
include_once(LIB_DIR."/dbconnect.php");
include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR."/helpers.php");

class taiwan_pay 
{
	//測試：
	//public $twp_url = "https://www.focas-test.fisc.com.tw/FOCAS_WS/API20/QRP/V2/";
	//public $RetURL = "http://test.shajiawang.com/site/deposit/taiwanpay_result/";
	//public $appURL = "saja://test.shajiawang.com/site/member/";
	
	//營運：
	public $twp_url = "https://www.focas.fisc.com.tw/FOCAS_WS/API20/QRP/V2/";
	public $RetURL = "saja://www.saja.com.tw/site/deposit/taiwanpay_result/";
	
	//驗證參數
	protected $twp_pin = "FAB4E0A7E8311F10217FBB75B276B9FEF92CC43EDD3282BA"; //"12345678901234567890FBB75B276B9FEF92CC43EDD321AB";
	
	//EMVCo QR Code
	//財金標準 QR Code
	protected $qrUrl = "TWQRP://殺價王/158/01/V1?";
	//D3 安全碼
	protected $secureCode = "AbilyBNDibal";
	//D10 交易幣別
	protected $txnCurrency = "901";
	//收單銀行
	protected $acqBank = "004";
	//特店代號 MID
	protected $merchantId = "004250898895001";
	//端末代號 TID
	protected $terminalId = "50010001";
	//交易類型 
	protected $txnType = "01,04";
	
	//加解密的金鑰 (驗證參數)
    protected $secret_key = "FAB4E0A7E8311F10217FBB75B276B9FEF92CC43EDD3282BA"; //"12345678901234567890FBB75B276B9FEF92CC43EDD321AB";
	//加解密方法
    protected $aes_method = "AES-128-CBC";
	//IV值
	protected $secret_iv = "FOCASAPIFOCASAPI";
	
	//回傳:
	public $ret = array(
		'status' =>1,
		'retCode'=>1,
		'retMsg'=>"OK",
		'action_list'=>array(
				"type"=>1,
				"page"=>9,
				"msg"=>"NULL"
			)
		);
	
	//串接入口
	public function home($var)
	{
		global $tpl, $db, $config, $deposit, $user, $member;
		
		//回傳:
		$ret = $this->ret;
		$check_var = false;
		$insert_exe = false;
		
		// 檢查交易金額
		if(floatval($var['amount']) < 0){
			$ret['status'] = 1002;
			$ret["retMsg"] = "交易金額錯誤 !";
		}
		
		//設定16碼訂單編號
		$set_ordern = $this->set_ordern($var['ordernumber']);
		
		/*
		* 產生 購物主掃 QR Code
		*/
		$qr_info = array();
		
		//D1 交易金額
		$qr_info['D1'] = "D1=". $var['amount'] .'00';
		//D2 訂單編號
		$qr_info['D2'] = "D2=". $set_ordern;
		//D3 安全碼
		$qr_info['D3'] = "D3=". $this->secureCode; 
		//D10 交易幣別
		$qr_info['D10'] = "D10=". $this->txnCurrency;
		//D11 收單行資訊
		$qr_info['D11'] = "D11=00,00400425089889500150010001;01,00400425089889500150010001";

		//訂單編號寫入`saja_cash_flow`.`saja_deposit_history`
		$arr_cond['dhid'] = $var['ordernumber'];
		$arr_cond['switch'] = "Y";
		$arr_upd['memo'] = $set_ordern;
		$updateH = $deposit->update_deposit_history($arr_cond, $arr_upd);
		
		$mk_QRCode = $this->qrUrl . implode("&", $qr_info);
		$url_QRCode = urlencode($mk_QRCode);
		
		//產生QRCode
		$txArr['qrcode_bar'] = $url_QRCode;
		$txArr['qrcode_url'] = BASE_URL.APP_DIR."/barcodegen/genbarcode.php?text=".$url_QRCode;
		
		/*
		* Web To APP 特店支付請求
		*/ 
		//QRCode加密
		$encQRCode = $this->QRCode_Encrypt($url_QRCode);

		//回傳網址加密 
		$RetURL = urlencode($this->RetURL);
		$encRetURL = $this->QRCode_Encrypt($this->RetURL, 6);
		
		error_log("[taiwan_pay/home]:url_QRCode ". $url_QRCode);
		error_log("[taiwan_pay/home]:RetURL ". $RetURL);

		$pay_info = array();
		$pay_info['post_action'] = $this->twp_url .'WebToAppReq';
		
		$pay_info['acqBank'] = $this->acqBank;
		$pay_info['encQRCode'] = $encQRCode;
		$pay_info['encRetURL'] = $encRetURL;
		$pay_info['merchantId'] = $this->merchantId;
		// 訂單編號 
		$pay_info['orderNumber'] = $set_ordern;
		$pay_info['terminalId'] = $this->terminalId;
		$pay_info['txnType'] = $this->txnType;
		
		//參數驗證內容: 各欄位依系統參數名稱由小到大排序後的值
		$inputdata = $this->acqBank . $encQRCode . $encRetURL . $this->merchantId . $pay_info['orderNumber'] . $this->terminalId . $this->txnType; 
		$pay_info['verifyCode'] = $this->verify_code($inputdata);
		
		$ret['action_data'] = $pay_info;
		error_log("[taiwan_pay/home]:pay_info ". json_encode($pay_info));
		
		//回傳錯誤訊息
		//echo $this->get_twpmsg('0088');
		
		return $ret;
	}

	//回傳結果
	public function result($var)
	{
		global $db, $config, $user, $member;
		
		//回傳:
		$ret = $this->ret;
		$check_var = false;
		$insert_exe = false;

		/*
		* Web To APP 特店支付返回
		*/
		//參數驗證內容: 各欄位依系統參數名稱由小到大排序後的值
		$inputdata = $var['acqBank'] . $var['cardPlan'] . $var['merchantId'] . $var['orderNumber'] . $var['responseCode'] . $var['terminalId']; 
		$verifyCode = $this->verify_code($inputdata);

		//訂單編號  
		$order_id = intval( substr($var['orderNumber'], 2) );
		
		if($var['verifyCode'] !== $verifyCode){
			$ret['status'] = 1001;
			$ret["retMsg"] = "返回參數驗證錯誤 !";
		} else {
			// 收單銀行 
			if( $var['responseCode']=='0000'
			&& $var['acqBank']==$this->acqBank
			&& $var['merchantId']==$this->merchantId
			&& $var['terminalId']==$this->terminalId
			){
				//充值
				$r = $this->twp_success($order_id, $var);
				
				if($r['status']==0){
					$ret['status'] = 200;
					$ret["retMsg"] = "充值成功 !";
				} else {
					$ret['status'] = $r['status'];
					$ret["retMsg"] = $r["retMsg"];
				}
			
			} else {
				
				//授權失敗
				if(!empty($order_id)){
					$r = $this->twp_failed($order_id, $var);
					$ret['status'] = $r['status'];
					$ret["retMsg"] = $r["retMsg"];
				} else { 
					$ret['status'] = 1003;
					$ret["retMsg"] = "充值失敗 !";
				}
			}
		}
		
		//return $ret; 
		echo '<!DOCTYPE html><html><body><script>
		alert("'. $ret["retMsg"] .'");
		var u = navigator.userAgent;
        var isAndroid = u.indexOf("Android") > -1 || u.indexOf("Adr") > -1;
        var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
        if(isAndroid){top.location.replace("https://play.google.com/store/apps/details?id=tw.com.saja.userapp"); }
		else{top.location.replace("saja://action_list:{\"type\":\"4\",\"page\":\"9\"}"); }
		</script></body></html>';
		
	    exit;
	}
	
	//授權失敗
	protected function twp_failed($order_id, $var)
	{
		global $db, $config, $deposit, $user, $member;;
		set_status($this->controller);
		
		$get_deposit_history = $deposit->get_deposit_history($order_id);
		if(!empty($get_deposit_history))
		{
			$orderid = $get_deposit_history[0]['dhid'];
			$depositid = $get_deposit_history[0]['depositid'];

			if(!empty($depositid))
			{
				$ret['status'] = 1007;
				$ret["retMsg"] = '充值失敗:'. $this->get_twpmsg($var['responseCode']);
				
				$arr_cond=array();
				$arr_cond['dhid'] = $orderid;

				$var["retMsg"] = $ret["retMsg"];
				$arr_upd=array();
				$arr_upd['data'] = json_encode($var);
				
				//儲值紀錄
				$deposit->update_deposit_history($arr_cond, $arr_upd);
			} else {
				$ret['status'] = 1006;
				$ret["retMsg"] = "查無訂單編號{$orderid}充值資料 !";
			}
		} else {
				$ret['status'] = 1005;
				$ret["retMsg"] = "查無訂單編號{$orderid}資料 !";
		}
		
		return $ret;
	}
	//授權成功
	protected function twp_success($order_id, $var)
	{
		global $db, $config, $deposit, $user, $member;;
		set_status($this->controller);
		
		$ret['status'] = 0;
		
		if(!empty($order_id))
		{
			$get_deposit_history = $deposit->get_deposit_history($order_id);
			
			if(!empty($get_deposit_history))
			{
				$userid = $get_deposit_history[0]['userid'];
				$orderid = $get_deposit_history[0]['dhid'];
				$depositid = $get_deposit_history[0]['depositid'];
				$status = $get_deposit_history[0]['status'];
				$spointid = $get_deposit_history[0]['spointid'];
				$driid = $get_deposit_history[0]['driid'];
				$nickname = $get_deposit_history[0]['modifiername'];

				if(!empty($depositid))
				{
					$get_deposit = $deposit->get_deposit_id($depositid);
						
					if($get_deposit)
					{
						$amount = round(floatval($get_deposit[0]['amount']));
						
							//修改訂單及加入贈送
							$switch = $get_deposit[0]['switch'];
							if($orderid && $switch == 'N')
							{
									$arr_data = $var;
									$arr_data['Td'] = $orderid;
									$arr_data['modifierid'] = $userid;
									$arr_data['modifiername'] = $nickname;
									$arr_data['modifiertype'] = "User";
									
									//儲值紀錄
									$deposit->set_deposit_history($arr_data, $get_deposit_history[0], 'taiwanpay');
									//儲值生效
									$deposit->set_deposit($depositid);
									//殺幣生效
									$deposit->set_spoint($spointid);
									//儲值規則
									$get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
									//儲值計數
									$deposit_conut = $deposit->get_Deposit_Count($userid);
									
									if(!empty($get_deposit_rule_item[0]['driid']))
									{
										$get_scode_promote = $deposit->get_scode_promote_rt($driid);
										//重機首充儲值送活動，活動內容寫死 20190910
											
											if(!empty($get_scode_promote)){
												foreach($get_scode_promote  as $sk => $sv){
													if(!empty($sv['productid']) && $sv['productid'] > 0){
														for($i = 0; $i < $sv['num']; $i++){
															$deposit->add_oscode($userid, $sv);
														}
													}
												}
											}
											
											//判斷是否第一次充值
											if($deposit_conut['num'] == 1)
											{
												$get_scode_promote = $deposit->get_scode_promote_rt($driid);
												if(!empty($get_scode_promote)){
													$deposit_times = $deposit->getDepositTimes();

													if ($deposit_times['times'] > 1) {
														//首儲送的倍數跑迴圈
														for($t = 0; $t < (int)$deposit_times['times']-1; $t++){													
															if ($t < (int)$deposit_times['times']) {
																foreach($get_scode_promote  as $sk => $sv){
																	if(!empty($sv['productid']) && $sv['productid'] > 0){
																		for($i = 0; $i < $sv['num']; $i++){
																			$deposit->add_oscode($userid, $sv);
																		}
																	}
																}
															}
														}
													}
												}

											}
										
										// 首次儲值送推薦人2塊
										// $this->add_deposit_to_user_src($userid);
									}
				
									// 開立電子發票
									//$Invoice = $this->createInvoice($depositid, $userid);
									$ret['depositid'] = $depositid;
									
									// 入帳時推播通知用戶
									$inp=array();
									$inp['title']="殺價幣入帳通知";
									$inp['body']="您先前繳費購買的殺價幣已入帳, 請至殺友專區查看~ ";
									$inp['productid']="";
									$inp['action']="4";
									$inp['url_title']="";
									$inp['url']="";
									$inp['sender']=99;		
									$inp['sendto']=$userid;
									$inp['page']=9;
									$inp['target']='saja';
									//$rtn=sendFCM($inp);
								
							} else {
								//error_log("Duplicated Update, do nothing !!");
								//$ret = "0000";
							}
					}
				} else {
					$ret['status'] = 1006;
					$ret["retMsg"] = "查無訂單編號{$orderid}充值資料 !";
				}
			} else {
				$ret['status'] = 1005;
				$ret["retMsg"] = "查無訂單編號{$orderid}資料 !";
			}
		} else {
			$ret['status'] = 1004;
			$ret["retMsg"] = "訂單編號空白 !";
		}
		
		return $ret;
	}
	
	/**
     * 設定訂單編號
     */
	protected function set_ordern($num)
    {
		$num_len = strlen($num);
		
		if($num_len < 14){
			$set_num = str_pad($num, 14, '0', STR_PAD_LEFT);
		} else {
			$set_num = $num;
		}
		
		$data = 'SJ'. $set_num;
		
		return $data;
    }
	
	//QRCode加密計算 (※若資料長度不為16倍數者, 請自行依規格補padding 0x00)
	protected function QRCode_Encrypt($qrcodedata, $len=4)
	{
		$qrcodedatahex = bin2hex($qrcodedata);
		$datalen = dechex( strlen($qrcodedata) );
		$datalenhex = str_pad($datalen, $len, '0', STR_PAD_LEFT); //qrcode加密資料長度2 bytes,所以16進位表示要補到4碼

		$inputdatahex = $datalenhex . $qrcodedatahex;
		$inputdata = pack('H*', $inputdatahex);

		$key = pack('H*', $this->secret_key);
		$iv = $this->secret_iv;
		
		//AES_128_CBC 加密
		$mcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $inputdata, MCRYPT_MODE_CBC, $iv); //加密後的資料
		$encresult = strtoupper( bin2hex($mcrypt) ); 

		$keyver = '02';//加密基碼版本02
		$enchex = $keyver . $encresult; //加密版本||加密後的資料
		$encbase64 = base64_encode( pack('H*', $enchex) ); //轉base64

		return urlencode($encbase64); //進行url encode
	}

	// 計算 verifyCode
	protected function verify_code($inputdata)
	{
		$key = $this->secret_key;
		$sha256 = $this->SHA256Encrypt($inputdata . $key);
		$verifyCode = strtoupper($sha256);
		
		return $verifyCode;
	}
	// SHA256加密
	protected function SHA256Encrypt($plainText)
	{
		return hash('sha256', $plainText, false);
	}
	
	//回傳錯誤訊息
	public function get_twpmsg($id)
	{
		$arr_key = array('0012','0013','0018','0019','0020','0021','0022','0023','0024','0025','0026','0027','0028','0029','0030','0031','0041','0042','0071','0081','0082','0083','0087','0088','0089','0098','0099','0101','0102','0104','0108','0201','0202','0203','0204','0205','0206','0207','0301','0302','0304','0401','0601','0602','0701','0702','0703','0801','1001','1002','1004','2999','4201','4202','4203','4204','4205','4401','4405','4406','4414','4501','4505','4507','4508','4512','4513','4514','4515','4516','4601','4602','4701','4703','4801','4802','4803','4804','4805','4806','4807','4808','4809','5004','6101','8037','8101','8104','8108','8115','9001','9002','9003','9004','9005','9006','9007','9008','9009','9101','9102','9103','9900','9901','9902','9997','9998','9999');
		
		$arr = array(
			'0012'=>"系統忙線中。",
			'0013'=>"交易逾時。",
			'0018'=>"驗證碼錯誤。",
			'0019'=>"特店代號不存在。",
			'0020'=>"端末代號不存在。",
			'0021'=>"系統暫停服務。",
			'0022'=>"使用者未依指定時間內送出交易。",
			'0023'=>"退款(貨)金額超出原購物金額。",
			'0024'=>"發卡行拒絕交易。",
			'0025'=>"驗證參數未進行初始化設定，或尚未執行更換驗證參數交易。",
			'0026'=>"驗證參數效期超出設定，請執行更換驗證參數交易。",
			'0027'=>"原交易授權失敗。",
			'0028'=>"該筆退款(貨)交易超出退款(貨)期限。",
			'0029'=>"(原)交易處理中。",
			'0030'=>"總手續費大於清算金額。",
			'0031'=>"清算金額*清算匯率不等於交易金額。",
			'0041'=>"未設定機關臨櫃繳款資訊。",
			'0042'=>"機關臨櫃繳款資訊比對異常。",
			'0071'=>"收單行系統傳送的回應訊息驗證碼錯誤。",
			'0081'=>"無相對應的原始交易資料。",
			'0082'=>"此(購物/退貨)訂單交易無法重覆授權。",
			'0083'=>"此訂單交易已取消。",
			'0087'=>"不支援此交易類型。",
			'0088'=>"解密失敗。",
			'0089'=>"驗證失敗。",
			'0098'=>"交易狀態未明。",
			'0099'=>"不明原因異常。",
			'0101'=>"訊息格式或內容編輯錯誤。",
			'0102'=>"訊息回應代碼錯誤。",
			'0104'=>"訊息發生一般檢核類錯誤。",
			'0108'=>"清算日期錯誤。",
			'0201'=>"發信單位未參加本項跨行業務或系統暫時無法運作。",
			'0202'=>"發信單位本項跨行業務停止或暫停營業。",
			'0203'=>"發信單位未在跨行作業運作狀態。",
			'0204'=>"收信單位未參加本項跨行業務或系統暫時無法運作。",
			'0205'=>"收信單位本項跨行業務停止或暫停營業。",
			'0206'=>"收信單位未在跨行作業運作狀態。",
			'0207'=>"收信單位今日不營業。",
			'0301'=>"押碼基碼不同步。",
			'0302'=>"訊息押碼錯誤。",
			'0304'=>"亂碼化設備故障。",
			'0401'=>"訊息之 MAPPING 欄位資料與原交易相關訊息之欄位資料不相符。",
			'0601'=>"交易訊息發生TIMEOUT。",
			'0602'=>"轉出單位交易訊息發生TIMEOUT，請做餘額查詢確認交易結果。",
			'0701'=>"財金本項跨行業務暫停或停止營業。",
			'0702'=>"財金主機未在跨行作業運作狀態。",
			'0703'=>"財金跨行業務系統錯誤。",
			'0801'=>"檔案故障。",
			'1001'=>"無效之訊息類別代碼或交易類別代碼。",
			'1002'=>"跨行交易序號重覆或繳費單位交易序號重覆。",
			'1004'=>"訊息內之跨行交易序號或繳費單位交易序號錯誤。",
			'2999'=>"其他類檢核錯誤。",
			'4201'=>"轉帳金額超過限額。",
			'4202'=>"轉帳累計金額超過限額。",
			'4203'=>"交易金額錯誤。",
			'4204'=>"金額超過繳納限額。",
			'4205'=>"感應式交易金額超過累計限額。",
			'4401'=>"帳號失效。",
			'4405'=>"帳號尚未生效。",
			'4406'=>"帳號已掛失。",
			'4414'=>"卡號有誤（IC交易用）。",
			'4501'=>"申請帳戶為問題帳戶。",
			'4505'=>"餘額不足。",
			'4507'=>"轉入帳戶或特店錯誤。",
			'4508'=>"非約定轉入帳號。",
			'4512'=>"存款單位應向聯徵中心查驗帳戶使用者之P33資訊。",
			'4513'=>"依存款單位向聯徵中心查驗帳戶使用者P33之結果，存款單位拒絕該筆交易。",
			'4514'=>"因聯徵中心系統維護中，存款單位暫無法取得查驗結果。",
			'4515'=>"帳戶/卡片為第一次進行電子支付交易，應進行身分確認。",
			'4516'=>"身分確認未成功，拒絕交易。",
			'4601'=>"轉帳累計次數操超過限制。",
			'4602'=>"感應式交易次數超過累計限制次數。",
			'4701'=>"無此交易。",
			'4703'=>"利用檢查碼檢核輸入資料有誤。",
			'4801'=>"繳款（稅）類別有誤；未經註冊之繳費交易。",
			'4802'=>"銷帳編號檢查碼有誤。",
			'4803'=>"繳款（稅）金額有誤。",
			'4804'=>"繳款（稅）期限有誤；逾期／未到。",
			'4805'=>"事業機構代號有誤。",
			'4806'=>"身份證營利事業統一編號/營利事業營利事業統一編號與存款帳號之身份證營利事業統一編號/營利事業營利事業統一編號不相同。",
			'4807'=>"入帳單位有誤。",
			'4808'=>"無此帳戶（依據委託單位、繳費類別及費用代號）。",
			'4809'=>"帳單重複繳納。",
			'5004'=>"中文欄位或變動長度欄位長度錯誤（IC交易用）。",
			'6101'=>"跨行可用餘額小於零，不得交易。",
			'8037'=>"購貨日期錯誤。",
			'8101'=>"交易IC金融卡交易驗證碼檢核錯誤（IC交易用）。",
			'8104'=>"IC金融卡交易序號重覆（IC交易用）。",
			'8108'=>"IC卡片序號錯誤，回發卡單位處理（IC交易用）。",
			'8115'=>"IC卡備註欄位檢核有誤（IC交易用）。",
			'9001'=>"該銀行未加入轉帳交易。",
			'9002'=>"該銀行未加入餘額查詢交易。",
			'9003'=>"稅籍資料檢查錯誤。",
			'9004'=>"關貿連結之申報或自繳資料不存在。",
			'9005'=>"營業稅稅籍編號有誤 ,請關閉瀏覽器後重新輸入。",
			'9006'=>"繳費單位本項跨行業務停止或暫停營業。",
			'9007'=>"繳費單位本項跨行業務未停止或未暫停營業。",
			'9008'=>"訊息傳輸長度小於實際傳輸資料長度。",
			'9009'=>"縣市機關鄉鎮代號與營業稅稅籍編號檢核不符。",
			'9101'=>"繳費（稅）資料錯誤。",
			'9102'=>"無帳單資料。",
			'9103'=>"信用卡授權失敗。",
			'9900'=>"交易進行中。",
			'9901'=>"轉出行與原交易不符。",
			'9902'=>"Session逾時。",
			'9997'=>"系統服務到達上限。請稍後再試。",
			'9998'=>"無法正確判斷電子錢包版本,請確認是否已經安裝新版電子錢包。",
			'9999'=>"系統發生不明錯誤,若已啟動轉帳或繳稅交易,請洽財金公司詢問該筆交易狀況,以避免重覆扣帳。",
		);
		
		//'Q2xx'=>"格式有錯，其中xx代表錯誤的欄位編號(編號若為3位，前2位以16進位表示，例如欄位130以D0表示)。",
		//'Q4xx'=>"xx欄位應帶而未帶， 其中xx代表錯誤的欄位編號(編號若為3位，前2位以16進位表示，例如欄位130以D0表示)。",
		//'Q6xx'=>"欄位值不合規定(即非法值)，其中xx代表錯誤的欄位編號(編號若為3位，前2位以16進位表示，例如欄位130以D0表示)。",
		
		if( in_array($id, $arr_key) ){
			$msg = $id .' '. $arr[$id];
		} else {
			if(substr($id, 0, 1)=='Q'){
				$msg = $id .' '. "欄位值格式有錯。";
			} else {
				$msg = $id;
			}
		}
		
		return $msg;
	}

}
?>
