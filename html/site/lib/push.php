<?php
//推播物件
class push_lib {
    
    //待发送的应用程序(appKey)，只能填一个
    private $app_key = '';
    //主密码
    private $master_secret ='';
    //推送的Api地址
    private $url = '';

    //若实例化的时候传入相应的值则按新的相应值进行
    public function __construct($app_key=null, $master_secret=null,$url=null) {
        if ($app_key) $this->app_key = $app_key;
        if ($master_secret) $this->master_secret = $master_secret;
        if ($url) $this->url = $url;
        date_default_timezone_set('Asia/Shanghai');
        $this->datetime = date('Y-m-d H:i:s');
    }
	
	public function broadcast_fcm($ids='', $msg=array(), $data=array(), $act='Y') {
		global $db, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		//參數未傳送返回false
		if(empty($ids) || empty($msg) ){
			return FALSE;
		}

		// API access key from Google API's Console
		define( 'API_ACCESS_KEY', 'AAAAbCNeUVQ:APA91bGcY4z5iUZyWFxirakTqx6h5XR1-f9K-XMNuiUsUuqx9S2Js4Sk4KSywwlaUD4YjcAqIlLbkQQ289G_vI996dZDOU32Bu9XPGBxI7vWrHtOqTUg_rqP1iN0Nu4RxF2AswUfg93U' );

		$headers = array(
			'Authorization: key=' . API_ACCESS_KEY,
			'Content-Type: application/json'
		);

		if($act=='N'){ }else{
			error_log("[broadcast_fcm]ids: ". $ids);
			
			//接收者token $ids
			$sendtos = explode(",", $ids);
			
			foreach ($sendtos as $key => $sendto) {
				$query = "SELECT `token` FROM saja_user.saja_user_device_token
					WHERE switch = 'Y'
					AND userid = '{$sendto}' ";
				$table = $db->getRecord($query);
				
				if (!empty($table)) {
					$tokenid = $table[0]['token'];
					$log_target='';
					$groupset='';
					$push_title = (empty($msg['title'])) ? "無標題" : $msg['title'];
					$body = (empty($msg['body'])) ? "無內容" : $msg['body'];
					$action = (empty($data['action'])) ? "" : $data['action'];
					$productid = (empty($data['productid'])) ? 0 : $data['productid'];
					$url_title = (empty($data['url_title'])) ? "" : $data['url_title'];
					$url = (empty($data['url'])) ? "" : $data['url'];
					$page = 0;
					
					$action_list = array(
						'type'		=> $action,
						'url_title'	=> $url_title,
						'url'		=> $url,
						'imgurl'	=> '',
						'page'		=> $page,
						'productid'	=> $productid
					);
					$data = array(
						'action'	=> $action,
						'productid'	=> $productid,
						'url_title'	=> $url_title,
						'url'		=> $url,
						'action_list'=> json_encode($action_list)
					);
					$fields = array(
						'registration_ids' 	=> $tokenid,
						'notification'		=> $msg,
						'data'				=> $data
					);
					error_log("[push_curl] : ".json_encode($fields));
			
					$query ="INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_{$log_target}msg_history`
					SET
						`title`			= '{$push_title}',
						`groupset`		= '{$groupset}',
						`body`			= '{$body}',
						`action`		= '{$action}',
						`productid`		= '{$productid}',
						`url_title`		= '{$url_title}',
						`url`			= '{$url}',
						`page`			= '{$page}',
						`sender`		= '1',
						`sendto`		= '{$sendto}',
						`sendtime`		= now(),
						`pushed`		= 'Y',
						`is_hand`       = 'N',
						`push_type`     = 'S',
						`insertt`		= now() ";
					error_log("[push_msg_history]:". $query);
					$db->query($query);
					
					//推播的Curl
					$this->push_curl($fields, $headers);
				}
			}
		}
	}

    //推送的Curl方法
    public function push_curl($param='', $header='') {
		if (empty($param)) { return false; }
		
		/*
		$postUrl = 'https://fcm.googleapis.com/fcm/send'; //$this->url;
        $curlPost = json_encode($param);
        $ch = curl_init();                                      //初始化curl
        curl_setopt($ch, CURLOPT_URL, $postUrl);                 //抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);                    //设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);            //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);                      //post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);          // 增加 HTTP Header（头）里的字段
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);        // 终止从服务端进行验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $data = curl_exec($ch);                                 //运行curl
        error_log("[broadcast_fcm]result: ". json_encode($data));
		curl_close($ch);
		*/
        
		//return $data;
    }
    
}