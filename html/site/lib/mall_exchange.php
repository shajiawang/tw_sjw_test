<?php
/*
*	兌換商品 mall_exchange.php
*/
include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR ."/wechat.class.php");
include_once(LIB_DIR."/helpers.php");

class exchange_ext 
{
	public $_v = array();
	
	//串接入口
	public function home($var=array())
	{
		global $mall, $member, $config, $user;
		
		//參數:
		$this->_v = $var;
		$this->userid = $var['userid'];
		$check_var = true;
		$insert_exe = false;
		
		error_log("[lib/mall_exchange] var :".json_encode($var));
		if($check_var) {
			//導入執行	
			switch ($var['type'])
			{
				case 'exchange': //訂單建立
				case 'exchange_test':
						$ret = $this->check_out();
					break;
				
				case 'exchange_pay': //訂單付現
						$ret = $this->exchange_pay();
					break;
					
				default:
						$ret['retCode']=-4;
						$ret['retMsg']='串接類型錯誤 !';	
					break;
			}
		}
		
		return $ret;
	}
	
	//兌換商品表單
	public function set_exchange($var=array())
	{
		global $db, $config, $mall, $member, $deposit;
		
		//參數:
		$ret = $var['setret']; 
		$product = $var['setret']['retObj']; //兌換商品資料
		$this->userid = $var['userid'];
		$check_var = true;
		$insert_exe = false;
		
		$ret['status'] = 0;	
		// 20200113檢查用戶是否有異常下標(出價為商品市價8成以上)現象
		$chk2 = $this->chk_strange_bid_history($this->userid);
		if(!empty($chk2['err'])) {
			$ret['retCode']=-8;
			$ret['retMsg']='兌換資格異常  請洽客服 !!';	
			$ret['status'] = 8;
		}
		
		//判斷鯊魚點來源是否正常
		$mall_check = $this->deposit_bonus_check($this->userid);
		if(!empty($mall_check['err'])) {
			$ret['retCode']=-9;
			$ret['retMsg']='鯊魚點資料異常  請洽客服 !!';
			$ret['status'] = 9;
		}
		
		if(empty($ret['status'])) {
			//可否補差額
			$eptype = ($product['eptype']) ? $product['eptype'] : 0;
			
			//可否刷卡
			$creditcard = $product['creditcard'];
			
			//會員鯊魚點餘額資料
			$get_spoint = $member->get_bonus($this->userid);
			$ret['retObj']['user_bonus'] = empty($get_spoint['amount']) ? 0 : intval($get_spoint['amount']);
			$ret['payment_list'] = array();
			
			if(empty($eptype) ){
				//AARONFU 現金(刷卡)補差額支付項目
				$payment_list = $deposit->row_drid_list('Y', '', 'mall');
				
				if(!empty($payment_list) ){
					foreach($payment_list as $k=>$v ){
						$drid = $v['drid'];
						
						$payment_arr[$drid] = $v;
					}
					
					if($creditcard=='N' ){
						unset($payment_arr[31]);
					}
					
					//AARONFU測試
					if($this->userid=='38802' || $this->userid=='1705'){}else{
						unset($payment_arr[28]);
					}
					
					$ret['payment_list'] = $payment_arr;
				}
			}
		}

		unset($ret['status']);
		
		return $ret;
	}
	
	//結帳及建立訂單
	public function new_checkout($var=array())
	{
		global $db, $config, $mall, $member, $deposit;
		
		$this->_v = $var;
		$this->userid = $var['userid'];
		
		$ret['status'] = 0;	
		$order_info['note'] = $this->_v['note'];
		$order_info['phone'] = $this->_v['phone'];
		$order_info['qrid'] = $this->_v['qrid'];
		
		$chk1['err'] = $chk2['err'] = '';
		
		//20200113檢查用戶是否有異常下標(出價為商品市價8成以上)現象
		$chk2 = $this->chk_strange_bid_history($this->userid);
		if(!empty($chk2['err'])) {
			$ret['retCode']=-8;
			$ret['retMsg']='兌換資格異常, 請洽客服 !!';
			$ret['status'] = 8;
		}
		
		//判斷鯊魚點來源是否正常
		$mall_check = $this->deposit_bonus_check($this->userid);
		if(!empty($mall_check['err'])) {
			$ret['retCode']=-9;
			$ret['retMsg']='帳號資料異常, 請洽客服 !!';
			$ret['status'] = 9;
		}
		
		
		if(empty($ret['status'])) 
		{
			//商品資料
			$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
				FROM `saja_exchange`.`saja_exchange_product` p 
				WHERE p.`prefixid` = 'saja'  
					AND p.epid = '{$this->_v['epid']}'
					AND p.switch = 'Y'
					LIMIT 1";
			$table = $db->getQueryRecord($query); 
			
			$order_info['esid'] = $esid = $table['table']['record'][0]['esid'];
			$retail_price = ($table['table']['record'][0]['retail_price']) ? (float)$table['table']['record'][0]['retail_price'] : 0;
			$cost_price = ($table['table']['record'][0]['cost_price']) ? (float)$table['table']['record'][0]['cost_price'] : 0;
			$process_fee = ($table['table']['record'][0]['process_fee']) ? (float)$table['table']['record'][0]['process_fee'] : 0;
			$order_info['point_price'] = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;
			$order_info['num'] = $this->_v['num'] * $table['table']['record'][0]['set_qty'];
			
			//可否補差額
			$order_info['eptype'] = ($table['table']['record'][0]['eptype']) ? $table['table']['record'][0]['eptype'] : 0;
			$order_info['creditcard'] = $table['table']['record'][0]['creditcard'];
			
			//分潤=(市價-進貨價)*數量
			$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

			//商品兌換總點數
			$used_point = $order_info['point_price'] * $order_info['num'];

			//商品處理費總數 $process_fee * $num;
			$order_info['used_process'] = $process_fee;
			
			//會員購物金餘額
			$get_rebate = $member->get_rebate($this->userid);
			$member_rebate = $get_rebate['amount']*1;
			$order_info['member_rebate'] = $member_rebate;
			
			//商品購物金折扣比例(點數) * 數量
			$order_info['rebate'] = empty($table['table']['record'][0]['rebate']) ? 0 : (float)$table['table']['record'][0]['rebate'] * $order_info['num'];
			
			//購物金折抵上限
			if (empty($order_info['rebate']) || $order_info['rebate']<0 ){
				$max_rebate = 0;
			}else{
				$max_rebate = ($member_rebate > $order_info['rebate']) ? $order_info['rebate'] : $member_rebate;
			}
			$order_info['max_rebate'] = $max_rebate;
			
			//使用購物金折抵
			$order_info['used_rebate'] = 0;
			
			//商品總費用 = 商品兌換總點數  +處理費
			$order_info['total_fee'] = $used_point + $order_info['used_process'];
			
			//會員鯊魚點餘額
			$get_spoint = $member->get_bonus($this->userid);
			$user_bonus_pay = (empty($get_spoint['amount']) || $get_spoint['amount']<0) ? 0 : floatval($get_spoint['amount']);
			$order_info['bonus_pay'] = $order_info['total_fee'];
			
			//現金(刷卡)差額
			$order_info['cash_pay'] = 0;

			//查驗可否補差額
			if(!empty($order_info['eptype']) ){
				if($user_bonus_pay < $order_info['total_fee']){
					$ret['status'] = 4;
					$ret['retCode']=-4;
					$ret['retMsg']='鯊魚點數不足 !!';
				}
			}else{
				//現金(刷卡)補差額
				if(empty($order_info['eptype']) && ($user_bonus_pay < $order_info['total_fee']) ) {
						
						//會員可扣鯊魚點
						$order_info['bonus_pay'] = $user_bonus_pay;
						
						//費用差額
						$pd_total = floatval($order_info['total_fee']) - floatval($user_bonus_pay);
						
						if($pd_total >= $max_rebate){ //費用差額 大於 購物金折抵上限
							$pay_used_rebate = $max_rebate;
							
							//補差額 = 費用差額 -購物金折抵上限
							$order_info['cash_pay'] = $pd_total - $pay_used_rebate;
						} else {
							 //購物金 完全折抵 費用差額
							 $pay_used_rebate = $pd_total;
							
							//補差額
							$order_info['cash_pay'] = 0;
						}
						
						//使用購物金折抵
						$order_info['used_rebate'] = $pay_used_rebate;
						
						$ret['status'] = 0;
				}
			}
			
			//查詢企業id
			$query = "SELECT `enterpriseid` FROM `saja_user`.`saja_enterprise`
				WHERE `prefixid` = 'saja' 
					AND `esid` = '{$esid}' 
					AND `switch` = 'Y' ";
			$recArr = $db->getQueryRecord($query); 
			$order_info['enterpriseid'] = $recArr['table']['record'][0]['enterpriseid'];
		}
		
		//if(empty($order_info['eptype']) && !empty($order_info['cash_pay']) ){ $ret['status'] = 0; } 
		error_log("[new_checkout]expw_check: ".$this->userid);
		if($this->userid =='38802'){ }else{
			//使用殺點(不補差額), 需查驗兌換密碼
			$chk1 = $this->expw_check();
			//if(!empty($chk1['err']))
			if($chk1['retCode'] !=1){
				$ret['retCode']=-3;
				$ret['retMsg']='兌換密碼錯誤 !!';
				$ret['status'] = 3;
			}
		}
		
		//20200707 檢查用戶今日兌換的特定商品數量是否小於3張及總價值是否小於1000
		$Arr = array(2419,2420,2421,2424,2427,2430,2431,2432,2435,2438,2441,2444,2445,2446);
		if (in_array($this->_v['epid'], $Arr)) {
			$chk3 = $this->chk_order_history($this->userid, $order_info);
			error_log("[lib/exchange_ext/chk_order_history] chk3 : ".json_encode($chk3));
			if(!empty($chk3['err'])) {
				$ret['retCode']=-10;
				$ret['retMsg']='特殊商品兌換資格異常, 請洽客服 !!';
				$ret['status'] = 10;
			}
		}
		
		
		if($order_info['used_rebate'] + $order_info['bonus_pay'] + $order_info['cash_pay'] != $order_info['total_fee']){
			$ret['retCode']=-2;
			$ret['retMsg']='兌換(支付)點數異常 !!';
			$ret['status'] = 2;
		}
		error_log("[new_checkout]order_info: ".json_encode($order_info) );
	
		$mk['orderid'] = '';
		if(empty($ret['status']) ) 
		{
			//產生訂單記錄
			$mk = $this->mk_order($order_info);

			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			
			if($this->userid == 1777 || $this->userid == 116 || $this->userid == 28) {
				$postdata['epid'] = $this->_v['epid'];
				$postdata['orderid'] = $mk['orderid'];
				$postdata['userid'] = $this->userid;
				$postdata['cost_price'] = $cost_price;
				$this->order_enterprise($order_info, $postdata); //訂單:企業
			}
			
			if (empty($mk['orderid']) ) {
				$ret['retCode']=-1;
				$ret['retMsg']='兌換失敗 !!';
			} else {
				//產生兌換訂單
				$ret['retCode']=1;
				$ret['retMsg']='OK';
				$ret['orderid'] = $mk['orderid'];
				
				//現金(刷卡)補差額, 繼續以下的處理
				if(empty($order_info['eptype']) && !empty($order_info['cash_pay']) ){
					$ret = $this->new_exchangepay($mk['orderid']);
				}
			}
		}

		unset($ret['status']);
		return $ret;
	}
	
	//訂單:企業
	public function order_enterprise($order_info, $postdata)
	{
		global $db, $config;
		
		//查詢企業id
		$query = "SELECT `profit_ratio` FROM `saja_user`.`saja_enterprise_shop` 
			WHERE `prefixid` = 'saja' AND `switch` = 'Y'
				AND `enterpriseid` = '{$order_info['enterpriseid']}' ";
		$recArr = $db->getQueryRecord($query); 
		$profit_ratio = ($recArr['table']['record'][0]['profit_ratio']) ? (float)$recArr['table']['record'][0]['profit_ratio'] : 0;

		if (($order_info['profit'] > 0) && ($postdata['cost_price'] > 0)) {
			$postdata['amount'] = $order_info['profit'];
		} else {
			$postdata['amount'] = $order_info['total_fee'] * ($profit_ratio/1000);
		}
		
		$url = "https://www.saja.com.tw/site/mall/giftDistributionProfits";
		//設定送出方式-POST
		$stream_options = array(
			'http' => array (
					'method' => "POST",
					'content' => json_encode($postdata),
					'header' => "Content-Type:application/json" 
			) 
		);
		$context  = stream_context_create($stream_options);
		// 送出json內容並取回結果
		$response = file_get_contents($url, false, $context);
		error_log("[exchange_ext/check_out] response : ".$response);
		
		// 讀取json內容
		$arr = json_decode($response,true);	
		error_log("[exchange_ext/check_out] arr : ".json_encode($arr));
	}
	
	//訂單付現支付方式
	public function new_exchangepay($orderid)
	{
		global $db, $config, $mall, $user, $deposit;
		
		//參數:
		$check_var = true;
		$insert_exe = false;
		$ret = array( 'retCode'=>1, 'retMsg'=>"OK", 'retType'=>"OBJ", 'retObj'=>array() );
		
		//支付方式編號
		$drid = $this->_v['drid'];
		
		if($check_var) {
			//訂單支付金額
			$order_info = $mall->order_info($this->userid, $orderid);
			error_log("[new_exchangepay]order_info: ".json_encode($order_info) );
			$cash_pay = $order_info['cash_pay'];
			$bonus_pay = $order_info['bonus_pay'];
			
			//繳款期限
			$last_day_t = strtotime($order_info['insertt']) + (86400*3);
			$last_day = date('Y-m-d H:i', $last_day_t);
			
			$payment_rule = $deposit->deposit_rule($drid);
			$payment_fee  = $payment_rule[0]["payment_fee"]; //支付手續費
			$process_p = $payment_fee * 100 .'%';//比例
			
			//訂單處理費
			$process_fee = round($cash_pay * $payment_fee);
			
			//更新 saja_order
			$query = "UPDATE `saja_exchange`.`saja_order` SET 
						`process_fee`='{$process_fee}',
						`confirm`='Y',
						`modifyt` = now()
						WHERE `orderid`='{$orderid}'";
			$db->query($query);
			
			if($bonus_pay > 0 && $cash_pay <= 0){
				//鯊魚點支付
				
				//更新 saja_order_log
				$arr_cond=array();
				$arr_cond['orderid'] = $orderid;
				// 添加資訊
				$arr_update=array();
				$arr_update['amount'] = $bonus_pay;
				$arr_update['drid'] = 0;
				$arr_update['paytime'] = date('Y-m-d H:i:s', time());
				$arr_update['switch'] = 'Y';
				$mall->update_order_log($arr_cond, $arr_update);
				
				$ret['orderid'] = $orderid;
				$ret['status'] = 0;
				
			} else {
				//現金(刷卡)補差額
				
				//繳費總金額
				$amount = $cash_pay + $process_fee;
				
				$get_payment['drid'] = $drid;
				$get_payment['name'] = $payment_rule[0]["name"]; //支付選項名稱
				$pay_act = $payment_rule[0]["act"]; // "creditcard"
				
				//支付選項
				$get_order['ordernumber'] = $orderid; //訂單編號
				$get_order['loc'] = 'mall';
				$get_order['act'] = $pay_act; //支付 "creditcard"或"remittance"
				$get_order['amount'] = $amount;
				$get_order['spoint'] = 0;
				$ret['retObj']['order_data'] = $get_order;

				if($pay_act=='creditcard')
				{
					//信用卡支付
					$get_payment['api_url'] = BASE_URL . APP_DIR ."/deposit/twcreditcard_pay/"; //支付後端 API網址
					
					//更新 saja_order_log
					$arr_cond=array();
					$arr_cond['orderid'] = $orderid;
					// 添加資訊
					$arr_update=array();
					$arr_update['amount'] = $amount;
					$arr_update['drid'] = 31; //兌換商城信用卡(紅陽)
					$arr_update['switch'] = 'Y';
					$mall->update_order_log($arr_cond, $arr_update);
					
					//會員信用卡編號
					$get_payment['ucid'] = 0; 
					$UserCreditCard = $user->getUserCreditCardTokenList($this->userid);
					if(!empty($UserCreditCard[0]) ){
						$get_payment['ucid'] = $UserCreditCard[0]["ucid"];
					}
					
					$chkStr = $orderid ."|". round(floatval($amount));
					$cs = new convertString();
					//加密簽名參數
					$get_payment['chkStr'] = $cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
				
					$get_payment['driid'] = 0;
					$get_payment['runAt'] = 'APP';
					$get_payment['pay_info'] = array("手續費：{$process_p}", "兌換補差額：". $cash_pay);
				}
				else if($pay_act=='remittance')
				{
					//銀行虛擬帳號
					$backno = '10002';
					
					if(intval($orderid) > 99999999999){
						$ret['retCode']=-10;
						$ret['retMsg']='訂單虛擬帳號超過範圍 !!';	
					} else {
					
						//建立虛擬帳號
						$account_sprintf = sprintf("%011d", $orderid);
						$virtual_account = $backno . $account_sprintf;
						
						//更新 saja_order_log
						$arr_cond=array();
						$arr_cond['orderid'] = $orderid;
						//添加資訊
						$arr_update=array();
						$arr_update['amount'] = $amount;
						$arr_update['drid'] = 28; //兌換商品匯款(一銀)
						$arr_update['switch'] = 'Y';
						$arr_update['mall_status'] = 'order';
						$arr_update['virtual_account'] = $virtual_account;
						$arr_update['memo'] = "back:007|virtual_account:". $virtual_account."|手續費:". $process_p ."|繳款期限:". $last_day;
						$mall->update_order_log($arr_cond, $arr_update);
						
						$_extrainfo_bankname =  '第一商業銀行';
						$_extrainfo_bankpic =  'bank_logo/logo-firstbank.png';
						$_extrainfo_lastday = "繳款期限：". $last_day;
						$_extrainfo_pay = array("手續費：{$process_p}", "兌換補差額：". $cash_pay, "繳費總金額：". $amount);
						$_extrainfo_text = array("銀行總代號：007", "銷帳編號：". $virtual_account);
						$_extrainfo_remark = '<div style="margin: 1rem; font-family:sans-serif;">
												 <br>1.每個帳號擁有各自專屬繳費號碼，請勿匯入他人專屬繳費的帳戶中。</br>';
						$_extrainfo_remark.='<br>2.若使用<font color="red"><b>第一銀行「臨櫃」/「實體ATM」</b></font>繳款，請務必使用「<font color="red"><b>繳費</b></font>」功能，其他銀行不在此限。</br>
												 <br>3.金額請輸入「總計」金額，勿自行扣除手續費，以免系統無法自動核銷帳並入帳。</br>
												 <br>4.繳款金額與繳款流程操作無誤，系統將於15~20分鐘自動入帳。</br>
												 <br>5.若操作有誤或繳款金額有誤，則需要人工入帳，請客服Line@saja 並提供您繳費收據與會員編號。</br>
												 <br>6.人工入帳作業處理流程需3~5個工作天不含假日，入帳金額將扣除人工處理手續費每筆15元。</div>';

						$get_payment['user_extrainfo_bankid'] = '007';
						$get_payment['user_extrainfo'] = $virtual_account;
						$get_payment['user_extrainfo_lastday'] = $_extrainfo_lastday;
						$get_payment['user_extrainfo_pay'] = $_extrainfo_pay;
						$get_payment['user_extrainfo_bankname'] = $_extrainfo_bankname;
						$get_payment['user_extrainfo_bankpic'] = $_extrainfo_bankpic;
						$get_payment['user_extrainfo_text'] = $_extrainfo_text;
						$get_payment['user_extrainfo_remark'] = $_extrainfo_remark;
					}
				}
				
				$ret['retObj']['payment_data'] = $get_payment;
			}
		}
		
		return $ret;
	}
	
	//產生訂單記錄
	public function mk_order($order_info) {
		global $db, $config;
		$time_start = microtime(true);

		$userid = $this->userid; 
		$epid 	= $this->_v['epid'];
		$esid 	= $order_info['esid'];	
		$r['err'] = '';
		
		if($epid == '525') {
			$memo = 'tel:'.$order_info['phone'];
		} else {
			$memo = '';
			if( empty($order_info['eptype']) ){
				$memo_info = $order_info;
				unset($memo_info['note']);
				unset($memo_info['phone']);
				$memo = json_encode($memo_info);
			}
		}
		
		if(empty($r['err'])) { //使用購物金折抵$order_info['used_rebate']
			//測試帳號訂單
			$try_user = array('1705', '38802');
			$o_type = 'exchange';
			if(in_array($this->userid, $try_user) ){
				$o_type = 'tryexchange'; 
			}
			
			//新增訂單
			$query = "INSERT INTO `saja_exchange`.`saja_order` SET
				`prefixid`='saja',
				`userid`='{$this->userid}',
				`status`='0',
				`epid`='{$epid}',
				`esid`='{$esid}',
				`num`='{$order_info['num']}',
				`point_price`='{$order_info['point_price']}',
				`process_fee`='{$order_info['used_process']}',
				`total_fee`='{$order_info['total_fee']}',
				`bonus_pay`='{$order_info['bonus_pay']}',
				`cash_pay`='{$order_info['cash_pay']}',
				`rebate_pay`='{$order_info['used_rebate']}',
				`profit`='{$order_info['profit']}',
				`memo` = '{$memo}',
				`note` = '{$order_info['note']}',  
				`type`='{$o_type}',
				`switch`='Y' ";
			
			if(empty($order_info['eptype']) ){
				$query .= ",`confirm`='N' ";
			}else{
				$query .= ",`confirm`='Y' ";
			}
			
			$query .= ",`insertt`=NOW()";
			error_log("[exchange_ext/mk_order]saja_order:". json_encode($query) );
			$res = $db->query($query); 
			
			//訂單編號
			$orderid = $db->_con->insert_id;
			$r['orderid'] = (empty($orderid)) ? '' : $orderid;
			
			if (!empty($order_info['qrid'])){
				$query = "UPDATE `saja_cash_flow`.`saja_qrcode` SET 
					`orderid` = '{$orderid}',
					`used` = 'Y',
					`used_time` = NOW() 
				WHERE `qrid` = '{$order_info['qrid']}' 
					AND `userid`='{$this->userid}' 
					AND `orderid` = 0 
					AND `used` = 'N'";
				$res = $db->query($query);
			}
			
			//商品卡
			if($order_info['eptype'] == '2' || $order_info['eptype'] == '3'){
				$query ="SELECT cid FROM `saja_exchange`.`saja_exchange_card` 
				WHERE epid = '{$epid}'
					AND userid = '0'
					AND orderid = '0'
					AND used = 'N'
					AND switch = 'Y'
					ORDER BY cid ASC,insertt ASC
					LIMIT {$order_info['num']}
				";

				$table3 = $db->getQueryRecord($query);
				//商品卡數量
				$card_num = count($table3['table']['record']);
				//商品卡庫存 = 商品卡數量 - 預留庫存
				$stock = $card_num - $order_info['reserved_stock'];

				if($stock >= $order_info['num']){ //庫存足夠
					$cid_value = '';
					foreach($table3['table']['record'] as $row){
						$cid_value .= $row['cid'].',';
					}
					//將cid組合成字串
					$cid_value = rtrim($cid_value,',');

					$db->query('start transaction');
					$affected_num = 0;
					$query = "UPDATE `saja_exchange`.`saja_exchange_card` SET 
						`userid` = '{$this->userid}',
						`orderid` = '{$orderid}',
						`used` = 'Y'
					WHERE `cid` IN ({$cid_value}) AND `userid` = 0 AND `orderid` = 0 AND `used` = 'N'";

					$res = $db->query($query);
					//update的資料筆數
					$affected_num = $db->_con->affected_rows;

					if($affected_num == $order_info['num']){		//更新比數與兌換數量符合
						$db->query('COMMIT');

						//商品卡訂單直接更新為已到貨狀態
						$query = "UPDATE `saja_exchange`.`saja_order` SET 
						`status` = '4'
						WHERE `orderid` = '{$orderid}'";
						$res = $db->query($query);
						
					}else{											//更新比數與兌換數量不符合
						$db->query('ROLLBACK');

						//刪除訂單
						$query = "UPDATE `saja_exchange`.`saja_order` SET 
						`switch` = 'N'
						WHERE `orderid` = '{$orderid}'";
						$res = $db->query($query);

						error_log("[exchange_ext/mk_order]:stock_not_enough，delete_order:".$orderid);
						$r['err'] = -2;
						return $r;
					}
			
				}else{ //庫存不足
					
					//刪除訂單
					$query = "UPDATE `saja_exchange`.`saja_order` SET 
					`switch` = 'N'
					WHERE `orderid` = '{$orderid}'";
					$res = $db->query($query);

					error_log("[exchange_ext/mk_order]:stock_not_enough，delete_order:".$orderid);
					$r['err'] = -2;
					return $r;
				}
			}//商品卡

			
			//查詢使用者收件相關資料
			$query = "SELECT `addressee`, `area`, `address`, `phone`, `gender` FROM `saja_user`.`saja_user_profile` 
				WHERE prefixid = 'saja' AND userid = '{$this->userid}' ";
			$consignee = $db->getQueryRecord($query);
			
			//判定是否傳值,無傳值則使用查詢資料替代
			$name = (empty($_POST['name'])) ? $consignee['table']['record'][0]['addressee'] : $_POST['name'];
			$zip = (empty($_POST['zip'])) ? $consignee['table']['record'][0]['area'] : $_POST['zip'];
			$zip = empty($zip) ? 0 : $zip;
			$address = (empty($_POST['address'])) ? $consignee['table']['record'][0]['address'] : $_POST['address'];
			$phone = (empty($_POST['phone'])) ? $consignee['table']['record'][0]['phone'] : $_POST['phone'];
			$gender = (empty($_POST['gender'])) ? $consignee['table']['record'][0]['gender'] : $_POST['gender'];
			
			//新增訂單收件人
			$query = "INSERT INTO `saja_exchange`.`saja_order_consignee` SET 
				`userid`='{$this->userid}',
				`orderid`='{$orderid}',
				`name`='{$name}',
				`phone`='{$phone}',
				`zip`='{$zip}',
				`address`='{$address}',
				`gender`='{$gender}',
				`prefixid`='saja',
				`insertt`=now()";
			error_log("[exchange_ext/mk_order]saja_order_consignee:". json_encode($query) );
			$db->query($query);
			
			//新增訂單記錄檔
			$query = "INSERT INTO `saja_exchange`.`saja_order_log` SET 
				`userid`='{$this->userid}',
				`orderid`='{$orderid}',
				`prefixid`='saja'";
			
			if(empty($order_info['eptype']) ){
				$query .= ",`switch`='N' ";
			}else{
				$query .= ",`switch`='Y' ";
			}
			
			$query .= ",`insertt`=NOW()";
			error_log("[exchange_ext/mk_order]saja_order_log:". json_encode($query) );
			$db->query($query);
			
			//兌換訂單, 扣除購物金
			$query = "INSERT INTO `saja_cash_flow`.`saja_rebate` SET 
			          `prefixid` = 'saja', 
			          `userid` = '{$this->userid}', 
					  `productid` = '{$epid}', 
					  `orderid` = '{$orderid}',
			          `behav` = 'user_exchange', "; 
			$query .= "`amount` = '-{$order_info['used_rebate']}', ";
			$query .= "`switch` = 'Y', 
			          `insertt` = now()";
			
			if(!empty($order_info['used_rebate']) ){
				$db->query($query);
				$reid = $db->_con->insert_id;
				error_log("[exchange_ext/mk_order]saja_rebate:reid {$reid} , ". json_encode($query));
			}
			
			//扣除點數
			$query = "INSERT INTO `saja_cash_flow`.`saja_bonus` SET 
			          `prefixid` = 'saja', 
			          `userid` = '{$this->userid}', 
					  `orderid` = '{$orderid}',
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', "; 
			$query .= "`amount` = '-{$order_info['bonus_pay']}', ";
			$query .= "`switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bonusid = $db->_con->insert_id;
			error_log("[exchange_ext/mk_order]saja_bonus:". json_encode($query));
			
			//商家增加點數(Frank-14/12/16)
			$query = "INSERT INTO `saja_cash_flow`.`saja_bonus_store` SET 
			          `prefixid` = 'saja', 
			          `bonusid` = '{$bonusid}', 
			          `esid` = '{$esid}', 
			          `enterpriseid` = '{$order_info['enterpriseid']}', 
			          `countryid` = '{$config['country']}', 
			          `behav` = 'user_exchange', 
			          `amount` = '{$order_info['total_fee']}',
					  `bonus_pay`='{$order_info['bonus_pay']}',
					  `cash_pay`='{$order_info['cash_pay']}', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			$bsid = $db->_con->insert_id;
			error_log("[exchange_ext/mk_order]saja_bonus_store:". json_encode($query));
			
			//新增紅利點數歷史資料(Frank-14/12/16)
			$query = "INSERT INTO `saja_exchange`.`saja_exchange_bonus_store_history` SET 
			          `prefixid` = 'saja', 
			          `esid` = '{$esid}', 
			          `orderid` = '{$orderid}',
			          `bsid` = '{$bsid}',
			          `enterpriseid` = '{$order_info['enterpriseid']}', 
			          `amount` = '{$order_info['total_fee']}',
					  `bonus_pay`='{$order_info['bonus_pay']}',
					  `cash_pay`='{$order_info['cash_pay']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			error_log("[exchange_ext/mk_order]saja_exchange_bonus_store_history:". json_encode($query));
			
			//新增商家紅利點數歷史資料
			$query = "INSERT INTO `saja_exchange`.`saja_exchange_bonus_history` SET 
			          `prefixid` = 'saja', 
			          `userid` = '{$this->userid}', 
			          `orderid` = '{$orderid}',
			          `bonusid` = '{$bonusid}',
			          `amount` = '-{$order_info['total_fee']}',
					  `bonus_pay`='{$order_info['bonus_pay']}',
					  `cash_pay`='{$order_info['cash_pay']}',
			          `seq` = '0', 
			          `switch` = 'Y', 
			          `insertt` = now()";
			$db->query($query);
			error_log("[exchange_ext/mk_order]saja_exchange_bonus_history:". json_encode($query));
			
			if($order_info['eptype'] != '2' && $order_info['eptype'] != '3' && $order_info['eptype'] != '4'){
				//扣除庫存
				$query = "INSERT INTO `saja_exchange`.`saja_stock` SET 
						`prefixid` = 'saja', 
						`behav` = 'user_exchange', 
						`epid` = '{$epid}',
						`num` = '-{$order_info['num']}', 
						`orderid` = '{$orderid}',
						`seq` = '0', 
						`switch` = 'Y', 
						`insertt` = now()";
				$db->query($query);
			}

			//圓夢券
			if ($order_info['eptype'] == 5){
				for($i = 0; $i < $order_info['num']; $i++){ 
					$query = "INSERT INTO `saja_cash_flow`.`saja_dscode` SET
						`prefixid`='saja',
						`userid`='{$this->userid}',
						`orderid`='{$orderid}',
						`used`='N',
						`amount`='1',
						`behav`='user_exchange',
						`feedback`='N',
						`switch`='Y' ,
						`insertt`=NOW()";
					$res = $db->query($query); 
					$dscode = $db->_con->insert_id;
				}
			}

			// 計算免費鯊魚點餘額並更新
			$query = "SELECT sum(free_amount) total_free_amount FROM `saja_cash_flow`.`saja_spoint_free_history`
				WHERE `userid`='{$this->userid}'
					AND `switch` = 'Y'
					AND `amount_type` = 2
				";
			$table = $db->getQueryRecord($query); 
			
			if ($table['table']['record'][0]['total_free_amount'] > 0) {
				$free_amount = ($table['table']['record'][0]['total_free_amount'] >= $order_info['total_fee']) ? $order_info['total_fee'] : $table['table']['record'][0]['total_free_amount'];
				$query = "INSERT INTO `saja_cash_flow`.`saja_spoint_free_history` SET
					`userid`='{$this->userid}',
					`behav` = 'user_exchange', 
					`amount_type` = 2, 
					`free_amount` = '-{$free_amount}', 
					`total_amount` = '-{$order_info['total_fee']}', 
					`bonusid` = '{$bonusid}', 
					`orderid` = '{$orderid}',
					`switch` = 'Y', 
					`insertt` = now()";
				$db->query($query);
			}
		}
		
		return $r;
	}
	
	//訂單付現支付方式
	public function exchange_pay()
	{
		global $db, $config, $mall, $user, $deposit;
		
		//參數:
		$check_var = true;
		$insert_exe = false;
		
		$ret = array(
			'retCode'=>1,
			'retMsg'=>"OK",
			'retType'=>"OBJ",
			'retObj'=>array()
		);
		
		$get_order['ordernumber'] = $this->_v['orderid']; //訂單編號
		if(empty($this->_v['orderid']) ) {
			$ret['retCode']=-1;
			$ret['retMsg']='訂單編號錯誤 !!';	
			$check_var = false;
		}
		
		//支付方式編號
		$drid = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		if(empty($drid) ) {
			$ret['retCode']=-3;
			$ret['retMsg']='支付方式參數錯誤 !!';	
			$check_var = false;
		}
		
		if($check_var) {
			$ret['retCode']=1;
			$ret['retMsg']='OK';
			
			//訂單支付金額
			$order_info = $mall->order_info($this->userid, $this->_v['orderid']);
			$order_memo = json_decode($order_info["memo"]);
			$cash_pay = $order_memo->cash_pay;
			
			//繳款期限
			$last_day_t = strtotime($order_info['insertt']) + (86400*3);
			$last_day = date('Y-m-d H:i', $last_day_t);
			
			$payment_rule = $deposit->deposit_rule($drid);
			$get_payment['drid'] = $drid;
			$get_payment['name'] = $payment_rule[0]["name"]; //支付選項名稱
			$pay_act = $payment_rule[0]["act"]; // "creditcard"
			
			//支付選項
			$get_order['loc'] = 'mall';
			$get_order['act'] = $pay_act; //支付 "creditcard"或"remittance"

			if($pay_act=='creditcard'){
				//信用卡支付
				$get_payment['api_url'] = BASE_URL . APP_DIR ."/deposit/twcreditcard_pay/"; //支付後端 API網址
				
				//訂單處理費
				$process_p = '5%';//比例
				$process = 0.05;//比例
				$process_fee = round($cash_pay * $process);
				
				//繳費總金額
				$amount = $cash_pay + $process_fee;
				$get_order['amount'] = $amount;
				$get_order['spoint'] = 0;
				$ret['retObj']['order_data'] = $get_order;
				
				//更新 saja_order
				$query = "UPDATE `saja_exchange`.`saja_order` SET 
						`process_fee`='{$process_fee}',
						`total_fee`='{$amount}',
						`cash_pay`='{$amount}',
						`modifyt` = now()
					WHERE `orderid`='{$this->_v['orderid']}'";
				$db->query($query);
				
				//更新 saja_order_log
				$arr_cond=array();
				$arr_cond['orderid'] = $this->_v['orderid'];
				// 添加資訊
				$arr_update=array();
				$arr_update['amount'] = $amount;
				$arr_update['drid'] = 31;
				$mall->update_order_log($arr_cond, $arr_update);
				
				//會員信用卡編號
				$get_payment['ucid'] = 0; 
				$UserCreditCard = $user->getUserCreditCardTokenList($this->userid);
				if(!empty($UserCreditCard[0]) ){
					$get_payment['ucid'] = $UserCreditCard[0]["ucid"];
				}
				
				$chkStr = $this->_v['orderid'] ."|". round(floatval($amount));
				$cs = new convertString();
				//加密簽名參數
				$get_payment['chkStr'] = $cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
			
				$get_payment['driid'] = 0;
				$get_payment['runAt'] = 'APP';
				$get_payment['pay_info'] = array("手續費：{$process_p}", "兌換補差額：". $cash_pay);
			}
			else if($pay_act=='remittance'){
				
				//銀行虛擬帳號
				$backno = '10002';
				
				if(intval($this->_v['orderid']) > 99999999999){
					$ret['retCode']=-10;
					$ret['retMsg']='訂單虛擬帳號超過範圍 !!';	
				} else {
				
					//建立虛擬帳號
					$account_sprintf = sprintf("%011d", $this->_v['orderid']);
					$virtual_account = $backno . $account_sprintf;
					
					//訂單處理費
					$process_p = '5%';//比例
					$process = 0.05;//比例
					$process_fee = round($cash_pay * $process);
					
					//繳費總金額
					$amount = $cash_pay + $process_fee;
					$get_order['amount'] = $amount;
					$get_order['spoint'] = 0;
					$ret['retObj']['order_data'] = $get_order;
					
					//更新 saja_order
					$query = "UPDATE `saja_exchange`.`saja_order` SET 
							`process_fee`='{$process_fee}',
							`total_fee`='{$amount}',
							`cash_pay`='{$amount}',
							`modifyt` = now()
						WHERE `orderid`='{$this->_v['orderid']}'";
					$db->query($query);
					
					//更新 saja_order_log
					$arr_cond=array();
					$arr_cond['orderid'] = $this->_v['orderid'];
					//添加資訊
					$arr_update=array();
					$arr_update['amount'] = $amount;
					$arr_update['drid'] = 28;
					$arr_update['virtual_account'] = $virtual_account;
					$arr_update['memo'] = "back:007|virtual_account:". $virtual_account."|手續費:5%|繳款期限:". $last_day;
					$mall->update_order_log($arr_cond, $arr_update);
					
					$_extrainfo_bankname =  '第一商業銀行';
					$_extrainfo_bankpic =  'bank_logo/logo-firstbank.png';
					$_extrainfo_lastday = "繳款期限：". $last_day;
					$_extrainfo_pay = array("手續費：{$process_p}", "兌換補差額：". $cash_pay, "繳費總金額：". $amount);
					$_extrainfo_text = array("銀行總代號：007", "銷帳編號：". $virtual_account);
					$_extrainfo_remark = '<div style="margin: 1rem; font-family:sans-serif;">
											 <br>1.每個帳號擁有各自專屬繳費號碼，請勿匯入他人專屬繳費的帳戶中。</br>';
					$_extrainfo_remark.='<br>2.若使用<font color="red"><b>第一銀行「臨櫃」/「實體ATM」</b></font>繳款，請務必使用「<font color="red"><b>繳費</b></font>」功能，其他銀行不在此限。</br>
											 <br>3.金額請輸入「總計」金額，勿自行扣除手續費，以免系統無法自動核銷帳並入帳。</br>
											 <br>4.繳款金額與繳款流程操作無誤，系統將於15~20分鐘自動入帳。</br>
											 <br>5.若操作有誤或繳款金額有誤，則需要人工入帳，請客服Line@saja 並提供您繳費收據與會員編號。</br>
											 <br>6.人工入帳作業處理流程需3~5個工作天不含假日，入帳金額將扣除人工處理手續費每筆15元。</div>';

					$get_payment['user_extrainfo_bankid'] = '007';
					$get_payment['user_extrainfo'] = $virtual_account;
					
					$get_payment['user_extrainfo_lastday'] = $_extrainfo_lastday;
					$get_payment['user_extrainfo_pay'] = $_extrainfo_pay;
					
					$get_payment['user_extrainfo_bankname'] = $_extrainfo_bankname;
					$get_payment['user_extrainfo_bankpic'] = $_extrainfo_bankpic;
					$get_payment['user_extrainfo_text'] = $_extrainfo_text;
					$get_payment['user_extrainfo_remark'] = $_extrainfo_remark;
				}
			}
			
			$ret['retObj']['payment_data'] = $get_payment;
		}
		
		return $ret;
	}
	
	//結帳及建立訂單
	public function check_out()
	{
		global $db, $config, $mall, $member, $deposit;
		
		$ret['status'] = 0;	
		$order_info['note'] = $this->_v['note'];
		$order_info['phone'] = $this->_v['phone'];
		$order_info['qrid'] = $this->_v['qrid'];

		/* 20160624*/
		$chk1['err'] = $chk2['err'] = '';
		$chk1 = $this->expw_check();
		
		// 20200113檢查用戶是否有異常下標(出價為商品市價8成以上)現象
		$chk2 = $this->chk_strange_bid_history($this->userid);
		
		if(!empty($chk1['err'])) {
			$ret['retCode']=-6;
			$ret['retMsg']='兌換密碼錯誤 !!';
			$ret['status'] = 6;
		}
		
		if(!empty($chk2['err'])) {
			$ret['retCode']=-8;
			$ret['retMsg']='兌換資格異常  請洽客服 !!';
			$ret['status'] = 8;
		} 
		
		//判斷鯊魚點來源是否正常
		$mall_check = $this->deposit_bonus_check($this->userid);
		if(!empty($mall_check['err'])) {
			$ret['retCode']=-9;
			$ret['retMsg']='帳號資料異常  請洽客服 !!';
			$ret['status'] = 9;
		}


		if(empty($ret['status'])) {
			$query ="SELECT p.*, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
			FROM `saja_exchange`.`saja_exchange_product` p 
			WHERE 
				p.`prefixid` = 'saja'  
				AND p.epid = '{$this->_v['epid']}'
				AND p.switch = 'Y'
				LIMIT 1
			" ;
			$table = $db->getQueryRecord($query); 

			$retail_price = ($table['table']['record'][0]['retail_price']) ? (float)$table['table']['record'][0]['retail_price'] : 0;
			$cost_price = ($table['table']['record'][0]['cost_price']) ? (float)$table['table']['record'][0]['cost_price'] : 0;
			$process_fee = ($table['table']['record'][0]['process_fee']) ? (float)$table['table']['record'][0]['process_fee'] : 0;
			$order_info['point_price'] = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;
			$order_info['num'] = $this->_v['num'] * $table['table']['record'][0]['set_qty'];
			$order_info['esid'] = $esid = $table['table']['record'][0]['esid'];
			
			//可否補差額
			$order_info['eptype'] = ($table['table']['record'][0]['eptype']) ? $table['table']['record'][0]['eptype'] : 0;
			$order_info['creditcard'] = $creditcard = $table['table']['record'][0]['creditcard'];
			
			if($this->_v['epid'] == 525) {
				$retail_price = $this->_v['total'];
				$cost_price = $this->_v['total'];
				$process_fee = 0;
				$order_info['point_price'] = $this->_v['total'];

				//分潤=(市價-進貨價)*數量
				$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

				//商品兌換總點數
				$used_point = $order_info['point_price'] * $order_info['num'];

				//商品處理費總數 $process_fee * $num;
				$order_info['used_process'] = $process_fee;

				//總費用
				$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			}
			else if($this->_v['epid'] == 689) {
				$retail_price = $this->_v['total'];
				$cost_price = $this->_v['total'];
				$process_fee = $this->_v['total'] - ($order_info['point_price'] * $this->_v['num']);

				//分潤=(市價-進貨價)*數量
				$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

				//商品兌換總點數
				$used_point = $order_info['point_price'] * $order_info['num'];

				//商品處理費總數 $process_fee;
				$order_info['used_process'] = $process_fee;

				//總費用
				$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			} else {
				//分潤=(市價-進貨價)*數量
				$order_info['profit'] = ($retail_price - $cost_price) * $order_info['num'];

				//商品兌換總點數
				$used_point = $order_info['point_price'] * $order_info['num'];

				//商品處理費總數 $process_fee * $num;
				$order_info['used_process'] = $process_fee;

				//總費用
				$order_info['total_fee'] = $order_info['used_process'] + $used_point;
			}
			
			//檢查使用者的鯊魚點數
			$query ="SELECT SUM(amount) bonus 
			FROM `saja_cash_flow`.`saja_bonus`  
			WHERE 
				`prefixid` = 'saja' 
				AND `switch` = 'Y'
				AND `userid` = '{$this->userid}' 
			";
			$recArr = $db->getQueryRecord($query); 
			$bonus_pay = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;

			//現金(刷卡)差額
			$order_info['cash_pay'] = 0;
			$order_info['bonus_pay'] = $order_info['total_fee'];
			
			//查驗可否補差額
			if($bonus_pay < $order_info['total_fee']) {
				if(! empty($order_info['eptype']) ){
					$ret['status'] = 5;
					$ret['retCode']=-5;
					$ret['retMsg']='鯊魚點數不足 !!';
				} else {
					//現金(刷卡)補差額 = 總費用 - 鯊魚點
					$order_info['cash_pay'] = $order_info['total_fee'] - $bonus_pay;
					$order_info['bonus_pay'] = $bonus_pay;
					$ret['status'] = 0;
				}
			}
			
			//查詢企業id
			$query = "SELECT enterpriseid 
			FROM `saja_user`.`saja_enterprise`
			WHERE 
				`prefixid` = 'saja' 
				AND `esid` = '{$esid}' 
				AND `switch` = 'Y'
			";
			$recArr = $db->getQueryRecord($query); 
			$order_info['enterpriseid'] = $recArr['table']['record'][0]['enterpriseid'];
		}
		
		if($this->_v['epid'] == 689 || $this->_v['epid'] == 2744) {
			if ($bonus_pay < $order_info['total_fee']){
				$ret['status'] = 5;
				$ret['retCode']=-5;
				$ret['retMsg']='鯊魚點數不足 !!';
			}
		}
		
		if(!empty($this->_v['qrid'])) {
			
			$query = "SELECT * 
				FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}qrcode`
				WHERE 
					`prefixid`='saja'
					`qrid` = '{$this->_v['qrid']}' 
					AND switch = 'Y'";
			$qrdata = $db->getQueryRecord($query);
			
			if (empty($qrdata['table']['record'][0])) {
				$ret['status'] = 7;
				$ret['retCode']=-7;
				$ret['retMsg']='此二維碼不存在 !!';
			} else if ($qrdata['table']['record'][0]['used'] != 'N' || $qrdata['table']['record'][0]['orderid'] != 0){
				$ret['status'] = 7;
				$ret['retCode']=-7;
				$ret['retMsg']='此二維碼優惠已使用 !!';
			} else {
				
			}
		}

		// 20200707檢查用戶今日兌換的特定商品數量是否小於3張及總價值是否小於1000
		$Arr = array(2419,2420,2421,2424,2427,2430,2431,2432,2435,2438,2441,2444,2445,2446);
		if (in_array($this->_v['epid'], $Arr)) {
			$chk3 = $this->chk_order_history($this->userid, $order_info);
			error_log("[lib/exchange_ext/chk_order_history] chk3 : ".json_encode($chk3));

			if(!empty($chk3['err'])) {
				$ret['retCode']=-10;
				$ret['retMsg']='特殊商品兌換資格異常  請洽客服 !!';
				$ret['status'] = 10;
			}
		}

		$mk['orderid'] = '';
		if(empty($ret['status'])) {
			//產生訂單記錄
			$mk = $this->mk_order($order_info);

			//回傳: 完成
			$ret['status'] = ($mk['err']) ? $mk['err'] : 200;
			
			if($this->userid == 1777 || $this->userid == 116 || $this->userid == 28) {
				
				//查詢企業id
				$query = "SELECT profit_ratio 
				FROM `saja_user`.`saja_enterprise_shop` 
				WHERE 
					`prefixid` = 'saja' 
					AND `enterpriseid` = '{$order_info['enterpriseid']}' 
					AND `switch` = 'Y'
				";
				$recArr = $db->getQueryRecord($query); 
				$profit_ratio = ($recArr['table']['record'][0]['profit_ratio']) ? (float)$recArr['table']['record'][0]['profit_ratio'] : 0;

				$postdata['epid'] = $this->_v['epid'];
				$postdata['orderid'] = $mk['orderid'];
				$postdata['userid'] = $this->userid;

				if (($order_info['profit'] > 0) && ($cost_price > 0)) {
					$postdata['amount'] = $order_info['profit'];
				} else {
					$postdata['amount'] = $order_info['total_fee'] * ($profit_ratio/1000);
				}
				
				$url = "https://www.saja.com.tw/site/mall/giftDistributionProfits";
				//設定送出方式-POST
				$stream_options = array(
					'http' => array (
							'method' => "POST",
							'content' => json_encode($postdata),
							'header' => "Content-Type:application/json" 
					) 
				);
				$context  = stream_context_create($stream_options);
				// 送出json內容並取回結果
				$response = file_get_contents($url, false, $context);
				error_log("[exchange_ext/check_out] response : ".$response);
				
				// 讀取json內容
				$arr = json_decode($response,true);	
				error_log("[exchange_ext/check_out] arr : ".json_encode($arr));
			}
			
			if (empty($mk['orderid']) ) {
				$ret['retCode']=-1;
				$ret['retMsg']='兌換失敗 !!';
			} else {
				//產生兌換訂單
				
				$ret['retCode']=1;
				$ret['retMsg']='OK';
				$ret['retObj']['orderid'] = $mk['orderid'];
				$ret['retObj']['controller_type'] = '';
				$ret['retObj']['total_fee'] = $order_info['total_fee'];
				$ret['retObj']['cash_pay'] = $order_info['cash_pay'];
				// $ret['retObj']['payment_list'] = array();
				$ret['retObj']['payment_list'] = json_decode('{}');
				
				//AARONFU 現金(刷卡)補差額支付項目
				$payment_list = $deposit->row_drid_list('Y', '', 'mall');
				
				if(!empty($payment_list) && $order_info['cash_pay'] > 0){
					foreach($payment_list as $k=>$v ){
						$drid = $v['drid'];
						$payment_arr[$drid] = $v;
					}
					
					if($order_info['creditcard']=='N' ){
						unset($payment_arr[31]);
					}
					
					$ret['retObj']['controller_type'] = 'exchange_pay';
					$ret['retObj']['payment_list'] = $payment_arr;
				}
			}
		}

		unset($ret['status']);
		//error_log("[exchange_ext/check_out] exchange OK:".json_encode($ret));
		
		return $ret;
	}
	
	// add by Thomas 2020/01/13 檢查用戶是否有異常下標紀錄(ex: 出了3次以上超過市價8折的價格 )
	// 有異常的下標紀錄, 可能是故意不得標要換成鯊魚點 & ibon
	public function chk_strange_bid_history($userid) 
	{
		global $db, $config;
		
		$query = "SELECT count(shid) cnt 
		               FROM `saja_shop`.`saja_strange_history` 
					  WHERE switch='Y' 
					    AND userid='${userid}' ";
		$table = $db->getQueryRecord($query);
		
		//error_log("[exchange_ext/chk_strange_bid_history] sql : ".$sql."-->".$table['table']['record'][0]['cnt']);
		$r=array("err"=>"");
		
		if(!empty($table['table']['record']) && $table['table']['record'][0]['cnt'] >=3 ) {
			$r['err'] = 8;
		}
        return $r;
	}
	
	//兌換密碼確認
	public function expw_check() 
	{
		global $db, $config;
		
		$time_start = microtime(true);
		
		$this->str = new convertString();
		$exchangepasswd = $this->str->strEncode($this->_v['expw'], $config['encode_key']);
		$userid = $this->_v ['userid']; //(empty($_POST['userid'])) ? $_SESSION['auth_id'] : $_POST['userid']; 
		$r['err'] = '';
		
		$query = "SELECT * FROM `saja_user`.`saja_user` 
		WHERE 
			prefixid = 'saja' 
			AND userid = '{$userid}' 
			AND exchangepasswd = '{$exchangepasswd}' 
			AND switch = 'Y' 
		LIMIT 1
		";
		
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			//'兌換密碼錯誤'
			$r['retCode']=-6;
			$r['retMsg']='兌換密碼錯誤 !!';				
			$r['err'] = 6;
		} else {
			$r['retCode']=1;
			$r['retMsg']='兌換密碼正確 !!';
			$r['err'] = 0;
		}
        return $r;
	}
	
	// 檢查點數是否有符合正常範圍
	public function deposit_bonus_check($userid) {
		
		global $db, $config;

		$query ="SELECT ifnull(SUM(amount),0) bonus 
		FROM `saja_cash_flow`.`saja_bonus`  
		WHERE 
			`prefixid` = 'saja' 
			AND `switch` = 'Y'
			AND `userid` = '{$userid}' 
			AND behav in ('product_close','system')
		";
		$table = $db->getQueryRecord($query); 
		error_log("[exchange_ext/deposit_bonus_check] bonus sql : ".$query."-->".$table['table']['record'][0]['bonus']);
		
		$query2 ="SELECT ifnull(SUM(amount),0) spoint 
		FROM `saja_cash_flow`.`saja_spoint`
		WHERE 
			`prefixid` = 'saja' 
			AND `switch` = 'Y'
			AND `userid` = '{$userid}' 
			AND behav in ('user_deposit','gift','system')
		";
		$table2 = $db->getQueryRecord($query2); 
		error_log("[exchange_ext/deposit_bonus_check] spoint sql : ".$query2."-->".$table2['table']['record'][0]['spoint']);
		
		$r=array("err"=>"");
		
		if($table['table']['record'][0]['bonus'] > $table2['table']['record'][0]['spoint']) {
		  $r['err'] =9;
		} 
		return $r;

	}
	
	// 檢查用戶今天兌換
	public function chk_order_history($userid, $orderdata) 
	{
		global $db, $config;
		$day = date("Y-m-d");
		
		$query = "SELECT SUM(num) as allnum, SUM(total_fee) as alltotal
		               FROM `saja_exchange`.`saja_order` 
					  WHERE switch='Y' 
						AND `type`='exchange'
						AND epid in (2419,2420,2421,2424,2427,2430,2431,2432,2435,2438,2441,2444,2445,2446)
					    AND userid='{$userid}' 
						AND insertt BETWEEN '{$day} 00:00:00' AND '{$day} 23:59:59' 
				";
		$table = $db->getQueryRecord($query);
		error_log("[lib/exchange_ext/chk_order_history] sql :".$query);
		error_log("[lib/exchange_ext/chk_order_history] orderdata num:".$orderdata['num']." total_fee ".$orderdata['total_fee']);
		error_log("[lib/exchange_ext/chk_order_history] allnum : ".($table['table']['record'][0]['allnum']+(int)$orderdata['num'])."  alltotal -->".($table['table']['record'][0]['alltotal']+$orderdata['total_fee']));
		
		$r=array("err"=>"");

		if(($table['table']['record'][0]['allnum']+(int)$orderdata['num']) >3 || ($table['table']['record'][0]['alltotal']+$orderdata['total_fee']) > 1000 ) {
			$r['err'] = 10;
		}
        return $r;
	}

}
?>
