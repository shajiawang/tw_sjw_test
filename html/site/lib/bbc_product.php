<?php
/*
*	殺品處理物件 bbc_product.php
*/

class bbc_product_lib
{
	//帶入參數
	public $_v = array();
	
	public function __construct()
	{
		global $config, $product;

	}

	//串接入口
	public function home()
	{
		global $config;

		//參數:
		$var = $this->_v;
		$userid = $this->_v['userid'];
		$check_var = true;
		$ret = true;

		return $ret;
	}

	//分類清單
	public function getProdCate()
	{
		global $config, $product;

		$ret = $this->ret;
		$_category = $product->jb_product_category();

		$ret['retCate'] = $_category;

		return $ret;
	}
	
	//取出商品清單
	public function getProdRow($pid, $field='*')
	{
		global $db, $config;

		if(empty($pid) ) {
			$ret['ProdInfo'] = array();

		} else {

			$query = "SELECT {$field} FROM `{$config['db_prefix']}_shop`.`{$config['default_prefix']}product`
				WHERE `switch`='Y' AND `productid`='{$pid}'
				LIMIT 1 ";
			error_log("[product_ext/getProdRow]: ". $query);
			$retArr = $db->getQueryRecord($query);

			if(!empty($retArr['table']['record'])) {

				$ret['ProdInfo'] = $retArr['table']['record'][0];
				//error_log("[product_ext/getProdRow]retArr: ". json_encode($r) );

				//每單位獎勵數值 //計算出會員本人的獎金比例
				if(!empty($ret['ProdInfo']["saja_fee"]) ){
					$ret['ProdInfo']['saja_fee'] = intval($ret['ProdInfo']["saja_fee"] * $this->ratio);
				}
				if(!empty($ret['ProdInfo']["bonus"]) ){
					$ret['ProdInfo']['bonus'] = intval($ret['ProdInfo']["bonus"] * $this->ratio);
				}

				if(!empty($ret['ProdInfo']["ads_fee"]) ){
					$ret['ProdInfo']['ads_fee'] = $ret['ProdInfo']["ads_fee"] * 1;
				}

				if(!empty($ret['ProdInfo']["ptid_url"]) ){
					$ret['ProdInfo']['ptid_url'] = IMG_URL.EVENT_IMGS_DIR ."/". $ret['ProdInfo']["ptid_url"];
				}

				if(!empty($ret['ProdInfo']["media_url"]) ){
					$ret['ProdInfo']['media_url'] = IMG_URL.EVENT_IMGS_DIR ."/". $ret['ProdInfo']["media_url"];
				}
			}
		}

		return $ret;
	}

	// 取出商品資訊
	public function getProdInfo($id='')
	{
		global $db, $config;

		$check_var = true;
		$ret = $this->ret;
		$ret['ProdInfo'] = array();

		if(empty($id) ) {
			$ret['retCode']=-1;
			$ret['retMsg']='PRODUCTID_IS_EMPTY';
			$check_var = false;
		}

		if($check_var){
			$query = "SELECT * FROM `{$config['db_prefix']}_shop`.`{$config['default_prefix']}product`
				WHERE `switch`='Y'
				AND `productid`='{$id}'
				LIMIT 1 ";
			$retArr = $db->getQueryRecord($query);

			if(!empty($retArr['table']['record'])) {
				$ret['ProdInfo']['productid'] = $retArr['table']['record'][0]["productid"];
				$ret['ProdInfo']['name'] = $retArr['table']['record'][0]["name"];
				$ret['ProdInfo']['title'] = $retArr['table']['record'][0]["description"];
				$ret['ProdInfo']['desc'] = $retArr['table']['record'][0]["description2"];
				$ret['ProdInfo']['rule'] = $retArr['table']['record'][0]["rule"];
				$ret['ProdInfo']['ads_fee'] = $retArr['table']['record'][0]["ads_fee"] * 1;

				$sajaFee = $retArr['table']['record'][0]["saja_fee"];
				$ret['ProdInfo']['saja_fee'] = $sajaFee;
				$ret['ProdInfo']['bonus'] = intval($sajaFee * $this->ratio);

				$ptid_url = $retArr['table']['record'][0]["ptid_url"];
				$ret['ProdInfo']['ptid_url'] = empty($ptid_url) ? '' : IMG_URL.EVENT_IMGS_DIR ."/". $ptid_url;

				$media_url = $retArr['table']['record'][0]["media_url"];
				$ret['ProdInfo']['media_url'] = empty($media_url) ? '' : IMG_URL.EVENT_IMGS_DIR ."/". $media_url;

				$ontime = strtotime($retArr['table']['record'][0]["ontime"]);
				$ret['ProdInfo']['ontime'] = date('H:i', $ontime);

				$offtime = strtotime($retArr['table']['record'][0]["offtime"]);
				$ret['ProdInfo']['offtime'] = date('H:i', $offtime);
			}
		}

		return $ret;
	}

}
?>
