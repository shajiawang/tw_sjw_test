<?php

class jpush {
    
    //待发送的应用程序(appKey)，只能填一个
    private $app_key = '121a5da92b8323619ddc0f55';
    //主密码
    private $master_secret ='78b1dfe2312ec82dee7a209b';
    //推送的Api地址
    private $url = "https://api.jpush.cn/v3/push";

    //若实例化的时候传入相应的值则按新的相应值进行
    public function __construct($app_key=null, $master_secret=null,$url=null) {
        if ($app_key) $this->app_key = $app_key;
        if ($master_secret) $this->master_secret = $master_secret;
        if ($url) $this->url = $url;
        date_default_timezone_set('Asia/Taipei');
        $this->datetime = date('Y-m-d H:i:s');
        error_log("[jpush] constructor : key:".$this->app_key.",secret:".$this->master_secret.",url:".$this->url);
    }


    /**
     * 极光推送
     * @param string $receiver   接收者的信息（all：所有用户；tag(20个)：Array标签组(并集)；tag_and(20个)：Array标签组(交集)；alias(1000)Array别名(并集)；registration_id(1000)注册ID设备标识(并集)）
     * @param string $content    推送的内容
     * @param unknown_type $m_type    推送附加字段的类型(可不填) http,tips,chat....
     * @param unknown_type $m_txt    推送附加字段的类型对应的内容(可不填) 可能是url,可能是一段文字
     * @param unknown_type $m_time    保存离线时间的秒数默认为一天(可不传)单位为秒
     * @return Ambigous <boolean, mixed>|boolean
     */
    public function push($receiver='all',$content='',$m_type='',$m_txt='',$m_time='86400'){
        $base64=base64_encode("$this->app_key:$this->master_secret");
        $header=array("Authorization:Basic $base64","Content-Type:application/json");
        $data = array();
        $data['platform'] = 'all';          //目标用户终端手机的平台类型android,ios,winphone
        $data['audience'] = $receiver;      //目标用户

        $data['notification'] = array(
            //统一的模式--标准模式
            "alert"=>$content,
            //安卓自定义
            "android"=>array(
                "alert"=>$content,
                "title"=>"",
                "builder_id"=>1,
                "extras"=>array("type"=>$m_type, "txt"=>$m_txt)
            ),
            //ios的自定义
            "ios"=>array(
                "alert"=>$content,
                "badge"=>"1",
                "sound"=>"default",
                "extras"=>array("type"=>$m_type, "txt"=>$m_txt)
            )
        );

        //苹果自定义---为了弹出值方便调测
        $data['message'] = array(
            "msg_content"=>$content,
            "extras"=>array("type"=>$m_type, "txt"=>$m_txt)
        );

        //附加选项
        $data['options'] = array(
            "sendno"=>time(),
            //保存离线时间的秒数默认为一天
            "time_to_live"=>$m_time,
            //布尔类型   指定 APNS 通知发送环境：0开发环境，1生产环境。或者传递false和true
            "apns_production"=>false,
        );
        
        $param = json_encode($data);
        $res = $this->push_curl($param,$header);

        if($res){             //得到返回值--成功已否后面判断
            return $res;
        }else{                //未得到返回值--返回失败
            return false;
        }
    }

    //推送的Curl方法
    public function push_curl($param="",$header="") {
        if (empty($param)) { return false; }
        $postUrl = $this->url;
        $curlPost = $param;
        $ch = curl_init();                                      //初始化curl
        curl_setopt($ch, CURLOPT_URL,$postUrl);                 //抓取指定网页
        curl_setopt($ch, CURLOPT_HEADER, 0);                    //设置header
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);            //要求结果为字符串且输出到屏幕上
        curl_setopt($ch, CURLOPT_POST, 1);                      //post提交方式
        curl_setopt($ch, CURLOPT_POSTFIELDS, $curlPost);
        curl_setopt($ch, CURLOPT_HTTPHEADER,$header);           // 增加 HTTP Header（头）里的字段
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);        // 终止从服务端进行验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        $data = curl_exec($ch);                                 //运行curl
        curl_close($ch);
        return $data;
    }
    
    /**
     * 调用推送方法
     * @param int $Aliasid        别名ID（设为用户userid）
     * @param string $Content    发送内容
     */
    public function send_jiguang($Aliasid='',$Content=''){
        if (empty($Aliasid)) {
            $jsondata = array(
                "remsg" 	=> "别名不能为空",
                "recode" 	=> -1
            );
        } elseif (empty($Content)) {
            $jsondata = array(
                "remsg" 	=> "发送内容不能为空",
                "recode" 	=> -1
            );
        } else {
            $alias = array(
                'alias' => array(
                    "$Aliasid"
                )
            );
            
            //推送附加字段的类型
            $m_type = 'https';
            
            //推送附加字段的类型对应的内容(可不填) 可能是url,可能是一段文字
            $m_txt = 'https://www.saja.com.tw/';
            
            //离线保留时间
            $m_time = 86400;
            
            //'all'
            $receive = $alias;
            
            //'这是一个测试的推送数据....测试....Hello World...'
            $content = $Content;
            
            //存储推送状态
            $message="";
            
            //调用极光推送函数
            $result = $this->push($receive,$content,$m_type,$m_txt,$m_time);
            if($result){
                $res_arr = json_decode($result, true);
                if(isset($res_arr['error'])){                   //如果返回了error则证明失败
                    //echo $res_arr['error']['message'];              //错误信息
                    //echo $res_arr['error']['code'];                 //显示错误码
                    $error_code=$res_arr['error']['code'];          //错误码
                    switch ($error_code) {
                        case 200:
                            $message= '发送成功！';
                            break;
                        case 1000:
                            $message= '失败(系统内部错误)';
                            break;
                        case 1001:
                            $message = '失败(只支持 HTTP Post 方法，不支持 Get 方法)';
                            break;
                        case 1002:
                            $message= '失败(缺少了必须的参数)';
                            break;
                        case 1003:
                            $message= '失败(参数值不合法)';
                            break;
                        case 1004:
                            $message= '失败(验证失败)';
                            break;
                        case 1005:
                            $message= '失败(消息体太大)';
                            break;
                        case 1008:
                            $message= '失败(appkey参数非法)';
                            break;
                        case 1011:
                            $message= '失败(没有满足条件的推送目标)';
                            break;
                        case 1020:
                            $message= '失败(只支持 HTTPS 请求)';
                            break;
                        case 1030:
                            $message= '失败(内部服务超时)';
                            break;
                        case 2002:
                            $message= '失败(API调用频率超出该应用的限制)';
                            break;
                        case 2003:
                            $message= '失败(该应用appkey已被限制调用 API)';
                            break;
                        case 2004:
                            $message= '失败(无权限执行当前操作)';
                            break;
                        case 2005:
                            $message= '失败(信息发送量超出合理范围)';
                            break;
                        default:
                            $message= '失败(返回其他状态，目前不清楚额，请联系开发人员！)';
                            break;
                    }
                    $jsondata = array(
                        "remsg" 	=> "$message",
                        "recode" 	=> "$error_code"
                    );
                }else{
                    $jsondata = array(
                        "remsg" 	=> "极光推送成功",
                        "recode" 	=> 1
                    );
                }
            }else{
                $jsondata = array(
                    "remsg" 	=> "接口调用失败或无响应",
                    "recode" 	=> -1
                );
            }
        }
        error_log("[send_jiguang] : ".json_encode($jsondata));
        // echo json_encode($jsondata);
        return json_encode($jsondata);
    }
}