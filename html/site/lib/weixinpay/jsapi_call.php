﻿<?php
    // Add By Thomas 150917 for data consistency check
	// 本程式會與微信溝通2次, 第1次會收到 $_GET 參數 : {"confirm":"3837_0.01","code":"031f7cea2032af9253c1b4271446ffdo","state":"SATE"}
	// 本段用於第1次被呼叫時, 防止呼叫的網頁被竄改金額和單號
	if(empty($_GET['confirm']) && empty($_GET['code'])) {
		include_once("../convertString.ini.php");
		include_once("../config.php");
		if(floatval($_POST['amount'])<0) {
		   echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Pay Amount Error !!");history.back();</script></body></html>';
		   exit;   
		}
		if(empty($_POST['chkStr'])) {
		   echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Empty Security Data !!");history.back();</script></body></html>';
		   exit;   
		}
		$cs=new convertString();
		$chkStr=$cs->strDecode($_POST['chkStr'],$config["encode_key"],$config["encode_type"]);
		error_log("[lib/weixinpay/jsapi_call] chkStr : ".$chkStr);
		error_log("[lib/weixinpay/jsapi_call] pay data : ".$_POST['orderid']."|".$_POST['amount']);
		$chkArr=explode("|",$chkStr);
		if(is_array($chkArr)) {
		   $orderid=$chkArr[0];
		   $amount=$chkArr[1];
		   
		   if(floatval($amount)!=floatval($_POST['amount'])) {
			   echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Total Fee Check Error !!");history.back();</script></body></html>';
			   exit;
		   } 
		   if($orderid!=$_POST['orderid']) {
			  echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Trade No. Check Failed !!");history.back();</script></body></html>';
			  exit;	
		   }
		} else {
		  echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Security Data Parse Error !!");history.back();</script></body></html>';
		   exit;
		}
	}
    // Add End 
?>
<?php
/**
 * JS_API支付demo
 * ====================================================
 * 在微信浏览器里面打开H5网页中执行JS调起支付。接口输入输出数据格式为JSON。
 * 成功调起支付需要三个步骤：
 * 步骤1：网页授权获取用户openid
 * 步骤2：使用统一支付接口，获取prepay_id
 * 步骤3：使用jsapi调起支付
*/

	include_once("./WxPayHelper/WxPayHelper.php");
	
	//使用jsapi接口
	$jsApi = new JsApi();
	
	//=========步骤1：网页授权获取用户openid============
	//通过code获得openid
	if (!isset($_GET['code']))
	{
		//触发微信返回code码
		$url = $jsApi->createOauthUrlForCode(WxPayConf::JS_API_CALL_URL."?confirm=".$_POST['orderid']."_".$_POST['amount']);
		//error_log("url: ".$url);
		header("Location: $url"); 
	}else
	{
		//获取code码，以获取openid
	    $code = $_GET['code'];
	    $confirm = explode("_", $_GET['confirm']);
	    //error_log("confirm: ".$confirm[1]);
		$jsApi->setCode($code);
		$openid = $jsApi->getOpenId();
	}
	
	//=========步骤2：使用统一支付接口，获取prepay_id============
	//使用统一支付接口
	$unifiedOrder = new UnifiedOrder();
	
	//设置统一支付接口参数
	//设置必填参数
	//appid已填,商户无需重复填写
	//mch_id已填,商户无需重复填写
	//noncestr已填,商户无需重复填写
	//spbill_create_ip已填,商户无需重复填写
	//sign已填,商户无需重复填写
	$unifiedOrder->setParameter("openid", $openid);						//openid
	$unifiedOrder->setParameter("body", "充值杀价币");					//商品描述
	$unifiedOrder->setParameter("out_trade_no", $confirm[0]);		    //商户订单号 
	$unifiedOrder->setParameter("total_fee", $confirm[1] * 100);		//总金额
	$unifiedOrder->setParameter("notify_url", WxPayConf::NOTIFY_URL);	//通知地址 
	$unifiedOrder->setParameter("trade_type", "JSAPI");					//交易类型
	//非必填参数，商户可根据实际情况选填
	//$unifiedOrder->setParameter("sub_mch_id","XXXX");					//子商户号  
	//$unifiedOrder->setParameter("device_info","XXXX");				//设备号 
	//$unifiedOrder->setParameter("attach","XXXX");						//附加数据 
	//$unifiedOrder->setParameter("time_start","XXXX");					//交易起始时间
	//$unifiedOrder->setParameter("time_expire","XXXX");				//交易结束时间 
	//$unifiedOrder->setParameter("goods_tag","XXXX");					//商品标记 
	//$unifiedOrder->setParameter("openid","XXXX");						//用户标识
	//$unifiedOrder->setParameter("product_id","XXXX");					//商品ID

	$prepay_id = $unifiedOrder->getPrepayId();
	//=========步骤3：使用jsapi调起支付============
	$jsApi->setPrepayId($prepay_id);

	$jsApiParameters = $jsApi->getParameters();
?>
<script type="text/javascript">
//调用微信JS api 支付
function jsApiCall()
{
	WeixinJSBridge.invoke(
		'getBrandWCPayRequest',
		<?php echo $jsApiParameters; ?>,
		function(res){
			// 返回res.err_msg,取值 
	        //get_brand_wcpay_request:ok  支付成功
	        //get_brand_wcpay_request:cancel 支付过程中用户取消
	        //get_brand_wcpay_request:fail 支付失败
	        WeixinJSBridge.log(res.err_msg);
	        if(res.err_msg == 'get_brand_wcpay_request:ok') {	//支付成功
	        	//alert("OK");
	        	//ajax
	        	
				var xmlHttp;
			    if (window.XMLHttpRequest) { // Mozilla, Safari, ...
					var xmlHttp = new XMLHttpRequest();
				}else if (window.ActiveXObject) { // IE
					var xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				}
				xmlHttp.open('POST', "/site/deposit/weixinpay_html/", true);
				xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				xmlHttp.onreadystatechange = function() {
					if (xmlHttp.readyState == 4) {
						<?php if (!$_POST['client']['json'] == 'Y')){ ?>
						document.getElementById("myDiv").innerHTML=xmlhttp.responseText;
						<?php }else{ ?>
						if (xmlHttp.responseText == 1) {
							alert('充值失败!');
							location.href="/site/member?channelid=1";
						}
						else if (xmlHttp.responseText == 2) {
							alert('已重覆充值!');
							location.href="/site/member?channelid=1";
						}
						else {
							var msg = '';
							if (xmlHttp.responseText == 200){ 
								alert('充值成功!');
								location.href="/site/member/";
							} 
						}
						<?php } ?>
					}
				}
				xmlHttp.send("orderid=<?php echo $confirm[0];?>&amount=<?php echo $confirm[1];?>&secretid=<?php echo md5($confirm[0].$confirm[1]);?>");
			    
			    //function updatepage(str){
			    	
				//	document.getElementById("result").innerHTML = "<font color='red' size='5'>" + str + "</font>";;
			    //}
			    
	        	
		        	/*$.post("http://test.shajiawang.com/site/deposit/weixinpay_html/?t="+getNowTime(), 
					{
                    	type:"ok", 
                    	orderid:"<?php echo $confirm[0]; ?>", 
                    	amount:"<?php echo $confirm[1]; ?>", 
                    	secretid:"<?php echo md5($confirm[0].$confirm[1]);?>"
                    }, 
					function(str_json) 
					{
						if(str_json) 
						{
							var json = $.parseJSON(str_json);
							console && console.log($.parseJSON(str_json));
							
							if (json.status == 1) {
								alert('充值失败!');
								location.href="/site/member?channelid=1";
							}
							else if (json.status == 2) {
								alert('已重覆充值!');
								location.href="/site/member?channelid=1";
							}
							else {
								var msg = '';
								if (json.status==200){ 
									alert('充值成功!');
									location.href="/site/member/";
								} 
							}
						}
					});*/

	        	//alert('<?php echo $confirm[0]; ?>');
	        	
	        }
	        else if(res.err_msg == 'get_brand_wcpay_request:fail') {	//支付失敗
	        	alert('微信支付失敗！');
	            location.href='/site/deposit/';
	        }
	        else if(res.err_msg == 'get_brand_wcpay_request:cancel') {	//取消支付
	        	alert('你已取消支付！');
	            location.href='/site/deposit/';
	        }
			
			//alert(res.err_code+res.err_desc+res.err_msg);
		}
	);
}

function callpay()
{
	if (typeof WeixinJSBridge == "undefined"){
	    if( document.addEventListener ){
	        document.addEventListener('WeixinJSBridgeReady', jsApiCall, false);
	    }else if (document.attachEvent){
	        document.attachEvent('WeixinJSBridgeReady', jsApiCall); 
	        document.attachEvent('onWeixinJSBridgeReady', jsApiCall);
	    }
	}else{
	    jsApiCall();
	}
}

window.onload = function() {
	callpay();
}
</script>
<div id="myDiv"></div>