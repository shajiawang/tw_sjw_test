﻿<?php
/**
* 	配置账号信息
    訂閱號
	define( "APP_ID" , 'wx1f6588911089fef0' );
    define( "APP_SECRET" , '72534391462697229643eb921652db94' );

    公眾號
    define( "APP_ID" , 'wxbd3bb123afa75e56' );
    define( "APP_SECRET" , '367c4259039bf38a65af9b93d92740ae' );	
*/

class WxPayConf
{
	//=======【基本信息设置】=====================================
	//微信公众号身份的唯一标识。审核通过后，在微信发送的邮件中查看
	const APPID = "wxbd3bb123afa75e56";
	//微信訂閱號
	// const APPID = "wx1f6588911089fef0";
	
	//受理商ID，身份标识
	const MCHID = "10011676";
	//商户支付密钥Key。审核通过后，在微信发送的邮件中查看
	const KEY = "8d806a7a258bab43991bdf637568a7e1";
	
	//JSAPI接口中获取openid，审核后在公众平台开启开发模式后可查看
	const APPSECRET = "367c4259039bf38a65af9b93d92740ae";
	//微信訂閱號
	//const APPSECRET = "72534391462697229643eb921652db94";
	
	//=======【JSAPI路径设置】===================================
	//获取access_token过程中的跳转uri，通过跳转将code传入jsapi支付页面
	const JS_API_CALL_URL = "http://test.shajiawang.com/site/lib/weixinpay/jsapi_call.php";
	
	//=======【证书路径设置】=====================================
	//证书路径,注意应该填写绝对路径
	const SSLCERT_PATH = "/var/www/html/site/lib/weixinpay/WxPayHelper/cacert/apiclient_cert.pem";
	const SSLKEY_PATH = "/var/www/html/site/lib/weixinpay/WxPayHelper/cacert/apiclient_key.pem";
	
	//=======【异步通知url设置】===================================
	//异步通知url，商户根据实际开发过程设定
	const NOTIFY_URL = "http://test.shajiawang.com/site/lib/weixinpay/notify_url.php";
}
	
?>