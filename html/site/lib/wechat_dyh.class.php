<?php

class Wechat_dyh
{
	public function createMenu($accessToken)//创建菜单
	{
		//$accessToken = $this->getAccessToken();	//获取access_token
		//构造POST给微信服务器的菜单结构体
		    
			$menuPostString = '{
				"button":[{
						"type":"view",
						"name":"我要杀价",
						"url":"http://test.shajiawang.com/site/user/weixinapi/"
					},
					/*
					{
						"name":"小珂首发",
						"sub_button":[
							  {
							   "type":"view",
							   "name":"即刻开杀",
							   "url":"http://test.shajiawang.com/site/product/today/"
							  },
							  {
							   "type":"view",
							   "name":"即刻兑换",
							   "url":"http://test.shajiawang.com/site/mall/exchange/?channelid=1&epcid=1&epid=501"
							  }
						]
					},
					*/
					{
						"type":"view",
						"name":"充值杀币",
						"url":"http://test.shajiawang.com/site/deposit/"
					}]
			}';
					
			$menuPostString = '{
				"button":[
				    {
						"name":"杀价王是啥",
						"sub_button":[
							  {
							   "type":"view",
							   "name":"《央视报导》",
							   "url":"http://mp.weixin.qq.com/s?__biz=MzA3MTg5NjExOA==&mid=207613878&idx=1&sn=f1110b92bca8ab9f494560cc24ef8f5c#rd"
							  },
							 
							  {
							   "type":"view",
							   "name":"《成交案例》",
							   "url":"http://mp.weixin.qq.com/s?__biz=MzA3MTg5NjExOA==&mid=207642417&idx=1&sn=d91fbf58205cb6149ce74ab88be86d21#rd"
							  },
							   {
							   "type":"view",
							   "name":"《快速入门》",
							   "url":"http://mp.weixin.qq.com/s?__biz=MzA3MTg5NjExOA==&mid=207563108&idx=1&sn=a6fcae0fc2ab27d77da6088b9ebd09fe#rd"
							  }
						]
					},
					{
						"name":"我要杀价",
						"sub_button": [
						      {
							     "type":"view",
								 "name":"《杀价列表》",
							     "url":"http://test.shajiawang.com/wx_dyh_auth.php?jdata=gotourl:/site/product/"
							  },
							  { 
							      "type":"view",
								  "name":"《我要杀房》",
							      "url":"http://test.shajiawang.com/wx_dyh_auth.php?jdata=gotourl:/site/product/saja/?productid=2854"
							  }
						      
						]
					},
					{
						"name":"热门活动",
						"sub_button":[
							  {
							   "type":"view",
							   "name":"《万人杀房活动》",
							   "url":"http://test.shajiawang.com/wx_dyh_auth.php?jdata=gotourl:/evt/2854.php"
							  },
							  {
							   "type":"click",
							   "name":"《激活码》",
                               "key":"house_passphrase"							  
							  },
							  {
							   "type":"click",
							   "name":"《邀请好友得免费杀房券》",
							   "key":"createQrCode"
							  },
							  {
							   "type":"click",
							   "name":"《登入測試》",
							   "key":"weixin_dyh_login"
							  }
						]
					}]
			}';
			// "url":"http://test.shajiawang.com/site/phpqrcode/?data=http://test.shajiawang.com/site/user/weixinapi/?u=MIJdhdBnnA/GftGaRmxOmw=="
		$menuPostUrl = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=".$accessToken;//POST的url
		$menu = dataPost($menuPostString, $menuPostUrl);//将菜单结构体POST给微信服务器
		error_log($menu);
		return $menu;
	}
	
	public function deleteMenu($accessToken)//创建菜单
	{
		//$accessToken = $this->getAccessToken();//获取access_token
		$menuPostUrl = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=".$accessToken;//POST的url
		error_log(getCurl($menuPostUrl));
		
		// $menu = dataPost($menuPostString, $menuPostUrl);//将菜单结构体POST给微信服务器
	}
}

function weixin_app_data($config, $model) {
	$query = "select * from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}weixin2_settings` 
			where 
			prefixid = '".$config['default_prefix_id']."' 
			and switch = 'Y'
			";
	$table = $model->getQueryRecord($query);
	
	return $table;
}

function weixin_access_token($config, $model) {
	
	// 从缓存中获取access_token
	$redis = new Redis();
	$redis->connect('127.0.0.1');
	$accessToken=$redis->get("WX_DYH_TK");
	$expired_ts=$redis->get("WX_DYH_TK_EXPIRED_TS");
	error_log("[wechat] WX_DYH_TK:".$accessToken);
	error_log("[wechat] WX_DYH_TK_EXPIRED_TS:".$expired_ts);
		
	if(!empty($accessToken)) {
		$now=time();
		if($now<$expired_ts) {
		   error_log("[wechat_dyh] now :".$now."<==> exp :".$expired_ts);
		   error_log("[wechat_dyh] got token from redis : ".$accessToken);
		   return $accessToken;
		}
	}

	$query = "SELECT * FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}weixin2_settings` 
		WHERE 
			prefixid = '".$config['default_prefix_id']."' 
			AND switch = 'Y' 
			and name = 'access_token'
			and (ontime <= NOW() && ontime != '0000-00-00 00:00:00') 
			and (offtime > NOW() && offtime != '0000-00-00 00:00:00') 
	";
	$table = $model->getQueryRecord($query);
	
	if ($table['table']['record'][0]['code']) {
		$accessToken = $table['table']['record'][0]['code'];
		$redis->set("WX_DYH_TK",$accessToken);
		$redis->set("WX_DYH_TK_EXPIRED_TS",strtotime($table['table']['record'][0]['offtime']));
	    
	}
	else {
	    
		$appdata_array = weixin_app_data($config, $model);
		foreach ($appdata_array['table']['record'] as $key => $value)
		{
			if ($value['name'] == "APP_ID") {
				$APP_ID = $value['code'];
			}
			if ($value['name'] == "APP_SECRET") {
				$APP_SECRET = $value['code'];
			}
		}
		
		/*
		AppID(应用ID)wx1f6588911089fef0
        AppSecret(应用密钥)72534391462697229643eb921652db94 
        $APP_ID = 'wx1f6588911089fef0';
		$APP_SECRET='72534391462697229643eb921652db94';
		*/
		$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=".$APP_ID."&secret=".$APP_SECRET;
		error_log("[weixin_access_token(dyh)] url : ".$url);
		$data = getCurl($url);					//通过自定义函数getCurl得到https的内容
		$resultArr = json_decode($data, true);	//转为数组
		
		$accessToken = $resultArr['access_token'];
		$expired_ts=strtotime('+7140 seconds');
		$TwoHours = date("Y-m-d H:i:s", $expired_ts);
		
		//刪除access_token
	    $query = "delete from `".$config['db'][2]['dbname']."`.`".$config['default_prefix']."weixin2_settings` where name = 'access_token'";
	    error_log($query);
		$model->query($query);
	    
	    //建立access_token
	    $query = "INSERT INTO `".$config['db'][2]['dbname']."`.`".$config['default_prefix']."weixin2_settings` ( 
					  `prefixid`,
					  `name`,
					  `code`, 
					  `ontime`,
					  `offtime`, 
					  `seq`,
					  `switch`, 
					  `insertt`
			      )
				  VALUES (
					  '{$config['default_prefix_id']}',
					  'access_token',
					  '{$accessToken}', 
					  NOW(),
					  '{$TwoHours}', 
					  '0',
					  'Y', 
					  NOW()
			      )"; 
		// error_log($query);		  
		$model->query($query);
		
		// 寫入redis
		$redis->set("WX_DYH_TK",$accessToken);
		$redis->set("WX_DYH_TK_EXPIRED_TS",$expired_ts);
	}
	return $accessToken;		//获取access_token
}

function getCurl($url) {//get https的内容
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);//不输出内容
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	$result =  curl_exec($ch);
	curl_close ($ch);
	return $result;
}

function dataPost($post_string, $url) {//POST方式提交数据
	$context = array ('http' => array ('method' => "POST", 'header' => "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) \r\n Accept: */*", 'content' => $post_string ) );
	$stream_context = stream_context_create ( $context );
	$data = file_get_contents( $url, FALSE, $stream_context );
	return $data;
}

?>
