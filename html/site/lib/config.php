<?php
/**
 * Configuration file
 * This file specifies all of the base values used throughout the app.
*/
$debug_mod   = "Y";
$host_name   = "sajadb01:3306";
$host2_name   = "sajadb02:3306";
$protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
$domain_name = 'test.shajiawang.com';
$img_domain_name = 'test.shajiawang.com';
$ofpay_gw_url = 'gw.pay.liantiao.ofcard.com/gateway/notify/ZDY_SJW_WAP/63';
$uppay_gw_url = 'www.uptogo.com.tw/payment/saja';
$wypay_gw_url = 'cn.womantreasure.com/modules/saja/doFictitiousDetonate.php';
define("BASE_URL",$protocol."://".$domain_name);
define("IMG_URL","http://". $img_domain_name);
define("COOKIE_DOMAIN", $domain_name);
define("OFPAY_GW_URL","http://". $ofpay_gw_url);
define("UPPAY_GW_URL","https://". $uppay_gw_url);
define("WYPAY_GW_URL","http://". $wypay_gw_url);
// Define global Variables.
define("APP_NAME", "杀价王");
define("APP_DESCRIPTION", "欢迎来到杀价王，全球首创杀价式拍卖营销平台");
define("APP_KEYWORDS", "杀价王, 杀价式拍卖, 竞标网站");

define("CACHE_ENABLE", true);
//define("APP_DIR", "/jm_devgrow");
//define("BASE_DIR", "D:\AppServ\www\jm_devgrow"); //dirname(dirname(__FILE__))
//define("LIB_DIR", "D:\AppServ\www\jm_devgrow\lib"); //dirname(__FILE__)
define("APP_DIR", "/site");
define("IMG_DIR", APP_DIR."/static/img");
define("IMAGES_DIR", APP_DIR."/images");
define("CSS_DIR",  APP_DIR."/static/css");
define("JS_DIR",  APP_DIR."/static/js");
define("AUDIO_DIR",  APP_DIR."/static/audio");
define("HTML_DIR",  APP_DIR."/static/html");
define("BASE_DIR", "/var/www/html/site");
define("LIB_DIR", "/var/www/html/site/lib");
define("HTML_FILE_DIR",  BASE_DIR."/static/html");
define("HEADIMGS_DIR",  IMAGES_DIR."/headimgs");
define("MEDALIMGS_DIR",  IMAGES_DIR."/medals");
define("ISSUESIMGS_DIR",  IMAGES_DIR."/site/issues");

// Set the default controller hte user is directed to (aka homepage).
define('ROUTER_DEFAULT_CONTROLLER', 'site');
define('ROUTER_DEFAULT_ACTION', 'home');

// redis connect 
//define('REDIS_SERVER','127.0.0.1');
//define('REDIS_PORT',6379);

define('UNIX_TS_URL','http://test.shajiawang.com/site/lib/cdntime.php');
define('OAUTH_HOST','https://ws.saja.com.tw');
// The following controllers/actions will not be cached:
$do_not_cache = array("user","");


############################################################################
# defient
############################################################################
$config["project_id"]             = "saja";
$config["default_main"]           = "/site";
$config["default_charset"]        = "UTF-8";
$config["sql_charset"]            = "utf8";
$config["default_prefix"]         = "saja_" ;  			// prefix
$config["default_prefix_id"]      = "saja" ;		// prefixid
$config["cookie_path"]            = "/";
$config["cookie_domain"]          = "";
$config["tpl_type"]               = "php";
$config["module_type"]            = "php";	  		// or xml
$config["fix_time_zone"]          = -8 ;                          // modify time zone
$config["default_time_zone"]      = 8 ;                           // default time zone
$config["max_page"]               = 20 ;
$config["max_range"]              = 10 ;
$config["encode_type"]            = "crypt" ; 			// crypt or md5
//$config["encode_key"]             = "%^$#@%S_d_+!" ; 			// crypt encode key
$config["encode_key"]             = "%^$#@%S_d_+!J_j=" ; 			// crypt encode key
$config["session_time"]           = 15	; 			// Session time out
$config["default_lang"]           = "zh_TW"	; 			// en , tc
$config["default_template"]       = "default"	; 		//
$config["default_topn"]           = 10	; 			// default top n
$config["debug_mod"]              = $debug_mod ; 			// enable all the debug console , N : disable , Y : enable
$config["max_upload_file_size"]   = 800000000 ; 			//unit is k
$config["expire"]                 = "60" ; 			// memcace expire time . sec
$config["domain_name"]            = $domain_name;
$config["protocol"]               = $protocol;
$config["admin_email"]            = array('pc028771@gmail.com') ;
$config["currency_email"]         = array('pc028771@gmail.com') ;
$config["transaction_time_limit"] = 5; //The minimized time between two transaction

$config["firstbank_companyid"]	  = "10000";
$config["firstbank_kindnum"]      = "9";
$config["firstbank_kindnum_mall"] = "8";

############################################################################
# FaceBook
############################################################################
$config['fb']['appid' ]  = "";
$config['fb']['secret']  = "";


############################################################################
# path
############################################################################
$config["path_site"]            = "/var/www/html/site"; //phpmodule
$config["path_class"]           = $config["path_site"]."/phpmodule/class" ;
$config["path_function"]        = $config["path_site"]."/phpmodule/function" ;
$config["path_include"]         = $config["path_site"]."/phpmodule/include" ;
$config["path_bin"]             = $config["path_site"]."/phpmodule/bin" ;
$config["path_data"]            = $config["path_site"]."/phpmodule/data/" ;
$config["path_cache"]           = $config["path_site"]."/phpmodule/data/cache" ;
$config["path_sources"]         = $config["path_site"]."/phpmodule/sources/".$config["module_type"] ;
$config["path_style"]           = $config["path_site"]."/phpmodule/template/".$config["tpl_type"] ;
$config["path_language"]        = $config["path_site"]."/phpmodule/language" ;
$config["path_image"]           = $config["default_main"]."/images".$config["default_main"] ;
$config["path_images"]          = dirname($config["path_site"])."/images".$config["default_main"] ;
$config["path_products_images"] = dirname(dirname($config["path_site"]))."/images/products" ;
$config["path_admin"]           = $config["path_site"]."/admin/" ;
$config["path_javascript"]      = $config["path_site"]."/javascript/" ;
$config["path_pdf_template"]    = $config["path_class"]."/tcpdf/template/" ;
$config["path_eamil_api"]       = "/usr/bin/" ;  // email api path
$config["prod_le"]				= "1";
############################################################################
# sql Shaun
############################################################################
$config["db"][0]["charset"]  = "utf8" ;
$config["db"][0]["host"]     = $host_name ;
$config["db"][0]["type"]     = "mysql";
$config["db"][0]["username"] = "saja";
$config["db"][0]["password"] = "saja#333" ;
// $config["db"][0]["password"] = "p%C2%0C%FA%06%A5%D8%E8%25z%14j%CB%3D%89%8A%F3-%AC%85%ACB%CF%E6%97%0B%8B5%D9%B3%0A%8F" ;
#$config["db_port"][0]       = "/var/lib/mysql/mysql.sock";

$config["db"][0]["dbname"]   = "saja_user" ;
$config["db"][1]["dbname"]   = "saja_cash_flow" ;
$config["db"][2]["dbname"]   = "saja_channel" ;
$config["db"][3]["dbname"]   = "saja_exchange" ;
$config["db"][4]["dbname"]   = "saja_shop" ;
$config["db"][5]["dbname"]   = "saja_view" ;											

############################################################################
# sql Shaun 2
############################################################################
$config["db2"][0]["charset"]  = "utf8" ;
$config["db2"][0]["host"]     = $host2_name ;
$config["db2"][0]["type"]     = "mysql";
$config["db2"][0]["username"] = "saja";
$config["db2"][0]["password"] = "saja#333" ;
// $config["db2"][0]["password"] = "p%C2%0C%FA%06%A5%D8%E8%25z%14j%CB%3D%89%8A%F3-%AC%85%ACB%CF%E6%97%0B%8B5%D9%B3%0A%8F" ;

$config["db2"][0]["dbname"]   = "saja_user" ;
$config["db2"][1]["dbname"]   = "saja_cash_flow" ;
$config["db2"][2]["dbname"]   = "saja_channel" ;
$config["db2"][3]["dbname"]   = "saja_exchange" ;
$config["db2"][4]["dbname"]   = "saja_shop" ;


############################################################################
# sql cluster Shaun
############################################################################
$config["dbc0"][0]["charset"]  = "utf8mb4" ;
$config["dbc0"][0]["host"]     = "dbc0";
$config["dbc0"][0]["type"]     = "mariadb";
$config["dbc0"][0]["username"] = "saja";
// $config["dbc0"][0]["password"] = "p%C2%0C%FA%06%A5%D8%E8%25z%14j%CB%3D%89%8A%F3-%AC%85%ACB%CF%E6%97%0B%8B5%D9%B3%0A%8F" ;
$config["dbc0"][0]["password"] = "saja#333" ;

$config["dbc0"][0]["dbname"]   = "saja_user" ;
$config["dbc0"][1]["dbname"]   = "saja_cash_flow" ;
$config["dbc0"][2]["dbname"]   = "saja_channel" ;
$config["dbc0"][3]["dbname"]   = "saja_exchange" ;
$config["dbc0"][4]["dbname"]   = "saja_shop" ;

############################################################################
# sql cluster Shaun
############################################################################
$config["dbc1"][0]["charset"]  = "utf8mb4" ;
$config["dbc1"][0]["host"]     = "dbc1";
$config["dbc1"][0]["type"]     = "mariadb";
$config["dbc1"][0]["username"] = "saja";
$config["dbc1"][0]["password"] = "saja#333" ;
//$config["dbc1"][0]["password"] = "p%C2%0C%FA%06%A5%D8%E8%25z%14j%CB%3D%89%8A%F3-%AC%85%ACB%CF%E6%97%0B%8B5%D9%B3%0A%8F" ;

$config["dbc1"][0]["dbname"]   = "saja_user" ;
$config["dbc1"][1]["dbname"]   = "saja_cash_flow" ;
$config["dbc1"][2]["dbname"]   = "saja_channel" ;
$config["dbc1"][3]["dbname"]   = "saja_exchange" ;
$config["dbc1"][4]["dbname"]   = "saja_shop" ;

############################################################################
# sql cluster Shaun
############################################################################
$config["dbc2"][0]["charset"]  = "utf8mb4" ;
$config["dbc2"][0]["host"]     = "dbc2";
$config["dbc2"][0]["type"]     = "mariadb";
$config["dbc2"][0]["username"] = "saja";
// $config["dbc2"][0]["password"] = "p%C2%0C%FA%06%A5%D8%E8%25z%14j%CB%3D%89%8A%F3-%AC%85%ACB%CF%E6%97%0B%8B5%D9%B3%0A%8F" ;
$config["dbc2"][0]["password"] = "saja#333" ;

$config["dbc2"][0]["dbname"]   = "saja_user" ;
$config["dbc2"][1]["dbname"]   = "saja_cash_flow" ;
$config["dbc2"][2]["dbname"]   = "saja_channel" ;
$config["dbc2"][3]["dbname"]   = "saja_exchange" ;
$config["dbc2"][4]["dbname"]   = "saja_shop" ;

############################################################################
# sql cluster Shaun
############################################################################
$config["dbc3"][0]["charset"]  = "utf8mb4" ;
$config["dbc3"][0]["host"]     = "dbc3";
$config["dbc3"][0]["type"]     = "mariadb";
$config["dbc3"][0]["username"] = "saja";
//$config["dbc3"][0]["password"] = "p%C2%0C%FA%06%A5%D8%E8%25z%14j%CB%3D%89%8A%F3-%AC%85%ACB%CF%E6%97%0B%8B5%D9%B3%0A%8F" ;
$config["dbc3"][0]["password"] = "saja#333" ;

$config["dbc3"][0]["dbname"]   = "saja_user" ;
$config["dbc3"][1]["dbname"]   = "saja_cash_flow" ;
$config["dbc3"][2]["dbname"]   = "saja_channel" ;
$config["dbc3"][3]["dbname"]   = "saja_exchange" ;
$config["dbc3"][4]["dbname"]   = "saja_shop" ;

############################################################################
# memcache
############################################################################
$config['memcache']['server_list']['session']['ip']   = 'localhost';
$config['memcache']['server_list']['session']['port'] = 11211;
$config['memcache']['MEM_PERSISTENT']                 = true;
$config['memcache']['MEM_TIMEOUT']                    = 1;
$config['memcache']['MEM_RETRY_INTERVAL']             = 1;
$config['memcache']['MEM_STATUS']                     = 1;
$config['memcache']['MEM_WEIGHT']                     = 1000;

############################################################################
# Payment
############################################################################
$config['payment_arr'] = array('alipay', 
					'bankcomm',
					'weixinpay',
					'ibonpay',
					'hinet',
					'alading',
					'creditcard',
					'payment',
					'taiwanpay' );

$config['alipay']['merchantnumber'] = '456025';
$config['alipay']['code']           = 'abcd1234';
$config['alipay']['paymenttype']    = 'ALIPAY_WAP'; //'ALIPAY'; //
$config['alipay']['url_payment']    = 'http://testmaple2.neweb.com.tw/CashSystemFrontEnd/Payment';
$config['alipay']['url_query']      = 'http://testmaple2.neweb.com.tw/CashSystemFrontEnd/Query';
$config['alipay']['nexturl']		= BASE_URL . APP_DIR .'/deposit/alipay_complete/';
//$config['alipay']['htmlurl']		= BASE_URL . APP_DIR .'/deposit/alipay_html/';
$config['alipay']['htmlurl']		= BASE_URL . APP_DIR .'/lib/alipay/alipayapi.php';
$config['alipay']['sellemail']		= "alpay@shajiawang.com";

$config['bankcomm']['htmlurl']		= APP_DIR .'/lib/bankcomm/merchant.php';
$config['bankcomm']['interfaceVersion'] = '1.0.0.0';			//銀聯支付版本
$config['bankcomm']['merchID']		= '301310053119880';		//商戶號
$config['bankcomm']['tranCode']		= 'cb2200_sign';			//
$config['bankcomm']['tranType']		= '0';						//0:B2C
$config['bankcomm']['curType']		= 'CNY';					//交易幣種
$config['bankcomm']['socket_ip']	= "127.0.0.1";				//socket服务ip
$config['bankcomm']['socket_port']	= "8080";					//socket服务端口
$config['bankcomm']['paymenttype']  = 'BANKCOMM_WAP'; 			//'BANKCOMM'; //
$config['bankcomm']['goodsURL']		= BASE_URL . APP_DIR .'/deposit/bankcomm_html/';
$config['bankcomm']['merURL']		= BASE_URL . APP_DIR .'/deposit/bankcomm_active_notify/';

$config['weixinpay']['htmlurl']		= BASE_URL . APP_DIR .'/lib/weixinpay/jsapi_call.php';
$config['weixinpay']['paymenttype'] = 'WEIXIN_WAP'; 			//'WEIXIN'; //

$config['ibonpay']['htmlurl'] = BASE_URL . APP_DIR."/deposit/ibonpay/";
$config['ibonpay']['merchantnumber'] = "456025";       //商家編號
$config['ibonpay']['paymenttype']   = "MMK";
$config['ibonpay']['code'] = "gcxa3him";              //藍新認證碼
$config['ibonpay']['code2'] = "sajo498";              //殺價認證
$config['ibonpay']['url_payment'] = "https://aquarius.neweb.com.tw:80/CashSystemFrontEnd/Payment";
$config['ibonpay']['nexturl'] = BASE_URL . APP_DIR ."/deposit/ibonpay_success/";   // callback URL (似乎沒用??)

$config['hinet']['htmlurl'] = BASE_URL . APP_DIR."/deposit/hinetpts_exc/";
$config['hinet']['paymenttype']="HINET_PTS";

$config['alading']['htmlurl'] = BASE_URL . APP_DIR."/deposit/alading_exc/";
$config['alading']['paymenttype']="ALADING_PTS";

//紅陽
$config['creditcard']['merchantnumber_test'] = "S1411179011"; 									//web測試 商家編號
$config['creditcard']['merchantnumber_app_test'] = "S1808019010"; 								//app測試 商家編號
$config['creditcard']['url_payment_test']="https://test.esafe.com.tw/Service/Etopm.aspx";		//測試 串接路徑
$config['creditcard']['merchantnumber'] = "S1411280405"; 										//web 商家編號
$config['creditcard']['merchantnumber_app'] = "S1411280413"; 									//app 商家編號
$config['creditcard']['url_payment']="https://www.esafe.com.tw/Service/Etopm.aspx";				//串接路徑
$config['creditcard']['code']="gcxa3him";														//交易密碼
$config['creditcard']['Card_Type']="2";															//交易類別 0：信用卡交易 1：銀聯卡交易
$config['creditcard']['paymenttype']="creditcard";
$config['creditcard']['Term']="0";																//web,android分期期數	
$config['creditcard']['Percent']="0";															//信用卡手續費(百分比)
$config['creditcard']['htmlurl']= BASE_URL . APP_DIR ."/deposit/twcreditcard_pay/";				//結果回傳路徑	
$config['creditcard']['RSTransactionInstallmentTermValue']="RSTransactionInstallmentTermNone";
$config['creditcard']['paymentURL']="https://www.esafe.com.tw/serviceAPP/Api/Passcode_Request.ashx";
$config['creditcard']['paymentURL_test']="https://test.esafe.com.tw/serviceAPP/Api/Passcode_Request.ashx";  
$config['creditcard']['key'] = 'Tqwco7J5VaROzBa2XcU9IJtsfuMkjUInum4F13w5R34='; 					//金鑰
$config['creditcard']['encIV'] = 'QQIu4oGpY+8M04C0e0yGuA=='; 									//IV 	
//ios分期期數 RSTransactionInstallmentTermNone：不分期 
//RSTransactionInstallmentTerm3：3期 
// RSTransactionInstallmentTerm6：6期 
// RSTransactionInstallmentTerm12：12期 
// RSTransactionInstallmentTerm18：18期 
// RSTransactionInstallmentTerm24：24期

//綠界
$config['creditcard']['ServiceURL'] = "https://payment-stage.ecpay.com.tw/Cashier/AioCheckOut/V5";           //服務位置
$config['creditcard']['HashKey'] = '5294y06JbISpM5x9' ;                                                      //測試用Hashkey，請自行帶入ECPay提供的HashKey
$config['creditcard']['HashIV'] = 'v77hoKGq4kWxNNIS' ;                                                       //測試用HashIV，請自行帶入ECPay提供的HashIV
$config['creditcard']['MerchantID'] = '2000132';                                                             //測試用MerchantID，請自行帶入ECPay提供的MerchantID
$config['creditcard']['EncryptType'] = '1';                                                                  //CheckMacValue加密類型，請固定填入1，使用SHA256加密
$config['creditcard']['ReturnURL'] = 'http://test.shajiawang.com/site/deposit/eccreditcard_pay_result';      //付款完成通知回傳的網址
$config['creditcard']['ClientBackURL'] = 'http://test.shajiawang.com/site/deposit/eccreditcard_pay_result';  //Client端返回特店的按鈕連結
$config['creditcard']['OrderResultURL'] = 'http://test.shajiawang.com/site/deposit/eccreditcard_pay_result';  //Client端返回特店的按鈕連結
$config['creditcard']['CreditInstallment'] = '' ;                                                            //分期期數，預設0(不分期)，信用卡分期可用參數為:3,6,12,18,24
$config['creditcard']['InstallmentAmount'] = 0 ;                                                             //使用刷卡分期的付款金額，預設0(不分期)
$config['creditcard']['Redeem'] = false ;                                                                    //是否使用紅利折抵，預設false
$config['creditcard']['UnionPay'] = false;                                                                   //是否為聯營卡，預設false;

$config['payment']['merchantnumber_test'] = "S1411179037"; 									//測試 商家編號
$config['payment']['merchantnumber_app_test'] = "S1411179037"; 									//測試 商家編號
$config['payment']['url_payment_test']="https://test.esafe.com.tw/Service/Etopm.aspx";		//測試 串接路徑
$config['payment']['merchantnumber'] = "S1812260311"; 										//web 商家編號
$config['payment']['merchantnumber_app'] = "S1812260311"; 										//web 商家編號
$config['payment']['url_payment']="https://www.esafe.com.tw/Service/Etopm.aspx";				//串接路徑
$config['payment']['code']="gcxa3him";														//交易密碼
$config['payment']['htmlurl']= BASE_URL . APP_DIR ."/deposit/payment24_pay/";					//結果回傳路徑	
$config['payment']['paymenttype']="payment";

$config['taiwanpay']['htmlurl']		= BASE_URL . APP_DIR .'/deposit/taiwanpay/'; //台灣 Pay 支付

###########country#############
/*
$config['country'] = 2; 		// default country id (中國)
$config['region'] = 3;          // default region id (華東)
$config['province'] = 2;		// default province id (上海)
//$config['province'] = 8;		// default province id (南京)
$config['channel'] = 1;         // default channel id (浦東)
//$config['channel'] = 10;         // default channel id (南京)
*/

$config['country'] = 1; 		// default country id (台灣)
$config['region'] = 8;          // default region id (台灣)
$config['province'] = 1;		// default province id (台灣)
$config['channel'] = 3;         // default channel id (台北)

###########currency#############
$config['sjb_rate'] = 1; 		//當地貨幣與殺價幣兌換比率
$config['currency'] = "NTD";	//幣別
$config['test_id']=Array(
       '1'=>'Y',
       '28'=>'Y',
       '9'=>'Y',
       '116' => 'Y',
       '586'=>'Y',
       '597'=>'Y',
       '785'=>'Y'
);

define("PASSWORD_SALT", $config["encode_key"]);

####### socket ########
#$config['wss_host']='ws.shajiawang.com';
#$config['wss_port']='443';
#$config['wss_url']='ws://'.$config['wss_host'].':'.$config['wss_port'].'/';
$config['wss_host']='ws.saja.com.tw';
$config['wss_port']='3334';
$config['wss_url']='wss://'.$config['wss_host'].':'.$config['wss_port'].'/';

###########invoice#############
$config['invoice']['key']='70D2ABE2F6EE45B0C474C88969FB08E2';
$config['invoice']['BuyerId']='00000000';																		// 買方統編
$config['invoice']['SellerId']='25089889';																		// 賣方統編
$config['invoice']['SellerName'] = '殺價王股份有限公司'; 														// 賣方名稱
$config['invoice']['SellerAddress'] = '台北市'; 																// 賣方地址
$config['invoice']['SellerTelephoneNumber'] = '0225501058'; 													// 賣方電話
$config['invoice']['CarrierType']='EG0544';                                                                     // 預設會員載具類型   
$config['invoice']['appId'] ='EINV4202004216892';
$config['invoice']['apikey'] = 'dUlUcUh2djR0ZWlaT1JHbA==';
########### add By Thomas 20190422 #############
#DB member的數目, 1代表沒有cluster
$config['num_db_member']=1;

// 使用redis 的SortedSet功能計算順位
$config["rank_bid_by_redis"]           = true;

// password to connect to redis
$config["rank_bid_redis_auth"]          = "Sjw#333"; 

// 鯊魚點收支敘述
$config['deposit_status_desc']['prod_sell']='得標商品折算';
$config['deposit_status_desc']['bid_by_saja']='得標送殺價幣';
$config['deposit_status_desc']['user_deposit']='儲值成功';
$config['deposit_status_desc']['gift']='贈送';
$config['deposit_status_desc']['bid_refund']='流標退款';
$config['deposit_status_desc']['sajabonus']='鯊魚點兌換';
$config['deposit_status_desc']['feedback']='(推薦)回饋';
$config['deposit_status_desc']['process_fee']='得標處理費';
$config['deposit_status_desc']['user_saja']='下標';
$config['deposit_status_desc']['prod_buy']='購買商品';
$config['deposit_status_desc']['system']='系統轉入';
$config['deposit_status_desc']['system_test']='系統測試';
$config['deposit_status_desc']['others']='其他';

// 簡訊王發送失敗敘述
$config['kmsgid']['-60014']="該門號設定了拒收簡訊，導致發送失敗，請跟您的電信公司洽詢，或改洽客服人工驗證開通。";
$config['kmsgid']['-1000']="發送內容違反NCC規範";
$config['kmsgid']['-1']="簡訊服務商系統異常";
$config['kmsgid']['-2']="授權錯誤(帳號/密碼錯誤)";
$config['kmsgid']['-4']="違反規則 發送端 870短碼VCSN 設定異常";
$config['kmsgid']['-5']="違反規則 接收端 門號錯誤";
$config['kmsgid']['-6']="接收端的門號停話異常";
$config['kmsgid']['-20']="預約時間錯誤 或時間已過";
$config['kmsgid']['-21']="有效時間錯誤";
$config['kmsgid']['-59999']="帳務異常 簡訊無法扣款";
$config['kmsgid']['-60002']="點數不足 無法發送簡訊";
$config['kmsgid']['-999949999']="境外IP限制(只接受台灣IP發送)";
$config['kmsgid']['-999959999']="12 小時內，相同容錯機制碼";
$config['kmsgid']['-999969999']="同秒, 同門號, 同內容簡訊";
$config['kmsgid']['-999979999']="鎖定來源IP";
$config['kmsgid']['-999989999']="簡訊為空";

############################################################################
# 各國語言代碼表：
############################################################################
$config['def_lang_code'] = "zh-TW";
$config['lang_code'][0] = array(0=>"af-ZA", 1=>"南非荷蘭語 - 南非");
$config['lang_code'][1] = array(0=>"ar-DZ", 1=>"阿拉伯語 - 阿爾及利亞");
$config['lang_code'][2] = array(0=>"ar-BH", 1=>"阿拉伯語 - 巴林");
$config['lang_code'][3] = array(0=>"ar-EG", 1=>"阿拉伯語 - 埃及");
$config['lang_code'][4] = array(0=>"ar-IQ", 1=>"阿拉伯語 - 伊拉克");
$config['lang_code'][5] = array(0=>"ar-JO", 1=>"阿拉伯語 - 約旦");
$config['lang_code'][6] = array(0=>"ar-KW", 1=>"阿拉伯語 - 科威特");
$config['lang_code'][7] = array(0=>"ar-LB", 1=>"阿拉伯語 - 黎巴嫩");
$config['lang_code'][8] = array(0=>"ar-LY", 1=>"阿拉伯語 - 利比亞");
$config['lang_code'][9] = array(0=>"ar-MA", 1=>"阿拉伯語 - 摩洛哥");
$config['lang_code'][10] = array(0=>"ar-OM", 1=>"阿拉伯語 - 阿曼");
$config['lang_code'][11] = array(0=>"ar-QA", 1=>"阿拉伯語 - 卡達");
$config['lang_code'][12] = array(0=>"ar-SA", 1=>"阿拉伯語 - 沙特阿拉伯");
$config['lang_code'][13] = array(0=>"ar-SY", 1=>"阿拉伯語 - 敘利亞");
$config['lang_code'][14] = array(0=>"ar-TN", 1=>"阿拉伯語 - 突尼斯");
$config['lang_code'][15] = array(0=>"ar-AE", 1=>"阿拉伯語 - 阿拉伯聯合酋長國");
$config['lang_code'][16] = array(0=>"ar-YE", 1=>"阿拉伯語 - 葉門");
$config['lang_code'][17] = array(0=>"be-BY", 1=>"白俄羅斯語 - 白俄羅斯");
$config['lang_code'][18] = array(0=>"bg-BG", 1=>"保加利亞語 - 保加利亞");
$config['lang_code'][19] = array(0=>"ca-ES", 1=>"加泰羅尼亞語 - 加泰羅尼");
$config['lang_code'][20] = array(0=>"cs-CZ", 1=>"捷克語 - 捷克共和國");
$config['lang_code'][21] = array(0=>"Cy-az-AZ", 1=>"阿塞拜疆語（西里爾） - 阿塞拜疆");
$config['lang_code'][22] = array(0=>"Cy-sr-SP", 1=>"塞爾維亞語（塞爾維亞） - 塞爾維亞");
$config['lang_code'][23] = array(0=>"Cy-uz-UZ", 1=>"烏茲別克語（西里爾） - 烏茲別克斯坦");
$config['lang_code'][24] = array(0=>"da-DK", 1=>"丹麥語 - 丹麥");
$config['lang_code'][25] = array(0=>"de-AT", 1=>"德語 - 奧地利");
$config['lang_code'][26] = array(0=>"de-DE", 1=>"德語 - 德國");
$config['lang_code'][27] = array(0=>"de-LI", 1=>"德語 - 列支敦士登");
$config['lang_code'][28] = array(0=>"de-LU", 1=>"德語 - 盧森堡");
$config['lang_code'][29] = array(0=>"de-CH", 1=>"德語 - 瑞士");
$config['lang_code'][30] = array(0=>"div-MV", 1=>"Dhivehi - 馬爾地夫");
$config['lang_code'][31] = array(0=>"el-GR", 1=>"希臘語 - 希臘");
$config['lang_code'][32] = array(0=>"en-AU", 1=>"英語 - 澳大利亞");
$config['lang_code'][33] = array(0=>"en-BZ", 1=>"英語 - 伯利茲");
$config['lang_code'][34] = array(0=>"en-CA", 1=>"英語 - 加拿大");
$config['lang_code'][35] = array(0=>"en-CB", 1=>"英語 - 加勒比");
$config['lang_code'][36] = array(0=>"en-IE", 1=>"英語 - 愛爾蘭");
$config['lang_code'][37] = array(0=>"en-JM", 1=>"英語 - 牙買加");
$config['lang_code'][38] = array(0=>"en-NZ", 1=>"英語 - 紐西蘭");
$config['lang_code'][39] = array(0=>"en-PH", 1=>"英語 - 菲律賓");
$config['lang_code'][40] = array(0=>"en-ZA", 1=>"英語 - 南非");
$config['lang_code'][41] = array(0=>"en-TT", 1=>"英語 - 特立尼達和多巴哥");
$config['lang_code'][42] = array(0=>"en-GB", 1=>"英語 - 英國");
$config['lang_code'][43] = array(0=>"en", 1=>"英語 - 美國");
$config['lang_code'][44] = array(0=>"en-ZW", 1=>"英語 - 津巴布韋");
$config['lang_code'][45] = array(0=>"es-AR", 1=>"西班牙語 - 阿根廷");
$config['lang_code'][46] = array(0=>"es-BO", 1=>"西班牙語 - 玻利維亞");
$config['lang_code'][47] = array(0=>"es-CL", 1=>"西班牙語 - 智利");
$config['lang_code'][48] = array(0=>"es-CO", 1=>"西班牙 - 哥倫比亞");
$config['lang_code'][49] = array(0=>"es-CR", 1=>"西班牙語 - 哥斯達黎加");
$config['lang_code'][50] = array(0=>"es-DO", 1=>"西班牙語 - 多米尼加");
$config['lang_code'][51] = array(0=>"es-EC", 1=>"西班牙語 - 厄瓜多爾");
$config['lang_code'][52] = array(0=>"es-SV", 1=>"西班牙語 - 薩爾瓦多");
$config['lang_code'][53] = array(0=>"es-GT", 1=>"西班牙語 - 瓜地馬拉");
$config['lang_code'][54] = array(0=>"es-HN", 1=>"西班牙語 - 洪都拉斯");
$config['lang_code'][55] = array(0=>"es-MX", 1=>"西班牙語 - 墨西哥");
$config['lang_code'][56] = array(0=>"es-NI", 1=>"西班牙語 - 尼加拉瓜");
$config['lang_code'][57] = array(0=>"es-PA", 1=>"西班牙語 - 巴拿馬");
$config['lang_code'][58] = array(0=>"es-PY", 1=>"西班牙語 - 巴拉圭");
$config['lang_code'][59] = array(0=>"es-PE", 1=>"西班牙語 - 秘魯");
$config['lang_code'][60] = array(0=>"es-PR", 1=>"西班牙語 - 波多黎各");
$config['lang_code'][61] = array(0=>"es-ES", 1=>"西班牙語 - 西班牙");
$config['lang_code'][62] = array(0=>"es-UY", 1=>"西班牙語 - 烏拉圭");
$config['lang_code'][63] = array(0=>"es-VE", 1=>"西班牙語 - 委內瑞拉");
$config['lang_code'][64] = array(0=>"et-EE", 1=>"愛沙尼亞語 - 愛沙尼亞");
$config['lang_code'][65] = array(0=>"eu-ES", 1=>"巴斯克語 - 巴斯克");
$config['lang_code'][66] = array(0=>"fa-IR", 1=>"波斯語 - 伊朗");
$config['lang_code'][67] = array(0=>"fi-FI", 1=>"芬蘭語 - 芬蘭");
$config['lang_code'][68] = array(0=>"fo-FO", 1=>"法語 - 法羅群島");
$config['lang_code'][69] = array(0=>"fr-BE", 1=>"法語 - 比利時");
$config['lang_code'][70] = array(0=>"fr-CA", 1=>"法語 - 加拿大");
$config['lang_code'][71] = array(0=>"fr-FR", 1=>"法語 - 法國");
$config['lang_code'][72] = array(0=>"fr-LU", 1=>"法語 - 盧森堡");
$config['lang_code'][73] = array(0=>"fr-MC", 1=>"法語 - 摩納哥");
$config['lang_code'][74] = array(0=>"fr-CH", 1=>"法語 - 瑞士");
$config['lang_code'][75] = array(0=>"gl-ES", 1=>"加利西亞 - 加利西亞");
$config['lang_code'][76] = array(0=>"gu-IN", 1=>"古吉拉特文 - 印度");
$config['lang_code'][77] = array(0=>"he-IL", 1=>"希伯來語 - 以色列");
$config['lang_code'][78] = array(0=>"hi-IN", 1=>"印地文 - 印度");
$config['lang_code'][79] = array(0=>"hr-HR", 1=>"克羅地亞 - 克羅地亞");
$config['lang_code'][80] = array(0=>"hu-HU", 1=>"匈牙利 - 匈牙利");
$config['lang_code'][81] = array(0=>"hy-AM", 1=>"亞美尼亞 - 亞美尼亞");
$config['lang_code'][82] = array(0=>"id-ID", 1=>"印尼 - 印尼");
$config['lang_code'][83] = array(0=>"is-IS", 1=>"冰島 - 冰島");
$config['lang_code'][84] = array(0=>"it-IT", 1=>"意大利語 - 意大利");
$config['lang_code'][85] = array(0=>"it-CH", 1=>"意大利語 - 瑞士");
$config['lang_code'][86] = array(0=>"ja-JP", 1=>"日文 - 日本");
$config['lang_code'][87] = array(0=>"ka-GE", 1=>"格魯吉亞 - 格魯吉亞");
$config['lang_code'][88] = array(0=>"kk-KZ", 1=>"哈薩克 - 哈薩克斯坦");
$config['lang_code'][89] = array(0=>"kn-IN", 1=>"卡納達 - 印度");
$config['lang_code'][90] = array(0=>"kok-IN", 1=>"康卡尼 - 印度");
$config['lang_code'][91] = array(0=>"ko-KR", 1=>"韓文 - 韓國");
$config['lang_code'][92] = array(0=>"ky-KZ", 1=>"吉爾吉斯 - 哈薩克斯坦");
$config['lang_code'][93] = array(0=>"lv-LV", 1=>"拉脫維亞 - 拉脫維亞");
$config['lang_code'][94] = array(0=>"Lt-az-AZ", 1=>"阿塞拜疆（拉丁） - 阿塞拜疆");
$config['lang_code'][95] = array(0=>"lt-LT", 1=>"立陶宛 - 立陶宛");
$config['lang_code'][96] = array(0=>"Lt-sr-SP", 1=>"塞爾維亞（拉丁） - 塞爾維亞");
$config['lang_code'][97] = array(0=>"Lt-uz-UZ", 1=>"烏茲別克（拉丁） - 烏茲別克斯坦");
$config['lang_code'][98] = array(0=>"mk-MK", 1=>"馬其頓（FYROM）");
$config['lang_code'][99] = array(0=>"ms-BN", 1=>"馬來文 - 文萊");
$config['lang_code'][100] = array(0=>"ms-MY", 1=>"馬來文 - 馬來西亞");
$config['lang_code'][101] = array(0=>"mr-IN", 1=>"馬拉地 - 印度");
$config['lang_code'][102] = array(0=>"mn-MN", 1=>"蒙古 - 蒙古");
$config['lang_code'][103] = array(0=>"nb-NO", 1=>"挪威語（Bokmål） - 挪威");
$config['lang_code'][104] = array(0=>"nn-NO", 1=>"挪威語（尼諾斯克） - 挪威");
$config['lang_code'][105] = array(0=>"nl-BE", 1=>"荷蘭語 - 比利時");
$config['lang_code'][106] = array(0=>"nl-NL", 1=>"荷蘭語 - 荷蘭");
$config['lang_code'][107] = array(0=>"pa-IN", 1=>"旁遮普邦 - 印度");
$config['lang_code'][108] = array(0=>"pl-PL", 1=>"波蘭語 - 波蘭");
$config['lang_code'][109] = array(0=>"pt-BR", 1=>"葡萄牙語 - 巴西");
$config['lang_code'][110] = array(0=>"pt-PT", 1=>"葡萄牙語 - 葡萄牙");
$config['lang_code'][111] = array(0=>"ro-RO", 1=>"羅馬尼亞語 - 羅馬尼亞");
$config['lang_code'][112] = array(0=>"ru-RU", 1=>"俄羅斯 - 俄羅斯");
$config['lang_code'][113] = array(0=>"sa-IN", 1=>"梵文 - 印度");
$config['lang_code'][114] = array(0=>"sk-SK", 1=>"斯洛伐克語 - 斯洛伐克");
$config['lang_code'][115] = array(0=>"sl-SI", 1=>"斯洛文尼亞語 - 斯洛維尼亞");
$config['lang_code'][116] = array(0=>"sq-AL", 1=>"阿爾巴尼亞 - 阿爾巴尼亞");
$config['lang_code'][117] = array(0=>"sv-FI", 1=>"瑞典語 - 芬蘭");
$config['lang_code'][118] = array(0=>"sv-SE", 1=>"瑞典語 - 瑞典");
$config['lang_code'][119] = array(0=>"sw-KE", 1=>"斯瓦希里語 - 肯尼亞");
$config['lang_code'][120] = array(0=>"syr-SY", 1=>"敘利亞 - 敘利亞");
$config['lang_code'][121] = array(0=>"ta-IN", 1=>"泰米爾語 - 印度");
$config['lang_code'][122] = array(0=>"te-IN", 1=>"泰盧固語 - 印度");
$config['lang_code'][123] = array(0=>"th-TH", 1=>"泰文 - 泰國");
$config['lang_code'][124] = array(0=>"tr-TR", 1=>"土耳其 - 土耳其");
$config['lang_code'][125] = array(0=>"tt-RU", 1=>"韃靼語 - 俄羅斯");
$config['lang_code'][126] = array(0=>"uk-UA", 1=>"烏克蘭 - 烏克蘭");
$config['lang_code'][127] = array(0=>"ur-PK", 1=>"烏爾都語 - 巴基斯坦");
$config['lang_code'][128] = array(0=>"vi-VN", 1=>"越南 - 越南");
$config['lang_code'][129] = array(0=>"zh-CN", 1=>"中文 - 中國");
$config['lang_code'][130] = array(0=>"zh-HK", 1=>"中文 - 香港");
$config['lang_code'][131] = array(0=>"zh-MO", 1=>"中文 - 澳門");
$config['lang_code'][132] = array(0=>"zh-SG", 1=>"中文 - 新加坡");
$config['lang_code'][133] = array(0=>"zh-TW", 1=>"中文 - 台灣");
$config['lang_code'][134] = array(0=>"zh-CHS", 1=>"簡體中文");
$config['lang_code'][135] = array(0=>"zh-CHT", 1=>"繁體中文");
$localtest=0;
if ($localtest==1){
      define("MONGO",'mongodb://saja:Sjw#123@localhost:27017');
}else{
      define("MONGO",'mongodb+srv://test:test@healthcoincluster.6rx6l.gcp.mongodb.net/');
}
define('MONGO_DB','healthcoin');

$config['NoSQL']['db']['username']='healthcoin';
$config['NoSQL']['db']['password']='Hc82870032';
$config['NoSQL']['db']['dbname']='healthcoin';
$config['NoSQL']['db']['url']="mongodb+srv://".$config['NoSQL']['db']['username'].":".$config['NoSQL']['db']['password'].
                                  "@healthcoincluster.6rx6l.gcp.mongodb.net/".$config['NoSQL']['db']['dbname'].
								  "?retryWrites=true&w=majority";