<?php
/**
 * Router Controller
 *
 * This controller and helper class route the user to wherever they need to go based on
 * the URL.  This was taken from Dan Sosedoff's blog, the source can be found here:
 * http://blog.sosedoff.com/2009/09/20/rails-like-php-url-router/
 */
include_once "KLogger.php";

class Router
{
    public $request_uri;
    public $routes;
    public $controller, $controller_name;
    public $action, $id;
    public $params;
    public $route_found = false;

    public function __construct()
	{
		$io = new intputOutput();

		$request = $_SERVER['REQUEST_URI'];

        $pos = strpos($request, '?');
        if($pos) $request = substr($request, 0, $pos);

		// 網址轉換
		$this->request_uri = str_replace(APP_DIR, '', $request);
        $this->routes = array();
    }

    /*
	//剖析網址的規則
	*/
	public function map($rule, $target=array(), $conditions=array())
	{
        $this->routes[$rule] = new Route($rule, $this->request_uri, $target, $conditions);
    }

    public function default_routes()
	{
        $this->map('/:controller');
        $this->map('/:controller/:action');
        $this->map('/:controller/:action/:id');
    }

    /*
	//設定網址參數
	*/
	private function set_route($route)
	{
		$this->route_found = true;
        $params = $route->params;

		$this->controller = isset($params['controller']) ? $params['controller'] : 'site';
		unset($params['controller']);

		$this->action = isset($params['action']) ? $params['action'] : '';
		unset($params['action']);

		$this->id = isset($params['id']) ? $params['id'] : '';

        //重整網址參數
		$this->params = array_merge($params, $_GET);

        if (empty($this->controller)) $this->controller = ROUTER_DEFAULT_CONTROLLER;
        if (empty($this->action)) $this->action = ROUTER_DEFAULT_ACTION;
        if (empty($this->id)) $this->id = null;

        //將 controller 名稱字首轉成大寫
		$w = explode('_', $this->controller);
        foreach($w as $k => $v) $w[$k] = ucfirst($v);
        $this->controller_name = implode('', $w);
    }

    public function execute()
	{
		foreach($this->routes as $route) {
			if ($route->is_matched) {
                $this->set_route($route);
                break;
            }
        }
    }
}

/*
//剖析網址參數 http://210.61.12.136/site/mall/exchange/aaa?channelid=&epcid=1&20140218093028
取得
["params"]= array(3) {
    ["controller"]= string(4) "mall"
    ["action"]= string(4) "exchange"
    ["id"]= string(8) "aaa"
}
*/
class Route
{
    public $is_matched = false;
    public $params;
    public $url;
    private $conditions;

    public function __construct($url, $request_uri, $target, $conditions)
	{
        $this->url = $url;
        $this->params = array();
        $this->conditions = $conditions;
        $p_names = array(); $p_values = array();

        preg_match_all('@:([\w]+)@', $url, $p_names, PREG_PATTERN_ORDER);
        $p_names = $p_names[0];

        $url_regex = preg_replace_callback('@:[\w]+@', array($this, 'regex_url'), $url);
        $url_regex .= '/?';

        if (preg_match('@^' . $url_regex . '$@', $request_uri, $p_values)) {
            array_shift($p_values);
            foreach($p_names as $index => $value) $this->params[substr($value,1)] = urldecode($p_values[$index]);
            foreach($target as $key => $value) $this->params[$key] = $value;
            $this->is_matched = true;
        }

        unset($p_names); unset($p_values);
    }

    public function regex_url($matches)
	{
        $key = str_replace(':', '', $matches[0]);
        if (array_key_exists($key, $this->conditions)) {
            return '('.$this->conditions[$key].')';
        }
        else {
            return '([a-zA-Z0-9_\+\-%]+)';
        }
    }
}

class intputOutput
{
	function __construct()
	{
		//检测 magic_quotes_gpc 是“on”还是“off”

		if(isset($_SERVER)){
			$_SERVER = (ini_get('magic_quotes_gpc')) ? $_SERVER : $this->stripslashesArr($_SERVER);
		}
		if(isset($_POST)){
			$_POST = (ini_get('magic_quotes_gpc')) ? $_POST : $this->stripslashesArr($_POST);
		}
		if(isset($_GET)){
			$_GET = (ini_get('magic_quotes_gpc')) ? $_GET : $this->stripslashesArr($_GET);
		}
		if(isset($_COOKIE)){
			$_COOKIE = (ini_get('magic_quotes_gpc')) ? $_COOKIE : $this->stripslashesArr($_COOKIE);
		}
		if(isset($_SESSION)){
			$_SESSION = (ini_get('magic_quotes_gpc')) ? $_SESSION : $this->stripslashesArr($_SESSION);
		}
		if(isset($_FILES)){
			$_FILES = (ini_get('magic_quotes_gpc')) ? $_FILES : $this->stripslashesArr($_FILES);
		}
	}


	function stripslashesArr($var)
	{
		//0error_reporting(E_ALL); ini_set('display_errors', '1'); 
		//var_dump($var);
		/*$log = new KLogger ( "/var/log/leo_op_traces_router" , KLogger::DEBUG );
		try{
		$output = array();
		if(is_array($var))
			{
	  			foreach ($var as $key => $value)
				{
					if(is_array($value)) {
						foreach($value as $key2 => $value2){
							if(is_array($value2)){
								foreach($value2 as $key3 => $value3){
									if (!(empty($value3))) $output[$key][$key2][$key3] = addslashes($value3);

								}
							}
							else{
	    						if (!(empty($value2)))$output[$key][$key2] = addslashes($value2);
							}
						}
					}
					else{
	    				if (!(empty($value)))$output[$key] = addslashes($value);
					}
				}
			}
		} catch (Exception $e) {

		  	$log->LogDebug("router 193: ".json_encode($var));
		  	$log->LogDebug("router 194:".$e->getMessage());
		  	$log->LogDebug("router 195: ".json_encode($output));
		}*/
		return $var;
	}
}
?>