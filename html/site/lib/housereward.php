<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

/*
usage passthru("php houserward.php recnum productid genuserid")
@recnum 單次下標數:
@productid 競標商品編號
@genuserid 競標者會員編號
*/
include_once "KLogger.php";
$log = new KLogger ( "/tmp/reward" , KLogger::DEBUG );

require_once('/var/www/html/site/lib/config.php');

$dbhost = "test.shajiawang.com";//$config["db"][0]['host'];
$dbuser = $config["db"][0]['username'];
$dbpasswd = $config["db"][0]["password"];
$dbname = $config["db"][4]["dbname"];
$dsn = "mysql:host=".$dbhost.";dbname=".$dbname;

if (sizeof($argv)==4){
    $recnum=$argv[1];
    $productid=$argv[2];
    $genuserid=$argv[3];
    //check base parameter
    if ((is_numeric($recnum))&&(is_numeric($productid))&&(is_numeric($genuserid))){
    }else{
        exit;
    }
}else{
    exit;
}
$log->LogDebug("start ".implode(",",$argv));
function getWaitRewardRec($conn,$recnum,$productid,$genuserid,$introducer1,$introducer2,$config){
    $input = array(
        ':productid' => "{$productid}",
        ':genuserid' => "{$genuserid}"
        );
    $sql = "SELECT bid_total,shid FROM `saja_shop`.`saja_history`  where productid=:productid and userid=:genuserid and spointid>0 and rewarded='N' order by shid desc limit 0,$recnum";
    $sth = $conn->prepare($sql);
    $sth->execute($input);
    $rows = $sth->fetchall(PDO::FETCH_ASSOC);
    foreach($rows as $row)
    {
        $shid=$row['shid'];
        $rewardbase=$row['bid_total'];
        //檢查是否已有記錄
        $canreward=check_rewarded($conn,$shid,$genuserid,($rewardbase/10));
        if ($canreward){
            $prefixid = $config['default_prefix_id'];
            $country = $config['country'];
            $behav="dream_reward";
            $amount=$rewardbase/10;
            $seq=0;
            $switch="Y";
            $insertt=date('Y-m-d H:i:s');
            $input=array(
            ':prefixid' => "{$prefixid}",
            ':userid' => "{$introducer1}",
            ':countryid' => "{$country}",
            ':behav' => "{$behav}",
            ':amount' => "{$amount}",
            ':seq' => "{$seq}",
            ':switch' => "{$switch}",
            ':insertt' => "{$insertt}",
            ':shid' => "{$shid}",
            ':genuserid' => "{$genuserid}"
            );
            $sql = "INSERT INTO `saja_cash_flow`.`saja_spoint` SET prefixid = :prefixid, userid = :userid, countryid = :countryid, behav = :behav, amount = :amount, seq = :seq, switch = :switch, insertt=:insertt, shid=:shid, genuserid=:genuserid";
            $sth = $conn->prepare($sql);
            $sth->execute($input);
        }

        if ($introducer2>0){
            //檢查是否已有記錄
            $canreward=check_rewarded($conn,$shid,$genuserid,($rewardbase/20));
            if ($canreward){
                $prefixid = $config['default_prefix_id'];
                $country = $config['country'];
                $behav="dream_reward";
                $amount=$rewardbase/20;
                $seq=0;
                $switch="Y";
                $insertt=date('Y-m-d H:i:s');
                $input=array(
                ':prefixid' => "{$prefixid}",
                ':userid' => "{$introducer2}",
                ':countryid' => "{$country}",
                ':behav' => "{$behav}",
                ':amount' => "{$amount}",
                ':seq' => "{$seq}",
                ':switch' => "{$switch}",
                ':insertt' => "{$insertt}",
                ':shid' => "{$shid}",
                ':genuserid' => "{$genuserid}"
                );
                $sql = "INSERT INTO `saja_cash_flow`.`saja_spoint` SET prefixid = :prefixid, userid = :userid, countryid = :countryid, behav = :behav, amount = :amount, seq = :seq, switch = :switch, insertt=:insertt, shid=:shid, genuserid=:genuserid";
                $sth = $conn->prepare($sql);
                $sth->execute($input);
                update_rewarded_status($conn,$shid);
            }
        }else{
            update_rewarded_status($conn,$shid);
        }
    }
}

function intro_by_id($conn,$genuserid){
    $input = array(
        ':genuserid' => "{$genuserid}"
    );
    $sql = "SELECT intro_by FROM `saja_user`.`saja_user_affiliate` where userid=:genuserid";
    $sth =$conn->prepare($sql);
    $sth->execute($input);
    $r1 =$sth->fetch(PDO::FETCH_ASSOC);
    if (sizeof($r1)>0){
        $intro_by_id = $r1['intro_by'];
    }else{
        $intro_by_id = -1;
    }
    return $intro_by_id;
}

function check_activate($conn,$userid){
    $input = array(
        ':userid' => "{$userid}"
    );
    $sql = "SELECT userid FROM `saja_user`.`saja_user` where userid=:userid and switch='Y'";
    $sth =$conn->prepare($sql);
    $sth->execute($input);
    $r1 =$sth->fetch(PDO::FETCH_ASSOC);
    if (sizeof($r1)>0){
        return true;
    }else{
        return false;
    }
}

function check_rewarded($conn,$shid,$genuserid,$bonus){
    $input = array(
        ':shid' => "{$shid}",
        ':genuserid' => "{$genuserid}"
    );
    $sql = "SELECT amount FROM `saja_cash_flow`.`saja_spoint` where shid=:shid and genuserid=:genuserid and behav='dream_reward'";
    $sth =$conn->prepare($sql);
    $sth->execute($input);
    $r2 =$sth->fetch(PDO::FETCH_ASSOC);
    if (sizeof($r2)>0){
        if (round($r2['amount'])==$bonus){
            return false;
        }else{
            return true;
        }
    }else{
        return true;
    }
}

function update_rewarded_status($conn,$shid){
    $mark=array(
        ':shid'=>"{$shid}"
    );
    $marksql = "UPDATE `saja_shop`.`saja_history` SET rewarded='Y' WHERE shid=:shid";
    $marksth = $conn->prepare($marksql);
    $marksth->execute($mark);
}

try
{
    $conn = new PDO($dsn,$dbuser,$dbpasswd);
    $conn->exec("SET CHARACTER SET utf8");
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //檢查是否有第一代上線
    $introducer1=intro_by_id($conn,$genuserid);
    if($introducer1>0){
        //檢查上線是否還是有效帳戶
        $usable=check_activate($conn,$introducer1);
        if ($usable){
            //檢查是否已有記錄
            $introducer2=intro_by_id($conn,$introducer1);
            if($introducer2>0){
                //檢查上線是否還是有效帳戶
                $usable=check_activate($conn,$introducer2);
                if ($usable){
                    //檢查是否已有記錄
                }else{
                    $introducer2=0;
                }
                getWaitRewardRec($conn,$recnum,$productid,$genuserid,$introducer1,$introducer2,$config);
            }
        }
    }
}
catch(PDOException $e)
{
    echo "Connection failed: ".$e->getMessage();
}
$log->LogDebug("end ");