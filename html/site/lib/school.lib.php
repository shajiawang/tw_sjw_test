﻿<?php

function getCity() {
	$City_Array = array(
		'BENGBU' => '蚌埠',
		'BEIJING' => '北京',
		'BINZHOU' => '滨州',
		'CHANGCHUN' => '长春',
	    'CHANGSHA' => '长沙',
	    'CHANGZHOU' => '常州',
	    'CHENGDU' => '成都',
		'CHONGQING' => '重庆',
		'DALIAN' => '大连',
		'FOSHAN' => '佛山',
		'FUZHOU' => '福州',
		'GUANGYUAN' => '广元',
		'GUANGZHOU' => '广州',
		'GUILIN' => '桂林',
		'GUIYANG' => '贵阳',
		'HAERBIN' => '哈尔滨',
		'HAIKOU' => '海口',
		'HANDAN' => '邯郸',
		'HANGZHOU' => '杭州',
		'HEFEI' => '合肥',
		'HUAIAN' => '淮安',
		'HUHEHAOTE' => '呼和浩特',
		'JINHUA' => '金华',
		'KUNMING' => '昆明',
		'LASHAN' => '乐山',
		'LUOHE' => '漯河',
		'MIANYANG' => '绵阳',
		'NANCHANG' => '南昌',
		'NANJING' => '南京',
		'NANNING' => '南宁',
		'NINGBO' => '宁波',
		'QINGDAO' => '青岛',
		'QUANZHOU' => '泉州',
		'SHANGHAI' => '上海',
		'SHANTOU' => '汕头',
		'SHAOGUAN' => '韶关',
		'SHENYANG' => '沈阳',
		'SHIJIAZHUANG' => '石家庄',
		'SUZHOU' => '苏州',
		'TIANJIN' => '天津',
        'TIELING' => '铁岭',
		'TONGLING' => '铜陵',
		'WUHAN' => '武汉',
		'XIAMEN' => '厦门',
		'XIAN' => '西安',
		'XIANGYANG' => '襄阳',
		'XIANYANG' => '咸阳',
		'XUZHOU' => '徐州',
		'YANBIANZHOU' => '延边州',
		'YANGZHOU' => '扬州',
		'ZHANJIANG' => '湛江',
		'ZHENGZHOU' => '郑州',
		'ZHONGSHAN' => '中山',
		'ZHOUKOU' => '周口',
		'ZHUHAI' => '珠海',
		'ZIBO' => '淄博'
	);
	
	return $City_Array;
}

function getSchool() {
	$School_Array = array(
		'BENGBU' => array(
			'AUFE' => '安徽财经大学'
		),
		'BEIJING' => array(
			'BCUEDU' => '北京城市学院', 
			'PKU' => '北京大学',
            'BIFT' => '北京服装学院',
            'BUAA' => '北京航空航天大学',
            'BVCLSS' => '北京劳动保障职业学院',
            'BUU' => '北京联合大学',
            'BJFU' => '北京林业大学',
            'BAC' => '北京农学院',
            'BDA' => '北京舞蹈学院',
            'BISTU' => '北京信息科技大学',
            'BIGC' => '北京印刷学院',
            'BUPT' => '北京邮电大学',
            'DPST' => '防灾科技学院',
            'THU' => '清华大学',
            'CNU' => '首都师范大学',
            'RUC' => '中国人民大学',
            'NACTA' => '中国戏曲学院'                   
		),
		'BINZHOU' => array(
			'BZTC' => '滨州学院'
		),
		'CHANGCHUN' => array(
			'CCUGHC' => '长春光华学院',
			'NENU' => '东北师范大学',
			'JLSU' => '吉林体育学院'
		),
		'CHANGSHA' => array(
			'CSSTTC' => '湖南安全技术职业学院',
			'HNGCJX' => '湖南工程职业技术学院',
			'HNISC' => '湖南信息学院'
		),
		'CHANGZHOU' => array(
			'CCZU' => '常州大学',
			'CZISGZ' => '常州旅游商贸高等职业技术学校'
		),
		'CHENGDU' => array(
			'CDUT' => '成都理工大学',
			'SICNU' => '四川师范大学',
			'SWJTU' => '西南交通大学'
		),
		'CHONGQING' => array(
			'SCFAI' => '四川美术学院',
			'CQUE' => '重庆第二师范学院',
			'CQTCEDU' => '重庆电信职业学院',
			'CTBU' => '重庆工商大学',
			'CQUT' => '重庆理工大学',
			'CQIVC' => '重庆轻工职业学院',
			'CQNU' => '重庆师范大学',
			'CQWU' => '重庆文理学院',
			'CQUPT' => '重庆邮电大学'
		),
        'DALIAN' => array(
			'LUIBE' => '辽宁队外经贸学院'
		),
        'FOSHAN' => array(
			'NUIT' => '广东东软学院'
		),
		'FUZHOU' => array(
			'FZU' => '福州大学',
			'FJNU' => '福建师范大学',
			'FJMU' => '福建医科大学',
			'FJTCM' => '福建中医药大学'
		),
        'GUANGYUAN' => array(
			'SCITC' => '四川信息职业技术学院'
		),
		'GUANGZHOU' => array(
			'GDUT' => '广东工业大学（大学城一卡通）',
			'GDAIB' => '广东农工商职业技术学院',
			'GDFS' => '广东女子职业技术学院',
			'GDUFS' => '广东外语外贸大学（大学城一卡通）',
			'GDPU' => '广东药学院（大学城一卡通）',
			'GZHU' => '广州大学（大学城一卡通）',
			'GZDXC' => '广州大学城非证件卡（大学城一卡通）',
			'SISE' => '广州大学华软软件学院',
			'GZAFA' => '广州美术学院（大学城一卡通）',
			'GZUCM' => '广州中医药大学（大学城一卡通）',
			'SCUT' => '华南理工大学（大学生一卡通）',
			'GCU' => '华南理工大学广州学院',
			'SCNU' => '华南师范大学（大学生一卡通）',
			'TXYKT' => '天绎校园一卡通',
			'XHCOM' => '星海音乐学院（大学城一卡通）',
			'SYSU' => '中山大学',
			'ZSDX' => '中山大学（大学城一卡通）',
			'NFU' => '中山大学南方学院'
		),
        'GUILIN' => array(
			'GLIET' => '桂林电子科技大学'
		),
		'GUIYANG' => array(
			'GZGSZY' => '贵州工商职业学院',
			'GZNU' => '贵州师范大学'
		),
        'HAERBIN' => array(
			'NEAU' => '东北农业大学'
		),
        'HAIKOU' => array(
			'HKC' => '海口经济学院'
		),
        'HANDAN' => array(
			'HEBEU' => '河北工程大学'
		),
		'HANGZHOU' => array(
			'HDU' => '杭州电子科技大学',
			'HZASPT' => '杭州科技职业技术学院',
			'HZNU' => '杭州师范大学',
			'ZUFE' => '浙江财经大学',
			'ZJICM' => '浙江传媒学院',
			'HZIC' => '浙江工商大学',
			'ZIME' => '浙江机电职业技术学院',
			'ZJPC' => '浙江警察学院',
			'ZUST' => '浙江科技学院',
			'ZSTU' => '浙江理工大学',
			'ZJVCC' => '浙江商业职业技术学院',
			'ZISU' => '浙江外国语学院',
			'ZJMC' => '浙江医学高等专科学校',
			'ZJVAA' => '浙江艺术职业技术学院',
			'ZJTCM' => '浙江中医药大学',
			'CJLU' => '中国计量学院'
		),
        'HEFEI' => array(
			'USTC' => '中国科学技术大学'
		),
        'HUAIAN' => array(
			'HYIT' => '淮阴工学院'
		),
		'HUHEHAOTE' => array(
			'IMNC' => '呼和浩特民族学院',
			'IMAA' => '内蒙古建筑职业技术学院'
		),
        'JINHUA' => array(
			'JHC' => '金华职业技术学院'
		),
		'KUNMING' => array(
			'YTBU' => '云南工商学院',
			'YNJGY' => '云南经济管理学院',
			'YNNI' => '云南民族大学'
		),
        'LESHAN' => array(
			'SWJTU' => '西南交通大学峨眉校区'
		),
        'LUOHE' => array(
			'LHVTC' => '漯河职业技术学院'
		),
        'MIANYANG' => array(
			'SWUST' => '西南科技大学'
		),
		'NANCHANG' => array(
			'JXUT' => '江西科技学院',
			'NIT' => '南昌工程学院'
		),
		'NANJING' => array(
			'NIIT' => '南京工业职业技术学院',
			'NJCIT' => '南京信息职业技术学院'
		),
        'NANNING' => array(
			'GXSDXY' => '广西水利电力职业技术学院'
		),
		'NINGBO' => array(
			'NBCC' => '宁波城市职业技术学院',
			'NBDU' => '宁波大红鹰学院',
			'NBU' => '宁波大学',
			'NBUT' => '宁波工程学院',
			'ZJFF' => '浙江纺织服装职业技术学院',
			'ZJBTI' => '浙江工商职业技术学院'
		),
        'QINGDAO' => array(
			'QDGW' => '青岛港湾职业技术学院'
		),
        'QUANZHOU' => array(
			'QZIT' => '泉州理工职业学院'
		),
		'SHANGHAI' => array(
			'ECUST' => '华东理工大学',
			'SHUFE' => '上海财经大学',
            'SHU' => '上海大学',
            'SSPU' => '上海第二工业大学',
            'SDJU' => '上海电机学院',
            'SHIEP' => '上海电力学院',
            'SUIBE' => '上海对外经贸大学',
            'SCC' => '上海海关学院',
            'SHMTU' => '上海海事大学',
            'SHOU' => '上海海洋大学',
            'GENCH' => '上海建桥学院',
            'SJTU' => '上海交通大学',
            'SHAFC' => '上海农林职业技术学院',
            'SHNU' => '上海师范大学',
            'AC' => '上海震旦职业学院',
            'TJU' => '同济大学'
		),
        'SHANTOU' => array(
			'STU' => '汕头大学'
		),
        'SHAOGUAN' => array(
			'SGUMC' => '韶关学院医学院'
		),
        'SHENYANG' => array(
			'HSMC' => '辽宁何氏医学院'
		),
        'SHIJIAZHUANG' => array(
			'HEUET' => '河北经贸大学'
		),
		'SUZHOU' => array(
			'SCU' => '苏州大学',
			'SDWZ' => '苏州大学文正学院',
			'ATSSU' => '苏州大学应用技术学院',
			'SADTI' => '苏州工艺美术职业技术学院',
			'SZEDU' => '苏州教育E卡通',
			'USTS' => '苏州科技学院',
			'SZJSJT' =>'苏州建设交通高等职业技术学校',
			'IVTEDU' => '苏州工业园区职业技术学院'
		),
		'TIANJIN' => array(
			'HEBUT' => '河北工业大学',
			'TUFE' => '天津财经大学',
			'TJUT' => '天津理工大学',
			'TJCU' => '天津商业大学',
			'TMU' => '天津医科大学',
			'TJYZH' => '天津医学高等专科学校',
			'TJCM' => '天津音乐学院',
			'TUTE' => '天津职业技术师范大学',
			'CAUC' => '中国民航大学'
		),
        'TIELING' => array(
			'TLSZ' => '铁岭师范高等专科学校'
		),
        'TONGLING' => array(
			'TLC' => '铜陵学院'
		),
		'WUHAN' => array(
			'HBPA' => '湖北警官学院',
			'WTC' => '武汉职业技术学院'
		),
        'XIAMEN' => array(
			'XMU' => '厦门大学'
		),
		'XIAN' => array(
			'SUST' => '陕西科技大学',
			'XEU' => '西安欧亚学院',
			'NWPU' => '西北工业大学'
		),
        'XIANGYANG' => array(
			'XYQCZY' => '襄阳汽车职业技术学院'
		),
        'XIANYANG' => array(
			'XZMY' => '西藏民族学院'
		),
		'XUZHOU' => array(
			'XZNU' => '江苏师范大学',
			'XZIT' => '徐州工程学院'
		),
        'YANBIANZHOU' => array(
			'YBU' => '延边大学'
		),
        'YANGZHOU' => array(
			'YPI' => '扬州工业职业技术学院'
		),
        'ZHANJIANG' => array(
			'GDWLXY' => '广东文理职业学院'
		),
        'ZHENGZHOU' => array(
			'XLXY' => '河南师范大学新联学院'
		),
        'ZHONGSHAN' => array(
			'ZSPT' => '中山职业技术学院'
		),
        'ZHOUKOU' => array(
			'ZKNU' => '周口师范学院'
		),
		'ZHUHAI' => array(
			'ZHBIT' => '北京理工大学珠海学院',
			'GDIT' => '广东科学技术职业学院',
			'JLUZH' => '吉林大学珠海学院'
		),
		'ZIBO' => array(
			'SDUT' => '山东理工大学',
			'SDVCLI' => '山东轻工职业学院'
		)
	);
	
	return $School_Array;
}


?>