<?php
//************************************************ ****
// Default
//************************************************ ****
// $page_header[ROUTER_DEFAULT_CONTROLLER][ROUTER_DEFAULT_ACTION] = '殺價王';
// $page_header2[ROUTER_DEFAULT_CONTROLLER][ROUTER_DEFAULT_ACTION] = '';
$page_header[ROUTER_DEFAULT_CONTROLLER][ROUTER_DEFAULT_ACTION] = '殺價王';


//************************************************ ****
// Product
//************************************************ ****
$page_header['product']['home'] = '殺戮戰場';
$page_header['product']['saja'] = '殺價商品';
$page_header['product']['sajabid'] = '  ';
$page_header['product']['sajatopay'] = '下標結算';
$page_header['product']['flash'] = '閃殺活動';
$page_header['product']['today'] = '今日必殺';


//************************************************ ****
// dream_product
//************************************************ ****
$page_header['dream_product']['home'] = '圓夢計劃';
$page_header['dream_product']['saja'] = '圓夢商品';
$page_header['dream_product']['sajabid'] = '  ';
$page_header['dream_product']['sajatopay'] = '下標結算';
$page_header['dream_product']['today'] = '今日圓夢';
$page_header['dream_history']['history_list'] = '圓夢記錄';

//************************************************ ****
// dream_event
//************************************************ ****
$page_header['dream_event']['home'] = '圓夢計劃活動';
$page_header['dream_event']['saja'] = '圓夢商品';


//************************************************ ****
// Mall
//************************************************ ****
$page_header['mall']['home'] = '鯊魚商城';
$page_header['mall']['exchange'] = '兌換商品';
$page_header['mall']['active'] = '鯊魚點ibon系統兌換流程';


//************************************************ ****
// Bid
//************************************************ ****
$page_header['bid']['home'] = '最新得標';
$page_header['bid']['detail'] = '最新得標';


//************************************************ ****
// Deposit
//************************************************ ****
$page_header['deposit']['home'] = '購買殺價幣';
$page_header['deposit']['confirm'] = '購買殺價幣';


//************************************************ ****
// Tech
//************************************************ ****
$page_header['tech']['home'] = '王者秘笈';
$page_header['tech']['detail'] = '王者秘笈';


//************************************************ ****
// Faq
//************************************************ ****
$page_header['faq']['home'] = '新手教學';
$page_header['faq']['detail'] = '新手教學';


//************************************************ ****
// History
//************************************************ ****
$page_header['history']['home'] = '殺價幣記錄';
$page_header['history']['sabi'] = '殺幣紀錄';
$page_header['history']['bidding'] = '殺價紀錄';
$page_header['history']['scode'] = 'S碼';
$page_header['history']['bid'] = '中標紀錄';
$page_header['history']['bonus'] = '鯊魚點紀錄';//點
$page_header['history']['store'] = '店點紀錄';
$page_header['history']['coupon'] = '殺價禮券';
$page_header['history']['saja'] = '殺價紀錄';

//************************************************ ****
// Scode
//************************************************ ****
$page_header['scode']['home'] = '領取超級殺價券';
$page_header['scode']['scode_list'] = '超級殺價券紀錄';
$page_header['scode']['accept'] = '收取明細';
$page_header['scode']['used'] = '使用明細';
$page_header['oscode']['home'] = '殺價券';
$page_header['oscode']['accept'] = '收取明細';
$page_header['oscode']['used'] = '使用明細';
$page_header['oscode']['serial'] = '序號激活明細';

//************************************************ ****
// Member
//************************************************ ****
$page_header['member']['home'] = '殺友專區';
$page_header['member']['edit'] = '修改密碼';
$page_header['member']['news'] = '站內公告';
$page_header['member']['order'] = '訂單紀錄';
$page_header['member']['shareQrcode'] = '分享專屬QRcode';



//************************************************ ****
// User
//************************************************ ****
$page_header['user']['login'] = '登錄';
$page_header['user']['logout'] = '註銷';
$page_header['user']['register'] = '註冊新帳號';
$page_header['user']['sso_register'] = '免費註冊';
//$page_header['user']['profile'] = '會員收件信息';
//$page_header['user']['profile_confirm'] = '會員收件信息';
$page_header['user']['profile'] = '我的帳號';
$page_header['user']['profile_confirm'] = '確認會員信息';

$page_header['user']['uname'] = '修改暱稱';
$page_header['user']['uarea'] = '修改分區';
$page_header['user']['upwd'] = '修改登入密碼';
$page_header['user']['uepwd'] = '修改兌換密碼';
$page_header['user']['unopwd'] = '小額免密設定';
$page_header['user']['ufpwd'] = '忘記兌換密碼';
$page_header['user']['upackage'] = '我的卡片';
$page_header['user']['uaddress'] = '修改收件人資訊';


//************************************************ ****
// GoogleMap
//************************************************ ****
$page_header['googlemap']['home'] = 'GoogleMap定位/導航';


//************************************************ ****
// Help
//************************************************ ****
$page_header['help']['home'] = '幫助中心';


//************************************************ ****
// Issues
//************************************************ ****
$page_header['issues']['home'] = '我的發問';
$page_header['issues']['issues_add'] = '我的發問';
$page_header['issues']['issues_detail'] = '我的發問';


//************************************************ ****
// About
//************************************************ ****
$page_header['about']['privacy'] = '隱私權政策';
$page_header['about']['service'] = '服務條款';
$page_header['about']['usage'] = '用戶使用規範';
?>