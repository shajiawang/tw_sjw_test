<?php
include_once("./config.php");
include_once("./helpers.php");
include_once("./dbconnect.php");
include_once("./convertString.ini.php");
/*
$ip='';
if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
    $ip = $temp_ip[0];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}

error_log("[ibon_process.php] Request from : ".$ip);
*/
$dbconfig=$config["db"];
	
$arrDBHost=array("TWN"=>"sajatwdbc", "CHN"=>"sajacndb01");
$country_code="TWN";
$i_prdate_no = "";
$i_Magnification = 1;              // 台幣與ibon倍率
// $i_CurrencyRate = 4.5;               // 當地貨幣與台幣匯率
$i_CurrencyRate = 1;               // 當地貨幣與台幣匯率
$stop_service=false;
/*
function is_allow($userid) {
	 global $dbconfig, $arrDBHost;
	 
	 $ARR_ALLOW_USERID=["437604","437112","430104","434814","437604","431024"];
	 if(in_array($userid,$ARR_ALLOW_USERID)) {
		return true; 
	 }
	 
	 $dbconfig[0]["host"]=$arrDBHost[$country_code];
	 $db=new mysql($dbconfig);
	 $db->connect();
	 $sql = "SELECT count(shid) cnt FROM saja_shop.saja_strange_history WHERE switch='Y' and userid='${userid}' ";
	 
	 // 有3筆以上就不准用ibon
	 $table = $db->getQueryRecord($sql);
	 error_log("[ibon_process/is_allow] sql : ".$sql."-->".$table['table']['record'][0]['cnt']);
     if($table['table']['record'][0]['cnt']>=3) {
		return false;  
     }
	 return true;
}
*/
function is_allow($userid) {
	 global $dbconfig, $arrDBHost;
	 
		 
	 $dbconfig[0]["host"]=$arrDBHost[$country_code];
	 $db=new mysql($dbconfig);
	 $db->connect();
	 $sql = "SELECT exchange_enable FROM saja_user.saja_user WHERE switch='Y' and userid='${userid}' ";
	 // $sql = "SELECT exchange_enable FROM saja_user.saja_user WHERE switch='Y' and (userid='${userid}' OR `name`='${userid}') ";
	 // exchange_enable='N' 就不准用ibon
	 $table = $db->getQueryRecord($sql);
	 error_log("[ibon_process/is_allow] sql : ".$sql."-->".$table['table']['record'][0]['exchange_enable']);
     if($table['table']['record'][0]['exchange_enable']=='Y') {
		return true;  
     }
	 
     return false;
}

// 檢查結標轉入的鯊魚點總額是否有超過充值的金額
function is_exceed_deposit($userid) {
	
	    global $dbconfig, $arrDBHost;
	  
	    $dbconfig[0]["host"]=$arrDBHost[$country_code];
	    $db=new mysql($dbconfig);
	    $db->connect();
		/*
		$sql = " SELECT u.userid, 
			(SELECT IFNULL(SUM(amount),0) FROM saja_cash_flow.saja_spoint WHERE behav IN ('user_deposit','gift','system','process_fee') and switch='Y' AND userid=u.userid)  as spoint,
			(SELECT IFNULL(SUM(amount),0) FROM saja_cash_flow.saja_bonus WHERE switch='Y' AND behav IN ('product_close') AND userid=u.userid) as bonus
			FROM saja_user.saja_user u
			WHERE u.switch='Y' 
			  AND u.prefixid='saja' 
			  AND u.userid='{$userid}' 
			  LIMIT 1 ";
		*/
		$sql = " SELECT u.userid, 
				( SELECT IFNULL(SUM(amount),0) FROM saja_cash_flow.saja_spoint 
				   WHERE spointid IN ( 
					   SELECT spointid from saja_cash_flow.saja_deposit_history 
						WHERE switch='Y' 
						  AND status='deposit' 
						  AND userid=u.userid
						  AND spointid>0
				   ) 
				   AND switch='Y'
				   AND behav in ('user_deposit','gift','system')
				) as spoint,
				(SELECT IFNULL(SUM(amount),0) 
				   FROM saja_cash_flow.saja_bonus 
				  WHERE switch='Y' 
					AND behav IN ('product_close') 
					AND userid=u.userid
				) as bonus
				FROM saja_user.saja_user u
				WHERE u.switch='Y' 
				  AND u.prefixid='saja' 
				  AND u.userid='{$userid}' 
				  LIMIT 1 ";
		$table = $db->getQueryRecord($sql);
	    error_log("[ibon_process/is_exceed_deposit] sql : ".$sql."-->".json_encode($table['table']['record'][0]));
		if(!$table['table']['record'][0] || !$table['table']['record'][0]['userid']) {
			return true;
		} else if($table['table']['record'][0]['bonus']>$table['table']['record'][0]['spoint']) { 
			return true;
		}
		return false;
}

function reply091001($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$ibon_bonus=0) {
    error_log("<SHOWDATA><BUSINESS>091002</BUSINESS><STOREID>".$i_STOREID."</STOREID><SHOPID>".$i_SHOPID."</SHOPID><DETAIL_NUM>".$i_DETAIL_NUM."</DETAIL_NUM><STATUS_CODE>".$i_STATUS_CODE."</STATUS_CODE><STATUS_DESC>".$e_STATUS_DESC."</STATUS_DESC><LISTDATA><DATA_1>".$ibon_bonus."</DATA_1><DATA_2>01</DATA_2><DATA_3></DATA_3><DATA_4></DATA_4><DATA_5></DATA_5></LISTDATA></SHOWDATA>");
	return "<SHOWDATA><BUSINESS>091002</BUSINESS><STOREID>".$i_STOREID."</STOREID><SHOPID>".$i_SHOPID."</SHOPID><DETAIL_NUM>".$i_DETAIL_NUM."</DETAIL_NUM><STATUS_CODE>".$i_STATUS_CODE."</STATUS_CODE><STATUS_DESC>".$e_STATUS_DESC."</STATUS_DESC><LISTDATA><DATA_1>".$ibon_bonus."</DATA_1><DATA_2>01</DATA_2><DATA_3></DATA_3><DATA_4></DATA_4><DATA_5></DATA_5></LISTDATA></SHOWDATA >";
    // return;
}

function reply091003($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$i_XMLData,$i_RETURNCODE) {
    error_log("<CONFIRMDATA_R><BUSINESS>091004</BUSINESS><STOREID>".$i_STOREID."</STOREID><SHOPID>".$i_SHOPID."</SHOPID><DETAIL_NUM>".$i_DETAIL_NUM."</DETAIL_NUM><STATUS_CODE>".$i_STATUS_CODE."</STATUS_CODE><STATUS_DESC>".$e_STATUS_DESC."</STATUS_DESC><CARDTYPE>".(string)$i_XMLData->CARDTYPE."</CARDTYPE><ITEM_TYPE>".(string)$i_XMLData->ITEM_TYPE."</ITEM_TYPE><RETURNCODE>".$i_RETURNCODE."</RETURNCODE></CONFIRMDATA_R>"); 
    return "<CONFIRMDATA_R><BUSINESS>091004</BUSINESS><STOREID>".$i_STOREID."</STOREID><SHOPID>".$i_SHOPID."</SHOPID><DETAIL_NUM>".$i_DETAIL_NUM."</DETAIL_NUM><STATUS_CODE>".$i_STATUS_CODE."</STATUS_CODE><STATUS_DESC>".$e_STATUS_DESC."</STATUS_DESC><CARDTYPE>".(string)$i_XMLData->CARDTYPE."</CARDTYPE><ITEM_TYPE>".(string)$i_XMLData->ITEM_TYPE."</ITEM_TYPE><RETURNCODE>".$i_RETURNCODE."</RETURNCODE></CONFIRMDATA_R>"; 
}

function chkPhoneCountry($phone) {
       global $i_CurrencyRate, $i_Magnification;
       $ret = false;
       if(empty($phone))
          return false;
       /*
	   if(!is_numeric($phone))
          return false; 
       */
	   $ret = false;
       if(strlen($phone)==11 && strpos($phone,'1')==0) {
           $ret = 'CHN';
           $i_CurrencyRate=4.0;
       } else if(strlen($phone)==10 && strpos($phone,'0')==0){
           $ret = 'TWN';
           $i_CurrencyRate=1.0;
       } else {
           $ret = 'TWN';
           $i_CurrencyRate=1.0;
       }	   
       return $ret;
}

// 鯊魚點數轉成貨幣金額(ibon點數)
function SajaBonusToIbon($bonus, $country_code) {
         global $i_CurrencyRate, $i_Magnification;
         error_log("[ibon_process/SajaBonusToIbon] bonus=${bonus}, country_code=${country_code}");
         $ret = 0;
         if($country_code=='TWN') {
            $i_CurrencyRate=1;
         } else if($country_code=='CHN'){
            $i_CurrencyRate=4;
         } 
         // 須為整數(無條件捨去)
         $ret = floor($bonus * $i_CurrencyRate*$i_Magnification);
         error_log("[ibon_process/SajaBonusToIbon] CurrencyRate,Magnification=${i_CurrencyRate},${i_Magnification}, ret=".$ret);
         return $ret;          
}

// 消費的ibon點數轉成等值鯊魚點
function IbonToSajaBonus($price, $country_code) {
         global $i_CurrencyRate, $i_Magnification;
         error_log("[ibon_process/IbonToSajaBonus] price=${price},country_code=${country_code}");
         $ret = 0;
         if($country_code=='TWN') {
            $i_CurrencyRate=1; 
         } else if($country_code=='CHN'){
            $i_CurrencyRate=4;
         } 
         // 無條件進入到整數
         $ret = ceil($price/$i_Magnification/$i_CurrencyRate);
         error_log("[ibon_process/IbonToSajaBonus] CurrencyRate,Magnification=${i_CurrencyRate},${i_Magnification}, ret=".$ret);
         return $ret;
}

function getQueryRecord($sql,$country_code='') {
         global $dbconfig, $arrDBHost;
         $table = false;
             if(empty($country_code)) {
                     // 如果沒有指定地區  則依$arrHost的順序連線撈資料
                     foreach($arrDBHost as $country=>$host) {
                              $dbconfig[0]["host"]=$host;
                              $dbconfig[0]["port"]="3306";
                              $db=new mysql($dbconfig);
                              $db->connect();
                              $table = $db->getQueryRecord($sql);
                              if(!empty($table['table']['record'])) {
                                      break;
                              }
                     }
             } else {
                     // 如果指定地區  則連線該地區的$Host撈資料
                     $dbconfig[0]["host"]=$arrDBHost[$country_code];
                     $dbconfig[0]["port"]="3306";
                     $db=new mysql($dbconfig);
                     $db->connect();
                     $table = $db->getQueryRecord($sql);
             }
             return $table;
}


function execCRUDSQL($sql, $country_code, $insert_id=false) {
	     global $dbconfig, $arrDBHost;
		 $dbconfig[0]["host"]=$arrDBHost[$country_code];
		 $db=new mysql($dbconfig);
		 $db->connect();
		 $ret = $db->query($sql);
		 if($insert_id) {
			$ret=$db->_con->insert_id; 
		 }
		 return $ret;
}

// 統計當天使用總金額
function UseIbonTotal($userid) {
	global $dbconfig, $arrDBHost;
	$dbconfig[0]["host"]=$arrDBHost[$country_code];
	$db=new mysql($dbconfig);
	$db->connect();
	
	$day = date("Y-m-d");
	error_log("[ibon_process/UseIbonTotal] userid=${userid}, day=${day}");
	$sql = "SELECT IFNULL(SUM(tr_ibons), 0) AS usetotal FROM saja_exchange.saja_exchange_ibon_record WHERE u_id= ".$userid." AND u_date BETWEEN '".$day." 00:00:00' AND '".$day." 23:59:59' ";
	error_log("[ibon_process]sql total : ".$sql);
	$table = $db->getQueryRecord($sql);
	error_log("[ibon_process] usetotal : ".$table['table']['record'][0]['usetotal']);
	return $table['table']['record'][0]['usetotal'];
	
}

if($stop_service) {
   $i_STATUS_CODE = "1011";
   $i_STATUS_DESC = iconv("UTF-8","BIG5","系統維護 暫停服務 ");
   error_log("[ibon_prcess] temporary stop service !!");
   $e_STATUS_DESC = $i_STATUS_DESC;
   $e_XMLData = reply091001("000000","0","000000",$i_STATUS_CODE,$e_STATUS_DESC,0);
   echo $e_XMLData;
   exit;
}
					

	//給 XML 函式處理
$XMLData=$_REQUEST['XMLData'];
error_log("[ibon_process] Request XML :".$XMLData);

$encode=mb_detect_encoding($XMLData);
error_log("[ibon_process.php] encode : ".$encode);

$o_XMLData = stripslashes($XMLData);
error_log("[ibon_process] o_XMLData :".$o_XMLData);

// $x_XMLData = iconv($encode, "big5", $o_XMLData);	//給 MySQL 儲存專用

$x_XMLData = str_replace("<?xml version='1.0' encoding='Big5'?>","",$o_XMLData);

/*
preg_match("/<STATUS_DESC>(.+)<\/STATUS_DESC>/",$o_XMLData,$matches);
$x_XMLData = str_replace($matches[0],"<STATUS_DESC></STATUS_DESC>",$x_XMLData);

preg_match_all("/<DATA_2>(.+)<\/DATA_2>/",$x_XMLData,$matches);
$x_XMLData = str_replace($matches[0],"<DATA_2></DATA_2>",$x_XMLData);
*/
/*
$pos1=strpos($x_XMLData,"<STATUS_DESC>");
$pos2=strpos($x_XMLData,"</STATUS_DESC>");
$x_XMLData=substr($x_XMLData,0,$pos1)."<STATUS_DESC>".substr($x_XMLData,$pos2);
*/
error_log("[ibon_process] x_XMLData :".$x_XMLData);

if($o_XMLData != "" && $o_XMLData != NULL) {
   $str = new convertString();
   
		$i_XMLData = simplexml_load_string($o_XMLData);
		$i_STOREID = (string)$i_XMLData->STOREID;
		$i_SHOPID = (string)$i_XMLData->SHOPID;
		$i_BUSINESS = (string)$i_XMLData->BUSINESS;
		$i_DETAIL_NUM = (string)$i_XMLData->DETAIL_NUM;
		$i_STATUS_CODE = (string)$i_XMLData->STATUS_CODE;
		$i_STATUS_DESC = (string)$i_XMLData->STATUS_DESC;
		$i_prdate_no = -1;
		
		$YmdHis = date("Y-m-d H:i:s");
		$Ymd = date("Ymd");
	try {	
		// $db=new mysql($config["db"]);
		// $db->connect();
		
		// 查符合該日期的檔次
		$sql = "SELECT no FROM saja_exchange.saja_exchange_ibon_prdate WHERE date_start <= '".$Ymd."' AND date_end >= '".$Ymd."' ";
		
		error_log("[ibon_process]sql 1 : ".$sql);
		
		$table = getQueryRecord($sql,'TWN');
		// $table = $db->getQueryRecord($sql);
		
		if(!empty($table['table']['record'])) {
		   $i_prdate_no = $table['table']['record'][0]['no'];
		} 
		
		error_log("ibon prdate_no :".$i_prdate_no);
	    	
        if ($i_BUSINESS == "091001" || $i_BUSINESS == "091003") {
            
            //安源Center→業者端  向業者端要求身份驗證
            if ($i_BUSINESS == "091001") {	
                $account_id = "";       // 兌換時輸入的帳號
                $userid="";             // user unique ID
                $saja_bonus = 0;        // Saja bonus點數
                $ibon_bonus = 0;        // 等值iBon 點數
				$can_use_total = 300;    //可以兌換金額
                
                if ($i_STATUS_CODE == "0000" && $i_STATUS_DESC != "" && $i_prdate_no != "")	 {
                    $account_id=(string)$i_XMLData->LISTDATA->DATA_1;
                    $account_id=strtolower(htmlspecialchars($account_id));
                    $exch_passwd = $str->strEncode((string)$i_XMLData->LISTDATA->DATA_2, $config['encode_key']);
                    
                    error_log("[ibon_process] id/pwd: ".$account_id."/".$i_XMLData->LISTDATA->DATA_2."/".$exch_passwd);
					
					// test
/* 					if($account_id != 1705 && $account_id != 28 && $account_id != 116) {
					   $i_STATUS_CODE = "1010";
                       $i_STATUS_DESC = iconv("UTF-8","BIG5","系統維護 暫停服務 !");
					   error_log("[ibon_prcess] temporary stop service !!");
					   $e_STATUS_DESC = $i_STATUS_DESC;
                       $e_XMLData = reply091001($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$ibon_bonus);
                       echo $e_XMLData;
                       exit;
					} */
					
					
					// 20200109 暫時停止服務
					if(!is_allow($account_id)) {
					   $i_STATUS_CODE = "1010";
                       $i_STATUS_DESC = iconv("UTF-8","BIG5","兌換帳號異常, 請洽客服 !");
					   $e_STATUS_DESC = $i_STATUS_DESC;
                       $e_XMLData = reply091001($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$ibon_bonus);
                       echo $e_XMLData;
                       exit;
					}
					
					if(is_exceed_deposit($account_id)) {
					   $i_STATUS_CODE = "1025";
                       $i_STATUS_DESC = iconv("UTF-8","BIG5","帳號異常, 請洽客服 !");
					   $e_STATUS_DESC = $i_STATUS_DESC;
                       $e_XMLData = reply091001($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$ibon_bonus);
                       echo $e_XMLData;
                       exit;
					}
                    
                    // 檢查帳號(手機號)格式 確定地區
                    $country_code = chkPhoneCountry($account_id);
                            
                    $sql = " SELECT u.userid, u.name, up.nickname, IFNULL(m.verified,'N') as verified, SUM(IFNULL(b.amount,0)) as saja_bonus, up.idnum ";
                    $sql.= " FROM saja_user.saja_user u ";
					$sql.= " JOIN saja_user.saja_user_profile up on u.userid=up.userid  AND u.switch='Y' AND up.switch='Y' "; 
					$sql.= " JOIN saja_user.saja_user_sms_auth m on u.userid=m.userid  AND u.switch='Y' AND m.switch='Y' ";
                    $sql.= " LEFT JOIN saja_cash_flow.saja_bonus b ON u.userid=b.userid AND u.switch='Y' AND b.switch='Y' ";
                    // $sql.= " WHERE REPLACE(u.name,'_','')='".($account_id)."' AND u.exchangepasswd='".$exch_passwd."'  ";
                    $sql.= " WHERE u.switch='Y' ";
                    $sql.= " AND (u.userid='".$account_id."' OR REPLACE(u.name,'_','')='".$account_id."' ) ";
                    $sql.= " AND u.exchangepasswd='".$exch_passwd."' ";
                    $sql.= " GROUP BY u.userid, u.name, up.nickname, m.verified ";	
                    
                    error_log("[ibon_process]sql 2 : ".$sql);
                     
                    // $table = $db->getQueryRecord($sql);
                    $table = getQueryRecord($sql,$country_code);
                    
                    if(!empty($table['table']['record'])) {
                        // add By Thomas 20200309 導入自然人身分驗證
						if(empty($table['table']['record'][0]['idnum'])) {
						    $i_STATUS_CODE = "1009";
                            $i_STATUS_DESC = iconv("UTF-8","BIG5","未通過身分證號驗證，無法使用紅利金，請先至殺價王APP=>殺友專區=>我的帳號 進行身分證號(自然人)驗證！");    	
						} else if($table['table']['record'][0]['verified']!='Y') {
                            $i_STATUS_CODE = "1002";
                            $i_STATUS_DESC = iconv("UTF-8","BIG5","未通過手機號驗證，無法使用紅利金，請先至殺價王APP=>殺友專區=>我的帳號 進行手機驗證！");
						} else {
						    $account_id = $table['table']['record'][0]['name'];
                            $userid = $table['table']['record'][0]['userid'];
                            $saja_bonus = $table['table']['record'][0]['saja_bonus'];
                            // $ibon_bonus = floor($saja_bonus * $i_Magnification * $i_CurrencyRate);  // RMB 1 = NTD 5 = iBon 5*6	,並取整數避免ibon端不接受小數回傳							
                            $ibon_bonus = SajaBonusToIbon($saja_bonus,$country_code);
							
							$use_total = UseIbonTotal($userid);
							if ((int)$use_total < $can_use_total) {
								
								$i_STATUS_CODE = "0000";
								$i_STATUS_DESC = iconv("UTF-8","BIG5","成功");
								// $i_STATUS_DESC = "OK";
							} else {
								error_log("[ibon_process/UseIbonTotal] 091001 use_total = ${use_total}");
								$i_STATUS_CODE = "1004";
								$i_STATUS_DESC = iconv("UTF-8","BIG5","今日兌換已超過".$can_use_total."上限金額，所以無法進行對兌換!!");
							}
						}
						/*
						if($table['table']['record'][0]['verified']=='Y') {
                            $account_id = $table['table']['record'][0]['name'];
                            $userid = $table['table']['record'][0]['userid'];
                            $saja_bonus = $table['table']['record'][0]['saja_bonus'];
                            // $ibon_bonus = floor($saja_bonus * $i_Magnification * $i_CurrencyRate);  // RMB 1 = NTD 5 = iBon 5*6	,並取整數避免ibon端不接受小數回傳							
                            $ibon_bonus = SajaBonusToIbon($saja_bonus,$country_code);
                            $i_STATUS_CODE = "0000";
                            $i_STATUS_DESC = iconv("UTF-8","BIG5","成功");
                            // $i_STATUS_DESC = "OK";
                        } else {							
                            // $i_STATUS_CODE = "1003";
                            $i_STATUS_CODE = "1002";
                            // $i_STATUS_DESC = "This phone number is not identified !!";
                            $i_STATUS_DESC = iconv("UTF-8","BIG5","由於您的手機號尚未認證通過，暫時無法使用紅利金，請至殺價王會員中心進行手機認證！");
                        }
						*/
                    } else {
                        // $i_STATUS_CODE = "1003";
                        $i_STATUS_CODE = "1001";
                        $i_STATUS_DESC = iconv("UTF-8","BIG5","查無符合條件之帳號，請確認所輸入之帳號/密碼是否正確!!");
                        // $i_STATUS_DESC = "Incorrect Account/Password !!";

                    } 
                } else {
                    // $i_STATUS_CODE = "1003";
                    // $i_STATUS_DESC = "電文內容有誤-1。";
                    $i_STATUS_CODE = "1003";
                    $i_STATUS_DESC = iconv("UTF-8","BIG5","電文內容有誤。");
                    // $i_STATUS_DESC = "Error Request Message !!";
                }
                
                $e_STATUS_DESC = $i_STATUS_DESC;
                            
                $e_XMLData = reply091001($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$ibon_bonus);
                /* echo iconv("utf-8","big-5","<?xml version='1.0' encoding='Big5'?>".$e_XMLData); */
                echo $e_XMLData;
                /*
                $sql = "INSERT into saja_exchange.saja_exchange_ibon_log (i_BUSINESS, i_DETAIL_NUM, account_id, userid, ibon_bonus, state, adate, input, output, prdate_no) Values ('".$i_BUSINESS."', '".$i_DETAIL_NUM."', '".$account_id."', '".$userid."', '".$ibon_bonus."', '1', '".$YmdHis."', '".str_replace("<?xml version='1.0' encoding='Big5'?>","",$x_XMLData)."', '".str_replace("<?xml version='1.0' encoding='Big5'?>","",$e_XMLData)."', '".$i_prdate_no."')";
                */
                /*
                $sql = "INSERT into saja_exchange.saja_exchange_ibon_log (i_BUSINESS, i_DETAIL_NUM, account_id, userid, ibon_bonus, state, adate, input, output, prdate_no) Values ('".$i_BUSINESS."', '".$i_DETAIL_NUM."', '".$account_id."', '".$userid."', '".$ibon_bonus."', '1', '".$YmdHis."', '".mysqli_real_escape_string($db->_con, $x_XMLData)."', '".mysqli_real_escape_string($db->_con, $e_XMLData)."', '".$i_prdate_no."')";
                */
                if(empty($userid)) 
                   $userid=-1;

                $sql = "INSERT into saja_exchange.saja_exchange_ibon_log (i_BUSINESS, i_DETAIL_NUM, account_id, userid, ibon_bonus, state, adate, prdate_no) Values ('".$i_BUSINESS."', '".$i_DETAIL_NUM."', '".$account_id."', '".$userid."', '".$ibon_bonus."', '1', '".$YmdHis."', '".$i_prdate_no."')";
                error_log("[ibon_process]sql 3 :".$sql);
                // $db->query($sql);
                execCRUDSQL($sql,"TWN", false);
                // reply091001($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$ibon_bonus);
                exit;
                
            } else if( $i_BUSINESS == "091003") {

                $userid = "";
                $account_id = "";
                $curr_saja_bonus = 0;      // 現有saja bonus點數
                $nickname="";
                $i_RETURNCODE = "1";
                $needed_ibon_bonus=0;     // 所需ibon總共點數
                $saja_bonus_to_reduce=0;  // 須扣除saja bonus 點數
				$can_use_total = 300;    //可以兌換金額
                // $db=new mysql($config["db"]);
                // $db->connect();
                
                $sql = "SELECT * FROM saja_exchange.saja_exchange_ibon_log ";
                $sql .= "WHERE i_DETAIL_NUM = '".$i_DETAIL_NUM."' ";
                $sql .= "AND state = '1' ";
                $sql .= "ORDER BY adate DESC LIMIT 1";
                
                error_log("sql 4 :".$sql);
                
                // $table = $db->getQueryRecord($sql);
                $table = getQueryRecord($sql,'TWN');
                
                if (!empty($table['table']['record'])) {
                    $userid = $table['table']['record'][0]['userid'];
                    $account_id=$table['table']['record'][0]['account_id'];
					
					if(!is_allow($account_id)) {
					   $i_STATUS_CODE = "1010";
					   $i_RETURNCODE = "0";
                       $i_STATUS_DESC = iconv("UTF-8","BIG5","兌換帳號異常, 請洽客服 !");
					   $e_STATUS_DESC = $i_STATUS_DESC;
                       $e_XMLData = reply091003($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$i_XMLData,$i_RETURNCODE);
                       echo $e_XMLData;
                       exit;
					}
					
					if(is_exceed_deposit($account_id)) {
					   $i_STATUS_CODE = "1025";
					    $i_RETURNCODE = "0";
                       $i_STATUS_DESC = iconv("UTF-8","BIG5","帳號異常, 請洽客服 !");
					   $e_STATUS_DESC = $i_STATUS_DESC;
                       $e_XMLData = reply091003($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$i_XMLData,$i_RETURNCODE);
                       echo $e_XMLData;
                       exit;
					}
                    
                    // 依手機號格式確定國家
                    $country_code = chkPhoneCountry($account_id);
                    
                    // if ($i_STATUS_CODE=="0000" && $i_prdate_no!= "") {
                    if ($i_STATUS_CODE=="0000" && $i_prdate_no>0) {
                        $i_dt = array(
                            "SERIAL_NO"=>array(),
                            "DATA_1"=>array(),
                            "DATA_2"=>array(),
                            "DATA_3"=>array(),
                            "DATA_4"=>array(),
                            "DATA_5"=>array(),
                            "DATA_6"=>array(),
                            "DATA_7"=>array(),
                            "DATA_8"=>array()
                        );
                        
                        for($i=0; $i<(int)$i_XMLData->TOTAL_COUNT; $i++) {
                            $i_same = 0;
                            for($j=0; $j<count($i_dt["SERIAL_NO"]); $j++){
                                // 重複商品項目合併數量及金額
                                if($i_dt["SERIAL_NO"][$j] != (string)$i_XMLData->LISTDATA->DATA[$i]->SERIAL_NO &&
                                $i_dt["DATA_1"][$j] == (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_1 &&
                                $i_dt["DATA_2"][$j] == (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_2 &&
                                $i_dt["DATA_7"][$j] == (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_7)
                                {
                                    $i_dt["DATA_3"][$j] = (int)$i_dt["DATA_3"][$j] + (int)$i_XMLData->LISTDATA->DATA[$i]->DATA_3;
                                    $i_dt["DATA_4"][$j] = (int)$i_dt["DATA_4"][$j] + (int)$i_XMLData->LISTDATA->DATA[$i]->DATA_4;
                                    $i_same = 1;
                                }
                            }
                            
                            if($i_same == 0){
                                array_push($i_dt["SERIAL_NO"], (string)$i_XMLData->LISTDATA->DATA[$i]->SERIAL_NO);
                                array_push($i_dt["DATA_1"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_1);  // 商品編號
                                array_push($i_dt["DATA_2"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_2);  // 商品名稱 
                                array_push($i_dt["DATA_3"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_3);  // 兌換數量
                                array_push($i_dt["DATA_4"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_4);  // 所需點數
                                array_push($i_dt["DATA_5"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_5);
                                array_push($i_dt["DATA_6"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_6);
                                array_push($i_dt["DATA_7"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_7);
                                array_push($i_dt["DATA_8"], (string)$i_XMLData->LISTDATA->DATA[$i]->DATA_8);
                            }
                        }
                        
                        if(count($i_dt["SERIAL_NO"])>0) {
							$i_ibonus_all = 0;
							
							// 檢查各商品所需鯊魚點  如發現無資料或0者立刻設定為資料錯誤
							$data_error=false;
							for($x=0; $x<count($i_dt["SERIAL_NO"]); $x++) {
								$i_ac_code = (string)$i_XMLData->ITEM_TYPE;		//各項次活動代號
								$i_id = $i_dt["DATA_1"][$x]; 					//各項次產品代碼
								$i_num = $i_dt["DATA_3"][$x];                   //兌換數量
								$i_ibonus = (int)$i_dt["DATA_4"][$x];           //兌換所需ibon點數

								$sql = " SELECT * FROM saja_exchange.saja_exchange_ibon_prlist ";
								$sql .= " WHERE ac_code = '".$i_ac_code."' ";
								$sql .= " AND id = '".$i_id."' ";
								$sql .= " AND prdate_no = '".$i_prdate_no."' ";	
								
								$table = getQueryRecord($sql, $country_code);
								error_log("[lib/ibon_process] sql 4.5:".$sql);								
								if(!empty($table['table']['record'])) {
									$pr_price = empty($table['table']['record'][0]['bonus'])?0:$table['table']['record'][0]['bonus'];
									error_log("[lib/ibon_process] exchange 4.5 : ".$i_ac_code."|".$i_id."|".$pr_price);
									if(empty($pr_price) || $pr_price==0) {
									   $data_error = true;
									   break;										   
									}									
								} else {				
									$data_error = true;
									break;    
								}

								$i_ibonus_all += $i_ibonus;								
							}
							
							// 如果資料錯誤則立刻彈回
							if($data_error) {
								$i_STATUS_CODE = "1008";
								$i_STATUS_DESC = iconv("UTF-8","BIG5","查無商品資料！");
								$i_RETURNCODE = "0";
								$e_STATUS_DESC = $i_STATUS_DESC;									
								$e_XMLData = reply091003($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$i_XMLData,$i_RETURNCODE);
								echo $e_XMLData;
								exit;									
							} 
							
							// 檢查用戶是否有足夠鯊魚點	
                            $sql=" SELECT u.userid, u.name, up.nickname, SUM(IFNULL(b.amount,0)) as saja_bonus ";
                            $sql.=" FROM saja_user.saja_user u ";
                            $sql.=" LEFT JOIN saja_cash_flow.saja_bonus b ON u.userid = b.userid AND u.switch='Y' AND b.switch='Y' ";
                            $sql.=" LEFT JOIN saja_user.saja_user_profile up ON u.userid = up.userid AND u.switch='Y' AND up.switch='Y' ";
                            $sql.=" WHERE u.userid='".$userid."' ";
                            
                            error_log("[ibon_process]sql 5 :".$sql);
                            
                            // $table = $db->getQueryRecord($sql);
                            $table = getQueryRecord($sql, $country_code);
                            
                            if(!empty($table['table']['record'])) {
                                $account_id=$table['table']['record'][0]['name'];
                                $curr_saja_bonus = $table['table']['record'][0]['saja_bonus'];
								if($curr_saja_bonus<=0) {
									 $data_error=true;
									 $i_STATUS_CODE = "1007";
									 $i_STATUS_DESC = iconv("UTF-8","BIG5","紅利點數不足，兌換失敗！");
									 $i_RETURNCODE = "0";	
									 $e_STATUS_DESC = $i_STATUS_DESC;									
									 $e_XMLData = reply091003($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$i_XMLData,$i_RETURNCODE);
									 echo $e_XMLData;
									 exit;								   
								}
                                $nickname= $table['table']['record'][0]['nickname'];
                                // $curr_ibon_bonus = floor($curr_saja_bonus* $i_Magnification * $i_CurrencyRate); // User現有的殺幣轉換成等值的ibon點數 
                                $curr_ibon_bonus = SajaBonusToIbon($curr_saja_bonus,$country_code);

								$use_total = UseIbonTotal($userid);
								error_log("[ibon_process] 091003 use_total :".$use_total.", i_ibonus_all :".$i_ibonus_all);
								if ($use_total < $can_use_total && (((int)$i_ibonus_all+(int)$use_total) <= $can_use_total)) {
									// 由紅利點數計算出等值的ibon點數足夠才可兌換
									// if($curr_ibon_bonus >= $needed_ibon_bonus) {
									if(true) {
										$i_STATUS_CODE = "0000";
										// $i_STATUS_DESC = "成功";
										$i_STATUS_DESC = "OK";
										$i_RETURNCODE = "1";
										for($x=0; $x<count($i_dt["SERIAL_NO"]); $x++) {
											$i_ac_code = (string)$i_XMLData->ITEM_TYPE;		//各項次活動代號
											$i_id = $i_dt["DATA_1"][$x]; 					//各項次產品代碼
											// $i_prname = $i_dt["DATA_2"][$x];             //各項次產品名稱
											$i_num = $i_dt["DATA_3"][$x];                   //兌換數量
											$i_ibonus = (int)$i_dt["DATA_4"][$x];           //兌換所需ibon點數
											  
											 
											$sql = " SELECT * FROM saja_exchange.saja_exchange_ibon_prlist ";
											$sql .= " WHERE ac_code = '".$i_ac_code."' ";
											$sql .= " AND id = '".$i_id."' ";
											$sql .= " AND prdate_no = '".$i_prdate_no."' ";
										
											error_log("[lib/ibon_process] exchange: ".$i_ac_code."|".$i_id."|".$i_num."|".$i_ibonus."|".$i_prname);	
											error_log("[lib/ibon_process] sql 7:".$sql);
											
											// $table = $db->getQueryRecord($sql);
											$table = getQueryRecord($sql,'TWN');
											
											if(!empty($table['table']['record'])) {
												//避免使用傳過來的總價
												//用傳來的商品數量 * 資料庫中的商品單價=總價 
												/*
												$pr_price = $table['table']['record'][0]['price'];  		    // 商品單價
												$pr_price_total = $i_num * $pr_price;							    // 商品總金額
												$saja_bonus_to_reduce=round($pr_price_total/$i_CurrencyRate,2); // 總金額換算成Saja bonus;
												
												*/
												$i_prname = $table['table']['record'][0]['pname']; 
												// 20180717 改為讀取bonus欄位作為單價
												$pr_price = $table['table']['record'][0]['bonus'];  		  // 商品單價
												// 如果撈不到商品資料則立刻跳出
												if(empty($pr_price)) {
												   $i_STATUS_CODE = "1008";
												   $i_STATUS_DESC = iconv("UTF-8","BIG5","查無商品資料！");
												   // $i_STATUS_DESC = "No bonus data !!";
												   $i_RETURNCODE = "0";
												   break;											   
												}
												$pr_price_total = $i_num * $pr_price;						  // 商品所需點數
												// $saja_bonus_to_reduce=floor($pr_price_total);       
												$saja_bonus_to_reduce=IbonToSajaBonus($pr_price_total,$country_code);
												
												// saja 紅利兌換紀錄
												$sql = "INSERT INTO saja_cash_flow.saja_bonus (prefixid, userid, countryid, behav, amount, seq, switch, insertt) ";
												$sql.= " values ('saja','".$userid."','2','ibon_exchange',".($saja_bonus_to_reduce*(-1)).",0,'Y',NOW()) ";
												error_log("[ibon_process]sql 8:".$sql);
												// $db->query($sql);
												//$bonusid=$db->_con->insert_id;
												$bonusid=execCRUDSQL($sql, $country_code,true);
												
												//ibon商品兌換紀錄
												$sql = " INSERT into saja_exchange.saja_exchange_ibon_record (bonusid, id, code, prname, detail_num, u_id, u_date, tr_num, tr_ibons, tr_price, prdate_no) ";
												$sql.= " VALUES ('".$bonusid."', '".$i_id."', '".$i_ac_code."', '".$i_prname."', '".$i_DETAIL_NUM."', '".$userid."', '".$YmdHis."', '".$i_num."', ".$i_ibonus.", ".$pr_price_total.", '".$i_prdate_no."'); ";
												error_log("[ibon_process]sql 9:".$sql);
												// $db->query($sql);
												execCRUDSQL($sql, 'TWN', false);
												/*
												$sql = " INSERT into saja_exchange.saja_exchange_vendor_record (bonusid, prodid, prod_name, tx_code, userid, commit_time, tx_quantity, total_bonus, total_price) ";
												$sql.= " VALUES ('".$bonusid."', '".$i_ac_code."-".$i_id."', '".$i_prname."', '".$i_DETAIL_NUM."', '".$userid."', '".$YmdHis."', '".$i_num."', ".$i_ibonus.", ".$pr_price_total."); ";
												*/
												$sql = "UPDATE saja_exchange.saja_exchange_ibon_prlist SET used=used+".$i_num." WHERE ac_code='".$i_ac_code."' AND id='".$i_id."' AND prdate_no='".$i_prdate_no."' ;" ;
												error_log("[ibon_process]sql 10 :".$sql); 
												// $db->query($sql);
												execCRUDSQL($sql, 'TWN', false);


												// 計算免費鯊魚點餘額並更新
												$sql = "SELECT sum(free_amount) total_free_amount
													FROM `saja_cash_flow`.`saja_spoint_free_history`
													WHERE `userid`='{$userid}'
													AND   `switch` = 'Y'
													AND   `amount_type` = 2
													";
												$table = getQueryRecord($sql,'TWN');
												error_log("[ibon_process]sql 11 :".$sql); 
												
												if ($table['table']['record'][0]['total_free_amount'] > 0) {
													$total_fee = $saja_bonus_to_reduce;
													$free_amount = ($table['table']['record'][0]['total_free_amount'] >= $total_fee) ? $total_fee : $table['table']['record'][0]['total_free_amount'];
												   
													$sql = "INSERT INTO `saja_cash_flow`.`saja_spoint_free_history` 
													SET
														`userid`='{$userid}',
														`behav` = 'ibon_exchange', 
														`amount_type` = 2, 
														`free_amount` = '-{$free_amount}', 
														`total_amount` = '-{$total_fee}', 
														`bonusid` = '{$bonusid}', 
														`switch` = 'Y', 
														`insertt` = now()
													";
													execCRUDSQL($sql,'TWN', true);
													error_log("[ibon_process]sql 12 :".$sql); 
												}

											} else {
												$i_STATUS_CODE = "1008";
												$i_STATUS_DESC = iconv("UTF-8","BIG5","查無商品資料！");
												// $i_STATUS_DESC = "No bonus data !!";
												$i_RETURNCODE = "0";	
												
											}
											
											
											
										}
										/*
										$i_STATUS_CODE = "0000";
										// $i_STATUS_DESC = "成功";
										$i_STATUS_DESC = "OK";
										$i_RETURNCODE = "1";
										*/
										/*
										$saja_bonus_to_reduce=round($i_total_price/$i_CurrencyRate,2);   // 將總金額(當地幣值)換算成人民幣(RMB,四捨五入到小數二位), 即為所扣除的紅利數量
										$sql = "INSERT INTO saja_cash_flow.saja_bonus (prefixid, userid, countryid, behav, amount, seq, switch,insertt) values ('saja','".$userid."','2','ibon_exchange',".$saja_bonus_to_reduce*(-1.00).",0,'Y',NOW()) ";
										error_log("[ibon_process]sql 9 :".$sql); 
										$db->query($sql);
										*/
									} else {
										// $i_STATUS_CODE = "1003";
										// $i_STATUS_DESC = "紅利點數不足，兌換失敗!!請使用殺價王站內信洽詢客服人員。";
										$i_STATUS_CODE = "1007";
										$i_STATUS_DESC = iconv("UTF-8","BIG5","紅利點數不足，兌換失敗！");
										// $i_STATUS_DESC = "Not Enough Bonus !!";
										$i_RETURNCODE = "0";
									}
									
								} else {
									$i_STATUS_CODE = "1004";
									$i_STATUS_DESC = iconv("UTF-8","BIG5","今日兌換超過".$can_use_total."上限金額，所以無法進行對兌換!!");
									$i_RETURNCODE = "0";
								}
								
                            } else {
                                // $i_STATUS_CODE = "1003";
                                // $i_STATUS_DESC = "電文內容有誤-1。";
                                $i_STATUS_CODE = "1006";
                                $i_STATUS_DESC = iconv("UTF-8","BIG5","查無紅利點數資料！");
                                // $i_STATUS_DESC = "No bonus data !!";
                                $i_RETURNCODE = "0";
                            }
                        }
                    } else {
                        // $i_STATUS_CODE = "1003";
                        // $i_STATUS_DESC = "電文內容有誤-2。";
                        $i_STATUS_CODE = "1005";
                        $i_STATUS_DESC = iconv("UTF-8","BIG5","暫無商品可進行紅利兌換！");
                        // $i_STATUS_DESC = "No available products for exchange !";
                        $i_RETURNCODE = "0";
                    }							
                } else {
                    // $i_STATUS_CODE = "1003";
                    // $i_STATUS_DESC = "電文內容有誤-3。";
                    $i_STATUS_CODE = "1004";
                    $i_STATUS_DESC = iconv("UTF-8","BIG5","紅利點數查詢作業異常。");
                    // $i_STATUS_DESC = "Error bonus information !!";
                    $i_RETURNCODE = "0";
                }
                
            } else {
                // $i_STATUS_CODE = "1003";
                // $i_STATUS_DESC = "電文內容有誤-4。";
                $i_STATUS_CODE = "1003";
                $i_STATUS_DESC = iconv("UTF-8","BIG5","電文內容有誤。");
                // $i_STATUS_DESC = "Error request message !!";
                $i_RETURNCODE = "0";
            } 
            
            $e_STATUS_DESC = $i_STATUS_DESC;
            
            $e_XMLData = reply091003($i_STOREID,$i_SHOPID,$i_DETAIL_NUM,$i_STATUS_CODE,$e_STATUS_DESC,$i_XMLData,$i_RETURNCODE);
            
            /* echo iconv("utf-8","big-5","<?xml version='1.0' encoding='Big5'?>".$e_XMLData); */
            echo $e_XMLData;
            /*
            
            $e_XMLData = "<?xml version='1.0' encoding='Big5'?><CONFIRMDATA_R><BUSINESS>091004</BUSINESS><STOREID>".$i_STOREID."</STOREID><SHOPID>".$i_SHOPID."</SHOPID><DETAIL_NUM>".$i_DETAIL_NUM."</DETAIL_NUM><STATUS_CODE>".$i_STATUS_CODE."</STATUS_CODE><STATUS_DESC>".$e_STATUS_DESC."</STATUS_DESC><CARDTYPE>".(string)$i_XMLData->CARDTYPE."</CARDTYPE><ITEM_TYPE>".(string)$i_XMLData->ITEM_TYPE."</ITEM_TYPE><RETURNCODE>".$i_RETURNCODE."</RETURNCODE></CONFIRMDATA_R>";
            
            echo iconv("utf-8","big-5","<?xml version='1.0' encoding='Big5'?><CONFIRMDATA_R><BUSINESS>091004</BUSINESS><STOREID>".$i_STOREID."</STOREID><SHOPID>".$i_SHOPID."</SHOPID><DETAIL_NUM>".$i_DETAIL_NUM."</DETAIL_NUM><STATUS_CODE>".$i_STATUS_CODE."</STATUS_CODE><STATUS_DESC>".$e_STATUS_DESC."</STATUS_DESC><CARDTYPE>".(string)$i_XMLData->CARDTYPE."</CARDTYPE><ITEM_TYPE>".(string)$i_XMLData->ITEM_TYPE."</ITEM_TYPE><RETURNCODE>".$i_RETURNCODE."</RETURNCODE></CONFIRMDATA_R>");
        
            */
            
            $e_remaining_point = ((int)$curr_ibon_bonus-(int)$needed_ibon_bonus);
            
            /*
            $sql = "INSERT into saja_exchange.saja_exchange_ibon_log (i_BUSINESS, i_DETAIL_NUM, userid, account_id, ibon_bonus, state, adate, input, output, prdate_no) Values ('".$i_BUSINESS."', '".$i_DETAIL_NUM."', '".$userid."', '".$account_id."', '".$e_remaining_point."', '2', '".$YmdHis."', '".str_replace("<?xml version='1.0' encoding='Big5'?>","",$x_XMLData)."', '".str_replace("<?xml version='1.0' encoding='Big5'?>","",$e_XMLData)."', '".$i_prdate_no."')";
            */
            /*
            $sql = "INSERT into saja_exchange.saja_exchange_ibon_log (i_BUSINESS, i_DETAIL_NUM, userid, account_id, ibon_bonus, state, adate, input, output, prdate_no) Values ('".$i_BUSINESS."', '".$i_DETAIL_NUM."', '".$userid."', '".$account_id."', '".$e_remaining_point."', '2', '".$YmdHis."', '".mysqli_real_escape_string($db->_con, $x_XMLData)."', '".mysqli_real_escape_string($db->_con, $e_XMLData)."', '".$i_prdate_no."')";
            */
            $sql = "INSERT into saja_exchange.saja_exchange_ibon_log (i_BUSINESS, i_DETAIL_NUM, userid, account_id, ibon_bonus, state, adate, prdate_no) Values ('".$i_BUSINESS."', '".$i_DETAIL_NUM."', '".$userid."', '".$account_id."', '".$e_remaining_point."', '2', '".$YmdHis."', '".$i_prdate_no."')";
            error_log("[ibon_process]sql 11 :".$sql);
            
            // $db->query($sql);
            execCRUDSQL($sql, 'TWN', false);
            
            
        } 
    } catch (Exception $e) {
	    error_log("[ibon_process] error : ".$e->getMessage());
	}
} else {
   error_log("[ibon_process] Empty XML Data !!");   
}
exit;
?>
