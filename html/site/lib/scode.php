<?php
//掃碼開通 超級殺價券

class scode_lib {

	//S碼開通
	public function act_scode($userid, $var)
	{
		global $db, $config, $scodeModel;
		
		$chk_var = false;
		$insert_var = false;
		$ip = GetIP();
		
		//回傳:
		$ret['status']=1;
		$ret['retCode']=1;
		$ret['retMsg']="OK";
		$ret['retType']="LIST";
		$ret['action_list']=array(
				"type"=>5,
				"page"=>9,
				"msg"=>"NULL"
			);
		
		if (empty($var['scsn']) || $var['scsn']=='NULL' || $var['scsn']=='null' ){
			$ret['status'] = -1001;
			$ret['action_list']["msg"]="序號或密碼 不可空白 !";
		} 
		else if (empty($var['scpw']) ){
			$ret['status'] = -10010;
			$ret['action_list']["msg"]="序號或密碼 不可空白 !";
		} else {
			
			$query = "SELECT *, unix_timestamp(offtime) as offtime, unix_timestamp() as `now` 
				FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_item`
				WHERE
					`prefixid` = '{$config['default_prefix_id']}'
					AND `serial` = '{$var['scsn']}'
					AND `switch` = 'Y'
				";
			$table = $db->getQueryRecord($query);
			$scode_item = isset($table['table']['record'][0]) ? $table['table']['record'][0] : '';

			if(empty($scode_item)) {
				//'不存在'
				$ret['status'] = -1002;
				$ret['action_list']["msg"]="此組序號不存在 !";
			} 
			elseif($var['scpw'] !== $scode_item['pwd'] ) {
				//'密碼錯誤'
				$ret['status'] = -1003;
				$ret['action_list']["msg"]="序號密碼錯誤 !";
			} 
			elseif($scode_item['verified'] == 'Y') {
				// 已開通
				$ret['status'] = -1004;
				$ret['action_list']["msg"]="此組序號已被使用 !";
			} 
			elseif($scode_item['offtime'] < $scode_item['now']) {
				//超時不可激活
				$ret['status'] = -1005;
				$ret['action_list']["msg"]="此組序號已過期 !";
			} else { 
				$insert_var = TRUE;
			}
		}
		
		if($insert_var) {
			//S碼激活
			$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_item`
			SET `verified`='Y', userid='{$userid}'
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND serial = '{$var['scsn']}'";
			$db->query($query);

			//實體S碼序號
			$scode_promote['spid'] = $scode_item['spid'];
			$scode_promote['behav'] = 'e';
			$scode_promote['promote_amount'] = 1;
			$scode_promote['batch'] = $scode_item['batch'];
			$scode_promote['num'] = $scode_item['amount'];

			//插入S碼收取記錄
			$scodeModel->insert_scode($scode_promote, $userid, $var['scsn']);

			//回傳:
			$ret['action_list']["msg"] = "序號 ". $var['scsn'] ." 成功開通 !";
			
			$log = date('Ymd H:i:s') .' - '. $userid .'_'. $var['scsn'];
			error_log("[lib/scode_lib/act_scode] ". $log);
		}
		
		return $ret;
	}
	
}
?>