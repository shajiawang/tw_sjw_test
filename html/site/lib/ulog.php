<?php
$noneedlogip=array('211.75.238.38','211.75.238.39','211.75.238.40','211.75.238.41','211.75.238.42');
if (in_array($argv[1],$noneedlogip)) die();
$noneedloguid=array('1705','28');
if (in_array($argv[2],$noneedloguid)) die();

if (($argv[1]=='210.71.254.62') &&($argv[2]==-1)) die();

if (preg_match('/bot|spider|crawler|curl|^$/i', $argv[4])>0) die();
require_once('/var/www/html/site/lib/config.php');
$dbhost = $config["db"][0]['host'];
$dbuser = $config["db"][0]['username'];
$dbpasswd = $config["db"][0]["password"];
$dbname = $config["db"][4]["dbname"];
$dsn = "mysql:host=".$dbhost.";dbname=".$dbname;
$conn = new PDO($dsn,$dbuser,$dbpasswd);
$conn->exec("SET CHARACTER SET utf8");
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

//移除前一日的數據
$yday = date("Y-m-d", strtotime("-1 day"));
$input = array(':atday' => "{$yday}" );
$ysql = "SELECT * FROM `saja_user`.`saja_user_detail_online` WHERE `atday`=:atday";
$sth = $conn->prepare($ysql);
$sth->execute($input);
$yresult = $sth->fetch(PDO::FETCH_ASSOC);
//error_log("[ulog]yresult: ". json_encode($yresult) ); 
if(sizeof($yresult)>0){
	$conn->exec("DELETE FROM `saja_user`.`saja_user_detail_online` WHERE `atday`='{$yday}' ");
}

if (sizeof($argv)>4){
    $ip=urldecode($argv[1]);
    $uid=urldecode($argv[2]);
    $uri=urldecode($argv[3]);
    $user_agent=urldecode($argv[4]);
    $request=empty($argv[5])? 0 : urldecode($argv[5]);

    $insertt=date("Y-m-d H:i:s");

    $input=array(
                ':userid' => "{$uid}",
                ':ip' => "{$ip}",
                ':uri' => "{$uri}",
                ':request' => "{$request}",
                ':user_agent' => "{$user_agent}",
                ':insertt' => "{$insertt}");

    $sql = "INSERT INTO `saja_user`.`saja_user_detail_log`  SET  uid = :userid, ip = :ip, uri = :uri, request = :request, user_agent = :user_agent, insertt=:insertt";
    $sth = $conn->prepare($sql);
    $sth->execute($input);
	
	$userid = (intval($uid)>0) ? $uid : 0;
	if($userid>0){
		$input = array(':userid' => "{$userid}" );
		$tsql = "SELECT * FROM `saja_user`.`saja_user_detail_online` WHERE `userid`=:userid AND `userid`>0";
		//error_log("[ulog]userid:{$userid} , tsql: ". $tsql );
		$sth = $conn->prepare($tsql);
		$sth->execute($input);
		$tresult = $sth->fetch(PDO::FETCH_ASSOC);
		//error_log("[ulog]tresult: ". json_encode($tresult) );
		if(!$tresult){
			//Insert 今日的
			$td = date("Y-m-d");
			$tt = date("H:i:s");
			$c = $conn->exec("INSERT INTO `saja_user`.`saja_user_detail_online` (userid, atday, attime) VALUES('{$userid}', '{$td}', '{$tt}')");
			error_log("[ulog]user_detail_online: ". json_encode($c) .", userid:{$userid}");
		}
	}

}
?>