<?php
   class ret {
         var $retCode ;
		 var $retMsg ;
		 var $retType ;
		 var $retObj ;
		 /*	
         public function __construct(){
             $this->retCode=1;
             $this->retMsg="OK";
             $this->retType="";
			 $this->retObj=null;
         }
		 */
		 public function __construct($code=1, $msg="OK", $type='MSG', $obj=array()){
             error_log("initializing ret class...");
			 $this->retCode=$code;
             $this->retMsg=$msg;
             $this->retType=$type;
			 $this->retObj=$obj;
         }
		 
         function setRetCode($code) {
		      $this->retCode=$code;
		 }
		 function getRetCode() {
		      return $this->retCode;
		 }
         function setRetMsg($msg) {
		      $this->retMsg=$msg;
		 }
		 function getRetMsg() {
		      return $this->retMsg;
		 }
		 function setRetType($type) {
		      $this->retType=$type;
		 }
		 function getRetType() {
		      return $this->retType;
		 }
		 function setRetObj($obj) {
		      $this->retObj=$obj;
		 }
		 function getRetObj() {
		      return $this->retObj;
		 }
		 function toJSON() {
		      $r=json_encode(array('ret'=>$this));
		      error_log("ret.class:".$r);
			  return $r;
		 }
   }
?>

