﻿<?php
// PHP version of merchant.jsp
//这是B2CAPI通用版的php客户端调用测试
//作    者：bocomm
//创建时间：2012-4-10
function isMobileBrowser() {
	$mobile_browser = '0';
	 
	if(preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
		$mobile_browser++;
	}
	 
	if((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml')>0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
		$mobile_browser++;
	}    
	 
	$mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'],0,4));
	$mobile_agents = array(
		'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
		'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
		'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
		'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
		'newt','noki','oper','palm','pana','pant','phil','play','port','prox',
		'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
		'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
		'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
		'wapr','webc','winw','winw','xda','xda-','Googlebot-Mobile');
	 
	if(in_array($mobile_ua,$mobile_agents)) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['ALL_HTTP']),'OperaMini')>0) {
		$mobile_browser++;
	}
	 
	if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'windows')>0) {
		$mobile_browser=0;
	}
    
	 if($mobile_browser>0)
	    return true;
    else 
	    return false;
	
} 

?>
<?php
	require_once("config.inc");
	//获得表单传过来的数据
	$interfaceVersion = $_REQUEST["interfaceVersion"];		
	$merID = $merchID; //商户号为固定	
	$orderid = $_REQUEST["orderid"];
	$orderDate = $_REQUEST["orderDate"];
	$orderTime = $_REQUEST["orderTime"];
	$tranType = $_REQUEST["tranType"];
	$amount = $_REQUEST["amount"];
	$curType = $_REQUEST["curType"];
	$orderContent = $_REQUEST["orderContent"];
	$orderMono = $_REQUEST["orderMono"];
	$phdFlag = $_REQUEST["phdFlag"];
	$notifyType = $_REQUEST["notifyType"];
	$merURL = $_REQUEST["merURL"];
	$goodsURL = $_REQUEST["goodsURL"];
	$jumpSeconds = $_REQUEST["jumpSeconds"];
	$payBatchNo = $_REQUEST["payBatchNo"];
	$proxyMerName = $_REQUEST["proxyMerName"];
	$proxyMerType = $_REQUEST["proxyMerType"];
	$proxyMerCredentials = $_REQUEST["proxyMerCredentials"];
	$netType = $_REQUEST["netType"];
	$issBankNo = $_REQUEST["issBankNo"];
	$tranCode = "cb2200_sign";
?>
<?php
	//額外的檢測
	// Add By Thomas 150917 for data consistency check
	require_once("../convertString.ini.php");
	require_once("../config.php");
	if(floatval($_REQUEST["amount"])<0) {
	   echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Pay Amount Error !!");history.back();</script></body></html>';
	   exit;   
	}
	if(empty($_REQUEST['chkStr'])) {
		echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Empty Security Data !!");history.back();</script></body></html>';
		exit;	
	}
	$cs=new convertString();
	$chkStr=$cs->strDecode($_REQUEST['chkStr'],$config["encode_key"],$config["encode_type"]);
	error_log("[lib/bankcomm/merchant] chkStr : ".$chkStr);
	error_log("[lib/bankcomm/merchant] pay data : ".$_REQUEST["orderid"]."|".$_REQUEST["amount"]);
	$chkArr=explode("|",$chkStr);
	if(is_array($chkArr)) {
	   $chk_orderid=$chkArr[0];
	   $chk_amount=$chkArr[1];
	   if(floatval($chk_amount)!=floatval($_REQUEST["amount"])) {
		   echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Total Fee Check Error !!");history.back();</script></body></html>';
		   exit;
	   } 
	   if($chk_orderid!=$_REQUEST["orderid"]) {
		  echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Order ID. Check Failed !!");history.back();</script></body></html>';
		  exit;	
	   }
	} else {
	   echo '<!DOCTYPE><html><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><body><script>alert("Security Data Parse Error !!");history.back();</script></body></html>';
	   exit;
	}
	// Add End 
?>
<?php	
	//Mono中加入bank代號供後續判斷
	$orderMono = $orderMono."/bank:".$issBankNo;
	
	$interfaceVersion="1.0.2.0";
	$source = "";
	
	//htmlentities($orderMono,"ENT_QUOTES","GB2312");
	//连接字符串
	$source = $interfaceVersion."|".$merID."|".$orderid."|".$orderDate."|".$orderTime."|".$tranType."|"
	.$amount."|".$curType."|".$orderContent."|".$orderMono."|".$phdFlag."|".$notifyType."|".$merURL."|"
	.$goodsURL."|".$jumpSeconds."|".$payBatchNo."|".$proxyMerName."|".$proxyMerType."|".$proxyMerCredentials."|".$netType;

    // $src_encode=mb_detect_encoding($data);
	
	  error_log($source);
	
	//连接地址
	$socketUrl = "tcp://".$socket_ip.":".$socket_port;
	$fp = stream_socket_client($socketUrl, $errno, $errstr, 30);
	$retMsg="";
	//
	if (!$fp) {
		echo "$errstr ($errno)<br />\n";
	} else 
	{
		$in  = "<?xml version='1.0' encoding='UTF-8'?>";
		$in .= "<Message>";
		$in .= "<TranCode>".$tranCode."</TranCode>";
		$in .= "<MsgContent>".$source."</MsgContent>";
		$in .= "</Message>";
        //error_log("bankcomm request=".$in);
		//$in=mb_convert_encoding($in, "UTF-8", mb_detect_encoding($in));
		fwrite($fp, $in);
		while (!feof($fp)) {
			$retMsg =$retMsg.fgets($fp, 1024);
			
		}
		fclose($fp);
	}	
    //error_log("bankcomm retMsg=".$retMsg);
	//解析返回xml
	$dom = new DOMDocument;
	$dom->loadXML($retMsg);

	$retCode = $dom->getElementsByTagName('retCode');
	$retCode_value = $retCode->item(0)->nodeValue;
	
	$errMsg = $dom->getElementsByTagName('errMsg');
	$errMsg_value = $errMsg->item(0)->nodeValue;

	$signMsg = $dom->getElementsByTagName('signMsg');
	$signMsg_value = $signMsg->item(0)->nodeValue;

	$orderUrl = $dom->getElementsByTagName('orderUrl');
	$orderUrl_value = $orderUrl->item(0)->nodeValue;

	/*
	if($issBankNo=='BOCOM' &&  isMobileBrowser()) {
	  $orderUrl_value="http://wap.95559.com.cn/mobilebank/B2CPayment";
    }
	*/

	if($retCode_value != "0")
       {
            echo "交易返回码：".$retCode_value."<br>";
            echo "交易错误信息：" .$errMsg_value."<br>";
       }
       else
       {

?> 

<html>
    <head>
        <title>银联在线支付</title>
        <meta http-equiv = "Content-Type" content = "text/html;charset=GBK">
    </head>

	<body bgcolor = "#FFFFFF" text = "#000000" onload="form1.submit()">
        <form name = "form1" method = "post" action = "<?php echo($orderUrl_value); ?>">
            <input type = "hidden" name = "interfaceVersion" value = "<?php echo($interfaceVersion); ?>">
            <input type = "hidden" name = "merID" value = "<?php echo($merchID); ?>">
			<input type = "hidden" name = "orderid" value = "<?php echo($orderid); ?>">
            <input type = "hidden" name = "orderDate" value = "<?php echo($orderDate); ?>">
            <input type = "hidden" name = "orderTime" value = "<?php echo($orderTime); ?>">
            <input type = "hidden" name = "tranType" value = "<?php echo($tranType); ?>">
            <input type = "hidden" name = "amount" value = "<?php echo($amount); ?>">
            <input type = "hidden" name = "curType" value = "<?php echo($curType); ?>">
            <input type = "hidden" name = "orderContent" value = "<?php echo($orderContent); ?>">
            <input type = "hidden" name = "orderMono" value = "<?php echo($orderMono); ?>">
            <input type = "hidden" name = "phdFlag" value = "<?php echo($phdFlag); ?>">
            <input type = "hidden" name = "notifyType" value = "<?php echo($notifyType); ?>">
            <input type = "hidden" name = "merURL" value = "<?php echo($merURL); ?>">
            <input type = "hidden" name = "goodsURL" value = "<?php echo($goodsURL); ?>">
            <input type = "hidden" name = "jumpSeconds" value = "<?php echo($jumpSeconds); ?>">
            <input type = "hidden" name = "payBatchNo" value = "<?php echo($payBatchNo); ?>">
            <input type = "hidden" name = "proxyMerName" value = "<?php echo($proxyMerName); ?>">
            <input type = "hidden" name = "proxyMerType" value = "<?php echo($proxyMerType); ?>">
            <input type = "hidden" name = "proxyMerCredentials" value = "<?php echo($proxyMerCredentials); ?>">
            <input type = "hidden" name = "netType" value = "<?php echo($netType); ?>">
            <input type = "hidden" name = "merSignMsg" value = "<?php echo($signMsg_value); ?>">
            <input type = "hidden" name = "issBankNo" value = "<?php echo($issBankNo); ?>">
		
       </form>
    </body>
 
</html>
<?php
	}
?>