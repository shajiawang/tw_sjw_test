<?php
/**
 * Template Model
 *
 * This class contains all of the functions used for rendering the HTML templates from
 * the various view files.
 */
include_once(LIB_DIR."/cache.php");

class TemplateModel 
{
    public $variables = array();
    public $title;
    public $msg;
    public $msg_type;
	public $model; 
	public $action;
	public $page_id;
	public $page_header;
	public $page_footer;
	public $page_action;
	
    /**
     * Simple function used to load template views.  If only a view/model is specified,
     * load only the file from the base template directory.  If an action is specified,
     * the file/model name is expected to also be the name of the folder in which the
     * actual view file is being held.
     */
    public function loadView($filename, $action = null)
	{
        global $tpl, $config, $user; 

		// If an action is specified, include the specific action.
		$file = BASE_DIR ."/view/". $filename;  
        
        if($action) {
            $file .= '/'. $action;
        }
        $file .= ".php";

        // Load the view file only if it exists.
        if(file_exists($file)) { 
		   include_once $file;
        }
    }
    
    /**
     * Renders default template views, based on the model and action supplied (including
     * header and footer views).
     * @param   $model              The name of the model file
     * @param   $action             When specified, name of the file ($file used as dir name)
     * @param   $html               When specified, name of the file ($file used as dir name)
     * @param   $caching_enabled    (optional): When specified, name of the file ($file used as dir name)
     */
    public function render($model, $action = null, $html = false, $caching_enabled = false, $app = null)
	{
		if($model == 'dream_event' && $action == 'home'){
			$this->loadView("head-event");
		} 
		else if ($action == 'iframe'){
			$this->loadView("head2");
		}
		else{
			$this->loadView("head");
		}
		
		if($model == 'dream_event' && $action == 'home'){
			$this->loadView("header-event");                      //圓夢碼首頁
        }
		else if($model == 'site' && $action == 'home'){
			$this->loadView("header-home");                      //首頁樣式
        } 
		/* else if (($model == 'product' && $action == 'home') || ($model == 'mall' && $action == 'home') || ($model == 'member' && $action == 'home') || ($model == 'product' && $action == 'search')) {*/
		else if ($model == 'product' && $action == 'search') {
            $this->loadView("header2");                          //4大類home頁
		} else if ($model == 'enterprise') {
		    if(($action!='login') && ($action!='loginto')){
			   $this->loadView("enterprise_header");             //商家
			}   
		} else if ($model == 'site' && ($action == 'login' || $action == 'login2')) {
			   $this->loadView("header-login");             	//登入頁
		} else {
			if($app!='Y'){
				$this->loadView("header");                       //其他 
			}
		}
        
        // Start caching everything rendered.  We start this after the
        // header, since the header may contain user session information
        // that shouldn't be cached.
        if($caching_enabled) {
            $cache = new CacheModel;
            $cache->start();
        }

        // Add a container DIV to the view HTML about to be inclued.
        if($html) {
//            echo "\n";
//			echo '<div data-role="content" class="ui-content2">';
			echo "\n";
			echo '<div class="'. $model .'">';
			echo "\n";
        }

        // Load this specific view
		$this->model = $model;
		$this->action = $action;
        $this->loadView($this->model, $this->action);
		
        // Close the container DIV.
        if($html) {
			echo "\n";
			echo '</div>';
			echo "\n";
			echo '</div><!--data-role="content"-->';
			echo "\n";
		}

		if($app!='Y'){
			if($model=='enterprise') {
				$this->loadView("enterprise_footer");
			} else {
				if($model == 'dream_event' && $action == 'home') { 
				} else{
					$this->loadView("footer");
				}
			}			
		}
		
		if($model=='enterprise') {
		    $this->loadView("enterprise_panel");
		} else {
			$this->loadView("panel");
        }
        // Stop caching.
        if($caching_enabled) {
            $cache->end();
        }
    }

    /**
     * Renders default template views, based on the model and action supplied (including
     * header and footer views).
     * @param   $model              The name of the model file
     * @param   $action             When specified, name of the file ($file used as dir name)
     * @param   $html               When specified, name of the file ($file used as dir name)
     * @param   $caching_enabled    (optional): When specified, name of the file ($file used as dir name)
     */
    public function render2($model, $action = null, $html = false, $caching_enabled = false, $app = null)
	{
        // Start caching everything rendered.  We start this after the
        // header, since the header may contain user session information
        // that shouldn't be cached.
        if($caching_enabled) {
            $cache = new CacheModel;
            $cache->start();
        }

        // Add a container DIV to the view HTML about to be inclued.
        if($html) {
//            echo "\n";
//			echo '<div data-role="content" class="ui-content2">';
			echo "\n";
			echo '<div class="'. $model .'">';
			echo "\n";
        }

        // Load this specific view
		$this->model = $model;
		$this->action = $action;
        $this->loadView($this->model, $this->action);
		
        // Close the container DIV.
        if($html) {
			echo "\n";
			echo '</div>';
			echo "\n";
			echo '</div><!--data-role="content"-->';
			echo "\n";
		}

        // Stop caching.
        if($caching_enabled) {
            $cache->end();
        }
    }	
	
	
     /**
     * Used to assign variables that can be used in the template files.
     * @param   $name       Name of the variable to be assigned
     * @param   $value      String or Array object
     */
    public function assign($name, $value)
	{
        $this->variables[$name] = $value;
    }
	
	public function set_page_id($page_id)
	{
        $this->page_id = $page_id;
    }

	public function set_page_header($page_model, $page_action)
	{
		include(LIB_DIR."/page_header.php"); 
		$this->page_header = empty($page_header[$page_model][$page_action]) ? $page_header['site']['home'] : $page_header[$page_model][$page_action];
		$this->page_footer = $page_model;
		$this->page_action = $page_action;
		
		//print_r($this->page_footer);
		//exit;
	}
    
    /**
     * Used to assign the page title of the rendered HTML file.
     */
    public function set_title($title)
	{
        $this->title = $title;
    }

     /**
     * This function prints the page title that has been set in the controller,
     * should only be used in the header view.
     */
    public function page_title()
	{
        $str = ($this->title) ? $this->title .' '. APP_NAME : APP_NAME; 
		// .' - '. APP_KEYWORDS;
        
		echo $str;
    }

     /**
     * Set any status or error messages to be passed into the view files.
     * @param   $the_msg    The message to be displayed in the status box.
     * @param   $type       Type of message, either 'success' or 'error' -
     *                      passed into the DIV object as a class (used for styling).
     */
    public function set_msg($the_msg, $type = null)
	{
        $this->msg = $the_msg;
        $this->msg_type = $type;
    }

    /** 
	* Displays the status or error message in the template.
    */
    public function get_msg()
	{
        if($this->msg_type) {
            $style = "success";
        } else {
            $style = "error";
        }
        if($this->msg) {
            echo "<div class='status message " . $style . "'>". $this->msg ."</div>\n";
        }
    }
}
?>
