﻿<?php
/* *
 * 功能：支付宝服务器异步通知页面
 * 版本：3.3
 * 日期：2012-07-23
 * 说明：
 * 以下代码只是为了方便商户测试而提供的样例代码，商户可以根据自己网站的需要，按照技术文档编写,并非一定要使用该代码。
 * 该代码仅供学习和研究支付宝接口使用，只是提供一个参考。


 *************************页面功能说明*************************
 * 创建该页面文件时，请留心该页面文件中无任何HTML代码及空格。
 * 该页面不能在本机电脑测试，请到服务器上做测试。请确保外部可以访问该页面。
 * 该页面调试工具请使用写文本函数logResult，该函数已被默认关闭，见alipay_notify_class.php中的函数verifyNotify
 * 如果没有收到该页面返回的 success 信息，支付宝会在24小时内按一定的时间策略重发通知
 */

require_once("alipay.config.php");
require_once("lib/alipay_notify.class.php");
include_once("lib/helpers.php");

include_once "saja/mysql.ini.php";
// include_once "/var/www/html/site/lib/config.php";

$config['default_prefix'] = 'saja_';
$config['default_prefix_id'] = 'saja';
$config["db"][0]["charset"]  = "utf8" ;
$config["db"][0]["host"]     = "sajadb01";
$config["db"][0]["type"]     = "mysql";
$config["db"][0]["username"] = "saja"; 
// $config["db"][0]["password"] = 'taXumEswetetexUdra4w';
$config["db"][0]["password"] = '4wtaXumEswetetexUdra';

$config["db"][0]["dbname"]   = "saja_user" ;
$config["db"][1]["dbname"]   = "saja_cash_flow" ;
//error_log("config:".$this->config->default_prefix);
//error_log("config:".$config->default_prefix);

$json = (empty($_POST['json'])) ? $_GET['json'] : $_POST['client']['json']; 

if ( (empty($_GET['json'])) && (empty($_POST['json'])) ){
	$json = $_POST['client']['json'];
}
		
//计算得出通知验证结果
$alipayNotify = new AlipayNotify($alipay_config);
$verify_result = $alipayNotify->verifyNotify();

if($verify_result) {//验证成功
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//请在这里加上商户的业务逻辑程序代

	
	//——请根据您的业务逻辑来编写程序（以下代码仅作参考）——
    //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
	
	//解析notify_data
	//注意：该功能PHP5环境及以上支持，需开通curl、SSL等PHP配置环境。建议本地调试时使用PHP开发软件
	$doc = new DOMDocument();	
	if ($alipay_config['sign_type'] == 'MD5') {
		$doc->loadXML($_POST['notify_data']);
	}
	
	if ($alipay_config['sign_type'] == '0001') {
		$doc->loadXML($alipayNotify->decrypt($_POST['notify_data']));
	}
	
	if (empty($_POST['notify_data']) && (!empty($_POST['notify_id']))){
		$notify_data = $_POST;
	}
	
	error_log("[alipay/notify_url] notify_data : ".$doc->saveXML());
	error_log("[alipay/notify_url] 支付寶建立訂單。。。");

	if( (!empty($doc->getElementsByTagName( "notify" )->item(0)->nodeValue)) || (!empty($notify_data))) {
		
		//判斷來源是否為APP
		if (!empty($_POST['notify_data'])){
			//商户订单号
			$out_trade_no = $doc->getElementsByTagName( "out_trade_no" )->item(0)->nodeValue;
			//支付宝交易号
			$trade_no = $doc->getElementsByTagName( "trade_no" )->item(0)->nodeValue;
			//交易状态
			$trade_status = $doc->getElementsByTagName( "trade_status" )->item(0)->nodeValue;
			// 交易金額
			$total_fee = $doc->getElementsByTagName("total_fee")->item(0)->nodeValue;
			
			$deposit_history_data = '{"out_trade_no":"'.$out_trade_no.'", "trade_no":"'.$trade_no.'","total_fee":'.$total_fee.', "trade_status":"'.$trade_status.'", "timepaid":"'.date('YmdHis').'", "paymenttype":"ALIPAY_WAP", "notify_data":"'.$_POST['notify_data'].'"}';		
		}else{
			//商户订单号
			$out_trade_no = $array['out_trade_no'];
			//支付宝交易号
			$trade_no = $array['trade_no'];
			//交易状态
			$trade_status = $array['trade_status'];
			// 交易金額
			$total_fee = $array['total_fee'];
			
			$deposit_history_data = '{"out_trade_no":"'.$out_trade_no.'", "trade_no":"'.$trade_no.'","total_fee":'.$total_fee.', "trade_status":"'.$trade_status.'", "timepaid":"'.date('YmdHis').'", "paymenttype":"ALIPAY_WAP", "notify_data":"'.$_POST.'"}';		
		}
		
		$model = new mysql($config["db"][0]);
		$model->connect();
		
		$db_user = $config["db"][0]['dbname'];
		$db_cash_flow = $config["db"][1]['dbname'];
		
		$query = "select dh.* from `{$db_cash_flow}`.`{$config['default_prefix']}deposit_history` dh 
			left outer join `{$db_cash_flow}`.`{$config['default_prefix']}deposit` d on
				d.prefixid = dh.prefixid 
				and d.depositid = dh.depositid 
				and d.userid = dh.userid 
				and d.switch = 'N' 
			where dh.dhid = '{$out_trade_no}' 
				and dh.status = 'order' 
				and dh.data = ''";
		$deposit_history_array = $model->getQueryRecord($query);
		
		if(!empty($deposit_history_array['table']['record'][0]['dhid']) ) {
		    
			$userid=$deposit_history_array['table']['record'][0]['userid'];
						
			error_log("update deposit_history start");
			$query = "update `{$db_cash_flow}`.`{$config['default_prefix']}deposit_history` 
			set 
				status = 'deposit',
				data = '{$deposit_history_data}', 
				modifyt = NOW() 
			where 
				dhid = '{$out_trade_no}'
				and status = 'order' 
				and data = ''";
			$model->query($query);
			error_log("update deposit_history end");
			error_log("update deposit start");
			$query = "update `{$db_cash_flow}`.`{$config['default_prefix']}deposit` 
			SET 
				switch = 'Y',
				modifyt = NOW()
			where 
				depositid = '{$deposit_history_array['table']['record'][0]['depositid']}'
				and switch = 'N'
			";
			$model->query($query);
			error_log("update deposit end");
			error_log("update spoint start");
			$query = "update `{$db_cash_flow}`.`{$config['default_prefix']}spoint` 
			SET  
				switch = 'Y',
				modifyt = NOW()
			where 
				spointid = '{$deposit_history_array['table']['record'][0]['spointid']}'
				and switch = 'N'
			";
			$model->query($query);
			error_log("update spoint end");
			error_log("update deposit_rule_item start");
			$query = "SELECT * 
				FROM `{$db_cash_flow}`.`{$config['default_prefix']}deposit_rule_item` 
				WHERE
					driid = '{$deposit_history_array['table']['record'][0]['driid']}'
					and drid = '1'
					AND drid IS NOT NULL
					AND switch = 'Y'
			";
			$deposit_rule_item_array = $model->getQueryRecord($query);
			error_log("select deposit_rule_item end");
			
			error_log("select scode_promote_rt start");
			$query = "SELECT rt.*, sp.name spname, sp.productid 
				FROM `{$db_cash_flow}`.`{$config['default_prefix']}scode_promote_rt` rt 
				left outer join `{$db_cash_flow}`.`{$config['default_prefix']}scode_promote` sp on
					rt.prefixid = sp.prefixid 
					and rt.spid = sp.spid 
					and sp.ontime <= NOW()
					and sp.offtime > NOW()
					and sp.switch = 'Y'
				WHERE
					rt.driid = '{$deposit_rule_item_array['table']['record'][0]['driid']}'
					and rt.behav = 'c'
					and rt.switch = 'Y'
			";
			$scode_promote_rt_array = $model->getQueryRecord($query);
			error_log("select scode_promote_rt end");
			
			if (is_array($scode_promote_rt_array['table']['record'])) {
				foreach ($scode_promote_rt_array['table']['record'] as $key => $value)
				{
					// scode
					if ($value['productid'] == 0)
					{
						error_log("INSERT scode start");
						$query = "INSERT INTO `{$db_cash_flow}`.`{$config['default_prefix']}scode` 
						SET 
							prefixid = '{$config['default_prefix_id']}', 
							userid = '{$deposit_history_array['table']['record'][0]['userid']}', 
							spid = '{$value['spid']}', 
							behav = 'c', 
							productid = '0', 
							amount = '{$value['num']}', 
							remainder = '{$value['num']}', 
							closed = 'N',
							seq = '0', 
							switch = 'Y',
							insertt=NOW()
						";
						$model->query($query);
						$scodeid = $model->_con->insert_id;
						error_log("INSERT scode end");
						
						error_log("INSERT scode_history start");
						$query = "INSERT INTO `{$db_cash_flow}`.`{$config['default_prefix']}scode_history` 
						SET 
							prefixid = '{$config['default_prefix_id']}', 
							userid = '{$deposit_history_array['table']['record'][0]['userid']}', 
							scodeid = '{$scodeid}',
							spid = '{$value['spid']}',
							promote_amount = '{$deposit_rule_item_array['table']['record'][0]['spoint']}', 
							num = '{$value['num']}', 
							memo = '{$value['spname']}', 
							batch = '0', 
							seq = '0', 
							switch = 'Y',
							insertt=NOW()
						";
						$model->query($query);
						error_log("INSERT scode_history end");
					}
					else {
					    // oscode
						error_log("INSERT oscode start");
						if(is_house_promote_effective($value['productid'])) {
							// 判定是否符合萬人殺房資格, 符合者送殺價券
							register_house_promote($value['productid'],$userid);
							for ($i = 0; $i < $value['num']; $i++) {
								   $query = "INSERT INTO `{$db_cash_flow}`.`{$config['default_prefix']}oscode` 
									SET 
										prefixid = '{$config['default_prefix_id']}', 
										userid = '{$deposit_history_array['table']['record'][0]['userid']}', 
										productid = '{$value['productid']}', 
										spid = '{$value['spid']}', 
										behav = 'user_deposit', 
										used = 'N',
										amount = '1', 
										verified = 'Y',
										seq = '0', 
										switch = 'Y',
										insertt=NOW()
									";
									$model->query($query);
							} 
                            add_oscode_to_user_src($userid,$value['productid']);							
						} else {
							for($i = 0; $i < $value['num']; $i++)
							{
								$query = "INSERT INTO `{$db_cash_flow}`.`{$config['default_prefix']}oscode` 
								SET 
									prefixid = '{$config['default_prefix_id']}', 
									userid = '{$deposit_history_array['table']['record'][0]['userid']}', 
									productid = '{$value['productid']}', 
									spid = '{$value['spid']}', 
									behav = 'user_deposit', 
									used = 'N',
									amount = '1', 
									verified = 'Y',
									seq = '0', 
									switch = 'Y',
									insertt=NOW()
								";
								$model->query($query);
							}
						}
						error_log("INSERT oscode end");
					}
				}
			}
			
		}
		
		if($trade_status == 'TRADE_FINISHED') {
			//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
			//注意：
			//该种交易状态只在两种情况下出现
			//1、开通了普通即时到账，买家付款成功后。
			//2、开通了高级即时到账，从该笔交易成功时间算起，过了签约时的可退款时限（如：三个月以内可退款、一年以内可退款等）后。
	
			//调试用，写文本函数记录程序运行情况是否正常
			//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
			
			error_log("訂單：".$out_trade_no.",支付寶：".$trade_no.",狀態：".$trade_status);
			
			// if ((!empty($json)) && ($json == 'Y')){
				// $ret=getRetJSONArray(1,'OK','JSON');
				// $ret['retObj']['dhid'] = $deposit_history_array['table']['record'][0]['dhid'];
				// $ret['retObj']['userid'] = $userid;
				// $ret['retObj']['amount'] = number_format($deposit_rule_item_array['table']['record'][0]['amount'], 2);
				// $ret['retObj']['spoint'] = number_format($deposit_rule_item_array['table']['record'][0]['spoint'], 2);
				// $ret['retObj']['secretid'] = md5($deposit_history_array['table']['record'][0]['dhid']."|".$userid."|".$deposit_rule_item_array['table']['record'][0]['spoint']);
				// echo json_encode($ret);
				// exit;
			// }else{
			
				echo "success";		//请不要修改或删除
			// }
			/*
			echo '
			<form name="alipay_notify_url" method="post" action="http://cn.sajawa.com/site/deposit/alipay_html/">
				<input type="hidden" name="out_trade_no" value="'.$out_trade_no.'"/>
				<input type="hidden" name="trade_no" value="'.$trade_no.'"/>
				<input type="hidden" name="result" value="success"/>
				<script language="javascript" type="text/JavaScript">document.forms.alipay_notify_url.submit();</script>
			</form>';
			*/
		} else if ($trade_status == 'TRADE_SUCCESS') {
			//判断该笔订单是否在商户网站中已经做过处理
				//如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
				//如果有做过处理，不执行商户的业务程序
					
			//注意：
			//该种交易状态只在一种情况下出现——开通了高级即时到账，买家付款成功后。
	
			//调试用，写文本函数记录程序运行情况是否正常
			//logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
			
			error_log("訂單：".$out_trade_no.",支付寶：".$trade_no.",狀態：".$trade_status);

			// if ((!empty($json)) && ($json == 'Y')){
				// $ret=getRetJSONArray(1,'OK','JSON');
				// $ret['retObj']['dhid'] = $deposit_history_array['table']['record'][0]['dhid'];
				// $ret['retObj']['userid'] = $userid;
				// $ret['retObj']['amount'] = number_format($deposit_rule_item_array['table']['record'][0]['amount'], 2);
				// $ret['retObj']['spoint'] = number_format($deposit_rule_item_array['table']['record'][0]['spoint'], 2);
				// $ret['retObj']['secretid'] = md5($deposit_history_array['table']['record'][0]['dhid']."|".$userid."|".$deposit_rule_item_array['table']['record'][0]['spoint']);
				// echo json_encode($ret);
				// exit;

			// }else{
			
				echo "success";		//请不要修改或删除
			
			// }
			
			/*
			echo '
			<form name="alipay_notify_url" method="post" action="http://cn.sajawa.com/site/deposit/alipay_html/">
				<input type="hidden" name="out_trade_no" value="'.$out_trade_no.'"/>
				<input type="hidden" name="trade_no" value="'.$trade_no.'"/>
				<input type="hidden" name="result" value="success"/>
				<script language="javascript" type="text/JavaScript">document.forms.alipay_notify_url.submit();</script>
			</form>';
			*/
		}
	}

	//——请根据您的业务逻辑来编写程序（以上代码仅作参考）——
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
} else {
    //验证失败
	// if ((!empty($json)) && ($json == 'Y')){
		// $ret=getRetJSONArray(1,'OK','JSON');
		// $ret['retMsg']="ERROR";			
		// $ret['retObj'] = new stdclass;
		// echo json_encode($ret);
		// exit;

	// }else{	
	
		echo "fail";
		
	// }	
    /*
    echo '
	<form name="alipay_notify_url" method="post" action="http://cn.sajawa.com/site/deposit/alipay_html/">
		<input type="hidden" name="result" value="fail"/>
		<script language="javascript" type="text/JavaScript">document.forms.alipay_notify_url.submit();</script>
	</form>';
	*/
    //调试用，写文本函数记录程序运行情况是否正常
    //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
}

function is_house_promote_effective($productid) {
           global $model, $config;
		   $ret=false;
		   error_log("[alipay/notify_url] check productid : ".$productid);
		   
		   if($productid!='2854') {
		      $ret=false;
			  error_log("[alipay/notify_url] effective : ".$ret);
			  return $ret;
		   }
		   
		   // $db = new mysql($config["db"][0]);
		   // $db->connect();
		              
		   $query = " select count(*) as now_total from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` where switch='Y' and productid='{$productid}' "; 
		   		    
		   //取得資料
		   error_log("[alipay/notify_url] is_house_promote_effective : ".$query);
		   $table = $model->getQueryRecord($query); 
		   
		   if(!empty($table['table']['record'][0])) {
		      $cnt=$table['table']['record'][0];
		   } else {
		      $cnt=false;
		   }
		   if($cnt) {
		      error_log("[alipay/notify_url] now total :".$cnt['now_total']);
		      if($cnt['now_total']<33333) {
			     $ret=true;
			  } else if($cnt['now_total']>=33333) {
			     $ret=false;
			  }
		   } else {
		      // query error
			  error_log("[alipay/notify_url] empty passphrase :".$productid);
			  $ret=false;   
		   }
		   error_log("[alipay/notify_url] Effective :".$ret);
		   return $ret;
}

function register_house_promote($productid,$userid) {
           global $model,$config;
		   
		   // $db = new mysql($config["db"][0]);
		   // $db->connect();
		  		   
		   $cnt=countPassphrase($productid,$userid,'');
		   $query = " select count(*) as now_total from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` where 1=1 ";
		   if(!empty($productid)) {
		      $query.=" and productid='{$productid}' "; 
		   }
		   if(!empty($userid)) {
		      $query.=" and userid='{$userid}' "; 
		   }
		   
		   error_log("[alipay/notify_url] register_house_promote : ".$query);
		   $table = $model->getQueryRecord($query); 
		   if(!empty($table['table']['record'][0])) {
		      $cnt=$table['table']['record'][0];
		   } else {
		      $cnt=false;
		   }
		   if($cnt) {
		      $now_total=$cnt['now_total'];
		      error_log("[alipay/notify_url] user passphrase cnt :".$cnt['now_total']);
		   }
		   
		   if($now_total>0) {
		      $update = " update `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` set switch='Y', modifyt=NOW() where productid='{$productid}' and userid='{$userid}' ";
			  $ret = $model->query($update);
			  error_log("[alipay/notify_url] update passphrase".$update."==>".$ret);
		   } else {
		      $insert = " insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` set switch='Y', productid='{$productid}', userid='{$userid}', 
		                  user_src=(select intro_by from saja_user.saja_user_affiliate where goto={$userid} order by insertt asc LIMIT 1), 
					      openid=(select uid from saja_user.saja_sso where ssoid=(select ssoid from saja_user.saja_user_sso_rt where userid={$userid}) order by insertt asc LIMIT 1), 
					      insertt=NOW(), modifyt=NOW() ";
			  $ret = $model->query($insert);
			  error_log("[alipay/notify_url] create passphrase : ".$insert."==>".$ret);
		   }
}	

// 充值成功時送推薦人及TA殺價券 (只一次)
function add_oscode_to_user_src($userid, $productid) { 
		global $model,$config;
		
		$passphrase=getPassphraseInfo($productid,$userid,'Y');
		if($passphrase!=false) {
			// TA 充值滿500,再送6張給TA
			if($passphrase['userid']>0) {
				$oscode_gived_to_ta=get_oscode('160',$userid,2854);
				error_log("[alipay/nofity_url/oscode_gived_to_src] ta: ".$oscode_gived_to_ta['cnt']);
				if($oscode_gived_to_ta['cnt']==0) {
					for ($i = 0; $i < 6; $i++) {
						 add_oscode2($passphrase['userid'], $productid, '160', 'b'); 
					}					 
				}
			}
		
		    // TA 充值滿500, 另外送3張給推薦人
			if($passphrase['user_src']>0) {
			   $oscode_gived_to_src=get_oscode('161',$passphrase['user_src'],2854);
			   error_log("[alipay/nofity_url/oscode_gived_to_src] src: ".$oscode_gived_to_src['cnt']);
			   if($oscode_gived_to_src['cnt']==0) {
				  for($i = 0; $i < 3; $i++) {
					  add_oscode2($passphrase['user_src'], $productid, '161', 'c');
				   }
			   }
			}
		}		
		return ;
}

function getPassphraseInfo($productid, $userid, $switch) {
	       global $model, $config;
		   $query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` where 1=1 ";
		   if(!empty($productid)) {
		      $query.=" and productid='{$productid}' "; 
		   }
		   if(!empty($userid)) {
		      $query.=" and userid='{$userid}' "; 
		   }
		   if(!empty($switch)) {
		      $query.=" and switch='{$switch}' ";
		   }
	       
		   //取得資料
		   $table = $model->getQueryRecord($query);
		  
		   if(!empty($table['table']['record'][0])) {
		       error_log("[alipay/nofity_url/getPassphraseInfo]:".$query."==>".$table['table']['record'][0]['ppid']);
			   return $table['table']['record'][0];
		   } else {
			   error_log("[alipay/nofity_url/getPassphraseInfo]:".$query."==>false !");
			   return false;
		   }	
}
	
function countPassphrase($productid, $userid, $switch ) {
	       global $model, $config;
		   $query = " select count(*) as now_total from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` where 1=1 ";
		   if(!empty($productid)) {
		      $query.=" and productid='{$productid}' "; 
		   }
		   if(!empty($userid)) {
		      $query.=" and userid='{$userid}' "; 
		   }
		   if(!empty($switch)) {
		      $query.=" and switch='{$switch}' ";
		   }
		   //取得資料
		   $table = $model->getQueryRecord($query); 
		   if(!empty($table['table']['record'][0])) {
		      error_log("[alipay/nofity_url/countPassphrase]".$query."==>".$table['table']['record'][0]['now_total']);
		      return $table['table']['record'][0];
		   } else {
		      error_log("alipay/nofity_url/countPassphrase]".$query."==>false !!");
		      return false;
		   }
}
	
function createPassphrase($productid, $userid, $switch='W', $user_src='', $openid='') {
	       global $model, $config;
		   $update = " insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` set switch='{$switch}', productid='{$productid}', userid='{$userid}', 
		               user_src=(select intro_by from saja_user.saja_user_affiliate where goto={$userid} order by insertt asc LIMIT 1), 
					   openid=(select uid from saja_user.saja_sso where ssoid=(select ssoid from saja_user.saja_user_sso_rt where userid={$userid}) order by insertt asc LIMIT 1), 
					   insertt=NOW(), modifyt=NOW() ";
		   $ret = $model->query($update);
	       error_log("[alipay/nofity_url/createPassphrase]:".$update."==>".$ret);
}
	
function updatePassphrase($productid, $userid, $switch) {
	// public function setPassphraseEffective($productid, $userid, $switch) {
	       global $model, $config;
		   $update = " update `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` set switch='{$switch}', modifyt=NOW() where productid='{$productid}' and userid='{$userid}' ";
		   $ret = $model->query($update);
	       error_log("[alipay/nofity_url/updatePassphrase]:".$update."==>".$ret);
}

    
//查詢是否贈送過某活動的殺價券
function get_oscode($spid, $userid='', $productid='') {
    	global $model, $config;
		$query = "SELECT count(*) as cnt FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		           WHERE `prefixid` = '{$config['default_prefix_id']}' AND `switch` = 'Y' ";
        if(!empty($spid)) {		
		    $query.=" AND `spid` = '{$spid}' ";
		}
		if(!empty($userid)) {		
		    $query.=" AND `userid` = '{$userid}' ";
		}
		if(!empty($productid)) {		
		    $query.=" AND `productid` = '{$productid}' ";
		}
		$table = $model->getQueryRecord($query); 
		error_log("[alipay/nofity_url/get_oscode]:".$query);
		if(!empty($table['table']['record'][0])) {
		   return $table['table']['record'][0];
	    } else {
		   return false;
	    }
}

//新增oscode資料
function add_oscode2($userid, $productid, $spid, $behav)
	{
        global $model, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$userid}', 
			productid = '{$productid}', 
			spid = '{$spid}', 
			behav = '{$behav}', 
			used = 'N',
			amount = '1', 
			verified = 'Y',
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$ret=$model->query($query);
		error_log("[alipay/nofity_url/add_oscode2]:".$query."==>".$ret);
	}
?>
