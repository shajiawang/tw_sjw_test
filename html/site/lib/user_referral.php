<?php
//會員贈送處理

class user_referral
{
	//送超殺券S碼 2020/1/21 AARONFU
	public function red_scode($userid, $act_spid='')
	{
		global $db, $config, $user;
		
		$chk_referral = false;
		$insert_spoint = false;
		$ip = GetIP();
		
		//回傳:
		$ret['status'] = 1;
		$ret['retCode']=1;
		$ret['retMsg']="OK";
		$ret['retType']="LIST";
		$ret['action_list']=array(
				"type"=>5,
				"page"=>9,
				"msg"=>"NULL"
			);
		
		//用戶
		$userArr = $user->get_user($userid);
		
		//檢查是否通過手機驗證  沒過都不能給
		$recArr1 = $user->validSMSAuth($userid);
		if(empty($recArr1) || $recArr1["verified"] !=='Y') {
			$chk_referral = false;
			$ret['status'] = -2001;
			$ret['action_list']["msg"]="用戶未通過手機驗證 !";
		} else {
			if(empty($userid) ){
				$ret['status'] = -2002;
				$ret['action_list']["msg"]="用戶資料錯誤 !";
			
			} else {
				
				//送S碼
				$ret_status = $this->set_red_scode($userid, $act_spid);
				
				if($ret_status==1){
					$ret['action_list']["page"] = 22;
					$ret['action_list']["msg"] = "完成領取 !";
				} else {
					$ret['status'] = -2003;
					$ret['action_list']["msg"]="用戶已領取 !";
				}
			}
		}
		
		return $ret;
	}
	
	//送S碼 2020/12/29 AARONFU
	protected function set_red_scode($userid, $act_spid)
	{
		global $db, $config;
		
		$today = date("Y-m-d H:i:s");
		$off = date('Y-m-d H:i:s',strtotime('+3 day') );
		$productid = '';//商品代碼
		
		// 延遲一點時間, 0.05 ~ 0.2 秒不等
		$sleep = 50000 * (getRandomNum()%4+1);
		usleep($sleep);
		
		//贈送活動 `saja_cash_flow`.`saja_scode_promote`
		$query_action = "SELECT `spid`,`onum`,`name`,`description`
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
			WHERE `prefixid`='{$config['default_prefix_id']}'
				AND `switch` = 'Y'
				AND `spid`='{$act_spid}' ";
		$table = $db->getRecord($query_action);
		if(empty($table) ){
			return 0;
		} else {
			$spid = $table[0]['spid']; //活動代碼
			$_cnt_user = $table[0]['onum']; //贈送數量
			$memo = $table[0]['name']; // .': '. $_cnt_user;
		}
		
		//用戶限領一次
		$query = "SELECT `userid` FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			WHERE `prefixid`='{$config['default_prefix_id']}'
				AND `switch` = 'Y'
				AND `userid`='{$userid}'
				AND `spid`='{$spid}' ";
		$recArr = $db->getRecord($query);
		
		if(! empty($recArr) ){
			return 0;
		} else {
			//贈送紀錄
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` SET
				   `prefixid`='{$config['default_prefix_id']}',
				   `userid`='{$userid}',
				   `spid`='{$spid}',
				   `behav`='d',
				   `amount`='{$_cnt_user}',
				   `remainder`='{$_cnt_user}',
				   `offtime`='{$off}',
				   `switch` = 'Y', 
				   `insertt`=NOW()
				   ";
			$db->query($query);
			$scodeid = $db->_con->insert_id;

			//活動使用紀錄
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history` SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$userid}',
			   `scodeid`='{$scodeid}',
			   `spid`='{$spid}',
			   `promote_amount`='1',
			   `num`='{$_cnt_user}',
			   `memo`='{$memo}',
			   `batch` = '0', 
			   `insertt`=NOW()
			";
			$db->query($query); 

			//寫入活動紀錄
			error_log("[lib/user_referral/red_scode] scode_history : ". date('Ymd H:i:s') .' - '. $query);
			
			return 1;
		}
	}
	
	//推薦送S碼 2019/11/28 AARONFU
	public function gift_scode($userid, $intro_by)
	{
		global $db, $config, $user;
		
		$chk_referral = false;
		$insert_spoint = false;
		$ip = GetIP();
		
		//回傳:
		$ret['status'] = 1;
		$ret['retCode']=1;
		$ret['retMsg']="OK";
		$ret['retType']="LIST";
		$ret['action_list']=array(
				"type"=>5,
				"page"=>9,
				"msg"=>"NULL"
			);
		
		if (empty($intro_by) ){
			$ret['status'] = -2002;
			$ret['action_list']["msg"]="推薦人ID 不能空白 !";
		}
		else if (! is_numeric($intro_by) ){
			$ret['status'] = -2003;
			$ret['action_list']["msg"]="推薦人ID 必須為數字 !";
		}
		else 
		{
			//新用戶
			$userArr = $user->get_user($userid);
			//推薦人
			$referralArr = $user->get_user($intro_by);
			
			// 1)驗證推薦人資格

			if(intval($intro_by) >= intval($userid) ){
				//自己不能推薦自己
				$chk_referral = false;
				$ret['status'] = -2005;
				$ret['action_list']["msg"]="推薦人資格錯誤 !";
			}
			else if($referralArr["insertt"] > $userArr["insertt"]){
				//帳號新的不可以推薦帳號舊的
				$chk_referral = false;
				$ret['status'] = -2006;
				$ret['action_list']["msg"]="推薦人資格錯誤 !";
			}
			else 
			{
				//推薦人的$intro_by 不能是新人$userid
				$query = "SELECT  `suaid`, `userid`, `intro_by`, `promoteid` 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
					WHERE `userid` = '{$intro_by}' 
						AND `intro_by` = '{$userid}'
						AND `act` = 'REG'
						AND switch = 'Y'
						AND `insertt` > '2019-09-24 12:00:00'
					";
				$introArr = $db->getQueryRecord($query);
				
				if(empty($introArr['table']['record']) ){
					$chk_referral = true;
				} else {
					$chk_referral = false;
					$ret['status'] = -2007;
					$ret['action_list']["msg"]="推薦人資格錯誤 !";
				}
			}

			// 檢查新人是否通過手機驗證  沒過都不能給
			$recArr1 = $user->validSMSAuth($userid);
			if(empty($recArr1) || $recArr1["verified"] !=='Y') {
				$chk_referral = false;
				$ret['status'] = -2008;
				$ret['action_list']["msg"]="新用戶未通過手機驗證 !";
			}
			
			// 檢查推薦人是否通過手機驗證  沒過也不能給
			$recArr2 = $user->validSMSAuth($intro_by);
			if(empty($recArr2) || $recArr2["verified"] !=='Y') {
				$chk_referral = false;
				$ret['status'] = -2009;
				$ret['action_list']["msg"]="推薦人未通過手機驗證 !";
			}
			
			if($chk_referral)
			{
				// 2)被推薦新人有無資料
				$query = "SELECT  `suaid`, `userid`,  `intro_by`, `promoteid` 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
					WHERE `userid` = '{$userid}' 
						AND `act` = 'REG'
						AND switch = 'Y'
						AND `insertt` > '2019-09-24 12:00:00'
					";
				$recArr = $db->getQueryRecord($query);
				
				// 3)設定推薦人
				if(empty($recArr['table']['record']) ){
					$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
						SET `come_from`='{$ip}',
							`userid`='{$userid}',
							`intro_by`='{$intro_by}',
							`avid`='9776',
							`act`='REG',
							`promoteid`='8',
							`insertt`=NOW()";
					$db->query($query);
					
					$insert_spoint = true;
				}
				else 
				{
					if(empty($recArr['table']['record'][0]["intro_by"]) ){
						//設定推薦人
						$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`  
							SET `intro_by`='{$intro_by}', `avid`='9776'
							WHERE `userid`='{$userid}'";
						$db->query($query);
						
						$insert_spoint = true;
					} else {
						$insert_spoint = false;

						$ret['status'] = -2001;
						$ret['action_list']["msg"]="推薦人已設定 !";
					}
				}
			
				// 4)推薦送
				if($insert_spoint){
					if(empty($userid) || empty($intro_by)){
						$ret['status'] = -2004;
						$ret['action_list']["msg"]="推薦送失敗 !";
					
					} else {
						
						$ret_status = $this->set_scode($userid, $intro_by);
						$ret['action_list']["msg"]="推薦人設定完成 !";
					}
				}
			}
		}
		
		return $ret;
	}
	
	//推薦送S碼 2019/11/28 AARONFU
	protected function set_scode($userid, $intro_by) 
	{
		global $db, $config;
		
		$today = date("Y-m-d H:i:s");
		$off = date('Y-m-d H:i:s',strtotime('+3 day') );
		$productid = '';//商品代碼
		$oscode_cnt = 0;//殺價券數量
		
		//活動代號
		$spid = 9776;
		
		//新人送數量
		$_cnt_user = 1;
		
		//推薦人送數量
		$_cnt_intro = 2;
		
		//備用贈送活動表
		$query_action = "SELECT `ontime`,`offtime`
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}action`
			WHERE `activity_type` = '{$activity_type}' ";
		//$table_action = $db->getQueryRecord($query_action);
			
		// 延遲一點時間, 0.05 ~ 0.2 秒不等
		$sleep = 50000 * (getRandomNum()%4+1);
		usleep($sleep);
		
		//新人 - 贈送紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$userid}',
			   `spid`='{$spid}',
			   `behav`='b',
			   `amount`='{$_cnt_user}',
			   `remainder`='{$_cnt_user}',
			   `offtime`='{$off}',
			   `switch` = 'Y', 
			   `insertt`=NOW()
			   ";
		$db->query($query);
		$scodeid = $db->_con->insert_id;

		//新人 - 活動使用紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history` SET
           `prefixid`='{$config['default_prefix_id']}',
           `userid`='{$userid}',
		   `scodeid`='{$scodeid}',
           `spid`='{$spid}',
		   `promote_amount`='1',
           `num`='{$_cnt_user}',
		   `memo`='23_設定推薦送超殺券_送 {$_cnt_user}張券',
		   `batch` = '0', 
           `insertt`=NOW()
		";
		$db->query($query); 

		//寫入活動紀錄
		error_log("[lib/user_referral/set_scode] scode_history : ". date('Ymd H:i:s') .' - '. $query);
		

		//推薦人 - intro_by 贈送紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$intro_by}',
			   `spid`='{$spid}',
			   `behav`='b',
			   `amount`='{$_cnt_intro}',
			   `remainder`='{$_cnt_intro}',
			   `offtime`='{$off}',
			   `switch` = 'Y', 
			   `insertt`=NOW()
			   ";
		$db->query($query);
		$scodeid_intro = $db->_con->insert_id;
		
		//推薦人 - 活動使用紀錄
		$query_intro = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history` SET
           `prefixid`='{$config['default_prefix_id']}',
           `userid`='{$intro_by}',
		   `scodeid`='{$scodeid_intro}',
           `spid`='{$spid}',
		   `promote_amount`='1',
           `num`='{$_cnt_intro}',
		   `memo`='22_邀請好友送S碼 {$_cnt_intro}張券',
		   `batch` = '0', 
           `insertt`=NOW()
		";
		$db->query($query_intro); 
		
		//寫入活動紀錄 2019/11/28 麻煩不要修改 AARONFU
		error_log("[lib/user_referral/set_scode] scode_history : ". date('Ymd H:i:s') .' - '. $query_intro);
		
		return 1;
	}
	
	//推薦新會員送幣
	public function gift_invite($userid, $intro_by)
	{
		global $db, $config, $user;
		
		$chk_referral = false;
		$insert_spoint = false;
		$ip = GetIP();
		
		//回傳:
		$ret['status'] = 1;
		$ret['retCode']=1;
		$ret['retMsg']="OK";
		$ret['retType']="LIST";
		$ret['action_list']=array(
				"type"=>5,
				"page"=>9,
				"msg"=>"NULL"
			);
		
		if (empty($intro_by) ){
			$ret['status'] = -1002;
			$ret['action_list']["msg"]="推薦人ID 不能空白 !";
		}
		else if (! is_numeric($intro_by) ){
			$ret['status'] = -1003;
			$ret['action_list']["msg"]="推薦人ID 必須為數字 !";
		}
		else 
		{
			//新用戶
			$userArr = $user->get_user($userid);
			//推薦人
			$referralArr = $user->get_user($intro_by);
			
			// 1)驗證推薦人資格

			if(intval($intro_by) >= intval($userid) ){
				//自己不能推薦自己
				$chk_referral = false;
				$ret['status'] = -1005;
				$ret['action_list']["msg"]="推薦人資格錯誤 !";
			}
			else if($referralArr["insertt"] > $userArr["insertt"]){
				//帳號新的不可以推薦帳號舊的
				$chk_referral = false;
				$ret['status'] = -1006;
				$ret['action_list']["msg"]="推薦人資格錯誤 !";
			}
			else 
			{
				//推薦人的$intro_by 不能是新人$userid
				$query = "SELECT  `suaid`, `userid`, `intro_by`, `promoteid` 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
					WHERE `userid` = '{$intro_by}' 
						AND `intro_by` = '{$userid}'
						AND `act` = 'REG'
						AND switch = 'Y'
						AND `insertt` > '2019-09-24 12:00:00'
					";
				$introArr = $db->getQueryRecord($query);
				
				if(empty($introArr['table']['record']) ){
					$chk_referral = true;
				} else {
					$chk_referral = false;
					$ret['status'] = -1007;
					$ret['action_list']["msg"]="推薦人資格錯誤 !";
				}
			}

			// 檢查新人是否通過手機驗證  沒過都不能給
			$recArr1 = $user->validSMSAuth($userid);
			//$query = "SELECT * FROM saja_user.saja_user_sms_auth WHERE userid='{$userid}' and switch='Y' ";
			//$recArr1=$db->getQueryRecord($query);
			//error_log("[lib/user_referral/gift_invite] check user : ".json_encode($recArr1['table']['record'][0]));
			//if($recArr1 && $recArr1['table']['record'][0]['verified']!='Y') {}
			if(empty($recArr1) || $recArr1["verified"] !=='Y') {
				$chk_referral = false;
				$ret['status'] = -1008;
				$ret['action_list']["msg"]="新用戶未通過手機驗證 !";
			}
			
			// 檢查推薦人是否通過手機驗證  沒過也不能給
			$recArr2 = $user->validSMSAuth($intro_by);
			//$query = "SELECT * FROM saja_user.saja_user_sms_auth WHERE userid='{$intro_by}' and switch='Y' ";
			//$recArr2=$db->getQueryRecord($query);
			//error_log("[lib/user_referral/gift_invite] check intro_by : ".json_encode($recArr2['table']['record'][0]));
			//if($recArr2 && $recArr2['table']['record'][0]['verified']!='Y') {}
			if(empty($recArr2) || $recArr2["verified"] !=='Y') {
				$chk_referral = false;
				$ret['status'] = -1009;
				$ret['action_list']["msg"]="推薦人未通過手機驗證 !";
			}
			
			if($chk_referral)
			{
				// 2)被推薦新人有無資料
				$query = "SELECT  `suaid`, `userid`,  `intro_by`, `promoteid` 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
					WHERE `userid` = '{$userid}' 
						AND `act` = 'REG'
						AND switch = 'Y'
						AND `insertt` > '2019-09-24 12:00:00'
					";
				$recArr = $db->getQueryRecord($query);
				
				// 3)設定推薦人
				if(empty($recArr['table']['record']) ){
					$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
						SET `come_from`='{$ip}',
							`userid`='{$userid}',
							`intro_by`='{$intro_by}',
							`act`='REG',
							`promoteid`='8',
							`insertt`=NOW()";
					$db->query($query);
					
					$insert_spoint = true;
				}
				else 
				{
					if(empty($recArr['table']['record'][0]["intro_by"]) ){
						//設定推薦人
						$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`  
							SET `intro_by`='{$intro_by}'
							WHERE `userid`='{$userid}'";
						$db->query($query);
						
						$insert_spoint = true;
					} else {
						$insert_spoint = false;

						$ret['status'] = -1001;
						$ret['action_list']["msg"]="推薦人已設定 !";
					}
				}
			
				// 4)推薦人送幣
				if($insert_spoint){
					if(empty($userid) || empty($intro_by)){
						$ret['status'] = -1004;
						$ret['action_list']["msg"]="推薦送失敗 !";
					
					} else {
						
						//2019-11-28 停用 推薦送幣
						//$ret_status = $this->gift_spoint_8($userid, $intro_by);
						if($ret_status<0) {
						   $ret['action_list']["msg"]="新用戶未通過手機驗證 !";	
						} else {
						   $ret['action_list']["msg"]="推薦人設定完成 !";
						}
					}
				}
			}
		}
		
		return $ret;
	}
	
	//推薦人送幣 2019/11/19 AARONFU
	protected function gift_spoint_8($userid, $intro_by) 
	{
		global $db, $config;
		
		$today = date("Y-m-d H:i:s");
		$productid = '';//商品代碼
		$oscode_cnt = 0;//券數量
		
		//新人送幣數量
		$spoint_cnt_user = 0;
		if ( $today >= '2019-11-22 10:00:00' ) {
			$spoint_cnt_user = 50; 
		}
				
		//推薦人送幣數量
		$spoint_cnt_intro = 50;
		
		//備用贈送活動表
		$query_action = "SELECT `ontime`,`offtime`
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}action`
			WHERE `activity_type` = '{$activity_type}' ";
		//$table_action = $db->getQueryRecord($query_action);
			
		// 延遲一點時間, 0.05 ~ 0.2 秒不等
		$sleep = 50000 * (getRandomNum()%4+1);
		usleep($sleep);
		
		// 檢查新人是否通過手機驗證  沒過都不能給
		$query = "SELECT * FROM saja_user.saja_user_sms_auth WHERE userid='{$userid}' and switch='Y' ";
		$recArr=$db->getQueryRecord($query);
		error_log("[lib/user_referral] check user : ".json_encode($recArr['table']['record'][0]));
		if($recArr && $recArr['table']['record'][0]['verified']!='Y') {
			error_log("[lib/user_referral] the phone No. of userid : {$userid} is not verified ,stop to give spoint! ");
			return -1;	
	    }
		
		//新人 - 送幣紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` SET
			`userid`='{$userid}',
			`behav`='gift',
			`countryid`='1',
			`amount`='{$spoint_cnt_user}',
			`remark`='18',
			`insertt`=NOW()
		";
		$db->query($query);
		$spointid = $db->_con->insert_id;

		//新人 - 活動送幣使用紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` SET
			`userid`='{$userid}',
			`behav`='gift',
			`activity_type`='18',
			`amount_type`='1',
			`free_amount`='{$spoint_cnt_user}',
			`total_amount`='{$spoint_cnt_user}',
			`spointid`='{$spointid}',
			`insertt`=NOW()
		";
		$db->query($query); 

		//新人 - 寫入活動紀錄
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` SET
			`userid`='{$userid}',
			`activity_type`=18,
			`insertt`=NOW()";
		$db->query($query); 
		error_log("[lib/user_referral/gift_spoint_8] activity_history : ". date('Ymd H:i:s') .' - '. $query);
		
				
		//推薦人 - intro_by 送幣紀錄
		$query_intro = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` SET
			`userid`='{$intro_by}',
			`memo`='{$userid}',
			`behav`='gift',
			`countryid`='1',
			`amount`='{$spoint_cnt_intro}',
			`remark`='8',
			`insertt`=NOW()
		";
		$db->query($query_intro);
		$spointid_intro = $db->_con->insert_id;
		
		//推薦人 - 活動送幣使用紀錄
		$query_intro = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` SET
			`userid`='{$intro_by}',
			`behav`='gift',
			`activity_type`='8',
			`amount_type`='1',
			`free_amount`='{$spoint_cnt_intro}',
			`total_amount`='{$spoint_cnt_intro}',
			`spointid`='{$spointid_intro}',
			`insertt`=NOW()
		";
		$db->query($query_intro); 

		$log = date('Ymd H:i:s') .' - '. $userid .'_'. $intro_by .'_'. $spoint_cnt_intro;
		// 2019/11/19 麻煩不要修改 AARONFU
		error_log("[lib/user_referral/gift_spoint_8] ". $log);
		
		return 1;
	}
	
	//邀請好友
	protected function mk_invite()
	{
		global $db, $config;
		
		$ret['status'] = -3;
		$ret['retCode']=1;
		$ret['retMsg']="OK";
		$ret['retType']="LIST";
		$ret['action_list']=array(
			"type"=>5,
			"page"=>7,
			"msg"=>"OK!"
		);
		
		return $ret;
	}

}
?>
