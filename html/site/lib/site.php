<?php
/*
 * Site Controller
 */

class Site{

	public $controller = array();
	public $params = array();
	public $id;

	/*
	* Loads a particular page from the 'site' directory in views
	* @param   $name   The name of the page to load (should match filename)
	*/
	public static function load_page($name) {

		global $tpl, $product;
		
		$_SESSION['channelid'] = "";

		if(!isset($_GET['channelid'])){
			if(!empty($_SESSION['user']['profile']['channelid'])){
				$_GET['channelid'] = $_SESSION['user']['profile']['channelid'];
			}else{
				$_GET['channelid'] = $_SESSION['channelid'];
			}
		}

		$standard = array("faq", "terms", "about");
		$proper = array("Frequently Asked Questions", "Terms of Service", "About Us");

		$tpl->set_page_id('/site/');
		$tpl->set_title('');
		$tpl->render("site", $name, true);
	}

	public static function home() {

		global $tpl, $product, $bid, $ad;

		$controller['controller'] = 'site';
		$controller['action'] = 'home';
		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_SESSION['auth_id']) ? htmlspecialchars($_POST['auth_id']) : $_SESSION['auth_id'];

        error_log("[lib/site/home] userid : ".$userid);
		//設定 Action 相關參數
		set_status($controller);

		//建立Redis連線
		$redis=getRedis();
		
		if($redis){
			
			// 取得首頁上方廣告資料
			$rdata='';
			// $rkey = "SiteHomeAdBannerList";
			$rkey = "SiteAdBannerList1";
			$rdata = $redis->get($rkey);
			if($rdata) {
			   // error_log("[lib/site] Get SiteAdBannerList1 from redis ...");	
			   $ad_list = json_decode($rdata,TRUE);
			} else {
				// error_log("[lib/site] Not get SiteAdBannerList1 from redis ...");
				$ad_list = $ad->get_ad_list(1, 'P');
				if(!empty($ad_list) && is_array($ad_list)) {

					//計算即將下架的廣告秒數
					foreach($ad_list as $res){
						$offtime_array[] = strtotime($res['offtime']);
					}

					//resdis有效時間(秒))
					$rexpire_time = min($offtime_array) - strtotime('now');
                    // error_log("[lib/site] expire time :".$rexpire_time);
					$redis->set($rkey, json_encode($ad_list));
					$redis->expire($rkey, $rexpire_time);
				}  		
			}
			
			//最新成交清單
			$rdata='';
			$rkey="recently_closed_product_list";
			$rdata = $redis->get($rkey);    
			if(!empty($rdata)) {
				$bid_list = json_decode($rdata,TRUE); 
			} else {	
			    // $bid_list = $bid->recently_closed_product_list('');
				if(!empty($bid_list) && is_array($bid_list))
					$redis->set($rkey,json_encode($bid_list));			  
			} 

			
			//商品列表
			$rdata='';
			$rkey = "inx_product_list";
			$rdata = $redis->get($rkey);
			if($rdata) {
				
				// $suserid =['38802','4068','5424','3807','468952','468407','62427','50856','468170','469316','187218','195102','456393','2788','62427','50856','1705','8001','468919','136254','28','46656','29109'];
				// $inuserid=$userid;
				// if(!in_array($inuserid,$suserid)) {				
					error_log("[lib/site] userid0 : ".$userid);
				   $product_list = json_decode($rdata,TRUE); 
				// }else{
					// error_log("[lib/site] userid00 : ".$userid);
					// $product_list = $product->inx_product_list($userid);
				   // if(!empty($product_list) && is_array($product_list)) {
					  // $redis->set($rkey, json_encode($product_list));
				   // }  
				// }			
			} else {
			   // $product_list = $product->inx_product_list($userid,'Y');
			   error_log("[lib/site] userid1 : ".$userid);
			   $product_list = $product->inx_product_list($userid);
               if(!empty($product_list) && is_array($product_list)) {
                  $redis->set($rkey, json_encode($product_list));
               }  			   
			}
			
		} else {
			error_log("[lib/site] userid2 : ".$userid);
			//商品列表
			$product_list = $product->inx_product_list($userid);
			//最新成交清單
		
		    // 
		    // $bid_list = $bid->recently_closed_product_list('');
		
			//取得首頁上方廣告資料
			$ad_list = $ad->get_ad_list(1, 'P');
        }
		
		//取得首頁上方廣告資料
		// $ad_list = $ad->get_ad_list(1, 'P');

        //成功人數
		$bid_user_count = $bid->bid_user_count();
		//成功節省金額
		$bid_money_count = $bid->bid_money_count(); 

		if($json != 'Y'){
			$tpl->assign('ad_list', $ad_list);
			$tpl->assign('product_list', $product_list['table']['record']);
			$tpl->assign('bid_list', $bid_list['table']['record']);
			$tpl->assign('bid_user_count', $bid_user_count);
			$tpl->assign('bid_money_count', $bid_money_count);
			$tpl->set_title('');
			$tpl->render('site', 'home', true);
		}else{
			$data['retCode'] = 1;
			$data['retMsg'] = "取得資料 !!";
			$data['retType'] = "MSG";
			$data['retObj']['data']['ad'] = $ad_list;
			$data['retObj']['data']['product_list'] = $product_list['table']['record'];
			$data['retObj']['data']['bid_list'] = $bid_list['table']['record'];
			$data['retObj']['data']['nowmicrotime'] = microtime(true);
			echo json_encode($data);
		}

	}


	public function login() {

    	global $tpl, $user;

    	$controller['controller'] = 'site';
		$controller['action'] = 'home';

		//設定 Action 相關參數
		set_status($controller);

		if(empty($_SESSION['auth_id']) && $_COOKIE['auth_id']){

			$user_profile = $user->get_user_profile($_COOKIE['auth_id']);
			$users = $user->get_user($_COOKIE['auth_id']);
			unset($users['passwd']);

			$users['profile'] = $user_profile;

			if(md5($users['userid'] . $users['name']) == $_COOKIE['auth_secret']){
				$_SESSION['user'] = $users;
				// Set session and cookie information.
				$_SESSION['auth_id'] = $users['userid'];
				$_SESSION['auth_email'] = $_COOKIE['auth_email'];
				$_SESSION['auth_secret'] = md5($users['userid'] . $users['name']);
			}

			// Set local publiciables with the user's info.
			$user->user_id = $users['userid'];
			$user->name = $users['profile']['nickname'];
			$user->email = '';
			$user->ok = true;
			$user->is_logged = true;

		}

	}

}
