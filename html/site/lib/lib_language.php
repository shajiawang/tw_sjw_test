<?php	
/**** 語系代碼 ******/
function lan_str($lang_key, $lang_category=0, $replace=null) {
	global $user_lan;
	
	$lang_value = $user_lan[$lang_category][$lang_key];
	
	if(!empty($replace)){
		$lang_value = str_replace("$$", $replace, $lang_value);
	}


	return $lang_value;
}

//用戶語系
class user_lang
{	
	//回傳:
	public $userid;
	
	public function __construct()
	{
		global $config;
		
		$this->userip = getUserIP();
	}

	/*
	 *	取得會員語系資料
	 *	$lang_code int 語系編號或代碼
	 */
	public function _user_language_redis()
	{
		global $config;
		
		$listtype = "userlanguage_". $this->userid;
		
		try{
			$yesterday = date("Y-m-d", strtotime("yesterday"));
			removeRedisList($listtype, $this->userip);
			
			//$redis_key = $this->redisS->keys("userlanguage_{$this->userid}");
			$redis_key = '';//getRedisList($listtype);
			
			if (empty($redis_key)){
				$userinfo = $this->_user_profile($this->userid);
				
				//語系表
				$lang_code = empty($userinfo["lang"]) ? $config['def_lang_code'] : $userinfo["lang"];
				$user_language = $this->_user_language($lang_code);
				
				//$this->redisS->setex("userlanguage_{$this->userid}", 86400, json_encode($this->get_user_language($lang_code)) );
				//setRedisList($listtype, $this->userip, json_encode($user_language) );
				
				//$redis_get = json_decode($this->redisS->get("userlanguage_{$this->userid}"),true);
				//$redis_get = inlistRedistList($listtype, $this->userip);
				$redis_get = $user_language; 
				
				return $redis_get;
			}else{
				//$redis_get = json_decode($this->redisS->get("userlanguage_{$this->userid}"),true);
				//$redis_get = inlistRedistList($listtype, $this->userip);
				$redis_get = array();
				return $redis_get;
			}
		}catch(Execption $e){
			error_log("Redis Service Error!");
			return -1;
		}
    }
	
	/*
	 *	取得會員基本資料
	 *	$id int 會員編號
	 */
	public function _user_profile($id)
	{
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();
	    
		$query ="SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		          WHERE	`prefixid` = '{$config['default_prefix_id']}'
			        AND userid = '{$id}'
			        AND switch = 'Y' ";

		$table = $db->getRecord($query);

		if(!empty($table[0])) {
			$table[0]['nickname']=urldecode($table[0]['nickname']);
            return $table[0];
		}
		return false;
    }
	
	/*
	 *	取得會員語系資料
	 *	$lang_code int 語系編號或代碼
	 */
	public function _user_language($lang_code=133)
	{
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT `lang_category`, `lang_key`, `lang_value`
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}language`
				WHERE switch = 'Y'
				AND `lang_key` IS NOT NULL
				AND (`lang_code_no`='{$lang_code}' OR `lang_code`='{$lang_code}')";
		$table = $db->getRecord($query);

		if(!empty($table)) {
			$lan = array();
			foreach($table as $k=>$v){
				$_cate = $v['lang_category'];
				$_key = $v['lang_key'];
				$lan[$_cate][$_key] = $v['lang_value'];
			}

			return $lan;
		}
		return false;
    }
	
}	
 
//載入lib include_once(LIB_DIR ."/lib_language.php");

//語系物件
$lnag = new user_lang();
$lnag->userid = empty($_REQUEST['auth_id']) ? $_SESSION['auth_id'] : htmlspecialchars($_REQUEST['auth_id']);
	
//用戶語系表
$user_lan = $lnag->_user_language_redis();
	
//取得語系代碼 $ret['retMsg'] = lan_str('AUTHENTICATE_CHECK_IS_ERROR');


?>