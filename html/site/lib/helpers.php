<?php
/**
 * Helper Functions
 * This file contains a lot of miscellaneous functions that are used throughout the app.
 */
	use ElephantIO\Client;
	use ElephantIO\Engine\SocketIO\Version2X;
define("REDIS_SERVER",'sajacache01');
  function getUserIP()
  {
      // Get real visitor IP behind CloudFlare network
      if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
      }
      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = $_SERVER['REMOTE_ADDR'];

      if(filter_var($client, FILTER_VALIDATE_IP))
      {
          $ip = $client;
      }
      elseif(filter_var($forward, FILTER_VALIDATE_IP))
      {
          $ip = $forward;
      }
      else
      {
          $ip = $remote;
      }
      if (inet_pton($ip)){
        return $ip;
      }else{
        error_log('[/libs/helpers] getUserIP fail: {$ip}');
        return -1;
      }
  }

  function getRedisList($listtype){
 //Connecting to Redis server on localhost
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    return $redis->keys("{$listtype}_*");
  }

  function setRedisList($listtype,$ip,$acttype=''){
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    //store data in redis list
    return $redis->set("{$listtype}_{$ip}", "{$acttype}");
  }
  function removeRedisList($listtype,$ip,$acttype=''){
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    //store data in redis list
    return $redis->del("{$listtype}_{$ip}");
  }
  function inlistRedistList($listtype,$ip){
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    //store data in redis list
    return $redis->exists("{$listtype}_{$ip}");
  }
  define('ES_SALT','sJw#333');
  //對接到 與內政部對接的主機走內部ip不驗簽之後加鎖ip
  function id_verify($personId,$applyYyymmdd,$applyCode,$issueSiteId){
        $fields = array(
          'personId'  => $personId,
          'applyYyymmdd'    => $applyYyymmdd,
          'applyCode'        => $applyCode,
          'issueSiteId'        => $issueSiteId
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://api.saja.com.tw/ris/' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, ( $fields ) );
        $result = curl_exec( $ch );
        curl_close( $ch );
        return $result;
  }
//身份證字號格式檢查
function id_card($cardid){
	$err ='';
	//先將字母數字存成陣列
	$alphabet =['A'=>'10','B'=>'11','C'=>'12','D'=>'13','E'=>'14','F'=>'15','G'=>'16','H'=>'17','I'=>'34',
				'J'=>'18','K'=>'19','L'=>'20','M'=>'21','N'=>'22','O'=>'35','P'=>'23','Q'=>'24','R'=>'25',
				'S'=>'26','T'=>'27','U'=>'28','V'=>'29','W'=>'32','X'=>'30','Y'=>'31','Z'=>'33'];
	//檢查字元長度
	if(strlen($cardid) !=10){$err = '1';}//長度不對

	//驗證英文字母正確性
	$alpha = substr($cardid,0,1);//英文字母
	$alpha = strtoupper($alpha);//若輸入英文字母為小寫則轉大寫
	if(!preg_match("/[A-Za-z]/",$alpha)){
		$err = '2';}else{
			//計算字母總和
			$nx = $alphabet[$alpha];
			$ns = $nx[0]+$nx[1]*9;//十位數+個位數x9
		}

	//驗證男女性別
	$gender = substr($cardid,1,1);//取性別位置
	if($gender !='1' && $gender !='2'){$err = '3';}//驗證性別

	//N2x8+N3x7+N4x6+N5x5+N6x4+N7x3+N8x2+N9+N10
	if($err ==''){
		$i = 8;
		$j = 1;
		$ms =0;
		//先算 N2x8 + N3x7 + N4x6 + N5x5 + N6x4 + N7x3 + N8x2
		while($i >= 2){
			$mx = substr($cardid,$j,1);//由第j筆每次取一個數字
			$my = $mx * $i;//N*$i
			$ms = $ms + $my;//ms為加總
			$j+=1;
			$i--;
		}
		//最後再加上 N9 及 N10
		$ms = $ms + substr($cardid,8,1) + substr($cardid,9,1);
		//最後驗證除10
		$total = $ns + $ms;//上方的英文數字總和 + N2~N10總和
		if( ($total%10) !=0){$err = '4';}
	}
	return ($err=='');
}
/**
 * Helper Functions
 * This file contains a lot of miscellaneous functions that are used throughout the app.
 */
function encrypt($code)
{
	return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(ES_SALT), $code, MCRYPT_MODE_CBC, md5(md5(ES_SALT))));
}
/**
* 解密
* @param [type] $code [description]
* @return [type]  [description]
*/
function decrypt($code)
{
	return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(ES_SALT), base64_decode($code), MCRYPT_MODE_CBC, md5(md5(ES_SALT))), "12");
}

// 6碼數字亂數
function get_shuffle($num='', $eword='')
{
	// if ($num > $eword){
		// $v1 = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
		// $v2 = array('0','1','2','3','4','5','6','7','8','9');
		// shuffle($v);
		// /*
		// $rand = time() * rand(1,9);
		// $rand1 = substr($rand, -6, 2);
		// $rand2 = substr($rand, -4, 2);
		// $rand3 = substr($rand, -2);

		// $code = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $rand3;
		// */
		// $code = $v[2].$v[7].$v[3].$v[8].$v[0].$v[9];
	// }else{
		// $v = array('0','1','2','3','4','5','6','7','8','9');
		// shuffle($v);
		// $code = $v[2].$v[7].$v[3].$v[8].$v[0].$v[9];
	// }
	$v1 = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
	$v2 = array('0','1','2','3','4','5','6','7','8','9');
	shuffle($v1);
	shuffle($v2);

	if (($num > $eword) && ($num > 0) && ($eword > 0)){
		//取得英文字
		for($i=0; $i<($eword); ++$i) {
			$c1[$i]= strtolower($v1[$i]);
		}
		//取得數字
		for($k=0; $k<($num-$eword); ++$k) {
			$c1[$k+$eword]= $v2[$k];
		}

		shuffle($c1);
		//組合字串
		for($c=0; $c<count($c1);$c++){
			$code .= $c1[$c];
		}

	} else {

		shuffle($v2);
		$code = $v2[2].$v2[7].$v2[3].$v2[8].$v2[0].$v2[9];

	}

	return $code;
}

function html_decode($str)
{
	$str = htmlspecialchars_decode($str);
	$str = str_replace("&gt", '>', $str);
	$str = str_replace("&lt", '<', $str);

	return $str;
}

function get_time()
{
	return $_SERVER['REQUSET_TIME'];
}

function get_panel_id()
{
	echo "-". md5($_SERVER['REQUEST_URI']);
}

/**
 * This function prints variables in the template, after setting them with the
 * $tpl->set_variable() function. Possible to extend this to support multiple
 * languages, as well as optionally returning a value (instead of echoing).
 * @param   $id	The name of the variable, prints the value passed from controller.
 */
function _v($id)
{
	global $tpl;

	return $tpl->variables[$id];
}

/**
 * //設定 Action 相關參數
 */
function set_status($controller)
{
	global $tpl, $status, $config;

	$page_id =  APP_DIR .'_'. $controller['controller'] .'_'. $controller['action'];
	$page_id = str_replace("/", '', $page_id);

	$tpl->set_page_id($page_id);

	$tpl->set_page_header($controller['controller'], $controller['action']);

	$status["status"]["args"] = array();

	// Update By Thomas 150921
	if(empty($_GET['channelid'])) {
	   $_GET["channelid"] = $config['channel'];
	   if(empty($_GET['channelid'])) {
	      $_GET["channelid"] = $_SESSION['user']['profile']['channelid'];
	   }
	}

	/*
	$_SESSION['channelid'] = $config['channel'];
	$_SESSION['channelid'] = (!empty($_SESSION['user']['profile']['channelid'])) ? $_SESSION['user']['profile']['channelid'] : $_SESSION['channelid'];

	if(!isset($_GET["channelid"])){
		$_GET["channelid"] = $_SESSION['channelid'];
	}
	*/

	$status["status"]["args"][] = "channelid=". $_GET["channelid"];

	$status["status"]["path"] = APP_DIR;

	if(!empty($_GET["fun"]) ){
		$status["status"]["path"] .= "/". $_GET["fun"];
	}
	if(!empty($_GET["act"]) ){
		if($_GET["act"]=='view' || $_GET["act"]=='home'){
		}
		else{
			$status["status"]["path"] .= "/". $_GET["act"];
		}
	}
	$status["status"]["path"] .= "/";

	$status["status"]["args"] = implode('&', $status["status"]["args"]);
	if (!empty($status["status"]["args"])) { $status["status"]["path"] .= ("?".$status["status"]["args"]); }

	//商品照片
	$status['path_image'] = $config['path_image'];

	$tpl->assign('status', $status);
}

/**
 * Redirect the user to any page on the site.
 * @param   $location	URL of where you want to return the user to.
 */
function return_to($location)
{
	$location = '/'. $location;
	header("Location: {$location}");
	exit();
}

function getRetJSONArray($code='', $msg='', $type='') {
         $ret=array();
		 $ret['retCode']=$code;
		 $ret['retMsg']=$msg;
		 $ret['retType']=$type;
		 if($type=='LIST') {
		    $ret['retObj']=array("data"=>array());
		 } else if($type=='JSON') {
		    $ret['retObj']=new stdClass();
		 }
		 return $ret;
}
/*
function getRetJSONArray() {
         $ret=array();
		 $ret['retCode']=1;
		 $ret['retMsg']="OK";
		 $ret['retType']="";
         $ret['retObj']=null;
		 return $ret;
}
*/
/**
 * Check to see if user is logged in and if not, redirect them to the login page.
 * If they're logged in, let them proceed.
 */
function login_required()
{
	global $user, $tpl;

	if($_POST['json']=='Y') {

	    $ymd=date("Ymd");
		$userid=addslashes($_POST['auth_id']);
		$tk = $_POST['tk'];
		// error_log("login_required: ".$userid."<-->".$tk);
		// error_log("login_required check: ".MD5($userid."|".$ymd));
		if(empty($userid)) {
		   $user->is_logged = false;
		   echo '{"retCode":-1,"retMsg":"EMPTY_USERID","retType":"TO_LOGIN_PAGE"}';
		   exit;
		} else if($userid!=$_POST['auth_id']) {
		   $user->is_logged = false;
		   echo '{"retCode":-2,"retMsg":"UNFORMAT_USERID","retType":"TO_LOGIN_PAGE"}';
		   exit;
		}
		if(MD5($userid."|".$ymd)==$tk) {
		   $_SESSION['auth_id']=$userid;
		   $user->is_logged = true;
		} else {
		   $user->is_logged = false;
		   echo '{"retCode":-3,"retMsg":"TOKEN_ERROR","retType":"TO_LOGIN_PAGE"}';
		   exit;
		}
	} else if(empty($_SESSION['auth_id']) || !$user->is_logged) {

		$user->is_logged = false;

		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_secret'] = '';
		$_SESSION['auth_email'] = '';

		setcookie("auth_id", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_email", "", time() - 3600, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", "", time() - 3600, "/", COOKIE_DOMAIN);

		//$tpl->set_msg("You must be logged in to access this section.",false);
		//User::login();
		// return_to('site');
		echo '<!DOCTYPE html><html><body><script>alert("請登入 享受更多優惠 !!"); location.href="/site/member/userlogin/";</script></body></html>';
	    exit;
	}

}

function enterprise_login_required()
{
	global $enterprise, $tpl;

	if((!(empty($_POST['json'])))&&($_POST['json']=='Y')) {

		$retArr=array();
		if (empty($_POST['enterpriseid'])){
			$retArr['retCode']=-1;
			$retArr['retMsg']='參數企業編號錯誤';
		}else{
			if (empty($_POST['ts'])){
				$retArr['retCode']=-2;
				$retArr['retMsg']='參數ts錯誤';
			}else{
				if (empty($_POST['tk'])){
					$retArr['retCode']=-3;
					$retArr['retMsg']='參數tk錯誤';
				}else{
					$userid=(empty($_POST['userid']))?'':$_POST['userid'];
					$enterpriseid=(empty($_POST['enterpriseid']))?'':$_POST['enterpriseid'];
					$total=(empty($_POST['total']))?'':$_POST['total'];
					$expw=(empty($_POST['expw']))?'':$_POST['expw'];
					$ts=(empty($_POST['ts']))?'':$_POST['ts'];
					$tk=(empty($_POST['tk']))?'':$_POST['tk'];
					$arrSignParams=[$userid,$enterpriseid,$total,$expw,$ts,ES_SALT];
					if(confirmSign($arrSignParams, $tk)==false){
						$retArr['retCode']=-4;
						$retArr['retMsg']='參數錯誤';
					}else{
						$retArr['retCode']=1;
						$retArr['retMsg']='ok';
					}
				}
			}
		}
		if ($retArr['retCode']<0){
			echo json_encode($retArr);
			die();
		}
	}else{
		// if(empty($_SESSION['sajamanagement']['enterprise']['enterpriseid']))
		if(!isset($_SESSION['sajamanagement']['enterprise']['enterpriseid']))
		{
			//$_SESSION['sajamanagement']['enterprise']['enterpriseid'] = '';
			$_SESSION['sajamanagement']['enterprise'] = '';
			setcookie("enterpriseid", "", time() - 3600, "/", COOKIE_DOMAIN);

			//$tpl->set_msg("You must be logged in to access this section.",false);
			//User::login();

			//return_to('site');
			echo "<!DOCTYPE html><html><body><script>alert('殺價王商家請先登入');location.href='/site/enterprise/login';</script></body></html>";
			exit;
		}
	}

}

//會員等級與儲值,下標,兌換等功能權限
function query_bid_able($userid)
{
	global $db, $config;

    if(!$db) {
	   $db = new mysql($config["db"]);
	   $db->connect();
	}

    $query = "SELECT * FROM saja_user.saja_user WHERE userid='${userid}' ";
	$table = $db->getQueryRecord($query);
	$data=false;
	if($table && $table['table']['record'][0]['userid']>0) {
		$get_user=$table['table']['record'][0];

		$user_level = (!empty($get_user["level"])) ? intval($get_user["level"]) : 0;
		//會員等級小於 25為a, 否則為b
		$level = ($user_level <25) ? 'a' : 'b';
		
		$data["level_num"] = $user_level;
		$data["level"] = $level;
		$data["bid_enable"] = $get_user["bid_enable"];
		$data["dream_enable"] = $get_user["dream_enable"];
		$data["exchange_enable"] = $get_user["exchange_enable"];
		$data["deposit_enable"] = $get_user["deposit_enable"];
    }

	return $data;
}
//鎖定/解鎖 : 下標/圓夢下標/兌換功能
function set_bid_able($userid, $key='exchange_enable', $fun='N')
{
	global $user, $tpl;

	if($key=="dream_enable" && $fun=="Y"){
		//查驗圓夢是否得標
		$pay_get =$user->dream_pay_get($userid);
		if($pay_get==true){
			$fun = "Y";
		} else {
			$fun = "N";
		}
	}

	//設定 鎖定/解鎖
	$r = $user->update_bid_able($userid, $key, $fun);

	return $r;
}

function getSession($key) {
         session_start();
		 return $_SESSION[$key];
}

function setSession($key,$value) {
         session_start();
		 $_SESSION[$key]=$value;
		 return;
}
function setSession2($key1,$key2,$value) {
         session_start();
		 $_SESSION[$key1][$key2]=$value;
		 return;
}
function setSession3($key1,$key2,$key3,$value) {
         session_start();
		 $_SESSION[$key1][$key2][$key3]=$value;
		 return;
}

//傳簡訊
function sendSMS($phone, $sMessage, $countrycode='886') {

		if (strlen($phone)==10 && strpos($phone,"09")===0) {
			//台灣 簡訊王 SMS-API
			$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". urlencode($sMessage);
			$to_url = "http://202.39.48.216/kotsmsapi-1.php?".$msg;
		    if(!$getfile=file($to_url)) {
				error_log('ERROR: 簡訊王 SMS-API 無法連線 !');
				return false;
			}
			else
			{
				$term_tmp = implode('', $getfile);
				$check_kmsgid = explode('=', $term_tmp);
				$kmsgid = (int)$check_kmsgid[1];
				// error_log("[ajax/helpers.sendSMS] kmsgid : ".$kmsgid);
				error_log("[helper/sendSMS] 1 to ${phone} : ".$kmsgid);
				if($kmsgid < 0) {
					//('手机号码错误!!', $this->config->default_main ."/user/register");
					error_log('ERROR: 簡訊王 發送錯誤!');
					return false;
				}
			}
			return true;
		} else if($countrycode='86' || $countrycode='852' || $countrycode='853') {
			// 中港澳 啟瑞雲發送簡訊服務
			include_once(LIB_DIR."/sendSMS.class.php");
			$sms    = new SendSMS();
            $result = $sms->send($phone, $sMessage);
			error_log("[helper/sendSMS] 2 to ${phone} : ".$result);
			if($result) {
			   $arr = json_decode($result, TRUE);
               if(is_array($arr) && $arr['success']==TRUE ) {
                  return true;
               } else {
                  return false;
               }				   
			} else {
			   return false;	
			}
		} else {
			// 其他地區 馬來西亞的 puzlebulk 簡訊服務
			// 號碼要自行加上國碼
			$arrPost['mobile_no'] = $countrycode.$phone;
			$arrPost['sms_type'] = 1;
			$arrPost['sms_content'] =urlencode(htmlspecialchars($sMessage,ENT_QUOTES));
			$arrPost['user_id']="heidyang";
			$arrPost['secret_key']="870e4b03fe80bb8f44bee79c7d70a6f0";
			$url="http://api.pulzebulk.com/bulksms.php";

			if(empty($arrPost) || empty($arrPost['mobile_no']) || empty($arrPost['sms_content'])) {
			     return false;
		    }
			//$ret = array('retCode'=>0, 'retMsg'=>"NO_DATA");
			error_log("[helpers/sendSMS] puzlebulk post : ".json_encode($arrPost));

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_POST, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arrPost));
			$response = curl_exec($ch);
			curl_close($ch);
			error_log("[helper/sendSMS] 3 to ${arrPost['mobile_no']} : ".$response);
			$arr=json_decode($response,true);
			if($arr['status']==200) {
				return true;
			} else {
				return false;
			}
		}
		
}
//
function postSMS($url, $data=''){
		$row = parse_url($url);
		$host = $row['host'];
		$port = $row['port'] ? $row['port'] : 80;
		$file = $row['path'];
		while (list($k,$v) = each($data))
		{
			$post .= rawurlencode($k)."=".rawurlencode($v)."&";
		}
		$post = substr($post , 0 , -1);
		$len = strlen($post);
		$fp = @fsockopen( $host ,$port, $errno, $errstr, 10);
		if (!$fp) {
			return "$errstr ($errno)\n";
		} else {
			$receive = '';
			$out = "POST $file HTTP/1.0\r\n";
			$out .= "Host: $host\r\n";
			$out .= "Content-type: application/x-www-form-urlencoded\r\n";
			$out .= "Connection: Close\r\n";
			$out .= "Content-Length: $len\r\n\r\n";
			$out .= $post;
			fwrite($fp, $out);
			while (!feof($fp)) {
				$receive .= fgets($fp, 128);
			}
			fclose($fp);
			$receive = explode("\r\n\r\n",$receive);
			unset($receive[0]);
			return implode("",$receive);
		}
}


/*
	$inp=array();
	$inp['title']="testtitle".time();
	$inp['body']="testbody";
	$inp['productid']="";
	$inp['action']="";
	$inp['url_title']="";
	$inp['url']="";
	$inp['sender']="5014";
	$inp['sendto']="5014";
	$inp['page']=0;
	$inp['target']='saja'
	$rtn=sendFCM($inp);

	title 推播標題 body 推播正文 productid 商品編號 action 動作 url_title 網頁標頭 url 連結網址 sender 發送者
	page 跳轉頁面 sendto 接收者（多人以,分隔）target token的來源預設為saja王
	以上 title body sender sendto  為必填之欄位
*/
function sendFCM($inp){
	 global $db, $config;
	//過濾商品名稱內 單引號 雙引號 右斜線
	error_log("sendFCM start:".json_encode($inp));
	$push_title = $inp['title'];//$this->io->input["post"]["title"];
	$push_title = str_replace('\\', '', $push_title);
	$push_title = str_replace('\'', '', $push_title);
	$push_title = str_replace('"', '', $push_title);
	$target=(empty($inp["target"]))?'saja': $inp["target"];
	$vibrate = 1;
	$sound = 1;
	
	if (empty($inp["body"])) {
		$rtn['retCode']=-2;
		$rtn['retMsg']='推播內容不得留白';
	}else{
		$body = (empty($inp['body']))? "":$inp['body'];
		$productid = (empty($inp['productid']))? "":$inp['productid'];
		if ($productid=='') $productid=0;
		$action = (empty($inp['action']))? "":$inp['action'];
		$url_title= (empty($inp['url_title']))? "":$inp['url_title'];
		$url = (empty($inp['url']))? "":$inp['url'];
		$sendto = (empty($inp['sendto']))? "":$inp['sendto'];
		$page = (empty($inp['page']))? "":$inp['page'];
		$groupset= "";
		$rtn=array();
		if (empty($sendto)) {
			$rtn['retCode']=0;
			$rtn['retMsg']='未指定接收ID';
		}else{
			$sendtos=explode(",",$sendto);
			if ($inp['target']=='saja'){

				if (sizeof($sendtos)>1){
				$query = "SELECT token
					FROM saja_user.saja_user_device_token
					WHERE switch = 'Y'
					AND userid in ({$sendto})";
				}else{
				$query = "SELECT token
					FROM saja_user.saja_user_device_token
					WHERE switch = 'Y'
					AND userid = '{$sendto}' ";
				}
			}else{
				$query = "SELECT token
					FROM saja_user.saja_enterprise_device_token
					WHERE switch = 'Y'
					AND enterpriseid = '{$sendto}' ";
			}
			if(!$db) {
			   $db = new mysql($config["db"]);
			   $db->connect();
			}
			$table = $db->getQueryRecord($query);
			if (!empty($table["table"]["record"])) {
				$ids = array();
				foreach ($table["table"]["record"] as $key => $value) {
					$ids[]=$value['token'];
				}
				// 每批最多一千筆推播
				$ids = array_chunk($ids, 1000);

				// API access key from Google API's Console
				if ($inp['target']=='saja'){
					define( 'API_ACCESS_KEY', 'AAAAbCNeUVQ:APA91bGcY4z5iUZyWFxirakTqx6h5XR1-f9K-XMNuiUsUuqx9S2Js4Sk4KSywwlaUD4YjcAqIlLbkQQ289G_vI996dZDOU32Bu9XPGBxI7vWrHtOqTUg_rqP1iN0Nu4RxF2AswUfg93U' );
					$log_target='';
				}else{
					$log_target='enterprise_';
					//
					//for test api key
					define( 'API_ACCESS_KEY', 'AAAASBjySok:APA91bFvEjhLF3x3QiZl4SD5Z-37VzaK0ejorZe86FaHdxJa_gGsUAVlISWySFPvZ_GlnZUJiueuUcQbLK346yCd4xuIOVlkaDyaQk10-UuWumhPIER6mGn3oU67vJo707n5SrHkFl2B' );
				}
				$action_list = array(
						'type'		=> $action,
						'url_title'	=> $url_title,
						'url'		=> $url,
						'imgurl'	=> '',
						'page'		=> $page,
						'productid'	=> $productid
					);
				$action_list = json_encode($action_list);

				$msg = array(
					'title'		=> $push_title,
					'body' 		=> $body,
					'vibrate'	=> $vibrate,
					'sound'		=> $sound
				);

				// 新舊規格一起傳
				if ($action == "4") {
					switch ($page) {
						case '1':
							$action = "saja";
							break;
						case '4':
							$action = "index";
							break;
						case '5':
							$action = "saja_history";
						break;
					}
				}

				$data = array(
					'action'	=> $action,
					'productid'	=> $productid,
					'url_title'	=> $url_title,
					'url'		=> $url,
					'action_list'=> "{$action_list}"
				);

				$headers = array(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);

				foreach ($ids as $key => $value) {
					//接收者token $ids
					$fields = array(
						'registration_ids' 	=> $value,
						'notification'		=> $msg,
						'data'				=> $data
					);
					error_log("[broadcast_fcm]fields: ". json_encode($fields));

					$ch = curl_init();
					curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec( $ch );
					error_log("[broadcast_fcm]result: ". json_encode($result));
					curl_close( $ch );
				}
				for ($i=0;$i<sizeof($sendtos);$i++){
					$query ="
					INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_{$log_target}msg_history`
					SET
						`title`			= '{$push_title}',
						`groupset`		= '{$groupset}',
						`body`			= '{$body}',
						`action`		= '{$inp["action"]}',
						`productid`		= {$productid},
						`url_title`		= '{$inp["url_title"]}',
						`url`			= '{$inp["url"]}',
						`page`			= '{$inp["page"]}',
						`sender`		= '{$inp["sender"]}',
						`sendto`		= '{$sendtos[$i]}',
						`sendtime`		= now(),
						`pushed`		= 'Y',
						`is_hand`       = 'N',
						`push_type`     = 'S',
						`insertt`		= now()
					" ;
					$db->query($query);
				}
				$rtn['retCode']=1;
				$rtn['retMsg']='finish';
			}else{
				$rtn['retCode']=-1;
				$rtn['retMsg']='指定ID不具有效token';
			}
		}
	}
	error_log("sendFCM end :".json_encode($rtn));
	return $rtn;
}


/*
    $actid 為必要參數其餘視需調整
          $ary_bidder = array('actid'   => 'BID_WINNER',
                    'name'    => $winner_name,
                    'productid' => "{$product['productid']}",
                    'status'  => $status,
                    'thumbnail_file' => $thumbnail_file ,
                    'thumbnail_url' => $r['thumbnail_url']
                );
*/

function sendSocketIO($pt){
	require_once __DIR__.'/socketio/vendor/autoload.php';
	$ret=array();
	$ret['retCode']=0;
	//$client = new SocketIO('api.saja.com.tw', 3336);
	$client = new Client(new Version2X('https://api.saja.com.tw:3336'));
	$client->initialize();
	try{
	    $ary_bidder=$pt;
	    $ary_bidder['ts']=time();
	    $ary_bidder['sign']=MD5($ary_bidder['ts']."|sjW333-_@");
	    $ts = time();
	    $rt1=$client->emit('subscribe',array('room'=>$ary_bidder['enterpriseid'],'ts'=>$ary_bidder['ts'],'sign'=>$ary_bidder['sign']));
	    $rt2=$client->emit('send_message',array('room'=>$ary_bidder['enterpriseid'],'message'=>json_encode($ary_bidder)));
	}catch(Exception $e){
		$ret['retCode']=$e->getCode();
		$ret['retCode']=$e->getMessage();
	}finally{
		return $ret;
	}

}
function sendSocket($pt){
  require("/var/www/html/site/lib/vendor/autoload.php"); // 載入wss client 套件(composer)
  $ret=array();
  $ret['retCode']=0;
  try{
    $ary_bidder=$pt;
    $ary_bidder['ts']=time();
    $ary_bidder['sign']=MD5($ary_bidder['ts']."|sjW333-_@");
    $ts = time();
    $wss_url=(in_array(array('mgr.saja.com.tw','www.saja.com.tw','saja.com.tw'),$_SERVER['HTTP_HOST']))?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
    $client = new WebSocket\Client($wss_url);
    $client->send(json_encode($ary_bidder));
  }catch(Exception $e){
    $ret['retCode']=$e->getCode();
    $ret['retCode']=$e->getMessage();
  }finally{
    return $ret;
  }
}

function getPhoneAddress($number) {

	if (strlen($number) != 11){
		if((strlen($number) == 10) && (substr((string)$number, 0, 2) == 09)) {
			$array['retCode']=1;
			$array['retMsg']='OK';
			$array['province'] = "台湾";
			$array['city'] = "台湾";
		}
		if((strlen($number) == 8) && (substr((string)$number, 0, 1) == 8)) {
			$array['retCode']=1;
			$array['retMsg']='OK';
			$array['province'] = "香港";
			$array['city'] = "香港";
		}
	}else{

		$apiurl = 'http://apis.juhe.cn/mobile/get';
        $params = array(
              'key' => 'f5b417a6997671f8c1c73f8350c9ec5b', //www.juhe.cn上申请的手机号码归属地查询接口的appkey
              'phone' => $number, //要查询的手机号码,
			  'dtype'=>'json'
        );
		$paramsString = http_build_query($params);
        $content = @file_get_contents($apiurl.'?'.$paramsString);
        error_log("[lib/helpers] getPhoneAddress : ".$content);
		$result = json_decode($content,true);
		if($result['error_code'] == '0'){
			$array=$result['result'];
			$array['retCode']=1;
			$array['retMsg']='OK';
			/*
			ex:
			"province":"浙江",
			"city":"杭州",
			"areacode":"0571",
			"zip":"310000",
			"company":"中国移动",
			"card":"移动动感地带卡"
			echo "省份：".$result['result']['province']."\r\n";
			echo "城市：".$result['result']['city']."\r\n";
			echo "区号：".$result['result']['areacode']."\r\n";
			echo "邮编：".$result['result']['zip']."\r\n";
			echo "运营商：".$result['result']['company']."\r\n";
			echo "类型：".$result['result']['card']."\r\n";
			*/
		}else{
			// echo $result['reason']."(".$result['error_code'].")";
			$array['retCode']=-1;
			$array['retMsg']=$result['reason'];
		}
		/*
		$xml = file_get_contents("http://life.tenpay.com/cgi-bin/mobile/MobileQueryAttribution.cgi?chgmobile=".$number);
		$xml = str_replace('gb2312', 'UTF-8', $xml);
		$xml = iconv('GBK', 'UTF-8', $xml);

		$ret = simplexml_load_string($xml);
		$json = json_encode($ret);
		$array = json_decode($json,TRUE);
		*/
	}
	return $array;

}

// function getCurl2($url) {//get https的内容
	// $ch = curl_init();
	// curl_setopt($ch, CURLOPT_URL,$url);
	// curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);//不输出内容
	// curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	// curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
	// $result =  curl_exec($ch);
	// curl_close ($ch);
	// return $result;
// }

function getInbetweenStrings($start, $end, $str){
    $matches = array();
    $regex = "/$start([a-zA-Z0-9_]*)$end/";
    preg_match_all($regex, $str, $matches);
    return $matches[1];
}

function sendWeixinMsg($uid, $data, $Type) {
	$options['appid'] = 'wxbd3bb123afa75e56';
    $options['appsecret'] = '367c4259039bf38a65af9b93d92740ae';
	$wx=new WeixinChat($options);
	$wx->sendCustomMessage($uid, $data, $Type);
}


function GetIP(){
	if(!empty($_SERVER["HTTP_CLIENT_IP"])){
		$cip = $_SERVER["HTTP_CLIENT_IP"];
	}
	elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
		$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	}
	elseif(!empty($_SERVER["REMOTE_ADDR"])){
		$cip = $_SERVER["REMOTE_ADDR"];
	}
	else{
		$cip = "unknown";
	}
	return $cip;
}

function enterprise_timeoutcheck($user){
	//檢查是否在log記錄中一天內除了logout外有其他操作記錄
	$query="SELECT insertt from {$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_log where user='{$user}' and act<>'logout' and insertt> (CURDATE() - interval 1 day) limit 0,1";
	$actcheck=$db->getRecord($query);
	return (sizeof($actcheck)<1);
}
function enterprise_log($user,$module,$act,$target='',$memo=''){
	 global $db, $config;
	 //新增商家版操作
	$query = "INSERT INTO `saja_user`.`{$config['default_prefix']}enterprise_log` SET `user` = '{$user}', `module` = '{$module}', `act` = '{$act}', `target` = '{$target}', `memo` = '{$memo}'";
	//var_dump($query);
	$db->query($query);
}

function getCountryData($arrCond) {
	       global $db, $config;
		   $query = " SELECT * FROM  `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}country`
		               WHERE switch='Y' ";
		   if(!empty($arrCond['isocountrycode']))
			  $query.=" AND (isocountrycode='{$arrCond['isocountrycode']}' OR isocountrycode2='{$arrCond['isocountrycode']}' ) ";
		   if(!empty($arrCond['countrycode']))
			  $query.=" AND countrycode='{$arrCond['countrycode']}' ";
		   if(!empty($arrCond['countryid']))
			  $query.=" AND countryid='{$arrCond['countryid']}' ";
	       if(!empty($arrCond['name'])) {
		      $arrCond['name']=strtoupper($arrCond['name']);
			  $query.=" AND (name like '{$arrCond['name']}' OR UPPER(ename)='{$arrCond['name']}') ";
		   }
		   // error_log("[helper]:".$query);
		   $table = $db->getQueryRecord($query);
		   if(empty($table['table']['record'])) {
		      return false;
		   } else {
		      return $table;
		   }
	}


function getProvinceData($arrCond) {
	global $db, $config;
	$query = " SELECT * FROM  `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province`
			   WHERE switch='Y' ";
	if(!empty($arrCond['provinceid']))
	  $query.=" AND provinceid='{$arrCond['provinceid']}' ";
	if(!empty($arrCond['name'])) {
	  $arrCond['name']=strtoupper($arrCond['name']);
	  $query.=" AND (name='{$arrCond['name']}' OR UPPER(ename)='{$arrCond['name']}') ";
	}
	// error_log("[helper]:".$query);
	$table = $db->getQueryRecord($query);
	if(empty($table['table']['record'])) {
	  return false;
	} else {
	  return $table;

	}
}

function getChannelData($arrCond) {
	global $db, $config;
	$query = " SELECT * FROM  `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel`
			   WHERE switch='Y' ";
	if(!empty($arrCond['channelid']))
	  $query.=" AND channelid='{$arrCond['provinceid']}' ";
	if(!empty($arrCond['name'])) {
	  $arrCond['name']=strtoupper($arrCond['name']);
	  $query.=" AND (name='{$arrCond['name']}' OR UPPER(ename)='{$arrCond['name']}') ";
	}
	error_log($query);
	$table = $db->getQueryRecord($query);
	if(empty($table['table']['record'])) {
	  return false;
	} else {
	  return $table;
	}
}

function bidrank($product, $price, $type, $price_start, $price_stop, $tickets, $userid){
	global $db, $config;
	// 非閃殺商品下標, 結標一分鐘以前, 單次下標將提示順位
	if($type == 'single') {

		if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > 60) ) {
			// 檢查重複
			$query= "SELECT count(userid) as cnt
					   FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history`
					  WHERE productid='{$product['productid']}'
						AND price={$price}
						AND switch='Y' ";

			$table = $db->getQueryRecord($query);
			$dup_cnt=$table['table']['record'][0]['cnt'];
			// error_log("[ajax/weixinbid]Dup chk: ".$query." --> ".$dup_cnt);
			if($dup_cnt>1) {
				// 重複出價
				$r['rank']=-1;
			} else if($dup_cnt<=1) {
				// 無重複出價
				$query1 = "SELECT count(*) as cnt FROM  `{$config['db'][4]["dbname"]}`.`v_unique_price`
						   WHERE productid='{$product['productid']}' AND price<{$price} ";
				// error_log("[ajax/weixinbid]rank chk cnt : ".$query1);
				$table1 = $db->getQueryRecord($query1);
				$r['rank'] =$table1['table']['record'][0]['cnt'];
				error_log("[ajax/weixinbid]rank : ".$r['rank']);
			}
		} else {
			// 不提示
			$r['total_num']=-1;
		}
	}

	// 非閃殺商品下標, 結標一分鐘以前, 區間下標10標以上, 將提示該區間內唯一出价個數, 及該次下標的唯一最低價
	if($type == 'range') {
		if ($product['offtime'] > 0 && ($product['offtime'] - $product['now'] > 60) ) {
			$r['total_num']=$tickets;
			// error_log("range tickets : ".$this->tickets);
			// 顯示提示
			// 取得該區間內唯一出價的個數
			if($tickets>1) {
				$query="SELECT * FROM saja_shop.saja_history sh JOIN saja_shop.v_unique_price up
						   ON sh.productid=up.productid AND sh.price=up.price AND sh.productid='{$product['productid']}' AND sh.switch='Y'
						WHERE sh.switch='Y'
						  AND sh.productid='{$product['productid']}'
						  AND sh.price between {$price_start} AND {$price_stop}
						  AND sh.userid='{$userid}' ORDER BY sh.price asc ";

				// error_log("[ajax/weixinbid]range chk: ".$query);
				$table = $db->getQueryRecord($query);
				error_log("[ajax/weixinbid]range chk count:".count($table['table']['record'])."-->lowest".$table['table']['record'][0]['price']);

				if(empty($table['table']['record'])) {
				   // 無唯一出价
				   $r['num_uniprice'] = 0;
				   $r['lowest_uniprice'] = 0;
				} else{
				   // 有唯一出价
				   $r['num_uniprice'] = count($table['table']['record']);
				   $r['lowest_uniprice'] = sprintf("%01.2f",$table['table']['record'][0]['price']);
				}
			} else {
				// 不提示
				$r['total_num']=-1;
			}
		} else {
			// 不提示
			$r['total_num']=-1;
		}
	}
	return $r;

}

/**
 * utf8字符转换成Unicode字符
 * @param  [type] $utf8_str Utf-8字符
 * @return [type]           Unicode字符
 */
function utf8_str_to_unicode($utf8_str) {
    // src code from :https://segmentfault.com/a/1190000003020776
	$unicode = 0;
    $unicode = (ord($utf8_str[0]) & 0x1F) << 12;
    $unicode |= (ord($utf8_str[1]) & 0x3F) << 6;
    $unicode |= (ord($utf8_str[2]) & 0x3F);
    return dechex($unicode);
}

/**
 * Unicode字符转换成utf8字符
 * @param  [type] $unicode_str Unicode字符
 * @return [type]              Utf-8字符
 */
function unicode_to_utf8($unicode_str) {
    // src code from :https://segmentfault.com/a/1190000003020776
	if(empty($unicode_str)) return;
	$utf8_str = '';
    $code = intval(hexdec($unicode_str));
    //这里注意转换出来的code一定得是整形，这样才会正确的按位操作
    $ord_1 = decbin(0xe0 | ($code >> 12));
    $ord_2 = decbin(0x80 | (($code >> 6) & 0x3f));
    $ord_3 = decbin(0x80 | ($code & 0x3f));
    $utf8_str = chr(bindec($ord_1)) . chr(bindec($ord_2)) . chr(bindec($ord_3));
    return $utf8_str;
}

/*
   $product: 商品資料[array]
   $bid_type : 下標方式 (single/range/lazy)
   $price : 單次的出價
   $price_start : 區間出價的起始值
   $price_end : 區間出價的結束值
   $price_unit:出價最小單位(預設0.01)
*/
function getBidTotalFee($product, $bid_type, $price, $price_start=0, $price_end=0, $price_unit=0.01) {
		$saja_fee_amount =0;
		$fee_total = 0;
		$bid_total = 0;
		if($bid_type=='single' || $price_end==0) {
		    $fee_total = $product['saja_fee'];
            $bid_total = $price;
		} else if ($bid_type=='range' || $bid_type=='lazy') {
		    $bid_count = (($price_end-$price_start)/$price_unit)+1;
			$fee_total = $product['saja_fee']*$bid_count;
			$bid_total = ($price_start+$price_end)*$bid_count/2;
		}
		/*
		   $product['totalfee_type'] : 下標總金額計算方式(O:免費,F:手續費,B:出價金額,A:手續費+出價金額)
		*/
		switch($product['totalfee_type']) {
		      case 'A':$saja_fee_amount = $bid_total+$fee_total;break;
              case 'B':$saja_fee_amount = $bid_total;break;
			  case 'O':$saja_fee_amount =0;break;
			  case 'F':
		      default:$saja_fee_amount = $fee_total;break;
		}
        return $saja_fee_amount;
}

    // 回傳用戶目前所用的瀏覽媒體
    function browserType() {
            $agent = strtolower($_SERVER['HTTP_USER_AGENT']);
            error_log("[helpers/browserType] user agent:".$agent);
            // 一般瀏覽器
            $btype="browser";

            if (strpos($agent, 'micromessenger')>0 ) {
                 $btype="weixin";
            } else if (strpos($agent, 'line')>0 ) {
                 $btype="line";
            } else if (strpos($agent, 'fban')>0 || strpos($agent, 'fbav')>0 || strpos($agent, 'facebook')>0) {
                 $btype="fb";
            } else if(strpos($agent,'alipayclient')>0) {
                 $btype="alipay";
            } else if(strpos($agent,'jkopay')>0) {
                 $btype="jkopay";
            }
            error_log("[helpers/browserType] browserType:".$btype);
            return $btype;
    }

// 由第三方sso uid 取得登入資料
    function getUserBySSO($uid, $type='', $uid2='')
	{
		global $db, $config;

        if (empty($uid) && empty($uid2)){
			return false;
		}

		$query = "SELECT u.*, s.ssoid, s.name as provider_name, s.uid as provider_uid, s.uid2 as union_id, s.switch as sso_switch
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso` s
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` us ON
			s.prefixid = us.prefixid
			AND s.ssoid = us.ssoid
			AND us.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u ON
			us.prefixid = u.prefixid
			AND us.userid = u.userid
			AND u.switch = 'Y'
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.name = '{$type}'
			AND s.switch = 'Y'
			AND u.userid IS NOT NULL
		";

		if (!empty($uid) && !empty($uid2)){
			$query .= " AND (s.uid = '{$uid}' OR s.uid2 = '{$uid2}') ";
		}

		if (!empty($uid) && empty($uid2)){
			$query .= " AND s.uid = '{$uid}' ";
		}

		if (empty($uid) && !empty($uid2)){
			$query .= " AND s.uid2 = '{$uid2}' ";
		}
		error_log("[helpers/getUserBySSO] : ".$query);

		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record']) ) {
			return false;
		} else if($table['table']['record'][0]['switch'] != 'Y' || $table['table']['record'][0]['sso_switch'] != 'Y') {
			return false;
		} else {
			$query ="SELECT `phone`
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				WHERE `prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$table['table']['record'][0]['userid']}'
				AND `switch`='Y'";

			$profile = $db->getQueryRecord($query);
			error_log("[helpers/getUserBySSO] : ".$query);

			$table['table']['record'][0]['phone'] = $profile['table']['record'][0]['phone'];
		}

		return $table['table']['record'][0];
	}

    // 輸入 : 第三方授權的資料json string
    // $auth_by: "line", "fb" ,weixin 等
    // $strOauth : 第三方授權回傳的資料(json 格式的string)
    // $strState : 額外攜帶的參數
    // 回傳 : 可以產生SSO 的data array
    function genSSOData($auth_by, $strOauth='', $strState='') {

           global $db, $config;

           if(empty($strOauth)) {
              return false;
           }
           $arrOauth = json_decode(urldecode($strOauth), true);

           $arrState = array();
           if(!empty($strState))
              $arrState = json_decode(urldecode($strState), true);

           $ts=str_replace(".","",microtime(true));

           switch ($auth_by) {
               case "line" :
                     $arrSSO = array(
                        'auth_by' 		=> "line",
                        'sso_data' 		=> $strOauth,
                        'state' 		=> $strState,
                        'nickname' 		=> $arrOauth['displayName'],
                        'headimgurl'    => $arrOauth['pictureUrl'],
                        'sso_name' 		=> "line",
                        'phone'         => "line_".$ts,
                        'type' 			=> "sso",
                        'gender'        => "0",
                        'sso_uid' 		=> $arrOauth['userId'],
                        'sso_uid2' 		=> "",
                        'user_src'      => $arrState['user_src']
                      );
                      break;

                case 'fb':
                      $arrSSO = array(
                        'auth_by' 		=> "fb",
                        'sso_data' 		=> $strOauth,
                        'state' 		=> $strState,
                        'nickname' 		=> $arrOauth['name'],
                        'headimgurl'    => $arrOauth['picture']['data']['url'],
                        'sso_name' 		=> "fb",
                        'phone'         => "fb_".$ts,
                        'type' 			=> "sso",
                        'gender'        => "",
                        'sso_uid' 		=> $arrOauth['id'],
                        'sso_uid2' 		=> "",
                        'user_src'      => $arrState['user_src']
                      );
                      break;
                case 'weixin':
                      $arrSSO = array(
                        'auth_by' 		=> "weixin",
                        'sso_data' 		=> $strOauth,
                        'state' 		=> $strState,
                        'nickname' 		=> $arrOauth['nickname'],
                        'headimgurl'    => $arrOauth['headimgurl'],
                        'sso_name' 		=> "weixin",
                        'phone'         => "weixin_".$ts,
                        'type' 			=> "sso",
                        'json'          => "Y",
                        'gender'        => "",
                        'sso_uid' 		=> $arrOauth['openid'],
                        'sso_uid2' 		=> $arrOauth['unionid'],
                        'user_src'      => $arrState['user_src']
                      );
                      break;
           }
           return $arrSSO;
    }

    // 將傳入的用戶設定為已登入狀態
    function setLogin($user, $usermodel='') {

        global $db, $config;

		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';
		$_SESSION['sessid']= '';

		$query = "SELECT *
		            FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		           WHERE `prefixid` = '{$config['default_prefix_id']}'
                     AND `userid` = '{$user['userid']}'
                     AND `switch` =  'Y' ";

		$table = $db->getQueryRecord($query);

		$user['profile'] = $table['table']['record'][0];

        $sessid = session_id();

       	$_SESSION['user'] = $user;
		$_SESSION['auth_id'] = $user['userid'];
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = md5($user['userid'] . $user['name']);
        $_SESSION['sessid']=$sessid;

        // $_SESSION['token']=md5($sessid);
        $redis=getRedis('','');
        if($redis) {
           $redis->set("SESSID:".$user['userid'],$sessid);
        }

		setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", md5($user['userid'] . $user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);
        setcookie("sessid",$sessid, time()+60*60*24*30, "/", COOKIE_DOMAIN);
        setcookie("auth_nickname",$user['profile']['nickname'], time()+60*60*24*30, "/", COOKIE_DOMAIN);

		// Set local publiciables with the user's info.
		if(isset($usermodel) && !empty($usermodel)) {
			$usermodel->user_id = $user['userid'];
			$usermodel->name = $user['profile']['nickname'];
			$usermodel->email = '';
			$usermodel->ok = true;
			$usermodel->is_logged = true;
            return $usermodel;
		} else {
            return true;
        }
    }

    // Add By Thomas 20181211 移除emoji 表情特殊字元
    function remove_emoji($text){
      return preg_replace('/[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0077}\x{E006C}\x{E0073}\x{E007F})|[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0073}\x{E0063}\x{E0074}\x{E007F})|[\x{1F3F4}](?:\x{E0067}\x{E0062}\x{E0065}\x{E006E}\x{E0067}\x{E007F})|[\x{1F3F4}](?:\x{200D}\x{2620}\x{FE0F})|[\x{1F3F3}](?:\x{FE0F}\x{200D}\x{1F308})|[\x{0023}\x{002A}\x{0030}\x{0031}\x{0032}\x{0033}\x{0034}\x{0035}\x{0036}\x{0037}\x{0038}\x{0039}](?:\x{FE0F}\x{20E3})|[\x{1F441}](?:\x{FE0F}\x{200D}\x{1F5E8}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F467})|[\x{1F468}](?:\x{200D}\x{1F468}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F466}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467}\x{200D}\x{1F466})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F467})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F469}\x{200D}\x{1F466})|[\x{1F469}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F469})|[\x{1F469}\x{1F468}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F468})|[\x{1F469}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F48B}\x{200D}\x{1F469})|[\x{1F469}\x{1F468}](?:\x{200D}\x{2764}\x{FE0F}\x{200D}\x{1F48B}\x{200D}\x{1F468})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B3})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B2})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B1})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F9B0})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F9B0})|[\x{1F575}\x{1F3CC}\x{26F9}\x{1F3CB}](?:\x{FE0F}\x{200D}\x{2640}\x{FE0F})|[\x{1F575}\x{1F3CC}\x{26F9}\x{1F3CB}](?:\x{FE0F}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FF}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FE}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FD}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FC}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FB}\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F9B8}\x{1F9B9}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F9DE}\x{1F9DF}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F46F}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93C}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{200D}\x{2640}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FF}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FE}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FD}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FC}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{1F3FB}\x{200D}\x{2642}\x{FE0F})|[\x{1F46E}\x{1F9B8}\x{1F9B9}\x{1F482}\x{1F477}\x{1F473}\x{1F471}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F9DE}\x{1F9DF}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F46F}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93C}\x{1F93D}\x{1F93E}\x{1F939}](?:\x{200D}\x{2642}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F692})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F680})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2708}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3A8})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3A4})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F4BB})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F52C})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F4BC})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3ED})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F527})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F373})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F33E})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2696}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F3EB})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{200D}\x{1F393})|[\x{1F468}\x{1F469}](?:\x{1F3FF}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FE}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FD}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FC}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{1F3FB}\x{200D}\x{2695}\x{FE0F})|[\x{1F468}\x{1F469}](?:\x{200D}\x{2695}\x{FE0F})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FF})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FE})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FD})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FC})|[\x{1F476}\x{1F9D2}\x{1F466}\x{1F467}\x{1F9D1}\x{1F468}\x{1F469}\x{1F9D3}\x{1F474}\x{1F475}\x{1F46E}\x{1F575}\x{1F482}\x{1F477}\x{1F934}\x{1F478}\x{1F473}\x{1F472}\x{1F9D5}\x{1F9D4}\x{1F471}\x{1F935}\x{1F470}\x{1F930}\x{1F931}\x{1F47C}\x{1F385}\x{1F936}\x{1F9D9}\x{1F9DA}\x{1F9DB}\x{1F9DC}\x{1F9DD}\x{1F64D}\x{1F64E}\x{1F645}\x{1F646}\x{1F481}\x{1F64B}\x{1F647}\x{1F926}\x{1F937}\x{1F486}\x{1F487}\x{1F6B6}\x{1F3C3}\x{1F483}\x{1F57A}\x{1F9D6}\x{1F9D7}\x{1F9D8}\x{1F6C0}\x{1F6CC}\x{1F574}\x{1F3C7}\x{1F3C2}\x{1F3CC}\x{1F3C4}\x{1F6A3}\x{1F3CA}\x{26F9}\x{1F3CB}\x{1F6B4}\x{1F6B5}\x{1F938}\x{1F93D}\x{1F93E}\x{1F939}\x{1F933}\x{1F4AA}\x{1F9B5}\x{1F9B6}\x{1F448}\x{1F449}\x{261D}\x{1F446}\x{1F595}\x{1F447}\x{270C}\x{1F91E}\x{1F596}\x{1F918}\x{1F919}\x{1F590}\x{270B}\x{1F44C}\x{1F44D}\x{1F44E}\x{270A}\x{1F44A}\x{1F91B}\x{1F91C}\x{1F91A}\x{1F44B}\x{1F91F}\x{270D}\x{1F44F}\x{1F450}\x{1F64C}\x{1F932}\x{1F64F}\x{1F485}\x{1F442}\x{1F443}](?:\x{1F3FB})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F8}\x{1F1F9}\x{1F1FA}](?:\x{1F1FF})|[\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F0}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1FA}](?:\x{1F1FE})|[\x{1F1E6}\x{1F1E8}\x{1F1F2}\x{1F1F8}](?:\x{1F1FD})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F7}\x{1F1F9}\x{1F1FF}](?:\x{1F1FC})|[\x{1F1E7}\x{1F1E8}\x{1F1F1}\x{1F1F2}\x{1F1F8}\x{1F1F9}](?:\x{1F1FB})|[\x{1F1E6}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1ED}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F7}\x{1F1FB}](?:\x{1F1FA})|[\x{1F1E6}\x{1F1E7}\x{1F1EA}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FE}](?:\x{1F1F9})|[\x{1F1E6}\x{1F1E7}\x{1F1EA}\x{1F1EC}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F7}\x{1F1F8}\x{1F1FA}\x{1F1FC}](?:\x{1F1F8})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EB}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F0}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1F7})|[\x{1F1E6}\x{1F1E7}\x{1F1EC}\x{1F1EE}\x{1F1F2}](?:\x{1F1F6})|[\x{1F1E8}\x{1F1EC}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F3}](?:\x{1F1F5})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1EE}\x{1F1EF}\x{1F1F2}\x{1F1F3}\x{1F1F7}\x{1F1F8}\x{1F1F9}](?:\x{1F1F4})|[\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}](?:\x{1F1F3})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1EC}\x{1F1ED}\x{1F1EE}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F4}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FF}](?:\x{1F1F2})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1EE}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1F1})|[\x{1F1E8}\x{1F1E9}\x{1F1EB}\x{1F1ED}\x{1F1F1}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FD}](?:\x{1F1F0})|[\x{1F1E7}\x{1F1E9}\x{1F1EB}\x{1F1F8}\x{1F1F9}](?:\x{1F1EF})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EB}\x{1F1EC}\x{1F1F0}\x{1F1F1}\x{1F1F3}\x{1F1F8}\x{1F1FB}](?:\x{1F1EE})|[\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F5}\x{1F1F8}\x{1F1F9}](?:\x{1F1ED})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1E9}\x{1F1EA}\x{1F1EC}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}](?:\x{1F1EC})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F9}\x{1F1FC}](?:\x{1F1EB})|[\x{1F1E6}\x{1F1E7}\x{1F1E9}\x{1F1EA}\x{1F1EC}\x{1F1EE}\x{1F1EF}\x{1F1F0}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F7}\x{1F1F8}\x{1F1FB}\x{1F1FE}](?:\x{1F1EA})|[\x{1F1E6}\x{1F1E7}\x{1F1E8}\x{1F1EC}\x{1F1EE}\x{1F1F2}\x{1F1F8}\x{1F1F9}](?:\x{1F1E9})|[\x{1F1E6}\x{1F1E8}\x{1F1EA}\x{1F1EE}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F8}\x{1F1F9}\x{1F1FB}](?:\x{1F1E8})|[\x{1F1E7}\x{1F1EC}\x{1F1F1}\x{1F1F8}](?:\x{1F1E7})|[\x{1F1E7}\x{1F1E8}\x{1F1EA}\x{1F1EC}\x{1F1F1}\x{1F1F2}\x{1F1F3}\x{1F1F5}\x{1F1F6}\x{1F1F8}\x{1F1F9}\x{1F1FA}\x{1F1FB}\x{1F1FF}](?:\x{1F1E6})|[\x{00A9}\x{00AE}\x{203C}\x{2049}\x{2122}\x{2139}\x{2194}-\x{2199}\x{21A9}-\x{21AA}\x{231A}-\x{231B}\x{2328}\x{23CF}\x{23E9}-\x{23F3}\x{23F8}-\x{23FA}\x{24C2}\x{25AA}-\x{25AB}\x{25B6}\x{25C0}\x{25FB}-\x{25FE}\x{2600}-\x{2604}\x{260E}\x{2611}\x{2614}-\x{2615}\x{2618}\x{261D}\x{2620}\x{2622}-\x{2623}\x{2626}\x{262A}\x{262E}-\x{262F}\x{2638}-\x{263A}\x{2640}\x{2642}\x{2648}-\x{2653}\x{2660}\x{2663}\x{2665}-\x{2666}\x{2668}\x{267B}\x{267E}-\x{267F}\x{2692}-\x{2697}\x{2699}\x{269B}-\x{269C}\x{26A0}-\x{26A1}\x{26AA}-\x{26AB}\x{26B0}-\x{26B1}\x{26BD}-\x{26BE}\x{26C4}-\x{26C5}\x{26C8}\x{26CE}-\x{26CF}\x{26D1}\x{26D3}-\x{26D4}\x{26E9}-\x{26EA}\x{26F0}-\x{26F5}\x{26F7}-\x{26FA}\x{26FD}\x{2702}\x{2705}\x{2708}-\x{270D}\x{270F}\x{2712}\x{2714}\x{2716}\x{271D}\x{2721}\x{2728}\x{2733}-\x{2734}\x{2744}\x{2747}\x{274C}\x{274E}\x{2753}-\x{2755}\x{2757}\x{2763}-\x{2764}\x{2795}-\x{2797}\x{27A1}\x{27B0}\x{27BF}\x{2934}-\x{2935}\x{2B05}-\x{2B07}\x{2B1B}-\x{2B1C}\x{2B50}\x{2B55}\x{3030}\x{303D}\x{3297}\x{3299}\x{1F004}\x{1F0CF}\x{1F170}-\x{1F171}\x{1F17E}-\x{1F17F}\x{1F18E}\x{1F191}-\x{1F19A}\x{1F201}-\x{1F202}\x{1F21A}\x{1F22F}\x{1F232}-\x{1F23A}\x{1F250}-\x{1F251}\x{1F300}-\x{1F321}\x{1F324}-\x{1F393}\x{1F396}-\x{1F397}\x{1F399}-\x{1F39B}\x{1F39E}-\x{1F3F0}\x{1F3F3}-\x{1F3F5}\x{1F3F7}-\x{1F3FA}\x{1F400}-\x{1F4FD}\x{1F4FF}-\x{1F53D}\x{1F549}-\x{1F54E}\x{1F550}-\x{1F567}\x{1F56F}-\x{1F570}\x{1F573}-\x{1F57A}\x{1F587}\x{1F58A}-\x{1F58D}\x{1F590}\x{1F595}-\x{1F596}\x{1F5A4}-\x{1F5A5}\x{1F5A8}\x{1F5B1}-\x{1F5B2}\x{1F5BC}\x{1F5C2}-\x{1F5C4}\x{1F5D1}-\x{1F5D3}\x{1F5DC}-\x{1F5DE}\x{1F5E1}\x{1F5E3}\x{1F5E8}\x{1F5EF}\x{1F5F3}\x{1F5FA}-\x{1F64F}\x{1F680}-\x{1F6C5}\x{1F6CB}-\x{1F6D2}\x{1F6E0}-\x{1F6E5}\x{1F6E9}\x{1F6EB}-\x{1F6EC}\x{1F6F0}\x{1F6F3}-\x{1F6F9}\x{1F910}-\x{1F93A}\x{1F93C}-\x{1F93E}\x{1F940}-\x{1F945}\x{1F947}-\x{1F970}\x{1F973}-\x{1F976}\x{1F97A}\x{1F97C}-\x{1F9A2}\x{1F9B0}-\x{1F9B9}\x{1F9C0}-\x{1F9C2}\x{1F9D0}-\x{1F9FF}]/u', '', $text);
    }

    // 移除emoji符號
    function emoji_remove($text) {
        $len = mb_strlen($text);
        $new_text = '';
        for ($i = 0; $i < $len; $i++) {
            $word = mb_substr($text, $i, 1);
            error_log("[emoji_remove] word : ".$word);
            if (strlen($word) <= 3) {
                $new_text .= $word;
            }
        }
        return $new_text;
    }

    // 檢查是否存在emoji特殊符號
    function emoji_exists($text) {
        $len = mb_strlen($text);
        for ($i = 0; $i < $len; $i++) {
            $word = mb_substr($text, $i, 1);
            if (strlen($word) > 3) {
                return true;
            }
        }
        return false;
    }

    // 轉成emoji表情的16進位字串
    function emoji_print($emoji) {
        $len = mb_strlen($emoji);
        $txt = '';
        for ($i = 0; $i < $len; $i++) {
            $hex = mb_substr($emoji, $i, 1);
            $txt .= strtolower(str_replace('%', '\x', urlencode($hex))) . "\r\n";
        }
        echo $txt;
    }

    // Add By Thomas 20181228 將http post request 抽出成為共用function
    // 因為不是給遠端Call所以回傳array即可
    function postReq($url='', $postdata='') {
           $ret = getRetJSONArray(0,"NO_DATA","JSON");

           if(empty($url)) {
              $ret['retCode']=-1;
              $ret['retMsg']="NO_URL";
              return $ret;
           }
            if(empty($postdata)) {
              $ret['retCode']=-2;
              $ret['retMsg']="NO_DATA";
              return $ret;
           }
           error_log("[lib/helps/postReq] postdata : ".json_encode($postdata));
           $stream_options = array(
                'http' => array (
                        'method' => "POST",
                        'content' => json_encode($postdata),
                        'header' => "Content-Type:application/json"
                )
            );
            $context  = stream_context_create($stream_options);
            // 送出json內容並取回結果
            $response = file_get_contents($url, false, $context);
            error_log("[lib/helps/postReq] response : ".$response);

            // 讀取json內容
            $arr = json_decode($response,true);
            return $arr;
    }

	function confirmSign($arrItem,$sign){
		return ($sign==(MD5(implode('|',$arrItem))));
	}

    // 驗簽 Add By Thomas 20181218
    // 編碼格式 MD5($arrItem[1]."|".$arrItem[2]."|"....."|".timestamp)(到秒值)
    // $arrItem 依序將參數塞入陣列
    function chkSign($arrItem, $sign) {
             $str = "";
             $baseTS=time();
             foreach($arrItem as $item) {
                $str.=$item."|";
             }
             // 時間差必須在正負三秒內  否則不會過
             for($key = $baseTS-3; $key<=$baseTS+3; ++$key) {
                 $chkSign = MD5($str.$key);
                 // error_log("[helpers] ${sign}:${chkSign}");
                 if($sign==($chkSign)) {
                    return true;
                 }
             }
             return false;
    }

    // 製簽 Add By Thomas 20181218
    // 編碼格式 MD5($arrItem[1]."|".$arrItem[2]."|"....."|".timestamp)(秒值)
    function genSign($arrItem) {
             $str = "";
             foreach($arrItem as $item) {
                $str.=$item."|";
             }
             $str.="".time();
             return MD5($str);
    }

    // 如果redis 出現異常則回傳空值
    // 讓程式視為沒有redis, 跑原本連線DB的流程
	function getRedis($ip='', $port='', $pwd='') {
	      if(empty($ip))
			  $ip='sajacache01';
		  if(empty($port))
			  $port=6379;
           if(empty($pwd))
              $pwd=$config["rank_bid_redis_auth"];
		  $redis=false;
		  try {
			  $redis = new Redis();
			  if($redis->connect($ip,$port,3)==false) {
                 return false;
              }
              $redis->auth($pwd);
	 	  } catch (Exception $e) {
			  error_log("[helpers/getRedis] error : ".$e->getMessage());
		      $redis = false;
		  } finally {
		      return $redis;
		  }
    }

    function redirect_post($url, array $data, array $headers = null) {

	// $params = array(
        // 'http' => array(
            // 'method' => 'POST',
            // 'content' => http_build_query($data)
        // )
    // );

	// if (!is_null($headers)) {
        // $params['http']['header'] = '';
        // foreach ($headers as $k => $v) {
            // $params['http']['header'] .= "$k: $v\n";
        // }
    // }

	// $ctx = stream_context_create($params);
    // $fp = @fopen($url, 'rb', false, $ctx);

	// if ($fp) {
        // echo @stream_get_contents($fp);
        // die();
    // } else {
        // Error
        // throw new Exception("Error loading '$url', $php_errormsg");
    // }
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>

    </head>
    <body>
    <form name="redirectpost" method="POST" action="<?php echo $url;?>" >
        <?php
        if ( !is_null($data) ) {
            foreach ($data as $k => $v) {
                echo '<input type="hidden" name="'.$k.'" value="'.$v.'">';
            }
        }
        ?>
    </form>
        <script type="text/javascript">
                document.forms["redirectpost"].submit();
        </script>
    </body>
    </html>

<?php
    }
// 若指定 min 和 max, 則取得 min和max 間的亂數
function getRandomNum($min='', $max='')
{
    list($usec, $sec) = explode(' ', microtime());
    srand($sec + $usec * 1000000);
    if(is_numeric($min) && is_numeric($max) && $min<=$max) {
        return rand($min, $max);
    } else {
        return rand();
    }
}

// 取得下標的Cache (redis可做為SortedSet)
function getBidCache($ip='', $port='', $pwd='')
{
    global $config;
    $redis=false;
    /*
    if(empty($ip))
        $ip='rank_bid_cache';
    if(empty($port))
        $port=6379;

    if(empty($pwd))
        $pwd=$config["rank_bid_redis_auth"];
    */
    error_log("[helpers/getBidCache] 1096 : ");
    $pwd=$config["rank_bid_redis_auth"];
    try {
        // $redis = new Redis();
        // $redis->connect($ip,$port);
        // $redis->auth($pwd);
        // 改連到redis Cluster
        if (BASE_URL=='https://www.saja.com.tw'){
        	$redis=new RedisCluster(NULL, ['sajacache02:3333','sajacache03:3333', 'sajacache04:3333', 3,3,3]);
    	}else{
    		$redis=new RedisCluster(NULL, ['localhost:6381','localhost:6382', 'localhost:6383', 3,3,3]);
    	}
    } catch (Exception $e) {
        error_log("[helpers/getBidCache] error : ".$e->getMessage());
        $redis = false;
    } finally {
        return $redis;
    }
}

// 取得DB/DB Cluster連線設定
function getDbConfig() {
      global $config;
      if($config['num_db_member']==1) {
         // DB單機
		 // error_log("[getDbConfig] return db");
         return $config["db"];
      } else {
		 // DB 叢集
         $idx="".(getRandomNum() % $config['num_db_member']);
         // error_log("[getDbConfig] return dbc".$idx);
         return $config["dbc".$idx];
      }
}

/**
 * 把杀价记录表中对应的商品数据以Sorted Set方式写入到redis缓存中
 * $productid    int    商品id
 * By WangXK5
 */
function insertDataRedis($productid = '') {
    global $bid;

    //判断是否传入商品id
    if (empty($productid)) {
        $result = array(
            "code" 	=> -2,
            "msg" 	=> "請輸入商品編號"
        );
    } else {
        //获取该商品id下的杀价记录数据
        $data = $bid->get_bid_price_stats($productid);

        //判断此商品id是否有数据
        if (empty($data)) {
            $result = array(
                "code" 	=> 0,
                "msg" 	=> "暫無數據"
            );
        } else {
            //调用连接redis的函数
            $redis = getBidCache();

            //拼接redis中的key： "PROD:RANK:"+產品編號
            $key = 'PROD:RANK:'.$productid;

            //写入redis前先删除缓存中数据
            $redis->zDelete($key);

            //for循环遍历
            for ($i = 0; $i < count($data); $i++) {
                //判断出价价格是否有角分
                //if (strpos($data[$i]['price'],'.00') !== FALSE) {
                //    $price = intval($data[$i]['price']);
                //} else {
                //    $price = number_format($data[$i]['price'],2);
                //}
                $price = intval($data[$i]['price']);
                //出价价格修改为12位
                $data[$i]['price'] = str_pad($price, 12, "0", STR_PAD_LEFT);

                //把杀价记录表中的数据写入到redis缓存中
                $redis->zAdd($key, $data[$i]['cnt'], $data[$i]['price']);
            }
            $result = array(
                "code" 	=> 1,
                "msg" 	=> "成功"
            );
        }
    }
    // echo json_encode($result);
    return $result;
}

// 把IP的中間兩段隱藏起來
function maskIP($ip) {
      if(empty($ip))
         return '';
      $arr_ip=explode(".",$ip);
      $ret = '';
      if($arr_ip && is_array($arr_ip)) {
         $ip2=strlen($arr_ip[1]);
         $arr_ip[1]='';
         for($i=0;$i<$ip2;++$i) {
             $arr_ip[1].='*';
         }
         $ip3=strlen($arr_ip[2]);
         $arr_ip[2]='';
         for($i=0;$i<$ip3;++$i) {
             $arr_ip[2].='*';
         }
         $ret = implode(".",$arr_ip);
      }
      error_log("[help/maskIP] : ".$ret);
      return $ret;
}

    // 呼叫系統 s3cmd 套件 將檔案sync到hicloud S3
    // 須先安裝 s3cmd (見 http://s3help.cloudbox.hinet.net/index.php/2015-02-12-07-08-03)
    // 用 s3cmd --configure 設置ACCESSKEY & ACCESSID
	// 並修改 .s3cfg 的signature_v2=True
    function syncToS3($FileRealPath, $BucketName='', $BucketDir='') {
        if(empty($FileRealPath)) {
           error_log("[site/libs/helpers] empty FileRealPath !!");
           return false;
        }
        if(!file_exists($FileRealPath)) {
           error_log("[site/libs/helpers] File not exists !!");
           return false;
        }
        // Bucket 預設是hicloud上的S3
        if(empty($BucketName)) {
           $BucketName='s3://img.saja.com.tw';
        }
        // Bucket目錄須以"/"結尾  否則會被當成檔案
        // 預設是商品目錄
        if(empty($BucketDir)) {
           $BucketDir='/site/images/site/product/';
        }
        $ret = shell_exec("s3cmd put --acl-public ${FileRealPath} ${BucketName}${BucketDir} ");
        error_log("[site/libs/helpers] s3cmd put --acl-public ${FileRealPath} ${BucketName}${BucketDir} ...".$ret);
        return true;
    }


	/*******信用卡代碼維護專用******/

	//顯示送出與傳送內容（測試用，非必要）
    function processMonitor($request_str, $response_str)
    {
        header('Content-Type: text/html; charset=utf-8');
        echo "Request:<br /><textarea style='width:500px;height:300px;'>" . $request_str . "</textarea><br /><br />Response:<br /><textarea style='width:500px;height:300px;'>" . $response_str . "</textarea>";
        exit(0);
    }

    // AES加密（資料加解密用）
    function dataEncrypt($plainText, $key, $iv)
    {
        $outputData='';
        try {
            $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
            //padding處理
            $len = strlen($plainText);
            $padding = $block - ($len % $block);
            $plainText .= str_repeat(chr($padding),$padding);

            $key=base64_decode($key);
            $iv=base64_decode($iv);
            $outputData=mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plainText, MCRYPT_MODE_CBC, $iv);
            if ($outputData!='') {$outputData=base64_encode($outputData);}
        } catch (Exception $ex) {
            //加密異常
            throw new Exception('資料錯誤');
            //return $ex->getMessage(); //回傳錯誤訊息
        }
        return $outputData;
    }

    // AES解密（資料加解密用）
    function dataDecrypt($encryptedTextData, $key, $iv)
    {
        $outputData='';
        try {
            $encryptedTextData=base64_decode($encryptedTextData);
            $block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
            $key=base64_decode($key);
            $iv=base64_decode($iv);

            $outputData=mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encryptedTextData, MCRYPT_MODE_CBC, $iv);
            if ($outputData=='') {return '';} //解密異常，回傳空值
            //padding處理
            $len=strlen($outputData);
            $pad = ord($outputData[$len - 1]);
            $outputData=substr($outputData, 0, $len-$pad);
        } catch (Exception $ex) {
            //解密異常
            throw new Exception('資料錯誤');
            //return $ex->getMessage(); //回傳錯誤訊息
        }
        return $outputData;
    }

    // SHA256加密
    function SHA256Encrypt($plainText)
    {
        return hash('sha256', $plainText, false);
    }

    //JSON解碼
    function json_decode_fix($json, $assoc = false){
        $json = str_replace(array("\n","\r"),"",$json);
        $json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
        $json = preg_replace('/(,)\s*}$/','}',$json);
        return json_decode($json,$assoc);
    }

    //JSON編碼
    function json_encode_fix($json){
        return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($json));
    }

	/*******信用卡代碼維護專用******/

	/* 馬來西亞提供的國際 SMS service provider
	   調用api :
       http://api.pulzebulk.com/bulksms.php?user_id=xxxxx&secret_key=xxxxxx&mobile_no=60174613139&sms_type=1&sms_content=urlencoded_message
	   回傳  {"status":200,"sms_id":"2013129-123"}
	         {"status":103,"reason":"Invalid mobile number"}

	   mobile_no要自行加上國碼, 參考
       http://en.wikipedia.org/wiki/List_of_mobile_phone_number_series_by_country
    */
    function send_sms($phone, $content, $sms_type=1, $countrycode='') {
           $arrPost['mobile_no'] = $countrycode.$phone;
           $arrPost['sms_type'] = $sms_type;
           $arrPost['sms_content'] =urlencode(htmlspecialchars($content,ENT_QUOTES));
           $arrPost['user_id']="heidyang";
           $arrPost['secret_key']="870e4b03fe80bb8f44bee79c7d70a6f0";
           $url="http://api.pulzebulk.com/bulksms.php";

           $ret = array('retCode'=>0, 'retMsg'=>"NO_DATA");
           if(empty($arrPost['mobile_no'] )) {
                  $ret['retCode']=-1;
                  $ret['retMsg']="EMPTY_MOBILE_NO";
                  return $ret;
           }
           if(empty($arrPost['sms_content'] )) {
                  $ret['retCode']=-2;
                  $ret['retMsg']="EMPTY_SMS_CONTENT";
                  return $ret;
           }
           if(empty($arrPost)) {
                  $ret['retCode']=-3;
                  $ret['retMsg']="ERROR_DATA";
                  return $ret;
           }
           error_log("[helpers/send_sms] post : ".json_encode($arrPost));

           $ch = curl_init();
           curl_setopt($ch, CURLOPT_URL, $url);
           curl_setopt($ch, CURLOPT_POST, true);
		   curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
           curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arrPost));
           $response = curl_exec($ch);
           curl_close($ch);
           error_log("[lib/send_sms] response : ".$response);

		   $arr=json_decode($response,true);
		   if($arr['status']==200) {
			   $ret['retCode']=1;
			   $ret['retMsg']="OK";
			   $ret['retObj']=$response;
		   } else {
			   $ret['retCode']=-6;
			   $ret['retMsg']=$arr['reason'];
			   $ret['retObj']=$response;
		   }
           return $ret;
   }
   
   // 電子發票應用服務API查詢用資料編碼function
   function inv_api_encode($plain, $secretKey='dUlUcUh2djR0zwlaT1JHbA=='){
           $EINV_APP_ID='EINV4202004216892';
           $EINV_APP_KEY='dUlUcUh2djR0zwlaT1JHbA==';
	       return bin2hex(hash_hmac("sha256",utf8_encode($plain) , utf8_encode($secretKey), true));
   }

	//回饋上二代: 使用者編號　訂單編號　金額　第一代回饋趴數　第二代回饋趴數//20201201leo
	function couponreward($userid, $depositid, $amount, $percent1=10,$percent2=5)
	{
		global $user, $tpl,$deposit;
		//check if already rewarded
		if ($deposit->get_rewarded($depositid)=='N'){
			
			//20210107 AARONFU 獲得推薦獎金通知
			$push_info['title']="獲得推薦獎金通知";
			$push_info['productid']=0;
			$push_info['target']="saja";
			$push_info['url_title']="";
			$push_info['url']="";
			$push_info['action']=4;
			$push_info['page']=14;
			$push_info['sender']="12";
			
			$introducer=$user->get_intro_by_id($userid);
			if ($introducer>0){
				$gift1 = ($amount/100)*$percent1;
				$gift1 = floor($gift1);
				$user->add_spoint_feedback($userid, $depositid, $introducer, $gift1);
				
				//回饋上1代
				$push_info['body']="恭喜你，獲得來自會員:{$userid} 推薦回饋獎勵《殺價幣 {$gift1}》";
				$push_info['sendto']=$introducer;
				sendFCM($push_info);
				error_log("[couponreward]: ".json_encode($push_info));
				
				$introducer2=$user->get_intro_by_id($introducer);
				if ($introducer2>0){
					$gift2 = ($amount/100)*$percent2;
					$gift2 = floor($gift2);
					$user->add_spoint_feedback($userid, $depositid, $introducer2, $gift2);
					
					//回饋上2代
					$push_info['body']="恭喜你，獲得來自會員:{$introducer} 推薦回饋獎勵《殺價幣 {$gift2}》";
					$push_info['sendto']=$introducer2;
					sendFCM($push_info);
					error_log("[couponreward]: ".json_encode($push_info));
				}
				
				//mark as rewarded
				$deposit->set_rewarded($depositid);
				
				return 1;
			}else{
				return -1;
			}
		}else{
			return -2;
		}
	}
	
	//新人完成手機驗證送 1張圓夢券
	function sms_dscode($userid, $phone=0)
	{
		global $user, $tpl, $deposit;
		
		//手機驗證送券
		$dpid = 13;
		
		//驗證圓夢券贈送活動
		$get_dscode = $deposit->get_dscode($userid, 'promote', $phone);
		if(!empty($get_dscode) ){ 
			return -1; 
		} else {
			
			//取得圓夢券贈送活動
			$get_promote = f_dscode_promotion(0, 'e', $dpid);
			error_log("[sms_dscode]:". json_encode($get_promote) );
			
			if(!empty($get_promote)){
				$num = $get_promote['spmemototal'];

				if(!empty($num) ){
					//贈送 dscode 圓夢券
					for($j=0; $j<$num; $j++){
						$dscodeid = $deposit->add_dscode($userid, 0, 'Y', 'N', 'promote', $dpid);
						
						$arr_cond['dscodeid']=$dscodeid;
						$arr_update['remark']=$phone;
						$deposit->update_dscode($arr_cond, $arr_update);
					}
				}
				
				return 1;
			}else{
				return 0;
			}
		}
	}
	
	//取得圓夢券贈送活動
	function f_dscode_promotion($driid=0, $behav='a', $dpid=0)
	{
		global $user, $tpl, $deposit;
		
		$get_promote = $deposit->get_dscode_promotion($driid, $behav, $dpid);
		error_log("[f_dscode_promotion]:". json_encode($get_promote) );
		
		$dscode_promot = array();
		
		if(!empty($get_promote)){
			$dscode_promot['spmemo'] = '';
			$dscode_promot['spmemototal'] = 0;
			
			foreach ($get_promote as $sk => $sv){
				$dscode_promot['spmemo'] .='<li><div class="de_title"><p>贈送活動:</p><p>( '. $sv["name"] .' )</p></div><div class="de_point">';
				$dscode_promot['spmemoapp'][$sk]['name'] = "贈送活動: ". $sv["name"];

				if(!empty($sv["num"])){
					$dscode_promot['spmemo'] .= $sv["num"] .'張';
					$dscode_promot['spmemoapp'][$sk]['dpid'] = $sv["dpid"];
					$dscode_promot['spmemoapp'][$sk]['num'] = $sv["num"] .'張';
					$dscode_promot['spmemototal'] = ($spmemototal + $sv["num"]);
				}

				$dscode_promot['spmemo'] .='</div></li>';
			}
			
			return $dscode_promot;
		}else{
			return array();
		}
	}

	//update idsix only
	function update_idsix(){
		global $db;
		$query = "select userid,idnum,idsix from saja_user.saja_user_profile where idsix='' and length(idnum)>10";
		$table = $db->getRecord($query);
		$data=false;
		for ($i=0;$i<sizeof($table);$i++){
			$idsix=substr(decrypt($table[$i]['idnum']),4,6);
			$userid=$table[$i]['userid'];
			$updsql="update saja_user.saja_user_profile set idsix='{$idsix}' where userid='{$userid}'";
			$db->query($updsql);
		}
		$query = "select userid,idnum,idsix from saja_user.saja_user_profile where idsix='' and length(idnum)=10";
		$table = $db->getRecord($query);
		$data=false;
		for ($i=0;$i<sizeof($table);$i++){
			$idsix=substr(($table[$i]['idnum']),4,6);
			$userid=$table[$i]['userid'];
			$updsql="update saja_user.saja_user_profile set idsix='{$idsix}' where userid='{$userid}'";
			$db->query($updsql);
		}
	}
	
	function safeStr($str) {  
		return str_replace("'","",$str);
	}
	
	//會員等級老手, 新手
	function lib_user_level($userid, $_update='Y'){
		global $db,$config, $user;
		
		//20201231 AARONFU 判斷假新手is_fake
		$user_profile = $user->get_user_profile($userid, 'Y');
		//error_log("[get_user_profile]: ". json_encode($user_profile));
		$is_fake = empty($user_profile) ? 'Y' : $user_profile['is_fake'];
		
		//會員等級以商品”結帳次數計算。票券類不計!
		//$BidCnt = $user->getPayBidCnt($userid);
		$sql="SELECT count(*) as cnt FROM (
			SELECT pgp.productid, pcr.pcid FROM `saja_shop`.`saja_pay_get_product` pgp 
			LEFT JOIN `saja_shop`.`saja_product_category_rt` pcr ON pcr.productid=pgp.productid AND pcr.prefixid='saja'
			WHERE pgp.switch='Y' AND pgp.userid='{$userid}' AND pgp.complete='Y' AND pcr.pcid !=166 AND pcr.pcid >0
			GROUP BY pgp.productid
		) t WHERE 1";
		
		error_log("[lib_user_level] query : ".$sql);
		$tmpl_raw = $db->getRecord($sql);
		error_log("[tmpl_raw]: ".json_encode($tmpl_raw));
		$BidCnt = $tmpl_raw[0]['cnt'];
		
		if($_update=='Y'){
			//更新會員等級
			$user->setUserLevel($userid, $BidCnt);
		}
		
		//UserLevel 新手: 小於25,且未上鎖($is_fake=N)
		$bid_able = query_bid_able($userid);
		$user_cnt = (int)$bid_able['level_num'];
		
		//判斷新老手會員
		if(!empty($userid) && $is_fake=='N' && intval($user_cnt) < 25){
			$userLevel = 'N'; //新手
		}else{
			$userLevel = 'Y'; //老手
		}
		
		//老手黑名單
		$black_uid = array('3629');
		
		if(!empty($userid) && in_array($userid, $black_uid) ){
			$userLevel = 'Y';
		}
		error_log("[user_profile]user:{$userid}, Level: {$userLevel}, user_cnt:{$user_cnt}, BidCnt:{$BidCnt}, is_fake:". $is_fake);
		
		$_userLevel['level'] = $userLevel;
		$_userLevel['bidcnt'] = $user_cnt;
		return $_userLevel;
	}

?>

