<?php
/*
*	信用卡支付後續處理(商城) creditcard_mall.php
*/
include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR ."/wechat.class.php");
include_once(LIB_DIR."/helpers.php");

class creditcard_ext 
{
	public $_v = array();
	
	//串接入口
	public function home($userid, $var=array())
	{
		global $mall, $member, $config, $user, $deposit;
		
		//參數:
		$pay_info = $this->_v = $var;
		$this->userid = $userid;
		$check_var = true;
		$insert_exe = false;
		error_log("[creditcard_mall/creditcard_ext/home] var :".json_encode($pay_info));
		
		//訂單支付金額
		$order_info = $mall->order_info($userid, $pay_info['Td']);
		error_log("[creditcard_mall/creditcard_ext/home] order_info :".json_encode($order_info));

		if(!empty($order_info)){

			$order_memo = json_decode($order_info["memo"]);
			$arr_cond=array();
			$arr_cond['orderid']=$pay_info['Td'];
			
			$arr_update=array();
			$arr_update['mall_status']='order';
			$arr_update['amount']= intval($pay_info['MN']);
			$arr_update['paytime']=date('Y-m-d H:i:s');
			$arr_update['memo']=json_encode($order_memo);
			$arr_update['insertid']=$userid;
			$arr_update['modifierid']=$userid;
			
			//更新 saja_order_log
			$mall->update_order_log($arr_cond, $arr_update);
			
			$ret['status'] = 0;
		} else {
			$ret['retCode']=-6;
			$ret['retMsg']='支付程式異常 !!';
			$ret['status'] = 6;
		}
		
		return $ret;
	}
	
	//支付結果
	public function pay_result($result_array=array())
	{
		global $db, $config, $mall, $user, $deposit;
		
		//參數:
		$this->_v = $result_array;
		$check_var = true;
		$insert_exe = false;
		$ret['userinfo']=array();
		
		$get_order_log = $mall->get_order_log($result_array['Td']);
		//error_log("[creditcard_mall/twcreditcard_pay_success] get_order_log: ".json_encode($get_order_log));
		
		if($get_order_log){
				$userid=$get_order_log['userid'];
				$orderid=$get_order_log['orderid'];
				$dh_status=$get_order_log['mall_status'];
				$order_memo=$get_order_log['memo'];
				$amount=round(floatval($get_order_log['amount']));
				$ret['userinfo']['userid'] = $userid;
				
				$user_profile=$user->get_user_profile($userid);
				$ret['userinfo']['nickname'] = $user_profile['nickname'];
				
				//if(!empty($order_memo)){
				//	$memo = json_decode($order_memo, true);
				//	$amount = round(floatval($memo['cash_pay']));
				//}
				error_log("[creditcard_mall/twcreditcard_pay_success] userid:".$userid.", orderid:".$orderid.", amount:".$amount.",status:".$dh_status);
                
				// Add By Thomas 2020/01/22 判斷儲值狀態, 若saja_order_log.mall_status="payment" 則不再跑後續流程  以避免重複扣點
				if($dh_status=="payment") {
					error_log("[creditcard_mall/twcreditcard_pay_success] DH Status has been 'payment' , do nothing !!");
					echo "0000";
                    return;					
				}
				// Add End
				
				if(!empty($orderid))
				{
						$TransID=$result_array['buysafeno']; //紅陽交易編號
						if($TransID==''){
							$TransID=$result_array['BuySafeNo'];//（相容信用卡交易參數）
						}
						$my_chkvalue=$result_array['web'].
									$config['creditcard']['code'].
									$TransID.
									$amount.
									$result_array['errcode'];
						error_log("[creditcard_mall/twcreditcard_pay_success] My Ori ChkValue=".$my_chkvalue);
						$my_chkvalue=strtoupper(sha1($my_chkvalue));
						error_log("[creditcard_mall/twcreditcard_pay_success] My ChkValue=".$my_chkvalue);

						// 不管異步同步, 回傳errorcode='00' 才表示成功
						if($result_array['errcode']=='00' && $my_chkvalue==$result_array['ChkValue'])
						{
							// 驗證ok, 修改訂單

							//帳號與金鑰設定（用於信用卡代碼交易）
							$key = $config['creditcard']['key']; 					//金鑰
							$encIV = $config['creditcard']['encIV']; 				//IV
							//判斷信用卡是否有資料
							if ($tokenData != '') {
								try {
									$tokenData = $this->dataDecrypt($tokenData, $key, $encIV);
								} catch (Exception $ex) {
									exit('解密失敗');
								}
								error_log("[creditcard_mall/twcreditcard_pay_success] tokenData: ".$tokenData);

								//將tokenData物件化以便存取
								$tokenDataObj= $this->json_decode_fix($tokenData);
								$verification = json_decode($tokenData, true);
								//物件化後，$tokenDataObj->paymentToken為信用卡代碼，更多參數說明請參考技術手冊
								//其中信用卡代碼、代碼認證資料、信用卡代碼有效期限、信用卡卡號末四碼均為必須妥善儲存保管的資料（下次交易均會用到）
								//以下為debug用，正式環境應刪除
								// error_log("[twcreditcard_pay_success] verificationCode :".$tokenDataObj->verificationCode." <===> ".$verification['verificationCode']);
								// $tokenData_decoded = "<br>timestamp: " . $tokenDataObj->timestamp . "<br>userID: " . $tokenDataObj->userID . "<br>paymentToken: " . $tokenDataObj->paymentToken . "<br>verificationCode: " . $tokenDataObj->verificationCode . "<br>tokenExpiryDate: " . $tokenDataObj->tokenExpiryDate . "<br>last4Cardno: " . $tokenDataObj->last4Cardno;

								//寫入信用卡資料
								// $card = $user->updUserProfileCreditCardToken($userid, json_encode($tokenDataObj));
								if (!empty($verification['paymentToken']) && !empty($verification['last4Cardno'])){
									$card = $user->addUserCreditCardToken($userid, $verification);
								}
							}

							if($dh_status=="order"){
								$arr_cond=array();
								$arr_cond['orderid']=$orderid;
								
								// 添加資訊到data欄位
								$arr_data['out_trade_no']=$result_array['Td'];
								$arr_data['vendor_no']=$result_array['buysafeno'];
								$arr_data['merchant_id']=$result_array['web'];
								$arr_data['amount']=$result_array['MN'];
								$arr_data['ApproveCode']=$result_array['ApproveCode'];
								$arr_data['Card_NO']=$result_array['Card_NO'];
								$arr_data['Card_Type']=$result_array['Card_Type'];
								$arr_data['SendType']=$result_array['SendType'];
								$arr_data['errcode']=$result_array['errcode'];
								
								$arr_update=array();
								$arr_update['data']= json_encode($arr_data);
								$arr_update['modifierid']=$userid;
								$arr_update['mall_status']='payment';
								$arr_update['paytime']=date('Y-m-d H:i:s');
								
								//更新 saja_order_log
								$mall->update_order_log($arr_cond, $arr_update);
								error_log("[creditcard_mall/twcreditcard_pay_success] update_order_log: ".json_encode($arr_update));

								// 開立電子發票 
								$Invoice = $mall->mall_Invoice($orderid, $userid, $arr_data);

							}else{
								error_log("[creditcard_mall/twcreditcard_pay_success] Duplicated Update, do nothing !!");
							}
							$ret['msg']="0000";

						}else{
							$ret['msg']="Data Consistency Check Error !!";
						}

				}else{
					$ret['msg']="Empty orderid id of ".$orderid." !!";
				}

		} else {
			$ret['msg']="Cannot find the data of ".$result_array['Td']." !!";
		}
		
		return $ret;
	}
	
	//JSON解碼
	public function json_decode_fix($json, $assoc = false) {
		$json = str_replace(array("\n","\r"),"",$json);
		$json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
		$json = preg_replace('/(,)\s*}$/','}',$json);
		return json_decode($json,$assoc);
	}
	
	// AES解密（資料加解密用）
	public function dataDecrypt($encryptedTextData, $key, $iv) {
		$outputData='';
		try {
			$encryptedTextData=base64_decode($encryptedTextData);
			$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$key=base64_decode($key);
			$iv=base64_decode($iv);

			$outputData=mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encryptedTextData, MCRYPT_MODE_CBC, $iv);
			if ($outputData=='') {return '';} //解密異常，回傳空值
			//padding處理
			$len=strlen($outputData);
			$pad = ord($outputData[$len - 1]);
			$outputData=substr($outputData, 0, $len-$pad);
		} catch (Exception $ex) {
			//解密異常
			throw new Exception('資料錯誤');
			//return $ex->getMessage(); //回傳錯誤訊息
		}
		return $outputData;
	}

}
?>
