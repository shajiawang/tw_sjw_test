<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");
//$app = new AppIni; 

$c = new IbonList;

if ($_GET['list'] == 1){
	$c->home();
}
class IbonList 
{
	public $str;
	
	// Ibon使用報表
	public function home()
	{
		global $db, $config, $usermodel;
		
		$db = new mysql($config["db2"]);
		$db->connect();

		$savename = date("YmjHis");  
		$month = sprintf("%02d",date("m")-1);
		if ($month == 0 || $month == 00){
			$month = "12";
		}  
		$title = "v_ibon_usage-$month";  

		$query = "SELECT * 
		FROM `{$config['db'][3]['dbname']}`.`v_ibon_usage`  
		WHERE 
			u_date between '2018-{$month}-01 00:00:00' AND '2018-{$month}-31 23:59:59'			
		";
		$table = $db->getQueryRecord($query);
		
		$result = $table['table']['record'];		
		
		header("Content-Type: application/vnd.ms-excel;charset=gbk");  
		header("Content-Disposition: attachment; filename=".$title.".xls");  
		header("Pragma: no-cache");  

?>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<table border="1">
	 <tr>
	  <td align="center"><?php echo "暱稱"; ?></td>
	  <td align="center"><?php echo "使用者帳號"; ?></td>
	  <td align="center"><?php echo "使用者編號"; ?></td>
	  <td align="center"><?php echo "商品名稱"; ?></td>
	  <td align="center"><?php echo "日期"; ?></td>
	  <td align="center"><?php echo "兌換數量"; ?></td>
	  <td align="center"><?php echo "總IBON點數"; ?></td>
	  <td align="center"><?php echo "總金額"; ?></td>
	 </tr>
	<?php foreach($result as $rk => $rv) { ?> 
	 <tr>
	  <td><?php echo $rv['nickname']; ?></td>
	  <td style="vnd.ms-excel.numberformat:@"><?php echo $rv['name']; ?></td>
	  <td><?php echo $rv['u_id']; ?></td>
	  <td><?php echo $rv['prname']; ?></td>
	  <td><?php echo $rv['u_date']; ?></td>
	  <td><?php echo $rv['tr_num']; ?></td>
	  <td><?php echo $rv['tr_ibons']; ?></td>
	  <td><?php echo $rv['tr_price']; ?></td>
	 </tr>
	<?php } ?>
	</table>
<?php
		return false;
	}
	
}
?>