<?php

ignore_user_abort(true)
set_time_limit(0);

while(file_get_content("gate.cfg")=="Y"){
    
    $var = array("我們要長大");
    
    unset($var);

    sleep(60); // sleep 1分鐘 (以秒為單位)
}

// 因為 unset 這個指令對於消毀陣列的效果是有限的！ 我苦思無策之時，突然想到了一個「爛」方法！由於PHP在整個程式結束後，將會有個垃圾回收機制，將這支程式所使用的記憶體空間都清掉。
// 咦…但程式結束了，我的排程呢？看好囉，我要放大絕了…


ignore_user_abort(true)
set_time_limit(0);

if(file_get_content("gate.cfg")=="Y"){
    
    $var = array("我們要長大");
    
    unset($var);

    sleep(60); // sleep 1分鐘 (以秒為單位)

    $ch = curl_init();

    $options = array( 
      CURLOPT_URL => "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'], 
      CURLOPT_HEADER => false, 
      CURLOPT_RETURNTRANSFER => true, 
      CURLOPT_TIMEOUT=>1,
      CURLOPT_USERAGENT => "Google Bot", 
      CURLOPT_FOLLOWLOCATION => true
    );

    curl_setopt_array($ch, $options);
    curl_exec($ch);

}


// CURL 是執行指定網域並取得 HTML 執始碼的指令，這邊就不多介紹了。這邊的做法是，只要讀到檔案裡的值是Y，代表我仍要繼續執行這支程式。
// 在執行完排程工作後，Sleep個1分鐘，接著再「執行自己」！這邊有個小技巧，在執行網址(自己)的時候，Timeout要設定為 1 秒(盡量短一點)。
// 否則，這個CURL將會等你執行完你剛發送給自己的請求，下場是…
// 執行發送給自己的請求 → 睡個 60 秒 → 執行發送的請求 → 再睡個60秒…(跑不完了)
// 這支程式永遠會卡在等待請求結束的時候 ( 不會有這一天了= =)
// 因此 Timeout 是絕對重要的！在發出請求後，立刻斷開，結束程式。此時另一個接到請求的網址(自己)，將會開始他的排程。而原本發送請求的程式將在結束後，該資源由記憶體做了消毀，進而達到「節能減碳」的功效。


?>