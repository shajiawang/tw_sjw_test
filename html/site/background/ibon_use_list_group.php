<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");
//$app = new AppIni; 

$c = new IbonList;

if ($_GET['list'] == 1){
	$c->home();
}
class IbonList 
{
	public $str;
	
	// Ibon使用報表
	public function home()
	{
		global $db, $config, $usermodel;
		
		$db = new mysql($config["db2"]);
		$db->connect();

		$savename = date("YmjHis");  
		$year = date("Y");
		$month = sprintf("%02d",date("m")-1);
		if ($month == 0 || $month == 00){
			$month = "12";
			$year = date("Y")-1;
		}  
		$title = "v_ibon_usage-$month-total";  

		$query = "SELECT nickname, name, u_id, SUM(tr_num) as total_num, SUM(tr_ibons) as total_ibons, SUM(tr_price) as total_price 
		FROM `{$config['db'][3]['dbname']}`.`v_ibon_usage`  
		WHERE 
			u_date between '{$year}-{$month}-01 00:00:00' AND '{$year}-{$month}-31 23:59:59'
		GROUP BY u_id		
		";
		$table = $db->getQueryRecord($query);
		
		$result = $table['table']['record'];		
		
		header("Content-Type: application/vnd.ms-excel;charset=gbk");  
		header("Content-Disposition: attachment; filename=".$title.".xls");  
		header("Pragma: no-cache");  

?>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<table border="1">
	 <tr>
	  <td align="center"><?php echo "暱稱"; ?></td>
	  <td align="center"><?php echo "使用者帳號"; ?></td>
	  <td align="center"><?php echo "使用者編號"; ?></td>
	  <td align="center"><?php echo "兌換數量"; ?></td>
	  <td align="center"><?php echo "總IBON點數"; ?></td>
	  <td align="center"><?php echo "總金額"; ?></td>
	 </tr>
	<?php foreach($result as $rk => $rv) { ?> 
	 <tr>
	  <td><?php echo $rv['nickname']; ?></td>
	  <td style="vnd.ms-excel.numberformat:@"><?php echo $rv['name']; ?></td>
	  <td><?php echo $rv['u_id']; ?></td>
	  <td><?php echo $rv['total_num']; ?></td>
	  <td><?php echo $rv['total_ibons']; ?></td>
	  <td><?php echo $rv['total_price']; ?></td>
	 </tr>
	<?php } ?>
	</table>
<?php
		return false;
	}
	
}
?>