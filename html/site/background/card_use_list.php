<?php
session_start();

include_once('/var/www/html/site/lib/config.php');
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR ."/ini.php");
include_once(BASE_DIR ."/model/user.php");
//$app = new AppIni; 

$c = new CardList;

if ($_GET['list'] == 1){
	$c->home();
}
class CardList 
{
	public $str;
	
	// 信用卡儲值對帳使用報表
	public function home()
	{
		global $db, $config, $usermodel;
		
		$db = new mysql($config["db2"]);
		$db->connect();

		$savename = date("YmjHis");  
		$month = sprintf("%02d",date("m")-1);
		if ($month == 0 || $month == 00){
			$month = "12";
		}  
		$title = "card_usage-$month";  

		$query = "SELECT d.*, dh.dhid, dh.insertname as uname, dh.spointid, dh.insertt as intime  
		FROM `{$config['db'][1]['dbname']}`.`saja_deposit` d 
		LEFT JOIN `{$config['db'][1]['dbname']}`.`saja_deposit_history` dh 
			on d.prefixid = dh.prefixid AND d.depositid = dh.depositid		
		WHERE 
			d.switch = 'Y' 
			AND dh.switch = 'Y' 
			AND (dh.insertt between '2018-{$month}-01 00:00:00' AND '2018-{$month}-31 23:59:59')			
		";
		$table = $db->getQueryRecord($query);
		
		$result = $table['table']['record'];		
		
		header("Content-Type: application/vnd.ms-excel;charset=gbk");  
		header("Content-Disposition: attachment; filename=".$title.".xls");  
		header("Pragma: no-cache");  

?>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<table border="1">
	 <tr>
	  <td align="center"><?php echo "商家自定編號"; ?></td>
	  <td align="center"><?php echo "交易金額"; ?></td>
	  <td align="center"><?php echo "交易日期"; ?></td>
	  <td align="center"><?php echo "使用者帳號"; ?></td>	  
	  <td align="center"><?php echo "使用者名稱"; ?></td>
	  <td align="center"><?php echo "訂單編號"; ?></td>
	  <td align="center"><?php echo "發票號碼"; ?></td>
	  <td align="center"><?php echo "發票種類"; ?></td>
	  <td align="center"><?php echo "發票類型"; ?></td>
	 </tr>
	<?php foreach($result as $rk => $rv) { ?> 
	 <tr>
	  <td><?php echo $rv['dhid']; ?></td>
	  <td><?php echo $rv['amount']; ?></td>
	  <td><?php echo $rv['intime']; ?></td>
	  <td style="vnd.ms-excel.numberformat:@"><?php echo $rv['userid']; ?></td>	  
	  <td><?php echo $rv['uname']; ?></td>
	  <td><?php echo $rv['depositid']; ?></td>
	  <td><?php echo $rv['invoiceno']; ?></td>
	  <td><?php echo '2'; ?></td>
	  <td><?php echo 'M'; ?></td>
	 </tr>
	<?php } ?>
	</table>
<?php
		return false;
	}
	
}
?>