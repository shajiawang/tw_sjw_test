<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
<body>		
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
			text-align:center;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content_reg {
			line-height:25px;
			position:relative;
			/*left:50%;
			margin-left:-139px;*/
		}
		#content_reg div {
			float:left;
			margin:1px;
			padding:5px;
		}
		#content_reg_td1 {
			width:70px;
			height:28px;
			clear:both;
			background-color:#CCC;
		}
		#content_reg_td2 {
			width:180px;
		}
		#content_reg_td2 input {
			width:170px;
			height:35px;
		}
		#content_reg_td3 {
			width:180px;
		}	
		#content_reg_td3 input {
			width:13px;
			height:35px;
		}	
		#content_reg_ps {
			width:262px;
			clear:both;
		}
		#content_btn {
			text-align:center;
			clear:both;
			/*left:50%;
			margin-left:-139px;*/
		}
		#content_btn_registration {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:10px;
		}
		</style>
		
<script type="text/javascript">
$(function(){
	$("#passwd").on("focus", function() { /*Do something...*/$("#passwd").val(''); }); 
	if($("#passwd").val()==""){ $("#passwd").val("******"); }
	
	$("#repasswd").on("focus", function() { /*Do something...*/$("#repasswd").val(''); }); 
	if($("#repasswd").val()==""){ $("#repasswd").val("******"); }
})
</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">免费注册</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">	
		
		<div id="content_scroll"><div id="content">
		
			<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/user_insert">
			<div id="content_reg">
				
				<div id="content_reg_td1">昵称</div><div id="content_reg_td2"><input name="nickname" type="text" /></div>
				<div id="content_reg_td1">性别</div><div id="content_reg_td3"><label for="gender">男</label><input type="radio" id="gender" name="gender" value="male" checked /> <label for="gender">女</label><input type="radio" name="gender" value="female" /></div>
				<div id="content_reg_td1">手机号码</div><div id="content_reg_td2"><input name="phone" type="text" /></div>
				<div id="content_reg_ps">↑ 请输入正确的手机号码。</div>
				<div id="content_reg_td1">密码</div><div id="content_reg_td2"><input type="password" name="passwd" id="passwd" /></div>
				<div id="content_reg_ps">↑ 以4~12个英文字母或数字为限</div>
				<div id="content_reg_td1">确认密码</div><div id="content_reg_td2"><input type="password" name="repasswd" id="repasswd" /></div>
				<div id="content_reg_ps">↑ 请再次输入密码确认</div>
			</div>
			<input type="hidden" name="email" value="">
			<input type="hidden" name="name" value="">
			<input type="hidden" name="cityid" value="">
			<input type="hidden" name="area" value="">
			<input type="hidden" name="address" value="">
			<input type="hidden" name="addressee" value="">
			<div id="content_btn"><input id="content_btn_registration" type="submit" value="确认" /></div>
			</form>
		
		</div></div>
		
		</div>
		</div>
	
	</div>
	</div>
	<?php require_once $this->tplVar['block']['footerjs']; ?>
