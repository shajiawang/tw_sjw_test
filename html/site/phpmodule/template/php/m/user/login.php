<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
			text-align:center;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content_login {
			text-align:center;
			padding: 15px;
			margin:0px 30px 20px 30px;
			background-color:#EFEFEF;
			border:1px solid #DDDDDD;
			border-radius:5px;
		}
		#content_login_text{
			line-height:25px;
		}
		#content_login_text input {
			width:185px;
			height:45px;
			margin-top:10px;
		}
		#content_btn_login {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:15px;
		}
		#content_btn_registration {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #83c41a;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #b8e356), color-stop(100%, #a5cc52) );
			background:-moz-linear-gradient( center top, #b8e356 5%, #a5cc52 100% );
			background:-ms-linear-gradient( top, #b8e356 5%, #a5cc52 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b8e356', endColorstr='#a5cc52');
			background-color:#b8e356;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #86ae47;
			-webkit-box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			-moz-box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			margin-top:12px;
		}
		</style>
		
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">注册/登录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
			<div id="content_scroll"><div id="content">
			
			<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/user_login">
			<div id="content_login">
				<div id="content_login_text"><input type="text" name="name" placeholder="手机号码/会员帐号" /></div>
				<div id="content_login_text"><input type="password" name="passwd" placeholder="请输入登录密码" /></div>
				<div><input id="content_btn_login" type="submit" value="登录" /></div>
				<?php /*<div><input id="content_btn_login" type="button" value="微信QQ登录" onclick="javascript:location.href='<?php echo $this->config->default_main; ?>/oauth/qqapi'"/></div>*/?>
				<div><input id="content_btn_login" type="button" value="新浪微博登录" onclick="javascript:location.href='<?php echo $this->config->default_main; ?>/oauth/sinaapi'" /></div>
			</div>
			<input type="hidden" name="location_url" value="<?php //echo $this->tplVar["status"]['get']['location_url']; ?>">
			</form>
			
			<div>还没有帐号吗？注册一个吧！</div>
			<div><input id="content_btn_registration" type="button" value="免费注册" onclick="javascript:location.href='<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/register'" /></div>
			<?php /*<div><input id="content_btn_registration" type="button" value="微信QQ注册" onclick="javascript:location.href='<?php echo $this->config->default_main; ?>/oauth/qqapi'" /></div>*/?>
			<div><input id="content_btn_registration" type="button" value="新浪微博注册" onclick="javascript:location.href='<?php echo $this->config->default_main; ?>/oauth/sinaapi'" /></div>
			
			</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
