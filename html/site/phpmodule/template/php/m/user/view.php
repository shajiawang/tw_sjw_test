<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>	
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			margin-top:45px;
			z-index:0;
			text-align:center;
		}
		#content_login {
			text-align:center;
			padding: 15px;
			margin:66px 30px 20px 30px;
			background-color:#EFEFEF;
			border:1px solid #DDDDDD;
			border-radius:5px;
		}
		#content_login_text{
			line-height:40px;
		}
		#content_login_text input {
			width:153px;
			height:25px;
		}
		#content_btn_login {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:10px;
		}
		#content_btn_registration {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #83c41a;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #b8e356), color-stop(100%, #a5cc52) );
			background:-moz-linear-gradient( center top, #b8e356 5%, #a5cc52 100% );
			background:-ms-linear-gradient( top, #b8e356 5%, #a5cc52 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b8e356', endColorstr='#a5cc52');
			background-color:#b8e356;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #86ae47;
			-webkit-box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			-moz-box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			margin-top:10px;
		}
		</style>
	
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">免费注册</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
		
			<div id="content_login">
				<div id="content_login_text">帐号：<input name="" type="text" /></div>
				<div id="content_login_text">密码：<input name="" type="text" /></div>
				<div><input id="content_btn_login" name="登入" type="submit" value="登入" /></div>
				<div><input id="content_btn_login" name="新浪微博登入" type="submit" value="新浪微博登入" /></div>
			</div>
			<div>还没有帐号吗？注册一个吧！</div>
			<div><input id="content_btn_registration" name="一般注册" type="submit" value="一般注册" onclick="javascript:location.href='registration.htm'" /></div>
			<div><input id="content_btn_registration" name="新浪微博注册" type="submit" value="新浪微博注册" onclick="javascript:location.href='registration.htm'" /></div>
		
		</div></div>
		
		</div>
		</div>
	
	</div>
	</div>		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		
		<?php /*
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a>後台人員管理</a>>><a>群組</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label">暱稱：</span>
					<input type="text" name="search_nickname" size="20" value="<?php echo $this->io->input['get']['search_nickname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">Email：</span>
					<input type="text" name="search_email" size="20" value="<?php echo $this->io->input['get']['search_email']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"></li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<?php $base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; ?>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_userid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_userid=desc">userid<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_userid"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_userid=">userid<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_userid=asc">userid<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>暱稱</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_email"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_email=desc">Email<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_email"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_email=">Email<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_email=asc">Email<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_provider_name"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_provider_name=desc">SSO名稱<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_provider_name"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_provider_name=">SSO名稱<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_provider_name=asc">SSO名稱<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_sso_email"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_sso_email=desc">SSO Email<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_sso_email"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_sso_email=">SSO Email<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_sso_email=asc">SSO Email<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_switch"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_switch=desc">啟用<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_switch"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_switch=">啟用<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_switch=asc">啟用<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'. $this->tplVar["status"]["get"]["fun"]; ?>/edit/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'. $this->tplVar["status"]["get"]["fun"]; ?>/delete/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"])) ;?>"></a></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="email"><?php echo $rv['email']; ?></td>
								<td class="column" name="provider_name"><?php echo $rv['provider_name']; ?></td>
								<td class="column" name="sso_email"><?php echo $rv['sso_email']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
								<td class="column" name="switch"><?php echo $rv['switch']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add">新增</a>
						</div>						
					</div>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		*/?>
	  
