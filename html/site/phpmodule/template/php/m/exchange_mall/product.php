<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
<body>		
<style type="text/css">
#content {
	height:auto;
	width:100%;
	position:relative;
	z-index:0;
}
#content_scroll {
	margin-top:45px;
}
#content_name {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	padding:12px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_img {
	text-align:center;
	padding-top:10px;
}
#content_img img{
	width:95%;
}
#content_information {
	line-height:25px;
	margin:10px;
	height:160px;
}
#content_information_tag {
	width:80px;
	height:25px;
	float:left;
	clear:both;
	color:#666;
}
#content_information_tag div{
	height:25px;
}
#content_form {
	float:left;
	margin-top:10px;
}
#content_form .form_select {
	/*height:30px;*/
}
#content_form .form_button {
	height:30px;
}
#content_information_money {
	color:#F00;
	font-weight:bold;
}
#content_title {
	margin-top:20px;
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	line-height:35px;
	padding-left:5px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_brief {
	line-height:25px;
	margin:20px;
}
#content_brief img{
	width: 100%;
}
#content_brief div {
	width: 100%;
}
#content_brief span {
	width: 100%;
}
#content_brief table{
	width: 100%;
}
#content_brief tr{
	width: 100%;
}
#content_brief td{
	width: 100%;
}
#content_brief p{
	width: 100%;
}
		
.content_btn_1 {
	font-size: 16px;
	font-family: Arial;
	font-weight: normal;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	border: 1px solid #83c41a;
	padding: 9px 12px;
	text-decoration: none;
	background: -webkit-gradient( linear, left top, left bottom, color-stop(5%, #b8e356), color-stop(100%, #a5cc52) );
	background: -moz-linear-gradient( center top, #b8e356 5%, #a5cc52 100% );
	background: -ms-linear-gradient( top, #b8e356 5%, #a5cc52 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b8e356', endColorstr='#a5cc52');
	background-color: #b8e356;
	color: #ffffff;
	display: inline-block;
	text-shadow: 1px 1px 0px #86ae47;
	-webkit-box-shadow: inset 1px 1px 0px 0px #d9fbbe;
	-moz-box-shadow: inset 1px 1px 0px 0px #d9fbbe;
	box-shadow: inset 1px 1px 0px 0px #d9fbbe;
}
.content_btn_2 {
	font-size: 16px;
	font-family: Arial;
	font-weight: normal;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	border: 1px solid #83c41a;
	padding: 9px 12px;
	text-decoration: none;
	background: -webkit-gradient( linear, left top, left bottom, color-stop(5%, #8DBC1F), color-stop(100%, #aaa) );
	background: -moz-linear-gradient( center top, #8DBC1F 5%, #aaa 100% );
	background: -ms-linear-gradient( top, #8DBC1F 5%, #aaa 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#8DBC1F', endColorstr='#aaa');
	background-color: #ddd;
	color: #ffffff;
	display: inline-block;
	text-shadow: 1px 1px 0px #86ae47;
	-webkit-box-shadow: inset 1px 1px 0px 0px #d9fbbe;
	-moz-box-shadow: inset 1px 1px 0px 0px #d9fbbe;
	box-shadow: inset 1px 1px 0px 0px #d9fbbe;
}

</style>

<script type="text/javascript">

$(function(){
	$('#content_title').on('click', function(){
		$(this).closest('#postsliding').find('#content_brief').stop().slideToggle(300);
	});
})
</script>	
<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        
		<div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">兑换商品</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">	
		
		<div id="content_scroll"><div id="content">
    
			<div id="content_name" class="name ellipsis"><?php echo $this->tplVar['table']['record'][0]['name']; ?></div>
			<div id="content_img">
				<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
				<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
				<?php endif; ?>
			</div>
			
			<div id="content_information">
				<div id="content_information_tag">市价</div><div>RMB <?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['retail_price']); ?> 元</div>
				<div id="content_information_tag">红利积分</div><div><span id="content_information_money"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?></span> 点</div>
				<div id="content_information_tag">处理费</div><div>红利积分 <?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['process_fee']); ?> 点</div>
				<div id="content_information_tag">兑换数量</div>
				
				<div id="content_form">
				<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/exchange_mall/checkout/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $this->io->input["get"]["epcid"]; ?>&epid=<?php echo $this->io->input["get"]["epid"]; ?>" enctype="multipart/form-data" >
				<div class="form_select" >
				<?php if(!empty($this->tplVar['table']['rt']['stock']) ) : ?>
					<select name="num" >
					<?php for ($i=1; $i <= $this->tplVar['table']['rt']['stock']; $i++) : ?>
						<option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
					<?php endfor; ?>
					</select>
					<?php endif ?>
				</div>
				
				<div class="form_button">
					<input type="hidden" name="checkout" value="Y">
					<input type="hidden" name="esid" value="<?php echo $this->tplVar['table']['record'][0]["esid"] ;?>">
					<?php if(!empty($this->tplVar['table']['rt']['stock']) ) : ?>
					<input type="submit" value="红利积分" class="content_btn_1">
					<?php else : ?>
					<input type="button" value="库存不足" class="content_btn_2" disabled >
					<?php endif ?>
				</div>
				</form>
				</div>
			</div>
			
			<div id="postsliding">
				<div id="content_title" >商品简介</div>
				<div id="content_brief" style="display:none;">
				<?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?>
				</div>
			</div>
		
		<?php require_once $this->tplVar['block']['footer']; ?>
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>	
	
		<?php /*
		<main role="main" id="content">
		<div class="content">
		<section class="block-frame">
		<h2>兌換商品</h2>
		<div class="body">
			
			<article class="ex_product_image">
				<div class="image">
				<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
				<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image; ?>/product/bf8a5804cad98a6d8039cc6c20447c95.jpg"/>
				<?php endif; ?>
				</div>
				<div class="clear"></div>
			</article>
				
			<div class="ex_product_info">
				<header>
					<h1>
						<a href="#<?php //echo $this->config->default_main.'/saja/?'.$this->tplVar['status']['args'].'&productid='.$this->tplVar['table']['record'][0]['productid']; ?>" class="name ellipsis">
							<?php echo $this->tplVar['table']['record'][0]['name']; ?>
						</a>
					</h1>
					<p style="text-align: center;">********************************************************</p>
				</header>
				
				<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/exchange_mall/checkout/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $this->io->input["get"]["epcid"]; ?>&epid=<?php echo $this->io->input["get"]["epid"]; ?>" enctype="multipart/form-data" >
				<ul class="info">
					<li>
						<span>市售價格：</span>
						<span><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['retail_price']); ?>元</span>
					</li>
					<li>
						<span>紅利點數：</span>
						<span><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?>點</span>
					</li>
					<li>
						<span>處理費用：</span>
						<span><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['process_fee']); ?>點</span>
					</li>
					<li>
						<span>商品數量：</span>
						<?php if(!empty($this->tplVar['table']['rt']['stock']) ) : ?>
						<select name="num" >
						<?php for ($i=1; $i <= $this->tplVar['table']['rt']['stock']; $i++) : ?>
							<option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
						<?php endfor; ?>
						</select>
						<?php endif ?>

					</li>
				</ul>
				<div class="button">
					<input type="hidden" name="checkout" value="Y">
					<input type="hidden" name="esid" value="<?php echo $this->tplVar['table']['record'][0]["esid"] ;?>">
					<span>付款方式：</span>
					<?php if(!empty($this->tplVar['table']['rt']['stock']) ) : ?>
					<input type="submit" value="使用紅利點數" class="submit">
					<?php else : ?>
					<input type="button" value="庫存不足" class="submit" disabled >
					<?php endif ?>

				</div>
				</form>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFCCCC; text-align: left; width: 770px; height: 28px; float:right; ">
			<span style="padding-left:20px;">商品介紹</span>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFF; text-align: left; width: 770px; float:right; ">
			<div style="padding-left:8px;"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></div>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFCCCC; text-align: left; width: 770px; height: 28px; float:right; ">
			<span style="padding-left:20px;">注意事項</span>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFF; text-align: left; width: 770px; float:right; ">
				<div style="padding-left:8px;">
					<p>1. 本圖僅提供參考，實際情況以實體商品為準。</p>
					<p>2. 若商品因賣家、廠商或經銷商的關係有缺貨的情況，我們會發送站內訊息通知您，若您不想等待，可選擇兌換等值商品(限一樣)或等值點點幣。</p>
					<p>3. 若您對本網站規則及說明有不明白之處，請見秒殺團購教學，或聯絡客服人員。</p>
					<p>4. 本圖及說明為yahoo購物中心所提供。查看更多商品請至Yahoo購物中心。</p>
					<p>5.本商品寄出不含贈品。</p>
				</div>
			</div>
		
		</div>
		</section>
		</div>
		
		<div class="clear"></div>
		<div id="show_ajax" style="display:none"></div>
		</main>
		*/?>
		
	
