<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
<style type="text/css">
#content {
	height:auto;
	width:100%;
	position:relative;
	z-index:0;
}
#content_scroll {
	margin-top:45px;
}
#content_name {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	padding:12px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_img {
	text-align:center;
	padding-top:10px;
}
#content_img img{
	width:95%;
}
#content_information {
	line-height:25px;
	margin:15px;
}
#content_information_tag {
	width:80px;
	float:left;
	clear:both;
	color:#666;
}
#content_information_money {
	color:#F00;
	font-weight:bold;
}
#content_title {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	line-height:35px;
	padding-left:5px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_brief {
	line-height:25px;
	margin:10px;
}
#content_brief img{
	width: 100%;
}
#content_brief div {
	width: 100%;
}
#content_brief span {
	width: 100%;
}
#content_brief table{
	width: 100%;
}
#content_brief tr{
	width: 100%;
}
#content_brief td{
	width: 100%;
}
#content_brief p{
	width: 100%;
}
</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">兑换商品</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
    
			<div id="content_name"><?php echo $this->tplVar['table']['record'][0]['name']; ?></div>
			<div id="content_img">
				<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
				<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
				<?php endif; ?>
			</div>
			<div id="content_information">
				<div id="content_information_tag">市价</div><div>RMB <?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['retail_price']); ?> 元</div>
				<div id="content_information_tag">红利积分</div><div><span id="content_information_money"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?></span> 点</div>
				<?php /*
				<div id="content_information_tag">处理费</div><div>红利积分 <?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['process_fee']); ?> 点</div>
				<div id="content_information_tag">兑换数量</div>*/?>
			</div>
			
			<div id="content_title">购买明细</div>
			
			<div id="content_brief">
				<div style="text-align: center; float:left; ">
					<div style="padding:12px 20px;">
					<span style="padding-left:10px;">确认兑换商品数据</span>
					<span style="padding-left:10px;">→</span>
					<span style="padding-left:10px;">填写收件人数据</span>
					<span style="padding-left:10px;">→</span>
					<span style="padding-left:10px;">确认收件人数据</span>
					<span style="padding-left:10px;">→</span>
					<span style="padding-left:10px; color:#FF00FF; ">购买订单完成</span>
					</div>
				</div>
					
				<div class="ex_product_info">
					<ul class="info" style="text-align: center;">
						<li>恭喜您完成兑换! 订单编号为 <?php echo $this->io->input["get"]["orderid"];?>，您可以前往会员中心查询订单。</li>
					</ul>
					
					<div class="button" >
						<table border="0">
							<tr>
								<td style="text-align: left;">
								<input type="button" value="查询订单" class="submit" style="text-align: left;" onclick="window.location.href='<?php echo $this->config->default_main; ?>/member/order/?channelid=<?php echo $this->io->input['get']['channelid']; ?>'" />
								</td>
								<td style="text-align: right;">
								<input type="button" value="前往商城" class="submit" style="text-align: right;" onclick="window.location.href='<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=1'" />
								</td>
							</tr>
						</table>
					</div>
				</div>
				
			</div>
			
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
	
		<?php /*
		<main role="main" id="content">
		<div class="content">
		<section class="block-frame">
		<h2>兑换商品</h2>
		<div class="body">
			
			<div style="text-align: center; width: 774px; height: 50px; float:left; ">
				<div style="padding:12px 20px;">
				<span style="padding-left:10px;">确认兑换商品数据</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px;">填写收件人数据</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px;">确认收件人数据</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px; color:#FF00FF; ">购买订单完成</span>
				</div>
			</div>
				
			<div style="text-align: left; width: 768px; height: 30px; border-top:1px solid #A5A5A5; border-bottom:1px solid #A5A5A5; float:right; ">
				<div style="padding:5px 40px;">兑换成功!</div>
			</div>
				
			<div class="ex_product_info">
				<ul class="info" style="text-align: center;">
					<li>恭喜您完成兑换! 订单编号为 <?php echo $this->io->input["get"]["orderid"];?>，您可以前往会员中心查询订单。</li>
				</ul>
				
				<div class="button" >
					<table border="0">
						<tr>
							<td style="width: 300px; text-align: left;">
							<input type="button" value="查询订单" class="submit" style="text-align: left;" onclick="window.location.href='<?php echo $this->config->default_main; ?>/member/order/?channelid=<?php echo $this->io->input['get']['channelid']; ?>'" />
							</td>
							<td style="width: 300px; text-align: right;">
							<input type="button" value="前往商城" class="submit" style="text-align: right;" onclick="window.location.href='<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=1'" />
							</td>
						</tr>
					</table>
				</div>
			</div>
			
		</div>
		</section>
		</div>
		
		<div class="clear"></div>
		<div id="show_ajax" style="display:none"></div>
		</main>

		*/?>
	
