<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
<style type="text/css">
#content {
	height:auto;
	width:100%;
	position:relative;
	z-index:0;
}
#content_scroll {
	margin-top:45px;
}
#content_name {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	padding:12px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_img {
	text-align:center;
	padding-top:10px;
}
#content_img img{
	width:95%;
}
#content_information {
	line-height:25px;
	margin:15px;
}
#content_information_tag {
	width:80px;
	float:left;
	clear:both;
	color:#666;
}
#content_information_money {
	color:#F00;
	font-weight:bold;
}
#content_title {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	line-height:35px;
	padding-left:5px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_brief {
	line-height:25px;
	margin:10px;
}
#content_brief img{
	width: 100%;
}
#content_brief div {
	width: 100%;
}
#content_brief span {
	width: 100%;
}
#content_brief table{
	width: 100%;
}
#content_brief tr{
	width: 100%;
}
#content_brief td{
	width: 100%;
}
#content_brief p{
	width: 100%;
}
</style>
		
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">兑换商品</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
    
			<div id="content_name"><?php echo $this->tplVar['table']['record'][0]['name']; ?></div>
			<div id="content_img">
				<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
				<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
				<?php endif; ?>
			</div>
			<div id="content_information">
				<div id="content_information_tag">市价</div><div>RMB <?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['retail_price']); ?> 元</div>
				<div id="content_information_tag">红利积分</div><div><span id="content_information_money"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?></span> 点</div>
				<?php /*
				<div id="content_information_tag">处理费</div><div>红利积分 <?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['process_fee']); ?> 点</div>
				<div id="content_information_tag">兑换数量</div>*/?>
			</div>
			
			<div id="content_title">购买明细</div>
			
			<div id="content_brief">
				<div style="text-align: center; float:left; ">
					<div style="padding:12px 20px;">
					<span style="padding-left:10px;">确认兑换商品数据</span>
					<span style="padding-left:10px;">→</span>
					<span style="padding-left:10px; color:#FF00FF;">填写收件人数据</span>
					<span style="padding-left:10px;">→</span>
					<span style="padding-left:10px;">确认收件人数据</span>
					<span style="padding-left:10px;">→</span>
					<span style="padding-left:10px;">购买订单完成</span>
					</div>
				</div>
			
				<div style="float:left; ">
					<table border="0">
						<tr style="background-color: #c9c9c9;">
							<td style="text-align: center; ">兑换数量</td>
							<td style="text-align: center; ">单价(红利积分)</td>
							<td style="text-align: center; ">兑换点数</td>
						</tr>
						<tr style="border: 1px solid #A5A5A5;">
							<td style="text-align: right; color:#FF6666;"><?php echo $this->tplVar['table']['record'][0]['num']; ?></td>
							<td style="text-align: right; "><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?></td>
							<td style="text-align: right; "><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_point']); ?></td>
						</tr>
					</table>
					<table border="0" style="background-color: #c9c9c9;">
						<tr>
							<td style="text-align: right; color:#FF6666;">本次兑换</td>
							<td style="text-align: right; "><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_point']); ?> 点</td>
						</tr>
						<tr>
							<td style="text-align: right; color:#FF6666;">处理费</td>
							<td style="text-align: right; "><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_process']); ?> 点</td>
						</tr>
						<tr>
							<td style="text-align: right; color:#FF6666;">总计</td>
							<td style="text-align: right; "><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['total_fee']); ?> 点</td>
						</tr>
					</table>
					<?php /*
					<div class="button" style="padding:5px 80px;"><input type="button" value="< 继续兑换" class="submit" onclick="window.location.href='<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=1'" ></div>
					*/?>
				</div>
				
				<div style="text-align: left;  border-top:1px solid #A5A5A5; border-bottom:1px solid #A5A5A5; float:right; ">
					<div style="padding:5px 40px;">收件人寄送资料</div>
				</div>
				
				<div class="ex_product_info">
					<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/exchange_mall/confirm_insert/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $this->io->input["get"]["epcid"]; ?>&epid=<?php echo $this->io->input["get"]["epid"]; ?>" enctype="multipart/form-data" >
					<ul class="info">
						<li>
							<table border="0" style="text-align: left; ">
								<tr>
									<td >姓名:</td>
									<td >
										<input type="text" id="name" name="name" value="<?php echo $this->tplVar['table']['rt']['user_profile']['addressee']; ?>" /> 
										<input type="radio" id="male" name="gender" value="male" <?php echo ($this->tplVar['table']['rt']['user_profile']['gender'] != 'female') ? 'checked' : ''; ?> />先生 
										<input type="radio" id="female" name="gender" value="female" <?php echo ($this->tplVar['table']['rt']['user_profile']['gender']=='female') ? 'checked' : ''; ?> />女士
									</td>
								</tr>
								<tr>
									<td >手机:</td>
									<td ><input type="text" id="phone" name="phone" value="<?php echo $this->tplVar['table']['rt']['user_profile']['phone']; ?>" /></td>
								</tr>
								<tr>
									<td >地址:</td>
									<td >
										<input type="text" name="zip" size="6" value="" />
										<input type="text" name="address" size="60" value="<?php echo $this->tplVar['table']['rt']['user_profile']['address']; ?>" />
									</td>
								</tr>
								<tr>
									<td >订单备注:</td>
									<td ><textarea name="memo" id="memo"></textarea></td>
								</tr>
							</table>
						</li>
					</ul>
					<div class="button" style="text-align: right;">
						<input type="hidden" name="orderid" value="<?php echo $this->io->input["get"]["orderid"] ;?>">
						<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]["location_url"]; ?>">
						<input type="submit" value="填写完成" class="submit">
					</div>
					</form>
				</div>
			</div>
		
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
	
		<?php /*
		<main role="main" id="content">
		<div class="content">
		<section class="block-frame">
		<h2>兌換商品</h2>
		<div class="body">
			
			<div style="text-align: center; width: 774px; height: 50px; float:left; ">
				<div style="padding:12px 20px;">
				<span style="padding-left:10px;">確認兌換商品資料</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px; color:#FF00FF;">填寫收件人資料</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px;">確認收件人資料</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px;">購買訂單完成</span>
				</div>
			</div>
			
			<div style="text-align: left; width: 768px; height: 30px; border-top:1px solid #A5A5A5; border-bottom:1px solid #A5A5A5; float:left; ">
				<div style="padding:5px 40px;">購買明細</div>
			</div>
			<div style="text-align: left; width: 774px; float:left; ">
				<div style="padding:5px 30px; width: 700px; ">
				<table border="0">
					<tr style="background-color: #c9c9c9;">
						<td style="text-align: center; width: 350px;">商品名稱</td>
						<td style="text-align: center; width: 50px;">數量</td>
						<td style="text-align: center; width: 150px;">單價(紅利點數)</td>
						<td style="text-align: center; width: 150px;">兌換點數</td>
					</tr>
					<tr style="border: 1px solid #A5A5A5;">
						<td style="text-align: center; width: 350px;"><a href="#" class="name ellipsis"><?php echo $this->tplVar['table']['record'][0]['name']; ?></a></td>
						<td style="text-align: right; width: 50px; color:#FF6666;"><?php echo $this->tplVar['table']['record'][0]['num']; ?></td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?></td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_point']); ?></td>
					</tr>
				</table>
				<table border="0" style="background-color: #c9c9c9;">
					<tr>
						<td style="text-align: right; width: 550px; color:#FF6666;">本次兌換</td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_point']); ?> 點</td>
					</tr>
					<tr>
						<td style="text-align: right; width: 550px; color:#FF6666;">處理費</td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_process']); ?> 點</td>
					</tr>
					<tr>
						<td style="text-align: right; width: 550px; color:#FF6666;">總計</td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['total_fee']); ?> 點</td>
					</tr>
				</table>
				</div>
				<div class="button" style="padding:5px 80px;"><input type="button" value="< 繼續兌換" class="submit" onclick="window.location.href='<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=1'" ></div>
			</div>
				
			<div style="text-align: left; width: 768px; height: 30px; border-top:1px solid #A5A5A5; border-bottom:1px solid #A5A5A5; float:right; ">
				<div style="padding:5px 40px;">收件人寄送資料</div>
			</div>
				
			<div class="ex_product_info">
				<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/exchange_mall/confirm_insert/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $this->io->input["get"]["epcid"]; ?>&epid=<?php echo $this->io->input["get"]["epid"]; ?>" enctype="multipart/form-data" >
				<ul class="info">
					<li>
						<table border="0" style="text-align: left; width: 600px;">
							<tr>
								<td style="width: 150px;">姓名:</td>
								<td style="width: 450px;">
									<input type="text" id="name" name="name" value="<?php echo $this->tplVar['table']['rt']['user_profile']['addressee']; ?>" /> 
									<input type="radio" id="male" name="gender" value="male" <?php echo ($this->tplVar['table']['rt']['user_profile']['gender'] != 'female') ? 'checked' : ''; ?> />先生 
									<input type="radio" id="female" name="gender" value="female" <?php echo ($this->tplVar['table']['rt']['user_profile']['gender']=='female') ? 'checked' : ''; ?> />小姐
								</td>
							</tr>
							<tr>
								<td style="width: 150px;">手機:</td>
								<td style="width: 450px;"><input type="text" id="phone" name="phone" value="<?php echo $this->tplVar['table']['rt']['user_profile']['phone']; ?>" /></td>
							</tr>
							<tr>
								<td style="width: 150px;">地址:</td>
								<td style="width: 450px;">
									<input type="text" name="zip" size="6" value="" />
									<input type="text" name="address" size="60" value="<?php echo $this->tplVar['table']['rt']['user_profile']['address']; ?>" />
								</td>
							</tr>
							<tr>
								<td style="width: 150px;">訂單備註:</td>
								<td style="width: 450px;"><textarea name="memo" id="memo"></textarea></td>
							</tr>
						</table>
					</li>
				</ul>
				<div class="button" style="text-align: right;">
					<input type="hidden" name="orderid" value="<?php echo $this->io->input["get"]["orderid"] ;?>">
					<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]["location_url"]; ?>">
					<input type="submit" value="填寫完成" class="submit">
				</div>
				</form>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFCCCC; text-align: left; width: 770px; height: 28px; float:right; ">
			<span style="padding-left:20px;">注意事項</span>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFF; text-align: left; width: 770px; float:right; ">
				<div style="padding-left:8px;">
					<p>1. 本圖僅提供參考，實際情況以實體商品為準。</p>
					<p>2. 若商品因賣家、廠商或經銷商的關係有缺貨的情況，我們會發送站內訊息通知您，若您不想等待，可選擇兌換等值商品(限一樣)或等值點點幣。</p>
					<p>3. 若您對本網站規則及說明有不明白之處，請見秒殺團購教學，或聯絡客服人員。</p>
					<p>4. 本圖及說明為yahoo購物中心所提供。查看更多商品請至Yahoo購物中心。</p>
				</div>
			</div>
			
		</div>
		</section>
		</div>
		
		<div class="clear"></div>
		<div id="show_ajax" style="display:none"></div>
		</main>
		*/?>
		
	
