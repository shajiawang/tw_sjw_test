<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
<body>		
<style data-ua-style="ios">
#content_img {
 position: relative;
 width: 100%;
 height: 286px;
}
#content_img #img_name{
	position:absolute;
	width:100%; 
	bottom:0px;	
}
#content_tag {
	position: absolute;
	margin-left: 5px;
	z-index: 5000;
}
#content_tag div {
	color: #FFF;
	padding: 3px;
	margin: 3px;
	float: left;
	font-size: 8pt;
	border-radius: 3px;
}
#content_tag_bonus {
	background-color: #FF0000;
}
#content_tag_store {
	background-color: #E4A68B;
}
#content_tag_qualifications {
	background-color: #A9E969;
}
#content_tag_area {
	background-color: #258BFF;
}
#content_tag_free {
	background-color: #FF00FF;
}

#content_information {
	line-height: 25px;
	margin: 15px;
}
#content_information_tag {
	width: 90px;
	float: left;
	clear: both;
	color: #666;
}
#content_information_money {
	color: #F00;
	font-weight: bold;
}
#content_time {
	background: rgb(252,234,187); /* Old browsers */
	background: -moz-linear-gradient(top, rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(252,234,187,1)), color-stop(50%, rgba(252,205,77,1)), color-stop(51%, rgba(248,181,0,1)), color-stop(100%, rgba(251,223,147,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fceabb', endColorstr='#fbdf93', GradientType=0 ); /* IE6-9 */
	text-shadow: 1px 1px 0 #FF9;
	line-height: 35px;
	text-align: center;
	padding: 10px 10px 5px 10px;
	margin: 0px 10px 0px 10px;
	border-radius: 3px 3px 0px 0px;
	clear: both;
	font-size: 16pt;
	font-weight: bold;
}
#content_bid {
	line-height: 45px;
	text-align: center;
	padding: 5px 10px 10px 10px;
	margin: 0px 10px 15px 10px;
	border-radius: 0px 0px 3px 3px;
	background-color: #D4D4D4;
}
.content_bid_continuous {
	width: 55px;
	height: 25px;
}
.content_bid_single {
	width: 150px;
	height: 25px;
}
.content_btn_continuous {
	font-size: 16px;
	font-family: Arial;
	font-weight: normal;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	border: 1px solid #eeb44f;
	padding: 9px 12px;
	text-decoration: none;
	background: -webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
	background: -moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
	background: -ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
	background-color: #ffc477;
	color: #ffffff;
	display: inline-block;
	text-shadow: 1px 1px 0px #cc9f52;
	-webkit-box-shadow: inset 1px 1px 0px 0px #fce2c1;
	-moz-box-shadow: inset 1px 1px 0px 0px #fce2c1;
	box-shadow: inset 1px 1px 0px 0px #fce2c1;
}
.content_btn_single {
	font-size: 16px;
	font-family: Arial;
	font-weight: normal;
	-moz-border-radius: 5px;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	border: 1px solid #83c41a;
	padding: 9px 12px;
	text-decoration: none;
	background: -webkit-gradient( linear, left top, left bottom, color-stop(5%, #b8e356), color-stop(100%, #a5cc52) );
	background: -moz-linear-gradient( center top, #b8e356 5%, #a5cc52 100% );
	background: -ms-linear-gradient( top, #b8e356 5%, #a5cc52 100% );
	filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b8e356', endColorstr='#a5cc52');
	background-color: #b8e356;
	color: #ffffff;
	display: inline-block;
	text-shadow: 1px 1px 0px #86ae47;
	-webkit-box-shadow: inset 1px 1px 0px 0px #d9fbbe;
	-moz-box-shadow: inset 1px 1px 0px 0px #d9fbbe;
	box-shadow: inset 1px 1px 0px 0px #d9fbbe;
}
#content_title {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(226,226,226,1)), color-stop(50%, rgba(219,219,219,1)), color-stop(51%, rgba(209,209,209,1)), color-stop(100%, rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom, rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe', GradientType=0 ); /* IE6-9 */
	line-height: 35px;
	padding-left: 5px;
	border-bottom: 1px solid #CCC;
	font-weight: bold;
	text-align: center;
}
#content_brief {
	line-height: 25px;
	margin: 10px;
}
#content_brief img{
	width: 100%;
}
#content_brief div {
	width: 100%;
}
#content_brief span {
	width: 100%;
}
#content_brief table{
	width: 100%;
}
#content_brief tr{
	width: 100%;
}
#content_brief td{
	width: 100%;
}
#content_brief p{
	width: 100%;
}
.page-content {
	position: absolute;
	top: 46px;
	left: 0;
	right: 0;
	bottom: 0;
}

</style>
		
<style type="text/css">
.message {
	width: 300px;
	background-color: #FFFFFF;
	border: 1px solid #AAAAAA;
	border-radius: 4px;
	display: none;
}

.message header {
	padding: 4px 0;
	background-color: #CCCCCC;
	margin: 3px;
	border-radius: 4px;
	position: relative;
}

.message header h2 {
	font-weight: bold;
	font-size: 16px;
	margin: 0 12px;
	line-height: 20px;
}

.message header .button-close {
	text-decoration: none;
	color: black;
	float: right;
	margin-right: -6px;
}

.message .entry {
	padding: 3px 0;
}

.message .entry p {
	margin: 0 6px;
	padding: 10px 0;
}

.message.center {
  position: fixed;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
  -webkit-transform: translate(-50%, -50%);
  -moz-transform: translate(-50%, -50%);
}
</style>
		

<script type="text/javascript">
var re<?php echo $this->io->loc;?>;

$.fn.displayCountDown = function() {
	var server_time = window.server_time;
	//alert('2');
	if (!server_time) { //alert('2.1');
		return this;
	}

	return this.each(function(){
		var $this    = $(this),
			offtime  = parseInt($this.data('offtime')),
			resttime = offtime - server_time,
			sec   = [resttime % 60],
			min   = [Math.floor(resttime % 3600 / 60)],
			hr    = [Math.floor(resttime % 86400 / 3600)],
			day   = [Math.floor(resttime / 86400)];
//alert('3');
		/*if($("#myid<?php echo $this->io->loc;?>").length <= 0) {
		   clearInterval(re<?php echo $this->io->loc;?>);
		   console.log('1');
		}*/
		if (offtime == 0) {
			return false;
		} 
		else if (resttime <= 0) {
			//window.document.location.reload();
		}
		else {
			sec[0] < 10 && sec.unshift('0');
			min[0] < 10 && min.unshift('0');
			hr[0]  < 10 && hr.unshift('0');
			day[0] < 10 && day.unshift('0');

			$this.find('.timer-digit.sec').text(sec.join(''));
			$this.find('.timer-digit.min').text(min.join(''));
			$this.find('.timer-digit.hr').text(hr.join(''));
			$this.find('.timer-digit.day').text(day.join(''));
		}
	});
}

$.get('<?php echo $this->config->protocol; ?>://<?php echo $this->config->domain_name; ?>/<?php echo $this->config->default_main; ?>/ping/view', function(server_time){
	console && console.log(server_time);
	
	if (isNaN(server_time) || parseInt(server_time) <= 0) {
		alert('系统时间错误!');
		return;
	}

	window.server_time = parseInt(server_time);

	$('.offtime').displayCountDown();
	
	re<?php echo $this->io->loc;?> = setInterval(function(){
	  window.server_time++;
	  $('.offtime').displayCountDown(); //alert('1');
	 }, 1000);
});

$(function(){
	$('.offtime').displayCountDown();
});
</script>
<script type="text/javascript">
$(function(){
	// var article = $('.product-list .product:eq(0)');
	// for(var i=0 ; i<10 ; i++) {
	// 	$('.product-list').append(article.clone());
	// }

	var articlePerRow = 3;
	$('.product-list .product').each(function(idx, elm){
		if (idx%articlePerRow + 1 == articlePerRow) {
			$(this).addClass('product-last');
		}
	});

	$('#msg-close').on('click', function(){
		$('#msg-dialog').hide();
	});

	$('#saja-single').on('click', function(){
		$.post(
			'<?php echo $this->config->default_main; ?>/product/insert',
			{
				type: "single",
				productid: '<?php echo $this->tplVar['table']['record'][0]['productid']; ?>',
				price: $('#price').val(),
				use_sgift: $('input:hidden[name=use_sgift], input:radio:checked[name=use_sgift]').val()
			},
			function(str_json){
				if (str_json) {
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					if (json.rank) {
						$('#msg').text('本次下标顺位第' + json.rank + '名');
					} else {
						$('#msg').text(json.msg);
					}
					$('#msg-dialog').show();
					$('#winner').val(json.winner || '无');
				}
			}
		);
	});

	$('#saja-range').on('click', function(){
		$.post(
			'<?php echo $this->config->default_main; ?>/product/insert',
			{
				type: "range",
				productid: '<?php echo $this->tplVar['table']['record'][0]['productid']; ?>',
				price_start: $('#price-start').val(),
				price_stop: $('#price-stop').val(),
				use_sgift: $('input:hidden[name=use_sgift], input:radio:checked[name=use_sgift]').val()
			},
			function(str_json){
				if (str_json) {
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					$('#msg').text(json.msg);
					$('#msg-dialog').show();
					$('#winner').val(json.winner || '无');
				}
			}
		);
	});
	
	$('#content_title').on('click', function(){
		$(this).closest('#postsliding').find('#content_brief').stop().slideToggle(300);
	});
})
</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
		
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
<!--網頁標籤--><div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="page-title">商品展示</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
<!--網頁標籤-->
      
<!--內容--><div class="page-content" data-scroll="" data-zone="">
			<div class="list-wrapper">
				
				<div id="content_img"> 
					
					<?php if ($this->tplVar['table']['record'][0]['filename']) : ?>
						<img id="contentXimg" class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>" width="100%" height="286" style="position:absolute;" />
					<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
						<img id="contentXimg" class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>" width="100%" height="286" style="position:absolute;" />
					<?php else : ?>
						<img id="contentXimg" class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" width="100%" height="286" style="position:absolute;" />
					<?php endif; ?>
					
					<div id="content_tag">
					
					<?php $bonus = floatval($this->tplVar['table']['record'][0]['bonus']); ?>
						<?php if (!empty($bonus)) : ?>
						<div id="content_tag_bonus" title="红利回馈%">
						<?php if ($this->tplVar['table']['record'][0]['bonus_type'] == 'ratio') : ?>
							<?php echo (float)sprintf("%0.2f", $bonus); ?>%
						<?php elseif ($this->tplVar['table']['record'][0]['bonus_type'] == 'value') : ?>
							<?php echo (float)sprintf("%0.2f", $bonus); ?>点
						<?php else : ?>
							0
						<?php endif; ?>
						</div>
					<?php endif; ?>
						
					<?php /*$gift = floatval($this->tplVar['table']['record'][0]['gift']); ?>
						<?php if (!empty($gift)) : ?>
						<div id="content_tag_store" title="店点回馈%">
						<?php if ($this->tplVar['table']['record'][0]['gift_type'] == 'ratio') : ?>
							<?php echo (float)sprintf("%0.2f", $gift); ?>%
						<?php elseif ($this->tplVar['table']['record'][0]['gift_type'] == 'value') : ?>
							<?php echo (float)sprintf("%0.2f", $gift); ?>点
						<?php else : ?>
							0
						<?php endif; ?>
						</div>
					<?php endif; */?>
					
					<?php if (!empty($this->tplVar['table']['rt']['saja_rule']) ) {
					$saja_rule = $this->tplVar['table']['rt']['saja_rule'][0];
					if($saja_rule["srid"]=='any_saja'){
					} elseif($saja_rule["srid"]=='never_win_saja'){ ?>
						<div id="content_tag_qualifications" title="新手限定(限中标次数 5 次以下)" >限新手</div>
					<?php } elseif($saja_rule["srid"]=='le_win_saja'){ ?>
						<div id="content_tag_qualifications" title="(限中标次数 <?php echo $saja_rule["value"];?> 次以下)" >限新手</div>
					<?php } elseif($saja_rule["srid"]=='ge_win_saja'){ ?>
						<div id="content_tag_qualifications" title="(限中标次数 <?php echo $saja_rule["value"];?> 次以上)" >限老手</div>
					<?php 
					} }//endif; ?>
					
					<?php /*if ((float)$this->tplVar['table']['record'][0]['loc_type'] == 'G') : ?>
						<div id="content_tag_area" title="区域">全国</div>
					<?php endif; */?>
						
					<?php if ((float)$this->tplVar['table']['record'][0]['saja_fee'] == 0.00) : ?>
						<div id="content_tag_free" title="免费(出价免扣杀币)">免費</div>
					<?php endif; ?>
					</div>
                    
					<div id="img_name" >
                       <div class="name ellipsis" style="padding:10px; background:rgba(0,0,0,0.5); color:#FFF; font-size:14px;">
					   <?php echo $this->tplVar['table']['record'][0]['name']; ?>
					   </div>
                    </div>
                </div>
				
				<?php if ($this->tplVar['table']['record'][0]['closed'] == 'N') : ?>
				<div id="content_time">
				  <?php if ($this->tplVar['table']['record'][0]['offtime']) : ?>
				  倒数时间 <div class="offtime" data-offtime="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>">
							<span class="timer-digit day">99</span>
							<span class="timer-unit day">天</span>
							<span class="timer-digit hr">23</span>
							<span class="timer-unit hr">时</span>
							<span class="timer-digit min">03</span>
							<span class="timer-unit min">分</span>
							<span class="timer-digit sec">01</span>
							<span class="timer-unit sec">秒</span>

						</div>
				  <?php endif; //00天00时00分00秒 ?>
				</div>
				
				<div id="content_bid">
					<?php //會員得標次數 $this->tplVar['table']['rt']['user_get_bid']?>
					<?php if (!empty($this->tplVar['table']['rt']['sgift'])) : ?>
					<div class="button">
						<input type="radio" name="use_sgift" value="N" checked/>使用杀币
						<input type="radio" name="use_sgift" value="Y"/>使用下标礼券
					</div>
					<?php else : ?>
					<input type="hidden" name="use_sgift" value="N"/>
					<?php endif; ?>
						
					<div>
						<input type="text" id="price-start" placeholder=" 例:1.00" class="content_bid_continuous"/> 元～
						<input type="text" id="price-stop" placeholder=" 例:2.05" class="content_bid_continuous"/> 元 
						<input id="saja-range" class="content_btn_continuous" type="button" value="连续出价" />
					</div>
					
					<div>
						<input class="content_bid_single" type="text" id="price" placeholder=" 例:5.02" /> 元 
						<input id="saja-single" class="content_btn_single" type="button" value="单次出价" />
					</div>
					
				</div>
				<?php else : ?>
					<!--顯示結標結果資訊-->
				<?php endif; ?>
				
				
				<div id="content_information">
                    <div id="content_information_tag">市价</div>
                    <div>RMB <?php echo number_format($this->tplVar['table']['record'][0]['retail_price'], 2); ?> 元</div>
                    <div id="content_information_tag">底价</div>
                    <div>RMB <span id="content_information_money"><?php echo number_format($this->tplVar['table']['record'][0]['price_limit'], 2); ?></span> 元</div>
                    <div id="content_information_tag">下标手续费</div>
                    <div>杀价币 <?php echo number_format($this->tplVar['table']['record'][0]['saja_fee'], 2); ?> 点</div>
                    <div id="content_information_tag">中标处理费<?php /*=市價 * 处理费%*/?></div>
                    <div>杀价币 <?php echo number_format($this->tplVar['table']['record'][0]['process_fee'], 2); ?> 点</div>
                    <div id="content_information_tag">目前中标</div>
                    <div>
						<?php if (!empty($this->tplVar['table']['rt']['history'][0]['nickname'])) : ?>
							<?php echo $this->tplVar['table']['rt']['history'][0]['nickname']; ?>
						<?php else : ?>
							无
						<?php endif; ?>
					</div>
					<?php /*
					<?php if ($this->tplVar['table']['record'][0]['offtime']) : ?>
						<li>
							<span>结标时间：</span>
							<span><?php echo date("Y-m-d H:i", $this->tplVar['table']['record'][0]['offtime']); ?></span>
						</li>
					<?php elseif (!empty($this->tplVar['table']['rt']['pay_get_product'])) : ?>
						<li>
							<span>结标时间：</span>
							<span><?php echo $this->tplVar['table']['rt']['pay_get_product'][0]['insertt']; ?></span>
						</li>
					<?php elseif ($this->tplVar['table']['record'][0]['closed'] == 'NB') : ?>
						<li>
							<span>结标时间：</span>
							<span><?php echo $this->tplVar['table']['record'][0]['modifyt']; ?></span>
						</li>
					<?php endif; ?>
					<?php if (!empty($this->tplVar['table']['record'][0]['user_limit'])) : ?>
						<li>
							<span>本档一人可下标<?php echo $this->tplVar['table']['record'][0]['user_limit']; ?>次</span>
						</li>
					<?php endif; ?>
					*/?>
                </div>
				  
				<div id="postsliding">
					<div id="content_title">商品简介</div>
					<div id="content_brief" style="display:none;">
					<?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?>
					</div>
				</div>
				
					<article class="message center" id="msg-dialog">
						<header>
							<h2>
								<span class="msg" id="msg-title">讯息</span>
								<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
							</h2>
						</header>
						<div class="entry">
							<p id="msg">Message content</p>
						</div>
					</article>
            
			<?php require_once $this->tplVar['block']['footer']; ?>
			</div>
			</div>
                
	</div>
	</div>
	<div id="myid<?php echo $this->io->loc;?>"></div>
	<?php require_once $this->tplVar['block']['footerjs']; ?>
	