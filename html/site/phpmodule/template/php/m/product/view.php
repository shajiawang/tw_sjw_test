<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
<body>		
<style data-ua-style="ios">
#content_tag {
	position: absolute;
	margin-left: 5px;
	z-index: 5000;
}
#content_tag div {
	color: #FFF;
	padding: 3px;
	margin: 3px;
	float: left;
	font-size: 8pt;
	border-radius: 3px;
}
#content_tag_bonus {
	background-color: #FF0000;
}
#content_tag_store {
	background-color: #E4A68B;
}
#content_tag_qualifications {
	background-color: #A9E969;
}
#content_tag_area {
	background-color: #258BFF;
}
#content_tag_free {
	background-color: #FF00FF;
}
.page-content {
	position: absolute;
	top: 46px;
	left: 0;
	right: 0;
	bottom: 0;
}
</style>
		
<style type="text/css">
.pagination {
	position:relative;
	width:100%;
	height:auto;
	font-size: 14px;
	margin-top: 10px;
}
.pagination .total-page {
	margin-right: 10px;
}
.pagination .total-record {
	color: #6666CC;
}
.page_content {
	position:absolute;
	width:100%;
	height:auto;
	font-size: 14px;
	float: left;
	height:30px;
	line-height: 15px;
	text-align:center;
	margin-top: 10px;
}
.page_content a {
	text-decoration: none;
	color: #FF6A00;
}
.page_content .prev-page {
	margin: 0px 5px;
	width:80px;
}
.page_content .next-page {
	margin: 0px 5px;
	width:80px;
}
.page_content .change-page {
	margin: 0px 5px;
}
</style>

<script type="text/javascript">
$(function(){
	$('#change-page').on('change', function(){
		window.location.href = window.location.href.replace(/p=\d+/, 'p='+$(this).val());
	});
});

$.fn.displayCountDown = function() {
	var server_time = window.server_time;
	if (!server_time) {
		return this;
	}

	return this.each(function(){
		var $this    = $(this),
			offtime  = parseInt($this.data('offtime')),
			resttime = offtime - server_time,
			sec   = [resttime % 60],
			min   = [Math.floor(resttime % 3600 / 60)],
			hr    = [Math.floor(resttime % 86400 / 3600)],
			day   = [Math.floor(resttime / 86400)];

		if (offtime == 0) {
			return false;
		} 
		else if (resttime <= 0) {
			//window.document.location.reload();
		}
		else {
			sec[0] < 10 && sec.unshift('0');
			min[0] < 10 && min.unshift('0');
			hr[0]  < 10 && hr.unshift('0');
			day[0] < 10 && day.unshift('0');

			$this.find('.timer-digit.sec').text(sec.join(''));
			$this.find('.timer-digit.min').text(min.join(''));
			$this.find('.timer-digit.hr').text(hr.join(''));
			$this.find('.timer-digit.day').text(day.join(''));
		}
	});
}

$.get('<?php echo $this->config->protocol; ?>://<?php echo $this->config->domain_name; ?>/<?php echo $this->config->default_main; ?>/ping/view', function(server_time){
	console && console.log(server_time);
	
	if (isNaN(server_time) || parseInt(server_time) <= 0) {
		alert('系统时间错误!');
		return;
	}

	window.server_time = parseInt(server_time);

	$('.offtime').displayCountDown();

	window.setInterval(function(){
		window.server_time++;
		$('.offtime').displayCountDown();
	}, 1000);
});

$(function(){
	$('.offtime').displayCountDown();
});
</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">商品展示</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
            <div class="list-content">
					<!--<div class="good-list-item" style="cursor: pointer;">
					  <div class="img">
                        <div id="content_tag">
                          <div id="content_tag_bonus">0.5%</div>
                          <div id="content_tag_store">0.5%</div>
                          <div id="content_tag_qualifications">限新手</div>
                          <div id="content_tag_area">全国</div>
                          <div id="content_tag_free">免费</div>
                        </div>
                        <div class="img-wrapper cloud7-img-wrapper load-done" data-list-img="img 16:9" style="width: 274px; height: 154px;">
						<img src="http://howard.sajar.com.tw/site/images/site/fb_head.jpg" style="opacity: 1; width: 274px; height:154px; position: absolute; left: 0px; display: block;"> 
						</div>
                      </div>
					  
                      <div class="info">
                        <div class="name">海鲜火锅礼盒<br />
                          OO天OO时OO分OO秒</div>
                        <div class="price-banner">
                          <div class="price-value" data-type="price">搶殺去</div>
                        </div>
                      </div>
                    </div>-->
					
				<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
				<div class="good-list-item" style="cursor: pointer;">
				
					<div class="img">
                        <div id="content_tag">
						<?php $bonus = floatval($rv['bonus']); ?>
						<?php if (!empty($bonus)) : ?>
							<div id="content_tag_bonus" title="红利回馈%">
							<?php if ($rv['bonus_type'] == 'ratio') : ?>
								<?php echo (float)sprintf("%0.2f", $bonus); ?>%
							<?php elseif ($rv['bonus_type'] == 'value') : ?>
								<?php echo (float)sprintf("%0.2f", $bonus); ?>点
							<?php else : ?>
								0
							<?php endif; ?>
							</div>
						<?php endif; ?>
							
						<?php /*$gift = floatval($rv['gift']); ?>
						<?php if (!empty($gift)) : ?>
							<div id="content_tag_store" title="店点回馈%">
							<?php if ($rv['gift_type'] == 'ratio') : ?>
								<?php echo (float)sprintf("%0.2f", $gift); ?>%
							<?php elseif ($rv['gift_type'] == 'value') : ?>
								<?php echo (float)sprintf("%0.2f", $gift); ?>点
							<?php else : ?>
								0
							<?php endif; ?>
							</div>
						<?php endif; */?>
							
						<?php if (!empty($rv['srid']) ) {
						if($rv['srid']=='any_saja'){
						} elseif($rv['srid']=='never_win_saja'){ ?>
							<div id="content_tag_qualifications" title="新手限定(限中标次数 5 次以下)" >限新手</div>
						<?php } elseif($rv['srid']=='le_win_saja'){ ?>
							<div id="content_tag_qualifications" title="(限中标次数 <?php echo $rv['value'];?> 次以下)" >限新手</div>
						<?php } elseif($rv['srid']=='ge_win_saja'){ ?>
							<div id="content_tag_qualifications" title="(限中标次数 <?php echo $rv['value'];?> 次以上)" >限老手</div>
						<?php 
						} }//endif; ?>
							
						<?php /*if ($rv['loc_type'] == 'G') : ?>
							<div id="content_tag_area" title="区域">全国</div>
						<?php endif; */?>
							
						<?php if ((float)$rv['saja_fee'] == 0.00) : ?>
							<div id="content_tag_free" title="免费(出价免扣杀币)">免费</div>
						<?php endif; ?>
						</div>
						
                        <div class="img-wrapper cloud7-img-wrapper load-done" data-list-img="img 16:9" style="width: 274px; height: 154px;">
							<?php if (!empty($rv['thumbnail'])) : ?>
							<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['thumbnail']; ?>" style="opacity: 1; width: 274px; height:154px; position: absolute; left: 0px; display: block;" />
							<?php elseif (!empty($rv['thumbnail_url'])) : ?>
							<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>" style="opacity: 1; width: 274px; height:154px; position: absolute; left: 0px; display: block;" />
							<?php else : ?>
							<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" style="opacity: 1; width: 274px; height:154px; position: absolute; left: 0px; display: block;" />
							<?php endif; ?>
						</div>
                    </div>
					
					<div class="info">
                        <div class="name">
							<p class="ellipsis" style="width:100%; height:15px; overflow:hidden;"><?php echo $rv['name']; ?></p>
						
							<?php if (!empty($rv['offtime'])) : ?>
							<div class="offtime" data-offtime="<?php echo $rv['offtime']; ?>">
								<span class="timer-digit day">99</span>
								<span class="timer-unit day">天</span>
								<span class="timer-digit hr">23</span>
								<span class="timer-unit hr">时</span>
								<span class="timer-digit min">01</span>
								<span class="timer-unit min">分</span>
								<span class="timer-digit sec">01</span>
								<span class="timer-unit sec">秒</span>
							</div><?php endif; //OO天OO时OO分OO秒?>
							<?php //原价：echo number_format($rv['retail_price'], 2); 元 ?>
						</div>
                        <div class="price-banner">
                          <div class="price-value" data-type="price">搶殺去</div>
                        </div>
                    </div>
				</div>
				</a>
				<?php endforeach; ?>
            </div>
			
			<div class="pagination">
				<?php if ($this->tplVar['table']['record']) : ?>
				<div class="page_content">
					<a class="prev-page" href="<?php echo $this->config->default_main.'/'. $this->io->input['get']['fun'].'/'.$this->io->input['get']['act'].'?p='.$this->tplVar['page']['previouspage']; ?>">上一页</a>
					&nbsp;/&nbsp;
					<a class="next-page" href="<?php echo $this->config->default_main.'/'. $this->io->input['get']['fun'].'/'.$this->io->input['get']['act'].'?p='.$this->tplVar['page']['nextpage']; ?>">下一页</a>
					至第
					<select id="change-page">
					<?php foreach($this->tplVar['page']['item'] as $pk => $pv) : ?>
						<option value="<?php echo $pv['p']; ?>" <?php echo ($pv['p']==$this->io->input["get"]["p"])? 'selected' : ''; ?> ><?php echo $pv['p']; ?></option>
					<?php endforeach; ?>
					</select>
					页
				</div>
				<?php endif; ?>
			</div>
			
			<div class="button-load-more">
			<span class="loading-image"></span>
			<span class="caption">&nbsp;&nbsp;<?php //require_once $this->tplVar['block']['footer']; ?></span> 
			</div>
			
        </div>
		</div>
	
	</div>
	</div>
	
<?php require_once $this->tplVar['block']['footerjs']; ?>
	
		<?php /*
		<main role="main" id="content">
			
			<div class="product-list">
			<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<article class="product">
					<div class="product-category-rt">
						<div class="product-category bonus">
							<span class="product-category-name">
								<?php if ($rv['bonus_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['bonus']); ?>%
								<?php elseif ($rv['bonus_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['bonus']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
						<div class="product-category store">
							<span class="product-category-name">
								<?php if ($rv['gift_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['gift']); ?>%
								<?php elseif ($rv['gift_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['gift']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
						
						<div class="product-category pcid-1"><span class="product-category-name">限新手</span></div>
						
					<?php if ($rv['loc_type'] == 'G') : ?>
						<div class="product-category pcid-2"><span class="product-category-name">全国</span></div>
					<?php endif; ?>
					
					<?php if ((float)$rv['saja_fee'] == 0.00) : ?>
						<div class="product-category pcid-3"><span class="product-category-name">免费</span></div>
					<?php endif; ?>
					</div>
					
					<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
				<?php if (!empty($rv['thumbnail'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['thumbnail']; ?>"/>
				<?php elseif (!empty($rv['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
				<?php endif; ?>
					</a>
					<header>
						<h3>
							<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>" class="name ellipsis">
								<?php echo $rv['name']; ?>
							</a>
						</h3>
					</header>
					<div class="offtime" data-offtime="<?php echo $rv['offtime']; ?>">
						<span class="timer-digit day">99</span>
						<span class="timer-unit day">天</span>
						<span class="timer-digit hr">23</span>
						<span class="timer-unit hr">时</span>
						<span class="timer-digit min">03</span>
						<span class="timer-unit min">分</span>
						<span class="timer-digit sec">01</span>
						<span class="timer-unit sec">秒</span>
					</div>
					<div class="saja-button">
						<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
							<img src="<?php echo $this->config->path_image; ?>/b_bid.png">
						</a>
					</div>
					<table class="info">
						<tbody>
							<td class="label">价值：</td>
							<td class="value retail_price"><?php echo number_format($rv['retail_price'], 2); ?></td>
							<td class="unit">元</td>
							<td class="label">关注：</td>
							<td class="value attention">987,654,321</td>
							<td class="unit">次</td>
						</tbody>
					</table>
				</article>

			<?php endforeach; ?>
			</div>
			
		</main>

		*/?>
	
