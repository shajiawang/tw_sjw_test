<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
<body>		
<style data-ua-style="ios">
#content {
	height: auto;
	width: 100%;
	position: relative;
	z-index: 0;
}
#content a {
	color: #000;
	display: block;
	text-decoration: none;
}
#content_list {
	position: relative;
	padding: 10px 8px;
	height: 105px;
	border-bottom: 1px solid #DDDDDD;
}
#content_img_a {
	overflow: hidden;
	position: relative;
	border: 3px solid #FFFFFF;
	box-shadow: 0 0 3px #999999;
	float: left;
	width: 99px;
}
#content_img_a img {
	height: 99px;
}
#content_text {
	/*position: absolute; top: 14px; right: 5px; line-height: 30px;*/
	float:right; width:62%; padding: 6px 1px;
	text-align: right;
	color: #000;
	font-size: 14px;
}
#content_text .name {
	height: 30px;
	overflow: hidden;
	line-height: 17px;
	/* font-size: 15px; vertical-align: middle; display: table-cell; */
}
.content_information_money {
	color: #F00;
	font-weight: bold;
}

.page-content {
	position: absolute;
	top: 46px;
	left: 0;
	right: 0;
	bottom: 0;
}
</style>

<style type="text/css">
.pagination {
	position:relative;
	width:100%;
	height:auto;
	font-size: 14px;
	margin-top: 10px;
}
.pagination .total-page {
	margin-right: 10px;
}
.pagination .total-record {
	color: #6666CC;
}
.page_content {
	position:absolute;
	width:100%;
	height:auto;
	font-size: 14px;
	float: left;
	height:30px;
	line-height: 15px;
	text-align:center;
	margin-top: 10px;
}
.page_content a {
	text-decoration: none;
	color: #FF6A00;
}
.page_content .prev-page {
	margin: 0px 5px;
	width:80px;
}
.page_content .next-page {
	margin: 0px 5px;
	width:80px;
}
.page_content .change-page {
	margin: 0px 5px;
}
</style>

<script type="text/javascript">
$(function(){
	$('#change-page').on('change', function(){
		window.location.href = window.location.href.replace(/p=\d+/, 'p='+$(this).val());
	});
});
</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="page-title">最新成交</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
		<div class="page-content" data-scroll="" data-zone="">
		<div class="list-wrapper">
			
			<div id="content">
				<!--<a href="">
                    <div id="content_list">
                      <div id="content_img_a"><img src="http://img.saja.com.tw/main/iphone5s.jpg" /></div>
                      <div id="content_text">iPhone 5S 土豪金<br />
                        中标者：李奥纳多皮卡丘<br />
                        成交金额：<b>RMB 9.00元</b></div>
                    </div>
                    </a>-->
				<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<a href="<?php echo $this->config->default_main.'/product/history_detail?productid='.$rv['productid']; ?>">
					<div id="content_list">
						<div id="content_img_a">
						<?php if (!empty($rv['filename'])) : ?>
							<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['filename']; ?>"/>
						<?php elseif (!empty($rv['thumbnail_url'])) : ?>
							<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>"/>
						<?php else : ?>
							<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
						<?php endif; ?>
						</div>
						
						<div id="content_text">
							<p class="name ellipsis" ><?php echo $rv['name']; ?></p>
							<br />中标者：<?php echo $rv['nickname']; ?>
							<br />成交金额：<b>RMB <?php echo sprintf("%0.2f", $rv['price']); ?>元</b>
						</div>
					</div>			
				</a>
				<?php endforeach; ?>
			</div>
			
			<div class="pagination">
				<?php if ($this->tplVar['table']['record']) : ?>
				<div class="page_content">
					<a class="prev-page" href="<?php echo $this->config->default_main.'/'. $this->io->input['get']['fun'].'/'.$this->io->input['get']['act'].'?p='.$this->tplVar['page']['previouspage']; ?>">上一页</a>
					&nbsp;/&nbsp;
					<a class="next-page" href="<?php echo $this->config->default_main.'/'. $this->io->input['get']['fun'].'/'.$this->io->input['get']['act'].'?p='.$this->tplVar['page']['nextpage']; ?>">下一页</a>
					至第
					<select id="change-page">
					<?php foreach($this->tplVar['page']['item'] as $pk => $pv) : ?>
						<option value="<?php echo $pv['p']; ?>" <?php echo ($pv['p']==$this->io->input["get"]["p"])? 'selected' : ''; ?> ><?php echo $pv['p']; ?></option>
					<?php endforeach; ?>
					</select>
					页
				</div>
				<?php endif; ?>
			</div>
			
			<div class="button-load-more">
			<span class="loading-image"></span>
			<span class="caption">&nbsp;&nbsp;<?php //require_once $this->tplVar['block']['footer']; ?></span> 
			</div>
        
		</div>
		</div>
        
    </div>
	</div>
	
	<?php require_once $this->tplVar['block']['footerjs']; ?>
