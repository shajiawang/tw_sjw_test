<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
<style type="text/css">
#content {
	height:auto;
	width:100%;
	position:relative;
	z-index:0;
}
#content_scroll {
	margin-top:45px;
}
#content_name {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	padding:12px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_img {
	text-align:center;
	padding-top:10px;
}

#content_img img{
	width:95%;
}
#content_information {
	line-height:25px;
	margin:15px;
}
#content_information_tag {
	width:80px;
	float:left;
	clear:both;
	color:#666;
}
#content_information_money {
	color:#F00;
	font-weight:bold;
}
#content_title {
	background: rgb(226,226,226); /* Old browsers */
	background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
	background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
	background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
	background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
	background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
	background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
	filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
	line-height:35px;
	padding-left:5px;
	border-bottom: 1px solid #CCC;
	font-weight:bold;
	text-align:center;
}
#content_brief {
	line-height:25px;
	margin:10px;
}
#content_brief img{
	width: 100%;
}
#content_brief div {
	width: 100%;
}
#content_brief span {
	width: 100%;
}
#content_brief table{
	width: 100%;
}
#content_brief tr{
	width: 100%;
}
#content_brief td{
	width: 100%;
}
#content_brief p{
	width: 100%;
}
</style>
<script type="text/javascript">
$(function(){
	$('#content_title').on('click', function(){
		$(this).closest('#postsliding').find('#content_brief').stop().slideToggle(300);
	});
})
</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="page-title">最新成交</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
		<div class="page-content" data-scroll="" data-zone="">
		<div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
		
			<div id="content_name"><?php echo $this->tplVar['table']['record'][0]['name']; ?></div>
            <div id="content_img">
				<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
					<img src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
				<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
					<img src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img src="http://img.saja.com.tw/main/iphone5s.jpg" />
				<?php endif; ?>
			</div>
            <div id="content_information">
                <div id="content_information_tag">中标时间</div><div>
					<?php if ($this->tplVar['table']['record'][0]['offtime']) : ?>
						<?php echo date("Y-m-d H:i", $this->tplVar['table']['record'][0]['offtime']); ?>
					<?php elseif (!empty($this->tplVar['table']['rt']['pay_get_product'])) : ?>
						<?php echo $this->tplVar['table']['rt']['pay_get_product'][0]['insertt']; ?>
					<?php elseif ($this->tplVar['table']['record'][0]['closed'] == 'NB') : ?>
						<?php echo $this->tplVar['table']['record'][0]['modifyt']; ?>
					<?php endif; ?></div>
                <div id="content_information_tag">市售价格</div><div>RMB <?php echo number_format($this->tplVar['table']['record'][0]['retail_price'], 2); ?> 元</div>
                <div id="content_information_tag">中标杀友</div><div>
					<?php if (!empty($this->tplVar['table']['record'][0]['nickname'])) : ?>
						<?php echo $this->tplVar['table']['record'][0]['nickname']; ?>
					<?php else : ?>
						无
					<?php endif; ?></div>
                <div id="content_information_tag">中标金额</div><div>RMB <span id="content_information_money"><?php echo number_format($this->tplVar['table']['record'][0]['price'], 2); ?></span> 元</div>
            </div>
			
            <div id="postsliding">
				<div id="content_title">商品简介</div>
				<div id="content_brief" style="display:none;">
				<?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?>
				</div>
			</div>
		
		<?php require_once $this->tplVar['block']['footer']; ?>
		</div></div>
        
		</div>
		</div>
        
    </div>
	</div>
	
	<?php require_once $this->tplVar['block']['footerjs']; ?>
	
		<?php /*
		<main role="main" id="content">
			<div class="product-info">
				<article class="saja-product">
					<header>
						<h1>
							商品名称：<?php echo $this->tplVar['table']['record'][0]['name']; ?>
						</h1>
					</header>
					
					<div style="position:absolute; width:392px; height:209px; ">
					<div style="position:absolute; left:1px; top:1px; z-index:2;">
					<img src="http://img.saja.com.tw/tp/div_templates_close_information_bidon.gif" />
					</div>
					
					<div class="image" style="width:392px; height:209px; overflow: hidden; ">
					<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
						<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
					<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
						<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
					<?php else : ?>
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
					<?php endif; ?>
					</div></div>
					
					<div class="clear"></div>
				</article>
				    
				<div class="saja-info">
					<ul class="info">
						<li>
							<span>商品编号：</span>
							<span><?php echo $this->tplVar['table']['record'][0]['productid']; ?></span>
						</li>
						<li>
							<span>中标杀友：</span>
							<span id="winner" style="color:#ff0000;">
							<?php if (!empty($this->tplVar['table']['record'][0]['nickname'])) : ?>
								<?php echo $this->tplVar['table']['record'][0]['nickname']; ?>
							<?php else : ?>
								无
							<?php endif; ?>
							</span>
						</li>
						
						<li>
							<span>市售价格：</span>
							<span>RMB <?php echo number_format($this->tplVar['table']['record'][0]['retail_price'], 2); ?>元</span>
						</li>
						<li>
							<span style="color:#ff0000;">中标金额：</span>
							<span style="color:#ff0000;">RMB <?php echo number_format($this->tplVar['table']['record'][0]['price'], 2); ?>元</span>
						</li>
						
					<?php if ($this->tplVar['table']['record'][0]['offtime']) : ?>
						<li>
							<span>中标时间：</span>
							<span><?php echo date("Y-m-d H:i", $this->tplVar['table']['record'][0]['offtime']); ?></span>
						</li>
					<?php elseif (!empty($this->tplVar['table']['rt']['pay_get_product'])) : ?>
						<li>
							<span>中标时间：</span>
							<span><?php echo $this->tplVar['table']['rt']['pay_get_product'][0]['insertt']; ?></span>
						</li>
					<?php elseif ($this->tplVar['table']['record'][0]['closed'] == 'NB') : ?>
						<li>
							<span>中标时间：</span>
							<span><?php echo $this->tplVar['table']['record'][0]['modifyt']; ?></span>
						</li>
					<?php endif; ?>
					</ul>
				</div>
				
				<div style="margin: 30px 30px 10px; background-color: #FFF; text-align:left; width:100%px; float:left; ">
					<div style="padding-left:8px;"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></div>
				</div>
				<div style="margin: 20px 30px 25px; background-color: #FFF; text-align:left; width:100%px; float:left; ">
					<div style="padding-left:8px;">
						<p>1. 本图仅提供参考，实际情况以实体商品为准。</p>
						<p>2. 若商品因卖家、厂商或经销商的关系有缺货的情况，我们会发送站内讯息通知您，若您不想等待，可选择兑换等值商品(限一样)或等值点点币。</p>
						<p>3. 若您对本网站规则及说明有不明白之处，请见秒杀团购教学，或联络客服人员。</p>
						<p>4. 本图及说明为yahoo购物中心所提供。查看更多商品请至Yahoo购物中心。</p>
						<p>5.本商品寄出不含赠品。</p>
					</div>
				</div>
				
				<div class="clear"></div>
			
			</div>
		</main>*/?>
	
