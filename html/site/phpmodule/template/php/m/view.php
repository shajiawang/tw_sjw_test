<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
<body>

<style data-ua-style="ios">
body, ul, li {
	margin: 0;
}
body {
	-webkit-user-select: none;
	-webkit-text-size-adjust: none;
}
#wrapper {
	float: left;
	position: relative;	/* On older OS versions "position" and "z-index" must be defined, */
	z-index: 1;			/* it seems that recent webkit is less picky and works anyway. */
	overflow: hidden;
	background: #aaa;
	background: #e3e3e3;
}
#scroller {
	height: 100%;
	float: left;
	padding: 0;
}
#scroller ul {
	list-style: none;
	display: block;
	float: left;
	width: 100%;
	height: 100%;
	padding: 0;
	margin: 0;
	text-align: left;
}
#scroller li {
	-webkit-box-sizing: border-box;
	-moz-box-sizing: border-box;
	-o-box-sizing: border-box;
	box-sizing: border-box;
	display: block;
	float: left;
	text-align: center;
	font-family: georgia;
	font-size: 18px;
	line-height: 140%;
}

.page-content {
	position: absolute;
	top: 46px;
	left: 0;
	right: 0;
	bottom: 0;
}
</style>

<script type="text/javascript">
var myScroll;
function loaded() {
	myScroll = new iScroll('wrapper', {
		snap: true,
		momentum: false,
		hScrollbar: false,
		onScrollEnd: function () {
		}
	});
}
</script>
<?php require_once $this->tplVar['block']['header']; ?> 
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0px, 0px, 0); left: 0px; right: 0px;"> 
    <div class="main-page" id="main-page" data-scroll="" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0px, 0px, 0); z-index: 1; visibility: visible;">
      
<?php //多頁滑動式廣告banner ?>
	  <div class="banner" style="overflow: hidden; -webkit-transform: translate3d(0px, 0px, 0);">
		<div id="wrapper" style="overflow: hidden;">
          <div id="scroller" style="-webkit-transition: -webkit-transform 0ms; transition: -webkit-transform 0ms; -webkit-transform-origin: 0px 0px; -webkit-transform: translate(0px, 0px) translateZ(0px);">
            <ul id="thelist">
              <li><img src="http://howard.sajar.com.tw/site/images/site/fb_head.jpg" class="item_img"></li>
              <li><img src="http://howard.sajar.com.tw/site/images/site/fb_head.jpg" class="item_img"></li>
              <li><img src="http://howard.sajar.com.tw/site/images/site/fb_head.jpg" class="item_img"></li>
            </ul>
          </div>
        </div>
        <div class="red-point">
          <div class="wrapper"> <span class=""></span><span class="active"></span></div>
        </div>
      </div>
	  
<?php //列表區塊 ?>
	  <div class="column">
        <a href="<?php echo $this->config->default_main ?>/product/?channelid=<?php echo $this->io->input['get']['channelid']; ?>">
		<div class="navigate-column" style="cursor: pointer;">
          <div class="wrapper red">
			<div class="title">杀价列表</div>
            <div class="sub-title">手机 数码 票券</div>
          </div>
        </div></a>
		
        <a href="<?php echo $this->config->default_main; ?>/exchange_mall/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=1">
		<div class="navigate-column" style="cursor: pointer;">
          <div class="wrapper yellow">
			<div class="title">兑换商城</div>
            <div class="sub-title">100%折抵</div>
          </div>
        </div></a>
		
        <a href="<?php echo $this->config->default_main; ?>/product/history/?channelid=<?php echo $this->tplVar['status']['get']['channelid']; ?>">
		<div class="navigate-column" style="cursor: pointer;">
          <div class="wrapper cyan">
			<div class="title">最新成交</div>
            <div class="sub-title">平均6元成交</div>
          </div>
        </div></a>
		
        <a href="<?php echo $this->config->default_main; ?>/deposit">
		<div class="navigate-column" style="cursor: pointer;">
          <div class="wrapper blue">
			<div class="title">杀价币充值</div>
            <div class="sub-title">充值爽杀价</div>
          </div>
        </div></a>
		
        <a href="<?php echo $this->config->default_main .'/faq'; ?>">
		<div class="navigate-column" style="cursor: pointer;">
          <div class="wrapper red">
			<div class="title">新手教学</div>
            <div class="sub-title">如何杀价</div>
          </div>
        </div></a>
		
		<?php if (empty($this->io->input['session']['user'])) : ?>
		<a href="<?php echo $this->config->default_main; ?>/user/login">
		<?php else : ?>
		<a href="<?php echo $this->config->default_main; ?>/member/?<?php echo $this->tplVar['status']['args']; ?>">	
		<?php endif; ?>
        <div class="navigate-column" style="cursor: pointer;">
          <div class="wrapper yellow">
			<div class="title">会员中心</div>
			<div class="sub-title">注册/登录</div>
          </div>
        </div></a>
      </div>
      
	  <?php require_once $this->tplVar['block']['footer']; ?>
	</div>
	</div>

<?php require_once $this->tplVar['block']['footerjs']; ?>	
<script>
var nWidth=document.body.clientWidth;
var nHeight=nWidth/320*180;
var divs = document.getElementsByClassName('item_img');
for(var i=0; i<divs.length; i++) { 
	divs[i].style.width=nWidth+"px";
	divs[i].style.height=nHeight+"px";
}
document.getElementById('wrapper').style.width=nWidth+"px";
document.getElementById('wrapper').style.height=nHeight+"px";
document.getElementById('scroller').style.width=(nWidth*divs.length)+"px";

function WinOnResize() {
var nWidth=document.body.clientWidth;
var nHeight=nWidth/320*180;
var divs = document.getElementsByClassName('item_img');
for(var i=0; i<divs.length; i++) { 
	divs[i].style.width=nWidth+"px";
	divs[i].style.height=nHeight+"px";
}
document.getElementById('wrapper').style.width=nWidth+"px";
document.getElementById('wrapper').style.height=nHeight+"px";
document.getElementById('scroller').style.width=(nWidth*divs.length)+"px";
}
window.onresize=WinOnResize
</script>
