<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content_name {
			background: rgb(226,226,226); /* Old browsers */
			background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
			background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
			padding:12px;
			border-bottom: 1px solid #CCC;
			font-weight:bold;
			text-align:center;
		}
		#content_img {
			text-align:center;
			padding-top:10px;
		}
		#content_img img{
			width:95%;
		}
		#content_information {
			line-height:25px;
			margin:15px;
		}
		#content_information_tag {
			width:80px;
			float:left;
			clear:both;
			color:#666;
		}
		#content_information_money {
			color:#F00;
			font-weight:bold;
		}
		#content_time {
			background: rgb(252,234,187); /* Old browsers */
			background: -moz-linear-gradient(top,  rgba(252,234,187,1) 0%, rgba(252,205,77,1) 50%, rgba(248,181,0,1) 51%, rgba(251,223,147,1) 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(252,234,187,1)), color-stop(50%,rgba(252,205,77,1)), color-stop(51%,rgba(248,181,0,1)), color-stop(100%,rgba(251,223,147,1))); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* IE10+ */
			background: linear-gradient(to bottom,  rgba(252,234,187,1) 0%,rgba(252,205,77,1) 50%,rgba(248,181,0,1) 51%,rgba(251,223,147,1) 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fceabb', endColorstr='#fbdf93',GradientType=0 ); /* IE6-9 */
			text-shadow: 1px 1px 0 #FF9;
			line-height:35px;
			text-align:center;
			padding:10px 10px 5px 10px;
			margin:0px 10px 0px 10px;
			border-radius:3px 3px 0px 0px;
			clear:both;
			font-size:16pt;
			font-weight:bold;
		}
		#content_bid {
			line-height:45px;
			text-align:center;
			padding:5px 10px 10px 10px;
			margin:0px 10px 15px 10px;
			border-radius:0px 0px 3px 3px;
			background-color:#D4D4D4;
		}
		#content_bid_continuous {
			width:60px;
			height:25px;
		}
		#content_bid_single {
			width:153px;
			height:25px;
		}
		#content_btn_continuous {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
		}
		#content_btn_single {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #83c41a;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #b8e356), color-stop(100%, #a5cc52) );
			background:-moz-linear-gradient( center top, #b8e356 5%, #a5cc52 100% );
			background:-ms-linear-gradient( top, #b8e356 5%, #a5cc52 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b8e356', endColorstr='#a5cc52');
			background-color:#b8e356;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #86ae47;
			-webkit-box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			-moz-box-shadow:inset 1px 1px 0px 0px #d9fbbe;
			box-shadow:inset 1px 1px 0px 0px #d9fbbe;
		}
		#content_title {
			background: rgb(226,226,226); /* Old browsers */
			background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
			background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
			background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
			background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
			background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
			line-height:35px;
			padding-left:5px;
			border-bottom: 1px solid #CCC;
			font-weight:bold;
			text-align:center;
		}
		#content_brief {
			line-height:25px;
			margin:10px;
		}
		</style>
		
		<style type="text/css">
		.message {
			width: 300px;
			background-color: #FFFFFF;
			border: 1px solid #AAAAAA;
			border-radius: 4px;
			display: none;
		}

		.message header {
			padding: 4px 0;
			background-color: #CCCCCC;
			margin: 3px;
			border-radius: 4px;
			position: relative;
		}

		.message header h2 {
			font-weight: bold;
			font-size: 16px;
			margin: 0 12px;
			line-height: 20px;
		}

		.message header .button-close {
			text-decoration: none;
			color: black;
			float: right;
			margin-right: -6px;
		}

		.message .entry {
			padding: 3px 0;
		}

		.message .entry p {
			margin: 0 6px;
			padding: 10px 0;
		}

		.message.center {
		  position: fixed;
		  left: 50%;
		  top: 50%;
		  transform: translate(-50%, -50%);
		  -webkit-transform: translate(-50%, -50%);
		  -moz-transform: translate(-50%, -50%);
		}
		</style>
		<style type="text/css">
		.detail,
		.receiver {
			width: 700px;
			margin: 10px auto;
		}
		.detail thead th.text,
		.detail tbody td.text {
			text-align: center;
		}
		.detail thead th.number,
		.detail tbody td.number,
		.detail tfoot th,
		.detail tfoot td {
			text-align: right;
		}
		.detail thead tr,
		.detail tfoot tr {
			background-color: #C9C9C9;
		}
		.detail tbody tr {
			border: 1px solid #A5A5A5;
		}
		.detail tfoot th {
			color: #FF6666;
		}
		.detail .name {
			width: 60%;
		}
		.detail .amount,
		.detail .price {
			width: 20%;
		}
		.receiver th {
			width: 30%;
			color: #6D6D6D;
			text-align: left;
			padding=left: 40px;
		}
		.receiver td {
			width: 70%;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">会员中心</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
    
			<?php /*
			<div id="content_name"><?php echo $this->tplVar['table']['record'][0]['name']; ?></div>
			<div id="content_img">
				<?php if ($this->tplVar['table']['record'][0]['filename']) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
				<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
				<?php endif; ?>
			</div>
			
			<div id="content_information">
				<div id="content_information_tag">市价</div><div>RMB <?php echo number_format($this->tplVar['table']['record'][0]['retail_price'], 2); ?> 元</div>
				<div id="content_information_tag">底价</div><div>RMB <span id="content_information_money"><?php echo number_format($this->tplVar['table']['record'][0]['price_limit'], 2); ?></span> 元</div>
				<div id="content_information_tag">下标手续费</div><div>杀价币 <?php echo number_format($this->tplVar['table']['record'][0]['saja_fee'], 2); ?> 点</div>
				<div id="content_information_tag">目前中标</div>
				<div><?php if (!empty($this->tplVar['table']['rt']['history'][0]['nickname'])) : ?>
					<?php echo $this->tplVar['table']['rt']['history'][0]['nickname']; ?>
					<?php else : ?>
					无
					<?php endif; ?>
				</div>
			</div>
			*/?>
			
			<div id="content_bid">
				<section class="block-frame">
					<h2>商品结账</h2>
					<div class="body">
						<h3>购买明细</h3>
						<table class="detail">
							<thead>
								<tr>
									<th class="name">商品名称</th>
									<th class="amount number">数量</th>
									<th class="price number">得标价(杀币)</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
								<tr>
									<td class="text"><?php echo $rv['name']; ?></td>
									<td class="number">1</td>
									<td class="number"><?php echo round($rv['price'], 2); ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<th>处理费</th>
									<td class="number"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['real_process_fee']); ?>杀币</td>
								</tr>
								<tr>
									<td></td>
									<th>总计</th>
									<td class="number"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['total']); ?>杀币</td>
								</tr>
							</tfoot>
						</table>
						
						<h3>收件人寄送资料</h3>
						
						<form method="post" action="<?php echo $this->config->default_main.'/pay_get_product/insert'; ?>">
							<table class="receiver">
								<tbody>
									<tr>
										<th>姓名：</th>
										<td>
											<input type="text" name="name"/>
											先生
											<input type="radio" name="gender" value="male">
											小姐
											<input type="radio" name="gender" value="female">
										</td>
									</tr>
									<tr>
										<th>手机：</th>
										<td>
											<input type="text" name="phone"/>
										</td>
									</tr>
									<tr>
										<th>地址：</th>
										<td>
											<input type="text" name="zip" size="6"/>
											<input type="text" name="address" size="60"/>
										</td>
									</tr>
									<tr>
										<th>订单备注：</th>
										<td>
											<input type="text" name="memo"/>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">
											<input type="hidden" name="pgpid" value="<?php echo $this->tplVar['table']['record'][0]['pgpid']; ?>">
											<input type="submit" value="确认结账">
										</td>
									</tr>
								</tfoot>
							</table>
						</form>
					</div>
				</section>
	
			</div>
		
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		
	<?php /* <main role="main" id="content">
			<div class="content">
				<section class="block-frame">
					<h2>商品結帳</h2>
					<div class="body">
						<h3>購買明細</h3>
						<table class="detail">
							<thead>
								<tr>
									<th class="name">商品名稱</th>
									<th class="amount number">數量</th>
									<th class="price number">得標價(殺幣)</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
								<tr>
									<td class="text"><?php echo $rv['name']; ?></td>
									<td class="number">1</td>
									<td class="number"><?php echo round($rv['price'], 2); ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<th>處理費</th>
									<td class="number"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['real_process_fee']); ?>殺幣</td>
								</tr>
								<tr>
									<td></td>
									<th>總計</th>
									<td class="number"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['total']); ?>殺幣</td>
								</tr>
							</tfoot>
						</table>
						<h3>收件人寄送資料</h3>
						<form method="post" action="<?php echo $this->config->default_main.'/pay_get_product/insert'; ?>">
							<table class="receiver">
								<tbody>
									<tr>
										<th>姓名：</th>
										<td>
											<input type="text" name="name"/>
											先生
											<input type="radio" name="gender" value="male">
											小姐
											<input type="radio" name="gender" value="female">
										</td>
									</tr>
									<tr>
										<th>手機：</th>
										<td>
											<input type="text" name="phone"/>
										</td>
									</tr>
									<tr>
										<th>地址：</th>
										<td>
											<input type="text" name="zip" size="6"/>
											<input type="text" name="address" size="60"/>
										</td>
									</tr>
									<tr>
										<th>訂單備註：</th>
										<td>
											<input type="text" name="memo"/>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">
											<input type="hidden" name="pgpid" value="<?php echo $this->tplVar['table']['record'][0]['pgpid']; ?>">
											<input type="submit" value="確認結帳">
										</td>
									</tr>
								</tfoot>
							</table>
						</form>
					</div>
				</section>
			</div>
			<div class="clear"></div>
		</main> */?>
	
