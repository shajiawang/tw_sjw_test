<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
		<style type="text/css">
		#content {
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #324FE1;
			text-decoration: none;
		}
		.content_data_table {
			margin:0 auto;
			display: table;
		}
		.content_data_title {
			background-color:#ECECEC;
		}
		.content_data_captionA{
			padding:8px 8px 8px 11px;
			display: table-caption;
			border:1px solid #FFF;
		}
		.content_data_captionB{
			padding:8px 8px 0px 11px;
			display: table-caption;
			border:1px solid #FFF;
		}
		.content_data_tr {
			line-height:25px;
			display: table-row;
		}
		.content_data_tr div {
			display: table-cell;
			border:1px solid #FFF;
			padding:5px;
			text-align:center;
			border-bottom:1px solid #ECECEC;
		}
		.content_data_td1 {
			width:140px;
		}
		.content_data_td2 {
			width:60px;
		}
		.content_data_td3 {
			width:60px;
		}
		</style>
		<script type="text/javascript">
		$(function(){
			$('.type', '#history-type').on('click', function(){
				$(this)
				.addClass('active')
				.parent().siblings().find('.type.active').removeClass('active');

				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			$( document ).on('click', '#history-list .detail', function(){
				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			//重新整理頁面時，直接觸發click event
			if (window.location.hash) {
				$('#history-type .type[href="'+window.location.hash+'"]').trigger('click');
			} else {
				$('#history-type .type:eq(0)').trigger('click');
			}

			$('#history-type').on('mousemove', function(e){
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				if ($(this).is('a')) {
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				}
			});
		});
		</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">订单记录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
    
            <div class="content_data_table">
                <?php /*<div class="content_data_captionA content_data_title">商品名称</div>*/?>
                <div class="content_data_tr content_data_title">
                   <div class="content_data_td1">订单时间</div>
				   <div class="content_data_td2">使用点数</div>
				   <div class="content_data_td3">出货状态</div>
                </div>
            </div>
            
            <?php if (!empty($this->tplVar['table']['record'])) : ?>
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
			<div class="content_data_table">
                <div class="content_data_captionB">
					<a href="<?php echo $rv['href']; ?>" ><span><?php echo $rv['name']; ?></span></a>
				</div>
                <div class="content_data_tr">
                    <div class="content_data_td1"><?php echo $rv['insertt']; ?></div>
					<div class="content_data_td2"><?php echo sprintf('%0.2f', $rv['total_fee']); ?></div>
					<div class="content_data_td3">
						<span>
						<?php if ($rv['status'] == '0') : ?>
						处理中
						<?php elseif ($rv['status'] == '1') : ?>
						缺货
						<?php else : ?>
						已出货
						<?php endif; ?>
						</span>
					</div>
                </div>
            </div>
			<?php endforeach; ?>
			<?php endif; ?>
		
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		<?php /*
		<main role="main" id="content">
			<aside class="aside member">
				<h2>会员专区</h2>
				<nav>
					<ul class="menu">
						<li><a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>">会员资料</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>">站内公告</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/service?<?php echo $this->tplVar['status']['args']; ?>">客服记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/history?<?php echo $this->tplVar['status']['args']; ?>">事务历史记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/order?<?php echo $this->tplVar['status']['args']; ?>"><b>订单记录</b></a></li>
					</ul>
				</nav>
			</aside>
			<div class="content">
				<section class="block-frame">
					<h2>订单记录</h2>
					<div class="body">
						<div class="history-list" id="history-list">
							
							<table>
							<thead>
								<tr>
									<th><span>订单时间</span></th>
									<th><span>使用点数</span></th>
									<th><span>商品名称</span></th>
									<th><span>出货状态</span></th>
									<th><span>订单明细</span></th>
								</tr>
							</thead>
							<tbody>
							
							<?php if (!empty($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
								<tr class="<?php echo $rk%2?"odd":"even"; ?>">
									<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
									<td class="total_fee"><span><?php echo sprintf('%0.2f', $rv['total_fee']); ?></span></td>
									<td class="name"><a href="<?php echo $rv['href']; ?>" target="_blank"><span><?php echo $rv['name']; ?></span></a></td>
									<td class="status">
										<span>
										<?php if ($rv['status'] == '0') : ?>
										处理中
										<?php elseif ($rv['status'] == '1') : ?>
										缺货
										<?php else : ?>
										已出货
										<?php endif; ?>
										</span>
									</td>
									<td class="spoint">
										<a class="detail" href="#<?php echo $this->config->default_main.'/history/saja_detail?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
											<span>明细<span>
										</a>
									</td>
								</tr>
							<?php endforeach; ?>
							<?php endif; ?>
							</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
			<div class="clear"></div>
		</main>*/?>

	
