<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #000;
			text-decoration: none;
		}
		#content_list {
			background-color:#EFEFEF;
			border-bottom:1px solid #DDDDDD;
			border-top:1px solid #FFF;
			padding:15px;
			font-size:16px;
			font-weight:bold;
			text-align:center;
			text-shadow: 1px 1px 0 #FFF;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">会员中心</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
	
			<div id="content_scroll"><div id="content">
        
            <div id="rows" style="width: 100%;">
			<a href="<?php echo $this->config->default_main; ?>/member/member_data/?<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">会员资料</div></a>
			<a href="<?php echo $this->config->default_main; ?>/member/news/?<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">站内公告</div></a>
			<?php /*<a href="<?php echo $this->config->default_main; ?>/member/service/?<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">客服记录</div></a>*/?>
			<a href="<?php echo $this->config->default_main; ?>/member/history/?<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">交易纪录</div></a>
			<a href="<?php echo $this->config->default_main; ?>/member/order/?<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">订单记录</div></a>
			<a href="<?php echo $this->config->default_main; ?>/user/logout"><div id="content_list">登出</div></a>
			</div>
    
			</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
	
	<?php require_once $this->tplVar['block']['footerjs']; ?>
	
