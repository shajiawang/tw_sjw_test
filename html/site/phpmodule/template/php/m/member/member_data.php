<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
		<style type="text/css">
		#content {
			z-index:0;
			display: table;
			margin:0 auto;
		}
		#content_scroll {
			margin-top:45px;
		}
		.content_data {
			line-height:25px;
			display: table-row;
		}
		.content_data div {
			display: table-cell;
			border:1px solid #FFF;
			padding:5px;
		}
		.content_data_td1 {
			width:70px;
			background-color:#CCC;
		}
		.content_data_td2 {
			width:180px;
		}
		#content_btn {
			text-align:center;
			clear:both;
		}
		#content_btn_edit {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:10px;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">会员资料</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
        
            <div>
                <div class="content_data">
                    <div class="content_data_td1">电子邮箱</div><div class="content_data_td2"><?php echo $this->tplVar['table']['record'][0]['email']; ?></div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">登入帐号</div>
					<div class="content_data_td2">
					<?php if (empty($this->tplVar['table']['record'][0]['name'])) : ?>
						<?php echo $this->tplVar['table']['record'][0]['provider_name']; ?>
					<?php else : ?>
						<?php echo $this->tplVar['table']['record'][0]['name']; ?>
					<?php endif; ?>
					</div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">昵称</div><div class="content_data_td2"><?php echo $this->tplVar['table']['record'][0]['nickname']; ?></div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">性别</div><div class="content_data_td2"><?php echo $this->tplVar['table']['record'][0]['gender']; ?></div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">杀币</div><div class="content_data_td2"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['spoint'][0]['amount']); ?> 点</div>
                </div>
				<div class="content_data">
                    <div class="content_data_td1">红利积点</div><div class="content_data_td2"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['bonus'][0]['amount']); ?> 点</div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">店点</div><div class="content_data_td2"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['gift'][0]['amount']); ?> 点</div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">收件地址</div><div class="content_data_td2"><?php echo $this->tplVar['table']['record'][0]['area'] . $this->tplVar['table']['record'][0]['address']; ?></div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">收件人</div><div class="content_data_td2"><?php echo $this->tplVar['table']['record'][0]['addressee']; ?></div>
                </div>
                <div class="content_data">
                    <div class="content_data_td1">连络电话</div><div class="content_data_td2"><?php echo $this->tplVar['table']['record'][0]['phone']; ?></div>
                </div>
            </div>
            <div id="content_btn"><input id="content_btn_edit" name="修改密码" type="submit" value="修改密码" onclick="javascript:location.href='<?php echo $this->config->default_main; ?>/member/edit?<?php echo $this->tplVar['status']['args']; ?>'" /></div>
           
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>	
		<?php /*
		<main role="main" id="content">
			
			<div class="content">
				<section class="block-frame">
					<h2>会员基本资料</h2>
					<div class="body">
						<table>
							<tbody>
								<tr>
									<th>Email:</th>
									<td class="email"><?php echo $this->tplVar['table']['record'][0]['email']; ?></td>
									<th>登入账号:</th>
									<td class="name">
									<?php if (empty($this->tplVar['table']['record'][0]['name'])) : ?>
										<?php echo $this->tplVar['table']['record'][0]['provider_name']; ?>
									<?php else : ?>
										<?php echo $this->tplVar['table']['record'][0]['name']; ?>
									<?php endif; ?>
									</td>
								</tr>
								<tr>
									<th>昵称:</th>
									<td class="nickname"><?php echo $this->tplVar['table']['record'][0]['nickname']; ?></td>
									<th>身份验证码:</th>
									<td class=""></td>
								</tr>
								<tr>
									<th>杀币:</th>
									<td class="spoint"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['spoint'][0]['amount']); ?></td>
									<th>红利积点:</th>
									<td class="bonus"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['bonus'][0]['amount']); ?></td>
								</tr>
								<tr>
									<th>性别:</th>
									<td class="gender"><?php echo $this->tplVar['table']['record'][0]['gender']; ?></td>
									<th>店点:</th>
									<td class="gift"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['gift'][0]['amount']); ?></td>
								</tr>
								<tr>
									<th>收件人:</th>
									<td class="addressee"><?php echo $this->tplVar['table']['record'][0]['addressee']; ?></td>
									<th>联络电话:</th>
									<td class="phone"><?php echo $this->tplVar['table']['record'][0]['phone']; ?></td>
								</tr>
								<tr>
									<th>收货地址:</th>
									<td class="address"><?php echo $this->tplVar['table']['record'][0]['area'].$this->tplVar['table']['record'][0]['address']; ?></td>
									<th></th>
									<td class=""></td>
								</tr>
								<tr>
									<td colspan="4"><hr></td>
								</tr>
								<tr>
									<th>更改密码:</th>
									<td class=""></td>
									<td class="sms_auth" colspan="2"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</section>
			</div>
			<div class="clear"></div>
		</main>*/?>

	
