<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
		<style type="text/css">
		#content {
			z-index:0;
			display: table;
			margin:0 auto;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #324FE1;
			text-decoration: none;
		}
		.content_data_tr {
			line-height:25px;
			display: table-row;
		}
		.content_data_tr div {
			display: table-cell;
			border:1px solid #FFF;
			padding:5px;
			text-align:center;
			border-bottom:1px solid #ECECEC;
		}
		.content_data_title {
			background-color:#ECECEC;
			
		}
		.content_data_td1 {
			width:90px;
		}
		.content_data_td2 {
			width:120px;
		}
		.content_data_td3 {
			width:70px;
		}
		#content_support {
			width:280px;
			line-height:30px;
			padding:5px;
		}
		#content_text {
			width:280px;
			height:100px;
		}
		#content_captcha {
			position:relative;
			line-height:55px;
		}
		#content_captcha input {
			 width:100px;
		}
		#content_captcha img{
			 padding:10px 0px 0px 10px;
			 position:absolute;
			 height:35px;
		}
		#content_recautions {
			line-height:25px;
		}
		#content_btn {
			text-align:center;
		}
		#content_btn_send {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:10px;
		}
		</style>
		<script type="text/javascript">
		$(function(){
			$('.type', '#history-type').on('click', function(){
				$(this)
				.addClass('active')
				.parent().siblings().find('.type.active').removeClass('active');

				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			$( document ).on('click', '#history-list .detail', function(){
				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			//重新整理頁面時，直接觸發click event
			if (window.location.hash) {
				$('#history-type .type[href="'+window.location.hash+'"]').trigger('click');
			} else {
				$('#history-type .type:eq(0)').trigger('click');
			}

			$('#history-type').on('mousemove', function(e){
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				if ($(this).is('a')) {
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				}
			});
		});
		</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">客服记录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
        
            <div>
                <div class="content_data_tr content_data_title">
                    <div class="content_data_td1">讯息标题</div><div class="content_data_td2">讯息时间</div><div class="content_data_td3">回覆状态</div>
                </div>
                <a href="#member_support_reply.htm"><div class="content_data_tr">
                    <div class="content_data_td1">个人帐号相关</div><div class="content_data_td2">2014/01/01 12:00</div><div class="content_data_td3">已回覆</div>
                </div></a>
                <a href="#member_support_reply.htm"><div class="content_data_tr">
                    <div class="content_data_td1">杀币相关</div><div class="content_data_td2">2014/01/01 12:00</div><div class="content_data_td3">已回覆</div>
                </div></a>
            </div>
            
            <div id="content_support">
                <div><b>联络站内信客服</b></div>
                <div>联络事项：<select name=""><option value="杀币相关">杀币相关</option></select></div>
                <div>建议/合作事项：<br /><textarea id="content_text" name="" cols="" rows=""></textarea></div>
                <div id="content_captcha">验证码：<input name="" type="text" /><img src="img/misc.gif" /></div>
                <div id="content_recautions">
                  <b>注意事项</b>
                  <ol>
                    <li>客服服务时间：周一至周五，上午九点至下午六点半。</li>
                    <li>再询问客服人员时，尽量避免言语不当及做人身攻击等不当内容，若经查证属实，将有可能会封锁您的帐号。</li>
                    <li>请注意站内信客服为提供会员的疑问做解答，并未做其他用途之使用。</li>
                    <li>其客服人员在回答问题时，并未让您觉得为最佳的回答，麻烦请您见谅。</li>
                  </ol>
                </div>
            </div>
            <div id="content_btn"><input id="content_btn_send" name="送出" type="button" value="送出"/></div>
			
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		<?php /*
		<main role="main" id="content">
			<aside class="aside member">
				<h2>会员专区</h2>
				<nav>
					<ul class="menu">
						<li><a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>">会员资料</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>">站内公告</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/service?<?php echo $this->tplVar['status']['args']; ?>"><b>客服记录</b></a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/history?<?php echo $this->tplVar['status']['args']; ?>">事务历史记录</b></a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/order?<?php echo $this->tplVar['status']['args']; ?>">订单记录</a></li>
					</ul>
				</nav>
			</aside>
			<div class="content">
				<section class="block-frame">
					<h2>客服记录</h2>
					<div class="body">
						
					</div>
				</section>
			</div>
			<div class="clear"></div>
		</main>*/?>

	
