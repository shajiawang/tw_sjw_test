<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #000;
			text-decoration: none;
		}
		#content_news {
			position:relative;
			padding:10px;
			margin:15px;
			line-height:22px;
			background-color:#F7F7F7;
			border:1px solid #DFDFDF;
			border-radius:5px;
		}
		#content_title_blue {
			color:#324FE1;
			font-weight:bold;
		}
		#content_title_gray {
			color:#999;
		}
		#content_date {
			text-align:right;
			color:#999;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">站内公告</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
        
            <div id="rows" style="width: 100%;">
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
			<a href="<?php echo $this->config->default_main; ?>/member/news_detail?<?php echo $this->tplVar['status']['args']."&newsid=".$rv['newsid']; ?>">
                <div id="content_news">
                    <div id="content_title_blue"><?php echo $rv['name'];?></div>
                    <div id="content_date"><?php echo $rv['ontime']; ?></div>
                </div>
            </a>
			<?php endforeach; ?>
			</div>
		
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		<?php /*
		<main role="main" id="content">
			<aside class="aside member">
				<h2>会员专区</h2>
				<nav>
					<ul class="menu">
						<li><a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>">会员资料</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>"><b>站内公告</b></a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/service?<?php echo $this->tplVar['status']['args']; ?>">客服记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/history?<?php echo $this->tplVar['status']['args']; ?>">事务历史记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/order?<?php echo $this->tplVar['status']['args']; ?>">订单记录</a></li>
					</ul>
				</nav>
			</aside>
			<div class="content">
				<section class="block-frame">
					<h2>站内公告</h2>
					<div class="body">
						<ol class="news-list">
						<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<li>
								
								<a class="ellipsis" href="javascript:slightbox(700,600,'<?php echo $rv['name'];?>','<?php echo $this->config->default_main; ?>/member/news_detail?<?php echo $this->tplVar['status']['args']."&newsid=".$rv['newsid']; ?>')">
									<?php if ($rv['public'] == 'Y') : ?>
									(首页公告)
									<?php else : ?>
									(站内公告)
									<?php endif; ?>
									：<?php echo $rv['name']; ?>
								</a>
								<span>发布日期：<?php echo $rv['ontime']; ?></span>
								<div class="clear"></div>
							</li>
						<?php endforeach; ?>
						</ol>
					</div>
				</section>
			</div>
			<div class="clear"></div>
		</main>*/?>

	
