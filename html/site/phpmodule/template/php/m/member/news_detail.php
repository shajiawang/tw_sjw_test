<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
<style type="text/css">
#content {
	height:auto;
	width:100%;
	position:relative;
	z-index:0;
}
#content_scroll {
	margin-top:45px;
}
#content a {
	color: #000;
	text-decoration: none;
}
#content_title {
	line-height:20px;
	background-color:#EFEFEF;
	border-bottom:1px solid #DDDDDD;
	border-top:1px solid #FFF;
	padding:10px;
	font-weight:bold;
}
#content_date {
	text-align:right;
	font-weight:normal;
}

#content_back {
	background-color:#EFEFEF;
	border-bottom:1px solid #DDDDDD;
	border-top:1px solid #FFF;
	width:100%;
	padding:10px;
	text-align:center;
}
#content_brief {
	line-height:25px;
	margin:10px;
}
#content_brief img{
	width: 100%;
}
#content_brief div {
	width: 100%;
}
#content_brief span {
	width: 100%;
}
#content_brief table{
	width: 100%;
}
#content_brief tr{
	width: 100%;
}
#content_brief td{
	width: 100%;
}
#content_brief p{
	width: 100%;
}
</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">站内公告</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
		
			<div id="content_title"><?php echo $this->tplVar['table']['record'][0]['name']; ?>
				<div id="content_date"><?php echo $this->tplVar['table']['record'][0]['ontime']; ?></div>
			</div>
            <div id="content_brief"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></div>
            <a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>"><div id="content_back"><< 回上一页 <<</div></a>
            
		
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
