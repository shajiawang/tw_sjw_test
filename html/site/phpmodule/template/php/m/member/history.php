<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>	
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #000;
			text-decoration: none;
		}
		#content_list {
			background-color:#FC9;
			border-bottom:1px solid #F90;
			border-top:1px solid #FFF;
			padding:15px;
			font-size:16px;
			font-weight:bold;
			text-align:center;
			text-shadow: 1px 1px 0 #FFF;
		}
		</style>
		<script type="text/javascript">
		$(function(){
			$('.type', '#history-type').on('click', function(){
				$(this)
				.addClass('active')
				.parent().siblings().find('.type.active').removeClass('active');

				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			$( document ).on('click', '#history-list .detail', function(){
				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			//重新整理頁面時，直接觸發click event
			if (window.location.hash) {
				$('#history-type .type[href="'+window.location.hash+'"]').trigger('click');
			} else {
				$('#history-type .type:eq(0)').trigger('click');
			}

			$('#history-type').on('mousemove', function(e){
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				if ($(this).is('a')) {
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				}
			});
		});
		</script>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">交易纪录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
        
            <a href="<?php echo $this->config->default_main; ?>/history/spoint?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">杀币纪录</div></a>
            <a href="<?php echo $this->config->default_main; ?>/history/saja?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">杀价纪录</div></a>
            <a href="<?php echo $this->config->default_main; ?>/history/sgift?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">杀价礼券</div></a>
            <a href="<?php echo $this->config->default_main; ?>/history/pay_get_product?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">中标纪录</div></a>
            <a href="<?php echo $this->config->default_main; ?>/history/bonus?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">红利积点</div></a>
            <a href="<?php echo $this->config->default_main; ?>/history/gift?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>"><div id="content_list">店点纪录</div></a>
    
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
	    
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		
		<?php /*
		<main role="main" id="content">
			<aside class="aside member">
				<h2>会员专区</h2>
				<nav>
					<ul class="menu">
						<li><a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>">会员资料</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>">站内公告</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/service?<?php echo $this->tplVar['status']['args']; ?>">客服记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/history?<?php echo $this->tplVar['status']['args']; ?>"><b>事务历史记录</b></a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/order?<?php echo $this->tplVar['status']['args']; ?>">订单记录</a></li>
					</ul>
				</nav>
			</aside>
			<div class="content">
				<section class="block-frame">
					<h2>事务历史记录</h2>
					<div class="body">
						<ul class="history-type" id="history-type">
							<li><a class="type" href="#<?php echo $this->config->default_main; ?>/history/spoint?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>">杀币记录</a></li>
							<li><a class="type" href="#<?php echo $this->config->default_main; ?>/history/saja?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>">杀价记录</a></li>
							<li><a class="type" href="#<?php echo $this->config->default_main; ?>/history/sgift?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>">杀价礼券</a></li>
							<li><a class="type" href="#<?php echo $this->config->default_main; ?>/history/pay_get_product?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>">得标记录</a></li>
							<li><a class="type" href="#<?php echo $this->config->default_main; ?>/history/bonus?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>">红利积点</a></li>
							<li><a class="type" href="#<?php echo $this->config->default_main; ?>/history/gift?sort_insertt=desc&<?php echo $this->tplVar['status']['args']; ?>">店点</a></li>
							<li class="clear"></li>
						</ul>
						<div class="history-list" id="history-list">
							<table>
								<thead>
									<tr>
										<th><span>时间</span></th>
										<th><span>状态</span></th>
										<th><span>明细</span></th>
										<th><span>点数</span></th>
										<th><span>发票编号</span></th>
									</tr>
								</thead>
								<tbody>
									<tr class="odd">
										<td><span>2013-14-79</span></td>
										<td><span>杀价中</span></td>
										<td class="name"><span>（预设）Appl iPhone 5S 16G 公司货<span></td>
										<td><span>25</span></td>
										<td><span>-</span></td>
									</tr>
									<tr class="even">
										<td><span>2013-14-79</span></td>
										<td><span>杀价中</span></td>
										<td class="name"><span>（预设）Appl iPhone 5S 16G 公司货（预设）Appl iPhone 5S 16G 公司货</span></td>
										<td><span>25</span></td>
										<td><span>-</span></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
			<div class="clear"></div>
		</main>*/?>

	
