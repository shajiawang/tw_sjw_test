<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>
		<style type="text/css">
		#content {
			z-index:0;
			display: table;
			margin:0 auto;
		}
		#content_scroll {
			margin-top:45px;
		}
		.content_data {
			line-height:25px;
			display: table-row;
		}
		.content_data div {
			display: table-cell;
			border:1px solid #FFF;
			padding:5px;
		}
		.content_data_td1 {
			width:70px;
			background-color:#CCC;
		}
		.content_data_td2 {
			width:180px;
		}
		#content_btn {
			text-align:center;
			clear:both;
		}
		#content_btn_edit {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:10px;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">订单记录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">			
			
			<table>
				<thead>
					<tr>
						<th><span>下标时间</span></th>
						<th><span>手续费</span></th>
						<th><span>下标次数</span></th>
					</tr>

				</thead>
				<tbody>
				<?php if (!empty($this->tplVar['table']['record'])) : ?>
				<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
					<tr class="<?php echo $rk%2?"odd":"even"; ?>">
						<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
						<td class="saja_fee"><span><?php echo sprintf('%0.2f', $rv['saja_fee'] * $rv['count']); ?></span></td>
						<td class="count"><span><?php echo $rv['count']; ?></span></td>
					</tr>
				<?php endforeach; ?>
				<?php endif; ?>
				</tbody>
			</table>
			
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>	
