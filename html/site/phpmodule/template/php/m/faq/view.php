<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #000;
			text-decoration: none;
		}
		#content_list {
			background-color:#EFEFEF;
			border-bottom:1px solid #DDDDDD;
			border-top:1px solid #FFF;
			width:100%;
			padding:10px;
		}
		</style>
		
		<style type="text/css">
		ul, li {
			margin: 0;
			padding: 0;
			list-style: none;
		}
		#qaContent {
			margin-top: 60px;
		}
		#qaContent h3 {
			text-indent: -9999px;
		}
		#qaContent h3.qa_group_1 {
		}
		#qaContent h3.qa_group_2 {
		}
		#qaContent ul.accordionPart {
		}
		#qaContent ul.accordionPart li {
			border-bottom: solid 1px #e3e3e3;
			padding-bottom: 12px;
			margin-top: 12px;
		}
		#qaContent ul.accordionPart li #qa_title {
			cursor: pointer;
		}
		#qaContent ul.accordionPart li .qa_title_on {
			text-decoration: underline;
		}
		#qaContent ul.accordionPart li .qa_content {
			margin: 6px 0 0;
			padding-left: 28px;
			color: #666;
		}
		</style>
		<script type="text/javascript">
		$(function(){
				// 幫 div.qa_title 加上 hover 及 click 事件
				// 同時把兄弟元素 div.qa_content 隱藏起來
				$('#qaContent ul.accordionPart li div.qa_title').hover(function(){
					$(this).addClass('qa_title_on');
				}, function(){
					$(this).removeClass('qa_title_on');
				}).click(function(){
					// 當點到標題時，若答案是隱藏時則顯示它；反之則隱藏
					$(this).next('#qa_content').slideToggle();
				}).siblings('#qa_content').hide();

				// 全部展開
				$('#qaContent .qa_showall').click(function(){
					$('#qaContent ul.accordionPart li #qa_content').slideDown();
					return false;
				});

				// 全部隱藏
				$('#qaContent .qa_hideall').click(function(){
					$('#qaContent ul.accordionPart li #qa_content').slideUp();
					return false;
				});
				
				var _q = <?php echo 'qt'. $this->tplVar['table']['record'][0]['fcid']; ?>;
				if(_q!=''){
					$(_q).siblings('#qa_content').show();
				}
		});
		</script>
		
	
	<?php require_once $this->tplVar['block']['header']; ?>
		
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="page-title">新手教学</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
		<div class="page-content" data-scroll="" data-zone="">
		<div class="list-wrapper">
		
			<div id="qaContent" >
			<ul class="accordionPart">
				<?php foreach($this->tplVar['table']['rt']['faq_category'] as $mk1 => $mv1) : ?>
				<li>
					<div id="qt<?php echo $mk1;?>" class="qa_title" >
						<div id="content_list"><?php echo $mv1["cname"];?></div>
					</div>
					<div id="qa_content">
					<?php foreach($mv1["menu"] as $fid => $fname) : ?>
						<?php /*novice_detailed.htm*/?>
						<a id="link_new" href="<?php echo $this->config->default_main;?>/faq/detail/?fid=<?php echo $fid;?>">
						<div style="color:#3E3E3E;"><?php echo $fname;?></div>
						</a>
					<?php endforeach; ?>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
			</div>
			
		</div>
		</div>
        
    </div>
	</div>
	
	<?php require_once $this->tplVar['block']['footerjs']; ?>
