<script type="text/javascript">
window.siteName = "杀价王";
window.themeName = "SajaWa";
var myCategory;
var myScroll;

$(function(){
    $('a').attr('data-transition', 'slide');
});

menu_check = 0;
function menu<?php echo $this->io->loc;?>() {
	if(menu_check == 0) {
		$("#page-<?php echo $this->io->loc;?>").animate({ whyNotToUseANonExistingProperty: -85 }, {
			step: function(now,fx) {
				$(this).css('-webkit-transform',"translate3d(" + now + "%, 0px, 0px)");
				$(this).css('-moz-transform',"translate3d(" + now + "%, 0px, 0px)");
			},
			duration:'fast'
		},'linear');
		menu_check = 1;
	}
	else {
		$("#page-<?php echo $this->io->loc;?>").animate({ whyNotToUseANonExistingProperty: 0 }, {
			step: function(now,fx) {
				$(this).css('-webkit-transform',"translate3d(" + now + "%, 0px, 0px)");
				$(this).css('-moz-transform',"translate3d(" + now + "%, 0px, 0px)");
			},
			duration:'fast'
		},'linear');
		menu_check = 0;
	}
}
</script>

<div id="layout" style="-webkit-transform: translate3d(0, 0, 0);">
<?php
if($this->io->input["get"]["fun"]=='exchange_mall'){
	$form_act = $this->config->default_main .'/exchange_mall/?channelid='. $this->io->input['get']['channelid'];
	$_category = $this->tplVar['table']['rt']['exchange_product_category'];
} else {
	$form_act = $this->config->default_main .'/product/?channelid='. $this->io->input['get']['channelid'];
	$_category = $this->tplVar['table']['rt']['product_category'];
}
$search_name = (isset($this->io->input["get"]["search_name"])) ? $this->io->input["get"]["search_name"] : '';	
?>

<!--殺價商品分类列表-->
<div class="drawer-panel-right" id="category-list-panel" style="position: absolute; z-index: 0; height: 100%; right:0px; top: 0px;">
    
	<div id="search-bar-<?php echo $this->io->loc;?>" class="search-bar" style="display:none;">
      <form id="product_search" method="get" action="<?php echo $form_act; ?>">
        <div class="search-box-wrapper">
			<input type="search" name="search_name" value="<?php echo (!empty($search_name)) ? $search_name : '';?>" class="search-box" placeholder="请输入查找内容">
          <div class="icon"> </div>
        </div>
      </form>
      <div class="button right search" onClick="$('#product_search').submit()" data-button="search" style="cursor: pointer;" ><!--搜寻按鈕--></div>
    </div>
	
	<?php 
	$item_c = array(
		0=>'purple',
		1=>'red',
		2=>'yellow',
		3=>'cyan',
		4=>'blue',
	);
	foreach($_category as $pck1 => $pcv1){
		if ($pcv1['layer'] == 1) { 
			$layer_rows[] = ($this->io->input["get"]["fun"]=='exchange_mall') ? $pcv1['epcid'] : $pcv1['pcid']; 
		}
	}
	$i=1; 
	foreach($layer_rows as $rv){
		$ii = $i % 5;
		$cat_class[$rv] = $item_c[$ii];
		$i++; 
	}
	?>
    
	<div id="category-list-<?php echo $this->io->loc;?>" class="category-list" data-scroll="" style="display:none;">
	<div id="Category1">
	
      <div class="scroll-content" style="left: 0px; right: 0px; position: absolute; top: 0px; z-index: 2; min-height: 100%; min-width: 100%; -webkit-transform: translate3d(0px, 0px, 0);">
		
		<?php foreach($_category as $pck1 => $pcv1) : ?>
		<?php 
		if ($pcv1['layer'] == 1) : 
		
		if($this->io->input["get"]["fun"]=='exchange_mall'){
			$L1_pcid = $pcv1['epcid'];
			$a_href = $this->config->default_main .'/exchange_mall/?channelid='. $this->io->input['get']['channelid'] .'&epcid='. $L1_pcid;
		} else {
			$L1_pcid = $pcv1['pcid'];
			$a_href = $this->config->default_main .'/product/?channelid='. $this->io->input['get']['channelid'] .'&pcid='. $L1_pcid;
		}
		?>
			<a href="<?php echo $a_href; ?>">
			<div class="category-item <?php echo $cat_class[$L1_pcid];?>" style="cursor: pointer;">
			  <div class="title"><?php echo $pcv1['name']; ?></div>
				<div class="sub-title">
				<?php foreach($_category as $pck2 => $pcv2) : ?>
				<?php if ($pcv2['layer'] == 2 && $pcv2['node'] == $L1_pcid) : ?>
				<?php echo $pcv2['name']; ?>
				<?php endif; ?>
				<?php endforeach; ?>
				</div>
			</div></a>
		<?php endif; ?>
		<?php endforeach; ?>
	  </div>
      <div style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; visibility: hidden;"></div>
      <div style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; visibility: hidden;"></div>
      <div style="z-index: 10000; width: 4px; position: absolute; background-color: rgba(64, 64, 64, 0.498039); border-top-left-radius: 3px; border-top-right-radius: 3px; border-bottom-right-radius: 3px; border-bottom-left-radius: 3px; opacity: 0; -webkit-transform: translate3d(-8px, 0px, 0); background-position: initial initial; background-repeat: initial initial;"></div>
    
	</div></div>
</div>
<!--殺價商品分类列表-->
