<?php if (!empty($this->tplVar['page']['item'])) :?>
<ul>
	<li>资料共<?php echo $this->tplVar['page']['total']; ?>笔</li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['firstpage'] ?>">最前页</a></li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['previouspage'] ?>">上一页</a></li>
	<li class="splitter"></li>
	<li>&nbsp;[&nbsp;</li>
	<li>
        <?php foreach ($this->tplVar['page']['item'] as $item) : ?>
        <a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $item['p']; ?>"><?php echo $item['p']; ?></a>
        <?php endforeach; ?>
	</li>
	<li>&nbsp;]&nbsp;</li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['nextpage'] ?>">下一页</a></li>
	<li class="splitter"></li>
	<li><a href="<?php echo $this->tplVar["status"]["path"] ?><?php echo $this->tplVar["status"]["search_path"] ;?><?php echo $this->tplVar["status"]["sort_path"] ;?>&p=<?php echo $this->tplVar['page']['lastpage'] ?>">最后页</a></li>
</ul>
<?php endif; ?>
