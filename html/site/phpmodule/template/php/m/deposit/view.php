<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content_logo {
			text-align:center;
			width: 100%;
			height:auto;
		}
		</style>
		<style type="text/css">
		.deposit-rule {
			/*list-style: none;*/
		}
		.deposit-rule li {
			float: left;
			text-align: center;
			width: 100%;
		}
		.deposit-rule .link {
			display: block;
			text-decoration: none;
			/*width: 214px;*/
			min-height: 198px;
			border: 1px solid #D5D5D5;
			border-radius: 5px;
			padding: 5px 0;
		}
		.deposit-rule .logo {
			display: block;
			max-width: 214px;
			max-height: 156px;
			margin: 0 auto;
		}
		.deposit-rule .name {
			text-align: center;
			color: #6a6a6a;
			width: 100%;
			height: 40px;
			line-height: 32px;
			font-size: 26px;
			/*position: absolute;*/
			bottom: 0;
			border-top: 1px solid #D5D5D5;
		}
		</style>
		
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">杀价币充值</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
    
			<div id="content_scroll">
			<div id="content">
			
			<div id="content_logo">
			<ul class="deposit-rule">
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<li class="rule">
					<a class="link" href="<?php echo $this->config->default_main.'/deposit/deposit_rule_item?'.$this->tplVar['status']['args'].'&drid='.$rv['drid']; ?>">
						<img class="logo" src="<?php echo $this->config->path_image.'/'.$rv['logo']; ?>">
						<div class="name"><?php echo $rv['name']; ?></div>
					</a>
				</li>
			<?php endforeach; ?>
			</ul>
			</div>
			
			</div></div>
		
        </div>
		</div>
	
	</div>
	</div>
	
<?php require_once $this->tplVar['block']['footerjs']; ?>

	<?php /*
		<main role="main" id="content">
			<div class="content">
				<ul class="deposit-rule">
					<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
					<li class="rule">
						<a class="link" href="<?php echo $this->config->default_main.'/deposit/deposit_rule_item?'.$this->tplVar['status']['args'].'&drid='.$rv['drid']; ?>">
							<img class="logo" src="<?php echo $this->config->path_image.'/'.$rv['logo']; ?>">
							<div class="name"><?php echo $rv['name']; ?></div>
						</a>
					</li>
				<?php endforeach; ?>
					<li class="clear"></li>
				</ul>
				<div class="clear"></div>
			</div>
		</main>
		*/?>
	
