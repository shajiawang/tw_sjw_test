<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content_logo {
			text-align:center;
		}
		#content_pay {
			line-height:25px;
			padding-top:15px;
			position:relative;
			left:50%;
			margin-left:-121px;
			text-align:center;
		}
		#content_pay div {
			float:left;
			margin:1px;
			padding:5px;
			border:1px solid #999;
			height:25px;
		}
		#content_pay_td1 {
			width:80px;
			clear:both;
			background-color:#CCC;
		}
		#content_pay_td2 {
			width:120px;
		}
		#content_pay_td3 {
			width:40px;
			clear:both;
		}
		#content_pay_td4 {
			width:80px;
		}
		#content_btn {
			text-align:center;
			padding-top:15px;
			clear:both;
		}
		#content_btn_pay {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
		}
		</style>
		
	
	<?php require_once $this->tplVar['block']['header']; ?>
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">杀价币充值</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll">
		<div id="content">
		
			<div id="content_logo">
			<img class="logo" src="<?php echo $this->config->path_image.'/'.$this->tplVar['table']['rt']['deposit_rule_item'][0]['logo']; ?>">
			</div>
			
			<form name="deposit-rule-item" method="POST" action="<?php echo $this->config->alipay['url_payment']; ?>">
			<div id="content_pay">
				
				<div id="content_pay_td1">充值点数</div>
				<div id="content_pay_td2"><?php echo sprintf("%d",$this->tplVar['table']['rt']['deposit_rule_item'][0]['spoint']); ?> 点</div>
				
				<div id="content_pay_td1">缴款金额</div>
				<div id="content_pay_td2">RMB <?php echo sprintf("%0.2f",$this->tplVar['table']['rt']['deposit_rule_item'][0]['amount']); ?> 元</div>
				
			</div>
			<div id="content_btn"><input id="content_btn_pay" name="确认充值" type="submit" value="确认充值" /></div>
			<input type="hidden" name="merchantnumber" value="<?php echo $this->config->alipay['merchantnumber']; ?>">
			<input type="hidden" name="ordernumber" value="<?php echo $this->tplVar['table']['record'][0]['ordernumber']; ?>">
			<input type="hidden" name="amount" value="<?php echo sprintf("%d",$this->tplVar['table']['rt']['deposit_rule_item'][0]['amount']); ?>">
			<input type="hidden" name="paymenttype" value="<?php echo $this->config->alipay['paymenttype']; ?>">
			<input type="hidden" name="paytitle" value="<?php echo $this->tplVar['table']['rt']['deposit_rule_item'][0]['rule_name'].' '.$this->tplVar['table']['rt']['deposit_rule_item'][0]['name']; ?>">
			<input type="hidden" name="hash" value="<?php echo $this->tplVar['table']['record'][0]['hash']; ?>">
			<input type="hidden" name="nexturl" value="<?php echo 'http://howard.sajar.com.tw'.$this->config->default_main.'/deposit/alipay_complete'; ?>">
			</form>

		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
		
	<?php require_once $this->tplVar['block']['footerjs']; ?>
	
	<?php /*
		<main role="main" id="content">
			<div class="content">
				<img class="logo" src="<?php echo $this->config->path_image.'/'.$this->tplVar['table']['rt']['deposit_rule_item'][0]['logo']; ?>">
				<form name="deposit-rule-item" method="POST" action="<?php echo $this->config->alipay['url_payment']; ?>">
					<ul>
						<li>充值方式：支付寶</li>
						<li>充值點數：<?php echo sprintf("%d",$this->tplVar['table']['rt']['deposit_rule_item'][0]['spoint']); ?></li>
						<li>交易金額：<?php echo sprintf("%0.2f",$this->tplVar['table']['rt']['deposit_rule_item'][0]['amount']); ?></li>
						<li>
							<input type="submit" value="確定充值">
							<input type="hidden" name="merchantnumber" value="<?php echo $this->config->alipay['merchantnumber']; ?>">
							<input type="hidden" name="ordernumber" value="<?php echo $this->tplVar['table']['record'][0]['ordernumber']; ?>">
							<input type="hidden" name="amount" value="<?php echo sprintf("%d",$this->tplVar['table']['rt']['deposit_rule_item'][0]['amount']); ?>">
							<input type="hidden" name="paymenttype" value="<?php echo $this->config->alipay['paymenttype']; ?>">
							<input type="hidden" name="paytitle" value="<?php echo $this->tplVar['table']['rt']['deposit_rule_item'][0]['rule_name'].' '.$this->tplVar['table']['rt']['deposit_rule_item'][0]['name']; ?>">
							<input type="hidden" name="hash" value="<?php echo $this->tplVar['table']['record'][0]['hash']; ?>">
							<input type="hidden" name="nexturl" value="<?php echo 'http://howard.sajar.com.tw'.$this->config->default_main.'/deposit/alipay_complete'; ?>">
						</li>
					</ul>
				</form>
			</div>
		</main>
		*/?>
	
