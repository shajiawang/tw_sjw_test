<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #324FE1;
			text-decoration: none;
		}
		.content_data_table {
			margin:0 auto;
			display: table;
		}
		.content_data_title {
			background-color:#ECECEC;
		}
		.content_data_captionA{
			padding:8px 8px 8px 11px;
			display: table-caption;
			border:1px solid #FFF;
		}
		.content_data_captionB{
			padding:8px 8px 0px 11px;
			display: table-caption;
			border:1px solid #FFF;
		}
		.content_data_tr {
			line-height:25px;
			display: table-row;
		}
		.content_data_tr div {
			display: table-cell;
			border:1px solid #FFF;
			padding:5px;
			text-align:center;
			border-bottom:1px solid #ECECEC;
		}
		.content_data_td1 {
			width:35px;
		}
		.content_data_td2 {
			width:120px;
		}
		.content_data_td3 {
			width:120px;
		}
		.content_blue {
			color:#324FE1;
		}
		.content_gray {
			color:#999;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">交易纪录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
			
			<div class="content_data_table">
                <div class="content_data_captionA content_data_title">礼券名称 × 数量</div>
                <div class="content_data_tr content_data_title">
                    <div class="content_data_td1">编号</div>
					<div class="content_data_td2">获得时间</div>
					<div class="content_data_td3">使用期限</div>
                </div>
            </div>
            
            <div>
			<?php if (!empty($this->tplVar['table']['record'])) : ?>
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<div class="content_data_table content_gray">
					<div class="content_data_captionA"><?php echo $rv['name']; ?> × <?php echo $rv['count']; ?> 张</div>
					<div class="content_data_tr">
						<div class="content_data_td1"><?php echo $rv['productid']; ?></div>
						<div class="content_data_td2"><?php echo $rv['insertt']; ?></div>
						<div class="content_data_td3">
							<?php if ($rv['offtime']) : ?>
							<?php echo date("Y-m-d H:i:s", $rv['offtime']); ?>
							<?php else : ?>
							****-**-** --:--:--
							<?php endif; ?>
						</div>
		
					</div>
				</div>
			<?php endforeach; ?>	
			<?php endif; ?>
			</div>
    
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
	    
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		
		<?php /*
		<table>
	<thead>
		<tr>
			<th><span>商品编号</span></th>
			<th><span>礼券名称</span></th>
			<th><span>数量</span></th>
			<th><span>获得时间</span></th>
			<th><span>使用期限</span></th>
		</tr>
	</thead>

	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="productid"><span><?php echo $rv['productid']; ?></span></td>
			<td class="name"><span><?php echo $rv['name']; ?></span></td>
			<td class="count"><span><?php echo $rv['count']; ?></span></td>
			<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
		<?php if ($rv['offtime']) : ?>
			<td class="offtime"><span><?php echo date("Y-m-d H:i:s", $rv['offtime']); ?></span></td>
		<?php else : ?>
			<td class="offtime"><span>****-**-** --:--:--</span></td>
		<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>*/?>

	


