<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			z-index:0;
			display: table;
			margin:0 auto;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #324FE1;
			text-decoration: none;
		}
		.content_data_tr {
			line-height:25px;
			display: table-row;
		}
		.content_data_tr div {
			display: table-cell;
			border:1px solid #FFF;
			padding:5px;
			text-align:center;
			border-bottom:1px solid #ECECEC;
		}
		.content_data_title {
			background-color:#ECECEC;
			
		}
		.content_data_td1 {
			width:120px;
		}
		.content_data_td2 {
			width:70px;
		}
		.content_data_td3 {
			width:70px;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">交易纪录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
        
			<div>
                <div class="content_data_tr content_data_title">
                    <div class="content_data_td1">储值时间</div>
					<div class="content_data_td2">状态</div>
					<div class="content_data_td3">明细</div>
                </div>
                
                <?php if (!empty($this->tplVar['table']['record'])) : ?>
				<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
				
				<div class="content_data_tr">
                    <div class="content_data_td1"><?php echo $rv['insertt']; ?></div>
					<div class="content_data_td2">
						<span>
						<?php if ($rv['status'] == 'deposit') : ?>
						储值成功
						<?php elseif ($rv['status'] == 'error') : ?>
						储值失败
						<?php else : ?>
						储值取消
						<?php endif; ?>
						</span>
					</div>
					<div class="content_data_td3"><?php echo sprintf('%0.2f', $rv['amount']); ?></div>
                </div>
				
				<?php endforeach; ?>
				<?php endif; ?>
            </div>
    
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
	    
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		
		<?php /*
		<table>
	<thead>
		<tr>
			<th><span>时间</span></th>
			<th><span>状态</span></th>
			<th><span>明细</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
			<td class="status">
				<span>
				<?php if ($rv['status'] == 'deposit') : ?>
				储值成功
				<?php elseif ($rv['status'] == 'error') : ?>
				储值失败
				<?php else : ?>
				储值取消
				<?php endif; ?>
				</span>
			</td>
			<td class="spoint"><span><?php echo sprintf('%0.2f', $rv['amount']); ?><span></td>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>*/?>

	
