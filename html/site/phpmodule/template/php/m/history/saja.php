<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
</head>
	<body>		
		<style type="text/css">
		#content {
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content a {
			color: #324FE1;
			text-decoration: none;
		}
		.content_data_table {
			margin:0 auto;
			display: table;
		}
		.content_data_title {
			background-color:#ECECEC;
		}
		.content_data_captionA{
			padding:8px 8px 8px 11px;
			display: table-caption;
			border:1px solid #FFF;
		}
		.content_data_captionB{
			padding:8px 8px 0px 11px;
			display: table-caption;
			border:1px solid #FFF;
		}
		.content_data_tr {
			line-height:25px;
			display: table-row;
		}
		.content_data_tr div {
			display: table-cell;
			border:1px solid #FFF;
			padding:5px;
			text-align:center;
			border-bottom:1px solid #ECECEC;
		}
		.content_data_td1 {
			width:60px;
		}
		.content_data_td2 {
			width:60px;
		}
		.content_data_td3 {
			width:60px;
		}
		.content_data_td4 {
			width:60px;
		}
		</style>
	
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page-<?php echo $this->io->loc;?>" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <a data-rel="back" data-transition="slide" class="ui-link"><div class="button left back" style="cursor: pointer;"></div></a>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu<?php echo $this->io->loc;?>()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">交易纪录</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
        
            <div class="content_data_table">
                <div class="content_data_captionA content_data_title">商品名称　<span style="color:#F00; font-weight:bold;">※ 点选商品名称有下标明细</span></div>
                <div class="content_data_tr content_data_title">
                    <div class="content_data_td1">商品编号</div>
					<div class="content_data_td2">手续费</div>
					<div class="content_data_td3">总下标数</div>
					<div class="content_data_td4">商品状态</div>
                </div>
            </div>
            
            <div>
			<?php if (!empty($this->tplVar['table']['record'])) : ?>
			<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
			
			<a href="<?php echo $this->config->default_main.'/history/saja_detail?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
			<div class="content_data_table">
                <div class="content_data_captionB"><?php echo $rv['name']; ?></div>
                <div class="content_data_tr">
                    <div class="content_data_td1"><?php echo $rv['productid']; ?></div>
					<div class="content_data_td2"><?php echo sprintf('%0.2f', $rv['saja_fee']); ?></div>
					<div class="content_data_td3"><?php echo $rv['count']; ?></div>
					<div class="content_data_td4">
						<span>
						<?php if ($rv['closed'] == 'Y') : ?>
						已结标
						<?php elseif ($rv['closed'] == 'N') : ?>
						抢标中
						<?php else : ?>
						流标
						<?php endif; ?>
						</span>
					</div>
                </div>
            </div>
			</a>
			
			<?php endforeach; ?>
			<?php endif; ?>
			</div>
    
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
	    
	<?php require_once $this->tplVar['block']['footerjs']; ?>
		
		<?php /*
		<table>
	<thead>
		<tr>
			<th><span>商品编号</span></th>
			<th><span>商品明细</span></th>
			<th><span>手续费</span></th>
			<th><span>下标次数</span></th>
			<th><span>商品状态</span></th>
			<th><span>下标明细</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="productid"><span><?php echo $rv['productid']; ?></span></td>
			<td class="name"><span><?php echo $rv['name']; ?></span></td>
			<td class="saja_fee"><span><?php echo sprintf('%0.2f', $rv['saja_fee']); ?></span></td>
			<td class="count"><span><?php echo $rv['count']; ?></span></td>
			<td class="closed">
				<span>
				<?php if ($rv['closed'] == 'Y') : ?>
				已结标
				<?php elseif ($rv['closed'] == 'N') : ?>
				竞标中
				<?php else : ?>
				流标
				<?php endif; ?>
				</span>
			</td>
			<td class="spoint">
				<a class="detail" href="#<?php echo $this->config->default_main.'/history/saja_detail?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
					<span>明细<span>
				</a>
			</td>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>*/?>

	

