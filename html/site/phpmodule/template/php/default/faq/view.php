<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			float: left;
			width: 790px;
			border: 1px solid #C1C1C1;
		}
		#content[role=main] .content .block-frame h2 {
			padding-left: 12px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #BB175D;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}
		#content[role=main] .content .body {
			border-top: 2px solid #BB175D;
			padding: 8px 6px;
		}
		#content[role=main] .content .body table {
			/*border: 1px solid #E4E4E4;*/
			width: 100%;
		}
		#content[role=main] .content .body table th {
			color: #878787;
			font-size: 1em;
			text-align: left;
			padding: 10px 6px;
			width: 20%;
		}
		#content[role=main] .content .body table td {
			text-align: left;
			width: 30%;
		}
		#content[role=main] .content .body hr {
			margin-left: 4px;
			margin-right: 4px;
		}

		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}
		.aside.member .menu li {
			margin: 4px 0;
		}
		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		</style>
		<style type="text/css">
		ul, li {
			margin: 0;
			padding: 0;
			list-style: none;
		}
		#qaContent {
			/* width: 500px; */
		}
		#qaContent h3 {
			/* width: 500px; height: 22px; */
			text-indent: -9999px;
		}
		#qaContent h3.qa_group_1 {
			background: url(qa_group_1.gif) no-repeat;
		}
		#qaContent h3.qa_group_2 {
			background: url(qa_group_2.gif) no-repeat;
		}
		#qaContent ul.accordionPart {
			/* margin: 10px 10px 50px 30px; */
		}
		#qaContent ul.accordionPart li {
			border-bottom: solid 1px #e3e3e3;
			padding-bottom: 12px;
			margin-top: 12px;
		}
		#qaContent ul.accordionPart li #qa_title {
			/* background: url(icon_q_a.gif) no-repeat 0px 3px;
			padding-left: 28px; color: #1186ec; */
			cursor: pointer;
		}
		#qaContent ul.accordionPart li .qa_title_on {
			text-decoration: underline;
		}
		#qaContent ul.accordionPart li .qa_content {
			margin: 6px 0 0;
			background: url(icon_q_a.gif) no-repeat 0px -24px;
			padding-left: 28px;
			color: #666;
		}
		</style>
		<script type="text/javascript">
		$(function(){
				// 幫 div.qa_title 加上 hover 及 click 事件
				// 同時把兄弟元素 div.qa_content 隱藏起來
				$('#qaContent ul.accordionPart li div.qa_title').hover(function(){
					$(this).addClass('qa_title_on');
				}, function(){
					$(this).removeClass('qa_title_on');
				}).click(function(){
					// 當點到標題時，若答案是隱藏時則顯示它；反之則隱藏
					$(this).next('#qa_content').slideToggle();
				}).siblings('#qa_content').hide();

				// 全部展開
				$('#qaContent .qa_showall').click(function(){
					$('#qaContent ul.accordionPart li #qa_content').slideDown();
					return false;
				});

				// 全部隱藏
				$('#qaContent .qa_hideall').click(function(){
					$('#qaContent ul.accordionPart li #qa_content').slideUp();
					return false;
				});
				
				var _q = <?php echo 'qt'. $this->tplVar['table']['record'][0]['fcid']; ?>;
				if(_q!=''){
					$(_q).siblings('#qa_content').show();
				}
		});
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		
		<main role="main" id="content">
		<aside class="aside member" style="width:200px;">
			<h2>帮助中心</h2>
			<section class="product-category-list">
			<?php //include_once($this->tplVar["block"]["help_menu"]); ?>
			<div id="qaContent" style="width:200px; background-color:#EFEFEF; font-size: 10pt;">
			<div style="text-align:center; text-decoration:none; height:25px; ">
				<a href="#" class="qa_showall">全部展开</a> | <a href="#" class="qa_hideall">全部隐藏</a>

			</div>
			<ul class="accordionPart">
				<?php foreach($this->tplVar['table']['rt']['faq_category'] as $mk1 => $mv1) : ?>
				<li>
					<div id="qt<?php echo $mk1;?>" class="qa_title" style="cursor: pointer; position:relative; height:31px;">
						<img src="http://img.saja.com.tw/tp/div_templates_server_link_title.gif" style="position:absolute; left:16px; top: 0px;" />
						<div style="position:absolute; left:16px; top:6px; width:178px; font-size:12pt; font-weight:bold; text-align:center; color: #444444;"><?php echo $mv1["cname"];?></div>
					</div>
					
					<div id="qa_content">
					<?php foreach($mv1["menu"] as $fid => $fname) : ?>
						<a id="ctl00_ctl00_ContentPlaceHolder1_link_new" href="<?php echo $this->config->default_main;?>/faq/?fid=<?php echo $fid;?>">
						<div style="cursor:pointer; text-decoration: none; position:relative; left:5px; width:178px; height:25px; background: url(&quot;&quot;) repeat scroll 0% 0% transparent;" onmouseover="this.style.background='url(http://img.saja.com.tw/tp/div_templates_server_link_on.gif)'; this.style.fontWeight='bold';" onmouseout="this.style.background='url()'; this.style.fontWeight='normal';">
							<?php if($this->tplVar['table']['record'][0]['faqid']==$fid) : ?>
							<div style="position:absolute; left:22px; top:6px; color:#ff0000; font-weight:bold;"><?php echo $fname;?></div>
							<?php else : ?>
							<div style="position:absolute; left:22px; top:6px; color:#3E3E3E;"><?php echo $fname;?></div>
							<?php endif; ?>
						</div>
						</a>
					<?php endforeach; ?>
					</div>
				</li>
				<?php endforeach; ?>
			</ul>
			</div>
			</section>
		</aside>
			
		<div class="content">
		<section class="block-frame">
		<h2><?php echo $this->tplVar['table']['record'][0]['cname'];?></h2>
		<div class="body">
			
			<!--QA_content 內容區-->
			<div style="float:left; width:750px;">
				<div style="height:10px;"></div>
				<div style="width:750px;">
					<div style="height:10px;"></div>
					
					<div style="height:12px;">
						<img src="http://img.saja.com.tw/tp/div_templates_server_icon.gif" /> <span style="position:relative; top:-2px; font-weight:bold; color:#3E3E3E;"><?php echo $this->tplVar['table']['record'][0]['menu'];?></span>
					</div>
					
					<div style="width:750px; height:9px; border-bottom:#DDDDDD 1px solid;"></div>
					<div style="height:10px;"></div>
					
					<div style=" line-height:25px;">
						<div style="margin-left:10px;">
							<div style="width:743px; text-align:right; font-weight:bold;">更新日期：<?php echo date("Y-m-d", strtotime($this->tplVar['table']['record'][0]['modifyt'] ) );?></div>
							
							<a name="" id=""></a>
							
							<?php if($this->tplVar['table']['record'][0]['desc_only']=='N') : ?>
							<div style="width:743px; float:left; font-weight:bold; border-bottom:#E8E8E8 dotted 1px;">
								<img src="http://img.saja.com.tw/tp/div_templates_novice_icon_question.gif" style="position:relative; top:4px;" />　<?php echo $this->tplVar['table']['record'][0]['name'];?>
							</div>
							<div style="width:28px; clear:both; float:left;">
								<img src="http://img.saja.com.tw/tp/div_templates_novice_icon_answer.gif" style="position:relative; top:5px;" />
							</div>
							<?php endif; ?>
							
							<div style="width:743px; float:left;">
								<?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']);?>
								
								<div align="right">
									<a href="#"><img src="http://img.saja.com.tw/tp/div_templates_novice_icon_top.gif" alt="" /></a>
								</div>
							</div>
							<div style="height:30px; clear:both;"></div>
						</div>
					</div>
					<div style="height:10px;"></div>
				</div>
				<div style="height:10px;"></div>
			</div>
			<!--QA_content 內容區-->
		
		</div>
		</section>
		</div>
		
		<div class="clear"></div>
		<div id="show_ajax" style="display:none"></div>
		</main>
		
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>