<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			float: left;
			width: 792px;
			border: 1px solid #C1C1C1;
		}
		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #BB175D;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}
		#content[role=main] .content .body {
			border-top: 2px solid #BB175D;
			padding: 10px 8px;
		}
		#content[role=main] .content .body table {
			/*border: 1px solid #E4E4E4;*/
			width: 100%;
		}
		#content[role=main] .content .body table th {
			color: #878787;
			font-size: 1em;
			text-align: left;
			padding: 13px 10px;
			width: 20%;
		}
		#content[role=main] .content .body table td {
			text-align: left;
			width: 30%;
		}
		#content[role=main] .content .body hr {
			margin-left: 8px;
			margin-right: 8px;
		}

		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}
		.aside.member .menu li {
			margin: 8px 0;
		}
		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		</style>
		<style type="text/css">
		.product-info {
			border: 1px solid #BCBCBC;
			width: 980px;
			padding: 12px 10px;
		}

		.ex_product_image {
			float: left;
			margin-top: 10px;
		}

		.ex_product_image .product-category-rt {
			position: absolute;
			left: 0px;
			top: 0px;
		}

		.ex_product_image .product-category {
			float: left;
			width: 43px;
			height: 25px;
			line-height: 22px;
			font-size: 12px;
			text-align: center;
			color: white;
			margin-right: 2px;
			background-repeat: no-repeat;
		}

		.ex_product_image .image {
			position: relative;
			border: 1px solid #A5A5A5;
		}

		.ex_product_image .thumbnail {
			display: block;
			margin: 0 auto;
			width: 305px;
			height: 286px;
		}

		.ex_product_image .product-category.bonus {
			background-image: url('<?php echo $this->config->path_image; ?>/t_bonus.png');
		}
		.ex_product_image .product-category.store {
			background-image: url('<?php echo $this->config->path_image; ?>/t_store.png');
		}
		.ex_product_image .product-category.pcid-1 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-1.png');
		}
		.ex_product_image .product-category.pcid-2 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-2.png');
		}
		.ex_product_image .product-category.pcid-3 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-3.png');
		}

		.ex_product_image .banner img {
			float: left;
			width: 232px;
			height: 100px;
			display: block;
			border: 0;
			margin-top: 8px;
		}

		.ex_product_info {
			float:right; /*float: left;*/
			width: 720px;
			/*border: 1px solid #BCBCBC;*/
			margin: 5px 3px;
		}
		
		.ex_product_info h1 {
			font-size: 20px;
			line-height: 30px;
			width: 456px;
			padding: 3px 1px;
			margin: 10px 3px;
			text-align: center;
		}

		.ex_product_info .name {
			text-decoration: none;
			color: #6D6D6D;
		}

		.ex_product_info .info {
			list-style: none;
			margin: 12px 40px;
			color: #6D6D6D;
		}

		.ex_product_info .info li {
			margin: 14px 0;
		}
		
		.ex_product_info .button {
			height: 62px;
			line-height: 62px;
			margin: 20px 55px;
		}

		.ex_product_info .button img {
			border: 0;
			width: 110px;
			height: 62px;
			vertical-align: middle;
		}

		.ex_product_info .button input {
			height: 26px;
			line-height: 26px;
			font-size: 12px;
			padding: 6px;
			color: #8C8C8C;
			vertical-align: middle;
			border: 1px solid #CDCDCD
		}

		.ex_product_info .button #price {
			width: 174px;
		}

		.ex_product_info .button #price-start {
			width: 68px;
		}

		.ex_product_info .button #price-stop {
			width: 68px;
		}
		</style>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
		<aside class="aside member">
			<h2>兑换商城列表</h2>
			<section class="product-category-list">
				<ul class="menu">
					<?php foreach($this->tplVar['table']['rt']['exchange_product_category'] as $pck1 => $pcv1) : ?>
					<?php if ($pcv1['layer'] == 1) : ?>
					<li>
						<?php echo $pck1?"<hr>":""; ?>
						<h3>
							<a href="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $pcv1['epcid']; ?>"><?php echo $pcv1['name']; ?></a>
						</h3>
						<div class="clear"></div>
						<div class="sub-category">
						<?php foreach($this->tplVar['table']['rt']['exchange_product_category'] as $pck2 => $pcv2) : ?>
							<?php if ($pcv2['layer'] == 2 && $pcv2['node'] == $pcv1['epcid']) : ?>
							<h4><a href="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $pcv2['epcid']; ?>"><?php echo $pcv2['name']; ?></a></h4>
							<?php endif; ?>
						<?php endforeach; ?>								
							<div class="clear"></div>
						</div>
					</li>
					<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</section>
			
			<nav>
				<ul>
				<li>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote1.jpg" width="198"></a>
				</li>
				<li>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote2.jpg" width="198"></a>
				</li>
				</ul>
			</nav>
			
		</aside>
			
		<div class="content">
		<section class="block-frame">
		<h2>兑换商品</h2>
		<div class="body">
			
			<div style="text-align: center; width: 774px; height: 50px; float:left; ">
				<div style="padding:12px 20px;">
				<span style="padding-left:10px;">确认兑换商品数据</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px; color:#FF00FF;">填写收件人数据</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px;">确认收件人数据</span>
				<span style="padding-left:10px;">→</span>
				<span style="padding-left:10px;">购买订单完成</span>
				</div>
			</div>
			
			<div style="text-align: left; width: 768px; height: 30px; border-top:1px solid #A5A5A5; border-bottom:1px solid #A5A5A5; float:left; ">
				<div style="padding:5px 40px;">购买明细</div>
			</div>
			<div style="text-align: left; width: 774px; float:left; ">
				<div style="padding:5px 30px; width: 700px; ">
				<table border="0">
					<tr style="background-color: #c9c9c9;">
						<td style="text-align: center; width: 350px;">商品名称</td>
						<td style="text-align: center; width: 50px;">数量</td>
						<td style="text-align: center; width: 150px;">单价(红利点数)</td>
						<td style="text-align: center; width: 150px;">兑换点数</td>
					</tr>
					<tr style="border: 1px solid #A5A5A5;">
						<td style="text-align: center; width: 350px;"><a href="#" class="name ellipsis"><?php echo $this->tplVar['table']['record'][0]['name']; ?></a></td>
						<td style="text-align: right; width: 50px; color:#FF6666;"><?php echo $this->tplVar['table']['record'][0]['num']; ?></td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?></td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_point']); ?></td>
					</tr>
				</table>
				<table border="0" style="background-color: #c9c9c9;">
					<tr>
						<td style="text-align: right; width: 550px; color:#FF6666;">本次兑换</td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['used_point']); ?> 点</td>
					</tr>
					<tr>
						<td style="text-align: right; width: 550px; color:#FF6666;">处理费</td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['process_fee']); ?> 点</td>
					</tr>
					<tr>
						<td style="text-align: right; width: 550px; color:#FF6666;">总计</td>
						<td style="text-align: right; width: 150px;"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['total_fee']); ?> 点</td>
					</tr>
				</table>
				</div>
				<div class="button" style="padding:5px 80px;"><input type="button" value="< 继续兑换" class="submit" onclick="window.location.href='<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=1'" ></div>
			</div>
				
			<div style="text-align: left; width: 768px; height: 30px; border-top:1px solid #A5A5A5; border-bottom:1px solid #A5A5A5; float:right; ">
				<div style="padding:5px 40px;">收件人寄送资料</div>
			</div>
				
			<div class="ex_product_info">
				<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/exchange_mall/confirm_update/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $this->io->input["get"]["epcid"]; ?>&epid=<?php echo $this->io->input["get"]["epid"]; ?>" enctype="multipart/form-data" >
				<ul class="info">
					<li>
						<table border="0" style="text-align: left; width: 600px;">
							<tr>
								<td style="width: 150px;">姓名:</td>
								<td style="width: 450px;">
									<input type="text" id="name" name="name" value="<?php echo $this->tplVar['table']['rt']['user_profile'][0]['name']; ?>" /> 
									<input type="radio" id="male" name="gender" value="male" <?php echo ($this->tplVar['table']['rt']['user_profile'][0]['gender'] != 'female') ? 'checked' : ''; ?> />先生 
									<input type="radio" id="female" name="gender" value="female" <?php echo ($this->tplVar['table']['rt']['user_profile'][0]['gender']=='female') ? 'checked' : ''; ?> />小姐
								</td>
							</tr>
							<tr>
								<td style="width: 150px;">手机:</td>
								<td style="width: 450px;"><input type="text" id="phone" name="phone" value="<?php echo $this->tplVar['table']['rt']['user_profile'][0]['phone']; ?>" /></td>
							</tr>
							<tr>
								<td style="width: 150px;">地址:</td>
								<td style="width: 450px;">
									<input type="text" name="zip" size="6" value="<?php echo $this->tplVar['table']['rt']['user_profile'][0]['zip']; ?>" />
									<input type="text" name="address" size="60" value="<?php echo $this->tplVar['table']['rt']['user_profile'][0]['address']; ?>" />
								</td>
							</tr>
							<tr>
								<td style="width: 150px;">订单备注:</td>
								<td style="width: 450px;"><textarea name="memo" id="memo"><?php echo $this->tplVar['table']['record'][0]['memo']; ?></textarea></td>
							</tr>
						</table>
					</li>
				</ul>
				<div class="button" style="text-align: right;">
					<input type="hidden" name="orderid" value="<?php echo $this->io->input["get"]["orderid"] ;?>">
					<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['location_url']; ?>">
					<input type="submit" value="填写完成" class="submit">
				</div>
				</form>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFCCCC; text-align: left; width: 770px; height: 28px; float:right; ">
			<span style="padding-left:20px;">注意事项</span>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFF; text-align: left; width: 770px; float:right; ">
				<div style="padding-left:8px;">
					<p>1. 本图仅提供参考，实际情况以实体商品为准。</p>
					<p>2. 若商品因卖家、厂商或经销商的关系有缺货的情况，我们会发送站内讯息通知您，若您不想等待，可选择兑换等值商品(限一样)或等值点点币。</p>
					<p>3. 若您对本网站规则及说明有不明白之处，请见秒杀团购教学，或联络客服人员。</p>
					<p>4. 本图及说明为yahoo购物中心所提供。查看更多商品请至Yahoo购物中心。</p>
				</div>
			</div>
			
		</div>
		</section>

		</div>
		
		<div class="clear"></div>
		<div id="show_ajax" style="display:none"></div>
		</main>
		
		<?php require_once $this->tplVar['block']['footer']; ?>
	
	</body>
</html>