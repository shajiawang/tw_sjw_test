<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			float: left;
			width: 792px;
			border: 1px solid #C1C1C1;
		}
		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #BB175D;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}
		#content[role=main] .content .body {
			border-top: 2px solid #BB175D;
			padding: 10px 8px;
		}
		#content[role=main] .content .body table {
			/*border: 1px solid #E4E4E4;*/
			width: 100%;
		}
		#content[role=main] .content .body table th {
			color: #878787;
			font-size: 1em;
			text-align: left;
			padding: 13px 10px;
			width: 20%;
		}
		#content[role=main] .content .body table td {
			text-align: left;
			width: 30%;
		}
		#content[role=main] .content .body hr {
			margin-left: 8px;
			margin-right: 8px;
		}

		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}
		.aside.member .menu li {
			margin: 8px 0;
		}
		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		.mall_product .thumbnail {
			display: block;
			margin: 0 auto;
			max-height: 144px;
			max-width: 235px;
		}
		</style>
		<script type="text/javascript">
		$(function(){
			var article = $('.product-list .product:eq(0)');
			for(var i=0 ; i<10 ; i++) {
				//$('.product-list').append(article.clone());
			}

			var articlePerRow = 3;
			$('.product-list .product').each(function(idx, elm){
				if (idx%articlePerRow + 1 == articlePerRow) {
					$(this).addClass('product-last');
				}
			});
		});
		</script>
		
		<script type="text/javascript">
		//商品分類
		function cat_select(sel) {
			var id=sel.value;
			window.location="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid="+id;
		}
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		
		<main role="main" id="content">
		<aside class="aside member">
			<h2>兑换商城列表</h2>
			<section class="product-category-list">
				<ul class="menu">
					<?php foreach($this->tplVar['table']['rt']['exchange_product_category'] as $pck1 => $pcv1) : ?>
					<?php if ($pcv1['layer'] == 1) : ?>
					<li>
						<?php echo $pck1?"<hr>":""; ?>
						<h3>
							<a href="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $pcv1['epcid']; ?>"><?php echo $pcv1['name']; ?></a>
						</h3>
						<div class="clear"></div>
						<div class="sub-category">
						<?php foreach($this->tplVar['table']['rt']['exchange_product_category'] as $pck2 => $pcv2) : ?>
							<?php if ($pcv2['layer'] == 2 && $pcv2['node'] == $pcv1['epcid']) : ?>
							<h4><a href="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $pcv2['epcid']; ?>"><?php echo $pcv2['name']; ?></a></h4>
							<?php endif; ?>
						<?php endforeach; ?>								
							<div class="clear"></div>
						</div>
					</li>
					<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</section>
			
			<nav>
				<ul>
				<li>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote1.jpg" width="198"></a>
				</li>
				<li>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote2.jpg" width="198"></a>
				</li>
				</ul>
			</nav>
		</aside>
			
		<div class="content">
		<section class="block-frame">
		<h2>兑换商城</h2>
		<div class="body">
		
			<div class="product_list_sort" style="margin-top: 7px; width: 792px; height: 35px; float:left;">
				<span style="padding-left:20px;">商品分类 : 
					<select name="search_epcid" id="search_epcid" onChange="cat_select(this);"><option value="">不限</option>
					<?php foreach ($this->tplVar['table']['rt']['exchange_product_category'] as $pck => $pcv) : ?>
					<?php if ($pcv['layer'] == 1) : 
					$epcid = isset($this->io->input["get"]["epcid"]) ? $this->io->input["get"]["epcid"] : '';
					?>
						<option value="<?php echo $pcv['epcid']; ?>" <?php echo ($pcv['epcid']==$epcid) ? 'selected' : '';?> ><?php echo $pcv['name']; ?></option>
					<?php endif ?>
					<?php endforeach; ?>
					</select>
				</span>
			</div>
			
			<div class="mall_promote" style="text-align: center; width: 778px; height: auto; float:left;">
				<div class="product-list" style="width: 770px; float:right;">
					<?php foreach($this->tplVar['table']["rt"]["product_promote_rt"] as $rk => $rv) : ?>
					<article class="mall_product" style="text-align: center; position: relative; width: 252px; height: 250px; border: 1px solid #A5A5A5; float:left;">
						<div style="height:30px; background-color: #FF99FF;">精选特卖</div>
						
						<a class="name ellipsis" href="<?php echo $this->config->default_main; ?>/exchange_mall/product/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $rv['epcid']; ?>&epid=<?php echo $rv['epid']; ?>">
					<?php if (!empty($rv['filename'])) : ?>
						<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['filename']; ?>"/>
					<?php elseif (!empty($rv['thumbnail_url'])) : ?>
						<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>"/>
					<?php else : ?>
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
					<?php endif; ?>
						</a>
						
						<header><h3>
						<a class="name ellipsis" href="<?php echo $this->config->default_main; ?>/exchange_mall/product/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $rv['epcid']; ?>&epid=<?php echo $rv['epid']; ?>"><?php echo $rv['name']; ?></a>
						</h3></header>
						
						<div class="product-price-1">市价: <?php echo sprintf("%0.2f", $rv['retail_price']); ?> 元</div>
						<div class="product-price-2" style="color:#ff0000;">点数: <?php echo sprintf("%0.2f", $rv['point_price']); ?> 点</div>
					</article>
					<?php endforeach; ?>
				</div>
				<?php /*
				<div>
					<article class="mall_product" style=" width: 252px; height: 260px; border: 1px solid #A5A5A5; float:left;">
						<div style="background-color: #FF99FF;">精选特卖</div>
						
						<header>
						<h3><a class="name ellipsis" href="#">精选特卖商品</a></h3>
						</header>
						
						<a class="name ellipsis" href="#">
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
						</a>
						
						<div class="product-price-1">市价: 1234 元</div>
						<div class="product-price-2" style="color:#ff0000;">点数: 1111 点</div>
					</article>
					<article class="mall_product" style=" width: 252px; height: 260px; border: 1px solid #A5A5A5; float:left;">
						<div style="background-color: #FF99FF;">精选特卖</div>
						
						<header>
						<h3><a class="name ellipsis" href="#">精选特卖商品</a></h3>
						</header>
						
						<a class="name ellipsis" href="#">
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
						</a>
						
						<div class="product-price-1">市价: 1234 元</div>
						<div class="product-price-2" style="color:#ff0000;">点数: 1111 点</div>
					</article>
					<article class="mall_product" style=" width: 252px; height: 260px; border: 1px solid #A5A5A5; float:left;">
						<div style="background-color: #FF99FF;">精选特卖</div>
						
						<header>
						<h3><a class="name ellipsis" href="#">精选特卖商品</a></h3>
						</header>
						
						<a class="name ellipsis" href="#">
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
						</a>
						
						<div class="product-price-1">市价: 1234 元</div>
						<div class="product-price-2" style="color:#ff0000;">点数: 1111 点</div>
					</article>
				</div>*/?>
				<?php /*
				<div>
					<article class="mall_product" style=" width: 252px; height: 260px; border: 1px solid #A5A5A5; float:left;">
						<div style="background-color: #FF99FF;">促销专区</div>
						
						<header>
						<h3><a class="name ellipsis" href="#">促销专区商品</a></h3>
						</header>
						
						<a class="name ellipsis" href="#">
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
						</a>
						
						<div class="product-price-1">市价: 1234 元</div>
						<div class="product-price-2" style="color:#ff0000;">点数: 1111 点</div>
					</article>
					<article class="mall_product" style=" width: 252px; height: 260px; border: 1px solid #A5A5A5; float:left;">
						<div style="background-color: #FF99FF;">促销专区</div>
						
						<header>
						<h3><a class="name ellipsis" href="#">促销专区商品</a></h3>
						</header>
						
						<a class="name ellipsis" href="#">
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
						</a>
						
						<div class="product-price-1">市价: 1234 元</div>
						<div class="product-price-2" style="color:#ff0000;">点数: 1111 点</div>
					</article>
					<article class="mall_product" style=" width: 252px; height: 260px; border: 1px solid #A5A5A5; float:left;">
						<div style="background-color: #FF99FF;">促销专区</div>
						
						<header>
						<h3><a class="name ellipsis" href="#">促销专区商品</a></h3>
						</header>
						
						<a class="name ellipsis" href="#">
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
						</a>
						
						<div class="product-price-1">市价: 1234 元</div>
						<div class="product-price-2" style="color:#ff0000;">点数: 1111 点</div>
					</article>
				</div>*/?>
			</div>
			
			<div class="product_list_sort" style="margin-top: 7px; background-color: #ddd; width: 770px; height: 25px; border: 1px solid #A5A5A5; float:left;">
				<span style="padding-left:10px;">排序</span>
				<span style="padding-left:10px;">优先推荐</span>
				<span style="padding-left:5px;">|</span>
				<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_point_price=desc"><span style="padding-left:10px;">价钱由高到低</span></a>
				<span style="padding-left:5px;">|</span>
				<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_point_price=asc"><span style="padding-left:10px;">价钱由低到高</span></a>
			</div>
			
			
			<div class="product-list" style="width: 770px; float:right;">
			<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<article class="mall_product" style="text-align: center; position: relative; width: 252px; height: 250px; border: 1px solid #A5A5A5; float:left;">
					<div class="product-category-rt"></div>
					<a class="name ellipsis" href="<?php echo $this->config->default_main; ?>/exchange_mall/product/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $rv['epcid']; ?>&epid=<?php echo $rv['epid']; ?>">
				<?php if (!empty($rv['thumbnail'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['thumbnail']; ?>"/>
				<?php elseif (!empty($rv['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg"/>
				<?php endif; ?>
					</a>
					
					<header>
					<h3><a class="name ellipsis" href="<?php echo $this->config->default_main; ?>/exchange_mall/product/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $rv['epcid']; ?>&epid=<?php echo $rv['epid']; ?>"><?php echo $rv['name']; ?></a></h3>
					</header>
					
					<div class="product-price-1">市价: <?php echo sprintf("%0.2f", $rv['retail_price']); ?> 元</div>
					<div class="product-price-2" style="color:#ff0000;">点数: <?php echo sprintf("%0.2f", $rv['point_price']); ?> 点</div>
				</article>
			<?php endforeach; ?>
			</div>
			
			<div class="page" style="float:right;">
				<?php include_once $this->tplVar["block"]["page"]; ?>
			</div>
		
		</div>
		</section>
		</div>
		
		<div class="clear"></div>
		<div id="show_ajax" style="display:none"></div>
		</main>
		
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>