<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			float: left;
			width: 792px;
			border: 1px solid #C1C1C1;
		}
		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #BB175D;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}
		#content[role=main] .content .body {
			border-top: 2px solid #BB175D;
			padding: 10px 8px;
		}
		#content[role=main] .content .body table {
			border: 1px solid #E4E4E4;
			width: 100%;
		}
		#content[role=main] .content .body table th {
			color: #878787;
			font-size: 1em;
			text-align: left;
			padding: 13px 10px;
			width: 20%;
		}
		#content[role=main] .content .body table td {
			text-align: left;
			width: 30%;
		}
		#content[role=main] .content .body hr {
			margin-left: 8px;
			margin-right: 8px;
		}

		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}
		.aside.member .menu li {
			margin: 8px 0;
		}
		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		</style>
		<style type="text/css">
		.product-info {
			border: 1px solid #BCBCBC;
			width: 980px;
			padding: 12px 10px;
		}

		.ex_product_image {
			float: left;
			margin-top: 10px;
		}

		.ex_product_image .product-category-rt {
			position: absolute;
			left: 0px;
			top: 0px;
		}

		.ex_product_image .product-category {
			float: left;
			width: 43px;
			height: 25px;
			line-height: 22px;
			font-size: 12px;
			text-align: center;
			color: white;
			margin-right: 2px;
			background-repeat: no-repeat;
		}

		.ex_product_image .image {
			position: relative;
			border: 1px solid #A5A5A5;
		}

		.ex_product_image .thumbnail {
			display: block;
			margin: 0 auto;
			width: 305px;
			height: 286px;
		}

		.ex_product_image .product-category.bonus {
			background-image: url('<?php echo $this->config->path_image; ?>/t_bonus.png');
		}
		.ex_product_image .product-category.store {
			background-image: url('<?php echo $this->config->path_image; ?>/t_store.png');
		}
		.ex_product_image .product-category.pcid-1 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-1.png');
		}
		.ex_product_image .product-category.pcid-2 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-2.png');
		}
		.ex_product_image .product-category.pcid-3 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-3.png');
		}

		.ex_product_image .banner img {
			float: left;
			width: 232px;
			height: 100px;
			display: block;
			border: 0;
			margin-top: 8px;
		}

		.ex_product_info {
			float: left;
			width: 460px;
			/*border: 1px solid #BCBCBC;*/
			margin: 10px 3px;
		}
		
		.ex_product_info h1 {
			font-size: 20px;
			line-height: 30px;
			width: 456px;
			padding: 3px 1px;
			margin: 10px 3px;
			text-align: center;
		}

		.ex_product_info .name {
			text-decoration: none;
			color: #6D6D6D;
		}

		.ex_product_info .info {
			list-style: none;
			margin: 20px 55px;
			color: #6D6D6D;
		}

		.ex_product_info .info li {
			margin: 14px 0;
		}
		
		.ex_product_info .button {
			height: 62px;
			line-height: 62px;
			margin: 20px 55px;
		}

		.ex_product_info .button img {
			border: 0;
			width: 110px;
			height: 62px;
			vertical-align: middle;
		}

		.ex_product_info .button input {
			height: 26px;
			line-height: 26px;
			font-size: 12px;
			padding: 6px;
			color: #8C8C8C;
			vertical-align: middle;
			border: 1px solid #CDCDCD
		}

		.ex_product_info .button #price {
			width: 174px;
		}

		.ex_product_info .button #price-start {
			width: 68px;
		}

		.ex_product_info .button #price-stop {
			width: 68px;
		}
		</style>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
		<aside class="aside member">
			<h2>兑换商城列表</h2>
			<section class="product-category-list">
				<ul class="menu">
					<?php foreach($this->tplVar['table']['rt']['exchange_product_category'] as $pck1 => $pcv1) : ?>
					<?php if ($pcv1['layer'] == 1) : ?>
					<li>
						<?php echo $pck1?"<hr>":""; ?>
						<h3>
							<a href="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $pcv1['epcid']; ?>"><?php echo $pcv1['name']; ?></a>
						</h3>
						<div class="clear"></div>
						<div class="sub-category">
						<?php foreach($this->tplVar['table']['rt']['exchange_product_category'] as $pck2 => $pcv2) : ?>
							<?php if ($pcv2['layer'] == 2 && $pcv2['node'] == $pcv1['epcid']) : ?>
							<h4><a href="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $pcv2['epcid']; ?>"><?php echo $pcv2['name']; ?></a></h4>
							<?php endif; ?>
						<?php endforeach; ?>								
							<div class="clear"></div>
						</div>
					</li>
					<?php endif; ?>
					<?php endforeach; ?>
				</ul>
			</section>
			
			<nav>
				<ul>
				<li>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote1.jpg" width="198"></a>
				</li>
				<li>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote2.jpg" width="198"></a>
				</li>
				</ul>
			</nav>
			
		</aside>
			
		<div class="content">
		<section class="block-frame">
		<h2>兑换商品</h2>
		<div class="body">
			
			<article class="ex_product_image">
				<div class="image">
				<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
				<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image; ?>/product/bf8a5804cad98a6d8039cc6c20447c95.jpg"/>
				<?php endif; ?>
				</div>
				<div class="clear"></div>
			</article>
				
			<div class="ex_product_info">
				<header>
					<h1>
						<a href="#<?php //echo $this->config->default_main.'/saja/?'.$this->tplVar['status']['args'].'&productid='.$this->tplVar['table']['record'][0]['productid']; ?>" class="name ellipsis">
							<?php echo $this->tplVar['table']['record'][0]['name']; ?>
						</a>
					</h1>
					<p style="text-align: center;">********************************************************</p>
				</header>
				
				<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/exchange_mall/checkout/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=<?php echo $this->io->input["get"]["epcid"]; ?>&epid=<?php echo $this->io->input["get"]["epid"]; ?>" enctype="multipart/form-data" >
				<ul class="info">
					<li>
						<span>市售价格：</span>
						<span><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['retail_price']); ?>元</span>
					</li>
					<li>
						<span>红利点数：</span>
						<span><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['point_price']); ?>点</span>
					</li>
					<li>
						<span>处理费用：</span>
						<span><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['process_fee']); ?>点</span>
					</li>
					<li>
						<span>商品数量：</span>
						<?php if(!empty($this->tplVar['table']['rt']['stock']) ) : ?>
						<select name="num" >
						<?php for ($i=1; $i <= $this->tplVar['table']['rt']['stock']; $i++) : ?>
							<option value="<?php echo $i; ?>" ><?php echo $i; ?></option>
						<?php endfor; ?>
						</select>
						<?php endif ?>

					</li>
				</ul>
				<div class="button">
					<input type="hidden" name="checkout" value="Y">
					<input type="hidden" name="esid" value="<?php echo $this->tplVar['table']['record'][0]["esid"] ;?>">
					<span>付款方式：</span>
					<?php if(!empty($this->tplVar['table']['rt']['stock']) ) : ?>
					<input type="submit" value="使用红利点数" class="submit">
					<?php else : ?>
					<input type="button" value="库存不足" class="submit" disabled >
					<?php endif ?>

				</div>
				</form>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFCCCC; text-align: left; width: 770px; height: 28px; float:right; ">
			<span style="padding-left:20px;">商品介绍</span>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFF; text-align: left; width: 770px; float:right; ">
			<div style="padding-left:8px;"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></div>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFCCCC; text-align: left; width: 770px; height: 28px; float:right; ">
			<span style="padding-left:20px;">注意事项</span>
			</div>
			
			<div style="margin: 6px 1px; background-color: #FFF; text-align: left; width: 770px; float:right; ">
				<div style="padding-left:8px;">
					<p>1. 本图仅提供参考，实际情况以实体商品为准。</p>
					<p>2. 若商品因卖家、厂商或经销商的关系有缺货的情况，我们会发送站内讯息通知您，若您不想等待，可选择兑换等值商品(限一样)或等值点点币。</p>
					<p>3. 若您对本网站规则及说明有不明白之处，请见秒杀团购教学，或联络客服人员。</p>
					<p>4. 本图及说明为yahoo购物中心所提供。查看更多商品请至Yahoo购物中心。</p>
					<p>5.本商品寄出不含赠品。</p>
				</div>
			</div>
		
		</div>
		</section>

		</div>
		
		<div class="clear"></div>
		<div id="show_ajax" style="display:none"></div>
		</main>
		
		<?php require_once $this->tplVar['block']['footer']; ?>
	
	</body>
</html>