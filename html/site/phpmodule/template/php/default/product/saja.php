<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		.product-info {
			margin: 20px auto;
			border: 1px solid #BCBCBC;
			width: 898px;
			padding: 22px 20px;
		}

		.saja-product {
			float: left;

		}

		.saja-product .product-category-rt {
			position: absolute;
			left: 0px;
			top: 0px;
		}

		.saja-product .product-category {
			float: left;
			width: 43px;
			height: 25px;
			line-height: 22px;
			font-size: 12px;
			text-align: center;
			color: white;
			margin-right: 2px;
			background-repeat: no-repeat;
		}

		.saja-product .image {
			position: relative;
			border: 1px solid #A5A5A5;
		}

		.saja-product .thumbnail {
			display: block;
			margin: 0 auto;
			max-width: 471px;
			max-height: 286px;
		}

		.saja-product h1 {
			font-size: 20px;
			text-align: left;
			line-height: 30px;
			width: 467px;
			padding: 0 2px;
			margin-bottom: 10px;
		}

		.saja-product .name {
			text-decoration: none;
			color: #6D6D6D;
		}

		.saja-product .product-category.bonus {
			background-image: url('<?php echo $this->config->path_image; ?>/t_bonus.png');
		}
		.saja-product .product-category.store {
			background-image: url('<?php echo $this->config->path_image; ?>/t_store.png');
		}
		.saja-product .product-category.pcid-1 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-1.png');
		}
		.saja-product .product-category.pcid-2 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-2.png');
		}
		.saja-product .product-category.pcid-3 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-3.png');
		}

		.saja-product .banner img {
			float: left;
			width: 232px;
			height: 100px;
			display: block;
			border: 0;
			margin-top: 8px;
		}

		.saja-info {
			float: left;
			width: 415px;
			border: 1px solid #BCBCBC;
			margin-left: 8px;
		}

		.saja-info .info {
			list-style: none;
			margin-top: 74px;
			margin-left: 76px;
			color: #6D6D6D;
		}

		.saja-info .info li {
			margin: 14px 0;
		}

		.saja-info .offtime {
			width: 334px;
			margin-left: 66px;
			color: #A6A6A6;
			letter-spacing: 4px;
		}

		.saja-info .offtime .timer-digit {
			font-size: 38px;
		}

		.saja-info .offtime .timer-unit {
			font-size: 14px;
		}

		.saja-info .button {
			height: 62px;
			line-height: 62px;
			margin-left: 74px;
		}

		.saja-info .button img {
			border: 0;
			width: 110px;
			height: 62px;
			vertical-align: middle;
		}

		.saja-info .button input {
			height: 26px;
			line-height: 26px;
			font-size: 12px;
			padding: 6px;
			color: #8C8C8C;
			vertical-align: middle;
			border: 1px solid #CDCDCD
		}

		.saja-info .button #price {
			width: 174px;
		}

		.saja-info .button #price-start {
			width: 68px;
		}

		.saja-info .button #price-stop {
			width: 68px;
		}

		.message {
			width: 300px;
			background-color: #FFFFFF;
			border: 1px solid #AAAAAA;
			border-radius: 4px;
			display: none;
		}

		.message header {
			padding: 4px 0;
			background-color: #CCCCCC;
			margin: 3px;
			border-radius: 4px;
			position: relative;
		}

		.message header h2 {
			font-weight: bold;
			font-size: 16px;
			margin: 0 12px;
			line-height: 20px;
		}

		.message header .button-close {
			text-decoration: none;
			color: black;
			float: right;
			margin-right: -6px;
		}

		.message .entry {
			padding: 3px 0;
		}

		.message .entry p {
			margin: 0 6px;
			padding: 10px 0;
		}

		.message.center {
		  position: fixed;
		  left: 50%;
		  top: 50%;
		  transform: translate(-50%, -50%);
		  -webkit-transform: translate(-50%, -50%);
		  -moz-transform: translate(-50%, -50%);
		}
		</style>
		
		<script type="text/javascript">
		$.fn.displayCountDown = function() {
			var server_time = window.server_time;
			if (!server_time) {
				return this;
			}

			return this.each(function(){
				var $this    = $(this),
					offtime  = parseInt($this.data('offtime')),
					resttime = offtime - server_time,
					sec   = [resttime % 60],
					min   = [Math.floor(resttime % 3600 / 60)],
					hr    = [Math.floor(resttime % 86400 / 3600)],
					day   = [Math.floor(resttime / 86400)];

				if (offtime == 0) {
					return false;
				} 
				else if (resttime <= 0) {
					//window.document.location.reload();
				}
				else {
					sec[0] < 10 && sec.unshift('0');
					min[0] < 10 && min.unshift('0');
					hr[0]  < 10 && hr.unshift('0');
					day[0] < 10 && day.unshift('0');

	    			$this.find('.timer-digit.sec').text(sec.join(''));
	    			$this.find('.timer-digit.min').text(min.join(''));
	    			$this.find('.timer-digit.hr').text(hr.join(''));
	    			$this.find('.timer-digit.day').text(day.join(''));
				}
			});
		}

		$.get('<?php echo $this->config->default_main; ?>/ping/view', function(server_time){
			console && console.log(server_time);
			
			if (isNaN(server_time) || parseInt(server_time) <= 0) {
				alert('系统时间错误!');
				return;
			}

			window.server_time = parseInt(server_time);

			$('.offtime').displayCountDown();

			window.setInterval(function(){
				window.server_time++;
				$('.offtime').displayCountDown();
			}, 1000);
		});

		$(function(){
			$('.offtime').displayCountDown();
		});
		</script>
		<script type="text/javascript">
		$(function(){
			// var article = $('.product-list .product:eq(0)');
			// for(var i=0 ; i<10 ; i++) {
			// 	$('.product-list').append(article.clone());
			// }

			var articlePerRow = 3;
			$('.product-list .product').each(function(idx, elm){
				if (idx%articlePerRow + 1 == articlePerRow) {
					$(this).addClass('product-last');
				}
			});

			$('#msg-close').on('click', function(){
				$('#msg-dialog').hide();
			});

			$('#saja-single').on('click', function(){
				$.post(
					'<?php echo $this->config->default_main; ?>/product/insert',
					{
						type: "single",
						productid: '<?php echo $this->tplVar['table']['record'][0]['productid']; ?>',
						price: $('#price').val(),
						use_sgift: $('input:hidden[name=use_sgift], input:radio:checked[name=use_sgift]').val()
					},
					function(str_json){
						if (str_json) {
							var json = $.parseJSON(str_json);
							console && console.log($.parseJSON(str_json));
							if (json.rank) {
								$('#msg').text('本次下标顺位第' + json.rank + '名');
							} else {
								$('#msg').text(json.msg);
							}
							$('#msg-dialog').show();
							$('#winner').val(json.winner || '无');
						}
					}
				);
			});

			$('#saja-range').on('click', function(){
				$.post(
					'<?php echo $this->config->default_main; ?>/product/insert',
					{
						type: "range",
						productid: '<?php echo $this->tplVar['table']['record'][0]['productid']; ?>',
						price_start: $('#price-start').val(),
						price_stop: $('#price-stop').val(),
						use_sgift: $('input:hidden[name=use_sgift], input:radio:checked[name=use_sgift]').val()
					},
					function(str_json){
						if (str_json) {
							var json = $.parseJSON(str_json);
							console && console.log($.parseJSON(str_json));
							$('#msg').text(json.msg);
							$('#msg-dialog').show();
							$('#winner').val(json.winner || '无');
						}
					}
				);
			});
		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		
		<main role="main" id="content">
			<div class="product-info">
				<article class="saja-product">
					<header>
						<h1>
							<a href="<?php echo $this->config->default_main.'/saja/?'.$this->tplVar['status']['args'].'&productid='.$this->tplVar['table']['record'][0]['productid']; ?>" class="name ellipsis">
								<?php echo $this->tplVar['table']['record'][0]['name']; ?>
							</a>
						</h1>
					</header>
					<div class="image">
					<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
						<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
					<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
						<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
					<?php else : ?>
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
					<?php endif; ?>
						
					<div class="product-category-rt">
					
					<?php $bonus = floatval($this->tplVar['table']['record'][0]['bonus']); ?>
					<?php if (!empty($bonus)) : ?>
						<div class="product-category bonus">
							<span class="product-category-name" title="红利回馈%">
								<?php if ($this->tplVar['table']['record'][0]['bonus_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $bonus); ?>%
								<?php elseif ($this->tplVar['table']['record'][0]['bonus_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $bonus); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
					<?php endif; ?>
					
					<?php /*$gift = floatval($this->tplVar['table']['record'][0]['gift']); ?>
					<?php if (!empty($gift)) : ?>
						<div class="product-category store">
							<span class="product-category-name" title="店点回馈%">
								<?php if ($this->tplVar['table']['record'][0]['gift_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $gift); ?>%
								<?php elseif ($this->tplVar['table']['record'][0]['gift_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $gift); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
					<?php endif; */?>
					
					<?php if (!empty($this->tplVar['table']['rt']['saja_rule']) ) {
					$saja_rule = $this->tplVar['table']['rt']['saja_rule'][0];
					if($saja_rule["srid"]=='any_saja'){
					} elseif($saja_rule["srid"]=='never_win_saja'){ ?>
						<div class="product-category pcid-1">
							<span class="product-category-name" title="新手限定(限中标次数 5 次以下)" >限新手</span>
						</div>
					<?php } elseif($saja_rule["srid"]=='le_win_saja'){ ?>
						<div class="product-category pcid-1">
							<span class="product-category-name" title="(限中标次数 <?php echo $saja_rule["value"];?> 次以下)" >限新手</span>
						</div>
					<?php } elseif($saja_rule["srid"]=='ge_win_saja'){ ?>
						<div class="product-category pcid-1">
							<span class="product-category-name" title="(限中标次数 <?php echo $saja_rule["value"];?> 次以上)" >限老手</span>
						</div>
					<?php 
					} }//endif; ?>
					
					<?php /*if ((float)$this->tplVar['table']['record'][0]['loc_type'] == 'G') : ?>
					<div class="product-category pcid-2">
					<span class="product-category-name" title="区域" >全国</span>
					</div>
					<?php endif; */?>
					
					<?php if ((float)$this->tplVar['table']['record'][0]['saja_fee'] == 0.00) : ?>
					<div class="product-category pcid-3">
						<span class="product-category-name" title="免费(出价免扣杀币)" >免费</span>
					</div>
					<?php endif; ?>

					</div>
					</div>
					
					<a href="#" class="banner">
						<img src="http://www.jgsons.com/image/paymentmethod.png"/>
					</a>
					<a href="#" class="banner">
						<img src="http://www.jgsons.com/image/paymentmethod.png"/>
					</a>
					<div class="clear"></div>
				</article>
				<div class="saja-info">
					<ul class="info">
						<li>
							<span>市价：</span>
							<span>RMB <?php echo number_format($this->tplVar['table']['record'][0]['retail_price'], 2); ?> 元</span>
						</li>
						<li>
							<span>底价：</span>
							<span>RMB <?php echo number_format($this->tplVar['table']['record'][0]['price_limit'], 2); ?> 元</span>
						</li>
						<li>
							<span>下标手续费：</span>
							<span>杀价币 <?php echo number_format($this->tplVar['table']['record'][0]['saja_fee'], 2); ?> 点</span>
						</li>
						<li>
							<span>中标处理费：</span>
							<span>杀价币 <?php echo number_format($this->tplVar['table']['record'][0]['process_fee'], 2); ?> 点</span>
						</li>
						<li>
							<span>目前得标：</span>
							<span id="winner">
						<?php if (!empty($this->tplVar['table']['rt']['history'][0]['nickname'])) : ?>
							<?php echo $this->tplVar['table']['rt']['history'][0]['nickname']; ?>
						<?php else : ?>
							无
						<?php endif; ?>
							</span>
						</li>
					
					<?php if ($this->tplVar['table']['record'][0]['offtime']) : ?>
						<li>
							<span>结标时间：</span>
							<span><?php echo date("Y-m-d H:i", $this->tplVar['table']['record'][0]['offtime']); ?></span>
						</li>
					<?php elseif (!empty($this->tplVar['table']['rt']['pay_get_product'])) : ?>
						<li>
							<span>结标时间：</span>
							<span><?php echo $this->tplVar['table']['rt']['pay_get_product'][0]['insertt']; ?></span>
						</li>
					<?php elseif ($this->tplVar['table']['record'][0]['closed'] == 'NB') : ?>
						<li>
							<span>结标时间：</span>
							<span><?php echo $this->tplVar['table']['record'][0]['modifyt']; ?></span>
						</li>
					<?php endif; ?>
					<?php if (!empty($this->tplVar['table']['record'][0]['user_limit'])) : ?>
						<li>
							<span>本档一人可下标<?php echo $this->tplVar['table']['record'][0]['user_limit']; ?>次</span>
						</li>

					<?php endif; ?>
					</ul>
					
				<?php if ($this->tplVar['table']['record'][0]['closed'] == 'N') : ?>
				<?php if ($this->tplVar['table']['record'][0]['offtime']) : ?>
					<div class="offtime" data-offtime="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>">
						<span class="timer-digit day">99</span>
						<span class="timer-unit day">天</span>
						<span class="timer-digit hr">23</span>
						<span class="timer-unit hr">时</span>
						<span class="timer-digit min">03</span>
						<span class="timer-unit min">分</span>
						<span class="timer-digit sec">01</span>
						<span class="timer-unit sec">秒</span>

					</div>
				<?php endif; ?>
				
					<?php //會員得標次數 $this->tplVar['table']['rt']['user_get_bid']?>
					<?php if (!empty($this->tplVar['table']['rt']['sgift'])) : ?>
					<div class="button">
						<input type="radio" name="use_sgift" value="N" checked/>使用杀币
						<input type="radio" name="use_sgift" value="Y"/>使用下标礼券
					</div>
					<?php else : ?>
					<input type="hidden" name="use_sgift" value="N"/>
					<?php endif; ?>
					<div class="button">
					
						<input type="text" id="price" placeholder="例如：5.03"/>
						<a id="saja-single" href="javascript:void(0);"><img src="<?php echo $this->config->path_image; ?>/b_saja_single.png"></a>
					</div>
					<div class="button">
						<input type="text" id="price-start" placeholder="例如：1.00"/>
						<span>～</span>
						<input type="text" id="price-stop" placeholder="例如：1.98"/>
						<a id="saja-range" href="javascript:void(0);"><img src="<?php echo $this->config->path_image; ?>/b_saja_range.png"></a>
					</div>
				<?php else : ?>
					<!--显示结标结果信息-->


				<?php endif; ?>
				</div>
				
				<div style="margin: 30px 30px 10px; background-color: #FFF; text-align:left; width:100%px; float:left; ">
					<div style="padding-left:8px;"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></div>
				</div>
				<div style="margin: 20px 30px 25px; background-color: #FFF; text-align:left; width:100%px; float:left; ">
					<div style="padding-left:8px;">
						<p>1. 本图仅提供参考，实际情况以实体商品为准。</p>
						<p>2. 若商品因卖家、厂商或经销商的关系有缺货的情况，我们会发送站内讯息通知您，若您不想等待，可选择兑换等值商品(限一样)或等值点点币。</p>
						<p>3. 若您对本网站规则及说明有不明白之处，请见秒杀团购教学，或联络客服人员。</p>
						<p>4. 本图及说明为yahoo购物中心所提供。查看更多商品请至Yahoo购物中心。</p>
						<p>5.本商品寄出不含赠品。</p>
					</div>
				</div>
				
				<div class="clear"></div>
			</div>
			
			<article class="message center" id="msg-dialog">
				<header>
					<h2>
						<span class="msg" id="msg-title">讯息</span>
						<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
					</h2>
				</header>
				<div class="entry">
					<p id="msg">Message content</p>
				</div>
			</article>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>