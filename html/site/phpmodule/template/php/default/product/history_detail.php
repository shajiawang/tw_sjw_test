<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		.product-info {
			margin: 20px auto;
			border: 1px solid #BCBCBC;
			width: 898px;
			padding: 22px 20px;
		}

		.saja-product {
			float: left;

		}

		.saja-product .product-category-rt {
			position: absolute;
			left: 0px;
			top: 0px;
		}

		.saja-product .product-category {
			float: left;
			width: 43px;
			height: 25px;
			line-height: 22px;
			font-size: 12px;
			text-align: center;
			color: white;
			margin-right: 2px;
			background-repeat: no-repeat;
		}

		.saja-product .image {
			position: relative;
			border: 1px solid #A5A5A5;
		}

		.saja-product .thumbnail {
			display: block;
			margin: 0 auto;
			max-width: 471px;
			max-height: 286px;
		}

		.saja-product h1 {
			font-size: 20px;
			text-align: left;
			line-height: 30px;
			width: 467px;
			padding: 0 2px;
			margin-bottom: 10px;
		}

		.saja-product .name {
			text-decoration: none;
			color: #6D6D6D;
		}

		.saja-product .product-category.bonus {
			background-image: url('<?php echo $this->config->path_image; ?>/t_bonus.png');
		}
		.saja-product .product-category.store {
			background-image: url('<?php echo $this->config->path_image; ?>/t_store.png');
		}
		.saja-product .product-category.pcid-1 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-1.png');
		}
		.saja-product .product-category.pcid-2 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-2.png');
		}
		.saja-product .product-category.pcid-3 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-3.png');
		}

		.saja-product .banner img {
			float: left;
			width: 232px;
			height: 100px;
			display: block;
			border: 0;
			margin-top: 8px;
		}

		.saja-info {
			float: left;
			width: 415px;
			border: 1px solid #BCBCBC;
			margin-left: 8px;
		}

		.saja-info .info {
			list-style: none;
			margin-top: 74px;
			margin-left: 76px;
			color: #6D6D6D;
		}

		.saja-info .info li {
			margin: 14px 0;
		}

		.saja-info .offtime {
			width: 334px;
			margin-left: 66px;
			color: #A6A6A6;
			letter-spacing: 4px;
		}

		.saja-info .offtime .timer-digit {
			font-size: 38px;
		}

		.saja-info .offtime .timer-unit {
			font-size: 14px;
		}

		.saja-info .button {
			height: 62px;
			line-height: 62px;
			margin-left: 74px;
		}

		.saja-info .button img {
			border: 0;
			width: 110px;
			height: 62px;
			vertical-align: middle;
		}

		.saja-info .button input {
			height: 26px;
			line-height: 26px;
			font-size: 12px;
			padding: 6px;
			color: #8C8C8C;
			vertical-align: middle;
			border: 1px solid #CDCDCD
		}

		.saja-info .button #price {
			width: 174px;
		}

		.saja-info .button #price-start {
			width: 68px;
		}

		.saja-info .button #price-stop {
			width: 68px;
		}

		.message {
			width: 300px;
			background-color: #FFFFFF;
			border: 1px solid #AAAAAA;
			border-radius: 4px;
			display: none;
		}

		.message header {
			padding: 4px 0;
			background-color: #CCCCCC;
			margin: 3px;
			border-radius: 4px;
			position: relative;
		}

		.message header h2 {
			font-weight: bold;
			font-size: 16px;
			margin: 0 12px;
			line-height: 20px;
		}

		.message header .button-close {
			text-decoration: none;
			color: black;
			float: right;
			margin-right: -6px;
		}

		.message .entry {
			padding: 3px 0;
		}

		.message .entry p {
			margin: 0 6px;
			padding: 10px 0;
		}

		.message.center {
		  position: fixed;
		  left: 50%;
		  top: 50%;
		  transform: translate(-50%, -50%);
		  -webkit-transform: translate(-50%, -50%);
		  -moz-transform: translate(-50%, -50%);
		}
		*/
		</style>
		<script type="text/javascript">
		$(function(){
			// var article = $('.product-list .product:eq(0)');
			// for(var i=0 ; i<10 ; i++) {
			// 	$('.product-list').append(article.clone());
			// }

			var articlePerRow = 3;
			$('.product-list .product').each(function(idx, elm){
				if (idx%articlePerRow + 1 == articlePerRow) {
					$(this).addClass('product-last');
				}
			});

			$('#msg-close').on('click', function(){
				$('#msg-dialog').hide();
			});

			$('#saja-single').on('click', function(){
				$.post(
					'<?php echo $this->config->default_main; ?>/product/insert',
					{
						type: "single",
						productid: '<?php echo $this->tplVar['table']['record'][0]['productid']; ?>',
						price: $('#price').val(),
						use_sgift: $('input:hidden[name=use_sgift], input:radio:checked[name=use_sgift]').val()
					},
					function(str_json){
						if (str_json) {
							var json = $.parseJSON(str_json);
							console && console.log($.parseJSON(str_json));
							if (json.rank) {
								$('#msg').text('本次下标顺位第' + json.rank + '名');
							} else {
								$('#msg').text(json.msg);
							}
							$('#msg-dialog').show();
							$('#winner').val(json.winner || '无');
						}
					}
				);
			});

			$('#saja-range').on('click', function(){
				$.post(
					'<?php echo $this->config->default_main; ?>/product/insert',
					{
						type: "range",
						productid: '<?php echo $this->tplVar['table']['record'][0]['productid']; ?>',
						price_start: $('#price-start').val(),
						price_stop: $('#price-stop').val(),
						use_sgift: $('input:hidden[name=use_sgift], input:radio:checked[name=use_sgift]').val()
					},
					function(str_json){
						if (str_json) {
							var json = $.parseJSON(str_json);
							console && console.log($.parseJSON(str_json));
							$('#msg').text(json.msg);
							$('#msg-dialog').show();
							$('#winner').val(json.winner || '无');
						}
					}
				);
			});
		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<div class="product-info">
				<article class="saja-product">
					<header>
						<h1>
							<?php /*<a href="<?php echo $this->config->default_main.'/saja/?'.$this->tplVar['status']['args'].'&productid='.$this->tplVar['table']['record'][0]['productid']; ?>" class="name ellipsis">*/?>
							商品名称：<?php echo $this->tplVar['table']['record'][0]['name']; ?>
						</h1>
					</header>
					
					<div style="position:absolute; width:392px; height:209px; ">
					<div style="position:absolute; left:1px; top:1px; z-index:2;">
					<img src="http://img.saja.com.tw/tp/div_templates_close_information_bidon.gif" />
					</div>
					
					<div class="image" style="width:392px; height:209px; overflow: hidden; ">
					<?php if (!empty($this->tplVar['table']['record'][0]['filename'])) : ?>
						<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$this->tplVar['table']['record'][0]['filename']; ?>"/>
					<?php elseif (!empty($this->tplVar['table']['record'][0]['thumbnail_url'])) : ?>
						<img class="thumbnail" src="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/>
					<?php else : ?>
						<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
					<?php endif; ?>
						<?php /*
						<div class="product-category-rt">
						<div class="product-category bonus">
							<span class="product-category-name">
								<?php if ($this->tplVar['table']['record'][0]['bonus_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $this->tplVar['table']['record'][0]['bonus']); ?>%
								<?php elseif ($this->tplVar['table']['record'][0]['bonus_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $this->tplVar['table']['record'][0]['bonus']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
						<div class="product-category store">
							<span class="product-category-name">
								<?php if ($this->tplVar['table']['record'][0]['gift_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $this->tplVar['table']['record'][0]['gift']); ?>%
								<?php elseif ($this->tplVar['table']['record'][0]['gift_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $this->tplVar['table']['record'][0]['gift']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
							<div class="product-category pcid-1"><span class="product-category-name">限新手</span></div>
							<div class="product-category pcid-2"><span class="product-category-name">全国</span></div>
							<div class="product-category pcid-3"><span class="product-category-name">免费</span></div>

						</div>*/?>
					</div></div>
					
					<?php /*<a href="#" class="banner">
						<img src="http://www.jgsons.com/image/paymentmethod.png"/>
					</a>
					<a href="#" class="banner">
						<img src="http://www.jgsons.com/image/paymentmethod.png"/>
					</a>*/?>
					<div class="clear"></div>
				</article>
				    
				<div class="saja-info">
					<ul class="info">
						<li>
							<span>商品编号：</span>
							<span><?php echo $this->tplVar['table']['record'][0]['productid']; ?></span>
						</li>
						<li>
							<span>中标杀友：</span>
							<span id="winner" style="color:#ff0000;">
							<?php if (!empty($this->tplVar['table']['record'][0]['nickname'])) : ?>
								<?php echo $this->tplVar['table']['record'][0]['nickname']; ?>
							<?php else : ?>
								无
							<?php endif; ?>
							</span>
						</li>
						
						<li>
							<span>市售价格：</span>
							<span>RMB <?php echo number_format($this->tplVar['table']['record'][0]['retail_price'], 2); ?>元</span>
						</li>
						<li>
							<span style="color:#ff0000;">中标金额：</span>
							<span style="color:#ff0000;">RMB <?php echo number_format($this->tplVar['table']['record'][0]['price'], 2); ?>元</span>
						</li>
						
					<?php if ($this->tplVar['table']['record'][0]['offtime']) : ?>
						<li>
							<span>中标时间：</span>
							<span><?php echo date("Y-m-d H:i", $this->tplVar['table']['record'][0]['offtime']); ?></span>
						</li>
					<?php elseif (!empty($this->tplVar['table']['rt']['pay_get_product'])) : ?>
						<li>
							<span>中标时间：</span>
							<span><?php echo $this->tplVar['table']['rt']['pay_get_product'][0]['insertt']; ?></span>
						</li>
					<?php elseif ($this->tplVar['table']['record'][0]['closed'] == 'NB') : ?>
						<li>
							<span>中标时间：</span>
							<span><?php echo $this->tplVar['table']['record'][0]['modifyt']; ?></span>
						</li>
					<?php endif; ?>
					</ul>
				</div>
				
				<div style="margin: 30px 30px 10px; background-color: #FFF; text-align:left; width:100%px; float:left; ">
					<div style="padding-left:8px;"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></div>
				</div>
				<div style="margin: 20px 30px 25px; background-color: #FFF; text-align:left; width:100%px; float:left; ">
					<div style="padding-left:8px;">
						<p>1. 本图仅提供参考，实际情况以实体商品为准。</p>
						<p>2. 若商品因卖家、厂商或经销商的关系有缺货的情况，我们会发送站内讯息通知您，若您不想等待，可选择兑换等值商品(限一样)或等值点点币。</p>
						<p>3. 若您对本网站规则及说明有不明白之处，请见秒杀团购教学，或联络客服人员。</p>
						<p>4. 本图及说明为yahoo购物中心所提供。查看更多商品请至Yahoo购物中心。</p>
						<p>5.本商品寄出不含赠品。</p>
					</div>
				</div>
				
				<div class="clear"></div>
			
			</div>
		</main>
	<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>