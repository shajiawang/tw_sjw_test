<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		.pay-get-product-list {
			list-style: none;
		}
		.pay-get-product {
			width: 848px;
			height: 196px;
			padding: 31px 0;
			border: 1px solid #C9C9C9;
			border-radius: 10px;
			margin: 12px auto;

			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #e5e5e5 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#e5e5e5));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#e5e5e5 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#e5e5e5 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e5e5e5',GradientType=0 );
		}
		.pay-get-product article {
		}
		.pay-get-product .product-info {
			padding: 16px 24px;
			text-align: right;
			margin-right: 18px;
			background-color: #FFFFFF;
			border: 1px solid #C9C9C9;
			width: 380px;
			height: 164px;
			float: right;
		}
		.pay-get-product .product-info .share-button {
			height: 30px;
		}
		.pay-get-product .product-info h3 {
			text-align: right;
			height: 40px;
			line-height: 40px;
		}
		.pay-get-product .product-info h3 a {
			color: #606060;
			font-weight: bold;
			text-decoration: none;
		}
		.pay-get-product .product-info time {
			font-size: 13px;
			height: 30px;
			line-height: 30px;
		}
		.pay-get-product .product-info .nickname {
			font-size: 15px;
			font-weight: bold;
			height: 30px;
			line-height: 30px;
		}
		.pay-get-product .product-info .nickname .red {
			font-size: 16px;
			color: #FF0000;
		}
		.pay-get-product .product-info .price {
			font-size: 15px;
			font-weight: bold;
			height: 30px;
			line-height: 30px;
		}
		.pay-get-product .product-info .price .red {
			font-size: 27px;
			color: #FF0000;
		}
		.pay-get-product .thumbnail {
			float: left;
			display: block;
		}
		.pay-get-product .thumbnail img {
			max-width: 367px;
			max-height: 196px;
			display: block;
			margin-left: 22px;
		}
		.pagination {
			font-size: 14px;
			line-height: 14px;
			margin: 10px 40px;
		}
		.pagination .total-page {
			margin-right: 10px;
		}
		.pagination .total-record {
			color: #6666CC;
		}
		.pagination .function {
			float: right;
		}
		.pagination .function a {
			text-decoration: none;
			color: #FF6A00;
		}
		.pagination .function .next-page {
			margin-right: 10px;
		}
		</style>
		<script type="text/javascript">
		$(function(){
			$('#change-page').on('change', function(){
				window.location.href = window.location.href.replace(/p=\d+/, 'p='+$(this).val());
			});
		});
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<div class="content">
				<ul class="pay-get-product-list">
				<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
					<li class="pay-get-product">
						<article>
							<a class="thumbnail" href="<?php echo $this->config->default_main.'/product/history_detail?productid='.$rv['productid']; ?>">
								<?php if (!empty($rv['filename'])) : ?>
									<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['filename']; ?>"/>
								<?php elseif (!empty($rv['thumbnail_url'])) : ?>
									<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>"/>
								<?php else : ?>
									<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
								<?php endif; ?>
							</a>
							<div class="product-info">
								<div class="share-button"></div>
								<header>
									<h3>
										商品名称：<a href="<?php echo $this->config->default_main.'/product/history_detail?productid='.$rv['productid']; ?>"><?php echo $rv['name']; ?></a>
									</h3>
								</header>
							<?php if (empty($rv['offtime'])) : ?>
								<time datetime="<?php echo $rv['insertt']; ?>">中标时间：<?php echo $rv['insertt']; ?></time>
							<?php else : ?>
								<time datetime="<?php echo date("Y-m-d H:i:s", $rv['offtime']); ?>"><?php echo date("Y-m-d H:i:s", $rv['offtime']); ?></time>
							<?php endif; ?>
								<div class="nickname">中标杀友：<span class="red"><?php echo $rv['nickname']; ?></span></div>
								<div class="price">中标金额：RMB<span class="red"><?php echo sprintf("%0.2f", $rv['price']); ?>元</span></div>
							</div>

							<div class="clear"></div>
						</article>
					</li>
				<?php endforeach; ?>
				</ul>
				
				<div class="pagination">
				<?php if ($this->tplVar['table']['record']) : ?>
					<?php /*<span class="total-page">第<?php echo $this->tplVar['page']['firstpage']; ?>页/共<?php echo $this->tplVar['page']['lastpage']; ?>页</span>
					<span class="total-record">共<?php echo $this->tplVar['page']['total']; ?>笔资料</span>*/?>
					<div class="function">
						<a class="prev-page" href="<?php echo $this->config->default_main.'/'. $this->io->input['get']['fun'].'/'.$this->io->input['get']['act'].'?p='.$this->tplVar['page']['previouspage']; ?>">上一页</a>
						&nbsp;/&nbsp;
						<a class="next-page" href="<?php echo $this->config->default_main.'/'. $this->io->input['get']['fun'].'/'.$this->io->input['get']['act'].'?p='.$this->tplVar['page']['nextpage']; ?>">下一页</a>
						至第
						<select id="change-page">
						<?php foreach($this->tplVar['page']['item'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['p']; ?>" <?php echo ($pv['p']==$this->io->input["get"]["p"]) ? 'selected':'';?> ><?php echo $pv['p']; ?></option>
						<?php endforeach; ?>
						</select>
						页
					</div>
				<?php endif; ?>
				</div>
				<div class="clear"></div>
			</div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>