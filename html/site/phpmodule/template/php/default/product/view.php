<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		.product .product-category.bonus {
			background-image: url('<?php echo $this->config->path_image; ?>/t_bonus.png');
		}
		.product .product-category.store {
			background-image: url('<?php echo $this->config->path_image; ?>/t_store.png');
		}
		.product .product-category.pcid-1 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-1.png');
		}
		.product .product-category.pcid-2 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-2.png');
		}
		.product .product-category.pcid-3 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-3.png');
		}
		</style>
		
		<style type="text/css">
		#content[role=main] .content {
			float: left;
			width: 792px;
			border: 1px solid #C1C1C1;
		}
		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #BB175D;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}
		#content[role=main] .content .body {
			border-top: 2px solid #BB175D;
			padding: 10px 8px;
		}
		#content[role=main] .content .body table {
			/*border: 1px solid #E4E4E4;*/
			width: 100%;
		}
		#content[role=main] .content .body table th {
			color: #878787;
			font-size: 1em;
			text-align: left;
			padding: 13px 10px;
			width: 20%;
		}
		#content[role=main] .content .body table td {
			text-align: left;
			width: 30%;
		}
		#content[role=main] .content .body hr {
			margin-left: 8px;
			margin-right: 8px;
		}

		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}
		.aside.member .menu li {
			margin: 8px 0;
		}
		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		.mall_product .thumbnail {
			display: block;
			margin: 0 auto;
			max-height: 144px;
			max-width: 235px;
		}
		</style>
		
		<script type="text/javascript">
		$.fn.displayCountDown = function() {
			var server_time = window.server_time;
			if (!server_time) {
				return this;
			}

			return this.each(function(){
				var $this    = $(this),
					offtime  = parseInt($this.data('offtime')),
					resttime = offtime - server_time,
					sec   = [resttime % 60],
					min   = [Math.floor(resttime % 3600 / 60)],
					hr    = [Math.floor(resttime % 86400 / 3600)],
					day   = [Math.floor(resttime / 86400)];

				if (offtime == 0) {
					return false;
				} 
				else if (resttime <= 0) {
					//window.document.location.reload();
				}
				else {
					sec[0] < 10 && sec.unshift('0');
					min[0] < 10 && min.unshift('0');
					hr[0]  < 10 && hr.unshift('0');
					day[0] < 10 && day.unshift('0');

	    			$this.find('.timer-digit.sec').text(sec.join(''));
	    			$this.find('.timer-digit.min').text(min.join(''));
	    			$this.find('.timer-digit.hr').text(hr.join(''));
	    			$this.find('.timer-digit.day').text(day.join(''));
				}
			});
		}

		$.get('<?php echo $this->config->default_main; ?>/ping/view', function(server_time){
			console && console.log(server_time);
			
			if (isNaN(server_time) || parseInt(server_time) <= 0) {
				alert('系统时间错误!');
				return;
			}

			window.server_time = parseInt(server_time);

			$('.offtime').displayCountDown();

			window.setInterval(function(){
				window.server_time++;
				$('.offtime').displayCountDown();
			}, 1000);
		});

		$(function(){
			$('.offtime').displayCountDown();
		});
		</script>
		<script type="text/javascript">
		$(function(){
			var article = $('.product-list .product:eq(0)');
			for(var i=0 ; i<10 ; i++) {
				//$('.product-list').append(article.clone());
			}

			var articlePerRow = 3;
			$('.product-list .product').each(function(idx, elm){
				if (idx%articlePerRow + 1 == articlePerRow) {
					$(this).addClass('product-last');
				}
			});
		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		
		<main role="main" id="content">
			<aside class="aside">
				<section class="product-category-list">
					<h2>杀价列表</h2>
					<ul class="menu">
					<?php foreach($this->tplVar['table']['rt']['product_category'] as $pck1 => $pcv1) : ?>
						<?php if ($pcv1['layer'] == 1) : ?>
						<li>
							<?php echo $pck1?"<hr>":""; ?>
							<h3>
								<a href="<?php echo $this->config->default_main; ?>/product?channelid=<?php echo $this->io->input['get']['channelid']; ?>&pcid=<?php echo $pcv1['pcid']; ?>"><?php echo $pcv1['name']; ?></a>
							</h3>
							<div class="clear"></div>
							<div class="sub-category">
							<?php foreach($this->tplVar['table']['rt']['product_category'] as $pck2 => $pcv2) : ?>
								<?php if ($pcv2['layer'] == 2 && $pcv2['node'] == $pcv1['pcid']) : ?>
								<h4><a href="<?php echo $this->config->default_main; ?>/product?channelid=<?php echo $this->io->input['get']['channelid']; ?>&pcid=<?php echo $pcv2['pcid']; ?>"><?php echo $pcv2['name']; ?></a></h4>
								<?php endif; ?>
							<?php endforeach; ?>								
								<div class="clear"></div>
							</div>
						</li>
						<?php endif; ?>
					<?php endforeach; ?>
					</ul>
				</section>
				
				<nav>
					<ul>
					<li>
					<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote1.jpg" width="198"></a>
					</li>
					<li>
					<a href="#"><img src="<?php echo $this->config->path_image; ?>/promote2.jpg" width="198"></a>
					</li>
					</ul>
				</nav>
			</aside>
			
			<div class="promote">
				<img src="<?php echo $this->config->path_image; ?>/fb_head.jpg" style="width:792px; height:222px;"/>
			</div>
			
			<div class="product-list">
			<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
				<article class="product">
					<div class="product-category-rt">
						
						<?php if (!empty($rv['bonus'])) : ?>
						<div class="product-category bonus">
							<span class="product-category-name" title="红利回馈%">
								<?php if ($rv['bonus_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['bonus']); ?>%
								<?php elseif ($rv['bonus_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['bonus']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
						<?php endif; ?>
						
						<?php /*$gift = floatval($this->tplVar['table']['record'][0]['gift']); ?>
						<?php if (!empty($gift)) : ?>
						<div class="product-category store">
							<span class="product-category-name" title="店点回馈%">
								<?php if ($rv['gift_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['gift']); ?>%
								<?php elseif ($rv['gift_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['gift']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
						<?php endif; */?>
						
						<?php if (!empty($rv['srid']) ) {
						if($rv['srid']=='any_saja'){
						} elseif($rv['srid']=='never_win_saja'){ ?>
							<div class="product-category pcid-1">
								<span class="product-category-name" title="新手限定(限中标次数 5 次以下)" >限新手</span>
							</div>
						<?php } elseif($rv['srid']=='le_win_saja'){ ?>
							<div class="product-category pcid-1">
								<span class="product-category-name" title="(限中标次数 <?php echo $rv['value'];?> 次以下)" >限新手</span>
							</div>
						<?php } elseif($rv['srid']=='ge_win_saja'){ ?>
							<div class="product-category pcid-1">
								<span class="product-category-name" title="(限中标次数 <?php echo $rv['value'];?> 次以上)" >限老手</span>
							</div>
						<?php 
						} }//endif; ?>
						
						<?php //区域
						/*if ($rv['loc_type'] == 'G') : ?>
						<div class="product-category pcid-2">
							<span class="product-category-name" title="区域" >全国</span>
						</div>
						<?php endif; */?>
							
						<?php if ((float)$rv['saja_fee'] == 0.00) : ?>
						<div class="product-category pcid-3">
							<span class="product-category-name" title="免费(出价免扣杀币)" >免费</span>
						</div>
						<?php endif; ?>
					
					</div>
					
					<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
				<?php if (!empty($rv['thumbnail'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['thumbnail']; ?>"/>
				<?php elseif (!empty($rv['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="http://img.saja.com.tw/main/iphone5s.jpg" />
				<?php endif; ?>
					</a>
					<header>
						<h3>
							<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>" class="name ellipsis">
								<?php echo $rv['name']; ?>
							</a>
						</h3>
					</header>
					
					<?php if (!empty($rv['offtime'])) : ?>
					<div class="offtime" data-offtime="<?php echo $rv['offtime']; ?>">
						<span class="timer-digit day">99</span>
						<span class="timer-unit day">天</span>
						<span class="timer-digit hr">23</span>
						<span class="timer-unit hr">时</span>
						<span class="timer-digit min">03</span>
						<span class="timer-unit min">分</span>
						<span class="timer-digit sec">01</span>
						<span class="timer-unit sec">秒</span>
					</div>
					<?php endif; ?>
					
					<?php //會員得標次數 $this->tplVar['table']['rt']['user_get_bid']?>
					<div class="saja-button">
						<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
							<img src="<?php echo $this->config->path_image; ?>/b_bid.png">
						</a>
					</div>
					
					<table class="info">
						<tbody>
							<td class="label">价值：</td>
							<td class="value retail_price"><?php echo number_format($rv['retail_price'], 2); ?></td>
							<td class="unit">元</td>
							<td class="label">关注：</td>
							<td class="value attention">987,654,321</td>
							<td class="unit">次</td>
						</tbody>
					</table>
				</article>

			<?php endforeach; ?>
			</div>
			<div class="clear"></div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>