<table>
	<thead>
		<tr>
			<th><span>商品编号</span></th>
			<th><span>礼券名称</span></th>
			<th><span>数量</span></th>
			<th><span>获得时间</span></th>
			<th><span>使用期限</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="productid"><span><?php echo $rv['productid']; ?></span></td>
			<td class="name"><span><?php echo $rv['name']; ?></span></td>
			<td class="count"><span><?php echo $rv['count']; ?></span></td>
			<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
		<?php if ($rv['offtime']) : ?>
			<td class="offtime"><span><?php echo date("Y-m-d H:i:s", $rv['offtime']); ?></span></td>
		<?php else : ?>
			<td class="offtime"><span>****-**-** --:--:--</span></td>
		<?php endif; ?>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>