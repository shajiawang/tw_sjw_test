<table>
	<thead>
		<tr>
			<th><span>商品编号</span></th>
			<th><span>商品明细</span></th>
			<th><span>手续费</span></th>
			<th><span>下标次数</span></th>
			<th><span>商品状态</span></th>
			<th><span>下标明细</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="productid"><span><?php echo $rv['productid']; ?></span></td>
			<td class="name"><span><?php echo $rv['name']; ?></span></td>
			<td class="saja_fee"><span><?php echo sprintf('%0.2f', $rv['saja_fee']); ?></span></td>
			<td class="count"><span><?php echo $rv['count']; ?></span></td>
			<td class="closed">
				<span>
				<?php if ($rv['closed'] == 'Y') : ?>
				已结标
				<?php elseif ($rv['closed'] == 'N') : ?>
				竞标中
				<?php else : ?>
				流标
				<?php endif; ?>
				</span>
			</td>
			<td class="spoint">
				<a class="detail" href="#<?php echo $this->config->default_main.'/history/saja_detail?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
					<span>明细<span>
				</a>
			</td>

		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>