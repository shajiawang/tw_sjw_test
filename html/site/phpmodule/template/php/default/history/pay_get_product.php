<table>
	<thead>
		<tr>
			<th><span>商品编号</span></th>
			<th><span>结标时间</span></th>
			<th><span>商品名称</span></th>
			<th><span>结账手续</span></th>
			<th><span>出货状态</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="productid"><span><?php echo $rv['productid']; ?></span></td>
			<td class="insertt"><span><?php echo date("Y-m-d H:i:s", $rv['insertt']); ?></span></td>
			<td class="name">
				<a href="<?php echo $this->config->default_main.'/product/saja?productid='.$rv['productid']; ?>"><span><?php echo $rv['name']; ?></span></a>
			</td>
			<td class="complete">
		<?php if ($rv['complete'] == 'Y') : ?>
				<span>已结账</span>
		<?php else : ?>
				<a href="<?php echo $this->config->default_main.'/pay_get_product?productid='.$rv['productid']; ?>">
					<span>未结账</span>

				</a>
		<?php endif; ?>
			</td>
			<td class="status"><span><?php echo $rv['status']; ?></span></td>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>