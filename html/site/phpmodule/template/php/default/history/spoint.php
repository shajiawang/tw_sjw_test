<table>
	<thead>
		<tr>
			<th><span>时间</span></th>
			<th><span>状态</span></th>
			<th><span>明细</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
			<td class="status">
				<span>
				<?php if ($rv['status'] == 'deposit') : ?>
				充值成功
				<?php elseif ($rv['status'] == 'error') : ?>
				充值失败
				<?php else : ?>
				充值取消
				<?php endif; ?>
				</span>
			</td>

			<td class="spoint"><span><?php echo sprintf('%0.2f', $rv['amount']); ?><span></td>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>