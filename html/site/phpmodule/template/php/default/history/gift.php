<table>
	<thead>
		<tr>
			<th><span>时间</span></th>
			<th><span>点数</span></th>
			<th><span>商品名称</span></th>
			<th><span>状态</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
			<td class="spoint"><span><?php echo sprintf('%0.2f', $rv['amount']); ?><span></td>
			<td class="name"><a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>" target="_blank"><span><?php echo $rv['name']; ?></span></a></td>
			<td class="behav">
				<span>
				<?php if ($rv['behav'] == 'product_close') : ?>
				结标回馈店点
				<?php elseif ($rv['behav'] == 'used') : ?>
				店点序号已使用
				<?php else : ?>
				取得店点
				<?php endif; ?>
				</span>

			</td>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>