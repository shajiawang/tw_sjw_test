<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			padding: 50px 160px;
		}

		#content[role=main] .content .logo {
			max-width: 214px;
			margin: 0 auto;
			display: block;
		}

		#content[role=main] .content .deposit-rule-item {
			width: 100%;
			border: 1px solid #DCDCDC;
		}

		#content[role=main] .content .deposit-rule-item th {
			text-align: center;
			padding: 5px;
			border: 1px solid #DCDCDC;
		}

		#content[role=main] .content .deposit-rule-item td {
			text-align: center;
			padding: 5px 0;
			border: 1px solid #DCDCDC;
		}

		#content[role=main] .content .deposit-rule-item .amount {
			color: #FB0000;
		}

		#content[role=main] .content .deposit-rule-item .donate {
			line-height: 22px;
		}
		</style>
		<script type="text/javascript">
		$(function(){

		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<div class="content">
				<img class="logo" src="<?php echo $this->config->path_image.'/'.$this->tplVar['table']['rt']['deposit_rule'][0]['logo']; ?>">
				<form name="deposit-rule-item" method="POST" action="<?php echo $this->config->default_main.'/deposit/'.$this->tplVar['table']['rt']['deposit_rule'][0]['act'].'?'.$this->tplVar['status']['args']; ?>">
					<table class="deposit-rule-item">
						<thead>
							<th>选择</th>
							<th>点数</th>
							<th>缴款金额</th>
						</thead>
						<tbody>
						<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="driid"><input type="radio" name="driid" value="<?php echo $rv['driid']; ?>"/></td>
								<td class="name"><?php echo $rv['name']; ?></td>
								<td class="amount">RMB : <?php echo sprintf("%0.2f", $rv['amount']); ?></td>
							</tr>
						<?php endforeach; ?>
						</tbody>
						<tfoot>
							<tr>
								<td class="donate" colspan="3">
									<input type="submit" value="确认充值">
								</td>
							</tr>
						</tfoot>
					</table>
				</form>
			</div>
		</main>

		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>