<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			padding: 50px 160px;
		}
		#content[role=main] .content .deposit-rule {
			list-style: none;
			width: 100%;
		}
		#content[role=main] .content .deposit-rule li {
			float: left;
			text-align: center;
			margin-right: 10px;
			position: relative;
		}
		#content[role=main] .content .deposit-rule .link {
			display: block;
			text-decoration: none;
			width: 214px;
			min-height: 190px;
			border: 1px solid #D5D5D5;
			border-radius: 5px;
			padding: 5px 0;
		}
		#content[role=main] .content .deposit-rule .logo {
			display: block;
			max-width: 214px;
			max-height: 156px;
			margin: 0 auto;
		}
		#content[role=main] .content .deposit-rule .name {
			text-align: center;
			color: #6a6a6a;
			width: 100%;
			height: 30px;
			line-height: 30px;
			font-size: 18px;
			position: absolute;
			bottom: 0;
			border-top: 1px solid #D5D5D5;
		}

		</style>
		<script type="text/javascript">
		$(function(){

		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<div class="content">
				<ul class="deposit-rule">
					<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
					<li class="rule">
						<a class="link" href="<?php echo $this->config->default_main.'/deposit/deposit_rule_item?'.$this->tplVar['status']['args'].'&drid='.$rv['drid']; ?>">
							<img class="logo" src="<?php echo $this->config->path_image.'/'.$rv['logo']; ?>">
							<div class="name"><?php echo $rv['name']; ?></div>
						</a>
					</li>
				<?php endforeach; ?>
					<li class="clear"></li>
				</ul>
				<div class="clear"></div>
			</div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>