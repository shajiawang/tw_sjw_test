<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			padding: 50px 160px;
		}

		#content[role=main] .content .logo {
			max-width: 214px;
			margin: 0 auto;
			display: block;
		}

		#content[role=main] .content .deposit-rule-item {
			width: 100%;
			border: 1px solid #DCDCDC;
		}

		#content[role=main] .content .deposit-rule-item th {
			text-align: center;
			padding: 5px;
			border: 1px solid #DCDCDC;
		}

		#content[role=main] .content .deposit-rule-item td {
			text-align: center;
			padding: 5px 0;
			border: 1px solid #DCDCDC;
		}

		#content[role=main] .content .deposit-rule-item .amount {
			color: #FB0000;
		}

		#content[role=main] .content .deposit-rule-item .donate {
			line-height: 22px;
		}


		</style>
		<script type="text/javascript">
		$(function(){

		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<div class="content">
				<img class="logo" src="<?php echo $this->config->path_image.'/'.$this->tplVar['table']['rt']['deposit_rule_item'][0]['logo']; ?>">
				<form name="deposit-rule-item" method="POST" action="<?php echo $this->config->alipay['url_payment']; ?>">
					<ul>
						<li>充值方式：支付宝</li>
						<li>充值点数：<?php echo sprintf("%d",$this->tplVar['table']['rt']['deposit_rule_item'][0]['spoint']); ?></li>
						<li>交易金额：<?php echo sprintf("%0.2f",$this->tplVar['table']['rt']['deposit_rule_item'][0]['amount']); ?></li>
						<li>
							<input type="submit" value="确定充值">
							<input type="hidden" name="merchantnumber" value="<?php echo $this->config->alipay['merchantnumber']; ?>">
							<input type="hidden" name="ordernumber" value="<?php echo $this->tplVar['table']['record'][0]['ordernumber']; ?>">
							<input type="hidden" name="amount" value="<?php echo sprintf("%d",$this->tplVar['table']['rt']['deposit_rule_item'][0]['amount']); ?>">
							<input type="hidden" name="paymenttype" value="<?php echo $this->config->alipay['paymenttype']; ?>">
							<input type="hidden" name="paytitle" value="<?php echo $this->tplVar['table']['rt']['deposit_rule_item'][0]['rule_name'].' '.$this->tplVar['table']['rt']['deposit_rule_item'][0]['name']; ?>">
							<input type="hidden" name="hash" value="<?php echo $this->tplVar['table']['record'][0]['hash']; ?>">
							<input type="hidden" name="nexturl" value="<?php echo 'http://howard.sajar.com.tw'.$this->config->default_main.'/deposit/alipay_complete'; ?>">
						</li>
					</ul>
				</form>

			</div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>