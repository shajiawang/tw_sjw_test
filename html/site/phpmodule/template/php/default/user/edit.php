<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?> 

	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/main.css" type="text/css">
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首页</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">使用者管理</a>>><a>编辑</a>
		</div>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">编辑数据</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">使用者ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="nickname">昵称</label></td>
					<td><input name="nickname" type="text" value="<?php echo $this->tplVar['table']['record'][0]['nickname']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="email">Email</label></td>
					<td><input name="email" type="text" value="<?php echo $this->tplVar['table']['record'][0]['email']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="city">City</label></td>
					<td><input name="city" type="text" value="<?php echo $this->tplVar['table']['record'][0]['city']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="area">Area</label></td>
					<td><input name="area" type="text" value="<?php echo $this->tplVar['table']['record'][0]['area']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="passwd">新密码：</label></td>
					<td><input name="passwd" type="password" value=""/></td>
				</tr>
				<tr>
					<td><label for="passwd_confirm">新密码确认：</label></td>
					<td><input name="passwd_confirm" type="password"/></td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar["status"]["get"]["userid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>

		</form>
	</body>
</html>