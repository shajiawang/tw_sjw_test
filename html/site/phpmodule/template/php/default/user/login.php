<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .registration {
			width: 600px;
			margin: 30px auto;
		}
		#content[role=main] .registration h2 {
			font-size: 1em;
			font-weight: bold;
			color: #FFA80B;
			margin-left: 20px;
			margin-bottom: 5px;
		}
		#content[role=main] .registration .frame {
			border: 1px solid #FFA80B;
		}
		#content[role=main] .registration .form {
			margin: 10px 0 20px 0;
			width: 600px;
		}
		#content[role=main] .registration .form .field th {
			padding-top: 10px;
			font-size: 13px;
			color: #FFA80B;
			text-align: right;
			padding-bottom: 5px;
		}
		#content[role=main] .registration .form .field td {
			font-size: 13px;
			padding-top: 10px;
			padding-left: 3px;
		}
		#content[role=main] .registration .form .comment th {
			font-size: 12px;
			color: #B0B0B0;
			text-align: right;
		}
		#content[role=main] .registration .form .comment td {
			font-size: 13px;
			padding-left: 5px;
		}
		#content[role=main] .registration .form .service td {
			font-size: 13px;
			padding-top: 15px;
			padding-left: 20px;
		}
		#content[role=main] .registration .form .service a {
			color: red;
			text-decoration: none;
		}
		#content[role=main] .registration .form .submit {
			width: 94px;
			height: 26px;
			margin: 0 auto;
			display: block;
			border: 2px solid #FFB300;
			border-radius: 5px;
			color: white;
			font-size: 11px;
			background: #ffb300;
			background: -moz-linear-gradient(top,  #ffb300 0%, #ffac00 50%, #ff9e00 98%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffb300), color-stop(50%,#ffac00), color-stop(98%,#ff9e00));
			background: -webkit-linear-gradient(top,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			background: -o-linear-gradient(top,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			background: -ms-linear-gradient(top,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			background: linear-gradient(to bottom,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffb300', endColorstr='#ff9e00',GradientType=0 );
		}
		</style>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<section class="registration">
				<h2>会员登录</h2>
				<div class="frame">
					<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/user_login">
						<table class="form">
							<tbody>
								
								<tr class="field">
									<th>登录账号</th>
									<td><input type="text" name="name" placeholder="手机号码/会员帐号" /></td>
								</tr>
								
								<tr class="field">
									<th>登录密码</th>
									<td><input type="password" name="passwd" placeholder="请输入登录密码" /></td>
								</tr>
							
							</tbody>
						</table>
						<div class="functions" style="text-align: center;">
							<input type="hidden" name="location_url" value="<?php //echo $this->tplVar["status"]['get']['location_url']; ?>">
							<div class="button submit"><input type="submit" value="送出" class="submit"></div>

							<?php /*
							<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
							<div class="clear"></div>*/?>
						</div>
					</form>
				</div>
			</section>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>