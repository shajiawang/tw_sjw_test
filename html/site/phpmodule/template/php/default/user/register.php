<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .registration {
			width: 600px;
			margin: 30px auto;
		}
		#content[role=main] .registration h2 {
			font-size: 1em;
			font-weight: bold;
			color: #FFA80B;
			margin-left: 20px;
			margin-bottom: 5px;
		}
		#content[role=main] .registration .frame {
			border: 1px solid #FFA80B;
		}
		#content[role=main] .registration .form {
			margin: 10px 0 20px 0;
			width: 600px;
		}
		#content[role=main] .registration .form .field th {
			padding-top: 10px;
			font-size: 13px;
			color: #FFA80B;
			text-align: right;
			padding-bottom: 5px;
			width: 150px;
		}
		#content[role=main] .registration .form .field td {
			font-size: 13px;
			padding-top: 10px;
			padding-left: 3px;
		}
		#content[role=main] .registration .form .comment th {
			font-size: 12px;
			color: #B0B0B0;
			text-align: right;
		}
		#content[role=main] .registration .form .comment td {
			font-size: 13px;
			padding: 5px 5px;
		}
		#content[role=main] .registration .form .service td {
			font-size: 13px;
			padding-top: 15px;
			padding-left: 20px;
		}
		#content[role=main] .registration .form .service a {
			color: red;
			text-decoration: none;
		}
		#content[role=main] .registration .form .submit {
			width: 94px;
			height: 26px;
			margin: 0 auto;
			display: block;
			border: 2px solid #FFB300;
			border-radius: 5px;
			color: white;
			font-size: 11px;
			background: #ffb300;
			background: -moz-linear-gradient(top,  #ffb300 0%, #ffac00 50%, #ff9e00 98%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffb300), color-stop(50%,#ffac00), color-stop(98%,#ff9e00));
			background: -webkit-linear-gradient(top,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			background: -o-linear-gradient(top,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			background: -ms-linear-gradient(top,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			background: linear-gradient(to bottom,  #ffb300 0%,#ffac00 50%,#ff9e00 98%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffb300', endColorstr='#ff9e00',GradientType=0 );
		}
		</style>
		<script type="text/javascript">
		$(function(){
			$("#passwd").on("focus", function() { /*Do something...*/$("#passwd").val(''); }); 
			if($("#passwd").val()==""){ $("#passwd").val("******"); }
			
			$("#repasswd").on("focus", function() { /*Do something...*/$("#repasswd").val(''); }); 
			if($("#repasswd").val()==""){ $("#repasswd").val("******"); }
		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<section class="registration">
				<h2>会员注册</h2>
				<div class="frame">
					<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/user_insert">
						<table class="form">
							<tbody>
								<tr class="field">
									<th>昵称 (必填)</th>
									<td><input type="text" name="nickname" value="" />昵称限2~8个字</td>
								</tr>
								<tr class="comment">
									<th>Nickname</th>
									<td>注册后无法更改，请慎选! 请勿使用不雅字眼，被检举的话账号将会被停权!</td>
								</tr>
								
								<tr class="field">
									<th>性别 (必填)</th>
									<td>
										男<input type="radio" name="gender" value="male" checked />
										女<input type="radio" name="gender" value="female" />
										注册后无法更改!
									</td>
								</tr>
								<tr class="comment">
									<th>Gender</th>
									<td></td>
								</tr>
								
								<tr class="field">
									<th>电子邮件 (必填)</th>
									<td>
										<input type="text" name="email" value="" />
									</td>
								</tr>
								<tr class="comment">
									<th>Email</th>
									<td></td>
								</tr>
							
								<tr class="field">
									<th>手机号码 (必填)</th>
									<td><input type="text" name="phone" value="" /></td>
								</tr>
								<tr class="comment">
									<th>Telephone</th>
									<td>请输入正确的手机号码。</td>
								</tr>
								
								<tr class="field">
									<th>密码 (必填)</th>
									<td><input type="password" name="passwd" id="passwd" /></td>
								</tr>
								<tr class="comment">
									<th>Password</th>
									<td>以4~12个英文字母或数字为限</td>
								</tr>
								<tr class="field">
									<th>确认密码 (必填)</th>
									<td><input type="password" name="repasswd" id="repasswd" /></td>
								</tr>
								<tr class="comment">
									<th>Re-Password</th>
									<td>请再次输入密码确认</td>
								</tr>
								
								<?php /*
								<tr class="field">
									<th>登入账号</th>
									<td><input type="text" name="name" /></td>
								</tr>
								<tr class="comment">
									<th>Username</th>
									<td>以4~12个英文字母或数字为限。此为登入之账号，不得更改，请正确填写!</td>
								</tr>
								<tr class="field">
									<th>收件地址</th>
									<td>
										邮编: <input type="text" name="area" size="6" /> 
										<input type="text" name="address" size="60" value="" />
									</td>
								</tr>
								<tr class="comment last">
									<th>Address</th>
									<td>此为得标收货地址，注册后无法更改!</td>
								</tr>
								<tr class="field">
									<th>收件人</th>
									<td>
										<input type="text" name="addressee" value="" />
									</td>
								</tr>
								<tr class="comment last">
									<th>Addressee</th>
									<td>此为得标收件人，注册后无法更改!</td>
								</tr>
								*/?>
								
								<?php /*
								<tr class="field">
									<th>验证码</th>
									<td>
										<?php require_once $this->config->default_main .'/php_captcha/view'; ?>
										<input type="text" name="captcha"/>
									</td>
								</tr>
								<tr class="comment">
									<th>Captcha</th>
									<td></td>
								</tr> 
								<tr class="service">
									<td colspan="2">
										<input type="checkbox" name="service"/>
										我已经年满18岁且详细阅读过，并同意 <a href="<?php echo $this->config->default_main; ?>/page/service">服务条款</a>
									</td>
								</tr> */?>
								<tr class="field">
									<td colspan="2">
										<input type="hidden" name="name" value="">
										<input type="hidden" name="cityid" value="">
										<input type="hidden" name="area" value="">
										<input type="hidden" name="address" value="">
										<input type="hidden" name="addressee" value="">
										<input class="submit" type="submit" value="确认"/>
									</td>
								</tr>
							</tbody>
						</table>
					</form>
				</div>
			</section>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>