<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
		
		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			margin-top:45px;
			z-index:0;
			
		}
		#content_reg {
			line-height:25px;
			padding-top:15px;
			position:relative;
			left:50%;
			margin-left:-139px;
			
		}
		#content_reg div {
			float:left;
			margin:1px;
			padding:5px;
		}
		#content_reg_td1 {
			width:70px;
			clear:both;
			background-color:#CCC;
		}
		#content_reg_td2 {
			width:180px;
		}
		#content_reg_td2 input {
			width:170px;
		}
		#content_reg_ps {
			width:262px;
			clear:both;
		}
		#content_btn {
			text-align:center;
			clear:both;
		}
		#content_btn_registration {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:10px;
		}
		</style>
	</head>
	<body>
	<div id="all">	
		<?php require_once $this->tplVar['block']['header']; ?>
		
		<div id="content">
        <form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/confirm_sms">
		<div id="content_reg">
            <div id="content_reg_td1">昵称</div><div id="content_reg_td2"><?php echo $this->tplVar['table']['record'][0]['nickname']; ?></div>
            <div id="content_reg_td1">手机号码</div><div id="content_reg_td2"><?php echo $this->tplVar['table']['record'][0]['phone']; ?></div>
            <div id="content_reg_td1">SMS check code</div><div id="content_reg_td2"><input name="code" type="text" /></div>
        </div>
        <input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
		<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>">
		<div id="content_btn"><input id="content_btn_registration" type="submit" value="确认" /></div>
		</form>
		</div>
	</div>
	<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>