<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
		
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首页</a>>><a>后台人员管理</a>>><a>群组</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label">昵称：</span>
					<input type="text" name="search_nickname" size="20" value="<?php echo $this->io->input['get']['search_nickname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">Email：</span>
					<input type="text" name="search_email" size="20" value="<?php echo $this->io->input['get']['search_email']; ?>"/>
				</li>
				<li class="button">
					<button>搜寻</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"></li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>删除</th>
								<?php $base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; ?>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_userid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_userid=desc">userid<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_userid"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_userid=">userid<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_userid=asc">userid<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>昵称</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_email"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_email=desc">Email<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_email"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_email=">Email<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_email=asc">Email<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_provider_name"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_provider_name=desc">SSO名称<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_provider_name"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_provider_name=">SSO名称<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_provider_name=asc">SSO名称<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_sso_email"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_sso_email=desc">SSO Email<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_sso_email"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_sso_email=">SSO Email<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_sso_email=asc">SSO Email<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_switch"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_switch=desc">启用<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_switch"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_switch=">启用<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_switch=asc">启用<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>

						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'. $this->tplVar["status"]["get"]["fun"]; ?>/edit/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'. $this->tplVar["status"]["get"]["fun"]; ?>/delete/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"])) ;?>"></a></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="email"><?php echo $rv['email']; ?></td>
								<td class="column" name="provider_name"><?php echo $rv['provider_name']; ?></td>
								<td class="column" name="sso_email"><?php echo $rv['sso_email']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
								<td class="column" name="switch"><?php echo $rv['switch']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add">新增</a>
						</div>						
					</div>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>