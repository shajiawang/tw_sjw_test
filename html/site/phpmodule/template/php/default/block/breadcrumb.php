<div class="breadcrumb header-style">
<a href="<?php echo $this->config->default_main; ?>">首页</a>>>

<?php
switch($this->io->input['get']['fun']) {
	case 'exchange_mall':
		$name = "兑换中心";
		break;
	case 'exchange_store':
		$name = "兑换店家";
		break;
	case 'exchange_product_category':
		$name = "兑换商品分类";
		break;
	case 'exchange_product':
		$name = "兑换商品";
		break;
	case 'order':
		$name = "订单管理";
		break;
	case 'stock':
		$name = "库存管理";
		break;
	case 'promote_rule':
		$name = "活动规则";
		break;
	case 'promote_rule_item':
		$name = "活动规则项目";
		break;
	case 'exchange_bonus_history':
		$name = "兑换红利记录";
		break;
	case 'exchange_gift_history':
		$name = "兑换礼券记录";
		break;
	case 'return_history':
		$name = "退货记录";
		break;
	case 'channel':
		$name = "经销商管理";
		break;
	case 'country':
		$name = "国别管理";
		break;
	case 'language':
		$name = "语系管理";
		break;
	case 'bonus':
		$name = "红利账户";
		break;
	case 'deposit':
		$name = "储值账户";
		break;
	case 'deposit_rule':
		$name = "储值规则";
		break;
	case 'deposit_rule_item':
		$name = "储值规则项目";
		break;
	case 'gift':
		$name = "礼券账户";
		break;
	case 'user':
		$name = "会员账号";
		break;
	case 'user_profile':
		$name = "会员详细资料";
		break;
	case 'user_sms_auth':
		$name = "会员手机验证记录";
		break;
	case 'news':
		$name = "会员公告";
		break;
	case 'issues_category':
		$name = "客服讯息分类";
		break;
	case 'issues':
		$name = "客服讯息";
		break;
	case 'faq_category':
		$name = "帮助中心分类";
		break;
	case 'faq':
		$name = "帮助中心问答";
		break;
	case 'admin_user':
		$name = "管理员账号";
		break;
}
?>
<a href="<?php echo $this->config->default_main.'/'.$this->io->input['get']['fun']; ?>"><?php echo $name; ?></a>>>

<?php
switch($this->tplVar['status']['get']['act']) {
	case 'add':
		$act = "新增".$name;
		break;
	case 'edit':
		$act = "编辑".$name;
		break;
	case 'view':
		$act = $name."列表";
		break;
}
?>
<a><?php echo $act; ?></a>
</div>
