		<footer class="footer">
			<div class="bar"></div>
			<div class="content">
				<table class="link-list">
					<thead>
						<th>用户帮助</th>
						<th>获得更新</th>
						<th>手机杀价王网</th>
						<th>商务合作</th>
						<th>公司信息</th>
					</thead>
					<tbody>
						<tr>
							<td>玩转杀价王</td>
							<td>邮件订阅</td>
							<td>杀价王网触屏版</td>
							<td>市场合作</td>
							<td>关于杀价王</td>
						</tr>
						<tr>
							<td>常见团购问题</td>
							<td>杀价王新浪微薄</td>
							<td>下载手机本</td>
							<td>友情连结</td>
							<td>加入杀价王</td>
						</tr>
						<tr>
							<td>北京往期团购</td>
							<td>杀价王腾讯微薄</td>
							<td></td>
							<td>杀价王商家</td>
							<td>团购用户协议</td>
						</tr>
						<tr>
							<td>邮箱白名单设置</td>
							<td>人人网公共主页</td>
							<td></td>
							<td>网站地图</td>
							<td>媒体报导</td>
						</tr>
					</tbody>
				</table>
				<table class="contact-info">
					<tbody>
						<tr>
							<td class="phone-number">
								<img class="icon" src="<?php echo $this->config->path_image; ?>/i_phone.png" alt="">
								<span class="number">4006-888-888</span>
							</td>
						</tr>
						<tr>
							<td class="phone-desc">客服电话免长途费</td>
						</tr>
						<tr>
							<td class="phone-desc">周一至周日08:00-22:00</td>
						</tr>
						<td class="mobile-app">
							<img class="bg-image" src="<?php echo $this->config->path_image; ?>/t_mobile.png" alt=""/>
							<div class="slogan1">移动杀价王</div>
							<div class="slogan2">给我杀价其余免谈！</div>
							<div class="app-list">

							</div>
						</td>
					</tbody>
				</table>
				<div class="clear"></div>
			</div>
			<div class="certificate-icon">
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/logo_1.gif"></a>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/logo_2.gif"></a>
				<a href="#"><img src="<?php echo $this->config->path_image; ?>/logo_3.gif"></a>
			</div>
			<div class="certificate-no">
				&copy;2013 nuomi.com京ICP证060807号京公网安备110105006181号 工商注册号110108009499245 互联网药品信息服务资格证编号（京-经营性-2011-0009）
			</div>
		</footer>
