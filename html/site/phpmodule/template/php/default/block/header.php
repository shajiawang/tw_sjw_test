<?php 
//20~05 05~14 14~20
$nowh = (int)date('H', time());
if($nowh>05 && $nowh<=13) {
	$nav_bc = 1;
} elseif($nowh>13 && $nowh<=20) {
	$nav_bc = 2;
} else {
	$nav_bc = 3;
}
?>
<header class="header">
			<?php if (empty($this->io->input['session']['user'])) : ?>
			<div class="login-entry collapsed">
				<ul class="login-list">
					<li class="user">登录</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/user/login" title="一般登录">
							<img src="<?php echo $this->config->path_image; ?>/i_login.png">
						</a>
					</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/oauth/qqapi" title="微信QQ登录">
							<img src="<?php echo $this->config->path_image; ?>/i_qq.png">
						</a>
					</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/oauth/sinaapi" title="新浪微博登录">
							<img src="<?php echo $this->config->path_image; ?>/i_sina.png">
						</a>
					</li>
					<?php /*
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/user/sso_login?provider=facebook" title="Facebook登入">
							<img src="<?php echo $this->config->path_image; ?>/i_facebook.png">
						</a>
					</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/user/sso_login?provider=yahoo" title="Yahoo登入">
							<img src="<?php echo $this->config->path_image; ?>/i_yahoo.png">
						</a>
					</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/user/sso_login?provider=twitter" title="Twitter登入">
							<img src="<?php echo $this->config->path_image; ?>/i_twitter.png">
						</a>
					</li>*/?>
				</ul>
			</div>
			
			<div class="register_entry collapsed">
				<ul class="register-list">
					<li class="user">免费注册</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/user/register" title="一般免费注册">
							<img src="<?php echo $this->config->path_image; ?>/i_login.png">
						</a>
					</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/oauth/qqapi" title="微信QQ注册" >
							<img src="<?php echo $this->config->path_image; ?>/i_qq.png">
						</a>
					</li>
					<li class="sso">
						<a href="<?php echo $this->config->default_main; ?>/oauth/sinaapi" title="新浪微博注册" >
							<img src="<?php echo $this->config->path_image; ?>/i_sina.png">
						</a>
					</li>
					<?php /*
					<li class="sso">
						<a href="https://zh-tw.facebook.com/" title="Facebook注册" >
							<img src="<?php echo $this->config->path_image; ?>/i_facebook.png">
						</a>
					</li>
					<li class="sso">
						<a href="https://tw.edit.yahoo.com/registration?.intl=tw&.src=fpctx&.done=http://tw.dictionary.com/?ei=UTF-8" title="Yahoo注册" >
							<img src="<?php echo $this->config->path_image; ?>/i_yahoo.png">
						</a>
					</li>
					<li class="sso">
						<a href="https://twitter.com/signup" title="Twitter注册" >
							<img src="<?php echo $this->config->path_image; ?>/i_twitter.png">
						</a>
					</li>*/?>
				</ul>
			</div>
		<?php endif; ?>
		
		<nav class="nav-function nav-bc<?php echo $nav_bc;?>">
		<div class="nav-w">
			<h1>全球首创杀价式拍卖导购页</h1>
			<ul class="header-function">
					<li class="set-homepage">
						<img class="icon" src="<?php echo $this->config->path_image; ?>/i_home.gif"><span>设为首页</span>
					</li>
					<li class="add-bookmark">
						<img class="icon" src="<?php echo $this->config->path_image; ?>/i_bookmarks.gif"><span>加入书签</span>
					</li>
					<li class="set-homepage">
						<a href="<?php echo $this->config->default_main.'/faq'; ?>">
						<img class="icon" src="<?php echo $this->config->path_image; ?>/i_home.gif"><span>帮助中心</span>
						</a>
					</li>
					<li class="exchange">
						<a href="<?php echo $this->config->default_main; ?>/exchange_mall/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&epcid=1">
						<img class="icon" src="<?php echo $this->config->path_image; ?>/i_cart.gif"><span>兑换商城</span>
						</a>
					</li>
			</ul>
			<ul class="header-function right">
				<?php if (empty($this->io->input['session']['user'])) : ?>
					<li>
						<a id="register" href="javascript:void(0);"><span>免费注册</span></a>
					</li>
					<li>
						<a id="login" href="javascript:void(0);"><span>登录</span></a>
					</li>
				<?php else : ?>
					<li>
						<a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>"><span>会员中心</span></a>
					</li>
					<li>
						<a href="<?php echo $this->config->default_main; ?>/user/logout"><span>注销</span></a>
					</li>
				<?php endif; ?>
			</ul>
		</div>
		</nav>
		
		<div class="banner">
				<table class="layer-2">
					<tbody>
						<tr>
							<td>
								<a href="<?php echo $this->config->default_main.'/?channelid='.$this->tplVar['status']['get']['channelid']; ?>">
									<img class="logo" src="<?php echo $this->config->path_image; ?>/logo.png">
								</a>
							</td>
							<td>
								<?php /*<span>北京</span>
								<select class="">
									<option value="">切换城市</option>
									<option>北京</option>
									<option>上海</option>
									<option>广州</option>
								</select>
								<div>xxxx</div>*/?>
							</td>
							<td>
								<nav class="nav-bar">
									<ul>
										<li class="border-left">
											<img class="border" src="<?php echo $this->config->path_image; ?>/bar_l.gif">
										</li>
										<li>
											<img class="icon" src="<?php echo $this->config->path_image; ?>/bar_process.gif">
											<a href="<?php echo $this->config->default_main.'/help'; ?>">杀价流程</a>
										</li>
										<li>
											<img class="splitter" src="<?php echo $this->config->path_image; ?>/bar_i.gif">
											<img class="icon" src="<?php echo $this->config->path_image; ?>/bar_cheats.gif">
											<a href="http://www.saja.com.tw/eventt/cheats.htm" target="_blank">杀价秘技</a>
										</li>
										<li>
											<img class="splitter" src="<?php echo $this->config->path_image; ?>/bar_i.gif">
											<img class="icon" src="<?php echo $this->config->path_image; ?>/bar_prepaid.gif">
											<a href="<?php echo $this->config->default_main; ?>/deposit">杀币充值</a>
										</li>
										<li>
											<img class="splitter" src="<?php echo $this->config->path_image; ?>/bar_i.gif">
											<img class="icon" src="<?php echo $this->config->path_image; ?>/bar_celebrity.gif">
											<a href="#">杀价王名人榜</a>
										</li>
										<li class="border-right">
											<img class="border" src="<?php echo $this->config->path_image; ?>/bar_r.gif">
										</li>
									</ul>
								</nav>
							</td>
						</tr>
					</tbody>
				</table>
				<nav clsas="nav-product-category">
					<ul class="product-category">
					<?php if (!empty($this->tplVar['table']['rt']['product_category'])) : ?>
						<?php foreach($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
						<?php if ($pcv['layer'] == 1) : ?>
						<li class="product-category-name">
							<a href="<?php echo $this->config->default_main; ?>/product/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>&pcid=<?php echo $pcv['pcid'] ?>"><?php echo $pcv['name']; ?></a>
						</li>
						<?php endif; ?>
						<?php endforeach; ?>
						<li class="clear"></li>
					<?php endif; ?>
					</ul>
				</nav>
		</div>
			
		<div class="search-bar">
				<div class="search block">
					<form method="get" action="<?php echo $this->config->default_main; ?>/product/view/?channelid=<?php echo $this->io->input['get']['channelid']; ?>">
						
						<img class="search-icon" src="<?php echo $this->config->path_image; ?>/i_search.png" alt=""/>
						
						<?php $search_name = (isset($this->io->input["get"]["search_name"])) ? $this->io->input["get"]["search_name"] : '';?>
						<input type="text" name="search_name" value="<?php echo (!empty($search_name)) ? $search_name : '';?>" class="search-name">
						<select name="search_pcid" class="search-pcid">
							<option value="">全部分类</option>
							<?php 
							$search_pcid = (isset($this->io->input["get"]["search_pcid"])) ? $this->io->input["get"]["search_pcid"] : '';
							$pc_id = (isset($this->io->input["get"]["pcid"])) ? $this->io->input["get"]["pcid"] : '';
							$pcid = (!empty($search_pcid)) ? $search_pcid : $pc_id;
							foreach ($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
							<?php if ($pcv['layer'] == 1) : ?>
								<option value="<?php echo $pcv['pcid']; ?>" <?php echo ($pcv['pcid']==$pcid) ? 'selected' : '';?> ><?php echo $pcv['name']; ?></option>
							<?php endif ?>
							<?php endforeach; ?>
						</select>
				
						<input type="submit" class="search-submit" value="搜寻"/>
						
						<ul class="statistic-data">
							<li class="border-left">
								<img class="icon" src="<?php echo $this->config->path_image; ?>/menu_total_l.gif" alt=""/>
							</li>
							<li class="center">
								累积杀价王<span class="value">123,456,789</span>位
								<span>共杀价节省</span><span class="value">6,543,210</span>元
							</li>
							<li class="border-right">
								<img class="icon" src="<?php echo $this->config->path_image; ?>/menu_total_r.gif" alt=""/>
							</li>
						</ul>
					</form>
				</div>
				
				<div class="news block">
					<ul class="news-list">
						<li>
							<img class="icon" src="<?php echo $this->config->path_image; ?>/t_hot.gif">
							<a class="title" href="#">欢迎来到杀价王，全球首创的『杀价式拍卖』竞标网站</a>
						</li>
					</ul>
				</div>
		</div>
</header>
