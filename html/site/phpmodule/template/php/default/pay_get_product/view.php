<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			border: 1px solid #C1C1C1;
		}

		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #FDA713;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}

		#content[role=main] .content .body {
			border-top: 2px solid #FD980E;
			padding: 10px 8px;
		}
		#content[role=main] .content .body h3 {
			padding: 0 40px;

			/*width: 768px;*/
			height: 30px;
			line-height: 30px;
			font-size: 16px;
			border-top: 1px solid #A5A5A5;
			border-bottom: 1px solid #A5A5A5;
		}

		.detail,
		.receiver {
			width: 700px;
			margin: 10px auto;
		}

		.detail thead th.text,
		.detail tbody td.text {
			text-align: center;
		}

		.detail thead th.number,
		.detail tbody td.number,
		.detail tfoot th,
		.detail tfoot td {
			text-align: right;
		}

		.detail thead tr,
		.detail tfoot tr {
			background-color: #C9C9C9;
		}

		.detail tbody tr {
			border: 1px solid #A5A5A5;
		}

		.detail tfoot th {
			color: #FF6666;
		}

		.detail .name {
			width: 60%;
		}
		.detail .amount,
		.detail .price {
			width: 20%;
		}

		.receiver th {
			width: 30%;
			color: #6D6D6D;
			text-align: left;
			padding=left: 40px;
		}
		.receiver td {
			width: 70%;
		}

		</style>
		<script type="text/javascript">
		$(function(){
		});
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<div class="content">
				<section class="block-frame">
					<h2>商品结账</h2>
					<div class="body">
						<h3>购买明细</h3>
						<table class="detail">
							<thead>
								<tr>
									<th class="name">商品名称</th>
									<th class="amount number">数量</th>
									<th class="price number">得标价(杀币)</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
								<tr>
									<td class="text"><?php echo $rv['name']; ?></td>
									<td class="number">1</td>
									<td class="number"><?php echo round($rv['price'], 2); ?></td>
								</tr>
							<?php endforeach; ?>
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<th>处理费</th>
									<td class="number"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['real_process_fee']); ?>杀币</td>
								</tr>
								<tr>
									<td></td>
									<th>总计</th>
									<td class="number"><?php echo sprintf("%0.2f", $this->tplVar['table']['record'][0]['total']); ?>杀币</td>
								</tr>
							</tfoot>
						</table>
						<h3>收件人寄送资料</h3>
						<form method="post" action="<?php echo $this->config->default_main.'/pay_get_product/insert'; ?>">
							<table class="receiver">
								<tbody>
									<tr>
										<th>姓名：</th>
										<td>
											<input type="text" name="name"/>
											先生
											<input type="radio" name="gender" value="male">
											小姐
											<input type="radio" name="gender" value="female">
										</td>
									</tr>
									<tr>
										<th>手机：</th>
										<td>
											<input type="text" name="phone"/>
										</td>
									</tr>
									<tr>
										<th>地址：</th>
										<td>
											<input type="text" name="zip" size="6"/>
											<input type="text" name="address" size="60"/>
										</td>
									</tr>
									<tr>
										<th>订单备注：</th>
										<td>
											<input type="text" name="memo"/>
										</td>
									</tr>
								</tbody>
								<tfoot>
									<tr>
										<td colspan="2">
											<input type="hidden" name="pgpid" value="<?php echo $this->tplVar['table']['record'][0]['pgpid']; ?>">
											<input type="submit" value="确认结账">
										</td>
									</tr>
								</tfoot>
							</table>
						</form>
					</div>
				</section>

			</div>
			<div class="clear"></div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>