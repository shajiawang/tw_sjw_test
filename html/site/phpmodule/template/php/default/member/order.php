<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}

		.aside.member .menu li {
			margin: 8px 0;
		}

		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		.aside.member .menu b {
			color: #ff0000; font-weight:bold;
		}

		#content[role=main] .content {
			float: left;
			width: 792px;
			border: 1px solid #C1C1C1;
		}

		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #FDA713;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}

		#content[role=main] .content .body {
			border-top: 2px solid #FD980E;
			padding: 10px 8px;
		}

		.body .history-type {
			list-style: none;
			height: 35px;
			border-bottom: 1px solid #FEBC0E;
		}

		.body .history-type li {
			float: left;
		}

		.body .history-type .type {
			display: block;
			height: 35px;
			line-height: 35px;
			padding-left: 24px;
			padding-right: 34px;
			text-decoration: none;
			color: black;
		}

		.body .history-type .type.active {
			color: white;
			background-color: #FD980E;
			border-top-right-radius: 20px;
		}

		.body .history-list {
			/*border-top: 2px solid #FD980E;*/
		}

		.body .history-list table {
			width: 100%;
			margin-top: 15px;
			border-top: 1px solid #E4E4E4;
			border-bottom: 1px solid #E4E4E4;
			border-collapse: separate;
			border-spacing: 4px;
		}

		.body .history-list table th {
			text-align: center;
			height: 25px;
			line-height: 25px;
			background-color: #E1E1E1;
			color: #414141;
		}

		.body .history-list table td {
			text-align: center;
			vertical-align: middle;
			color: #747474;
			padding: 2px 0;
			height: 32px;
			line-height: 18px;
		}

		.body .history-list table td.name {
			max-width: 296px;
		}

		.body .history-list table tr.even td {
			background-color: #F1F1F1;
		}
		</style>
		<script type="text/javascript">
		$(function(){
			$('.type', '#history-type').on('click', function(){
				$(this)
				.addClass('active')
				.parent().siblings().find('.type.active').removeClass('active');

				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			$( document ).on('click', '#history-list .detail', function(){
				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					$('#history-list').load( href.substr(idx+1) );
				}
			});

			//重新整理頁面時，直接觸發click event
			if (window.location.hash) {
				$('#history-type .type[href="'+window.location.hash+'"]').trigger('click');
			} else {
				$('#history-type .type:eq(0)').trigger('click');
			}

			$('#history-type').on('mousemove', function(e){
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				if ($(this).is('a')) {
					console.log('x:' + e.clientX + ' , y:' + e.clientY);
				}
			});
		});
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<aside class="aside member">
				<h2>会员专区</h2>
				<nav>
					<ul class="menu">
						<li><a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>">会员资料</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>">站内公告</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/service?<?php echo $this->tplVar['status']['args']; ?>">客服记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/history?<?php echo $this->tplVar['status']['args']; ?>">事务历史记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/order?<?php echo $this->tplVar['status']['args']; ?>"><b>订单记录</b></a></li>
					</ul>

				</nav>
			</aside>
			<div class="content">
				<section class="block-frame">
					<h2>订单记录</h2>
					<div class="body">
						<div class="history-list" id="history-list">
							
							<table>
							<thead>
								<tr>
									<th><span>订单时间</span></th>
									<th><span>使用点数</span></th>
									<th><span>商品名称</span></th>
									<th><span>出货状态</span></th>
									<?php /*<th><span>订单明细</span></th>*/?>
								</tr>
							</thead>
							<tbody>
							<?php if (!empty($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
								<tr class="<?php echo $rk%2?"odd":"even"; ?>">
									<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
									<td class="total_fee"><span><?php echo sprintf('%0.2f', $rv['total_fee']); ?></span></td>
									<td class="name"><a href="<?php echo $rv['href']; ?>" target="_blank"><span><?php echo $rv['name']; ?></span></a></td>
									<td class="status">
										<span>
										<?php if ($rv['status'] == '0') : ?>
										处理中
										<?php elseif ($rv['status'] == '1') : ?>
										缺货
										<?php else : ?>
										已出货
										<?php endif; ?>
										</span>
									</td>
									<?php /*<td class="spoint">
										<a class="detail" href="#<?php echo $this->config->default_main.'/history/saja_detail?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
											<span>明细<span>
										</a>
									</td>*/?>

								</tr>
							<?php endforeach; ?>
							<?php endif; ?>
							</tbody>
							</table>
						</div>
					</div>
				</section>
			</div>
			<div class="clear"></div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>
