<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			float: left;
			width: 792px;
			border: 1px solid #C1C1C1;
		}
		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #FDA713;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}
		#content[role=main] .content .body {
			border-top: 2px solid #FD980E;
			padding: 10px 8px;
		}
		#content[role=main] .content .body table {
			border: 1px solid #E4E4E4;
			width: 100%;
		}
		#content[role=main] .content .body table th {
			color: #878787;
			font-size: 1em;
			text-align: left;
			padding: 13px 10px;
			width: 20%;
		}
		#content[role=main] .content .body table td {
			text-align: left;
			width: 30%;
		}
		#content[role=main] .content .body hr {
			margin-left: 8px;
			margin-right: 8px;
		}

		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}
		.aside.member .menu li {
			margin: 8px 0;
		}
		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		.aside.member .menu b {
			color: #ff0000; font-weight:bold;
		}
		</style>
		<script type="text/javascript">
		$(function(){

		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<aside class="aside member">
				<h2>会员专区</h2>
				<nav>
					<ul class="menu">
						<li><a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>"><b>会员资料</b></a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>">站内公告</a></li>
						<?php /*<li><a href="<?php echo $this->config->default_main; ?>/member/service?<?php echo $this->tplVar['status']['args']; ?>">客服记录</a></li>*/?>
						<li><a href="<?php echo $this->config->default_main; ?>/member/history?<?php echo $this->tplVar['status']['args']; ?>">交易纪录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/order?<?php echo $this->tplVar['status']['args']; ?>">订单记录</a></li>
					</ul>

				</nav>
			</aside>
			<div class="content">
				<section class="block-frame">
					<h2>会员基本资料</h2>
					<div class="body">
						<table>
							<tbody>
								<tr>
									<th>Email:</th>
									<td class="email"><?php echo $this->tplVar['table']['record'][0]['email']; ?></td>
									<th>登入账号:</th>
									<td class="name">
									<?php if (empty($this->tplVar['table']['record'][0]['name'])) : ?>
										<?php echo $this->tplVar['table']['record'][0]['provider_name']; ?>
									<?php else : ?>
										<?php echo $this->tplVar['table']['record'][0]['name']; ?>
									<?php endif; ?>
									</td>
								</tr>
								<tr>
									<th>昵称:</th>
									<td class="nickname"><?php echo $this->tplVar['table']['record'][0]['nickname']; ?></td>
									<th>身份验证码:</th>
									<td class=""></td>
								</tr>
								<tr>
									<th>杀币:</th>
									<td class="spoint"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['spoint'][0]['amount']); ?></td>
									<th>红利积点:</th>
									<td class="bonus"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['bonus'][0]['amount']); ?></td>
								</tr>
								<tr>
									<th>性别:</th>
									<td class="gender"><?php echo $this->tplVar['table']['record'][0]['gender']; ?></td>
									<th>店点:</th>
									<td class="gift"><?php echo sprintf("%.2f", $this->tplVar['table']['rt']['gift'][0]['amount']); ?></td>
								</tr>
								<tr>
									<th>收件人:</th>
									<td class="addressee"><?php echo $this->tplVar['table']['record'][0]['addressee']; ?></td>
									<th>联络电话:</th>
									<td class="phone"><?php echo $this->tplVar['table']['record'][0]['phone']; ?></td>
								</tr>
								<tr>
									<th>收货地址:</th>
									<td class="address"><?php echo $this->tplVar['table']['record'][0]['area'].$this->tplVar['table']['record'][0]['address']; ?></td>
									<th></th>
									<td class=""></td>
								</tr>
								<tr>
									<td colspan="4"><hr></td>
								</tr>
								<tr>
									<th>更改密码:</th>
									<td class=""></td>
									<td class="sms_auth" colspan="2"></td>
								</tr>
							</tbody>
						</table>
					</div>
				</section>

			</div>
			<div class="clear"></div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>