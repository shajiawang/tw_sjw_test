<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content[role=main] .content {
			float: left;
			width: 792px;
			border: 1px solid #C1C1C1;
		}

		#content[role=main] .content .block-frame h2 {
			padding-left: 16px;
			font-size: 1.1em;
			font-weight: bold;
			height: 60px;
			line-height: 60px;
			vertical-align: middle;
			border-bottom: 1px solid #FDA713;
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #ffffff 48%, #e1dee2 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(48%,#ffffff), color-stop(100%,#e1dee2));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#ffffff 48%,#e1dee2 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e1dee2',GradientType=0 );
		}

		#content[role=main] .content .body {
			border-top: 2px solid #FD980E;
			padding: 10px 8px;
		}

		#content[role=main] .content .body .news-list {
			margin-left: 30px;
		}

		#content[role=main] .content .body .news-list li {
			height: 22px;
			line-height: 22px;
		}

		#content[role=main] .content .body .news-list a {
			max-width: 524px;
			display: inline-block;
			vertical-align: top;
			height: 22px;
			line-height: 22px;
			text-decoration: none;
		}

		#content[role=main] .content .body .news-list span {
			line-height: 22px;
		}

		.aside.member {
			border: 1px solid #777777;
		}

		.aside.member h2 {
			background: #ffffff;
			background: -moz-linear-gradient(top,  #ffffff 0%, #efefef 100%);
			background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(100%,#efefef));
			background: -webkit-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -o-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: -ms-linear-gradient(top,  #ffffff 0%,#efefef 100%);
			background: linear-gradient(to bottom,  #ffffff 0%,#efefef 100%);
			filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#efefef',GradientType=0 );
			height: 40px;
			line-height: 40px;
			color: black;
			text-align: center;
		}

		.aside.member .menu {
			list-style: none;
			text-align: center;
		}

		.aside.member .menu li {
			margin: 8px 0;
		}

		.aside.member .menu li a {
			text-decoration: none;
			color: black;
		}
		
		.aside.member .menu b {
			color: #ff0000; font-weight:bold;
		}
		</style>
		<script type="text/javascript">
		$(function(){

		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		<main role="main" id="content">
			<aside class="aside member">
				<h2>会员专区</h2>
				<nav>
					<ul class="menu">
						<li><a href="<?php echo $this->config->default_main; ?>/member?<?php echo $this->tplVar['status']['args']; ?>">会员资料</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/news?<?php echo $this->tplVar['status']['args']; ?>"><b>站内公告</b></a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/service?<?php echo $this->tplVar['status']['args']; ?>">客服记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/history?<?php echo $this->tplVar['status']['args']; ?>">事务历史记录</a></li>
						<li><a href="<?php echo $this->config->default_main; ?>/member/order?<?php echo $this->tplVar['status']['args']; ?>">订单记录</a></li>
					</ul>

				</nav>
			</aside>
			<div class="content">
				<section class="block-frame">
					<h2>站内公告</h2>
					<div class="body">
						<ol class="news-list">
						<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<li>
								<?php /*
								<a id="pw_btn" href=javascript:slightbox(420,380,'忘记密码？？','<?php echo $luckybuyUrl; ?>/member_pwd.php')></a>
								*/?>
								<a class="ellipsis" href="javascript:slightbox(700,600,'<?php echo $rv['name'];?>','<?php echo $this->config->default_main; ?>/member/news_detail?<?php echo $this->tplVar['status']['args']."&newsid=".$rv['newsid']; ?>')">
									<?php if ($rv['public'] == 'Y') : ?>
									(首页公告)
									<?php else : ?>
									(站内公告)
									<?php endif; ?>
									：<?php echo $rv['name']; ?>
								</a>
								<span>发布日期：<?php echo $rv['ontime']; ?></span>
								<div class="clear"></div>
							</li>
						<?php endforeach; ?>
						</ol>
					</div>
				</section>

			</div>
			<div class="clear"></div>
		</main>
		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>