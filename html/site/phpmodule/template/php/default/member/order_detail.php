<table>
	<thead>
		<tr>
			<th><span>下标时间</span></th>
			<th><span>手续费</span></th>
			<th><span>下标次数</span></th>
		</tr>

	</thead>
	<tbody>
	<?php if (!empty($this->tplVar['table']['record'])) : ?>
	<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
		<tr class="<?php echo $rk%2?"odd":"even"; ?>">
			<td class="insertt"><span><?php echo $rv['insertt']; ?></span></td>
			<td class="saja_fee"><span><?php echo sprintf('%0.2f', $rv['saja_fee'] * $rv['count']); ?></span></td>
			<td class="count"><span><?php echo $rv['count']; ?></span></td>
		</tr>
	<?php endforeach; ?>
	<?php endif; ?>
	</tbody>
</table>