<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>

		<style type="text/css">
		#content {
			height:auto;
			width:100%;
			position:relative;
			z-index:0;
		}
		#content_scroll {
			margin-top:45px;
		}
		#content_edit {
			line-height:25px;
			position:relative;
			left:50%;
			margin-left:-139px;
			margin-top:12px;
		}
		#content_edit div {
			float:left;
			margin:1px;
			padding:5px;
		}
		#content_edit_td1 {
			width:70px;
			height:45px;
			clear:both;
			background-color:#CCC;
		}
		#content_edit_td2 {
			width:180px;
			height:45px;
		}
		#content_edit_td2 input {
			width:180px;
			height:45px;
		}
		#content_edit_ps {
			width:262px;
			clear:both;
		}
		#content_btn {
			text-align:center;
			clear:both;
		}
		#content_btn_registration {
			font-size:16px;
			font-family:Arial;
			font-weight:normal;
			-moz-border-radius:5px;
			-webkit-border-radius:5px;
			border-radius:5px;
			border:1px solid #eeb44f;
			padding:9px 12px;
			text-decoration:none;
			background:-webkit-gradient( linear, left top, left bottom, color-stop(5%, #ffc477), color-stop(100%, #fb9e25) );
			background:-moz-linear-gradient( center top, #ffc477 5%, #fb9e25 100% );
			background:-ms-linear-gradient( top, #ffc477 5%, #fb9e25 100% );
			filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#ffc477', endColorstr='#fb9e25');
			background-color:#ffc477;
			color:#ffffff;
			display:inline-block;
			text-shadow:1px 1px 0px #cc9f52;
			-webkit-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			-moz-box-shadow:inset 1px 1px 0px 0px #fce2c1;
			box-shadow:inset 1px 1px 0px 0px #fce2c1;
			margin-top:10px;
		}
		</style>
	</head>
	<body>
	<?php require_once $this->tplVar['block']['header']; ?>
	
	<div id="page" class="drawer-panel-main" style="position: absolute; z-index: 2; height: 100%; -webkit-transform: translate3d(0, 0, 0); left: 0px; right: 0px;"> 
    <div class="sub-page page-good-list" style="position: absolute; left: 0px; right: 0px; top: 0px; bottom: 0px; -webkit-transform: translate3d(0, 0, 0); z-index: 1; visibility: visible;">
      
	  <div class="header-bar">
        <div class="button left back" data-button="back" style="cursor: pointer;" onclick="window.location='<?php echo $this->config->default_main; ?>/member/?<?php echo $this->tplVar['status']['args']; ?>'" ></div>
        <div class="button right drawer" data-button="drawer" style="cursor: pointer;" onclick="menu()"></div>
        <div class="search-bar"><?php /*
          <form>
            <input type="search" class="search-box" placeholder="请输入查找内容">
            <div class="icon"></div>
          </form>
        */?></div>
		<div class="page-title">修改密码</div>
        <div class="button right search" style="cursor: pointer;"></div>
      </div>
      
        <div class="page-content" data-scroll="" data-zone="">
        <div class="list-wrapper">
		
		<div id="content_scroll"><div id="content">
        
            <form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/member_edit">
			<div id="content_edit">
                <div id="content_edit_td1">旧密码</div><div id="content_edit_td2"><input type="password" name="oldpasswd" id="oldpasswd" /></div>
                <div id="content_edit_td1">新密码</div><div id="content_edit_td2"><input type="password" name="passwd" id="passwd" /></div>
                <div id="content_edit_ps">↑ 以4~12个英文字母或数字为限</div>
                <div id="content_edit_td1">确认密码</div><div id="content_edit_td2"><input type="password" name="repasswd" id="repasswd" /></div>
                <div id="content_edit_ps">↑ 请再次输入密码确认</div>
            </div>
			<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>">
            <div id="content_btn"><input id="content_btn_registration" name="确认" type="submit" value="确认" /></div>
			</form>
			
		</div></div>
		
	    </div>
		</div>
	
	</div>
	</div>
	
	<?php require_once $this->tplVar['block']['footerjs']; ?>
