<?php require_once $this->tplVar['block']['doctype']; ?>
	
<?php require_once $this->tplVar['block']['head']; ?>
		
		<style type="text/css">
		.product .product-category.bonus {
			background-image: url('<?php echo $this->config->path_image; ?>/t_bonus.png');
		}
		.product .product-category.store {
			background-image: url('<?php echo $this->config->path_image; ?>/t_store.png');
		}
		.product .product-category.pcid-1 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-1.png');
		}
		.product .product-category.pcid-2 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-2.png');
		}
		.product .product-category.pcid-3 {
			background-image: url('<?php echo $this->config->path_image; ?>/pcid-3.png');
		}
		.product {
			display: none;
		}
		.labels {
			margin-left: 10px;
			margin-right: 10px;
			margin-bottom: 10px;
			padding-top: 5px;
			padding-left: 10px;
			padding-right: 10px;
			border-bottom: 1px solid #A5A5A5;
		}
		.labels ul {
			list-style: none;
		}
		.labels .label {
			float: left;
		}
		.labels .label a {
			display: block;
			margin: 0 3px;
			padding: 5px 10px;
			border-top: 1px solid #A5A5A5;
			border-left: 1px solid #A5A5A5;
			border-right: 1px solid #A5A5A5;
			text-decoration: none;
			border-top-left-radius: 5px;
			border-top-right-radius: 10px;
			color: #000000;
			background-color: #C9C9C9;
		}
		.labels .label a.active {
			background-color: #FFDD00;
		}
		</style>
		<script type="text/javascript">
		$.fn.displayCountDown = function() {
			var server_time = window.server_time;
			if (!server_time) {
				return this;
			}

			return this.each(function(){
				var $this    = $(this),
					offtime  = parseInt($this.data('offtime')),
					resttime = offtime - server_time,
					sec   = [resttime % 60],
					min   = [Math.floor(resttime % 3600 / 60)],
					hr    = [Math.floor(resttime % 86400 / 3600)],
					day   = [Math.floor(resttime / 86400)];

				if (offtime == 0) {
					return false;
				} 
				else if (resttime <= 0) {
					//window.document.location.reload();
				}
				else {
					sec[0] < 10 && sec.unshift('0');
					min[0] < 10 && min.unshift('0');
					hr[0]  < 10 && hr.unshift('0');
					day[0] < 10 && day.unshift('0');

	    			$this.find('.timer-digit.sec').text(sec.join(''));
	    			$this.find('.timer-digit.min').text(min.join(''));
	    			$this.find('.timer-digit.hr').text(hr.join(''));
	    			$this.find('.timer-digit.day').text(day.join(''));
				}
			});
		}

		$.get('<?php echo $this->config->default_main; ?>/ping/view', function(server_time){
			console && console.log(server_time);
			
			if (isNaN(server_time) || parseInt(server_time) <= 0) {
				alert('系统时间错误!');
				return;
			}

			window.server_time = parseInt(server_time);

			$('.offtime').displayCountDown();

			window.setInterval(function(){
				window.server_time++;
				$('.offtime').displayCountDown();
			}, 1000);
		});

		$(function(){
			$('.offtime').displayCountDown();
		});
		</script>
		
		<script type="text/javascript">
		$(function(){
			var articlePerRow = 3,
				init_type = window.location.hash.match(/type=(\w+\-limit)/);

			$('.product-list .product:visible').each(function(idx, elm){
				if (idx%articlePerRow + 1 == articlePerRow) {
					$(this).addClass('product-last');
				}
			});

			
			$('.label a').on('click', function(){
				var href = $(this).prop('href'),
					idx = href.indexOf('#');

				if (idx !== -1) {
					var match = href.substr(idx+1).match(/type=(\w+\-limit)/),
						type = (match && match[1]) || 'time-limit';

					$('.labels .active').removeClass('active');
					$(this).addClass('active');
					$('.product').hide()
					.filter('.'+type).show();
				}
			});

			if (init_type.length > 1) {
				$('.label a[href$='+init_type[1]+']').trigger('click');
			} else {
				$('.label a:eq(0)').trigger('click');
			}
		})
		</script>
	</head>
	<body>
		<?php require_once $this->tplVar['block']['header']; ?>
		
		<main role="main" id="content">
			<aside class="aside">
				<article class="block news">
					<header>
						<h2><a href="">媒体报导</a></h2>
					</header>
					<img class="img" src="<?php echo $this->config->path_image; ?>/image_1.gif"/>
				</article>
				
				<article class="block testimony">
					<header>
						<h2><a href="">杀友见证</a></h2>
					</header>
					<img class="img" src="<?php echo $this->config->path_image; ?>/image_2.gif"/>
				</article>
				
				<article class="block rule">
					<header>
						<h2><a href="">得标规则</a></h2>
					</header>
					<img class="img" src="<?php echo $this->config->path_image; ?>/image_3.gif"/>
				</article>
				
				<section class="block winner-list">
				<h2><a>最新成交</a></h2>
				<?php foreach($this->tplVar['table']['rt']['pay_get_product'] as $rk => $rv) : ?>
					<article class="winner">
						<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['filename']; ?>">
						<header>
							<h3 class="ellipsis"><a href="<?php echo $this->config->default_main.'/product/history'; ?>"><?php echo $rv['name']; ?></a></h3>
						</header>
						<div class="saja_price ellipsis">得标价：<?php echo sprintf("%0.2f", $rv['price']); ?>元</div>
						<div class="saja_winner ellipsis">杀价王：<?php echo $rv['nickname']; ?></div>
					</article>
				<?php endforeach; ?>
					<div class="more">
						<a href="<?php echo $this->config->default_main.'/product/history'; ?>">更多&nbsp;></a>
					</div>
				</section>
			</aside>
			
			<div class="promote">
				<img src="<?php echo $this->config->path_image; ?>/fb_head.jpg" style="width:792px; height:222px;"/>
			</div>
			
			<div class="product-list">
				<div class="labels">
					<ul class="">
						<li class="label"><a class="active" href="#type=time-limit">时间标</a></li>
						<li class="label"><a href="#type=count-limit">综合标</a></li>
						<?php /*<li class="label"><a href="#type=count-limit">次数标</a></li>*/?>
					</ul>
					<div class="clear"></div>
				</div>
			
			<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
			
				<?php /*<article class="product <?php echo ((int)$rv['offtime']) ? "time-limit" : "count-limit"; ?>" style="display: block;">*/?>
				<article class="product <?php echo ($rv['bid_type']=='T') ? "time-limit" : "count-limit"; ?>" style="display: block;">
					<div class="product-category-rt">
						
						<?php if (!empty($rv['bonus'])) : ?>
						<div class="product-category bonus">
							<span class="product-category-name" title="红利回馈%">
								<?php if ($rv['bonus_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['bonus']); ?>%
								<?php elseif ($rv['bonus_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['bonus']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
						<?php endif; ?>
						
						<?php /*$gift = floatval($this->tplVar['table']['record'][0]['gift']); ?>
						<?php if (!empty($gift)) : ?>
						<div class="product-category store">
							<span class="product-category-name" title="店点回馈%">
								<?php if ($rv['gift_type'] == 'ratio') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['gift']); ?>%
								<?php elseif ($rv['gift_type'] == 'value') : ?>
									<?php echo (float)sprintf("%0.2f", $rv['gift']); ?>点
								<?php else : ?>
									0
								<?php endif; ?>
							</span>
						</div>
						<?php endif; */?>
						
						<?php if (!empty($rv['srid']) ) {
						if($rv['srid']=='any_saja'){
						} elseif($rv['srid']=='never_win_saja'){ ?>
							<div class="product-category pcid-1">
								<span class="product-category-name" title="新手限定(限中标次数 5 次以下)" >限新手</span>
							</div>
						<?php } elseif($rv['srid']=='le_win_saja'){ ?>
							<div class="product-category pcid-1">
								<span class="product-category-name" title="(限中标次数 <?php echo $rv['value'];?> 次以下)" >限新手</span>
							</div>
						<?php } elseif($rv['srid']=='ge_win_saja'){ ?>
							<div class="product-category pcid-1">
								<span class="product-category-name" title="(限中标次数 <?php echo $rv['value'];?> 次以上)" >限老手</span>
							</div>
						<?php 
						} }//endif; ?>
						
						<?php //区域
						/*if ($rv['loc_type'] == 'G') : ?>
						<div class="product-category pcid-2">
							<span class="product-category-name" title="区域" >全国</span>
						</div>
						<?php endif; */?>
							
						<?php if ((float)$rv['saja_fee'] == 0.00) : ?>
						<div class="product-category pcid-3">
							<span class="product-category-name" title="免费(出价免扣杀币)" >免费</span>
						</div>
						<?php endif; ?>
					
					</div>
					
					<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
				<?php if (!empty($rv['thumbnail'])) : ?>
					<img class="thumbnail" src="<?php echo $this->config->path_image.'/product/'.$rv['thumbnail']; ?>"/>
				<?php elseif (!empty($rv['thumbnail_url'])) : ?>
					<img class="thumbnail" src="<?php echo $rv['thumbnail_url']; ?>"/>
				<?php else : ?>
					<img class="thumbnail" src="https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBMdXQgXPPBOQs3Ib-WWE-Prt3ZVv4rbrjUMurmY8iYo8hRo9vzw"/>
				<?php endif; ?>
					</a>
					<header>
						<h3>
							<a class="name ellipsis" href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>"><?php echo $rv['name']; ?></a>
						</h3>
					</header>
					
					<?php if (!empty($rv['offtime'])) : ?>
					<div class="offtime" data-offtime="<?php echo $rv['offtime']; ?>">
						<span class="timer-digit day">99</span>
						<span class="timer-unit day">天</span>
						<span class="timer-digit hr">23</span>
						<span class="timer-unit hr">时</span>
						<span class="timer-digit min">03</span>
						<span class="timer-unit min">分</span>
						<span class="timer-digit sec">01</span>
						<span class="timer-unit sec">秒</span>
					</div>
					<?php endif; ?>
					
					<?php //會員得標次數 $this->tplVar['table']['rt']['user_get_bid']?>
					<div class="saja-button">
						<a href="<?php echo $this->config->default_main.'/product/saja?'.$this->tplVar['status']['args'].'&productid='.$rv['productid']; ?>">
							<img src="<?php echo $this->config->path_image; ?>/b_bid.png">
						</a>
					</div>
					
					<table class="info">
						<tbody>
							<td class="label">价值：</td>
							<td class="value retail_price"><?php echo number_format($rv['retail_price'], 2); ?></td>
							<td class="unit">元</td>
							<td class="label">关注：</td>
							<td class="value attention">987,654,321</td>
							<td class="unit">次</td>
						</tbody>
					</table>
				</article>
			
			<?php endforeach; ?>
			</div>
			<div class="clear"></div>
		</main>

		<?php require_once $this->tplVar['block']['footer']; ?>
	</body>
</html>