<?php
$time_start = microtime(true);

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "判斷會員前 $time 秒\n";

if (empty($this->io->input['session']['user'])) {
	$this->jsonPrintMsg('请先登入会员账号', 'login_first');
	//$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "判斷會員後 $time 秒\n";

$userid = $this->io->input['session']['user']["userid"];

$countryid = 2;//國家ID
$currency_unit = 0.01;//貨幣最小單位
$last_rank = 12;//預設順位提示（順位排名最後一位）
$saja_time_limit = 0;//每次下標時間間隔限制
$display_rank_time_limit = 1;//（秒）下標後離結標還有n秒才顯示順位
$range_saja_time_limit = 60;//（秒）離結標n秒以上，才可連續下標

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標時間前 $time 秒\n";

//檢查最後下標時間
$query = "
SELECT 
unix_timestamp(MAX(insertt)) as insertt,
unix_timestamp() as `now`
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` 
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND userid = '{$this->io->input['session']['user']['userid']}' 
AND switch = 'Y'
";
$table = $this->model->getQueryRecord($query);

if (!empty($table['table']['record'])) {
	if ((int)$table['table']['record'][0]['now'] - (int)$table['table']['record'][0]['insertt'] <= $saja_time_limit) {
    	$this->jsonPrintMsg('下标时间间隔过短，请稍后再下标!', 'saja_time_limit');
	}
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標時間後 $time 秒\n";

/*
'saja_time_limit' 這個是預留以後要多語系用的, 用這個 id 去抓出目前對應語系的文字
*/

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查商品存在＋截標前 $time 秒\n";

//讀取商品資料
$query ="
SELECT 
p.*,
unix_timestamp(p.offtime) as offtime,
unix_timestamp() as `now`
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND p.productid = '".$this->io->input['post']['productid']."'
AND p.display = 'Y'
AND p.switch = 'Y'
GROUP BY p.productid
LIMIT 1
" ;
// echo '<pre>';echo $query;exit;
$table = $this->model->getQueryRecord($query);

if (empty($table['table']['record'])) {
    $this->jsonPrintMsg('商品不存在!', 'product_not_exist');
}

$product = $table['table']['record'][0];

if (
	($product['offtime'] == 0 && $product['locked'] == 'Y')
	|| ($product['offtime'] > 0 && $product['now'] > $product['offtime'])
) {
    $this->jsonPrintMsg('商品已结标!', 'product_closed');
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查商品存在＋截標後 $time 秒\n";

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標金額前 $time 秒\n";

//檢查下標金額
switch ($this->io->input['post']['type']) 
{
	case 'single': // 單次下標
		if (empty($this->io->input['post']['price'])) {
		    $this->jsonPrintMsg('金额不可空白!', 'price_empty');
		}
		else if (!preg_match("/^[\d]+[\.]*[\d]{0,2}$/", $this->io->input['post']['price'])) {
		    $this->jsonPrintMsg('金额格式错误!', 'price_error');
		}
		else if ((float)$this->io->input['post']['price'] < (float)$product['price_limit']) {
		    $this->jsonPrintMsg('金额低于竞标底价!', 'price_too_low');
		}

		//殺價記錄數量
		$tickets = 1;
		
		//殺價起始價格
		$price = (float)$this->io->input['post']['price'];
		break;
	case 'range': // 連續下標
		if ($product['offtime'] > 0 && $product['offtime'] - $product['now'] < $range_saja_time_limit) {
		    $this->jsonPrintMsg('超过可连续下标时间!', 'over_range_saja_time_limit');
		}

		if (
			empty($this->io->input['post']['price_start']) 
			|| empty($this->io->input['post']['price_stop'])
		) {
		    $this->jsonPrintMsg('金额不可空白!', 'price_empty');
		}
		else if (
			//!preg_match("/^[\d]+[\.]*[\d]{0,2}$/", $this->io->input['post']['price_start'])
			//|| !preg_match("/^[\d]+[\.]*[\d]{0,2}$/", $this->io->input['post']['price_stop'])
			!is_numeric($this->io->input['post']['price_start']) 
			|| !is_numeric($this->io->input['post']['price_stop'])
		) {
		    $this->jsonPrintMsg('金额格式错误!', 'price_error');
		}
		else if ((float)$this->io->input['post']['price_start'] > (float)$this->io->input['post']['price_stop']) {
		    $this->jsonPrintMsg('起始价格不可超过结束价格!', 'over_price_stop');
		}
		else if ((float)$this->io->input['post']['price_stop'] > (float)$product['retail_price']) {
		    $this->jsonPrintMsg('金额超过商品市价!', 'over_retail_price');
		}
		else if ((float)$this->io->input['post']['price_start'] < (float)$product['price_limit']) {
		    $this->jsonPrintMsg('金额低于竞标底价!', 'lower_price_limit');
		}

		//殺價記錄數量
		$tickets = ((float)$this->io->input['post']['price_stop'] - (float)$this->io->input['post']['price_start']) / $currency_unit + 1;
		
		if ($tickets > (int)$product['usereach_limit']) {
		    $this->jsonPrintMsg('超过可连续下标次数限制!', 'over_usereach_limit');
		}
		
		//殺價起始價格
		$price = (float)$this->io->input['post']['price_start'];
		break;
	default:
	    $this->jsonPrintMsg('下标类别错误', 'saja_type_error');
		break;
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標金額後 $time 秒\n";

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標次數前 $time 秒\n";

//檢查使用者下標次數限制
if ((int)$product['user_limit'] > 0) 
{
	$query = "
	SELECT 
	count(*) as count
	FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` 
	WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND productid = '".$this->io->input['post']['productid']."'
	AND userid = '{$this->io->input['session']['user']['userid']}' 
	AND switch = 'Y'
	";
	$table = $this->model->getQueryRecord($query);
	$tickets_avaiable = (int)$product['user_limit'] - (int)$table['table']['record'][0]['count'];

	if ($tickets > $tickets_avaiable) {
	    $this->jsonPrintMsg('超过可下标次数限制!', 'over_user_limit');
	}
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標次數後 $time 秒\n";

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查殺幣or禮券+使用殺幣or禮券前 $time 秒\n";

$values = array();
if ($this->io->input['post']['use_sgift'] == 'Y') 
{
	//檢查禮券數量
	$query ="
	SELECT 
	*
	FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}sgift` 
	WHERE 
	prefixid = '{$this->config->default_prefix_id}'
	AND productid = '{$this->io->input["post"]["productid"]}'
	AND userid = '{$this->io->input['session']['user']['userid']}'
	AND used = 'N'
	AND switch = 'Y'
	ORDER BY seq
	";
	$table = $this->model->getQueryRecord($query);
	if (empty($table['table']['record'])) {
		$sgift = 0;
	}
	else {
		$sgift = count($table['table']['record']);//殺價禮券數量
	}

	if ($tickets > $sgift) {
	    $this->jsonPrintMsg('下标礼券不足!', 'sgift_not_enough');
	}

	//使用禮券
	for($i = 0 ; $i < $tickets ; $i++) {
		$query = "
		UPDATE `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}sgift` 
		SET 
		used = 'Y',
		used_time = NOW()
		WHERE
		prefixid = '{$this->config->default_prefix_id}'
		AND sgiftid = '{$table['table']['record'][$i]['sgiftid']}'
		AND switch = 'Y'
		";
		$res = $this->model->query($query);
		if (!$res) {
	    	$this->jsonPrintMsg('使用下标礼券错误!', 'sgift_error');
		}

		//殺價下標歷史記錄（對應下標記錄與下標禮券id）
		$values[] = "
		(
			'".$this->config->default_prefix_id."',
			'".$this->io->input['post']['productid']."',
			'".$this->io->input['session']['user']['userid']."',
			'0',
			'{$table['table']['record'][$i]['sgiftid']}',
			'".($price + $i * $currency_unit)."',
			NOW()
		)";
	}
}
else 
{
	//檢查殺幣餘額
	$query = "
	SELECT 
	SUM(amount) as amount
	FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` 
	WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND userid = '".$this->io->input['session']['user']['userid']."'
	AND countryid = '{$countryid}'
	AND switch = 'Y'
	";
	$table = $this->model->getQueryRecord($query);

	$saja_fee = (int)$tickets * (float)$product['saja_fee'];//殺價手續費
	if ($saja_fee > (float)$table['table']['record'][0]['amount']) {
	    $this->jsonPrintMsg('杀币不足!', 'spoint_not_enough');
	}

	//使用殺幣
	$query = "
	INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` 
	(
		`prefixid`,
		`userid`,
		`countryid`,
		`behav`,
		`amount`,
		`insertt`
	)
	VALUE
	(
		'".$this->config->default_prefix_id."',
		'".$this->io->input['session']['user']['userid']."',
		'".$countryid."',
		'user_saja',
		'".-$saja_fee."',
		NOW()
	)
	";
	$res = $this->model->query($query);
	$spointid = $this->model->_con->insert_id;

	for ($i = 0 ; $i < $tickets ; $i++) {
		//殺價下標歷史記錄（對應下標記錄與使用殺幣記錄id）
		$values[] = "
		(
			'".$this->config->default_prefix_id."',
			'".$this->io->input['post']['productid']."',
			'".$this->io->input['session']['user']['userid']."',
			'{$spointid}',
			'0',
			'".($price + $i * $currency_unit)."',
			NOW()
		)";
	}
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查殺幣or禮券+使用殺幣or禮券後 $time 秒\n";

if (empty($values)) {
	$this->jsonPrintMsg('下标失败!', 'saja_failed');
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "記錄下標前 $time 秒\n";

//產生下標歷史記錄
$query = "
INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` 
(
	prefixid,
	productid,
	userid,
	spointid,
	sgiftid,
	price,
	insertt
)
VALUES
".implode(',', $values)."
";
$res = $this->model->query($query);

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "記錄下標後 $time 秒\n";

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "提示順位前 $time 秒\n";

//單次下標而且離結標還有十分鐘以上，回傳目前順位
//if ($this->io->input['post']['type'] == 'single' && $product['offtime'] - $product['now'] > $display_rank_time_limit) {
if ($this->io->input['post']['type'] == 'single') 
{
	$rank = $last_rank;//最大顯示排名為第12位

    $query = "
    SELECT
    h.*,
    up.nickname
    FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
    LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
    	h.prefixid = up.prefixid
    	AND h.userid = up.userid
    	AND up.switch = 'Y'
    WHERE
	h.prefixid = '{$this->config->default_prefix_id}'
	AND h.productid = '{$this->io->input["post"]["productid"]}'
	AND h.switch = 'Y'
	GROUP BY h.price
	HAVING COUNT(*) = 1
	ORDER BY h.price ASC
	LIMIT {$last_rank}
    ";
	$table = $this->model->getQueryRecord($query);

	if (!empty($table['table']['record'])) 
	{
		//回傳中標者
		$ret['winner'] = $table['table']['record'][0]['nickname']; 

		foreach($table['table']['record'] as $rk => $rv) {
			if ($rv['price'] == $this->io->input['post']['price']) {
				$rank = $rk + 1;
				break;
			}
		}
	}

	//回傳下标顺位
	$ret['rank'] = $rank;
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "提示順位後 $time 秒\n";

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標次數+超過下標次數截標前 $time 秒\n";

if ((int)$product['saja_limit'] > 0) 
{
	//如果有下標次數限制，則檢查目前已下標次數
	$query = "
	SELECT 
	COUNT(*) as count 
	FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` 
	WHERE 
	prefixid = '{$this->config->default_prefix_id}'
	AND productid = '{$this->io->input["post"]["productid"]}'
	AND switch = 'Y'
	";
	$table = $this->model->getQueryRecord($query);

	//若下標次數大於標數限制，則鎖定殺價商品
	if ((int)$table['table']['record'][0]['count'] >= (int)$product['saja_limit']) {
		$query = "
		UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` 
		SET 
		locked = 'Y'
		WHERE 
		prefixid = '".$this->config->default_prefix_id."' 
		AND productid = '".$this->io->input['post']['productid']."'
		AND switch = 'Y'		
		";
		$res = $this->model->query($query);
	}
}

$time_end = microtime(true);
$time = $time_end - $time_start;
echo "檢查下標次數+超過下標次數截標前 $time 秒\n";

//回傳下标狀態
$ret['msg'] = '下标成功!';
$ret['status'] = 1;
echo json_encode($ret);
