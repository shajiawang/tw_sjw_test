<?php
##############################################################################################################################################
#// Status Start 

if (empty($this->io->input["get"]["channelid"])) {
	$this->io->input["get"]["channelid"] = 1;
	// $this->jsPrintMsg('经销商错误!!', $this->config->default_main);
}
if (empty($this->io->input["get"]["productid"])) {
	$this->jsPrintMsg('商品ID错误!!', $this->config->default_main);
}


// Channel Start
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Channel End

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if(!empty($this->io->input["get"]["fun"])){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if(!empty($this->io->input["get"]["act"])){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
// Sort End

// Page Start
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
$query ="
SELECT 
p.*,
unix_timestamp(p.offtime) as offtime,
unix_timestamp() as `now`,
pt.filename
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
	
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND unix_timestamp() >= unix_timestamp(p.ontime)
AND p.display = 'Y'
AND p.productid = '{$this->io->input["get"]["productid"]}'
AND p.switch = 'Y'
" ;
// echo '<pre>';echo $query;exit;
$table = $this->model->getQueryRecord($query);

//中标处理费 = 市價 * 处理费% * 兌換比率
$process_fee = ( (float)$table['table']['record'][0]['process_fee']/100 ) * (float)$table['table']['record'][0]['retail_price']  * $this->config->sjb_rate;
$table['table']['record'][0]['process_fee'] = $process_fee;

// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start
 
// Relation product_category 分類
$query ="
SELECT * FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND channelid = '{$this->io->input["get"]["channelid"]}'
AND switch = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

// Relation pay_get_product 商品得標記錄 
$query ="
SELECT * FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	pgp.prefixid = up.prefixid
	AND pgp.userid = up.userid 
	AND up.switch = 'Y'
WHERE 
pgp.prefixid = '".$this->config->default_prefix_id."'
AND pgp.productid = '{$this->io->input["get"]["productid"]}'
AND pgp.switch = 'Y'
AND up.userid IS NOT NULL
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["pay_get_product"] = $recArr['table']['record'];

if (!empty($this->io->input['session']['user'])) {
	$query ="
	SELECT 
	* 
	FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."sgift` 
	WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND productid = '{$this->io->input["get"]["productid"]}'
	AND userid = '{$this->io->input['session']['user']['userid']}'
	AND used = 'N'
	AND switch = 'Y'
	ORDER BY seq
	";
	$recArr = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["sgift"] = $recArr['table']['record'];

	$query = "
	SELECT
	up.*
	FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh 
	LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
		sh.prefixid = up.prefixid
		AND sh.userid = up.userid
		AND up.switch = 'Y'
	WHERE
	sh.prefixid = '{$this->config->default_prefix_id}'
	AND sh.productid = '{$this->io->input["get"]["productid"]}'
	AND sh.switch = 'Y'
	AND up.userid IS NOT NULL
	GROUP BY sh.price
	HAVING COUNT(*) = 1
	ORDER BY sh.price ASC
	LIMIT 1
	";
	$recArr = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["history"] = $recArr['table']['record'];
}

// Relation user_get_bid 會員得標次數
$table["table"]["rt"]["user_get_bid"] = '';
if(isset($this->io->input['session']['user']['userid']) )
{
	$query ="
	SELECT count(*) user_bid FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}pay_get_product` pg 
	WHERE 
	pg.prefixid = '{$this->config->default_prefix_id}'
	AND pg.userid = '{$this->io->input['session']['user']['userid']}'
	AND pg.switch = 'Y'
	";
	$recArr = $this->model->getQueryRecord($query); 
	$table["table"]["rt"]["user_get_bid"] = (int)$recArr['table']['record'][0]['user_bid'];
}

// Relation product_rule_rt 下標條件關聯
$query ="SELECT pr.srid, pr.value
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt` pr
WHERE 
pr.prefixid = '{$this->config->default_prefix_id}'
AND pr.productid = '{$this->io->input["get"]["productid"]}'
AND pr.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query); 
$table["table"]["rt"]["saja_rule"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();