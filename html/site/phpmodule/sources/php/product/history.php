<?php
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$this->config->max_page = 10;

##############################################################################################################################################
#// Status Start 

if (empty($this->io->input["get"]["channelid"])) {
	 $this->io->input["get"]["channelid"] = 1;
	// $this->jsPrintMsg('经销商错误!!', $this->config->default_main);
}

// Channel Start
$status["status"]["args"] = array();
if(empty($this->io->input["get"]["channelid"])){
	if (empty($this->io->input['session']['channelid'])) {
		$this->io->input['session']['channelid'] = 1;
	}
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
}
$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
// Channel End

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
$status["status"]["sort"] = "";
$sub_sort_query = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(name|description|seq)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End

// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON 
	pgp.prefixid = p.prefixid
	AND pgp.productid = p.productid
	AND p.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt` sp ON 
	pgp.prefixid = sp.prefixid
	AND pgp.productid = sp.productid
	AND sp.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	sp.prefixid = cs.prefixid
	AND sp.storeid = cs.storeid
	AND cs.channelid = '".$this->io->input["get"]["channelid"]."'
	AND cs.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	pgp.prefixid = up.prefixid
	AND pgp.userid = up.userid
	AND up.switch = 'Y'
WHERE 
pgp.prefixid = '".$this->config->default_prefix_id."' 
AND pgp.switch = 'Y'
AND p.productid IS NOT NULL
AND sp.productid IS NOT NULL
AND cs.channelid IS NOT NULL
AND up.userid IS NOT NULL 
ORDER BY pgp.insertt DESC
" ;
// $query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT 
p.*,
unix_timestamp(p.offtime) as offtime,
unix_timestamp() as `now`,
pgp.price,
pgp.insertt,
up.nickname,
pt.filename
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON 
	pgp.prefixid = p.prefixid
	AND pgp.productid = p.productid
	AND p.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt` sp ON 
	pgp.prefixid = sp.prefixid
	AND pgp.productid = sp.productid
	AND sp.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	sp.prefixid = cs.prefixid
	AND sp.storeid = cs.storeid
	AND cs.channelid = '".$this->io->input["get"]["channelid"]."'
	AND cs.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	pgp.prefixid = up.prefixid
	AND pgp.userid = up.userid
	AND up.switch = 'Y'
WHERE 
pgp.prefixid = '".$this->config->default_prefix_id."' 
AND pgp.switch = 'Y'
AND p.productid IS NOT NULL
AND sp.productid IS NOT NULL
AND cs.channelid IS NOT NULL
AND up.userid IS NOT NULL 
ORDER BY pgp.insertt DESC
" ;
// $query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
// echo '<pre>';echo $query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
* 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND channelid = '{$this->io->input["get"]["channelid"]}'
AND switch = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();