<?php
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

if (empty($this->io->input["get"]["channelid"])) {
	$this->io->input["get"]["channelid"] = 1;
	// $this->jsPrintMsg('经销商错误!!', $this->config->default_main);
}
if (empty($this->io->input["get"]["pcid"])) {
	//$this->jsPrintMsg('商品分类错误!!', $this->config->default_main);
}


// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
$status["status"]["sort"] = "";
$sub_sort_query = " ORDER BY p.offtime ASC"; 
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(name|description|seq)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = ($this->io->input['session']['m_prefix']=='m') ? " AND p.mob_type !='N'" : "";
$sub_search_query .= " AND p.closed = 'N' 
AND p.display = 'Y' 
AND p.switch = 'Y' 
AND unix_timestamp() >= unix_timestamp(p.ontime)
AND (
	(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
	OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N')
)";
$category_search_query = "";
if(!empty($this->io->input["get"]["search_name"])){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .= " AND p.`name` like '%".$this->io->input["get"]["search_name"]."%'";
}
if(!empty($this->io->input["get"]["search_pcid"])){
	$status["status"]["search"]["search_pcid"] = $this->io->input["get"]["search_pcid"] ;
	$status["status"]["search_path"] .= "&search_pcid=".$this->io->input["get"]["search_pcid"] ;
	$category_search_query .=  "
	LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt` pc ON
	p.prefixid = pc.prefixid
	AND p.productid = pc.productid
	AND pc.`pcid` = '{$this->io->input["get"]["search_pcid"]}'
	AND pc.switch = 'Y' ";
	$sub_search_query .= " AND pc.pcid IS NOT NULL";
}
if(!empty($this->io->input["get"]["pcid"])){
	$status["status"]["search"]["pcid"] = $this->io->input["get"]["pcid"] ;
	$status["status"]["search_path"] .= "&pcid=".$this->io->input["get"]["pcid"] ;
	$category_search_query .=  "
	LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt` pc ON
	p.prefixid = pc.prefixid
	AND p.productid = pc.productid
	AND pc.`pcid` = '{$this->io->input["get"]["pcid"]}'
	AND pc.switch = 'Y' ";
	$sub_search_query .= " AND pc.pcid IS NOT NULL";
}
// Search End

// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt` sp ON 
	p.prefixid = sp.prefixid
	AND p.productid = sp.productid
	AND sp.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	sp.prefixid = cs.prefixid
	AND sp.storeid = cs.storeid
	AND cs.channelid = '".$this->io->input["get"]["channelid"]."'
	AND cs.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
{$category_search_query}
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND sp.productid IS NOT NULL
AND cs.channelid IS NOT NULL
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT 
p.*,
unix_timestamp(p.offtime) as offtime,
unix_timestamp() as `now`,
pt.filename as thumbnail
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt` sp ON 
	p.prefixid = sp.prefixid
	AND p.productid = sp.productid
	AND sp.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	sp.prefixid = cs.prefixid
	AND sp.storeid = cs.storeid
	AND cs.channelid = '".$this->io->input["get"]["channelid"]."'
	AND cs.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
{$category_search_query}
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND sp.productid IS NOT NULL
AND cs.channelid IS NOT NULL
" ; 
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query); 

// Table Record End 

// Relation product_rule_rt 下標條件關聯
if($table['table']['record'])
{
	foreach($table['table']['record'] as $tk => $tv)
	{
		$productid = $tv['productid'];
		$table['table']['record'][$tk]['srid'] = '';
		$table['table']['record'][$tk]['value'] = '';
		
		$query ="SELECT pr.srid, pr.value
		FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt` pr
		WHERE 
		pr.prefixid = '{$this->config->default_prefix_id}'
		AND pr.productid = '{$productid}'
		AND pr.switch = 'Y'
		";
		$recArr = $this->model->getQueryRecord($query); 
		if(!empty($recArr['table']['record'][0]) ) {
			$table['table']['record'][$tk]['srid'] = $recArr['table']['record'][0]['srid'];
			$table['table']['record'][$tk]['value'] = $recArr['table']['record'][0]['value'];
		}
	}
}

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
prefixid = '{$this->config->default_prefix_id}' 
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

// Relation user_get_bid 會員得標次數
$table["table"]["rt"]["user_get_bid"] = '';
if(isset($this->io->input['session']['user']['userid']) )
{
	$query ="
	SELECT count(*) user_bid FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}pay_get_product` pg 
	WHERE 
	pg.prefixid = '{$this->config->default_prefix_id}'
	AND pg.userid = '{$this->io->input['session']['user']['userid']}'
	AND pg.switch = 'Y'
	";
	$recArr = $this->model->getQueryRecord($query); 
	$table["table"]["rt"]["user_get_bid"] = (int)$recArr['table']['record'][0]['user_bid'];
}

// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();