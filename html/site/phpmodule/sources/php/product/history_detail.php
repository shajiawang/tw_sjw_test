<?php
##############################################################################################################################################
#// Status Start 

if (empty($this->io->input["get"]["channelid"])) {
	$this->io->input["get"]["channelid"] = 1;
	// $this->jsPrintMsg('经销商错误!!', $this->config->default_main);
}
if (empty($this->io->input["get"]["productid"])) {
	$this->jsPrintMsg('商品ID错误!!', $this->config->default_main);
}


// Channel Start
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Channel End

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if(!empty($this->io->input["get"]["fun"])){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if(!empty($this->io->input["get"]["act"])){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
// Sort End

// Page Start
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
/*
$query ="
SELECT 
p.*,
unix_timestamp(p.offtime) as offtime,
unix_timestamp() as `now`,
pt.filename
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y' 
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND unix_timestamp() >= unix_timestamp(p.ontime)
AND p.display = 'Y'
AND p.productid = '{$this->io->input["get"]["productid"]}'
AND p.switch = 'Y'
" ;
// echo '<pre>';echo $query;exit;
$table = $this->model->getQueryRecord($query);
*/

$query ="
SELECT 
p.*,
unix_timestamp(p.offtime) as offtime,
unix_timestamp() as `now`,
pgp.price,
pgp.insertt,
up.nickname,
pt.filename
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON 
	pgp.prefixid = p.prefixid
	AND pgp.productid = p.productid
	AND p.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt` sp ON 
	pgp.prefixid = sp.prefixid
	AND pgp.productid = sp.productid
	AND sp.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	sp.prefixid = cs.prefixid
	AND sp.storeid = cs.storeid
	AND cs.channelid = '".$this->io->input["get"]["channelid"]."'
	AND cs.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	pgp.prefixid = up.prefixid
	AND pgp.userid = up.userid
	AND up.switch = 'Y'
WHERE 
pgp.prefixid = '".$this->config->default_prefix_id."' 
AND pgp.switch = 'Y'
AND p.productid IS NOT NULL
AND sp.productid IS NOT NULL
AND cs.channelid IS NOT NULL
AND up.userid IS NOT NULL
AND p.productid = '{$this->io->input["get"]["productid"]}'
" ;
$table = $this->model->getQueryRecord($query);

// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
* 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND channelid = '{$this->io->input["get"]["channelid"]}'
AND switch = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

$query ="
SELECT 
* 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	pgp.prefixid = up.prefixid
	AND pgp.userid = up.userid 
	AND up.switch = 'Y'
WHERE 
pgp.prefixid = '".$this->config->default_prefix_id."'
AND pgp.productid = '{$this->io->input["get"]["productid"]}'
AND pgp.switch = 'Y'
AND up.userid IS NOT NULL
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["pay_get_product"] = $recArr['table']['record'];

if (!empty($this->io->input['session']['user'])) {
	$query ="
	SELECT 
	* 
	FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."sgift` 
	WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND productid = '{$this->io->input["get"]["productid"]}'
	AND userid = '{$this->io->input['session']['user']['userid']}'
	AND used = 'N'
	AND switch = 'Y'
	ORDER BY seq
	";
	$recArr = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["sgift"] = $recArr['table']['record'];

	$query = "
	SELECT
	up.*
	FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh 
	LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
		sh.prefixid = up.prefixid
		AND sh.userid = up.userid
		AND up.switch = 'Y'
	WHERE
	sh.prefixid = '{$this->config->default_prefix_id}'
	AND sh.productid = '{$this->io->input["get"]["productid"]}'
	AND sh.switch = 'Y'
	AND up.userid IS NOT NULL
	GROUP BY sh.price
	HAVING COUNT(*) = 1
	ORDER BY sh.price ASC
	LIMIT 1
	";
	$recArr = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["history"] = $recArr['table']['record'];
}
// Relation End 
##############################################################################################################################################




$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();