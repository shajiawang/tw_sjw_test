<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

if (empty($this->io->input['post']['driid'])) {
	$this->jsAlertMsg('请先选择充值额度');
}


$countryid = 2;
$currency  = 'RMB';

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

require_once "saja/convertString.ini.php";
$this->str = new convertString();
##############################################################################################################################################
// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
// $query ="
// SELECT 
// *
// FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history`
// WHERE 
// prefixid   = '".$this->config->default_prefix_id."' 
// AND userid = '{$this->io->input["session"]['user']["userid"]}'
// AND status = 'order'
// AND switch = 'Y'
// " ;
// $table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################



##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
* 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` 
WHERE 
prefixid      = '".$this->config->default_prefix_id."'
AND channelid = '{$this->io->input["get"]["channelid"]}'
AND switch    = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

$query = "
SELECT 
dri.*,
dr.name as rule_name,
dr.logo
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri 
LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr ON 
	dri.prefixid = dr.prefixid
	AND dri.drid = dr.drid 
	AND dr.switch = 'Y'
WHERE 
dri.prefixid   = '".$this->config->default_prefix_id."' 
AND dri.driid  = '{$this->io->input["post"]["driid"]}'
AND dri.switch = 'Y'
AND dr.drid IS NOT NULL 
";
$recArr = $this->model->getQueryRecord($query);
$table['table']['rt']['deposit_rule_item'] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################

//If order not exist, create order
if (empty($table['table']['record'])) {
	$query = "
	INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history`
	(
		prefixid,
		userid,
		driid,
		status,
		insertt
	)
	VALUE 
	(
		'".$this->config->default_prefix_id."' ,
		'{$this->io->input["session"]['user']["userid"]}',
		'{$this->io->input["post"]["driid"]}',
		'order',
		NOW()
	)
	";
	$res = $this->model->query($query);
	$dhid = $this->model->_con->insert_id;

	$query ="
	SELECT 
	*
	FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history`
	WHERE 
	prefixid   = '".$this->config->default_prefix_id."' 
	AND dhid   = '{$dhid}'
	AND switch = 'Y'
	" ;
	$recArr = $this->model->getQueryRecord($query);
	$table['table']['record'] = $recArr['table']['record'];
}
else {
	$query = "
	UPDATE `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history`
	SET 
	driid = '{$this->io->input["post"]["driid"]}'
	WHERE
	prefixid   = '".$this->config->default_prefix_id."' 
	AND dhid   = '{$table['table']['record'][0]['dhid']}'
	AND status = 'order'
	AND switch = 'Y'
	";
	$res = $this->model->query($query);
}

//Create ordernumber
$table['table']['record'][0]['ordernumber'] = $table['table']['record'][0]['dhid'];//strtotime($table['table']['record'][0]['insertt']) + (int)$table['table']['record'][0]['dhid'];
$table['table']['record'][0]['hash'] = md5($this->config->alipay['merchantnumber'].$this->config->alipay['code'].sprintf("%d", $table['table']['rt']['deposit_rule_item'][0]['amount']).$table['table']['record'][0]['ordernumber']);

$this->tplVar('table' , $table['table']) ;
// $this->tplVar('status',$status["status"]);
$this->display();