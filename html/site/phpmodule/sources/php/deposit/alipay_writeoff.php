<?php
$countryid = 2;
$currency = 'RMB';

 $code               = $this->config->alipay['code'];
 $merchantnumber     = $this->io->input['post']['merchantnumber'];
 $ordernumber        = $this->io->input['post']['ordernumber'];
 $amount             = $this->io->input['post']['amount'];
 $paymenttype        = $this->io->input['post']['paymenttype'];

 $serialnumber       = $this->io->input['post']['serialnumber'];
 $writeoffnumber     = $this->io->input['post']['writeoffnumber'];
 $timepaid           = $this->io->input['post']['timepaid'];
 $tel                = $this->io->input['post']['tel'];
 $hash               = $this->io->input['post']['hash'];

 $verify = md5("merchantnumber=".$merchantnumber.
               "&ordernumber=".$ordernumber.
               "&serialnumber=".$serialnumber.
               "&writeoffnumber=".$writeoffnumber.
               "&timepaid=".$timepaid.
               "&paymenttype=".$paymenttype.
               "&amount=".$amount.
               "&tel=".$tel.
               $code);
 
 //print "verify=".$verify;
 if($hash === $verify){
 	if ($merchantnumber != $this->config->alipay['merchantnumber']) {
 		die();
 	}

// file_put_contents('alipay_log', json_encode($this->io->input['post']));

	require_once "saja/mysql.ini.php";
	$this->model = new mysql($this->config->db[0]);
	$this->model->connect();

	$query ="
	SELECT 
	dh.*,
	unix_timestamp(dh.insertt) as insertt,
	unix_timestamp(dh.modifyt) as modifyt,
	dri.amount,
	dri.spoint
	FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh 
	LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri ON 
	     dh.prefixid = dri.prefixid
	     AND dh.driid = dri.driid 
	     AND dri.switch = 'Y'
	WHERE 
	dh.prefixid   = '".$this->config->default_prefix_id."' 
	AND dh.dhid   = '{$this->io->input['post']['ordernumber']}'
	AND dh.status = 'order'
	AND dh.switch = 'Y'
	" ;
	$table = $this->model->getQueryRecord($query);
	if (empty($table['table']['record'])) {
		die();
	}

	$order = queryOrder($this, $table['table']['record']);
	if (
		empty($order) 
		|| $order['status'] != 1 
		|| $order['merchantnumber'] != $this->config->alipay['merchantnumber']
		|| $order['ordernumber'] != $table['table']['record'][0]['dhid']
	) {
		die();
	}

    $query = "
    INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit`
    (
         `prefixid`,
         `userid`,
         `countryid`,
         `behav`,
         `currency`,
         `amount`,
         `insertt`
    )
    VALUE 
    (
         '".$this->config->default_prefix_id."',
         '".$table['table']['record'][0]['userid']."', 
         '".$countryid."', 
         'user_deposit', 
         '{$currency}', 
         '".$table['table']['record'][0]['amount']."', 
         NOW()
    )
    ";
    $this->model->query($query);
    $depositid = $this->model->_con->insert_id;

    if (!empty($table['table']['record'][0]['spointid'])) {
      $spointid = $table['table']['record'][0]['spointid'];
    }
    else {
      $query = "
      INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint`
      (
           `prefixid`,
           `userid`,
           `countryid`,
           `behav`,
           `amount`,
           `insertt`
      )
      VALUE 
      (
           '".$this->config->default_prefix_id."',
           '".$table['table']['record'][0]['userid']."', 
           '".$countryid."', 
           'user_deposit', 
           '".$table['table']['record'][0]['spoint']."', 
           NOW()
      )
      ";
      $this->model->query($query);
      $spointid = $this->model->_con->insert_id;
    }

  	$query = "
  	UPDATE `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history`
  	SET 
    data      = '".json_encode($this->io->input['post'])."',
    status    = 'deposit',
    spointid  = '{$spointid}',
    depositid = '{$depositid}'
  	WHERE 
  	prefixid   = '".$this->config->default_prefix_id."' 
  	AND dhid   = '{$ordernumber}'
  	AND status = 'order'
  	AND switch = 'Y'
  	";
  	$this->model->query($query);
 }

function queryOrder($obj, $record) {
     $adminurl = $obj->config->alipay['url_query'];
     $code = $obj->config->alipay['code'];
     $merchantnumber     = $obj->io->input['post']['merchantnumber'];
     $ordernumber        = $obj->io->input['post']['ordernumber'];
     $paymenttype        = $obj->config->alipay['paymenttype'];
     $writeoffnumber     = $obj->io->input['post']['writeoffnumber'];
     $timecreateds       = date("Ymd", $record[0]['modifyt']);
     $timecreatede       = date("Ymd", $record[0]['modifyt']);
     $timepaids          = date("Ymd", $record[0]['modifyt']);
     $timepaide          = date("Ymd", $record[0]['modifyt']);     
     $status             = 1;
     $operation          = 'queryorders';

     $time = date('YmdHis');

     $hash = md5($operation.$code.$time);

     $postdata = "merchantnumber=".$merchantnumber."&ordernumber=".$ordernumber."&writeoffnumber=".$writeoffnumber.
                 "&paymenttype=".$paymenttype."&timecreateds=".$timecreateds."&timecreatede=".$timecreatede.
                 "&timepaids=".$timepaids."&timepaide=".$timepaide."&status=".$status.
                 "&operation=".$operation."&time=".$time."&hash=".$hash;

     $url = parse_url($adminurl);
     if (empty($url['port'])) {
     	if ($url['scheme'] == 'http') {
     		$url['port'] = 80;
     	}
     	else if ($url['scheme'] == 'https') {
     		$url['port'] = 443;
     	}
     }
// echo '<pre>';print_r($url);exit;
     $postdatalen = strlen($postdata);
     $postdata = "POST ".$url['path']." HTTP/1.0\r\n".
                "Content-Type: application/x-www-form-urlencoded\r\n".
                "Host: ".$url['host'].":".$url['port']."\r\n".
                "Content-Length: ".$postdatalen."\r\n".
                "\r\n".
                $postdata;

     $receivedata = "";

     //-- 若不用SSL(https)連接，則改為 $fp = fsockopen ($url['host'], $url['port'], $errno, $errstr, 90);
     $fp = fsockopen ($url['host'], $url['port'], $errno, $errstr, 90);

     //$fp = fsockopen ($hostip, $hostport, &$errno, &$errstr, 90);
     if(!$fp) { 
          echo "$errstr ($errno)<br>\n";
     }else{ 
          fputs ($fp, $postdata);

          do{ 
               if(feof($fp)){
                    //echo "connect is break\n";
                 	break;
               }
               $tmpstr = fgets($fp,128);
               $receivedata = $receivedata.$tmpstr;
          }while(true); //!($tmpstr=="0")
          fclose ($fp);
     }

     $receivedata = str_replace("\r","",trim($receivedata));
     $isbody = false;
     $httpcode = null;
     $httpmessage = null;
     $result = "";
     $array1 = explode("\n",$receivedata);
     for($i=0;$i<count($array1);$i++){
          if($i==0){
               $array2 = explode(" ",$array1[$i]);
               $httpcode = $array2[1];
               $httpmessage = $array2[2];
          }else if(!$isbody){
               if(strlen($array1[$i])==0) $isbody = true;
          }else{
               $result = $result.$array1[$i];
          }
     }

     if($httpcode!="200"){
          if($httpcode=="404") echo "网址错误，无法找到网页!";
          else if($httpcode=="500") echo "服务器错误!";

          else echo $httpmessage;
          return;
     }
     $result = iconv("BIG5","UTF8",$result);
// print_r( iconv("BIG5","UTF8",$result) );exit;
     parse_str($result, $args);

     return $args;
}