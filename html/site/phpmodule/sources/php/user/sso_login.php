<?php
// the selected provider
if (!empty($this->io->input['get']["provider"])) {
	$provider_name = $this->io->input['get']["provider"];
}

require_once "hybridauth/config.php";
require_once "hybridauth/Hybrid/Auth.php";

if (!empty($this->io->input['get']['hauth_start']) || !empty($this->io->input['get']['hauth_done'])) {
	require_once( "hybridauth/Hybrid/Endpoint.php" ); 
	Hybrid_Endpoint::process();
}
else if (!empty($this->io->input['get']["provider"])) {
	try{
		// initialize Hybrid_Auth with a given file
		$hybridauth = new Hybrid_Auth( $config );
		// try to authenticate with the selected provider
		$adapter = $hybridauth->authenticate( $provider_name );
		// then grab the user profile 
		$user_profile = $adapter->getUserProfile();
		if (!empty($user_profile->emailVerified)) {
			$user_profile->email = $user_profile->emailVerified;
		}
		$user_profile->provider = $provider_name;
	}
	catch( Exception $e ){
		echo "Error: please try again!";
		echo "Original error message: " . $e->getMessage();
	}

	require_once "saja/mysql.ini.php";
	$this->model = new mysql($this->config->db[0]);
	$this->model->connect();
	
	$query = "
	SELECT
	u.*, s.ssoid, s.name as provider_name, s.uid as provider_uid, s.switch as sso_switch
	FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso` s 
	LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt` us ON
		s.prefixid = us.prefixid
		AND s.ssoid = us.ssoid
		AND us.switch = 'Y'
	LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u ON
		us.prefixid = u.prefixid
		AND us.userid = u.userid
		AND u.switch = 'Y'
	WHERE
	s.prefixid = '".$this->config->default_prefix_id."' 
	AND s.name = '".$this->io->input["get"]["provider"]."'
	AND s.uid = '".$user_profile->identifier."'
	AND s.switch = 'Y'
	AND u.userid IS NOT NULL
	";
	$table = $this->model->getQueryRecord($query);

	if (empty($table['table']['record'])) {
		$_SESSION['sso'] = (array)$user_profile;
		header("location: ".$this->config->default_main."/user/register");
		die();
	}

	$user = $table['table']['record'][0];
	if ($user['switch'] != 'Y' || $user['sso_switch'] != 'Y') {
		$this->jsPrintMsg('帳號已註銷!!', $this->config->default_main);
	}

	$query = "
	SELECT
	*
	FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
	WHERE
	prefixid = '".$this->config->default_prefix_id."' 
	AND userid = '".$user['userid']."'
	AND switch =  'Y'
	";
	$table = $this->model->getQueryRecord($query);

	$user['profile'] = $table['table']['record'][0];
	$_SESSION['user'] = $user;

	if (!empty($this->io->input['get']['location_url'])) {
		header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));
	}
	else {
		header("location:".$this->config->default_main);	
	}
}