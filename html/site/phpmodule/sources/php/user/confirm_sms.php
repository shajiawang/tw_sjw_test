<?php
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

//取得會員資料
$query = "
SELECT u.*, usa.code, usa.verified  
FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile` u
LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` usa ON 
	u.prefixid = usa.prefixid
	AND u.userid = usa.userid
	AND usa.switch = 'Y'
WHERE 
u.`prefixid` = '{$this->config->default_prefix_id}'
AND u.`userid` = '{$this->io->input['post']['userid']}'
AND u.`switch` = 'Y'
";
$table = $this->model->getQueryRecord($query); 

if (empty($table['table']['record'])) {
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['post']['userid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['post']['userid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$this->jsPrintMsg('会员账号错误!!', $this->config->default_main ."/user/register");
	exit;
} 
elseif ($table['table']['record'][0]['verified'] == 'Y') {
	$this->jsPrintMsg('手机号码已认证!!', $this->config->default_main);
	exit;
}
elseif ($table['table']['record'][0]['code'] == $this->io->input['post']['code']) {
	//手机号码认证完成
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` SET 
	verified='Y'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['post']['userid']}'
	AND `switch` = 'Y'
	";
	$this->model->query($query);
	
	$this->jsPrintMsg('手机号码认证完成!!', $this->config->default_main);
} else {
	$this->jsPrintMsg('手机号码认证尚未完成!!', $this->config->default_main);
	exit;
}

