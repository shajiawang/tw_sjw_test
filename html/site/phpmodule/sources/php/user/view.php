<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入會員帳號!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(userid|email|nickname|birthday|city|area|mobile|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_nickname"] != ''){
	$status["status"]["search"]["search_nickname"] = $this->io->input["get"]["search_nickname"] ;
	$status["status"]["search_path"] .= "&search_nickname=".$this->io->input["get"]["search_nickname"] ;
	$sub_search_query .=  "
		AND u.`nickname` like '%".$this->io->input["get"]["search_nickname"]."%'";
}
if($this->io->input["get"]["search_email"] != ''){
	$status["status"]["search"]["search_email"] = $this->io->input["get"]["search_email"] ;
	$status["status"]["search_path"] .= "&search_email=".$this->io->input["get"]["search_email"] ;
	$sub_search_query .=  "
		AND u.email like '%".$this->io->input["get"]["search_email"]."%'";
}
if($this->io->input["get"]["search_provider_name"] != ''){
	$status["status"]["search"]["search_provider_name"] = $this->io->input["get"]["search_provider_name"] ;
	$status["status"]["search_path"] .= "&search_provider_name=".$this->io->input["get"]["search_provider_name"] ;
	$sub_search_query .=  "
		AND s.name = '".$this->io->input["get"]["search_provider_name"]."'
		AND s.ssoid IS NOT NULL";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="
SELECT count(*) as num 
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT 
u.*,
s.name as provider_name,
s.email as sso_email
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt` us ON 
	u.prefixid = us.prefixid
	AND u.userid = us.userid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso` s ON 
	us.prefixid = s.prefixid
	AND us.ssoid = s.ssoid
WHERE 
u.prefixid = '".$this->config->default_prefix_id."' 
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
$query ="
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["user"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();