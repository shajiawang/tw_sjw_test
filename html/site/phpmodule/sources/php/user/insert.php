<?php
//login_chk();
require_once "saja/convertString.ini.php";
$this->str = new convertString();

$name = "";
$passwd = "";
//网站会员注册
if (empty($this->io->input['session']['sso'])) {
	//检查会员账号
	if (empty($this->io->input['post']['name'])) {
		$this->jsAlertMsg('请填写使用者账号!!');
	}
	else if (!preg_match("/^[A-Za-z0-9]{4,12}$/", $this->io->input['post']['name'])) {
		$this->jsAlertMsg('账号格式不正确!!');
	}

	$name = $this->io->input['post']['name'];

	//检查密码
	if (empty($this->io->input['post']['passwd'])) {
		$this->jsAlertMsg('请填写密码!!');
	}
	else if (empty($this->io->input['post']['repasswd'])) {
		$this->jsAlertMsg('请填写确认密码!!');
	}
	else if (!preg_match("/^[A-Za-z0-9]{4,12}$/", $this->io->input['post']['repasswd'])) {
		$this->jsAlertMsg('密码格式不正确!!');
	}
	else if ($this->io->input['post']['passwd'] !== $this->io->input['post']['repasswd']) {
		$this->jsAlertMsg('确认密码错误!!');
	}

	$passwd = $this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key);
}

if (!empty($this->io->input['session']['sso']['email'])) {
	$email = $this->io->input['session']['sso']['email'];
}
else if (!empty($this->io->input['post']['email'])) {
	if (!filter_var($this->io->input['post']['email'], FILTER_VALIDATE_EMAIL)) {
		$this->jsAlertMsg('Email格式不正确!!');
	}
	$email = $this->io->input['post']['email'];
} 
else {
	$this->jsAlertMsg('Email请勿空白!!');
}

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

//重複帳號檢查
if (empty($this->io->input['session']['sso'])) {
	$query = "
	SELECT 
	*
	FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`
	WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND name = '{$name}'
	";
	$table = $this->model->getQueryRecord($query);
	if (!empty($table['table']['record'])) {
		$this->jsAlertMsg('此账号已存在!!');
	}
}

//重複Email檢查
$query = "
SELECT 
*
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND email = '{$email}'
";
$table = $this->model->getQueryRecord($query);
// echo '<pre>';print_r($table);exit;
if (!empty($table['table']['record'])) {
	$this->jsAlertMsg('此Email已存在!!');
}

//重複暱稱檢查
$query = "
SELECT 
*
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND nickname = '{$this->io->input['post']['nickname']}'
";
$table = $this->model->getQueryRecord($query);
if (!empty($table['table']['record'])) {
	$this->jsAlertMsg('此昵称已存在!!');
}



//加入會員資料
$query = "
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`
(
	`prefixid`,
	`name`,
	`passwd`,
	`email`,
	`insertt`
)
VALUE
(
	'".$this->config->default_prefix_id."',
	'{$name}',
	'{$passwd}',
	'{$email}',
	NOW()
)
";
$this->model->query($query);			
$userid = $this->model->_con->insert_id;

//加入SSO資料
$query = "
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso`
(
	`prefixid`,
	`name`,
	`uid`,
	`seq`,
	`insertt`
)
VALUE
(
	'".$this->config->default_prefix_id."',
	'".$this->io->input["session"]['sso']["provider"]."',
	'".$this->io->input["session"]['sso']["identifier"]."',
	'0',
	NOW()
)
";
$this->model->query($query);
$ssoid = $this->model->_con->insert_id;

$query = "
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt`
(
	`prefixid`,
	`userid`,
	`ssoid`,
	`insertt`
)
VALUE
(
	'".$this->config->default_prefix_id."',
	'{$userid}',
	'{$ssoid}',
	NOW()
)
";
$this->model->query($query);

$query = "
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
(
	`prefixid`,
	`userid`,
	`nickname`,
	`gender`,
	`cityid`,
	`area`,
	`address`,
	`addressee`,
	`phone`,
	`insertt`
)
VALUE
(
	'".$this->config->default_prefix_id."',
	'{$userid}',
	'{$this->io->input['post']['nickname']}',
	'{$this->io->input['post']['gender']}',
	'{$this->io->input['post']['cityid']}',
	'{$this->io->input['post']['area']}',
	'{$this->io->input['post']['address']}',
	'{$this->io->input['post']['addressee']}',
	'{$this->io->input['post']['phone']}',
	NOW()
)
";
$this->model->query($query);


if (!empty($this->io->input['session']['sso'])) {
	header("location:".$this->config->default_main."/user/sso_login?provider=".$this->io->input['session']['sso']['provider']);
}
else {
	header("location:".$this->config->default_main."/user/send_email_verification");
}