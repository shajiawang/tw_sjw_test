<?php
require_once "saja/convertString.ini.php";
$this->str = new convertString();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$email = $this->str->strDecode($this->io->input['get']['uid'], $this->config->encode_key);

##############################################################################################################################################

$query = "
SELECT *  
FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `email` = '{$email}' 
AND (UNIX_TIMESTAMP()- UNIX_TIMESTAMP(insertt) )< 86400
AND `switch` = 'Y'
";
$table = $this->model->getQueryRecord($query);

if (empty($table['table']['record'])) {
	$this->jsPrintMsg('会员认证错误!!', $this->config->default_main);
	exit;
} 
elseif ($table['table']['record'][0]['verified']=='Y') {
	$this->jsPrintMsg('会员注册数据已认证!!', $this->config->default_main);
	exit;
} 
else {
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` SET 
	verified='Y'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `email` = '{$email}'
	";
	$this->model->query($query);
	
	$this->jsPrintMsg('会员注册数据认证完成!!', $this->config->default_main);
}

