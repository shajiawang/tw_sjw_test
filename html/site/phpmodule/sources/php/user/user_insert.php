<?php
//login_chk();
require_once "saja/convertString.ini.php";
$this->str = new convertString();

$name = "";
$passwd = "";

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
//网站会员注册

//昵称检查
if (empty($this->io->input['post']['nickname'])) {
	$this->jsAlertMsg('请填写昵称!!');
}
$query = "
SELECT *
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
WHERE 
`prefixid` = '".$this->config->default_prefix_id."'
AND `nickname` = '{$this->io->input['post']['nickname']}'
AND `switch` = 'Y'
";
$profiletable = $this->model->getQueryRecord($query);
if (!empty($profiletable['table']['record'])) {
	$this->jsAlertMsg('此昵称已存在!!');
}

if($this->io->input['session']['m_prefix']=='m'){}
else
{
	if (!empty($this->io->input['post']['email'])) {
		if (!filter_var($this->io->input['post']['email'], FILTER_VALIDATE_EMAIL)) {
			$this->jsAlertMsg('Email格式不正确!!');
		}
	} else {
		$this->jsAlertMsg('Email请勿空白!!');
	}
	$email = $this->io->input['post']['email'];
		
	//重复Email检查
	$query = "
	SELECT * 
	FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`
	WHERE 
	`prefixid` = '".$this->config->default_prefix_id."'
	AND `email` = '{$email}'
	AND `switch` = 'Y'
	";
	$table = $this->model->getQueryRecord($query);
	if (!empty($table['table']['record'])) {
		$this->jsAlertMsg('此Email已存在!!');
	}

	/*
	//检查会员账号
	if (empty($this->io->input['post']['name'])) {
		$this->jsAlertMsg('请填写使用者账号!!');
	}
	else if (!preg_match("/^[A-Za-z0-9]{4,12}$/", $this->io->input['post']['name'])) {
		$this->jsAlertMsg('账号格式不正确!!');
	}
	$name = $this->io->input['post']['name'];
	
	//重复账号检查
	$query = "
	SELECT * 
	FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`
	WHERE 
	`prefixid` = '".$this->config->default_prefix_id."'
	AND `name` = '{$name}'
	AND `switch` = 'Y'
	";
	$table = $this->model->getQueryRecord($query);
	if (!empty($table['table']['record'])) {
		$this->jsAlertMsg('此账号已存在!!');
	}*/
}

//手机号码检查
if (empty($this->io->input['post']['phone'])) {
	$this->jsAlertMsg('请填写手机号码!!');
}
$phone = $this->io->input['post']['phone'];

$query = "
SELECT *
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
WHERE 
`prefixid` = '".$this->config->default_prefix_id."'
AND `phone` = '{$phone}'
AND `switch` = 'Y'
";
$phonetable = $this->model->getQueryRecord($query);
if (!empty($phonetable['table']['record'])) {
	$this->jsAlertMsg('此手机号码已存在!!');
}
if($this->io->input['session']['m_prefix']=='m')
{
	//只允许13、150、151、158、159和189开头并且长度是11位的手机号码，事实上现在可用的手机号码格式非常多
	if(preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/", $phone)){
		//验证通过    
	}else{    
		//$this->jsAlertMsg('此手机号码格式不对!!');    
	}
	
	$email = '';
}


//检查密码
if (empty($this->io->input['post']['passwd'])) {
	$this->jsAlertMsg('请填写密码!!');
}
else if (!preg_match("/^[A-Za-z0-9]{4,12}$/", $this->io->input['post']['passwd'])) {
	$this->jsAlertMsg('密码格式不正确!!');
}
else if (empty($this->io->input['post']['repasswd'])) {
	$this->jsAlertMsg('请填写确认密码!!');
}
else if ($this->io->input['post']['passwd'] !== $this->io->input['post']['repasswd']) {
	$this->jsAlertMsg('确认密码错误!!');
}
$passwd = $this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key);


//会员账号
$name = $phone;

//加入会员数据
$query = "
INSERT INTO `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user`
(
	`prefixid`,
	`name`,
	`passwd`,
	`email`,
	`insertt`
)
VALUE
(
	'".$this->config->default_prefix_id."',
	'{$name}',
	'{$passwd}',
	'{$email}',
	NOW()
)
";
$this->model->query($query);			
$userid = $this->model->_con->insert_id;

$query = "
INSERT INTO `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile`
(
	`prefixid`,
	`userid`,
	`nickname`,
	`gender`,
	`cityid`,
	`area`,
	`address`,
	`addressee`,
	`phone`,
	`insertt`
)
VALUE
(
	'{$this->config->default_prefix_id}',
	'{$userid}',
	'{$this->io->input['post']['nickname']}',
	'{$this->io->input['post']['gender']}',
	'{$this->io->input['post']['cityid']}',
	'{$this->io->input['post']['area']}',
	'{$this->io->input['post']['address']}',
	'{$this->io->input['post']['addressee']}',
	'{$phone}',
	NOW()
)
";
$this->model->query($query);

//新增SMS check code
$code = time() * rand(1,9); 
$checkcode = substr($code, -6); 
$query = "
INSERT INTO `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` SET
	`prefixid`='{$this->config->default_prefix_id}',
	`userid`='{$userid}',
	`code`='{$checkcode}',
	`verified`='N', 
	`insertt`=NOW()
";
$this->model->query($query); 

//驗證
if($this->io->input['session']['m_prefix']=='m') {
	//寄送SMS check code
	//$this->jsPrintMsg('確認手机 SMS check-code!!', $this->config->default_main ."/user/send_sms/?uid={$userid}");
	$this->jsPrintMsg('会员账号已建立!!', $this->config->default_main);
} else {
	//寄送确认信
	$this->jsPrintMsg('会员账号已建立!!', $this->config->default_main ."/user/send_verification/?uid={$userid}");
}