<?php
//login_chk();
require_once "saja/convertString.ini.php";
$this->str = new convertString();

$name = "";
$passwd = "";

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

##############################################################################################################################################
//网站会员注册

//昵称检查
if (empty($this->io->input['post']['nickname'])) {
	$this->jsAlertMsg('请填写昵称!!');
}
$query = "
SELECT *
FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile`
WHERE 
`prefixid` = '".$this->config->default_prefix_id."'
AND `nickname` = '{$this->io->input['post']['nickname']}'
AND `switch` = 'Y'
";
$profiletable = $this->model->getQueryRecord($query);
if (!empty($profiletable['table']['record'])) {
	$this->jsAlertMsg('此昵称已存在!!');
}

//手机号码检查
if (empty($this->io->input['post']['phone'])) {
	$this->jsAlertMsg('请填写手机号码!!');
}
$phone = $this->io->input['post']['phone'];

$query = "
SELECT *
FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile`
WHERE 
`prefixid` = '".$this->config->default_prefix_id."'
AND `phone` = '{$phone}'
AND `switch` = 'Y'
";
$phonetable = $this->model->getQueryRecord($query);
if (!empty($phonetable['table']['record'])) {
	$this->jsAlertMsg('此手机号码已存在!!');
}
if($this->io->input['session']['m_prefix']=='m')
{
	//只允许13、150、151、158、159和189开头并且长度是11位的手机号码，事实上现在可用的手机号码格式非常多
	if(preg_match("/^13[0-9]{1}[0-9]{8}$|15[0189]{1}[0-9]{8}$|189[0-9]{8}$/", $phone)){
		//验证通过    
	}else{    
		//$this->jsAlertMsg('此手机号码格式不对!!');    
	}
	
	$email = '';
}

//会员账号
$name = $phone;

//加入会员数据
$query = "
INSERT INTO `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user`
(
	`prefixid`,
	`name`,
	`passwd`,
	`email`,
	`sso_name`,
	`insertt`
)
VALUE
(
	'".$this->config->default_prefix_id."',
	'{$name}',
	'{$passwd}',
	'{$email}',
	'{$this->io->input["session"]['sso']["provider"]}',
	NOW()
)
";
$this->model->query($query);			
$userid = $this->model->_con->insert_id;

$query = "
INSERT INTO `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile`
(
	`prefixid`,
	`userid`,
	`nickname`,
	`gender`,
	`cityid`,
	`area`,
	`address`,
	`addressee`,
	`phone`,
	`insertt`
)
VALUE
(
	'{$this->config->default_prefix_id}',
	'{$userid}',
	'{$this->io->input['post']['nickname']}',
	'{$this->io->input['post']['gender']}',
	'{$this->io->input['post']['cityid']}',
	'{$this->io->input['post']['area']}',
	'{$this->io->input['post']['address']}',
	'{$this->io->input['post']['addressee']}',
	'{$phone}',
	NOW()
)
";
$this->model->query($query);


//加入SSO資料
$query = "
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso`
(
	`prefixid`,
	`name`,
	`uid`,
	`insertt`
)
VALUE
(
	'{$this->config->default_prefix_id}',
	'{$this->io->input["session"]['sso']["provider"]}',
	'{$this->io->input["session"]['sso']["identifier"]}',
	NOW()
)
";
$this->model->query($query);
$ssoid = $this->model->_con->insert_id;

$query = "
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt`
(
	`prefixid`,
	`userid`,
	`ssoid`,
	`insertt`
)
VALUE
(
	'".$this->config->default_prefix_id."',
	'{$userid}',
	'{$ssoid}',
	NOW()
)
";
$this->model->query($query);

// 設成已登入
	$query = "
	SELECT
	u.*, s.ssoid, s.name as provider_name, s.uid as provider_uid, s.switch as sso_switch
	FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}sso` s 
	LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sso_rt` us ON
		s.prefixid = us.prefixid
		AND s.ssoid = us.ssoid
		AND us.switch = 'Y'
	LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` u ON
		us.prefixid = u.prefixid
		AND us.userid = u.userid
		AND u.switch = 'Y'
	WHERE
	s.prefixid = '{$this->config->default_prefix_id}' 
	AND s.name = '{$this->io->input["session"]['sso']["provider"]}'
	AND s.uid = '{$this->io->input["session"]['sso']["identifier"]}'
	AND s.switch = 'Y'
	AND u.userid IS NOT NULL
	";
	$table = $this->model->getQueryRecord($query);
	$user = $table['table']['record'][0];
	
	$query = "
	SELECT *
	FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile`
	WHERE
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `userid` = '{$userid}'
	AND `switch` =  'Y'
	";
	$table = $this->model->getQueryRecord($query);

	$user['profile'] = $table['table']['record'][0];
	$_SESSION['user'] = $user;
	
header("location:". $this->config->default_main);
