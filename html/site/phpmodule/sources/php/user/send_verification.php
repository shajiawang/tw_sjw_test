<?php
require_once "saja/convertString.ini.php";
$this->str = new convertString();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]); 
$this->model->connect();

//取得Email
$query = "
SELECT email 
FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `userid` = '{$this->io->input['get']['uid']}'
AND `switch` = 'Y'
";
$table = $this->model->getQueryRecord($query);

if (empty($table['table']['record'])) {
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$this->jsPrintMsg('會員資料錯誤!!', $this->config->default_main ."/user/register");
	exit;
}
$umail = $table['table']['record'][0]['email']; 
$uid = $this->str->strEncode($umail, $this->config->encode_key);
//$deuid = $this->str->strDecode($uid, $this->config->encode_key);


require_once "phpmailer/class.phpmailer.php";
$mail = new PHPMailer;

$msgid = md5(time()).md5($umail);
$header1 = "References:LB".$msgid;

$mail->isSMTP(); // set mailer to use SMTP
$mail->Host = "smtp.sendgrid.net"; //SMTP主機     
$mail->Port = 25; //default is 25, gmail is 465 or 587
$mail->SMTPAuth = true; // Enable SMTP authentication
$mail->Username = 'iwantit'; // SMTP username
$mail->Password = 'C7wLu5As'; // SMTP password
$mail->SMTPSecure = 'tls'; // tls or ssl connection as req
// $mail->SMTPDebug = 1;
$mail->AddCustomHeader($header1);
$mail->CharSet = 'UTF-8';
$mail->Encoding = "base64" ;

$mail->AddAddress($umail);
$mail->From = 'support@sajawa.com';
$mail->FromName = '全球首創殺價式拍賣網站-殺價王';
$mail->Subject = '殺價王註冊認證信';
$mail->IsHTML(true);

$mail_Body = '親愛的會員 您好：<br><br>';
$mail_Body.= '這封認證信是由殺價王系統發出，用以處理您註冊認證，當您收到本「認證信函」後， 請直接點選下方連結，無需回信。<br><br>';
$mail_Body.= '按此認證，為了確保您的會員資料安全，連結將於此信件寄出後24小時後失效。 如果您有其他疑問，請您至「殺價王會員中心聯絡客服」。<br><br>';
$mail_Body.= "http://{$this->config->domain_name}{$this->config->default_main}/user/confirm_verification/?uid={$uid}<br><br>";
$mail_Body.= '<br><br>';
$mail_Body.= '殺價王<br>';
$mail_Body.= '敬上 www.saja.com.tw<br>';
$mail->Body = $mail_Body;

if(!$mail->Send())
{
	echo $mail->ErrorInfo ; 
	exit;
}

//寄送確認信
$this->jsPrintMsg('已寄送認證信, 請前往註冊填寫的電郵收取!!', $this->config->default_main);
