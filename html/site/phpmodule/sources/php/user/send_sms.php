<?php
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

//取得會員資料
$query = "
SELECT u.*, usa.code, usa.verified  
FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile` u
LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` usa ON 
	u.prefixid = usa.prefixid
	AND u.userid = usa.userid
	AND usa.switch = 'Y'
WHERE 
u.`prefixid` = '{$this->config->default_prefix_id}'
AND u.`userid` = '{$this->io->input['get']['uid']}'
AND u.`switch` = 'Y'
";
$table = $this->model->getQueryRecord($query); 

if (empty($table['table']['record'])) {
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$this->jsPrintMsg('会员账号错误!!', $this->config->default_main ."/user/register");
	exit;
}
if($table['table']['record'][0]['verified']=='Y'){
	$this->jsPrintMsg('手机号码已认证!!', $this->config->default_main);
	exit;
}

$code = $table['table']['record'][0]['code']; 
$user_phone = $table['table']['record'][0]['phone'];
$user_phone_0 = substr($user_phone, 0, 1);
$user_phone_1 = substr($user_phone, 1, 1);

if($user_phone_0=='0' && $user_phone_1=='9'){
	$phone = $user_phone;
} else {
	$phone = '86'. $user_phone;
}

//簡訊王 SMS-API
$sMessage = strtolower(urlencode("SMS check-code : ". $code ." sajawa.com"));
$msg = "username=saja&password=sj9889&dstaddr={$phone}&smbody=". $sMessage;
$to_url = "http://202.39.48.216/kotsmsapi-1.php?". $msg;
  
if(!$getfile=file($to_url)) {
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$this->jsPrintMsg('ERROR: SMS-API 无法连接 !', $this->config->default_main ."/user/register");
	exit;
}
$term_tmp = implode('', $getfile);
$check_kmsgid = explode('=', $term_tmp);
$kmsgid = (int)$check_kmsgid[1];

if($kmsgid < 0) {
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sms_auth` SET 
	switch='N'
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `userid` = '{$this->io->input['get']['uid']}'
	";
	$this->model->query($query);
	
	$this->jsPrintMsg('手机号码认证错误!!', $this->config->default_main ."/user/register");
	exit;
}

##############################################################################################################################################
// Relation Start
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];
 
// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"] . $status["status"]["search_path"] . $status["status"]["sort_path"] . $status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]); 
$this->display();
