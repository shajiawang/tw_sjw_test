<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

if (empty($this->io->input["get"]["userid"])) {
	$this->jsAlertMsg('userid 错误!!');
}

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$query ="
SELECT 
u.*,
s.name as provider_name,
s.email as sso_email
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt` us ON 
	u.prefixid = us.prefixid
	AND u.userid = us.userid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso` s ON 
	us.prefixid = s.prefixid
	AND us.ssoid = s.ssoid
WHERE 
u.prefixid = '".$this->config->default_prefix_id."' 
AND u.userid = '".$this->io->input["get"]["userid"]."'
" ;
$table = $this->model->getQueryRecord($query);


##############################################################################################################################################
// Relation Start 
$query ="
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["user_profile"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();