<?php
if (!empty($this->io->input['session']['user'])) {
	header("location:". $this->config->default_main); exit;
} 

if (empty($this->io->input['post']["name"]) ) {
	$this->jsAlertMsg('登录账号错误!!');
}
if (empty($this->io->input['post']["passwd"]) ) {
	$this->jsAlertMsg('登录密码错误!!');
}

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

require_once "saja/convertString.ini.php";
$this->str = new convertString();
	
	$query = "
	SELECT u.* 
	FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u 
	WHERE 
	u.prefixid = '".$this->config->default_prefix_id."' 
	AND u.name = '".$this->io->input['post']["name"]."' 
	AND u.switch = 'Y' 
	";
	$table = $this->model->getQueryRecord($query);

	if (empty($table['table']['record'])) {
		$this->jsPrintMsg('登录账号不存在!!', $this->config->default_main."/user/register");
		die();
	}
	$user = $table['table']['record'][0];
	
	$passwd = $this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key);
	if ($user['passwd'] !== $passwd ) {
		$this->jsAlertMsg('登录密码错误!!');
	}


	$query = "
	SELECT
	*
	FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
	WHERE
	prefixid = '".$this->config->default_prefix_id."' 
	AND userid = '".$user['userid']."'
	AND switch =  'Y'
	";
	$table = $this->model->getQueryRecord($query);

	$user['profile'] = $table['table']['record'][0];
	$_SESSION['user'] = $user;
	

	if (!empty($this->io->input['get']['location_url'])) {
		header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));
	}
	else {
		header("location:".$this->config->default_main);	
	}
