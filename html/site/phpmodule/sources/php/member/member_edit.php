<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/convertString.ini.php";
$this->str = new convertString();

$name = "";
$passwd = "";

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
//会员資料

//检查密码
if (empty($this->io->input['post']['oldpasswd'])) {
	$this->jsAlertMsg('请填写旧密码!!');
}

$query = "
SELECT u.* 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u 
WHERE 
u.prefixid = '".$this->config->default_prefix_id."' 
AND u.userid = '".$this->io->input['post']["userid"]."' 
AND u.switch = 'Y' 
";
$table = $this->model->getQueryRecord($query); 
if (empty($table['table']['record'])) {
	$this->jsPrintMsg('账号不存在!!', $this->config->default_main."/user/register");
	die();
}
$user = $table['table']['record'][0];

$oldpasswd = $this->str->strEncode($this->io->input['post']['oldpasswd'], $this->config->encode_key);

if ($user['passwd'] !== $oldpasswd ) {
	$this->jsAlertMsg('旧密码错误!!');
}

if (empty($this->io->input['post']['passwd'])) {
	$this->jsAlertMsg('请填写新密码!!');
}
else if (!preg_match("/^[A-Za-z0-9]{4,12}$/", $this->io->input['post']['passwd'])) {
	$this->jsAlertMsg('密码格式不正确!!');
}
else if (empty($this->io->input['post']['repasswd'])) {
	$this->jsAlertMsg('请填写确认密码!!');
}
else if ($this->io->input['post']['passwd'] !== $this->io->input['post']['repasswd']) {
	$this->jsAlertMsg('确认密码错误!!');
}
$passwd = $this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key);

//修改会员数据
$query = "
UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` SET 
`passwd`='{$passwd}'
WHERE 
prefixid   = '".$this->config->default_prefix_id."'
AND userid = '".$this->io->input["post"]["userid"]."'
";
$this->model->query($query);	
		
session_destroy();
$this->jsPrintMsg('密码已变更, 请重新登入!!', $this->config->default_main."/user/login");
