<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 

$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
// Path End 

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["args"][] = "p=".$this->io->input["get"]["p"] ;
}
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);
if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

#// Status Stop 
##############################################################################################################################################



##############################################################################################################################################
// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
$query = "
SELECT 
u.*, up.*, s.name as provider_name
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u 
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	u.prefixid = up.prefixid
	AND u.userid = up.userid
	AND up.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt` us ON 
	u.prefixid = us.prefixid
	AND u.userid = us.userid
	AND us.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso` s ON 
	us.prefixid = s.prefixid
	AND us.ssoid = s.ssoid
	AND s.switch = 'Y'
WHERE 
u.prefixid = '".$this->config->default_prefix_id."' 
AND u.userid = '{$userid}' 
AND u.switch = 'Y'
";
$table = $this->model->getQueryRecord($query); 
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
* 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND channelid = '{$this->io->input["get"]["channelid"]}'
AND switch = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

$query ="
SELECT 
userid, sum(amount) as amount 
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND userid = '{$this->io->input["session"]['user']["userid"]}'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["spoint"] = $recArr['table']['record'];

$query ="
SELECT 
userid, sum(amount) as amount 
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."gift` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND userid = '{$this->io->input["session"]['user']["userid"]}'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["gift"] = $recArr['table']['record'];

$query ="
SELECT 
userid, sum(amount) as amount 
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND userid = '{$this->io->input["session"]['user']["userid"]}'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["bonus"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################



$this->tplVar('table' , $table['table']) ;
$this->tplVar('status' , $status['status']) ;
$this->display();