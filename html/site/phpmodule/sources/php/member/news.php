<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
// Sort End

// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["args"][] = "p=".$this->io->input["get"]["p"] ;
}
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}
##############################################################################################################################################


##############################################################################################################################################
// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
$query = "
SELECT 
*
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."news` n 
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_news_rt` cn ON 
	n.prefixid = cn.prefixid
	AND n.newsid = cn.newsid
	AND cn.channelid = '{$this->io->input['get']['channelid']}'
	AND cn.switch = 'Y'
WHERE 
n.prefixid = '".$this->config->default_prefix_id."' 
AND unix_timestamp() BETWEEN unix_timestamp(ontime) AND unix_timestamp(offtime)
AND n.switch = 'Y'
AND cn.channelid IS NOT NULL 
"; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query); 
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];
 
// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status' , $status['status']) ;
$this->display();