<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Sort Start
$status["status"]["sort"] = "";
$status["status"]["sort_path"] = "";
$sub_sort_query = "";

/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(insertt|status)/";
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "b.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

// Path Start 
$status["status"]["path"] = $this->config->default_main; 
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["args"][] = "p=".$this->io->input["get"]["p"] ;
}
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

#// Status Stop 
##############################################################################################################################################

##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT count(*) as num 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus` b 
LEFT OUTER JOIN `{$db_cash_flow}`.`{$this->config->default_prefix}settlement_history` sh ON 
	b.prefixid = sh.prefixid
	AND b.bonusid = sh.bonusid
	AND sh.switch = 'Y'	
LEFT OUTER JOIN `{$db_shop}`.`{$this->config->default_prefix}product` p ON 
	p.prefixid = sh.prefixid
	AND p.productid = sh.productid
	AND sh.switch = 'Y'	
WHERE 
b.prefixid = '{$this->config->default_prefix_id}' 
AND b.userid = '{$this->io->input['session']['user']['userid']}'
AND b.switch = 'Y'
" ;
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query = "
SELECT b.insertt, b.amount, b.behav, p.productid, p.name 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus` b 
LEFT OUTER JOIN `{$db_cash_flow}`.`{$this->config->default_prefix}settlement_history` sh ON 
	b.prefixid = sh.prefixid
	AND b.bonusid = sh.bonusid
	AND sh.switch = 'Y'	
LEFT OUTER JOIN `{$db_shop}`.`{$this->config->default_prefix}product` p ON 
	p.prefixid = sh.prefixid
	AND p.productid = sh.productid
	AND sh.switch = 'Y'	
WHERE 
b.prefixid = '{$this->config->default_prefix_id}' 
AND b.userid = '{$this->io->input['session']['user']['userid']}' 
AND b.switch = 'Y'
";
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query); 

// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query); 
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];
 
// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"] . $status["status"]["search_path"] . $status["status"]["sort_path"] . $status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();