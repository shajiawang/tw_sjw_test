<?php
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

// Path Start 
$status["status"]["path"] = $this->config->default_main; 
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End

// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(faqid|name|seq|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

$sub_sort_query = "";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End 


// Search Start
$status["status"]["search"] = "";
$join_search_query ="";
$rt_search_query ="";	//AND fc.fcid IS NOT NULL";

$sub_search_query = "";
if(isset($this->io->input["get"]["fid"]) && $this->io->input["get"]["fid"] != ''){
	$status["status"]["search"]["fid"] = $this->io->input["get"]["fid"] ;
	$status["status"]["search_path"] .= "&fid=".$this->io->input["get"]["fid"] ;
	$sub_search_query .=  "
		AND f.faqid ='{$this->io->input["get"]["fid"]}'";
} else {
	$sub_search_query .= " AND f.faqid ='2'"; 
}
// Search End

// Page Start
if(!empty($this->io->input["get"]["p"]) ){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
} else {
	$status["status"]["search"]["p"] = 1;
	$status["status"]["path"] .= "&p=1";
}
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################
// Table  Start 

$query ="
SELECT f.*, fc.name cname 
FROM `{$db_user}`.`{$this->config->default_prefix}faq` f 
LEFT OUTER JOIN `{$db_user}`.`{$this->config->default_prefix}faq_category` fc ON 
	f.prefixid = fc.prefixid 
	AND f.fcid = fc.fcid 
	AND fc.`switch`='Y' 
WHERE 
f.`prefixid` = '{$this->config->default_prefix_id}' 
AND f.`switch`='Y' 
";
$query .= $sub_search_query; 
$query .= $sub_sort_query; 
//$query .= $query_limit;  
$table = $this->model->getQueryRecord($query); 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start

// Relation product_category
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query); 
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];
 
// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

// Relation faq_category
$query ="
SELECT f.faqid faqid, f.fcid fcid, f.menu menu, f.seq fseq, fc.seq cseq, fc.name cname
FROM `{$db_user}`.`{$this->config->default_prefix}faq` f 
LEFT OUTER JOIN `{$db_user}`.`{$this->config->default_prefix}faq_category` fc ON 
	f.prefixid = fc.prefixid 
	AND f.fcid = fc.fcid 
	AND fc.`switch`='Y' 
WHERE 
f.`prefixid` = '{$this->config->default_prefix_id}' 
AND f.`switch`='Y'
ORDER BY cseq, fseq 
";
$recArr = $this->model->getQueryRecord($query); 

if($recArr["table"]['record']){ 
foreach($recArr["table"]['record'] as $rk => $rv)
{
	$fcid = $rv['fcid'];
	$faqid = $rv['faqid'];
	
	$menuArr[$fcid]['cname'] = $rv['cname']; 
	$menuArr[$fcid]['menu'][$faqid] = $rv['menu'];	 
}
}
$table["table"]["rt"]["faq_category"] = $menuArr;

// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"] . $status["status"]["search_path"] . $status["status"]["sort_path"] . $status["status"]["p"];


$this->tplVar('table' , $table['table']);
$this->tplVar('status',$status["status"]);
$this->display();
