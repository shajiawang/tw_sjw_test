<?php
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('收件人姓名错误!!');
}
if (empty($this->io->input["post"]["phone"])) {
	$this->jsAlertMsg('收件人手机错误!!');
}
if (empty($this->io->input["post"]["zip"])) {
	$this->jsAlertMsg('收件人邮编错误!!');
}
if (empty($this->io->input["post"]["address"])) {
	$this->jsAlertMsg('收件人地址错误!!');
}

// Check Variable End

##############################################################################################################################################
// Update Start

$orderid = $this->io->input["post"]["orderid"];

//訂單確定 Relation order
$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}order` SET 
	`memo` = '{$this->io->input["post"]["memo"]}'
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `orderid` = '{$orderid}'
"; 
$this->model->query($query);

//新增訂單收件人 Relation order_consignee
$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}order_consignee` SET 
	`switch`='N' 
where 	
	`prefixid`='{$this->config->default_prefix_id}'
	AND `userid`='{$userid}'
	AND `orderid`='{$orderid}'
";
$this->model->query($query);

$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}order_consignee` SET 
	`userid`='{$userid}',
	`orderid`='{$orderid}',
	`name`='{$this->io->input["post"]["name"]}',
	`phone`='{$this->io->input["post"]["phone"]}',
	`zip`='{$this->io->input["post"]["zip"]}',
	`address`='{$this->io->input["post"]["address"]}',
	`gender`='{$this->io->input["post"]["gender"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
";
$this->model->query($query);

// Update End
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])) );
