<?php
$backurl = "?channelid={$this->io->input['get']['channelid']}&epcid={$this->io->input["get"]["epcid"]}&epid={$this->io->input["get"]["epid"]}";

if (empty($this->io->input["get"]["orderid"])) {
	$location_url = $this->config->default_main ."/exchange_mall/product/{$backurl}";
	$location_url = base64_encode(urlencode($location_url));
	$this->jsPrintMsg('订单编号错误!!', $location_url);
} else {
	$orderid = $this->io->input["get"]["orderid"];
	$location_url = $this->config->default_main ."/exchange_mall/confirm/{$backurl}&orderid={$orderid}";
	$location_url = base64_encode(urlencode($location_url));
}

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
}

$userid = $this->io->input['session']['user']["userid"];

#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

if (empty($this->io->input["get"]["epcid"])) {
	//$this->jsPrintMsg('商品分类ID错误!!', $location_url);
}
if (empty($this->io->input["get"]["epid"])) {
	$this->jsPrintMsg('商品ID错误!!', $location_url);
}


// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
$query ="
SELECT o.*, p.name name, p.description description 
FROM `{$db_exchange}`.`{$this->config->default_prefix}order` o 
LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p ON 
	p.prefixid = o.prefixid 
	AND p.epid = o.epid 
WHERE 
o.prefixid = '{$this->config->default_prefix_id}' 
AND o.`switch`='Y' 
AND o.orderid = '{$orderid}' 
AND p.epid IS NOT NULL 
LIMIT 1
" ;
$table = $this->model->getQueryRecord($query); 

if (empty($table['table']['record'])) {
    $this->jsPrintMsg('订单不存在!', $location_url);
}

	
$num = isset($table['table']['record'][0]["num"]) ? (int)$table['table']['record'][0]["num"] : 0;
$point_price = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;

//商品兌換總點數
$used_point = $point_price * $num;
$table['table']['record'][0]['used_point'] = $used_point;

// Table Record End 

// Table End 
##############################################################################################################################################



##############################################################################################################################################
// Insert Start
if($table['table']['record'][0]["confirm"]=='N')
{
	//訂單確定 Relation order
	$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}order` SET 
		`confirm`='Y'  
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}' 
		AND `orderid` = '{$orderid}'
	"; 
	$this->model->query($query);

	//扣除 庫存  Relation stock
	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}stock` SET 
		`epid`='{$this->io->input["get"]["epid"]}',
		`num`='-{$num}',
		`orderid`='{$orderid}',
		`behav`='user_exchange',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	";
	$this->model->query($query);

	//扣除 紅利點數 Relation bonus
	$query ="INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}bonus` SET 
		`userid`='{$userid}',
		`behav`='user_exchange',
		`amount`='-{$table['table']['record'][0]['total_fee']}',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	";
	$this->model->query($query);
	$bonusid = $this->model->_con->insert_id;

	//Relation exchange_bonus_history
	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_history` SET 
		`userid`='{$userid}',
		`orderid`='{$orderid}',
		`bonusid`='{$bonusid}',
		`amount`='-{$table['table']['record'][0]['total_fee']}',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	";
	$this->model->query($query);
}

// Insert End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation order_consignee
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}order_consignee` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'  
AND `orderid`='{$orderid}'
AND `switch` = 'Y' 
LIMIT 1
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["user_profile"] = $recArr['table']['record'];

// Relation product_category
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();
