<?php
$backurl = "?channelid={$this->io->input['get']['channelid']}&epcid={$this->io->input["get"]["epcid"]}&epid={$this->io->input["get"]["epid"]}";
$location_url = $this->config->default_main ."/exchange_mall/product/{$backurl}"; 
$location_url = base64_encode(urlencode($location_url));

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

if (empty($this->io->input["get"]["epcid"])) {
	//$this->jsPrintMsg('商品分类ID错误!!', $location_url);
}
if (empty($this->io->input["get"]["epid"])) {
	$this->jsPrintMsg('商品ID错误!!', $location_url);
}


// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
$order_status = '0';
$num = isset($this->io->input["post"]["num"]) ? (int)$this->io->input["post"]["num"] : 0;

$query ="
SELECT p.*, 
unix_timestamp(offtime) as offtime, 
unix_timestamp() as `now` 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
WHERE 
p.`prefixid` = '{$this->config->default_prefix_id}'  
AND p.epid = '{$this->io->input["get"]["epid"]}'
AND p.switch = 'Y'
LIMIT 1
" ;
$table = $this->model->getQueryRecord($query); 

if (empty($table['table']['record'])) {
    $this->jsPrintMsg('商品不存在!', $location_url);
}
if ($table['table']['record'][0]['now'] > $table['table']['record'][0]['offtime']) {
    //$this->jsPrintMsg('商品兑换已结束', $location_url);
}


$retail_price = ($table['table']['record'][0]['retail_price']) ? (float)$table['table']['record'][0]['retail_price'] : 0;
$cost_price = ($table['table']['record'][0]['cost_price']) ? (float)$table['table']['record'][0]['cost_price'] : 0;
$point_price = ($table['table']['record'][0]['point_price']) ? (float)$table['table']['record'][0]['point_price'] : 0;
$process_fee = ($table['table']['record'][0]['process_fee']) ? (float)$table['table']['record'][0]['process_fee'] : 0;

//分潤=(市價-進貨價)*數量
$profit = ($retail_price - $cost_price) * $num;

//商品兌換總點數
$used_point = $point_price * $num;

//商品處理費總數
$used_process = $process_fee * $num;

//總費用
$total_fee = $used_process + $used_point;

$table['table']['record'][0]['num'] = $num;
$table['table']['record'][0]['used_point'] = $used_point;
$table['table']['record'][0]['used_process'] = $used_process;
$table['table']['record'][0]['total_fee'] = $total_fee;


//檢查使用者的紅利點數
$query ="
SELECT SUM(amount) bonus 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus`  
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch` = 'Y'
AND `userid` = '{$userid}' 
";
$recArr = $this->model->getQueryRecord($query); 
$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;

if($user_bonus < $total_fee) {
	$this->jsAlertMsg('红利点数不足!!');	 
}


// Table Record End 

// Table End 
##############################################################################################################################################



##############################################################################################################################################
// Insert Start

$this->io->input["get"]["orderid"] = '';
if (!empty($this->io->input["post"]["checkout"]) && $this->io->input["post"]["checkout"]=='Y') 
{
	//新增訂單 Relation order
	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}order` SET 
		`userid`='{$userid}',
		`status`='{$order_status}', 
		`epid`='{$this->io->input["get"]["epid"]}',
		`num`='{$num}',
		`esid`='{$this->io->input["post"]["esid"]}',
		`point_price`='{$point_price}',
		`process_fee`='{$used_process}',
		`total_fee`='{$total_fee}',
		`profit`='{$profit}',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	"; 
	$this->model->query($query);
	$orderid = $this->model->_con->insert_id; 

	$this->io->input["get"]["orderid"] = $orderid;
}

// Insert End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation user_profile
$table["table"]["rt"]["user_profile"] = $this->io->input['session']['user']["profile"];

// Relation product_category
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'  
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################

$location_url = $this->config->default_main ."/exchange_mall/confirm/{$backurl}"; 
$location_url = base64_encode(urlencode($location_url));
$status["status"]['location_url'] = $location_url;

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]); 
//echo '<pre>';print_r($status);die();
$this->display();
