<?php
##############################################################################################################################################
#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

if (empty($this->io->input["get"]["epcid"])) {
	//$this->jsPrintMsg('商品分类ID错误!!', $this->config->default_main);
}

if (empty($this->io->input["get"]["epid"])) {
	$this->jsPrintMsg('商品ID错误!!', $this->config->default_main);
}


// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
// Sort End

// Page Start
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

// Table  Start 

// Table Count Start 
// Table Count end 


// Table Record Start
$query ="
SELECT p.*,
unix_timestamp(p.offtime) as offtime,
unix_timestamp() as `now`,
pt.filename 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.eptid = pt.eptid
	AND pt.switch = 'Y'
WHERE 
p.`prefixid` = '{$this->config->default_prefix_id}' 
AND unix_timestamp() >= unix_timestamp(p.ontime)  
AND p.epid = '{$this->io->input["get"]["epid"]}'
AND p.switch = 'Y'
" ;
$table = $this->model->getQueryRecord($query);

// Table Record End 

// Table End 
##############################################################################################################################################



##############################################################################################################################################
// Relation Start 
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'  
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

//Relation stock
$query ="
SELECT SUM(num) num
FROM `{$db_exchange}`.`{$this->config->default_prefix}stock`  
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch` = 'Y' 
AND `epid` = '{$this->io->input["get"]["epid"]}'
";
$recArr = $this->model->getQueryRecord($query); 
$stock = ($recArr['table']['record'][0]['num']) ? (int)$recArr['table']['record'][0]['num'] : 0;
$table["table"]["rt"]["stock"] = ($stock > 20) ? 20 : $stock;


// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();