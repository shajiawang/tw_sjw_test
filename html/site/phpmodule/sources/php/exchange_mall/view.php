<?php
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

##############################################################################################################################################
#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

if (empty($this->io->input["get"]["epcid"])) {
	//$this->jsPrintMsg('商品分类错误!!', $this->config->default_main);
}

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(point_price|name|seq|modifyt)/";
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`". str_replace('sort_','',$sk) ."` ". $sv;
	}
	$sub_sort_query =  " ORDER BY ". implode(',', $orders);
} else {
	$sub_sort_query =  " ORDER BY p.offtime ASC";
}
// Sort End

// Search Start
$status["status"]["search"] = "";

$join_search_query = "";

$sub_search_query = ($this->io->input['session']['m_prefix']=='m') ? " AND p.mob_type !='N' " : "";
$sub_search_query .= " AND pc.epid IS NOT NULL AND c.epcid IS NOT NULL ";

if(!empty($this->io->input["get"]["search_name"])){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	
	$join_search_query .= "
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` pc ON 
	p.prefixid = pc.prefixid 
	AND p.epid = pc.epid 
	AND pc.`switch`='Y' 
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` c ON 
	c.prefixid = pc.prefixid 
	AND c.epcid = pc.epcid
	AND c.channelid = '{$this->io->input["get"]["channelid"]}'
	AND c.`switch`='Y' ";
	
	$sub_search_query .= " AND p.`name` like '%".$this->io->input["get"]["search_name"]."%' ";
}
if(!empty($this->io->input["get"]["search_pcid"])){
	$status["status"]["search"]["search_pcid"] = $this->io->input["get"]["search_pcid"] ;
	$status["status"]["search_path"] .= "&search_pcid=".$this->io->input["get"]["search_pcid"] ;
	
	$join_search_query .=  "
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` pc ON 
	p.prefixid = pc.prefixid 
	AND p.epid = pc.epid 
	AND pc.`epcid` = '{$this->io->input["get"]["search_pcid"]}' 
	AND pc.`switch`='Y' 
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` c ON 
	c.prefixid = pc.prefixid 
	AND c.epcid = pc.epcid
	AND c.channelid = '{$this->io->input["get"]["channelid"]}'
	AND c.`switch`='Y' ";
}
if(!empty($this->io->input["get"]["epcid"])){
	$status["status"]["search"]["epcid"] = $this->io->input["get"]["epcid"] ;
	$status["status"]["search_path"] .= "&epcid=".$this->io->input["get"]["epcid"] ;
	
	$join_search_query .=  "
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` pc ON 
	p.prefixid = pc.prefixid 
	AND p.epid = pc.epid 
	AND pc.`epcid` = '{$this->io->input["get"]["epcid"]}' 
	AND pc.`switch`='Y' 
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` c ON 
	c.prefixid = pc.prefixid 
	AND c.epcid = pc.epcid
	AND c.channelid = '{$this->io->input["get"]["channelid"]}'
	AND c.`switch`='Y' ";
}
// Search End


// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################
// Table  Start 

// Table Count Start
$query ="
SELECT count(*) as num
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
{$join_search_query} 
WHERE 
p.prefixid = '{$this->config->default_prefix_id}' 
AND p.switch = 'Y'
" ;
$query .= $sub_search_query ;
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 


// Table Record Start
$query ="
SELECT p.*, c.epcid
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
{$join_search_query} 
WHERE 
p.prefixid = '{$this->config->default_prefix_id}'  
AND p.switch = 'Y' 
";

$query .= $sub_search_query ;
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query); //var_dump($query);

if($table['table']['record'])
foreach($table['table']['record'] as $k => $v) {
	$table['table']['record'][$k] = $v;
	
	$query ="
	SELECT pt.filename 
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` pt ON 
		p.prefixid = pt.prefixid
		AND p.eptid = pt.eptid
		AND pt.switch = 'Y'
	WHERE 
		p.prefixid = '{$this->config->default_prefix_id}'
		AND p.epid = '{$v["epid"]}'
		AND p.switch = 'Y' 
	";
	$recArr = $this->model->getQueryRecord($query); 
	$table['table']['record'][$k]['thumbnail'] = $recArr['table']['record'][0]['filename'];
}

// Table Record End 

// Table End 
##############################################################################################################################################



##############################################################################################################################################
// Relation Start
$query ="
SELECT * 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];
 
// Relation exchange_product_category
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `channelid` = '{$this->io->input["get"]["channelid"]}'
AND `switch` = 'Y'
ORDER BY `seq`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_product_category"] = $recArr['table']['record'];

// Relation product_promote_rt 精选特卖
$query ="
SELECT p.*, pp.epcid, pt.filename, pr.seq pp_seq 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p
LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.eptid = pt.eptid
	AND pt.switch = 'Y'
LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}product_promote_rt` pr ON 
	p.prefixid = pr.prefixid 
	AND p.epid = pr.epid
	AND pr.closed = 'N' 	
	AND pr.switch = 'Y'
LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}product_promote` pp ON 
	pp.prefixid = pp.prefixid 
	AND pp.ppid = pr.ppid
	AND pp.`epcid` = '{$this->io->input["get"]["epcid"]}' 
	AND pp.switch = 'Y' 
WHERE 
p.prefixid = '{$this->config->default_prefix_id}'
AND p.switch = 'Y'
AND pr.ppid IS NOT NULL
AND pr.epid IS NOT NULL 
AND pp.`epcid` IS NOT NULL
ORDER BY `pp_seq`
limit 6
";
$recArr = $this->model->getQueryRecord($query); 
$table["table"]["rt"]["product_promote_rt"] = $recArr['table']['record'];
//var_dump($table["table"]["rt"]["product_promote_rt"]);


// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"] . $status["status"]["search_path"] . $status["status"]["sort_path"] . $status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();
