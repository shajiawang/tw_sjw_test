<?php
//QQ：2845249491
//密碼：xumu4hUF
require_once("/var/www/html/site/phpmodule/include/QQAPI/qqConnectAPI.php");
$qc = new QC(); //
$qc->qq_callback(); //"5393EF36EA109E3AF4D1801DBDCC3A25"
$token = $qc->get_openid(); //"6EB10D3EA7459C45F077197702F9635C" 

if ($token) 
{
	$_SESSION['token'] = $token;
	$uid = $token; 
	
	require_once "saja/mysql.ini.php";
	$this->model = new mysql($this->config->db[0]);
	$this->model->connect();
	
	$query = "
	SELECT
	u.*, s.ssoid, s.name as provider_name, s.uid as provider_uid, s.switch as sso_switch
	FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}sso` s 
	LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sso_rt` us ON
		s.prefixid = us.prefixid
		AND s.ssoid = us.ssoid
		AND us.switch = 'Y'
	LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` u ON
		us.prefixid = u.prefixid
		AND us.userid = u.userid
		AND u.switch = 'Y'
	WHERE
	s.prefixid = '{$this->config->default_prefix_id}' 
	AND s.name = 'qq'
	AND s.uid = '{$uid}'
	AND s.switch = 'Y'
	AND u.userid IS NOT NULL
	";
	$table = $this->model->getQueryRecord($query);
	
	if (empty($table['table']['record'])) {
		//$_SESSION['sso'] = (array)$token; //$user_profile;
		$_SESSION['sso']["provider"] = 'qq';
		$_SESSION['sso']["identifier"] = $uid;
		
		header("location: ".$this->config->default_main."/user/sso_register");
		die();
	}

	$user = $table['table']['record'][0];
	if ($user['switch'] != 'Y' || $user['sso_switch'] != 'Y') {
		$this->jsPrintMsg('账号已注销!!', $this->config->default_main);
	}

	$query = "
	SELECT *
	FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile`
	WHERE
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `userid` = '{$user['userid']}'
	AND `switch` =  'Y'
	";
	$table = $this->model->getQueryRecord($query);

	$user['profile'] = $table['table']['record'][0];
	$_SESSION['user'] = $user;
	
	header("location:".$this->config->default_main);	
} 
else 
{
	$this->jsPrintMsg('授权失败!!', $this->config->default_main);
}

