<?php
require_once("/var/www/html/site/phpmodule/include/QQAPI/qqConnectAPI.php");
$qc = new QC(); 
$qc->qq_login();

/*
object(QC)#7 (6) {
  ["kesArr":"QC":private]=>
  NULL
  ["APIMap":"QC":private]=>
  array(20) {
    ["add_blog"]=>
    array(3) {
      [0]=>
      string(38) "https://graph.qq.com/blog/add_one_blog"
      [1]=>
      array(3) {
        [0]=>
        string(5) "title"
        ["format"]=>
        string(4) "json"
        ["content"]=>
        NULL
      }
      [2]=>
      string(4) "POST"
    }
    ["add_topic"]=>
    array(3) {
      [0]=>
      string(39) "https://graph.qq.com/shuoshuo/add_topic"
      [1]=>
      array(8) {
        [0]=>
        string(8) "richtype"
        [1]=>
        string(7) "richval"
        [2]=>
        string(3) "con"
        [3]=>
        string(7) "#lbs_nm"
        [4]=>
        string(6) "#lbs_x"
        [5]=>
        string(6) "#lbs_y"
        ["format"]=>
        string(4) "json"
        [6]=>
        string(13) "#third_source"
      }
      [2]=>
      string(4) "POST"
    }
    ["get_user_info"]=>
    array(3) {
      [0]=>
      string(39) "https://graph.qq.com/user/get_user_info"
      [1]=>
      array(1) {
        ["format"]=>
        string(4) "json"
      }
      [2]=>
      string(3) "GET"
    }
    ["add_one_blog"]=>
    array(3) {
      [0]=>
      string(38) "https://graph.qq.com/blog/add_one_blog"
      [1]=>
      array(3) {
        [0]=>
        string(5) "title"
        [1]=>
        string(7) "content"
        ["format"]=>
        string(4) "json"
      }
      [2]=>
      string(3) "GET"
    }
    ["add_album"]=>
    array(3) {
      [0]=>
      string(36) "https://graph.qq.com/photo/add_album"
      [1]=>
      array(4) {
        [0]=>
        string(9) "albumname"
        [1]=>
        string(10) "#albumdesc"
        [2]=>
        string(5) "#priv"
        ["format"]=>
        string(4) "json"
      }
      [2]=>
      string(4) "POST"
    }
    ["upload_pic"]=>
    array(3) {
      [0]=>
      string(37) "https://graph.qq.com/photo/upload_pic"
      [1]=>
      array(11) {
        [0]=>
        string(7) "picture"
        [1]=>
        string(10) "#photodesc"
        [2]=>
        string(6) "#title"
        [3]=>
        string(8) "#albumid"
        [4]=>
        string(7) "#mobile"
        [5]=>
        string(2) "#x"
        [6]=>
        string(2) "#y"
        [7]=>
        string(9) "#needfeed"
        [8]=>
        string(11) "#successnum"
        [9]=>
        string(7) "#picnum"
        ["format"]=>
        string(4) "json"
      }
      [2]=>
      string(4) "POST"
    }
    ["list_album"]=>
    array(2) {
      [0]=>
      string(37) "https://graph.qq.com/photo/list_album"
      [1]=>
      array(1) {
        ["format"]=>
        string(4) "json"
      }
    }
    ["add_share"]=>
    array(3) {
      [0]=>
      string(36) "https://graph.qq.com/share/add_share"
      [1]=>
      array(11) {
        [0]=>
        string(5) "title"
        [1]=>
        string(3) "url"
        [2]=>
        string(8) "#comment"
        [3]=>
        string(8) "#summary"
        [4]=>
        string(7) "#images"
        ["format"]=>
        string(4) "json"
        [5]=>
        string(5) "#type"
        [6]=>
        string(8) "#playurl"
        [7]=>
        string(5) "#nswb"
        [8]=>
        string(4) "site"
        [9]=>
        string(7) "fromurl"
      }
      [2]=>
      string(4) "POST"
    }
    ["check_page_fans"]=>
    array(2) {
      [0]=>
      string(41) "https://graph.qq.com/user/check_page_fans"
      [1]=>
      array(2) {
        ["page_id"]=>
        string(9) "314416946"
        ["format"]=>
        string(4) "json"
      }
    }
    ["add_t"]=>
    array(3) {
      [0]=>
      string(28) "https://graph.qq.com/t/add_t"
      [1]=>
      array(5) {
        ["format"]=>
        string(4) "json"
        [0]=>
        string(7) "content"
        [1]=>
        string(9) "#clientip"
        [2]=>
        string(10) "#longitude"
        [3]=>
        string(15) "#compatibleflag"
      }
      [2]=>
      string(4) "POST"
    }
    ["add_pic_t"]=>
    array(3) {
      [0]=>
      string(32) "https://graph.qq.com/t/add_pic_t"
      [1]=>
      array(8) {
        [0]=>
        string(7) "content"
        [1]=>
        string(3) "pic"
        ["format"]=>
        string(4) "json"
        [2]=>
        string(9) "#clientip"
        [3]=>
        string(10) "#longitude"
        [4]=>
        string(9) "#latitude"
        [5]=>
        string(9) "#syncflag"
        [6]=>
        string(15) "#compatiblefalg"
      }
      [2]=>
      string(4) "POST"
    }
    ["del_t"]=>
    array(3) {
      [0]=>
      string(28) "https://graph.qq.com/t/del_t"
      [1]=>
      array(2) {
        [0]=>
        string(2) "id"
        ["format"]=>
        string(4) "json"
      }
      [2]=>
      string(4) "POST"
    }
    ["get_repost_list"]=>
    array(2) {
      [0]=>
      string(38) "https://graph.qq.com/t/get_repost_list"
      [1]=>
      array(7) {
        [0]=>
        string(4) "flag"
        [1]=>
        string(6) "rootid"
        [2]=>
        string(8) "pageflag"
        [3]=>
        string(8) "pagetime"
        [4]=>
        string(6) "reqnum"
        [5]=>
        string(9) "twitterid"
        ["format"]=>
        string(4) "json"
      }
    }
    ["get_info"]=>
    array(2) {
      [0]=>
      string(34) "https://graph.qq.com/user/get_info"
      [1]=>
      array(1) {
        ["format"]=>
        string(4) "json"
      }
    }
    ["get_other_info"]=>
    array(2) {
      [0]=>
      string(40) "https://graph.qq.com/user/get_other_info"
      [1]=>
      array(3) {
        ["format"]=>
        string(4) "json"
        [0]=>
        string(5) "#name"
        [1]=>
        string(7) "fopenid"
      }
    }
    ["get_fanslist"]=>
    array(2) {
      [0]=>
      string(42) "https://graph.qq.com/relation/get_fanslist"
      [1]=>
      array(6) {
        ["format"]=>
        string(4) "json"
        [0]=>
        string(6) "reqnum"
        [1]=>
        string(10) "startindex"
        [2]=>
        string(5) "#mode"
        [3]=>
        string(8) "#install"
        [4]=>
        string(4) "#sex"
      }
    }
    ["get_idollist"]=>
    array(2) {
      [0]=>
      string(42) "https://graph.qq.com/relation/get_idollist"
      [1]=>
      array(5) {
        ["format"]=>
        string(4) "json"
        [0]=>
        string(6) "reqnum"
        [1]=>
        string(10) "startindex"
        [2]=>
        string(5) "#mode"
        [3]=>
        string(8) "#install"
      }
    }
    ["add_idol"]=>
    array(3) {
      [0]=>
      string(38) "https://graph.qq.com/relation/add_idol"
      [1]=>
      array(3) {
        ["format"]=>
        string(4) "json"
        [0]=>
        string(7) "#name-1"
        [1]=>
        string(11) "#fopenids-1"
      }
      [2]=>
      string(4) "POST"
    }
    ["del_idol"]=>
    array(3) {
      [0]=>
      string(38) "https://graph.qq.com/relation/del_idol"
      [1]=>
      array(3) {
        ["format"]=>
        string(4) "json"
        [0]=>
        string(7) "#name-1"
        [1]=>
        string(10) "#fopenid-1"
      }
      [2]=>
      string(4) "POST"
    }
    ["get_tenpay_addr"]=>
    array(2) {
      [0]=>
      string(45) "https://graph.qq.com/cft_info/get_tenpay_addr"
      [1]=>
      array(4) {
        ["ver"]=>
        int(1)
        ["limit"]=>
        int(5)
        ["offset"]=>
        int(0)
        ["format"]=>
        string(4) "json"
      }
    }
  }
  ["recorder":protected]=>
  object(Recorder)#8 (2) {
    ["inc":"Recorder":private]=>
    object(stdClass)#10 (10) {
      ["appid"]=>
      string(9) "101011175"
      ["appkey"]=>
      string(32) "a6037a56bbab29d79fd39f882701d36a"
      ["callback"]=>
      string(42) "http://210.61.12.136/site/oauth/qqcallback"
      ["scope"]=>
      string(225) "get_user_info,add_share,list_album,add_album,upload_pic,add_topic,add_one_blog,add_weibo,check_page_fans,add_t,add_pic_t,del_t,get_repost_list,get_info,get_other_info,get_fanslist,get_idolist,add_idol,del_idol,get_tenpay_addr"
      ["errorReport"]=>
      bool(true)
      ["storageType"]=>
      string(4) "file"
      ["host"]=>
      string(9) "localhost"
      ["user"]=>
      string(4) "root"
      ["password"]=>
      string(4) "root"
      ["database"]=>
      string(4) "test"
    }
    ["error":"Recorder":private]=>
    object(ErrorCase)#9 (1) {
      ["errorMsg":"ErrorCase":private]=>
      array(3) {
        [20001]=>
        string(66) "<h2>配置文件损坏或无法读取，请重新执行intall</h2>"
        [30001]=>
        string(63) "<h2>The state does not match. You may be a victim of CSRF.</h2>"
        [50001]=>
        string(167) "<h2>可能是服务器无法请求https协议</h2>可能未开启curl支持,请尝试开启curl支持，重启web服务器，如果问题仍未解决，请联系我们"
      }
    }
  }
  ["urlUtils"]=>
  object(URL)#11 (1) {
    ["error":"URL":private]=>
    object(ErrorCase)#12 (1) {
      ["errorMsg":"ErrorCase":private]=>
      array(3) {
        [20001]=>
        string(66) "<h2>配置文件损坏或无法读取，请重新执行intall</h2>"
        [30001]=>
        string(63) "<h2>The state does not match. You may be a victim of CSRF.</h2>"
        [50001]=>
        string(167) "<h2>可能是服务器无法请求https协议</h2>可能未开启curl支持,请尝试开启curl支持，重启web服务器，如果问题仍未解决，请联系我们"
      }
    }
  }
  ["error":protected]=>
  object(ErrorCase)#13 (1) {
    ["errorMsg":"ErrorCase":private]=>
    array(3) {
      [20001]=>
      string(66) "<h2>配置文件损坏或无法读取，请重新执行intall</h2>"
      [30001]=>
      string(63) "<h2>The state does not match. You may be a victim of CSRF.</h2>"
      [50001]=>
      string(167) "<h2>可能是服务器无法请求https协议</h2>可能未开启curl支持,请尝试开启curl支持，重启web服务器，如果问题仍未解决，请联系我们"
    }
  }
  ["keysArr"]=>
  array(3) {
    ["oauth_consumer_key"]=>
    int(101011175)
    ["access_token"]=>
    NULL
    ["openid"]=>
    NULL
  }
}
*/
