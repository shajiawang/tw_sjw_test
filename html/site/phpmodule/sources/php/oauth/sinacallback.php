<?php
include_once("/var/www/html/site/phpmodule/include/SinaAPI/saetv2.ex.class.php");

$o = new SaeTOAuthV2( WB_AKEY , WB_SKEY );

if (isset($_REQUEST['code'])) {
	$keys = array();
	$keys['code'] = $_REQUEST['code'];
	$keys['redirect_uri'] = WB_CALLBACK_URL;
	try {
		$token = $o->getAccessToken( 'code', $keys ) ;
	} catch (OAuthException $e) {
	}
}

/*
 http://howard.sajar.com.tw/site/oauth/sinacallback?code=e873f59f095a2726e9d6a98ed363e7b4
$token = array("access_token"=> "2.00Rj2b2EHq4bOD60766cfe0auCiYgB", 
	"remind_in"=> "126669", 
	"expires_in"=> 126669, 
	"uid"=> "3983736051"
);
*/

if ($token) 
{
	$_SESSION['token'] = $token;
	//setcookie( 'weibojs_'.$o->client_id, http_build_query($token) );
	//授权完成,<a href="weibolist.php">进入你的微博列表页面</a><br />
	$uid = $token["uid"]; 
	
	require_once "saja/mysql.ini.php";
	$this->model = new mysql($this->config->db[0]);
	$this->model->connect();
	
	$query = "
	SELECT
	u.*, s.ssoid, s.name as provider_name, s.uid as provider_uid, s.switch as sso_switch
	FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}sso` s 
	LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_sso_rt` us ON
		s.prefixid = us.prefixid
		AND s.ssoid = us.ssoid
		AND us.switch = 'Y'
	LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` u ON
		us.prefixid = u.prefixid
		AND us.userid = u.userid
		AND u.switch = 'Y'
	WHERE
	s.prefixid = '{$this->config->default_prefix_id}' 
	AND s.name = 'sina'
	AND s.uid = '{$uid}'
	AND s.switch = 'Y'
	AND u.userid IS NOT NULL
	";
	$table = $this->model->getQueryRecord($query);
	
	if (empty($table['table']['record'])) {
		//$_SESSION['sso'] = (array)$token; //$user_profile;
		$_SESSION['sso']["provider"] = 'sina';
		$_SESSION['sso']["identifier"] = $uid;
		
		header("location: ".$this->config->default_main."/user/sso_register");
		die();
	}

	$user = $table['table']['record'][0];
	if ($user['switch'] != 'Y' || $user['sso_switch'] != 'Y') {
		$this->jsPrintMsg('账号已注销!!', $this->config->default_main);
	}

	$query = "
	SELECT *
	FROM `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_profile`
	WHERE
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `userid` = '{$user['userid']}'
	AND `switch` =  'Y'
	";
	$table = $this->model->getQueryRecord($query);

	$user['profile'] = $table['table']['record'][0];
	$_SESSION['user'] = $user;
	
	header("location:".$this->config->default_main);	
} 
else 
{
	$this->jsPrintMsg('授权失败!!', $this->config->default_main);
}
