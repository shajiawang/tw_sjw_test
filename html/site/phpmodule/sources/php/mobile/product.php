<?php

$recArr = array(
	1 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'productid' => 1,
					'type' => 'complex', //綜合標
					'retail_price' => 9990, //建議售價
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg', //產品主圖
					'spoint' => 35, //所需殺幣
					'limit_price' => 1, //殺價底價
					'offtime' => '2013-11-05 22:30:00' //結標時間
				)
			),
			"rt" => array(
				"product_category_rt" => array(
					array(
						"productid" => 1,
						"pcid" => 1,
						"name" => "強檔熱賣"
					)
				),
				"saja_history" => array(
					array(
						"productid" => 1,
						"userid" => 1,
						"name" => 'Mark' //目前得標殺友
					)
				)
			)
		)
	),
	2 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'productid' => 2,
					'type' => 'saja', //下標數
					'retail_price' => 6666, //建議售價
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg', //產品主圖
					'spoint' => 66, //所需殺幣
					'limit_price' => 10, //殺價底價
					'offtime' => '2014-01-15 12:00:00' //結標時間
				)
			),
			"rt" => array(
				"product_category_rt" => array(
					array(
						"productid" => 2,
						"pcid" => 1,
						"name" => "強檔熱賣"
					)
				),
				"saja_history" => array(
					array(
						"productid" => 1,
						"userid" => 2,
						"name" => 'Tom' //目前得標殺友
					)
				)
			)
		)
	),
	3 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'productid' => 3,
					'type' => 'complex', //綜合標
					'retail_price' => 6485, //建議售價
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg', //產品主圖
					'spoint' => 50, //所需殺幣
					'limit_price' => 100, //殺價底價
					'offtime' => '2013-12-15 11:43:00' //結標時間
				)
			),
			"rt" => array(
				"product_category_rt" => array(
					array(
						"productid" => 3,
						"pcid" => 1,
						"name" => "強檔熱賣"
					)
				),
				"saja_history" => array(
					array(
						"productid" => 3,
						"userid" => 3,
						"name" => 'Stark' //目前得標殺友
					)
				)
			)
		)
	)
);

$this->tplVar("table", $recArr[$this->io->input["get"]["productid"]]['table']);
$this->display();