<?php
$table = array(
	"table" => array(
		"record" => array(
			0 => array(
				'tutorialid' => 1,
				'tcid' => 1,
				'name' => '快速上手',
			),
			1 => array(
				'tutorialid' => 2,
				'tcid' => 1,
				'name' => '殺價式拍賣概觀',
			),
			2 => array(
				'tutorialid' => 3,
				'tcid' => 2,
				'name' => '馬上體驗殺價的快感!!',
			),
			3 => array(
				'tutorialid' => 4,
				'tcid' => 2,
				'name' => '如何殺價?',
			),
			4 => array(
				'tutorialid' => 5,
				'tcid' => 3,
				'name' => '結標後要怎麼付款?',
			),
			5 => array(
				'tutorialid' => 6,
				'tcid' => 3,
				'name' => '什麼是流標?',
			)
		),
		"rt" => array(
			"tutorial_category" => array(
				array(
					"tcid" => 1,
					"name" => '新手必看'
				),
				array(
					"tcid" => 2,
					"name" => '殺價競標相關'
				),
				array(
					"tcid" => 3,
					"name" => '得標後續相關'
				)
			)
		)
	)
);

$this->tplVar("table", $table['table']);
$this->display();