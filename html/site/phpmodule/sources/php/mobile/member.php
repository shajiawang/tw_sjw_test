<?php

$table = array(
	"table" => array(
		"record" => array(
			0 => array(
				'userid' => 1,
				'recipient' => 'Matt', //收件人
				'sex' => 'male',
				'email' => 'test@saja.com.tw',
				'address' => '104台北市中山區中山北路一段104號',
				'phone' => '0952123456',
				'nickname' => '測試人'
			)
		),
		"rt" => array(
			'spoint' => array(
				"userid" => 1,
				"amount" => 130
			),
			'bonus' => array(
				"userid" => 1,
				"amount" => 30
			),
			'sgift' => array(
				"userid" => 1,
				"amount" => 30
			),
			'bulletin' => array(
				array(
					'bulletinid' => 1,
					"name" => '【再加碼】光陽機車再來一台',
					'description' => '光陽機車再來一台光陽機車再來一台光陽機車再來一台光陽機車再來一台',
					'ontime' => '2013-10-05 17:12:34',
					'type' => 'member'
				),
				array(
					'bulletinid' => 2,
					"name" => '【系統維修】系統維修公告',
					'description' => '維修維修維修維修維修維修公告公告公告公告公告公告', 
					'ontime' => '2013-11-15 10:22:18',
					'type' => 'system'
				)
			)
		)
	)
);

$this->tplVar("table", $table['table']);
$this->display();