<?php
// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
} else {
	$this->io->input["get"]["p"] = 1;
}
// Page End


$recArr = array(
	1 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'productid' => 1,
					'type' => 'complex', //綜合標
					'retail_price' => 9990, //建議售價
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'spoint' => 35, //所需殺幣
					'limit_price' => 1, //殺價底價
					'offtime' => '2013-11-05 22:30:00' //結標時間
				),
				1 => array(
					'productid' => 2,
					'type' => 'time', //時間標
					'retail_price' => 6485, //建議售價
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'spoint' => 50, //所需殺幣
					'limit_price' => 100, //殺價底價
					'offtime' => '2013-12-15 11:43:00' //結標時間
				)
			),
			"rt" => array(
				"product_category_rt" => array(
					array(
						"productid" => 1,
						"pcid" => 1,
						"name" => "強檔熱賣"
					),
					array(
						"productid" => 2,
						"pcid" => 1,
						"name" => "強檔熱賣"
					)
				)
			)
		)
	),
	2 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'productid' => 3,
					'type' => 'saja', //下標數
					'retail_price' => 6666, //建議售價
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'spoint' => 66, //所需殺幣
					'limit_price' => 10, //殺價底價
					'offtime' => '2014-01-15 12:00:00' //結標時間
				),
				1 => array(
					'productid' => 4,
					'type' => 'complex', //綜合標
					'retail_price' => 6485, //建議售價
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'spoint' => 50, //所需殺幣
					'limit_price' => 100, //殺價底價
					'offtime' => '2013-12-15 11:43:00' //結標時間
				)
			),
			"rt" => array(
				"product_category_rt" => array(
					array(
						"productid" => 3,
						"pcid" => 1,
						"name" => "強檔熱賣"
					),
					array(
						"productid" => 4,
						"pcid" => 1,
						"name" => "強檔熱賣"
					)
				)
			)
		)
	)
);

$page[1] = array(
	"rec_start" => 1,
	"rec_end" => 2,
	"firstpage" => 1,
	"lastpage" => 2,
	"previousrange" => -9,
	"nextrange" => 11,
	"previouspage" => 2,
	"nextpage" => 2,
	"thispage" => 1,
	"total" => 4,
	"loop" => 4,
	"max_page" => 2,
	"max_range" => 10,
	"item" => array(
		0 => array(
			"p" => 1
		),
		1 => array(
			"p" => 2
		)
	)
);
$page[2] = array(
	"rec_start" => 3,
	"rec_end" => 4,
	"firstpage" => 1,
	"lastpage" => 2,
	"previousrange" => -9,
	"nextrange" => 11,
	"previouspage" => 1,
	"nextpage" => 1,
	"thispage" => 2,
	"total" => 4,
	"loop" => 4,
	"max_page" => 2,
	"max_range" => 10,
	"item" => array(
		0 => array(
			"p" => 1
		),
		1 => array(
			"p" => 2
		)
	)
);

$this->tplVar("table", $recArr[$this->io->input["get"]["p"]]['table']);
$this->tplVar("page", $page[$this->io->input["get"]["p"]]);
$this->display();