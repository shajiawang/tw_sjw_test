<?php
// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
} else {
	$this->io->input["get"]["p"] = 1;
}
// Page End


$recArr = array(
	1 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'sgiftid' => 1,
					'amount' => 3,
					'insertt' => '2013-12-21 04:11:33'
				),
				1 => array(
					'sgiftid' => 2,
					'amount' => 5,
					'insertt' => '2013-01-13 14:31:16'
				)
			),
			"rt" => array(
				"sgift_rule" => array(
					array(
						"sgiftid" => 1,
						"productid" => 1,
						"name" => "光陽機車Nikita 300 Fi雙燈五期噴射版"
					),
					array(
						"sgiftid" => 2,
						"productid" => 2,
						"name" => "光陽機車G5 Fi噴射版"
					)
				)
			)
		)
	),
	2 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'sgiftid' => 3,
					'amount' => 1,
					'insertt' => '2013-12-21 04:11:33'
				),
				1 => array(
					'sgiftid' => 2,
					'amount' => 2,
					'insertt' => '2013-01-13 14:31:16'
				)
			),
			"rt" => array(
				"sgift_rule" => array(
					array(
						"sgiftid" => 3,
						"productid" => 3,
						"name" => "Nikita 300 Fi雙燈五期噴射版"
					),
					array(
						"sgiftid" => 4,
						"productid" => 4,
						"name" => "G5 Fi噴射版"
					)
				)
			)
		)
	)
);

$page[1] = array(
	"rec_start" => 1,
	"rec_end" => 2,
	"firstpage" => 1,
	"lastpage" => 2,
	"previousrange" => -9,
	"nextrange" => 11,
	"previouspage" => 2,
	"nextpage" => 2,
	"thispage" => 1,
	"total" => 4,
	"loop" => 4,
	"max_page" => 2,
	"max_range" => 10,
	"item" => array(
		0 => array(
			"p" => 1
		),
		1 => array(
			"p" => 2
		)
	)
);
$page[2] = array(
	"rec_start" => 3,
	"rec_end" => 4,
	"firstpage" => 1,
	"lastpage" => 2,
	"previousrange" => -9,
	"nextrange" => 11,
	"previouspage" => 1,
	"nextpage" => 1,
	"thispage" => 2,
	"total" => 4,
	"loop" => 4,
	"max_page" => 2,
	"max_range" => 10,
	"item" => array(
		0 => array(
			"p" => 1
		),
		1 => array(
			"p" => 2
		)
	)
);

$this->tplVar("table", $recArr[$this->io->input["get"]["p"]]['table']);
$this->tplVar("page", $page[$this->io->input["get"]["p"]]);
$this->display();