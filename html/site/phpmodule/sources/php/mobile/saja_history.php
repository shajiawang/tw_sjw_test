<?php
// Page Start
if(!empty($this->io->input["get"]["p"])){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
} else {
	$this->io->input["get"]["p"] = 1;
}
// Page End


$recArr = array(
	1 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'productid' => 1,
					'name' => '美國VORNADO533渦流空氣循環機（黑色）',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'complex', //綜合標
					'retail_price' => 9990, //建議售價
					'spoint' => 35, //所需殺幣
					'limit_price' => 1 //殺價底價
				),
				1 => array(
					'productid' => 2,
					'name' => '家樂福提貨券500元',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'time', //時間標
					'retail_price' => 6485, //建議售價
					'spoint' => 50, //所需殺幣
					'limit_price' => 100 //殺價底價
				),
				2 => array(
					'productid' => 3,
					'name' => '美國VORNADO533渦流空氣循環機（黑色）',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'time', //時間標
					'retail_price' => 6485, //建議售價
					'spoint' => 50, //所需殺幣
					'limit_price' => 100 //殺價底價
				),
				3 => array(
					'productid' => 4,
					'name' => '家樂福提貨券500元',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'time', //時間標
					'retail_price' => 6485, //建議售價
					'spoint' => 50, //所需殺幣
					'limit_price' => 100 //殺價底價
				)
			),
			"rt" => array(
				"pay_get_product" => array(
					array(
						"productid" => 1,
						"userid" => 2,
						"name" => "John",
						"price" => 13,
						"inesrtt" => '2013-12-15 11:43:00'
					),
					array(
						"productid" => 2,
						"userid" => 12,
						"name" => "Mary",
						"price" => 53,
						"inesrtt" => '2013-12-15 11:43:00'
					),
					array(
						"productid" => 3,
						"userid" => 55,
						"name" => "Jack",
						"price" => 26,
						"inesrtt" => '2013-12-15 11:43:00'
					),
					array(
						"productid" => 4,
						"userid" => 122,
						"name" => "Kim",
						"price" => 83,
						"inesrtt" => '2013-12-15 11:43:00'
					)
				)
			)
		)
	),
	2 => array(
		"table" => array(
			"record" => array(
				0 => array(
					'productid' => 1,
					'name' => '中國VORNADO66渦流空氣循環機（黑色）',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'complex', //綜合標
					'retail_price' => 9990, //建議售價
					'spoint' => 35, //所需殺幣
					'limit_price' => 1 //殺價底價
				),
				1 => array(
					'productid' => 2,
					'name' => '大潤發提貨券1000元',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'time', //時間標
					'retail_price' => 6485, //建議售價
					'spoint' => 50, //所需殺幣
					'limit_price' => 100 //殺價底價
				),
				2 => array(
					'productid' => 3,
					'name' => '中國VORNADO66渦流空氣循環機（黑色）',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'time', //時間標
					'retail_price' => 6485, //建議售價
					'spoint' => 50, //所需殺幣
					'limit_price' => 100 //殺價底價
				),
				3 => array(
					'productid' => 4,
					'name' => '大潤發提貨券1000元',
					'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
					'type' => 'time', //時間標
					'retail_price' => 6485, //建議售價
					'spoint' => 50, //所需殺幣
					'limit_price' => 100 //殺價底價
				)
			),
			"rt" => array(
				"pay_get_product" => array(
					array(
						"productid" => 5,
						"userid" => 2,
						"name" => "Hank",
						"price" => 6,
						"inesrtt" => '2013-12-15 11:43:00'
					),
					array(
						"productid" => 6,
						"userid" => 12,
						"name" => "Taylor",
						"price" => 52,
						"inesrtt" => '2013-12-15 11:43:00'
					),
					array(
						"productid" => 7,
						"userid" => 55,
						"name" => "Michael",
						"price" => 72,
						"inesrtt" => '2013-12-15 11:43:00'
					),
					array(
						"productid" => 8,
						"userid" => 122,
						"name" => "Meow",
						"price" => 13,
						"inesrtt" => '2013-12-15 11:43:00'
					)
				)
			)
		)
	)
);

$page[1] = array(
	"rec_start" => 1,
	"rec_end" => 4,
	"firstpage" => 1,
	"lastpage" => 2,
	"previousrange" => -9,
	"nextrange" => 11,
	"previouspage" => 2,
	"nextpage" => 2,
	"thispage" => 1,
	"total" => 8,
	"loop" => 8,
	"max_page" => 4,
	"max_range" => 10,
	"item" => array(
		0 => array(
			"p" => 1
		),
		1 => array(
			"p" => 2
		)
	)
);
$page[2] = array(
	"rec_start" => 5,
	"rec_end" => 8,
	"firstpage" => 1,
	"lastpage" => 2,
	"previousrange" => -9,
	"nextrange" => 11,
	"previouspage" => 1,
	"nextpage" => 1,
	"thispage" => 2,
	"total" => 8,
	"loop" => 8,
	"max_page" => 4,
	"max_range" => 10,
	"item" => array(
		0 => array(
			"p" => 1
		),
		1 => array(
			"p" => 2
		)
	)
);

$this->tplVar("table", $recArr[$this->io->input["get"]["p"]]['table']);
$this->tplVar("page", $page[$this->io->input["get"]["p"]]);
$this->display();