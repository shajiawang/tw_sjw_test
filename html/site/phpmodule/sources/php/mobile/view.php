<?php
$table = array(
	"table" => array(
		"record" => array(
			0 => array(
				'productid' => 1,
				'type' => 'complex', //綜合標
				'retail_price' => 9990, //建議售價
				'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
				'name' => '大潤發提貨券 500元',
				'description' => '可以全台走透透的提貨券，您可以至各地大潤發分店進行消費',
				'spoint' => 35, //所需殺幣
				'limit_price' => 1, //殺價底價
				'offtime' => '2013-11-25 22:30:00' //結標時間
			),
			1 => array(
				'productid' => 2,
				'type' => 'time', //時間標
				'retail_price' => 6485, //建議售價
				'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
				'name' => 'PRADA太陽眼鏡',
				'description' => '可以全台走透透的提貨券，您可以至各地大潤發分店進行消費',
				'spoint' => 50, //所需殺幣
				'limit_price' => 100, //殺價底價
				'offtime' => '2013-12-15 11:43:00' //結標時間
			),
			2 => array(
				'productid' => 3,
				'type' => 'time', //時間標
				'retail_price' => 6485, //建議售價
				'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
				'name' => '大潤發提貨券 500元',
				'description' => '可以全台走透透的提貨券，您可以至各地大潤發分店進行消費',
				'spoint' => 50, //所需殺幣
				'limit_price' => 100, //殺價底價
				'offtime' => '2013-12-16 11:43:00' //結標時間
			),
			3 => array(
				'productid' => 4,
				'type' => 'time', //時間標
				'retail_price' => 6485, //建議售價
				'thumbnail' => 'http://img.saja.com.tw/main/by_mian0917.jpg',
				'name' => '大潤發提貨券 500元',
				'description' => '可以全台走透透的提貨券，您可以至各地大潤發分店進行消費',
				'spoint' => 50, //所需殺幣
				'limit_price' => 100, //殺價底價
				'offtime' => '2013-12-17 11:43:00' //結標時間
			)
		)
	)
);


$this->tplVar("table", $table['table']);
$this->display();