<?php
if(empty($this->io->input['get']['epcid'])) {
	$this->io->input['get']['epcid'] = 0;
}

$table[0] = array(
	"table" => array(
		"record" => array(),
		"rt" => array(
			"exchange_product_category" => array(
				array(
					"epcid" => 1,
					"name" => '3C產品'
				),
				array(
					"epcid" => 2,
					"name" => '汽車'
				),
				array(
					"epcid" => 3,
					"name" => '機車'
				)
			),
			'bonus' => array(
				"userid" => 1,
				"amount" => 1300
			),
			"stock" => array()
		)
	)
);
$table[1] = array(
	"table" => array(
		"record" => array(
			0 => array(
				'epid' => 1,
				'name' => '奇美電視',
				'fee' => 20,
				'retail_price' => 1000,
				'point_price' => 300
			),
			1 => array(
				'epid' => 4,
				'name' => 'Macbook Pro',
				'fee' => 20,
				'retail_price' => 1000,
				'point_price' => 300
			),
			2 => array(
				'epid' => 5,
				'name' => 'iPhone 5s',
				'fee' => 20,
				'retail_price' => 1000,
				'point_price' => 300
			),
			3 => array(
				'epid' => 6,
				'name' => 'iPad Mini',
				'fee' => 20,
				'retail_price' => 1000,
				'point_price' => 300
			)
		),
		"rt" => array(
			"exchange_product_category" => array(
				array(
					"epcid" => 1,
					"name" => '3C產品'
				),
				array(
					"epcid" => 2,
					"name" => '汽車'
				),
				array(
					"epcid" => 3,
					"name" => '機車'
				)
			),
			'bonus' => array(
				"userid" => 1,
				"amount" => 1300
			),
			'stock' => array(
				0 => array(
					"epid" => 1,
					"num" => 43
				),
				1 => array(
					"epid" => 4,
					"num" => 73
				),
				2 => array(
					"epid" => 5,
					"num" => 13
				),
				3 => array(
					"epid" => 6,
					"num" => 23
				)
			)
		)
	)
);
$table[2] = array(
	"table" => array(
		"record" => array(
			0 => array(
				'epid' => 3,
				'name' => '馬自達5',
			)
		),
		"rt" => array(
			"exchange_product_category" => array(
				array(
					"epcid" => 1,
					"name" => '3C產品'
				),
				array(
					"epcid" => 2,
					"name" => '汽車'
				),
				array(
					"epcid" => 3,
					"name" => '機車'
				)
			),
			'bonus' => array(
				"userid" => 1,
				"amount" => 1300
			),
			'stock' => array(
				0 => array(
					"epid" => 3,
					"num" => 43
				)
			)
		)
	)
);
$table[3] = array(
	"table" => array(
		"record" => array(
			0 => array(
				'epid' => 2,
				'name' => '三陽機車',
			)
		),
		"rt" => array(
			"exchange_product_category" => array(
				array(
					"epcid" => 1,
					"name" => '3C產品'
				),
				array(
					"epcid" => 2,
					"name" => '汽車'
				),
				array(
					"epcid" => 3,
					"name" => '機車'
				)
			),
			'bonus' => array(
				"userid" => 1,
				"amount" => 1300
			),
			'stock' => array(
				0 => array(
					"epid" => 2,
					"num" => 43
				)
			)
		)
	)
);


$this->tplVar("table", $table[$this->io->input['get']['epcid']]['table']);
$this->display();