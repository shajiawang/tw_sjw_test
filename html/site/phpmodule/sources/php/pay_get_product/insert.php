<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

// Check Variable Start
if (empty($this->io->input["post"]["pgpid"])) {
	$this->jsAlertMsg('结标商品错误!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('收件人姓名错误!!');
}
if (empty($this->io->input["post"]["phone"])) {
	$this->jsAlertMsg('收件人手机错误!!');
}
if (empty($this->io->input["post"]["zip"])) {
	$this->jsAlertMsg('收件人邮编错误!!');
}
if (empty($this->io->input["post"]["address"])) {
	$this->jsAlertMsg('收件人地址错误!!');
}

// Check Variable End


$query ="
SELECT 
p.*, 
unix_timestamp(offtime) as offtime, 
unix_timestamp() as `now`,
pgp.pgpid,
pgp.price 
FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp 
LEFT OUTER JOIN `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product` p ON 
	pgp.prefixid = p.prefixid
	AND pgp.productid = p.productid
	AND p.switch = 'Y'
WHERE 
pgp.`prefixid` = '{$this->config->default_prefix_id}'
AND pgp.pgpid  = '{$this->io->input["post"]["pgpid"]}'
AND pgp.userid = '{$this->io->input['session']['user']['userid']}'
AND pgp.switch = 'Y'
" ;
$table = $this->model->getQueryRecord($query); 

if (empty($table['table']['record'])) {
    $this->jsPrintMsg('商品不存在!', $this->config->default_main);
}

$product = $table['table']['record'][0];
$product['price'] = round($product['price'] * $this->config->sjb_rate, 2);
$product['real_process_fee'] = round($product['retail_price']*$product['process_fee']/100*$this->config->sjb_rate, 2);
$product['total'] = $product['real_process_fee'] + $product['price'];

##############################################################################################################################################
// Insert Start

//訂單確定 Relation order
$query ="
INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}order` 
(
	prefixid,
	userid,
	pgpid,
	type,
	status,
	num,
	point_price,
	process_fee,
	total_fee,
	insertt
)
VALUE 
(
	'{$this->config->default_prefix_id}',
	'{$userid}',
	'{$product['pgpid']}',
	'saja',
	'0',
	'1',
	'{$product['price']}',
	'{$product['real_process_fee']}',
	'{$product['total']}',
	NOW()
)
";
$this->model->query($query);
$orderid = $this->model->_con->insert_id;

//新增訂單收件人 Relation order_consignee
$query ="
INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}order_consignee`
(
	prefixid,
	userid,
	orderid,
	name,
	phone,
	zip,
	address,
	gender,
	insertt
)
VALUE
(
	'{$this->config->default_prefix_id}',
	'{$userid}',
	'{$orderid}',
	'{$this->io->input["post"]["name"]}',
	'{$this->io->input["post"]["phone"]}',
	'{$this->io->input["post"]["zip"]}',
	'{$this->io->input["post"]["address"]}',
	'{$this->io->input["post"]["gender"]}',
	now()
)
";
$this->model->query($query);

// Insert End
##############################################################################################################################################

header("location: ".$this->config->default_main.'/member');