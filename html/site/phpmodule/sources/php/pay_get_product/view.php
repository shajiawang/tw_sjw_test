<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('请先登入会员账号!!', $this->config->default_main."/user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
// $countryid = $this->io->input["get"]["countryid"];
$countryid = 2;


#// Status Start 
$status["status"]["search_path"] = '';
$status["status"]["sort_path"] = '';
$status["status"]["p"] = '';

// Arg Start
$status["status"]["args"] = array();
if(!empty($this->io->input["get"]["channelid"])){
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}else{
	$this->io->input['session']['channelid'] = isset($this->io->input['session']['channelid']) ? $this->io->input['session']['channelid'] : 1;
	$this->io->input["get"]["channelid"] = $this->io->input['session']['channelid'];
	$status["status"]["args"][] = "channelid=".$this->io->input["get"]["channelid"] ;
}
// Arg End

if (empty($this->io->input["get"]["productid"])) {
	$this->jsPrintMsg('商品ID错误!!', $location_url);
}

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/". $this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

$status["status"]["args"] = implode('&', $status["status"]["args"]);

if (!empty($status["status"]["args"])) {
	$status["status"]["path"] .= ("?".$status["status"]["args"]);
}

##############################################################################################################################################

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_shop = $this->config->db[4]['dbname'];

// Table  Start 

// Table Count Start 
// Table Count end 

// Table Record Start
$query ="
SELECT 
p.*, 
unix_timestamp(offtime) as offtime, 
unix_timestamp() as `now`,
pgp.pgpid,
pgp.price 
FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp 
LEFT OUTER JOIN `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product` p ON 
	pgp.prefixid = p.prefixid
	AND pgp.productid = p.productid
	AND p.switch = 'Y'
WHERE 
pgp.`prefixid`    = '{$this->config->default_prefix_id}'
AND pgp.productid = '{$this->io->input["get"]["productid"]}'
AND pgp.userid    = '{$this->io->input['session']['user']['userid']}'
AND pgp.switch    = 'Y'
" ;
$table = $this->model->getQueryRecord($query); 

if (empty($table['table']['record'])) {
    $this->jsPrintMsg('商品不存在!', $this->default_main);
}

//得標價格
$price = (float)$table['table']['record'][0]['price'] * $this->config->sjb_rate;
$table['table']['record'][0]['price'] = round($price, 2);

//中标处理费 = 市價 * 处理费% * 兌換比率
$real_process_fee = (float)$table['table']['record'][0]['retail_price'] * ((float)$table['table']['record'][0]['process_fee']/100) * $this->config->sjb_rate;
$table['table']['record'][0]['real_process_fee'] = round($real_process_fee, 2);

//费用总计
$table['table']['record'][0]['total'] = $table['table']['record'][0]['real_process_fee'] + $table['table']['record'][0]['price'];

// Table Record End 

// Table End 
##############################################################################################################################################

##############################################################################################################################################
// Relation Start 

// Relation product_category
$query ="
SELECT * 
FROM `".$this->config->db[4]["dbname"]."`.`{$this->config->default_prefix}product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'  
AND channelid = '{$this->io->input["get"]["channelid"]}' 
AND switch = 'Y' 
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];


$query ="
SELECT 
userid, SUM(amount) as amount
FROM `".$this->config->db[1]["dbname"]."`.`{$this->config->default_prefix}spoint` 
WHERE 
`prefixid`    = '{$this->config->default_prefix_id}'  
AND `countryid` = '{$countryid}' 
AND userid    = '".$this->io->input['session']['user']['userid']."'
AND `switch`  = 'Y' 
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["spoint"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]); 
//echo '<pre>';print_r($status);die();
$this->display();
