<?PHP
$debug_mod   = "Y";
//$host_name   = "localhost";
$host_name   = "sajadb01";
$protocol    = "https";
// $domain_name = 'www.saja.com.tw';
$domain_name = $_SERVER['SERVER_NAME'];

############################################################################
# defient
############################################################################
$config["project_id"]             = "saja"; 
$config["base_url"]               = "http://".$domain_name; 
$config["default_main"]           = "/site"; 
$config["default_charset"]        = "UTF-8";
$config["sql_charset"]            = "utf8";
$config["default_prefix"]         = "saja_" ;  			// prefix
$config["default_prefix_id"]      = "saja" ;		// prefixid
$config["cookie_path"]            = "/";  
$config["cookie_domain"]          = "";  
$config["tpl_type"]               = "php"; 
$config["module_type"]            = "php";	  		// or xml
$config["fix_time_zone"]          = -8 ;                          // modify time zone 
$config["default_time_zone"]      = 8 ;                           // default time zone 
$config["max_page"]               = 20 ;
$config["max_range"]              = 10 ;
$config["encode_type"]            = "crypt" ; 			// crypt or md5
$config["encode_key"]             = "%^$#@%S_d_+!" ; 			// crypt encode key
$config["session_time"]           = 15	; 			// Session time out
$config["default_lang"]           = "zh_TW"	; 			// en , tc
$config["default_template"]       = "default"	; 		// 
$config["default_topn"]           = 10	; 			// default top n
$config["debug_mod"]              = $debug_mod ; 			// enable all the debug console , N : disable , Y : enable 
$config["max_upload_file_size"]   = 800000000 ; 			//unit is k
$config["expire"]                 = "60" ; 			// memcace expire time . sec
$config["domain_name"]            = $domain_name;
$config["protocol"]               = $protocol;
$config["admin_email"]            = array('admin@shajiawang.com') ; 
$config["currency_email"]         = array('admin@shajiawang.com') ; 
$config["transaction_time_limit"] = 5; //The minimized time between two transaction
############################################################################
# FaceBook
############################################################################
$config['fb']['appid' ]  = "";
$config['fb']['secret']  = "";


############################################################################
# path
############################################################################
$config["path_site"]            = "/var/www/html/site/phpmodule";
$config["path_class"]           = $config["path_site"]."/class" ; 
$config["path_function"]        = $config["path_site"]."/function" ; 
$config["path_include"]         = $config["path_site"]."/include" ; 
$config["path_bin"]             = $config["path_site"]."/bin" ; 
$config["path_data"]            = $config["path_site"]."/data/" ; 
$config["path_cache"]           = $config["path_site"]."/data/cache" ; 
$config["path_sources"]         = $config["path_site"]."/sources/".$config["module_type"] ; 
$config["path_style"]           = $config["path_site"]."/template/".$config["tpl_type"] ; 
$config["path_language"]        = $config["path_site"]."/language" ; 
$config["path_image"]           = $config["default_main"]."/images".$config["default_main"] ; 
$config["path_images"]          = dirname($config["path_site"])."/images".$config["default_main"] ; 
$config["path_products_images"] = dirname(dirname($config["path_site"]))."/images/products" ; 
$config["path_admin"]           = $config["path_site"]."/admin/" ; 
$config["path_javascript"]      = $config["path_site"]."/javascript/" ; 
$config["path_pdf_template"]    = $config["path_class"]."/tcpdf/template/" ; 
$config["path_eamil_api"]       = "/usr/bin/" ;  // email api path 

############################################################################
# sql Shaun 
############################################################################
$config["db"][0]["charset"]  = "utf8" ;
$config["db"][0]["host"]     = $host_name ;
$config["db"][0]["type"]     = "mysql";
$config["db"][0]["port"]     = "3306";
$config["db"][0]["username"] = "saja"; 
$config["db"][0]["password"] = 'saja#333' ;
#$config["db_port"][0]       = "/var/lib/mysql/mysql.sock";

$config["db"][0]["dbname"]   = "saja_user" ;
$config["db"][1]["dbname"]   = "saja_cash_flow" ;
$config["db"][2]["dbname"]   = "saja_channel" ;
$config["db"][3]["dbname"]   = "saja_exchange" ;
$config["db"][4]["dbname"]   = "saja_shop" ;

############################################################################
# memcache 
############################################################################
$config['memcache']['server_list']['session']['ip']   = 'localhost';
$config['memcache']['server_list']['session']['port'] = 11211;
$config['memcache']['MEM_PERSISTENT']                 = true;
$config['memcache']['MEM_TIMEOUT']                    = 1;
$config['memcache']['MEM_RETRY_INTERVAL']             = 1;
$config['memcache']['MEM_STATUS']                     = 1;
$config['memcache']['MEM_WEIGHT']                     = 1000;

############################################################################
# Payment 
############################################################################
$config['alipay']['merchantnumber'] = '456025';
$config['alipay']['code']           = 'abcd1234';
$config['alipay']['paymenttype']    = 'ALIPAY';
$config['alipay']['url_payment']     = 'http://testmaple2.neweb.com.tw/CashSystemFrontEnd/Payment';
$config['alipay']['url_query']       = 'http://testmaple2.neweb.com.tw/CashSystemFrontEnd/Query';

$config['sjb_rate'] = 1;//當地貨幣與殺價幣兌換比率
$config['country'] = 2; //中國

