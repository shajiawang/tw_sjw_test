<?php    
$PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR; //QRCode 產生目錄
$PNG_WEB_DIR = 'temp/'; //QRCode 圖片路徑

include_once "qrlib.php"; //QRCode 模組

if (!file_exists($PNG_TEMP_DIR)) { //判斷資料夾存在於否
	mkdir($PNG_TEMP_DIR);
}

$errorCorrectionLevel = 'M'; //複雜度
$matrixPointSize = 10; //大小

if(!empty($_GET['size'])) {
  $matrixPointSize = $_GET['size'];
}

if ($_REQUEST['data'] != "") { //判斷是否為空直
	$filename = $PNG_TEMP_DIR.md5($_REQUEST['data'].'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png'; //路徑+檔名
	QRcode::png($_REQUEST['data'], $filename, $errorCorrectionLevel, $matrixPointSize, 2); //產生QRCode檔案
	
	Header("Content-type: image/png"); //宣告PNG
	ImagePng(imagecreatefrompng($PNG_WEB_DIR.basename($filename))); //輸出圖檔 
}
?>
<form name="qrcode" action="index.php" method="post" >
輸入網址：<input type="text" name="data" size="128"  value=""/>
<input type="submit" value="確定" />
</form>
