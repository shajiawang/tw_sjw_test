<?php
$app = _v('APP');
?>
<div class="article">
    <div class="saja_lists_box">
        <!-- 下標商品 -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq_1',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">下標商品</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq_1" class="linkBox" style="display: none;">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p class="list-collapse-title">下標費用</p>
                        <p>商品內頁中的<span class="bid-rule d-inlineflex align-items-center"><img src="<?PHP echo APP_DIR; ?>/static/img/rule.png">下標規則</span>會註明該商品的下標時收取的費用為何，且依照不同的商品有不同的下標費用。</p>
                        
                        <p class="list-collapse-title">下標方式</p>
                        <p>分為單次下標、連續下標：</p>
                        <p>1.單次下標：可任選一種數字出價，但出價金額<font color="#E02020">不可高於該商品官方售價。</font></p>
                        <div class="faq-img">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/faq/faq1-1.png" alt="">
                        </div>
                        <p>2.連續下標：可設定下標的金額範圍，每次下標標數不得超過100標且<font color="#E02020">在商品結標前一分鐘禁止連續下標，只能使用單次下標。</font></p>
                        <div class="faq-img">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/faq/faq1-2.png" alt="">
                        </div>
                        <p>下標完成後系統會提示目前商品下標的狀況，讓你可以更準確的下標！</p>
						<div class="faq-img">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/faq/faq1-3.png" alt="">
                        </div>
		
						<p class="list-collapse-title">下標查詢</p>              
                        <p>在下標時，可查詢之前的下標紀錄。</p>
                        <div class="faq-img">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/faq/faq1-4.png" alt="">
                        </div>
					     <p>點選下標記錄查詢頁右上角，可快速選擇下標區間索引</p>              
                                
                        <div class="faq-img">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/faq/faq1-5.png" alt="">
                        </div>               
                    </div>
                </div>
            </div>
        </div>
        
        <!-- 結標規則及得標 -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq_2',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">結標規則及得標</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq_2" class="linkBox" style="display: none;">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p class="list-collapse-title">結標規則</p>
                        <p>在時間截止時，由出價最低且不跟別人重覆者得標。</p>
				          <div class="faq-img">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/faq/faq2-1.png" alt="">
                        </div>
						<p class="list-collapse-title">流標</p>
						<p>所有出價數字皆重覆，沒有最低唯一，或廠商無法提供、斷貨等外在因素無法出貨時，殺價王有權以流標處理，<font color="#E02020">流標後殺價幣100%全部退回。</font><p>

                        <p class="list-collapse-title">得標</p>
                        <p>得標者須在<font color="#E02020">3天內</font>支付得標處理費完成結帳程序，否則視同放棄，若放棄商品下標時<font color="#E02020">全部支出的費用將不會轉贈鯊魚點</font>。</p>
                        <p class="list-collapse-title">結帳流程：</p>
                        <p>【殺友專區】>【殺價紀錄-已得標】點選得標商品右下角<span class="link red">結帳去</span>並購買足夠的殺價幣進行結帳。</p>
						<br>
                        <p>*得標處理費會因為每個商品的不同而有不同的金額，在商品頁的<span class="bid-rule d-inlineflex align-items-center"><img src="<?PHP echo APP_DIR; ?>/static/img/rule.png">下標規則</span>可清楚看到該商品的得標處理費，得標者則可在<span class="link red">結帳去</span>中看到。</p>
                        <p><font color="#E02020">得標處理費可由已下標手續費100%全額折抵。</font><p>
                        <div class="faq-img">
                            <img src="<?PHP echo APP_DIR; ?>/static/img/faq/faq2-2.png" alt="">
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <!-- 殺價幣 -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq_3',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">殺價幣</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq_3" class="linkBox" style="display: none;">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p class="list-collapse-title">什麼是殺價幣：</p>
                        <p>殺價幣為殺價王下標的代幣，須先購買殺價幣才能進行下標及結帳。</p>

                        <p class="list-collapse-title">如何購買殺價幣：</p>
                        <p>請登入殺價王後，進入【殺友專區】點選【購買殺價幣】，選擇欲購買的項目即可進行購買。殺價幣購買完成後可在【殺友專區】>【殺價幣-獲得】裡看到購買成功的記錄及發票。</p>
                        <br>
                        <p><strong>*購買殺價幣時常會搭配不同的活動送大檔商品下標券，送券資訊以當日購買殺價幣頁面公告為準。</strong></p>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- 鯊魚點 -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq_4',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">鯊魚點</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq_4" class="linkBox" style="display: none;">
                <div class="list-collapse">
                    <div id="onlyContent">
<!--                        <p class="list-collapse-title">什麼是鯊魚點？</p>-->
                        <p>使用殺價幣下標商品，如未得標，我們將贈送你與下標費用相同金額的鯊魚點，讓您不會有損失，您可將得到的鯊魚點至鯊魚商城和所有相關合作廠商使用。</p>
                        <br>
                        <p><strong>*鯊魚點不可兌現、返還成殺價幣或轉讓，鯊魚點不足時，亦無法以殺價幣補足剩餘部分，所有鯊魚點只能由贈送取得。</strong></p>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- 殺價券 -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq_5',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">殺價券</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq_5" class="linkBox" style="display: none;">
                <div class="list-collapse">
                    <div id="onlyContent">
<!--                        <p class="list-collapse-title">什麼是殺價券？</p>-->
                        <p>得到商品殺價劵等於拿到免費的下標次數，一張殺價券可以下標該商品一次，優先折抵下標費用金額較高的部分，一樣的商品可以同時使用多張殺價劵，。</p>
                        <br>
                        <p><strong>*殺價券請您務必在該商品結標前使用，逾期則失效不予補回，若下標商品流標，殺價券不做退還，若未得標，殺價券下標部分也不再贈送鯊魚點，亦不得換成殺價幣。</strong></p>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- 分享商品拿殺價劵 -->
		<!--
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq_6',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">分享商品拿殺價劵</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq_6" class="linkBox" style="display: none;">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>成為殺價王會員後，分享自己的專屬鏈結給朋友，邀請朋友加入殺價王並領取殺價券或購買殺價幣，就可以獲得免費的殺價券！</p>
                        <p><strong>*禁止通過作弊手段進行虛假邀請註冊騙取獎勵，一旦被監測到，將封鎖您的帳號！</strong></p>
                    </div>
                </div>
            </div>
        </div>
		-->
		
        <!-- 最新得標 -->
        <div class="list-group">
            <div class="list-title-group">
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onClick="ReverseDisplay('faq_7',this,'blod','scroll');">
                    <div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">最新得標</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
            <div id="faq_7" class="linkBox" style="display: none;">
                <div class="list-collapse">
                    <div id="onlyContent">
                        <p>商品結標後您可以至【最新得標】查看該商品的下標人數及出價金額狀況，可得知過去每檔商品最後得標金額、參與人次以及得標者。</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="saja_lists_box">
        <!-- 幫助中心(換頁) -->
        <div class="list-group">
            <div class="list-title-group">
				<?php if($app=='Y') { ?>
                <a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/help/?APP=Y'">
                <?php }else{ ?>
				<a class="list-title-link d-flex align-items-center" href="javascript:void(0);" onclick="javascript:location.href='<?php echo BASE_URL.APP_DIR; ?>/help/?<?php echo $cdnTime; ?>'">
                <?php } ?>
					<div class="list-titlebox d-inlineflex align-items-center mr-auto">
                        <div class="list-title">幫助中心</div>
                    </div>
                    <div class="list-rtxtbox d-inlineflex align-items-center">
                        <div class="r-arrow"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
</div><!-- /article -->

<!--  廣告版位  -->
<div class="lModalBox"></div>