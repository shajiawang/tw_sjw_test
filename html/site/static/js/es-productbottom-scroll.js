//Object
    /*
    
        esOptions.menudom.parent : tab的父層
        esOptions.menudom.wrapper : tab包覆層
        esOptions.menudom.menuItem : tab按鈕元件
        esOptions.menudom.line : tab底線
        
        esOptions.contentdom.showBox : 內容的父層
        esOptions.contentdom.boxScroll : 捲動框
        esOptions.contentdom.showItem : 各tab物件
        esOptions.contentdom.itemClass : tab物件Class
        esOptions.contentdom.boxWrapper : 各tab包覆層

        esOptions.jsonInfo.type
        esOptions.jsonInfo.dataType
        esOptions.jsonInfo.contentType
        esOptions.jsonInfo.async : json的非同步
    */

var esOptions = {
    tabArr: [],                 //儲存由php生成的按鈕陣列
    tabItem: [],                //tab各頁id以及對應的代入ajax的參數
    actItemId: '',                //作用中的頁面ID
    actItemIndex: '',             //作用中的頁面索引
    class: {
        active: 'on-active'
    },
    menudom: {
        parent: '.horscroll',
        wrapper: '.menu-wrapper',
        menuItem: 'a.menu-a',
        line: '.sideline'
    },
    contentdom:{
        showBox: '.sajaWrapper-show-box',
        boxScroll: '.sajaWrapper-boxscroll',
        showItem: '.show-item',
        boxWrapper: '.sajalist-wrapper',
        itemClass: 'sajalist-wrapper d-inlineflex flex-wrap',
        itemAll: ''
    },
    difference: '0',              //觸底誤差值
    jsonInfo: {
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        async: 'false',
        cache: 'false',
        timeout: '4000',
        processData: false,
        contentType: false
    }
}

var $tabgroup;                                           //建立tabgroup
//各id建立物件 tabItem
function createDom() {
    //生成按鈕
    $.each($tabArr,function(i,item){
        //產生按鈕
        $(esOptions.menudom.parent)
        .find(esOptions.menudom.line)
        .before(
            $('<a/>')
            .attr('id',item.pcid)
            .addClass('menu-a')
            .attr('href','#')
            .text(item.name)
        );
    })
    //改變按鈕寬度
    changBtnW();
    
    //整理各頁籤資料
    $tabgroup = $(esOptions.menudom.parent).find(esOptions.menudom.menuItem);
    //取得要設定active的頁面
    if(esOptions.actItemId == ''){            //未設定active頁id，則預設第一個
        esOptions.actItemIndex = '1';
    }else{
        $.map($tabArr,function(item, index){
            if(item['pcid'] == esOptions.actItemId){
                esOptions.actItemIndex = index;
            }
        })
    }
    //儲存各頁籤資料
    $tabgroup.each(function(){
        var $that = $(this);
        var $thatId = $that.attr('id');
        var $name = $that.text();
        
        if($thatId == esOptions.actItemId){
            $that.addClass(esOptions.class.active);
        }
        esOptions.tabItem.push({pcid:$thatId,name:$name,page:0,total:1,endpage:1});
    });
    //生成容器(翻頁區)
    $.each(esOptions.tabItem,function(i,item){
        $(esOptions.contentdom.boxScroll)
            .append(
                $('<div/>')
                .attr('id','show-'+item.pcid)
                .addClass('show-item')
                .append(
                    $('<div/>')
                    .addClass(esOptions.contentdom.itemClass)
                )
                .append(
                    $('<div/>')
                    .addClass('esloading')
                    .append(
                        $('<div/>')
                        .addClass('d-inlineflex align-items-center')
                        .append(
                            $('<img/>')
                            .addClass('loading')
                            .attr('src',esOptions.loadingUrl)
                        )
                        .append(
                            $('<span/>')
                            .text('加載中')
                        )
                    )
                )
            );
        $('#show-'+esOptions.actItemId).addClass(esOptions.class.active);
    });
    
    //設定翻頁區寬度
    changeW();
};

//設定按鈕寬度
function changBtnW() {
//        var $height = $(options.dom.menuItem).outerHeight(true);
//        $(options.dom.parent).css('height',$height);

    //設定各按鈕間距，要判斷前後按鈕字數
    //body 預設為12px, 以rem計算, ex: 24px = 2rem;
    var $items = $(esOptions.menudom.menuItem);
    $items.each(function(){
        var $that = $(this);
        var $itemW = $that.text().length,
            $itemPrev = $that.prev().text().length,
            $itemNext = $that.next().text().length;
        var $paddRight = '',
            $paddLeft = '';
        
        //利用字數判斷間距
        switch(true) {
            case $itemW == 2 || $itemW == 3 :
                $paddLeft = 2.292;
                $paddRight = 2.292;
                if($that.index() == 0){                         //左邊沒按鈕，表示該按鈕在最左邊
                    $paddLeft = 2.667;
                }
                break;

            case $itemW > 4 || $itemW == 4 :
                //右間距
                switch(true) {
                    case $itemPrev == 2 || $itemPrev == 3 :
                        $paddLeft = 2.292;
                        break;
                    case $itemPrev > 4 || $itemPrev == 4 :
                        $paddLeft = 1.834;
                        break;
                    default :                                   //左邊沒按鈕，表示該按鈕在最左邊
                        $paddLeft = 2.667;
                }
                //左間距
                switch(true) {
                    case $itemNext == 2 || $itemNext == 3 :
                        $paddRight = 2.292;
                        break;
                    case $itemNext > 4 || $itemNext == 4 :
                        $paddRight = 1.834;
                        break;
                    default :                                   //右邊沒按鈕，表示該按鈕在最右邊
                        $paddRight = 2.667;
                }
                break;
        }

        $that.css({'padding-left':$paddLeft+'rem','padding-right':$paddRight+'rem'});
    });
    
    //當menu 數量小於4時，寬度平均分配
    if($items.length < 4){
        $items.each(function(){
            var $that = $(this);
            $that.css('width','calc(100vw / '+$items.length+' )');
        })
    };

    //menu底線預設
    var $itemW = $items.first().innerWidth();
    $(esOptions.menudom.line).width($itemW);
};

//顯示區寬度
function changeW() {
    var $wrapper = $(esOptions.contentdom.boxScroll),
        $item = $(esOptions.contentdom.showItem),
        $itemNum = $item.length,
        $winW = $(window).outerWidth();
    $item.width($winW);
    $wrapper.width($itemNum * $winW);
}

var iTop = 0;
var iHeight = 0;
var clientHeight = 0;  
var iIntervalId = null; 
  
getPageHeight();

// 添加定時檢測事件，每1.5秒檢測一次  
iIntervalId = setInterval("_onScroll();", 600);

//取得計算距離的各種參數 clientHeight (螢幕高度) iHeight (內容總高度)
function getPageHeight() {
    var $showID = $(esOptions.contentdom.boxScroll).find('.'+esOptions.class.active);
    clientHeight = $showID.height();   //517
    iHeight = $showID.find(esOptions.contentdom.boxWrapper).height();   //6553
}

// 判斷滾動條是否到達底部 
function reachBottom($id) {  
    var scrollTop = 0;
    var $showID = $(esOptions.contentdom.boxScroll).find('.'+esOptions.class.active).attr('id');
    scrollTop = $('#'+$showID).scrollTop();
    //console.log(scrollTop + clientHeight);  //6107
    //alert(scrollTop);  2351
    if((scrollTop > 0) && (scrollTop + clientHeight == iHeight)) {  
        return true;    
    } else {    
        return false;
    }
}
reachBottom();

// 檢測事件，檢測滾動條是否接近或到達頁面的底部區域，0.99是為了更接近底部時
function _onScroll() {
    //目前作用中的頁面
    var $showID = $(esOptions.contentdom.boxScroll).find('.'+esOptions.class.active).attr('id');
    if($showID){
        var $actTabID = $showID.replace('show-','');
    }
    var $nowPage,$totalPage;
    $.map(esOptions.tabItem,function(item, index){
        if(item['pcid'] === $actTabID){
            $nowPage = item['page'];                       //目前頁數
            $totalPage = item['total'];                     //找到對應ID的總頁數
        }
    });
    iTop = $('#'+$showID).scrollTop();
    getPageHeight();
    if(((iTop+clientHeight)>parseInt(iHeight*0.9))||reachBottom($actTabID)) {
        //console.log($nowPage+'_'+$totalPage);
        if($nowPage < $totalPage) {
            esOptions.show($actTabID);
        }else{
            
        }
    }  
};  