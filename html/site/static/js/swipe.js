//切換頁簽時回頂
function goTop() {
    $('body,html').stop().animate({scrollTop:0},500);
}
$(function(){
    if($(".swipe-navbar").length>0){
        //取得標籤群的父層
        var Tab = document.querySelector(".swipe-navbar");
        //取得內容群的父層
        var Content = document.querySelector(".swipe-navbar-content");
        //active狀態
        var Active = "on-active";

        //取得標籤群數量
        var Page = Tab.querySelectorAll(".swipe-navbar-item");
        var PageLenght = Page.length;       

        //初始化active
        function init(ActName) {
            var TabActDiv = Tab.querySelector('.'+Active);
            var ConActDiv = Content.querySelector('.'+Active);
            TabActDiv.classList.remove(ActName);
            ConActDiv.classList.remove(ActName);
        }
        //增加active的標籤位置
        function ActChang(A){
            var TabChang = Tab.querySelector("[href='"+A+"']");
            var ConChang = Content.querySelector(A);
            TabChang.classList.add(Active);
            ConChang.classList.add(Active);
            // if (PageNow == "1"){
                // $('#type').val('lazy');
                // $('#lazy_count').click();
            // } else if (PageNow == "2"){
                // $('#type').val('range');
                // $('#price_start').click();
            // } else if (PageNow == "3"){
                // $('#type').val('single');
                // $('#price').click();
            // }
			if (PageNow == "1"){
                $('#type').val('range');
                $('#price_start').click();
            } else if (PageNow == "2"){
                $('#type').val('single');
                $('#price').click();
            }
            return;
        }


        //取得原始作用中頁面編號
        for(var i=0; i<Page.length; i++){
            if($(Page[i]).hasClass(Active)){
                var nowActive = i+1;
            }
        }
        var PageNow = nowActive;

        function PageToId(num){
            return '#tab' + num;
        }

        function showTab(a){
            var $a = $(a);
            if($a.hasClass(Active)) return;

            href = $a.attr("href");
            if(!/^#/.test(href)) return ;

            $a.parent().find("."+Active).removeClass(Active);
            $a.addClass(Active);

            var bd = $a.parents(".swipe-tab").find(".swipe-navbar-content");
            bd.find("."+Active).removeClass(Active);

            $(href).addClass(Active);
            // if (href == "#tab1"){
                // $('#type').val('lazy');
                // $('#lazy_count').click();
            // } else if (href == "#tab2"){
                // $('#type').val('range');
                // $('#price_start').click();
            // } else if (href == "#tab3"){
                // $('#type').val('single');
                // $('#price').click();
            // }
			if (href == "#tab1"){
                $('#type').val('range');
                $('#price_start').click();
            } else if (href == "#tab2"){
                $('#type').val('single');
                $('#price').click();
            }
        }
        $.showTab = showTab;

        //向左滑
        $(document).bind("swipeleft",function(e){
            var $that = $(this);
            if($that.find(".modal").length > 0){
                return false;                                   //開啟小鍵盤時，關閉滑動
            }else{
                if (PageNow < PageLenght){
                    PageNow +=1;
                    init(Active);
                    ActChang(PageToId(PageNow));
                }
                e.stopPropagation;
            }
            goTop();
        })
        //向右滑
        $(document).bind("swiperight",function(e){
            var $that = $(this);
            if($that.find(".modal").length > 0){
                return false;                                   //開啟小鍵盤時，關閉滑動
            }else{
                if (PageNow > 1){
                    PageNow -=1;
                    init(Active);
                    ActChang(PageToId(PageNow));
                }
                e.stopPropagation;
            }
            goTop();
        })

        $(document).on("click", ".swipe-navbar-item", function(e) {
            var $a = $(e.currentTarget);
            var href = $a.attr("href");
            if($a.hasClass(Active)) return;
            if(!/^#/.test(href)) return;
            var str = href;
            str = str.replace("#tab", "");
            PageNow = parseInt(str);
            e.preventDefault();
            showTab($a);
            goTop();
          });
    
    }
})