var $locationUrl = location.protocol+'//'+location.host+'/site';

function esloading($boolean){
    var esloadOptions = {
        class: {
            box: 'esloadingBox',
            imgbox: 'esloadingImgBox',
            img: 'esloadingImg',
            bg: 'esloadingBg'
        }
    }
    var $loadingImgUrl = $locationUrl+'/static/img/esloading.svg';
    if($boolean){
        esloading(0);
        $('body').append(
            $('<div/>')
            .addClass(esloadOptions.class.box)
            .append(
                $('<div/>')
                .addClass(esloadOptions.class.imgbox)
                .append(
                    $('<img/>')
                    .addClass(esloadOptions.class.img)
                    .attr('src',$loadingImgUrl)
                )
            )
            .append(
                $('<div/>')
                .addClass(esloadOptions.class.bg)
            )
        )
        $('body').addClass('modal-open');
    }else{
        $('body').find('.'+esloadOptions.class.box).remove();
        $('body').removeClass('modal-open');
    }
}