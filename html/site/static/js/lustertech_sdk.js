// sdk
// 判斷APP內外
function is_kingkr_obj() {
    if('undefined'==(typeof app)) {
        return false;
    }
    return  app.is_kingkr_obj();
}

//複製文字
function copyText(str) {
    if('undefined'==(typeof app)) {
       alert("複製失敗 !!");
       return false;
    }
    let ret=app.copyText(str);
    if(ret==1) {
       alert("複製完成 !!");
       return true;
    } else {
        alert("複製失敗 !!");
        return false;
    }
}

// 複製當前網址
function copyUrlToClipboard() {
    if('undefined'==(typeof app)) {
        return false;
    }
    return app.copyUrlToClipboard();
}

// 第三方登入
function login(platform, forwardurl, callbackMethod) {
    if('undefined'==(typeof app)) {
        return false;
    }
    return app.login(platform,forwardurl, callbackMethod);
}

// 调起支付
function payType(data,type,callbackMethod) {
    if('undefined'==(typeof app)) {
        return false;
    }
    // alert(type+":"+data);
    return app.payType(data,type,callbackMethod);
}

 // qrcode('qrcodeResult');
function qrcode(type) {
    if('undefined'==(typeof app)) {
        return false;
    }
    app.qrcode(type);
    app.getQrCodeResult();
}

function awakeOtherBrowser(url) {
    if('undefined'==(typeof app)) {
        return false;
    }
    return app.awakeOtherBrowser(url);
}


function share() {
  if('undefined'==(typeof app)) {
      return false;
  }
  if(arguments.length==1){
      app.share(arguments[0]);
  }else if(arguments.length==2){
      app.share(arguments[0],arguments[1]);
  }else if(arguments.length==4){
      app.share('share',arguments[0],arguments[1],arguments[2],arguments[3]);
  }else if(arguments.length==5){
      app.share(arguments[0],arguments[1],arguments[2],arguments[3],arguments[4]);
  }
}
