var $counter_object = {
    getType: 'ajax',                    //取得資料方式    ajax || rand    (ajax或隨機)
    updateTime: 6,                      //更新頻率 (秒)
    data: {
        $defaultPeoNum: 16528,          //預設人數
        $defaultmoneyNum: 59584168,     //預設成功節省
    }
}
var $counter_number;                    //節省金暫存
var $number = {};


$(function () {
    if($counter_object.getType == 'ajax'){
        getNumData();                                                                                //Ajax初始 取得資料
        setInterval(getNumData, $counter_object.updateTime * 1000);	                     //Ajax更新頻率 (毫秒)
    }else{
        $('.success-title .num').text($counter_object.data.$defaultPeoNum);                          //將預設人數帶入頁面dom
        $('.success-money .jackpot').attr('data-money', $counter_object.data.$defaultmoneyNum);      //將預設節省金帶入頁面dom
        
        getRandNum();                                                                                //rand初始 取得資料
        var int = setInterval(getRandNum, $counter_object.updateTime * 1000);	                     //rand更新頻率 (毫秒)
    } 
});



//取得Ajax資料 (2選1)
function getNumData() {
    var $locationUrl = location.protocol+'//'+location.host+'/site';
    
    //組合網址
    var $ajaxUrl = $locationUrl+'/bid/getBidAllTotalCount/';

    $.ajax({  
        url: $ajaxUrl,
        contentType: 'application/json; charset=utf-8',
        type: 'POST',
        dataType: 'json',
        async: false,
        cache: false,
        timeout: 4000,
        processData: false,
        contentType: false,
        complete: endLoading,
        error: showFailure,  
        success: showResponse
    });
    function showResponse(data) {
        if(data['retCode'] == '1') {
            var $arr = data['retObj'];
            //console.log($arr['bid_money_count']);                                         //節省金額
            //console.log($arr['bid_user_count']);                                          //成功人數
            
        //人數
            $('.success-title .num').text($arr['bid_user_count']);                          //更新成功人數Dom
            console.log($arr);
        //成功省錢
            $counter_number = $('.success-money .jackpot').attr('data-money');              //取得Dom上的舊金額
            
            //首次沒有值，先取值，並創建li物件
            if($counter_number == undefined ){
                $counter_number = $arr['bid_money_count'];
                createLi($counter_number);                                                  //建立節省金物件 (首次 一次性)
            }
            $number.old = String($counter_number).length;                                   //儲存舊金額 長度 (增加千分位用)
            
            $counter_number = $arr['bid_money_count'];
            $('.success-money .jackpot').attr('data-money', $counter_number);               //更新Dom (節省金)
            $number.new = String($counter_number).length;                                   //儲存新金額 長度 (增加千分位用)
            setCounter($counter_number);                                                    //變更畫面
        }
    }
    //加載動作完成 (不論結果success或error)
    function endLoading() {
    }
    
    //加載錯誤訊息
    function showFailure() {
    }
}

//取得隨機資料 (2選1)
function getRandNum() {
    //人數
        var $people = $('.success-title .num').text();                              //取得 html 預存 (人數)
        var np = randNumberPeo();                                                   //取得隨機亂數 (人數)
        $people = parseInt($people) + parseInt(np);                                 //加上隨機數 (人數)
        $('.success-title .num').text($people);                                     //更新Dom (人數)
    //成功省錢
        $counter_number = $('.success-money .jackpot').attr('data-money');          //抓取節省金 (節省金)
        //首次創建li物件
        if($counter_number == $counter_object.data.$defaultmoneyNum){
            $counter_number = $counter_object.data.$defaultmoneyNum;
            createLi($counter_number);                                              //建立節省金物件 (一次性)
        }
        $number.old = String($counter_number).length;                               //儲存舊金額 長度 (增加千分位用)
        
	    var n = randNumber();                                                       //取得隨機亂數 (節省金)
        $counter_number = parseInt($counter_number) + parseInt(n);                  //加上隨機數 (節省金)

        $('.success-money .jackpot').attr('data-money', $counter_number);           //更新Dom (節省金)
        $number.new = String($counter_number).length;                               //儲存新金額 長度 (增加千分位用)
        setCounter($counter_number);                                                //變更畫面
}



//亂數增加殺價成功人數
function randNumberPeo(){
	var maxNum = 3;  
	var minNum = 1;  
	var n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;  
	return n;	
}

//亂數增加節省金額
function randNumber(){
	var maxNum = 999;  
	var minNum = 111;  
	var n = Math.floor(Math.random() * (maxNum - minNum + 1)) + minNum;  
	return n;	
}



//原始創建擺放li數目  (初始載入用)
function createLi($counter_number) {
    var $liLength = String($counter_number).length;
    for (var i = 0;i < $liLength; i++){
        $('.success-money .jackpot').append('<li><span><i>0</i><i>1</i><i>2</i><i>3</i><i>4</i><i>5</i><i>6</i><i>7</i><i>8</i><i>9</i></span></li>');
    }
    var $dc = $('.counter span');
    whileLoop($liLength,$dc);
}

//變更顯示的數字
function setCounter($counter_number) {
    var number = $counter_number;       //取每個位置的數字
    //數字數目變多時補li
    if($number.old < $number.new){
        $('.success-money .jackpot').append('<li><span><i>0</i><i>1</i><i>2</i><i>3</i><i>4</i><i>5</i><i>6</i><i>7</i><i>8</i><i>9</i></span></li>');
        createComma();
    }
	var $digital = $('.counter span');
    //更新數字
    for (var i = $digital.length - 1; i >= 0; i--) {
    	var val = parseInt (number / Math.pow (10, i), 10);
    	number = number % Math.pow (10, i);
    	$digital.eq ($digital.length - 1 - i).attr ('class', 'n' + (val % 10));
    }
}

//變動數字時，移除千分位，重新加入千分位
function createComma() {
    //判斷千分位
    var $long = $number.new;
    var $dc = $('.counter span');
    
    //將原來的千分位刪除，重新加
    $(".comma").remove();
    whileLoop($long,$dc);    
}
//迴圈 判斷千分位
function whileLoop($liLength,dc){
    var comma = $liLength;
    while(comma > 3){
        comma-=3;
        var $parent = $(dc[comma]).parent();
        $('<li class="comma">,</li>').insertBefore($parent);
    }
}
