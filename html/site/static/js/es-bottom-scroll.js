//Object
    /*
    
        esOptions.menudom.parent : tab的父層
        esOptions.menudom.menuItem : tab按鈕元件
        
        esOptions.contentdom.parent : 內容的父層
        esOptions.contentdom.menuItem : 內容元件
        
        esOptions.jsonInfo.type
        esOptions.jsonInfo.dataType
        esOptions.jsonInfo.contentType
        esOptions.jsonInfo.async : json的非同步
    */

var esOptions = {
    isTab: 'false',             //是否為多頁tab
    tabItem: [],                //tab各頁id以及對應的代入ajax的參數
    actItem: '',                //作用中的頁面
    class: {
        active: 'on-active'
    },
    menudom: {
        parent: '.horscroll',
        wrapper: '.menu-wrapper',
        menuItem: 'a.menu-a',
        line: '.sideline'
    },
    contentdom:{
        parent: '.swipe-navbar-content',
        showItem: '.content-item',
        ulClass: 'saja_listsimg_box'
    },
    difference: '0',              //觸底誤差值
    jsonInfo: {
        type: 'POST',
        dataType: 'json',
        contentType: 'application/json; charset=utf-8',
        async: false,
        cache: false,
        timeout: '4000',
        processData: false,
        contentType: false
    }
}

var $tabgroup;

var $nowAct = '';                                           //儲存active位置用
//各id建立物件 tabItem
function createDom() {
    //改變按鈕寬度
    changBtnW();
    
    //整理各頁籤資料
    $tabgroup = $(esOptions.menudom.parent).find(esOptions.menudom.menuItem);
    
    if($tabgroup.length>1){
        $tabgroup.each(function(){
            var $that = $(this);
            var $thatId = $that.attr('href').replace('#','');
            if($that.hasClass(esOptions.class.active)){                             //儲存作用中ID
                $nowAct = $thatId;
            }
            esOptions.tabItem.push({id:$thatId,page:0,total:1,endpage:1});          //塞入預設值
        });
        $.each(esOptions.tabItem,function(i,item){
            //生成容器
            $(esOptions.contentdom.parent)
                .append(
                    $('<div/>')
                    .attr('id',item['id'])
                    .addClass('content-item')
                    .append(
                        $('<ul/>')
                        .addClass(esOptions.contentdom.ulClass)
                    )
                    .append(
                        $('<div/>')
                        .addClass('esloading')
                        .append(
                            $('<div/>')
                            .addClass('d-inlineflex align-items-center')
                            .append(
                                $('<img/>')
                                .addClass('loading')
                                .attr('src',esOptions.loadingUrl)
                            )
                            .append(
                                $('<span/>')
                                .text('加載中')
                            )
                        )
                    )
                );
                $('#'+$nowAct).addClass(esOptions.class.active);
        });
    }else{
        esOptions.tabItem.push({page:0,total:1,endpage:1});
        $.each(esOptions.tabItem,function(i,item){
            //生成容器
            $(esOptions.contentdom.parent)
                .append(
                    $('<ul/>')
                    .addClass(esOptions.contentdom.ulClass)
                )
                .append(
                    $('<div/>')
                    .addClass('esloading')
                    .append(
                        $('<div/>')
                        .addClass('d-inlineflex align-items-center')
                        .append(
                            $('<img/>')
                            .addClass('loading')
                            .attr('src',esOptions.loadingUrl)
                        )
                        .append(
                            $('<span/>')
                            .text('加載中')
                        )
                    )
                )
        });
    }

};

//設定按鈕寬度
function changBtnW() {
//        var $height = $(options.dom.menuItem).outerHeight(true);
//        $(options.dom.parent).css('height',$height);

    //設定各按鈕間距，要判斷前後按鈕字數
    //body 預設為12px, 以rem計算, ex: 24px = 2rem;
    var $items = $(esOptions.menudom.menuItem);
    $items.each(function(){
        var $that = $(this);
        var $itemW = $that.text().length,
            $itemPrev = $that.prev().text().length,
            $itemNext = $that.next().text().length;
        var $paddRight = '',
            $paddLeft = '';
        
        //利用字數判斷間距
        switch(true) {
            case $itemW == 2 || $itemW == 3 :
                $paddLeft = 2.292;
                $paddRight = 2.292;
                if($that.index() == 0){                         //左邊沒按鈕，表示該按鈕在最左邊
                    $paddLeft = 2.667;
                }
                break;

            case $itemW > 4 || $itemW == 4 :
                //右間距
                switch(true) {
                    case $itemPrev == 2 || $itemPrev == 3 :
                        $paddLeft = 2.292;
                        break;
                    case $itemPrev > 4 || $itemPrev == 4 :
                        $paddLeft = 1.834;
                        break;
                    default :                                   //左邊沒按鈕，表示該按鈕在最左邊
                        $paddLeft = 2.667;
                }
                //左間距
                switch(true) {
                    case $itemNext == 2 || $itemNext == 3 :
                        $paddRight = 2.292;
                        break;
                    case $itemNext > 4 || $itemNext == 4 :
                        $paddRight = 1.834;
                        break;
                    default :                                   //右邊沒按鈕，表示該按鈕在最右邊
                        $paddRight = 2.667;
                }
                break;
        }

        $that.css({'padding-left':$paddLeft+'rem','padding-right':$paddRight+'rem'});
    });
    
    //當menu 數量小於4時，寬度平均分配
    if($items.length < 4){
        $items.each(function(){
            var $that = $(this);
            $that.css('width','calc(100vw / '+$items.length+' )');
        })
    };

    //menu底線預設
    var $itemW = $items.first().innerWidth();
    $(esOptions.menudom.line).width($itemW);
};
  
var iTop = 0;
var iHeight = 0;
var clientHeight = 0;  
var iIntervalId = null;   
  
getPageHeight(); 

// 添加定時檢測事件，每1.5秒檢測一次  
iIntervalId = setInterval("_onScroll();", 600);

//取得計算距離的各種參數 clientHeight (螢幕高度) iHeight (內容總高度)
function getPageHeight() {
    if(document.body.clientHeight && document.documentElement.clientHeight) {    
        clientHeight = (document.body.clientHeight < document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;            
    } else {    
        clientHeight = (document.body.clientHeight > document.documentElement.clientHeight) ? document.body.clientHeight : document.documentElement.clientHeight;        
    }
    iHeight = Math.max(document.body.scrollHeight,document.documentElement.scrollHeight);
}

// 判斷滾動條是否到達底部 
function reachBottom() {  
    var scrollTop = 0;  
    if(document.documentElement && document.documentElement.scrollTop) {    
        scrollTop = document.documentElement.scrollTop;
    } else if (document.body) {    
        scrollTop = document.body.scrollTop;
    }
    //alert(scrollTop);  2351
    if((scrollTop > 0) && (scrollTop + clientHeight == iHeight)) {  
        return true;    
    } else {    
        return false;   
    }  
    
}
reachBottom();

// 檢測事件，檢測滾動條是否接近或到達頁面的底部區域，0.99是為了更接近底部時
function _onScroll() {
    //目前作用中的頁面
    var $actTabID = $(esOptions.contentdom.parent).find('.'+esOptions.class.active).attr('id');
    var $nowPage,$totalPage;
    $.map(esOptions.tabItem,function(item, index){
        if(item['id'] === $actTabID){
            $nowPage = item['page'];                       //目前頁數
            $totalPage = item['total'];                     //找到對應ID的總頁數
        }
    });
    iTop = document.documentElement.scrollTop + document.body.scrollTop;
    getPageHeight();
    if(((iTop+clientHeight)>parseInt(iHeight*0.9))||reachBottom()) {
        if($nowPage < $totalPage) {
            esOptions.show($actTabID);
        }else{
            
        }
    }  
};  