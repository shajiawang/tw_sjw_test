var $locationUrl = location.protocol+'//'+location.host+'/site';

// --------------------------- 共用function ---------------------------//
    var esAOptions = {
        vClass: {                               //影音廣告
            vModal: {
                show: false,
                flexABtn: 'flexA-btn',
                AmodalV: 'Amodal-v',
                AmodalRWDV: 'Wrapper-v',
                AmodalBgV: 'AmodalBg-v',
                ischecktime: 0 
            },
            reModal: {
                id: 'receiveModal',
                mheader: 'rmodal-header',
                mbody: 'rmodal-body',
                mimg: 'rmodal-img',
                mtextBox: 'rmodal-text-box',
                mtextName: 'rmodal-name',                   //殺價券名稱放置位置
                mtextNum: 'rmodal-num',                   //殺價券數量
                mbtn: 'rmodal-btn',
                mbg: 'rmodal-bg',
                ticketid: '7976',
                ticketName: '',
                ticketNum: ''
            }
        },
        imgClass: {
            cModal: {
                show: false,
                full: false,
                pclass: '.cModalBox',
                modalid: 'cModal',
                zone: '8628',
                modalW: 320,
                modalH: 480
            },
            sModal: {
                show: false,
                full: false,
                pclass: '.sModalBox',
                modalid: 'sModal',
                zone: '8634',
                modalW: 320,
                modalH: 50
            },
            mModal: {
                show: false,
                full: false,
                pclass: '.mModalBox',
                modalid: 'mModal',
                zone: '8632',
                modalW: 320,
                modalH: 100
            },
            lModal: {
                show: false,
                full: false,
                pclass: '.lModalBox',
                modalid: 'lModal',
                zone: '8633',
                modalW: 300,
                modalH: 250
            }
        },
        userid: '',
        played: false
    }
    //判斷使用者裝置
    function whDevice() {
        //判斷使用者裝置是否支援觸控功能，判斷是否為行動版 (有觸控功能的筆電也判斷為行動版)
        function isMobile() {
            try{ document.createEvent("TouchEvent"); return true; }
            catch(e){ return false;}
        }
        
        //判斷使用者裝置為Android或iOS
        function isDevice() {
            var u = navigator.userAgent;
            var isAndroid = u.indexOf('Android') > -1 || u.indexOf('Adr') > -1; //android終端
            var isiOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/); //ios終端
            if(isAndroid){
                return 'Android';
            }else if(isiOS){
                return 'iOS';
            }
        }
        
        if(isMobile()){
            var $mobile = isDevice();
            switch($mobile){
                case 'Android':
                    //console.log('行動版Android');
                    break;
                case 'iOS':
                    //console.log('行動版ios');
                    break;
            }
        }else{
            //console.log('電腦版');
        }
    }

    //判斷廣告阻擋
    function iscfAdBlock(callback) {
        if ($('#myTestAd').length > 0){
            callback(false);
        }else{
            callback(true);
        }
    }

// --------------------------- 彈窗 影音廣告 ---------------------------//
    $(window).on('load', function(){
        $.each(esAOptions.vClass.vModal,function(i,item){
            //console.log(i+'__'+item)
            if(i == 'show' & item == true){
                if(esAOptions.userid !== '' && esAOptions.userid !== undefined){
                    checkticket(function(bool){
                        if(bool == true){
                            //建立浮動廣告Btn與視窗
                            createAmodalV();
                            
                            //計算window與廣告的尺寸與顯示位置
                            calculatedSizeV();
                            
                            //A彈窗開啟事件
                            $('.'+esAOptions.vClass.vModal.flexABtn).on('click',function(){
                                //偵測廣告阻擋
                                iscfAdBlock(function(bool){
                                    if(bool == false){
                                        whDevice();                     //判斷使用者裝置
                                        AmodalOpenV();                  //開啟影音廣告視窗
                                    }else{
                                        alert('偵測到廣告阻擋，請排除廣告阻擋後，再試一次');
                                    }
                                })
                            })

                            //A彈窗關閉事件
                            $('.'+esAOptions.vClass.vModal.AmodalBgV).on('click',function(){
                                //console.log('關閉廣告視窗');
                                AmodalCloseV();                  //關閉影音廣告視窗
                            })
                        }
                    })
                }else{
                    //console.log('請先登入會員');
                }
            }
        })
    });
    
    $(window).on('resize', function(){
        $.each(esAOptions.vClass.vModal,function(i,item){
            if(i == 'show' & item == true){
                calculatedSizeV();               //計算window與廣告的尺寸與顯示位置
                
                //判斷影音廣告是否開啟
                if($('#Amodal-v').css('display')!=='none'){
//                    window.location.reload();
                    addAvideo();                    //重載廣告
                }
            }
        })
        
    });

    //判斷庫存與領取狀態
    function checkticket(callback) {
        var $ajaxUrl = $locationUrl + '/oscode/getAdOscodeItem/';
        $.ajax({  
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            dataType: 'json',
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,  
            success: showResponse
        });

        function showResponse(data){                                              //生成div的function
            if(data["retCode"] == '1'){
                //console.log('取得資料成功');
                var $arr = data["retObj"];
                //console.log($arr);
                if(!($arr.spid == null || $arr.spid == undefined || $arr.spid == '')){
                    if($arr.getcount < $arr.totalcount){
                        esAOptions.vClass.reModal.ticketid = $arr.spid;
                        esAOptions.vClass.reModal.ticketName = $arr.name;
                        esAOptions.vClass.reModal.ticketNum = $arr.onum;
                        callback(true);
                    }else{
                        //console.log('會員今日已達領取上限!')
                        callback(false);
                    }
                }else{
                    //console.log('所有殺價券全數贈送完畢!')
                    callback(false);
                }
            }else if(data["retCode"] == '-10001'){
                //console.log('請先登入');
                callback(false);
            }
        }
        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }
        //加載錯誤訊息
        function showFailure() {
        }
    }

    //建立浮動廣告Btn與視窗
    function createAmodalV() {
        $('body')
        .append(
            $('<div/>')
            .addClass(esAOptions.vClass.vModal.flexABtn)
            .append(
                $('<img/>')
                .addClass('img-fluid')
                .attr('src',$locationUrl + '/static/img/lookBtn.png')
            )
        )
        .append(
            $('<div/>')
            .attr('id',esAOptions.vClass.vModal.AmodalV)
            .append(
                $('<div/>')
                .attr('id',esAOptions.vClass.vModal.AmodalRWDV)
            )
        )
        .append(
            $('<div/>')
            .addClass(esAOptions.vClass.vModal.AmodalBgV)
        )
    }
    
    //計算window與廣告的尺寸與顯示位置
    function calculatedSizeV() {
        var $winW = $(window).width();
        var $winH = $(window).height();
        var $videoW;
        if($winW < 500){
            $videoW = parseInt($winW * 0.8);
        }else{
            $videoW = parseInt($winW * 0.6);
        }
        var $videoH = parseInt($videoW / 16 * 9);
        $('#'+esAOptions.vClass.vModal.AmodalV).css({'top':parseInt(($winH-$videoH)/2),'width':$videoW,'height':$videoH});
    }

    //A彈窗開啟
    function AmodalOpenV() {
        $('#'+esAOptions.vClass.vModal.AmodalV).show();
        $('.'+esAOptions.vClass.vModal.AmodalBgV).show();
        $('body').addClass('modal-open');
        addAvideo();
    }

    //A彈窗關閉
    function AmodalCloseV() {
        $('#'+esAOptions.vClass.vModal.AmodalV).hide();
        $('.'+esAOptions.vClass.vModal.AmodalBgV).hide();
        $('body').removeClass('modal-open');
        $('#'+esAOptions.vClass.vModal.AmodalRWDV).text('');             //關閉彈窗時,移除廣告
        $('.'+esAOptions.vClass.vModal.AmodalBgV).remove();
    }
    
    //載入廣告
    function addAvideo() {
        $('#'+esAOptions.vClass.vModal.AmodalRWDV).text('');             //初始化
        $('#'+esAOptions.vClass.vModal.AmodalRWDV)
            .append(
                $('<ins/>')
                .addClass('clickforcepreroll')
                .attr('data-ad-moAutoplay','true')
                .attr('data-ad-zone','4092')
                .attr('data-ad-width','100%')
                .attr('data-ad-height','100%')
                .attr('data-ad-mute','true')
            )
            .append(
                $('<script/>')
                .attr('type','text/javascript')
                .attr('src','//cdn.doublemax.net/js/cfvast.js')
            )
        //開始偵測有無廣告影片
        checkV();
    }
    
    //判斷有無廣告
    function checkV() {
        var checkVideo = setInterval(function(){
            if($('.clickforcepreroll').find('iframe').contents().length > 0){
                esAOptions.played = true;
                clearInterval(checkVideo);
            }else{
                esAOptions.played = false;
            }
        },1000);
        
        setTimeout(function() {
            clearInterval(checkVideo);
        },3000);
    }

    //廣告播畢，執行function
    function afterClickforceVad() {
        if(esAOptions.played == true){
            //傳送回資料庫
            posttdata(function(bool){
                if(bool == true){
                    AmodalCloseV();
                    createReceiveModal();                   //建立訊息視窗
                    ReceiveModalOpen();                     //顯示訊息視窗
                }else{
                    console.log(bool);
                }
            })
        }else{
            console.log('目前無廣告可觀看，敬請期待');
        }
    }

    //回存資料
    function posttdata(callback) {
        var $ajaxUrl = $locationUrl + '/oscode/sendAdOscode/';
        //console.log($ajaxUrl)
        var $userid = esAOptions.userid;
        var $ticketid = esAOptions.vClass.reModal.ticketid;
        //console.log('會員ID:'+userid+'__送券ID:'+ticketid);
        var formData = new FormData();
 		    formData.append('spid',$ticketid);
        $.ajax({  
            url: $ajaxUrl,
            contentType: 'application/json; charset=utf-8',
            type: 'POST',
            data:formData,
            dataType: 'json',
            async: 'false',
            cache: 'false',
            timeout: '4000',
            processData: false,
            contentType: false,
            complete: endLoading,
            error: showFailure,  
            success: showResponse
        });

        function showResponse(data){                                              //生成div的function
            var $retCode = data["retCode"];
            switch($retCode){
                case 1:
                    //console.log('取得資料成功');
                    callback(true);
                    break;
                case -10001:
                    callback('請先登入');
                    break;
                case -10002:
                    callback('領取序號有誤');
                    break;
                case -10003:
                    callback('今天已到領取上限');
                    break;
                case -10004:
                    callback('領取失敗，此卷已送完');
                    break;
                case -10005:
                    callback('領取失敗');
                    break;
                default:
                    break;
            }
        } 
        //加載動作完成 (不論結果success或error)
        function endLoading() {
        }
        //加載錯誤訊息
        function showFailure() {
        }
    }

    //創建領取視窗
    function createReceiveModal() {
        var $mtextName = esAOptions.vClass.reModal.ticketName;
        var $mtextNum = esAOptions.vClass.reModal.ticketNum;
        $('body')
        .append(
            $('<div/>')
            .attr('id',esAOptions.vClass.reModal.id)
//            .append(
//                $('<div/>')
//                .addClass(esAOptions.vClass.reModal.mheader)
//                .text('已觀看廣告')
//            )
            .append(
                $('<div/>')
                .addClass(esAOptions.vClass.reModal.mbody)
                .append(
                    $('<div/>')
                    .addClass(esAOptions.vClass.reModal.mimg)
                    .append(
                        $('<img/>')
                        .addClass('img-fluid')
                        .attr('src',$locationUrl + '/static/img/bid-checkout-img.png')
                    )
                )
                .append(
                    $('<div/>')
                    .addClass(esAOptions.vClass.reModal.mtextBox)
                    .append(
                        $('<div/>')
                        .text('恭喜獲得')
                    )
                    .append(
                        $('<div/>')
                        .addClass(esAOptions.vClass.reModal.mtextName)
                        .text($mtextName)
                    )
                    .append(
                        $('<div/>')
                        .append('殺價券')
                        .append(
                            $('<span/>')
                            .addClass(esAOptions.vClass.reModal.mtextNum)
                            .text($mtextNum)
                        )
                        .append('張')
                    )
                )
                .append(
                    $('<div/>')
                    .addClass(esAOptions.vClass.reModal.mbtn)
                    .text('關閉')
                )
            )
        )
        .append(
            $('<div/>')
            .addClass(esAOptions.vClass.reModal.mbg)
        )
    }

    //領取視窗開啟
    function ReceiveModalOpen() {
        $('#'+esAOptions.vClass.reModal.id).show();
        $('.'+esAOptions.vClass.reModal.mbg).show();
        $('body').addClass('modal-open');
        
        //增加點擊事件
        $('.'+esAOptions.vClass.reModal.mbtn).on('click',function(){
            ReceiveModalClose();
        })
        $('.'+esAOptions.vClass.reModal.mbg).on('click',function(){
            ReceiveModalClose();
        })
    }

    //領取視窗關閉
    function ReceiveModalClose() {
        $('#'+esAOptions.vClass.reModal.id).hide();
        $('.'+esAOptions.vClass.reModal.mbg).hide();
        $('body').removeClass('modal-open');
        window.location.reload();
        //關閉彈窗時,移除視窗
//        $('#'+esAOptions.vClass.reModal.id).remove();
//        $('.'+esAOptions.vClass.reModal.mbg).remove();
    }



// --------------------------- 靜態廣告 ---------------------------//
    /*
        在頁面中新增div作為生成位置
        ex: <div class="mModalBox"></div>
        蓋板廣告 cModalBox
        小型廣告 sModalBox
        中型廣告 mModalBox
        大型廣告 lModalBox
        
        RWD位置微調 請使用CSS調整父層框
    */
    $(window).on('load', function(){
        iscfAdBlock(function(bool){
            if(bool == false){
                //建立廣告容器
                createAmodal();
                //新增廣告生成
                addAmodal();
                //計算window與廣告的尺寸與顯示位置
                calculatedSize();
            }else{
                //console.log('廣告阻擋');
            }
        })
    });
    $(window).on('resize', function(){
        //計算window與廣告的尺寸與顯示位置
        calculatedSize();
    });
    
    //建立廣告容器
    function createAmodal() {
        $.each(esAOptions.imgClass,function(i,item){
            if(item.show == true){
                $(item.pclass).append(
                    $('<div/>')
                    .attr('id',item.modalid)
                )
            }
        })
    }
    
    //載入廣告
    function addAmodal() {
        $.each(esAOptions.imgClass,function(i,item){
            if(item.show == true){
                $('#'+item.modalid).text('');             //初始化
                $('#'+item.modalid)
                    .append(
                        $('<ins/>')
                        .addClass('clickforceads')
                        .attr('data-ad-zone',item.zone)
                    )
                    .append(
                        $('<script async/>')
                        .attr('type','text/javascript')
                        .attr('src','//cdn.doublemax.net/js/init.js')
                    )
            }
        })  
    }

    //計算window與廣告的尺寸
    function calculatedSize() {
        $.each(esAOptions.imgClass,function(i,item){
            if(item.show == true){
                var $winW = $(window).width();
                var $winH = $(window).height();
                var $aW;
                if(item.full == true){
                    $aW = $(item.pclass).width();
                }else{
                    $aW = item.modalW;
                }
                var $aH = parseInt($aW / item.modalW * item.modalH);    //廣告尺寸 取得等比縮放
                
                
                if(i=='cModal'){                                        //蓋板廣告調整
//                    $('#'+item.modalid).css({'width':$winW - 60,'height':$winH - 60,'margin':'30px'});
                    $(item.pclass).addClass('align-items-center justify-content-center');
                    $('#'+item.modalid).css({'width':'320','height':'480','margin':'30px auto','z-index':'999','position': 'relative'});
                    $('#'+item.modalid).find('ins').css({'width':'320','height':'480','top':'0','left':'0'});
                    
                    //關閉按鈕、背景遮罩建立
                    $('#'+item.modalid)
                        .prepend(
                            $('<div/>')
                            .addClass('cModal-close')
                            .text('x')
                        )
                        .after(
                            $('<div/>')
                            .addClass('cModal-bg')
                        )
                    
                    $('.cModal-close').on('click', function(){
                        $(item.pclass).remove();
                        $('body').removeClass('modal-open');
                    })
                    $('.cModal-bg').on('click', function(){
                        $(item.pclass).remove();
                        $('body').removeClass('modal-open');
                    })
                }else{
                    $('#'+item.modalid).css({'width':$aW,'height':$aH});
                    $('#'+item.modalid).find('ins').css({'width':$aW,'height':$aH});
                }
                changeASize(i,item);
            }
        })        
    }
    
    //調整RWD尺寸
    //RWD位置微調 請使用CSS調整父層框
    function changeASize(i,item) {
        var checkiframe;
        if(item.show == true){
            if($(item.pclass).length > 0){
                checkiframe = setInterval(function(){
                    var $afinsW = $('#'+item.modalid).css('width');
                    var $afinsH = $('#'+item.modalid).css('height');
                    var $ifm = $('#'+item.modalid).find('iframe');
                    $ifm.css({'width':$afinsW,'height':$afinsH});

                    //蓋板廣告調整
                    if(i == 'cModal') {
                        if($ifm.contents().find('.cfadif8628').length > 0){
                            $(item.pclass).find('.cModal-bg').show();
                            $(item.pclass).addClass('d-flex');
                            $('body').addClass('modal-open');
                            clearInterval(checkiframe);
                        }
                    }else{
                        $(item.pclass).show();
                        if(i == 'sModal'){
                            var $container = $('[class$="-content"]');
                            $container.each(function(){
                                var $that = $(this);
                                var $thatClass = $that.attr('class');
                                if($thatClass == 'home-content' || $thatClass == 'broadcast-content' || $thatClass == 'enterprise-login-content' || $thatClass == 'enterprise-content' || $thatClass == 'first-content' || $thatClass == 'other-content'){
                                    var bottom = parseFloat($('.barItems').height());
                                    var addH = parseFloat($('#'+item.modalid).css('height'));
                                    $that.css({'padding-bottom': bottom + addH + 'px'});
                                }
                            })
                            if($ifm.contents().find('.cfadim').length > 0){
                                $ifm.contents().find('.cfadim').css({'width':$afinsW,'height':$afinsH});
                                $('.sajaFooterNavBar').css({'-wibkit-box-shadow': '0 2px 10px 0px #AAA','-moz-box-shadow': '0 2px 10px 0px #AAA',' -ms-box-shadow': '0 2px 10px 0px #AAA','-o-box-shadow': '0 2px 10px 0px #AAA','box-shadow': '0 2px 10px 0px #AAA'});
                                $('.sajaWrapper-show-box').css({'height': 'calc(100vh - 3.75rem - 4.584rem - 4.167rem - 4.834rem)'});
                                clearInterval(checkiframe);
                            }
                        }else{
                            if($ifm.contents().find('.cfadim').length > 0){
                                $ifm.contents().find('body').css({'text-align':'center'});
                                $ifm.contents().find('img:not(".cflogo")').css({'width':$afinsW,'height':$afinsH,'margin':'0 auto','display':'block'});
                                $ifm.contents().find('.cfadim').css({'width':$afinsW,'height':$afinsH});
                                clearInterval(checkiframe);

                            //巢狀內嵌式iframe
                            }else if($ifm.contents().find('.cfadif8634').length > 0){
                                $ifm.contents().find('body').css({'text-align':'center'});
                                $ifm.contents().find('img:not(".cflogo")').css({'width':$afinsW,'height':$afinsH,'margin':'0 auto','display':'block'});
                                $ifm.contents().find('.cfadif8634').css({'width':$afinsW,'height':$afinsH});
                                clearInterval(checkiframe);
                            }
                        }
                        
                    }
                    //isimgcfAdBlock(i,item);
                },500);
                function stopcheckiframe() {
                    clearInterval(checkiframe);
                }
            }
        }
    }

    //偵測廣告阻擋 true 為 有阻擋
    //無阻擋時才show，以免空div佔位
//    function isimgcfAdBlock(i,item) {
//        //console.log(i,item)
//        var $ifm = $('#'+item.modalid).find('iframe');
//        if(i == 'sModal'){
//            $(item.pclass).show();
//            var $container = $('[class$="-content"]');
//            $container.each(function(){
//                var $that = $(this);
//                var $thatClass = $that.attr('class');
//                if($thatClass == 'home-content' || $thatClass == 'broadcast-content' || $thatClass == 'enterprise-login-content' || $thatClass == 'enterprise-content' || $thatClass == 'first-content' || $thatClass == 'other-content'){
//                    var bottom = parseFloat($('.barItems').height());
//                    var addH = parseFloat($('#'+item.modalid).css('height'));
//                    $that.css({'padding-bottom': bottom + addH + 'px'});
//                }
//            })
//            $('.sajaWrapper-show-box').css({'height': 'calc(100vh - 3.75rem - 4.584rem - 4.167rem - 4.834rem)'});
//        }else{
//            $(item.pclass).show();
//        }
//    }

