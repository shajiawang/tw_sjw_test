/*使用說明*/
/*
    請在需要使用小鍵盤處，增加class,預設為 'keyboard'
    樣板有兩個，有小數點、無小數點
    若要展開有小數點的，增加class,'dian',這是判斷生成有無小數點鍵盤的依據
    
    應用範例
    <!--  鍵盤-自訂義CSS  -->
      <?php $kbcss = 'css/keyboard_v1.2.css'; ?>
      <link rel="stylesheet" href="<?=base_url() ?><?php echo $kbcss; ?>?ver=<?php echo filemtime($kbcss); ?>">
      
    <!--  鍵盤-自訂義JS  -->
    <?php $kbjs = 'js/keyboard_v1.2.js'; ?>
    <script src="<?=base_url() ?><?php echo $kbjs; ?>?ver=<?php echo filemtime($kbjs); ?>"></script>
    
    <!--  小鍵盤動作  -->
    <script>
        $(function(){
            options.dom.callerId = '#oriPayTotal';                          //預設一開始的呼叫者,在keyboard.JS會做變更 點擊者
            options.btn.sure = '確定';                                       //小鍵盤送出按鈕名稱
            $(options.dom.caller).each(function(i){
                var $modalNum = options.dom.keyidName+i;                    //在小鍵盤加編號，因應同畫面有多個呼叫小鍵盤者
                //console.log($modalNum);
                var $that = $(this);
                $that.on("click",function(e){
                    options.dom.callerId = $that;                           //記錄點擊哪個input的ID
                    
                    $('#'+$modalNum).on('show.bs.modal', function (e) {     //開啟小鍵盤後 接著執行那些動作
                                                                            //但若 預設開啟時，此段不會執行，
                                                                            //若要設定預設開啟時的動作 請另外寫在網頁開啟後的動作裡

                    });

                    $('#'+$modalNum).on('hide.bs.modal', function (e) {     //關閉小鍵盤後 接著執行那些動作
                        var $callerId = $(options.dom.callerId);            //取得按鈕ID
                    });
                });
            });
            
        //自動開啟小鍵盤
        setTimeout(function(){
            $(options.dom.callerId).click();                //小鍵盤預設開啟 (類似input的focus)
        },options.time.openDelay);
        
        //防止小鍵盤沒吊起，每毫秒判斷未吊起時重新點擊開啟
        var interval = null;
        $(function(){
            //$(".captcha_keyboard").click();
            interval = setInterval(Continuous,500);
        })
        //持續判斷小鍵盤
        function Continuous() {
            var $modal = $("[id^='"+options.dom.keyidName+"']");
            if($modal.length > 0){
                if($modal.hasClass("show")){
                    //已吊起，要移除持續判斷
                    clearInterval(interval);
                }else{
                    $(options.dom.callerId).click();
                }
            }else{
                $(options.dom.callerId).click();
            }
            
        }
    })
</script>
*/


var options = {
    dom: {
        caller: '.keyboard', //用來定義 此class會展開小鍵盤
        keyidName: 'numkeyModal', //小鍵盤本體(無id符號)
        keyModal: '#' + this.keyidName, //小鍵盤本體(有id符號)
        callerId: '' //紀錄呼叫小鍵盤者ID
    },
    point: 2, //取小數位數
    btn: {
        sure: '送出' //sure按鈕的名稱
    },
    modalBg: {
        isShow: 'Y' //彈窗時是否出現背景 大寫 Y 或 N
    },
    time: {
        openDelay: '200', //預設自動開啟小鍵盤的毫秒數
        reopen: '500' //若預設的開啟沒成功，自動偵測小鍵盤開啟的毫秒數
    }
};
//ex:需要彈窗時無背景，options.modalBg.isShow = 'N'


//增加底層 預留鍵盤的假div,num高度帶入鍵盤高度
function addBlank(num) {
    $("body").append('<div class="addBlank"></div>');
    $(".addBlank").css("paddingTop", num);
}
//移除預留鍵盤的假div
function removeBlank() {
    $(".addBlank").remove();
}

//調整輸入框位置
//控制鍵盤捲動位置
//情況一 最頂 (直接顯示)
//情況二 中間 (捲動到輸入區域)
//情況三 最底 (頁面底部增加鍵盤高度，並捲到最下方) (關閉時記得減回來)
function controlSize($wherekey) {
    //原始scroll
    var $olderScroll = $(window).scrollTop();

    //先計算視窗高度
    var $phH = $(window).innerHeight();
    //整個頁面高度
    var $pageH = $("body").height();
    //計算鍵盤高度
    var $boardPx = $(".number-style").outerHeight();
    var $keyboardH = (parseFloat($boardPx) * 3.5 + 4) * 4 + 4;


    //輸入框位置
    var $inputPlace = $wherekey.offset().top;
    //輸入框元件高度
    var $inputH = $wherekey.height();
    //儲存當前捲動值
    var $nowScroll = $(window).scrollTop();

    //輸入框與鍵盤間的安全距離
    var $safeH = 30;

    //計算扣除鍵盤剩餘高度可容許值 下限
    var $mintotalH = $phH - $keyboardH - $inputH - $safeH;
    //計算扣除鍵盤剩餘高度可容許值 上限
    var $maxtotalH = $pageH - $keyboardH - $inputH - $safeH;

    if ($inputPlace > $mintotalH) {
        if ($inputPlace < $maxtotalH) {
            $('html, body').animate({ scrollTop: $inputPlace - ($mintotalH / 2) }, 300);
            //console.log(1)
        } else {
            addBlank($keyboardH);
            $('html, body').animate({ scrollTop: $inputPlace - ($mintotalH / 2) }, 300);
            //console.log(2)
        }
    }


    //toDebug("視窗高度:"+$phH+"..."+"頁面高度:"+$pageH+"..."+"鍵盤高度:"+$keyboardH+"..."+"輸入框位置:"+$inputPlace+"..."+"可容許值 下限:"+$mintotalH+"..."+"可容許值 上限:"+$pageH+"..."+"輸入框元件高度:"+$inputH+"..."+"安全距離:"+$safeH);
}

/*按鈕動作*/
function Calculation(which, $domEle) {
    //建立閃爍文字游標
    $numBoxLi.eq($tabNum).append($pic);
    options.dom.callerId = which;

    function addnum(num) {
        var $domEle = $("#passport");
        var oldtext = $domEle.val();
        if (oldtext == '0') {
            oldtext = ''
        }
        $domEle.val(parseFloat(oldtext + num));
        which.val((parseFloat(oldtext + num) / Math.pow(10, options.point)).toFixed(options.point));
        return;
    };
    $("#b1").on('click', function() {
        addnum("1");
    });
    $("#b2").on('click', function() {
        addnum("2");
    });
    $("#b3").on('click', function() {
        addnum("3");
    });
    $("#b4").on('click', function() {
        addnum("4");
    });
    $("#b5").on('click', function() {
        addnum("5");
    });
    $("#b6").on('click', function() {
        addnum("6");
    });
    $("#b7").on('click', function() {
        addnum("7");
    });
    $("#b8").on('click', function() {
        addnum("8");
    });
    $("#b9").on('click', function() {
        addnum("9");
    });
    $("#b0").on('click', function() {
        addnum("0");
    });
    $("#dn").on('click', function() {
        var domEle = $("#passport");
        var oldtext = domEle.val();
        //判斷是否已經輸入小數點
        var placedn = oldtext.indexOf(".");
        if (!$(this).hasClass("disabled")) {
            if (placedn == -1) {
                domEle.val(oldtext + '.');
                which.val(oldtext + '.');
            }
        }
    });

    $("#x").on('click', function() {
        var old = $domEle.val();

        if (old.length > 0) {
            $tabNum = which.val().length
            if (old.length == 1) {
                which.val('')
            } else {
                which.val((old.substr(0, old.length - 1) / Math.pow(10, options.point)).toFixed(options.point));
            }

            $domEle.val(old.substr(0, old.length - 1));
            //$numBoxLi.eq($tabNum-1).removeClass("act").find("span").text('');
            //$numBoxLi.find(".pic").remove();
            //$numBoxLi.eq($tabNum-1).append($pic);
        }
    });

    //確定按鈕
    $("#sure").on("click", function(e) {
            if ($domEle.val() != '') {
                which.val(String(which.val()));
            } else {
                which.val('');
            };

            $('[id^=' + options.dom.keyidName + ']').modal('toggle');
            e.stopPropagation;
            e.preventDefault;
        })
        //動畫效果
    function testAnim(x) {
        $('.modal .modal-dialog').attr('class', 'modal-dialog ' + x + ' animated');
    };
    $('[id^=' + options.dom.keyidName + ']').on('show.bs.modal', function(e) {
        var anim = "fadeInUp";
        testAnim(anim);
    });

    $('[id^=' + options.dom.keyidName + ']').on('hide.bs.modal', function(e) {
        var anim = "fadeOutDown";
        testAnim(anim);
        //$(options.dom.caller).unbind();
        $('[id^=' + options.dom.keyidName + ']').remove();
        //$numBoxLi.find(".pic").remove();
    });
}

/*生成鍵盤*/
function hasDianClass(Boolean, modalNum) {
    var backBtn = '/site/static/img/Delete.png'; //退回鍵圖示
    if (Boolean) {
        console.log('5K4XU3')
            //true時，有小數點的鍵盤
            //options.btn.sure = '<i class="esfas chevron-down graycolor"></i>';                           //小鍵盤送出按鈕改為圖示
        $("body").append('<div class="modal fade' + ((options.modalBg.isShow == "N") ? ' noShowBg' : '') + '" id="' + modalNum + '" tabindex="-1" role="dialog" aria-labelledby="numkeyModalLabel" aria-hidden="true">\
            <div class="modal-dialog" role="document">\
                <div id="sure" class="col btn number-style gray sure-btn">' + options.btn.sure + '</div>\
                <div class="modal-content numberkeyboard-box">\
                    <div class="hide">\
                        <input id="passport" type="text" class="form-control" placeholder="請輸入數字"/>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="b1" class="col btn number-style">1</div>\
                        <div id="b2" class="col btn number-style">2</div>\
                        <div id="b3" class="col btn number-style">3</div>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="b4" class="col btn number-style">4</div>\
                        <div id="b5" class="col btn number-style">5</div>\
                        <div id="b6" class="col btn number-style">6</div>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="b7" class="col btn number-style">7</div>\
                        <div id="b8" class="col btn number-style">8</div>\
                        <div id="b9" class="col btn number-style">9</div>\
                    </div>\
                    <div class="row no-gutters">\
                        <div id="dn" class="col btn number-style gray">.</div>\
                        <div id="b0" class="col btn number-style">0</div>\
                        <div id="x" class="col btn number-style gray"><img src="' + backBtn + '"></div>\
                    </div>\
                </div>\
            </div>\
        </div>');
    } else {
        console.log('ASFASW')
            //false時，無小數點的鍵盤
        $("body").append('<div class="modal fade' + ((options.modalBg.isShow == "N") ? ' noShowBg' : '') + '" id="' + modalNum + '" tabindex="-1" role="dialog" aria-labelledby="numkeyModalLabel" aria-hidden="true">\
            <div class="modal-dialog" role="document">\
                        <div class="modal-content numberkeyboard-box">\
                            <div class="hide">\
                                <input id="passport" type="text" class="form-control" placeholder="請輸入數字"/>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="b1" class="col btn number-style">1</div>\
                                <div id="b2" class="col btn number-style">2</div>\
                                <div id="b3" class="col btn number-style">3</div>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="b4" class="col btn number-style">4</div>\
                                <div id="b5" class="col btn number-style">5</div>\
                                <div id="b6" class="col btn number-style">6</div>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="b7" class="col btn number-style">7</div>\
                                <div id="b8" class="col btn number-style">8</div>\
                                <div id="b9" class="col btn number-style">9</div>\
                            </div>\
                            <div class="row no-gutters">\
                                <div id="x" class="col btn number-style gray"><img src="' + backBtn + '"></div>\
                                <div id="b0" class="col btn number-style">0</div>\
                                <div id="sure" class="col btn number-style gray">' + options.btn.sure + '</div>\
                            </div>\
                        </div>\
                    </div>\
                </div>');
    };
}

/*小鍵盤動作*/
$(function() {
    $(options.dom.caller).each(function(i, e) {
        var $modalNum = options.dom.keyidName + i; //小鍵盤本體
        var $modalAddId = "#" + $modalNum; //小鍵盤本體(加id符號)
        var $that = $(this);

        //自動處理input
        /*自動將輸入區域type改為text，才能即時顯示小數點，否則number時，如「56.」會顯示空白*/
        /*readonly 使用原因：防止預設鍵盤跳出*/
        var $type = $that.attr("type");
        switch ($type) {
            case "number":
                $(this).attr("type", "text");
                break;
            case "tel":
                $(this).attr("type", "text");
                break;
            case "password":
                //生成明暗碼按鈕
                //changeBtn($(this));
                break;
            case "text":
                break;
        }
        $that
            .attr("data-toggle", "modal")
            .attr("data-target", $modalAddId)
            .attr("readonly", "readonly")
            .attr("unselectable", "return false;") //解決IOS,readonly無效
            .attr("onfocus", "this.blur()") //解決IOS,readonly無效
            .addClass("readonlyIOS") //解決IOS,readonly無效

        $that.on("click", function(e) {
            var $dom = $(this);
            /* 判斷生成哪個鍵盤 */
            var hasDian = $dom.hasClass('dian'); //回傳true or false
            if ($('#' + $modalNum).length > 0) {
                $('#' + $modalNum).remove();
            }
            hasDianClass(hasDian, $modalNum); //判斷生成有無小數點鍵盤

            var $domEle = $($modalAddId).find("#passport"); //暫存區

            //將原先輸入框裡的值帶入鍵盤的暫存區
            if ($dom.val() != '') {
                $domEle.val($dom.val() * Math.pow(10, options.point));
            } else {
                $domEle.val('');
            }

            e.stopPropagation;
            e.preventDefault;

            //計算
            Calculation($that, $domEle);

        })

    })
})