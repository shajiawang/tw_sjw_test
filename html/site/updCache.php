#!/usr/bin/php
<?php
  
  include_once "lib/config.php";
  
  function logs($msg) {
	 file_put_contents("/tmp/updCache.log", date("Y-m-d H:i:s")."--".$msg."\n", FILE_APPEND | LOCK_EX);
  }
    
  function sendReq($url, $method="GET", $params) {
        if(empty($url))
            return false;
        if($method=="GET") {
           logs("[updCache] sendReq GET : ".$url); 
           $ret = file_get_contents($url);   
        } else if ($method=="POST") {
            // logs("[updCache] sendReq POST ");
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            $ret=curl_exec($ch);
            curl_close($ch); 
        }
        logs("[updCache] sendReq : ".$ret);
        return $ret;
  }
  
  // 每分鐘更新 cache 內的 inx_product_list 值
  sendReq(BASE_URL.APP_DIR."/product/getSiteHomeAdBannerList/?reload=Y","GET","");
  sendReq(BASE_URL.APP_DIR."/product/inx_product_list/?reload=Y","GET","");
  sendReq(BASE_URL.APP_DIR."/bid/recently_closed_product_list/?reload=Y","GET","");

?>
