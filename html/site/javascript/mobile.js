window.cloud7IndexData = {
    banner: [{
        src: "/Media/xmatomall/Pics/201418104429609XSRY.jpg",
        url: "Goods/Subject/28"
    }, {
        src: "/Media/xmatomall/Pics/201418104523844YRYH.jpg",
        url: "Goods/Subject/22"
    }],
    column: [{
        title: "精品推荐",
        subTitle: "三文鱼 大闸蟹 北极虾",
        url: "Goods/Subject/29",
        background: "red"
    }, {
        title: "超值海鲜",
        subTitle: "畅销经典的大礼包",
        url: "Goods/List",
        background: "yellow"
    }, {
        title: "海鲜礼盒",
        subTitle: "燕窝 海参 鲍鱼",
        url: "Goods/List?categoryId=5",
        background: "cyan"
    }, {
        title: "休闲零食",
        subTitle: "不一样的海鲜",
        url: "Goods/List?categoryId=3",
        background: "blue"
    }, {
        title: "自取点",
        subTitle: "前门 五道口 中关村",
        url: "Subbranch/List",
        background: "red"
    }, {
        title: "码头故事",
        subTitle: "每个人都有一个码头梦",
        url: "blog/34",
        background: "yellow"
    }]
};
;

(function () {
    var aG = function (aM, aK, aL) {
        return {
            win32: aL === "Win32",
            ie: /MSIE ([^;]+)/.test(aM),
            ieMobile: window.navigator.msPointerEnabled,
            ieVersion: Math.floor((/MSIE ([^;]+)/.exec(aM) || [0, "0"])[1]),
            ios: (/iphone|ipad/gi).test(aK),
            iphone: (/iphone/gi).test(aK),
            ipad: (/ipad/gi).test(aK),
            iosVersion: parseFloat(("" + (/CPU.*OS ([0-9_]{1,5})|(CPU like).*AppleWebKit.*Mobile/i.exec(aM) || [0, ""])[1]).replace("undefined", "3_2").replace("_", ".").replace("_", "")) || false,
            safari: /Version\//gi.test(aK) && /Safari/gi.test(aK),
            uiWebView: /(iPhone|iPod|iPad).*AppleWebKit(?!.*Safari)/i.test(aM),
            android: (/android/gi).test(aK),
            androidVersion: parseFloat("" + (/android ([0-9\.]*)/i.exec(aM) || [0, ""])[1]),
            webkit: /AppleWebKit/.test(aK),
            uc: aK.indexOf("UCBrowser") !== -1,
            Browser: / Browser/gi.test(aK),
            MiuiBrowser: /MiuiBrowser/gi.test(aK)
        }
    }(navigator.userAgent, navigator.appVersion, navigator.platform);
    var aJ, J, z, au, x;
    var l = {
        positive: true,
        negative: false
    };

    function E(aN, aK) {
        for (var aL = 0, aM = aN.length; aL < aM; aL++) {
            aK(aN[aL], aL)
        }
    }

    function G(aM, aK) {
        for (var aL in aM) {
            aK(aL, aM[aL])
        }
    }

    function D(aM, aK) {
        for (var aL = 0; aL < aM; ++aL) {
            aK(aL)
        }
    }

    function F(aN, aK) {
        for (var aL = 0, aM = aN.length; aL !== aM; ++aL) {
            aK(aN.item(aL), aL)
        }
    }

    function aF(aK) {
        return function () {
            var aN = aK + "(";
            for (var aL = 0, aM = arguments.length; aL < aM; aL++) {
                if (aL !== 0) {
                    aN += ","
                }
                aN += arguments[aL].toString()
            }
            aN += ")";
            return aN
        }
    }

    function at() {
        var aN = "",
            aK;
        for (var aL = 0, aM = arguments.length; aL < aM; aL++) {
            aK = arguments[aL];
            if (aK && aL !== 0) {
                aN += " "
            }
            aN += aK.toString()
        }
        return aN
    }
    var al = aF("scale");
    var ai = aF("rgba");
    var aj = aF("rotateY");
    var aE = aF("translate3d");

    function ac(aN, aL, aM, aK) {
        E(aN.split(aL), function (aP) {
            var aO = aP.split(aM);
            aK(aO[0], aO[1])
        })
    }

    function A(aM, aK, aL) {
        if (aK <= aL) {
            return aM >= aK && aM < aL
        } else {
            return A(aM, aL, aK)
        }
    }
    var C = (function () {
        var aK = {};
        var aL = ["Array", "Boolean", "Date", "Number", "Object", "RegExp", "String", "Window", "HTMLDocument"];
        E(aL, function (aM) {
            aK[aM] = function (aN) {
                return Object.prototype.toString.call(aN) == "[object " + aM + "]"
            }
        });
        return aK
    })();

    function B(aK, aL) {
        G(aL, function (aM, aN) {
            aK[aM] = aN
        });
        return aK
    }

    function s(aK, aL) {
        var aM = {};
        B(aM, aK);
        B(aM, aL);
        return aM
    }

    function k(aK, aL) {
        return s(aL, aK || {})
    }

    function af(aM, aK, aL) {
        if (aK <= aL) {
            return aM < aK ? aK : aM > aL ? aL : aM
        } else {
            return af(aM, aL, aK)
        }
    }

    function aw(aM, aO, aL) {
        var aN;

        function aK(aP) {
            if (aP === undefined) {
                return aN
            } else {
                if (aN !== aP) {
                    aN = aP;
                    aO(aP)
                }
            }
        }
        aL && aK(aM);
        return aK
    }

    function i(aN, aK) {
        var aL = q(aK);
        var aO = q();
        var aM = 0;
        return {
            check: function () {
                if (++aM === aN) {
                    aO.trig();
                    aL.trig();
                    aO = null;
                    aL = null
                }
            },
            onDone: function (aQ, aP) {
                if (aM === aN) {
                    aQ()
                } else {
                    aP && aP({
                        onLoadingEnd: aO.regist
                    });
                    aL.regist(aQ)
                }
            }
        }
    }

    function h(aK) {
        return function (aM) {
            var aN = 0;
            for (var aL = aM; aL !== null; aL = aL.offsetParent) {
                aN += aL[aK]
            }
            return aN
        }
    }
    var t = h("offsetLeft");
    var u = h("offsetTop");

    function ag(aK) {
        aK.parentNode.removeChild(aK)
    }

    function e(aM, aN) {
        var aL = aN.childNodes,
            aK = aL.length;
        while (aK !== 0) {
            aM.appendChild(aL[0]);
            --aK
        }
    }

    function ae(aK, aN) {
        aK.setAttribute("data-z", "");
        var aL = aK.parentNode;
        if (!aL) {
            aL = document.createElement("div");
            aL.appendChild(aK)
        }
        var aM = aL.querySelector("[data-z]>" + aN);
        aK.removeAttribute("data-z");
        return aM
    }
    var d = Element.prototype.appendChild;
    Element.prototype.appendChild = function (aK) {
        if (this.hasAttribute("data-zone")) {
            var aL = ae(this, "[data-footer]");
            aL ? this.insertBefore(aK, aL) : d.call(this, aK)
        } else {
            d.call(this, aK)
        }
    };

    function aI() {
        var aK = document.createElement("div");
        Y(aK, function (aL) {
            aL.preventDefault();
            aL.onTouchMove(function (aM) {
                aM.preventDefault()
            })
        });
        j(aK, {
            position: "fixed",
            left: 0,
            top: 0,
            right: 0,
            bottom: 0
        });
        document.body.appendChild(aK);
        return aK
    }

    function H(aK, aM) {
        var aL = document.createElement("div");
        j(aL, {
            position: "absolute",
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
            "z-index": aM
        });
        aK.appendChild(aL);
        return aL
    }

    function p(aM, aK) {
        var aL = document.createElement(aM);
        aK && G(aK, function (aN, aO) {
            if (aO !== undefined) {
                switch (aN) {
                case "classList":
                    if (C.String(aO)) {
                        aL.classList.add(aO)
                    } else {
                        E(aO, function (aP) {
                            aL.classList.add(aP)
                        })
                    }
                    break;
                case "css":
                    j(aL, aO);
                    break;
                case "attr":
                    G(aL, function (aP, aQ) {
                        aL.setAttribute(aP, aQ)
                    });
                    break;
                case "innerHTML":
                    aL.innerHTML = aO;
                    break;
                case "src":
                case "href":
                case "title":
                    aL.setAttribute(aN, aO);
                    break;
                default:
                    if (aN.substring(0, 5) === "data-") {
                        aL.setAttribute(aN, aO)
                    }
                    break
                }
            }
        });
        return aL
    }

    function y(aO) {
        var aP = {};
        var aK = {
            field: function (aR, aS, aQ) {
                aR.innerHTML = aQ[aS]
            },
            text: function (aR, aS, aQ) {
                aR.textContent = aQ[aS]
            },
            src: function (aR, aS, aQ) {
                aR.setAttribute("src", aQ[aS])
            },
            href: function (aR, aS, aQ) {
                aR.setAttribute("href", aQ[aS])
            },
            attr: function (aR, aS, aQ) {
                ac(aS, " ", ":", function (aT, aU) {
                    aR.setAttribute(aT, aQ[aU])
                })
            },
            list: function (aS, aT, aQ) {
                var aR = aT.split(" ");
                aM(aR[0], aQ[1], aS)
            },
            "class": function (aR, aS, aQ) {
                E(aS.split(" "), function (aT) {
                    aR.classList.add(aQ[aT])
                })
            }
        };
        var aN = document.createElement("div");
		F(aO.querySelectorAll("[data-template-id]"), function (aR) {
            var aQ = aR.querySelector("[data-main]") || aR;
            aP[aR.getAttribute("data-template-id")] = aQ;
            aQ.removeAttribute("data-template-id");
            aQ.removeAttribute("data-main")
        });
        ag(aO);

        function aL(aT, aR, aQ) {
            var aU = aP[aT];
            var aS = aU.cloneNode(true);
            aN.appendChild(aS);
            if (aR !== undefined) {
                F(aN.querySelectorAll("*"), function (aV) {
                    G(aV.dataset, function (aX, aY) {
                        var aW = aK[aX];
                        if (aW !== undefined) {
                            aW(aV, aY, aR);
                            aV.removeAttribute("data-" + aX)
                        }
                    })
                });
                aQ && aQ(aS, aR)
            }
            return aS
        }

        function aM(aT, aS, aR, aQ) {
            E(aS, function (aU, aV) {
                aR.appendChild(aL(aT, aU, function (aX, aW) {
                    aQ && aQ(aX, aW, aV)
                }))
            })
        }
        return {
            make: aL,
            makeList: aM,
            addHandler: function (aQ) {
                B(aK, aQ)
            }
        }
    }
    var K = {
        transform: ["-webkit-transform", "-ms-transform", "transform"],
        transition: ["-webkit-transition", "transition"],
        "backface-visibility": ["-webkit-backface-visibility", "-mozila-backface-visibility", "backface-visibility"],
        "transform-style": ["-webkit-transform-style", "transform-style"],
        perspective: ["-webkit-perspective", "perspective"]
    };

    function ap(aK, aM, aN, aL) {
        if (aM in K) {
            E(K[aM], function (aO) {
                aL ? aK.style.removeProperty(aO) : aK.style.setProperty(aO, aN, "")
            })
        } else {
            aL ? aK.style.removeProperty(aM) : aK.style.setProperty(aM, aN, "")
        }
        aK.setAttribute("data-remove", "");
        aK.removeAttribute("data-remove")
    }

    function j(aM, aK, aL, aN) {
        if (C.String(aK)) {
            ap(aM, aK, aL, aN)
        } else {
            G(aK, function (aO, aP) {
                ap(aM, aO, aP, aN)
            })
        }
    }

    function q(aK) {
        var aL = null;
        return {
            trig: function () {
                var aM = arguments;
                aK && aK.apply(null, aM);
                var aO = [];
                for (var aN = aL; aN !== null; aN = aN.previous) {
                    aO.push(aN.value)
                }
                E(aO, function (aP) {
                    aP.apply(null, aM)
                })
            },
            regist: function (aN) {
                var aM = {
                    value: aN,
                    next: null,
                    previous: aL
                };
                if (aL !== null) {
                    aL.next = aM
                }
                aL = aM;
                return {
                    remove: function () {
                        if (aM.previous !== null) {
                            aM.previous.next = aM.next
                        }
                        if (aM.next !== null) {
                            aM.next.previous = aM.previous
                        } else {
                            aL = aM.previous
                        }
                        aM = null
                    }
                }
            }
        }
    }

    function L(aK, aM, aN) {
        function aL(aQ, aR) {
            for (var aO = 0, aP = aR.length; aO !== aP; ++aO) {
                if (aQ === aR.item(aO)) {
                    return true
                }
            }
            return false
        }
        document.addEventListener(aK, function (aO) {
            var aQ = document.querySelectorAll(aM),
                aP = aO.target;
            while (aP !== null && aP !== document && aP.nodeName.toLowerCase() !== "body") {
                if (aL(aP, aQ)) {
                    aN && aN(aO, aP)
                }
                aP = aP.parentNode
            }
        }, false)
    }

    function M(aK, aL) {
        document.addEventListener(aK, function (aM) {
            var aN = aM.target;
            while (aN !== null && aN !== document && aN.nodeName.toLowerCase() !== "html") {
                aL(aN);
                aN = aN.parentNode
            }
        }, false)
    }

    function g(aK, aL, aM) {
        aK.addEventListener(aL, aM, false);
        return {
            remove: function () {
                aK.removeEventListener(aL, aM)
            }
        }
    }

    function Q(aK, aM) {
        var aL = g(aK, "DOMNodeInsertedIntoDocument", function () {
            aM && aM(aK);
            aL.remove()
        })
    }

    function az(aK, aL, aM) {
        return function (aO, aP, aN) {
            aN = aN || false;
            if (window.navigator.msPointerEnabled) {
                aO.addEventListener(aK, aP, aN);
                return {
                    remove: function () {
                        aO.removeEventListener(aK, aP, aN)
                    }
                }
            } else {
                aO.addEventListener(aL, aP, aN);
                if (aG.win32) {
                    aO.addEventListener(aM, aP, aN)
                }
                return {
                    remove: function () {
                        aO.removeEventListener(aL, aP, aN);
                        if (aG.win32) {
                            aO.removeEventListener(aM, aP, aN)
                        }
                    }
                }
            }
        }
    }
    var aa = az("MSPointerDown", "touchstart", "mousedown");
    var X = az("MSPointerMove", "touchmove", "mousemove");
    var W = az("MSPointerUp", "touchend", "mouseup");

    function aA(aK) {
        return function (aM) {
            var aR = q(),
                aQ = q();
            var aO = v(aM),
                aP = w(aM);
            var aN = X(document, function (aS) {
                aO = v(aS);
                aP = w(aS);
                aR.trig(aS, aO, aP)
            });
            var aL = W(document, function (aS) {
                aQ.trig(aS, aO, aP);
                aN.remove();
                aL.remove()
            });
            aM.onTouchMove = aR.regist;
            aM.onTouchEnd = aQ.regist;
            aK(aM, aO, aP)
        }
    }

    function Y(aK, aL) {
        return aa(aK, aA(aL))
    }

    function r(aK) {
        return function (aL) {
            if ("touches" in aL && aL.touches[0] !== undefined) {
                return aL.touches[0][aK]
            } else {
                return aL[aK]
            }
        }
    }
    var v = r("pageX");
    var w = r("pageY");

    function an(aK, aP, aN, aL, aM, aO) {
        return Y(aK, function (aQ, aT, aU) {
            aM && aM(aQ);
            var aS = false;
            var aR = aQ.onTouchMove(function (aV, aW, aX) {
                aV.distanceX = aW - aT;
                aV.distanceY = aX - aU;
                if (aP(aV)) {
                    aR.remove();
                    aS = true;
                    aN(aV, aW, aX)
                }
            });
            aQ.onTouchEnd(function (aV, aW, aX) {
                if (!aS) {
                    aL && aL(aV, aW, aX)
                }
                aO && aO()
            })
        })
    }

    function ao(aK) {
        return aK.distanceX * aK.distanceX + aK.distanceY * aK.distanceY > au * au
    }

    function V(aK, aL, aM) {
        j(aK, "cursor", "pointer");
        return an(aK, ao, function () {
            aK.classList.remove("tap")
        }, function (aN, aO, aP) {
            aL && aL(aN, aO, aP)
        }, function (aN) {
            aK.classList.add("tap");
            if (aM === true) {
                aN.stopPropagation()
            }
        }, function () {
            aK.classList.remove("tap")
        })
    }

    function av(aK) {
        return function (aM, aN, aL) {
            return an(aM, ao, function (aO) {
                var aP = Math.abs(aO.distanceY) / Math.sqrt(aO.distanceX * aO.distanceX + aO.distanceY * aO.distanceY);
                var aQ = (aP >= 0.5) ^ aK;
                if (aQ) {
                    aO.preventDefault();
                    aO.direction = aK ? aO.distanceX > 0 : aO.distanceY > 0;
                    aA(aN)(aO)
                } else {
                    aL.onSenseFailure && aL.onSenseFailure()
                }
            }, aL.onSenseFailure, aL.onSenseStart, null)
        }
    }
    var T = av(true);
    var U = av(false);

    function n(aL) {
        var aM = aL ? T : U;
        var aK = aL ? function (aN) {
                return aN
            } : function (aN, aO) {
                return aO
            };
        return function (aO, aQ, aP, aN) {
            aN = aN || {};
            if (aQ.zLeft === undefined) {
                aD(aQ, 0, 0)
            }
            var aR = aM(aQ, function (aT, aY, aZ) {
                var aS = q();
                var a0 = false;
                var aX = aL ? function (a7) {
                        aD(aQ, a7, 0)
                    } : function (a7) {
                        aD(aQ, 0, a7)
                    };
                var a2 = aK(aY, aZ);
                var a1 = aL ? aQ.zLeft : aQ.zTop;
                var a3 = aT.timeStamp || +new Date();
                var aV = a2;
                var aW = a3;
                var aU = aT.direction;
                var a4 = [],
                    a5 = 0;
                aP({
                    direction: aT.direction,
                    onDragEnd: aS.regist,
                    setMove: function (a7) {
                        aX = a7
                    },
                    preventDefault: function () {
                        a0 = true
                    },
                    targetPos: a1
                });
                if (a0) {
                    return
                }

                function a6(a9, a7) {
                    if (a9 > 200) {
                        a4 = [];
                        a5 = 0;
                        return
                    }
                    var a8;
                    a5 += a9;
                    while (a5 > 300) {
                        a8 = a4.shift();
                        a5 -= a8.timeDiff
                    }
                    a4.push({
                        timeDiff: a9,
                        distDiff: a7
                    })
                }
                aT.onTouchMove(function (ba, bb, bc) {
                    ba.preventDefault();
                    var bd, a7, a8 = aK(bb, bc);
                    var be = (ba.timeStamp || +new Date()) - aW;
                    var a9 = a8 - aV;
                    if (aU === undefined || !(a9 * (aU ? 1 : -1) < -20)) {
                        bd = a1 + a8 - a2;
                        a7 = a8 === aV ? aU : a8 > aV;
                        if (a7 !== aU || be > 200) {
                            a4 = [];
                            a5 = 0
                        } else {
                            a6(be, a9)
                        }
                        aU = a7;
                        aV = a8;
                        aW = ba.timeStamp || +new Date();
                        aX(bd, {
                            direction: a7
                        })
                    }
                });
                aT.onTouchEnd(function (a9, bb, bc) {
                    var a7 = aK(bb, bc);
                    var ba = (a9.timeStamp || +new Date());
                    var be = ba - aW;
                    var a8 = a7 - aV;
                    a6(be, a8);
                    var bf = 0;
                    E(a4, function (bg) {
                        bf += bg.distDiff
                    });
                    var bd = a5 === 0 ? 0 : (bf + a8) / a5;
                    aS.trig({
                        targetPos: a1 + a7 - a2,
                        direction: bd === 0 ? aU : bd > 0,
                        distance: a7 - a2,
                        speed: bd,
                        duration: ba - a3
                    })
                })
            }, aN);
            return {
                remove: aR.remove
            }
        }
    }
    var N = n(true);
    var O = n(false);

    function R(aK, aL) {
        while (aK !== null && aK !== document.body) {
            if (aG.ios && aK.hasAttribute("data-scroll")) {
                function aM() {
                    aL(aK.scrollTop, aK.clientHeight, aK)
                }
                setTimeout(aM, 0);
                return g(aK, "scroll", aM)
            } else {
                if (!aG.ios && aK.hasAttribute("data-page-scroll")) {
                    if (aK.zScroll === undefined) {
                        aK.zScroll = q()
                    }
                    setTimeout(function () {
                        aL(document.body.scrollTop, aK.parentNode.clientHeight, aK.parentNode)
                    }, 0);
                    return aK.zScroll.regist(aL)
                }
            }
            aK = aK.parentNode
        }
    }

    function P(aM, aK) {
        var aL = g(aM, "load", function () {
            aK();
            aL.remove()
        })
    }

    function S(aL, aK) {
        var aM = null;
        g(aL, "DOMSubtreeModified", function () {
            if (aM === null) {
                aM = setTimeout(function () {
                    aK();
                    aM = null
                }, 0)
            }
        })
    }

    function aH(aM, aK) {
        var aN = aM,
            aL = 0;
        aK && G(aK, function (aO, aP) {
            if (aP !== "" && aP !== undefined) {
                aN += aL++ === 0 ? "?" : "&";
                aN += encodeURIComponent(aO) + "=" + encodeURIComponent(aP)
            }
        });
        return aN
    }

    function a(aK) {
        aK = k(aK, {
            arg: {},
            requestHeader: {},
            method: "get",
            data: null
        });
        var aL = new XMLHttpRequest();
        aL.onload = function (aN) {
            var aM = aL.responseText;
            if (aK.isJson) {
                aM = JSON.parse(aM)
            }
            if (aK.dataHandler !== undefined) {
                aM = aK.dataHandler(aM)
            }
            aK.onLoad(aM, aN)
        };
        aL.onerror = aK.onError;
        aL.open(aK.method, aH(aK.url, aK.arg), true);
        G(aK.requestHeader, function (aM, aN) {
            aL.setRequestHeader(aM, aN)
        });
        aL.send(aK.data);
        return aL
    }
    a.xWWW = function (aK) {
        var aM = "",
            aL = 0;
        G(aK, function (aN, aO) {
            if (aL++ !== 0) {
                aM += "&"
            }
            aM += encodeURIComponent(aN);
            aM += "=";
            aM += encodeURIComponent(aO)
        });
        return aM
    };

    function ad(aL) {
        var aK = {};
        ac(aL, "&", "=", function (aM, aN) {
            aK[aM] = aN
        });
        return aK
    }

    function ak(aL, aK) {
        return {
            route: function (aP, aM) {
                var aQ;
                for (var aN = 0, aO = aL.length; aN !== aO; ++aN) {
                    aQ = aL[aN];
                    if (aQ.route(aP, aM)) {
                        return aQ.result
                    }
                }
                return aK
            }
        }
    }

    function aC(aK, aP, aN, aO, aL) {
        return function aM() {
            var aR = false;
            aL = C.String(aN) ? aL : aO;
            j(aK, "transition", aP);
            if (aG.android && aG.androidVersion < 3) {
                aR = true;
                j(aK, aN, aO);
                aL && aL()
            } else {
                j(aK, "transition", aP);

                function aS() {
                    if (!aR) {
                        aR = true;
                        j(aK, "transition", "none", true);
                        aL && aL()
                    }
                }

                function aQ(aT) {
                    aK.addEventListener(aT, aS, false)
                }
                E(["transitionend", "webkitTransitionEnd"], aQ);
                setTimeout(function () {
                    j(aK, aN, aO)
                }, 0)
            }
            return aM
        }
    }

    function aB(aK, aO, aM, aN, aL) {
        return aC(aK, aO, aM, aN, aL)()
    }

    function ax(aN, aK) {
        var aO = null;

        function aL() {
            if (aO !== null) {
                window.clearTimeout(aO)
            }
            aO = null
        }

        function aM() {
            aL();
            aO = window.setTimeout(function () {
                aO = window.setTimeout(arguments.callee, aK);
                aN()
            }, aK)
        }
        aM();
        return {
            start: aM,
            stop: aL
        }
    }
    var ah = function () {
        var aM = null;
        var aL = null,
            aK = null;
        return function (aQ) {
            var aN = false;
            var aO;

            function aP() {
                if (!aN) {
                    aO = {
                        previous: aL,
                        next: null,
                        task: aQ
                    };
                    if (aL !== null) {
                        aL.next = aO
                    } else {
                        aK = aO
                    }
                    aL = aO;
                    if (aM === null) {
                        aM = window.setTimeout(function () {
                            if (aK !== null) {
                                aM = window.setTimeout(arguments.callee, 1000 / 60);
                                for (var aR = aK; aR !== null; aR = aR.next) {
                                    aR.task()
                                }
                            } else {
                                aM = null
                            }
                        }, 1000 / 60)
                    }
                    aN = true
                }
            }
            aP();
            return {
                start: aP,
                stop: function () {
                    if (aN) {
                        if (aO.previous !== null) {
                            aO.previous.next = aO.next
                        } else {
                            aK = aO.next
                        } if (aO.next !== null) {
                            aO.next.previous = aO.previous
                        } else {
                            aL = aO.previous
                        }
                        aN = false
                    }
                }
            }
        }
    }();

    function f(aL, aO, aM, aP) {
        var aN = 0.0001;

        function aK(aV) {
            function aQ(aY) {
                var aW = 1 - aY,
                    aX = aW * aW;
                var aZ = aY * aY,
                    a1 = aZ * aY;
                var a2 = aY * aX,
                    a0 = aZ * aW;
                return [3 * aL * a2 + 3 * aM * a0 + a1, 3 * aO * a2 + 3 * aP * a0 + a1]
            }
            var aR = 0,
                aT = 1;
            var aS = (aT + aR) / 2;
            var aU = aQ(aS)[0];
            while (Math.abs(aV - aU) > aN) {
                if (aV > aU) {
                    aR = aS
                } else {
                    aT = aS
                }
                aS = (aT + aR) / 2;
                aU = aQ(aS)[0]
            }
            return aQ(aS)[1]
        }
        return function (aQ) {
            return function (aR) {
                return aK(aR / aQ)
            }
        }
    }
    var ay = {
        quadraticEase: function (aK) {
            var aL = aK * aK;
            return function (aM) {
                return (aL - Math.pow((aK - aM), 2)) / aL
            }
        },
        ease: f(0.25, 1, 0.25, 1),
        linear: function (aK) {
            return function (aL) {
                return aL / aK
            }
        },
        easeInOut: f(0.42, 0, 0.58, 1)
    };

    function b(aK) {
        var aL = aK.duration * 1000,
            aU = (aK.timing || ay.quadraticEase)(aL),
            aQ = aK.onStart,
            aN = aK.onAnimate,
            aO = aK.onEnd,
            aP = aK.onReverseEnd;
        var aM = true,
            aR = true;
        var aV = aK.wall ? aI() : null;
        aQ && aQ();
        aN(0);
        var aS = new Date();
        var aT = ah(function () {
            var aW = new Date() - aS;
            if (aW >= aL) {
                aT.stop();
                aN(aR ? 1 : 0);
                aV && ag(aV);
                aM = false;
                aR ? aO && aO() : aP && aP()
            } else {
                aN(aU(aR ? aW : aL - aW))
            }
        });
        return s(aT, {
            reverse: function () {
                aR = !aR;
                if (!aM) {
                    aT.start();
                    aM = true;
                    if (aR === true) {
                        aQ && aQ()
                    }
                }
            }
        })
    }
    b.Timing = ay;
    b.Bezier = f;

    function I(aK, aO) {
        var aM = aK.onAnimate,
            aN = aK.startPos,
            aL = aK.endPos - aN;
        return b({
            timing: aK.timing,
            duration: aK.duration,
            onAnimate: function (aP) {
                aM(aN + aL * aP)
            },
            onEnd: aK.onEnd,
            wallTarget: aO,
            noAnimate: aK.noAnimate
        })
    }
    var aD = function (aK, aL, aM, aN) {
        aK.zLeft = aL;
        aK.zTop = aM;
        j(aK, "transform", aE(aL + "px", aM + "px", 0) + (aN || ""))
    };

    function c(aL, aK) {
        return k(aL, s(aK, {
            noAnimate: J
        }))
    }
    var o = function () {
        function aL(aN, aO) {
            return function (aP, aS, aR) {
                var aQ = 0,
                    aT = 0;
                return B(aN(aP, aR), {
                    map: aO ? function (aU) {
                        return aU
                    } : function (aU) {
                        return -aU
                    },
                    display: aw(false, function (aU) {
                        if (aU) {
                            aS.appendChild(aP);
                            aQ = aP.offsetWidth;
                            aT = aS.offsetWidth
                        } else {
                            ag(aP)
                        }
                    }),
                    left: aO ? function (aU) {
                        return aU
                    } : function (aU) {
                        return aT - aQ - aU
                    },
                    drawer: function () {
                        return aP
                    }
                })
            }
        }

        function aM(aN, aP) {
            var aO = 0,
                aQ = null;
            return {
                init: function (aR) {
                    aD(aN, aR(0), 0)
                },
                start: function () {
                    aO = aN.offsetWidth;
                    aQ = H(aN, 1);
                    j(aQ, "background", "black")
                },
                animate: function (aS, aR) {
                    aS = af(aS, 0, aO);
                    j(aQ, "opacity", 0.8 - aS / aO * 0.8);
                    aD(aN, aR(0), 0, al(0.8 + aS / aO * 0.2))
                },
                end: function () {
                    ag(aQ);
                    aQ = null
                },
                mainMove: function (aR) {
                    aD(aP, aR, 0)
                }
            }
        }

        function aK(aT, aN) {
            aN = c(aN, {
                effect: aM,
                canSwipe: true,
                duration: 0.32
            });
            var aS = aT.querySelector(".drawer-panel-main"),
                aR = aT.querySelector(".drawer-panel-left"),
                aU = aT.querySelector(".drawer-panel-right");
            var aZ = null;
            var aX = q(),
                aY = q(),
                aW = q();
            E([aS, aR, aU], function (a0) {
                j(a0, {
                    position: "absolute",
                    "z-index": 0,
                    height: "100%"
                });
                aD(a0, 0, 0)
            });
            j(aS, {
                "z-index": 2,
                left: 0,
                right: 0
            });
            var aO = null;
            var aP = null;

            function aQ(a0) {
                var a1 = a0.revealer,
                    a2 = a0.isOpen;
                return function (a5) {
                    a5 = k(a5, {
                        duration: aN.duration
                    });
                    a1.display(true);
                    a1.init(a1.left);
                    var a3 = a0.offsetWidth;
                    var a6 = !a2();

                    function a4() {
                        a2(a6);
                        a1.display(a6);
                        a5.onEnd && a5.onEnd()
                    }
                    if (aN.noAnimate) {
                        j(a1.drawer(), "top", document.body.scrollTop + "px");
                        aC(aS, "ease-in-out all " + aN.duration + "s", "transform", aE((a6 ? a1.map(a3) : 0) + "px", 0, 0), a4)();
                        aS.zLeft = a6 ? a1.map(a3) : 0;
                        aS.zTop = document.body.scrollTop
                    } else {
                        a1.start(a1.left);
                        I({
                            timing: b.Bezier(0.455, 0.03, 0.515, 0.955),
                            startPos: a6 ? 0 : a3,
                            endPos: a6 ? a3 : 0,
                            onAnimate: function (a7) {
                                a1.animate(a7, a1.left);
                                a1.mainMove(a1.map(a7))
                            },
                            onEnd: function () {
                                a1.end();
                                a4()
                            },
                            duration: a5.duration
                        }, aT)
                    }
                }
            }
            E([aR, aU], function (a0) {
                var a1 = q();
                a0.isOpen = aw(false, function (a2) {
                    a1.trig(a2);
                    aO = a2 === true ? a0 : null;
                    if (a2) {
                        aZ = H(aS, 10000);
                        Y(aZ, a0.toggle);
                        aS.classList.add("open")
                    } else {
                        ag(aZ)
                    }
                });
                a0.revealer = aL(aN.effect, a0 === aR)(a0, aT, aS);
                a0.toggle = aQ(a0);
                a0.onToggle = a1.regist;
                ag(a0)
            });

            function aV() {
                if (aN.noAnimate) {
                    return
                }
                aP = N(aT, aS, function (a1) {
                    var a2 = a1.direction;
                    var a3 = aO === null ? (a2 ? aR : aU) : aO;
                    var a4 = a3.revealer,
                        a5 = a3.isOpen;
                    a4.display(true);
                    a4.init(a4.left);
                    var a0 = a3.offsetWidth;
                    if (a0 === 0) {
                        a1.preventDefault()
                    }
                    a4.start(a4.left);
                    a4.animate(a4.map(a1.targetPos), a4.left);
                    aX.trig();
                    a1.setMove(function (a7) {
                        var a6 = af(a7, 0, a4.map(a0));
                        a4.animate(a4.map(a6), a4.left);
                        a4.mainMove(a6)
                    });
                    a1.onDragEnd(function (a6) {
                        var a8 = af(a6.targetPos, 0, a4.map(a0));
                        var a9 = a4.map(a8) / a0;
                        var a7 = (a8 <= 0) ^ a6.direction ? a9 > 0.15 : a9 > 0.85;
                        I({
                            timing: b.Bezier(0.455, 0.03, 0.515, 0.955),
                            startPos: a4.map(a8),
                            endPos: a7 ? a0 : 0,
                            onAnimate: function (ba) {
                                a4.animate(ba, a4.left);
                                a4.mainMove(a4.map(ba))
                            },
                            onEnd: function () {
                                a4.end();
                                a4.display(a7);
                                a5(a7);
                                aW.trig()
                            },
                            duration: aN.duration
                        }, aT);
                        aY.trig()
                    })
                })
            }
            if (aN.canSwipe) {
                aV()
            }
            return {
                leftDrawer: function () {
                    return aR
                },
                rightDrawer: function () {
                    return aU
                },
                onSwipeStart: aX.regist,
                onSwipeUp: aY.regist,
                onSwipeEnd: aW.regist,
                disableSwipe: aw(false, function (a0) {
                    if (aN.canSwipe) {
                        if (a0) {
                            aP.remove()
                        } else {
                            aV()
                        }
                    }
                })
            }
        }
        aK.effect = {
            basic: aM
        };
        return aK
    }();
    var am = function () {
        var aL = {
            vertical: 0,
            horizontal: 1
        };

        function aK(a5, aM) {
            aM = k(aM, {
                rollbackDuration: 0.35,
                inertial: 20,
                topBlank: -1,
                bottomBlank: -1,
                useBrowserScroll: false,
                scrollBarBackground: "rgba(64,64,64,0.5)",
                scrollType: aL.vertical
            });
            var bq = aM.useScrollBar || aM.scrollType === aL.vertical;
            var a3 = aM.scrollType === aL.horizontal ? function (br, bs) {
                    aD(br, bs, 0)
                } : function (br, bs) {
                    aD(br, 0, bs)
                };

            function aY(br) {
                return br.offsetHeight
            }

            function aZ(br) {
                return br.offsetWidth
            }
            var a1 = aM.scrollType === aL.horizontal;
            var aW = a1 ? N : O;
            var a2 = a1 ? aZ : aY,
                bo = a1 ? aY : aZ;
            var bi = q();
            var bd = q();
            var bk = q();
            var bh = q();
            var a0 = false;
            j(a5, "overflow", "hidden");

            function aO() {
                var br = document.createElement("div");
                j(br, {
                    position: "absolute",
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0,
                    visibility: "hidden"
                });
                B(br, {
                    visible: aw(false, function (bs) {
                        j(br, "visibility", bs ? "visible" : "hidden")
                    })
                });
                return br
            }
            var bp = aO();
            var aQ = aO();
            a5.appendChild(bp);
            a5.appendChild(aQ);
            var aR = a5.querySelector(".scroll-content");
            j(aR, {
                left: "0",
                right: "0",
                position: "absolute",
                top: "0",
                "z-index": "2",
                "min-height": "100%",
                "min-width": "100%"
            });
            aD(aR, 0, 0);
            var aT = 0;
            var a9 = document.createElement("div");
            if (bq) {
                j(a9, {
                    "z-index": "10000",
                    width: "4px",
                    position: "absolute",
                    background: aM.scrollBarBackground,
                    "border-radius": "3px",
                    opacity: 0
                });
                aD(a9, a5.offsetWidth - 8, 0);
                a5.appendChild(a9)
            }

            function bm() {
                if (bq && ba > 0) {
                    aD(a9, a7 - 8, af((-aT) / aS * a6, -ba + 5, a6 - 5))
                }
            }

            function bn(br) {
                if (aM.topBlank !== -1) {
                    br = Math.min(aM.topBlank, br)
                }
                if (aM.bottomBlank !== -1) {
                    br = Math.max(aP - aM.topBlank, br)
                }
                aT = br;
                a3(aR, br);
                bm();
                bp.visible(br > 0);
                aQ.visible(br < aP);
                if (br > 0) {
                    a3(bp, br - a6)
                }
                if (br < aP) {
                    a3(aQ, br + aS)
                }
            }
            var bj = null;
            var aV = null;
            if (aM.useBrowserScroll) {
                j("content", "auto");
                return
            }
            var a6 = 0,
                aS = 0,
                ba = 0,
                a7 = 0,
                aP = 0;
            var bb = 1;
            var bc = ah(function () {
                if (bb > 0) {
                    bb -= 0.04;
                    j(a9, "opacity", bb + "")
                } else {
                    j(a9, "opacity", 0);
                    bc.stop()
                }
            });

            function aU(br) {
                if (br) {
                    bc.stop();
                    bb = 1;
                    j(a9, "opacity", 1)
                } else {
                    bc.start()
                }
            }

            function aX() {
                bj = null;
                bd.trig();
                aU(0)
            }

            function a4(bt, bs) {
                var br = bt > aT;
                bj = I({
                    startPos: aT,
                    endPos: bt,
                    duration: bs,
                    onAnimate: function (bu) {
                        bn(bu);
                        bh.trig({
                            scrollTop: -aT,
                            direction: br
                        })
                    },
                    onEnd: function () {
                        bn(bt);
                        aX()
                    }
                })
            }

            function a8() {
                a6 = a2(a5);
                a7 = bo(a5);
                aS = a2(aR);
                aP = a6 - aS;
                var br = a6 / aS;
                if (br > 1) {
                    j(a9, "height", 0);
                    ba = 0
                } else {
                    ba = br * a6;
                    j(a9, "height", ba + "px")
                }
            }

            function aN(br) {
                if (aM.topBlank !== -1 && br >= aM.topBlank) {
                    return true
                } else {
                    if (aM.bottomBlank !== -1 && -br + aP >= aM.bottomBlank) {
                        return true
                    }
                }
                return false
            }

            function be(br) {
                return aw(false, function (bs) {
                    if (bs) {
                        aR.classList.add(br)
                    } else {
                        aR.classList.remove(br)
                    }
                })
            }
            var bg = be("scroll-limit-top");
            var bf = be("scroll-limit-bottom");

            function bl() {
                aV = aW(a5, aR, function (br) {
                    a8();
                    a0 = true;
                    if (bj !== null) {
                        bj.stop();
                        bj = null
                    }
                    bi.trig({
                        scrollTop: -aT
                    });
                    aU(true);
                    br.setMove(function (bu, bt) {
                        var bs = bt.direction;
                        bn(bu);
                        bg(bs === l.positive && aT >= 0);
                        bf(bs === l.negative && aT <= aP);
                        bh.trig({
                            scrollTop: -aT,
                            direction: bs
                        })
                    });
                    br.onDragEnd(function (bs) {
                        var bu = bs.speed * aM.inertial;
                        bg(false);
                        bf(false);
                        if (aT > 0) {
                            a4(0, aM.rollbackDuration)
                        } else {
                            if (aT < aP) {
                                a4(aP, aM.rollbackDuration)
                            } else {
                                if (Math.abs(bu) > 3) {
                                    var bt = aT;
                                    bj = ah(function () {
                                        if (Math.abs(bu) < 0.8 || aN(aT)) {
                                            bj.stop();
                                            if (aT > 0) {
                                                a4(0, aM.rollbackDuration)
                                            } else {
                                                if (aT < aP) {
                                                    a4(aP, aM.rollbackDuration)
                                                } else {
                                                    aX()
                                                }
                                            }
                                            return
                                        }
                                        bt += bu;
                                        if (!A(bt, aP, 0)) {
                                            bu = bu * 0.5
                                        } else {
                                            bu = bu * 0.95
                                        }
                                        bn(bt);
                                        bh.trig({
                                            scrollTop: -aT,
                                            direction: bs.direction
                                        })
                                    });
                                    bj.start()
                                } else {
                                    bd.trig({
                                        scrollTop: -aT
                                    });
                                    aU(false);
                                    return
                                }
                            }
                        }
                        bk.trig({
                            scrollTop: aT,
                            stopScrollBack: bj.stop,
                            startScrollBack: bj.start,
                            removeScrollBack: function () {
                                bj.stop();
                                aU(false)
                            }
                        })
                    })
                }, {
                    onSenseStart: function () {
                        if (bj !== null) {
                            bj.stop();
                            if (aT <= 0 && aT + aS >= a6) {
                                bj = null;
                                bd.trig({
                                    scrollTop: -aT
                                })
                            }
                            aU(false)
                        }
                    },
                    onSenseFailure: function () {
                        if (bj !== null) {
                            bj.start()
                        }
                    }
                })
            }
            bl();
            a8();
            return {
                onScrollStart: bi.regist,
                onScrollEnd: bd.regist,
                onScrollMove: bh.regist,
                onScrollUp: bk.regist,
                disableScroll: aw(false, function (br) {
                    if (br) {
                        aV.remove();
                        aV = null
                    } else {
                        bl()
                    }
                }),
                scrollTop: function (br) {
                    if (br === undefined) {
                        return -aT
                    } else {
                        bn(-br)
                    }
                },
                scrollSize: function () {
                    return a2(aR)
                },
                contentSize: function () {
                    return a2(a5)
                },
                refresh: function () {
                    a8();
                    bn(aT);
                    if (bj !== null) {
                        bj.stop();
                        if (aT > 0) {
                            a4(0, aM.rollbackDuration)
                        } else {
                            if (aT < aP) {
                                a4(aP, aM.rollbackDuration)
                            } else {
                                bj = null;
                                bd.trig();
                                aU(0)
                            }
                        }
                    }
                },
                clear: function (br) {
                    if (bj !== null) {
                        bj.stop()
                    }
                    a8();
                    bn(br === undefined ? 0 : -br);
                    j(a9, "opacity", 0)
                },
                stopScroll: function () {
                    if (bj !== null) {
                        bj.stop()
                    }
                    j(a9, "opacity", 0)
                },
                topBlank: function () {
                    return bp
                },
                bottomBlank: function () {
                    return aQ
                }
            }
        }
        aK.direction = l;
        aK.scrollType = aL;
        return aK
    }();
    var ab = function () {
        var aK = {
            SlideIn: function (aO, aN) {
                var aP = aO.offsetWidth;
                return {
                    lay: function (aQ) {
                        aD(aN, aQ ? aP : 0, 0)
                    },
                    onCut: function (aQ) {
                        aD(aN, aP - aQ * aP, 0)
                    },
                    timing: ay.quadraticEase,
                    transition: function (aR, aQ, aS) {
                        return aC(aN, "all ease-in-out " + aQ + "s", "transform", aE(aR ? 0 : aP + "px", 0, 0), aS)
                    }
                }
            },
            FadeIn: function (aO, aN) {
                return {
                    lay: function (aP) {
                        j(aN, "opacity", aP ? 0 : 1)
                    },
                    onCut: function (aP) {
                        j(aN, "opacity", aP + "")
                    },
                    timing: ay.ease,
                    transition: function (aQ, aP, aR) {
                        return aC(aN, "all " + aP + "s", "opacity", aQ ? 1 : 0, aR)
                    }
                }
            }
        };
        var aL = {
            normal: 0,
            reverse: 1,
            noEffect: 2
        };

        function aM(aN, aQ) {
            aQ = c(aQ, {
                effect: aK.SlideIn,
                duration: 0.2
            });
            var aS = aQ.effect;
            var aR = aQ.duration;
            var aU = aQ.noAnimate;

            function aV(aX) {
                j(aX, {
                    position: "absolute",
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: 0
                });
                aD(aX, 0, 0);
                if (!aG.ios) {
                    aX.setAttribute("data-page-scroll", "")
                }
            }
            var aT = aN.querySelector(".main-page");
            aV(aT);
            var aW = {};
            F(aN.querySelectorAll(".sub-page"), function (aX) {
                aV(aX);
                aW[aX.getAttribute("id")] = aX;
                aX.removeAttribute("id");
                ag(aX)
            });
            var aO = aT;
            if (!aG.ios) {
                g(window, "scroll", function () {
                    if (aO !== null) {
                        aO.zScroll && aO.zScroll.trig(document.body.scrollTop, aN.clientHeight, aN)
                    }
                })
            }

            function aP(aY, aZ, a2) {
                var a3 = aO;
                aO = a2;
                if (a2 === a3) {
                    return
                }
                aZ = k(aZ, {
                    effect: aS,
                    duration: aR
                });
                var a1 = aY === aL.normal;
                j(a2, {
                    "z-index": a1 ? 1 : -1
                });
                document.body.classList.add("cutting");
                if (aG.ios) {
                    j(a2, {
                        visibility: "hidden"
                    });
                    aN.appendChild(a2);
                    F(a3.querySelectorAll("[data-scroll]"), function (a5) {
                        a5.pScrollTop = a5.scrollTop
                    });
                    F(a2.querySelectorAll("[data-scroll]"), function (a5) {
                        if (a5.pScrollTop !== undefined && a5.pScrollTop !== a5.scrollTop) {
                            a5.scrollTop = a5.pScrollTop
                        }
                    })
                } else {
                    document.body.appendChild(a2);
                    a3.pScrollTop = document.body.scrollTop;
                    j(a2, {
                        position: "fixed",
                        top: (-a2.pScrollTop || 0) + "px"
                    })
                }
                var aX = aZ.effect(aN, a1 ? a2 : a3);
                var a4 = aX.onCut;
                aX.lay && aX.lay(a1);

                function a0() {
                    aX.clear && aX.clear(a1);
                    ag(a3);
                    j(a2, {
                        "z-index": 1
                    });
                    document.body.classList.remove("cutting");
                    if (!aG.ios) {
                        aN.appendChild(a2);
                        j(a2, {
                            position: "absolute",
                            top: 0
                        });
                        document.body.scrollTop = a2.pScrollTop
                    }
                }
                setTimeout(function () {
                    j(a2, {
                        visibility: "visible"
                    });
                    if (aY === aL.noEffect) {
                        a0()
                    } else {
                        if (aU) {
                            aX.transition(a1, aZ.duration, a0)()
                        } else {
                            b({
                                timing: aX.timing,
                                onAnimate: a1 ? a4 : function (a5) {
                                    a4(1 - a5)
                                },
                                onEnd: a0,
                                duration: aZ.duration,
                                wallTarget: document.body
                            })
                        }
                    }
                }, 0)
            }
            B(aN, {
                curPage: function () {
                    return aO
                },
                mainPage: function () {
                    return aT
                },
                displayMain: function (aX) {
                    aP(aL.reverse, aX, aT)
                },
                popupPage: function (aZ, aX) {
                    aX = aX || {};
                    var aY = aW[aZ].cloneNode(true);

                    function a0(a1) {
                        aP(a1.cutinMode || aL.normal, a1, aY)
                    }
                    a0(aX);
                    return B(aY, {
                        display: a0,
                        isDisplayed: function () {
                            return aO === aY
                        }
                    })
                }
            });
            return aN
        }
        aM.CutEffect = aK;
        aM.CutinMode = aL;
        return aM
    }();

    function aq(a1, aL) {
        var a2 = a1.offsetWidth;
        aL = k(aL, {
            duration: 0.3
        });
        if (aG.win32) {
            Y(a1, function (a8) {
                a8.preventDefault()
            })
        }
        var aW = [];
        var aM = 0;
        var a7 = a1.querySelector("ul");
        var a5 = q();
        var a3 = q();
        var a4 = q();
        var a6 = q();
        var aO = q();
        var aK = q();
        var aN = q();
        j(a7, {
            height: "100%",
            margin: "0",
            padding: "0",
            position: "relative"
        });
        var aY = a7.querySelectorAll("li");
        var aV = aY.length;
        j(a1, "overflow", "hidden");
        j(a7, "width", (aY.length + 2) * a2 + "px");
        F(aY, function (a8) {
            j(a8, {
                height: "100%",
                width: a2 + "px",
                display: "block",
                "text-style": "none",
                "float": "left"
            });
            aW.push(a8)
        });
        var aU = false;
        var aT = aY.item(0),
            aX = aY.item(aV - 1);
        if (aV !== 1) {
            aD(a7, -a2, 0);
            a7.insertBefore(aX.cloneNode(true), aT);
            S(aX, function () {
                a7.replaceChild(aX.cloneNode(true), a7.querySelector("li:first-child"))
            });
            a7.appendChild(aT.cloneNode(true));
            S(aT, function () {
                a7.replaceChild(aT.cloneNode(true), a7.querySelector("li:last-child"))
            })
        }

        function aZ(bc, a8, bb) {
            var bd = bb ? 0.5 : 0.35;
            var a9 = a8 === undefined ? 0 : (a8 ? 1 : -1) * a2 * bd;
            var be = bc + a9;
            var ba = Math.floor(be / a2);
            return ba + (be - ba * a2 >= a2 / 2 ? 1 : 0)
        }
        var a0 = false;

        function aS(a9, ba, a8) {
            aU = true;
            var bb = (-a9 + aV - 1) % aV;
            aK.trig({
                targetItem: bb
            });
            a0 = true;
            aB(a7, a8 || aL.duration + "s ease-out", {
                transform: aE(a9 * a2 + "px", 0, 0)
            }, function () {
                a0 = false;
                if (bb === 0 || bb === aV - 1) {
                    aD(a7, (-bb - 1) * a2, 0, 0)
                } else {
                    a7.zLeft = a9 * a2
                }
                aM = bb;
                aN.trig({
                    curItem: aM
                });
                ba && ba();
                aU = false
            })
        }
        N(a1, a1, function (a8) {
            if (aW.length <= 1 || aU) {
                a8.preventDefault();
                return
            }
            var a9 = a7.zLeft;
            aO.trig();
            a5.trig();
            a8.setMove(function (bb) {
                var ba = af(bb, -a2, a2);
                aD(a7, a9 + ba, 0);
                a4.trig({
                    progress: ba
                })
            });
            a8.onDragEnd(function (ba) {
                var bb = af(ba.targetPos, -a2, a2);
                var bc = aZ(a9 + bb, ba.distance > 0, ba.duration < 100);
                a6.trig();
                aS(bc, a3.trig)
            })
        }, {
            preventDefault: true
        });

        function aR(a8) {
            if (aV !== 1) {
                aO.trig();
                aK.trig({
                    targetItem: a8
                });
                aD(a7, (a8 + 1) * -a2, 0);
                aM = a8;
                aN.trig({
                    curItem: aM
                })
            }
        }

        function aP(a8) {
            if (aW.length > 1 && !a0) {
                aO.trig();
                aS(-a8 - 1)
            }
        }

        function aQ(a8) {
            return function () {
                aP(aM + a8)
            }
        }
        setTimeout(function () {
            aR(aM)
        }, 0);
        B(a1, {
            onSwipeStart: a5.regist,
            onSwipeUp: a6.regist,
            onSwipeMove: a4.regist,
            onSwipeEnd: a3.regist,
            onCutStart: aO.regist,
            onCutEnd: aN.regist,
            onAnimateStart: aK.regist,
            cutLeft: aQ(1),
            cutRight: aQ(-1),
            cutTo: aP,
            display: aR,
            length: function () {
                return aW.length
            },
            item: function (a8) {
                return aW[(a8 + aW.length) % aW.length]
            }
        });
        return a1
    }

    function ar(aK) {
        return {
            str: aK,
            cur: 0
        }
    }

    function m(aN) {
        aN.str += ")";

        function aK() {
            return aN.str.charAt(aN.cur)
        }

        function aM() {
            ++aN.cur
        }

        function aL() {
            function aS() {
                var aV = "";
                var aU = aK();
                while (A(aU.charCodeAt(0), 48, 58) || A(aU.toUpperCase().charCodeAt(0), 65, 91)) {
                    aV += aU;
                    aM();
                    aU = aK()
                }
                return aG[aV]
            }

            function aR() {
                var aU = aK();
                aM();
                aM();
                return aU
            }

            function aP() {
                var aU;
                switch (aK()) {
                case "!":
                    aM();
                    return !aP();
                case "(":
                    aM();
                    aU = aL();
                    aM();
                    return aU;
                default:
                    return aS()
                }
            }
            var aO = aP();
            while (aK() !== ")") {
                var aQ = aR();
                var aT = aP();
                if (aQ === "&") {
                    aO = aO && aT
                } else {
                    aO = aO || aT
                }
            }
            return aO
        }
        return aL()
    }
    F(document.querySelectorAll("[data-ua-style]"), function (aK) {
        if (!m(ar(aK.getAttribute("data-ua-style").replace(" ", "")))) {
            ag(aK)
        }
    });
    window.Z = window.Z || {};
    B(window.Z, {
        Direction: l,
        ua: aG,
        is: C,
        defaultArg: k,
        loopArray: E,
        loopNodeList: F,
        loopObj: G,
        loop: D,
        insert: B,
        extend: s,
        TupleString: aF,
        styleString: at,
        parsePairString: ac,
        Switcher: aw,
        Checker: i,
        range: af,
        Event: q,
        bindEvent: g,
        on: L,
        onBubble: M,
        onInsert: Q,
        onTouchStart: Y,
        onTouchMove: X,
        onTouchEnd: W,
        onDragH: N,
        onDragV: O,
        onSwipeStartH: T,
        onSwipeStartV: U,
        onTap: V,
        onScroll: R,
        onImgLoad: P,
        onSubtreeModified: S,
        querySonChild: ae,
        pageLeft: t,
        pageTop: u,
        removeNode: ag,
        element: p,
        HTMLTemplate: y,
        appendChildren: e,
        css: j,
        translate: aD,
        Transition: aC,
        transition: aB,
        Timer: ax,
        requestAnimate: ah,
        animate: b,
        DrawerPanel: o,
        ScrollPanel: am,
        PagePanel: ab,
        SimpleSlideListPanel: aq,
        url: aH,
        parseSearchString: ad,
        ajax: a,
        Router: ak,
        zMode: aw(window.zMode === true, function (aK) {
            aJ = aK;
            z = !aJ && !aG.ios && !aG.win32;
            au = z ? 5 : 20;
            x = z ? 0.8 : 0.5
        }, true),
        noAnimate: aw(window.noAnimate === true, function (aK) {
            J = aK
        }, true),
        onLoad: function (aK) {
            window.addEventListener("load", aK, false)
        }
    });
    Z.onLoad(function () {
        M("focusin", function (aK) {
            aK.classList.add("focus")
        });
        M("focusout", function (aK) {
            aK.classList.remove("focus")
        })
    })
})();
;

(function () {
    var b = Z.Checker,
        j = Z.onTouchStart,
        m = Array.prototype.slice,
        c = Z.css,
        n = Z.Transition,
        f = Z.insert,
        l = Z.removeNode;
    var e = false;
    var a = b(1);

    function g(o) {
        return function (r) {
            var p = m.call(arguments, 1);
            if (!e) {
                e = true;
                window.bdmapInit = function () {
                    a.check();
                    delete window.bdmapInit
                };
                var q = document.createElement("script");
                q.src = "http://api.map.baidu.com/api?v=1.3&callback=bdmapInit";
                document.body.appendChild(q)
            }
            a.onDone(function () {
                r(o.apply(null, p))
            })
        }
    }
    var i = g(function (s, p, o) {
        var r = document.createElement("div");
        c(s, "visibility", "hidden");
        c(r, {
            height: "100%",
            width: "100%"
        });
        s.appendChild(r);
        j(s, function (u) {
            u.stopPropagation()
        });
        var q = new BMap.Map(r);
        var t = [];
        Z.loopArray(o, function (v) {
            var x = new BMap.Point(parseFloat(v.lng), parseFloat(v.lat));
            var w = new BMap.Marker(x);
            q.addOverlay(w);
            t.push(x);
            if (v.hasOwnProperty("name")) {
                var u = new BMap.InfoWindow(p(v));
                w.addEventListener("click", function () {
                    w.openInfoWindow(u)
                })
            }
        });
        if (t.length !== 0) {
            q.centerAndZoom(t[0], 16);
            q.setViewport(t)
        } else {
            q.centerAndZoom("北京市")
        }
        c(s, "visibility", "visible")
    });
    var d = g(function (o) {
        navigator.geolocation.getCurrentPosition(function (q) {
            var p = new BMap.Geocoder();
            p.getLocation(new BMap.Point(q.coords.longitude, q.coords.latitude), o)
        }, function () {}, {
            timeout: 2000,
            maximumAge: 600000,
            enableHighAccuracy: true
        })
    });

    function h(o) {
        var p = "";
        p += "http://api.map.baidu.com/marker?location=";
        p += o.lat + ",";
        p += o.lng + "&title=";
        p += o.address;
        p += "&content=&output=html";
        return p
    }

    function k(o, q) {
        var p = document.createElement("div");
        document.body.appendChild(p);
        c(p, {
            transform: "translate3d(100%,0,0)",
            background: "white",
            position: "fixed",
            "z-index": 1,
            left: 0,
            right: 0,
            top: 0,
            bottom: 0
        });
        n(p, "0.2s", {
            transform: "translate3d(0,0,0)"
        }, function () {
            setTimeout(function () {
                Z.lbs.markerMap(function () {
                    q && q()
                }, p, function (r) {
                    var t = document.createElement("div");
                    var u = document.createElement("div");
                    c(u, {
                        color: "#0085EA",
                        "font-size": "16px",
                        "font-weight": "bold",
                        "line-height": "16px",
                        "padding-bottom": "10px",
                        overflow: "hidden",
                        "white-space": "nowrap",
                        "text-overflow": "ellipsis",
                        width: "220px",
                        "margin-bottom": 0
                    });
                    u.textContent = r.name;
                    t.appendChild(u);

                    function s(v, x) {
                        var w = document.createElement("div");
                        c(w, {
                            "font-size": "12px",
                            "line-height": "16px",
                            color: "#52525F",
                            "vertical-align": "top"
                        });
                        var y = document.createElement("span");
                        y.textContent = v;
                        w.appendChild(y);
                        var z = document.createElement("span");
                        c(z, {
                            "margin-left": "5px",
                            display: "inline-block",
                            width: "184px"
                        });
                        z.textContent = x;
                        w.appendChild(z);
                        t.appendChild(w)
                    }
                    s("地址", r.address);
                    s("电话", r.telephone);
                    return t
                }, o)
            }, 500)
        })();
        f(p, {
            remove: function () {
                n(p, "0.2s", {
                    transform: "translate3d(100%,0,0)"
                }, function () {
                    l(p)
                })()
            }
        });
        return p
    }
    Z.lbs = {
        markerMap: i,
        mapLink: h,
        popupMakerMap: k,
        getPosition: function (o) {
            d(function () {}, o)
        }
    }
})();
;

(function () {
    window.cloud7 = window.cloud7 || {};
    var g = Z.PagePanel.CutinMode,
        p = Z.loopObj,
        o = Z.loopNodeList,
        j = Z.Event,
        G = Z.url,
        m = Z.insert,
        k = Z.extend,
        u = Z.onTap,
        q = Z.on,
        f = Z.css,
        B = Z.transition,
        x = Z.removeNode,
        C = Z.translate,
        n = Z.loopArray,
        s = Z.onInsert,
        v = Z.onTouchStart,
        F = Z.ua,
        d = Z.Checker,
        E = Z.TupleString,
        i = Z.element,
        t = Z.onScroll,
        r = Z.onImgLoad,
        w = Z.pageTop,
        b = Z.bindEvent;
    var D = E("translate3d");
    (function () {
        var H = function () {
            var I = window.WeixinJSBridge;
            I.on("menu:share:appmessage", function () {
                var J = window.dataForWeixin;
                I.invoke("sendAppMessage", {
                    appid: J.appId,
                    link: J.url,
                    desc: J.desc,
                    title: J.title
                }, function (K) {})
            });
            I.on("menu:share:timeline", function () {
                var J = window.dataForWeixin;
                I.invoke("shareTimeline", {
                    link: J.url,
                    desc: J.desc,
                    title: J.title
                }, function (K) {})
            })
        };
        if (document.addEventListener) {
            document.addEventListener("WeixinJSBridgeReady", H, false)
        } else {
            if (document.attachEvent) {
                document.attachEvent("WeixinJSBridgeReady", H);
                document.attachEvent("onWeixinJSBridgeReady", H)
            }
        }
    }());

    function h(J, I, H) {
        J.classList.add("cloud7-img-wrapper");
        f(J, {
            width: I + "px",
            height: H + "px"
        })
    }

    function c(K, N, J, H, I, L) {
        I && h(K.parentNode, J, H);

        function M(O, P) {
            f(K, {
                position: "absolute",
                left: O + "px",
                top: P + "px",
                display: "block"
            })
        }
        r(K, function () {
            var O = J / H;
            var R = K.naturalWidth || K.width,
                P = K.naturalHeight || K.height,
                Q = R / P;
            if (O > Q) {
                f(K, "width", J + "px");
                M(0, (H - J / R * P) / 2 << 0)
            } else {
                f(K, "height", H + "px");
                M((J - H / P * R) / 2 << 0, 0)
            }
            K.parentNode.classList.add("load-done");
            L && L()
        });
        K.src = N
    }

    function z(M, I, K, L, H) {
        h(M, L, H);
        M.classList.add("loading");
        var J = t(M, function (P, N, O) {
            var Q = w(M) - w(O);

            if (Q >= P - H && Q < P + N) {
                J.remove();
                f(I, "opacity", 0);
                c(I, K, L, H, false, function () {
                    B(I, "0.2s 0.2s", "opacity", 1);
                    M.classList.remove("loading")
                })
            }
        })
    }

    function a(I, H) {
        var J = document.createElement("div");
        J.classList.add("wall");
        I.appendChild(J);
        H && v(J, H)
    }

    function y(H) {
        var I = H.querySelector(".wall");
        I !== null && x(I)
    }

    function l(I, H) {
        return i("div", {
            classList: H,
            innerHTML: I
        })
    }
    var A = function (I) {
        var H = document.createElement("a");
        H.href = I;
        return H.href
    };

    function e(H) {
        var aW = H.welcomePage,
            ah = H.layout,
            aV = H.template,
            ay = H.pagePanel,
            aF = H.prepareData || [],
            am = d(aF.length + 2);
        var ab = null,
            aU = null,
            V = null;
        var aR = H.siteTitle,
            aP = H.siteDescription,
            aQ = H.siteLogo,
            az = H.pageSize;
        var P = undefined,
            O = undefined,
            Q = undefined;
        var aK;
        var L = {}, J = {}, S = {};
        var M = null;
        n(aF.concat("index"), function (aX) {
            switch (aX) {
            case "index":
                cloud7.getIndex(function (aY) {
                    ab = aY;
                    am.check()
                });
                break;
            case "subbranchCityList":
                cloud7.getSubbranchCityList(function (aY) {
                    aU = aY;
                    am.check()
                });
                break;
            case "goodCategoryList":
                cloud7.getGoodCategoryList(function (aY) {
                    V = aY;
                    am.check()
                });
                break
            }
        });

        function aL(aX) {
            var aY = aX.title;
            document.title = aY;
            window.dataForWeixin = {
                appId: "",
                picture: aX.picture === undefined ? aQ === undefined ? undefined : A(aQ) : A(aX.picture),
                url: location.href,
                desc: aX.description || aP || aY,
                title: aY
            }
        }

        function aM(aY, aX) {
            aY.shareData = aX;
            aL(aX)
        }
        var aC = j(),
            X = 0,
            Y = [];
        window.onpopstate = function (aX) {
            if (M !== null) {
                M.slideOut(true)
            }
            var aY = aX.state === null ? 0 : aX.state.count;
            if (Y.length === 0) {
                if (aX.state !== null) {
                    X = aY
                } else {
                    X = sessionStorage.getItem("history") === null ? 0 : parseInt(sessionStorage.getItem("history"), 10)
                }
                Y[X] = true;
                aC.trig({
                    isNew: true,
                    popState: "refresh"
                })
            } else {
                aC.trig({
                    isNew: Y[aY] !== true,
                    popState: aY < X ? "back" : "forward"
                });
                Y[aY] = true;
                X = aY
            }
            sessionStorage.setItem("history", X)
        };

        function aG(aY, aX) {
            window.history.pushState({
                count: ++X,
                title: document.title
            }, document.title, arguments.length === 1 ? aY : G(aY, aX));
            Y[X] = true;
            sessionStorage.setItem("history", X)
        }

        function aJ(aY, aX) {
            window.history.replaceState({
                count: X,
                title: document.title
            }, document.title, arguments.length === 1 ? aY : G(aY, aX))
        }
        var W = {
            pushState: aG,
            replaceState: aJ,
            onPopState: aC.regist,
            isRoot: function () {
                return X === 0
            },
            back: function () {
                window.history.back()
            },
            forward: function () {
                window.history.forward()
            },
            go: function (aX) {
                window.history.go(aX)
            }
        };
        var ae = j();

        function af(a3, aZ, a2, a0) {
            var a4 = a3.path,
                aY = a3.arg;
            var aX = aK.route(a4, aY);
            var a1 = aX(a4, aY, aZ || {}, a2) === true;
            ae.trig({
                name: aX.routeName
            });
            if (a0) {
                (a1 ? aJ : aG)(a4, aY)
            }
            aL(ay.curPage().shareData || {
                title: aR
            })
        }

        function T() {
            if (W.isRoot()) {
                af(routingTable.$.link(), g.reverse, true, true)
            } else {
                W.back()
            }
        }

        function U() {
            if (M !== null) {
                ad(window.routingTable.$.link(), {
                    cutinMode: g.noEffect
                });
                M.slideOut()
            } else {
                ad(window.routingTable.$.link())
            }
        }

        function ad(aY, aX) {
            af(aY, aX, true, true)
        }

        function ag(aZ, aX) {
            if (/^http:\/\//.test(aZ)) {
                location.href = aZ
            } else {
                if (cloud7.offline === true) {
                    aZ = aZ.replace(".", cloud7.rootName)
                }
                var aY = aZ.split("?");
                ad({
                    path: aY[0],
                    arg: Z.parseSearchString(aY[1] || "")
                }, aX)
            }
        }

        function R(aY, aX) {
            af({
                path: location.pathname.substring(1),
                arg: Z.parseSearchString(decodeURI(location.search.substring(1)))
            }, {
                cutinMode: aY === "back" ? g.reverse : aY === "refresh" ? g.noEffect : g.normal
            }, aX || aY !== "back", false)
        }
        var ac = true;
        W.onPopState(function (aX) {
            if (!ac) {
                am.onDone(function () {
                    R(aX.popState, aX.isNew)
                })
            }
        });
        am.onDone(function () {
            R("refresh", true);
            ac = false
        });

        function aj(aY, aZ, aX) {
            var a0 = null;
            return function (a4, a1, a2, a3) {
                var a5 = false;
                if (a0 === null) {
                    a0 = ay.popupPage(aY, a2);
                    Q && Q(a0);
                    aX && aX(a0)
                } else {
                    if (!a0.isDisplayed()) {
                        a0.display(a2)
                    } else {
                        a5 = true
                    }
                } if (a3) {
                    aZ(a0, a1, a4)
                }
                return a5
            }
        }

        function K(aX, aY) {
            return function (a3, aZ, a0) {
                var a1 = cloud7.parseId(a3, aZ);
                var a2 = L[a1];
                if (a2 !== undefined) {
                    a2.display(a0)
                } else {
                    a2 = ay.popupPage(aX || "content-item-page", a0);
                    aY(a2, a1);
                    Q && Q(a2);
                    L[a1] = a2
                }
            }
        }

        function al(a0, aX) {
            var aZ = a0.querySelector(".loading-tips");
            aZ && aZ.abort();
            var aY = aV.make("loading-tips");
            a0.classList.add("loading");
            a0.appendChild(aY);

            function a1() {
                a0.classList.remove("loading");
                x(aY)
            }
            m(aY, {
                remove: a1,
                abort: function () {
                    aX && aX();
                    a1()
                }
            });
            return aY
        }

        function an(aX) {
            var a2 = aX.contentGetter,
                a5 = aX.listWrapper,
                aZ = aX.contentCache,
                a8 = aX.onLoad,
                a1 = aX.getArg,
                a7 = aX.needMore === true,
                a9 = aX.page;
            var ba = a9.dataList = [];
            a5.innerHTML = "";
            var a4 = document.createElement("div");
            a4.classList.add("list-content");
            a5.appendChild(a4);
            var a0 = 1;

            function a3(bb) {
                a2(function (bc) {
                    if (aZ !== undefined) {
                        n(bc, function (bd) {
                            aZ[bd.id] = bd;
                            ba.push(bd)
                        })
                    }
                    aX.onFirstLoad && aX.onFirstLoad.apply(null, arguments) && delete aX.onFirstLoad;
                    a8.apply(null, arguments);
                    bb.apply(null, arguments)
                }, k(a1, {
                    page: a0++
                }))
            }

            function aY(bc) {
                var bb = aV.make("button-load-more");
                var bd = false;
                if (bc < 10) {
                    bb.querySelector(".caption").innerHTML = "已无更多"
                } else {
                    var be = u(bb, function () {
                        if (!bd) {
                            bd = true;
                            bb.classList.add("loading");
                            bb.querySelector(".caption").innerHTML = "正在载入...";
                            a3(function (bf) {
                                bb.classList.remove("loading");
                                if (bf.length === az) {
                                    bb.querySelector(".caption").innerHTML = "显示下10条"
                                } else {
                                    be.remove();
                                    bb.querySelector(".caption").innerHTML = "已无更多"
                                }
                                bd = false
                            })
                        }
                    })
                }
                return bb
            }
            var a6 = al(a9, a2.abort);
            a3(function (bb) {
                a6.remove();
                if (a7 && bb.length !== 0) {
                    a5.appendChild(aY(bb.length))
                }
            })
        }

        function ak(aX) {
            var a1 = aX.id,
                a3 = aX.onLoad,
                a0 = aX.contentGetter,
                aY = aX.contentCache || J;
            var aZ = aY[a1];
            if (aZ !== undefined) {
                a3(aZ)
            } else {
                var a2 = al(aX.page);
                a0(function (a4) {
                    a2.remove();
                    aY[a1] = a4;
                    a3(a4)
                }, a1)
            }
        }

        function aS(aZ) {
            var aX = aV.make(aZ);
            var aY = 0;
            aX.slideIn = function (a0) {
                if (!F.ios) {
                    aY = document.body.scrollTop
                }
                document.body.appendChild(aX);
                f(aX, {
                    transform: D("100%", aY + "px", 0),
                    display: "block"
                });
                B(aX, "0.3s", {
                    transform: D("0", aY + "px", 0)
                }, function () {
                    if (M !== null) {
                        x(M)
                    }
                    ah.classList.add("hide");
                    C(aX, 0, 0);
                    a0 && a0();
                    M = aX
                })
            };
            aX.slideOut = function (a0) {
                ah.classList.remove("hide");
                if (!F.ios) {
                    C(aX, 0, aY);
                    document.body.scrollTop = aY
                }
                if (a0 && a0 === true) {
                    M = null;
                    x(aX)
                } else {
                    B(aX, "0.3s", {
                        transform: D("100%", aY + "px", 0)
                    }, function () {
                        M = null;
                        x(aX)
                    })
                }
            };
            s(aX, function () {
                P && P(aX)
            });
            return aX
        }

        function aE(aY) {
            var a0 = aS("map-page");
            O && O(a0);
            var aX = a0.querySelector(".content");
            var aZ = al(a0);
            a0.slideIn(function () {
                setTimeout(function () {
                    Z.lbs.markerMap(function () {
                        aZ.remove()
                    }, aX, function (a1) {
                        return aV.make("map-info-window", a1)
                    }, aY)
                }, 500)
            })
        }

        function aa(aX) {
            return function (a1, a3, aZ) {
                var aY = a3.split(" ");
                var a2 = aY[1] === undefined ? 0 : aY[1].split(":");
                var a0 = a1.querySelector("img");
                if (aZ[aY[0]] === undefined) {
                    x(a1)
                } else {
                    s(a1, function () {
                        var a5 = a1.offsetWidth;
                        var a4 = a2 === 0 ? a1.offsetHeight : a5 / parseInt(a2[0], 10) * parseInt(a2[1], 10) + 0.5 << 0;
                        aX(a1, a1.querySelector("img"), aZ[aY[0]], a5, a4)
                    })
                }
            }
        }
        aV && aV.addHandler({
            telephone: function (aY, aZ, aX) {
                aY.setAttribute("href", "tel:" + aX[aZ]);
                u(aY, function () {}, true)
            },
            address: function (aY, aZ, aX) {
                u(aY, function () {
                    aE([aX])
                }, true)
            },
            link: function (aZ, a1, aY) {
                var a0 = a1.split("?"),
                    aX = {};
                Z.parsePairString(a0[1], "&", "=", function (a2, a3) {
                    aX[a2] = aY[a3]
                });
                u(aZ, function () {
                    ad(routingTable[a0[0]].link(aX))
                })
            },
            url: function (aY, aZ, aX) {
                u(aY, function () {
                    ag(aX[aZ])
                })
            },
            ratio: function (aX, aZ) {
                var aY = aZ.split(":");
                s(aX, function () {
                    var a0 = aX.offsetWidth / parseInt(aY[0], 10) * parseInt(aY[1], 10) + 0.5 << 0;
                    f(aX, {
                        height: a0 + "px"
                    })
                })
            },
            img: aa(function (a1, aY, aZ, a0, aX) {
                c(aY, aZ, a0, aX, true)
            }),
            listImg: aa(z),
            slideImg: aa(function (a1, aY, aZ, a0, aX) {
                h(a1, a0, aX);
                aY.setAttribute("data-slide-src", aZ);
                aY.zCenterAttr = {
                    width: a0,
                    height: aX
                }
            }),
            order: function (aY, aZ, aX) {
                if (aX.isUp === true) {
                    u(aY, function () {
                        cloud7.order(aX.id + "-" + aZ)
                    })
                } else {
                    aY.classList.add("sold-out");
                    aY.innerHTML = "此商品已下架"
                }
            },
            intro: function (aY, aZ, aX) {
                aY.innerHTML = aX[aZ].replace(/<img[^>]*>/g, "")
            }
        });
        q("click", "a[data-geo-address]", function (aX, aY) {
            aX.preventDefault();
            var aZ = aY.getAttribute("href").replace("geo:", "").split(",");
            aE([{
                lat: aZ[1],
                lng: aZ[0]
            }])
        });
        var ao = [];

        function aI(aX, aY) {
            aY.routeName = aX;
            ao.push({
                route: routingTable[aX].isIn,
                result: aY
            })
        }

        function aN(aY, aX) {
            return function (aZ) {
                return K(aY, function (a1, a0) {
                    ak({
                        id: a0,
                        page: a1,
                        contentGetter: aX.contentGetter,
                        contentCache: aX.contentCache,
                        onLoad: function (a2) {
                            aZ(a1, a2, a0, aX.onLoad(a1, a2, a0))
                        }
                    })
                })
            }
        }

        function aO(aY, aX) {
            return function (aZ) {
                return aj(aY, function (a2, a3, a4) {
                    var a1 = undefined;
                    var a0 = aX.contentGetter;
                    a0.getId && m(a3, {
                        id: a0.getId(a4)
                    });
                    an({
                        contentGetter: a0,
                        contentCache: J,
                        getArg: a3,
                        page: a2,
                        listWrapper: a2.querySelector(".list-wrapper"),
                        needMore: true,
                        onFirstLoad: function (a6, a5) {
                            a1 = aX.onFirstLoad(a2, a3, a6, a5)
                        },
                        onLoad: function (a5) {
                            aZ(a2, a5, a3, a1)
                        }
                    })
                }, aZ.doPage)
            }
        }

        function ar(aX) {
            return {
                contentGetter: cloud7.getBlogPost(aX),
                onLoad: function (aZ, aY) {
                    aM(aZ, {
                        title: aY.title + "-" + aR,
                        description: aY.intro,
                        picture: aY.img
                    });
                    return aX
                }
            }
        }
        var aq = {
            contentGetter: cloud7.getBlogList,
            contentCache: J,
            onFirstLoad: function (aY, aZ, aX) {
                var a0 = aX.length === 0 ? "资讯列表" : aX[0].blogName;
                aM(aY, {
                    title: a0 + "-" + aR
                });
                return a0
            }
        };
        var aB = {
            contentGetter: cloud7.getSubbranchList,
            contentCache: J,
            onFirstLoad: function (aY, aZ) {
                var aX = aZ.city || aU[0];
                aM(aY, {
                    title: aX + "的分店列表-" + aR
                });
                return aX
            }
        };
        var aA = {
            contentGetter: cloud7.getSubbranch,
            onLoad: function (aY, aX) {
                aM(aY, {
                    title: aX.name + "-" + aR,
                    picture: aX.img,
                    description: "地址:" + aX.address + " 电话:" + aX.telephone
                })
            }
        };
        var au = {
            contentGetter: cloud7.getGalleryList,
            contentCache: S,
            onFirstLoad: function (aX) {
                aM(aX, {
                    title: "相册列表-" + aR
                })
            }
        };
        var at = {
            contentGetter: cloud7.getGallery,
            contentCache: S,
            onLoad: function (aY, aX) {
                aM(aY, {
                    title: aX.title + "-" + aR
                })
            }
        };
        var aw = {
            contentGetter: cloud7.getGoodList,
            contentCache: J,
            onFirstLoad: function (aZ, a0, aX, aY) {
                var a1 = "";
                if (a0.keywords !== undefined) {
                    a1 = a0.keywords
                } else {
                    if (a0.categoryId !== undefined) {
                        a1 = V[a0.categoryId].categoryName
                    } else {
                        a1 = aX[0].categoryName
                    }
                }
                aM(aZ, {
                    title: a1 + "-" + aR
                });
                return aY
            }
        };
        var av = {
            contentGetter: cloud7.getGood,
            onLoad: function (aY, aX) {
                aM(aY, {
                    title: aX.name + "-" + aR,
                    picture: aX.img
                })
            }
        };
        var ax = {
            contentGetter: cloud7.getGoodSubject,
            onLoad: function (aY, aX) {
                aM(aY, {
                    title: aX.title + "-" + aR,
                    img: aX.img,
                    description: aX.info
                })
            }
        };
        var ap = {
            blogList: aO("page-blog-list", aq),
            blogPost: aN("page-blog-post", ar("blogPost")),
            page: aN("page-blog-post", ar("page")),
            subbranchList: aO("page-subbranch-list", aB),
            subbranch: aN("page-subbranch-detail", aA),
            galleryList: aO("page-gallery-list", au),
            gallery: aN("page-gallery", at),
            goodList: aO("page-good-list", aw),
            good: aN("page-good-detail", av),
            goodSubject: aN("page-good-subject", ax)
        };
        aI("$", ay.displayMain);

        function ai(aX) {
            var a0 = Z.SimpleSlideListPanel(aX);
            var aY = a0.length();

            function aZ(a3, a6) {
                a3 = (a3 + aY) % aY;
                var a4 = a0.item(a3);
                var a2 = a4.querySelectorAll("img[data-slide-src]");
                var a1 = a2.length,
                    a5 = 0;
                if (a1 === 0) {
                    if (a6) {
                        aZ(a3 + 1, false);
                        aZ(a3 - 1, false)
                    }
                } else {
                    o(a2, function (a8) {
                        r(a8, function () {
                            if (++a5 === a1 && a6) {
                                aZ(a3 + 1, false);
                                aZ(a3 - 1, false)
                            }
                        });
                        var a9 = a8.getAttribute("data-slide-src");
                        var a7 = a8.zCenterAttr;
                        if (a7) {
                            c(a8, a9, a7.width, a7.height, false);
                            delete a9.zCenterAttr
                        } else {
                            a8.src = a9
                        }
                        a8.removeAttribute("data-slide-src")
                    })
                }
            }
            a0.onAnimateStart(function (a1) {
                aZ(a1.targetItem, true)
            });
            return a0
        }

        function aH(a5, a0, aX, a1) {
            var a3 = aX.querySelector(".red-point .wrapper");
            var a2 = [],
                aY = null;
            aV.makeList(a5, a0, aX.querySelector("ul"), function (a7, a6) {
                var a8 = document.createElement("span");
                a3.appendChild(a8);
                a2.push(a8);
                a1 && a1(a7, a6)
            });
            var a4 = ai(aX);

            function aZ(a6) {
                aY = a2[a6];
                aY.classList.add("active")
            }
            aZ(0);
            a4.onCutStart(function () {
                aY.classList.remove("active")
            });
            a4.onAnimateStart(function (a6) {
                aZ(a6.targetItem)
            });
            return a4
        }

        function I(a2, a0, aX, aY, a1) {
            var a3 = aX.querySelector("ul");
            var aZ = null;
            n(a0, function (a4, a5) {
                if (a5 % aY === 0) {
                    aZ = document.createElement("li");
                    a3.appendChild(aZ)
                }
                aZ.appendChild(aV.make(a2, a4, a1))
            });
            return ai(aX)
        }

        function N(a4, aY, a3) {
            var a2 = aY.item,
                a1 = {}, a0 = aY.title;
            n(aY.item, function (a6, a5) {
                a1["#" + a6.name] = a5
            });

            function aZ(a6) {
                var a5 = a2[a6];
                aJ("#" + a5.name);
                aM(a3, {
                    title: a5.title + "-" + a0 + "-" + aR,
                    description: a5.intro,
                    picture: a5.img
                })
            }
            a4.onAnimateStart(function (a5) {
                aZ(a5.targetItem)
            });
            var aX = a1.hasOwnProperty(location.hash) ? a1[location.hash] : 0;
            a4.display(aX);
            aZ(aX)
        }

        function aT(aZ, aX) {
            aX = aX || 2000;
            var aY = null;

            function a1() {
                if (aY !== null) {
                    clearTimeout(aY);
                    aY = null
                }
            }

            function a0() {
                if (aY === null) {
                    aY = setInterval(aZ.cutLeft, aX)
                }
            }
            b(aZ, "DOMNodeInsertedIntoDocument", a0);
            b(aZ, "DOMNodeRemovedFromDocument", a1);
            aZ.onSwipeStart(a1);
            aZ.onSwipeEnd(a0);
            a0()
        }

        function aD(aX, a1, a0, aZ) {
            var aY = a1.querySelector(".button-city");
            aY.classList.add("hide");
            u(aY, function () {
                aY.classList.add("expand");
                var a2 = i("div", {
                    classList: ["city-list"],
                    css: {
                        transform: "translate3d(0,-100%,0)",
                        opacity: 0
                    }
                });
                B(a2, "0.2s ease-in-out", {
                    transform: "translate3d(0,0,0)",
                    opacity: 1
                }, function () {
                    f(a2, {
                        "z-index": 100
                    })
                });

                function a3() {
                    y(a1);
                    x(a2);
                    aY.classList.remove("expand")
                }
                n(aZ, function (a4) {
                    if (a4 !== a0) {
                        var a5 = document.createElement("div");
                        a5.innerHTML = a4;
                        a2.appendChild(a5);
                        u(a5, function () {
                            a1.querySelector(".cur-city").innerHTML = a4;
                            a3();
                            ad(routingTable.subbranchList.link({
                                city: a4
                            }))
                        })
                    }
                });
                a1.appendChild(a2);
                a(a1, a3)
            })
        }
        am.onDone(function () {
            H.onLoad({
                indexData: ab,
                subbranchCityList: aU,
                goodCategoryList: V,
                jump: ad,
                jumpURL: ag,
                goBack: T,
                goHome: U,
                history: W,
                onJump: ae.regist,
                setTitle: aM,
                loadList: an,
                loadContent: ak,
                ListPage: aj,
                ContentPage: K,
                registRoute: function (aX) {
                    p(aX, aI)
                },
                registPage: function (aX) {
                    p(aX, function (aZ, aY) {
                        aI(aZ, ap[aZ](aY))
                    })
                },
                Loading: al,
                slidePage: aS,
                popupMarkerMap: aE,
                curSubPage: function () {
                    return M
                },
                doSubPage: function (aX) {
                    Q = aX
                },
                doMapPage: function (aX) {
                    O = aX
                },
                doSlidePage: function (aX) {
                    P = aX
                },
                LazyLoadSlideListPanel: ai,
                RedPointsSlideListPanel: aH,
                ColumnSlideListPanel: I,
                popupCityList: aD,
                doAlbumSlideList: N,
                slideTimer: aT
            });
            aK = Z.Router(ao, function (aZ, aX, aY) {
                Q && Q(ay.popupPage("not-found", aY))
            });
            if (aW !== null) {
                f(ah, {
                    transform: "translate3d(100%,0,0)"
                });
                setTimeout(function () {
                    B(ah, "all 0.3s ease", {
                        transform: "translate3d(0,0,0)"
                    });
                    B(aW, "all 0.3s ease", {
                        opacity: 0
                    }, function () {
                        x(aW);
                        x(document.getElementById("welcome-style"))
                    })
                }, 0)
            }
        });
        setTimeout(am.check, 1000)
    }
    Z.onLoad(function () {
        q("touchstart", "a[href]", function (H, I) {
            I.zTouchStart = true;
            setTimeout(function () {
                I.zTouchStart = false
            }, 700)
        });
        q("click", "a[href]", function (H, I) {
            if (!I.zTouchStart) {
                H.preventDefault();
                I.zTouchStart = false
            }
        });
        o(document.querySelectorAll("img[data-load-src]"), function (H) {
            var I = H.getAttribute("data-load-src");
            if (cloud7.offline) {
                H.src = I
            } else {
                H.src = "/Themes/" + window.themeName + "/Styles/img/" + I
            }
            H.removeAttribute("data-load-src")
        })
    });
    m(Z, {
        addWall: a,
        removeWall: y,
        imgHtml: l,
        Cloud7PageSystem: e,
        CenterImg: c,
        ScrollLoadImg: z
    })
})();
;

(function () {
    window.cloud7 = {};
    var c = Z.extend,
        f = Z.loopArray;
    var i = /{([^{}]*)}/;

    function g(l) {
        var k = [];
        f(l.split("/"), function (n, m) {
            if (m === 0) {} else {
                if (i.test(n)) {
                    k.push({
                        isArg: true,
                        str: RegExp.$1
                    })
                } else {
                    k.push({
                        isArg: false,
                        str: n
                    })
                }
            }
        });
        return {
            isIn: function (o) {
                var p = o.split("/");
                if (p[0] === "" && p.length !== 1) {
                    p.shift()
                }
                if (p.length !== k.length) {
                    return false
                }
                var q;
                for (var m = 0, n = p.length; m !== n; ++m) {
                    q = k[m];
                    if (!q.isArg && q.str.toLowerCase() !== p[m].toLowerCase()) {
                        return false
                    }
                }
                return true
            },
            link: function (m) {
                var n = "/",
                    o = c({}, m);
                f(k, function (q, p) {
                    if (p !== 0) {
                        n += "/"
                    }
                    if (q.isArg) {
                        n += o[q.str];
                        delete o[q.str]
                    } else {
                        n += q.str
                    }
                });
                return {
                    path: n,
                    arg: o || {}
                }
            }
        }
    }
    window.routingTable = function (l) {
        var k = {};
        Z.loopObj(l, function (m, n) {
            k[m] = g(n)
        });
        return k
    }({
        $: "/",
        good: "/Goods/Detail/{id}",
        goodList: "/Goods/List",
        goodCategoryList: "/Goods/Category",
        subbranchList: "/Subbranch/List",
        subbranchCityList: "/Subbranch/CityList",
        subbranch: "/Subbranch/Detail/{id}",
        goodSubject: "/Goods/Subject/{id}",
        blogList: "/blog/{id}",
        blogPost: "/blogpost/{id}",
        page: "/page/{id}",
        galleryList: "/ImageGallery",
        gallery: "/ImageGallery/{id}"
    });

    function a(k) {
        Z.ajax(c(k, {
            requestHeader: {
                Accept: "application/json",
                "X-Requested-With": "XMLHttpRequest"
            },
            url: k.url
        }))
    }

    function h(l) {
        var k = l;
        if (k << 0 === k) {
            return k + ""
        } else {
            /(\d*\.)(\d*)/.exec(l + "0");
            return RegExp.$1 + RegExp.$2.substring(0, 2)
        }
    }

    function b(k) {
        a({
            url: k.url,
            arg: c(k.arg, {
                pageSize: 10
            }),
            onLoad: function (l) {
                var m = [];
                f(k.listHandler ? k.listHandler(l) : l, function (n) {
                    m.push(k.itemHandler(n))
                });
                k.onGet(m, l.length)
            },
            isJson: true
        })
    }

    function e(k, l) {
        if (l === undefined) {
            l = true
        }
        return {
            orderId: k.Id,
            id: k.Id,
            name: k.ProductPart.ProductName,
            price: h(k.ProductPart.Unit),
            oldPrice: h(k.ProductPart.Price),
            subjectPrice: h(k.ProductPart.Price),
            img: k.ProductPart.BigImg,
            isUp: l && k.ProductPart["Double.IsUp"] === "true",
            text: k.BodyPart.Text,
            detail: k.BodyPart.Text
        }
    }

    function j(k) {
        return {
            id: k.Id,
            img: k.SubbranchPart.ImagePath,
            name: k.SubbranchPart.Title,
            address: k.Locations.LocationPart.LocationName,
            description: k.BodyPart.Text,
            telephone: k.Locations.LocationPart.TelePhoneNumber,
            lat: k.Locations.LocationPart.Latitude,
            lng: k.Locations.LocationPart.Longitude
        }
    }

    function d(k) {
        var l = [];
        f(k.Images, function (m) {
            l.push({
                title: m.Title || "",
                intro: m.Caption,
                img: m.PublicUrl,
                name: m.Name
            })
        });
        return {
            item: l,
            id: k.Name,
            title: k.Name,
            intro: k.Caption,
            img: k.CoverImagePath
        }
    }
    cloud7.getGoodList = function (l, k) {
        a({
            url: routingTable.goodList.link(k).path,
            arg: c(k, {
                pageSize: 10
            }),
            onLoad: function (m) {
                if (m.data === null) {
                    l([], 0);
                    return
                }
                m = m.data;
                var n = [];
                f(m.data, function (o) {
                    n.push(e(o))
                });
                l(n, m.count)
            },
            isJson: true
        })
    };
    cloud7.getGood = function (l, k) {
        a({
            url: routingTable.good.link({
                id: k
            }).path,
            dataHandler: function (m) {
                return e(JSON.parse(m.data))
            },
            onLoad: l,
            isJson: true
        })
    };
    cloud7.getSubbranch = function (l, k) {
        a({
            url: routingTable.subbranch.link({
                id: k
            }).path,
            dataHandler: function (m) {
                return j(JSON.parse(m.data))
            },
            onLoad: l,
            isJson: true
        })
    };
    cloud7.getSubbranchList = function (l, k) {
        b({
            url: routingTable.subbranchList.link().path,
            arg: k,
            itemHandler: j,
            listHandler: function (m) {
                return JSON.parse(m.data || "[]")
            },
            onGet: l
        })
    };
    cloud7.getGoodSubject = function (l, k) {
        a({
            url: routingTable.goodSubject.link({
                id: k
            }).path,
            onLoad: function (m) {
                var o = m.data;
                var n = {
                    img: o.SubjectImage,
                    type: o.SubjectType,
                    title: o.SubjectTitle,
                    info: o.SubjectBody
                };
                if (o.SubjectType === "Package") {
                    (function (q) {
                        var r = [];
                        var p = q.SubjectState;
                        f(q.Packages, function (t) {
                            var s = [];
                            f(t.GoodsParts, function (u) {
                                s.push({
                                    id: u.GoodsPart.ProductId,
                                    name: u.GoodsPart.ProductName,
                                    price: h(u.GoodsPart.Price),
                                    img: u.GoodsPart.Image
                                })
                            });
                            r.push({
                                orderId: t.Id,
                                name: t.PackageTitle,
                                detail: t.PackageDetials,
                                subjectPrice: h(t.PackagePrice),
                                price: h(t.PackageOldPrice),
                                goods: s,
                                isUp: p && t.PackageState
                            })
                        });
                        n.packages = r
                    })(o)
                } else {
                    (function (r) {
                        var q = r.SubjectState;
                        var p = [];
                        f(r.ProductParts, function (s) {
                            p.push(e(s, q))
                        });
                        n.goods = p
                    })(o)
                }
                l(n)
            },
            isJson: true
        })
    };
    cloud7.getGoodCategoryList = function (k) {
        a({
            url: routingTable.goodCategoryList.link().path,
            onLoad: function (l) {
                var m = [];
                f(l.data, function (n) {
                    m.push({
                        id: n.Id,
                        categoryName: n.CategoryName,
                        tags: [n.CategoryDetail]
                    })
                });
                k(m)
            },
            isJson: true
        })
    };
    cloud7.getSubbranchCityList = function (k) {
        a({
            url: routingTable.subbranchCityList.link().path,
            onLoad: function (l) {
                k(l.data)
            },
            isJson: true
        })
    };
    cloud7.getBlogList = function (l, k) {
        b({
            url: routingTable.blogList.link(k).path,
            arg: k,
            itemHandler: function (m) {
                var n = m.ImageGalleryPart;
                return {
                    id: m.Id,
                    img: n ? n.CoverImagePath : m.BlogPostPart.imgs[0],
                    isImgText: m.BlogPostPart.BlogType === "false",
                    text: m.BodyPart.Text,
                    title: m.TitlePart.Title,
                    blogName: m.BlogPostPart.BlogName,
                    intro: m.BlogPostPart.Content
                }
            },
            onGet: l
        })
    };
    cloud7.getBlogList.getId = function (k) {
        return cloud7.parseId(k)
    };
    cloud7.getBlogPost = function (k) {
        return function (m, l) {
            a({
                url: routingTable[k].link({
                    id: l
                }).path,
                onLoad: function (q) {
                    var n = document.createElement("div");
                    n.innerHTML = q;
                    var o = n.querySelector("#content");
                    var p = n.querySelector(".coverImage img");
                    var r = o.querySelector("section p");
                    m({
                        type: k,
                        img: p !== null ? p.getAttribute("src") : undefined,
                        title: o.querySelector("header h1").innerHTML,
                        text: o.querySelector("section").innerHTML,
                        blogName: k === "blogPost" ? o.querySelector("#blog_nav a").innerHTML : "",
                        intro: r === null ? "" : r.textContent
                    })
                }
            })
        }
    };
    cloud7.getGalleryList = function (l, k) {
        b({
            url: routingTable.galleryList.link({}).path,
            arg: k,
            itemHandler: d,
            listHandler: function (m) {
                return m.data
            },
            onGet: l
        })
    };
    cloud7.getGallery = function (l, k) {
        a({
            url: routingTable.gallery.link({
                id: k
            }).path,
            dataHandler: function (m) {
                return d(m.data)
            },
            onLoad: l,
            isJson: true
        })
    };
    cloud7.order = function (k) {
        window.location.href = "/Order/Info/" + k
    };
    cloud7.parseId = function (k) {
        var l = k.split("/");
        return l[l.length - 1]
    };
    cloud7.getIndex = function (k) {
        k(window.cloud7IndexData)
    }
})();
;

(function () {
    var f = Z.loopArray,
        h = Z.onTap,
        d = Z.css,
        g = Z.loopNodeList,
        a = Z.appendChildren,
        c = Z.CenterImg,
        i = Z.ScrollLoadImg,
        e = Z.element;

    function b(j, k) {
        return e("div", {
            classList: ["button", k ? "left" : "right", j],
            "data-button": j
        })
    }
    Z.onLoad(function () {
        Z.noAnimate(true);
        g(document.querySelectorAll("header[data-header]"), function (p) {
            var q = e("div", {
                classList: ["header-bar"]
            });
            f(p.getAttribute("data-header").split(" "), function (r, s) {
                q.appendChild(b(r, s === 0))
            });
            a(q, p);
            p.parentNode.replaceChild(q, p)
        });
       /*
	   g(document.querySelectorAll(".page-content"), function (p) {
            p.setAttribute("data-zone", "");
            p.appendChild(e("a", {
                classList: ["link-cloud7"],
                href: "javascript:void(0);",
                innerHTML: "&copy;2013 杀价王",
                "data-footer": ""
            }))
        });
		*/
        var l = document.getElementById("layout");
        var n = document.getElementById("main-page");
        var o = Z.HTMLTemplate(document.getElementById("template"));
        var k = Z.DrawerPanel(document.getElementById("layout"));
        var j = k.rightDrawer();
        var m = l.offsetWidth;
        Z.Cloud7PageSystem({
            pageSize: 10,
            layout: l,
            welcomePage: document.getElementById("welcome-page"),
            pagePanel: Z.PagePanel(document.getElementById("page")),
            template: o,
            siteTitle: window.siteName,
            prepareData: ["subbranchCityList", "goodCategoryList"],
            onLoad: function (C) {
                var s = C.jump,
                    r = C.indexData;
                C.doSubPage(function (D) {
                    Z.loopNodeList(D.querySelectorAll("[data-button]"), function (E) {
                        var F = E.getAttribute("data-button");
                        switch (F) {
                        case "back":
                            h(E, C.goBack);
                            break;
                        case "drawer":
                            h(E, j.toggle);
                            break
                        }
                    })
                });
                C.doSlidePage(function (D) {
                    h(D.querySelector(".back"), D.slideOut)
                });
                C.slideTimer(C.RedPointsSlideListPanel("banner-item", r.banner, n.querySelector(".banner")));
                o.makeList("navigate-column", r.column, n.querySelector(".column"));

                function A(E, F, D) {
                    E.preventDefault();
                    if (F.value !== "") {
                        F.blur();
                        s(routingTable.goodList.link({
                            keywords: F.value
                        }), D || {});
                        F.value = ""
                    }
                }
                var q = {};
                (function () {
                    var D = ["red", "yellow", "cyan", "blue", "purple"];
                    var G = j.querySelector(".search-bar");
                    var H = G.querySelector("input");
                    var E = j.querySelector(".category-list");
                    var F = E.querySelector(".scroll-content");
                    var I = b("search", false);
                    G.appendChild(I);
                    Z.onTouchStart(j, function (K) {
                        K.onTouchMove(function (L) {
                            L.preventDefault()
                        })
                    });

                    function J(K) {
                        A(K, H, {
                            effect: Z.PagePanel.CutEffect.FadeIn,
                            duration: 0.32
                        });
                        j.toggle()
                    }
                    h(I, function (K) {
                        if (H.value === "") {
                            H.focus()
                        } else {
                            J(K)
                        }
                    });
                    G.addEventListener("submit", J);
                    f(C.goodCategoryList, function (L, M) {
                        q[L.id] = L;
                        var K = o.make("category-item", {
                            id: L.id,
                            title: L.categoryName,
                            subTitle: L.tags.join("/")
                        }, function (N, O) {
                            N.classList.add(D[M % 5]);
                            h(N, function () {
                                s(routingTable.goodList.link({
                                    categoryId: O.id
                                }), {
                                    effect: Z.PagePanel.CutEffect.FadeIn,
                                    duration: 0.32
                                });
                                j.toggle()
                            })
                        });
                        F.appendChild(K)
                    });
                    Z.ScrollPanel(E)
                })();
                var t = function (F, E, D, G) {
                    F.querySelector(".page-title").innerHTML = G;
                    o.makeList("blog-list-item", E, F.querySelector(".list-content"), function (I, H) {
                        I.classList.add(H.img === undefined ? "without-img" : "with-img")
                    })
                };

                function u(E, D) {
                    E.appendChild(o.make("blogpost", D))
                }
                var B = C.subbranchCityList,
                    p = B[0];

                function z(G, E, D) {
                    G.querySelector(".button-city").classList.remove("hide");
                    var F = G.querySelector(".cur-city");
                    p = D.city || p;
                    F.innerHTML = p;
                    o.makeList("subbranch-list-item", E, G.querySelector(".list-content"))
                }
                z.doPage = function (D) {
                    C.popupCityList(D.querySelector(".button-city"), D, p, B);
                    h(D.querySelector(".button.map"), function () {
                        D.dataList && C.popupMarkerMap(D.dataList)
                    })
                };

                function y(E, D) {
                    E.querySelector(".page-content").appendChild(o.make("subbranch-detail", D))
                }

                function v(F, D) {
                    F.querySelector(".page-content").appendChild(o.make("good-detail", D));
                    var E = F.querySelector(".price").offsetWidth + 18;
                    d(F.querySelector(".button-buy-now"), {
                        left: ((m - E) / 2 << 0) + E + "px"
                    })
                }

                function w(G, D, H, F) {
                    var E = G.querySelector(".list-wrapper");
                    if (H.keywords !== undefined) {
                        E.insertBefore(o.make("search-result", {
                            name: H.keywords,
                            count: F
                        }), E.querySelector("div"))
                    }
                    if (F === 0) {
                        E.appendChild(o.make("empty-search"))
                    }
                    o.makeList("good-list-item", D, G.querySelector(".list-content"))
                }
                w.doPage = function (D) {
                    var E = D.querySelector("input");

                    function F(G) {
                        A(G, E)
                    }
                    h(D.querySelector(".button.search"), F);
                    D.querySelector("form").addEventListener("submit", F)
                };

                function x(H, E) {
                    var F = m / 16 * 9 << 0;
                    var D = m - 46;
                    var I = H.querySelector(".page-content");
                    var G = I.querySelector(".list-content");
                    H.querySelector(".page-title").innerHTML = E.title;
                    I.querySelector(".info").innerHTML = E.info;
                    c(I.querySelector(".img-wrapper img"), E.img, m, F, true);
                    if (E.type.toLocaleLowerCase() === "package") {
                        o.makeList("subject-package-item", E.packages, G, function (R, M) {
                            var O = R.querySelector(".img-wrapper");
                            var N = M.goods;
                            var L = 0;

                            function P(aa, ab, Y, V) {
                                var U = M.goods[L++];
                                var X = e("div", {
                                    css: {
                                        position: "absolute",
                                        left: aa + "px",
                                        top: ab + "px"
                                    }
                                });
                                var W = document.createElement("img");
                                X.appendChild(W);
                                O.appendChild(X);
                                Z.onInsert(X, function () {
                                    i(X, W, U.img, Y, V)
                                });
                                h(X, function () {
                                    s(routingTable.good.link({
                                        id: U.id
                                    }))
                                })
                            }

                            function S(U) {
                                d(O, "height", U + "px")
                            }
                            var T, Q, K, J;
                            switch (N.length) {
                            case 2:
                                Q = D / 2 << 0;
                                P(0, 0, Q, Q);
                                P(Q + 5, 0, D - 5 - Q, Q);
                                S(Q);
                                break;
                            case 3:
                                T = ((D - 10) / 3 + 0.7) << 0;
                                K = D - 5 - T;
                                J = T * 2 + 5;
                                P(0, 0, K, J);
                                P(K + 5, 0, T, T);
                                P(K + 5, T + 5, T, T);
                                S(J);
                                break;
                            case 4:
                                Q = D / 2 << 0;
                                P(0, 0, Q, Q);
                                P(Q + 5, 0, D - 5 - Q, Q);
                                P(0, Q + 5, Q, Q);
                                P(Q + 5, Q + 5, D - 5 - Q, Q);
                                S(Q * 2 + 5);
                                break;
                            case 5:
                                Q = D / 2 << 0;
                                T = ((D - 10) / 3 + 0.7) << 0;
                                K = D - 5 - T;
                                J = T * 2 + 5;
                                P(0, 0, K, J);
                                P(K + 5, 0, T, T);
                                P(K + 5, T + 5, T, T);
                                P(0, J + 5, Q, Q);
                                P(Q + 5, J + 5, D - 5 - Q, Q);
                                S(Q + J + 5);
                                break;
                            case 6:
                                T = ((D - 10) / 3 + 0.7) << 0;
                                K = D - 5 - T;
                                J = T * 2 + 5;
                                P(0, 0, K, J);
                                P(K + 5, 0, T, T);
                                P(K + 5, T + 5, T, T);
                                P(K + 5, J + 5, T, T);
                                P(K - T, J + 5, T, T);
                                P(0, J + 5, D - T * 2 - 10, T);
                                S(T + J + 5);
                                break
                            }
                        })
                    } else {
                        o.makeList("subject-list-item", E.goods, G)
                    }
                }
                C.registPage({
                    blogList: t,
                    blogPost: u,
                    page: u,
                    subbranchList: z,
                    subbranch: y,
                    good: v,
                    goodList: w,
                    goodSubject: x
                })
            }
        })
    })
})();
;
