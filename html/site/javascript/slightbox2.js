	function slightbox2(width, height, tit, url, back) {
		var winWinth = $(window).width(), winHeight = $(document).height();
		$("body").append("<div class='winbg'></div>");
		
		$("body").append("<div class='stbox'><div class='stboxtit'><span class='stboxTxt'>" + tit + "</span><span class='stboxClose'>X</span></div><iframe class='winIframe' frameborder='0' hspace='0' src=" + url + "></iframe></div>");
		
		$(".winbg").css({ width: winWinth, height: winHeight, background: "#000", position: "absolute", left: "0", top: "0" });
		
		$(".winbg").fadeTo(0, 0.7);
		var stboxLeft = $(window).width() / 2 - width / 2;
		var stboxTop = $(window).height() / 2 - height / 2 + $(window).scrollTop();
		
		$(".stbox").css({ width: width, height: height, left: stboxLeft, top: stboxTop, background: "#fff", position: "absolute" });
		
		$(".stboxtit").css({ width: width, height: "30px",  background: "#666", "line-height": "30px", position: "relative" });
		
		$(".stboxTxt").css({width: "88%", height: "30px", "overflow" : "hidden", "text-indent": "8px", "float": "left", "color": "#FFF",  "font-size": "13px" });
		
		$(".stboxClose").css({ "width": "30px", "float": "right", "font-size": "26px", "color": "#FFF", "cursor": "pointer", "backgroundColor" : "#666", "text-align" : "center",  "font-family":"Verdana" });
		
		var winIframeHeight = height - 30;		
		$(".winIframe").css({ width: width, height: winIframeHeight,  background: "#FFF" });
		
		$(".stboxClose").hover(	function() {
		$(this).css("color", "#FC0");
		}, function() {
		$(this).css("color", "#FFF");
		});
		
		$(".stboxClose, .winbg").click(function() {
			if(back){ top.location=back; }
			$(".winbg").remove();
			$(".stbox").remove();
		});
	}