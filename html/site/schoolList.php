<?php  include_once('lib/school.lib.php'); ?>
<?php     
	 /*
	    $ret格式 :
		     [
			  {"id":"BENGBU","name":"蚌埠","schools":[{"id":"AUFE","name":"安徽财经大学"}]}
			 ,{"id":"BINZHOU","name":"滨州","schools":[{"id":"BZTC","name":"滨州学院"}]} 
			 ,{"id":"CHANGCHU","name":"长春","schools":[{"id":"CCUGHC","name":"长春光华学院"},{"id":"JLSU","name":"东北师范大学"},{"id":"NENU","name":"吉林体育学院"}]}
             ,{}
			 ,{} 
			 ...]			 

     */
	 
$city = getCity();
$school = getSchool();
$k = 0;
 
foreach ($city as $key => $value) {
	$ret[$k]['id'] = $key; 
	$ret[$k]['name'] = $value;
	$i = 0;
	
	if(!empty($school[$key])) {
		foreach ($school[$key] as $num => $value2) {
			$ret[$k]['schools'][$i]['id'] = $num;
			$ret[$k]['schools'][$i]['name'] = $value2;	
			$i++;
		}
	}
	$k++;
}
     
   echo json_encode(array("retCode"=>1,
	                      "retMsg"=>"OK",
						  "retType"=>"LIST",
					      "retObj"=>array("data"=>$ret)));
	
?>