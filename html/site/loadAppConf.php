<?php

   $_POST2=json_decode($HTTP_RAW_POST_DATA,true);
   if($_POST2) {
	  foreach($_POST2 as $k=> $v) {
	   $_POST[$k]=$v;
	  }
   }
   
   $LANG=strtolower($_POST['client']['lang']);
   error_log("lang:".$LANG);
   $arr=array();
   $arr['retCode']=1;
   $arr['retType']="LIST";
   $arr['retMsg']="OK";
   $arr['lang']=$LANG;
   if(empty($LANG) || $LANG=='zh_cn') {
      // [channelid, Cname, Ename, Longitude, Latitude]
      $area=array(
	   array("channelid"=>"1","cname"=>"上海","ename"=>"Shanghai","long"=>"121.26","lat"=>"31.12")
	  ,array("channelid"=>"2","cname"=>"北京","ename"=>"Beijing","long"=>"116.28","lat"=>"39.54")
	  ,array("channelid"=>"52","cname"=>"广州","ename"=>"Guangzhou","long"=>"113.18","lat"=>"23.10")
	  ,array("channelid"=>"37","cname"=>"重庆","ename"=>"Chungking","long"=>"106.33","lat"=>"29.33")
	  ,array("channelid"=>"48","cname"=>"深圳","ename"=>"Shenzhen","long"=>"114.05","lat"=>"22.32")
	  ,array("channelid"=>"57","cname"=>"成都","ename"=>"Chengdu","long"=>"104.04","lat"=>"30.39")
	  );
   } else if($LANG=='zh_tw') {
      $area=array(
       array("channelid"=>"1","cname"=>"上海","ename"=>"Shanghai","long"=>"121.26","lat"=>"31.12")
	  ,array("channelid"=>"2","cname"=>"北京","ename"=>"Beijing","long"=>"116.28","lat"=>"39.54")
	  ,array("channelid"=>"52","cname"=>"廣州","ename"=>"Guangzhou","long"=>"113.18","lat"=>"23.10")
	  ,array("channelid"=>"37","cname"=>"重慶","ename"=>"Chungking","long"=>"106.33","lat"=>"29.33")
	  ,array("channelid"=>"48","cname"=>"深圳","ename"=>"Shenzhen","long"=>"114.05","lat"=>"22.32")
	  ,array("channelid"=>"57","cname"=>"成都","ename"=>"Chengdu","long"=>"104.04","lat"=>"30.39")
	  );
   }
/*   
手机数码
家用电器
服饰内衣、鞋靴童装
家居家纺、锅具餐具
运动健康
箱包、珠宝饰品、手表
美容护理
名牌精品
家具家装
票券积分
游戏电玩
休闲旅游
房屋土地
餐饮美食
交通
其他
台湾馆
*/
   $arr['retObj']['data']=$area;
   // $arr['retObj']['page']=new stdClass();
   $arr['showNeighborIcon']='N';
  
   $arr['ws_url']='ws://www.shajiawang.com.cn';
   $arr['wss_url']='wss://www.shajiawang.com.cn';
 
   echo json_encode($arr);
?>
