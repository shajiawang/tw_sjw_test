<?php
header("Content-Type:text/html; charset=utf-8");

if (isset($_GET['upload'])) {

	$file_name = $_FILES['Filedata']['name']; //取得檔名
	$file_size = number_format(($_FILES['Filedata']['size']/1024), 1, '.', ''); //取得檔案大小
	$allowSubName = $_POST['allowSubName']; //允許檔案格式
	$allowMaxSize = $_POST['maxSize']; //允許上傳大小
	$subn_array = explode(",",$allowSubName);//分割允許上傳副檔名
	$userid = $_POST['enterpriseid']; //取得使用者編號
	
	$checkSubName = "";
	$checkSize = "";
	$checkmsg = "";
	
	$fn_array=explode(".",$file_name);//分割檔名
	$mainName = $fn_array[0];//檔名
	$subName = $fn_array[1];//副檔名	
	
	//判斷檔案格式
	foreach($subn_array as $index => $value){
		if($subName == $value){			
			$checkSubName ="ok";
						
			break;
		}else{
			$checkSubName ="不符";
		
		}
	}
	
	//判斷上傳檔案
	if($file_size <= $allowMaxSize){		
		$checkSize = "ok";
	}else{
		$checkSize = "太大";
	}
	

	if($checkSize == "ok" && $checkSubName == "ok"){
		$upFloder = $_POST['upFloder'];
		if($upFloder != ""){
			$upload_dir = $upFloder.'/';
		}
			
		//中文檔名處理
		if (mb_strlen($mainName,"Big5") != strlen($mainName))
		{
			$mainName = "_".$userid."_sbc".date("ymdHi");//重新命名=檔名+日期
			$file_name = sprintf("%s.%s",$mainName,$subName);//組合檔名
		}else{
			$mainName = "_".$userid."_".$fn_array[0];//重新命名=檔名
			$file_name = sprintf("%s.%s",$mainName,$subName);//組合檔名
		}	
		$upload_file = $upload_dir . basename($file_name);
		
		
		//檔名重覆處理
		$x=1;
		while(file_exists($upload_file)){
			$file_name = "_".$userid."_".sprintf("%s_%d.%s",$fn_array[0] ,$x++ ,$subName);//組合檔名
			$upload_file = $upload_dir . basename($file_name);
		}
		
		
		// 取得上傳圖片
		if ($subName == 'jpg'){
			$src = imagecreatefromjpeg($_FILES['Filedata']['tmp_name']);
		}else if($subName == 'png'){
			$src = imagecreatefrompng($_FILES['Filedata']['tmp_name']);
		}else{
			$src = imagecreatefromgif($_FILES['Filedata']['tmp_name']);
		}		

		// 取得來源圖片長寬
		$src_w = imagesx($src);
		$src_h = imagesy($src);
		if($_POST['prevImg'] == 'preUpLoadImg1'){
			// 假設要長寬不超過90
			if($src_w > $src_h){
			  $thumb_w = 120;
			  $thumb_h = 160;
			}else{
			  $thumb_h = 160;
			  $thumb_w = 120;
			}
		}else{
			// 假設要長寬不超過800X600
			if($src_w > $src_h){
			  $thumb_w = 800;
			  $thumb_h = 600;
			}else{
			  $thumb_h = 800;
			  $thumb_w = 600;
			}		
		}	
		// 建立縮圖
		$thumb = imagecreatetruecolor($thumb_w, $thumb_h);
		
		// 開始縮圖
		imagecopyresampled($thumb, $src, 0, 0, 0, 0, $thumb_w, $thumb_h, $src_w, $src_h);
		// 儲存縮圖到指定 thumb 目錄
		$spfile_name = sprintf("%s_%d.%s",$mainName ,$x++ ,$subName);//組合檔名
		if ($subName == 'jpg'){
			imagejpeg($thumb, $upload_dir ."sp/". basename($file_name));
		}else if($subName == 'png'){
			imagepng($thumb, $upload_dir ."sp/". basename($file_name));
		}else{
			imagegif($thumb, $upload_dir ."sp/". basename($file_name));
		}
		
		$temploadfile = $_FILES['Filedata']['tmp_name'];
		$result = move_uploaded_file($temploadfile , $upload_file);
	
	}else{
		
		$checkmsg = sprintf("1.檔案格式：%s<hr> 2.檔案大小：%s<hr> 3.尺寸建議：800X600",$checkSubName,$checkSize);
	}
	
?>
	<?php if (isset($result)){ //判斷上傳結果?>
        <script language = "javascript">	
            window.opener.document.getElementById("<?php echo $_POST['prevImg']; ?>").src = '<?php echo $_POST['url']; ?>'+'<?php echo $file_name; ?>';
            window.opener.document.getElementById("<?php echo $_POST['prevImg']; ?>").width = 100;
            window.opener.document.getElementById("<?php echo $_POST['formName']; ?>").<?php echo $_POST['rePic']; ?>.value = '<?php echo $file_name; ?>';
            window.close();
        </Script>
    <?php }else{ printf("<b style='color:red;'>上傳失敗:</b><hr>%s",$checkmsg); }?>
    
<?php }else{ ?>
    <?php include_once('upload_f.php');?>
<?php }?>