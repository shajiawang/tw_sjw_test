<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <title>圖片上傳</title>
	<link rel="stylesheet" type="text/css" href="css/imgareaselect/imgareaselect-default.css" />
	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="js/imgareaselect/jquery.imgareaselect.pack.js"></script>
</head>
<body>
   <div>
    <form action="upload.php" enctype="multipart/form-data" method="post">
	  <label for="preview_photo">Photo</label>
	  <input name="photo" id="photo" value="" type="file"  placeholder="" >
	  <input type="submit" value="上傳" >
	</form>
   </div>
</body>
</html>
