<?php

		//參數接收設定
		$filedir = $_POST['filedir'];   //檔案子目錄路徑
		$img = $_POST['pic'];   //圖檔
		$userid = $_POST['userid'];   //使用者名稱
		$pic_name = $_POST['pic_name'];   //圖檔名稱

		//尋找檔案內是否含png檔頭
		$img_format_check = strpos($img,'data:image/png;base64,');

		//去除png檔頭取得純圖檔資料 需注意 data url 格式 與來源是否相符 ex:image/png
		$img = str_replace('data:image/png;base64,','', $img);
		//過濾圖檔資料裡的換行符號
		$img = preg_replace("/\s+/", '', $img);

		//傳送參數檢查
		if (empty($filedir)) {
			$data['retCode'] = -1;
			$data['retMsg'] = "檔案路徑錯誤";
		} else if (empty($img)) {
			$data['retCode'] = -2;
			$data['retMsg'] = "圖檔資料未傳送";
		} else if (empty($userid)) {
			$data['retCode'] = -3;
			$data['retMsg'] = "使用者id未傳送";
		} else if (empty($pic_name)) {
			$data['retCode'] = -4;
			$data['retMsg'] = "圖檔名稱未傳送";
		} else if ($img_format_check === false) {
			$data['retCode'] = -5;
			$data['retMsg'] = "圖檔格式錯誤";
		} else if (!(base64_encode(base64_decode($img, true)) === $img)) {
			$data['retCode'] = -6;
			$data['retMsg'] = "圖檔資料格式錯誤";
		} else {

			$img_data = base64_decode($img);//解base64碼
			$file = '../images/'.$filedir.'_'.$userid.'_'.$pic_name.'.png';//檔名 包含資料夾路徑 請記得此資料夾需 777 權限 方可寫入圖檔
			$success = file_put_contents($file, $img_data);
			$data['retCode'] = 1;
			$data['retMsg'] = "圖檔上傳成功";
		}
		//執行結果輸出json
		$data['retType'] = "MSG";
		echo json_encode($data);

?>
