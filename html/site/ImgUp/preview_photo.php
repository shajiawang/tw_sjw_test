<?php
	error_log(json_encode($_FILES));
	$dest_file_path="uploads/ori/";
	$dest_file_name="test";
	
	if($_FILES["photo"]["error"] > 0){
	   error_log("Error: ".$_FILES["photo"]["error"]);
	}else{
	   $ftype="";
	   error_log("[upload] file type : ".$_FILES["photo"]["type"]);
	   error_log("[upload] file tmp_name : ".$_FILES["photo"]["tmp_name"]);
	   error_log("[upload] file size : ".$_FILES["photo"]["size"]);
	   switch ($_FILES["photo"]["type"]) {
	          case "image/jpeg": $ftype="jpg";break;
			  case "image/jpg": $ftype="jpg";break;
			  case "image/gif": $ftype="gif";break;
			  case "image/png": $ftype="png";break;
	  }
	  $dest_file_name.=".".$ftype;
	  move_uploaded_file($_FILES["photo"]["tmp_name"],$dest_file_path.$dest_file_name);
	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">
    <title>圖片裁切預覽</title>
	<link rel="stylesheet" type="text/css" href="css/imgareaselect/imgareaselect-default.css" />
	<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="js/imgareaselect/jquery.imgareaselect.pack.js"></script>
</head>
<body>
    <div id="ori_div" >
	  <label for="ori_photo">Ori Photo</label>
	  <img src="/uploads/ori/<?php echo $dest_file_name; ?>" style="height:auto;" alt="Ori Photo" id="ori_photo" name="ori_photo" border="0" >
    </div>
	<form method="post" action="cut_photo.php">
	  <input type="hidden" name="x1" id="x1" value="">
	  <input type="hidden" name="y1" id="y1" value="">
	  <input type="hidden" name="x2" id="x2" value="">
	  <input type="hidden" name="y2" id="y2" value="">
	  <input type="hidden" name="w" id="w" value="">
	  <input type="hidden" name="h" id="h" value="">
	  <input type="hidden" name="fname" id="fname" value="<?php echo $dest_file_name; ?>">
	  <input type="submit" value=" 裁剪 180X180 " >
	</form>
	<script>
	
	var cut_w=cut_h=ori_w=ori_h=0;
	$(document).ready(function() {
			cut_w=180;
	        cut_h=180;
			ori_w=$('#ori_photo').width();
			ori_h=$('#ori_photo').height();
			$('#fname').val('<?php echo $dest_file_name; ?>');
			$('<div id="cut_preview"><img src="<?php echo "/uploads/ori/".$dest_file_name; ?>" id="preview_cut_photo" ></div>').css({
					float: 'left',
					position: 'relative',
					overflow: 'hidden',
					width: cut_w +'px',
					height: cut_h+'px'
			}).insertAfter($('#ori_photo'));
			
			$('#ori_photo').imgAreaSelect({
				x1:0,
				y1:0,
				x2:cut_w,
				y2:cut_h,
				maxWidth:cut_w,
				maxHeight:cut_h,
				handles: true,
				onSelectChange: preview,
				onSelectEnd: endSelect
			});
	});
	
	function preview(img, selection) {
		var scaleX = cut_w / (selection.width || 1);
		var scaleY = cut_h / (selection.height || 1);
	    $('#preview_cut_photo').css({
			width: Math.round(scaleX * $('#ori_photo').width()) + 'px',
			height: Math.round(scaleY * $('#ori_photo').height()) + 'px',
			marginLeft: '-' + Math.round(scaleX * selection.x1) + 'px',
			marginTop: '-' + Math.round(scaleY * selection.y1) + 'px'
		});
	}

    function endSelect(img, selection) {
		 console.log(JSON.stringify(selection));
		 $('#x1').val(selection.x1);
		 $('#y1').val(selection.y1);
		 $('#x2').val(selection.x2);
		 $('#y2').val(selection.y2);
		 $('#w').val(selection.width);
		 $('#h').val(selection.height);
	}		
	</script>
</body>
</html>
