#!/usr/bin/php
<?php
require_once '/var/www/html/site/phpmodule/include/config.ini.php';
require_once '/var/www/html/site/phpmodule/class/site.ini.php';
require_once '/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php';

// require_once '\phpmodule\include\config.ini.php';
// require_once '\phpmodule\class\site.ini.php';
// require_once '\oauth\WeixinAPI\WeixinChat.class.php';

class CronJobScript
{
	function __construct()
	{
		$this->countryid = 2;
		$this->config = new config();
		require_once "saja/mysql.ini.php";
		// require_once "..\..\lib\saja\mysql.ini.php";
		$this->model = new mysql($this->config->db[0]);
		$this->model->connect();
	}

	function changeProductStatus($productid, $status)
	{
		//將商品資料更改為已結標
		$query = "
		UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
		SET
		closed = '{$status}'
		WHERE
		prefixid = '".$this->config->default_prefix_id."'
		AND productid = '{$productid}'
		AND switch = 'Y'
		";
                $this->log("Change Product Status : ".$query);
		$res = $this->model->query($query);
	}

	//流標
	//退殺幣是By userid, 針對當時所有下標但流標的商品, 下標金額加總後一起退 (不分商品productid, 也不分同一檔商品下了幾次標)
	function nobodyBid($productid)
	{
		$this->changeProductStatus($productid, 'NB');

		//取得使用者殺幣下標記錄
		$query = "
		SELECT
		distinct s.spointid
		,s.*
		FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
		LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s ON
			h.prefixid = s.prefixid
			and h.spointid = s.spointid
			and s.switch = 'Y'
		WHERE
		h.prefixid = '{$this->config->default_prefix_id}'
		AND h.productid = '{$productid}'
		AND type = 'bid'
		AND h.switch = 'Y'
		AND s.spointid IS NOT NULL
		";
		$table = $this->model->getQueryRecord($query);

		//統計使用者下標費用
		$spoint = array();
		if (!empty($table['table']['record'])) {
			foreach ($table['table']['record'] as $rk => $rv) {
				$userid = $rv['userid'];
				if (empty($spoint[$userid])) {
					$spoint[$userid]['countryid'] = $rv['countryid'];
					$spoint[$userid]['amount'] = 0.0;
				}

				$spoint[$userid]['amount'] += (float)$rv['amount'];
			}
		}

		//將手續費退還給使用者
		foreach ($spoint as $userid => $data)
		{
			$query = "
			INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint`
			(
				prefixid,
				userid,
				countryid,
				behav,
				amount,
				insertt
			)
			VALUE
			(
				'".$this->config->default_prefix_id."',
				'{$userid}',
				'{$data['countryid']}',
				'bid_refund',
				'".($data['amount'] * -1)."',
				NOW()
			)
			";
			$res = $this->model->query($query);
			$spointid = $this->model->_con->insert_id;

			$query = "
			INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history`
			(
				prefixid,
				productid,
				type,
				userid,
				spointid,
				insertt
			)
			VALUE
			(
				'".$this->config->default_prefix_id."',
				'".$this->product['productid']."',
				'refund',
				'{$userid}',
				'{$spointid}',
				NOW()
			)
			";
			$res = $this->model->query($query);
		}
	}

	//回饋使用者紅利
	function add_user_bonus($userid, $bonus, $behav)
	{
		$query = "
		INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus`
		(
			prefixid,
			userid,
			countryid,
			behav,
			amount,
			insertt
		)
		VALUE
		(
			'{$this->config->default_prefix_id}',
			'{$userid}',
			'{$this->countryid}',
			'{$behav}',
			'{$bonus}',
			NOW()
		)
		";
		$res = $this->model->query($query);
		if (!$res) {
			return false;
		}
		return $this->model->_con->insert_id;
	}

	//回饋使用者店點
	function add_user_gift($userid, $gift, $behav)
	{
		$query = "
		INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."gift`
		(
			prefixid,
			userid,
			countryid,
			behav,
			amount,
			insertt
		)
		VALUE
		(
			'{$this->config->default_prefix_id}',
			'{$userid}',
			'{$this->countryid}',
			'{$behav}',
			'{$gift}',
			NOW()
		)
		";
		$res = $this->model->query($query);
		if (!$res) {
			return false;
		}
		return $this->model->_con->insert_id;
	}

	//分潤
	function profitSharing($table) //---新增的部份
	{
		$total_bonus = 0.0;//總回饋紅利
		$total_count = 0;//總標數
		if (!empty($table['table']['record'])) {
			//紅利及店點回饋

			foreach($table['table']['record'] as $rk => $user) {

				//免費下標或是使用殺幣
				if ((float)$this->product['saja_fee'] == 0 || empty($user['sgiftid'])) {
					$total_count += (int)$user['count'];
				}

				if ($user['userid'] == $this->winner['userid']) {
					continue;//得標者不回饋
				}

				$bonusid = 0;
				if ($this->product['bonus_type'] != 'none') {
					$bonus = 0;
                    if($this->product['is_flash']=='Y') {
					    // 閃殺商品
						// 計算回饋商家紅利
						if ($this->product['bonus_type'] == 'ratio') {
							$bonus = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['bonus'] / 100;
						    // 總和(每次下標的出價)
							$bonus+=$user['bid_total'];
						}
						else if ($this->product['bonus_type'] == 'value') {
							// 每一標回饋固定金額
							$bonus = (int)$user['count'] * (float)$this->product['bonus'];
						}
						$total_bonus += $bonus;

						// 計算回饋使用者紅利(殺幣部份)---新增的部份
						if ($this->product['bonus_type'] == 'ratio') {
							$saja_bonus = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['bonus'] / 100;
						    // 總和(每次下標的出價)
							$saja_bonus+=$user['bid_total'];
						}
						else if ($this->product['bonus_type'] == 'value') {
							// 每一標回饋固定金額
							$saja_bonus = (int)$user['count'] * (float)$this->product['bonus'];
						}
						error_log("[script] ".$user['userid']." saja_bonus : ".$saja_bonus);
						if ($saja_bonus > 0) {
							//回饋使用者紅利
							$bonusid = $this->add_user_bonus($user['userid'], $saja_bonus, 'product_close');
						}
					} else {
						// 非閃殺商品
						// 計算回饋商家紅利
						if ($this->product['bonus_type'] == 'ratio') {
							$bonus = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['bonus'] / 100;
						}
						else if ($this->product['bonus_type'] == 'value') {
							$bonus = (int)$user['count'] * (float)$this->product['bonus'];
						}
						$total_bonus += $bonus;

						//計算回饋使用者紅利額度(殺幣部份)---新增的部份
						if ($this->product['bonus_type'] == 'ratio') {
							$saja_bonus = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['bonus'] / 100;
						}
						else if ($this->product['bonus_type'] == 'value') {
							$saja_bonus = (int)$user['count'] * (float)$this->product['bonus'];
						}
						if ($saja_bonus > 0) {
							//回饋使用者紅利
							$bonusid = $this->add_user_bonus($user['userid'], $saja_bonus, 'product_close');
						}
					}
				}

				$giftid = 0;
				/*  // 未使用
				if ($this->product['gift_type'] != 'none') {
					$gift = 0;

					//計算回饋店點額度
					if ($this->product['gift_type'] == 'ratio') {
						$gift = (int)$user['count'] * (float)$this->product['saja_fee'] * (float)$this->product['gift'] / 100;
					}
					else if ($this->product['gift_type'] == 'value') {
						$gift = (int)$user['count'] * (float)$this->product['gift'];
					}

					if ($gift > 0) {
						//回饋使用者店點
						$giftid = $this->add_user_gift($user['userid'], $gift, 'product_close');
					}
				}
                */
				$query = "
				INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."settlement_history`
				(
					prefixid,
					userid,
					productid,
					bonusid,
					giftid,
					insertt
				)
				VALUE
				(
					'{$this->config->default_prefix_id}',
					'{$user['userid']}',
					'{$this->product['productid']}',
					'{$bonusid}',
					'{$giftid}',
					NOW()
				)
				";
				$res = $this->model->query($query);
			}
		}

		if ((float)$this->product['saja_fee'] != 0) {
			$fee = (float)$this->product['saja_fee'];
		}
		else if ((float)$this->product['ads_fee'] != 0) {
			$fee = (float)$this->product['ads_fee'];
		} else {
			$fee = 0;
		}

		//廠商分潤資料
		$profit_base = $total_count * $fee
				+ $this->product['retail_price'] * (float)$this->product['process_fee'] * $this->config->sjb_rate
				+ $this->winner['price'] * $this->config->sjb_rate
				- $total_bonus
				- $this->product['base_value'];

		$profit_sharing = round($profit_base * $this->product['split_rate'], 2);//給供應商的分潤

		$profit = $profit_base - $profit_sharing;//經銷商的分潤

		//2018-08-10 判斷分潤是否為負
		if ($profit < 0){
			$profit = 0;
		}

		$query = "
		INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."profit_sharing`
		(
			prefixid,
			pscid,
			productid,
			profit,
			insertt
		)
		VALUES
		(
			'{$this->config->default_prefix_id}',
			'2',
			'{$this->product['productid']}',
			'{$profit}',
			NOW()
		),
		(
			'{$this->config->default_prefix_id}',
			'1',
			'{$this->product['productid']}',
			'{$profit_sharing}',
			NOW()
		)
		";
		$res = $this->model->query($query);
	}

	function getCloseProducts($productid)
	{

		if(!empty($productid) && $productid !=""){
			$rule = "productid ='{$productid}'";
		}else{
		  $rule = "((unix_timestamp(offtime) > 0 AND unix_timestamp() > unix_timestamp(offtime))
								OR (unix_timestamp(offtime) = 0 AND locked = 'Y'))";
		}

		$query = "
		SELECT
		*,
		unix_timestamp(offtime) as offtime
		FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
		WHERE
					prefixid = '".$this->config->default_prefix_id."'
					AND ".$rule."
					AND closed = 'N'
					AND display = 'Y'
					AND switch = 'Y'
		";

		return $this->model->getQueryRecord($query);
	}

	//取得得標者資料
	function getWinner($productid)
	{
		$query = "
	    SELECT
	    *
	    FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history`
	    WHERE
		prefixid = '{$this->config->default_prefix_id}'
		AND productid = '{$productid}'
		AND type = 'bid'
		AND switch = 'Y'
		GROUP BY price
		HAVING COUNT(*) = 1
		ORDER BY price ASC
		LIMIT 1
		";
		return $this->model->getQueryRecord($query);
	}

	//取得openid資料
	function getWechatopenid($userid)
	{
		$query = "SELECT * FROM `saja_user`.`v_user_profile_sso` WHERE userid='{$userid}' and sso_name='weixin' ORDER BY userid ASC LIMIT 1 ";
		error_log($query);
		return $this->model->getQueryRecord($query);
	}

	function getSajaHistory($productid)
	{
		//統計分潤下標記錄(一般殺價, 只算殺价幣的標數)

	    // LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s ON

		$query = " SELECT h.userid, count(*) as count, h.sgiftid
		             FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
	                 JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
					   ON h.prefixid = s.prefixid
	    	          AND h.spointid = s.spointid
	    	          AND s.switch = 'Y'
		            WHERE h.prefixid = '{$this->config->default_prefix_id}'
		              AND h.productid = '{$productid}'
		              AND h.type = 'bid'
		              AND h.switch = 'Y'
                      AND h.spointid > 0
		         GROUP BY h.userid, h.sgiftid ";
		return $this->model->getQueryRecord($query);
	}

	function getSajaHistory_flash($productid)
	{
		//統計分潤下標記錄(閃殺, 只要有下標就算)
	   	$query = "
		  SELECT h.userid, count(*) as count, h.sgiftid
		    FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
	    LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
		      ON h.prefixid = s.prefixid
	    	 AND h.spointid = s.spointid
	    	 AND s.switch = 'Y'
		   WHERE h.prefixid = '{$this->config->default_prefix_id}'
		     AND h.productid = '{$productid}'
		     AND h.type = 'bid'
		     AND h.switch = 'Y'
		GROUP BY h.userid, h.sgiftid ";

		return $this->model->getQueryRecord($query);
	}

	//---新增的部份
	function getSajaDollarsHistory($productid)
	{
		//統計分潤下標記錄(for 殺幣)
	       /*  ori
               $query = "
		SELECT
		h.userid, count(*) as count, h.sgiftid
		FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
	    LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s ON
	    	h.prefixid = s.prefixid
	    	AND h.spointid = s.spointid
	    	AND s.switch = 'Y'
		WHERE
		h.prefixid = '{$this->config->default_prefix_id}'
		AND h.productid = '{$productid}'
		and h.spointid != '0'
		AND h.type = 'bid'
		AND h.switch = 'Y'
		GROUP BY h.userid, h.sgiftid
		";
                */
        $query = " SELECT h.userid, count(*) as count, h.sgiftid, sum(price) as bid_total
                FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
            LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s ON
                h.prefixid = s.prefixid
                AND h.spointid = s.spointid
                AND s.switch = 'Y'
                WHERE
                h.prefixid = '{$this->config->default_prefix_id}'
                AND h.productid = '{$productid}'
                and h.spointid != '0'
                AND h.type = 'bid'
                AND h.switch = 'Y'
                GROUP BY h.userid, h.sgiftid ";
                error_log("[getSajaDollarsHistory] : ".$query);
		return $this->model->getQueryRecord($query);
	}


	function closeProduct()
	{

		//商家id
		$auth_id	= empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);

		//商品編號
    $productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);

    //手動結標密碼
		$sec_key	= empty($_POST['sec_key']) ? htmlspecialchars($_GET['sec_key']) : htmlspecialchars($_POST['sec_key']);

		//手動結標密碼正確才可使用單一商品結標功能
    if($sec_key != md5("qazwsxedc")){
		   $productid = "";
		}
	  //echo $sec_key;

		//取得需結標之商品
		$table = $this->getCloseProducts($productid);
		if (empty($table['table']['record'])) {

			$this->log("No Product to Close !!");
			//die();
			$data['retCode'] = -2;
			$data['retMsg'] = "查無可結標商品";
			$data['retType'] = "MSG";

			echo json_encode($data,JSON_UNESCAPED_UNICODE);

			return;
		}

		//檢查用單一商品結標功能結標者與提供者是否相同
		if (!empty($productid) && $productid !="") {

			if($table['table']['record'][0][vendorid] != $auth_id){
        $this->log("Your ID: ".$auth_id." is Diffrent From Vendor ID !!");
				//die();
				$data['retCode'] = -3;
				$data['retMsg'] = "商家與商品提供者不同無法結標";
				$data['retType'] = "MSG";

				echo json_encode($data,JSON_UNESCAPED_UNICODE);

				return;
			}

		}

 

      //將得標者寫入得標紀錄
			$this->winner = $recArr['table']['record'][0];

			$query = "
			INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product`
			(
				prefixid,
				productid,
				userid,
				price,
				insertt
			)
			VALUE
			(
				'".$this->config->default_prefix_id."',
				'{$this->winner['productid']}',
				'{$this->winner['userid']}', 
				'{$this->winner['price']}',
				NOW()
			)
			";

			$res = $this->model->query($query);
			$this->profitSharing($saja_history);
			$this->changeProductStatus($product['productid'], 'Y');

			// Wechat通知中標者
			// 20150821 By Thomas
			try {
				$options['appid'] = 'wxbd3bb123afa75e56';
			  $options['appsecret'] = '367c4259039bf38a65af9b93d92740ae';
				$wx=new WeixinChat($options);
				$sso=$this->getWechatopenid($this->winner['userid']);
				if (!empty($sso['table']['record'])) {
					if(!empty($sso['table']['record'][0]['uid'])) {
						//$msg='【最终中标通知】 恭喜你！干掉数以百位的杀友。成为最终<a href="http://www.saja.com.tw/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$product['productid'].'">《'.$product['name'].'》</a>的中标者。请记得在七天内，至「会员」「交易纪录」「中标纪录」进行「前往结账」完成最终领取商品的程序。';
						$msg='【最终中标通知】 恭喜你！干掉数以百位的杀友。成为最终<a href="http://test.shajiawang.com/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$product['productid'].'">《'.$product['name'].'》</a>的中标者。请记得在七天内，至「会员」「交易纪录」「中标纪录」进行「前往结账」完成最终领取商品的程序。';
            //$msg='【最终中标通知】 恭喜你！干掉数以百位的杀友。成为最终<a href="http://qc.saja.tw/wx_auth.php?jdata=gotourl:/site/product/saja/?productid='.$product['productid'].'">《'.$product['name'].'》</a>的中标者。请记得在七天内，至「会员」「交易纪录」「中标纪录」进行「前往结账」完成最终领取商品的程序。';
						error_log("[script]notify userid: ".$sso['table']['record'][0]['userid']." wechat : ".$sso['table']['record'][0]['uid']);
						$wx->sendCustomMessage('oNmoZtxOu0pKnlUjZjO0beFCSfEY',$msg,'text');
						$wx->sendCustomMessage($sso['table']['record'][0]['uid'],$msg,'text');
					}
				}
			} catch (Exception $e) {
			  error_log($e->getMessage());
			}

			// 呼叫建立bid_info靜態網頁的function
			// 20150714 By Thomas
			$ch = curl_init();
			//curl_setopt($ch, CURLOPT_URL, "http://www.saja.com.tw/site/bid/genBidDetailHtml/?productid=".$product['productid']."&type=deal&bg=Y");
			curl_setopt($ch, CURLOPT_URL, "http://test.shajiawang.com/site/bid/genBidDetailHtml/?productid=".$product['productid']."&type=deal&bg=Y");
			//curl_setopt($ch, CURLOPT_URL, "http://qc.saja.tw/site/bid/genBidDetailHtml/?productid=".$product['productid']."&type=deal&bg=Y");
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			$ret=curl_exec($ch);
			curl_close($ch);
			//echo ("Gen bid info HTML of prod ".$product['productid']." :".$ret);
			$this->log("Gen bid info HTML of prod ".$product['productid']." :".$ret);
			// 拿掉得標者redis cache
			// $redis = new Redis();
			// $redis->connect('127.0.0.1');
			// $redis->del('PROD:'.$product['productid'].':BIDDER');

			$data['retCode'] = 1;
			$data['retMsg'] = "結標完成";
			$data['retType'] = "MSG";

		}

		echo json_encode($data,JSON_UNESCAPED_UNICODE);

		return;

	}

        function log($msg) {
	     	 //echo(date("Y-m-d H:i:s")."--".$msg."\n");
				 // $file = fopen("script_log.txt","w");
         // echo fwrite($file,date("Y-m-d H:i:s")."--".$msg."\n");
         // fclose($file);
				 file_put_contents("script_log.txt", date("Y-m-d H:i:s")."--".$msg."\n", FILE_APPEND | LOCK_EX);
	}
}

$script = new CronJobScript();
$script->closeProduct();

