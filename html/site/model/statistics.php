<?php

/*
* Statistics Model 模組
*/
class StatisticsModel {

	public function __construct() {
	}

	/*
	*	取得使用者使用裝置記錄
	*	$userid			int			會員編號
	*/
	public function get_user_device($userid) {

		global $db, $config;

		$query = "SELECT *
								FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_device`
								WHERE userid = '{$userid}'
								AND `switch` = 'Y'
							";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}else{
			return 'FALSE';
		}

	}


	/*
	*	寫入使用者使用裝置記錄
	*	$userid			int			會員編號
	*	$device		chr			裝置類型
	*/
	public function insert_user_device($userid, $device) {

		global $db, $config;

		switch($device) {
				case android:
					$process = "Y";
				  break;
				case ios:
					$process = "Y";
				  break;
				case web:
					$process = "Y";
					break;
				default:
					$process = "N";
				  break;
		}

		if($process == "Y") {
				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_device`
													SET userid = '{$userid}',
															{$device} = NOW(),
															insertt = NOW()
									";
				$db->query($query);
				return 'TRUE';
		}else{
				return 'FALSE';
		}

	}

	/*
	*	更新使用者使用裝置記錄
	*	$userid			int			會員編號
	*	$device		chr			裝置類型
	*/
	public function update_user_device($userid, $device) {

		global $db, $config;

		switch($device) {
				case android:
					$process = "Y";
				  break;
				case ios:
					$process = "Y";
				  break;
				case web:
					$process = "Y";
					break;
				default:
					$process = "N";
				  break;
		}

		if($process == "Y") {
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_device`
											SET {$device} = NOW()
										WHERE userid = '{$userid}'
									";
				$db->query($query);
				return 'TRUE';
		}else{
				return 'FALSE';
		}

	}

	/*
	*	計算使用者使用裝置
	*/
	public function count_user_device() {

		global $db, $config;

				$query = "SELECT COUNT(`userid`) AS user_count, sum(IF(`android` IS NOT NULL, 1, 0)) AS android_count, sum(IF(`ios` IS NOT NULL, 1, 0)) AS ios_count, sum(IF(`web` IS NOT NULL, 1, 0)) AS web_count
									FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_device`
									";

 				$table = $db->getQueryRecord($query);

				if(!empty($table['table']['record']) && $table['table']['record'][0]['user_count'] > 0) {
					return $table['table']['record'];
				}else{
					return 'FALSE';
				}

	}

	/*
	*	紀錄老人回娘家活動來源
	*/
	public function set_activity_source($source_num = '', $phone = '0') {

		global $db, $config;

		$query = " INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}activity_statistics` 
				SET source = '{$source_num}',
				phone = '{$phone}',
				insertt = NOW() 
			";
		$db->query($query);

	}

}
?>
