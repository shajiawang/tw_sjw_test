<?php
/**
 * User Model 模組
 *
 * This class contains all of the functions used for creating, managing and deleting
 * users.
 */

class UserModel {
    public $user_id = 0;
    public $name = "Guest";
    public $email = "Guest";
    public $ok = false;
    public $msg = "You have been logged out!";
    public $is_logged = false;
	public $str;

    /*
     *	Set all internal publiciables to 'Guest' status, then check to see if
     *	a user session or cookie exists.
     */
    public function __construct() {
        if(!$this->check_session()) {
			$this->check_cookie();
		}
		return $this->ok;
    }

    /*
     *	檢查會員是否已登入,並存在SESSION值
     */
    public function check_session() {
		if(!empty($_SESSION['auth_id']) && !empty($_SESSION['auth_secret'])) {
            return $this->check($_SESSION['auth_id'], $_SESSION['auth_secret']);
        } else {
            return false;
		}
    }

    /*
     *	Check to see if any cookies exist on the user's computer/browser.
     */
    public function check_cookie() {
		if(!empty($_COOKIE['auth_id']) && !empty($_COOKIE['auth_secret'])) {
            return $this->check($_COOKIE['auth_id'], $_COOKIE['auth_secret']);
        } else {
            return false;
		}
    }

	/*
     *	Validate the user's email address.
	 *	$email			varchar				郵件信箱
     */
	public function validEmail($email) {
		$isValid = true;
        $atIndex = strrpos($email, "@");

		if(is_bool($atIndex) && !$atIndex) {
            $isValid = false;
        } else {
            $domain = substr($email, $atIndex+1);
            $local = substr($email, 0, $atIndex);
            $localLen = strlen($local);
            $domainLen = strlen($domain);

			if ($localLen < 1 || $localLen > 64) {
                $isValid = false;
            } else if($domainLen < 1 || $domainLen > 255) {
                $isValid = false;
            } else if($local[0] == '.' || $local[$localLen-1] == '.') {
                $isValid = false;
            } else if(preg_match('/\\.\\./', $local)) {
                $isValid = false;
            } else if(!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
                $isValid = false;
            } else if(preg_match('/\\.\\./', $domain)) {
                $isValid = false;
            } else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
                if(!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
                    $isValid = false;
                }
            }

			if($isValid && !(checkdnsrr($domain,"MX") ||  checkdnsrr($domain,"A"))) {
                $isValid = false;
            }
        }
        return $isValid;
    }

    /*
     *	Check to see if the user is logged in based on their session data.
     */
    public function is_logged()	{
		$logged = $this->check($_SESSION['auth_id'], $_SESSION['auth_secret']); //$_SESSION['auth_email']

		if($logged) {
			return true;
        } else {
			return false;
		}
    }

	/*
     *	This function checks the session/cookie info to see if it's real by comparing it
     *	to what is stored in the database.
	 *	$id			int				會員編號
	 *	$secret		varchar			加密編碼
     */
    public function check($id, $secret) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		// Get the user's info from the database.
        $query = "SELECT u.*, up.nickname
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
			u.prefixid = up.prefixid
			AND u.userid = up.userid
			AND up.`switch`='Y'
		WHERE
			u.prefixid = '{$config['default_prefix_id']}'
			AND u.userid = '{$id}'
			AND u.switch = 'Y'
			AND up.userid IS NOT NULL
		";
		$table = $db->getQueryRecord($query);

		//判斷是否有相關資料
		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'][0];
			//$md5 = md5($results['userid'] . $results['name']);
			//if($md5 == $secret)

			//判斷會員編號是否相同
			if($results['userid'] == $id) {
				$this->user_id = $results['userid'];
                $this->email = $results['email'];
                $this->name = $results['nickname'];
                $this->ok = true;
                $this->is_logged = true;
                return true;
            }
		}
        return false;
    }

    /*
     *	This function checks the session/cookie info to see if it's real by comparing it
     *	to what is stored in the database.
	 *	$id			int				會員編號
	 *	$agree		varchar			是否同意
     */
    public function update_agreement($userid,$agree){
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();
        $query = "SELECT userid FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` WHERE userid = '{$userid}' AND switch = 'Y'";
		$table = $db->getQueryRecord($query);
		//判斷是否有相關資料
		if(!empty($table['table']['record'])) {
			$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` set agree='{$agree}' WHERE  userid='{$userid}'";
			$result=$db->query($sql);
			if ($result){
				return 1;
			}else{
				return -2;
			}
		}else{
			return -1;
		}

    }

	/*
     *	2016/05/06
	 *	檢查Login的id是否為宣稱的手機號碼
	 *	2019/09/28
	 *	先檢查該手機號 已驗證的資料是否超過三筆
	 *	$id				int				會員編號
	 *	$phone			varchar			手機號碼
	 */
	public function validUserAuth($id, $phone) {

	    global $db, $config;

		$query = "SELECT name
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		WHERE `prefixid` = '{$config['default_prefix_id']}'
			AND `userid` = '{$id}'
			AND `switch` = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			return false;
		} else {
			// error_log("[model/validUserAuth] phone validate : ".$id."->".$phone);
			// Add By Thomas 2019/09/28 先檢查該手機號已驗證的資料是否超過三筆
			// $query = "SELECT count(authid) as cnt
			// 	FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			// 	WHERE `prefixid` = '{$config['default_prefix_id']}'
			// 		AND `phone`='{$phone}'
			// 		AND `verified`='Y'
			// 		AND `switch` = 'Y'
			// ";
			// $table = $db->getQueryRecord($query);
			// error_log("[model/validUserAuth] phone validated cnt : ".$table['table']['record'][0]['cnt']);
			// if($table['table']['record'][0]['cnt']>=3) {
			// 	// 視為已驗證過
			//     return array('verified'=>'Y');
			// }
			// add End

			$query = "SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$id}'
				AND `switch` = 'Y'
			";
			$table = $db->getQueryRecord($query);

			if(!empty($table['table']['record'])) {
				return $table['table']['record'][0];
			} else {
				//新增SMS check code
				$shuffle = get_shuffle();
				$checkcode = substr($shuffle, 0, 6);

				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$id}',
					`phone`='{$phone}',
					`code`='{$checkcode}',
					`verified`='N',
					`insertt`=NOW()
				";
				$db->query($query);

				$query2 = "SELECT *
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				WHERE
					`prefixid` = '{$config['default_prefix_id']}'
					AND `userid` = '{$id}'
					AND `switch` = 'Y'
				";
				$table = $db->getQueryRecord($query2);

				return $table['table']['record'][0];
			}
		}
    }

	/*
	 *	2016/05/06
	 *	改成檢查Login的id是否為宣稱的手機號碼
	 *	$id				int				會員編號
	 *	$phone			varchar			手機號碼
	 */
    /*
	public function validUserAuth($id, $phone) {

	    global $db, $config;

		$query = "SELECT name
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		WHERE `prefixid` = '{$config['default_prefix_id']}'
			AND `userid` = '{$id}'
			AND `switch` = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			return false;
		} else {
			$query = "SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$id}'
				AND `switch` = 'Y'
			";
			$table = $db->getQueryRecord($query);

			if(!empty($table['table']['record'])) {
				return $table['table']['record'][0];
			} else {
				//新增SMS check code
				$shuffle = get_shuffle();
				$checkcode = substr($shuffle, 0, 6);

				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$id}',
					`phone`='{$phone}',
					`code`='{$checkcode}',
					`verified`='N',
					`insertt`=NOW()
				";
				$db->query($query);

				$query2 = "SELECT *
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				WHERE
					`prefixid` = '{$config['default_prefix_id']}'
					AND `userid` = '{$id}'
					AND `switch` = 'Y'
				";
				$table = $db->getQueryRecord($query2);

				return $table['table']['record'][0];
			}
		}
    }
    */
	/*
	 *	2016/06/23
	 *	手機驗證檢查
	 *	$userid			int			使用者編號
	 */
	public function validSMSAuth($userid) {
	    global $db, $config;

		$query = "SELECT name
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		WHERE `prefixid` = '{$config['default_prefix_id']}'
			AND `userid` = '{$userid}'
			AND `switch` = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			return false;
		} else {
			$query = "SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$userid}'
				AND `switch` = 'Y'
			";
			$table = $db->getQueryRecord($query);

			if(!empty($table['table']['record'])) {
				return $table['table']['record'][0];
			} else {
				return false;
			}
		}
    }

    /*
	 *	2016/05/06
	 *	改成檢查Login的id是否為宣稱的手機號碼
	 *	$id				int				會員編號
	 *	$phone			varchar			手機號碼
	 */
	public function validUserAuth2($id, $phone) {
		global $db, $config;

		$query = "SELECT *
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
		WHERE `prefixid` = '{$config['default_prefix_id']}'
			AND `phone`= '{$phone}'
			AND `userid` = '{$id}'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			// 找到有驗證過的資料=>回傳該筆資料
			return $table['table']['record'][0];
		} else {
			// 找不到有驗證過的資料=>新增一筆verified='N'且回傳
			//新增SMS check code
			$shuffle = get_shuffle();
			$checkcode = substr($shuffle, 0, 6);
			$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			SET	`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$id}',
				`phone`='{$phone}',
				`code`='{$checkcode}',
				`verified`='N',
				`insertt`=NOW()
			";
			$db->query($query);
			error_log("[m/user/validUserAuth]:".$query);
			$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
					   WHERE `prefixid` = '{$config['default_prefix_id']}'
						 AND `phone`= '{$phone}'
						 AND `userid` = '{$id}' ";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])) {
			   return $table['table']['record'][0];
			} else {
			   return false;
			}
		}
    }

    /*
	 *	取得會員基本資料
	 *	$id				int				會員編號
	 */
	public function get_user_profile($id, $get_profile='') {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();
        
		//$query ="SELECT userid, nickname, thumbnail_file, thumbnail_url, gender, countryid,	regionid, provinceid, channelid, area, address, addressee, phone,	rphone, bonus_noexpw, src_from, agree
		//FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		//WHERE `prefixid` = '{$config['default_prefix_id']}' AND userid = '{$id}' AND switch = 'Y' ";
		
		$sql = empty($get_profile) ? "*" : " userid, nickname, is_fake, thumbnail_file, thumbnail_url, gender, countryid,	
				regionid, provinceid, channelid, area, address, addressee, phone,	rphone, bonus_noexpw, src_from, agree ";
		
		$query ="SELECT {$sql} FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		          WHERE	`prefixid` = '{$config['default_prefix_id']}'
			        AND userid = '{$id}'
			        AND switch = 'Y' ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			$table['table']['record'][0]['nickname']=urldecode($table['table']['record'][0]['nickname']);
            return $table['table']['record'][0];
		}
		return false;
    }

	/*
	 *	會員註冊資料
	 *	$id					int				會員編號
	 *	$loginid			int				會員帳號
	 *	$user_type			int				會員類型
	 */
	public function get_user($id='', $loginid='', $user_type='P') {
		global $config;

		if(empty($id) && empty($loginid)) {
		   return false;
		}
		$db = new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT *
				   FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
				  WHERE `prefixid` = '{$config['default_prefix_id']}'
				    AND switch = 'Y' ";
		if(!empty($id)) {
		   $query.=" AND userid='{$id}' ";
		}
		if(!empty($loginid)) {
		   $query.=" AND name='{$loginid}' ";
		}
		if(!empty($user_type)) {
		   $query.=" AND user_type='{$user_type}' ";
		}
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }

    /*
	 * 會員其他資訊分類(Frank.Kao-14/11/18)
	 */
    public function get_user_extrainfo_category() {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

    	$query = "select *
    	FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo_category`
    	where
    		`prefixid` = '{$config['default_prefix_id']}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }

    /*
	 * 會員其他資訊(Frank.Kao-14/11/18)
	 *	$uecid			int				會員
	 *	$uid			int				會員編號
	 */
    public function get_user_extrainfo($uecid,$uid='') {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		if(!empty($uid)) {
		   $userid=$uid;
		} else {
		   $userid=$_SESSION['auth_id'];
		}
    	$query = "select ueid, userid, uecid, uecid as seq, field1name, field2name, field3name, field4name, field5name, field6name, field7name, field8name
        	FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo`
    	where
    		`prefixid` = '{$config['default_prefix_id']}'
    		and userid = '{$userid}'
    		and uecid = '{$uecid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }

    /*
	 *	會員其他資訊(Frank.Kao-14/11/18)
	 *	$uid			int				會員編號
	 */
    public function get_user_extrainfo2($uid='') {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		if(!empty($uid)) {
		   $userid=$uid;
		} else {
		   $userid=$_SESSION['auth_id'];
		}
        $query = "SELECT IFNULL(ue.ueid,'') as ueid, IFNULL(ue.userid,'') as userid, uec.uecid, uec.name, IFNULL(ue.uecid,'') as seq,
		IFNULL(ue.field1name,'') as field1name, IFNULL(ue.field2name,'') as field2name, IFNULL(ue.field3name,'') as field3name, IFNULL(ue.field4name,'') as field4name, IFNULL(ue.field5name,'') as field5name, IFNULL(ue.field6name,'') as field6name, IFNULL(ue.field7name,'') as field7name, IFNULL(ue.field8name,'') as field8name,
		IFNULL(uec.field1name,'') as field1Title, IFNULL(uec.field2name,'') as field2Title, IFNULL(uec.field3name,'') as field3Title, IFNULL(uec.field4name,'') as field4Title, IFNULL(uec.field5name,'') as field5Title, IFNULL(uec.field6name,'') as field6Title, IFNULL(uec.field7name,'') as field7Title, IFNULL(uec.field8name,'') as field8Title
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo_category` uec
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` ue ON
			uec.prefixid = ue.prefixid
			AND uec.uecid = ue.uecid
			AND ue.userid = '{$uid}'
    	where
    		uec.switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }

    /*
	 * 確認會員其他資訊(Frank.Kao-14/11/25)
	 */
    public function check_user_extrainfo_category() {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

    	$query = "SELECT uec.uecid, uec.name, uec.field1name, uec.field2name, uec.field3name, uec.field4name,
						 uec.field5name, uec.field6name, uec.field7name, uec.field8name, uec.flag
    	FROM `saja_user_extrainfo_category` uec
    	LEFT JOIN `saja_user_extrainfo` ue ON
    		ue.prefixid = uec.prefixid
    		AND ue.uecid = uec.uecid
    		AND ue.userid = '{$_SESSION['auth_id']}'
    		and ue.switch = 'Y'
    	WHERE
    		uec.prefixid = 'saja'
    		AND uec.switch = 'Y'

    		and uec.webshow = 1
    	";
    	$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }

	/*
	 *	取得所在地相關資料
	 *	Add By Thomas 20141216
	 */
	public function getMaxid() {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$sql = "SELECT ";
		$sql.= "(SELECT max(countryid) FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}country` where switch='Y') as max_country_id, ";
		$sql.= "(SELECT max(zoneid) FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}zone` where switch='Y') as max_zone_id, ";
		$sql.= "(SELECT max(channelid) FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel` where switch='Y') as max_channel_id ";
		$sql.= " FROM DUAL ";
		$table = $db->getQueryRecord($sql);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false;
	}

	/*
	 * check_id_verified
	 */
	public function check_id_verified($idnum) {
        global $db, $config;
        $crypt_id=encrypt($idnum);
		$query = "SELECT userid
		FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}user_profile`
		WHERE
			prefixid = '{$config["default_prefix_id"]}'
			AND (idnum='{$idnum}' or idnum='{$crypt_id}')
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return true;
		}else{
			return false;
		}
    }

    /*
	 *	取得特定國家相關資料
	 *	Add By Thomas 20141216
	 *	$countryid			int				國家編號
	 */
	public function getChannelList($countryid) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$sql = "SELECT c.countryid, c.name as country_name, c.isocountrycode, c.countrycode ";
		$sql.= " , z.zoneid, z.name as zone_name ";
		$sql.= " ,ch.channelid, ch.name as channel_name ";
		$sql.= "  FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}country` c ";
		$sql.= "  JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}zone` z ON c.countryid=z.countryid AND c.switch='Y' and z.switch='Y' ";
		$sql.= "  JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel` ch ON z.zoneid=ch.zoneid AND z.switch='Y' and ch.switch='Y' ";
        $sql.= " WHERE 1=1 ";

		if(!empty($countryid)) {
			$sql.= "   AND c.countryid=${countryid} ";
		}

		$sql.=" ORDER BY c.countryid asc, z.zoneid asc, ch.channelid asc ";

		$table = $db->getQueryRecord($sql);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 *	推薦好友清單
	 *	$userid		int			會員編號
	 *	$nickname	varchar		搜尋名稱
	 * 	$sIdx		int		 	目前頁碼
	 * 	$perpage	int 		筆數
 	 */
	public function getReferralList($userid, $nickname='', $sIdx='', $perpage='') {
		global $config, $db;

		//SQL指令 - 取得同推薦人的好友清單
		/*
		$query = "SELECT ua.`userid`, ua.`insertt`, up.`nickname`, up.`thumbnail_file`, up.`thumbnail_url`, up.`gender`
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` ua
				LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up  ON ua.userid = up.userid
				WHERE ua.intro_by = '{$userid}'
					AND ua.userid <> '{$userid}'
					AND ua.`intro_by` >0
					AND ua.`promoteid` ='8'
					AND ua.`switch` = 'Y'
					AND ua.`insertt` > '2019-09-24 12:00:00' ";
		*/
		//SQL指令 - 取得同推薦人的好友清單
		$query = "SELECT ua.`userid`, ua.`insertt`, up.`nickname`, up.`thumbnail_file`, up.`thumbnail_url`, up.`gender`
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` ua
				LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up  
				   ON ua.userid = up.userid
				WHERE ua.intro_by = '{$userid}'
					AND ua.userid <> '{$userid}'
					AND ua.`intro_by` >0
					AND ua.`switch` = 'Y'
					AND ua.`insertt` > '2019-09-24 12:00:00' ";
		if (!empty($nickname)) {
			$query .= " AND up.nickname LIKE '%{$nickname}%'";
		}

		$query .= " ORDER BY ua.userid desc ";
		//error_log("[m/user/getReferralList]:".$query);

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			if(!empty($perpage) && !empty($sIdx)) {
				$query_limit.= " LIMIT ".$sIdx.",".$perpage;
			} else {
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/".$tv['thumbnail_file'];
				} elseif(empty($tv['thumbnail_url']) && empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/_DefaultHeadImg.jpg";
				}
				unset($table['table']['record'][$tk]['thumbnail_file']);
			}

			return $table;
		}

		return false;
	}

	/*
	 *	新增連線來源等資訊
	 *	Add By Thomas 2015/07/01
	 *	$come_from			varchar				連線來源
	 *	$productid 			int					商品編號
	 *	$act				varchar				執行動作
	 *	$goto				varchar				前往連結
	 *	$memo				varchar				備註
	 *	$userid				int					會員編號
	 *	$useragent			varchar				連線種類
	 *	$intro_by			varchar				來源編號
	 */
	public function logAffiliate($come_from, $productid, $act, $goto, $memo, $userid='', $user_agent='', $intro_by='') {
		global $config;
		if($act=='PROD|VIEW') {
		    error_log("userid : ".$userid." ".$act." productid :".$productid);
    		return 1;
        }
		$db=new mysql($config["db2"]);
		$db->connect();

		if(intval($intro_by) < intval($userid) ){
			$intro_user = $intro_by;
		} else {
			$intro_user = ''; 
		}
			$sql=" INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
						SET come_from='{$come_from}',
						    intro_by='{$intro_user}',
							userid='{$userid}',
							productid='{$productid}',
							user_agent='{$user_agent}',
	                        act='{$act}',
							goto='{$goto}',
							memo='{$memo}',
							insertt=NOW(),
							switch='Y' ";
			return $db->query($sql);
	}

	/*
	 *	統計連線來源筆數
	 *	$userid				int					會員編號
	 */
	public function  countRecommandUsers($userid) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$sql = "SELECT count(distinct(userid)) as cnt
				 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
				WHERE act='REG'
				  AND switch='Y'
				  AND intro_by='{$userid}' ";

		$table = $db->getQueryRecord($sql);
		if(!empty($table['table']['record'][0])) {
		   return $table['table']['record'][0]['cnt'];
		} else {
		   return false;
		}
	}

	/*
	 *	新增會員關注商品
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態
	 */
	public function createUserCollectData($userid, $productid, $prodtype, $switch) {
		global $config;
		$db2 = new mysql($config["db2"]);
		$db2->connect();
		$sql=" INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_collect_rt`
			  SET userid='{$userid}',
				  productid='{$productid}',
				  prod_type='{$prodtype}',
				  switch='{$switch}',
				  insertt=NOW() ";
		$db2->query($sql);
		$id=mysqli_insert_id($db2->_con);
		return $id;
	}

	/*
	 *	更新會員關注商品
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態
	 */
	public function updateUserCollectData($switch, $ucrid, $userid='', $productid='') {
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_collect_rt`
				 SET switch='{$switch}'
			   WHERE 1=1 ";
		if(!empty($ucrid)) {
		  $sql.= " AND ucrid='{$ucrid}' ";
		}
		if(!empty($userid)) {
		  $sql.= " AND userid='{$userid}' ";
		}
		if(!empty($productid)) {
		  $sql.= " AND productid='{$productid}' ";
		}
		return $db->query($sql);
	}

	/*
	 *	取得會員關注商品資料
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態
	 */
	public function getUserCollectData($userid, $productid='', $prodtype='', $switch='') {
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$query = "SELECT * FROM saja_user.saja_user_collect_rt rt WHERE 1=1 ";
		if(!empty($userid)) {
		   $query.=" AND userid='{$userid}' ";
		}
		if(!empty($prodtype)) {
		   $query.=" AND prod_type='{$prodtype}' ";
		}
		if(!empty($switch)) {
		   $query.=" AND switch='{$switch}' ";
		}
		if(!empty($productid)) {
		   $query.=" AND productid='{$productid}' ";
		}

		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
	}

	/*
	 *	取得會員關注清單
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態
	 */
	public function getCollectProdList($userid, $productid='', $prodtype='', $switch='') {
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$query = "SELECT rt.ucrid, rt.userid, rt.productid, rt.switch, p.name, p.retail_price, p.thumbnail_url, unix_timestamp(p.offtime) as offtime,
		                 IFNULL(pt.filename,'') as filename, p.ptid
				  FROM saja_user.saja_user_collect_rt rt
				  JOIN saja_shop.saja_product p ON
						rt.productid=p.productid
				  LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
						p.prefixid = pt.prefixid
						AND p.ptid = pt.ptid
						AND pt.switch = 'Y'
				  WHERE 1=1 ";
		if(!empty($userid)) {
		   $query.=" AND rt.userid='{$userid}' ";
		}
		if(!empty($prodtype)) {
		   $query.=" AND rt.prod_type='{$prodtype}' ";
		}
		if(!empty($switch)) {
		   $query.=" AND rt.switch='{$switch}' ";
		}
		if(!empty($productid)) {
		   $query.=" AND rt.productid='{$productid}' ";
		}

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$tv['filename'];
				}
			}

			return $table['table'];
		}

	}

	/*
	 *	取得會員簡訊驗証資料
	 *	$arrCond			array			 相關參數
	 */
	public function getSmsAuthData($arrCond) {

		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				  WHERE 1=1 ";
		if(!empty($arrCond['phone'])) {
			$query.= " AND `phone`='{$arrCond['phone']}' ";
		}
		if(!empty($arrCond['code'])) {
			$query.= " AND `code`='{$arrCond['code']}' ";
		}
		if(!empty($arrCond['authid'])) {
			$query.= " AND `authid`={$arrCond['authid']} ";
		}
		if(!empty($arrCond['userid'])) {
			$query.= " AND `userid`={$arrCond['userid']} ";
		}
		if(!empty($arrCond['switch'])) {
			$query.= " AND `switch`='{$arrCond['switch']}' ";
		}
		if(!empty($arrCond['verified'])) {
			$query.= " AND `verified`='{$arrCond['verified']}' ";
		}
		if(!empty($arrCond['user_type'])) {
			$query.= " AND `user_type`='{$arrCond['user_type']}' ";
		}
		$query.=" ORDER BY authid desc ";

		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}
	}

	/*
	 *	新增會員簡訊驗証資料
	 *	$arrUpd				array			 新增內容
	 */
	public function createSmsAuthData($arrUpd) {

		global $db,$config;

		$sql = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
		 SET `prefixid`='{$config['default_prefix_id']}', `insertt`=NOW()";
		if(!empty($arrUpd['code'])) {
			$sql.=",`code`='{$arrUpd['code']}' ";
		}
		if(!empty($arrUpd['phone'])) {
			$sql.=", `phone`='{$arrUpd['phone']}' ";
		}
		if(!empty($arrUpd['userid'])) {
			$sql.=", `userid`='{$arrUpd['userid']}' ";
		}
		if(!empty($arrUpd['verified'])) {
			$sql.=", `verified`='{$arrUpd['verified']}' ";
		}
		if(!empty($arrUpd['user_type'])) {
			$sql.=", `user_type`='{$arrUpd['user_type']}' ";
		}
		$db->query($sql);
		$authid=mysqli_insert_id($db->_con);
		error_log("[m/user/createSmsAuthData]code:".$arrUpd['code'].", phone:".$arrUpd['phone'].", userid:".$arrUpd['userid'].", authid:".$authid);
		return $authid;
	}

	/*
	 *	刪除會員簡訊驗証資料
	 *	$arrCond				array			 刪除條件
	 */
	public function delSmsAuthData($arrCond) {
		global $db,$config;
		$sql = "DELETE * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` ";
		$sql.=" WHERE 1=1 ";
		if(!empty($arrCond['authid'])) {
			$sql.=" AND `authid`='{$arrCond['authid']}' ";
		}
		if(!empty($arrCond['phone'])) {
			$sql.=" AND `phone`='{$arrCond['phone']}' ";
		}
		if(!empty($arrCond['userid'])) {
			$sql.=" AND `userid`='{$arrCond['userid']}' ";
		}
		if(!empty($arrCond['code'])) {
			$sql.=" AND `code`='{$arrCond['code']}' ";
		}
		if(!empty($arrCond['verified'])) {
			$sql.=" AND `verified`='{$arrCond['verified']}' ";
		}
		if(!empty($arrUpd['user_type'])) {
			$sql.=" AND `user_type`='{$arrUpd['user_type']}' ";
		}
		$ret=$db->query($sql);
		error_log("[m/user/delSmsAuthData]".$ret.":".$sql);
		return $ret;
    }

	/*
	 *	取得會員簡訊驗証資料
	 *	$arrUpd				array			 更新內容
	 *	$arrCond			array			 更新條件
	 */
    public function updateSmsAuthData($arrUpd, $arrCond) {
		global $db,$config;
		$sql = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				  SET `modifyt`=NOW() ";
		if(!empty($arrUpd['verified'])) {
			$sql.=", `verified`='{$arrUpd['verified']}' ";
		}
		if(!empty($arrUpd['code'])) {
			$sql.=", `code`='{$arrUpd['code']}' ";
		}
		if(!empty($arrUpd['phone'])) {
			$sql.=", `phone`='{$arrUpd['phone']}' ";
		}
		if(!empty($arrUpd['userid'])) {
			$sql.=", `userid`='{$arrUpd['userid']}' ";
		}
		if(!empty($arrUpd['user_type'])) {
			$sql.=", `user_type`='{$arrUpd['user_type']}' ";
		}
		$sql.=" WHERE 1=1 ";
		if(!empty($arrCond['authid'])) {
			$sql.=" AND `authid`='{$arrCond['authid']}' ";
		}
		if(!empty($arrCond['phone'])) {
			$sql.=" AND `phone`='{$arrCond['phone']}' ";
		}
		if(!empty($arrCond['userid'])) {
			$sql.=" AND `userid`='{$arrCond['userid']}' ";
		}
		if(!empty($arrCond['code'])) {
			$sql.=" AND `code`='{$arrCond['code']}' ";
		}
		if(!empty($arrCond['verified'])) {
			$sql.=" AND `verified`='{$arrCond['verified']}' ";
		}
		if(!empty($arrCond['user_type'])) {
			$sql.=" AND `user_type`='{$arrCond['user_type']}' ";
		}
		error_log("[m/user/updateSmsAuthData]:".$sql);
		return $db->query($sql);
    }

	/*
	 *	修改個人資料-頭像修改
	 *	$userid		int			會員編號
	 *	$picname	varchar		圖片檔名
 	 */
	public function updateHeadImg($userid, $picname) {
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				SET thumbnail_file ='{$picname}'
				WHERE userid='{$userid}'";
		return $db->query($sql);
	}

	/*
	 *	修改個人資料-身份證編號
	 *	$authid		int			會員編號
	 *	$personId	varchar		會員身份證編號
 	 */
	public function set_idnum($idnum,$userid) {
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$term=($idnum[1]=='1')?'male':'female';
		$idsix=substr($idnum,4,6);
		$idnum=encrypt($idnum);
		$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				SET idnum ='{$idnum}',idsix='{$idsix}' ,gender='{$term}'
				WHERE userid='{$userid}'";
		return $db->query($sql);
	}

	/*
	 *	取得用戶等級及經驗點數值
	 *	By Thomas 20160407
	 *	$userid		int			會員編號
 	 */
	public function getUserLvlExpts($userid) {
		if(empty($userid)) {
			error_log("[m/user] empty userid !! ");
			return false;
		}
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$sql = "  SELECT e.userid, e.total_expts, m.level, m.mdlid, m.title
				   FROM (SELECT userid, IFNULL(SUM(pts),0) as total_expts
						   FROM saja_user.saja_user_expts
						  WHERE switch='Y' and userid={$userid} ) e
				   JOIN saja_medals_def m
					 ON e.total_expts between m.min_expts and m.max_expts
					AND m.switch='Y' AND m.type='E'
				   WHERE e.userid={$userid} ";
		$table = $db->getQueryRecord($sql);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		} else {
			return false;
		}
	}

	/*
	 *	修改卡包資訊
	 *	$arrCond		array			更新條件
	 *	$arrInfo		array			更新內容
 	 */
	public function updCardPackInfo($arrCond, $arrInfo) {
		global $config, $db;
		$sql = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo`
				  SET modifyt=NOW() ";
		for($idx=1;$idx<=8;++$idx) {
			$sql.=",field".$idx."name='".$arrInfo['field'.$idx.'name']."'";
		}
		if(!empty($arrInfo['switch'])) {
			$sql.=",switch='".$arrInfo['switch']."' ";
		}
		if(!empty($arrInfo['seq'])) {
			$sql.=",seq=".$arrInfo['seq'];
		}
		$sql.=" WHERE 1=1 ";
		if(!empty($arrCond['ueid'])) {
			$sql.=" AND ueid='".$arrCond['ueid']."' ";
		}
		if(!empty($arrCond['uecid'])) {
			$sql.=" AND uecid='".$arrCond['uecid']."' ";
		}
		if(!empty($arrCond['userid'])) {
			$sql.=" AND userid='".$arrCond['userid']."' ";
		}
		if(!empty($arrCond['switch'])) {
			$sql.=" AND switch='".$arrCond['switch']."' ";
		}
		error_log("[m/user/updCardPackInfo]:".$sql);
		$ret=$db->query($sql);
		if($ret==1) {
			error_log("[m/user/updCardPackInfo]: userid:".$arrCond['userid']." updated ueid:".$ueid);
		}
		return $ret;
	}

	/*
	 *	新增卡包資訊
	 *	$arrInfo		array			新增內容
 	 */
	public function addCardPackInfo($arrInfo) {
		global $config, $db;
		$sql = " INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo`
						SET prefixid='saja', modifyt=NOW(), insertt=NOW() ";
		for($idx=1;$idx<=8;++$idx) {
		   $sql.=",field".$idx."name='".$arrInfo['field'.$idx.'name']."'";
		}
		if(!empty($arrInfo['uecid'])) {
			$sql.=" ,uecid='".$arrInfo['uecid']."' ";
		}
		if(!empty($arrInfo['userid'])) {
			$sql.=" ,userid='".$arrInfo['userid']."' ";
		}
		if(!empty($arrInfo['switch'])) {
			$sql.=" ,switch='".$arrInfo['switch']."' ";
		}
		error_log("[m/user/addCardPackInfo]:".$sql);
		$ret = $db->query($sql);
		if($ret==1) {
			$ueid=mysqli_insert_id($db->_con);
			error_log("[m/user/addCardPackInfo]: userid:".$arrInfo['userid']." created ueid:".$ueid);
			return $ueid;
		} else {
			return $ret;
		}
	}

	/*
	 *	會員中標商品List
	 *	$userid			int			會員編號
	 *	$productid		int			商品編號
 	 */
	public function getUserAwardProdList($userid='', $productid='') {
		global $config, $db;
		$query = " SELECT pgp.*, p.retail_price, p.name, p.closed, p.thumbnail_url
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
					JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
					  ON pgp.productid=p.productid
					 AND pgp.switch='Y'
				 WHERE 1=1
					 AND p.closed='Y'
					 AND p.switch='Y'
					 AND pgp.switch='Y'
					 AND pgp.complete='Y' ";
		if(!empty($userid)) {
			$query.=" AND pgp.userid='{$userid}' ";
		}
		if(!empty($productid)) {
			$query.=" AND pgp.productid='{$productid}' ";
		}
		$query.=" ORDER BY p.offtime desc ";
		error_log("[m/user/getUserAwardProdList]:".$query);
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
	}

	/*
	 *	我的殺友清單
	 *	$userid		int			會員編號
	 *	$nickname	varchar		搜尋名稱
	 * 	$sIdx		int		 	目前頁碼
	 * 	$perpage	int 		筆數
 	 */
	public function getFriendsList($userid, $nickname='', $sIdx='', $perpage='') {
		global $config, $db;
		//SQL指令 - 取得同推薦人的殺友清單
		$query = " SELECT ua.userid, up.nickname, up.thumbnail_file, up.thumbnail_url, up.gender, p.name as come_from
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` ua
					LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up
					ON ua.userid = up.userid
					LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` p
					ON p.provinceid = up.provinceid
					WHERE ua.intro_by = '{$userid}'
					AND ua.userid <> '{$userid}'
					AND ua.act='REG' ";

		if (!empty($nickname)) {
			$query .= " AND up.nickname LIKE '%{$nickname}%'";
		}

		$query .= " ORDER BY ua.userid ASC";

		error_log("[m/user/getFriendsList]:".$query);
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			if(!empty($perpage) && !empty($sIdx)) {
				$query_limit.= " LIMIT ".$sIdx.",".$perpage;
			} else {
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/".$tv['thumbnail_file'];
				} elseif(empty($tv['thumbnail_url']) && empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/_DefaultHeadImg.jpg";
				}

				unset($table['table']['record'][$tk]['thumbnail_file']);
			}

			return $table['table'];
		}

		return false;
	}

	/*
	 *	修改会员数据
	 *	$arrCond		array			更新條件
	 *	$arrUpd			array			更新內容
 	 */
	public function updUserData($arrUpd, $arrCond) {
		global $config, $db;
		if(count($arrCond)<1 || count($arrUpd)<1) {
			return false;
		}

		$idx=0;
		$r=0;
		$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` SET prefixid='saja' ";
		if(!empty($arrUpd['name'])) {
		   $query.=", `name`='{$arrUpd['name']}' ";
		   $idx++;
		}
		if(!empty($arrUpd['passwd'])) {
		   $query.=", `passwd`='{$arrUpd['passwd']}' ";
		   $idx++;
		}
		if(!empty($arrUpd['exchangepasswd'])) {
		   $query.=", `exchangepasswd`='{$arrUpd['exchangepasswd']}' ";
		   $idx++;
		}

		$query.=" WHERE `prefixid` = '{$config['default_prefix_id']}' ";
		if(!empty($arrCond['userid'])) {
		   $query.=" AND `userid`='".$arrCond['userid']."' ";
		}
		if(!empty($arrCond['name'])) {
		   $query.=" AND `name`='".$arrCond['name']."' ";
		}

		try {
		   $r=$db->query($query);
		   error_log("[m/user/updUserData]:".$query."-->".$r);
        } catch (Exception $e) {
           error_log("[m/user/updUserData]:".$query."-->".$e->getMessage());
		   $r=-1;
        } finally {
            return $r;
        }
	}

	/*
	 *	修改個人資料-手機號碼修改
	 *	$userid		int			會員編號
	 *	$phone		varchar		手機號碼
 	 */
	public function updUserProfileData($phone, $userid) {
	   global $config, $db;
	   $r=0;
	   $sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				SET phone ='{$phone}'
				WHERE userid='{$userid}'";
		try {
			$r=$db->query($sql);
			error_log("[m/user/updUserProfileData]:".$sql."-->".$r);
		} catch (Exception $e) {
			error_log("[m/user/updUserProfileData]:".$sql."-->".$e->getMessage());
			$r=-1;
		} finally {
			return $r;
		}
	}

	/*
	 *	殺手榜
	 *	$type		int			排序方式
	 * 	$sIdx		int		 	目前頁碼
	 * 	$perpage	int 		分頁筆數
 	 */
	public function getAssassinList($type, $sIdx='', $perpage='') {
		global $config, $db;

		switch($type) {
			case 1 :$query = " SELECT up.userid, up.nickname, up.thumbnail_file, up.thumbnail_url, p.name as come_from,
						  (select COUNT(pgp.userid) from `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp where pgp.userid = up.userid and pgp.switch='Y' ) as num_award,
					      '' as prod_name, '' as award_time
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up
					LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` p
					ON p.provinceid = up.provinceid
					GROUP BY up.userid
					HAVING num_award > 2
					ORDER BY num_award DESC ";break;
			case 2 : $query = "SELECT up.userid, up.nickname, up.thumbnail_file, up.thumbnail_url, p.name as come_from,
						           1 as num_award, pp.name as prod_name, pgp.insertt as award_time
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		                 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` pp
						   ON pgp.productid=pp.productid
					     JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up
						   ON pgp.userid=up.userid
					LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` p
					       ON up.provinceid=p.provinceid
                     WHERE pgp.insertt > NOW()-INTERVAL 15 DAY
				     ORDER BY pgp.insertt DESC ";break;
		}

		//總筆數
		$getnum = $db->getQueryRecord("select count(*) as num from  (".$query." ) cnt ");
		$num = (!empty($getnum)) ? ($getnum['table']['record'][0]['num']) : 0;
		if(empty($perpage)) {
		    $perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		}
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			if(!empty($perpage) && !empty($sIdx)) {
				$query_limit.= " LIMIT ".$sIdx.",".$perpage;
			} else {
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/".$tv['thumbnail_file'];
				} elseif(empty($tv['thumbnail_url']) && empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/_DefaultHeadImg.jpg";
				}

				unset($table['table']['record'][$tk]['thumbnail_file']);
			}

			return $table['table'];
		}

		return false;
	}

	/*
	 *	商品訂單資料
	 *	$orderid	int			訂單編號
	 */
	public function getOrderDetail($orderid='', $type) {
		global $config, $db;

		$query = " SELECT o.orderid, o.userid, o.status, o.type, o.num, o.esid, o.point_price, o.process_fee, o.total_fee, o.profit,
						  o.memo, o.confirm, oc.name, oc.address, oc.phone, oc.zip, oc.gender, o.insertt, os.outtime, os.outtype, os.outcode,
						  os.outmemo, os.backtime, os.backmemo";
		if ($type == "saja") {
			$query .= " ,(select p.productid from `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON pgp.productid=p.productid AND p.switch='Y' where pgp.pgpid = o.pgpid and pgp.switch='Y' ) as prod_id
						,(select p.name from `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON pgp.productid=p.productid AND p.switch='Y' where pgp.pgpid = o.pgpid and pgp.switch='Y' ) as prod_name
						";
		} else {
			$query .= " ,o.epid as prod_id, (select ep.name from `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` ep where ep.epid = o.epid and ep.switch='Y' ) as prod_name ";
		}

		$query .= " FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o
					LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` oc
						ON o.orderid=oc.orderid AND o.switch='Y'
					LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_shipping` os
						ON o.orderid=os.orderid AND os.switch='Y'
					WHERE o.switch='Y'
						AND oc.switch='Y'
						AND o.orderid = {$orderid}";
		error_log("[m/user/getOrderDetail]:".$query);
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
	}

	/*
	 *	合格人數資料
	 *	$userid		int			會員編號
	 */
	public function  RecommandDepositUsers($userid) {

		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		if(!empty($userid)) {

			$sql = "SELECT userid, insertt
					 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
					WHERE act='REG'
					  AND switch='Y'
					  AND intro_by='{$userid}' ";

			$table = $db->getQueryRecord($sql);
			$count = 0;

			foreach($table['table']['record'] as $tk => $tv) {
				if(!empty($tv['userid'])) {
					$sql = "SELECT count(depositid) as num
							 FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
							WHERE switch='Y'
							  AND userid='{$tv['userid']}'
							  AND modifyt > '{$tv['insertt']}' ";

					$table2 = $db->getQueryRecord($sql);

					if($table2['table']['record'][0]['num'] > 0) {
						$count = $count + 1;
					}
				}
			}

			return $count;
		} else {
		   return false;
		}
	}

	/*
	 *	推薦人資料
	 *	$userid		int			會員編號
	 */
	public function getAffiliateUser($userid) {

		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		if(!empty($userid)) {
			$sql = "SELECT `suaid`, `userid`, `intro_by`, `promoteid`, insertt
					 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
					WHERE act='REG'
					  AND switch='Y'
					  AND userid='{$userid}' ";
			$user_table = $db->getQueryRecord($sql);

			if(!empty($user_table['table']['record'])) {
				$introby_uid = $user_table['table']['record'][0]['intro_by'];
				$table['table']['record'][0] = $user_table['table']['record'][0];

				$query ="SELECT nickname, thumbnail_file, thumbnail_url
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					WHERE `prefixid` = '{$config['default_prefix_id']}'
						AND userid = '{$introby_uid}'
						AND switch = 'Y' ";
					$profile_table = $db->getQueryRecord($query);
					if(!empty($profile_table['table']['record'][0])) {
						$table['table']['record'][0]['nickname']=urldecode($profile_table['table']['record'][0]['nickname']);
						$table['table']['record'][0]['thumbnail_file']=$profile_table['table']['record'][0]['thumbnail_file'];
						$table['table']['record'][0]['thumbnail_url']=$profile_table['table']['record'][0]['thumbnail_url'];
					}

				return $table['table']['record'][0];
			}
		}

		return false;
	}

	/*
	 *	上一層推薦人
	 *	$intro_by int 推薦人會員編號
	 */
	public function affiliateParents($intro_by) {

		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		$uid = $intro_by;
		if(!empty($uid)) {
			$sql = "SELECT `suaid`, `userid`, `intro_by`, `promoteid`, insertt
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
					WHERE `userid` = '{$uid}'
						AND `act` = 'REG'
						AND `switch` = 'Y'
						AND `promoteid`=8 ";

			$_affiliate = $db->getQueryRecord($sql);

			if(!empty($_affiliate['table']['record'][0])) {
				$introby_uid = $_affiliate['table']['record'][0]['intro_by'];
				$table['table']['record'][0] = $_affiliate['table']['record'][0];

				$query ="SELECT nickname, thumbnail_file, thumbnail_url
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
					WHERE `prefixid` = '{$config['default_prefix_id']}'
						AND userid = '{$introby_uid}'
						AND switch = 'Y' ";
					$profile_table = $db->getQueryRecord($query);
					if(!empty($profile_table['table']['record'][0])) {
						$table['table']['record'][0]['nickname']=urldecode($profile_table['table']['record'][0]['nickname']);
						$table['table']['record'][0]['thumbnail_file']=$profile_table['table']['record'][0]['thumbnail_file'];
						$table['table']['record'][0]['thumbnail_url']=$profile_table['table']['record'][0]['thumbnail_url'];
					}

				return $table['table']['record'][0];
			}
		}

		return false;
	}

	/*
	 *	會員微信openid資料
	 *	$userid		int			會員編號
	 */
	public function getUserWeixinOpenID($userid) {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		if(!empty($userid)) {
			$sql = "SELECT uid
					 FROM `{$config['db'][0]['dbname']}`.`v_user_profile_sso`
					WHERE sso_name='weixin'
					  AND userid='{$userid}' ";

			$table = $db->getQueryRecord($sql);

			if(!empty($table['table']['record'][0])) {
				return $table['table']['record'][0];
			} else {
			   return false;
			}
		}
	}

	/*
	 *	會員商戶資料
	 *	$userid		int			會員編號
	 */
	public function get_user_enterprise_rt($userid) {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		if(!empty($userid)) {
			$sql = "SELECT count(ueid) as num
					 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt`
					WHERE switch='Y'
					  AND userid='{$userid}' ";

			$table = $db->getQueryRecord($sql);

			if(!empty($table['table']['record'][0])) {
				return $table['table']['record'][0];
			} else {
				return false;
			}
		}
	}

    public function get_user_sso_list($userid, $sso_name='') {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();
		$sql = "SELECT u.userid, u.name, ss.* FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
						JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` rt
						  ON   u.userid=rt.userid
						JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso` ss
						  ON   rt.ssoid=ss.ssoid
						WHERE  u.prefixid = '{$config['default_prefix_id']}'
						  AND  rt.switch='Y'
						  AND  rt.prefixid='{$config['default_prefix_id']}'
						  AND  ss.switch='Y'
						  AND  ss.prefixid='{$config['default_prefix_id']}'
						  AND  u.userid='{$userid}'";
		if(!empty($sso_name)) {
		   $sql.=" AND ss.name='{$sso_name}' ";
		}
		$table = $db->getQueryRecord($sql);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
    }

    // Add By Thomas 20180810 查詢　sso
    public function get_sso_data($arrCond) {
		global $db, $config;

		$query = "select * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
		           where prefixid = '{$config['default_prefix_id']}' ";
        if(!empty($arrCond['name'])) {
           $query.=" AND name = '{$arrCond['name']}' ";
        }
        if(!empty($arrCond['uid'])) {
           $query.=" AND uid = '{$arrCond['uid']}' ";
		}
        if(!empty($arrCond['uid2'])) {
           $query.=" AND uid2 = '{$arrCond['uid2']}' ";
		}
        if(!empty($arrCond['ssoid'])) {
           $query.=" AND ssoid = '{$arrCond['ssoid']}' ";
        }
        if(!empty($arrCond['seq'])) {
           $query.=" AND seq = '{$arrCond['seq']}' ";
        }
        if(!empty($arrCond['switch'])) {
           $query.=" AND switch = '{$arrCond['switch']}' ";
        } else {
           $query.=" AND switch = 'Y' ";
		}
		$table = $db->getQueryRecord($query);
		if($table['table']['record']) {
			return $table['table']['record'];
		} else {
			return false;
		}
	}

    // Add By Thomas 20180810 查詢　sso_rt
    public function get_sso_rt_data($arrCond) {
		global $db, $config;

		$query = "select * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso_rt`
		           where prefixid = '{$config['default_prefix_id']}' ";
        if(!empty($arrCond['userid'])) {
           $query.=" AND userid = '{$arrCond['userid']}' ";
		}
        if(!empty($arrCond['ssoid'])) {
           $query.=" AND ssoid = '{$arrCond['ssoid']}' ";
        }
        if(!empty($arrCond['seq'])) {
           $query.=" AND seq = '{$arrCond['seq']}' ";
        }
        if(!empty($arrCond['switch'])) {
           $query.=" AND switch = '{$arrCond['switch']}' ";
        } else {
           $query.=" AND switch = 'Y' ";
		}
		$table = $db->getQueryRecord($query);

		if($table['table']['record']) {
			return $table['table']['record'];
		} else {
			return false;
		}
	}

    // 刪除sso資料 : 只能by ssoid 刪除, 以避免條件不足造成誤刪
    public function delete_sso($ssoid) {
		global $db, $config;
        if(empty($ssoid)) {
            return false;
        }
		$query = "DELETE from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
                   WHERE prefixid = '{$config['default_prefix_id']}'
                     AND ssoid='{$ssoid}' ";
        $del = $db->query($query);
        error_log("[m/user/delete_sso] ".$query." => ".$del);
        return $del;
	}

    // 刪除sso_rt資料 : 只能by ssoid 刪除, 以避免條件不足造成誤刪
	public function delete_sso_rt($ssoid) {
		global $db, $config;
        if(empty($ssoid)) {
            return false;
        }
        error_log("[m/user/delete_sso_rt] rt ssoid to delete : ".$ssoid);
		$query = "DELETE from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt`
                   WHERE prefixid = '{$config['default_prefix_id']}'
                     AND ssoid='{$ssoid}' ";
        $del = $db->query($query);
        error_log("[m/user/delete_sso_rt] ".$query." => ".$del);
        return $del;
	}

    // 新增sso資料 add By Thomas 20181130
    public function create_sso($sso_name, $uid, $uid2='', $sso_data='') {
		global $db, $config;
		if(empty($sso_name)) {
			error_log("[m/user/create_sso] Empty sso_name !!");
			return false;
		} else if($sso_name!=addslashes($sso_name)) {
			error_log("[m/user/create_sso] Error sso_name !!");
			return false;
		}
		if(empty($uid)) {
			error_log("[m/user/create_sso] Empty uid !!");
			return false;
		} else if($uid!=addslashes($uid)) {
			error_log("[m/user/create_sso] Error uid !!");
			return false;
		}
		if(!empty($uid2) && $uid2!=addslashes($uid2) ) {
			error_log("[m/user/create_sso] Error uid2 !!");
			return false;
		}
		if(!empty($sso_data)) {
		   $sso_data=addslashes($sso_data);
		}
		$query = " INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
				  (prefixid, name, uid, uid2, sso_data,insertt) values ('saja', '{$sso_name}', '{$uid}','{$uid2}','{$sso_data}',NOW()) ";

		$ssoid = 0;
		if($db->query($query)) {
		  $ssoid = $db->_con->insert_id;
		}
		error_log("[m/user/create_sso] ".$query." => ".$ssoid);
		return $ssoid;
    }

    // 新增sso_rt資料 add By Thomas 20181130
    public function create_sso_rt($userid, $ssoid) {
		global $db, $config;
		if(empty($userid)) {
			error_log("[m/user/create_sso_rt] Empty userid !!");
			return false;
		}
		if(empty($ssoid)) {
			error_log("[m/user/create_sso_rt] Empty ssoid !!");
			return false;
		}
		if($userid!=addslashes($userid) || $ssoid!=addslashes($ssoid)) {
		   error_log("[m/user/create_sso_rt] Error userid or ssoid !! ");
		   return false;
		}

		$query = " INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt`
				 (prefixid, ssoid, userid, insertt) values ('saja', '{$ssoid}', '{$userid}',NOW() ) ";

		$sso_rt_id=0;
		if($db->query($query)) {
		  $sso_rt_id = $db->_con->insert_id;
		}
		error_log("[m/user/create_sso_rt] ".$query." => ".$sso_rt_id);
		return $sso_rt_id;
    }

	/*
	 *	得標資料
	 *	$userid		int			會員編號
	 */
	public function pay_get_product_count($userid) {
		global $db, $config;

		if(!empty($userid)) {
			$query = "SELECT count(*) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
			where
				`prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$userid}'
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);

			if(!empty($table['table']['record'][0])) {
				return $table['table']['record'][0]['count'];
			} else {
			   return false;
			}
		}
	}


	/*
	 *	修改個人資料-信用卡修改
	 *	$userid		int			會員編號
	 *	$token		varchar		信用卡資料
 	 */
	public function updUserProfileCreditCardToken($userid, $token) {
		global $config, $db;
		$r=0;
		$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				SET creditcard_token ='{$token}'
				WHERE userid='{$userid}'";
		try {
			$r=$db->query($sql);
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql."-->".$r);
		} catch (Exception $e) {
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql."-->".$e->getMessage());
			$r=-1;
		} finally {
			return $r;
		}
	}


    /*
	 *	取得會員信用卡資料
	 *	$id				int				會員編號
	 */
	public function get_user_token($id) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT creditcard_token
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND userid = '{$id}'
			AND switch = 'Y'
		";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			$table['table']['record'][0]['token']=json_decode(urldecode($table['table']['record'][0]['creditcard_token']), true);
            return $table['table']['record'][0]['token'];
		}
		return false;
    }


    /*
	 *	取得會員信用卡清單資料
	 *	$userid				int				會員編號
	 */
	public function getUserCreditCardTokenList($userid) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT ucid, cardbank, cardtype, creditcard_num4, expiration_month, expiration_year, is_default
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_creditcard`
		WHERE
			userid = '{$userid}'
			AND switch = 'Y'
		";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
            return $table['table']['record'];
		}
		return false;
    }


    /*
	 *	取得會員信用卡資料
	 *	$userid				int				會員編號
	 *	$ucid				int				會員信用卡編號
	 */
	public function getUserCreditCardToken($userid,$ucid) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT memo
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_creditcard`
		WHERE
			userid = '{$userid}'
			AND ucid = '{$ucid}'
			AND switch = 'Y'
		";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			$table['table']['record'][0]['token']=json_decode(urldecode($table['table']['record'][0]['memo']), true);
            return $table['table']['record'][0]['token'];
		}
		return false;
    }


	/*
	 *	修改會員信用卡資料
	 *	$userid		int			會員編號
	 *	$token		varchar		信用卡資料
 	 */
	public function addUserCreditCardToken($userid, $token) {
		global $config, $db;


		$memo = json_encode($token);
		$r=0;
		$sql=" INSERT INTO  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_creditcard`
				SET
					tokenData ='{$token['paymentToken']}',
					memo ='{$memo}',
					creditcard_num4 ='{$token['last4Cardno']}',
					expiration_month ='{$token['expiration_month']}',
					expiration_year ='{$token['expiration_year']}',
					cardbank ='{$token['cardbank']}',
					cardtype ='{$token['cardtype']}',
					userid = '{$userid}',
					insertt = now()
		";
		try {
			$r=$db->query($sql);
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql."-->".$r);
		} catch (Exception $e) {
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql."-->".$e->getMessage());
			$r=-1;
		} finally {
			return $r;
		}
	}

	/*
	 *	刪除會員信用卡資料
	 *	$userid		int			會員編號
	 *	$ucid		varchar		信用卡編號
 	 */
	public function delUserCreditCardToken($userid, $ucid) {
		global $config, $db;

		$r=0;
		$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_creditcard`
				SET switch ='N'
				WHERE
					ucid='{$ucid}'
					AND userid='{$userid}'
				";
		try {
			$r=$db->query($sql);
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql."-->".$r);
		} catch (Exception $e) {
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql."-->".$e->getMessage());
			$r=-1;
		} finally {
			return $r;
		}
	}

	/*
	 *	修改會員信用卡資料 (預設使用)
	 *	$userid		int			會員編號
	 *	$ucid		varchar		信用卡編號
 	 */
	public function editUserCreditCardToken($userid, $ucid) {
		global $config, $db;

		$r=0;
		$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_creditcard`
				SET is_default ='N'
				WHERE userid='{$userid}' ";

		$sql2=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_creditcard`
				SET is_default ='Y'
				WHERE
					ucid='{$ucid}'
					AND userid='{$userid}'
				";
		try {
			$r1=$db->query($sql);
			$r=$db->query($sql2);
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql2."-->".$r);
		} catch (Exception $e) {
			error_log("[m/user/updUserProfileCreditCardToken]:".$sql2."-->".$e->getMessage());
			$r=-1;
		} finally {
			return $r;
		}
	}

	// 手機號碼登入只能用帳號驗證
	public function check_sso_name($userid,$phone){
		global $config, $db;
		if (empty($userid) || empty($phone)) {
			return false;
		}

		$query = "	SELECT
						u.name, usr.ssoid
					FROM
						`{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
					LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` usr ON u.userid = usr.userid
					AND usr.switch = 'Y'
					WHERE
						u.switch = 'Y'
					AND u.userid = '{$userid}'";
		$table = $db->getQueryRecord($query);

		$u_name = $table['table']['record'][0]['name'];
		$ssoid = $table['table']['record'][0]['ssoid'];

		if ( !empty($ssoid) ) {
			return true;
		}else if($u_name == $phone){
			return true;
		}else{
			return false;
		}
	}

	// 每支手機只能驗證一種登入方式
	/*
	public function check_sso_verify($userid,$phone){
		global $config, $db;
		if (empty($userid) || empty($phone)) {
			return false;
		}

		$query = "	SELECT
						s.`name`
					FROM
						`{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso` s
					LEFT JOIN`{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` usr
						ON s.ssoid = usr.ssoid
					AND usr.switch = 'Y'
					WHERE
						s.switch = 'Y'
					AND usr.userid = '{$userid}' ";
		$table = $db->getQueryRecord($query);
		if ( !empty($table['table']['record'][0]) ) {
			$sso_name = $table['table']['record'][0]['name'];
			$where = "AND s.name = '{$sso_name}'";
		}else{
			$sso_name = "phone";
			$where = "AND s.name is NULL";
		}

		$query = "	SELECT
						usa.authid,
						usa.userid,
						IFNULL(s.`name`, 'phone') name
					FROM
						`{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa
					LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` usr ON usr.userid = usa.userid
					AND usr.switch = 'Y'
					LEFT JOIN saja_sso s ON s.ssoid = usr.ssoid
					AND s.switch = 'Y'
					WHERE
						usa.switch = 'Y'
					AND usa.verified = 'Y'
					AND usa.phone = '{$phone}'
					{$where} ";

		$table = $db->getQueryRecord($query);
		if (empty($table['table']['record'])) {
			return true;
		}else{
			return false;
		}

	}
	*/
	// Add By Thomas 2020/01/16
	// 每支手機只能驗證一個帳號
	public function check_sso_verify($userid,$phone){

		   global $config, $db;
		   if (empty($userid) || empty($phone)) {
				return false;
		   }
		   error_log("[m/user] check_sso_verify for : ${userid}=>${phone} ");
		   $phone=htmlspecialchars($phone);
		   $userid=htmlspecialchars($userid);

		   $query = " SELECT count(userid) as cnt
		                FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
					  WHERE switch='Y'
					    AND verified='Y'
						AND phone='{$phone}' ";

		   $table = $db->getQueryRecord($query);
		   if(!$table) {
		       // 異常沒撈到
			   error_log("[m/user] dup ${phone} : error !!");
			   return false;
		   } else if($table['table']['record'][0]['cnt']>0) {
              // 撈到資料  手機號的資料筆數確實 >0
			  error_log("[m/user] dup ${phone} : ".$table['table']['record'][0]['cnt']);
			  return false;
           }
           error_log("[m/user] dup ${phone} : ".$table['table']['record'][0]['cnt']);
           return true;

	}


    /*
	 *	取得會員公告資料
	 *	$newsid				int				資料編號
	 *	$public				int				公開模式
	 */
	public function getNews($newsid,$kind='N') {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();
		if (!empty($newsid)){
			$news = " AND `newsid` = '{$newsid}' ";
		}

		if (!empty($kind)){
			$public = " AND `public` = '{$kind}' ";
		}

		$query ="SELECT newsid, name, description, public, osversion
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}news`
		WHERE 1 = 1
			{$news}
			{$public}
			AND `ontime` <= NOW()
			AND `offtime` > NOW()
			AND `switch` = 'Y'
			ORDER BY seq ASC ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {

			foreach($table['table']['record'] as $tk => $tv) {
				$description = str_replace(array("\r\n\t&nbsp;","\r","\n","\t","&nbsp;"), '', (trim(strip_tags($tv['description']))));
				$table['table']['record'][$tk]['description'] = html_decode($description);
			}
            return $table['table']['record'];
		}
		return false;
    }

	//查驗圓夢是否得標
	public function dream_pay_get($userid){
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        //查驗圓夢是否得標
		$sql ="SELECT pp.`pgpid`,pp.`productid`,pp.`userid`,pp.`complete`
			FROM `saja_shop`.`saja_pay_get_product` pp
			LEFT JOIN `saja_shop`.`saja_product` p ON  p.`productid`=pp.`productid`
			WHERE pp.`prefixid`='saja'
				AND pp.`switch`='Y'
				AND pp.`complete`='Y'
				AND p.`ptype`=1
				AND p.`switch`='Y'
				AND pp.`userid`='{$userid}'
			";
		$recArr = $db->getQueryRecord($sql);
		if(empty($recArr['table']['record']) ) {
			return true;
		} else {
			return false;
		}
    }
	//鎖定/解鎖下標兌換
	public function update_bid_able($userid, $key='exchange_enable', $var='N'){
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $sql ="UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` SET ";
		$sql .="`{$key}`='{$var}',";
		$sql .="`modifyt` = now()
			WHERE `userid` = '{$userid}' ";
		$result=$db->query($sql);

		if($result){
			return true;
		} else {
			return false;
		}
    }


    /*
	 *	取得會員載具資料
	 *	$id				int				會員編號
	 */
	public function get_user_carrier($id) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT carrier_type,carrier_kind, carrier_aggregate,invoice_paper,invoice_type,buyer_id,buyer_name,carrier_id1,carrier_id2,phonecode,npcode,donate_mark,npoban,invoice_send
		           FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
		          WHERE	`prefixid` = '{$config['default_prefix_id']}'
			        AND userid = '{$id}'
			        AND switch = 'Y' ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
            return $table['table']['record'][0];
		}
		return false;
    }

	/* //20201201leo
	 *	上一層推薦人
	 *	$intro_by int 推薦人會員編號
	 */
	public function get_intro_by_id($userid) {
		global $config,$db;
		$sql="SELECT intro_by from saja_user.saja_user_affiliate WHERE `userid` = '{$userid}'
		AND `act` = 'REG'
		AND `switch` = 'Y'";
		$raw=$db->getRecord($sql);
		if (sizeof($raw)>0){
			$intro=(empty($raw[0]['intro_by']))?0:$raw[0]['intro_by'];
			if ($intro>=0){
				return $intro;
			}else{
				return -1;
			}
		}else{
			return -1;
		}
	}

	//新增spoint資訊紅利新增時標註是那筆訂單來的誰生成的
	public function add_spoint_feedback($genuserid,$depositid,$uid, $amount, $switch='Y') {
        global $db, $config;
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint`
			SET
			prefixid = '{$config['default_prefix_id']}',
			userid = '{$uid}',
			countryid = '{$config['country']}',
			behav = 'feedback',
			amount = '{$amount}',
			seq = '0',
			switch = '{$switch}',
            depositid = '{$depositid}',
            genuserid = '{$genuserid}',
			insertt=NOW()
		";
		$db->query($query);
		return $spointid = $db->_con->insert_id;
	}

	/*20201202 leo
	 *	POPUSER資料
	 *	userids		array(int)			會員編號array
	 */
	public function getUsersSinfo($userids) {
		global $db,$config;
		if(!empty($userids)) {
			$introby_uid=implode("','",$userids);
			$query ="SELECT userid,nickname, IFNULL(thumbnail_file,'') as thumbnail_file , IFNULL(thumbnail_url,'') as thumbnail_url
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
				WHERE `prefixid` = '{$config['default_prefix_id']}'
					AND userid in ('{$introby_uid}')
					AND switch = 'Y' ";
			$table = $db->getRecord($query);
			for ($i=0;$i<sizeof($table);$i++){
				$table[$i]['nickname']=urldecode($table[$i]['nickname']);
			}
			return $table;
		}
		return false;
	}

    /*20201203 leo
	 *	settin推薦人
	 *	userid
	 *  intro_by
	 */
	public function setUserAffiliate($userid,$intro_by) {
		global $db,$config;
		if($this->get_intro_by_id($userid)<0) {

			// 記錄連線來源
			// 抓來源IP
			if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$ip = $temp_ip[0];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}

			// 抓User Agent
			$memo=$_POST['client'];
			if(!empty($memo)) {
				$user_agent='APP';
			} else {
				$memo=$_SERVER['HTTP_USER_AGENT'];
				if(strpos($memo, 'MicroMessenger')>0) {
					$user_agent='WEIXIN';
				} else {
					$user_agent='BROWSER';
				}
			}
			if(intval($intro_by) < intval($userid) ){
				$intro_user = $intro_by;
			} else {
				$intro_user = ''; 
			}
				$insert = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` SET
						`come_from`='{$ip}',
						`intro_by`='{$intro_user}',
						`userid`='{$userid}',
						`act`='REG',
						`user_agent`='{$user_agent}',
						`insertt`=NOW(),
						`modifyt`=NOW() ";
				$db->query($insert);
			
		}else{
			if(intval($intro_by) < intval($userid) ){
				$intro_user = $intro_by;
			} else {
				$intro_user = ''; 
			}
				$insert = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` SET
						`intro_by`='{$intro_user}'
						WHERE `userid`='{$userid}'";
				$db->query($insert);
			
		}
	}
	
	//20201224 更新會員等級
	public function setUserLevel($userid, $count=0) {
		global $db,$config;
		
		if(!empty($userid) && !empty($count)) {
			//$query ="SELECT count(*) as count, `userid` FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
			//	WHERE `prefixid` = '{$config['default_prefix_id']}' AND `switch` = 'Y' AND `bid_price_total` > 0
			//		AND `userid`='{$userid}' ";
			//error_log("[pay_get_product]count: ". $query);
			//$table = $db->getRecord($query);
			//$count = empty($table[0]['count']) ? 0 : $table[0]['count'];
			
			$query ="UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
				SET `level`='{$count}'
				WHERE `userid`='{$userid}' ";
			error_log("[setUserLevel]: ". $query);
			$db->query($query);
			
			return true;
		}
		return false;
	}
	
	//20201225 分組取會員ID
	public function getModUserid($remainder, $divisor) {
		global $db,$config;
  
		$query ="SELECT `userid` FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
				WHERE `prefixid` = '{$config['default_prefix_id']}' AND switch = 'Y' 
					AND `bid_enable` = 'Y'
					AND `exchange_enable` = 'Y'
					AND `deposit_enable` = 'Y'
					AND MOD(userid, {$divisor}) = {$remainder} ";
		//error_log("[getModUserid]: ". $query );
		$table = $db->getRecord($query);
		error_log("[getModUserid]Record: ". json_encode($table) );

		if(!empty($table)) {
            foreach($table as $k=>$v){
				$user_arr[] = $v['userid'];
			}
			//error_log("[user_arr]: ". json_encode($user_arr) );
			$str = implode(",", $user_arr);
			
			return $str;
		}
		return false;
    }

	/* 取最近0.5小時的會員等級
	* 20210104 以“實體商品”結標次數計算。票券類不列入計算
	*/
	public function getPayBidCnt($userid){
		global $db,$config;
		
		//$sql = "select count(*) as cnt  from `saja_shop`.`saja_pay_get_product` where switch='Y' and userid='{$userid}' and complete='Y'";
		$sql="SELECT count(*) as cnt FROM (
			SELECT pgp.productid, pcr.pcid FROM `saja_shop`.`saja_pay_get_product` pgp 
			LEFT JOIN `saja_shop`.`saja_product_category_rt` pcr ON pcr.productid=pgp.productid AND pcr.prefixid='saja'
			WHERE pgp.switch='Y' AND pgp.userid='{$userid}' AND pgp.complete='Y' AND pcr.pcid !=166 AND pcr.pcid >0
			GROUP BY pgp.productid
		) t WHERE 1";
		
		try{
			$redis = getRedis();
			if ($redis->exists('PayBidCnt_'.$userid)){
			}else{
				$tmpl_raw=$db->getRecord($sql);
				$redis->setex("PayBidCnt_".$userid,30*60,$tmpl_raw[0]['cnt']);
			}
			return $redis->get('PayBidCnt_'.$userid);
        } catch (Exception $e) {
			$tmpl_raw=$db->getRecord($sql);
			return $tmpl_raw[0]['cnt'];
		}

	}
}

?>