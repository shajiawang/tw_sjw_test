<?php
/**
 * Broadcast Model 模組
 */

class BroadcastModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET) {
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

	/*
	 *	直播資料取得
 	 *	$lbid			int			直播間編號
	 *	$lbuserid		int			直播主編號
	 */
	public function getLiveBroadcast($lbid, $lbuserid='', $pcid='')	{
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT lb.*, lbu.lbname
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}live_broadcast` lb
		left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}live_broadcast_user` lbu
			on lb.prefixid = lbu.prefixid AND lb.lbuserid = lbu.lbuserid
		WHERE lb.`prefixid` = '{$config['default_prefix_id']}'
			AND lb.`switch` = 'Y'
			AND lb.ontime <= NOW()
			AND lb.offtime > NOW()
		";
		if(!empty($lbid)) {
		   $query.=" AND lb.`lbid` = {$lbid} ";
		}
		if(!empty($lbuserid)) {
		   $query.=" AND lb.`lbuserid` = {$lbuserid} ";
		}
		if(!empty($pcid)) {
		   $query.=" AND lb.`pcid` = {$pcid} ";
		}
		$query.=" ORDER BY lb.lbid DESC ";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record']) ) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 *	直播時間表資料取得
 	 *	$lbid			int			直播間編號
	 *	$lbuserid		int			直播主編號
	 */
	public function getLiveBroadcastSchedule($lbid, $lbuserid='') {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT lbs.*
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}live_broadcast_schedule` lbs
		WHERE lbs.`prefixid` = '{$config['default_prefix_id']}'
			AND lbs.`switch` = 'Y'
			AND lbs.ontime <= NOW()
			AND lbs.offtime > NOW()
		";
		if(!empty($lbid)) {
		   $query.=" AND lbs.`lbid` = {$lbid} ";
		}
		if(!empty($lbuserid)) {
		   $query.=" AND lbs.`lbuserid` = {$lbuserid} ";
		}
		$query.=" ORDER BY lbs.lbid DESC ";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 *	直播殺價卷活動資料取得
	 *	$lbuserid			int					直播主編號
	 *	$big				varchar				大檔商品
 	 *	$productid			int					商品編號
	 */
	public function getScodePromoteList($lbuserid, $productid, $big) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT splr.num ,splr.lbuserid, splr.splrid, sp.*, p.name as product_name
		FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_lb_rt` splr
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp
				on splr.prefixid = sp.prefixid AND splr.spid = sp.spid
        LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
                on sp.productid = p.productid
		WHERE 	splr.`prefixid` = '{$config['default_prefix_id']}'
			AND splr.`switch`='Y'
			AND sp.`switch`='Y'
            AND p.`switch` = 'Y'
			AND sp.`behav`='lb'
			AND NOW() between sp.`ontime` and sp.`offtime`
			AND sp.`productid` > 0
			AND sp.`broadcast_use` = 'Y'
			AND	splr.lbuserid = '{$lbuserid}'
		";
		if(!empty($big)) {
		   $query.=" AND p.`is_big` = '{$big}' ";
		}
		if(!empty($productid)) {
		   $query.=" AND sp.`productid` = '{$productid}' ";
		}
		$query.=" ORDER BY sp.spid ASC ";

        $table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 *	直播殺價卷取得
 	 *	$uid			int			會員編號
 	 *	$spid			int			活動編號
 	 *	$productid		int			商品編號
	 *	$lbuserid		int			直播主編號
	 *  $behav    		string  	使用行為, user_regster:新戶註冊, user_deposit充值, b分享, d系統, e序號, f得標, lb直播, ad廣告送殺價卷
	 *	$fromuserid		int			分享回饋人會員編號
	 */
	public function insertUserOscode($uid, $spid, $productid, $lbuserid, $behav, $fromuserid=0)	{
		global $db, $config;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		SET
			prefixid = '{$config['default_prefix_id']}',
			userid = '{$uid}',
			spid = '{$spid}',
			productid = '{$productid}',
			behav = '{$behav}',
			amount = 1,
            serial='',
			lbuserid = '{$lbuserid}',
			fromuserid = '{$fromuserid}',
			insertt = NOW()
		";
		$db->query($query);
		$ebhid=$db->_con->insert_id;
		return $ebhid;
	}

	/*
	 *	查詢是否送過播殺價卷
 	 *	$uid			int			會員編號
 	 *	$spid			int			活動編號
	 *  $behav    		string  	使用行為, user_regster:新戶註冊, user_deposit充值, b分享, d系統, e序號, f得標, lb直播, ad廣告送殺價卷
	 */
	public function getUserOscode($uid, $spid,$behav="") {
		global $db, $config;

		if($behav != "") {
			$query = "SELECT count(*) count
				FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
				WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$uid}'
				AND `spid` = '{$spid}'
				AND `behav` = '{$behav}'
				AND `switch` = 'Y'
			";
		} else {
			$query = "SELECT count(*) count
				FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
				WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND `userid` = '{$uid}'
				AND `spid` = '{$spid}'
				AND `switch` = 'Y'
			";
		}

		//只撈自己領的券
		$query.=" AND `fromuserid` = 0 ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record']) ) {
			return $table['table']['record'];
		}
		return false;
	}

    // 由第三方sso uid 取得登入資料
    public function get_user($uid, $type='', $uid2='') {
		global $db, $config;

        if (empty($uid) && empty($uid2)) {
			return false;
		}

		$query = "SELECT u.*, s.ssoid, s.name as provider_name, s.uid as provider_uid, s.uid2 as union_id, s.switch as sso_switch
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso` s
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` us ON
			s.prefixid = us.prefixid
			AND s.ssoid = us.ssoid
			AND us.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u ON
			us.prefixid = u.prefixid
			AND us.userid = u.userid
			AND u.switch = 'Y'
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.name = '{$type}'
			AND s.switch = 'Y'
			AND u.userid IS NOT NULL
		";

		if (!empty($uid) && !empty($uid2)) {
			$query .= "AND (s.uid = '{$uid}' OR s.uid2 = '{$uid2}')";
		}

		if (!empty($uid) && empty($uid2)) {
			$query .= "AND s.uid = '{$uid}'";
		}

		if (empty($uid) && !empty($uid2)) {
			$query .= "AND s.uid2 = '{$uid}'";
		}

		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])) {
			return false;
		} else if($table['table']['record'][0]['switch'] != 'Y' || $table['table']['record'][0]['sso_switch'] != 'Y') {
			return false;
		}
		return $table['table']['record'][0];
	}

    // 未使用
    public function get_sso($arrCond) {
		global $db, $config;

		$query = "select ssoid FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
		           where prefixid = '{$config['default_prefix_id']}'
                    and  switch = 'Y' ";

        if(!empty($arrCond['name'])) {
           $query.=" AND name= '{$arrCond['name']}' ";
        }
        if(!empty($arrCond['uid']) && !empty($arrCond['uid2'])) {
           $query.=" AND ( uid='{$arrCond['uid']}' OR uid2= '{$arrCond['uid2']}') ";
        }
        if(!empty($arrCond['uid']) && empty($arrCond['uid2'])) {
           $query.=" AND uid='{$arrCond['uid']}' ";
        }
        if(empty($arrCond['uid']) && !empty($arrCond['uid2'])) {
           $query.=" AND uid2='{$arrCond['uid2']}' ";
        }

		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'][0]) ) {
			return false;
		} else {
			return $table['table']['record'][0];
		}
	}

    // 未使用
    public function insertSSO($arrCreate) {
		global $db, $config;
		$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
		set prefixid = '{$config['default_prefix_id']}',
			seq = '0',
			switch = 'Y',
			insertt = NOW(),
			name = '{$arrCreate['name']}',
			uid = '{$arrCreate['uid']}',
			uid2 = '{$arrCreate['uid2']}',
			sso_data = '{$arrCreate['sso_data']}'
		";
		$ret = $db->query($query);
		if($ret>0) {
		   return $db->_con->insert_id;
		} else {
		   return false;
		}
    }

    // 未使用
    public function insertSSO_rt($userid, $ssoid) {
		global $db, $config;
		$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt`
			set prefixid = '{$config['default_prefix_id']}',
				userid = '{$userid}',
				ssoid = '{$ssoid}',
				seq = '0',
				switch = 'Y',
				insertt = NOW()
		";
		$ret = $db->query($query);
		if($ret>0) {
			return $db->_con->insert_id;
		} else {
			return false;
		}
    }

	/*
	 *	更新送卷統計資料
	 *	$onum						int						活動組數
	 *	$spid						int						活動編號
	 */
    public function updateScodePromote($onum, $spid) {
		global $db, $config;
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		SET
			amount=amount+1,
			scode_sum=scode_sum+".$onum."
		WHERE spid='".$spid."'";

		$ret = $db->query($query);
		if($ret>0) {
		   return $db->_con->insert_id;
		} else {
		   return false;
		}
    }


	/*
	 *	直播間資料取得
 	 *	$lbid			int			直播間編號
	 *	$lbuserid		int			直播主編號
	 */
	public function getLiveBroadcastRoom($lbid, $lbuserid='', $pcid='')	{
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT lb.lbid, lb.lbuserid, lb.name, lb.description, lb.thumbnail, lb.lb_url, lb.share_link, lb.promote_link, lb.adpage_link, lb.keyword, lb.pcid, lb.ontime, lb.offtime, lb.uptime, lbu.lbname,
		 								unix_timestamp(lb.ontime) as unix_ontime, unix_timestamp(lb.offtime) as unix_offtime, unix_timestamp() as `unix_now`
							FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}live_broadcast` lb
					left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}live_broadcast_user` lbu
								on lb.prefixid = lbu.prefixid AND lb.lbuserid = lbu.lbuserid
							WHERE lb.`prefixid` = '{$config['default_prefix_id']}'
								AND lb.`switch` = 'Y'";
		//限定直播中才輸出
		//$query = $query." AND lb.ontime <= NOW() AND lb.offtime > NOW()";

		if(!empty($lbid)) {
		   $query.=" AND lb.`lbid` = {$lbid} ";
		}
		if(!empty($lbuserid)) {
		   $query.=" AND lb.`lbuserid` = {$lbuserid} ";
		}
		if(!empty($pcid)) {
		   $query.=" AND lb.`pcid` = {$pcid} ";
		}
		$query.=" ORDER BY lb.ontime ASC, lb.lbid DESC LIMIT 1 ";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record']) ) {
			return $table['table']['record'][0];
		}
		return false;
	}


}
?>
