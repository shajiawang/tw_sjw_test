<?php
/**
 * Issues Model 模組
 */

class IssuesModel {
    public $id;
    public $email;
    public $ok;
    public $msg;

    public function __construct() {
        global $db;
        
        $this->id = isset($_SESSION['auth_id']) ? $_SESSION['auth_id'] : 0;
        $this->email = isset($_SESSION['auth_email']) ? $_SESSION['auth_email'] : '';
        $this->ok = false;

        return $this->ok;
    }

	/*
	 * 寫入客戶留言
	 * $data	array	新增資料項目 
	 */		
	public function insert_Issues($data) {
		
        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues` 
					SET `title` = '{$data['title']}',
						`content` = '{$data['content']}',
						`ispid` = '{$data['ispid']}',
						`ispid2` = '{$data['ispid2']}',
						`ispid3` = '{$data['ispid3']}',
						`uicid` = '{$data['uicid']}',
						`userid` = '{$data['userid']}',
						`info` = '{$data['info']}',
						`status` = '1'";
		$db->query($query);
		return $spointid = $db->_con->insert_id;
	}
	
	/*
	 * 更新客戶留言
	 * $Uiid	 	int 	留言編號
	 * $data		array	更新資料項目
	 */		
	public function update_Issues($uiid, $data) {
		global $db, $config;
	
		$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues` SET ";
		
		if(isset($data['status']) ){ 
			$query .= "`status` = '{$data['status']}', "; 
		}
		if(isset($data['ispid']) ){ 
			$query .= "`ispid` = '{$data['ispid']}', "; 
		}
		if(isset($data['ispid2']) ){ 
			$query .= "`ispid2` = '{$data['ispid2']}', "; 
		}
		if(isset($data['ispid3']) ){ 
			$query .= "`ispid3` = '{$data['ispid3']}', "; 
		}
		
		$query .= "`modifyt` = NOW()
				WHERE `uiid` = '{$uiid}'";
		$db->query($query);		
    }
	
	public function update_Issues_Reply($uiid, $userid) {
		global $db, $config;
	
		$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_reply` SET 
					`readtime` = NOW(),
					`modifyt` = NOW()
				WHERE `uiid` = '{$uiid}' AND `userid`= '{$userid}'";
		$db->query($query);		
    }		

	/*
	 * 寫入客戶回應留言
	 * $data	array	新增資料項目 
	 */		
	public function insert_Issues_Reply($data) {
        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_reply` SET 
				`uiid` = '{$data['uiid']}',
				`userid` = '{$data['userid']}', 
				`content` = '{$data['content']}',
				`insertt` = NOW(),
				`modifyt` = NOW()";			
		$db->query($query);
		return $id = $db->_con->insert_id;			
	}
	public function update_reply_rpfid($uirid, $rpfid) {
        global $db, $config;
	
		$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_reply` SET 
					`rpfid` = '{$rpfid}',
					`modifyt` = NOW()
				WHERE `uirid` = '{$uirid}'";			
		$db->query($query);
	}
	
	/*
	 * 取得客戶留言清單
	 * $uiid	int 	留言編號
	 * $uicid	int 	分類編號 
	 * $status	int 	留言狀態 
	 * $userid	int 	使用者編號
	 * $from	int 	來源  	 
	 */		
	public function get_Issues_List($userid, $uiid='') {
        global $db, $config;
		
		//SQL指令 ic.name, ic.description, ic.seq, 
		$query = "SELECT i.*, up.nickname as username
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues` i
				LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON i.userid = up.userid						
				WHERE i.`userid` = {$userid} ";
		
		if (!empty($uiid)){
			$query .= " AND i.uiid = {$uiid}";	
		}
		//if (!empty($uicid)){ $query .= " AND i.uicid = {$uicid}"; }
		//if (!empty($status)){ $query .= " AND i.status = {$status}"; }
		
		$query .= " ORDER BY i.modifyt DESC ";
		
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)){
			$perpage = $config['max_page'];
		}

		if($num)
		{
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			foreach($table['table']['record'] as $tk => $tv) {
				$table['table']['record'][$tk]['insertt'] = Date("Y-m-d", strtotime($tv['insertt']));
				$table['table']['record'][$tk]['stat_color'] = ($tv['status']=='2')?'red':'gray';
				$table['table']['record'][$tk]['stat_text'] = ($tv['status']=='2')?'已回覆':'';
			}			

			return $table['table'];
		}

		return false;		
    }
	/*
	public function get_Issues_List($uiid='', $uicid='', $status='', $userid='', $from='') {
        global $db, $config;
		
		if ($from == 'list') {
			//SQL指令
			$query = "SELECT i.*, ic.name, ic.description, ic.seq, up.nickname as username 
						FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues` i
						LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_category` ic ON i.uicid = ic.uicid 
						LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON i.userid = up.userid
						WHERE i.status >= '1'";
		}else{
			//SQL指令
			$query = "SELECT i.*, ic.name, ic.description, ic.seq, up.nickname as username
						FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues` i
						LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_category` ic ON i.uicid = ic.uicid
						LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON i.userid = up.userid						
						WHERE i.status >= '1'";
		}
		
		if (!empty($uiid)){
			$query .= " AND i.uiid = {$uiid}";	
		}
		if (!empty($uicid)){
			$query .= " AND i.uicid = {$uicid}";
		}
		if (!empty($userid)){
			$query .= " AND i.userid = {$userid}";
		}				
		if (!empty($status)){
			$query .= " AND i.status = {$status}";
		}
		
		$query .= " ORDER BY i.insertt DESC, i.uicid  DESC ";
		
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)){
			$perpage = $config['max_page'];
		}

		if($num)
		{
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

	
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			foreach($table['table']['record'] as $tk => $tv)
			{
				$table['table']['record'][$tk]['insertt'] = Date("Y-m-d", strtotime($tv['insertt']));
			}			

			return $table['table'];
		}

		return false;		
    }*/	

	/*
	 * 取得客戶留言細節及相關回應內容
	 * $uiid	int 	留言編號
	 * $uicid	int 	分類編號 
	 * $status	int 	留言狀態 
	 * $userid	int 	使用者編號 		 
	 */		
	public function get_Issues_Detail($uiid, $userid, $pt=0){
        global $db, $config;
		
		//SQL指令
		$query = "SELECT `uirid`, `content`, `insertt`, `status`, `adminid`, `rpfid`, `readtime`
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_reply`
				WHERE `uiid` = {$uiid}";
		
		if(!empty($pt) ){
			$pti = intval($pt);
			$query .= " AND `uirid` < {$pt} ";
		}
		
		$query .= " ORDER BY `uirid` DESC";
		
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = 3;
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = $query . " LIMIT ". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		}
		
		return false;		
    }
	
	/*
	public function get_Issues_Detail($uiid, $userid, $uicid='', $status='')
        global $db, $config;
		
		//SQL指令
		//$query = "SELECT ir.`uirid`, ir.`content`, ir.`insertt`, ir.`status`, ir.`adminid`, ir.`rpfid`, ir.`readtime`
		//		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues` i
		//		LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_reply` ir ON i.uiid = ir.uiid 
		//		WHERE i.`uiid` = {$uiid}
		//			AND i.`userid` = {$userid} "; //WHERE i.status >= 1";
		//if (!empty($uiid)){$query .= " AND i.uiid = {$uiid}";}
		//if (!empty($uicid)){ $query .= " AND i.uicid = {$uicid}"; }
		//if (!empty($userid)){ $query .= " AND i.userid = {$userid}"; }						
		
		$query = "SELECT ir.`uirid`, ir.`content`, ir.`insertt`, ir.`status`, ir.`adminid`, ir.`rpfid`, ir.`readtime`
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_reply` ir
				WHERE ir.`uiid` = {$uiid}";
		$query .= " ORDER BY ir.`insertt` DESC";
		
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		}
		return false;		
    }*/
	
	/*
	 * 取得客戶留言分類
	 */		
	public function get_Issues_category() {
        global $db, $config;

		//SQL指令
		$query = "SELECT uicid, name, seq  
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_issues_category`
					WHERE switch = 'Y'";
				
		$query .= " ORDER BY seq ASC";
		
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if (empty($perpage)){
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		}
		
		return false;		
    }		

}
?>