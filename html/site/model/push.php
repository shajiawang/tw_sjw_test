<?php
/**
 * Push Model 模組
 */

class PushModel {

	public function get_auto_push_msg($pushid=""){
		global $db, $config;

		if (empty($pushid)) {
			return false;
		}

		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "	SELECT title,body,action,productid,url_title,url,page
					FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_msg` pm
					WHERE switch = 'Y'
					AND pushed = 'N'
					AND is_hand = 'N'
					AND pushid = '{$pushid}'";

		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])){
			return $table['table']['record'];
		}else{
			return false;
		}
	}

	public function get_hand_push_msg($pushid=''){
		global $db, $config;

		$where = "AND pm.pushid = '{$pushid}'";
		if (empty($pushid)) {
			$where = "";
		}

		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "	SELECT pm.pushid, pm.title, pm.body, pm.action, pm.productid, pm.url_title,
						   pm.url, pm.sendto, pm.page, sp.name product_name
					FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_msg` pm
					LEFT JOIN saja_shop.saja_product sp ON pm.productid = sp.productid
						AND sp.switch = 'Y'
					WHERE pm.switch = 'Y'
						AND pm.pushed = 'N'
						AND pm.is_hand = 'Y'
						AND pm.sendtime < now()
					{$where}";

		//取得資料

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])){
			return $table['table']['record'];
		}else{
			return false;
		}
	}

	public function used_hand_push_msg($pushid=""){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		if(!empty($pushid)){
			$pushid = implode($pushid, ',');
			$query = "UPDATE {$config['db'][2]['dbname']}.{$config['default_prefix']}push_msg
				SET pushed = 'Y'
				WHERE pushed = 'N'
				AND pushid in ({$pushid});";
			$db->query($query);
			return true;
		}else{
			return false;
		}
	}

	public function record_push_msg($productid,$title,$msg_body,$sendto){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		$title=addslashes($title);
        $msg_body=addslashes($msg_body);

		if(!empty($productid)){
			try{
				$query = "
					INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_msg_history`
					(
						title,
						body,
						action,
						productid,
						sender,
						pushed,
						is_hand,
						switch,
						push_type,
						sendto,
						sendtime,
						insertt
					)
					VALUE
					(
						'{$title}',
						'{$msg_body}',
						'0',
						'{$productid}',
						'12',
						'Y',
						'N',
						'Y',
						'S',
						'{$sendto}',
						NOW(),
						NOW()
					)
					";
				return $db->query($query);
			}catch(Exception $e){
				return $false;
			}
		}else{
			return false;
		}
	}

public function get_broadcast_history_detail($pushid,$userid,$push_type=''){
		global $db, $config;

		$where = "AND pm.pushid = '{$pushid}'";
		$db = new mysql($config["db2"]);
		$db->connect();
		if ($push_type==''||$push_type=='C'||$push_type=='H'){
				$query = "	SELECT pm.pushid, pm.sendtime, pm.title, pm.body, pm.action, pm.productid, pm.url_title,
						   pm.url, pm.sendto, pm.page, sp.name product_name
					FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_msg` pm
					LEFT JOIN saja_shop.saja_product sp ON pm.productid = sp.productid
						AND sp.switch = 'Y'
					WHERE pm.switch = 'Y'
						AND	pm.pushid= '{$pushid}'
						AND pm.sendtime < now()
					{$where}";
		}else{
				$query = "	SELECT pm.pushid, pm.sendtime, pm.title, pm.body, pm.action, pm.productid, pm.url_title,
						   pm.url, pm.sendto, pm.page, sp.name product_name
					FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_msg_history` pm
					LEFT JOIN saja_shop.saja_product sp ON pm.productid = sp.productid
						AND sp.switch = 'Y'
					WHERE pm.switch = 'Y'
						AND	pm.pushid= '{$pushid}'
						AND pm.sendtime < now()
					{$where}";
		}
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])){
			if (empty($table['table']['record'][0]['sendto'])){
				return $table;
			}else{
				$sid=explode(',',$table['table']['record'][0]['sendto']);
				if (in_array($userid,$sid)){
					return $table;
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
	}
    public function update_broadcast_history_status($pushid,$userid,$push_type='',$method_type){
		global $db, $config;


		$db = new mysql($config["db2"]);
		$db->connect();
		//push_type 為S時讀個人
		$target_table=($push_type==''||$push_type=='C'||$push_type=='H')?'push_msg':'push_msg_history';
		$query="select sendto from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}{$target_table}` where pushid= '{$pushid}'";
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])){
			//update publish
			if ($push_type==''||$push_type=='C'||$push_type=='H'){
				//公告的閱讀記錄在user_profile
				$query1="select publish_read,del_rec from  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` where userid='{$userid}'";
				$table1 = $db->getQueryRecord($query1);
				$publish_read=empty($table1['table']['record'][0]['publish_read']) ?0:$table1['table']['record'][0]['publish_read'];
				$del_rec=empty($table1['table']['record'][0]['del_rec']) ?0:$table1['table']['record'][0]['del_rec'];
				$delid=explode(',', $del_rec);
				$doneid=explode(',',$publish_read);
				if ($method_type=='POST'){
					if (!(in_array($pushid,$doneid))) {
	                    //確認是否是無指定對像的公告
						if (empty($table['table']['record'][0]['sendto'])	){
						    if ($publish_read==0){
							    $publish_read=$pushid;
						    }else{
							    $publish_read.=",$pushid";
						    }
						    $query = "UPDATE  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
						    SET publish_read = '".$publish_read."'
						    WHERE userid = '{$userid}'";
						    $db->query($query);
							return true;
						}else{
							//確認自己是否在接受對像中
						    $sid=explode(',',$table['table']['record'][0]['sendto']);
							if (in_array($userid,$sid)){
							    if ($publish_read==0){
								    $publish_read=$pushid;
							    }else{
								    $publish_read.=",$pushid";
							    }

							    $query = "UPDATE  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							    SET publish_read = '".$publish_read."'
							    WHERE userid = '{$userid}'";
							    $db->query($query);
								return true;
							}else{
								return false;
							}

						}
					}
				}else{
				if (!(in_array($pushid,$delid))) {
	                    //確認是否是無指定對像的公告
						if (empty($table['table']['record'][0]['sendto'])	){
						    if ($del_rec==0){
							    $del_rec=$pushid;
						    }else{
							    $del_rec.=",$pushid";
						    }
						    $query = "UPDATE  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
						    SET del_rec = '".$del_rec."'
						    WHERE userid = '{$userid}'";
						    $db->query($query);
							return true;
						}else{
							//確認自己是否在接受對像中
						    $sid=explode(',',$table['table']['record'][0]['sendto']);
							if (in_array($userid,$sid)){
							    if ($del_rec==0){
								    $del_rec=$pushid;
							    }else{
								    $del_rec.=",$pushid";
							    }
							    $query = "UPDATE  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							    SET del_rec = '".$del_rec."'
							    WHERE userid = '{$userid}'";
							    $db->query($query);
								return true;
							}else{
								return false;
							}

						}
					}
				}
				//檢查是否已讀過此公告
				
				return true;
			}else{
				//更新個人閱讀記錄
				if ($table['table']['record'][0]['sendto']==$userid){
					//update private
					if ($method_type=='POST'){
						$query = "UPDATE {$config['db'][2]['dbname']}.{$config['default_prefix']}push_msg_history
						SET done = '1'
						WHERE pushid = '{$pushid}'";
					}else{
						$query = "UPDATE {$config['db'][2]['dbname']}.{$config['default_prefix']}push_msg_history
						SET del = '1'
						WHERE pushid = '{$pushid}'";						
					}
					$db->query($query);
					return true;
				}else{
					return false;
				}
			}
		}else{
			return false;
		}
	}
    /*
	* get_broadcast_history 取得目標被推播記錄
	*/
	public function get_broadcast_history($userid,$page=0){
		global $db, $config;
		$perpage = 10;
		$db = new mysql($config["db"]);
		$db->connect();
		if (empty($page)) $page=1;
		$query="select publish_read,del_rec from  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` where userid='{$userid}'";
		$table = $db->getQueryRecord($query);
		//var_dump($query);
		$publish_read=empty($table['table']['record'][0]['publish_read']) ?0:$table['table']['record'][0]['publish_read'];
		$del_rec=empty($table['table']['record'][0]['del_rec']) ?0:$table['table']['record'][0]['del_rec'];		
/*
		$querycount = "select count(*) as num from ((select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0)or (isnull(sendto) and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='C' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0)or (isnull(sendto) and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='C' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0)or (isnull(sendto) and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='H' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0)or (isnull(sendto) and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='H' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'Y' as for_single,push_type,groupset,done,sendto from `saja_channel`.`saja_push_msg_history` where pushed='Y' and switch='Y' and sendto = '$userid' and del='0' ))as b";


		$query =  "select * from ((select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (isnull(sendto) and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='C' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where (( position(',$userid,' in CONCAT(',',sendto,','))>0) or (isnull(sendto) and force_show='Y') and pushid not in($del_rec))  and pushed='Y' and switch='Y' and push_type='C' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (isnull(sendto) and force_show='Y') and pushid not in($del_rec)) and pushed='Y' and switch='Y' and push_type='H' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (isnull(sendto) and force_show='Y') and pushid not in($del_rec)) and pushed='Y' and switch='Y' and push_type='H' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'Y' as for_single,push_type,groupset,done,sendto from `saja_channel`.`saja_push_msg_history` where pushed='Y' and switch='Y' and sendto = '$userid' and del='0' ))as b"." order by sendtime desc limit ".(($page-1)*$perpage).",10";
*/
		$querycount = "select count(*) as num from ((select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0)or (isnull(sendto) and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='C' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (sendto='' and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='C' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (sendto='' and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='H' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (sendto='' and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='H' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'Y' as for_single,push_type,groupset,done,sendto from `saja_channel`.`saja_push_msg_history` where pushed='Y' and switch='Y' and sendto = '$userid' and del='0' ))as b";


		$query =  "select * from ((select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (isnull(sendto) and force_show='Y'))  and pushed='Y' and switch='Y' and push_type='C' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (sendto='' and force_show='Y') and pushid not in($del_rec))  and pushed='Y' and switch='Y' and push_type='C' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'1' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (sendto='' and force_show='Y') and pushid not in($del_rec)) and pushed='Y' and switch='Y' and push_type='H' and pushid in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'N' as for_single,push_type,groupset,'0' as done,sendto from `saja_channel`.`saja_push_msg` where ((position(',$userid,' in CONCAT(',',sendto,','))>0) or (sendto='' and force_show='Y') and pushid not in($del_rec)) and pushed='Y' and switch='Y' and push_type='H' and pushid not in($publish_read) and pushid not in($del_rec))union
(select pushid,title,body,sendtime,'Y' as for_single,push_type,groupset,done,sendto from `saja_channel`.`saja_push_msg_history` where pushed='Y' and switch='Y' and sendto = '$userid' and del='0' ))as b"." order by sendtime desc limit ".(($page-1)*$perpage).",10";

		//取得資料

//總筆數
		$getnum = $db->getQueryRecord($querycount);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$pageset['rec_start']=($page-1)*$perpage+1;
			$pageset['totalcount']=$num;
			$pageset['totalpages']=ceil($num/$perpage);
			$pageset['page']=$page;

			//取得資料
			$table = $db->getQueryRecord($query);
			//var_dump($query);
		} else {
			$table['table']['record'] = '';
		}
			//var_dump($table['table']['record']);
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $pageset;
/*
			foreach($table['table']['record'] as $tk => $tv)
			{
				//中標價
				if($tv['price']) {
					$price = round($tv['price']);
				} else {
					$price = 0;
				}
				$table['table']['record'][$tk]['price'] = $price;
				$table['table']['record'][$tk]['nickname'] = urldecode($tv['nickname']);

				//市價
				$table['table']['record'][$tk]['retail_price'] = round($tv['retail_price']); //sprintf("%0.2f", $tv['retail_price']);
			}*/

			return $table;
		}
		return false;
	}

	public function get_product_list($mins=30,$pushed_product_id=''){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		$where = "";
		if (!empty($pushed_product_id)){
			$pushed_product_id = rtrim($pushed_product_id,',');
			$where = "AND sp.productid NOT IN ({$pushed_product_id}) ";
		}

		$secoed = $mins*60;

		$query = "	SELECT
						sp.productid, sp.`name` product_name, su.userid, su.nickname, udk.token
					FROM
						saja_shop.saja_product sp
					LEFT JOIN saja_shop.saja_history sh ON sh.productid = sp.productid
					AND sh.switch = 'Y'
					LEFT JOIN saja_user.saja_user_profile su ON su.userid = sh.userid
					AND su.switch = 'Y'
					LEFT JOIN saja_user.saja_user_device_token udk on sh.userid = udk.userid
					AND udk.switch = 'Y'
					WHERE
						sp.switch = 'Y'
					AND su.userid IS NOT NULL
					AND udk.token IS NOT NULL
					AND TIMESTAMPDIFF(SECOND, now(), sp.offtime) between 0 AND {$secoed}
					{$where}
					GROUP BY
						token,
					 	sp.productid";

		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])){
			return $table['table']['record'];
		}else{
			return false;
		}
	}

	public function get_winner($productid=""){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		if (!empty($productid)) {
			$query = "SELECT
					pgp.userid, su.nickname, udk.token, p.`name` product_name
				FROM
					saja_shop.saja_pay_get_product pgp
				LEFT JOIN saja_shop.saja_product p ON p.productid = pgp.productid
				AND p.switch = 'Y'
				LEFT JOIN saja_user.saja_user_profile su ON su.userid = pgp.userid
				AND su.switch = 'Y'
				LEFT JOIN saja_user.saja_user_device_token udk on udk.userid = pgp.userid
				AND udk.switch = 'Y'
				WHERE
					pgp.switch = 'Y'
				AND pgp.productid = '{$productid}' ";

			//取得資料
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])){
				return $table['table']['record'];
			}else{
				return false;
			}
		}
	}

	public function get_token($userid=''){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		$where = "";
		if ($userid != "") {
			$where = "AND userid in ({$userid})";
		}
		$query = "SELECT udk.token
			FROM saja_user.saja_user_device_token udk
			WHERE switch = 'Y' ".$where;

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])){
			$token = array();
			foreach ($table['table']['record'] as $key => $value) {
				$token[]=$value['token'];
			}
			$token = array_chunk($token, 1000);
			return $token;
		}else{
			return false;
		}
	}

	public function get_productname($productid=''){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		if ($productid!='') {
			$query = "SELECT p.name product_name
				FROM saja_shop.saja_product p
				WHERE switch = 'Y'
				AND productid = '{$productid}' ";

			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])){
				return $table['table']['record'][0]['product_name'];
			}else{
				return "";
			}

		}
	}

	public function get_nickname($userid=''){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		if ($userid!='') {
			$query = "SELECT nickname
				FROM saja_user.saja_user_profile up
				WHERE switch = 'Y'
				AND userid = '{$userid}' ";

			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])){
				return $table['table']['record'][0]['nickname'];
			}else{
				return "";
			}

		}
	}
	
	//20201225 取得排程推播送券
	public function get_push_scode_msg($pushid=''){
		global $db, $config;

		$where = "AND pm.pushid = '{$pushid}'";
		if (empty($pushid)) {
			$where = "";
		}

		$query = "SELECT pm.pushid, pm.ontime, pm.sendto, pmt.sendtime, pmt.push_serial, pmt.ttype, pmt.qty, pmt.usergroup
					,pmt.title, pmt.body, pmt.action, pmt.page, pmt.url_title, pmt.url, pmt.productid, sp.name product_name
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_msg` pm
			LEFT JOIN `saja_channel`.`saja_push_msg_time` pmt ON pmt.pushid = pm.pushid AND pmt.switch = 'Y'
			LEFT JOIN `saja_shop`.`saja_product` sp ON pm.productid = sp.productid AND sp.switch = 'Y'
			WHERE pm.switch = 'Y'
				AND pm.pushed = 'N'
				AND pm.push_type = 'G'
				AND now() BETWEEN pm.ontime AND pm.offtime
				AND DATE_FORMAT(pmt.lasttime, '%Y-%m-%d') <> CURDATE()
				AND (pmt.sendtime='00:00:00' OR pmt.sendtime < now() )
				AND pmt.pushed = 'N'
				{$where}";

		//取得資料
		//error_log("[get_push_scode_msg]sql: ".$query);
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])){
			return $table['table']['record'];
		}else{
			return false;
		}
	}
	public function used_push_scode($push_serial, $nexttime=''){
		global $db, $config;

		$_serial = explode("_", $push_serial);
		$pushid = $_serial[0];

		if(!empty($pushid)){
			//$pushid = implode($pushid, ',');
			$query = "UPDATE `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_msg_time` SET "; 
			//$query .= " pushed = 'Y'";
			$query .= " `lasttime` = now()";
			$query .= "	WHERE pushed = 'N'
				AND `push_serial`='{$push_serial}';";
			$db->query($query);
			return true;
		}else{
			return false;
		}
	}

	/* 已讀(yes_cnt)/未讀(no_cnt)*/
	public function getNotifyCnt($userid){
		global $db, $config;
		$singcnt=$db->getRecord("select count(*) as msgcnt,done from `saja_channel`.`saja_push_msg_history` where pushed='Y' and  done ='1' and sendto='{$userid}' and switch='Y'");
		$yes_cnt=(sizeof($singcnt)>0)?$singcnt[0]['msgcnt']:0;
		$singcnt=$db->getRecord("select count(*) as msgcnt,done from `saja_channel`.`saja_push_msg_history` where pushed='Y' and  done ='0' and sendto='{$userid}' and switch='Y'");
		$no_cnt=(sizeof($singcnt)>0)?$singcnt[0]['msgcnt']:0;
		//for all
		$rtn=$db->getRecord("SELECT count(*) as cnt FROM `saja_channel`.`saja_push_msg` where pushed='Y' and length(sendto)=0 or sendto is null");
		$allicnt=$rtn[0]['cnt'];
		$rtn=$db->getRecord("select count(*) as cnt from (SELECT concat(',',`sendto`,',') as sendto  FROM `saja_channel`.`saja_push_msg` where pushed='Y' and length(sendto)>0 ) as a where sendto like '%,".$userid.",%'");
		//all plus assign to you
		$multicnt=$rtn[0]['cnt']+$allicnt;
		$query1="SELECT publish_read FROM `saja_user`.`saja_user_profile` where userid='{$userid}'";
		$cnt=$db->getRecord($query1);
		if (!(empty($cnt[0]['publish_read']))){
			$terms=explode(',',$cnt[0]['publish_read']);
			$multi_y_cnt=sizeof($terms);
		}else{
			$multi_y_cnt=0;
		}
		$multi_n_cnt=$multicnt-$multi_y_cnt;
		$no_cnt+=$multi_n_cnt;
		$yes_cnt+=$multi_y_cnt;
		return array("no_cnt"=>$no_cnt,"yes_cnt"=>$yes_cnt);
	}

	//全部標示成己讀
	public function clrNotify($userid,$type=1){
		global $db, $config;
		try{
			if ($type==1){
				//mark single
				$db->query("update `saja_channel`.`saja_push_msg_history` set done ='1' where sendto ='{$userid}'");
				$mark='';
				var_dump("update `saja_channel`.`saja_push_msg_history` set done ='1' where sendto ='{$userid}'");
				//mark assign
				$rtn=$db->getRecord("select GROUP_CONCAT(pushid)as lst from ((select sendto,pushid from (SELECT concat(',',`sendto`,',') as sendto,pushid FROM `saja_channel`.`saja_push_msg` where pushed='Y' and length(sendto)>0 ) as a where sendto like '%,".$userid.",%')union (SELECT sendto ,pushid FROM `saja_channel`.`saja_push_msg` where pushed='Y' and length(sendto)=0 or sendto is null)) as a");
				$mark=$rtn[0]['lst'];
				$db->query("update `saja_user`.`saja_user_profile` set publish_read ='{$mark}' where userid='{$userid}'");
				var_dump("update `saja_user`.`saja_user_profile` set publish_read ='{$mark}' where userid='{$userid}'");
			}else{
				$db->query("update `saja_channel`.`saja_push_msg_history` set done ='0' where sendto ='{$userid}'");
				var_dump("update `saja_channel`.`saja_push_msg_history` set done ='0' where sendto ='{$userid}'");
				$db->query("update `saja_user`.`saja_user_profile` set publish_read ='' where userid='{$userid}'");
				var_dump("update `saja_user`.`saja_user_profile` set publish_read ='' where userid='{$userid}'");
			}
			return true;
		}catch(Exception $e){
			return $false;
		}
	}
}
?>
