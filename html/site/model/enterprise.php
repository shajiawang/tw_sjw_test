<?php
/**
 * Scode Model 模組
 */

class EnterpriseModel
{
	public $msg;
	public $sort_column = 'seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET){
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

	//統計單日營業情形(只計完成交易的)
	public function getOsVer(){
		global $db, $config;
		$os_type=htmlentities(strtolower($_REQUEST['os_type']));
		//總筆數
		$getnum = $db->getQueryRecord("select appversion from saja_channel.saja_enterprise_app_os where os_type='".$os_type."'");
		if (sizeof($getnum['table']['record'])>0){
			return $getnum['table']['record'][0]['appversion'];
		}else{
			return 0;
		}
	}

	//統計單日營業情形(只計完成交易的)
	public function bonus_rec($enterpriseid,$insertt,$type){
		global $db, $config;
		$set_sort = $this->set_sort();
		if ($type=='sum'){
			$query="select sum(total_price)  as ans " ;
		}else{
			$query="select count(*) as ans " ;
		}
		$query .= "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr
		LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` bs ON
		     bs.bonusid=evr.bonusid
		LEFT JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
			evr.userid=up.userid
		WHERE 	evr.prefixid = '{$config['default_prefix_id']}'
			AND evr.switch = 'Y'
			AND evr.vendorid = '{$enterpriseid}'
			and evr.tx_status in (4)";
		if ($insertt!='')$query.=" and evr.insertt like '".$insertt."%'";


		//總筆數
		$getnum = $db->getQueryRecord($query);
		return $getnum['table']['record'][0]['ans'];
	}

	public function store_bonus_list($enterpriseid,$ctype)	{
        global $db, $config;
        //如果要查退款的正項可以用 enterprise_refund的view
		//排序
		$set_sort = $this->set_sort();
		$enterprise_profile=$this->getEnterpriseProfile($enterpriseid);
		//搜尋
		//$set_search = $this->accept_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		switch ($ctype){
			case "0":
				$query_record = "SELECT evrid, prod_name, commit_time, IF(tx_status=4,total_bonus,(total_bonus*-1))as total_bonus, sys_profit, net_bonus, nickname,thumbnail_file,thumbnail_url,insertt,tx_status,IF(tx_status=4,'收款','退款 ') as ctype,fr ";
				$query = "from (SELECT evrid, prod_name, commit_time, evr.total_price as total_bonus, bs.sys_profit, (evr.total_price-bs.sys_profit) as net_bonus, up.nickname,up.thumbnail_file,up.thumbnail_url,IF(tx_status=4,evr.insertt,evr.modifyt) as insertt ,evr.tx_status, 'evr' as fr FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr
				LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` bs ON
				     bs.bonusid=evr.bonusid
				LEFT JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
					evr.userid=up.userid
				WHERE 	evr.prefixid = '{$config['default_prefix_id']}'
					AND evr.switch = 'Y'
					AND evr.vendorid = '{$enterpriseid}'
					and evr.tx_status in('-1','-2','4') group by evrid  union
					select evrid, prod_name, commit_time, total_bonus, sys_profit, net_bonus, nickname,thumbnail_file,thumbnail_url,insertt,tx_status,'refund' as fr from  `{$config['db'][3]['dbname']}`.`enterprise_refund` where
										vendorid = '{$enterpriseid}' group by evrid
					union SELECT evr.wid as evrid, '商家提現' as prod_name, '' as commit_time, amount as total_bonus, '0' as sys_profit, amount as net_bonus, '' as nickname, '' as thumbnail_file,'' as thumbnail_url,insertt, tx_status, 'withdraw' as fr FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}enterprise_withdraw` evr
				WHERE 	 evr.switch = 'Y'
					AND evr.esid = '{$enterpriseid}')ulog
				";
				$query .= ($set_sort) ? $set_sort : "ORDER BY insertt DESC";
				$getnum = $db->getQueryRecord($query_count . $query);
			break;
			case "1":
				$query_record = "SELECT evrid, prod_name, commit_time, total_bonus, sys_profit, net_bonus, nickname,thumbnail_file,thumbnail_url,insertt,tx_status,'收款' as ctype,fr ";
				$query = "from (SELECT evrid, prod_name, commit_time, evr.total_price as total_bonus, bs.sys_profit, (evr.total_price-bs.sys_profit) as net_bonus, up.nickname,up.thumbnail_file,up.thumbnail_url,evr.insertt,evr.tx_status,'evr' as fr FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr
				LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` bs ON
				     bs.bonusid=evr.bonusid
				LEFT JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
					evr.userid=up.userid
				WHERE 	evr.prefixid = '{$config['default_prefix_id']}'
					AND evr.switch = 'Y'
					AND evr.vendorid = '{$enterpriseid}'
					and evr.tx_status =4 union
					select evrid, prod_name, commit_time, total_bonus, sys_profit, net_bonus, nickname,thumbnail_file,thumbnail_url,insertt,tx_status,'refound' as fr from  `{$config['db'][3]['dbname']}`.`enterprise_refund` where
										vendorid = '{$enterpriseid}' group by evrid
					)ulog
				";
				$query .= ($set_sort) ? $set_sort : "ORDER BY insertt DESC";
				$getnum = $db->getQueryRecord($query_count . $query);
			break;
			case "2":
				$query_record = "SELECT evrid, prod_name, commit_time, (evr.total_price*(-1)) as total_bonus, bs.sys_profit, (evr.total_price-bs.sys_profit) as net_bonus, up.nickname,up.thumbnail_file,up.thumbnail_url,evr.modifyt as insertt,evr.tx_status,'退款' as ctype, 'evr' as fr ";
				$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr
				LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` bs ON
				     bs.bonusid=evr.bonusid
				LEFT JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
					evr.userid=up.userid
				WHERE 	evr.prefixid = '{$config['default_prefix_id']}'
					AND evr.switch = 'Y'
					AND evr.vendorid = '{$enterpriseid}'
					and evr.tx_status in ('-2','-1') group by evr.evrid
				";
				$query .= ($set_sort) ? $set_sort : "ORDER BY evr.modifyt DESC";
				$getnum = $db->getQueryRecord("select count(*)as num from (".$query_count . $query.")a");
			break;
			case "3":
				$query_record = "SELECT evr.wid as evrid, '商家提現' as prod_name, '' as commit_time, amount as total_bonus, '0' as sys_profit, amount as net_bonus, '' as nickname, '' as thumbnail_file,'' as thumbnail_url, insertt, tx_status,'提現' as ctype, 'withdraw' as fr ";
				$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}enterprise_withdraw` evr
				WHERE 	 evr.switch = 'Y'
					AND evr.esid = '{$enterpriseid}'
				";
				$query .= ($set_sort) ? $set_sort : "ORDER BY evr.insertt DESC";
				$getnum = $db->getQueryRecord("select count(*)as num from (".$query_count . $query.")a");
			break;
			default:
			break;
		}

		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
			for ($i=0;$i<sizeof($table['table']['record']);$i++){
				$table['table']['record'][$i]['total_bonus']=number_format($table['table']['record'][$i]['total_bonus']);
				if ($table['table']['record'][$i]['fr']=='withdraw'){
					$table['table']['record'][$i]['evrid_item']='提現編號:'.($table['table']['record'][$i]['evrid']);
					switch ($table['table']['record'][$i]['tx_status']) {
						case '2':
							$table['table']['record'][$i]['tx_status']='提現處理中';
							break;
						case '4':
							$table['table']['record'][$i]['tx_status']='提現已完成';
							break;
						default:
							$table['table']['record'][$i]['tx_status']='提現已取消';
							break;
					}
					$table['table']['record'][$i]['ctype']='提現';
					$table['table']['record'][$i]['total_bonus']='-'.abs($table['table']['record'][$i]['total_bonus']);
				}else{
					$table['table']['record'][$i]['prod_name']=$table['table']['record'][$i]['nickname'];
					$table['table']['record'][$i]['evrid_item']='訂單編號:'.($table['table']['record'][$i]['evrid']);
					$table['table']['record'][$i]['tx_status']= ($table['table']['record'][$i]['tx_status']=='4')?'交易完成':'交易取消';
					$table['table']['record'][$i]['total_bonus']=($table['table']['record'][$i]['total_bonus']>0)?'+'.$table['table']['record'][$i]['total_bonus']:$table['table']['record'][$i]['total_bonus'];
				}
				if(empty($table['table']['record'][$i]['thumbnail_url']) && !empty($table['table']['record'][$i]['thumbnail_file'])) {
					$table['table']['record'][$i]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/".$table['table']['record'][$i]['thumbnail_file'];
				} elseif(empty($table['table']['record'][$i]['thumbnail_url']) && empty($table['table']['record'][$i]['thumbnail_file'])) {
					$table['table']['record'][$i]['thumbnail_url'] =  ($table['table']['record'][$i]['ctype']=='提現')?$enterprise_profile['thumbnail_url']:IMG_URL.APP_DIR."/images/headimgs/_DefaultHeadImg.jpg";
				}
				unset($table['table']['record'][$i]['thumbnail_file']);
			}
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			return $table;
		}

		return false;
    }

	public function getEnterpriseDetail($eid) {
		global $db, $config;

		$query = "SELECT ea.enterpriseid, ea.account_contactor, ea.account_title, ea.account_phone, ea.execute_contactor, ea.execute_title, ea.execute_phone, ea.accountname, ea.accountnumber, ea.bank_id, ea.bankname, ea.bankbranch_id, ea.branchname,
						ep.companyname, ep.marketingname, ep.thumbnail_file, ep.thumbnail_url, ep.name, ep.uniform, ep.phone, ep.fax, ep.countryid, ep.channelid, ep.area, ep.cityid, ep.address, ep.email,
						es.owner, es.url, es.businessontime, es.businessofftime, es.contact_phone, es.contact_mobile, es.contact_email, es.service_email, es.turnover, es.profit_ratio, es.transaction, es.trade, es.marketingmodel, es.business_area, es.employees, es.franchise_store, es.seniority, es.source, es.card_demand, es.operation_overview,
						est.name as exchange_store_name, st.name as store_name
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` e
				   JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_account` ea
					 ON  e.enterpriseid=ea.enterpriseid
				   JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` ep
					 ON  e.enterpriseid=ep.enterpriseid
				   JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` es
					 ON  e.enterpriseid=es.enterpriseid
		LEFT OUTER JOIN  `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` est
					 ON  e.esid=est.esid
		LEFT OUTER JOIN  `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store` st
					 ON  e.storeid=st.storeid
		LEFT OUTER JOIN  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt` uer
					 ON  e.enterpriseid=uer.enterpriseid
				  WHERE e.prefixid = '{$config['default_prefix_id']}'
					AND e.enterpriseid = '{$eid}'
					AND e.switch = 'Y'
					LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['enterpriseid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}

	//算總提領不含取消的
	public function getTotalWithdraw($enterpriseid) {
		global $db, $config;
		$query = "SELECT SUM(amount) as total_withdraw
				  FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}enterprise_withdraw`
				 WHERE switch = 'Y' AND esid = '{$enterpriseid}' AND tx_status>'0' ";
		$table1 = $db->getQueryRecord($query);
		if(empty($table1['table']['record'])) {
		    return 0;
		}else{
			return round($table1['table']['record'][0]['total_withdraw']);
		}
	}

    // 取得擁有鯊魚點數
	public function getEnterpriseTotalBonus($enterpriseid) {
		global $db, $config;
		$query = "SELECT SUM(evr.total_bonus) as total_net_bonus
				  FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr
				 WHERE evr.prefixid = '{$config['default_prefix_id']}'
				   AND evr.switch = 'Y'
				   AND evr.vendorid = '{$enterpriseid}'
				   AND evr.tx_status='4' ";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'])) {
			return 0;
		}else{
			$total_withdraw=$this->getTotalWithdraw($enterpriseid);
			return (round($table['table']['record'][0]['total_net_bonus'])-$total_withdraw);
		}
	}

	//申請提現
	public function withDraw($enterpriseid,$amount){
		global $db, $config;
		$ret=array();
		$qrcode=array();
		$qrcode['type']=5;
		$qrcode['page']=0;
		$ret['retCode']=1;
		if ($amount>$this->getEnterpriseTotalBonus($enterpriseid)){
			$qrcode['msg']="提現金額不可以超過現有的點數 !";
		}else{
			try{
				$query =" INSERT INTO `saja_exchange`.`saja_enterprise_withdraw`
						 SET
						`esid`			= '{$enterpriseid}',
						`amount`	= '{$amount}',
						`tx_status`		= '2',
						`insertt`		= now() " ;
				error_log($query);
				$db->query($query);

				$qrcode['msg']="你的提現申請已收到 !";
			}catch(Exception $e){
				$qrcode['msg']=$e.getMessage();
			}
		}
		$ret['action_list']=$qrcode;
		return $ret;
	}

	//取得企業詳細資料
	public function getUserEnterprise($userid) {
		global $db, $config;

		$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt` uer
				  WHERE uer.prefixid = '{$config['default_prefix_id']}'
					AND uer.userid = '{$userid}'
					AND uer.switch = 'Y'
					LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['enterpriseid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}

	//取得企業詳細資料 by esid
	public function getUserEnterpriseE($esid) {
		global $db, $config;

		$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt` uer
				  WHERE uer.prefixid = '{$config['default_prefix_id']}'
					AND uer.enterpriseid = '{$esid}'
					AND uer.switch = 'Y'
					LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['enterpriseid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}

	//取得企業詳細資料
	public function getEnterpriseProfile($eid) {

		global $db, $config;

		$query = "SELECT ep.enterpriseid, ep.companyname, ep.marketingname, ep.thumbnail_file, ep.thumbnail_url, ep.name, ep.uniform, ep.phone, ep.fax, ep.countryid, ep.channelid, ep.area, ep.cityid, ep.address, ep.email
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` ep
				  WHERE ep.prefixid = '{$config['default_prefix_id']}'
					AND ep.enterpriseid = '{$eid}'
					AND ep.switch = 'Y'
					LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['enterpriseid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}

	//取得企業帳戶資料
	public function getEnterpriseAccount($eid) {

		global $db, $config;

		$query = "SELECT ea.enterpriseid, ea.enterpriseid, ea.account_contactor, ea.account_title, ea.account_phone, ea.execute_contactor, ea.execute_title, ea.execute_phone,
						ea.accountname, ea.accountnumber, ea.bank_id, ea.bankname, ea.bankbranch_id, ea.branchname
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_account` ea
				  WHERE ea.prefixid = '{$config['default_prefix_id']}'
					AND ea.enterpriseid = '{$eid}'
					AND ea.switch = 'Y'
					LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['enterpriseid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}

	//取得企業營業資料
	public function getEnterpriseShop($eid) {

		global $db, $config;

		// 店家名稱改抓saja_exchange_store的name
		$query = "SELECT est.name, es.enterpriseid, es.owner, es.url, es.businessontime, es.businessofftime, es.contact_phone, es.contact_mobile, es.contact_email, es.service_email, es.turnover,
						es.profit_ratio, es.transaction, es.trade, es.marketingmodel, es.business_area, es.employees, es.franchise_store, es.seniority, es.source,
						es.card_demand, es.operation_overview
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` es
				   JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` e
				     ON es.enterpriseid = e.enterpriseid
				   JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` est
				     ON e.esid = est.esid
				  WHERE es.prefixid = '{$config['default_prefix_id']}'
					AND es.enterpriseid = '{$eid}'
					AND es.switch = 'Y'
					AND e.switch='Y'
			        AND est.switch='Y'
					LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['enterpriseid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}

	//取得企業會員資料
	public function getEnterpriseUser($eid) {
		global $db, $config;

		$query = "SELECT u.userid, usa.verified
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
					LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt` uer
						ON u.userid=uer.userid
					LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa
						ON u.userid=usa.userid
				  WHERE u.prefixid = '{$config['default_prefix_id']}'
					AND uer.enterpriseid = '{$eid}'
					AND u.switch = 'Y'
				  LIMIT 1
		";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['userid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}

	//取得Qrcode交易資料
	public function getQrcodeTxRecord($evrid) {
		global $db, $config;

		$query=" SELECT evr.*, sum(IFNULL(b.amount,0)) as curr_bonus
			FROM saja_user.saja_user_profile up
			LEFT JOIN saja_cash_flow.saja_bonus b
			ON up.switch='Y' and b.switch='Y'  AND up.userid=b.userid
			LEFT JOIN saja_exchange.saja_exchange_vendor_record evr
			ON up.switch='Y' and evr.switch='Y' AND up.userid=evr.userid
			WHERE 1=1 ";
		$query.=(" AND evr.evrid='".$evrid."'");

		//error_log("[vendorConfirm2]".$query);
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'][0]['evrid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}
	//取得企業營業資料
	public function getTxEnterpriseShop($eid) {

		global $db, $config;

		// 店家名稱改抓saja_exchange_store的name
		$query = "SELECT * , st.name as marketingname
				FROM `saja_user`.`saja_enterprise_profile` p
				JOIN `saja_user`.`saja_enterprise_shop` s
				  ON p.enterpriseid = s.enterpriseid
				JOIN `saja_user`.`saja_enterprise` e
				  ON p.enterpriseid = e.enterpriseid
				LEFT OUTER JOIN `saja_exchange`.`saja_exchange_store` st
				  ON e.esid = st.esid
			   WHERE p.prefixid = '{$config['default_prefix_id']}'
			     AND p.enterpriseid = '".$eid."'
			     AND p.switch = 'Y'
				 AND s.switch = 'Y'
				 AND e.switch = 'Y'
				 AND st.switch = 'Y'";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['enterpriseid'])) {
		   return "";
		}
		return $table['table']['record'][0];
	}
	// 修改Qrcode交易資料
	public function updQrcodeTxRecord($arrUpd,$arrCond,$db) {
		global $db, $config;

		if(empty($arrUpd)) {
			return false;
		}

		$ret=false;
		$sql=" UPDATE saja_exchange.saja_exchange_vendor_record SET modifyt=NOW() ";
		foreach($arrUpd as $key => $value) {
		   $sql.=(" ,".$key."='".addslashes($value)."'");
		}

		$sql.=" WHERE 1=1 ";
		if(count($arrCond)>0) {
		   foreach($arrCond as $key => $value) {
			   $sql.=(" AND ".$key."='".addslashes($value)."'");
		   }
		}
		//error_log("[vendorConfirm2] ".$sql);
		$ret = $db->query($sql);
		return $ret;
	}

	public function rollBackTx($enterpriseid,$evrid){
		global $db, $config;
	 	$sql="SELECT evrid,total_price,commit_time,tx_status from saja_exchange.saja_exchange_vendor_record where evrid='{$evrid}' and vendorid='{$enterpriseid}'";
	 	$ordercheck=$db->getRecord($sql);
	 	$ret=array();

	 	if (sizeof($ordercheck)>0){
	 		if ((time()-strtotime(date($ordercheck[0]['commit_time'])))>300){
		 		$ret['retCode']=0;
		 		$ret['retMsg']='訂單已成立超過限定時間無法取消';
	 		}else{
	 			if ($ordercheck[0]['tx_status']=='-2'){
			 		$ret['retCode']=-10;
			 		$ret['retMsg']='此訂單已取消過';
	 			}else{
					$sql="SELECT bonusid from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` where behav='user_qrcode_tx' and tx_no='{$evrid}' and abs(amount)=".$ordercheck[0]['total_price'];
			 		$bonuscheck=$db->getRecord($sql);
			 		if (sizeof($bonuscheck)>0){
				 		$sql="SELECT bsid from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` where bonusid='".$bonuscheck[0]['bonusid']."' and enterpriseid=".$enterpriseid;
				 		$bonusstorecheck=$db->getRecord($sql);
				 		if (sizeof($bonusstorecheck)>0){
					 		$sql="SELECT orderid from `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` where (memo like '鲨鱼点支付%' or memo like '鯊魚點支付%') and tx_data like '%bonusid\":".$bonuscheck[0]['bonusid']."%'";
					 		$reccheck=$db->getRecord($sql);
					 		if (sizeof($reccheck)>0){
								try {
								    //兌換記錄
								    $db->query("UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` SET modifyt=NOW(),tx_status='-2' where prefixid='saja' and evrid='{$evrid}' ");
								    //紅利使用記錄
									$db->query("INSERT INTO  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`
									(`prefixid`, `userid`, `countryid`, `behav`, `amount`, `seq`, `memo`, `switch`, `insertt`, `modifyt`, `tx_no`)
									SELECT `prefixid`, `userid`, `countryid`, `behav`, abs(`amount`), `seq`, `memo`, `switch`, `insertt`, `modifyt`, concat(`tx_no`,'_')
									FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` where tx_no='{$evrid}' ");
								    //$db->query("UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` SET modifyt=NOW(),switch='N' where prefixid='saja' and tx_no='{$evrid}' ");
			                        //商家紅利點數收取記錄
									$db->query("UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` SET modifyt=NOW(),switch='N' where prefixid='saja' and bsid=".$bonusstorecheck[0]['bsid']);
									//訂單記錄
								    $db->query("UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` SET modifyt=NOW(),switch='N',status='5' where prefixid='saja' and orderid=".$reccheck[0]['orderid']);
								} catch (Exception $e) {
							 		$ret['retCode']=$e->getCode();
							 		$ret['retMsg']=$e->getMessage();
								}
						 		$ret['retCode']=1;
						 		$ret['retMsg']='交易已取消';
					 		}else{
						 		$ret['retCode']=-14;
						 		$ret['retMsg']='查無相關訂單資料';
					 		}
				 		}else{
					 		$ret['retCode']=-13;
					 		$ret['retMsg']='查無相關商家紅利資料';
				 		}
			 		}else{
				 		$ret['retCode']=-12;
				 		$ret['retMsg']='查無相關紅利資料';
			 		}
	 			}
	 		}
	 	}else{
	 		$ret['retCode']=-11;
	 		$ret['retMsg']='查無交換相關資料';
	 	}
	 	$ret['evrid']=$evrid;
	 	return $ret;
	}
}
?>