<?php
/**
 * Oscode Model 模組
 */

class OscodeModel {
	public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET) {
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

	/**
     * Search Method : set_search
     */
	public function accept_list_set_search() {
		global $status, $config;

		$rs = array();

		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp ON
			s.prefixid = sp.prefixid
			AND s.spid = sp.spid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
			s.prefixid = p.prefixid
			AND s.productid = p.productid
			AND p.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = "AND p.productid IS NOT NULL "; //"AND sp.spid IS NOT NULL AND sh.scodeid IS NOT NULL ";

		return $rs;
	}

	public function oscode_accept_list($userid, $productid='', $spid='') {
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->accept_list_set_search();

		$set_where = '';
		if (!empty($productid)) {
			//條件
			$set_where .= " and s.productid = '{$productid}'";
		}
		if (!empty($spid)) {
			//條件
			$set_where .= " and s.spid = '{$spid}'";
		}

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT s.*, sp.name spname, p.name, p.closed ";

		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` s
		{$set_search['join_query']}
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.switch = 'Y'
			AND s.userid = '{$userid}'
			AND s.serial = ''
		";

		$query .= $set_search['sub_search_query'];
		$query .= $set_where;
		$query .= ($set_sort) ? $set_sort : "ORDER BY s.insertt DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			return $table;
		}

		return false;
    }

	/**
     * Search Method : set_search
     */
	public function used_list_set_search() {
		global $status, $config;

		$rs = array();

		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
			s.prefixid = p.prefixid
			AND s.productid = p.productid
			AND p.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = "AND p.productid IS NOT NULL ";

		return $rs;
	}

	public function oscode_used_list($userid) {
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->used_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT s.*, p.name ";

		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` s
		{$set_search['join_query']}
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.switch = 'Y'
			AND s.userid = '{$userid}'
			AND used = 'Y'
		";

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : "ORDER BY s.used_time DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);

		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			return $table;
		}

		return false;
    }

	/**
     * Search Method : set_search
     */
	public function serial_list_set_search() {
		global $status, $config;

		$rs = array();

		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp ON
			s.prefixid = sp.prefixid
			AND s.spid = sp.spid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
			s.prefixid = p.prefixid
			AND s.productid = p.productid
			AND p.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = "AND p.productid IS NOT NULL "; //"AND sp.spid IS NOT NULL AND sh.scodeid IS NOT NULL ";

		return $rs;
	}

	public function oscode_serial_list($userid)	{
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->serial_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT s.*, sp.name spname, p.name, p.closed ";

		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` s
		{$set_search['join_query']}
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.switch = 'Y'
			AND s.userid = '{$userid}'
			AND s.serial != ''
			AND s.verified = 'Y'
		";

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : "ORDER BY s.modifyt DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			return $table;
		}

		return false;
    }

	//可用的殺價券列表 By Thomas 20160412
	public function oscode_available_list($userid) {
		global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//SQL指令
		$query_count = " SELECT count(*) as num from ( select count(*) as num ";
		$query_record = "SELECT s.oscodeid, s.userid, s.productid, s.behav, s.insertt,
								p.name, p.closed, count( * ) total_amount, unix_timestamp(p.offtime) as offtime, p.ptid, p.saja_fee,
								IFNULL(p.thumbnail_url,'') as thumbnail_url, IFNULL(pt.filename,'') as filename ";
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` s
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
					ON s.prefixid = p.prefixid
				   AND s.productid = p.productid
				   AND p.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
					ON p.ptid = pt.ptid
				   AND pt.switch = 'Y'
				 WHERE s.switch = 'Y'
				   AND s.userid = '{$userid}'
				   AND (
					 (s.used='N' AND s.serial ='')
					  OR
					 (s.used='N' AND s.serial !='' AND s.verified = 'N')
				   )
				   AND p.offtime > NOW()
		";
		$query .= $set_search['sub_search_query'];
		$query .= " group by s.productid ";
		$query .= ($set_sort) ? $set_sort : "ORDER BY s.insertt DESC";
		$query_count_last = " ) os";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)){
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		return false;
	}

    //收取殺價券統計列表(Frank.Kao -- 14/11/13 -- 14/12/22)
    public function oscode_accept($userid)
	{
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->accept_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num from ( select count(*) as num ";
		// $query_record = "SELECT s.*, sp.name spname, p.name, p.closed, count( * ) total_amount, p.offtime ";
		$query_record = "SELECT s.oscodeid, s.userid, s.productid, s.spid, s.behav, s.insertt, sp.name spname,
		                        p.name, p.closed, count( * ) total_amount, unix_timestamp(p.offtime) as offtime, p.ptid, p.saja_fee,
								IFNULL(p.thumbnail_url,'') as thumbnail_url, IFNULL(pt.filename,'') as filename ";
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` s
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		WHERE
			s.prefixid = '{$config['default_prefix_id']}'
			AND s.switch = 'Y'
			AND s.userid = '{$userid}'
			AND s.serial = ''
		";
		$query .= $set_search['sub_search_query'];
		$query .= " group by s.productid, s.spid ";
		$query .= ($set_sort) ? $set_sort : "ORDER BY s.insertt DESC";
		$query_count_last = " ) os";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			return $table;
		}

		return false;
    }

	/*
	 oscodeid 限定Ｓ碼id
	 userid 會員id
	 productid 購物商品id
	 spid S碼活動id
	 behav 使用行為
	 used 已使用
	 used_time 使用時間
	 amount 贈送組數
	 serial 序號
	 pwd S碼密碼
	 verified 激活
	 seq
	 insertt
	*/
	//插入S碼收取記錄
	public function insert_oscode($info, $uid, $memo) {
		global $db, $config;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		SET
           `prefixid`='{$config['default_prefix_id']}',
           `userid`='{$uid}',
           `spid`='{$info['spid']}',
           `behav`='{$info['behav']}',
           `amount`='{$info['num']}',
           `insertt`=NOW()
		";
		$db->query($query);
		$scodeid = $db->_con->insert_id;
	}

	//S碼 收取總數
	public function oscode_accept_sum($uid) {
		global $db, $config;

		$query = "SELECT count(*) amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND switch = 'Y'
			";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record'])) {
			return (int)$rs['table']['record'][0]['amount'];
		}

		return 0;
	}

	//S碼 序號收取總數
	public function oscode_active_sum($uid) {
		global $db, $config;

		$query = "SELECT count(*) num
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND serial !=''
				AND switch = 'Y'
			";
		$rs1 = $db->getQueryRecord($query);

		$query = "SELECT count(*) num
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND serial !=''
				AND verified = 'Y'
				AND switch = 'Y'
			";
		$rs2 = $db->getQueryRecord($query);

		if(!empty($rs1['table']['record']) && !empty($rs2['table']['record'])) {
			$rs['sum'] = (int)$rs1['table']['record'][0]['num'];
			$rs['act'] = (int)$rs2['table']['record'][0]['num'];
		} else {
			$rs['sum'] = $rs['active'] = 0;
		}

		return $rs;
	}

	//S碼 已用總數
	public function oscode_used_sum($uid) {
		global $db, $config;

		$query = "SELECT count(*) amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			WHERE
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND used = 'Y'
				AND switch = 'Y'
			";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record'])) {
			return (int)$rs['table']['record'][0]['amount'];
		}

		return 0;
	}

	//取得 S碼可用總數
	public function get_oscode($uid) {
		global $db, $config;

		$query = "SELECT count(*) amount
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` os
		left join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = os.`prefixid`
			and p.`productid` = os.`productid`
			and p.`offtime` > NOW()
			and p.`switch` = 'Y'
		WHERE
			os.`prefixid`='{$config['default_prefix_id']}'
			AND os.`userid`='{$uid}'
			AND (
			(os.used = 'N' AND os.serial ='') OR ( os.used = 'N' AND os.serial !='' AND os.verified = 'N') OR (os.used = 'N' AND os.verified = 'N')
			)
			AND os.switch = 'Y'
			AND p.offtime > NOW()
		";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record'])) {
			return (int)$rs['table']['record'][0]['amount'];
		}

		return 0;
	}

	// 2015/10/01 By Thomas
	//用戶($uid)對特定商品($productid)可用的殺價券總數
	public function get_prod_oscode($productid, $uid) {
		global $db, $config;

		$query = "SELECT count(*) amount
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` os
		left join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = os.`prefixid`
			and p.`productid` = os.`productid`
			and p.`offtime` > NOW()
			and p.`switch` = 'Y'
		WHERE
			os.`prefixid`='{$config['default_prefix_id']}'
			AND os.`userid`='{$uid}'
			AND p.`productid`='{$productid}'
			AND os.switch = 'Y'
			AND (
			 (os.used = 'N' AND os.serial ='') OR ( os.used = 'N' AND os.serial !='' AND os.verified = 'N') OR (os.used = 'N' AND  os.serial IS NULL)
			)
			and p.offtime > NOW()
		";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record']) ) {
			return (int)$rs['table']['record'][0]['amount'];
		} else {
		    return 0;
		}
	}

	//限定S碼過期總數
	public function get_oscode_expired($uid) {
		global $db, $config;

		$query = "SELECT count(*) amount
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` os
		left join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = os.`prefixid`
			and p.`productid` = os.`productid`
			and p.`offtime` <= NOW()
		WHERE
			os.`prefixid`='{$config['default_prefix_id']}'
			AND os.`userid`='{$uid}'
			AND (
			(os.used = 'N' AND os.serial ='') OR ( os.used = 'N' AND os.serial !='' AND os.verified = 'N')
			)
			AND os.switch = 'Y'
			and p.offtime <= NOW()
		";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record'])) {
			return (int)$rs['table']['record'][0]['amount'];
		}

		return 0;
	}

	/*
	 *	殺價卷(限定S碼)可用項目清單
	 *	$uid				int					會員編號
	 */
	public function get_oscode_list($uid) {
		global $db, $config;

		//SQL指令
		$query_count = " SELECT count(*) as num from ( select count(*) as num ";

		$query_record = " SELECT os.used, p.productid, p.name, p.ontime, p.offtime, p.lb_used, SUM(os.amount) as total_amount ";

		$query = " FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` os
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = os.`prefixid`
			and p.`productid` = os.`productid`
			and p.`offtime` > NOW()
			and p.`switch` = 'Y'
		WHERE
			os.`prefixid`='{$config['default_prefix_id']}'
			AND os.`userid`='{$uid}'
			AND (
			(os.used = 'N' AND os.serial ='') OR ( os.used = 'N' AND os.serial !='' AND os.verified = 'N') OR (os.used = 'N' AND  os.serial IS NULL)
			)
			AND os.switch = 'Y'
			AND p.offtime > NOW()
		GROUP BY os.productid
		";
		$query_count_last = " ) os";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$table['table']['record'][$tk]['ontime'] = Date("Y-m-d", strtotime($tv['ontime']));
				$table['table']['record'][$tk]['offtime'] = Date("Y-m-d", strtotime($tv['offtime']));
				$table['table']['record'][$tk]['total_amount'] = round($table['table']['record'][$tk]['total_amount']);
			}

			return $table;
		}

		return false;
	}

	/*
	 *	殺價卷(限定S碼)過期項目清單
	 *	$uid				int				會員編號
	 */
	public function get_oscode_expired_list($uid) {
		global $db, $config;

		//SQL指令
		$query_count = " SELECT count(*) as num from ( select count(*) as num ";

		$query_record = " SELECT os.used, p.productid, p.name, p.ontime, p.offtime, p.lb_used, count(os.productid) as total_amount ";

		$query = " FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` os
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = os.`prefixid`
			and p.`productid` = os.`productid`
		WHERE
			os.`prefixid`='{$config['default_prefix_id']}'
			AND os.`userid`='{$uid}'
			AND os.switch = 'Y'
			AND (
			(os.used = 'Y') OR ( p.offtime <= NOW())
			)
		GROUP BY os.used, os.productid
		";

		$query_count_last = " ) os";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " ORDER BY p.offtime DESC LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv)
			{
				$table['table']['record'][$tk]['ontime'] = Date("Y-m-d", strtotime($tv['ontime']));
				$table['table']['record'][$tk]['offtime'] = Date("Y-m-d", strtotime($tv['offtime']));
			}

			return $table;
		}

		return false;
	}

	/*
	 *	隨機取得殺價卷(限定S碼) - 廣告檔期用
	 */
	public function get_ad_oscode()	{
		global $db, $config;

		$query = " SELECT sp.*, p.name as pname FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = sp.`prefixid`
			and p.`productid` = sp.`productid`
			and p.`offtime` <= NOW()
			and p.display = 'Y'
			and p.closed = 'N'
		WHERE
			sp.`prefixid`='{$config['default_prefix_id']}'
			AND sp.switch = 'Y'
			AND sp.behav = 'ad'
			AND NOW() between sp.`ontime` and sp.`offtime`
		ORDER BY Rand() Limit 1 ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}

		return false;
	}

	/*
	 *	查詢會員已領取殺價卷(限定S碼)次數 - 廣告檔期用
	 *	$uid							varchar							會員編號
	 */
	public function get_ad_oscode_user($uid) {
		global $db, $config;

		//SQL指令
		$query_count = " SELECT count(*) as num from ( select count(*) as num ";

		$query_record = " SELECT os.used, p.name, p.ontime, p.offtime, count(os.productid) as total_amount ";

		$query = " FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` os
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = os.`prefixid`
			and p.`productid` = os.`productid`
			and p.`offtime` > NOW()
			and p.`switch` = 'Y'
		WHERE
			os.`prefixid`='{$config['default_prefix_id']}'
			AND os.`userid`='{$uid}'
			AND (
			(os.used = 'N' AND os.serial ='') OR ( os.used = 'N' AND os.serial !='' AND os.verified = 'N')
			)
			AND os.switch = 'Y'
			AND p.offtime > NOW()
			AND os.behav = 'ad'
			AND (TO_DAYS(now()) - TO_DAYS(os.insertt)) = 0

		";
		$query_count_last = " ) os";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table']['record'][0];
		}

		return false;
	}


	/*
	 *	確認殺價卷(限定S碼)資料 - 廣告檔期用
	 *	$spid							int							活動編號
	 */
	public function get_ad_scode($spid)	{
		global $db, $config;

		$query = " SELECT sp.*, p.name as pname FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		on
			p.`prefixid` = sp.`prefixid`
			and p.`productid` = sp.`productid`
			and p.`offtime` <= NOW()
			and p.display = 'Y'
			and p.closed = 'N'
		WHERE
			sp.`prefixid`='{$config['default_prefix_id']}'
			AND sp.switch = 'Y'
			AND sp.behav = 'ad'
			AND NOW() between sp.`ontime` and sp.`offtime`
			AND sp.`spid`='{$spid}'
		";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}

		return false;
	}

	/*
	 *	領取殺價卷 - 廣告檔期用
	 *	$data							array							殺價卷資料
	 *	$uid							varchar							會員編號
	 */
	public function send_oscode($data, $uid) {
		global $db, $config;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		SET
           `prefixid`='{$config['default_prefix_id']}',
           `userid`='{$uid}',
           `spid`='{$data['spid']}',
		   `productid`='{$data['productid']}',
           `behav`='ad',
           `amount`='1',
		   `pwd`='',
		   `serial`='',
           `insertt`=NOW()
		";
		$db->query($query);
		$scodeid = $db->_con->insert_id;


		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		SET
           `scode_sum`=`scode_sum` + 1,
		   `amount`=`amount` + '{$data['onum']}'
        WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND spid = '{$data['spid']}'
			AND switch = 'Y'
		";
		$db->query($query);

		return $scodeid;
	}
	
	
	/*
	 *	領取殺價卷 - 大檔汽車用
	 *	$uid							array							會員編號
	 *	$productid						varchar							商品編號
	 *	$num							int								送卷數量
	 */
	public function send_oscode_lsit($uid,$productid,$num) {
		global $db, $config;
		
		for($idx=1;$idx<=$num;++$idx) {
			$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
			SET
			   `prefixid`='{$config['default_prefix_id']}',
			   `userid`='{$uid}',
			   `spid`='0',
			   `productid`='{$productid}',
			   `behav`='d',
			   `amount`='1',
			   `pwd`='',
			   `serial`='',
			   `remark`='spbid20',
			   `insertt`=NOW()
			";
			$db->query($query);
			$scodeid = $db->_con->insert_id;
		}
		return $idx;
	}	
	

	/*
	 *	判斷是否領過活動殺價卷 - 大檔汽車用
	 *	$uid							int								會員編號
	 *	$pid							varchar							商品編號
	 */	
	public function get_oscode_lsit($uid, $pid) {
		global $db, $config;

		$query = "SELECT count(*) amount
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` os
		WHERE
			os.`prefixid`='{$config['default_prefix_id']}'
			AND os.`productid`='{$pid}'
			AND os.`userid`='{$uid}'
			AND os.`switch` = 'Y'
			AND os.`remark`='spbid20'
		";
		$rs = $db->getQueryRecord($query);

		if(!empty($rs['table']['record'])) {
			return (int)$rs['table']['record'][0]['amount'];
		}

		return 0;
	}

	/*
	@productid 殺價券「Ｎ」組的商品編號
	@codepid 殺價券可下標的對象
	@codenum 殺價券張數
	@userid 得標者
	 */
	public function insert_bid_oscode($productid,$codepid,$codenum,$userid){
		global $db, $config;
		try{
			for($idx=0;$idx<$codenum;$idx++) {
				if (($idx%100)==0){
					$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` (`prefixid`,`userid`,`spid`,`productid`,`behav`,`amount`,`insertt`) VALUES ('{$config['default_prefix_id']}','{$userid}','0','{$codepid}','f','1',NOW())";
				}else{
					$query.=",('{$config['default_prefix_id']}','{$userid}','0','{$codepid}','f','1',NOW())";
				}
				if (($idx%100)==99) {
					$db->query($query);
					$query="";
				}
			}
		    if ($query!='') $db->query($query);
		}catch(Exception $e){
			error_log("insert_bid_oscode: $productid fail");
		}
	}
}
?>
