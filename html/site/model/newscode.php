<?php
/**
 * Scode Model 模組
 */

class NewscodeModel {
	public $msg;
	public $sort_column = 'name|seq|modifyt'; 

    public function __construct() {
    }
	
	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;
		
		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = ''; 
		
		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt"; 
		
		if($_GET){
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}
		
		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc') {
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}
		
		return $sub_sort_query;
	}
	
	/**
     * Search Method : set_search
     */
	public function accept_list_set_search() {
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp ON
			s.prefixid = sp.prefixid
			AND s.spid = sp.spid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history` sh ON
			s.prefixid = sh.prefixid
			AND s.scodeid = sh.scodeid
			AND sh.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = "AND sp.spid IS NOT NULL 
		AND sh.scodeid IS NOT NULL ";
		
		return $rs;
	}
	
	public function accept_list($userid) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->accept_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT s.*, sp.name spname, sh.promote_amount, sh.memo ";
		
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` s 
		{$set_search['join_query']}
		WHERE 
			s.prefixid = '{$config['default_prefix_id']}' 
			AND s.switch = 'Y'
			AND s.userid = '{$userid}'
			AND s.behav != 'a'
		"; 
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : "ORDER BY s.insertt DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		
		return false;
    }
	
	/**
     * Search Method : set_search
     */
	public function used_list_set_search() {
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			s.prefixid = p.prefixid
			AND s.productid = p.productid
			AND p.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = "AND p.productid IS NOT NULL ";
		
		return $rs;
	}
	
	public function used_list($userid) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->used_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT s.*, p.name ";
		
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` s 
		{$set_search['join_query']}
		WHERE 
			s.prefixid = '{$config['default_prefix_id']}' 
			AND s.switch = 'Y'
			AND s.userid = '{$userid}'
			AND s.behav = 'a'
		"; 
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : "ORDER BY s.insertt DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		
		return false;
    }
	
	
	//會員分享連結送S碼
	//$user_src => 推薦人userid (saja_user.userid && saja_scode_promote.spid)
	//$userid => 被招募者userid
	public function register_scode($userid, $src) {
		global $db, $config;
		
		$this->msg = '';
		$this->str = new convertString();
		
		$user_src = $this->str->decryptAES128($config['encode_key'], base64_decode($src) );
		$user_src = explode("&&", $user_src);
		
		error_log("[model/scode]register_scode : ".$user_src[0]."|".$user_src[1]);
		
		//取得 S碼活動
		$scode_promote = $this->get_scode_promote($user_src[1]);
		
		if(empty($scode_promote)) {
			$this->msg = 'no_scode_promote';
		} elseif(time() > strtotime($scode_promote[0]["offtime"])) {
			$this->msg = 'scode_promote_off';
		} else {
			$scode_promote[0]['promote_amount'] = 1;
			
			//插入S碼收取記錄
			$this->insert_scode($scode_promote[0], $user_src[0], $userid);
			
			$query = "SELECT sum(promote_amount) as amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history`
			WHERE 
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$user_src[0]}'
				AND spid='{$user_src[1]}'
				AND switch = 'Y'
			";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])) {
				$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				SET
				   `sp_count`='{$table['table']['record'][0]['amount']}'
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `userid`='{$user_src[0]}'
					AND switch = 'Y'
				";
				$db->query($query);
			}
		}
		
		return $this->msg;
	}
	
	//插入S碼收取記錄
	public function insert_scode($info, $uid, $memo) {	
		global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
		SET
           `prefixid`='{$config['default_prefix_id']}',
           `userid`='{$uid}',
           `spid`='{$info['spid']}',
           `behav`='{$info['behav']}',
           `amount`='{$info['num']}',
           `remainder`='{$info['num']}',
           `switch` = 'N', 
           `insertt`=NOW()
		";
		$db->query($query);
		$scodeid = $db->_con->insert_id;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history`
		SET
           `prefixid`='{$config['default_prefix_id']}',
           `userid`='{$uid}',
		   `scodeid`='{$scodeid}',
           `spid`='{$info['spid']}',
		   `promote_amount`='{$info['promote_amount']}',
           `num`='{$info['num']}',
		   `memo`='{$memo}',
		   `switch` = 'N', 
           `insertt`=NOW()
		";
		$db->query($query);
		
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		SET
           `scode_sum`=`scode_sum` + '{$info['promote_amount']}',
		   `amount`=`amount` + '{$info['num']}'
        WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND spid = '{$info['spid']}' 
			AND switch = 'Y'
		";
		$db->query($query);
	}
	
	/*
	* 取得 S碼活動
		array(15) {
			["spid"]=>  string(1) "1"
			["behav"]=>  string(1) "b" 活動型態 b分享,c充值,d系統,e其他 
			["ontime"]=>  string(19) "2014-01-01 00:00:00"
			["offtime"]=>  string(19) "2015-01-01 00:00:00"
			["name"]=>  string(18) "個人分享連結"
			["description"]=>  string(41) "S碼的常態性活動,個人分享連結"
			["scode_sum"]=>  string(1) "0"
			["amount"]=>  string(1) "0"
			["spname"]=>  string(10) "S碼獎勵"
			["num"]=>  string(1) "1"
		}
	*/
	public function get_scode_promote($spid) {	
		global $db, $config;
		
		$query = "SELECT s.*, sp.name spname, sp.num num
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` s
			LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` sp ON 
				sp.prefixid = s.prefixid
				AND sp.spid = s.spid
				AND sp.switch = 'Y'
			WHERE 
				s.prefixid = '{$config['default_prefix_id']}' 
				AND s.spid = '{$spid}'
				AND unix_timestamp( s.offtime ) >0 
				AND unix_timestamp() >= unix_timestamp( s.ontime ) 
				AND unix_timestamp() <= unix_timestamp( s.offtime ) 
				AND s.switch = 'Y'
				AND sp.spid IS NOT NULL
			";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
	}
	
	//S碼 已用總數
	public function used_sum($uid) {	
		global $db, $config;
		
		$this->set_scode_closed($uid);
		
		$query = "SELECT userid, sum(amount) as amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			WHERE 
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND behav = 'a'
				AND switch = 'Y'
			";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return (int)$table['table']['record'][0]['amount'];
		}
		
		return false;
	}
	
	//S碼 收取且過期總數
	public function expired_sum($uid) {	
		global $db, $config;
		
		$this->set_scode_closed($uid);
		
		$query = "SELECT userid, sum(remainder) as amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			WHERE 
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND behav != 'a'
				AND closed = 'Y'
				AND switch = 'Y'
			";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return (int)$table['table']['record'][0]['amount'];
		}
		
		return false;
	}
	
	//S碼 收取總數
	public function accept_sum($uid) {	
		global $db, $config;
		
		$this->set_scode_closed($uid);
		
		$query = "SELECT userid, sum(amount) as amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			WHERE 
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND behav != 'a'
				AND switch = 'Y'
			";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return (int)$table['table']['record'][0]['amount'];
		}
		
		return false;
	}
	
	//取得未過期 S碼
	public function get_scode($uid)	{	
		global $db, $config;
		
		$this->set_scode_closed($uid);
		
		$query = "SELECT userid, sum(remainder) as amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode`
			WHERE 
				`prefixid`='{$config['default_prefix_id']}'
				AND `userid`='{$uid}'
				AND behav != 'a'
				AND closed = 'N'
				AND switch = 'Y'
			";
		$table1 = $db->getQueryRecord($query);
	
		if(!empty($table1['table']['record'])) {
			$balance = (int)$table1['table']['record'][0]['amount'];
			return $balance;
		}
		
		return 0;
	}
	
	//將 scode 更改為已過期
	private function set_scode_closed($uid) {
		global $db, $config;
		
		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` 
		set closed = 'Y'
		WHERE 
			`prefixid`='{$config['default_prefix_id']}'
			AND `userid`='{$uid}'
			AND (TO_DAYS(now()) - TO_DAYS(insertt)) > 30
			AND behav !='a'
			AND switch = 'Y'
		";
		$db->query($query);
	}
	
	//查詢scode_promote
	public function scode_promote($pid, $behav='b') {
		global $db, $config;
		
		$query = "SELECT * FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `switch` = 'Y'
			AND `behav` = '{$behav}'
			AND NOW() between `ontime` and `offtime` 
			AND `productid` = '{$pid}'
		    LIMIT 0, 1
		";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0]) ) {
			return $table['table']['record'][0];
		}
		
		return false;
	}
	
	//限定S碼收取記錄
	public function add_oscode($info) {	
		global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		SET
           `prefixid` = '{$config['default_prefix_id']}',
           `userid` = '{$info['userid']}',
           `productid` = '{$info['productid']}', 
           `spid` = '{$info['spid']}',
           `behav` = '{$info['behav']}',
           `amount` = '1',
           `insertt` = NOW()
		";
		$db->query($query);
	}
	
	//設定scode_promote
	public function set_scode_promote($info) {	
		global $db, $config;
		
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		SET
           `scode_sum` = `scode_sum` + 1,
		   `amount` = `amount` + '{$info['onum']}'
        WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND spid = '{$info['spid']}' 
			AND switch = 'Y'
		";
		$db->query($query);
	}
	
	//殺價券查詢記錄
	public function get_oscode($pid, $uid) {	
		global $db, $config;
		
		$query = "select count(*) num from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` 
		where
           `prefixid` = '{$config['default_prefix_id']}' 
           and `userid` = '{$uid}' 
           and `productid` = '{$pid}' 
           and `switch` = 'Y'
		";
		
		$table = $db->getQueryRecord($query);
		error_log("[m/scodeModel/get_oscode]:".$query);
		if(!empty($table['table']['record'][0]['num'])) {
			return $table['table']['record'][0]['num'];
		} else {
			return false;
		}
	}
}
?>