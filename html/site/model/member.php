<?php
/**
 * Member Model 模組
 */
class MemberModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/*
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET) {
			foreach($_GET as $gk => $gv) {
				if(preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

    /**
     * 站内公告
     */
	public function news_list_set_search() {
		global $status, $config;

		$rs = array();
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_news_rt` cn ON
			n.prefixid = cn.prefixid
			AND n.newsid = cn.newsid
			AND cn.channelid = '{$_GET['channelid']}'
			AND cn.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = "AND cn.channelid IS NOT NULL ";

		return $rs;
	}

	public function news_list()	{
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->news_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT * ";

		$query = "FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}news` n
		{$set_search['join_query']}
		WHERE
			n.prefixid = '{$config['default_prefix_id']}'
			AND unix_timestamp() >= unix_timestamp(n.ontime)
			AND n.switch = 'Y'
		";

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : ' ORDER BY n.seq ';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}

		return false;
    }

	/*
     * 站内公告
     */
	public function news_detail() {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		//SQL指令
		$query = "SELECT *
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}news` n
		WHERE
			n.prefixid = '{$config['default_prefix_id']}'
			AND n.newsid = '{$_GET["newsid"]}'
			AND n.switch = 'Y'
		";

		//取得資料
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}

		return false;
    }

	/*
	 * 鯊魚點餘額
	 */
	public function get_bonus($uid)	{
        global $db, $config;

		$query = "SELECT userid, IFNULL(sum(amount),0) as amount
		FROM `{$config["db"][1]["dbname"]}`.`{$config['default_prefix']}bonus`
		WHERE
			prefixid = '{$config["default_prefix_id"]}'
			AND userid = '{$uid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false;
    }

	/*
	 * 店點餘額
	 */
	public function get_gift($uid) {
        global $db, $config;

		$query = "SELECT userid, sum(amount) as amount
		FROM `{$config["db"][1]["dbname"]}`.`{$config['default_prefix']}gift`
		WHERE
			prefixid = '{$config["default_prefix_id"]}'
			AND userid = '{$uid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}

		return false;
    }

	/*
	 * 殺價幣餘額
	 */
	public function get_spoint($uid) {
        global $db, $config;

		$query = "SELECT userid, sum(amount) as amount
		FROM `{$config["db"][1]["dbname"]}`.`{$config['default_prefix']}spoint`
		WHERE
			prefixid = '{$config["default_prefix_id"]}'
			AND userid = '{$uid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}

		return false;
    }

	/*
	 *	取得會員相關資料
	 *	$id				int				會員編號
	 */
	public function get_info($id, $email = null) {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT * , up.bonus_noexpw, if (up.idnum!='','Y','N') as id_varifyied, name as phone,
    	  (SELECT IFNULL(name,'')
		     FROM `{$config["db"][2]["dbname"]}`.`{$config['default_prefix']}province`
		    WHERE `switch`='Y' AND provinceid=up.provinceid) as come_from,
		  (SELECT IFNULL(verified,'')
		     FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}user_sms_auth`
            WHERE userid='{$id}' ) as verified
		FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}user` u
		LEFT OUTER JOIN `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}user_profile` up ON
			u.prefixid = up.prefixid
			AND u.userid = up.userid
			AND up.`switch`='Y'
		WHERE
			u.prefixid = '{$config["default_prefix_id"]}'
			AND u.userid = '{$id}'
			AND u.switch = 'Y'
			AND up.userid IS NOT NULL
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'][0];
            $results['nickname']=urldecode($results['nickname']);
			unset($results['passwd']);

			return $results;
		}
		return false;
    }

	public function getMedalsList($arrCond) {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();
		$query = "SELECT * FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}medals_def`
				  WHERE switch='Y' ";
		if(!empty($arrCond['mdlid'])) {
		   $query.=" AND mdlid='{$arrCond["mdlid"]}' ";
		}
		if(!empty($arrCond['type'])) {
		   $query.=" AND type='{$arrCond["type"]}' ";
		}
		if(!empty($arrCond['expts'])) {
		   $query.=" AND {$arrCond['expts']} between min_expts AND max_expts' ";
		}

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

	public function getUserMedalsList($arrCond) {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		$query = "SELECT rt.umrid, rt.mdlid, rt.userid, md.type, md.title, md.subtitle FROM
				 `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}medals_def` md JOIN
				 `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}user_medals_rt` rt
				 ON rt.mdlid=md.mdlid
				AND md.switch='Y'
				AND rt.switch='Y' ";

		if(!empty($arrCond['mdlid'])) {
		   $query.=" AND rt.mdlid='{$arrCond["mdlid"]}' ";
		}
		if(!empty($arrCond['userid'])) {
			$query.=" AND rt.userid='{$arrCond["userid"]}' ";
		}
		if(!empty($arrCond['type'])) {
			$query.=" AND md.type='{$arrCond["type"]}' ";
		}

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 * 取得店點明細清單
	 * $userid		int 	使用者編號
	 * $page		int 	頁碼
	 * $perpage		int 	筆數
	 */
	public function getStoreGiftList($userid) {
        global $db, $config;

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT g.giftid, g.userid, g.behav, g.amount, g.expired_time, g.esid, es.name as es_name";
		$query = " 	FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}gift` g
					LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es ON g.esid = es.esid
					WHERE g.switch = 'Y' AND now() < g.expired_time ";

		if (!empty($userid)){
			$query .= " AND g.userid = {$userid}";
		}

		$query .= " ORDER BY g.seq ASC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$getnum = $getnum['table']['record'][0]['num'];
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = 10;
			$_POST['perpage'] = 10;
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		}
		return false;

    }

	/*
	 * 取得會員店點點數
	 * $userid		int 	使用者編號
	 * $esid		int 	兌換中心分店編號
	 * $page		int 	頁碼
	 * $perpage		int 	筆數
	 */
	public function getStoreGiftSum($userid, $esid='') {
        global $db, $config;

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT es.esid, es.name as es_name, SUM(g.amount) as amount";
		$query = " 	FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es
					LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}gift` g ON es.esid = g.esid
					WHERE g.switch = 'Y' AND now() < g.expired_time  ";

		if(!empty($userid)) {
			$query .= " AND g.userid = {$userid}";
		}
		if(!empty($esid)){
			$query .= " AND g.esid = {$esid}";
		} else {
			$query .= " GROUP BY g.esid";
		}

		if(!empty($esid)) {
			//取得資料
			$table = $db->getQueryRecord($query_record . $query);
		} else {

			//總筆數
			$getnum = $db->getQueryRecord($query_count . $query);
			$getnum	= $getnum['table']['record'][0]['num'];
			$num = (!empty($getnum)) ? $getnum : 0;
			$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

			if(empty($perpage)) {
				$perpage = 10;
				$_POST['perpage'] = 10;
			}

			if($num) {
				//分頁資料
				$page = $db->recordPage($num, $this);
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
				//取得資料
				$table = $db->getQueryRecord($query_record . $query . $query_limit);
			} else {
				$table['table']['record'] = '';
			}
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		}
		return false;
    }
	
	
	/*
	 * 購物金餘額
	 * $uid		int 	使用者編號
	 */
	public function get_rebate($uid)	{
        global $db, $config;

		$query = "SELECT userid, IFNULL(sum(amount),0) as amount
		FROM `{$config["db"][1]["dbname"]}`.`{$config['default_prefix']}rebate`
		WHERE
			prefixid = '{$config["default_prefix_id"]}'
			AND userid = '{$uid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false;
    }
	
	
	/*
	 * 圓夢卷餘額
	 * $uid				int 		使用者編號
	 * $feedback		varchar 	是否返饋 ( Y:返饋, N:不返饋)
	 */
	public function get_dscode($uid,$feedback='')	{
        global $db, $config;

		$query = "SELECT userid, IFNULL(sum(amount),0) as amount
		FROM `{$config["db"][1]["dbname"]}`.`{$config['default_prefix']}dscode`
		WHERE
			prefixid = '{$config["default_prefix_id"]}'
			AND userid = '{$uid}'
			AND switch = 'Y'
			AND used ='N'
		";
		if(!empty($feedback)) {
			$query .= " AND feedback = '{$feedback}'";
		}
		
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false;
	}

    /* 20201202 leo
	 * 購物金餘額
	 * $uid				int 		使用者編號
	 */
	public function getRebate($uid)	{
        global $db, $config;

		$query = "SELECT userid, IFNULL(sum(amount),0) as amount
		FROM `{$config["db"][1]["dbname"]}`.`{$config['default_prefix']}rebate`
		WHERE
			prefixid = '{$config["default_prefix_id"]}'
			AND userid = '{$uid}'
			AND switch = 'Y'
		";
		$table = $db->getRecord($query);
		if (sizeof($table)>0){
			return $table[0];
		}else{
			return array('userid'=>$uid,'amount'=>0);
		}
	}

	//20201202 leo 
	//trans rebate from a to b(s)
	/*
		$uid				int 		使用者編號
		$receiver
	*/
	public function transRebate($uid,$receiver,$coin){
		global $db, $config;
		$tx_time=date("Y-m-d H:i:s");
		$owner=$this->getRebate($uid);
		$amount=sizeof($receiver)*$coin;
		if ($owner['amount']>$amount){
			$sql="INSERT INTO `saja_cash_flow`.`saja_rebate` (prefixid, userid,amount, behav,insertt,touserid,fromuserid) VALUES ";
			for ($i=0;$i<sizeof($receiver);$i++){
				$sql.="('saja','{$uid}','-{$coin}','user_gift','{$tx_time}','{$receiver[$i]}','{$uid}'),";
				$sql.="('saja','{$receiver[$i]}','{$coin}','user_gift','{$tx_time}','{$receiver[$i]}','{$uid}')";
				if ($i<(sizeof($receiver)-1)) $sql.=",";
			}
			$db->query($sql);
			return 1;
		}else{
			return -1;
		}
	}

	//20201202 leo[棄用]
	//trans rebate from a to b
	/*
		$uid				int 		使用者編號
		$receiver
	*/
	public function transCoupon($uid,$receiver,$coin,$coupon_type){
		global $db, $config;
		$tx_time=date("Y-m-d H:i:s");
		$owner=$this->get_dscode($uid,$coupon_type);
		$amount=sizeof($receiver)*$coin;
		if ($owner['amount']>$amount){
			for ($i=0;$i<sizeof($receiver);$i++){
				$sql="INSERT INTO `saja_cash_flow`.`saja_dscode` (prefixid, userid,amount, behav,insertt,fromuserid,feedback) VALUES ";
				for ($j=0;$j<$coin;$j++){
					$sql.="('saja','{$receiver[$i]}','1','user_gift','{$tx_time}','{$uid}','{$coupon_type}')";
					if ($j<($coin-1)) $sql.=",";
				}
				$db->query($sql);
				//get non usedid
				$subsql="select dscodeid from `saja_cash_flow`.`saja_dscode` where used='N' and feedback='{$coupon_type}' limit 0, $coin";
				$rs=$db->getRecord($subsql);
				$inidx=array();
				for ($k=0;$k<sizeof($rs);$k++){
					$inidx[$k]=$rs[$k]['dscodeid'];
				}
				$instr=implode(",",$inidx);
				//mark it with used
				$str="UPDATE `saja_cash_flow`.`saja_dscode` SET used =  'Y', touserid='{$receiver[$i]}' WHERE dscodeid IN ({$instr})";
				$db->query($str);
			}
			return 1;
		}else{
			return -1;
		}
	}

	//20210121 leo
	//trans spoint from a to b(s)
	/*
		$uid				int 		使用者編號
		$receiver
	*/
	public function transSpoint($uid,$receiver,$coin){
		global $db, $config;
		$tx_time=date("Y-m-d H:i:s");
		$owner=$this->get_spoint($uid);
		$amount=sizeof($receiver)*$coin;
		if ($owner['amount']>$amount){
			$sql="INSERT INTO `saja_cash_flow`.`saja_spoint` (prefixid, userid,amount, behav,insertt,touserid,fromuserid) VALUES ";
			for ($i=0;$i<sizeof($receiver);$i++){
				$sql.="('saja','{$uid}','-{$coin}','user_gift','{$tx_time}','{$receiver[$i]}','{$uid}'),";
				$sql.="('saja','{$receiver[$i]}','{$coin}','user_gift','{$tx_time}','{$receiver[$i]}','{$uid}')";
				if ($i<(sizeof($receiver)-1)) $sql.=",";
			}
			$db->query($sql);
			return 1;
		}else{
			return -1;
		}
	}	

    //20210121 leo
	//trans scode from a to b(s)
	/*
		$uid				int 		使用者編號
		$receiver
	*/
	public function transScode($uid,$receiver,$coin){
		global $db, $config, $scodeModel;
		$tx_time=date("Y-m-d H:i:s");
		$owner=$scodeModel->get_scode($uid);
		$amount=sizeof($receiver)*$coin;
		if ($owner>$amount){
			for ($i=0;$i<sizeof($receiver);$i++){
				$qty=$coin;				
				//get non usedid
				$subsql="select scodeid,remainder,offtime from `saja_cash_flow`.`saja_scode` where  `userid`='{$uid}' AND behav not in  ('a','gift_out') AND offtime > NOW() AND closed = 'N' AND switch = 'Y' order by offtime";
				$rs=$db->getRecord($subsql);
				for ($k=0;$k<sizeof($rs);$k++){
					if ($qty>0){
						$scodeid=$rs[$k]['scodeid'];
						$offtime=$rs[$k]['offtime'];
						if ($qty>=$rs[$k]['remainder']){
							$remainder=$rs[$k]['remainder'];
							//加
							$sql="INSERT INTO `saja_cash_flow`.`saja_scode` (prefixid, userid,amount,remainder, behav,insertt,fromuserid,touserid,offtime) VALUES ";
							$sql.="('saja','{$receiver[$i]}','{$remainder}','{$remainder}','gift_in','{$tx_time}','{$uid}','{$receiver[$i]}','{$offtime}')";
							$db->query($sql);
							//減 
							$sql="INSERT INTO `saja_cash_flow`.`saja_scode` (prefixid, userid,amount,remainder, behav,insertt,fromuserid,touserid) VALUES ";
							$sql.="('saja','{$uid}','-{$remainder}','0','gift_out','{$tx_time}','{$uid}','{$receiver[$i]}')";
							$db->query($sql);		

							$db->query("UPDATE `saja_cash_flow`.`saja_scode` SET remainder='0' where userid='{$uid}' and scodeid='{$scodeid}'");
							$qty=$qty-$rs[$k]['remainder'];
						}else{
							//加
							$sql="INSERT INTO `saja_cash_flow`.`saja_scode` (prefixid, userid,amount,remainder, behav,insertt,fromuserid,touserid,offtime) VALUES ";
							$sql.="('saja','{$receiver[$i]}','{$qty}','{$qty}','gift_in','{$tx_time}','{$uid}','{$receiver[$i]}','{$offtime}')";
							$db->query($sql);
							//減 
							$sql="INSERT INTO `saja_cash_flow`.`saja_scode` (prefixid, userid,amount,remainder, behav,insertt,fromuserid,touserid) VALUES ";
							$sql.="('saja','{$uid}','-{$qty}','0','gift_out','{$tx_time}','{$uid}','{$receiver[$i]}')";
							$db->query($sql);

							$remainder=$rs[$k]['remainder']-$qty;
							$db->query("UPDATE `saja_cash_flow`.`saja_scode` SET remainder='{$remainder}' where userid='{$uid}' and scodeid='{$scodeid}'");
							$qty=0;
						}	
					}
				}
			}
			return 1;
		}else{
			return -1;
		}
	}	

    //20210122 leo
	//trans oscode from a to b(s)
	/* 將原先可用的設為 used='Y' fromuserid touserid  再新增一筆轉入的
		$uid				int 		使用者編號
		$receiver
		$productid 			int			限定商品ID
	*/
	public function transOscode($uid,$receiver,$coin,$productid){
		global $db, $config, $oscModel;
		$tx_time=date("Y-m-d H:i:s");
		$oscModel = new OscodeModel;
		$owner=$oscModel->get_oscode_list($uid);
		foreach($owner['table']['record'] as $tk =>$tv){
			if ($owner['table']['record'][$tk]['productid'] == $productid) $owner=$owner['table']['record'][$tk]['total_amount'];
		}
		$amount=sizeof($receiver)*$coin;
		if ($owner>$amount){
			for ($i=0;$i<sizeof($receiver);$i++){			
				//get non usedid
				$subsql="SELECT os.oscodeid,os.used, p.productid, p.name, p.ontime, p.offtime, p.lb_used FROM `saja_cash_flow`.`saja_oscode` os LEFT OUTER JOIN `saja_shop`.`saja_product` p on p.`prefixid` = os.`prefixid` and p.`productid` = os.`productid` and p.`offtime` > NOW() and p.`switch` = 'Y' WHERE os.`prefixid`='saja' AND os.`userid`='{$uid}' and p.`productid`='{$productid}' AND ( (os.used = 'N' AND os.serial ='') OR ( os.used = 'N' AND os.serial !='' AND os.verified = 'N') OR (os.used = 'N' AND os.serial IS NULL) ) AND os.switch = 'Y' AND p.offtime > NOW() order by p.offtime limit 0,{$coin}";
				$rs=$db->getRecord($subsql);
				for ($k=0;$k<sizeof($rs);$k++){
					$oscodeid=$rs[$k]['oscodeid'];
					$offtime=$rs[$k]['offtime'];
					//加
					$sql = "INSERT INTO `saja_cash_flow`.`saja_oscode` (`prefixid`,`userid`,`spid`,`productid`,`behav`,`amount`,`insertt`,fromuserid,touserid,offtime,from_oscodeid) VALUES ('{$config['default_prefix_id']}','{$uid}','0','{$productid}','gift_in','1',NOW(),'{$uid}','{$receiver[$i]}','{$offtime}','{$oscodeid}')";
					$db->query($sql);
					$db->query("UPDATE `saja_cash_flow`.`saja_oscode` SET used='Y',fromuserid='{$uid}',touserid='{$receiver[$i]}' where userid='{$uid}' and oscodeid='{$oscodeid}'");
				}
			}
			return 1;
		}else{
			return -1;
		}
	}		
}
?>