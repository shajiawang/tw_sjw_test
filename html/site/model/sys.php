<?php
/**
 * Sys Model 模組
 */

class SysModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt'; 

    public function __construct() {
		
        global $db;
        $this->ok = false;
        return $this->ok;		
    }

 	/*
	 * 取得log清單
	 */	   
    public function log_list() {
		global $db, $config;
	
		$query = "SELECT logid, insertt, msg, client
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}applog` 
					ORDER BY  `logid` DESC 
					LIMIT 50";

		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}

		return false;
    }
	
	/*
	 * 寫入log
	 * $data			array			新增資料項目 
	 */		
	public function add_Log($msg, $client) {
		
        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}applog` 
					SET client = '{$client}',
						msg = '{$msg}'";		
		$db->query($query);

		return $logid = $db->_con->insert_id;
	}

	public function app_os_list($type) {
		
        global $db, $config;
		
		$query = "SELECT `osversion`, `type`, `appversion`
					FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}app_os` a
					WHERE 
					`type` = '{$type}'
					LIMIT 1";
					
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}

		return false;
	}
	
	public function addVisitLog($arrLog) {
		
	        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}visit_log` 
					SET insertt = NOW() ";
        foreach($arrLog as $k=>$v) {
              $query.=", `${k}`='${v}' ";
        }
        error_log("[sys/addVisitLog] : ".$query);		
		$db->query($query);
		return $db->_con->insert_id;	
	}
}
?>