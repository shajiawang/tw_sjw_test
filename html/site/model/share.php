<?php
/**
 * Share Model 模組
 */

class ShareModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET)
		foreach($_GET as $gk => $gv) {
			if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
				$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
				$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc') {
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

  /*
	 *	取得分享送殺價卷活動檔次商品目錄
   *	$s_date		  date			 開始日期(2018-12-11)
   *	$e_date			date			 結束日期(2018-12-11)
   *	$lbuserid			int			 直播主編號(behav = lb專用)
	 */
	public function getLbScodePromoteMenu($sdate ="", $edate ="", $lbuserid ="") {
		global $db, $config;

		$query = "SELECT sp.`spid`, p.`name`, sp.`ontime`, sp.`offtime` , sp.`productid`,
						 IFNULL(pt.`filename`,'') as thumbnail,pt2.`filename` as thumbnail2,
						 p.`retail_price`,p.`rule`,p.`description`,p.`display`,p.`closed`,p.`offtime` as product_offtime
				  FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp
          LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_lb_rt` splr
            on sp.`spid` = splr.`spid`
				  LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
					  on sp.`productid` = p.`productid`
				  LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
						 ON p.`prefixid` = pt.`prefixid`
						AND p.`ptid` = pt.`ptid`
						AND pt.`switch` = 'Y'
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt2
					   ON p.`prefixid` = pt2.`prefixid`
						AND p.`ptid2` = pt2.`ptid`
						AND pt2.`switch` = 'Y'
				  WHERE sp.`prefixid` = '{$config['default_prefix_id']}'
					AND sp.`switch`='Y'
					AND p.`switch` = 'Y'
					AND sp.`productid` > 0
					AND sp.`broadcast_use` = 'Y'
					AND sp.`behav`= 'lb'
				 ";

		//取得現在時間
		$nowtime = date('Y-m-d H:i:s');

		//設定結標開始與結束日期
		if($sdate != "" && $edate !="") {
			$query.=" AND sp.`ontime` < '{$nowtime}' AND sp.`offtime` >= '{$sdate} 00:00:00' AND sp.`offtime` <= '{$edate} 23:59:59'";
		} elseif($sdate != "" && $edate ="") {
			$query.=" AND sp.`ontime` < '{$nowtime}' AND sp.`offtime` >= '{$sdate} 00:00:00'";
		} elseif($sdate = "" && $edate !="") {
			$query.=" AND sp.`ontime` < '{$nowtime}' AND sp.`offtime` <= '{$edate} 23:59:59'";
		} else {
		   $query.=" AND sp.`ontime` < '{$nowtime}' AND sp.`offtime` >= '{$nowtime}'";
		}
		//直播主編號
		if($lbuserid != "") {
		   $query.=" AND splr.lbuserid = '{$lbuserid}'";
		}
			$query.=" ORDER BY sp.offtime ASC";

		error_log("[share/getLbScodePromoteMenu] : ".$query);
		$table = $db->getQueryRecord($query);

		foreach($table['table']['record'] as $tk => $tv)
		{
			$table['table']['record'][$tk]['rule'] = base64_encode($tv['rule']);
			$table['table']['record'][$tk]['description'] = base64_encode(htmlspecialchars_decode($tv['description']));
		}

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}

		return false;
	}

	/*
	 *	分享送殺價卷活動資料取得
	 *	$productid		int			 		商品編號
	 *	$big			varchar		 		大檔商品
	 *  $behav      	string     			使用行為(user_regster:新戶註冊, user_deposit充值, b分享, d系統, e序號, f得標, lb直播, ad廣告送殺價卷)
	 *	$s_date		  	date			 	開始日期(2018-12-11)
	 *	$e_date			date			 	結束日期(2018-12-11)
	 *	$lbuserid		int			 		直播主編號(behav = lb專用)
	*/
	public function getScodePromoteList($productid="", $big ="", $behav="", $sdate ="", $edate ="", $lbuserid ="") {
		global $db, $config;

		if($behav == "b") {//一般商品分享
			$query = "SELECT spr.num, spr.sprid, sp.*, p.name as product_name
					  FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` spr
					  LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp
							  on spr.prefixid = sp.prefixid AND spr.spid = sp.spid
					LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						on sp.productid = p.productid
					  WHERE spr.`prefixid` = '{$config['default_prefix_id']}'
					  AND spr.`switch`='Y'
					  AND sp.`switch`='Y'
					  AND p.`switch` = 'Y'
					  AND sp.`productid` > 0
					 ";
		} else if($behav == "lb") {//直播商品分享
			$query = "SELECT splr.num ,splr.lbuserid, splr.splrid, sp.*, p.name as product_name
					FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_lb_rt` splr
					LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp
						on splr.prefixid = sp.prefixid AND splr.spid = sp.spid
					LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						on sp.productid = p.productid
					WHERE splr.`prefixid` = '{$config['default_prefix_id']}'
					  AND splr.`switch`='Y'
					  AND sp.`switch`='Y'
					  AND p.`switch` = 'Y'
					  AND sp.`productid` > 0
					  AND sp.`broadcast_use` = 'Y'
				   ";
		} else {//使用行為未送參直接返回錯誤
			return false;
		}

		//商品編號
		if($productid != "") {
		   $query.=" AND sp.`productid` = '{$productid}'";
		}
		//大檔商品
		if($big != "") {
		   $query.=" AND p.`is_big` = '{$big}'";
		}
		//使用行為
		if($behav != "") {
		   $query.=" AND sp.`behav`= '{$behav}'";
		}
		//開始與結束日期
		if($sdate != "" && $edate !="") {
		   $query.=" AND sp.`ontime` >= '{$sdate} 00:00:00' AND sp.`offtime` <= '{$edate} 23:59:59'";
		} else {
		   $query.=" AND NOW() between sp.`ontime` and sp.`offtime` ";
		}
		//直播主編號
		if($lbuserid != "") {
			$query.=" AND splr.lbuserid = '{$lbuserid}' ";
		}
		$query.=" ORDER BY sp.spid ASC ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record']) ) {
			return $table['table']['record'];
		}

		return false;
	}

	/*
	 *	分享殺價卷取得
 	 *	$uid			int			會員編號
 	 *	$spid			int			活動編號
 	 *	$productid		int			商品編號
	 *	$lbuserid		int			分享主編號
	 */
	public function insertUserOscode($uid, $spid, $productid, $user_src) {
		global $db, $config;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		SET
			prefixid = '{$config['default_prefix_id']}',
			userid = '{$uid}',
			spid = '{$spid}',
			productid = '{$productid}',
			behav = 'b',
			amount = 1,
            serial='',
			lbuserid = 0,
			srcid = '{$user_src}',
			insertt = NOW()
		";
		$db->query($query);
		$ebhid=$db->_con->insert_id;
		return $ebhid;
	}

	/*
	 *	查詢是否送殺價卷
 	 *	$uid			int			會員編號
 	 *	$spid			int			活動編號
	 */
	public function getUserOscode($uid, $spid) {
		global $db, $config;
		$query = "SELECT count(*) count
		FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		WHERE
			`prefixid` = '{$config['default_prefix_id']}'
			AND `userid` = '{$uid}'
			AND `spid` = '{$spid}'
			AND `switch` = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 *	查詢單位時間內使用者已領過殺價卷商品編號與張數
 	 *	$uid			int			 	會員編號
 	 *	$behav			string	 		推薦來源
	 *	$sdate			date		 	開始日期
	 *	$edate			date		 	結束日期
	 */
	public function getUserAlreadyTakenOscode($uid,$behav,$sdate,$edate) {
		global $db, $config;

		$sub_query = " SELECT `spid`
					   FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
						 WHERE behav = '{$behav}'";

		//取得現在時間
		$nowtime = date('Y-m-d H:i:s');

		//設定結標開始與結束日期
		if($sdate != "" && $edate !="") {
			$sub_query.=" AND `ontime` < '{$nowtime}' AND `offtime` >= '{$sdate} 00:00:00' AND `offtime` <= '{$edate} 23:59:59'";
		}elseif($sdate != "" && $edate ="") {
			$sub_query.=" AND `ontime` < '{$nowtime}' AND `offtime` >= '{$sdate} 00:00:00'";
		}elseif($sdate = "" && $edate !="") {
			$sub_query.=" AND `ontime` < '{$nowtime}' AND `offtime` <= '{$edate} 23:59:59'";
		} else {
			$sub_query.=" AND `ontime` < '{$nowtime}' AND `offtime` >= '{$nowtime}'";
		}

		$sub_query.=" AND `switch` = 'Y'";

		$query = "SELECT spid,productid, COUNT(productid) AS total
              FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
              WHERE spid in ({$sub_query})
                AND `userid` = '{$uid}'
                AND `switch` = 'Y'
                GROUP BY spid
             ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record']) ) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 *	更新送卷統計資料
	 *	$onum						int						活動組數
	 *	$spid						int						活動編號
	 */
    public function updateScodePromote($onum, $spid) {
		global $db, $config;
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		SET
			amount=amount+1,
			scode_sum=scode_sum+".$onum."
		WHERE spid='".$spid."'";

		$ret = $db->query($query);
		if($ret>0) {
		   return $db->_con->insert_id;
		} else {
		   return false;
		}
    }

}
?>
