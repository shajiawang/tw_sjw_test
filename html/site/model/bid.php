<?php
/**
 * Bid Model 模組
 */

class BidModel {
	public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET) {
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc') {
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

	/**
     * Search Method : set_search
     */
	public function product_list_set_search() {
		global $status, $config;

		$rs = array();
        // 最新得標 官方價大於98元的會出現
		// No channelid
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
			pgp.prefixid = p.prefixid
			AND pgp.productid = p.productid
			AND p.switch = 'Y'
            AND p.retail_price>98
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			pgp.prefixid = sp.prefixid
			AND pgp.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid
			AND cs.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
			pgp.prefixid = up.prefixid
			AND pgp.userid = up.userid
			AND up.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h ON
			pgp.prefixid = h.prefixid
			AND pgp.productid = h.productid
			AND pgp.userid = h.userid
			AND pgp.price = h.price
			AND h.switch = 'Y'
		";

		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';

		$rs['sub_search_query'] = "AND p.productid IS NOT NULL
			AND sp.productid IS NOT NULL
			AND cs.channelid IS NOT NULL
			AND up.userid IS NOT NULL
		";

		return $rs;
	}

	public function product_list() {

        // global $db, $config;
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();

        //排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令

		$query_count = " SELECT count(*) as num from ( select count(*) as num ";
		$query_record = "SELECT p.productid
		, CONCAT('".BASE_URL.APP_DIR."/bid/detail/?productid=',p.productid) as link_url
		, p.name, p.retail_price, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
		, pgp.price, pgp.insertt, h.nickname, IFNULL(pt.filename,'') as thumbnail ";

		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		{$set_search['join_query']}
		WHERE
			pgp.prefixid = '{$config['default_prefix_id']}'
			AND pgp.switch = 'Y'
		";

		$query .= $set_search['sub_search_query'];
		$query .= " GROUP BY pgp.productid ";
		$query .= ($set_sort) ? $set_sort : "ORDER BY pgp.insertt DESC";
		$query_count_last = " ) os ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv)
			{
				//中標價
				if($tv['price']) {
					$price = round($tv['price']);
				} else {
					$price = 0;
				}
				$table['table']['record'][$tk]['price'] = $price;
				$table['table']['record'][$tk]['nickname'] = urldecode($tv['nickname']);

				//市價
				$table['table']['record'][$tk]['retail_price'] = round($tv['retail_price']); //sprintf("%0.2f", $tv['retail_price']);
			}

			return $table;
		}

		return false;
    }

    public function get_info($id) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT p.productid, p.name, p.description, p.retail_price,
				p.thumbnail_url, p.ontime, p.offtime, p.saja_limit, p.usereach_limit,
				p.saja_fee,	p.process_fee, p.closed, p.totalfee_type, p.pay_type,
				unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`,
				pgp.price, pgp.insertt, pgp.userid, up.nickname, pt.filename, pt.filename as thumbnail,
				up.thumbnail_file as m_thumbnail_file, up.thumbnail_url as m_thumbnail_url, p.ptype,p.chainstatus,p.tx_hash
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
			pgp.prefixid = p.prefixid
			AND pgp.productid = p.productid
			AND p.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			pgp.prefixid = sp.prefixid
			AND pgp.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid
			AND cs.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
			pgp.prefixid = up.prefixid
			AND pgp.userid = up.userid
			AND up.switch = 'Y'
		WHERE
			pgp.prefixid = '{$config["default_prefix_id"]}'
			AND p.productid = '{$id}'
			AND pgp.switch = 'Y'
			AND p.productid IS NOT NULL
			AND sp.productid IS NOT NULL
			AND cs.channelid IS NOT NULL
			AND up.userid IS NOT NULL
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			//中標價
			if($table['table']['record'][0]['price']) {
				$table['table']['record'][0]['price'] = str_replace(",","",(number_format($table['table']['record'][0]['price'])));
			} else {
				$table['table']['record'][0]['price'] = 0;
			}

			//市價
			$table['table']['record'][0]['retail_price'] = str_replace(",","",(number_format($table['table']['record'][0]['retail_price']))); //number_format($table['table']['record'][0]['retail_price'], 2);

			//简介
			$description = (!empty($table['table']['record'][0]['description']) ) ? $table['table']['record'][0]['description'] : '空白';
			$table['table']['record'][0]['description'] = html_decode($description);

			return $table['table']['record'][0];
		}
		return false;
    }

    //是否下標
    public function get_bid_user($productid) {
        global $db, $config;

		$query = "select productid
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
			and userid = '{$_SESSION['auth_id']}'
		    and productid = '{$productid}'
		    AND productid IS NOT NULL
			AND userid IS NOT NULL
			limit 0, 1
		";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false;
    }

	// Add By Thomas 20151106
	 public function getUserSajaHistory($userid, $productid='', $orderBy=' price ASC ', $limit='') {
        global $db, $config;

		$query = "select * FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		  WHERE prefixid = '{$config["default_prefix_id"]}'
			AND switch='Y'
			AND userid = '{$userid}' ";
		if(!empty($productid)) {
		   $query.=" AND productid = '{$productid}' ";
		}

        if(!empty($orderBy)) {
	       $query.=" ORDER BY ".$orderBy;
		}
		if(!empty($limit) && is_int($limit)) {
		   $query.=" LIMIT 0,".$limit;
		}

		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }

    //下標記錄統計
    public function get_bid_info($productid) {
        // global $db, $config;
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "select price, count(price) as num
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
		    and productid = '{$productid}'
		    and type = 'bid'
		    AND productid IS NOT NULL
			AND userid IS NOT NULL
			and switch = 'Y'
			group by price
			order by price
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record']))
		{
			return $table['table']['record'];
		}
		return false;
    }

    public function product_cancelled_list() {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = $this->set_sort();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`, pt.filename thumbnail ";

		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid
			AND cs.channelid = '{$_GET["channelid"]}'
			AND cs.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND closed = 'NB'
			AND p.switch = 'Y'
		";

		$query .= ($set_sort) ? $set_sort : "ORDER BY p.offtime DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];


		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				//市價
				$table['table']['record'][$tk]['retail_price'] = str_replace(",","",(number_format($tv['retail_price']))); //sprintf("%0.2f", $tv['retail_price']);
				//流標原因
				if ($this->bid_info($tv['productid']) < $tv['saja_limit']) {
					$table['table']['record'][$tk]['memo'] = '未達'.$tv['saja_limit'].'標';
				} else {
					$table['table']['record'][$tk]['memo'] = '無唯一流標';
				}
			}

			return $table;
		}

		return false;
    }

    //競標次數
    public function bid_info($productid) {
        // global $db, $config;
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "select count(*) as num
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
		    and productid = '{$productid}'
		    and type = 'bid'
		    AND productid IS NOT NULL
			AND userid IS NOT NULL
			and switch = 'Y'
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['num'];
		}
		return false;
    }

    /*
	 *	取得流標商品資料
	 *	$id					int					商品編號
	 */
    public function get_cancelled_info($id) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT p.productid, p.name, p.description, p.retail_price, p.thumbnail_url,
		p.ontime, p.offtime, p.saja_limit, p.usereach_limit, p.saja_fee, p.process_fee,
		p.closed, p.totalfee_type, p.pay_type, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`,
		pt.filename, pt.filename as thumbnail
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid
			AND cs.channelid = '{$_GET['channelid']}'
			AND cs.switch = 'Y'
		WHERE
			p.prefixid = '{$config["default_prefix_id"]}'
			AND p.productid = '{$id}'
			AND p.switch = 'Y'
			AND p.productid IS NOT NULL
			AND sp.productid IS NOT NULL
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			//中標價
			if($table['table']['record'][0]['price']) {
				$table['table']['record'][0]['price'] = str_replace(",","",(number_format($table['table']['record'][0]['price'], 2)));
			} else {
				$table['table']['record'][0]['price'] = 0;
			}

			//市價
			$table['table']['record'][0]['retail_price'] = str_replace(",","",(number_format($table['table']['record'][0]['retail_price']))); //number_format($table['table']['record'][0]['retail_price'], 2);

			//简介
			$description = (!empty($table['table']['record'][0]['description']) ) ? $table['table']['record'][0]['description'] : '空白';
			$table['table']['record'][0]['description'] = html_decode($description);

			//流標原因
			if ($this->bid_info($table['table']['record'][0]['productid']) < $table['table']['record'][0]['saja_limit']) {
				$table['table']['record'][0]['memo'] = '未達'.$table['table']['record'][0]['saja_limit'].'標';
			} else {
				$table['table']['record'][0]['memo'] = '無唯一流標';
			}

			return $table['table']['record'][0];
		}
		return false;
    }

    /*
	 *	取得流標商品下標記錄
	 *	$productid					int					商品編號
	 */
    public function get_cancelled_bid_info($productid) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "select price, count(price) as num
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
		    and productid = '{$productid}'
		    and type = 'bid'
		    AND productid IS NOT NULL
			AND userid IS NOT NULL
			and switch = 'Y'
			group by price
			order by price
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }

    //下標金標數統計
    public function get_bid_list($productid, $type, $sIdx='', $perpage='')
	{
        // global $db, $config;
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();
		if ($type == 'A') {
			$query = "select price, count(price) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
			where
				prefixid = '{$config["default_prefix_id"]}'
				and productid = '{$productid}'
				and type = 'bid'
				AND productid IS NOT NULL
				AND userid IS NOT NULL
				and switch = 'Y'
				group by price
				order by price
			";

			//總筆數
			$getnum = $db->getQueryRecord($query);
			$getnum	= count($getnum['table']['record']);
			$num = (!empty($getnum)) ? $getnum : 0;
			$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];


			if (empty($perpage)) {
				$perpage = $config['max_page'];
			}

			if($num) {
				//分頁資料
				$page = $db->recordPage($num, $this);

				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

				//取得資料
				$table = $db->getQueryRecord($query . $query_limit);
			} else {
				$table['table']['record'] = '';
			}
		} else {
			$query1 = "select price
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
			where
				prefixid = '{$config["default_prefix_id"]}'
				AND productid = '{$productid}'

			";
			$price = $db->getQueryRecord($query1);
			$bidprice = (!empty($price['table']['record'][0]['price'])) ? $price['table']['record'][0]['price'] : 0 ;

			$query2 = "select price, count(price) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
			where
				prefixid = '{$config["default_prefix_id"]}'
				AND productid = '{$productid}'
				AND type = 'bid'
				AND productid IS NOT NULL
				AND userid IS NOT NULL
				AND switch = 'Y'
				AND price <= {$bidprice}
				group by price
				order by price DESC
				LIMIT 0,6

			";
			$table1 = $db->getQueryRecord($query2);

			$query3 = "select price, count(price) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
			where
				prefixid = '{$config["default_prefix_id"]}'
				AND productid = '{$productid}'
				AND type = 'bid'
				AND productid IS NOT NULL
				AND userid IS NOT NULL
				AND switch = 'Y'
				AND price >= {$bidprice}
				group by price
				order by price DESC
				LIMIT 0,4

			";
			$table2 = $db->getQueryRecord($query3);

			$table_list = array_merge($table1['table']['record'], $table2['table']['record']);

			function score_sort($a, $b) {
				if($a['price'] == $b['price']) return 0;
				return ($a['price'] > $b['price']) ? 1 : -1;
			}

			usort($table_list, 'score_sort');
			$table['table']['record'] = $table_list;

			//總筆數
			$getnum	= count($table_list);
			$num = (!empty($getnum)) ? $getnum : 0;
			$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

			if (empty($perpage)) {
				$perpage = $config['max_page'];
			}

			if($num) {
				//分頁資料
				$page = $db->recordPage($num, $this);
			} else {
				$table['table']['record'] = '';
			}
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		}

		return false;
    }

	/*
	 *	取得競標中商品某會員下標金額統計清單
	 *	$userid						int					會員編號
	 *	$productid					int					商品編號
	 *	$startprice					int					開始下標價位
	 */
    public function get_onbid_list($productid, $userid, $startprice) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$endprice = (($startprice + 500) - 1);
		if ($startprice>=0)$term1=" AND (price between '{$startprice}' AND '{$endprice}')";
		$query = "select price, count(price) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
			AND productid = '{$productid}'
			AND type = 'bid'
			AND productid IS NOT NULL
			AND userid IS NOT NULL
			AND switch = 'Y' ".$term1;

		if(!empty($userid)) {
		   $query.=" AND userid = '{$userid}' ";
		}

		$query.=" group by price order by price ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}

		return false;
    }

	/*
	 *	統計特定商品某會員下標總數
	 *	$userid						int					會員編號
	 *	$productid					int					商品編號
	 */
    public function user_bid_count($userid, $productid) {

		if(empty($productid)) {
			return 0;
		}

		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "select count(shid) as bid_count
		 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE prefixid = '{$config["default_prefix_id"]}'
		  AND productid = '{$productid}'
		  AND switch='Y'
		  AND type='bid' ";

		if(!empty($userid)) {
		   $query.=" AND userid = '{$userid}' ";
		}

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['bid_count'];
		} else {
			return 0;
		}
	}

	/*
	 *	取得特定商品某會員最大下標金額
	 *	$userid						int					會員編號
	 *	$productid					int					商品編號
	 */
    public function user_bid_max($userid, $productid) {
		if(empty($productid)) {
		  return 0;
		}
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "select price
		 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE prefixid = '{$config["default_prefix_id"]}'
		  AND productid = '{$productid}'
		  AND switch='Y'
		  AND type='bid' ";

		if(!empty($userid)) {
		   $query.=" AND userid = '{$userid}' ";
		}

		$query.=" GROUP BY price ORDER BY price DESC LIMIT 0, 1 ";

		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['price'];
		} else {
			return 0;
		}
	}

	/*
	 *	取得特定商品某會員最後下標時間
	 *	$userid						int					會員編號
	 *	$productid					int					商品編號
	 */
    public function user_bid_last($userid, $productid) {

		if(empty($productid)) {
			return 0;
		}
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "select insertt
		 FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		WHERE prefixid = '{$config["default_prefix_id"]}'
		  AND productid = '{$productid}'
		  AND switch='Y'
		  AND type='bid'
		";

		if(!empty($userid)) {
		   $query.=" AND userid = '{$userid}' ";
		}

		$query.=" ORDER BY insertt DESC	LIMIT 0, 1 ";

		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['insertt'];
		} else {
			return 0;
		}
	}

	//總計下標成功人數
    public function bid_user_count() {
        global $db, $config;
		$query1 = "select count(price) as total
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
		where
			prefixid = '{$config["default_prefix_id"]}'
			AND switch = 'Y'
		group by
			userid
		";
		$table = $db->getQueryRecord($query1);

		if(!empty($table['table']['record']))
		{
			//成功人數固定加163333
			$num = count($table['table']['record']);
			return $num + (int)16333;
		}
		return false;
    }


    //總計節省金額
    // 原撈DB 3次  改為 2次
    public function bid_money_count() {
        global $db, $config;
        //下標總額
		// $query1 = "select SUM(h.price) as total
		// FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		// LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
		// 	ON h.productid = pgp.productid AND pgp.userid = h.userid
		// where
		// 	pgp.prefixid = '{$config["default_prefix_id"]}'
		// 	AND	h.prefixid = '{$config["default_prefix_id"]}'
		// 	AND pgp.switch = 'Y'
		// 	AND h.switch = 'Y'
		// ";

    $query1 = "SELECT IFNULL(SUM(pgp.bid_price_total),0) as total
				FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
				WHERE pgp.switch = 'Y'
				";

		$table = $db->getQueryRecord($query1);

        // SUM(商品巿價)
		$query2 = "select SUM(retail_price) as total
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			ON p.productid = pgp.productid
		where
			pgp.prefixid = '{$config["default_prefix_id"]}'
			AND	p.prefixid = '{$config["default_prefix_id"]}'
			AND pgp.switch = 'Y'
			AND p.switch = 'Y'
		";
		$table2 = $db->getQueryRecord($query2);

        if(!empty($table['table']['record']) && !empty($table2['table']['record'][0]['total']))	{
			$all_discount = $table2['table']['record'][0]['total'] - $table['table']['record'][0]['total'];
			$all_discount = $all_discount + 17000000 + 250000000;
			return $all_discount;
		}
		return false;
    }

		/*
		*最新成交紀錄
		*/
		public function recently_closed_product_list($page_number=1,$keyword,$ymd,$ctype=-1) {

	        global $db, $config;
			/*
			global $config;
			$db = new mysql($config["db2"]);
			$db->connect();
            */
      //預設分頁參數設定
			$term='';
			if (!(empty($keyword))) $term=" and sp.name like '%{$keyword}%'";
			if (!(empty($ymd))) $term.=" and left(pgp.insertt,10) = '{$ymd}'";

      if(empty($page_number)){
				$page_number = 1;
			}
			/*switch ($ctype) {
				case '0':
					$term.= " AND sp.is_flash = 'N' and sp.ptype='0' and  sp.limitid='0' and sp.retail_price>=10000";
					break;
				case '1': //圓夢
					$term.= " AND sp.is_flash = 'N' and sp.ptype='1' ";
					break;
				case '2'://新手
					$term.= " AND sp.is_flash = 'N' and sp.limitid in (21,22) ";
					break;
				case '3'://老手
					$term.= " AND sp.is_flash = 'N' and limitid='24' ";
					break;
				case '4':
					$term.= " AND sp.is_flash = 'Y' ";
					break;
				default://0 大檔
					//$term.= " AND sp.is_flash = 'N' and sp.ptype='0' and  sp.limitid='0' and sp.retail_price>=10000";
					break;
			}*/
      //取得總筆數
			$query = "SELECT count(*) as totalcount
			          FROM `saja_shop`.`saja_pay_get_product` pgp
	                LEFT JOIN `saja_shop`.`saja_product` sp ON pgp.productid = sp.productid
	                LEFT JOIN saja_shop.`saja_product_thumbnail` spt ON sp.ptid = spt.ptid
					LEFT JOIN `saja_user`.`saja_user_profile` up ON pgp.userid = up.userid
	                WHERE pgp.switch = 'Y'
									  AND pgp.bid_price_total > 0
									  AND sp.switch = 'Y'
									  AND sp.is_test = 'N'
									  AND sp.retail_price>98
									$term
								ORDER BY pgp.insertt DESC";

      $result = $db->getQueryRecord($query);

			if(!empty($result['table']['record'][0]['totalcount'])){
        $totalcount = $result['table']['record'][0]['totalcount'];
      }else{
        $totalcount = 0;
      }

			//檢查是有紀錄
	    if($totalcount >= 1){

				//設定每一分頁筆數資料
				if(empty($perpage)){
					$perpage = $config['max_page'];
				}

				//計算分頁開始筆數
	      $rec_start = ($page_number-1) * $perpage + 1;

				//計算總共頁數
	      $totalpages = ceil($totalcount/$perpage);

	      //分頁參數輸出參數設定
				$page["rec_start"]       = $rec_start ;
				$page["totalcount"]      = $totalcount ;				//總共筆數
				$page["totalpages"]      = $totalpages;					//總共頁數
				$page["perpage"]      	 = $perpage ;						//每頁筆數
				$page["page"]       	   = $page_number ; 			//現在頁數

				//分頁索引頁碼產生
	      $max_range = $config['max_range'];
				$ploops  = floor(($page_number-1)/$max_range)*$max_range + 1 ;
				$ploope  = $ploops + $max_range -1;
				for($i=$ploops;$i <= $ploope;$i++){
					$page["item"][]["p"] = $i ;
				}

	      //取得最新成交紀錄
		  switch ($ctype) {
		  	case '0':
		  		$orderby=" ORDER BY pgp.insertt DESC";
		  		break;
		  	case '1':
		  		$orderby=" ORDER BY sp.retail_price desc";
		  		break;
		  	default:
		  		$orderby=" ORDER BY pgp.insertt DESC";
		  		break;
		  }
	      $query = "SELECT pgp.productid,
				                 CONCAT('".BASE_URL.APP_DIR."/bid/detail/?productid=',pgp.productid) as link_url,
												 sp.name,
												 sp.retail_price,
												 unix_timestamp(sp.offtime) as offtime,
												 unix_timestamp() as now,
												 pgp.price,
												 pgp.insertt,
												 pgp.nickname,
												 IFNULL(spt.filename,'') as thumbnail,
												 sp.thumbnail_url,
												 up.thumbnail_file as m_thumbnail_file,
												 up.thumbnail_url as m_thumbnail_url,
												 sp.ptype,sp.tx_hash,
												 pgp.userid
					FROM `saja_shop`.`saja_pay_get_product` pgp
	                LEFT JOIN `saja_shop`.`saja_product` sp ON pgp.productid = sp.productid
	                LEFT JOIN saja_shop.`saja_product_thumbnail` spt ON sp.ptid = spt.ptid
					LEFT JOIN `saja_user`.`saja_user_profile` up ON pgp.userid = up.userid
	                WHERE pgp.switch = 'Y'
									  AND pgp.bid_price_total > 0
									  AND sp.switch = 'Y'
									  AND sp.is_test = 'N'
									  AND sp.retail_price>98
									  $term $orderby
									";

	      $query_limit = " LIMIT ". ($rec_start-1) .",". ($perpage);

				$table = $db->getQueryRecord($query . $query_limit);

			}else{
				$table['table']['record'] = '';
			}

      //有紀錄返回設定
			if(!empty($table['table']['record'])){

				//分頁參數輸出
				$table['table']['page'] = $page;

				//輸出資料轉換
				foreach($table['table']['record'] as $tk => $tv){

					//中標價
					if($tv['price']){
						$price = round($tv['price']);
					}else{
						$price = 0;
					}
					$table['table']['record'][$tk]['price'] = $price;
					$etherscan_prefix=(strpos($_SERVER['SERVER_NAME'],'saja.com.tw')>0)?'https://etherscan.io/tx/':'https://rinkeby.etherscan.io/tx/';

					if ($table['table']['record'][$tk]['tx_hash']!='')
					$table['table']['record'][$tk]['tx_hash'] = $etherscan_prefix.$table['table']['record'][$tk]['tx_hash'];

					//得標者
					$table['table']['record'][$tk]['nickname'] = urldecode($tv['nickname']);

					//市價
					$table['table']['record'][$tk]['retail_price'] = round($tv['retail_price']);

					//得標者頭像
					if(empty($table['table']['record'][$tk]['m_thumbnail_url'])){
						if(empty($table['table']['record'][$tk]['m_thumbnail_file'])){
							$table['table']['record'][$tk]['m_thumbnail_file'] = '_DefaultHeadImg.jpg';
						}
					}

					//結標時間是否超過24小時(1.24小時內 2.超過24小時)
					if(($table['table']['record'][$tk]['now'] - $table['table']['record'][$tk]['offtime']) <= 86400){
						$table['table']['record'][$tk]['is_over_time'] = '0';
					}else{
						$table['table']['record'][$tk]['is_over_time'] = '1';
					}
				}

			  return $table;
		  }

			//無紀錄返回
	    return false;
    }

	//閃殺活動最新成交
	public function flash_closed_product($page_number=1, $vendorid) {

	    global $db, $config;

		//預設分頁參數設定
		if(empty($page_number)){
				$page_number = 1;
		}

		$sec = time() - 3600 * 6;
		$off = date('Y-m-d H:i:s', $sec);

		$join_sql = "LEFT JOIN `saja_shop`.`saja_product` sp ON pgp.productid = sp.productid
					LEFT JOIN saja_shop.`saja_product_thumbnail` spt ON sp.ptid = spt.ptid ";

		$where_sql = "AND sp.vendorid = '{$vendorid}'
					AND sp.is_flash = 'Y'
					AND sp.offtime >= '{$off}'
					AND sp.switch = 'Y'
					AND sp.is_test = 'N'
					AND sp.retail_price>98 ";

		//取得總筆數
		$query = "SELECT count(*) as totalcount FROM `saja_shop`.`saja_pay_get_product` pgp ";
		$query .= $join_sql;
		$query .= "WHERE pgp.switch = 'Y' AND pgp.bid_price_total > 0 ";
		$query .= $where_sql ." ORDER BY pgp.insertt DESC";

		$result = $db->getQueryRecord($query);

		if(!empty($result['table']['record'][0]['totalcount'])){
			$totalcount = $result['table']['record'][0]['totalcount'];
		}else{
			$totalcount = 0;
		}

		//檢查是有紀錄
	    if($totalcount >= 1){

			//設定每一分頁筆數資料
			if(empty($perpage)){
				$perpage = $config['max_page'];
			}

			//計算分頁開始筆數
			$rec_start = ($page_number-1) * $perpage + 1;

			//計算總共頁數
			$totalpages = ceil($totalcount/$perpage);

			//分頁參數輸出參數設定
				$page["rec_start"]       = $rec_start ;
				$page["totalcount"]      = $totalcount ;				//總共筆數
				$page["totalpages"]      = $totalpages;					//總共頁數
				$page["perpage"]      	 = $perpage ;					//每頁筆數
				$page["page"]       	   = $page_number ; 			//現在頁數

			//分頁索引頁碼產生
			$max_range = $config['max_range'];
			$ploops  = floor(($page_number-1)/$max_range)*$max_range + 1 ;
			$ploope  = $ploops + $max_range -1;
			for($i=$ploops;$i <= $ploope;$i++){
				$page["item"][]["p"] = $i ;
			}

			//取得最新成交紀錄
			$query = "SELECT pgp.productid,
						CONCAT('".BASE_URL.APP_DIR."/kusobid/detail/?productid=',pgp.productid) as link_url,
						sp.name,
						sp.retail_price,
						unix_timestamp(sp.offtime) as offtime,
						unix_timestamp() as now,
						pgp.price,
						pgp.insertt,
						pgp.nickname,
						IFNULL(spt.filename,'') as thumbnail,
						sp.thumbnail_url,
						up.thumbnail_file as m_thumbnail_file,
						up.thumbnail_url as m_thumbnail_url,
						sp.ptype,
						sp.vendorid,
						pgp.userid
					FROM `saja_shop`.`saja_pay_get_product` pgp ";
	        $query .= $join_sql ." LEFT JOIN `saja_user`.`saja_user_profile` up ON pgp.userid = up.userid ";
	        $query .= "WHERE pgp.switch = 'Y' AND pgp.bid_price_total > 0 ";
			$query .= $where_sql ." ORDER BY pgp.insertt DESC";

			$query_limit = " LIMIT ". ($rec_start-1) .",". ($perpage);

			$table = $db->getQueryRecord($query . $query_limit);

		}else{
			$table['table']['record'] = '';
		}

		//有紀錄返回設定
		if(!empty($table['table']['record'])){

				//分頁參數輸出
				$table['table']['page'] = $page;

				//輸出資料轉換
				foreach($table['table']['record'] as $tk => $tv){

					//中標價
					if($tv['price']){
						$price = round($tv['price']);
					}else{
						$price = 0;
					}
					$table['table']['record'][$tk]['price'] = $price;

					//得標者
					$table['table']['record'][$tk]['nickname'] = urldecode($tv['nickname']);

					//市價
					$table['table']['record'][$tk]['retail_price'] = round($tv['retail_price']);

					//得標者頭像
					if(empty($table['table']['record'][$tk]['m_thumbnail_url'])){
						if(empty($table['table']['record'][$tk]['m_thumbnail_file'])){
							$table['table']['record'][$tk]['m_thumbnail_file'] = '_DefaultHeadImg.jpg';
						}
					}

					//結標時間是否超過24小時(1.24小時內 2.超過24小時)
					if(($table['table']['record'][$tk]['now'] - $table['table']['record'][$tk]['offtime']) <= 86400){
						$table['table']['record'][$tk]['is_over_time'] = '0';
					}else{
						$table['table']['record'][$tk]['is_over_time'] = '1';
					}
				}

			return $table;
		 }

		//無紀錄返回
	    return false;
    }

    // add By Thomas 20190423
    // 取得特定商品下標價格與次數統計List
    public function get_bid_price_stats($productid)
    {

           global $db, $config;

           if(empty($productid)) {
              error_log("[m/bid/get_bid_price_stats] NO productid !!");
              return false;
           }
           $query = "   SELECT price , count(price) as cnt ".
                    "     FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` ".
                    "    WHERE switch='Y' ".
                    "      AND productid='{$productid}' ".
                    " GROUP BY price ORDER BY cnt asc, price asc ";
           error_log("[[m/bid/get_bid_price_stats]] query : ".$query);
           $table = $db->getQueryRecord($query);
           if(!empty($table['table']['record']))
		   {
			  return $table['table']['record'];
		   }
		   return false;
	}

	public function get_history_detail($productid, $userid, $price = 0){

		global $db, $config;

		$query = "SELECT shd.title, shd.thumbnail_filename as shd_thumbnail_filename
				FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` as h
				LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` as shd
				ON h.shdid = shd.shdid
				WHERE h.productid = '{$productid}'
				AND h.userid = '{$userid}'
				AND h.price = '{$price}'
				AND h.switch = 'Y'
				LIMIT 1 ";

		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record']))
		{
		   return $table['table']['record'][0];
		}
		return false;
	}


	/*
	 *	取得特定商品會員最後下標資料統計清單
	 *	$productid					int					商品編號
	 */
    public function get_bid_user_list($productid) {
        global $db, $config;

		$query = "select userid, count(shid) as bidall
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
			and userid <> '0'
		    and productid = '{$productid}'
		    AND productid IS NOT NULL
			AND userid IS NOT NULL
			AND spointid <> 0
		GROUP BY userid
		HAVING COUNT(shid)> 19
		";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }


	/*
	 *	取得特定商品&會員最後下標資料
	 *	$userid						int					會員編號
	 *	$productid					int					商品編號
	 */
	public function get_bid_user_new($userid,$productid) {
		global $db, $config;

		$query = "select userid, insertt
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
			and userid = '{$userid}'
		    and productid = '{$productid}'
		    AND productid IS NOT NULL
			AND userid IS NOT NULL
			AND spointid <> 0
		order by insertt DESC LIMIT 1
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false;
	}

    // 修改上鋉狀態為QUEUE/SCUESS
    public function updateBlockChain($productid, $status,$token_id,$targetid='',$tx_hash='',$ipfs_hash='') {
		global $db, $config;
		$term1='';
		$term2='';
		if ($tx_hash!='') $term1=", tx_hash='$tx_hash'";
		if ($ipfs_hash!='') $term2=", ipfs_hash='$ipfs_hash'";
		$query = "UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
					SET  chainstatus='".$status."',token_id=".$token_id." $term1 $term2 where prefixid ='saja' ";
		if ($productid==0){
			//非大檔
			$query.="and chainstatus='N' and productid in(".$targetid.")";
		}else{
			$query.="and productid='".$productid."'";
		}
		error_log("[bid/updateBlockChain]".$query);
		$db->query($query);
	}

	public function getDailybidded($targetdate=''){
		global $db, $config;
		if ($targetdate==''){
			$cntdate=date("Y-m-d",strtotime("-1 day"));
		}else{
			$cntdate=$targetdate;
		}

		$query = "SELECT productid FROM `saja_shop`.`saja_product` WHERE closed='Y' and offtime like '{$cntdate}%'";
		$db->query($query);
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
	}

}
?>
