<?php
/**
 * History Model 模組
 */

class HistoryModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt'; 

    public function __construct() {
    }
	
	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;
		
		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = ''; 
		
		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt"; 
		
		if($_GET){
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}
		
		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}
		
		return $sub_sort_query;
	}
	
	/**
     * Search Method : 杀幣纪录
     */
	public function spoint_list_set_search() {
		global $status, $config;
		
		$rs = array();
		$rs['join_query'] = " 
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh 
		    ON 	s.prefixid = dh.prefixid
			AND s.spointid = dh.spointid
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d 
		    ON	dh.prefixid = d.prefixid
			AND dh.depositid = d.depositid
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		
		return $rs;
	}

	/*
	 *	殺價幣清單資料
	 *	$uid				int					會員編號
	 *	$kind				varchar				顯示型態 (all:全部殺幣, get:取得殺幣, use:使用殺幣)
	 */
	public function spoint_list($uid, $kind) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->spoint_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
	
		/* 20181204 Add By Thomas 修改發票號碼 dh.invoiceno 
		$query_record = "SELECT s.behav, s.insertt, es.name as title, dh.invoiceno, IFNULL(s.amount,0) as amount, '' as note ";

		 $query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` s
		 LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es
		    ON 	s.prefixid = es.prefixid
			AND s.esid = es.esid
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh 
		    ON 	s.prefixid = dh.prefixid
			AND s.spointid = dh.spointid
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d 
		    ON	dh.prefixid = d.prefixid
			AND dh.depositid = d.depositid			
		  WHERE s.prefixid = '{$config['default_prefix_id']}'
			AND s.userid = '{$uid}'
			AND s.switch = 'Y'
			AND s.amount != 0
		";
		*/
		
		// 20191119 加入送幣紀錄以辨別gift 怎麼來的
		$query_record = "SELECT s.behav, s.insertt, es.name as title, dh.invoiceno, IFNULL(s.amount,0) as amount, '' as note, s.memo
						 ,(SELECT up.nickname
						     FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up 
  						    WHERE up.userid=s.memo 
							  AND s.memo REGEXP '^[[:digit:]]+$' ) AS new_user_name
						 ,(SELECT activity_type from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint_free_history` 
						    WHERE switch='Y' 
							  AND spointid=s.spointid) as activity_type, s.spointid, iv.is_winprize  ";
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` s
		 LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es
		    ON 	s.prefixid = es.prefixid
			AND s.esid = es.esid
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh 
		    ON 	s.prefixid = dh.prefixid
			AND s.spointid = dh.spointid
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d 
		    ON	dh.prefixid = d.prefixid
			AND dh.depositid = d.depositid	
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` iv 
		    ON 	iv.prefixid = dh.prefixid
			AND iv.invoiceno = dh.invoiceno
		WHERE s.prefixid = '{$config['default_prefix_id']}'
			AND s.userid = '{$uid}'
			AND s.switch = 'Y'
			AND s.amount != 0
		";
		
        if(!empty($kind) && $kind == "get") {
			$query .= " AND s.behav in('prod_sell','bid_by_saja','user_deposit','gift','bid_refund','sajabonus','feedback') ";
		} 
		if(!empty($kind) && $kind == "use") {
			$query .= " AND s.behav in('prod_sell','process_fee','user_saja','prod_buy') ";
		}		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY s.insertt DESC';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if (empty($perpage)){
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);		
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);		
			//取得資料
			// error_log("[m/mall] spoint_list : ".$query_record . $query . $query_limit);
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
			
		} else {
		    $page = $db->recordPage($num, $this);
			$table['table']['record'] = '';
		}
		
		$table['table']['page'] = $page;
		if(!empty($table['table']['record']) ) {
			return $table;
		}
		return false;
    }
	
	/**
     * Search Method : 殺價纪录
     */
	public function saja_list_set_search() {
		global $status, $config;
		
		$rs = array();

        $rs['join_query'] = " JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			h.prefixid = p.prefixid
			AND h.productid = p.productid
            AND p.switch = 'Y' 
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			     p.prefixid = pt.prefixid
		    AND  p.ptid=pt.ptid
			AND pt.switch='Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		return $rs;
	}
	
	/*
	 *	殺價紀錄清單
	 *	$uid					int					會員編號
	 *	$spointid				varchar				殺幣帳目ID
	 *	$closed					varchar				商品結標狀態(Y:已結標, N:競標中, NB:流標, NP:凍結殺幣, W:已結標並中標者)
  	 */
	public function saja_list($uid, $spointid=false, $closed='') {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->saja_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num from ( select count(*) as num ";

		$query_record = " SELECT h.scodeid, pt.filename, h.spointid, 
		                       p.productid, p.saja_fee, p.name, p.thumbnail_url, p.closed, p.retail_price, p.limitid, 
							   unix_timestamp(offtime) as offtime,  count(*) as count, 
							   IFNULL(pgp.userid,'') as final_winner_userid, pgp.pgpid, pgp.complete, 
							   h.nickname, h.insertt as order_time, p.offtime as pofftime, p.totalfee_type ";							   
							   
		if ($spointid) {
			$query_add = " and h.spointid != '0'";
		}
		if(!empty($closed)) {
			switch($closed) {
				case "Y": 	//已結標&&非中標者
					$query_add2 = " and p.closed = 'Y' and pgp.userid != {$uid}";
					break;
				case "N":	//競標中
					$query_add2 = " and p.closed = 'N' and p.offtime>NOW() ";
					break;
				case "NP":	//凍結殺幣
					$query_add2 = " and p.closed = 'N' and p.offtime>NOW() and h.spointid>0 ";
					break;
				case "NB":	//流標
					$query_add2 = " and p.closed = 'NB' ";
					break;
				case "W":	//已結標&&中標者
					$query_add2 = " and p.closed = 'Y' and pgp.userid = {$uid}";
					break;			
			}
		}
		
		$query = " FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp 
			ON p.productid = pgp.productid AND pgp.switch = 'Y' 
		WHERE 
			h.prefixid = '{$config['default_prefix_id']}'
			AND h.userid = '{$uid}'
			AND h.switch = 'Y'
			AND p.productid IS NOT NULL
			AND h.type='bid'
			AND p.display = 'Y'
			AND p.ptype = 0
			{$query_add}
			{$query_add2}
			GROUP BY h.productid
		"; 		
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : ' ORDER BY h.shid DESC ';
		
		$query_count_last = " ) os ";
		
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);		

		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
	
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			// error_log("[model/history] saja_list : ".$query_record . $query . $query_limit);
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			
			$table['table']['page'] = $page;
			
			foreach($table['table']['record'] as $tk => $tv) {
				$table['table']['record'][$tk]['retail_price'] = round($tv['retail_price']);
				// 付款總金額
				$total_data = $this->get_total($tv['productid'], $uid);
                if($total_data){
					
					switch($tv['totalfee_type']) {
						case 'A':
							$saja_fee_amount = $total_data['totalprice']+$total_data['totalfee'];
							break;
						case 'B':
							$saja_fee_amount = $total_data['totalprice'];
							break;			  
						case 'O':
							$saja_fee_amount = 0;
							break;
						case 'F':
							$saja_fee_amount = $total_data['totalfee'];
							break;
						default:
							$saja_fee_amount = $total_data['totalfee'];
							break;			  
					}
			    	$table['table']['record'][$tk]['alltotal'] = ceil($saja_fee_amount);
				} else {
					$table['table']['record'][$tk]['alltotal'] = 0;
				}
            }	
			return $table;
		}
		return false;
    }
	
	/*
	* 殺價纪录商品資料
	*/
	public function saja_detail_set_search() {
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			h.prefixid = p.prefixid
			AND h.productid = p.productid
			AND p.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		
		return $rs;
	}
	
	public function saja_detail($productid, $uid) {
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->saja_detail_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT h.*, p.saja_fee, count(*) as count ";
		
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
		{$set_search['join_query']}
		WHERE 
			h.prefixid = '{$config['default_prefix_id']}'
			AND h.productid = '{$productid}'
			AND h.userid = '{$uid}'
			AND h.switch = 'Y' 
			AND p.productid IS NOT NULL 
			GROUP BY h.spointid, h.scodeid, h.oscodeid, h.sgiftid
		";
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : ' ORDER BY h.shid DESC ';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		$num = (!empty($getnum['table']['record']) ) ? count($getnum['table']['record']) : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
				
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			// error_log("[model/history/saja_detail]:".$query_record . $query . $query_limit);
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			foreach($table['table']['record'] as $rk => $rv) {
				$get_saja_fee = sprintf('%0.2f', $rv['saja_fee'] * $rv['count']);
				$table['table']['record'][$rk]['saja_fee'] = $get_saja_fee;
				$table['table']['record'][$rk]['bidtime'] = substr($rv['insertt'], -14);

				if($rv['count']>1) {
					$query ="SELECT min( price ) price1, max( price ) price2, max(spointid) spoint, max(scodeid) scode, max(oscodeid) oscode, max(sgiftid) sgift 
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
					WHERE insertt = '{$rv['insertt']}'
						AND productid = '{$productid}'			
						AND userid = '{$uid}'
					 GROUP BY insertt ORDER BY price desc
					";
					$recArr = $db->getQueryRecord($query); 
					
					$price1 = (empty($recArr['table']['record'][0]['price1']) ) ? 0 : $recArr['table']['record'][0]['price1'];
					$price2 = (empty($recArr['table']['record'][0]['price2']) ) ? 0 : $recArr['table']['record'][0]['price2'];
					$table['table']['record'][$rk]['price'] = str_replace(",","",(number_format($price1, 2))).' - '. str_replace(",","",(number_format($price2, 2)));
					
					if($recArr['table']['record'][0]['oscode']!=='0') {
						$by = '殺價券 ';
						$table['table']['record'][$rk]['saja_fee'] = 0;
					} elseif($recArr['table']['record'][0]['scode']!=='0') {
						$by = '超級殺價券 ';
						$table['table']['record'][$rk]['saja_fee'] = 0;
					} elseif($recArr['table']['record'][0]['sgift']!=='0') {
						$by = '禮券 ';
						$table['table']['record'][$rk]['saja_fee'] = 0;
					} else  {
						$by = '殺價幣 ';
					}
					
					if ($rv['type'] == 'refund') {
						$table['table']['record'][$rk]['type'] = $by .'流標退點';
					} else {
						$table['table']['record'][$rk]['type'] = $by .'連續下標';
					}
				} else {
					$query ="SELECT (select amount from saja_cash_flow.saja_spoint where spointid=sh.spointid ) as refund_fee, price, spointid spoint, scodeid scode, oscodeid oscode, sgiftid sgift 
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
					WHERE shid = '{$rv['shid']}'
					";
					$recArr = $db->getQueryRecord($query); 
					
					$price = (empty($recArr['table']['record'][0]['price']) ) ? 0 : $recArr['table']['record'][0]['price'];

					$table['table']['record'][$rk]['price'] = number_format($price, 2);
					
					if($recArr['table']['record'][0]['oscode']!=='0') {
						$by = '殺價卷 ';
						$table['table']['record'][$rk]['saja_fee'] = 0;
					} elseif($recArr['table']['record'][0]['scode']!=='0') {
						$by = '超級殺價券 ';
						$table['table']['record'][$rk]['saja_fee'] = 0;
					} elseif($recArr['table']['record'][0]['sgift']!=='0') {
						$by = '禮券 ';
						$table['table']['record'][$rk]['saja_fee'] = 0;
					} else  {
						$by = '殺價幣 ';
					}
					
					if ($rv['type'] == 'refund') {
					    $refund_fee = (empty($recArr['table']['record'][0]['refund_fee']) ) ? 0 : $recArr['table']['record'][0]['refund_fee'];
					    $table['table']['record'][$rk]['saja_fee']= str_replace(",","",number_format(-$refund_fee, 2));
						$table['table']['record'][$rk]['type'] = $by .'流標退點';
					} else {
						$table['table']['record'][$rk]['type'] = $by .'下標';
					}
					
				}
			}
			return $table;
		}
		return false;
    }
	
	/*
	 *	鯊魚點使用清單
	 *	$uid				int					會員編號
	 *	$kind				varchar				顯示型態 (get:取得點數, use:使用點數)
	 */
	public function bonus_list($uid, $kind) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		// $query_record = "SELECT evr.evrid, evr.vendor_name as name, b.bonusid, b.insertt, b.amount, b.behav, b.switch, b.tx_no, evr.tx_status ";		
		$query_record = "SELECT evr.evrid, evr.vendor_name as name, b.bonusid, b.insertt, b.amount, b.behav, b.switch, b.seq, b.tx_no, b.orderid, evr.tx_status ";
		/*$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` b 
		           LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr 
				                ON  b.bonusid=evr.bonusid
                               AND evr.switch='Y'								
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND b.switch = 'Y' "; */

		if (!empty($kind) && $kind == "get"){
			//order_refund:缺貨退費, product_close:結標轉紅利 
			$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` b 
		           LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr 
				                ON  b.bonusid=evr.bonusid
                               AND evr.switch='Y'								
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND b.switch = 'Y' ";
			$query .= " AND (b.behav in ('order_refund', 'product_close', 'system', 'system_test', 'order_close') or ((b.behav='user_qrcode_tx') and amount>0))";
		}
		if (!empty($kind) && $kind == "use"){
			$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` b
		           LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr
				                ON  b.bonusid=evr.bonusid
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND (evr.tx_status in(-2,-1,4) OR evr.tx_status IS null )
						AND b.amount<0 ";
			//ibon_exchange:ibon商品兌換, user_exchange:商品兌換, user_qrcode_tx:掃碼兌換
			$query .= " AND b.behav in ('ibon_exchange', 'user_exchange', 'user_qrcode_tx', 'other_exchange') ";
		}
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY b.insertt DESC';

        error_log("[m/history/bonus_list] num : ".$query_count . $query);
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			error_log("[m/history/bonus_list] SQL : ".$query_record . $query . $query_limit);
			//取得資料
			
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		return false;
    }
	/*
	public function bonus_list($uid, $kind) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		// $query_record = "SELECT evr.evrid, evr.vendor_name as name, b.bonusid, b.insertt, b.amount, b.behav, b.switch, b.seq ";	
        $query_record = "SELECT evr.evrid, evr.vendor_name as name, b.bonusid, b.insertt, b.amount, b.behav, b.switch, b.seq, b.tx_no, evr.tx_status ";			
		$query = " FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` b 
		           LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` evr 
				                ON  b.bonusid=evr.bonusid
                               AND evr.switch='Y'								
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND b.switch = 'Y' "; 
		
		if (!empty($kind) && $kind == "get"){
			//order_refund:缺貨退費, product_close:結標轉紅利 , order_close:得標贈(等市值)鯊魚點, system : 系統手動轉入
			$query .= " AND b.behav in ('order_refund', 'product_close', 'system','order_close') ";
		}
		if (!empty($kind) && $kind == "use"){
			//ibon_exchange:ibon商品兌換, user_exchange:商品兌換, user_qrcode_tx:掃碼兌換, other_system_exchange:外部商城兌換
			$query .= " AND b.behav in ('ibon_exchange', 'user_exchange', 'user_qrcode_tx','other_system_exchange') ";
		}
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY b.insertt DESC';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		return false;
    }
	*/
	
	 public function get_es_name($bonusid){
    	global $db;
    	$rs=$db->getRecord("SELECT bonusid FROM `saja_cash_flow`.`saja_bonus` WHERE tx_no in (select replace(tx_no,'_','') from `saja_cash_flow`.`saja_bonus` where bonusid='{$bonusid}')");
    	//var_dump("SELECT bonusid FROM `saja_cash_flow`.`saja_bonus` WHERE tx_no in (select replace(tx_no,'_','') from `saja_cash_flow`.`saja_bonus` where tx_no='{$bonusid}')");
    	$rs=$db->getRecord("select ep.* from `saja_exchange`.`saja_order` so,`saja_user`.`saja_enterprise_profile` ep where so.tx_data like '%{\"bonusid\":".$rs[0]['bonusid'].",%' and ep.enterpriseid=so.esid");
    	return $rs[0]['marketingname'];
    }
	
	public function get_bonus_list($uid) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT IFNULL((SELECT evrid 
		                           FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` 
								  WHERE bonusid=b.bonusid ),'') as evrid, ".
		                " b.bonusid, b.insertt, b.amount, b.behav, b.switch ";		
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` b 
				 WHERE   b.prefixid = '{$config['default_prefix_id']}'
				   AND b.userid = '{$uid}'
				   AND b.switch = 'Y' "; 
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY b.insertt DESC';
		
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		
		return false;
    }
	
	public function get_bonus_product($uid, $bonusArr) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		//SQL指令
		if ($bonusArr['behav'] == 'product_close' || $bonusArr['behav'] == 'order_close') {
			$query = "SELECT p.productid pid, p.name, '' as ex_num 
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
			inner join `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}settlement_history` sh on
				sh.prefixid = p.prefixid
				and sh.productid = p.productid
				and sh.bonusid = '{$bonusArr['bonusid']}'
				and sh.userid = '{$uid}'
				and sh.switch = 'Y'
			where 
				p.prefixid = '{$config['default_prefix_id']}'
				and p.switch = 'Y'
			";
		} else if($bonusArr['behav'] == 'ibon_exchange') {
			$query=" SELECT ibonexcid ,id as pid, prname as name, tr_num as ex_num ";
			$query.=" FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_ibon_record` ";
			$query.=" WHERE bonusid='{$bonusArr['bonusid']}' ";
		} else if($bonusArr['behav'] == 'user_qrcode_tx') {
			$query=" SELECT prod_pn as pid, vendor_name as name, FORMAT(tx_quantity,0) as ex_num ";
			$query.=" FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` ";
			if( !empty($bonusArr['tx_no'])) {
				$tx_no=str_replace('_','',$bonusArr['tx_no']);
				$query.=" WHERE evrid='${tx_no}' ";
			} else {
			    $query.=" WHERE bonusid='{$bonusArr['bonusid']}' ";
			}
		} else if($bonusArr['behav'] == 'other_exchange') {
		    // 外部商城兌換 - 由訂單中找到兌換數量
			// 另由訂單中的esid 找到外部商城名稱  
			// 給鯊魚點明細顯示用 By Thomasd 20200422
			$query = " SELECT es.name AS name , o.num AS ex_num 
						 FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o 
						 JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es 
						   ON o.esid=es.esid 
						WHERE orderid ='{$bonusArr['orderid']}' 
						LIMIT 1 ";		
		
		} else {
			$query = "SELECT o.orderid, o.epid as pid, p.name, o.num as ex_num  
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
			JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o on
				o.prefixid = p.prefixid
				and o.epid = p.epid
				and o.switch = 'Y'				
			LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` ebh on
				ebh.prefixid = o.prefixid
				and ebh.orderid = o.orderid
				and ebh.bonusid = '{$bonusArr['bonusid']}'
				and ebh.userid = '{$uid}'
				and ebh.switch = 'Y'
			where p.prefixid = '{$config['default_prefix_id']}'
				and o.userid = '{$uid}'
				and o.orderid= ebh.orderid
			";
			
		}
		error_log("[m/history]  get_bonus_product : ".$query);
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		} else {
		    return false;
		}
    }
    
	/*
    public function get_bonus_product($uid, $bonusArr) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		//SQL指令
		if ($bonusArr['behav'] == 'product_close' || $bonusArr['behav'] == 'order_close') {
			$query = "SELECT p.productid pid, p.name, '' as ex_num 
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
			inner join `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}settlement_history` sh on
				sh.prefixid = p.prefixid
				and sh.productid = p.productid
				and sh.bonusid = '{$bonusArr['bonusid']}'
				and sh.userid = '{$uid}'
				and sh.switch = 'Y'
			where 
				p.prefixid = '{$config['default_prefix_id']}'
				and p.switch = 'Y'	";
		} else if($bonusArr['behav'] == 'ibon_exchange') {
			$query=" SELECT ibonexcid ,id as pid, prname as name, tr_num as ex_num ";
			$query.=" FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_ibon_record` ";
			$query.=" WHERE bonusid='{$bonusArr['bonusid']}' ";
		} else if($bonusArr['behav'] == 'user_qrcode_tx') {
		    $query=" SELECT prod_pn as pid, prod_name as name, FORMAT(tx_quantity,0) as ex_num ";
			$query.=" FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_vendor_record` ";
			$query.=" WHERE bonusid='{$bonusArr['bonusid']}' ";
		} else {
			$query = "SELECT o.orderid, o.epid as pid, p.name, o.num as ex_num  
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
			JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o on
				o.prefixid = p.prefixid
				and o.epid = p.epid
				and o.switch = 'Y'				
			LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` ebh on
				ebh.prefixid = o.prefixid
				and ebh.orderid = o.orderid
				and ebh.bonusid = '{$bonusArr['bonusid']}'
				and ebh.userid = '{$uid}'
				and ebh.switch = 'Y'
			where p.prefixid = '{$config['default_prefix_id']}'
				and o.userid = '{$uid}'
				and o.orderid= ebh.orderid
			";
			
		}
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		} else {
		    return false;
		}
    }
	*/
	/*
     * Search Method : 殺價S碼
     */
	public function scode_list_set_search()	{
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` s ON 
			sh.prefixid = s.prefixid
			AND sh.scodeid = s.scodeid
			AND s.used = 'N'
			AND s.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			s.prefixid = p.prefixid
			AND s.productid = p.productid
			AND p.switch = 'Y'	
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		
		return $rs;
	}
	
	public function scode_list($uid) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->scode_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT p.*, count(*) as count, s.insertt, unix_timestamp(p.offtime) as offtime ";
		
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history` sh  
		{$set_search['join_query']}
		WHERE 
			sh.prefixid = '{$config['default_prefix_id']}'
			AND sh.userid = '{$uid}'
			AND sh.switch = 'Y'
			AND s.scodeid IS NOT NULL 
			AND p.productid IS NOT NULL
			GROUP BY sh.productid
		"; 
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY s.insertt DESC';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			return $table;
		}
		return false;
    }
	
	/**
     * Search Method : 禮券
     */
	public function coupon_list_set_search() {
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}sgift` s ON 
			sh.prefixid = s.prefixid
			AND sh.sgiftid = s.sgiftid
			AND s.used = 'N'
			AND s.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			s.prefixid = p.prefixid
			AND s.productid = p.productid
			AND p.switch = 'Y'	
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		
		return $rs;
	}
	
	public function coupon_list($uid) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->coupon_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT p.*, count(*) as count, s.insertt, unix_timestamp(p.offtime) as offtime ";
		
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}settlement_history` sh  
		{$set_search['join_query']}
		WHERE 
			sh.prefixid = '{$config['default_prefix_id']}'
			AND sh.userid = '{$uid}'
			AND sh.switch = 'Y'
			AND s.sgiftid IS NOT NULL 
			AND p.productid IS NOT NULL
			GROUP BY sh.productid
		"; 
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY s.insertt DESC';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			return $table;
		}
		return false;
    }
	
	/**
     * Search Method : 店点纪录
     */
	public function store_list_set_search() {
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}settlement_history` sh ON 
			g.prefixid = sh.prefixid
			AND g.giftid = sh.giftid
			AND sh.switch = 'Y'	
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			sh.prefixid = p.prefixid
			AND sh.productid = p.productid
			AND p.switch = 'Y'		
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		return $rs;
	}
	
	public function store_list($uid) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->store_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT g.insertt, g.amount, g.behav, p.productid, p.name ";
		
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}gift` g  
		{$set_search['join_query']}
		WHERE 
			g.prefixid = '{$config['default_prefix_id']}'
			AND g.userid = '{$uid}'
			AND g.switch = 'Y'
		"; 
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY g.insertt DESC';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
				
		
		if (empty($perpage)){
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			return $table;
		}
		return false;
    }
	
	/**
     * Search Method : 中標纪录
     */
	public function bid_list_set_search() {
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o ON 
			pgp.prefixid = o.prefixid
			AND pgp.pgpid = o.pgpid 
			AND o.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			pgp.prefixid = p.prefixid
			AND pgp.productid = p.productid
			AND p.switch = 'Y'		
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		
		return $rs;
	}
	
	public function bid_list($uid) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->bid_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT pgp.*, unix_timestamp(pgp.insertt) as insertt, p.name,p.bid_spoint,p.bid_scode, o.status ";
		
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp  
		{$set_search['join_query']}
		WHERE 
			pgp.prefixid = '{$config['default_prefix_id']}'
			AND pgp.userid = '{$uid}'
			AND pgp.switch = 'Y'
		"; 
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY pgp.insertt DESC';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		
		return false;
    }
	
	/**
     * 中標纪录
     */
	public function bid_checkout($productid, $uid) {
        global $db, $config;
        
		if(!empty($uid)) {
			$query ="SELECT p.*, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`
			              , pgp.pgpid, pgp.price, pgp.userid, pgp.complete, pt.filename as thumbnail
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
				pgp.prefixid = p.prefixid
				AND pgp.productid = p.productid
				AND p.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON 
				p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'				
			WHERE 
				pgp.`prefixid` = '{$config['default_prefix_id']}'
				AND pgp.productid = '{$productid}' 
				AND pgp.userid = '{$uid}'
				AND pgp.switch = 'Y'
			";
			
			$recArr = $db->getQueryRecord($query); 

			// 計算折抵金額 justice_lee 20190603
			$query ="SELECT sum(price) total_price, count(*) cnt 
				FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` 
				WHERE productid = '{$productid}'
				AND userid = '{$uid}'
				AND spointid > 0 
				group by userid,productid";

			$table = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'][0] = '';
		}
		
		if(!empty($recArr['table']['record'][0]) ) {
			// 計算折抵金額 justice_lee 20190603
			$recArr['table']['record'][0]['saja_cnt'] = $table['table']['record'][0]['cnt'];
			$recArr['table']['record'][0]['saja_total_price'] = $table['table']['record'][0]['total_price'];
			return $recArr['table']['record'][0];
		}
		return false;
    }

	public function get_total($productid, $uid)	{
        global $db, $config;
		//SQL指令
        /*
		$query_record = " SELECT SUM(h.price) as totalprice, SUM(p.saja_fee) as totalfee 
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h 
							JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
							ON h.prefixid = p.prefixid AND h.productid = p.productid AND p.switch = 'Y'
							WHERE 
								h.prefixid = '{$config['default_prefix_id']}'
								AND h.userid = '{$uid}'
								AND h.productid = '{$productid}'
								AND h.switch = 'Y'
								AND p.productid IS NOT NULL
								AND h.type='bid'
								AND h.spointid > 0 
							GROUP BY h.productid
						";
		*/
		$query_record = " SELECT SUM(h.price) as totalprice, SUM(h.bid_total) as totalfee 
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h 
							JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
							  ON h.prefixid = p.prefixid 
							 AND h.productid = p.productid 
							WHERE 
								h.prefixid = '{$config['default_prefix_id']}'
								AND h.userid = '{$uid}'
								AND h.productid = '{$productid}'
								AND h.switch = 'Y'
								AND p.productid IS NOT NULL
								AND h.type='bid'
								AND h.spointid > 0 
							GROUP BY h.productid
						";
		//取得資料
		$table = $db->getQueryRecord($query_record);
			
		if(!empty($table['table']['record']) ) {
		
			return $table['table']['record'][0];
		}
		return false;
    }

	/*
	 *	殺價幣收入資料
	 *	$uid			int				會員編號
	 */
	public function spoint_in_list($uid) {
        global $db, $config;
        
		//搜尋
		$set_search = $this->spoint_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT s.behav, s.insertt, es.name as title, d.invoiceno, IFNULL(s.amount,0) as amount, '' as note ";

		 $query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` s
		 LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es
		    ON 	s.prefixid = es.prefixid
			AND s.esid = es.esid 
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh 
			ON 	s.prefixid = dh.prefixid
			AND s.spointid = dh.spointid
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d 
			ON	dh.prefixid = d.prefixid
			AND dh.depositid = d.depositid
		  WHERE s.prefixid = '{$config['default_prefix_id']}'
			AND s.userid = '{$uid}'
			AND s.switch = 'Y' 
			AND s.behav in('prod_sell','bid_by_saja','user_deposit','gift','bid_refund','sajabonus','feedback','feedback')
		";
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		//判斷是否有資料
		if($num) {
			$table = $db->getQueryRecord($query_record . $query);
			$table['table']['num'] = $num;
		}
		
		if(!empty($table['table']['record'])) {
			return $table;
		}
		return false;
    }

	/*
	 *	殺價幣支出資料
	 *	$uid			int				會員編號
	 */
	public function spoint_out_list($uid) {
        global $db, $config;
        
		//搜尋
		$set_search = $this->spoint_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT s.behav, s.insertt, es.name as title, IFNULL(s.amount,0) as amount, '' as note ";

		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` s
		LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es
		    ON 	s.prefixid = es.prefixid
			AND s.esid = es.esid			 
		WHERE s.prefixid = '{$config['default_prefix_id']}'
			AND s.userid = '{$uid}'
			AND s.switch = 'Y' 
			AND s.behav in('prod_sell','process_fee','user_saja','other_system_use')
		";
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		//判斷是否有資料
		if($num) {
			$table = $db->getQueryRecord($query_record . $query);
			$table['table']['num'] = $num;
		}
		
		if(!empty($table['table']['record'])) {
			return $table;
		}
		return false;
    }

	/*
	 *	殺價幣使用資料
	 *	$spointid					int						流水編號
	 */
	public function getSpointById($spointid) {
        global $db, $config;
        
		//SQL指令
		$query = "SELECT s.amount FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` s 
		WHERE s.prefixid = '{$config['default_prefix_id']}' 
			AND s.spointid = '{$spointid}' 
			AND s.switch = 'Y' 
		";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0]['amount'];
		}
		return false;
    }	
	
	// 會員得標限定商品的次數(預設 limitid=7 )
	public function get_num_bid_of_limited($userid, $limitids='', $stime='') {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

        if(empty($limitids)) {
		   $limitids=['7'];	
		}
		if(empty($stime)) {
			$stime='2019-10-01 00:00:00';
		}
		$str_limitid = "";
		$str_stime="";
		
		if(!empty($stime)) {
		    $str_stime=" AND offtime > '".$stime."' ";	
		}
		if(is_array($limitids)) {
		   for($idx=0; $idx<count($limitids); ++$idx) {
			   $str_limitid.="'".$limitids[$idx]."',";                     				   
		   }
		   $str_limitid=substr($str_limitid,0,-1);
		   $str_limitid="AND limitid IN (".$str_limitid.") ";
		} 
		if(!empty($userid)) {
			$query ="SELECT count(*) user_bid
			   FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pg
			  WHERE pg.prefixid = '{$config['default_prefix_id']}'
				AND pg.userid = '{$userid}'
				AND pg.switch = 'Y'
				AND pg.productid IN ( select productid from saja_shop.saja_product where switch='Y' {$str_limitid} {$str_stime} ) ";

			
			$table = $db->getQueryRecord($query);
			// error_log("[m/history/get_num_bid_of_limited] : ".$query);
			if(!empty($table['table']['record'])) {
				$results = (int)$table['table']['record'][0]['user_bid'];
				error_log("[m/history/get_num_bid_of_limited] result :".$results);
				return $results;
			}
			return false;
		}
		return false;
	}
	
	public function product_list($spointid, $uid) {
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->saja_detail_set_search();
		
		//SQL指令
		$query_record = "SELECT p.name ";
		
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
		{$set_search['join_query']}
		WHERE 
			h.prefixid = '{$config['default_prefix_id']}'
			AND h.spointid = '{$spointid}'
			AND h.switch = 'Y' 
			AND p.productid IS NOT NULL 
		GROUP BY h.spointid
		LIMIT 1";

		//取得資料
		$table = $db->getQueryRecord($query_record . $query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['name'];
		}
		return false;
    }
	
	
	/*
	 *	購物金使用清單
	 *	$uid				int					會員編號
	 *	$kind				varchar				顯示型態 (get:取得點數, use:使用點數)
	 */
	public function rebate_list($uid, $kind) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT b.reid, b.insertt, b.amount, b.behav, b.switch,  b.productid, b.orderid, b.touid, b.memo ";
		if (!empty($kind) && $kind == "get"){
			//order_refund:缺貨退費, user_deposit:使用者儲值, bid_close:結標轉入 system:系統轉入, system_test:系統測試, gift:別人轉贈 
			$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}rebate` b 
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND b.amount > 0 
						AND b.switch = 'Y' ";
			$query .= " AND (b.behav in ('order_refund', 'user_deposit', 'bid_close', 'system', 'system_test') or ((b.behav='others') and amount>0) or ((b.behav='gift') and amount>0))";
		}
		if (!empty($kind) && $kind == "use"){
			$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}rebate` b
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND b.amount < 0 
						AND b.switch = 'Y' ";
			// user_exchange:兌換折抵, gift:轉贈別人
			$query .= " AND (b.behav in ('user_exchange') or ((b.behav='others') and amount<0) or ((b.behav='gift') and amount<0)) ";
		}
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : 'ORDER BY b.insertt DESC';

        error_log("[m/history/discount_list] num : ".$query_count . $query);
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			error_log("[m/history/discount_list] SQL : ".$query_record . $query . $query_limit);
			//取得資料
			
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		return false;
    }	
	
	
	public function get_rebate_product($uid, $discountArr) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		//SQL指令

			$query = "SELECT o.orderid, o.epid as pid, p.name, o.num as ex_num  
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
			JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o on
				o.prefixid = p.prefixid
				and o.epid = p.epid
				and o.switch = 'Y'				
			where p.prefixid = '{$config['default_prefix_id']}'
				and o.userid = '{$uid}'				
			";
			
		error_log("[m/history/get_discount_product]  get_discount_product : ".$query);
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		} else {
		    return false;
		}
    }
	
	//新增rebate資訊
	public function add_rebate($uid, $amount, $orderid=0, $switch='N') {
        global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}rebate` SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$uid}', 
			orderid = '{$orderid}',
			behav = 'user_deposit', 
			amount = '{$amount}', 
			seq = '0', 
			switch = '{$switch}',
			insertt=NOW()
		";
		$db->query($query);
		return $reid = $db->_con->insert_id;
	}
	//rebate 購物金生效
	public function set_rebate($reid) {
        global $db, $config;
		
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}rebate` SET  
			`switch` = 'Y',
			`modifyt`=NOW()
			WHERE 
			`reid` = '{$reid}'
		";
		$db->query($query);
	} 
	
	
	/*
	 *	圓夢券使用清單
	 *	$uid				int					會員編號
	*	$kind				varchar				顯示型態 (Y:已使用, use:未使用)
	 */
	public function dscode_list($uid, $kind) {
        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT b.dscodeid, b.userid, b.epcid, b.behav, b.used, b.used_time, b.offtime, b.amount, b.feedback, b.fromuserid, b.touserid, b.promotion, b.remark, b.depositid, b.modifyt ";
		if (!empty($kind) && $kind == "N"){
			$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode` b 
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND b.used = 'N'
						AND b.switch = 'Y' ";
		}
		if (!empty($kind) && $kind == "Y"){
			$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode` b
				 WHERE 	b.prefixid = '{$config['default_prefix_id']}'
						AND b.userid = '{$uid}'
						AND b.used = 'Y'
						AND b.switch = 'Y' ";
		}
		$query .= ($set_sort) ? $set_sort : ' ORDER BY b.insertt DESC';
        error_log("[m/history/discount_list] num : ".$query_count . $query); 
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			error_log("[m/history/discount_list] SQL : ".$query_record . $query . $query_limit);
			//取得資料
			
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}
		return false;
    }		
	
}
?>