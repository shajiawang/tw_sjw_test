<?php
/**
 * App_Exe Model 模組
 *
 * This class contains all of the functions used for creating, managing and deleting
 * users.
 */

class AppExeModel {
    public $user_id = 0;
    public $name = "Guest";
    public $email = "Guest";
    public $ok = false;
    public $msg = "You have been logged out!";
    public $is_logged = false;
	public $str;
	
    /*
     *	Set all internal publiciables to 'Guest' status, then check to see if
     *	a user session or cookie exists.
     */
    public function __construct() {
        // if(!$this->check_session()) {
			// $this->check_cookie();
		// }
		return $this->ok;
    }
	
	/*
	 *	二維碼資料確認
	 *	$code			varchar				二維碼內容
	 *
	 */
	public function qrcode_check($code) {
		global $db, $config;

		$query = "SELECT * 
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}qrcode`
			WHERE 
				`prefixid`='{$config['default_prefix_id']}'
				AND `name`='{$code}'
				AND switch = 'Y'
			";
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record']) ) {
			return $table['table']['record'][0];
		}
		
		return false;
	}


}

?> 