<?php
/**
 * Mall Model 模組
 */
class MallModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt'; 

    public function __construct() {
    }
	
	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;
		
		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = ''; 
		
		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt"; 
		
		if($_GET){
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}
		
		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}
		
		return $sub_sort_query;
	}
	
	/**
     * Search Method : product_list_set_search
     */
	public function product_list_set_search() {
		global $status, $config;
		
		$rs = array();
		
		$rs['join_query'] = "LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail` pt ON 
			p.prefixid = pt.prefixid
			AND p.eptid = pt.eptid
			AND pt.switch = 'Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = ($_SESSION['m_prefix']=='m') ? "AND p.mob_type !='N' " : '';
		
		if(!empty($_GET["epcid"])) {
			$epcid = $_GET["epcid"];
		} else {
			$epcid = 1;
		}

		$status["status"]["search"]["epcid"] = $epcid;
		$status["status"]["search_path"] .= "&epcid=".$epcid;
		// 取消epcid & channel 的限制
		$rs['join_query'] .= "LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category_rt` pc ON 
			p.prefixid = pc.prefixid 
			AND p.epid = pc.epid 
			AND pc.`switch`='Y' 
		LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` c ON 
			c.prefixid = pc.prefixid 
			AND c.epcid = pc.epcid
			AND c.`switch`='Y' 
		";
		
		$rs['sub_search_query'] .= "AND pc.epid IS NOT NULL AND c.epcid IS NOT NULL ";
		return $rs;
	}
	
	/*
	 * 兌換商品清單
	 */
	public function product_list($use_type, $epcid='') {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->product_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT unix_timestamp(p.offtime) as offtime, c.epcid, pt.filename as thumbnail, 
						p.epid, p.name, p.thumbnail_url, p.tx_url ";
		
		$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
		{$set_search['join_query']}
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}' 
			AND p.switch = 'Y'
			AND p.ontime <= NOW()
			AND p.offtime > NOW()
			AND p.epid > 300 
 			AND (p.use_type = '{$use_type}' OR p.use_type = '0')	
		"; 
		
		if(!empty($epcid)) {
			$query .= " AND c.epcid = ".$epcid." ";
		}
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq, p.insertt DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			$table = $db->getQueryRecord($query_record . $query );
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			// 2019/12/18 改於 model中直接指定圖檔URL (By Thomas)
			foreach($table['table']['record'] as $tk => $tv) {
			    if(!empty($table['table']['record'][$tk]['thumbnail'])){
					//$table['table']['record'][$tk]['thumbnail'] = IMG_URL."/site/images/site/product/".$table['table']['record'][$tk]['thumbnail'];
					//$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL."/site/images/site/product/".$table['table']['record'][$tk]['thumbnail'];
					
					//台1購 20200218 AARONFU
					if($epcid=='158'){
						$table['table']['record'][$tk]['thumbnail_url'] = $table['table']['record'][$tk]['thumbnail'];
						$table['table']['record'][$tk]['thumbnail'] = $table['table']['record'][$tk]['thumbnail'];
					}else{
						$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL."/site/images/site/product/".$table['table']['record'][$tk]['thumbnail'];
						$table['table']['record'][$tk]['thumbnail'] = IMG_URL."/site/images/site/product/".$table['table']['record'][$tk]['thumbnail'];
					}
				}
			}
			
			//foreach($table['table']['record'] as $tk => $tv) { $productid = $tv['productid']; }
			//台1購 20191203 AARONFU
			//if($epcid=='158'){
			//	foreach($table['table']['record'] as $tk => $tv) {
			//		$table['table']['record'][$tk]["api_url"] = BASE_URL.APP_DIR.'/mall/shop66/';
			//		if(!empty($table['table']['record'][$tk]['thumbnail'])){
			//			$table['table']['record'][$tk]['thumbnail_url'] = $table['table']['record'][$tk]['thumbnail'];
			//			$table['table']['record'][$tk]['thumbnail'] = $table['table']['record'][$tk]['thumbnail'];
			//		}
			//	}
			//}
			
			return $table;
		}
		
		return false;
    }
	
	/*
	 * 特定分類兌換商品清單
	 */
	public function getExchangeProdList($channelid=1,$page=0,$perPage=10,$arrIncludeEpcids='',$arrExcludeEpcids='',$use_type) {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		//排序
		$set_sort = $this->set_sort();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT c.epcid, pt.filename as thumbnail, unix_timestamp(p.offtime) as unix_offtime, p.* ";
		
		$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
	   LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail` pt ON 
			p.prefixid = pt.prefixid
			AND p.eptid = pt.eptid
			AND pt.switch = 'Y'
	   LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category_rt` c ON
	        p.prefixid = c.prefixid
			AND p.epid=c.epid
			AND c.switch = 'Y'			
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}' 
			AND p.switch = 'Y'
			AND p.display = 'Y'
			AND p.ontime <= NOW()
			AND p.offtime > NOW()
			AND p.epid > 300 
			AND (p.use_type = '{$use_type}' OR p.use_type = '0')	
		";

		//指定撈某些特定類型		
		if (!empty($arrIncludeEpcids)) {
			$query .= " AND c.epcid IN (";
			for($idx=0;$i<count($arrIncludeEpcids);++$i) {
			    if($idx>0) {
                   $query .=",";   
				}				
			    $query .= $arrIncludeEpcids[$idx];
			}
            $query .= ") ";			
		} 
		
		// 排除某些類型不撈(ex : 第三方服務 id=40)
		if (!empty($arrExcludeEpcids)) {
			$query .= " AND c.epcid NOT IN (";
			for($idx=0;$i<count($arrExcludeEpcids);++$i) {
			    if($idx>0) {
                   $query .=",";   
				}				
			    $query .= $arrExcludeEpcids[$idx];
			}
            $query .= ") ";			
		}
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq, p.insertt DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		if($num) {
			$table = $db->getQueryRecord($query_record . $query );
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			return $table;
		}
		
		return false;
    }
	
	/**
     * Search Method : set_search
     */
	public function order_list_set_search()	{
		global $status, $config;
		
		$rs = array();
		$rs['join_query'] = "";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		
		return $rs;
	}
	
	/*
	 *	查詢訂單清單
	 *	$uid					int						會員編號
	 *	$type					varchar					訂單種類 saja:殺價類, exchange:兌換類
	 *	$status					int						訂單狀態 0:處理中, 1:已出貨, 2:退費, 3:已發送(結標取得殺價券 殺價幣等等), 4:已到貨
	 */
	public function order_list($uid, $type='', $status='') {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->order_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT o.orderid, o.userid, o.status, o.type, o.epid, o.pgpid, o.num, o.esid, o.point_price, o.process_fee, o.total_fee, o.memo, o.insertt, o.modifyt, 
								os.outtime, os.outtype, os.outcode, os.outmemo, os.backtime, os.backmemo, os.closetime, os.closememo ";
		
				/*$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o
				   LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_shipping` os ON
						o.orderid=os.orderid AND os.switch = 'Y'
		{$set_search['join_query']}
		WHERE
			o.prefixid = '{$config['default_prefix_id']}'
			AND o.userid = '{$uid}'
			AND o.confirm = 'Y'
			AND o.switch = 'Y'
		";*/
		$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o
				   LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_shipping` os ON
						o.orderid=os.orderid
		{$set_search['join_query']}
		WHERE
			o.prefixid = '{$config['default_prefix_id']}'
			AND o.userid = '{$uid}'
			AND o.confirm = 'Y'
		";
		/*if(!empty($type)) {
			$query .= " AND o.type = '{$type}'";
		}*/
		/*if($kind=='all'){
			$status = '';
		}elseif($kind=='process'){
			$status = '0';
		}elseif($kind=='close'){
			$status = '1';
		}else{
			$status = '2';
		}*/
		if (!(empty($status))){
			switch ($status){
				case '1':
					$query .= " AND o.status < 3 ";
					break;
				case '2':
					$query .= " AND o.status in(3,4,5) ";
					break;
				/*case '3':
					$query .= " AND o.status =5 ";
					break;*/
				default:
					break;
			}
		}		
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : " ORDER BY o.insertt DESC ";

				
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);
			
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			$status = _v('status');
			
			foreach($table['table']['record'] as $tk => $tv) { 
				$table['table']['record'][$tk] = $tv;
				$table['table']['record'][$tk]['memo'] = (!empty($tv['memo'])) ? $tv['memo'] : '';
				// $table['table']['record'][$tk]['outtime'] = (!empty($tv['outtime'])) ? $tv['outtime'] : '0000-00-00 00:00:00';
				$table['table']['record'][$tk]['outtime'] = (!empty($tv['outtime'])) ? $tv['outtime'] : '預定 '.date('Y-m-d', strtotime($tv['insertt']. ' + 18 day'));
				$table['table']['record'][$tk]['outtype'] = (!empty($tv['outtype'])) ? $tv['outtype'] : '';
				$table['table']['record'][$tk]['outcode'] = (!empty($tv['outcode'])) ? $tv['outcode'] : '目前尚未指定配送';
				$table['table']['record'][$tk]['outmemo'] = (!empty($tv['outmemo'])) ? "\n<br>".$tv['outmemo'] : '';
				$table['table']['record'][$tk]['backtime'] = (!empty($tv['backtime'])) ? $tv['backtime'] : '0000-00-00 00:00:00';
				$table['table']['record'][$tk]['backmemo'] = (!empty($tv['backmemo'])) ? $tv['backmemo'] : '';
				$table['table']['record'][$tk]['closetime'] = (!empty($tv['closetime'])) ? $tv['closetime'] : '0000-00-00 00:00:00';
				$table['table']['record'][$tk]['closememo'] = (!empty($tv['closememo'])) ? "\n<br>".$tv['closememo'] : '';
				switch ($tv['type']){
					case 'saja':
							$query = "SELECT p.productid, p.name, p.thumbnail_url, pt.filename
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` gp
							LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
								p.prefixid = gp.prefixid
								AND p.productid = gp.productid
								AND p.switch = 'Y'
							LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
								p.prefixid = pt.prefixid
								AND p.ptid = pt.ptid
								AND pt.switch = 'Y'
							WHERE
								gp.prefixid = '{$config['default_prefix_id']}'
								AND gp.pgpid = '{$tv['pgpid']}'
								AND gp.switch = 'Y'
								AND p.productid IS NOT NULL
							";
							$recArr = $db->getQueryRecord($query);
							$rs = isset($recArr['table']['record'][0]) ? $recArr['table']['record'][0] : '';

							$table['table']['record'][$tk]['productid'] =  ($rs) ? $rs['productid'] : '';
							$table['table']['record'][$tk]['name'] =  ($rs) ? $rs['name'] : '';
							$table['table']['record'][$tk]['href'] =  ($rs) ? APP_DIR .'/product/saja/?'. $status['status']['args'] .'&productid='. $rs['productid'] : '';
							$table['table']['record'][$tk]['thumbnail'] =  (!empty($rs['filename'])) ? $rs['filename'] : '';
							$table['table']['record'][$tk]['thumbnail_url'] =  ($rs) ? $rs['thumbnail_url'] : '';
							break;
					case 'user_qrcode_tx':
							$query = "SELECT marketingname ,thumbnail_url,thumbnail_file as  thumbnail from saja_user.saja_enterprise_profile
							WHERE 
							prefixid = 'saja'
							AND enterpriseid = '".$table['table']['record'][$tk]['esid']."' AND switch = 'Y'
							";

							$recArr = $db->getQueryRecord($query);
							$rs = isset($recArr['table']['record'][0]) ? $recArr['table']['record'][0] : '';

							$table['table']['record'][$tk]['productid'] =  '';
							$table['table']['record'][$tk]['name'] =  ($rs) ? $rs['marketingname'] : '';
							$table['table']['record'][$tk]['href'] =   '';
							$table['table']['record'][$tk]['thumbnail'] =  (!empty($rs['thumbnail'])) ? $rs['thumbnail'] : '';
							$table['table']['record'][$tk]['thumbnail_url'] =  ($rs) ? $rs['thumbnail_url'] : '';
                            // temporary update by thomas
                            // $tv['type']="exchange";							
							break;
					default:
							$query = "SELECT p.epid, p.name, p.thumbnail_url, pt.filename as thumbnail
							FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p
							LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail` pt ON
								p.prefixid = pt.prefixid
								AND p.eptid = pt.eptid
								AND pt.switch = 'Y'
							WHERE
							p.prefixid = '{$config['default_prefix_id']}'
							AND p.epid = '{$tv['epid']}'
							AND p.switch = 'Y'
							";
							$recArr = $db->getQueryRecord($query);
							$rs = isset($recArr['table']['record'][0]) ? $recArr['table']['record'][0] : '';

							$table['table']['record'][$tk]['productid'] = ($rs) ? $rs['epid'] : '';
							$table['table']['record'][$tk]['name'] =  ($rs) ? $rs['name'] : '';
							$table['table']['record'][$tk]['href'] =  ($rs) ? APP_DIR .'/mall/exchange/?'. $status['status']['args'] .'&epid='. $tv['epid'] : '';
							$table['table']['record'][$tk]['thumbnail'] =  (!empty($rs['thumbnail'])) ? $rs['thumbnail'] : '';
							$table['table']['record'][$tk]['thumbnail_url'] =  ($rs) ? $rs['thumbnail_url'] : '';					
							break;		
				}

			}
			
			return $table;
		}

		return false;
    }

	/*
	 *	查詢商城購買清單
	 *	$uid					int						會員編號
	 *	$type					varchar					訂單種類 saja:殺價類, exchange:兌換類 lifpay:生活繳費
	 */
	public function exchange_order_list($uid, $type='') {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->order_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT o.orderid, o.userid, o.status, o.type, o.epid, o.pgpid, o.num, o.esid, o.point_price, o.process_fee, o.total_fee, o.memo, o.insertt, o.modifyt, o.tx_data, 
								os.outtime, os.outtype, os.outcode, os.outmemo, os.backtime, os.backmemo, os.closetime, os.closememo, os.returntime, os.returnmemo ";
		
		$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o 
				   LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_shipping` os ON
						o.orderid=os.orderid AND os.switch = 'Y'			
		{$set_search['join_query']}
		WHERE 
			o.prefixid = '{$config['default_prefix_id']}' 
			AND o.userid = '{$uid}'
			AND o.confirm = 'Y'
			AND o.switch = 'Y'
		"; 
		
		if(!empty($type)) {
			$query .= " AND o.type = '{$type}'";
		}else{
			$query .= " AND o.type in ('exchange','lifepay')";
		}
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : " ORDER BY o.insertt DESC ";

				
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);
			
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			$status = _v('status');
			
			foreach($table['table']['record'] as $tk => $tv) { 
				$table['table']['record'][$tk] = $tv;
				$table['table']['record'][$tk]['memo'] = (!empty($tv['memo'])) ? $tv['memo'] : '';
				$table['table']['record'][$tk]['outtime'] = (!empty($tv['outtime'])) ? $tv['outtime'] : '0000-00-00 00:00:00';
				$table['table']['record'][$tk]['outtype'] = (!empty($tv['outtype'])) ? $tv['outtype'] : '';
				$table['table']['record'][$tk]['outcode'] = (!empty($tv['outcode'])) ? $tv['outcode'] : '目前尚未指定配送';
				$table['table']['record'][$tk]['outmemo'] = (!empty($tv['outmemo'])) ? $tv['outmemo'] : '';
				$table['table']['record'][$tk]['backtime'] = (!empty($tv['backtime'])) ? $tv['backtime'] : '0000-00-00 00:00:00';
				$table['table']['record'][$tk]['backmemo'] = (!empty($tv['backmemo'])) ? $tv['backmemo'] : '';
				$table['table']['record'][$tk]['returntime'] = (!empty($tv['returntime'])) ? $tv['returntime'] : '0000-00-00 00:00:00';
				$table['table']['record'][$tk]['returnmemo'] = (!empty($tv['returnmemo'])) ? $tv['returnmemo'] : '';
				$table['table']['record'][$tk]['closetime'] = (!empty($tv['closetime'])) ? $tv['closetime'] : '0000-00-00 00:00:00';
				$table['table']['record'][$tk]['closememo'] = (!empty($tv['closememo'])) ? $tv['closememo'] : '';

				if ($tv['status']=='5') {
					$table['table']['record'][$tk]['closetime'] = $table['table']['record'][$tk]['returntime'];
				}

				$query = "SELECT p.epid, p.name, p.thumbnail_url, ptf.filename as thumbnail 
					FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p
					LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` ptf ON 
						p.prefixid = ptf.prefixid
						AND p.epftid = ptf.epftid
						AND ptf.switch = 'Y'			
					WHERE 
					p.prefixid = '{$config['default_prefix_id']}' 
					AND p.epid = '{$tv['epid']}'
					AND p.switch = 'Y'
				";
				$recArr = $db->getQueryRecord($query);
				$rs = isset($recArr['table']['record'][0]) ? $recArr['table']['record'][0] : '';
				
				$table['table']['record'][$tk]['productid'] = ($rs) ? $rs['epid'] : '';
				$table['table']['record'][$tk]['name'] =  ($rs) ? $rs['name'] : '';
				$table['table']['record'][$tk]['href'] =  ($rs) ? APP_DIR .'/mall/exchange/?'. $status['status']['args'] .'&epid='. $tv['epid'] : '';
				$table['table']['record'][$tk]['thumbnail'] =  (!empty($rs['thumbnail'])) ? $rs['thumbnail'] : '';
				$table['table']['record'][$tk]['thumbnail_url'] =  ($rs) ? $rs['thumbnail_url'] : '';

				$tx_data = $table['table']['record'][$tk]['tx_data'];
				unset($table['table']['record'][$tk]['tx_data']);
				if (!empty($tx_data)) {
					$tx_data = json_decode($tx_data,true);
					switch ($tx_data['type']) {
						case '1'://信用卡
							$tx_query = "SELECT bankname
								FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` 
								WHERE bankid = '{$tx_data['bankid']}'";
							$tx_recArr = $db->getQueryRecord($tx_query);
							$bank_name = isset($tx_recArr['table']['record'][0]['bankname']) ? $tx_recArr['table']['record'][0]['bankname'] : '';
							
							$table['table']['record'][$tk]['idnumber'] = $tx_data['idnumber'];
							$table['table']['record'][$tk]['bank_name'] = $bank_name;
							$table['table']['record'][$tk]['telpaytype_name'] = "";
							$table['table']['record'][$tk]['telecom_op_name'] = "";
							$table['table']['record'][$tk]['endtime'] = $tx_data['endtime'];
							$table['table']['record'][$tk]['sale_number'] = $tx_data['number'];
							$table['table']['record'][$tk]['number'] = "";
							$table['table']['record'][$tk]['account_name'] = "";
							$table['table']['record'][$tk]['branch'] = "";
							$table['table']['record'][$tk]['type_name'] = "信用卡費";
							break;
						case '2'://房貸
						case '3'://車貸
						case '4'://信貸
						case '5'://學貸
							$tx_query = "SELECT bankname
								FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` 
								WHERE bankid = '{$tx_data['bankid']}'";
							$tx_recArr = $db->getQueryRecord($tx_query);
							$bank_name = isset($tx_recArr['table']['record'][0]['bankname']) ? $tx_recArr['table']['record'][0]['bankname'] : '';
							$account_name = isset($tx_recArr['table']['record'][0]['account_name']) ? $tx_recArr['table']['record'][0]['account_name'] : '';
							$branch = isset($tx_recArr['table']['record'][0]['branch']) ? $tx_recArr['table']['record'][0]['branch'] : '';
							
							$table['table']['record'][$tk]['idnumber'] = $tx_data['idnumber'];
							$table['table']['record'][$tk]['bank_name'] = $bank_name;
							$table['table']['record'][$tk]['telpaytype_name'] = "";
							$table['table']['record'][$tk]['telecom_op_name'] = "";
							$table['table']['record'][$tk]['endtime'] = $tx_data['endtime'];
							$table['table']['record'][$tk]['sale_number'] = $tx_data['number'];
							$table['table']['record'][$tk]['number'] = "";
							$table['table']['record'][$tk]['account_name'] = $account_name;
							$table['table']['record'][$tk]['branch'] = $branch;
							$table['table']['record'][$tk]['type_name'] = "貸款費";
							break;
						case '6'://電信費
							$tx_query = "SELECT name
								FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}telpaytype` 
								WHERE tpid = '{$tx_data['tpid']}'";
							$tx_recArr = $db->getQueryRecord($tx_query);
							$telpaytype_name = isset($tx_recArr['table']['record'][0]['name']) ? $tx_recArr['table']['record'][0]['name'] : '';
							$tx_query = "SELECT name
								FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}telecom_op` 
								WHERE toid = '{$tx_data['toid']}'";
							$tx_recArr = $db->getQueryRecord($tx_query);
							$telecom_op_name = isset($tx_recArr['table']['record'][0]['name']) ? $tx_recArr['table']['record'][0]['name'] : '';
							
							$table['table']['record'][$tk]['idnumber'] = $tx_data['idnumber'];
							$table['table']['record'][$tk]['bank_name'] = "";
							$table['table']['record'][$tk]['telpaytype_name'] = $telpaytype_name;
							$table['table']['record'][$tk]['telecom_op_name'] = $telecom_op_name;
							$table['table']['record'][$tk]['endtime'] = $tx_data['endtime'];
							$table['table']['record'][$tk]['sale_number'] = "";
							$table['table']['record'][$tk]['number'] = $tx_data['number'];
							$table['table']['record'][$tk]['account_name'] = "";
							$table['table']['record'][$tk]['branch'] = "";
							$table['table']['record'][$tk]['type_name'] = "電信費";
							break;
					}
				}
			}
			return $table;
		}
		return false;
    }
    
    //訂單兌換查詢(Frank - 14/11/26)
    public function order_exchange($uid, $epid)	{
		global $db, $config;
		
		$query = "select * 
		from `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
		where 
			prefixid = '{$config['default_prefix_id']}'
			AND epid = '{$epid}'
			AND userid = '{$uid}'
			and type = 'exchange'
			and epid is not null
			and status != '2'
			AND switch = 'Y'
		limit 0, 1
		";
		
		$recArr = $db->getQueryRecord($query); 
		
		if(!empty($recArr['table']['record'][0])) {
			return $recArr['table']['record'][0];
		}
		return false;
	}
    
    /*
	 * 商品資訊
	 */
	public function get_info($id) {
        
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
       
		$query ="SELECT unix_timestamp(p.offtime) as `unix_offtime`, unix_timestamp() as `now`, pt.filename as thumbnail, ptf.filename as thumbnail_file,
		ptf2.filename as thumbnail_file2, ptf3.filename as thumbnail_file3, ptf4.filename as thumbnail_file4, 
		p.epid, p.esid, p.custpid, p.name, p.description, p.thumbnail_url, p.method_type, p.ui_type, p.tx_url, pc.epcid, 
		p.ontime, p.offtime, p.process_fee, p.point_price, p.retail_price, p.limit_qty, p.pass_sms_auth, p.use_type, p.eptype, p.set_qty, p.reserved_stock, p.memo 
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
		LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category_rt` pc ON 
			p.prefixid = pc.prefixid 
			AND p.epid = pc.epid 
			AND pc.`switch`='Y' 		
		LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail` pt ON 
			p.prefixid = pt.prefixid
			AND p.eptid = pt.eptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` ptf ON 
			p.prefixid = ptf.prefixid
			AND p.epftid = ptf.epftid
			AND ptf.switch = 'Y'
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` ptf2 ON 
			p.prefixid = ptf2.prefixid
			AND p.epftid2 = ptf2.epftid
			AND ptf2.switch = 'Y'	
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` ptf3 ON 
			p.prefixid = ptf3.prefixid
			AND p.epftid3 = ptf3.epftid
			AND ptf3.switch = 'Y'	
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` ptf4 ON 
			p.prefixid = ptf4.prefixid
			AND p.epftid4 = ptf4.epftid
			AND ptf4.switch = 'Y'				
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND p.epid = '{$id}'
			AND p.switch = 'Y'
		";
		$recArr = $db->getQueryRecord($query);
		
		if(!empty($recArr['table']['record'][0])) {
			//商品简介
			$description = (!empty($recArr['table']['record'][0]['description']) ) ? $recArr['table']['record'][0]['description'] : '空白';
			$recArr['table']['record'][0]['description'] = html_decode($description);
			return $recArr['table']['record'][0];
		}

		return false;
    }
	
	
	/*
	* 商品庫存
	*/
	public function get_stock($id) {
        // global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
       
		$query ="SELECT SUM(num) num
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}stock` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}'
			AND epid = '{$id}'
			AND switch = 'Y'
		";
		
		$recArr = $db->getQueryRecord($query); 
		
		if(!empty($recArr['table']['record'][0])) {
			$stocknum = ($recArr['table']['record'][0]['num']) ? (int)$recArr['table']['record'][0]['num'] : 0;
			$stock = ($stocknum > 20) ? 20 : $stocknum;
			
			return $stock;
		}
		return false;
    }
    
	/*
	* 商品分類
	*/
	public function product_category($channelid) {
		// global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		$query ="SELECT epcid, name 
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `layer`	= '1' 
			AND `switch` = 'Y'
			AND `display` = 'Y'
			
		"; 
		if(!empty($channelid)){
			$query .= " AND `channelid` = '{$channelid}' "; 			
		}
		$query .= " ORDER BY seq "; 	
		
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
	}
	
	/*
	* 商品分類清單
	*/
	public function product_category_list($layer, $node=0) {
		global $config;
		
		if(empty($layer)) {
           $layer=3; 
        }
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		$query ="SELECT epcid, name, node, layer, if(layer=4,'0','1') next,
			IF (layer = 2, CONCAT('category/',thumbnail),thumbnail) thumbnail
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` 
		WHERE 	`prefixid` = '{$config['default_prefix_id']}' 
		AND `display` = 'Y'
		AND `switch` = 'Y' ";

		if(!empty($layer)) {
		   $query.=" AND `layer` = {$layer} ";
		}
		if(!empty($node)) {
		   $query.=" AND `node` = {$node} ";
		}		
		
		$query.=" ORDER BY seq ";
		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
	}
	
	/*
	* 單項商品分類
	*/
	public function get_product_category() {
		// global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		$query ="SELECT pc.* 
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` pc 
		left outer join `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category_rt` rt on 
			pc.prefixid = rt.prefixid 
			AND pc.epcid = rt.epcid
			AND rt.switch = 'Y'
		WHERE 
			pc.prefixid = '{$config['default_prefix_id']}' 
			AND pc.channelid = '{$_GET['channelid']}' 
			AND rt.epid = '{$_GET['epid']}' 
			AND pc.switch = 'Y' 
			ORDER BY pc.seq
		"; 
		
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
	}
	
	/*
	* 廣告
	*/
	public function get_ad() {
        // global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT *
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad` 
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND acid = '3'
				AND switch = 'Y'
				AND adid IS NOT NULL
				and promotetype = 'P'
				and ontime <= NOW()
				and offtime > NOW()
				ORDER BY ontime desc
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			$num = count($table['table']['record']);
			$rand = rand(0, $num-1);
			
			$result = $table['table']['record'][$rand]; 
			return $result;
		}
		return false;
    }
    
    /*
	* 抓取訂單
	*/
	public function get_order($uid, $oid="") {
        global $db, $config;
        
        if($oid != "") {
        	$query_order = " and orderid = '{$oid}'";
        }
        
		$query = "SELECT *
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				{$query_order}
				AND userid = '{$uid}'
				AND userid IS NOT NULL
				and type = 'exchange'
				and confirm = 'Y'
				AND switch = 'Y'
				ORDER BY insertt desc
				limit 0, 1
			";
			
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
    
    /*
	* 抓取訂單(Frank - 14/11/28)
	*/
	public function get_order_product($uid, $epid) {
        global $db, $config;
        
        $query = "SELECT *
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$uid}'
				AND userid IS NOT NULL
				and epid = '{$epid}'
				and status = '0'
				and type = 'exchange'
				AND switch = 'Y'
				ORDER BY insertt desc
				limit 0, 1
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
    
    /*
	 * 改變訂單狀態
	 */
	public function set_order($uid, $oid="", $switch="N", $confirm="", $status="") {
        global $db, $config;
        
        if ($oid != "") {
        	$query_order = " and orderid = '{$oid}'";
        }

        if ($confirm != "") {
        	$query_confirm = " confirm = '{$confirm}', ";
        }
		
        if ($status != "") {
        	$query_status = " status = '{$status}', ";
        }		
        
		$query = "update `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			set	switch = '{$switch}',
				{$query_confirm}
				{$query_status}
				modifyt = NOW()
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				{$query_order}
				AND userid = '{$uid}'
				AND userid IS NOT NULL
				and type = 'exchange'
			";
		$res = $db->query($query); 
    }
    
    //查詢庫存數量
    function stock_check($id) {
    	global $db, $config;
       
		$query ="SELECT SUM(num) num
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}stock` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}'
			AND epid = '{$id}'
			AND switch = 'Y'
		";
		
		$recArr = $db->getQueryRecord($query); 
		
		if(!empty($recArr['table']['record'][0])) {
			return $recArr['table']['record'][0]['num'];
		}
		return false;
    }
    
    //查詢紅利點數
    function bonus_check($id) {
    	global $db, $config;
        
        $query ="SELECT SUM(amount) bonus 
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus`  
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `switch` = 'Y'
			AND `userid` = '{$id}' 
		";
		$recArr = $db->getQueryRecord($query); 
		
		if(!empty($recArr['table']['record'][0])) {
			$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;
			return $user_bonus;
		}
		return false;
    }
    
    //新增訂單
    function add_order($infoArr, $uid) {
    	global $db, $config;

		if(!empty($infoArr['esid'])) {
			$esid = $infoArr['esid'];  
		} else {
			$esid = 1; 
		}

		if(!empty($infoArr['num'])) {
			$num = $infoArr['num'];  
		} else {
			$num = 1; 
		}
		
		if(!empty($infoArr['confirm'])) {
			$order_confirm = $infoArr['confirm'];  
		} else {
			$order_confirm = 'N'; 
		}			

		if(!empty($infoArr['status'])) {
			$status = $infoArr['status'];
		} else {
			$status = '0';
		}

		if(!empty($infoArr['type'])) {
			$type = $infoArr['type'];
		} else {
			$type = 'exchange';
		}
				
		$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
		SET
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$uid}',
			`status`='{$status}',
			`type`='{$type}',
			`epid`='{$infoArr['epid']}',
			`esid`='{$esid}',
			`num`='{$num}',

			`point_price`='{$infoArr['point_price']}',
			`process_fee`='{$infoArr['process_fee']}',
			`total_fee`='{$infoArr['point_price']}' + '{$infoArr['process_fee']}',
			`profit`='0',
			`cost`='{$infoArr['cost']}',
			`memo` = '{$infoArr['memo']}',
			`tx_data` = '{$infoArr['tx_data']}',	
			`confirm`='{$order_confirm}',
			`insertt`=NOW()
		";
		$res = $db->query($query); 
		$orderid = $db->_con->insert_id;
		
		return $orderid;
	}

	//新增訂單收件人
	function add_order_consignee($oid, $userArr) {
		global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` 
		SET 
			`userid`='{$userArr['userid']}',
			`orderid`='{$oid}',
			`name`='{$userArr['addressee']}',
			`phone`='{$userArr['phone']}',
			`zip`='{$userArr['area']}',
			`address`='{$userArr['address']}',
			`gender`='{$userArr['gender']}',
			`prefixid`='{$config['default_prefix_id']}',
			`insertt`=now()
		";
		$db->query($query);
	}
	
	//查詢紅利點數歷史資料
	function get_exchange_bonus_history($oid, $uid) {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		$query = "select * from `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` 
		where
			`prefixid` = '{$config['default_prefix_id']}' 
			and `orderid` = '{$oid}'
			and `userid` = '{$uid}'
			and `switch` = 'Y'
		";
		$recArr = $db->getQueryRecord($query); 
		
		if(!empty($recArr['table']['record'][0])) {
			return $recArr['table']['record'][0];
		}
		return false;
	}
	
	//加點數
	function add_bonus($exchange_bonus_Arr, $uid) {
		global $db, $config;
		
		$amount = 0 - $exchange_bonus_Arr['amount'];

		if(!empty($exchange_bonus_Arr['behav'])) {
			$behav = $exchange_bonus_Arr['behav'];  
		} else {
			$behav = 'user_exchange'; 
		}
		
		$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` set 
		          `prefixid` = '{$config['default_prefix_id']}', 
		          `userid` = '{$uid}', 
		          `countryid` = '{$config['country']}', 
		          `behav` = '{$behav}', 
		          `amount` = '{$amount}', 
		          `seq` = '0', 
		          `switch` = 'Y', 
		          `insertt` = now()";
		$db->query($query);
		$bonusid = $db->_con->insert_id;
		return $bonusid;
	}

	//新增紅利點數歷史資料
	function add_exchange_bonus_history($exchange_bonus_Arr, $uid, $bonusid) {
		global $db, $config;
		
		$amount = 0 - $exchange_bonus_Arr['amount'];
		
		$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_history` set 
		          `prefixid` = '{$config['default_prefix_id']}', 
		          `userid` = '{$uid}', 
		          `orderid` = '{$exchange_bonus_Arr['orderid']}',
		          `bonusid` = '{$bonusid}',
		          `amount`='{$amount}',
		          `seq` = '0', 
		          `switch` = 'Y', 
		          `insertt` = now()";
		$db->query($query);
		$ebhid=$db->_con->insert_id;
		return $ebhid;	
	}
	
	// 取得qrcode交易資料狀態
	// Add By Thomas 20141224
	public function getTxStatus($evrid) {

		global $db, $config;

		$tx_status='';

		if(!empty($evrid)) {
			$query=" SELECT tx_status FROM saja_exchange.saja_exchange_vendor_record ";
			$query.=" WHERE switch='Y' AND evrid='${evrid}' ";

			$table = $db->getQueryRecord($query);
			$tx_status=$table['table']['record'][0]['tx_status'];
		} else {
			error_log("[model mall.getTxStatus] evrid is empty !!");
		}
		return $tx_status;
	}
	
	// 取出Qrcode交易資料
	public function getQrcodeTxRecord($arrCond) {
		global $db, $config;

		if(count($arrCond)>0) {
			$query=" SELECT 
						evr.evrid, evr.vendorid, evr.userid, evr.vendor_name, evr.tx_code, evr.salt, 
						evr.tx_type, evr.prod_pn, evr.vendor_prodid, evr.prod_name, evr.tx_currency, evr.unit_price, 
						evr.tx_quantity, evr.total_price, evr.total_bonus, evr.bonusid, evr.tx_status, evr.qrcode_time,
						evr.keyin_time, evr.commit_time, evr.total_bonus, evr.memo, 							
						sum(IFNULL(b.amount,0)) as curr_bonus, up.bonus_noexpw";

			$query.=" FROM saja_user.saja_user_profile up ";
			$query.=" LEFT JOIN saja_cash_flow.saja_bonus b ";
			$query.=" ON up.switch='Y' and b.switch='Y'  AND up.userid=b.userid ";
			$query.=" LEFT JOIN saja_exchange.saja_exchange_vendor_record evr ";
			$query.=" ON up.switch='Y' and evr.switch='Y' AND up.userid=evr.userid ";
			$query.=" WHERE 1=1 ";

			foreach($arrCond as $key => $value) {
				$query.=(" AND ".$key."='".addslashes($value)."'");
			}

			$table = $db->getQueryRecord($query);

			if(!empty($table['table']['record'][0]['evrid'])) {
				return $table['table']['record'][0];
			} 
		}
		return false;
	}
	
	public function getExchangeVendorRecord($arrCond) {
		global $db, $config;

		if(count($arrCond)>0) {
		   $query=" SELECT * FROM saja_exchange.saja_exchange_vendor_record WHERE 1=1 ";		   
		   foreach($arrCond as $key => $value) {
			  $query.=(" AND ".$key."='".addslashes($value)."'");
		   }
		   $table = $db->getQueryRecord($query);
		   if(!empty($table['table']['record'][0]['evrid'])) {
			  return $table['table']['record'];
		   } 
		}
		return false;
	}
	
	// 修改Qrcode交易資料
	public function updQrcodeTxRecord($arrUpd,$arrCond) {
		global $db, $config;

		if(empty($arrUpd)) {
			return false;
		}
		$ret=false;
		$sql=" UPDATE saja_exchange.saja_exchange_vendor_record SET modifyt=NOW() ";
		foreach($arrUpd as $key => $value) {
			$sql.=(" ,".$key."='".addslashes($value)."'");
		}

		$sql.=" WHERE 1=1 ";
		if(count($arrCond)>0) {
			foreach($arrCond as $key => $value) {
				$sql.=(" AND ".$key."='".addslashes($value)."'");
			}
		}
		$ret = $db->query($sql);
		return $ret;
	}
	
	public function getAds($acid='', $showType='') {
		global $config;
		
		if(empty($acid))   $acid=1;  
		if(empty($showType))  $showType='P';  

		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT * FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad` 
			WHERE prefixid = '{$config['default_prefix_id']}'
				AND acid = '${acid}'
				AND switch = 'Y'
				AND adid IS NOT NULL
				and promotetype = '${showType}'
				and ontime <= NOW()
				and offtime > NOW()
				ORDER BY seq asc
			";
		$table = $db->getQueryRecord($query); 
		error_log("[m/mall/get_ad]: ".$query."-->".count($table['table']['record']));	
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
		    return false;
		}
    }
		
	public function getHotExchangeProdList($page=0,$perPage=10) {
		global $db, $config;

		$query = " SELECT p.epid, p.esid,p.name, p.eptid, p.ontime, unix_timestamp(p.offtime) as offtime, 
				  p.process_fee, p.retail_price, p.point_price,p.cost_price, 
				  pt.filename as thumbnail, count(o.orderid) as cnt ";
		$query.= " FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
		  LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail` pt ON 
			  p.prefixid = pt.prefixid
			  AND p.eptid = pt.eptid
			  AND pt.switch = 'Y'
			 JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}_exchange_product_category` o ON 
				  p.epid= o.epid
				  AND o.type='exchange' 
				  AND o.switch='Y' 
				  AND o.epid>0
			WHERE p.prefixid = '{$config['default_prefix_id']}' 
				  AND p.switch = 'Y' ";
		$query.= "		  
			GROUP BY p.epid, p.esid,p.name, p.eptid, p.ontime, p.offtime, 
				  p.process_fee, p.retail_price, p.point_price,p.cost_price, thumbnail
			ORDER BY cnt desc
		"; 
		if(!empty($perPage) && !empty($page)) {
		   $query.=" LIMIT ".$page." , ".$perPage;
		}
		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
	}
	
	/*
	 *	商家增加點數
	 *	$bonusid		int		紅利記錄編號
	 *	$esid			int		兌換中心編號
	 *	$enterpriseid	int		企業編號
	 *	$total_fee		int		紅利金額
	 *	$sys_profit		int		分潤
	 */
	function add_bonus_store($bonusid, $esid, $enterpriseid, $total_fee, $sys_profit=0) {
		global $db, $config;
		
		$query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` set 
				  `prefixid` = '{$config['default_prefix_id']}', 
				  `bonusid` = '{$bonusid}', 
				  `esid` = '{$esid}', 
				  `enterpriseid` = '{$enterpriseid}', 
				  `countryid` = '{$config['country']}', 
				  `behav` = 'user_exchange', 
				  `amount` = '{$total_fee}',
				  `sys_profit` = '{$sys_profit}',	
				  `seq` = '0', 
				  `switch` = 'Y', 
				  `insertt` = now()";
		$db->query($query);
		$bsid = $db->_con->insert_id;		
		return $bsid;
	}
	
	/*
	 *	新增紅利點數歷史資料
 	 *	$bsid			int			紅利記錄編號
	 *	$orderid		int			紅利記錄編號
	 *	$vndr_prodid	int			兌換中心編號
	 *	$vndr_txid		varchar		企業編號
	 *	$total_fee		int			紅利金額
	 *	$profit_ratio	int			分潤比
	 */
	function exchange_bonus_store_history($bsid, $orderid, $vndr_prodid, $vndr_txid, $total_fee, $profit_ratio=0) {
		global $db, $config;
		
		$query = "insert into `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_bonus_store_history` set 
				  `prefixid` = '{$config['default_prefix_id']}', 
				  `esid` = '{$vndr_prodid}', 
				  `orderid` = '{$orderid}',
				  `bsid` = '{$bsid}',
				  `enterpriseid` = '{$vndr_txid}', 
				  `amount` = '{$total_fee}',
				  `profit_ratio` = '{$profit_ratio}',
				  `seq` = '0', 
				  `switch` = 'Y', 
				  `insertt` = now()";
		$db->query($query);	
	}

    /*
	 *	改變紅利記錄狀態
	 *	$userid			int			使用者編號
	 *	$bonusid		int			紅利編號
	 *	$switch			varchar		狀態
	 */
	public function set_bonus($userid, $bonusid, $switch) {
        global $db, $config;
        
		$query = "update `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}bonus` 
			set
				switch = '{$switch}',
				modifyt = NOW()
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$userid}'
				AND bonusid = '{$bonusid}'
			";
		$res = $db->query($query); 
    }
	
    /*
	 * 取得訂單資料
	 * $oid			int			訂單編號
	 */
	public function check_order($oid="") {
        global $db, $config;
        
		$query = "SELECT *
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` 
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND orderid = '{$oid}'
				AND userid IS NOT NULL
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }	


	/*
	 * 鯊魚點兌換殺價幣 
	 *	$userid			int		會員編號
	 *	$amount			int		金額
	 *	$currency		int		幣別
	 */
	public function exchange_bonus($userid, $amount, $currency='')
	{
        global $db, $config;

		if($currency=='') {
		   $currency=$config['currency'];
		}
		
		$total = $amount*1.1;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$userid}', 
			countryid = '{$config['country']}', 
			behav = 'sajabonus', 
			currency = '{$currency}', 
			amount = '{$total}', 
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$db->query($query);
		$array['depositid'] = $db->_con->insert_id;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$userid}', 
			countryid = '{$config['country']}', 
			behav = 'sajabonus', 
			amount = '{$amount}', 
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$db->query($query);
		$array['spointid'] = $db->_con->insert_id;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history`
			SET 
			prefixid='{$config['default_prefix_id']}',
			userid='{$userid}',
			driid='0',
			depositid = '{$array['depositid']}', 
			spointid = '{$array['spointid']}', 
			status = 'sajabonus',
			insertid = '{$_SESSION['user']['profile']['userid']}',
			inserttype = 'User',
			insertname = '{$_SESSION['user']['profile']['nickname']}',
			insertt=NOW(),
			modifierid = '{$_SESSION['user']['profile']['userid']}',
			modifiertype = 'User',
			modifiername = '{$_SESSION['user']['profile']['nickname']}'
		";
		$res = $db->query($query);
		$array['dhid'] = $db->_con->insert_id;
		return $array;
	}	
	
    /*
	 * 取得商家資料
	 * $esid			int			商家編號
	 */
	public function get_store($esid="")	{
        global $db, $config;
        
		$query = "SELECT es.*, ep.enterpriseid
			FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` es
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` ep ON es.prefixid = ep.prefixid AND es.esid = ep.esid			
			WHERE
				es.prefixid = '{$config['default_prefix_id']}'
				AND es.esid = '{$esid}'
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }	

    /*
	 * 取得商家介紹人資料
	 * $enterpriseid			int			商家編號
	 */
	public function get_introducer($enterpriseid="") {
        global $db, $config;
        
		$query = "SELECT userid
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt`		
			WHERE enterpriseid = '{$enterpriseid}'
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
	
	/*
	 *	新增分潤分配點數
 	 *	$userid		int			會員編號
	 *	$amount		int			殺價幣數量
	 *	$behav		varchar		狀態
	 */
	function add_distribution_profits($userid, $amount, $behav, $esid=NULL) {
		global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$userid}', 
			countryid = '{$config['country']}', 
			behav = '{$behav}', 
			amount = '{$amount}', 
			seq = '0', 
			switch = 'Y',
			esid = '{$esid}', 
			insertt=NOW()
		";
		$db->query($query);
		$bsid = $db->_con->insert_id;		
		return $bsid;
	}	

	/*
	 *	各分類商品統計
 	 *	$layer		int			分類階層
	 */
	public function product_category_count($layer) {
		// global $db, $config;
		global $config;
		if(empty($layer)) {
           $layer=1; 
        }
		$db=new mysql($config["db2"]);
		$db->connect();
		
		$query ="SELECT epc.epcid, epc.name, epc.layer, COUNT(epcr.epid) as total 
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` epc
		left join `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category_rt` epcr 
			on epc.prefixid = epcr.prefixid AND epc.epcid = epcr.epcid
		left join `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` ep 
			on ep.prefixid = epcr.prefixid AND ep.epid = epcr.epid 			
		WHERE epc.`prefixid` = '{$config['default_prefix_id']}' 
			AND epc.`switch` = 'Y' 
			AND epcr.`switch` = 'Y'
			AND epcr.`switch` = 'Y'	
			AND ep.switch = 'Y'
			AND ep.epid > 300 
			AND ep.ontime <= NOW()
			AND ep.offtime > NOW()				
		";
		if(!empty($layer)) {
		   $query.=" AND epc.`layer` = ${layer} ";
		}
		$query.=" GROUP BY epc.epcid ORDER BY epc.seq ASC"; 
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
	}
		
	/*
	 * 銀行機構清單
	 */
	public function BankList() {
        // global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT bankid, bankname, bank_switch1, bank_switch2, bank_switch3, bank_switch4, bank_switch5			
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` 
			WHERE
				switch = 'Y'
				ORDER BY bankid ASC
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			$result = $table['table']['record']; 
			return $result;
		}
		return false;
    }

	/*
	 * 銀行機構清單
	 */
	public function BankData($bankid, $kind) {
        // global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT content{$kind} as content ";
		$query .= " FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}bank` 
			WHERE
				switch = 'Y' 
				AND bankid = {$bankid} 
			ORDER BY bankid ASC
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			$result = $table['table']['record'][0]; 
			return $result;
		}
		return false;
    }
	
	/*
	 * 貸款分類清單
	 */
	public function loan() {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT loanid, name 
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}loan` 
			WHERE
				switch = 'Y'
				ORDER BY loanid ASC
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			$result = $table['table']['record']; 
			return $result;
		}
		return false;
    }
	
	/*
	 * 電信公司清單
	 */
	public function telecompany() {
        // global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT toid, name 
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}telecom_op` 
			WHERE
				switch = 'Y'
				ORDER BY toid ASC
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			$result = $table['table']['record']; 
			return $result;
		}
		return false;
    }	

	/*
	 * 電信繳費分類清單
	 */
	public function telpaytype() {
        // global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT tpid, toid, name  
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}telpaytype` 
			WHERE
				switch = 'Y'
				ORDER BY tpid ASC
			";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			$result = $table['table']['record']; 
			return $result;
		}
		return false;
    }

	/*
	 * 生活繳費商品清單
	 */
	public function lifepay_product_list() {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->product_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT unix_timestamp(p.offtime) as offtime, c.epcid, pt.filename as thumbnail, 
						p.epid, p.name, p.thumbnail_url, p.tx_url, p.memo ";
		
		$query = "FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
		{$set_search['join_query']}
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}' 
			AND p.switch = 'Y'
			AND p.ontime <= NOW()
			AND p.offtime > NOW()
			AND p.epid < 300 
 			AND (p.use_type = '{$use_type}' OR p.use_type = '0')	
		"; 
		
		if(!empty($epcid)) {
			$query .= " AND c.epcid = ".$epcid." ";
		}
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq, p.insertt DESC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		
		if($num) {

			$table = $db->getQueryRecord($query_record . $query );
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
		
			return $table['table']['record'];
		}
		
		return false;
    }
	
	/*
	 * 兌換分類商品清單
	 */
	public function category_product_list($use_type, $epcid='') {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
        
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->product_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num from (";
		$query_record = "select * from ( ";
		$query = " SELECT unix_timestamp(p.offtime) as offtime, c.epcid, ptf.filename as thumbnail, 
						p.epid, p.name, p.thumbnail_url, p.tx_url, p.point_price, p.retail_price, p.description, p.seq, p.insertt FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` ptf ON 
			p.prefixid = ptf.prefixid
			AND p.epftid = ptf.epftid
			AND ptf.switch = 'Y'	
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}' 
			AND p.switch = 'Y'
			AND p.display = 'Y'
			AND p.ontime <= NOW()
			AND p.offtime > NOW()
			AND p.epid > 300 
			AND (SELECT SUM(num) as numtotal 
							FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}stock`  
							WHERE 
							`switch` = 'Y' 
							AND `epid` = p.epid) > 0 
			AND p.eptype = 0
 			AND (p.use_type = '{$use_type}' OR p.use_type = '0')	
		"; 
		
		if(!empty($epcid)) {
			$query .= " AND c.epcid in ( ".$epcid.") ";
		}
		
		$query .= $set_search['sub_search_query']; 
		
		$query .= " UNION ALL SELECT unix_timestamp(p.offtime) as offtime, c.epcid, ptf.filename as thumbnail, 
						p.epid, p.name, p.thumbnail_url, p.tx_url, p.point_price, p.retail_price, p.description, p.seq, p.insertt, p.memo 
					FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` p 
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` ptf ON 
			p.prefixid = ptf.prefixid
			AND p.epftid = ptf.epftid
			AND ptf.switch = 'Y'	
		WHERE 
			p.prefixid = '{$config['default_prefix_id']}' 
			AND p.switch = 'Y'
			AND p.display = 'Y'
			AND p.ontime <= NOW()
			AND p.offtime > NOW()
			AND p.epid > 300 
			AND (SELECT count(cid) total 
							FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card`  
							WHERE 
							`userid` = 0
							AND `orderid` = 0
							AND `used` = 'N'
							AND `switch` = 'Y' 
							AND `epid` = p.epid) > 0 
			AND (p.eptype = 2 OR p.eptype = 3)
 			AND (p.use_type = '{$use_type}' OR p.use_type = '0')	
		"; 
		
		if(!empty($epcid)) {
			$query .= " AND c.epcid in ( ".$epcid.") ";
		}
		
		$query .= $set_search['sub_search_query']; 		
		
		$query .= ($set_sort) ? $set_sort : " ) as t group by epid ORDER BY seq, insertt DESC";
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);
			$table = $db->getQueryRecord($query_record . $query );
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			//foreach($table['table']['record'] as $tk => $tv) { $productid = $tv['productid']; }
			foreach($table['table']['record'] as $tk => $tv) {
				$table['table']['record'][$tk]['point_price'] = round($tv['point_price']);
				$table['table']['record'][$tk]['retail_price'] = round($tv['retail_price']);
				$table['table']['record'][$tk]['description'] = str_replace(array("\r\n\t&nbsp;","\r","\n","\t","&nbsp;"), '', (trim(strip_tags($tv['description']))));

			}
			return $table;
		}
		
		return false;
    }

	/*
	* 分類資料
	*/
	public function get_category_data($layer, $epcid) {
		global $config;
		if(empty($layer)) {
           $layer=1; 
        }
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		$query ="SELECT epcid, name, node, layer, thumbnail   
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_category` 
		WHERE 	`prefixid` = '{$config['default_prefix_id']}' 
		AND `switch` = 'Y' 
		AND `display` = 'Y'
		";
		if(!empty($layer)) {
		   $query.=" AND `layer` = {$layer} ";
		}
		if(!empty($node)) {
		   $query.=" AND `node` = {$node} ";
		}		
		
		$query.=" ORDER BY seq "; 
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
	}


	public function get_sys($id='') {
		global $config;
		
		$id = (empty($id)) ? 1 : $id;

		$db=new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT id,name,description,thumbnail,used
				FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}sys` 
				WHERE prefixid = '{$config['default_prefix_id']}'
				AND id = '{$id}'
				AND switch = 'Y'
				AND used = 'Y'
				ORDER BY id asc
			";
		$table = $db->getQueryRecord($query); 
		error_log("[m/mall/get_sys]: ".$query."-->".count($table['table']['record']));	
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
		    return false;
		}
	}
	
	/*
	* 商品卡庫存
	*/
	public function get_exchange_card($epid) {
        // global $db, $config;
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
       
		$query ="SELECT COUNT(cid) num
		FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card` 
		WHERE 
			epid = '{$epid}'
			AND userid = '0'
			AND orderid = '0'
			AND used = 'N'
			AND switch = 'Y'
		";
		
		$recArr = $db->getQueryRecord($query); 
		
		if(!empty($recArr['table']['record'][0])) {
			$stock = ($recArr['table']['record'][0]['num']) ? (int)$recArr['table']['record'][0]['num'] : 0;
			
			return $stock;
		}
		return false;
    }

}
?>
