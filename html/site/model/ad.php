<?php
/**
 * Faq Model 模組
 */
class AdModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt'; 

    public function __construct(){
    }
    
	public function get_ad_list($kind=1, $promotetype='', $acid=0, $esid=0, $adid=0) {
		
		global $db, $config;		

		$query = " SELECT ad.adid, ad.name, ad.seq, ad.qrcode_url as action, 
						  ad.thumbnail_url, ad.thumbnail_url2, ad.thumbnail, ad.thumbnail2, 
						  ad.offtime , ad.action_memo, ad.esid
				from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad` ad
				LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad_category` ac ON ad.acid = ac.acid
				where 
				  ad.switch = 'Y' 
				  and ad.used = 'Y' 
				  and ac.switch = 'Y' 
				  and ac.used = 'Y' 
				  and NOW() between ad.ontime and ad.offtime ";

		if(!empty($kind)) {
			$query.=" and ac.place = '".$kind."'";
		}
		
		if(!empty($promotetype)) {
			$query.=" and ad.promotetype = '".$promotetype."' ";
		}
		
		if($acid>0) {
			$query.=" and ad.acid = '".$acid."' ";
		}
		
		if($esid>0) {
			$query.=" and ad.esid = '".$esid."' ";
		}
		
		if($adid>0) {
			$query.=" and ad.adid = '".$adid."' ";
		}

		$query.=" order by ad.seq asc";

        // error_log("[m/ad/get_ad_list] : ".$query);
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			foreach ($table['table']['record'] as $key => $value) {
				$action_list = array();
				if (!empty($value['action_memo'])) {
					$action_memo = json_decode($value['action_memo'],true);
					$action_list['type'] = $action_memo['type'];
					$action_list['url_title'] = $action_memo['url_title'];
					$action_list['url'] = $action_memo['url'];
					$action_list['imgurl'] = $action_memo['imgurl'];
					$action_list['page'] = $action_memo['page'];
					$action_list['productid'] = $action_memo['productid'];
					$action_list['epcid'] = $action_memo['epcid'];
					$action_list['layer'] = $action_memo['layer'];
					$action_list['epid'] = $action_memo['epid'];
					$action_list['action_type'] = $action_memo['action_type'];
				}
				$table['table']['record'][$key]['action_list'] = $action_list;
			}
			return $table['table']['record'];
		} else {
			return array();
		}
	}
	
	public function get_mallad_list($arrCond) {
		
		global $db, $config;		

		$query = " SELECT ad.adid, ad.name, ad.seq, ad.qrcode_url as action, 
						  ad.thumbnail_url, ad.thumbnail_url2, ad.thumbnail, ad.thumbnail2, 
						  ad.offtime , ad.action_memo, ad.esid
				from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad` ad
				LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad_category` ac 
				  ON ad.acid = ac.acid
				where 
				  ad.switch = 'Y' 
				  and ad.used = 'Y' 
				  and ac.switch = 'Y' 
				  and ac.used = 'Y' 
				  and NOW() between ad.ontime and ad.offtime ";	
		
		if($arrCond['acid']>0) {
			$query.=" and ad.acid = '".$arrCond['acid']."' ";
		}
		
		if($arrCond['adid']>0) {
			$query.=" and ad.adid = '".$arrCond['adid']."' ";
		}
		
		if($arrCond['esid']>0) {
			$query.=" and ad.esid = '".$arrCond['esid']."' ";
		}
		if($arrCond['epcid']>0) {
			$query.=" and ad.epcid = '".$arrCond['epcid']."' ";
		}
		$query.=" order by ad.seq asc";

        // error_log("[m/ad/get_mallad_list] : ".$query);
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			foreach ($table['table']['record'] as $key => $value) {
				$action_list = array();
				if (!empty($value['action_memo'])) {
					$action_memo = json_decode($value['action_memo'],true);
					$action_list['type'] = $action_memo['type'];
					$action_list['url_title'] = $action_memo['url_title'];
					$action_list['url'] = $action_memo['url'];
					$action_list['imgurl'] = $action_memo['imgurl'];
					$action_list['page'] = $action_memo['page'];
					$action_list['productid'] = $action_memo['productid'];
					$action_list['epcid'] = $action_memo['epcid'];
					$action_list['layer'] = $action_memo['layer'];
					$action_list['epid'] = $action_memo['epid'];
				}
				$table['table']['record'][$key]['action_list'] = $action_list;
			}
			return $table['table']['record'];
		} else {
			return array();
		}
	}
	
	
	//首頁彈窗專用
	public function get_ad_popups_list($kind=5, $promotetype='') {
		
		global $db, $config;
		

		$query = " SELECT ad.adid, ad.name, ad.seq, ad.qrcode_url as action, ad.thumbnail, ad.thumbnail2, ad.offtime , ad.action_memo
				from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad` ad
				LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad_category` ac ON ad.acid = ac.acid
				where 
				  ad.switch = 'Y' 
				  and ad.used = 'Y' 
				  and ac.switch = 'Y' 
				  and ac.used = 'Y' 
				  and NOW() between ad.ontime and ad.offtime ";

		if(!empty($kind)) {
			$query.=" and ac.place = '".$kind."'";
		}
		
		if(!empty($promotetype)) {
			$query.=" and ad.promotetype = '".$promotetype."'";
		}

		$query.=" order by ad.seq asc";

		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			foreach ($table['table']['record'] as $key => $value) {
				$action_list = array();
				if (!empty($value['action_memo'])) {
					$action_memo = json_decode($value['action_memo'],true);
					$action_list[0]['type'] = $action_memo['type'];
					$action_list[0]['url_title'] = urldecode($action_memo['url_title']);
					$action_list[0]['url'] = $action_memo['url'];
					$action_list[0]['imgurl'] = $action_memo['imgurl'];
					$action_list[0]['page'] = $action_memo['page'];
					$action_list[0]['productid'] = $action_memo['productid'];
					$action_list[0]['epcid'] = $action_memo['epcid'];
					$action_list[0]['layer'] = $action_memo['layer'];
					$action_list[0]['epid'] = $action_memo['epid'];
					$action_list[1]['type'] = $action_memo['type2'];
					$action_list[1]['url_title'] = urldecode($action_memo['url_title2']);
					$action_list[1]['url'] = $action_memo['url2'];
					$action_list[1]['imgurl'] = $action_memo['imgurl2'];
					$action_list[1]['page'] = $action_memo['page2'];
					$action_list[1]['productid'] = $action_memo['productid2'];
					$action_list[1]['epcid'] = $action_memo['epcid2'];
					$action_list[1]['layer'] = $action_memo['layer2'];
					$action_list[1]['epid'] = $action_memo['epid2'];
				}
				$table['table']['record'][$key]['action_list'] = $action_list;
			}
			return $table['table']['record'];
		} else {
			return array();
		}
	}
}
?>