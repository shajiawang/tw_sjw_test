<?php
/**
 * Exchangecard Model 模組
 */

class ExchangecardModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET) {
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc') {
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

	public function exchange_card_list($userid, $eccid='0', $page_number=1){
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//SQL指令
		$query_count = " SELECT count(*) as totalcount ";
		$query_record = "SELECT ec.cid, ec.orderid, ec.card_status, ep.name, eptf.filename as thumbnail ";

		$query = " FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card` ec 
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o ON 
			ec.orderid = o.orderid
			AND o.switch = 'Y'
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` ep ON 
			o.epid = ep.epid 
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` eptf ON 
			ep.epftid = eptf.epftid ";

		$query.= " WHERE
			ec.userid = '{$userid}'
			AND ec.card_status = '{$eccid}'
			AND ec.orderid <> '0'
			AND ec.used = 'Y'
			AND ec.switch = 'Y'";

		$query .= ($set_sort) ? $set_sort : " ORDER BY ec.cid ASC ";

		//總筆數
		$result = $db->getQueryRecord($query_count . $query); 
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		if(!empty($result['table']['record'][0]['totalcount'])){
			$totalcount = $result['table']['record'][0]['totalcount'];
		}else{
			$totalcount = 0;
		}

		$order_cnt =0;
		$order_table['table']['record'] =array();
		if ($eccid=='0') {
			// 狀態處理中 未發卡片的訂單
			// card_status=2 處理中
			$str_select = "	SELECT	
								'-1' as cid,
								o.orderid,
								'2' AS card_status,
								ep.`name`,
								eptf.filename AS thumbnail";
			$str_cnt = " SELECT count(*) as order_cnt";
			$query_order = "
							FROM
								`{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o
							LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` ep 
								ON o.epid = ep.epid
							LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` eptf 
								ON ep.epftid = eptf.epftid
							WHERE
								o.switch = 'Y'
							AND ep.eptype = 4
							AND `status` IN (0, 1, 2)
							AND o.userid = '{$userid}'
							ORDER BY o.insertt";
			$order_result = $db->getQueryRecord($str_cnt.$query_order);
			$order_cnt = $order_result['table']['record'][0]['order_cnt'];
			if ($order_cnt >= 1) {
				$order_table = $db->getQueryRecord($str_select.$query_order);
			}
		}	

		//檢查是有紀錄
		if( ((int)$totalcount+(int)$order_cnt) >= 1){

			//計算分頁開始筆數
			$rec_start = ($page_number-1) * $perpage + 1;

			//計算總共頁數
			$totalpages = ceil($totalcount/$perpage);

			//分頁參數輸出參數設定
			$page["rec_start"]       = $rec_start ;
			$page["totalcount"]      = $totalcount+$order_cnt ; //總共筆數
			$page["totalpages"]      = $totalpages;				//總共頁數
			$page["perpage"]      	 = $perpage ;				//每頁筆數
			$page["page"]       	 = $page_number ; 			//現在頁數

			//分頁索引頁碼產生
			$max_range = $config['max_range'];
			$ploops  = floor(($page_number-1)/$max_range)*$max_range + 1 ;
			$ploope  = $ploops + $max_range -1;
			for($i=$ploops;$i <= $ploope;$i++){
				$page["item"][]["p"] = $i ;
			}

			if ($totalcount>=1) {
				//分頁資料
				$query_limit = " LIMIT ". ($rec_start-1) .",". ($perpage);
				//取得資料
				$table = $db->getQueryRecord($query_record . $query . $query_limit);
			}
			
			$table['table']['page'] = $page;
			$table['table']['record'] = array_merge($order_table['table']['record'], $table['table']['record']);
			return $table;
		}else{
			$table['table']['record'] = '';
			return false;
		}
		return false;
	}

	public function exchange_card_info($userid, $cid=''){
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//SQL指令
		$query_record = "SELECT ec.cid, ec.code1, ec.codeformat1, ec.code2, ec.codeformat2, ec.orderid, ec.card_status, ec.modifyt as used_time, o.total_fee, o.type,
						o.insertt as buy_time, o.insertt as finish_time, ep.name, eptf.filename as thumbnail, ep.description ";

		$query = " FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card` ec 
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o ON 
			ec.orderid = o.orderid
			AND o.switch = 'Y'
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` ep ON 
			o.epid = ep.epid 
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` eptf ON 
			ep.epftid = eptf.epftid ";

		$query.= " WHERE
			ec.userid = '{$userid}'
			AND ec.cid = '{$cid}'
			AND ec.orderid <> '0'
			AND ec.used = 'Y'
			AND ec.switch = 'Y'";

		$query .= " LIMIT 1 ";

		$table = $db->getQueryRecord($query_record . $query);

		//檢查是有紀錄
		if(!empty($table['table']['record'])){

			// 將description 做html_decode
			foreach($table['table']['record'] as $k=>$v) {
			    $table['table']['record'][$k]['description']=html_decode($v['description']); 	
			}
			
						for($i = 1; $i <= 4; $i ++){
			if(!empty($table['table']['record'][0]['code'.$i]) && $table['table']['record'][0]['code'.$i] != '0'){
					//barcodeformat 已array格式輸出
					$table['table']['record'][0]['barcodeformat'][] = array(
						'code' => $table['table']['record'][0]['code'.$i] , 
						'type' => $table['table']['record'][0]['codeformat'.$i]);
				}
			}
			//條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
			//$barcodeformat_type = '1';

			//barcodeformat 已array格式輸出
			// $table['table']['record'][0]['barcodeformat'] = array(
			// 	array('code' => $table['table']['record'][0]['code1'] , 'type' => $table['table']['record'][0]['codeformat1']),
			// 	array('code' => $table['table']['record'][0]['code2'] , 'type' => $table['table']['record'][0]['codeformat2'])
			// );
			// unset($table['table']['record'][0]['codeformat1']);
			// unset($table['table']['record'][0]['codeformat2']);
			return $table;

		}else{
			$table['table']['record'] = '';
			return false;
		}
			
		return false;
	}

	public function exchange_card_info_orderid($userid, $orderid=''){
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//SQL指令
		$query_record = "SELECT
							'-1' as cid,
							'' as code1,
							'' as codeformat1,
							'' as code2,
							'' as codeformat2,
							'2' as card_status,
							'' as used_time,
							o.orderid,
							o.total_fee,
							o.insertt AS buy_time,
							o.insertt AS finish_time,
							o.type,
							ep.name,
							eptf.filename AS thumbnail ";

		$query = " FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o 
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` ep ON 
			o.epid = ep.epid 
		LEFT JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product_thumbnail_file` eptf ON 
			ep.epftid = eptf.epftid ";

		$query.= " WHERE
			o.userid = '{$userid}'
			AND o.orderid = '{$orderid}'
			AND o.switch = 'Y'";

		$query .= " LIMIT 1 ";

		$table = $db->getQueryRecord($query_record . $query);

		//檢查是有紀錄
		if(!empty($table['table']['record'])){
			return $table;
		}else{
			$table['table']['record'] = '';
			return false;
		}
			
		return false;
	}
	
	public function update_exchange_card($userid, $cid=''){

		global $db, $config;

		if(empty($userid)){
			return false;
		}

		if(empty($cid)){
			return false;
		}

		$query = "UPDATE `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_card` SET card_status = 1
				WHERE userid = '{$userid}' AND cid = '{$cid}' ";

		if($db->query($query)){
			return true;
		}else{
			return false;
		}
  }
}
?>
