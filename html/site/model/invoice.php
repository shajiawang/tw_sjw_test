<?php
/**
 * Invoice Model 模組
 */
class InvoiceModel {
    public $id;
    public $email;
    public $ok;
    public $msg;

    public function __construct()
	{
        global $db;
        
        $this->id = isset($_SESSION['auth_id']) ? $_SESSION['auth_id'] : 0;
        $this->email = isset($_SESSION['auth_email']) ? $_SESSION['auth_email'] : '';
        $this->ok = false;

        return $this->ok;
    }

	/**
	 * 查询：取得储存资料
	 * @param int $userid    用户id
	 * @param int $depositid    未開發票的單號
	 * @return array
	 */
    public function getDepositForInvoice($depositid = '',$userid = '')
    {
        global $db, $config;
        
        if(empty($depositid) && empty($userid)) {
           return false;   
        }
        
        //SQL指令
        $query = " SELECT depositid, invoiceno, modifyt, switch 
        FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
        WHERE prefixid ='{$config['default_prefix_id']}' ";
        if(!empty($userid)) {
	       $query.= " AND userid ='{$userid}' ";
		}   
        if(!empty($depositid)) {
           $query.= " AND depositid ='{$depositid}' ";
        }
		//取得資料
		$table = $db->getQueryRecord($query);
        
		error_log("[m/deposit/getInvoice]:".$query);
        
	    if(!empty($table['table']['record'][0])) {
	        return $table['table']['record'][0];
	    } else {
	        return false;
	    }
    }
	
    /**
     * 查询：取得單筆發票字軌資料
     * @param date $year
     * @param date $date
     * @return array
     */
    public function getInvoiceRail($yyyy = '',$mm = '', $switch='Y') {
        global $db, $config;
       
        $query = " SELECT railid, rail_prefix, seq_start, used, total
        FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
        WHERE prefixid ='{$config['default_prefix_id']}'
        AND switch = '{$switch}'
        AND year = '{$yyyy}'
        AND (start_month = '{$mm}' OR end_month = '{$mm}' )
        ORDER BY railid ASC LIMIT 0,1 ";
        
        //取得資料
        $table = $db->getQueryRecord($query);
        
	    if(!empty($table['table']['record'][0])) {
    	    return $table['table']['record'][0];
        } else {
            return false;
        }
    }
	
     /**
     * 更新發票資料
     * @param int $userid
     * @param int $depositid
     * @param string $invoiceno
     * @param string $random_code
     * @param int $railid	 
     * @return array
     */
    public function updateInvoice($arrUpd, $arrCond) {
        global $db, $config;
		
        if(empty($arrCond) || count($arrCond)<1) {
           return false;   
        }
        if(empty($arrUpd) || count($arrUpd)<1) {
           return false;   
        }
        $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` SET ";
        foreach($arrUpd as $k=>$v) {
            $query.= " ".$k."='".$v."',";      
        }
        $query = substr($query,0,-1);
        $query.=" WHERE prefixid='saja' ";
        
        // 如$arrCond內有 invoicenos項  先不處理
        foreach($arrCond as $k=>$v) {
            if($k=="invoicenos") {
			   continue;	
			}
			$query.= " AND ".$k."='".$v."' ";      
        }
		
		// 處理 invoicenos項
		// $arrCond['invoicenos'] 為 Array of invoiceno
		if(is_array($arrCond['invoicenos'])) {
			$query.= " AND invoiceno in (";
			foreach($arrCond['invoicenos'] as $v) {
               $query.= "'".$v."',";   
            }
            $query = substr($query,0,-1);			
			$query.=" ) ";   
		}
        $db->query($query);
		$ret =  $db->_con->affected_rows;
        error_log("[m/invoice/updateInvoice] : ".$query."-->".$ret);
        return $ret;
    }
	
	
    /**
     * 修改發票字軌資料
     * @param bigint $railid
     * @param string $rail_prefix
     * @param int $contrast
     * @return array
     */
    public function updateInvoiceRail($railid = '',$rail_prefix = '',$switch = '') {
        global $db, $config;
        
        if (empty($switch)) {
            $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
            SET used = used + 1 
            WHERE rail_prefix ='{$rail_prefix}'
            AND railid='{$railid}' ";
        } else {
            $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
            SET switch = '{$switch}'
            WHERE rail_prefix ='{$rail_prefix}'
            AND railid='{$railid}' ";
        }
		return $db->query($query);
    }

	/*
	 *	取得需上傳發票的清單
	 *	$date				varchar					期別 ex:20180708
	 */
	public function getInvoiceListForUpload($idate) {
        global $db, $config;

		$InvoiceYear = substr($idate,0 ,4); // 發票年份
		$InvoiceMonth = substr($idate,4 ,2); // 
		$InvoiceMonth2 = substr($idate,6 ,2); // 結束月
		
        $query = "SELECT *
                    FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` iv
                    JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_proditems` item                    
                      ON iv.siid=item.siid
                   WHERE iv.switch='Y' 
                     AND item.switch='Y'
                     AND iv.upload='N' 
                     AND DATE_FORMAT(iv.invoice_datetime,'%Y%m') IN ('".$InvoiceYear.$InvoiceMonth."','".$InvoiceYear.$InvoiceMonth2."') 
                ORDER BY iv.invoice_datetime ASC ";
        
        //取得資料
		$table = $db->getQueryRecord($query);		
		error_log("[m/invoice/getInvoiceListForUpload]:".$query);
		
		/*
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		*/
		if(count($table['table']['record']) > 0){
			return $table['table']['record'];
		}
		return false;
    }	
	
	/*
	 *	由發票號碼取得單筆發票資料
	 *	$invoiceno				varchar					發票編號
	 */
	public function getInvoiceByNo($invoiceno) {
        global $db, $config;
		
		$query = " SELECT * 
		             FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice`
		            WHERE prefixid = '{$config['default_prefix_id']}' 
			          AND switch = 'Y' 
			          AND invoiceno = '${invoiceno}' ";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		error_log("[m/invoice/getInvoiceByNo] query :".$query);
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
	
	/*
	 *	由發票號碼取得單筆發票資料
	 *	$invoiceno				varchar					發票編號
	 *	$type					varchar					查詢類型 (upload : 已申報)
	 */
	public function getInvoiceDetailByNo($invoiceno,$type='') {
        global $db, $config;
		
		$query = " SELECT i.*, it.sipid, it.description, it.quantity, it.unitprice, it.amount, it.seq_no  
		             FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` i
					 JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_proditems` it
		               ON i.siid=it.siid
					WHERE i.prefixid = '{$config['default_prefix_id']}' 
			          AND i.switch = 'Y'
                      AND it.switch='Y'					  
			          AND i.invoiceno = '{$invoiceno}' ";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		error_log("[m/invoice/getInvoiceDetailByNo] query :".$query);
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
	

	/*
	 *	取得該月份可用字軌
	 *	$year				varchar				年分
	 *	$month				varchar				月份 (左補零)
	 */	
    public function getInvoiceRailList($yyyy,$mm)
    {
        global $db, $config;
        
        $query = " SELECT railid, rail_prefix, seq_start, used, total
					FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
					WHERE prefixid ='{$config['default_prefix_id']}'
					AND switch = 'Y'
					AND year = '{$yyyy}'
					AND (start_month = '{$mm}' OR end_month = '{$mm}')
					ORDER BY railid ASC ";
        
        //取得資料
        $table = $db->getQueryRecord($query);
        
	    if(!empty($table['table']['record'])) {
    	    return $table['table']['record'];
        } else {
            return false;
        }
    }
	
	/*
	 *	由PK取得發票字軌資料
	 *	$railid					int		PK
	 */	
    public function getInvoiceRailById($railid = '') {
        global $db, $config;
		if(empty($railid)) {
			return false;
		}   
        $query = " SELECT railid, rail_prefix, year, start_month, end_month, seq_start, used, total 
        FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
        WHERE prefixid ='{$config['default_prefix_id']}' 
        AND railid = '{$railid}' ";
        
		error_log("[m/invoice/getInvoiceRailById] query :".$query);
		//取得資料
        $table = $db->getQueryRecord($query);
        
	    if(!empty($table['table']['record'])) {
    	    return $table['table']['record'][0];
        } else {
            return false;
        }
    }
	
    // 新增發票單頭資料
	public function addInvoiceData($arrNewInvoice) {
		global $db, $config;
        $query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice`
			SET 
			prefixid='{$config['default_prefix_id']}',
			invoiceno='{$arrNewInvoice['invoiceno']}',
            userid='{$arrNewInvoice['userid']}',
			invoice_datetime='{$arrNewInvoice['invoice_datetime']}',
			buyer_name='{$arrNewInvoice['buyer_name']}',
            donate_mark='{$arrNewInvoice['donate_mark']}',
            npoban='{$arrNewInvoice['npoban']}',
            carrier_type='{$arrNewInvoice['carrier_type']}',
            carrier_id1='{$arrNewInvoice['carrier_id1']}',
            carrier_id2='{$arrNewInvoice['carrier_id2']}',
			random_number='{$arrNewInvoice['random_number']}',
			sales_amt={$arrNewInvoice['sales_amt']},
			tax_amt={$arrNewInvoice['tax_amt']},
            upload='{$arrNewInvoice['upload']}',
			total_amt={$arrNewInvoice['total_amt']},
			railid='{$arrNewInvoice['railid']}',
            utype='{$arrNewInvoice['utype']}',
            is_winprize='{$arrNewInvoice['is_winprize']}',
			print_mark='{$arrNewInvoice['print_mark']}',
			buyer_id='{$arrNewInvoice['buyer_id']}',
			seller_id='{$arrNewInvoice['seller_id']}',
			insertt=NOW()
		";
		$db->query($query);
		$siid = $db->_con->insert_id;
		return $siid;
    }
	
	// 新增發票單身(品項明細)資料
	public function addInvoiceProdItem($arrNewInvoice) {
		global $db, $config;
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_proditems`
			SET 
			seq_no={$arrNewInvoice['seq_no']},
			siid='{$arrNewInvoice['siid']}',
			unitprice={$arrNewInvoice['unitprice']},
			quantity={$arrNewInvoice['quantity']},
			amount={$arrNewInvoice['amount']},
			description='{$arrNewInvoice['description']}',
			insertt=NOW()
		    ";
		$db->query($query);
		$sipid = $db->_con->insert_id;
		return $sipid;
    }
    
    // 折讓相關單元功能
    // 取得某張發票的已折讓金額
    public function getAllowanceAmt($invoiceno='') {
		if(empty($invoiceno)) {
		   return false;
		}  
		global $db, $config;
		$query = "SELECT SUM(IFNULL(amount,0)) as total
				   FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items` 
				  WHERE ori_invoiceno='{$invoiceno}' 
					AND switch='Y' ";

		//取得資料
		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0]['total'];
		} else {
			return 0;
		}
    }
    
    // 計算並更新發票已折讓金額
    public function updAllowanceAmt($invoiceno) {
		if(empty($invoiceno))
		   return false;
		global $db, $config;
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice 
					SET allowance_amt=( SELECT SUM(IFNULL(amount,0)) as total
						   FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items` 
						  WHERE prefixid ='{$config['default_prefix_id']}' 
							AND ori_invoiceno='{$invoiceno}' 
							AND switch='Y' ) 
				 WHERE prefixid ='{$config['default_prefix_id']}' 
				   AND invoiceno='{$invoiceno}' ";         
		//取得資料
		$ret = $db->query($query);
		error_log("[m/invoice/updAllowanceAmt] ".$query."-->".$ret);
		return $ret;
    }
	
    // 取得某用戶所有發票資料, 依時間先後排序
    public function getInvoiceList($arrCond, $arrOrderBy='') {
		if(empty($userid))
		   return false;
		global $db, $config;  
		// 透過depository_history取得發票編號
		$query = " SELECT * from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice`
				   WHERE prefixid ='{$config['default_prefix_id']}' ";
		// Where Condition
		if(!empty($arrCond['switch'])) {
		   $query.=" AND switch='${arrCond['switch']}' ";
		}
		if(!empty($arrCond['userid'])) {
		   $query.=" AND userid='${arrCond['userid']}' ";    
		}
		if(!empty($arrCond['invoiceno'])) {
		   $query.=" AND invoiceno='${arrCond['invoiceno']}' ";    
		}
		if(!empty($arrCond['railid'])) {
		   $query.=" AND railid='${arrCond['railid']}' ";    
		}
		if(!empty($arrCond['upload'])) {
		   $query.=" AND upload='${arrCond['upload']}' ";    
		}
		if(!empty($arrCond['is_winprize'])) {
		   $query.=" AND is_winprize='${arrCond['is_winprize']}' ";    
		}
		if(!empty($arrCond['utype'])) {
		   $query.=" AND utype='${arrCond['utype']}' ";    
		}

		// order by
		if(empty($arrOrderBy)) {
		  $query.=" ORDER BY invoice_datetime asc ";
		} else {
		  // ToDo    
		}
		//取得資料
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}
    }
    
    // 取得用戶折讓中發票
    public function getInvoiceListInAllowance($userid, $utype='') {
		if(empty($userid))
		   return false;
		global $db, $config;  
		// 在allowance_items資料中編號有出現的發票, 並以可折讓金額小到大, 日期舊到新排列
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` 
				  WHERE prefixid ='{$config['default_prefix_id']}' 
				   AND userid = '{$userid}'
                   AND is_void = 'N'
				   AND utype = 'S'
				   AND upload!='N' 
                   AND is_winprize='N'				   
				   AND invoiceno IN (
						  SELECT ai.ori_invoiceno  
							FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance` a
							JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items` ai
							  ON a.salid = ai.salid
						   WHERE 1=1
							 AND a.switch='Y' 
							 AND ai.switch='Y'
							 AND a.userid='{$userid}'
				   )
				AND allowance_amt>0 ";
		if(!empty($utype)) {
		   $query.= " AND utype='{$utype}' ";
		}	
		$query.=" ORDER BY (allowance_amt-sales_amt) asc, invoice_datetime asc,  siid asc ";

		//取得資料
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}
    }
    
    // 取得用戶未折讓過的發票
    public function getInvoiceListNotInAllowance($userid, $utype='') {
		if(empty($userid)) {
		   return false;
		}   
		global $db, $config;
		 // 在allowance_items資料中編號沒有出現的發票, 並以日期舊到新, 及可折讓金額小到大排列
		$query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` 
					WHERE prefixid ='{$config['default_prefix_id']}' 
					  AND userid = '{$userid}'
                      AND is_void = 'N'
				      AND utype = 'S'
				      AND upload!='N'
                      AND is_winprize='N'						  
					  AND invoiceno NOT IN 
					 (
						 SELECT ai.ori_invoiceno  
							FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance` a
							JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items` ai
							  ON a.salid = ai.salid
						   WHERE 1=1
							 AND a.switch='Y' 
							 AND ai.switch='Y'
							 AND ai.ori_invoiceno!=''
							 AND a.userid='{$userid}'
					  ) ";

		if(!empty($utype)) {
		   $query.= " AND utype='${utype}' ";
		}
		$query.=" ORDER BY invoice_datetime ASC, (allowance_amt-sales_amt) asc, siid asc ";         
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}
    }
    
    // 新增折讓單主檔資料
    public function createAllowance($arrNew='') {
        if(empty($arrNew) || !is_array($arrNew)){
            return false;
        }
		global $db, $config;
        $allowance_no=time();
		
        $query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance`
			SET 
			userid='{$arrNew['userid']}',
            allowance_no='${allowance_no}',
			allowance_date='{$arrNew['allowance_date']}',
			invoice_buyer_id='{$arrNew['invoice_buyer_id']}',
            invoice_buyer_name='{$arrNew['invoice_buyer_name']}',
            allowance_amt=${arrNew['allowance_amt']},
            allowance_tax_amt=${arrNew['allowance_tax_amt']},
            allowance_type='${arrNew['allowance_type']}',
            insertt=NOW()
		";
		$db->query($query);
		$salid = $db->_con->insert_id;
		return $salid;
    }
    
    // 新增折讓單明細資料
    public function createAllowanceItem($arrNew='') {
		if(empty($arrNew) || !is_array($arrNew))
			return false;
		global $db, $config;
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items`
			SET 
			salid='{$arrNew['salid']}',
            seq_no='{$arrNew['seq_no']}',
			ori_invoice_date='{$arrNew['ori_invoice_date']}',
			ori_invoiceno='{$arrNew['ori_invoiceno']}',
            unitprice={$arrNew['unitprice']},
            quantity=${arrNew['quantity']},
            amount=${arrNew['amount']},
            tax=${arrNew['tax']},
            tax_type='${arrNew['tax_type']}',
            insertt=NOW()
		";
		$db->query($query);
		$saliid = $db->_con->insert_id;
		return $saliid;
    }
    
    // 取得折讓單主檔資料
    public function getAllowanceList($arrCond) {
		if(empty($arrCond))
		  return false;
		global $db, $config;   
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance` WHERE 1=1 "; 
		if(!empty($arrCond['salid'])) {
			$query.=" AND salid='".$arrCond['salid']."' ";
		}	
		if(!empty($arrCond['allowance_no'])) {
			$query.=" AND allowance_no='".$arrCond['allowance_no']."' ";
		}		
		if(!empty($arrCond['userid'])) {
			$query.=" AND userid='".$arrCond['userid']."' ";
		}
		/*
		if(!empty($arrCond['allowance_no'])) {
			$query.=" AND allowance_no='".$arrCond['allowance_no']."' ";
		}
		*/
		if(!empty($arrCond['allowance_date'])) {
			$query.=" AND allowance_date='".$arrCond['allowance_date']."' ";
		}
		if(!empty($arrCond['allowance_type'])) {
			$query.=" AND allowance_type='".$arrCond['allowance_type']."' ";
		}
		if(!empty($arrCond['upload'])) {
			$query.=" AND upload='".$arrCond['upload']."' ";
		}
		if(!empty($arrCond['switch'])) {
			$query.=" AND switch='".$arrCond['switch']."' ";
		}
		if(!empty($arrCond['_ORDERBY'])) {
			$query.=" ORDER BY ".$arrCond['_ORDERBY'];
		}
		if(!empty($arrCond['_LIMIT'])) { 
			$query.=" LIMIT ".$arrCond['_LIMIT'];
		}
		error_log("[model/invoice/getAllowanceList] ".$query);
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
    }
    
    // 取得折讓單明細檔資料
    public function getAllowanceItemList($arrCond) {
		if(empty($arrCond))
			return false;
		global $db, $config;   
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance_items` WHERE 1=1 "; 
		
		if(!empty($arrCond['salid'])) {
			$query.=" AND salid='".$arrCond['salid']."' ";
		}
		if(!empty($arrCond['saliid'])) {
			$query.=" AND saliid='".$arrCond['saliid']."' ";
		}
		if(!empty($arrCond['seq_no'])) {
			$query.=" AND seq_no='".$arrCond['seq_no']."' "; 
		}
		if(!empty($arrCond['ori_invoiceno'])) {
			$query.=" AND ori_invoiceno='".$arrCond['ori_invoiceno']."' ";
		}
		if(!empty($arrCond['ori_invoice_date'])) {
			$query.=" AND ori_invoice_date='".$arrCond['ori_invoice_date']."' ";
		}
		if(!empty($arrCond['tax_type'])) {
			$query.=" AND tax_type='".$arrCond['tax_type']."' ";
		}
		if(!empty($arrCond['switch'])) {
			$query.=" AND switch='".$arrCond['switch']."' ";
		}
		if(!empty($arrCond['_ORDERBY'])) {
			$query.=" ORDER BY ".$arrCond['_ORDERBY'];
		}	
		if(!empty($arrCond['_LIMIT'])) {
			$query.=" LIMIT ".$arrCond['_LIMIT'];
		}
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
    }
    
    // 產生發票號碼
    // 傳入 : date 物件
    // 回傳 : 發票號碼和所使用的字軌資料
    public function genInvoiceNo($inv_date='') {
		global $db, $config;
		
		$inv_date = strtotime($inv_date);
		$yyyy=date('Y');
		$mm=date('m');
		if(!empty($inv_date)) {
			$yyyy=date('Y',$inv_date);
			$mm=date('m',$inv_date);
		}

		$track = $this->getInvoiceRail($yyyy,$mm,'Y');
		if(is_array($track) && $track['rail_prefix']) {
			$track['invoiceno'] = $track['rail_prefix'] . str_pad(($track['seq_start'] + $track['used']),8,'0',STR_PAD_LEFT);           
			error_log("[m/invoice/genInvoiceNo] ret : ".json_encode($track));
			return $track;        
		} else {
			error_log("[m/invoice/genInvoiceNo] NO AVAILABLE INVOICE RAIL DATA !!");
			return false;
		}
    }
    
    // 產生預設發票資料data array
    public function genDefaultInvoiceArr($buyer_name='', $total_amt='', $sales_amt='', $utype='S', $tax_rate=0.05, $invoice_datetime='') {
		global $db, $config;
		if(empty($buyer_name)) {
			return false;
		}
		if(empty($total_amt) && empty($sales_amt)) {
			return false;
		}
		
		$arrInvoice = array();
		$arrInvoice['tax_rate'] = $tax_rate;
		$arrInvoice['random_number'] = substr(rand(1000,9999),0,4) ;
		$arrInvoice['buyer_name'] = $buyer_name;
		
		if(!empty($total_amt) && !empty($sales_amt)) {
		    $arrInvoice['total_amt'] = $total_amt;
			$arrInvoice['sales_amt'] = $sales_amt;
			$arrInvoice['tax_amt'] = $total_amt-$sales_amt;
		} else if(!empty($total_amt) && empty($sales_amt)) {     
			$arrInvoice['total_amt'] = $total_amt;
			$arrInvoice['sales_amt'] = round($total_amt/(1.0+$arrInvoice['tax_rate']));        
			$arrInvoice['tax_amt'] = $total_amt-$arrInvoice['sales_amt'];
		} else if(empty($total_amt) && !empty($sales_amt)) {
			$arrInvoice['sales_amt'] = $sales_amt;
			$arrInvoice['tax_amt'] = round($sales_amt* $arrInvoice['tax_rate']);
			$arrInvoice['total_amt'] = $sales_amt+$arrInvoice['tax_amt'];
		}   
		
		if (empty($invoice_datetime)){
			$invoice_time = date('Y-m-d H:i:s',time());
		}else{ 
			$invoice_time = $invoice_datetime;
		}
		
		$arrInvoice['seller_id']="25089889";
		$arrInvoice['seller_name']="殺價王股份有限公司";
		$arrInvoice['seller_addr']="台北市";
		$arrInvoice['seller_telno']="0225501058";
		$arrInvoice['buyer_id']="00000000";
		$arrInvoice['invoice_type']="07";
		$arrInvoice['donate_mark']="0";
		$arrInvoice['print_mark']="Y";
		$arrInvoice['nopban']="";
		$arrInvoice['upload']="N";
		$arrInvoice['utype']=$utype;
		$arrInvoice['is_winprize']="N";
		$arrInvoice['tax_type']="1";
		$arrInvoice['carrier_type']="";
		$arrInvoice['carrier_id1']="";
		$arrInvoice['carrier_id2']="";
		$arrInvoice['invoice_datetime']=$invoice_time;

		error_log("[m/invoice/genDefaultInvoiceArr] invoice data : ".json_encode($arrInvoice));
		return ($arrInvoice);
    }   

    // 修改折讓單狀態
    public function updateAllowance($arrUpd, $arrCond) {
		global $db, $config;

		if(empty($arrUpd) || !is_array($arrUpd) || count($arrUpd)<=0) {
			return false;   
		}
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}allowance` ";
		$query.= " SET modifyt=NOW() ";
		if(!empty($arrUpd['userid'])) {
			$query.= " ,userid='{$arrUpd["userid"]}' ";        
		}
		if(!empty($arrUpd['allowance_no'])) {
			$query.= " ,allowance_no='{$arrUpd["allowanceno"]}' ";        
		}
		if(!empty($arrUpd['invoice_buyer_id'])) {
			$query.= " ,invoice_buyer_id='{$arrUpd["invoice_buyer_id"]}' ";        
		}
		if(!empty($arrUpd['invoice_buyer_name'])) {
			$query.= " ,invoice_buyer_name='{$arrUpd["invoice_buyer_name"]}' ";        
		}
		if(!empty($arrUpd['allowance_amt'])) {
			$query.= " ,allowance_amt='{$arrUpd["allowance_amt"]}' ";        
		}
		if(!empty($arrUpd['allowance_tax_amt'])) {
			$query.= " ,allowance_tax_amt='{$arrUpd["allowance_tax_amt"]}' ";        
		}
		if(!empty($arrUpd['allowance_type'])) {
			$query.= " ,allowance_type='{$arrUpd["allowance_type"]}' ";        
		}
		if(!empty($arrUpd['upload'])) {
			$query.= " ,upload='{$arrUpd["upload"]}' ";        
		}
		if(!empty($arrUpd['switch'])) {
			$query.= " ,switch='{$arrUpd["switch"]}' ";        
		}
		if(!empty($arrUpd['allowance_for'])) {
			$query.= " ,allowance_for='{$arrUpd["allowance_for"]}' ";        
		}
		if(!empty($arrUpd['objid'])) {
			$query.= " ,objid='{$arrUpd["objid"]}' ";        
		}
		if(!empty($arrUpd['memo'])) {
			$query.= " ,memo='{$arrUpd["memo"]}' ";        
		}
		$query.=" WHERE 1=1 ";
		if(!empty($arrCond['salid'])) {
			$query.= " AND salid='{$arrCond["salid"]}' ";        
		}
		if(!empty($arrCond['allowance_no'])) {
			$query.= " AND allowance_no='{$arrCond["allowance_no"]}' ";        
		}
		if(!empty($arrCond['userid'])) {
			$query.= " AND userid='{$arrCond["userid"]}' ";        
		}
		if(!empty($arrCond['invoice_buyer_id'])) {
			$query.= " AND invoice_buyer_id='{$arrCond["invoice_buyer_id"]}' ";        
		}
		if(!empty($arrCond['invoice_buyer_name'])) {
			$query.= " AND invoice_buyer_name='{$arrCond["invoice_buyer_name"]}' ";        
		}
		if(!empty($arrCond['upload'])) {
			$query.= " AND upload='{$arrCond["upload"]}' ";        
		}
		if(!empty($arrCond['switch'])) {
			$query.= " AND switch='{$arrCond["switch"]}' ";        
		}
		// 新增 allowance_nos 提供批次處理用
		if(is_array($arrCond['allowancenos'])) {
		   $query.= " AND allowance_no in (";
		   foreach($arrCond['allowancenos'] as $v) {
				   $query.= "'".$v."',";   
		   }
		   $query = substr($query,0,-1);			
		   $query.=" ) ";   
		}  
		// return $db->query($query)?1:0;
		$db->query($query);
		$ret =  $db->_con->affected_rows;
        error_log("[m/invoice/updateAllowance] : ".$query."-->".$ret);
        return $ret;
    } 
	
	/*
	 *	取得已上傳發票的清單
	 *	$date				varchar					期別 ex:20180708
	 */
	public function getInvoiceListForComplete($idate) {
        global $db, $config;

		$InvoiceYear = substr($idate,0 ,4); // 發票年份
		$InvoiceMonth = substr($idate,4 ,2); // 
		$InvoiceMonth2 = substr($idate,6 ,2); // 結束月
		
        $query = "SELECT *
                    FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` iv
                    JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_proditems` item                    
                      ON iv.siid=item.siid
                   WHERE iv.switch='Y' 
                     AND item.switch='Y'
                     AND iv.upload='P' 
                     AND DATE_FORMAT(iv.invoice_datetime,'%Y%m') IN ('".$InvoiceYear.$InvoiceMonth."','".$InvoiceYear.$InvoiceMonth2."') 
                ORDER BY iv.invoice_datetime ASC ";
        
        //取得資料
		$table = $db->getQueryRecord($query);		
		error_log("[m/invoice/getInvoiceListForComplete]:".$query);
		
		/*
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		*/
		if(count($table['table']['record']) > 0){
			return $table['table']['record'];
		}
		return false;
	}
	
	public function updateInvoiceComplete($siid){

		global $db, $config;

		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice`
					SET upload = 'Y'
					WHERE siid ='{$siid}'
					AND upload = 'P'
					AND switch = 'Y' ";         
		//取得資料
		$ret = $db->query($query);
		return $ret;	
	}
	
	/*
	public function createInvoicePrize($arrNew) {
		
	       global $db, $config;
		   
		   $query = " INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_prize` SET ";
							       
		   foreach($arrNew as $k=>$v) {
			    // error_log($k);
				$query.= " ${k}='${v}',";				
		        
		   }
		   $query=substr($query,0,-1);
		   error_log("[m/invoice/createInvoicePrize] SQL : ".$query);
		   $ret = $db->query($query);
		   return $ret;	
	}
	
	public function getInvoicePrize($arrCond) {
		
	       global $db, $config;
		   
		   $query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_prize` WHERE 1=1 ";
							       
		   foreach($arrCond as $k=>$v) {
			    $query.= " AND ${k}='${v}' ";				
		   }
		   error_log("[m/invoice/getInvoicePrize] SQL : ".$query);
		   $table = $db->getQueryRecord($query);		
		   if(count($table['table']['record']) > 0){
			  return $table['table']['record'];
		   }
		   return false;
	}
	*/
	
	public function createInvoicePrizeno($arrNew) {
		
	       global $db, $config;
		   
		   $query = " INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_prizeno` SET ";
							       
		   foreach($arrNew as $k=>$v) {
				$query.= " ${k}='${v}',";						        
		   }
		   $query=substr($query,0,-1);
		   // 如有unique key 相同的, 僅修改modify 時間
		   $query.=" ON DUPLICATE KEY UPDATE modifyt=NOW() ";
		   error_log("[m/invoice/createInvoicePrizeno] SQL : ".$query);
		   $ret = $db->query($query);
		   return $ret;	
	}
	
	public function getInvoicePrizeno($arrCond) {
		
	       global $db, $config;
		   
		   $query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_prizeno` WHERE 1=1 ";
							       
		   foreach($arrCond as $k=>$v) {
			    $query.= " AND ${k}='${v}' ";				
		   }
		   error_log("[m/invoice/getInvoicePrizeno] SQL : ".$query);
		   $table = $db->getQueryRecord($query);		
		   if(count($table['table']['record']) > 0){
			  return $table['table']['record'];
		   }
		   return false;
	}
	
	// 取得發票的超級特獎/特獎/頭獎 (8位數字皆符合)
	public function getPrizeOfExactlyDigit($yyyy,$mm, $digit=8) {
		
	       global $db, $config;

           $query = "SELECT i.siid,i.invoiceno, i.userid, i.invoice_datetime, p.invoYm, p.year, p.start_month, p.end_month, p.prize_desc, p.prize_no
					   FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` i
					   JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_prizeno` p
						 ON RIGHT(i.invoiceno,${digit})=p.prize_no
						AND LENGTH(p.prize_no)=${digit}
						AND p.switch='Y'
						AND p.year='${yyyy}' 
						AND (p.start_month='${mm}' OR p.end_month='${mm}')
					  WHERE i.switch='Y' 
						AND i.is_void='N' 
						AND LEFT(i.invoiceno,2) IN (
						    SELECT DISTINCT(rail_prefix)
						      FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
						     WHERE year='${yyyy}'
						       AND (start_month='${mm}' OR end_month='${mm}')
						) ";		   
		    error_log("[m/invoice/getPrizeOfExactlyDigit] SQL : ".$query);
		    $table = $db->getQueryRecord($query);		
		    if(count($table['table']['record']) > 0){
			  return $table['table']['record'];
		    }
		    return false;
	}
	
	// 取得發票的 2~6獎 (部分數字符合)
	public function getPrizeOfPartialDigit($yyyy,$mm, $digit=3) {
		
	       global $db, $config;
           /*
           $query = " SELECT *
					  FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` i
					 WHERE i.switch='Y' 
						AND i.is_void='N' 
					    AND RIGHT(i.invoiceno,${digit}) IN (
						    SELECT RIGHT(prize_no,${digit}) 
						      FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_prizeno`
						     WHERE switch='Y'
						     	AND year='${yyyy}' 
						      AND (start_month='${mm}' OR end_month='${mm}')
						      AND (prize_desc LIKE 'firstPrizeNo%' OR prize_desc LIKE 'sixthPrizeNo%')
						 )
					    AND LEFT(i.invoiceno,2) IN (
						    SELECT DISTINCT(rail_prefix)
						      FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
						     WHERE YEAR='${yyyy}'
						       AND (start_month='${mm}' OR end_month='${mm}')
						  ) ";
            */						  
		    $query = "SELECT i.siid,i.invoiceno, i.userid, i.invoice_datetime, p.invoYm, p.year, p.start_month, p.end_month, p.prize_desc, p.prize_no
					   FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice` i
					   JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_prizeno` p
						 ON RIGHT(i.invoiceno,${digit})=RIGHT(p.prize_no,${digit})
						AND (prize_desc LIKE 'firstPrizeNo%' OR prize_desc LIKE 'sixthPrizeNo%')
						AND p.switch='Y'
						AND p.year='${yyyy}' 
						AND (p.start_month='${mm}' OR p.end_month='${mm}')
					  WHERE i.switch='Y' 
						AND i.is_void='N' 
						AND LEFT(i.invoiceno,2) IN (
						    SELECT DISTINCT(rail_prefix)
						      FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
						     WHERE year='${yyyy}'
						       AND (start_month='${mm}' OR end_month='${mm}')
						) ";		
			error_log("[m/invoice/getPrizeOfPartialDigit] SQL : ".$query);
		    $table = $db->getQueryRecord($query);		
		    if(count($table['table']['record']) > 0){
			  return $table['table']['record'];
		    }
		    return false;
	}
	
	
	public function get_donatelist() {
		
	       global $db, $config;

           $query = "SELECT dename as name, decode as code
					   FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}donate` 
					  WHERE switch='Y' AND used='Y'";		   
		    error_log("[m/invoice/get_donatelist] SQL : ".$query);
		    $table = $db->getQueryRecord($query);		
		    if(count($table['table']['record']) > 0){
			  return $table['table']['record'];
		    }
		    return false;
	}
	
}
?>