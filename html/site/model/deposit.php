<?php
/**
 * Deposit Model 模組
 */
class DepositModel {
    public $id;
    public $email;
    public $ok;
    public $msg;

    public function __construct() {
        global $db;
		
        $this->id = isset($_SESSION['auth_id']) ? $_SESSION['auth_id'] : 0;
        $this->email = isset($_SESSION['auth_email']) ? $_SESSION['auth_email'] : '';
        $this->ok = false;

        return $this->ok;
    }


	public function deposit_rule($id) {
        global $db, $config;
		
		$query ="SELECT drid, name, description, remark, act, notify_url, logo, banner ,mark, type
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}'
			AND drid = '{$id}'
			AND switch = 'Y'
			ORDER BY seq
		";
		
		//取得資料
		$recArr = $db->getQueryRecord($query);
		
		if(!empty($recArr['table']['record'])) {
			return $recArr['table']['record'];
		}
		
		return false;
	}
	
    public function deposit_rule_test($id) {
        global $db, $config;
		
		$query ="SELECT drid, name, description, act, notify_url, logo, banner  
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` 
		WHERE 
			prefixid = '{$config['default_prefix_id']}'
			AND drid = '{$id}'
			ORDER BY seq
			";
		
		//取得資料
		$recArr = $db->getQueryRecord($query);
		
		if(!empty($recArr['table']['record'])) {
			return $recArr['table']['record'];
		}
		
		return false;
	}
	
	//儲值規則列表
	public function row_drid_list($json, $deposit_enable='Y', $loc='deposit') {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT `drid`, `name`, `description`, `act`, `notify_url`, `logo`, `banner`, `mark`, `type`, `payment_fee`
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND drid is not null
			AND drid NOT IN (12)
			AND switch = 'Y'
			AND `loc`='{$loc}'
		";
		if ($json == 'Y') {
			$query .= " AND drid <> '10' ";
		}
		//20200117 暫時移除信用卡
		if($deposit_enable !=='Y'){
			$query .= " AND drid <> '8' ";
		}
		
		$query .= " ORDER BY seq asc ";
		//取得資料
		error_log("[dream_house/row_drid_list]: ".$query);
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
	//儲值規則列表
	public function row_drid_list_test() {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT *
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND drid is not null
		";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
    
    //資料列表
	public function row_list($id) {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT `driid`, `drid`, `name`, `description`, `amount`, `spoint`, `custom`, 
		`code1`, `codeformat1`, `code2`, `codeformat2`, `code3`, `code4`
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item`
		WHERE `prefixid` = '{$config['default_prefix_id']}'
			AND `drid` = '{$id}'
			AND `switch` = 'Y' 
		ORDER BY `seq` DESC 	
		";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
	//資料列表
	public function row_list_test($id) {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT driid, drid, name, description, amount, spoint 
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND drid = '{$id}'
		";
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
	//資料列表
	public function deposit_rule_item($id) {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT dri.*, dr.name as rule_name, dr.logo
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` dri
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` dr ON 
			dri.prefixid = dr.prefixid
			AND dri.drid = dr.drid 
			AND dr.switch = 'Y'
		WHERE
			dri.prefixid = '{$config['default_prefix_id']}'
			AND dri.driid = '{$id}'
			AND dri.switch = 'Y'
			AND dr.drid IS NOT NULL
		";
		//error_log("[deposit_rule_item]:".$query);
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
	//SELECT * FROM saja_ deposit_rule_item WHERE switch='Y' AND drid=13 AND amount<1100 ORDER BY amount DESC LIMIT 1
	//比對 amount 取資料
	public function compare_deposit_rule_item($drid, $amount) {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item`
			WHERE `prefixid`='{$config['default_prefix_id']}' AND `switch`='Y'
			AND `drid`='{$drid}'
			AND `amount` < '{$amount}'
			ORDER BY `amount` DESC LIMIT 1";
		//error_log("[compare_deposit_rule_item]:". $query);
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		
		return false;
    }
	
	//新增deposit_history資訊
	public function add_deposit_history($uid, $driid, $depositid, $spointid, $ip, $switch='Y', $carrierdata, $dscode_num=0 ) {
        global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` SET 
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$uid}',
			`driid`='{$driid}',
			`depositid` = '{$depositid}',
			`spointid` = '{$spointid}',
			`dscode_num` = '{$dscode_num}',
			`status`='order',
			`src_ip`='{$ip}',
			`insertid` = '{$_SESSION['auth_id']}',
			`inserttype` = 'User',
			`insertname` = '{$_SESSION['user']['profile']['nickname']}',
			`insertt`=NOW(),
			`modifierid` = '{$_SESSION['auth_id']}',
			`modifiertype` = 'User',
            `switch`='{$switch}',
			`modifiername` = '{$_SESSION['user']['profile']['nickname']}',
			`carrierdata` = '{$carrierdata}'
		";
		$res = $db->query($query);
		$dhid = $db->_con->insert_id;
		
		return $dhid;
	}
    
    //新增deposit_history資訊
	public function addDepositHistory($arrNew) {
        global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` SET 
			`prefixid`='{$config['default_prefix_id']}',
			`userid`='{$arrNew['userid']}',
			`driid`='{$arrNew['driid']}',
			`depositid` = '{$arrNew['depositid']}',
			`spointid` = '{$arrNew['spointid']}',
			`status`='{$arrNew['status']}',
			`src_ip`='{$arrNew['src_ip']}',
			`insertid` = '{$arrNew['insertid']}',
			`inserttype` = '{$arrNew['inserttype']}',
			`insertname` = '{$arrNew['insertname']}',
			`data`='{$arrNew['data']}',
			`insertt`=NOW(),
			`modifierid` = '{$arrNew['modifierid']}',
			`modifiertype` = '{$arrNew['modifiertype']}',
			`switch`='{$arrNew['switch']}',
			`modifiername` = '{$arrNew['modifiername']}'
		";
		if(!empty($arrNew['drid']) ){ $query .= ",`drid`='{$arrNew['drid']}'"; }
		if(!empty($arrNew['amount']) ){ $query .= ",`amount`='{$arrNew['amount']}'"; }
		if(!empty($arrNew['dscode_num']) ){ $query .= ",`dscode_num` = '{$arrNew['dscode_num']}'"; }
		if(!empty($arrNew['memo']) ){ $query .= ",`memo` = '{$arrNew['memo']}'"; }
		
		$res = $db->query($query);
		$dhid = $db->_con->insert_id;
		error_log("[addDepositHistory]dhid:{$dhid}, query:". $query);
		
		return $dhid;
	}
	
	//資料列表
	public function get_deposit($id) {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT d.depositid, d.amount amount, sp.amount paytitle FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d
		LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh ON 
			d.prefixid = dh.prefixid AND d.depositid = dh.depositid AND d.switch = 'Y' 
		left join `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` sp ON 
			dh.prefixid = sp.prefixid and dh.spointid = sp.spointid and dh.switch = 'Y' 
		WHERE
			d.prefixid = '{$config['default_prefix_id']}'
			AND d.depositid = '{$id}'
			AND d.switch = 'Y'
			AND d.depositid IS NOT NULL
		";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
    //新增deposit資訊
	public function add_deposit($uid, $amount, $currency='', $switch='N') {
        global $db, $config;

		if($currency=='') {
		   $currency=$config['currency'];
		}
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` SET 
			`prefixid` = '{$config['default_prefix_id']}', 
			`userid` = '{$uid}', 
			`countryid` = '{$config['country']}', 
			`behav` = 'user_deposit', 
			`currency` = '{$currency}', 
			`amount` = '{$amount}', 
			`seq` = '0', 
			`switch` = '{$switch}',
			`insertt`=NOW()
		";
		$db->query($query);
		return $depositid = $db->_con->insert_id;
	}

	//新增spoint資訊
	public function add_spoint($uid, $amount, $switch='N') {
        global $db, $config;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` SET 
			`prefixid` = '{$config['default_prefix_id']}', 
			`userid` = '{$uid}', 
			`countryid` = '{$config['country']}', 
			`behav` = 'user_deposit', 
			`amount` = '{$amount}', 
			`seq` = '0', 
			`switch` = '{$switch}',
			`insertt`=NOW()
		";
		$db->query($query);
		return $spointid = $db->_con->insert_id;
	}
	
	//新增dscode資訊
	public function add_dscode($uid, $depositid, $switch='N', $feedback='Y', $behav='user_deposit', $promotion=0) {
        global $db, $config;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode` SET 
			`prefixid` = '{$config['default_prefix_id']}', 
			`userid` = '{$uid}',
			`depositid` = '{$depositid}',
			`feedback` = '{$feedback}', 
			`behav` = '{$behav}',
			`promotion` = '{$promotion}',
			`amount` = '1', 
			`seq` = '0', 
			`switch` = '{$switch}',
			`insertt`=NOW()
		";
		$db->query($query);
		return $dscodeid = $db->_con->insert_id;
	}
	//修改 dscode
	public function update_dscode($arr_cond, $arr_update) {
		// 不允許全部改或沒改
		if(count($arr_cond)<1 || count($arr_update)<1) {
			return false;
		}
		global $db, $config;

		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode` SET modifyt = NOW() ";
		foreach($arr_update as $key => $value) {
			$query.=(" ,".$key."='".addslashes($value)."'");
		}

		$query.=" where 1=1 ";
		foreach($arr_cond as $key => $value) {
			$query.=(" and ".$key."='".addslashes($value)."'");
		}
		//error_log("[m/deposit/update_deposit] : ".$query);
		return $db->query($query);
	}
	//deposit資訊
	public function get_dscode($userid, $behav=null, $remark=null, $dscodeid=0) {
        global $db, $config;
		
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode` 
			WHERE `prefixid`='{$config['default_prefix_id']}' AND `switch`='Y'
			AND `userid`= '{$userid}' ";
			
		if(!empty($behav) ){ $query .= "AND `behav`='{$behav}'"; }
		if(!empty($remark) ){ $query .= "AND `remark`='{$remark}'"; }
		if(!empty($dscodeid) ){ $query .= "AND `dscodeid`='{$dscodeid}'"; }
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
	}
	
	//dscode_promotion 圓夢券贈送活動
	public function get_dscode_promotion($driid, $behav='a', $dpid=0) {
        global $db, $config;
		
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode_promotion`
			WHERE `prefixid` = '{$config['default_prefix_id']}' AND `switch` = 'Y'
                AND `ontime` <= NOW()
                AND `offtime` > NOW() ";
		if(!empty($dpid) ){
			$query .= "AND `dpid` = '{$dpid}' ";
		}else{
			$query .= "AND `driid`='{$driid}' AND `behav`='{$behav}' ";
		}
		
		error_log("[m/get_dscode_promotion]: ".$query);
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
	}
	//dscode 生效
	public function set_dscode($dhid) {
        global $db, $config;
		
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}dscode` SET  
			`switch`='Y', `modifyt`=NOW()
			WHERE `depositid`='{$dhid}'
		";
		$db->query($query);
	}
	//deposit_history資訊
	public function deposit_history_record($userid, $drid, $amount=0, $memo=null) {
        global $db, $config;
		
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` 
			WHERE `prefixid`='{$config['default_prefix_id']}' AND `switch`='Y' AND `status`='order'
			AND `userid` = '{$userid}'
			AND `drid` = '{$drid}' ";
		
		if(!empty($amount) ){
			$query .= "AND `amount` = '{$amount}' ";
		}
		if(!empty($memo) ){
			$query .= "AND `memo` LIKE '%{$memo}%' ";
		}
		$query .= " ORDER BY `dhid`";
		
		error_log("[deposit_history_record]query: ". $query);
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
				if (!empty($table['table']['record'][0]['memo'])) {
					$action_memo = json_decode($table['table']['record'][0]['memo'],true);
					$table['table']['record'][0]['memo'] = $action_memo;
				}
				if (!empty($table['table']['record'][0]['data'])) {
					$action_data = json_decode($table['table']['record'][0]['data'],true);
					$table['table']['record'][0]['data'] = $action_data;
				}			
			return $table['table']['record'];
		}
		
		return false;
	}
	//deposit_history資訊
	public function get_deposit_history($dhid) {
        global $db, $config;
		
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` 
			WHERE dhid = '{$dhid}'";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
				if (!empty($table['table']['record'][0]['memo'])) {
					$action_memo = json_decode($table['table']['record'][0]['memo'],true);
					$table['table']['record'][0]['memo'] = $action_memo;

				}
				if (!empty($table['table']['record'][0]['data'])) {
					$action_data = json_decode($table['table']['record'][0]['data'],true);
					$table['table']['record'][0]['data'] = $action_data;
				}			
			return $table['table']['record'];
		}
		
		return false;
	}
	
	//deposit_history_by_driid
	public function get_deposit_history_by_driid($driid, $status='') {

        global $db, $config;
        
		//排序
		$set_sort = $this->set_sort();
		
		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT dh.dhid, dh.userid, dh.driid, dh.depositid, dh.spointid, dh.status, dh.data, dh.memo ";		
		$query = "FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh
			LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d ON dh.depositid = d.depositid
			where dh.driid in ('{$driid}') 
			AND dh.switch = 'Y'	
			AND dh.memo IS NOT NULL 
			AND dh.memo != '' "; 
		
		if (!empty($status)){
			$query .= "AND dh.status = ".$status." ";
		}

		$query .= ($set_sort) ? $set_sort : 'ORDER BY dh.insertt DESC';

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query); 
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			
			foreach ($table['table']['record'] as $key => $value) {

				if (!empty($value['memo'])) {
					$action_memo = json_decode($value['memo'],true);
					$table['table']['record'][$key]['memo'] = $action_memo;

				}
				if (!empty($value['data'])) {
					$action_data = json_decode($value['data'],true);
					$table['table']['record'][$key]['data'] = $action_data;

				}				
			}			
			$table['table']['page'] = $page;
			return $table;
		}
		return false;
	}	
	
	//修改deposit_history資訊
	public function set_deposit_history($payArr, $get_deposit_history, $type) {
		global $db, $config;
		
		$modifiertype="User";
		$modifierid=$_SESSION['user']['profile']['userid'];
		$modifiername=$_SESSION['user']['profile']['nickname'];
		$orderid=$payArr['out_trade_no'];
		
		if ($type == 'alipay') {
			$deposit_history_data = '{"out_trade_no":"'.$payArr['out_trade_no'].'", "trade_no":"'.$payArr['trade_no'].'", "result":"'.$payArr['trade_no'].'", "timepaid":"'.date('YmdHis').'", "paymenttype":"'.$config['alipay']['paymenttype'].'"}';
		} else if ($type == 'bankcomm') {
			$deposit_history_data = '{"out_trade_no":"'.$payArr['out_trade_no'].'", "trade_no":"'.$payArr['trade_no'].'", "result":"'.$payArr['result'].'", "timepaid":"'.$payArr['orderDate'].$payArr['orderTime'].'", "OrderMono":"'.$payArr['orderMono'].'", "paymenttype":"'.$config['bankcomm']['paymenttype'].'"}';
		} else if ($type == 'weixinpay') {
			$deposit_history_data = '{"out_trade_no":"'.$payArr['out_trade_no'].'", "trade_no":"'.$payArr['trade_no'].'", "result":"get_brand_wcpay_request:ok", "timepaid":"'.date('YmdHis').'", "paymenttype":"'.$config['weixinpay']['paymenttype'].'"}';
		} else if ($type == 'ibonpay') {
		    $orderid=$payArr['ordernumber'];
			$modifiertype="System";
			$modifierid="0";
			$modifiername="ibonpay_success";
			$deposit_history_data=json_encode($payArr);
		} else if ($type == $config['hinet']['paymenttype']) {
            $deposit_history_data = '{"out_trade_no":"'.$payArr['out_trade_no'].'", "spoints":"'.$payArr['spoints'].'", "userid":"'.$payArr['userid'].'", "serialno":"'.$payArr['serialno'].'", "paymenttype":"'.$config['hinet']['paymenttype'].'", "timepaid":"'.date('YmdHis').'"}';
		} else if ($type == $config['alading']['paymenttype']) {
            $deposit_history_data = '{"out_trade_no":"'.$payArr['out_trade_no'].'", "spoints":"'.$payArr['spoints'].'", "userid":"'.$payArr['userid'].'", "serialno":"'.$payArr['serialno'].'", "paymenttype":"'.$config['alading']['paymenttype'].'", "timepaid":"'.date('YmdHis').'"}';
		} else if ($type=='creditcard') {
		    $orderid=$payArr['out_trade_no'];
			$modifierid=$payArr['modifierid'];
			$modifiername=$payArr['modifiername'];
			$modifiertype=$payArr['modifiertype'];
		    unset($payArr['modifierid']);
			unset($payArr['modifiername']);
			unset($payArr['modifiertype']);
			$deposit_history_data =json_encode($payArr);
		}else{
		    $orderid=$payArr['Td'];
			$modifierid=$payArr['modifierid'];
			$modifiername=$payArr['modifiername'];
			$modifiertype=$payArr['modifiertype'];
		    unset($payArr['modifierid']);
			unset($payArr['modifiername']);
			unset($payArr['modifiertype']);
			$deposit_history_data =json_encode($payArr);
		}

		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` 
			set
				status = 'deposit', 
				data = '{$deposit_history_data}', 
				modifierid = '{$modifierid}',
				modifiertype = '{$modifiertype}',
				modifiername = '{$modifiername}', 
				modifyt = NOW() 
			where 
				dhid = '{$orderid}'
				and status = 'order'
        		";
		error_log("[m/deposit/set_deposit_history] : ".$query);
		
		$db->query($query);
    }
	
	//修改saja_deposit_history
	public function update_deposit_history($arr_cond,$arr_update) {
   		   // 不允許全部改或沒改
		   if(count($arr_cond)<1 || count($arr_update)<1) {
			  return false;
		   }
		   
		   global $db, $config;
		   
		   $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` 
		             SET modifyt = NOW()";
		   foreach($arr_update as $key => $value) {
		       $query.=(" ,".$key."='".addslashes($value)."'");
		   }
		   
		   $query.=" where 1=1 ";
		   foreach($arr_cond as $key => $value) {
		       $query.=(" and ".$key."='".addslashes($value)."'");
		   }
		   error_log("[m/deposit/update_deposit_history] : ".$query);
		   return $db->query($query);
	}
	
	//修改saja_deposit
	public function update_deposit($arr_cond,$arr_update) {
		// 不允許全部改或沒改
		if(count($arr_cond)<1 || count($arr_update)<1) {
			return false;
		}
		global $db, $config;

		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` 
					SET modifyt = NOW() ";
		foreach($arr_update as $key => $value) {
			$query.=(" ,".$key."='".addslashes($value)."'");
		}

		$query.=" where 1=1 ";
		foreach($arr_cond as $key => $value) {
			$query.=(" and ".$key."='".addslashes($value)."'");
		}
		error_log("[m/deposit/update_deposit] : ".$query);
		return $db->query($query);
	}
    
    //deposit 生效
	public function set_deposit($depositid)	{
        global $db, $config;
		
		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` 
			SET 
				switch = 'Y',
				modifyt=NOW()
			where 
				depositid = '{$depositid}'
				and switch = 'N'
		";
		$db->query($query);
	}

	//spoint 生效
	public function set_spoint($spointid) {
        global $db, $config;
		
		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
			SET  
			switch = 'Y',
			modifyt=NOW()
			where 
			spointid = '{$spointid}'
		";
		$db->query($query);
	} 
	
	// 取得 scode promote 資料
	public function get_scode_promote_info($arrCond) {
	    global $db, $config;
	    
		$query = "select * from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` where 1=1 ";
	    if(isset($arrCond['spid']) && !empty($arrCond['spid'])) {
			$query.=" and spid={$arrCond['spid']} ";
		}	
	    if(isset($arrCond['productid']) && !empty($arrCond['productid'])) {
			$query.=" and productid={$arrCond['productid']} ";
		}	
	    if(isset($arrCond['behav']) && !empty($arrCond['behav'])) {
			$query.=" and behav={$arrCond['behav']} ";
	    }
		
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} 
		return false;
	}
	
	//scode_promote資訊
	public function get_scode_promote($amount, $type="alipay") {
        global $db, $config;

		$querylimit = ' and spid > 155 ';
		
		$query = "select * 
			from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` 
			where amount <= '{$amount}'
				{$querylimit}
				and ontime <= NOW()
				and offtime > NOW()
				and behav = 'c'
				and switch = 'Y'
			order by amount desc
			limit 0, 1";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		
		return false;
	}
	
	//scode_promote資訊
	public function get_scode_promote_rt($driid) {
        global $db, $config;
		/*
		$query = "select rt.*, sp.name spname, sp.productid, sp.offtime   
			from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` rt 
			left outer join `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp on
				rt.prefixid = sp.prefixid 
				and rt.spid = sp.spid 
			where 
				rt.driid = '{$driid}'
				and sp.switch = 'Y'
                and sp.ontime <= NOW()
                and sp.offtime > NOW()
				and rt.behav = 'c'
				and rt.switch = 'Y' ";
		*/
		// add product thumbnail & thumbnail_url by Thomas 20190712
		$query = "select rt.*, sp.name spname, sp.productid, sp.offtime, p.thumbnail_url, pt.filename as thumbnail                 		
			from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` rt 
			left outer join `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp on
				rt.prefixid = sp.prefixid 
				AND rt.spid = sp.spid 
			left outer join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
			    sp.prefixid = p.prefixid
			    AND sp.productid = p.productid
			left outer join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
                p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
			where 
				rt.driid = '{$driid}'
				and sp.switch = 'Y'
                and sp.ontime <= NOW()
                and sp.offtime > NOW()
				and rt.behav = 'c'
				and rt.switch = 'Y' ";
		
		// error_log("[m/deposit/get_scode_promote_rt] : ".$query);
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
	}
    
    // 取得scode_promote資訊 Add By Thomas 20190107
	public function getScodePromoteData($arrCond) {
        global $db, $config;
		
		$query = "SELECT sp.*, rt.sprid, rt.driid, rt.amount, rt.num 
			 FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` rt 
			 LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp 
			   ON rt.prefixid = sp.prefixid 
			  AND rt.spid = sp.spid 
			WHERE sp.switch = 'Y' AND rt.switch='Y'
              AND sp.ontime <= NOW()
              AND sp.offtime > NOW() ";
		
        if(!empty($arrCond['spid'])) {
           $query.= " AND sp.spid='${arrCond['spid']}' ";    
        }
        if(!empty($arrCond['behav'])) {
           $query.= " AND sp.behav='${arrCond['behav']}' ";    
        }
        if(!empty($arrCond['sprid'])) {
           $query.= " AND rt.sprid='${arrCond['sprid']}' ";    
        }
        if(!empty($arrCond['driid'])) {
           $query.= " AND rt.driid='${arrCond['driid']}' ";    
        }
        
        //取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}
	
	//scode_promote資訊
	public function getDepositFreegoods($driid) {
        global $db, $config;
		
		$query = "select rt.driid, rt.seq, rt.name, rt.num, sp.name spname, sp.productid  
		    from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` rt 
			left outer join `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp on
				rt.prefixid = sp.prefixid 
				and rt.spid = sp.spid 
			where 
				and sp.ontime <= NOW()
				and sp.offtime > NOW()
				and rt.driid = '{$driid}'
				and rt.behav = 'c' ";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
	}
	
	//新增scode資訊
	public function add_scode($uid, $scode_promote) {
        global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$uid}', 
			spid = '{$scode_promote['spid']}', 
			behav = 'c', 
			productid = '0', 
			amount = '{$scode_promote['num']}', 
			remainder = '{$scode_promote['num']}', 
			closed = 'N',
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$db->query($query);
		return $scodeid = $db->_con->insert_id;		
	}
	
	//新增scode_history資訊
	public function add_scode_history($uid, $scodeid, $scode_promote)
	{
        global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$uid}', 
			scodeid = '{$scodeid}', 
			spid = '{$scode_promote['spid']}', 
			promote_amount = '{$scode_promote['num']}', 
			num = '{$scode_promote['num']}', 
			memo = '滿額送S碼',
			batch = '0',
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$db->query($query);
	}
	
	//充值送限定S碼
	public function oscode_promote_by_deposit($driid, $productid=0, $spid=0) {
    	global $db, $config;
		
		$query = "SELECT rt.*, sp.onum, sp.name spname, sp.productid, sp.offtime                		
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote_rt` rt 
			LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` sp on
				rt.prefixid = sp.prefixid
				AND rt.spid = sp.spid
			WHERE sp.`prefixid`='{$config['default_prefix_id']}' AND sp.switch = 'Y'
				AND NOW() between sp.`ontime` AND sp.`offtime`
				AND rt.driid = '{$driid}'
				AND rt.behav = 'c'
				AND rt.switch = 'Y' ";
		
		if(!empty($productid) ){
			$query .= " AND sp.productid = '{$productid}'";
		}
		if(!empty($spid) ){
			$query .= " AND rt.spid = '{$spid}'";
		}
		
		error_log("[oscode_promote_by_deposit]: ".$query);
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	//充值送S碼更新組數
	public function update_oscode_promote($onum, $spid) {
    	global $db, $config;
		
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` SET 
				`amount` = amount + 1 ,
				`scode_sum` = scode_sum + ".$onum." 
			WHERE `prefixid` = '{$config['default_prefix_id']}' AND `switch` = 'Y' 
				AND `spid` = '{$spid}' "; 
		$db->query($query);
		//error_log("[update_oscode_promote]: ".$query);
    }
	//TA充值送限定殺價券
	public function oscode_promote($userid, $driid, $productid=0) {
		global $config, $deposit;
		
		//充值送限定S碼的數量
		$oscode_promote = $deposit->oscode_promote_by_deposit($driid, $productid);
		//error_log("[set_oscode_promote]: ".json_encode($oscode_promote) );
		
		$cnt = 0;
		$spmemo_name="";
		$spmemo_num="";
		$spmemototal=0;
		$spmemo_product="";
		$spid="";
		
		if(!empty($oscode_promote) ){
			$i = 1;
			foreach($oscode_promote as $sk => $sv){
				$cnt = intval($sv["onum"]);
				
				if($userid !='38802' && $sv["spid"]=='9966'){ continue; }else{
					$spmemo[$sk]['name'] = $sv["spname"];
					$spmemo[$sk]['num'] = $cnt.' 張';
					$spmemo[$sk]['spmemototal'] = empty($cnt)? 0 : $cnt;
					$spmemo[$sk]['productid'] = $sv["productid"];
					$spmemo[$sk]['spid'] = $sv["spid"];
				}
				$i++;
			}
		}else{
			$spmemo[0]['name'] = $spmemo_name;
			$spmemo[0]['num'] = $spmemo_num;
			$spmemo[0]['spmemototal'] = $spmemototal;
			$spmemo[0]['productid'] = $spmemo_product;
			$spmemo[0]['spid'] = $spid;
		}
		//error_log("[set_oscode_promote]: ".json_encode($spmemo) );

		return $spmemo;
	}
	
	//oscode 限定S碼生效
	public function set_oscode($uid, $spid='', $dhid, $behav='user_deposit') {
        global $db, $config;
		
		$date1 = date('Y-m-d H:i', time() );
		//限三日內使用
		//$offtime = date('Y-m-d H:i', strtotime("$date1 +3 day") );
		$offtime = '0000-00-00 00:00:00';
		
		$query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` SET  
			`offtime`='{$offtime}', `switch`='Y', `modifyt`=NOW()
			WHERE `used`='N'
			AND `dhid`='{$dhid}'
			AND `userid`='{$uid}'
			AND `behav`='{$behav}'
		";
		
		if(!empty($spid) ){
		$query .= " AND `spid`='{$spid}' ";
		}
		
		error_log("[set_oscode]: ".json_encode($query));
		$db->query($query);
	}
	
	//新增oscode資訊
	public function add_oscode($uid, $scode_promote) {
        global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$uid}', 
			productid = '{$scode_promote['productid']}', 
			spid = '{$scode_promote['spid']}', 
			behav = 'user_deposit', 
			used = 'N',
			amount = '1', 
			verified = 'N',
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$db->query($query);
	}
	
	//新增oscode資訊2
	public function add_oscode2($userid, $productid, $spid, $behav, $switch='Y', $dhid=0)	{
        global $db, $config;
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode` SET 
			`prefixid` = '{$config['default_prefix_id']}', 
			`userid` = '{$userid}', 
			`productid` = '{$productid}', 
			`spid` = '{$spid}',
			`dhid` = '{$dhid}', 
			`behav` = '{$behav}', 
			`used` = 'N',
			`amount` = '1', 
			`verified` = 'N',
			`seq` = '0', 
			`switch` = '{$switch}',
			`insertt`=NOW() ";
		$db->query($query);
		if($userid=='38802'){
			error_log("[add_oscode2]: ".$query);
		}
	}
	
	//資料列表
	public function get_deposit_rule_item($id) {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT dri.*, dr.name as rule_name, dr.logo, dr.banner, dr.act
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` dri
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` dr ON 
			dri.prefixid = dr.prefixid
			AND dri.drid = dr.drid 
			AND dr.switch = 'Y'
		WHERE
			dri.prefixid = '{$config['default_prefix_id']}'
			AND dri.driid = '{$id}'
			AND dri.switch = 'Y'
			AND dr.drid IS NOT NULL
		";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
	//資料列表
	public function get_deposit_rule_item_test($id)	{
        global $db, $config;
		
		//SQL指令
		$query = " SELECT dri.*, dr.name as rule_name, dr.logo
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` dri
		LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` dr ON 
			dri.prefixid = dr.prefixid
			AND dri.drid = dr.drid 
		WHERE
			dri.prefixid = '{$config['default_prefix_id']}'
			AND dri.driid = '{$id}'
			AND dr.drid IS NOT NULL
		";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
	//修改scode資訊
	public function set_scode($uid, $amount) {
        global $db, $config;
		
		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode` 
			SET 
			switch = 'Y',
			modifyt = NOW()
			where
			userid = '{$uid}' 
			and amount = '{$amount}'
			and switch = 'N'
			order by insertt desc
		";
		$db->query($query);
	}
	
	//修改scode資訊
	public function set_scode_history($uid, $amount) {
        global $db, $config;
		
		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_history` 
			SET 
			switch = 'Y',
			modifyt = NOW()
			where
			userid = '{$uid}' 
			and promote_amount = '{$amount}'
			and switch = 'N'
			order by insertt desc
		";
		$db->query($query);
	}
	
	//儲值記錄
	public function get_deposit_id($id)	{
        global $db, $config;
		
		//SQL指令
		$query = " SELECT depositid, amount, switch FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND depositid = '{$id}'
		";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		
		return false;
    }
	
	//Add By Thomas 
	// 取得單筆殺幣資訊 (by spoint id)
	public function get_spoint($spointid) {
		global $db, $config;

		$query = "select * 
		from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
		where spointid = '{$spointid}' ";

		//取得資料
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		} else {
			return false;
		}
	}
	
	//取得歡樂點兌換
	public function get_ch_serial($serial) {
	   global $db, $config;
	   
	   $query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}ch_serial` WHERE serial_no = '$serial' and switch!='X' ";
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false; 
	}
	
	// 修改ch_serial (by id)
	public function update_ch_serial($serial_no, $arr_update) {
		global $db, $config;

		if(count($arr_update)<1) {
			return 0;
		}
		$query = " UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}ch_serial` set modifyt=NOW() ";
		if(!empty($arr_update['userid'])) {
			$query.=(",userid='".$arr_update['userid']."' ");
		}
		if(!empty($arr_update['err_num'])) {
			$query.=(",err_num=".$arr_update['err_num']);
		}
		if(!empty($arr_update['switch'])) {
			$query.=(",switch='".$arr_update['switch']."' ");
		}
		if(!empty($arr_update['memo'])) {
			$query.=(",memo='".$arr_update['memo']."' ");
		}
		if(!empty($arr_update['dhid'])) {
			$query.=(",dhid='".$arr_update['dhid']."' ");
		}
		if(!empty($arr_update['modifyerid'])) {
			$query.=(",modifyerid='".$arr_update['modifyerid']."' ");
		}
		$query.=" WHERE serial_no = '{$serial_no}'";
		$db->query($query);
	}

	// 由drid及點數取得deposit_rule_item
	public function get_deposit_rule_item_by_drid_spoint($drid, $spoint) {
	    global $db, $config;
		
		$query = "SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` 
		WHERE 
		    drid='${drid}' 
			AND spoint='${spoint}' 
			AND switch in ('Y','N') 
		";
		
        //取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'][0])) {
		    return $table['table']['record'][0];
		} else {
		    return false;
		}			
	}
	
	//送限定S碼活動(Frank - 14/12/27)
	public function give_oscode_by_promote($uid, $spid) {
    	global $db, $config;
		
		$query = "SELECT * 
		FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `behav` = 'b'
			AND NOW() between `ontime` and `offtime` 
			AND `spid` = '{$spid}' 
			AND `switch` = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		//查詢是否送過限定s碼
		$query = "SELECT count(*) count
		FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}' 
			AND `userid` = '{$uid}'
			AND `spid` = '{$spid}' 
			AND `switch` = 'Y'
		";
		$count = $db->getQueryRecord($query);
		
		$spid = $table['table']['record'][0]['spid'];
		$onum = $table['table']['record'][0]['onum'];
		$productid = $table['table']['record'][0]['productid'];
		$count = $count['table']['record'][0]['count'];
		
		if($onum > 0 && $count == 0) {
			for($i = 0; $j < $onum; ++$j) {
				$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		        SET 
		            prefixid = '{$config['default_prefix_id']}',
                    userid = '{$uid}',
					spid = '{$spid}',
					productid = '{$productid}',
					behav = 'user_deposit',
					amount = 1,
					insertt = NOW()
				";
				if($i == 0) {
					error_log($query);
				}
				$db->query($query);
			}
			if($spid > 0) {
				$update = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote` 
				SET 
					amount = amount + 1 ,
					scode_sum = scode_sum + ".$onum." 
				WHERE 
					prefixid = '{$config['default_prefix_id']}',
					spid = '{$spid}',
					switch = 'Y'
				"; 
				$db->query($update);
			}
		}
    }
	
	public function getPassphraseInfo($productid, $userid, $switch) {
		global $db, $config;
		
		$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` where 1=1 ";
		if(!empty($productid)) {
			$query.=" and productid='{$productid}' "; 
		}
		if(!empty($userid)) {
			$query.=" and userid='{$userid}' "; 
		}
		if(!empty($switch)) {
			$query.=" and switch='{$switch}' ";
		}

		//取得資料
		$table = $db->getQueryRecord($query);
		error_log("[m/deposit/getPassphraseInfo]:".$query."==>".$table['table']['record'][0]['ppid']);
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		} else {
			return false;
		}	
	}
	
	public function canBidHouse($userid) {
		global $db, $config;
		
		$query=" SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` 
				WHERE switch='Y' AND productid='2854' AND userid='".$userid."' ";
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'][0])) {	      
			return $table['table']['record'][0];
		} else {
			// 20151019 如果沒通關, 只要有充值500元以上也算
			$query=" SELECT count(*) as now_total FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` 
					WHERE prefixid = '{$config['default_prefix_id']}'  
					  AND switch='Y' 
					  AND userid={$userid} 
					  AND amount>=500.00 
					  AND insertt between '2015-08-05 00:00:00' AND '2016-01-01 00:00:00' ";
			$table = $db->getQueryRecord($query);
			if($table['table']['record'][0]>=1) {  			  
				return $table['table']['record'][0];
			} else {
				return false;
			}
		}
	}
	
	public function countPassphrase($productid, $userid, $switch ) {
		global $db, $config;
		$query = " select count(*) as now_total from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` where 1=1 ";
		
		if(!empty($productid)) {
			$query.=" and productid='{$productid}' "; 
		}
		if(!empty($userid)) {
			$query.=" and userid='{$userid}' "; 
		}
		if(!empty($switch)) {
			$query.=" and switch='{$switch}' ";
		}
		//取得資料
		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		} else {
			return false;
		}
	}
	
	public function createPassphrase($productid, $userid, $switch='W', $user_src='', $openid='') {
		global $db, $config;
		$update = " insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` set switch='{$switch}', productid='{$productid}', userid='{$userid}', 
				   user_src=(select intro_by from saja_user.saja_user_affiliate where goto={$userid} order by insertt asc LIMIT 1), 
				   openid=(select uid from saja_user.saja_sso where ssoid=(select ssoid from saja_user.saja_user_sso_rt where userid={$userid}) order by insertt asc LIMIT 1), 
				   insertt=NOW(), modifyt=NOW() ";
		$ret = $db->query($update);
	}
	
	public function updatePassphrase($productid, $userid, $switch) {
		global $db, $config;
		$update = " update `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}passphrase` set switch='{$switch}', modifyt=NOW() where productid='{$productid}' and userid='{$userid}' ";
		$ret = $db->query($update);
	}

    
    //查詢是否送過限定s碼(Frank - 14/12/27)
    public function get_oscode($spid, $userid='', $productid='', $switch='Y', $dhid=0, $behav='') {
    	global $db, $config;
		
		$query = "SELECT count(*) as cnt FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}oscode`
		           WHERE `prefixid` = '{$config['default_prefix_id']}' AND `switch` = '{$switch}' ";
        if(!empty($spid)) {		
		    $query.=" AND `spid` = '{$spid}' ";
		}
		if(!empty($userid)) {		
		    $query.=" AND `userid` = '{$userid}' ";
		}
		if(!empty($productid)) {		
		    $query.=" AND `productid` = '{$productid}' ";
		}
		if(!empty($dhid)) {		
		    $query.=" AND `dhid` = '{$dhid}' ";
		}
		if(!empty($behav)) {		
		    $query.=" AND `behav` = '{$behav}' ";
		}
		
		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'][0])) {
		   return $table['table']['record'][0];
	    } else {
		   return false;
	    }
    }
	
    /*
	 * 回饋殺價幣2塊
	 *	$userid			int		會員編號
	 *	$amount			int		金額
	 *	$currency		int		幣別
	 */
	public function add_Gived_Deposit($userid, $amount, $currency='', $src_ip)
	{
		global $db, $config;

		if($currency=='') {
			$currency=$config['currency'];
		}
		
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$userid}', 
			countryid = '{$config['country']}', 
			behav = 'gift', 
			currency = '{$currency}', 
			amount = '{$amount}', 
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$db->query($query);
		$array['depositid'] = $db->_con->insert_id;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` 
			SET 
			prefixid = '{$config['default_prefix_id']}', 
			userid = '{$userid}', 
			countryid = '{$config['country']}', 
			behav = 'gift', 
			amount = '{$amount}', 
			seq = '0', 
			switch = 'Y',
			insertt=NOW()
		";
		$db->query($query);
		$array['spointid'] = $db->_con->insert_id;

		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history`
			SET 
			prefixid='{$config['default_prefix_id']}',
			userid='{$userid}',
			driid='0',
			depositid = '{$array['depositid']}', 
			spointid = '{$array['spointid']}', 
			status='gift',
			src_ip='{$src_ip}',
			insertid = '{$_SESSION['user']['profile']['userid']}',
			inserttype = 'User',
			insertname = '{$_SESSION['user']['profile']['nickname']}',
			insertt=NOW(),
			modifierid = '{$_SESSION['user']['profile']['userid']}',
			modifiertype = 'User',
			modifiername = '{$_SESSION['user']['profile']['nickname']}'
		";
		$res = $db->query($query);
		$array['dhid'] = $db->_con->insert_id;
	
		return $array;
	}	

	//資料列表
	public function get_Deposit_Count($userid) {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT count(d.depositid) as num
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d
		LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh ON 
			d.prefixid = dh.prefixid AND d.depositid = dh.depositid AND d.switch = 'Y' 
		left join `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}spoint` sp ON 
			dh.prefixid = sp.prefixid and dh.spointid = sp.spointid and dh.switch = 'Y' 
		WHERE
			d.prefixid = '{$config['default_prefix_id']}'
			AND d.userid = '{$userid}'
			AND d.switch = 'Y'
			AND d.depositid IS NOT NULL
		";
		
		//取得資料
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		
		return false;
    }
    
    /**
     * 查询：取得储存资料
     * @param int $userid    用户id
     * @param int $depositid    未開發票的單號
     * @return array
     */
    public function getDepositForInvoice($depositid = '',$userid = '') {
        global $db, $config;
        
        if(empty($depositid) && empty($userid)) {
           return false;   
        }
        
        //SQL指令
        $query = " SELECT depositid, invoiceno, amount, insertt, switch 
        FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
        WHERE prefixid ='{$config['default_prefix_id']}' ";
        if(!empty($userid)) {
			$query.= " AND userid ='{$userid}' ";
		}	
        if(!empty($depositid)) {
			$query.= " AND depositid ='{$depositid}' ";
        }
		//取得資料
		$table = $db->getQueryRecord($query);
        
	    if(!empty($table['table']['record'][0])) {
	        return $table['table']['record'][0];
	    } else {
	        return false;
	    }
    }
	
	public function getDepositHistoryForInvoice($depositid = '',$userid = '') {
        global $db, $config;
        
        if(empty($depositid) && empty($userid)) {
           return false;   
        }
        
        //SQL指令
        $query = " SELECT d.depositid, dh.dhid, d.amount, dh.invoiceno, d.insertt, d.switch 
        FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d
		JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh
		  ON  d.depositid=dh.depositid
        WHERE dh.prefixid ='{$config['default_prefix_id']}' ";
        if(!empty($userid))
	       $query.= " AND dh.userid ='{$userid}' ";
        if(!empty($depositid))
           $query.= " AND d.depositid ='{$depositid}' ";
        
		//取得資料
		$table = $db->getQueryRecord($query);
       
	    if(!empty($table['table']['record'][0])) {
	        return $table['table']['record'][0];
	    } else {
	        return false;
	    }
    }
    
    /**
     * 查询：取得发票字轨资料
     * @param date $year
     * @param date $date
     * @return array
     */
    public function getInvoiceRail($year = '',$mm = '', $status='') {
        global $db, $config;
        if(empty($status)) {
           $status='Y';
		}
        $query = " SELECT *
        FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
        WHERE prefixid ='{$config['default_prefix_id']}' 
        AND switch = '{$status}'
        AND year = '{$year}'
        AND (start_month = '{$mm}' OR end_month = '{$mm}')
        ORDER BY railid ASC
        LIMIT 0,1 ";
        
		//取得資料
        $table = $db->getQueryRecord($query);
        
	    if(!empty($table['table']['record'][0])) {
    	    return $table['table']['record'][0];
        } else {
            return false;
        }
    }

    /**
     * 更新：將 發票號碼 和 隨機號碼 回寫到 saja_deposit
     * @param int $userid
     * @param int $depositid
     * @param string $invoiceno
     * @param string $random_code
     * @param int $railid	 
     * @return array
     */
    public function updateDepositInvoice($userid = '',$depositid = '',$invoiceno = '',$random_code = '',$railid = '') {
        global $db, $config;
        
        $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
        SET invoiceno = '{$invoiceno}',random_code = '{$random_code}',railid = '{$railid}'
        WHERE userid ='{$userid}'
        AND depositid = '{$depositid}' ";
		return $db->query($query);
    }
	
	public function updateDepositHistoryInvoice($userid = '',$depositid = '',$invoiceno = '') {
        global $db, $config;
        
        $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history`
                     SET invoiceno = '{$invoiceno}'
                   WHERE userid ='{$userid}'
                     AND depositid = '{$depositid}' ";
		$ret =  $db->query($query);
		return $ret;
    }
    
    /**
     * 更新：修改已使用的发票号码
     * @param bigint $railid
     * @param string $rail_prefix
     * @param int $contrast
     * @return array
     */
    public function updateInvoiceRail($railid = '',$rail_prefix = '',$contrast = '') {
        global $db, $config;
        
        if (empty($contrast)) {
            $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
            SET used = used + 1 
            WHERE rail_prefix ='{$rail_prefix}'
            AND railid='{$railid}' ";
        } else {
            $query = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
            SET switch = 'E'
            WHERE rail_prefix ='{$rail_prefix}'
            AND railid='{$railid}' ";
        }
		return $db->query($query);
    }
	
	/*
	 *	取得需上傳發票的清單
	 *	$date				varchar					期別 ex:20180708
	 */
	public function getInvoiceList($idate) {
        global $db, $config;

		$InvoiceYear = substr($idate,0 ,4); // 發票日期
		$InvoiceMonth = substr($idate,4 ,2); // 開始月
		$InvoiceMonth2 = substr($idate,6 ,2); // 結束月
		
		//SQL指令
		$query = " SELECT d.depositid, d.userid, d.currency, d.amount, d.invoiceno, d.random_code, dh.insertname, d.insertt 
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d
		LEFT JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` dh ON 
			d.prefixid = dh.prefixid AND d.depositid = dh.depositid AND d.switch = 'Y' 		
		WHERE
			d.prefixid = '{$config['default_prefix_id']}' 
			AND d.invoice_upload = 'N' 
			AND d.switch = 'Y' 
			AND d.depositid IS NOT NULL 
			AND d.invoiceno IS NOT NULL 
			AND d.random_code IS NOT NULL 
			AND d.behav = 'user_deposit' ";
			
		$query .= " AND d.insertt between '{$InvoiceYear}-{$InvoiceMonth}-01' AND '{$InvoiceYear}-{$InvoiceMonth2}-31' ";
		$query .= " ORDER BY insertt ASC ";
		//取得資料
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}

		return false;
    }	
	
	/*
	 *	確認發票號碼是否使用
	 *	$invoiceno				varchar					發票編號
	 *	$type					varchar					查詢類型
	 */
	public function getInvoiceByNo($invoiceno,$type='')	{
        global $db, $config;
		
		//SQL指令
		$query = " SELECT d.depositid, d.userid, d.currency, d.amount, d.invoiceno, d.random_code, d.invoice_upload, d.insertt, d.modifyt 
		FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` d
		WHERE
			d.prefixid = '{$config['default_prefix_id']}' 
			AND d.switch = 'Y' 
			AND d.depositid IS NOT NULL 
			AND d.invoiceno = '{$invoiceno}' 
			AND d.random_code IS NOT NULL 
			AND d.behav = 'user_deposit' ";
		
		if($type == "use") {	
			$query .= " AND d.invoice_upload = 'Y' ";
		} else {
			$query .= " AND d.invoice_upload != 'Y' ";
		}
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		
		return false;
    }


	/*
	 *	取得未使用發票清單
	 *	$year					varchar					發票編號
	 *	$type					varchar					查詢類型
	 */	
    public function getInvoiceRailList($year = '',$smonth = '',$emonth = '') {
        global $db, $config;
        
        $query = " SELECT railid, rail_prefix, seq_start, used, total
        FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}invoice_rail`
        WHERE prefix ='{$config['default_prefix_id']}'
        AND switch = 'Y'
        AND year = '{$year}'
        AND (start_month = '{$smonth}' OR end_month = '{$emonth}' )
        ORDER BY railid ASC ";
        
        //取得資料
        $table = $db->getQueryRecord($query);
        
	    if(!empty($table['table']['record'])) {
    	    return $table['table']['record'];
        } else {
            return false;
        }
    }
	
    // 取得儲值項目資料List 
    // By Thomas 20190107
	public function getDepositRuleItems($arrCond='', $strOrderBy='') {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item`
                    WHERE prefixid = '{$config['default_prefix_id']}' AND switch='Y' ";		
        if(!empty($arrCond['drid'])) {
            $query.= " AND drid='${arrCond['drid']}' ";  
        }
        if(!empty($arrCond['driid'])) {
            $query.= " AND driid='${arrCond['driid']}' ";  
        }
        if(!empty($arrCond['amount'])) {
            $query.= " AND amount=${arrCond['amount']} ";  
        }
        if(!empty($arrCond['spoint'])) {
            $query.= " AND spoint=${arrCond['spoint']} ";  
        }
        if(!empty($strOrderBy)) {
			$query.= " ORDER BY ".$strOrderBy;    
        } else {
			$query.= " ORDER BY driid asc ";   
        }
        
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }
	
    // 取得首次儲值倍數送資料
	public function getDepositTimes() {
        global $db, $config;
		
		$nowtime = date('Y-m-d H:i:s');
		
		//SQL指令
		$query = " SELECT dtid, times, thumbnail, btime, etime 
					FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_times`
                    WHERE prefixid = '{$config['default_prefix_id']}'
					AND `btime` < '{$nowtime}' AND `etime` >= '{$nowtime}'
					order by insertt desc 
					limit 1";		
        
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }	


	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;
		
		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = ''; 
		
		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt"; 
		
		if($_GET){
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}
		
		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}
		
		return $sub_sort_query;
	}
	
	// 統計在 $mins (分鐘)內 刷卡超過 $limit 次的資料
	// add By Thomas 2020/01/18
	public function count_swipe_creditcard($mins=60,$limit=3) {
		   global $db, $config;
		   //SQL指令
		$query = " select userid, count(dhid) as cnt
		             from `saja_cash_flow`.`saja_deposit_history`
					where switch = 'Y' 
					  and status = 'deposit' 
					  and driid in (select `saja_cash_flow`.`saja_deposit_rule_item`.`driid` 
					         from `saja_cash_flow`.`saja_deposit_rule_item` 
							where `drid` = 8) 
					  and `modifyt` > current_timestamp() - interval ${mins} minute 
					  group by userid 
					  having count(dhid) > ${limit} ";		
       	// error_log("[m/deposit/count_sipte_creditcard] : ".$query);
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'][0]['userid'])) {
			error_log("[m/count_swipe_creditcard] ret : ".json_encode($table['table']['record']));
			return $table;
		} else {
		    return false;
		}
	}
	
	// 由銷帳編號撈取儲值歷史紀錄
	public function get_deposit_history_by_writeoffid($writeoffid) {
	        if(empty($writeoffid)) 
			   return false;
			$query = " SELECT * FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_history` 
			            WHERE prefixid='saja' 
						  AND switch='Y' 
						  AND data='${$writeoffid}' ";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'][0])) {
			   return $table['table']['record'][0];
		    }
		    return false;
	}

	//20201201 check if already rewarded
	public function get_rewarded($depositid){
		global $config,$db;
		$sql="SELECT rewarded from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` WHERE `depositid` = '{$depositid}'";
		$raw=$db->getRecord($sql);
		if (sizeof($raw)>0){
			return $raw[0]['rewarded'];
		}else{
			return 'N';
		}
	}

	//20201201 check if already rewarded
	public function set_rewarded($depositid){
		global $config,$db;
		$sql="UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit` set rewarded='Y' WHERE `depositid` = '{$depositid}'";
		$db->query($sql);
	}

	//20201210 leo check if user deposited
	/*
	@userid 會員編號
	@odby  排序欄位
	@li 每頁筆數
	@currency 幣別
	*/
	public function get_deposit_summy($userid=0,$odby='desc',$li='50',$currency='NTD'){
		global $config,$db;
		if ($userid>0){
			$sql="SELECT ifnull(sum(sd.amount),0) as total FROM saja_cash_flow.saja_deposit sd, saja_cash_flow.saja_deposit_history sdh where sd.depositid=sdh.depositid and sdh.switch='Y' and sd.switch='Y' and sdh.userid='{$userid}' and sd.currency='{$currency}' ";
			$raw=$db->getRecord($sql);
			if (sizeof($raw)>0){
				return $raw[0]['total'];
			}else{
				return 0;
			}
		}else{
			$sql="SELECT ifnull(sum(sd.amount),0) as total,sdh.userid FROM saja_cash_flow.saja_deposit sd, saja_cash_flow.saja_deposit_history sdh where sd.depositid=sdh.depositid and sdh.switch='Y' and sd.switch='Y' and sd.currency='{$currency}' group by userid order by total $odby limit 0,$li";
			$raw=$db->getRecord($sql);
			if (sizeof($raw)>0){
				return $raw;
			}else{
				return array(array('total'=>0,'userid'=>0));
			}
		}
	}
}
?>