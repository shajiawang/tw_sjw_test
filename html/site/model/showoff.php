<?php
/**
 * Showoff Model 模組
 */

class ShowoffModel {
    public $id;
    public $email;
    public $ok;
    public $msg;

    public function __construct() {
        global $db;
        
        $this->id = isset($_SESSION['auth_id']) ? $_SESSION['auth_id'] : 0;
        $this->email = isset($_SESSION['auth_email']) ? $_SESSION['auth_email'] : '';
        $this->ok = false;

        return $this->ok;
    }

	/*
	 * 寫入曬單
	 * $data	array	新增資料項目 
	 */		
	public function insert_Showoff($data) {
        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff` 
					SET userid = '{$data['userid']}',
						productid = '{$data['productid']}',
						title = '{$data['title']}',
						content = '{$data['content']}',
						seq = '0',
						switch = 'Y'";		
		$db->query($query);
		return $spointid = $db->_con->insert_id;		
	}
	
	/*
	 * 更新曬單
	 * $shofid	 	int 	留言編號
	 * $data		array	更新資料項目
	 */		
	public function update_Showoff($shofid, $data) {
		global $db, $config;
	
		$query = "update `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff` 
					set seq = '{$data['seq']}',
						switch = '{$data['switch']}',
						modifyt = '{$data['modifyt']}'
					where shofid = '{$shofid}'";
		$db->query($query);		
    }
	
	/*
	 * 寫入曬單圖片
	 * $data	array	新增資料項目 
	 */		
	public function insert_Showoff_Pics($data) {
        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff_pics` 
					SET shofid = '{$data['shofid']}', 
						ori_fname = '{$data['ori_fname']}', 
						thumbnail_url = '{$data['thumbnail_url']}', 
						seq = '{$data['seq']}',						
						switch = 'Y'";		
		$db->query($query);
		return $spointid = $db->_con->insert_id;		
	}
	
	/*
	 * 更新曬單圖片
	 * $shofpid	 	int 	圖片編號
	 * $data		array	更新資料項目
	 */		
	public function update_Showoff_Pics($shofpid, $data) {
		global $db, $config;
	
		$query = "update `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff_pics` 
					set 
						seq = '{$data['seq']}',
						switch = '{$data['switch']}',
						modifyt = '{$data['modifyt']}'
					where shofpid = '{$shofpid}'";
		$db->query($query);		
    }

	/*
	 * 刪除曬單圖片
	 * $shofid	 	int 	曬單編號
	 * $data		array	更新資料項目
	 */		
	public function delete_Showoff_Pics($shofid) {
		global $db, $config;
		
		$query = "delete from `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff_pics` 
					where shofid = '{$shofid}'";
		$db->query($query);		
	
    }		

	/*
	 * 取得曬單清單
	 * $shofid		int 	留言編號
	 * $productid	int 	分類編號 
	 * $userid		int 	使用者編號
	 * $sIdx		int 	目前頁碼
	 * $perpage		int 	筆數	 
	 */		
	public function get_Showoff_List($shofid='', $productid='', $userid='', $sIdx='', $perpage='') {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT s.*, up.nickname, up.thumbnail_url, up.thumbnail_file, up.channelid
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff` s
					LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up 
					ON s.userid = up.userid 
					WHERE s.seq >= 1 AND s.switch = 'Y' ";
		
		
		if(!empty($shofid)) {
			$query .= " AND s.shofid = {$shofid}";	
		}
		if(!empty($productid)) {
			$query .= " AND s.productid = {$productid}";
		}
		if(!empty($userid)) {
			$query .= " AND s.userid = {$userid}";
		}	
		
		$query .= " ORDER BY s.seq ASC";

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			if(!empty($perpage) && !empty($sIdx)) {			
				$query_limit.= " LIMIT ".$sIdx.",".$perpage;
			}else{				
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			//變更圖示內容
			foreach($table['table']['record'] as $tk => $tv) {			
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/".$tv['thumbnail_file'];
				} elseif(empty($tv['thumbnail_url']) && empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/_DefaultHeadImg.jpg";
				}
				
				unset($table['table']['record'][$tk]['thumbnail_file']);
			}
			
			return $table['table'];
		}
		
		return false;		
		
    }	

	/*
	 * 取得曬單細節內容
	 * $shofid	int 	留言編號	
	 */		
	public function get_Showoff_Detail($shofid='') {
		
        global $db, $config;
		
		//SQL指令
		$query = " SELECT * 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff`
					WHERE switch = 'Y' AND shofid = {$shofid}";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}

		return false;		
    }	
	
	/*
	 * 取得客曬單圖示清單
	 * $shofid	int 	留言編號	
	 */		
	public function get_Showoff_Pics($shofid='') {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT * 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff_pics`
					WHERE seq >= '1' AND switch = 'Y' AND shofid = {$shofid}";
		$query .= " ORDER BY seq ASC";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}

		return false;		
    }
	
	/*
	 * 取得曬單是否存在
	 * $productid	int 	分類編號 
	 */		
	public function get_Showoff_have($productid='') {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT count(shofid) as num
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_showoff` s
					WHERE s.switch = 'Y' AND s.productid = {$productid}";

		//取得資料
		$table = $db->getQueryRecord($query);				
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}

		return false;		
	}	
}
?>