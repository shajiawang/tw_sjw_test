<?php
/**
 * Faq Model 模組
 */
class FaqModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct(){
    }

    /*
	 *	幫助中心清單
	 *	$hcid		int			幫助中心
	 */
	public function help_list($hcid) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT h.hid, h.name, h.description
		FROM `{$config["db"][2]["dbname"]}`.`{$config['default_prefix']}help` h
		WHERE
			h.prefixid = '{$config["default_prefix_id"]}'
			AND h.hcid ='{$hcid}'
			AND h.switch = 'Y'
		ORDER BY
			h.seq ASC
		";
		$table = $db->getRecord($query);

		if(!empty($table)) {
			foreach($table as $rk => $rv) {
				//简介
				$description = (!empty($table[$rk]['description']) ) ? $table[$rk]['description'] : '空白';
				$table[$rk]['description'] = html_decode($description);
			}

			return $table;
		}
		return false;
    }

	/*
	 *	幫助中心清單
	 *	$hcid		int			幫助中心
	 */
	public function help_category() {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT h.hcid, h.name
		FROM `{$config["db"][2]["dbname"]}`.`{$config['default_prefix']}help_category` h
		WHERE
			h.prefixid = '{$config["default_prefix_id"]}'
			AND h.switch = 'Y'
		ORDER BY
			h.seq ASC
		";
		$table = $db->getRecord($query);

		if(!empty($table)) {
			return $table;
		}
		return false;
    }

	/*
	 *	秘笈清單
	 *	$cid		int			分類
	 */
	public function tech_list($cid) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT f.faqid, f.name, f.description
		FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}faq` f
		WHERE
			f.prefixid = '{$config["default_prefix_id"]}'
			AND f.fcid ='{$cid}'
			AND f.`mod`='b'
			AND f.switch = 'Y'
		ORDER BY
			f.seq ASC
		";
		$table = $db->getRecord($query);

		if(!empty($table)) {
			foreach($table as $rk => $rv) {
				//简介
				$description = (!empty($table[$rk]['description']) ) ? $table[$rk]['description'] : '空白';
				$table[$rk]['description'] = html_decode($description);
			}

			return $table;
		}
		return false;
    }

	/*
	 *	秘笈分類
	 */
	public function tech_category() {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT h.fcid, h.name cname
		FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}faq_category` h
		WHERE
			h.prefixid = '{$config["default_prefix_id"]}'
			AND h.switch = 'Y'
			AND h.`mod`='b'
		ORDER BY
			h.seq ASC
		";
		$table = $db->getRecord($query);

		if(!empty($table)) {
			return $table;
		}
		return false;
    }

	/*
	 *	新手教學清單
	 *	$cid		int			分類
	 */
	public function faq_list_a($cid) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT f.faqid, f.name, f.description
		FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}faq` f
		WHERE
			f.prefixid = '{$config["default_prefix_id"]}'
			AND f.fcid ='{$cid}'
			AND f.`mod`='a'
			AND f.switch = 'Y'
		ORDER BY
			f.seq ASC
		";
		$table = $db->getRecord($query);

		if(!empty($table)) {
			foreach($table["table"]['record'] as $rk => $rv) {
				//简介
				$description = (!empty($table[$rk]['description']) ) ? $table[$rk]['description'] : '空白';
				$table[$rk]['description'] = html_decode($description);
			}

			return $table;
		}
		return false;
    }

	/*
	 *	新手教學分類
	 */
	public function faq_category() {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT h.fcid, h.name cname
		FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}faq_category` h
		WHERE
			h.prefixid = '{$config["default_prefix_id"]}'
			AND h.switch = 'Y'
			AND h.`mod`='a'
		ORDER BY
			h.seq ASC
		";
		$table = $db->getRecord($query);

		if(!empty($table)) {
			return $table;
		}
		return false;
    }

	public function faq_list($mod='a') {
        // global $db, $config;
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT f.faqid faqid, f.fcid fcid, f.menu menu, f.seq fseq, fc.seq cseq, fc.name cname
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}faq` f
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}faq_category` fc ON
			f.prefixid = fc.prefixid
			AND f.fcid = fc.fcid
			AND fc.`switch`='Y'
		WHERE
			f.`prefixid` = '{$config['default_prefix_id']}'
			AND f.`mod`='{$mod}'
			AND f.`switch`='Y'
			ORDER BY cseq, fseq
		";
		$recArr = $db->getRecord($query);

		if(!empty($recArr)) {
			foreach($recArr as $rk => $rv) {
				$fcid = $rv['fcid'];
				$faqid = $rv['faqid'];
				$menuArr[$fcid]['cname'] = $rv['cname'];
				$menuArr[$fcid]['menu'][$faqid] = $rv['menu'];
			}
			return $menuArr;
		}
		return false;
    }
	public function faq_detail($id) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

        $query = "SELECT f.*, fc.name cname
		FROM `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}faq` f
		LEFT OUTER JOIN `{$config["db"][0]["dbname"]}`.`{$config['default_prefix']}faq_category` fc ON
			f.prefixid = fc.prefixid
			AND f.fcid = fc.fcid
			AND fc.`switch`='Y'
		WHERE
			f.prefixid = '{$config["default_prefix_id"]}'
			AND f.faqid ='{$id}'
			AND f.switch = 'Y'
		";
		$table = $db->getRecord($query);

		if(!empty($table[0])) {
			//简介
			$description = (!empty($table[0]['description']) ) ? $table[0]['description'] : '空白';
			$table[0]['description'] = html_decode($description);

			return $table[0];
		}
		return false;
    }

}
?>