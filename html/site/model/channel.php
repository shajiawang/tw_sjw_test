<?php
/**
 * Channel Model 模組
 */

class ChannelModel
{
	public $msg;
	public $sort_column = 'name|seq|modifyt'; 

    public function __construct() {
    }
	
	/**
     * Channel Method : get_channel
     */
	public function get_channel()
	{
        // global $db, $config;
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT channelid, provinceid, name  
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel` 
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND switch = 'Y' 
            AND channelid > 0 
			ORDER BY channelid
		";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record']))
		{
			return $table['table']['record']; 
		}
		return false;
    }
    
    /**
     * Region Method : get_region
     */
	public function get_region() {
        // global $db, $config;
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT regionid, countryid, name 
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}region` 
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND switch = 'Y'
			ORDER BY regionid
		";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
    }
    
    /**
     * Province Method : get_province
     */
	public function get_province() {
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT provinceid, countryid, name 
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` 
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND switch = 'Y'
			ORDER BY provinceid
		";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
    }
    
    /**
     * Country Method : get_country
     */
	public function get_country() {
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
        
		$query = "SELECT countryid, name, ename, countrycode, isocountrycode, isocountrycode2, currency, countrycode 
		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}country` 
		WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND switch = 'Y'
			ORDER BY countryid
		";
		$table = $db->getQueryRecord($query); 
			
		if(!empty($table['table']['record'])) {
			return $table['table']['record']; 
		}
		return false;
    }
}
?>
