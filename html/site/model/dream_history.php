<?php
/**
 * Dreamhistory Model 模組
 */

class DreamHistoryModel {
	public $msg;
	public $sort_column = 'name|seq|modifyt'; 

	public function __construct() {
	}
	
	/**
	 * Base Method : set_sort
	 */
	public function set_sort() {
		global $status;
		
		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = ''; 
		
		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt"; 
		
		if($_GET){
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}
		
		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc'){
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}
		
		return $sub_sort_query;
	}

	public function saja_list_set_search() {
		global $status, $config;
		
		$rs = array();

		$rs['join_query'] = " JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON 
			h.prefixid = p.prefixid
			AND h.productid = p.productid
			AND p.switch = 'Y' 
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
				 p.prefixid = pt.prefixid
			AND  p.ptid=pt.ptid
			AND pt.switch='Y'
		";
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = '';
		return $rs;
	}

	public function get_total($productid, $uid)	{
		global $db, $config;
		//SQL指令
		$query_record = " SELECT SUM(h.price) as totalprice, SUM(p.saja_fee) as totalfee 
							FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h 
							JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p 
							ON h.prefixid = p.prefixid AND h.productid = p.productid AND p.switch = 'Y'
							WHERE 
								h.prefixid = '{$config['default_prefix_id']}'
								AND h.userid = '{$uid}'
								AND h.productid = '{$productid}'
								AND h.switch = 'Y'
								AND p.productid IS NOT NULL
								AND h.type='bid'
								AND h.spointid > 0 
							GROUP BY h.productid
						";
		//取得資料
		$table = $db->getQueryRecord($query_record);
			
		if(!empty($table['table']['record']) ) {
		
			return $table['table']['record'][0];
		}
		return false;
	}
	
	/*
	 *	殺價紀錄清單
	 *	$uid					int					會員編號
	 *	$closed					varchar				商品結標狀態(Y:已結標, N:競標中, NB:流標, NP:凍結殺幣, W:已結標並中標者)
	 */
	public function history_list($uid, $closed='') {
		global $db, $config;
		
		//排序
		$set_sort = $this->set_sort();
		
		//搜尋
		$set_search = $this->saja_list_set_search();
		
		//SQL指令
		$query_count = " SELECT count(*) as num from ( select count(*) as num ";

		$query_record = " SELECT h.scodeid, pt.filename, h.spointid, 
							   p.productid, p.saja_fee, p.name, p.thumbnail_url, p.closed, p.retail_price, 
							   unix_timestamp(offtime) as offtime,  count(*) as count, 
							   IFNULL(pgp.userid,'') as final_winner_userid, pgp.pgpid, pgp.complete, 
							   h.nickname, h.insertt as order_time, p.offtime as pofftime, p.totalfee_type ";							   
							   
		if(!empty($closed)) {
			switch($closed) {
				case "N":	//競標中
					$query_add2 = " and p.closed = 'N' and p.offtime>NOW() ";
					break;			
				case "W":	//已結標&&中標者
					$query_add2 = " and p.closed = 'Y' and pgp.userid = {$uid}";
					break;			
			}
		}
		
		$query = " FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp 
			ON p.productid = pgp.productid AND pgp.switch = 'Y' 
		WHERE 
			h.prefixid = '{$config['default_prefix_id']}'
			AND h.userid = '{$uid}'
			AND h.switch = 'Y'
			AND p.productid IS NOT NULL
			AND h.type='bid'
			AND p.display = 'Y'
			AND p.ptype= '1'
			{$query_add2}
			GROUP BY h.productid
		";
		
		$query .= $set_search['sub_search_query']; 
		$query .= ($set_sort) ? $set_sort : ' ORDER BY h.shid DESC ';
		
		$query_count_last = " ) os ";
		
		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);

		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
	
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			//取得資料
			//20191004 leo  unmark error_log("[model/dream_history] saja_list : ".$query_record . $query . $query_limit);
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			
			$table['table']['page'] = $page;
			
			foreach($table['table']['record'] as $tk => $tv) {
				$table['table']['record'][$tk]['retail_price'] = round($tv['retail_price']);
				// 付款總金額
				$total_data = $this->get_total($tv['productid'], $uid);
				if($total_data){
					
					switch($tv['totalfee_type']) {
						case 'A':
							$saja_fee_amount = $total_data['totalprice']+$total_data['totalfee'];
							break;
						case 'B':
							$saja_fee_amount = $total_data['totalprice'];
							break;			  
						case 'O':
							$saja_fee_amount = 0;
							break;
						case 'F':
							$saja_fee_amount = $total_data['totalfee'];
							break;
						default:
							$saja_fee_amount = $total_data['totalfee'];
							break;			  
					}
					$table['table']['record'][$tk]['alltotal'] = ceil($saja_fee_amount);
				} else {
					$table['table']['record'][$tk]['alltotal'] = 0;
				}
			}	
			return $table;
		}
		return false;
	}


	/*
	 *	取得競標中商品某會員下標金額統計清單
	 *	$userid						int					會員編號
	 *	$productid					int					商品編號
	 *	$perpage					int					每頁筆數
	 *  $orderby					varchar				排序方式
	 */
    public function history_detail($productid, $userid, $perpage, $type, $orderby) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		$query = "SELECT hh.paytype, hh.`start`, hh.`end`, hd.title, hd.thumbnail_filename,hh.insertt
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_head` hh
		LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` hd
		ON hh.shdid = hd.shdid
		where	hh.productid = '{$productid}'
			AND hh.productid IS NOT NULL
			AND hh.userid IS NOT NULL
			AND hh.switch = 'Y'
		";

		$update_time_query = "SELECT hh.insertt 
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_head` hh
			LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` hd
			ON hh.shdid = hd.shdid
			where	hh.productid = '{$productid}'
				AND hh.productid IS NOT NULL
				AND hh.userid IS NOT NULL
				AND hh.switch = 'Y'
			order by insertt desc";


		if($type == 1){
			$query.=" AND hh.userid = '{$userid}' ";
		}

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;

		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			switch ($orderby) {
				case 1:
					$query_orderby = " ORDER BY hh.start ASC";
					break;
				case 2:
					$query_orderby = " ORDER BY hh.start DESC";
					break;
				case 3:
					$query_orderby = " ORDER BY hh.insertt ASC";
					break;
				case 4:
					$query_orderby = " ORDER BY hh.insertt DESC";
					break;
				
				default:
					$query_orderby = " ORDER BY hh.start ASC";
					break;
			}
			//取得資料
			$table = $db->getQueryRecord($query.  $query_orderby. $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			$update_time = $db->getQueryRecord($update_time_query);
			$table['table']['update_time'] = $update_time['table']['record'][0]['insertt'];

			return $table;
		}

		return false;
    }
}
?>