<?php
/**
 * Product Model 模組
 */

class ProductModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET) {
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc') {
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

	/**
     * Search Method : product_list_set_search
     */
	public function product_list_set_search() {
		global $status, $config;

		$rs = array();

		$rs['join_query'] = '';
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = ($_SESSION['m_prefix'] == 'm') ? "AND p.mob_type !='N' " : '';

		$rs['sub_search_query'] .= " AND p.closed = 'N'
			AND p.display = 'Y'
			AND p.switch = 'Y'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND (
				(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
				OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N')
			) ";
		if(!empty($_GET["pcid"])) {
			$status["status"]["search"]["pcid"] = $_GET["pcid"] ;
			$status["status"]["search_path"] .= "&pcid=".$_GET["pcid"] ;

			$rs['join_query'] .= "LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pc ON
			p.prefixid = pc.prefixid
			AND p.productid = pc.productid
			AND pc.`pcid` = '{$_GET["pcid"]}'
			AND pc.switch = 'Y' ";

			$rs['sub_search_query'] .= " AND pc.pcid IS NOT NULL
			";
		}

		return $rs;
	}

	public function product_list($channelid='', $storeid='', $arr_exclude_storeids='') {
        global $db, $config;

		if(empty($arr_exclude_storeids) && empty($storeid) && empty($storeid)) {
			$arr_exclude_storeids=$config['exclude_storeids'];
		}
		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`, pt.filename as thumbnail
		                        , p.ptid, sp.storeid, cs.channelid, p.productid, p.name, p.offtime, p.thumbnail_url, p.limitid ";

		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid ";
		$query.= " AND cs.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.is_flash != 'Y'
			AND p.lb_used != 'Y'
			AND sp.productid IS NOT NULL
			AND cs.channelid IS NOT NULL

		";

		if(!empty($channelid)) {
            $query.= " AND cs.channelid IN ('{$channelid}','0') ";
        }

		if(!empty($storeid)) {
			// 只能看到自己上架的商品
			$query.= " AND sp.storeid='{$storeid}' ";
		}

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq, p.offtime ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$productid = $tv['productid'];
				$tv['offtime']=$tv['unix_offtime'];
				//红利回馈
				if($tv['bonus_type'] == 'ratio') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'%';
				} elseif($tv['bonus_type'] == 'value') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'點';
				} else {
					$table['table']['record'][$tk]['bonus'] = 0;
				}

				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($productid);

				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];

				// 取得商品限定相關資料
				$product_limit = $this->get_product_limited($tv['limitid']);

				$table['table']['record'][$tk]['plname'] = $product_limit['plname'];
				$table['table']['record'][$tk]['mvalue'] = $product_limit['mvalue'];
				$table['table']['record'][$tk]['svalue'] = $product_limit['svalue'];
				$table['table']['record'][$tk]['kind'] = $product_limit['kind'];
				$table['table']['record'][$tk]['plpic'] = $product_limit['plpic'];
				$table['table']['record'][$tk]['colorcode'] = $product_limit['colorcode'];
			}

			return $table;
		}

		return false;
    }

	/*
	* 首頁殺價列表
	*/
	public function inx_product_list() {
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`, pt.filename as thumbnail
		                        , p.ptid, sp.storeid, cs.channelid, p.productid, p.name, p.offtime, p.thumbnail_url, p.limitid, p.lb_used, p.ptype ";
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid ";
		$query.= " AND cs.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.is_flash != 'Y'
			AND p.display_all = 'Y'
			AND p.ptype =0
			AND sp.productid IS NOT NULL
			AND cs.channelid IS NOT NULL

		";

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq, p.offtime ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT 0,12";

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$productid = $tv['productid'];
				$tv['offtime']=$tv['unix_offtime'];
				//红利回馈
				if($tv['bonus_type'] == 'ratio') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'%';
				} elseif($tv['bonus_type'] == 'value') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'點';
				} else {
					$table['table']['record'][$tk]['bonus'] = 0;
				}

				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($productid);

				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];

				// 取得商品限定相關資料
				$product_limit = $this->get_product_limited($tv['limitid']);

				$table['table']['record'][$tk]['plname'] = $product_limit['plname'];
				$table['table']['record'][$tk]['mvalue'] = $product_limit['mvalue'];
				$table['table']['record'][$tk]['svalue'] = $product_limit['svalue'];
				$table['table']['record'][$tk]['kind'] = $product_limit['kind'];
				$table['table']['record'][$tk]['plpic'] = $product_limit['plpic'];
				$table['table']['record'][$tk]['colorcode'] = $product_limit['colorcode'];

			}
			return $table;
		}

		return false;
    }


	/*
	* 首頁今日必杀列表
	*/
	public function get_today() {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = '';

		//搜尋
		$set_search = ''; //$this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT pd.thumbnail_url, pt.filename as thumbnail, pt2.filename as thumbnail2, unix_timestamp(p.offtime) as unix_offtime, p.productid, p.name, p.offtime, p.link, pd.description ";

		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_today` p
		           JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` pd
				     ON p.productid=pd.productid AND pd.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND pd.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt2 ON
			p.prefixid = pt2.prefixid
			AND pd.ptid2 = pt2.ptid
			AND pt2.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.switch = 'Y'
			AND pd.switch = 'Y'
			AND pd.closed = 'N'
			AND pd.display = 'Y'
			AND p.display = 'Y'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND (unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime) )
		";
		$query .= ($set_sort) ? $set_sort : "ORDER BY p.seq ASC, p.offtime ASC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			error_log("[product] get_today : ".$query_record . $query . $query_limit);
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$table['table']['record'][$tk]['description'] = str_replace(array("\r\n\t&nbsp;","\r","\n","\t","&nbsp;"), '', (trim(strip_tags($tv['description']))));
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['thumbnail'];
				}
			}

			return $table['table'];
		}
		return false;
    }

	/*
	 * APP首頁今日必杀列表
	 * 商品圖示使用優先順序為p.thumbnail_url -> pd.thumbnail_url -> pt.filename
	 */
	public function get_today_app($pcid) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = '';

		//搜尋
		$set_search = ''; //$this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT distinct(p.productid), p.name, p.thumbnail_url as thumbnail_url_t, pd.ontime, unix_timestamp(pd.offtime) as offtime,
					p.seq, p.link, pd.retail_price, pd.thumbnail_url, pd.ptid, pt.filename, pcr.pcid ";
		$query = " FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_today` p
		    JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` pd ON
			p.prefixid = pd.prefixid
			AND p.productid = pd.productid
			AND pd.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid > 0
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr ON
			p.productid = pcr.productid
			AND p.productid > 0
			AND pcr.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category` pc ON
			p.prefixid = pc.prefixid
			AND pcr.pcid = pc.pcid
			AND pc.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.productid > 0
			AND p.switch = 'Y'
			AND p.display = 'Y'
			AND pc.layer = 1
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND (unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime) )
		";
	    if(!empty($pcid)) {
		   $query.=" AND pcr.pcid={$pcid} ";
		}

		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq ASC, p.offtime ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count.$query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url_t'])) {
					if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
						$table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
					}
				} else {
					$table['table']['record'][$tk]['thumbnail_url'] = $tv['thumbnail_url_t'];
				}
			}

			return $table['table'];
		}
		return false;
    }

	/*
	* 下標條件關聯
	*/
	public function get_product_rule_rt($productid)	{
        global $db, $config;

		$query ="SELECT pr.srid, pr.value
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt` pr
		WHERE
			pr.prefixid = '{$config['default_prefix_id']}'
			AND pr.productid = '{$productid}'
			AND pr.switch = 'Y'
		";

		$recArr = $db->getQueryRecord($query);
		if(empty($recArr['table']['record'][0])) {
			$recArr['table']['record'][0]['srid'] = '';
			$recArr['table']['record'][0]['value'] = '';
		}

		return $recArr['table']['record'][0];
    }

    /*
	   閃殺開關 : table saja_admin_user, userid=0的row, 其switch='Y' 表示顯示/site/product/adview上的qrcode
	*/
	public function show_flash_qrcode() {
		global $db, $config;
		$query = " SELECT verified from saja_user.saja_admin_user where prefixid = '{$config['default_prefix_id']}' and userid=0 ";
		$retArr = $db->getQueryRecord($query);
		if(!empty($retArr['table']['record'][0])) {
			return $retArr['table']['record'][0]['verified'];
		} else {
			return "N";
		}
	}

    /*
	* 商品資訊
	*/
	public function get_info($id) {
        if(empty($id))
		   return false;
		global $db, $config;
		// 20160412 By Thomas ： 加中標者userid(final_winner_userid)
		// 20170607 By Thomas : 加商品所屬 storeid , channelid
		// 移除 AND p.display = 'Y'
		$query ="SELECT p.*, unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`,
		         IFNULL(pt.filename,'') as thumbnail,
				 IFNULL(pgp.userid,'') as final_winner_userid,
				 IFNULL(pgp.price,0) as final_bid_price,
				 sprt.storeid,
				 csrt.channelid,
				 pt2.filename as thumbnail2,
				 pt3.filename as thumbnail3,
				 pt4.filename as thumbnail4
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
		     ON p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt2 ON
			p.prefixid = pt2.prefixid
			AND p.ptid2 = pt2.ptid
			AND pt2.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt3 ON
			p.prefixid = pt3.prefixid
			AND p.ptid3 = pt3.ptid
			AND pt3.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt4 ON
			p.prefixid = pt4.prefixid
			AND p.ptid4 = pt4.ptid
			AND pt4.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		     ON	p.productid = pgp.productid
            AND pgp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sprt
             ON p.productid = sprt.productid
            AND sprt.switch='Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` csrt
             ON sprt.storeid = csrt.storeid
            AND csrt.switch='Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.productid = '{$id}'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND p.switch = 'Y'
			LIMIT 1 ";
		$recArr = $db->getQueryRecord($query);

		if(!empty($recArr['table']['record'][0])) {
			//中标处理费 = 处理费% * 市價 * 兌換比率
			$recArr['table']['record'][0]['process_fee_ratio']=$recArr['table']['record'][0]['process_fee'];
			$process_fee = ((float)$recArr['table']['record'][0]['process_fee']/100 ) * (float)$recArr['table']['record'][0]['retail_price']  * $config['sjb_rate'];
			$recArr['table']['record'][0]['process_fee'] = strval(round($process_fee, 0));

			// 红利回馈
			if($recArr['table']['record'][0]['bonus_type'] == 'ratio') {
                $recArr['table']['record'][0]['bonus'] = number_format($recArr['table']['record'][0]['bonus'],0,".","");
			} elseif($recArr['table']['record'][0]['bonus_type'] == 'value') {
				$recArr['table']['record'][0]['bonus'] = (float)sprintf("%0.2f", $recArr['table']['record'][0]['bonus']);
			} else {
				$recArr['table']['record'][0]['bonus'] = 0;
			}

			// 店点回馈
			if($recArr['table']['record'][0]['gift_type'] == 'ratio') {
				$recArr['table']['record'][0]['gift'] = (float)sprintf("%0.2f", $recArr['table']['record'][0]['gift']);
			} elseif($recArr['table']['record'][0]['gift_type'] == 'value') {
				$recArr['table']['record'][0]['gift'] = (float)sprintf("%0.2f", $recArr['table']['record'][0]['gift']);
			} else {
				$recArr['table']['record'][0]['gift'] = 0;
			}

			//市價
			$recArr['table']['record'][0]['retail_price'] = str_replace(",","",(number_format($recArr['table']['record'][0]['retail_price']))); //number_format($recArr['table']['record'][0]['retail_price'], 2);

			//底價
			$recArr['table']['record'][0]['price_limit'] = str_replace(",","",(number_format($recArr['table']['record'][0]['price_limit'], 2)));

			//下标手续费
			$recArr['table']['record'][0]['osaja_fee'] = round($recArr['table']['record'][0]['saja_fee'],2);

			$recArr['table']['record'][0]['saja_fee'] =  round($recArr['table']['record'][0]['saja_fee'], 0);


			//商品简介
			$description = (!empty($recArr['table']['record'][0]['description']) ) ? $recArr['table']['record'][0]['description'] : '空白';
			$recArr['table']['record'][0]['description'] = html_decode($description);

			// Relation product_rule_rt 下標條件關聯
			$rule_rt = $this->get_product_rule_rt($id);
		    $recArr['table']['record'][0]['srid'] = $rule_rt['srid'];
			$recArr['table']['record'][0]['value'] = $rule_rt['value'];

			// 取得商品限定相關資料
			$product_limit = $this->get_product_limited($tv['limitid']);

			$recArr['table']['record'][0]['plname'] = $product_limit['plname'];
			$recArr['table']['record'][0]['mvalue'] = $product_limit['mvalue'];
			$recArr['table']['record'][0]['svalue'] = $product_limit['svalue'];
			$recArr['table']['record'][0]['kind'] = $product_limit['kind'];
			$recArr['table']['record'][0]['plpic'] = $product_limit['plpic'];
			$recArr['table']['record'][0]['colorcode'] = $product_limit['colorcode'];

			$recArr['table']['record'][0]['offtime']=(int)$recArr['table']['record'][0]['unix_offtime'];

			$recArr['table']['record'][0]['thumbnail']='/site/images/site/product/'.$recArr['table']['record'][0]['thumbnail'];
			if(!empty($recArr['table']['record'][0]['thumbnail2'])) {
				$recArr['table']['record'][0]['thumbnail2']='/site/images/site/product/'.$recArr['table']['record'][0]['thumbnail2'];
			}
			if(!empty($recArr['table']['record'][0]['thumbnail3'])) {
				$recArr['table']['record'][0]['thumbnail3']='/site/images/site/product/'.$recArr['table']['record'][0]['thumbnail3'];
			}
			if(!empty($recArr['table']['record'][0]['thumbnail4'])) {
				$recArr['table']['record'][0]['thumbnail4']='/site/images/site/product/'.$recArr['table']['record'][0]['thumbnail4'];
			}
			return $recArr['table']['record'][0];
		}
		return false;
    }

	/*
	* 下标礼券
	*/
	public function get_sgift($id) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		if(!empty($_SESSION['auth_id']) && !empty($id)) {
			$query ="SELECT SUM(amount) amount
			FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}sgift`
			WHERE
				`prefixid` = '{$config['default_prefix_id']}'
				AND productid = '{$id}'
				AND userid = '{$_SESSION['auth_id']}'
				AND used = 'N'
				AND switch = 'Y'
			";

			$recArr = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'][0] = '';
		}

		if(!empty($recArr['table']['record'][0]) ){
			return $recArr['table']['record'][0]['amount'];
		}
		return false;
    }

	/*

	public function get_bidded($productid) {
        global $db, $config;

		if(!empty($productid)) {
			$query ="SELECT up.*, sh.*,
			         (SELECT name from `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` pv where pv.switch = 'Y' AND pv.provinceid=up.provinceid) as comefrom
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
				sh.prefixid = up.prefixid
				AND sh.userid = up.userid
				AND up.switch = 'Y'
			WHERE sh.prefixid = '{$config['default_prefix_id']}'
				AND sh.productid = '{$productid}'
				AND sh.switch = 'Y'
				AND up.userid IS NOT NULL
				GROUP BY sh.price
				HAVING COUNT(*) = 1
				ORDER BY sh.price ASC
				LIMIT 1 ";
			$recArr = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'][0] = '';
		}

		if(!empty($recArr['table']['record'][0])) {
			return $recArr['table']['record'][0];
		}
		return false;
    }
    */

    /*
	* 目前中标
      // 20190424 移除 "comefrom"  By Thomas
	*/
	public function get_bidded($productid) {
        global $db, $config;

        $db=new mysql($config["db2"]);
		$db->connect();
        if(!empty($productid)) {
            $query ="SELECT sh.shid, sh.userid, sh.productid, sh.nickname, sh.price, sh.src_ip, sh.insertt, up.thumbnail_file, up.thumbnail_url
			   FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
				sh.prefixid = up.prefixid
				AND sh.userid = up.userid
				AND up.switch = 'Y'
			  WHERE sh.prefixid = '{$config['default_prefix_id']}'
				AND sh.productid = '{$productid}'
				AND sh.switch = 'Y'
                AND sh.type = 'bid'
				AND sh.userid IS NOT NULL
				GROUP BY sh.price
				HAVING COUNT(sh.price) = 1
				ORDER BY sh.price ASC
				LIMIT 1 ";
            // error_log("[m/model/get_bidded] : ".$query);

			$recArr = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'][0] = false;
		}
		if(!empty($recArr['table']['record'][0]) && $recArr['table']['record'][0]['price']>0) {
			 error_log("[m/model/get_bidded] : ".json_encode($recArr['table']['record'][0]));
            return $recArr['table']['record'][0];
		}
		return false;
    }


	/*
	* 會員得標次數
	*/
	public function user_get_bid() {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		if(isset($_SESSION['auth_id'])) {
			$query ="SELECT count(*) user_bid
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pg
			WHERE
				pg.prefixid = '{$config['default_prefix_id']}'
				AND pg.userid = '{$_SESSION['auth_id']}'
				AND pg.switch = 'Y'
			";

			$table = $db->getQueryRecord($query);

			if(!empty($table['table']['record'])) {
				$results = (int)$table['table']['record'][0]['user_bid'];
				return $results;
			}
			return false;
		}
		return false;
	}


	/*
	* 殺價商品分類
	*/
	public function product_category() {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT *
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category`
		WHERE `prefixid` = '{$config['default_prefix_id']}'
			AND `channelid` = '{$_GET['channelid']}'
			AND `ptype` = 0
			AND `switch` = 'Y'
			ORDER BY seq
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'];
			return $results;
		}
		return false;
	}


	/*
	* 殺價商品分類
	*/
	public function product_category_list()	{
		global $config;

		$rkey='product_category_list';
	    $redis = getRedis();
        $jsonStr = $redis->get($rkey);
		if(!empty($jsonStr)) {
		   return json_decode($jsonStr,true);
	    }

		$db=new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT seq, pcid, name, thumbnail_url FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category`
		          WHERE `prefixid` = '{$config['default_prefix_id']}'
				    AND `layer`=1
				    AND `ptype` = 0
					AND `switch` = 'Y'
			   ORDER BY seq ";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'];
			$redis->set($rkey,json_encode($results));
			return $results;
		}
		return false;
	}

	/*
	* 取得某殺價商品所屬分類資料(array)
	*/
	public function get_product_category($productid) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query ="SELECT *
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt`
			WHERE `prefixid` = '{$config['default_prefix_id']}'
				AND `switch` = 'Y'
				AND `productid` = '{$productid}'
				ORDER BY seq desc
		";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'];
			return $results;
		}
		return false;
	}

	/*
	* 快閃列表(Frank-14/12/16)
	*/
	public function product_flash_list($channelid='' ,$storeid='') {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT distinct(p.productid) as ppid, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`, p.thumbnail_url , pt.filename,  sp.storeid, cs.channelid, p.* ";
        $query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			{$set_search['join_query']}
			LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
				p.prefixid = sp.prefixid
				AND p.productid = sp.productid
				AND sp.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
				p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
				sp.prefixid = cs.prefixid
				AND sp.storeid = cs.storeid
				AND cs.switch = 'Y'
			 WHERE  p.prefixid = '{$config['default_prefix_id']}'
				AND p.is_flash = 'Y'
				AND p.switch = 'Y'
				AND p.display = 'Y'
				AND p.lb_used != 'Y'
				AND sp.productid IS NOT NULL
			";

			if(!empty($channelid)) {
                $query.= " AND cs.channelid IN ('${channelid}','0') ";
            }
			if(!empty($storeid)) {
				$query.= " AND sp.storeid = '".$storeid."' ";
			}

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : " ORDER BY offtime ASC ";


		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			error_log("[m/product_flash_list] : ".$query_record . $query . $query_limit);
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$productid = $tv['productid'];

				//红利回馈
				if($tv['bonus_type'] == 'ratio') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'%';
				} elseif($tv['bonus_type'] == 'value') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'点';
				} else {
					$table['table']['record'][$tk]['bonus'] = 0;
				}

				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($productid);

				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];
			}

			return $table;
		}

		return false;
    }


    /*

    // 閃殺 20181128 活動用
    public function product_flash_list_all($channelid='') {
        // global $db, $config;
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = $this->set_sort();
		$set_sort = false;

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
        $query_record = "SELECT distinct(p.productid) as ppp, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`, p.thumbnail_url , pt.filename as thumbnail2, p.* ";
		// Not By Channel
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid2 = pt.ptid
			AND pt.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
            AND p.switch='Y'
            AND p.display='Y'
			AND p.is_flash = 'Y'
			AND p.lb_used != 'Y'
		";

		if(!empty($channelid)) {
		    $query.=" AND cs.channelid='{$channelid}' ";
		}

		$query .= $set_search['sub_search_query'];
        $query .= " ORDER BY offtime ASC, p.productid ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT 999 ";
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}

		return false;

    } */

	// 閃殺 20181128 活動用
    public function product_flash_list_all($hostid='') {
        // global $db, $config;
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = $this->set_sort();
		$set_sort = false;

		//搜尋
		$set_search = $this->product_list_set_search($hostid);

		//SQL指令
		$query_count = " SELECT count(*) as num ";
        $query_record = "SELECT distinct(p.productid) as ppp, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`, p.thumbnail_url , pt.filename as thumbnail2, p.* ";
		// Not By Channel
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid2 = pt.ptid
			AND pt.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
            AND p.switch='Y'
            AND p.display='Y'
			AND p.is_flash = 'Y'
			AND p.lb_used != 'Y'
		";

		if(!empty($hostid)) {
		    $query.=" AND p.vendorid='${hostid}' ";
		}

		$query .= $set_search['sub_search_query'];
        $query .= " ORDER BY offtime ASC, p.productid ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT 999 ";
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}

		return false;

    }

	/*
	* 跑馬燈
	*/
	public function get_marquee() {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query = "SELECT *
			FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}ad`
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND acid = '2'
				AND switch = 'Y'
				AND adid IS NOT NULL
				and promotetype = 'M'
				and ontime <= NOW()
				and offtime > NOW()
				ORDER BY ontime desc
			";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'];
			return $results;
		}
		return false;
    }


	/*  ad broadcast */
	public function getBroadcastInfo($lat,$lng) {
		global $db, $config;

		$query = " SELECT * FROM `{$config['db'][2]['dbname']}`.`v_saja_ad_broadcast_rt` WHERE
			prefixid = '{$config['default_prefix_id']}'
			AND acid = '5'
			AND adid > 0
			AND promotetype = 'P'
			AND NOW() between ontime and offtime
			AND DATE_FORMAT(NOW(),'%H:%i:%S') between broadcast_begin_time and broadcast_end_time
			ORDER BY abid asc ";
		$table = $db->getQueryRecord($query);
		if(empty($table['table']['record'][0]['adid'])) {
		   return "";
		}
		return $table['table']['record'];
	}

	//Add By Thomas 20151021
	//取得中標者及其中標資訊
	public function getBidWinnerInfo($productid) {
		global $db, $config;
		// 得標者
		if(empty($productid)) {
			error_log("[M/product/getBidWinner] Empty productid !! ");
			return false;
		}
		$query="SELECT h.userid, h.productid, h.nickname, h.price, h.src_ip ".
			  ",(select name from `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}product` where productid=h.productid LIMIT 1 ) as prodname ".
			  ",(select uid from `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}sso` where ssoid=(SELECT ssoid FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sso_rt` WHERE userid=h.userid and switch='Y' LIMIT 1 ) and switch='Y' and name='weixin' LIMIT 1) as openid ".
			  ",(select uid2 from `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}sso` where ssoid=(SELECT ssoid FROM `{$config['db'][0]["dbname"]}`.`{$config['default_prefix']}user_sso_rt` WHERE userid=h.userid and switch='Y' LIMIT 1 ) and switch='Y' and name='weixin' LIMIT 1) as unionid ".
			  ",(select name from saja_channel.saja_province where switch='Y' and provinceid=(select provinceid from saja_user.saja_user_profile where userid=h.userid) and switch='Y' LIMIT 1) as comefrom ".
			  " FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h ".
			  " WHERE (h.productid, h.price) = (SELECT productid, min_price FROM  `{$config['db'][4]["dbname"]}`.`v_min_unique_price` WHERE productid='{$productid}' LIMIT 1) ";
		$table = $db->getQueryRecord($query);
		if(!empty($table['table']['record'])) {
		  return $table['table']['record'];
		}
		return false;
	}

	public function getProdList($sIdx='', $perPage='',$lPrice='', $orderBy=' offtime asc ', $pcid) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query = " SELECT distinct(p.productid), p.retail_price, p.name, unix_timestamp(p.offtime) as offtime,
						 p.thumbnail_url, p.ptid, pt.filename, max(pcr.pcid) as pcid , sp.storeid, cs.channelid, p.limitid ";
		$query.= " FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid
			AND cs.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr ON
			 p.prefixid = pcr.prefixid
			AND p.productid = pcr.productid
			AND pcr.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category` pc ON
			p.prefixid = pc.prefixid
			AND pcr.pcid = pc.pcid
			AND pc.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		WHERE p.prefixid = '{$config['default_prefix_id']}'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND (
				(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
				OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N')
			)
			AND p.is_flash != 'Y'
			AND p.closed='N'
			AND p.display='Y'
			AND p.locked='N'
			AND p.lb_used != 'Y'
			AND sp.productid IS NOT NULL
			AND cs.channelid IS NOT NULL
			AND pc.layer = 1 ";

		if(!empty($lPrice)) {
		   $query.= " AND p.retail_price >=".$lPrice;
		}
		if(!empty($pcid)) {
		   $query.= " AND pcr.pcid = ".$pcid;
		}
		// 防止重複出現(不分區 pcid=0, 分類pcid>0 , join出來會有兩筆)
		$query.=" GROUP BY p.productid, p.retail_price, p.name, unix_timestamp(p.offtime) ,
				 p.thumbnail_url, p.ptid, pt.filename, sp.storeid, cs.channelid ";

		if(!empty($orderBy)) {
		   $query.= " ORDER BY ".$orderBy;
		}

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		// $perpage = $perPage;

		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			if(!empty($perpage) && !empty($sIdx)) {
				$query_limit.= " LIMIT ".$sIdx.",".$perpage;
			} else {
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			error_log("[m/product/getProdList]:".$query . $query_limit);
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($table['table']['record'][$tk]['productid']);
				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];

				// 取得商品限定相關資料
				$product_limit = $this->get_product_limited($table['table']['record'][$tk]['limitid']);

				$table['table']['record'][$tk]['plname'] = $product_limit['plname'];
				$table['table']['record'][$tk]['mvalue'] = $product_limit['mvalue'];
				$table['table']['record'][$tk]['svalue'] = $product_limit['svalue'];
				$table['table']['record'][$tk]['kind'] = $product_limit['kind'];
				$table['table']['record'][$tk]['plpic'] = $product_limit['plpic'];
				$table['table']['record'][$tk]['colorcode'] = $product_limit['colorcode'];

			}
			$table['table']['page'] = $page;
			return $table['table'];
		}

	}

	public function getMoneySaved() {
		global $config;
		$db=new mysql($config["db2"]);
		$db->connect();

		$query="select ROUND(SUM(p.retail_price-pgp.price),2) as amount,
					 (select count(userid) from saja_user where switch='Y') as num_member
			   from (`saja_shop`.`saja_pay_get_product` `pgp`
			   join `saja_shop`.`saja_product` `p` on((`pgp`.`productid` = `p`.`productid`)))
			  where ((`pgp`.`switch` = 'Y') and (`p`.`closed` = 'Y') and (`p`.`switch` = 'Y'))";
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
	}

	public function getAdBannerList($type=1, $limit=5, $promotetype='') {
		global $db, $config;
		$query="";
		switch($type) {
			case 1 :// 首頁的橫幅廣告(App用)
					$query.=" select seq, qrcode_url as action, thumbnail_url, thumbnail, thumbnail2
							 from saja_channel.saja_ad
							where switch='Y' and acid=1
							  and NOW() between ontime and offtime";

					if(!empty($promotetype)) {
						$query.=" and promotetype = '".$promotetype."'";
					}

					$query.=" order by seq asc ";
					break;

			case 2 :// 兌換商城中 "附近" 的廣告
					$query.=" select seq, qrcode_url as action, thumbnail_url
								 from saja_channel.saja_ad
								where switch='Y' and acid=3
								  and NOW() between ontime and offtime
								ORDER BY RAND() LIMIT 1";
					break;
			case 3 :// 殺友專區中 "簽到" 的廣告
					$query.=" select seq, qrcode_url as action, thumbnail_url
								 from saja_channel.saja_ad
								where switch='Y' and acid=4
								  and NOW() between ontime and offtime
								ORDER BY RAND() LIMIT 3";
					break;
			case 4 :// 首頁的橫幅廣告(web用)
					$query.=" select adid, name, qrcode_url as action, thumbnail, thumbnail2, thumbnail_url
								from saja_channel.saja_ad
								where switch='Y' and acid=1
								and NOW() between ontime and offtime";

					if(!empty($promotetype)) {
						   $query.=" and promotetype = '".$promotetype."'";
					}

					$query.=" order by seq asc ";
					break;
			default:break;
		}

		// error_log($query);
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//取得分頁資料
			$page = $db->recordPage($num, $this);
			if($type==1) {
				if(!empty($perpage) && !empty($sIdx)) {
					$query_limit.= " LIMIT ".$sIdx.",".$perpage;
				}else{
					$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
				}
			}
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		} else {
			return false;
		}
	}

	/*
	 *	統計結標人數差額
	 *	$productid		int		商品編號
	 */
	public function getNumBidsGap($productid) {
		global $db, $config;
		// 得標者

		if(empty($productid)) {
			error_log("[M/product/getNumBidsGap] Empty productid !! ");
			return false;
		}

		$query = "SELECT COUNT(shid) as num
					FROM `{$config['db'][4]["dbname"]}`.`{$config['default_prefix']}history` h
					WHERE switch='Y' AND spointid >0 AND productid = '{$productid}'";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

	/*
	 *	商品圖示清單
	 *	$productid		int		商品編號
	 */
	public function getProductThumbnailList($productid) {
		global $db, $config;
		// 得標者

		if(empty($productid)) {
			error_log("[M/product/getProductThumbnailList] Empty productid !! ");
			return false;
		}

		$query = "SELECT pt.filename
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
					LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ON
						p.prefixid = pt.prefixid
						AND p.ptid = pt.ptid
						AND pt.switch = 'Y'
					WHERE p.productid = '{$productid}'";

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
	}

    /*
	 * 商品資訊 小程序用 2017-01-26
	 */
	public function get_info2($id) {
        global $db, $config;
		// 20160412 By Thomas
		// 加一個中標者userid(final_winner_userid)
		$query ="SELECT p.*, unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`,
		         IFNULL(pt.filename,'') as thumbnail,
				 IFNULL(pgp.userid,'') as final_winner_userid,
				 IFNULL(pgp.price,0) as final_bid_price
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
		     ON p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		     ON	p.productid = pgp.productid
            AND pgp.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.productid = '{$id}'
			AND p.display = 'Y'
			AND p.switch = 'Y' ";

		$recArr = $db->getQueryRecord($query);

		if(!empty($recArr['table']['record'][0]) )
		{
			//中标处理费 = 处理费% * 市價 * 兌換比率
			$recArr['table']['record'][0]['process_fee_ratio']=$recArr['table']['record'][0]['process_fee'];
			$process_fee = ( (float)$recArr['table']['record'][0]['process_fee']/100 ) * (float)$recArr['table']['record'][0]['retail_price']  * $config['sjb_rate'];
			$recArr['table']['record'][0]['process_fee'] = strval(round($process_fee, 0));

			// 红利回馈
			if($recArr['table']['record'][0]['bonus_type'] == 'ratio') {
                $recArr['table']['record'][0]['bonus'] = number_format($recArr['table']['record'][0]['bonus'],0,".","");
			} elseif($recArr['table']['record'][0]['bonus_type'] == 'value') {
				$recArr['table']['record'][0]['bonus'] = (float)sprintf("%0.2f", $recArr['table']['record'][0]['bonus']);
			} else {
				$recArr['table']['record'][0]['bonus'] = 0;
			}

			// 店点回馈
			if($recArr['table']['record'][0]['gift_type'] == 'ratio') {
				$recArr['table']['record'][0]['gift'] = (float)sprintf("%0.2f", $recArr['table']['record'][0]['gift']);
			} elseif($recArr['table']['record'][0]['gift_type'] == 'value') {
				$recArr['table']['record'][0]['gift'] = (float)sprintf("%0.2f", $recArr['table']['record'][0]['gift']);
			} else {
				$recArr['table']['record'][0]['gift'] = 0;
			}

			//市價
			$recArr['table']['record'][0]['retail_price'] = str_replace(",","",(number_format($recArr['table']['record'][0]['retail_price']))); //number_format($recArr['table']['record'][0]['retail_price'], 2);

			//底價
			$recArr['table']['record'][0]['price_limit'] = str_replace(",","",(number_format($recArr['table']['record'][0]['price_limit'], 2)));

			//下标手续费
			$recArr['table']['record'][0]['saja_fee'] =  round($recArr['table']['record'][0]['saja_fee'], 0);

			//商品简介
			$description = (!empty($recArr['table']['record'][0]['description']) ) ? $recArr['table']['record'][0]['description'] : '空白';
			$recArr['table']['record'][0]['description'] = html_decode($description);

			// Relation product_rule_rt 下標條件關聯
			$rule_rt = $this->get_product_rule_rt($id);

			$recArr['table']['record'][0]['srid'] = $rule_rt['srid'];
			$recArr['table']['record'][0]['value'] = $rule_rt['value'];

			$recArr['table']['record'][0]['offtime']=(int)$recArr['table']['record'][0]['unix_offtime'];

			return $recArr['table']['record'][0];
		}
		return false;
    }

	/*
	 * 商戶商品列表 2017-03-20
	 *
	 *	$channelid		int		地區編號
	 *	$storeid		int		店家編號
	 *	$flash			int		商品種類
	 *
	 */
	public function enterprise_product_list($channelid='' ,$storeid='', $flash='') {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT distinct(p.productid) as ppid, unix_timestamp(p.offtime) as offtime, unix_timestamp() as `now`, p.thumbnail_url , pt.filename, sp.storeid, cs.channelid, p.* ";
        $query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			{$set_search['join_query']}
			LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
				p.prefixid = sp.prefixid
				AND p.productid = sp.productid
				AND sp.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
				p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
				sp.prefixid = cs.prefixid
				AND sp.storeid = cs.storeid
				AND cs.switch = 'Y'
			 WHERE  p.prefixid = '{$config['default_prefix_id']}'
				AND p.is_flash = 'Y'
				AND sp.productid IS NOT NULL
			";
			if(!empty($channelid) && $channelid <> "0") {
				$query.=" AND cs.channelid = '{$channelid}' ";
		    }
			if(!empty($storeid)) {
				$query.= " AND sp.storeid = '".$storeid."'
				";
			}

		$query .= " AND p.switch = 'Y'";
		$query .= ($set_sort) ? $set_sort : " ORDER BY offtime DESC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($config['max_page']);

			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$productid = $tv['productid'];

				//红利回馈
				if($tv['bonus_type'] == 'ratio') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'%';
				} elseif($tv['bonus_type'] == 'value') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'点';
				} else {
					$table['table']['record'][$tk]['bonus'] = 0;
				}

				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($productid);

				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];
			}

			return $table;
		}

		return false;
    }

    /*
	 *	商戶商品資料
	 *	$pid		int		商品編號
	 */
	public function enterprise_product_detail($pid)	{
        global $db, $config;
		$query ="SELECT p.*, unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`, unix_timestamp(p.ontime) as unix_ontime, IFNULL(pt.filename,'') as thumbnail
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
		  ON p.prefixid = pt.prefixid AND p.ptid = pt.ptid AND pt.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.productid = '{$pid}'
			AND p.switch = 'Y' ";

		$recArr = $db->getQueryRecord($query);

		if(!empty($recArr['table']['record'][0])) {
			//市價
			$recArr['table']['record'][0]['retail_price'] = str_replace(",","",(number_format($recArr['table']['record'][0]['retail_price']))); //number_format($recArr['table']['record'][0]['retail_price'], 2);
			$recArr['table']['record'][0]['offtime']=(int)$recArr['table']['record'][0]['unix_offtime'];

			return $recArr['table']['record'][0];
		}
		return false;
    }

	/*
	 *	商品關鍵字查詢
	 *	$prodKeyword			varchar				查詢關鍵字
	 *	$prodType				varchar				商品類別
	 *	$prodClose				varchar				商品下標狀態 (Y:已結標,N:競標中,NB:流標)
	 *	$sIdx					varchar				頁碼
	 *	$perPage				varchar				每頁個數
	 */
	public function getProdSearch($prodKeyword='', $prodType='', $prodClose='', $sIdx='', $perPage='') {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();

		$query_count = " SELECT count(*) as num from ( select count(*) as num ";

		$query_record = " SELECT distinct(p.productid), p.retail_price, p.name, unix_timestamp(p.offtime) as unix_offtime, p.offtime,
						p.thumbnail_url, p.ptid, pt.filename, max(pcr.pcid) as pcid , sp.storeid, cs.channelid, p.limitid, p.lb_used, p.ptype ";

		$query = " FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
				p.prefixid = sp.prefixid
				AND p.productid = sp.productid
				AND sp.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
				sp.prefixid = cs.prefixid
				AND sp.storeid = cs.storeid
				AND cs.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr ON
				 p.prefixid = pcr.prefixid
				AND p.productid = pcr.productid
				AND pcr.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category` pc ON
				p.prefixid = pc.prefixid
				AND pcr.pcid = pc.pcid
				AND pc.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
				p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'
			WHERE p.prefixid = '{$config['default_prefix_id']}'
				AND p.is_flash != 'Y'
				AND p.display='Y'
				AND sp.productid IS NOT NULL
				AND cs.channelid IS NOT NULL
				AND pc.layer = 1
				AND p.switch = 'Y'
				AND p.ptype =0
				AND unix_timestamp() >= unix_timestamp(p.ontime)
				AND (
					(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
					OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N')
				)
		";

		if(!empty($prodKeyword)) {
		   $query.= " AND (p.name like '%".$prodKeyword."%' or p.description like '%".$prodKeyword."%')";
		}
		if(!empty($prodType)) {
		   $query.= " AND pc.pcid = ".$prodType;
		}
		if(!empty($prodClose)) {
		   $query.= " AND p.closed = '".$prodClose."'";
		}
		// 防止重複出現(不分區 pcid=0, 分類pcid>0 , join出來會有兩筆)
		$query.=" GROUP BY p.productid, p.retail_price, p.name, unix_timestamp(p.offtime), p.thumbnail_url, p.ptid, pt.filename, sp.storeid, cs.channelid ";

		$query_count_last = " ) os ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query . $query_count_last);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			if(!empty($perpage) && !empty($sIdx)) {
				$query_limit.= " ORDER BY p.seq ASC, p.offtime ASC LIMIT ".$sIdx.",".$perpage;
			}else{
				$query_limit = " ORDER BY p.seq ASC, p.offtime ASC LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=IMG_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($table['table']['record'][$tk]['productid']);

				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];

				// 取得商品限定相關資料
				$product_limit = $this->get_product_limited($table['table']['record'][$tk]['limitid']);

				$table['table']['record'][$tk]['plname'] = $product_limit['plname'];
				$table['table']['record'][$tk]['mvalue'] = $product_limit['mvalue'];
				$table['table']['record'][$tk]['svalue'] = $product_limit['svalue'];
				$table['table']['record'][$tk]['kind'] = $product_limit['kind'];
				$table['table']['record'][$tk]['plpic'] = $product_limit['plpic'];
				$table['table']['record'][$tk]['colorcode'] = $product_limit['colorcode'];

			}
			$table['table']['page'] = $page;
			return $table['table'];
		}else{
			return false;
		}
	}

	/*
	 *	取得特定分類商品
	 *	$pcid							int					分類編號
	 *	$storeid						int					商家編號
	 *	$channelid						int					經銷商編號
	 *	$arr_exclude_storeids			array				不顯示的相關商家編號
	 *	$p								int					頁碼
	 */
	public function getCategoryProductList($channelid='', $storeid='', $arr_exclude_storeids='', $pcid='', $p=1, $userid='') {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();
        /*
		if(empty($arr_exclude_storeids) && empty($storeid) && empty($storeid)) {
			$arr_exclude_storeids=$config['exclude_storeids'];
		}
		*/
		// error_log("[m/getCategoryProductList] userid : ".$userid);
		if (!empty($userid)) {
			$query = "SELECT count(*) as count
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
			where
				`prefixid` = '{$config['default_prefix_id']}'
				AND userid = '{$userid}'
				AND switch = 'Y'
			";
			$countdata = $db->getQueryRecord($query);
			$total = $countdata['table']['record'][0]['count'];
		}
		if($userid=='28')
		   $total = 0;

		//排序
		$set_sort = $this->set_sort();

		//SQL指令
		$query_count = " SELECT count(*) as num ";

		$query_record = "SELECT unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`, pt.filename as thumbnail
		                        , p.ptid, sp.storeid, cs.channelid, p.productid, p.name, p.ptid, p.offtime, p.is_flash, pc.pcid, p.limitid, p.lb_used ";

		$query = " FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid ";
		/*
		if (empty($userid) || $userid=='28' || $userid =='6387' || $userid =='7829' || $userid=='1705'){
			$query.= " AND cs.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
				p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pc ON
				p.prefixid = pc.prefixid
				AND p.productid = pc.productid
				AND pc.switch = 'Y'
			WHERE
				p.prefixid = '{$config['default_prefix_id']}'
				AND p.is_flash != 'Y'
				AND sp.productid IS NOT NULL
				AND cs.channelid IS NOT NULL
				AND p.closed = 'N'
				AND p.display = 'Y'
				AND p.switch = 'Y'
				AND p.ptype = 0
				AND unix_timestamp() >= unix_timestamp(p.ontime)
				AND (
					(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
					OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N'))
			";
		} else {
			$query.= " AND cs.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
				p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pc ON
				p.prefixid = pc.prefixid
				AND p.productid = pc.productid
				AND pc.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited` pl ON
				p.limitid = pl.plid
				AND pl.switch = 'Y'
			WHERE
				p.prefixid = '{$config['default_prefix_id']}'
				AND p.is_flash != 'Y'
				AND sp.productid IS NOT NULL
				AND cs.channelid IS NOT NULL
				AND p.closed = 'N'
				AND p.display = 'Y'
				AND p.switch = 'Y'
				AND p.ptype = 0
				AND unix_timestamp() >= unix_timestamp(p.ontime)
				AND (
					(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
					OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N'))
				AND ( (p.limitid = 0) OR ((p.limitid !=0) AND (pl.mvalue = 0) AND (pl.svalue <= {$total})) OR ((p.limitid !=0) AND (pl.svalue = 0) AND (pl.mvalue >= {$total})))
			";
		}
		*/
		$query.= " AND cs.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
				p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pc ON
				p.prefixid = pc.prefixid
				AND p.productid = pc.productid
				AND pc.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited` pl ON
				p.limitid = pl.plid
				AND pl.switch = 'Y'
			WHERE
				p.prefixid = '{$config['default_prefix_id']}'
				AND p.is_flash != 'Y'
				AND sp.productid IS NOT NULL
				AND cs.channelid IS NOT NULL
				AND p.closed = 'N'
				AND p.display = 'Y'
				AND p.switch = 'Y'
				AND p.ptype = 0
				AND unix_timestamp() >= unix_timestamp(p.ontime)
				AND (
					(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
					OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N'))
				AND ( (p.limitid = 0) OR ((p.limitid !=0) AND (pl.mvalue = 0) AND (pl.svalue <= {$total})) OR ((p.limitid !=0) AND (pl.svalue = 0) AND (pl.mvalue >= {$total})))
			";
		$suserid =['1705'];
		$inuserid=$userid;
		$ticket="";
		if ($pcid==166) $ticket=" and p.targetid= '{$userid}'";
		$query.= $ticket;
		if(!in_array($inuserid,$suserid)) {
			$query.= " AND p.productid != '179043'";
		}else{
			$query.= " AND p.productid != '147373'";
		}
		if($pcid != 0) {
			$query.= " AND pc.`pcid` = '{$pcid}' ";
		}else{
			$query.= " AND p.`display_all` = 'Y' ";
		}

		if(!empty($channelid)) {
            $query.= " AND cs.channelid IN ('{$channelid}','0') ";
        }

		if(!empty($storeid)) {
			// 只能看到自己上架的商品
			$query.= " AND sp.storeid='{$storeid}' ";
		}
		/*
		if ($userid != '1705' ){
			$query.= " AND p.`productid` != '147292' ";
		}
		*/
		/*
		if(is_array($arr_exclude_storeids) && count($arr_exclude_storeids)>0) {
		   	$cnt_total=count($arr_exclude_storeids);
			if($cnt_total>0) {
				$query.= " AND sp.storeid NOT IN (";
				for($i=0;$i<$cnt_total;++$i) {
					if($i==0) {
						$query.="'".$arr_exclude_storeids[$i]."'";
					} else {
						$query.=",'".$arr_exclude_storeids[$i]."'";
					}
				}
				$query.=") ";
			}
		}
        */
		$query .= $set_search['sub_search_query'];

		$query .= ($set_sort) ? $set_sort : " GROUP BY p.productid ORDER BY p.ordertype asc, p.seq ASC, p.offtime ASC, p.productid ASC ";


		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		//$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$num = count($getnum['table']['record'] > 0) ? count($getnum['table']['record']) : 0;

		// error_log("[m/category_product_list] 1 :".$getnum['table']['record'][0]['num'] );

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			$start = ($p-1)*($config['max_page']);
			// 防止LIMIT 有負值
			if($start<0)
			   $start=0;
			$query_limit = " LIMIT ". $start .",". $config['max_page'];
			if ($p == "all") {
				$query_limit = "";
			}

			// 票券類區商品看到30檔
			if(($userid!=28 && $userid!=2788) && $pcid==166) {
			   $query_limit = " LIMIT 0,30 ";
			}

			error_log("[getCategoryProductList]userid: {$userid} , pcid:".$pcid);

			//測試下標功能
			//if(($userid!=1705) && $pcid==96) {
		 	//   $table['table']['record'] = '';
		    //}else
			if(($userid!=28 && $userid!=2788) && $pcid==166 && $start>0) {
		 	   $table['table']['record'] = '';
		    } else {
			   //取得資料
			   error_log("[m/getCategoryProductList] : ".$query_record . $query . $query_limit);
			   $table = $db->getQueryRecord($query_record . $query . $query_limit);
			}
		} else {
			$table['table']['record'] = '';
		}



		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$productid = $tv['productid'];
				$tv['offtime']=$tv['unix_offtime'];
				//红利回馈
				if($tv['bonus_type'] == 'ratio') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'%';
				} elseif($tv['bonus_type'] == 'value') {
					$table['table']['record'][$tk]['bonus'] = sprintf("%1\$u",((float)sprintf("%0.2f", $tv['bonus']))) .'點';
				} else {
					$table['table']['record'][$tk]['bonus'] = 0;
				}

				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($productid);

				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];

				// 取得商品限定相關資料
				$product_limit = $this->get_product_limited($tv['limitid']);

				$table['table']['record'][$tk]['plname'] = $product_limit['plname'];
				$table['table']['record'][$tk]['mvalue'] = $product_limit['mvalue'];
				$table['table']['record'][$tk]['svalue'] = $product_limit['svalue'];
				$table['table']['record'][$tk]['kind'] = $product_limit['kind'];
				$table['table']['record'][$tk]['plpic'] = $product_limit['plpic'];
				$table['table']['record'][$tk]['colorcode'] = $product_limit['colorcode'];

			}

			return $table;
		}

		return false;
    }

	public function getProductById($productid, $display='Y', $ontime_chk='Y') {
		if(empty($productid)) {
			return false;
		}
	    $redis_key='getProductById:'.$productid."|".$display."|".$ontime_chk;
	    $redis = getRedis();
        $jsonStr = $redis->get($redis_key);

		// if(!empty($jsonStr)) {
                   // error_log("[m/getProductById] get product : ".$productid." from cache ...");
		   // return json_decode($jsonStr,true);
	    // }

		global $db, $config;

        // 存在redis的資料 需可共用
        // (下標時會檢查商品的結標時間, 是否locked, 可下標次數限制 ... 等等)
        // 所以把 saja_product 的所有欄位都存進去 讓程式去撈cache, 減少撈DB
		// 2019/04/05 移除 store和channel的join, 暫時都回傳 0
		// 另新增join 下標規則(saja_product_rule_rt.srid)
        $query = " SELECT p.*,
                      unix_timestamp(p.offtime) as offtime, unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`,
                      IFNULL(pt.filename,'') as thumbnail,
                      IFNULL(pgp.userid,'') as final_winner_userid,
                      IFNULL(pgp.price,0) as final_bid_price,
					  0 as storeid,
					  0 as channelid,
                      pt2.filename as thumbnail2,
                      pt3.filename as thumbnail3,
                      pt4.filename as thumbnail4,
					  IFNULL(rule.srid,'any_saja') as srid,
                      IFNULL(rule.value,0) as srvalue
                FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
				LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
                       ON p.prefixid = pt.prefixid
                      AND p.ptid = pt.ptid
                      AND pt.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt2
                       ON p.prefixid = pt2.prefixid
                      AND p.ptid2 = pt2.ptid
                      AND pt2.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt3
                       ON p.prefixid = pt3.prefixid
                		  AND p.ptid3 = pt3.ptid
                			AND pt3.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt4
                      ON p.prefixid = pt4.prefixid
                		 AND p.ptid4 = pt4.ptid
                		 AND pt4.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
                      ON	p.productid = pgp.productid
                     AND pgp.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt` rule
				      ON p.prefixid=rule.prefixid
                     AND p.productid = rule.productid
                     AND rule.switch='Y'
				WHERE
                     p.prefixid = '{$config['default_prefix_id']}'
                     AND p.productid = '{$productid}'
                     AND p.switch = 'Y'
            ";
		/*
        $query = "SELECT  p.productid, p.name, p.description, p.rule, p.retail_price, p.locked,
  				            p.thumbnail_url, p.ontime, p.offtime, p.saja_limit, p.usereach_limit, p.saja_fee,
              				p.process_fee, p.closed, p.totalfee_type, p.pay_type, p.checkout_money, p.display,
                      unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`,
                      IFNULL(pt.filename,'') as thumbnail,
                      IFNULL(pgp.userid,'') as final_winner_userid,
                      IFNULL(pgp.price,0) as final_bid_price,
                      sprt.storeid,
                      csrt.channelid,
                      pt2.filename as thumbnail2,
                      pt3.filename as thumbnail3,
                      pt4.filename as thumbnail4
                FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
                       ON p.prefixid = pt.prefixid
                      AND p.ptid = pt.ptid
                      AND pt.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt2
                       ON p.prefixid = pt2.prefixid
                      AND p.ptid2 = pt2.ptid
                      AND pt2.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt3
                       ON p.prefixid = pt3.prefixid
                		  AND p.ptid3 = pt3.ptid
                			AND pt3.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt4
                      ON p.prefixid = pt4.prefixid
                		 AND p.ptid4 = pt4.ptid
                		 AND pt4.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
                      ON	p.productid = pgp.productid
                     AND pgp.switch = 'Y'
                LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sprt
                      ON p.productid = sprt.productid
                     AND sprt.switch='Y'
                LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` csrt
                      ON sprt.storeid = csrt.storeid
                     AND csrt.switch='Y'
                WHERE
                     p.prefixid = '{$config['default_prefix_id']}'
                     AND p.productid = '{$productid}'
                     AND p.switch = 'Y'
            ";
        */
		if($display != "") {
			$query = $query." AND p.display = '{$display}'";
		} else {
			$query = $query;
		}

		if($ontime_chk == 'N') {
			$query = $query;
		} else {
			$query = $query." AND unix_timestamp() >= unix_timestamp(p.ontime)";
		}

		$query= $query." LIMIT 1";

		$recArr = $db->getQueryRecord($query);

		if(!empty($recArr['table']['record'][0])) {

			//中标处理费 = 处理费% * 市價 * 兌換比率
			$recArr['table']['record'][0]['process_fee_ratio']=$recArr['table']['record'][0]['process_fee'];
			$process_fee = ( (float)$recArr['table']['record'][0]['process_fee']/100 ) * (float)$recArr['table']['record'][0]['retail_price']  * $config['sjb_rate'];
			$recArr['table']['record'][0]['process_fee'] = strval(round($process_fee, 0));

			//市價
			$recArr['table']['record'][0]['retail_price'] = str_replace(",","",(number_format($recArr['table']['record'][0]['retail_price']))); //number_format($recArr['table']['record'][0]['retail_price'], 2);

			//下标手续费
			$recArr['table']['record'][0]['saja_fee'] =  round($recArr['table']['record'][0]['saja_fee'], 0);

			//商品简介
			$description = (!empty($recArr['table']['record'][0]['description']) ) ? $recArr['table']['record'][0]['description'] : '空白';
			$recArr['table']['record'][0]['description'] = html_decode($description);

			//商品規則
			/*
			$rule = '
			<p>◎ <span style="color: #E32A00">此檔商品限下標20次</span>。</p>
			<p>◎ 得標者須在3天內支付得標處理費完成結帳程序，否則<span style="color: #E32A00">視同放棄</span>，若放棄商品下標時<span style="color: #E32A00">全部支出的費用將不會轉贈鯊魚點</span>。</p>
			<p>◎ 得標者的帳號所支付的所有下標手續費，將<span style="color: #E32A00">不會轉贈成鯊魚點</span>。</p>
			<p>◎ 本商品每次下標需支付殺價幣&nbsp;<span style="color: #000; font-weight: bolder;">$'.$recArr['table']['record'][0]['saja_fee'].'/標</span>。</p>
			<p>◎ 時間到時，出價最低且不與他人重複者得標。</p>
			<p>◎ 得標者需在3日內結帳，違者視同棄標。</p>
			<p>◎ 棄標者不得要求退回已支付手續費。</p>
			<p>◎ <span style="color: #E32A00">得標者須在結帳時支付得標處理費 $'.ceil($recArr['table']['record'][0]['process_fee']+$recArr['table']['record'][0]['checkout_money']).'</span>。</p>
			<p>◎ <span style="color: #E32A00">得標處理費可由已下標手續費100%全額折抵</span>。</p>
			<p>◎ <span style="color: #E32A00">未得標者不損失，手續費'.ceil($recArr['table']['record'][0]['bonus']).'%轉贈鯊魚點</span>。</p>
			<p>◎ 鯊魚點可至商城1:1使用&nbsp; 請安心下標。</p>
			<p>◎ <span style="color: #E32A00">本產品已投保新光產物產品責任保險$200,000,000元。</span></p>
			<p>◎ <span style="color: #E32A00">保單編號：130009AKP0000036。</span></p>';
            */
			// Add By Thomas 2020/01/30
			// user_limit < 999999 者 規則中顯示下標限制為 user_limit 次, 超過999999則視為不限下標次數
			if($recArr['table']['record'][0]['user_limit']>=999999) {
			   $rule = '<p>◎ <span style="color: #E32A00">本檔商品不限制用戶下標次數</span>。</p>'; 	
			} else if($recArr['table']['record'][0]['user_limit']>0) {
			   $rule = '<p>◎ <span style="color: #E32A00">本檔商品限下標 '.$recArr['table']['record'][0]['user_limit'].' 次</span>。</p>'; 
			}
			$rule.='<p>◎ 得標者須在3天內支付得標處理費完成結帳程序，否則<span style="color: #E32A00">視同放棄</span>，若放棄商品下標時<span style="color: #E32A00">全部支出的費用將不會轉贈鯊魚點</span>。</p>
			<p>◎ 得標者的帳號所支付的所有下標手續費，將<span style="color: #E32A00">不會轉贈成鯊魚點</span>。</p>
			<p>◎ 本商品每次下標需支付殺價幣&nbsp;<span style="color: #000; font-weight: bolder;">$'.$recArr['table']['record'][0]['saja_fee'].'/標</span>。</p>
			<p>◎ 時間到時，出價最低且不與他人重複者得標。</p>
			<p>◎ 得標者需在3日內結帳，違者視同棄標。</p>
			<p>◎ 棄標者不得要求退回已支付手續費。</p>
			<p>◎ <span style="color: #E32A00">得標者須在結帳時支付得標處理費 $'.ceil($recArr['table']['record'][0]['process_fee']+$recArr['table']['record'][0]['checkout_money']).'</span>。</p>
			<p>◎ <span style="color: #E32A00">得標處理費可由已下標手續費100%全額折抵</span>。</p>
			<p>◎ <span style="color: #E32A00">未得標者不損失，手續費'.ceil($recArr['table']['record'][0]['bonus']).'%轉贈鯊魚點</span>。</p>
			<p>◎ 鯊魚點可至商城1:1使用&nbsp; 請安心下標。</p>
			<p>◎ <span style="color: #E32A00">本產品已投保新光產物產品責任保險$200,000,000元。</span></p>
			<p>◎ <span style="color: #E32A00">保單編號：130009AKP0000036。</span></p>'; 			
			$recArr['table']['record'][0]['rule']  = (!empty($recArr['table']['record'][0]['rule']) ) ? $recArr['table']['record'][0]['rule'] : $rule;		
			
			// Relation product_rule_rt 下標條件關聯
			$rule_rt = $this->get_product_rule_rt($productid);

			$recArr['table']['record'][0]['srid'] = $rule_rt['srid'];
			$recArr['table']['record'][0]['value'] = $rule_rt['value'];

			// 取得商品限定相關資料
			$product_limit = $this->get_product_limited($recArr['table']['record'][0]['limitid']);

			$recArr['table']['record'][0]['plname'] = $product_limit['plname'];
			$recArr['table']['record'][0]['mvalue'] = $product_limit['mvalue'];
			$recArr['table']['record'][0]['svalue'] = $product_limit['svalue'];
			$recArr['table']['record'][0]['kind'] = $product_limit['kind'];
			$recArr['table']['record'][0]['plpic'] = $product_limit['plpic'];
			$recArr['table']['record'][0]['colorcode'] = $product_limit['colorcode'];
			$recArr['table']['record'][0]['limit_group'] = $product_limit['limit_group'];


			$recArr['table']['record'][0]['product_offtime']=$recArr['table']['record'][0]['offtime'];
			$recArr['table']['record'][0]['offtime']=(int)$recArr['table']['record'][0]['unix_offtime'];
			// $recArr['table']['record'][0]['thumbnail']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail'];
            $recArr['table']['record'][0]['thumbnail']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail'];

			if (!empty($recArr['table']['record'][0]['thumbnail2'])) {
				// $recArr['table']['record'][0]['thumbnail2']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail2'];
				$recArr['table']['record'][0]['thumbnail2']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail2'];
			}
			if(!empty($recArr['table']['record'][0]['thumbnail3'])) {
				// $recArr['table']['record'][0]['thumbnail3']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail3'];
			    $recArr['table']['record'][0]['thumbnail3']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail3'];

			}
			if(!empty($recArr['table']['record'][0]['thumbnail4'])) {
				// $recArr['table']['record'][0]['thumbnail4']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail4'];
				$recArr['table']['record'][0]['thumbnail4']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail4'];
			}

			$redis->set($redis_key, json_encode($recArr['table']['record'][0]));
			return $recArr['table']['record'][0];
		}

		return false;
	}

	// Add By Thomas 2019/01/04
    // 結標用單元功能
    // 取得商品訊
    public function getProducts($arrCond) {

		global $db, $config;
        $query = "SELECT *, unix_timestamp(offtime) as offtime
                    FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
                   WHERE prefixid = '{$config['default_prefix_id']}' ";
        if(!empty($arrCond['is_flash'])) {
           $query.=" AND is_flash='{$arrCond['is_flash']}' ";
        }
        if(!empty($arrCond['is_big'])) {
           $query.=" AND is_big='{$arrCond['is_big']}' ";
        }
        if(!empty($arrCond['switch'])) {
           $query.=" AND switch='{$arrCond['switch']}' ";
        }
        if(!empty($arrCond['display'])) {
           $query.=" AND display='{$arrCond['display']}' ";
        }
		if(!empty($arrCond['productid'])) {
           $query.=" AND productid='{$arrCond['productid']}' ";
        }
        if(!empty($arrCond['closed'])) {
           $query.=" AND closed='{$arrCond['closed']}' ";
        }
        if(!empty($arrCond['bid_type'])) {
           $query.=" AND bid_type='{$arrCond['bid_type']}' ";
        }
        if(!empty($arrCond['totalfee_type'])) {
           $query.=" AND totalfee_type='{$arrCond['totalfee_type']}' ";
        }
        if($arrCond['_on_sale']) {
           $query.=" AND (
           (unix_timestamp(offtime)-5 between 0 AND unix_timestamp())
            OR
           (unix_timestamp(offtime) = 0 AND locked = 'Y')
           ) ";
        }
        $table = $db->getQueryRecord($query);
        if(!empty($table['table']['record'])) {
            return $table['table']['record'];
        } else {
            return false;
        }
	}

    //取得得標者資料
	public  function getWinner($productid='') {
		global $db, $config;
        if(empty($productid)) {
           return false;
		}
        $query = " SELECT *
                FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
                WHERE
                prefixid = '{$config['default_prefix_id']}'
                AND productid = '{$productid}'
                AND type = 'bid'
                AND switch = 'Y'
                GROUP BY price
                HAVING COUNT(*) = 1
                ORDER BY price ASC
                LIMIT 1
		";
		$table = $db->getQueryRecord($query);
        if(!empty($table['table']['record'])) {
            return $table['table']['record'];
        } else {
            return false;
        }
	}

    // 寫入得標資料
    public function createPayGetData($productid, $userid, $price, $nickname) {
		global $db, $config;
		error_log("[m/product/changeProductStatus] productid:".$productid.",userid:".$userid.",price:".$price.",nickname:".$nickname);
		if(empty($productid) || empty($userid) || empty($price) || empty($nickname)) {
			return false;
		}
		$query = "
			INSERT INTO `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product`
			(
				prefixid,
				productid,
				userid,
				price,
				nickname,
				bid_price_total,
				insertt
			)
            SELECT * FROM (SELECT '{$config['default_prefix_id']}',
				'{$productid}',
				'{$userid}',
				'{$price}' as price,
				'{$nickname}',
				'{$price}',
				NOW()) AS tmp
			WHERE NOT EXISTS (
			    SELECT userid FROM  `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` WHERE productid = '{$productid}' and userid = '{$userid}'
			)";
		error_log("[m/product/createPayGetData] : ".$query);
		return $db->query($query);
    }

    // 修改商品資訊
    public function updateProduct($arrUpd, $arrCond) {
		global $db, $config;
		$query = "UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
					SET  prefixid ='saja' ";
		if(!empty($arrUpd['closed'])) {
		  $query.=", closed='{$arrUpd['closed']}' ";
		}
		if(!empty($arrUpd['switch'])) {
		   $query.=", switch='{$arrUpd['switch']}' ";
		}
		if(!empty($arrUpd['offtime'])) {
		  $query.=", offtime='{$arrUpd['offtime']}' ";
		}
		if(!empty($arrUpd['ontime'])) {
		  $query.=", ontime='{$arrUpd['ontime']}' ";
		}
		$query.= "WHERE prefixid = '{$config['default_prefix_id']}' ";
		if(!empty($arrCond['productid'])) {
		   $query.=" AND productid='${arrCond['productid']}' ";
		}
		if(!empty($arrCond['switch'])) {
		   $query.=" AND switch='{$arrCond['switch']}' ";
		}
		if(!empty($arrCond['closed'])) {
		   $query.=" AND closed='{$arrCond['closed']}' ";
		}
		error_log("[m/product/updateProduct] : ".$query);
		return $db->query($query);
    }

	// 新增商品資訊
	public function createProduct($arrNew) {
		global $db, $config;
		$query =" INSERT INTO `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
				 SET
				`prefixid`		= '{$config["default_prefix_id"]}',
				`name`			= '{$arrNew["name"]}',
				`description`	= '{$arrNew["description"]}',
				`description2`	= '{$arrNew["description2"]}',
				`rule`			= '{$arrNew["rule"]}',
				`retail_price`	= '{$arrNew["retail_price"]}',
				`ptid`			= '{$arrNew["ptid"]}',
				`ptid2`			= '{$arrNew["ptid2"]}',
				`ptid3`			= '{$arrNew["ptid3"]}',
				`ptid4`			= '{$arrNew["ptid4"]}',
				`pftid`			= '{$arrNew["pftid"]}',
				`thumbnail_url`	= '{$arrNew["thumbnail_url"]}',
				`ontime`		= '{$arrNew["ontime"]}',
				`offtime`		= '{$arrNew["offtime"]}',
				`saja_limit`	= '{$arrNew["saja_limit"]}',
				`user_limit`	= '{$arrNew["user_limit"]}',
				`usereach_limit`= '{$arrNew["usereach_limit"]}',
				`saja_fee`		= '{$arrNew["saja_fee"]}',
				`process_fee`	= '{$arrNew["process_fee"]}',
				`price_limit`	= '{$arrNew["price_limit"]}',
				`bonus_type`	= '{$arrNew["bonus_type"]}',
				`bonus`			= '{$arrNew["bonus"]}',
				`gift_type`		= '{$arrNew["gift_type"]}',
				`gift`			= '{$arrNew["gift"]}',
				`vendorid`		= '{$arrNew["vendorid"]}',
				`split_rate`	= '{$arrNew["split_rate"]}',
				`base_value`	= '{$arrNew["base_value"]}',
				`display`		= '{$arrNew["display"]}',
				`bid_type`		= '{$arrNew["bid_type"]}',
				`mob_type`		= '{$arrNew["mob_type"]}',
				`loc_type`		= '{$arrNew["loc_type"]}',
				`is_flash`		= '{$arrNew["is_flash"]}',
				`flash_loc`     = '{$arrNew["flash_loc"]}',
				`flash_loc_map` = '{$arrNew["flash_loc_map"]}',
				`bid_spoint`	= '{$arrNew["bid_spoint"]}',
				`bid_scode`		= '{$arrNew["bid_scode"]}',
				`bid_oscode`	= '{$arrNew["bid_oscode"]}',
				`pay_type`		= '{$arrNew["pay_type"]}',
				`seq`			= '{$arrNew["seq"]}',
				`totalfee_type`	= '{$arrNew["totalfee_type"]}',
				`is_big`		= '{$arrNew["is_big"]}',
				`checkout_type`	= '{$arrNew["checkout_type"]}',
				`checkout_money`= '{$arrNew["checkout_money"]}',
				`insertt`		= now() " ;
		error_log("[m/product/createProduct] : ".$query);
		return $db->query($query);
	}
    // Add End

	/*
	 *	限定條件關聯
	 *	$limitid				int					限定編號
	 */
	public function get_product_limited($limitid) {

        global $db, $config;

		$query ="SELECT pl.name as plname, pl.mvalue, pl.svalue, pl.kind, pl.thumbnail as plpic, c.colorcode, pl.limit_group
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited` pl
		LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}color` c
			ON pl.colorid = c.cid
			AND c.used = 'Y'
			AND c.switch = 'Y'
		WHERE
			pl.plid = '{$limitid}'
			AND pl.used = 'Y'
			AND pl.switch = 'Y'
		";
		$recArr = $db->getQueryRecord($query);

		if(empty($recArr['table']['record'][0])) {
			$recArr['table']['record'][0]['plname'] = '';
			$recArr['table']['record'][0]['mvalue'] = '';
			$recArr['table']['record'][0]['svalue'] = '';
			$recArr['table']['record'][0]['kind'] = '';
			$recArr['table']['record'][0]['plpic'] = '';
			$recArr['table']['record'][0]['colorcode'] = '';
		}

		return $recArr['table']['record'][0];
    }

	/*
	 *	熱門殺價商品列表
	 */
	public function hot_product_list() {
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`, pt.filename as thumbnail
		                        , p.ptid, sp.storeid, cs.channelid, p.productid, p.name, p.offtime, p.thumbnail_url, p.limitid ";
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid ";
		$query.= " AND cs.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.hot_used = 'Y'
			AND p.is_flash != 'Y'
			AND p.ptype =0
			AND sp.productid IS NOT NULL
			AND cs.channelid IS NOT NULL
		";

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq ASC, p.hot_prod DESC, p.offtime ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)){
			$perpage = 12;
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				$productid = $tv['productid'];
				$tv['offtime']=$tv['unix_offtime'];
				//红利回馈
				if($tv['bonus_type'] == 'ratio') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'%';
				} elseif($tv['bonus_type'] == 'value') {
					$table['table']['record'][$tk]['bonus'] = (float)sprintf("%0.2f", $tv['bonus']) .'點';
				} else {
					$table['table']['record'][$tk]['bonus'] = 0;
				}

				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}

				// 取得商品限定相關資料
				$product_limit = $this->get_product_limited($tv['limitid']);

				$table['table']['record'][$tk]['plname'] = $product_limit['plname'];
				$table['table']['record'][$tk]['mvalue'] = $product_limit['mvalue'];
				$table['table']['record'][$tk]['svalue'] = $product_limit['svalue'];
				$table['table']['record'][$tk]['kind'] = $product_limit['kind'];
				$table['table']['record'][$tk]['plpic'] = $product_limit['plpic'];
				$table['table']['record'][$tk]['colorcode'] = $product_limit['colorcode'];
			}

			return $table['table'];
		}

		return false;
    }

    /*
	 *	增加商品點擊數
	 */
    public function add_prod_click($productid='',$num='') {

           global $db, $config;

           if(empty($productid)) {
              return false;
           }
           if(empty($num) || !is_numeric($num)) {
               $num=1;
           }
           $query = " UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` SET hot_prod=hot_prod+".$num.
                    " WHERE productid='${productid}' ";
           if($db->query($query)) {
              return $num;
           } else {
              return false;
           }
    }


	/*
	 *	取得競標中商品某會員下標金額統計清單
	 *	$userid						int					會員編號
	 *	$productid					int					商品編號
	 *	$startprice					int					開始下標價位
	 */
    public function get_user_onbid_list($productid, $userid, $perpage, $type) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();


		$query = "select ROUND(price) as price, count(price) as count
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
		where
			prefixid = '{$config["default_prefix_id"]}'
			AND productid = '{$productid}'
			AND type = 'bid'
			AND productid IS NOT NULL
			AND userid IS NOT NULL
			AND switch = 'Y'
		";

		if($type == 1){
			$query.=" AND userid = '{$userid}' ";
		}

		$query.=" group by price order by price ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}

		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);

			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query. $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table;
		}

		return false;
    }


	/*
	 *	直播殺價商品列表
	 */
	public function lb_product_list() {
        global $db, $config;

		//排序
		$set_sort = $this->set_sort();

		//搜尋
		$set_search = $this->product_list_set_search();

		//SQL指令
		$query_count = " SELECT count(*) as num ";
		$query_record = "SELECT unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`, pt.filename as thumbnail
		                        , p.ptid, sp.storeid, cs.channelid, p.productid, p.name, p.ontime, p.offtime, p.thumbnail_url, p.limitid ";
		$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
		{$set_search['join_query']}
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}store_product_rt` sp ON
			p.prefixid = sp.prefixid
			AND p.productid = sp.productid
			AND sp.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel_store_rt` cs ON
			sp.prefixid = cs.prefixid
			AND sp.storeid = cs.storeid ";
		$query.= " AND cs.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.is_flash != 'Y'
			AND p.lb_used = 'Y'
			AND sp.productid IS NOT NULL
			AND cs.channelid IS NOT NULL
			AND cs.channelid IS NOT NULL
		";

		$query .= $set_search['sub_search_query'];
		$query .= ($set_sort) ? $set_sort : " ORDER BY p.seq ASC, p.hot_prod DESC, p.offtime ASC ";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];

		// if (empty($perpage)){
			// $perpage = 100;
		// }

		if($num) {
			//分頁資料
			// $page = $db->recordPage($num, $this);
			// $query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			// $table['table']['page'] = $page;

			foreach($table['table']['record'] as $tk => $tv) {
				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
				}
			}

			return $table['table']['record'];
		}

		return false;
    }

	/*
	 *	現在直播殺價商品列表
	 */
	public function lb_product_list_now() {
			global $db, $config;

			//取得現在時間
			$nowtime = date('Y-m-d H:i:s');
			//$nowtime = '2020-05-24 10:36:01';

			//結束時間
			$endtime = date('Y-m-d').' 23:00:00';
			//$endtime = '2020-05-24 10:36:01';

			//SQL指令
			$query_record = "SELECT p.productid, p.name, p.ontime, p.offtime, pt.filename as thumbnail, p.thumbnail_url
															, unix_timestamp(p.ontime) as unix_ontime, unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `unix_now`";
			$query = "FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p";
			$query.= " LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
										p.prefixid = pt.prefixid
										AND p.ptid = pt.ptid
										AND pt.switch = 'Y'
									WHERE
										p.prefixid = '{$config['default_prefix_id']}'
										AND p.is_flash != 'Y'
										AND p.lb_used = 'Y'
							";
			//設定輸出商品開始與結束區間
			$query.=" AND p.`offtime` >= '{$nowtime}' AND p.`offtime` < '{$endtime}'";
			//$query.=" AND p.`offtime` >= '2019-07-12 22:00:01'";
			//$query.=" AND p.`offtime` >= '2019-07-30 22:00:01'";
			//$query.=" AND p.`offtime` >= '2020-05-24 10:36:01'";

			//依商品下架時間與權重排序
			$query.=" order by p.offtime, p.seq ASC";

			//取得資料
			$table = $db->getQueryRecord($query_record . $query);

			//商品圖片處理
			if(!empty($table['table']['record'])) {

					foreach($table['table']['record'] as $tk => $tv) {
							if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
									$table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['filename'];
							}
					}

					return $table['table']['record'];
			}

			return false;
	}

	// 只能下一擋未結標的新手限定商品
	// $ptype 商品類別 0:一般商品,1:圓夢商品
	public function new_hand_rule($productid='', $userid='', $ptype='', $limit_group=''){
		global $db, $config;
		if (!empty($productid) && !empty($userid) && !empty($limitid) ) {
			$where = "";
			if ($ptype!='') {
				$where = " AND p.ptype = '{$ptype}' ";
			}
			$query = "SELECT
						h.productid,
						p.name,
						h.userid,
						h.nickname,
						pl.name as pl_name,
						pl.limit_group
					FROM
						`{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` h
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON h.productid = p.productid
						AND p.switch = 'Y'
					LEFT JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited` pl
						ON p.limitid = pl.plid
						AND pl.switch = 'Y'
					WHERE
						h.switch = 'Y'
					AND (
						p.offtime > now()
						OR p.closed = 'N'
					)
					AND h.userid = '{$userid}'
					AND pl.limit_group = '{$limit_group}'
					AND p.productid <> '{$productid}'
					{$where}
					GROUP BY
						h.productid";
			$table = $db->getQueryRecord($query);
			if(!empty($table['table']['record'])) {
				$retCheck['checkCode'] = 2;
				$retCheck['checkMsg'] = "已有下標其他".$table['table']['record'][0]['pl_name']."限定商品";
				return $retCheck;
			}else{
				$retCheck['checkCode'] = 1;
				$retCheck['checkMsg'] = "";
				return $retCheck;
			}
		}else{
			$retCheck['checkCode'] = 99;
			$retCheck['checkMsg'] = "商品及會員資料有誤";
			return $retCheck;
		}
	}

	/*202012128
    重新產生票券商品權重設定
	 */
	public function reset_oscode_sets_info(){
		global $db, $config;
		$db->query("TRUNCATE `saja_shop`.`saja_oscode_set_rule_setting`");
		$sets=$db->getRecord("select count(*)as cnt from `saja_shop`.`saja_oscode_set_rule`");
		$oscode_sets=$db->getRecord("select productid,name from `saja_shop`.`saja_product` where codenum=1 and offtime>now()");
		$redis = getRedis();
		$oscodear="";	
		for ($i=0;$i<sizeof($oscode_sets);$i++){
			$qstr="insert into `saja_shop`.`saja_oscode_set_rule_setting`(`oscode_productid`,`name`,`srid`,`weight`,`insertt`) values
			('{$oscode_sets[$i]['productid']}','{$oscode_sets[$i]['name']}','1','1',NOW())";
			for ($j=1;$j<($sets[0]['cnt']);$j++){
				$cnt=$j+1;
				$qstr.=",('{$oscode_sets[$i]['productid']}','{$oscode_sets[$i]['name']}','{$cnt}','1',NOW())";
			}
			$oscodear[$i]=$oscode_sets[$i]['productid'];
			$db->query($qstr);
		}
		//var_dump($oscodear);
		$redis->set('oscodelst',implode(',',$oscodear));
	}
	//202012128
	public function reset_product_tmp(){
		global $db, $config;
		$db->query("TRUNCATE `saja_shop`.`saja_product_tmp`");
	}

	public function gen_product_tmp($method=2,$gap=2){
		global $db, $config;
		
		$redis = getRedis();
		$limit_str=$db->getRecord("select plid,name from `saja_shop`.`saja_product_limited` where used='Y'");
		switch($method){
			case 1:
				$start_time=strtotime("+60 minutes",time());
				$waitar = implode(",",array_slice(shuffle(explode(",",$redis->get('oscodelst'))),0,(60/$gap)));
				//每個lv看到的商品一致只是張數不同
				for ($i=0;$i<sizeof($limit_str);$i++){
					$j=$i+1;
					$pickup=$db->getRecord("Select oscode_productid,name,srid,qty,handling_fee from `saja_oscode_set_rule_setting` srs,`saja_oscode_set_rule` sr where srs.srid=sr.sets_id and oscode_productid in ({$waitar}) and srid='{$j}' ");
					for ($k=0;$k<sizeof($pickup);$k++){
						$pickup[$k]['limitname']=$limit_str[$i]['name'];
						$pickup[$k]['offtime']=date("Y-m-d H:i:s",strtotime("+".(($k+1)*$gap)." minutes",$start_time));
						var_dump(json_encode($pickup[$k]));
					}
				}
			break;
			case 2:
				//一次就生成隔天的720檔 每全部跑一輪就reset
				$start_time=strtotime(date("Y-m-d",strtotime("+1 day"))." 00:00:00");

				//on檔商品數
				$waitar=explode(",",$redis->get('oscodelst'));
				$maxset=sizeof($waitar);
				//var_dump($redis->get('oscodelst'));
				//chanell 數
				$maxlimit=sizeof($limit_str);
				echo "目標商品編號,商品名稱,分組編號,數量,處理費,上架時間,分組名稱,下架時間<BR>";
				for ($m=0;$m<(1440/$gap);$m++){
					if (($m%$maxset)==0){
						shuffle($waitar);
					}
					$pickup=$db->getRecord("Select oscode_productid,name,srid,qty,handling_fee from `saja_shop`.`saja_oscode_set_rule_setting` srs,`saja_shop`.`saja_oscode_set_rule` sr where srs.srid=sr.sets_id and oscode_productid in (".$waitar[$m%$maxset].") order by srid");
					$offtime=date("Y/m/d H:i",strtotime("+".(($m)*$gap)." minutes",$start_time));
					$ontime=date("Y/m/d H:i",strtotime("+".(($m-30)*$gap)." minutes",$start_time));
					for ($s=0;$s<($maxlimit*1);$s++){
						$pickup[$s]['name']=$pickup[$s]['name'].$pickup[$s]['qty']." 張";
						$pickup[$s]['ontime']=$ontime;
						$pickup[$s]['limitname']=$limit_str[$s]['name'];
						$pickup[$s]['offtime']=$offtime;
						echo $pickup[$s]['oscode_productid'].",".$pickup[$s]['name'].",".$pickup[$s]['srid'].",".$pickup[$s]['qty'].",".$pickup[$s]['handling_fee'].",".$pickup[$s]['ontime'].",".$pickup[$s]['limitname'].",".$pickup[$s]['offtime']."<BR>";
						//var_dump(json_encode($pickup[$s]));
					}
				}
			break;
			case 3:
				//一次就生成隔天的720檔 每全部跑一輪就reset
				//六輪各跑各的商品
				$start_time=strtotime(date("Y-m-d",strtotime("+1 day"))+" 00:00:00");
				$maxset=sizeof(explode(",",$redis->get('oscodelst')));
				$maxlimit=sizeof($limit_str);
				for ($m=0;$m<(1440/$gap);$m++){
					//$srid=($m%(sizeof($limit_str))+1);
					for ($n=0;$n<$maxlimit;$n++){
						if ($m%$maxset==0) $waitar[$n] = implode(",",(shuffle(explode(",",$redis->get('oscodelst')))));
						$pickup=$db->getRecord("Select oscode_productid,name,srid,qty,handling_fee from `saja_oscode_set_rule_setting` srs,`saja_oscode_set_rule` sr where srs.srid=sr.sets_id and oscode_productid in (".$waitar[$n].") and srid='".($n+1)."' ");
						$pickup[0]['limitname']=$limit_str[$n]['name'];
						$pickup[0]['offtime']=date("Y-m-d H:i:s",strtotime("+".(($m+1)*$gap)." minutes",$start_time));						
					}
				}
			break;
		}
	}



	//20201222
    //票券商品張數權重
	public function oscode_set_rule($userid=''){
		global $db, $config,$user;

		//取得會員等級
		$level = 'a';
		if(!empty($userid) ){
			$user_able = $user->getPayBidCnt($userid);//query_bid_able($userid);
			//會員等級大於 25為b, 否則為a
			$level = ($user_able>25) ? 'b' : 'a';

			//$level = (!empty($user_able["level"]) ) ? $user_able["level"] : 'a';
		}
		
		$ret['qty'] = 5;
		$ret['handling_fee'] = 5;

		//$weight
		$query = "SELECT `sets_id`,`type`,`qty`,`handling_fee` FROM `saja_shop`.`saja_oscode_set_rule` WHERE `switch`='Y' ORDER BY `type`";
		$sets = $db->getRecord($query);
		//error_log("[oscode_set_rule]sets: ".json_encode($sets));

		if(!empty($sets)){
			foreach($sets as $k=>$v){
				$sid = $v['sets_id'];
				$type = $v['type'];
				$sets_arr[$sid] = $v;
				$weight_arr[$type][] = $sid;
			}
			//error_log("[oscode_set_rule]sets_arr: ".json_encode($sets_arr));
			//error_log("[oscode_set_rule]weight_arr: ".json_encode($weight_arr));
		
			//分老手陣列, 新手陣列
			if($level =='b'){
				//新手隨機第一筆 
				$get_weight_arr = $weight_arr['b'];
				shuffle($weight_arr['a']);
				array_push($get_weight_arr, $weight_arr['a'][0]);
			}else{
				//老手隨機第一筆
				$get_weight_arr = $weight_arr['a'];
				shuffle($weight_arr['b']);
				array_push($get_weight_arr, $weight_arr['b'][0]);
			}
			
			//error_log("[oscode_set_rule]get_weight: ".json_encode($get_weight_arr));
			//權重隨機陣列
			shuffle($get_weight_arr);
		}
		error_log("[oscode_set_rule]shuffle: ".json_encode($get_weight_arr));
		
		//張數 => 權重陣列第一筆
		if(!empty($get_weight_arr) ){
			$id = $get_weight_arr[0];
			$ret['qty'] = $sets_arr[$id]['qty'];
			$ret['handling_fee'] = $sets_arr[$id]['handling_fee'];
		}
		
		return $ret;
	}

	//getLast3buy
	public function getLast3buy($userid){
		global $db, $config;
		//SELECT codepid,pgp.userid FROM `saja_pay_get_product` pgp , `saja_product` p WHERE pgp.productid=p.productid and codepid >0
		$getLast3buy="";
		$rtn=$db->getRecord("SELECT GROUP_CONCAT(codepid)as lastbuy FROM (select codepid from `saja_shop`.`saja_pay_get_product` pgp , `saja_shop`.`saja_product` p WHERE pgp.productid=p.productid and codepid >0 and pgp.userid ='{$userid}' order by pgp.insertt desc limit 0,3) as a");
		if (sizeof($rtn)>0) $getLast3buy=$rtn[0]['lastbuy'];
		return $getLast3buy;
	}

	/*
	@last3buy int string with comma最後三次購券記錄
	@items_per_page int 一頁秀Ｎ筆
	*/
	public function pickup_promote($userid,$last3buy='',$items_per_page=20){
		global $db, $config;
		//先取未達標的最近20筆「最多」
		//首次取二十筆　30分內會存在redis
		$redis = getRedis();
		if ($redis->exists('oscodelst_'.$userid)){
			$pickup=explode(",",$redis->get('oscodelst_'.$userid));
		}else{
			$pickup=array();
			//PartZero:取最快到期七筆＋最貴兩筆且未達標者
			$zeropart=$db->getRecord("select codepid from ((SELECT codepid ,(unix_timestamp(offtime)-unix_timestamp(now()))/3600 as duetime FROM `saja_shop`.`saja_tmpl_product` where offtime>now() and (floor(sold*0.3*retail_price)<product_cost) order by duetime limit 0,7) union (select codepid,'0' as duetime from `saja_shop`.`saja_tmpl_product` where offtime>now() and (floor(sold*0.3*retail_price)<product_cost) order by product_cost limit 0,2)) as tmp group by codepid");
			for($i=0;$i<sizeof($zeropart);$i++){
				$pickup[]=$zeropart[$i]['codepid'];
			}
			$last3buy=implode(",",$pickup);
			//PartOne:取未達標且較貴重的優先取到75％
			$firstpart=$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($last3buy) and offtime>now() and  (floor(sold*0.3*retail_price)<product_cost)  order by (sold*0.3*retail_price/product_cost), product_cost desc limit 0,$items_per_page");
			$firstPartSize=sizeof($firstpart);
			if ($firstPartSize>0){
				//不足75％全取　超過取75％
				if ($firstPartSize<=(floor($items_per_page*0.75)-sizeof($pickup))){
					for ($i=0;$i<$firstPartSize;$i++){
						$pickup[]=$firstpart[$i]['codepid'];
					}
				}else{
					for ($i=0;$i<(floor($items_per_page*0.75)-sizeof($pickup));$i++){
						$pickup[]=$firstpart[$i]['codepid'];
					}
				}
				shuffle($pickup);
				$partOneSkip=implode(",",$pickup);
				//扣掉PART1 後取未到期六成做ＰＡＲＴ二[取15%(賣的不好又比較貴的）]
				$lim=ceil(($items_per_page-sizeof($pickup))*0.6);
				$secPart=$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($partOneSkip) and offtime>now() and  (floor(sold*0.3*retail_price)<product_cost)  order by floor((sold*0.3*retail_price/product_cost)*100), product_cost  desc limit 0,$lim");
				for ($j=0;$j<sizeof($secPart);$j++){
					$pickup[]=$secPart[$j]['codepid'];
				}
				$partTwoSkip=implode(",",$pickup);
				$lim=$items_per_page-sizeof($pickup);
				//從全部剩下的挑出最貴的10%
				$thirdPart=	$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($partTwoSkip) and offtime>now()  order by product_cost desc ,(sold*0.3*retail_price/product_cost)  limit 0,$lim");
				for ($j=0;$j<sizeof($thirdPart);$j++){
					$pickup[]=$thirdPart[$j]['codepid'];
				}
			}else{
				//扣掉PART1 後取未到期六成做ＰＡＲＴ二[取15%(賣的不好又比較貴的）]
				$lim=ceil($items_per_page*0.6);
				$secPart=$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE  offtime>now()  order by floor((sold*0.3*retail_price/product_cost)*100), product_cost  desc limit 0,$lim");
				for ($j=0;$j<sizeof($secPart);$j++){
					$pickup[$j]=$secPart[$j]['codepid'];
				}
				//從全部剩下的挑出最貴的10%
				$partTwoSkip=implode(",",$pickup);
				$lim=$items_per_page-sizeof($pickup);
				$thirdPart=	$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($partTwoSkip) and offtime>now() order by product_cost desc ,(sold*0.3*retail_price/product_cost)  desc limit 0,$lim");
				for ($j=0;$j<sizeof($thirdPart);$j++){
					$pickup[]=$thirdPart[$j]['codepid'];
				}
			}
			$redis->setex('oscodelst',1800,implode(',',$pickup));
		}
		return $pickup;
	}

	public function pickup_promote_V1($userid,$last3buy='',$items_per_page=20){
		global $db, $config;
		//先取未達標的最近20筆「最多」
		//首次取二十筆　30分內會存在redis
		$redis = getRedis();
		if ($redis->exists('oscodelst_'.$userid)){
			$pickup=explode(",",$redis->get('oscodelst_'.$userid));
		}else{
			$pickup=array();

			if ($last3buy==''){
				$firstpart=$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE offtime>now() and  (floor(sold*0.3*retail_price)<product_cost)  order by product_cost,(sold*0.3*retail_price/product_cost) desc limit 0,$items_per_page");
			}else{
				$firstpart=$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($last3buy) and offtime>now() and  (floor(sold*0.3*retail_price)<product_cost)  order by product_cost,(sold*0.3*retail_price/product_cost) desc limit 0,$items_per_page");
			}
			$firstPartSize=sizeof($firstpart);
			if ($firstPartSize>0){
				//不足75％全取　超過取75％
				if ($firstPartSize<=floor($items_per_page*0.75)){
					for ($i=0;$i<$firstPartSize;$i++){
						$pickup[$i]=$firstpart[$i]['codepid'];
					}
				}else{
					for ($i=0;$i<floor($firstPartSize*0.75);$i++){
						$pickup[$i]=$firstpart[$i]['codepid'];
					}
				}
				shuffle($pickup);
				$partOneSkip=implode(",",$pickup);
				//扣掉PART1 後取六成做ＰＡＲＴ二
				$lim=ceil(($items_per_page-sizeof($pickup))*0.6);
				$secPart=$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($partOneSkip) and offtime>now() and  (floor(sold*0.3*retail_price)<product_cost)  order by product_cost,(sold*0.3*retail_price/product_cost) desc limit 0,$lim");
				for ($j=0;$j<sizeof($secPart);$j++){
					$pickup[]=$secPart[$j]['codepid'];
				}
				$partTwoSkip=implode(",",$pickup);
				$lim=$items_per_page-sizeof($pickup);
				$thirdPart=	$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($partTwoSkip) and offtime>now()  order by product_cost,(sold*0.3*retail_price/product_cost) desc limit 0,$lim");
				for ($j=0;$j<sizeof($thirdPart);$j++){
					$pickup[]=$thirdPart[$j]['codepid'];
				}
			}else{
				//皆已達標取未到期六成做ＰＡＲＴ二
				$lim=ceil($items_per_page*0.6);
				$secPart=$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE  offtime>now()  order by product_cost,(sold*0.3*retail_price/product_cost) desc limit 0,$lim");
				for ($j=0;$j<sizeof($secPart);$j++){
					$pickup[$j]=$secPart[$j]['codepid'];
				}
				//
				$partTwoSkip=implode(",",$pickup);
				$lim=$items_per_page-sizeof($pickup);
				$thirdPart=	$db->getRecord("SELECT codepid,name,(sold*0.3*retail_price/product_cost),floor(sold*0.3*retail_price) as ticket_income ,sold,retail_price,product_cost,(unix_timestamp(offtime)-unix_timestamp(now()))/3600,offtime,now() as duetime FROM `saja_shop`.`saja_tmpl_product` WHERE codepid not in ($partTwoSkip) and offtime>now() order by product_cost,(sold*0.3*retail_price/product_cost) desc limit 0,$lim");
				for ($j=0;$j<sizeof($thirdPart);$j++){
					$pickup[]=$thirdPart[$j]['codepid'];
				}
			}
			$redis->setex('oscodelst',1800,implode(',',$pickup));
		}
		return $pickup;
	}

	/*update tmpl_product_redis*/
	public function reset_tmpl_product_redis(){
		global $db, $config;
		$redis = getRedis();
		$tmpl_raw=$db->getRecord("select * from `saja_shop`.`saja_tmpl_product`");
		for($i=0;$i<sizeof($tmpl_raw);$i++){
			$redis->set("tmpl_raw_".$tmpl_raw[$i]['codepid'],json_encode($tmpl_raw[$i], JSON_UNESCAPED_UNICODE));
		}
	}
	/*
	取得開始時間如果還有未到期的就以最後一筆再＋兩分鐘不然就以進入時間計同時回傳目前還在的是那幾樣
	*/
	public function getStartTime($userid){
		global $db, $config;
		$tmpl_raw=$db->getRecord("SELECT codepid,offtime from `saja_shop`.`saja_product` where offtime>now() and closed='N' and targetid='{$userid}' order by offtime");
		$using=array();
		$rtn=array();
		$rtn['starttime']=time();
		error_log("3067 --- ".$rtn['starttime']);
		for($i=0;$i<sizeof($tmpl_raw);$i++){
			//$rtn['starttime']=strtotime(date("Y/m/d H:i:s",$tmpl_raw[$i]['offtime']));
			$rtn['starttime']=strtotime($tmpl_raw[$i]['offtime']);
			if ($tmpl_raw[$i]['codepid']!='0') $using[$i]=$tmpl_raw[$i]['codepid'];
		}
		error_log("3073 --- ".$rtn['starttime']);
		$rtn['data']=$using;
		return $rtn;
	}

	public function genProdsFromTmpl($schedule,$userid,$gap=2,$items_per_page=20){
		global $db, $config;
		$redis = getRedis();
		$using=$this->getStartTime($userid);
		$start_time=$using['starttime'];
		$using=$using['data'];
		//去除未過期的
		error_log("3020 $userid".json_encode($schedule)." - ".json_encode($using));
		if (sizeof($using)>=20){
		}else{
			if (sizeof($using)>0){
				$schedule_tp=array_diff($schedule,$using);
				$schedule=array();
				foreach($schedule_tp as $v){
					$schedule[]=$v;
				}
			}
			error_log("3022 $userid".json_encode($schedule)." - ".json_encode($using));
			if (sizeof($using)>=20){
			}else{
				$schedule_tp=$schedule;
				if ((sizeof($schedule)+$usingsize)>$items_per_page) $schedule=array_slice($schedule_tp,0,($items_per_page-$usingsize));
				error_log("3032 $userid".json_encode($schedule)." - ".json_encode($using));
				for ($i=0;$i<sizeof($schedule);$i++){
					$offtime=(date("Y-m-d H:i:s",strtotime("+".(($i+1)*$gap)." minutes",$start_time)));
					$ret = $this->oscode_set_rule($userid);
					$tmpl_raw=json_decode($redis->get("tmpl_raw_".$schedule[$i]),true);
					$_prefixid=$tmpl_raw['prefixid'];
					$_productid=NULL;
					$_ptype=$tmpl_raw['ptype'];
					$_psrid=$tmpl_raw['psrid'];
					$_codepid=$tmpl_raw['codepid'];
					$_codenum=$ret['qty'];//$tmpl_raw['codenum'];
					$_name=$ret['qty']." 張 - ".$tmpl_raw['name'];
					$_description=$tmpl_raw['description'];
					$_description2=$tmpl_raw['description2'];
					$_rule=$tmpl_raw['rule'];
					$_retail_price=$ret['qty']*$tmpl_raw['retail_price'];
					$_cost_price=$ret['qty']*$tmpl_raw['cost_price'];
					$_ptid=$tmpl_raw['ptid'];
					$_ptid2=$tmpl_raw['ptid2'];
					$_ptid3=$tmpl_raw['ptid3'];
					$_ptid4=$tmpl_raw['ptid4'];
					$_pftid=$tmpl_raw['pftid'];
					$_thumbnail_url=$tmpl_raw['thumbnail_url'];
					//$_ontime=$start_time;//$tmpl_raw['ontime'];
					$_offtime=$offtime;//$tmpl_raw['offtime'];
					$_saja_limit=$tmpl_raw['saja_limit'];
					$_user_limit=$tmpl_raw['user_limit'];
					$_usereach_limit=$tmpl_raw['usereach_limit'];
					$_saja_fee=$ret['handling_fee'];
					$_ads_fee=$tmpl_raw['ads_fee'];
					$_process_fee=30;//$tmpl_raw['process_fee'];
					$_price_limit=$tmpl_raw['price_limit'];
					$_bonus_type=$tmpl_raw['bonus_type'];
					$_bonus=$tmpl_raw['bonus'];
					$_gift_type=$tmpl_raw['gift_type'];
					$_gift=$tmpl_raw['gift'];
					$_vendorid=$tmpl_raw['vendorid'];
					$_split_rate=$tmpl_raw['split_rate'];
					$_base_value=$tmpl_raw['base_value'];
					$_locked=$tmpl_raw['locked'];
					$_display=$tmpl_raw['display'];
					$_display_all=$tmpl_raw['display_all'];
					$_closed='N';
					$_mob_type=$tmpl_raw['mob_type'];
					$_bid_type=$tmpl_raw['bid_type'];
					$_totalfee_type=$tmpl_raw['totalfee_type'];
					$_loc_type=$tmpl_raw['loc_type'];
					$_is_flash=$tmpl_raw['is_flash'];
					$_is_big=$tmpl_raw['is_big'];
					$_flash_loc=$tmpl_raw['flash_loc'];
					$_flash_loc_map=$tmpl_raw['flash_loc_map'];
					$_contact_info=$tmpl_raw['contact_info'];
					$_bid_scode=$tmpl_raw['bid_scode'];
					$_bid_spoint=$tmpl_raw['bid_spoint'];
					$_bid_oscode=$tmpl_raw['bid_oscode'];
					$_pay_type=$tmpl_raw['pay_type'];
					$_checkout_type=$tmpl_raw['checkout_type'];
					$_checkout_money=$tmpl_raw['checkout_money'];
					$_is_test=$tmpl_raw['is_test'];
					$_is_discount=$tmpl_raw['is_discount'];
					$_limitid=$tmpl_raw['limitid'];
					$_hot_used=$tmpl_raw['hot_used'];
					$_hot_prod=$tmpl_raw['hot_prod'];
					$_lb_used=$tmpl_raw['lb_used'];
					$_is_exchange=$tmpl_raw['is_exchange'];
					$_epid=$tmpl_raw['epid'];
					$_ordertype=$tmpl_raw['ordertype'];
					$_orderbonus=$tmpl_raw['orderbonus'];
					$_everydaybid=$tmpl_raw['everydaybid'];
					$_freepaybid=$tmpl_raw['freepaybid'];
					$_seq=$tmpl_raw['seq'];
					$_switch=$tmpl_raw['switch'];
					$_color_use=$tmpl_raw['color_use'];
					$_size_use=$tmpl_raw['size_use'];
					$_insertt=$tmpl_raw['insertt'];
					$_modifyt=$tmpl_raw['modifyt'];
					$_ipfs_hash=$tmpl_raw['ipfs_hash'];
					$_tx_hash=$tmpl_raw['tx_hash'];
					$_chainstatus=$tmpl_raw['chainstatus'];
					$_token_id=$tmpl_raw['token_id'];
					$_sold=$tmpl_raw['sold'];
					$_product_cost=$tmpl_raw['product_cost'];
					//$ts="INSERT INTO `saja_shop`.`saja_product` (`prefixid`, `productid`, `ptype`, `psrid`, `codepid`, `codenum`, `name`, `description`, `description2`, `rule`, `retail_price`, `cost_price`, `ptid`, `ptid2`, `ptid3`, `ptid4`, `pftid`, `thumbnail_url`, `ontime`, `offtime`, `saja_limit`, `user_limit`, `usereach_limit`, `saja_fee`, `ads_fee`, `process_fee`, `price_limit`, `bonus_type`, `bonus`, `gift_type`, `gift`, `vendorid`, `split_rate`, `base_value`, `locked`, `display`, `display_all`, `closed`, `mob_type`, `bid_type`, `totalfee_type`, `loc_type`, `is_flash`, `is_big`, `flash_loc`, `flash_loc_map`, `contact_info`, `bid_scode`, `bid_spoint`, `bid_oscode`, `pay_type`, `checkout_type`, `checkout_money`, `is_test`, `is_discount`, `limitid`, `hot_used`, `hot_prod`, `lb_used`, `is_exchange`, `epid`, `ordertype`, `orderbonus`, `everydaybid`, `freepaybid`, `seq`, `switch`, `color_use`, `size_use`, `insertt`, `modifyt`, `ipfs_hash`, `tx_hash`, `chainstatus`, `token_id`, `targetid`) VALUES('{$_prefixid}', NULL, '{$_ptype}', '{$_psrid}', '{$_codepid}', '{$_codenum}', '{$_name}', '{$_description}', '{$_description2}', '{$_rule}', '{$_retail_price}', '{$_cost_price}', '{$_ptid}', '{$_ptid2}', '{$_ptid3}', '{$_ptid4}', '{$_pftid}', '{$_thumbnail_url}', NOW(),DATE_ADD(NOW(), INTERVAL ".(($i+1)*$gap)." MINUTE), '{$_saja_limit}', '{$_user_limit}', '{$_usereach_limit}', '{$_saja_fee}', '{$_ads_fee}', '{$_process_fee}', '{$_price_limit}', '{$_bonus_type}', '{$_bonus}', '{$_gift_type}', '{$_gift}', '{$_vendorid}', '{$_split_rate}', '{$_base_value}', '{$_locked}', '{$_display}', '{$_display_all}', '{$_closed}', '{$_mob_type}', '{$_bid_type}', '{$_totalfee_type}', '{$_loc_type}', '{$_is_flash}', '{$_is_big}', '{$_flash_loc}', '{$_flash_loc_map}', '{$_contact_info}', '{$_bid_scode}', '{$_bid_spoint}', '{$_bid_oscode}', '{$_pay_type}', '{$_checkout_type}', '{$_checkout_money}', '{$_is_test}', '{$_is_discount}', '{$_limitid}', '{$_hot_used}', '{$_hot_prod}', '{$_lb_used}', '{$_is_exchange}', '{$_epid}', '{$_ordertype}', '{$_orderbonus}', '{$_everydaybid}', '{$_freepaybid}', '{$_seq}', '{$_switch}', '{$_color_use}', '{$_size_use}', '{$_insertt}', '{$_modifyt}', '{$_ipfs_hash}', '{$_tx_hash}', '{$_chainstatus}', '{$_token_id}', '{$userid}')";
					$ts="INSERT INTO `saja_shop`.`saja_product` (`prefixid`, `productid`, `ptype`, `psrid`, `codepid`, `codenum`, `name`, `description`, `description2`, `rule`, `retail_price`, `cost_price`, `ptid`, `ptid2`, `ptid3`, `ptid4`, `pftid`, `thumbnail_url`, `ontime`, `offtime`, `saja_limit`, `user_limit`, `usereach_limit`, `saja_fee`, `ads_fee`, `process_fee`, `price_limit`, `bonus_type`, `bonus`, `gift_type`, `gift`, `vendorid`, `split_rate`, `base_value`, `locked`, `display`, `display_all`, `closed`, `mob_type`, `bid_type`, `totalfee_type`, `loc_type`, `is_flash`, `is_big`, `flash_loc`, `flash_loc_map`, `contact_info`, `bid_scode`, `bid_spoint`, `bid_oscode`, `pay_type`, `checkout_type`, `checkout_money`, `is_test`, `is_discount`, `limitid`, `hot_used`, `hot_prod`, `lb_used`, `is_exchange`, `epid`, `ordertype`, `orderbonus`, `everydaybid`, `freepaybid`, `seq`, `switch`, `color_use`, `size_use`, `insertt`, `modifyt`, `ipfs_hash`, `tx_hash`, `chainstatus`, `token_id`, `targetid`) VALUES('{$_prefixid}', NULL, '{$_ptype}', '{$_psrid}', '{$_codepid}', '{$_codenum}', '{$_name}', '{$_description}', '{$_description2}', '{$_rule}', '{$_retail_price}', '{$_cost_price}', '{$_ptid}', '{$_ptid2}', '{$_ptid3}', '{$_ptid4}', '{$_pftid}', '{$_thumbnail_url}', NOW(),'{$offtime}', '{$_saja_limit}', '{$_user_limit}', '{$_usereach_limit}', '{$_saja_fee}', '{$_ads_fee}', '{$_process_fee}', '{$_price_limit}', '{$_bonus_type}', '{$_bonus}', '{$_gift_type}', '{$_gift}', '{$_vendorid}', '{$_split_rate}', '{$_base_value}', '{$_locked}', '{$_display}', '{$_display_all}', '{$_closed}', '{$_mob_type}', '{$_bid_type}', '{$_totalfee_type}', '{$_loc_type}', '{$_is_flash}', '{$_is_big}', '{$_flash_loc}', '{$_flash_loc_map}', '{$_contact_info}', '{$_bid_scode}', '{$_bid_spoint}', '{$_bid_oscode}', '{$_pay_type}', '{$_checkout_type}', '{$_checkout_money}', '{$_is_test}', '{$_is_discount}', '{$_limitid}', '{$_hot_used}', '{$_hot_prod}', '{$_lb_used}', '{$_is_exchange}', '{$_epid}', '{$_ordertype}', '{$_orderbonus}', '{$_everydaybid}', '{$_freepaybid}', '{$_seq}', '{$_switch}', '{$_color_use}', '{$_size_use}', '{$_insertt}', '{$_modifyt}', '{$_ipfs_hash}', '{$_tx_hash}', '{$_chainstatus}', '{$_token_id}', '{$userid}')";
					$db->query($ts);
					$lastproductid=$db->_con->insert_id;
					$this->genDupCategoryRt($tmpl_raw['productid'],$lastproductid);
					$this->genDupStoreRt($tmpl_raw['productid'],$lastproductid);
					$this->genRuleRt($tmpl_raw['productid'],$lastproductid);
				}
			}
		}
	}

	/*產生複製的商品編號
	@opid int 複製來源商品編號
	@npid int 複製目的商品編號
	*/
	public function genDupCategoryRt($opid,$npid){
		global $db, $config;
		$query ="SELECT * FROM `saja_shop`.`saja_product_category_rt` WHERE `prefixid` = 'saja' AND `switch`='Y' AND `productid` = '{$opid}'";
		$old_product_cat = $db->getRecord($query);
		//Relation product_category
		if(sizeof($old_product_cat)>0)
		{
			foreach($old_product_cat as $rk => $rv) {
				$query = "INSERT INTO `saja_shop`.`saja_product_category_rt` SET
				`prefixid`='saja',
				`pcid`='{$rv['pcid']}',
				`productid`='{$npid}',
				`seq`='{$rv['seq']}',
				`insertt`=NOW() ON DUPLICATE KEY UPDATE `prefixid`='saja'";
				$db->query($query);
			}
		}else{
			$query = "INSERT INTO `saja_shop`.`saja_product_category_rt` SET
			`prefixid`='saja',
			`pcid`='166',
			`productid`='{$npid}',
			`seq`='0',
			`insertt`=NOW() ON DUPLICATE KEY UPDATE `prefixid`='saja'";
			$db->query($query);
		}
	}

	/*產生複製的商家資訊
	@opid int 複製來源商品編號
	@npid int 複製目的商品編號
	*/
	public function genDupStoreRt($opid,$npid){
		global $db, $config;
		$query ="SELECT * FROM `saja_channel`.`saja_store_product_rt` WHERE `prefixid` = '{$this->config->default_prefix_id}' AND `switch`='Y' AND `productid` = '{$opid}'";
		$old_product_store = $db->getRecord($query);

		//Relation product_category
		if(sizeof($old_product_store)>0)
		{
			foreach($old_product_store as $rk => $rv) {
				$query = "INSERT INTO `saja_channel`.`saja_store_product_rt` SET
				`prefixid`='saja',
				`storeid`='{$rv['storeid']}',
				`productid`='{$npid}',
				`seq`='{$rv['seq']}',
				`insertt`=NOW()";
				$db->query($query);
			}
		}else{
			$query = "INSERT INTO `saja_channel`.`saja_store_product_rt` SET
				`prefixid`='saja',
				`storeid`='0',
				`productid`='{$npid}',
				`seq`='0',
				`insertt`=NOW()";
				$db->query($query);
		}
	}

	/*產生複製的條件資訊
	@opid int 複製來源商品編號
	@npid int 複製目的商品編號
	*/
	public function genRuleRt($opid,$npid){
		global $db, $config;
		$query ="SELECT * FROM `saja_shop`.`saja_product_rule_rt` WHERE `prefixid` = 'saja' AND `switch`='Y' AND `productid` = '{$opid}'";
		$old_product_rule = $db->getRecord($query);

		//Relation product_category
		if(sizeof($old_product_rule>0))
		{
			foreach($old_product_rule as $rk => $rv) {
				$query = "INSERT INTO `saja_shop`.`saja_product_rule_rt` SET
				`prefixid`='saja',
				`srid`='{$rv['srid']}',
				`value`='{$rv['value']}',
				`productid`='{$npid}',
				`seq`='{$rv['seq']}',
				`insertt`=NOW() ON DUPLICATE KEY UPDATE `prefixid`='saja'";
				$db->query($query);
			}
		}else{
			$query = "INSERT INTO `saja_shop`.`saja_product_rule_rt` SET
			`prefixid`='saja',
			`srid`='any_saja',
			`value`='0',
			`productid`='{$npid}',
			`seq`='0',
			`insertt`=NOW() ON DUPLICATE KEY UPDATE `prefixid`='saja'";
			$db->query($query);
		}
	}

	public function getNextBig(){
		global $db;
		$rtn=$db->getRecord("select codepid,name,offtime from saja_shop.saja_tmpl_product where offtime>date_add(CURDATE(),interval 1 day) order by offtime limit 0,1");
		if (sizeof($rtn)>0){
			return $rtn[0];
		}else{
			return array();
		}
	}
}
?>
