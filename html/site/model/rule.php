<?php
/**
 * Rule Model 模組
 */

class RuleModel {
    public $id;
    public $email;
    public $ok;
    public $msg;

    public function __construct() {
        global $db;
        
        $this->id = isset($_SESSION['auth_id']) ? $_SESSION['auth_id'] : 0;
        $this->email = isset($_SESSION['auth_email']) ? $_SESSION['auth_email'] : '';
        $this->ok = false;

        return $this->ok;
    }

	/*
	 * 寫入儲值規則
	 * $data	array	新增資料項目 
	 */		
	public function insert_Rule($data) {
		
        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` 
					SET name = '{$data['title']}',
						description = '{$data['content']}',
						act = '{$data['act']}',
						logo = '{$data['logo']}',
						banner = '{$data['banner']}',
						seq = '0',
						switch = 'Y'";		
		$db->query($query);
		
		return $spointid = $db->_con->insert_id;		
	
	}
	
	/*
	 * 更新儲值規則
	 * $drid	 	int 	儲值規則編號
	 * $data		array	更新資料項目
	 */		
	public function update_Rule($drid, $data) {
		global $db, $config;
	
		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` 
					set name = '{$data['title']}',
						description = '{$data['content']}',
						act = '{$data['act']}',
						logo = '{$data['logo']}',
						banner = '{$data['banner']}',
						seq = '{$data['seq']}',
						switch = '{$data['switch']}',
						modifyt = '{$data['modifyt']}'
					where drid = '{$drid}'";
		$db->query($query);		
	
    }
	
	/*
	 * 寫入儲值規則項目
	 * $data	array	新增資料項目 
	 */		
	public function insert_Rule_Item($data) {
		
        global $db, $config;
	
		$query = "INSERT INTO `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` 
					SET drid = '{$data['drid']}', 
						countryid = '{$data['countryid']}', 
						name = '{$data['name']}',
						description = '{$data['content']}',
						amount = '{$data['amount']}',
						spoint = '{$data['spoint']}',
						seq = '{$data['seq']}',						
						switch = 'Y'";		
		$db->query($query);
		
		return $spointid = $db->_con->insert_id;		
	}
	
	/*
	 * 更新儲值規則項目
	 * $driid	 	int 	項目編號
	 * $data		array	更新資料項目
	 */		
	public function update_Rule_Item($driid, $data) {
		global $db, $config;
	
		$query = "update `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` 
					set 
						seq = '{$data['seq']}',
						switch = '{$data['switch']}',
						modifyt = '{$data['modifyt']}'
					where driid = '{$driid}'";
		$db->query($query);		
	
    }

	/*
	 * 刪除儲值規則項目
	 * $driid	 	int 	儲值規則項目編號
	 */		
	public function delete_Rule_Item($driid) {
		global $db, $config;
		
		$query = "delete from `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` 
					where driid = '{$driid}'";
		$db->query($query);		
	
    }		

	/*
	 * 取得儲值規則清單
	 * $drid		int 	儲值規則編號
	 */		
	public function get_Rule_List($drid='') {
        global $db, $config;
		
		//SQL指令
		$query = " SELECT drid, name as rule_name, description, act, logo, banner, seq, notify_url
					FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule`
					WHERE switch = 'Y' ";
		
		if (!empty($drid)){
			$query .= " AND drid = {$drid}";	
		}
		$query .= " AND drid < 6 ";
		
		$index = ($page-1)*$perpage;
		
		$query .= " ORDER BY seq ASC";

		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
				
		
		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			
			//取得資料
			$table = $db->getQueryRecord($query);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			return $table['table'];
		}
		
		return false;		
    }	

	/*
	 * 取得儲值規則項目清單
	 * $drid	int 	儲值規則編號
	 * $driid	int 	儲值規則項目編號	 
	 */		
	public function get_Rule_Item($drid='', $driid='') {
        global $db, $config;
		
		$query = " SELECT ri.driid, ri.name as item_name, ri.countryid, ri.seq, ri.amount, ri.spoint, ri.description, ri.drid, r.name as rule_name 
					FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule_item` ri
					LEFT OUTER JOIN `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit_rule` r ON ri.drid = r.drid 
					WHERE ri.switch = 'Y'";		

		if(!empty($drid)) {
			$query .= " AND ri.drid = {$drid}";	
		}
		
		if(!empty($driid)) {
			$query .= " AND ri.driid = {$driid}";	
		}		
			
		$query .= " ORDER BY ri.drid ASC, ri.seq ASC ";
		
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table'];
		}

		return false;		
    }

	/*
	 * 取得拆帳規則項目清單
	 * $psriid	int 	拆帳規則項目編號
	 * $psrid	int 	拆帳回饋規則編號
	 * $pscid	int 	拆帳回饋分類編號	 
	 */		
	public function get_profit_sharing_rule_item($psriid, $psrid, $pscid) {
		
        global $db, $config;
		
		$query = " SELECT psri.*, psc.name as cname, psr.name as rname 
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}profit_sharing_rule_item` psri
					LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}profit_sharing_category` psc ON psri.pscid = psc.pscid
					LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}profit_sharing_rule` psr ON psri.psrid = psr.psrid 
					WHERE psri.switch = 'Y'";		

		if(!empty($psriid)) {
			$query .= " AND psri.psriid = {$psriid}";	
		}
		
		if(!empty($psrid)) {
			$query .= " AND psri.psrid = {$psrid}";	
		}		

		if(!empty($pscid)) {
			$query .= " AND psri.pscid = {$pscid}";	
		}
	
		//取得資料
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}

		return false;		

    }

	/*
	 * 寫入拆帳資料
	 * $data	array	新增資料項目 
	 */		
	public function insert_Profit_Sharing($data) {
		
        global $db, $config;

		if(!empty($data['productid'])) {
			$productfrom.= " productid = '{$data['productid']}',";	
		}
		
		if(!empty($data['epid'])) {
			$productfrom .= " epid = '{$data['epid']}',";	
		}		

		if(!empty($data['evrid'])) {
			$productfrom .= " evrid = '{$data['evrid']}',";	
		}		
		
		$query = "INSERT INTO `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}profit_sharing` 
					SET prefixid = '{$config['default_prefix_id']}',
						pscid = '{$data['pscid']}',
						{$productfrom}
						orderid = '{$data['orderid']}',						
						profit = '{$data['profit']}',
						insertt = NOW(),
						switch = 'Y'";		
		$db->query($query);
		
		return $spointid = $db->_con->insert_id;		
	}	
	
}
?>