<?php
/**
 * User Model 模組
 *
 * This class contains all of the functions used for creating, managing and deleting
 * users.
 */
class VerifiedModel {
    public $user_id = 0;
    public $name = "Guest";
    public $email = "Guest";
    public $ok = false;
    public $msg = "You have been logged out!";
    public $is_logged = false;
	public $str;
	
    /*
     *	Set all internal publiciables to 'Guest' status, then check to see if
     *	a user session or cookie exists.
     */
    public function __construct() {
        if(!$this->check_session() ) $this->check_cookie();
       
	   return $this->ok;
    }
    
    /*
     *	檢查會員是否已登入,並存在SESSION值
     */
    public function check_session() {
		if(!empty($_SESSION['auth_id']) && !empty($_SESSION['auth_secret'])){
            return $this->check($_SESSION['auth_id'], $_SESSION['auth_secret']);
        } else { 
            return false;
		}	
    }

    /*
     *	Check to see if any cookies exist on the user's computer/browser.
     */
    public function check_cookie() {
		if(!empty($_COOKIE['auth_id']) && !empty($_COOKIE['auth_secret'])) {
            return $this->check($_COOKIE['auth_id'], $_COOKIE['auth_secret']);
        } else {
            return false;
		}	
    }
	
	/*
     *	Validate the user's email address.
	 *	$email			varchar				郵件信箱
     */
	public function validEmail($email) {
		$isValid = true;
        $atIndex = strrpos($email, "@");
        
		if (is_bool($atIndex) && !$atIndex) {
            $isValid = false;
        } else {
            $domain = substr($email, $atIndex+1);
            $local = substr($email, 0, $atIndex);
            $localLen = strlen($local);
            $domainLen = strlen($domain);
           
			if ($localLen < 1 || $localLen > 64) {
                $isValid = false;
            } else if ($domainLen < 1 || $domainLen > 255) {
                $isValid = false;
            } else if ($local[0] == '.' || $local[$localLen-1] == '.') {
                $isValid = false;
            } else if (preg_match('/\\.\\./', $local)) {
                $isValid = false;
            } else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain)) {
                $isValid = false;
            } else if (preg_match('/\\.\\./', $domain)) {
                $isValid = false;
            } else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/', str_replace("\\\\","",$local))) {
                if(!preg_match('/^"(\\\\"|[^"])+"$/', str_replace("\\\\","",$local))) {
                    $isValid = false;
                }
            }
            
			if($isValid && !(checkdnsrr($domain,"MX") ||  checkdnsrr($domain,"A"))) {
                $isValid = false;
            }
        }
        return $isValid;
    }

    /*
     *	Check to see if the user is logged in based on their session data.
     */
    public function is_logged()	{
		$logged = $this->check($_SESSION['auth_id'], $_SESSION['auth_secret']); //$_SESSION['auth_email']
		
		if($logged) return true;
        else return false;
    }
	
	/*
     *	This function checks the session/cookie info to see if it's real by comparing it
     *	to what is stored in the database.
	 *	$id			int				會員編號
	 *	$secret		varchar			加密編碼	
     */
    public function check($id, $secret) {
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
		
		// Get the user's info from the database.
        $query = "SELECT u.*, up.nickname 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON 
			u.prefixid = up.prefixid 
			AND u.userid = up.userid
			AND up.`switch`='Y'
		WHERE 
			u.prefixid = '{$config['default_prefix_id']}' 
			AND u.userid = '{$id}' 
			AND u.switch = 'Y'
			AND up.userid IS NOT NULL 
		";
		$table = $db->getQueryRecord($query); 
		
		//判斷是否有相關資料
		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'][0];
			
			//判斷會員編號是否相同
			if($results['userid'] == $id) {
				$this->user_id = $results['userid'];
                $this->email = $results['email'];
                $this->name = $results['nickname'];
                $this->ok = true;
                $this->is_logged = true;
                return true;
            }
		}
        return false;
    }
	
	/*  
	 *	2016/05/06
	 *	改成檢查Login的id是否為宣稱的手機號碼
	 *	$id				int				會員編號
	 *	$phone			varchar			手機號碼
	 */	
	public function validUserAuth($id, $phone) {
	    global $db, $config;
		
		$query = "SELECT name  
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		WHERE `prefixid` = '{$config['default_prefix_id']}' 
			AND `userid` = '{$id}'
			AND `switch` = 'Y'
		";
		$table = $db->getQueryRecord($query); 

		if(empty($table['table']['record'])) {
			return false;
		} else {
			$query = "SELECT * 
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `userid` = '{$id}'
				AND `switch` = 'Y'
			";
			$table = $db->getQueryRecord($query); 
			
			if(!empty($table['table']['record'])) {
				return $table['table']['record'][0];
			} else {
				//新增SMS check code
				$shuffle = get_shuffle();
				$checkcode = substr($shuffle, 0, 6);
				
				$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
				SET
					`prefixid`='{$config['default_prefix_id']}',
					`userid`='{$id}',
					`phone`='{$phone}',
					`code`='{$checkcode}',
					`verified`='N',
					`insertt`=NOW()
				";
				$db->query($query);
				
				$query2 = "SELECT * 
				FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
				WHERE 
					`prefixid` = '{$config['default_prefix_id']}' 
					AND `userid` = '{$id}'
					AND `switch` = 'Y'
				";
				$table = $db->getQueryRecord($query2);
				
				return $table['table']['record'][0];
			}
		}
    }

	/* 
	 *	2016/06/23
	 *	手機驗證檢查
	 *	$userid			int			使用者編號
	 */
	public function validSMSAuth($userid) {
	    global $db, $config;

		$query = "SELECT name  
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
		WHERE `prefixid` = '{$config['default_prefix_id']}' 
			AND `userid` = '{$userid}'
			AND `switch` = 'Y'
		";
		$table = $db->getQueryRecord($query);
		
		if(empty($table['table']['record'])) {
			return false;
		} else {
			$query = "SELECT * 
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
			WHERE 
				`prefixid` = '{$config['default_prefix_id']}' 
				AND `userid` = '{$userid}'
				AND `switch` = 'Y'
			";
			$table = $db->getQueryRecord($query); 
			
			if(!empty($table['table']['record'])) {
				return $table['table']['record'][0];
			} else {
				return false;
			}
		}
    }
	
    /*  
	 *	2016/05/06
	 *	改成檢查Login的id是否為宣稱的手機號碼
	 *	$id				int				會員編號
	 *	$phone			varchar			手機號碼
	 */
	public function validUserAuth2($id, $phone) {
		global $db, $config;
		$query = "SELECT * 
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
		WHERE `prefixid` = '{$config['default_prefix_id']}' 
			AND `phone`= '{$phone}'
			AND `userid` = '{$id}'
		";		
		$table = $db->getQueryRecord($query); 
		error_log("[m/user/validUserAuth] : ".$query);    
					
		if(!empty($table['table']['record'])) {			
			// 找到有驗證過的資料=>回傳該筆資料
			return $table['table']['record'][0];			
		} else {			
			// 找不到有驗證過的資料=>新增一筆verified='N'且回傳
			//新增SMS check code
			$shuffle = get_shuffle();
			$checkcode = substr($shuffle, 0, 6);
			$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
			SET	`prefixid`='{$config['default_prefix_id']}',
				`userid`='{$id}',
				`phone`='{$phone}',
				`code`='{$checkcode}',
				`verified`='N',
				`insertt`=NOW()
			";
			$db->query($query);
			error_log("[m/user/validUserAuth]:".$query);		
			$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
					   WHERE `prefixid` = '{$config['default_prefix_id']}' 
						 AND `phone`= '{$phone}'
						 AND `userid` = '{$id}' ";		
			$table = $db->getQueryRecord($query); 
			if(!empty($table['table']['record'])) {			
			   return $table['table']['record'][0];			
			} else {
			   return false;
			}
		}
    }  
	
    /*
	 *	取得會員基本資料
	 *	$id				int				會員編號	 
	 */
	public function get_user_profile($id) {
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
       
		$query ="SELECT *
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
		WHERE 
			`prefixid` = '{$config['default_prefix_id']}'
			AND userid = '{$id}'
			AND switch = 'Y'
		";
		
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
	
	/*
	 *	會員註冊資料
	 *	$id					int				會員編號
	 *	$loginid			int				會員帳號
	 *	$user_type			int				會員類型	 
	 */
	public function get_user($id='', $loginid='', $user_type='P') {
		global $config;
		
		if(empty($id) && empty($loginid)) {
		   return false;
		}
		$db = new mysql($config["db2"]);
		$db->connect();
       
		$query ="SELECT *
				   FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` 
				  WHERE `prefixid` = '{$config['default_prefix_id']}'
				    AND switch = 'Y' ";
		if(!empty($id)) {
		   $query.=" AND userid='{$id}' ";
		}
		if(!empty($loginid)) {
		   $query.=" AND name='{$loginid}' ";
		}
		if(!empty($user_type)) {
		   $query.=" AND user_type='{$user_type}' ";
		}
		error_log("[m/user/get_user]:".$query);
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
    
    /*
	 * 會員其他資訊分類(Frank.Kao-14/11/18)
	 */
    public function get_user_extrainfo_category() {
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
    	
    	$query = "select *
    	FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo_category` 
    	where 
    		`prefixid` = '{$config['default_prefix_id']}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }
    
    /*
	 * 會員其他資訊(Frank.Kao-14/11/18)
	 *	$uecid			int				會員	 
	 *	$uid			int				會員編號	 
	 */
    public function get_user_extrainfo($uecid,$uid='') {
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
    	
		if(!empty($uid)) {
		   $userid=$uid;
		} else {
		   $userid=$_SESSION['auth_id'];
		}
    	$query = "select ueid, userid, uecid, uecid as seq, field1name, field2name, field3name, field4name, field5name, field6name, field7name, field8name 
        	FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` 
    	where 
    		`prefixid` = '{$config['default_prefix_id']}'
    		and userid = '{$userid}'
    		and uecid = '{$uecid}'
			AND switch = 'Y'
		";
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }
	
    /*
	 *	會員其他資訊(Frank.Kao-14/11/18)
	 *	$uid			int				會員編號	 	 
	 */
    public function get_user_extrainfo2($uid='') {
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
    	
		if(!empty($uid)) {
		   $userid=$uid;
		} else {
		   $userid=$_SESSION['auth_id'];
		}
        $query = "SELECT IFNULL(ue.ueid,'') as ueid, IFNULL(ue.userid,'') as userid, uec.uecid, uec.name, IFNULL(ue.uecid,'') as seq, 
		IFNULL(ue.field1name,'') as field1name, IFNULL(ue.field2name,'') as field2name, IFNULL(ue.field3name,'') as field3name, IFNULL(ue.field4name,'') as field4name, IFNULL(ue.field5name,'') as field5name, IFNULL(ue.field6name,'') as field6name, IFNULL(ue.field7name,'') as field7name, IFNULL(ue.field8name,'') as field8name,
		IFNULL(uec.field1name,'') as field1Title, IFNULL(uec.field2name,'') as field2Title, IFNULL(uec.field3name,'') as field3Title, IFNULL(uec.field4name,'') as field4Title, IFNULL(uec.field5name,'') as field5Title, IFNULL(uec.field6name,'') as field6Title, IFNULL(uec.field7name,'') as field7Title, IFNULL(uec.field8name,'') as field8Title
		FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo_category` uec
		LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` ue ON 
			uec.prefixid = ue.prefixid 
			AND uec.uecid = ue.uecid
			AND ue.userid = '{$uid}'
    	where 
    		uec.switch = 'Y'
		";
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }	
    
    /*
	 * 確認會員其他資訊(Frank.Kao-14/11/25)
	 */
    public function check_user_extrainfo_category() {
    	// global $db, $config;
		global $config;
		
		$db = new mysql($config["db2"]);
		$db->connect();
    	
    	$query = "SELECT uec.* 
    	FROM `saja_user_extrainfo_category` uec 
    	LEFT JOIN `saja_user_extrainfo` ue ON 
    		ue.prefixid = uec.prefixid
    		AND ue.uecid = uec.uecid
    		AND ue.userid = '{$_SESSION['auth_id']}'
    		and ue.switch = 'Y'
    	WHERE 
    		uec.prefixid = 'saja'
    		AND uec.switch = 'Y'
    		AND ue.uecid IS NULL
    		and uec.webshow = 1 
    	";
    	$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		}
		return false;
    }
	
	/*
	 *	取得所在地相關資料
	 *	Add By Thomas 20141216
	 */
	public function getMaxid() {
	       
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect(); 

		$sql = "SELECT ";
		$sql.= "(SELECT max(countryid) FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}country` where switch='Y') as max_country_id, ";
		$sql.= "(SELECT max(zoneid) FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}zone` where switch='Y') as max_zone_id, ";
		$sql.= "(SELECT max(channelid) FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel` where switch='Y') as max_channel_id ";
		$sql.= " FROM DUAL ";
		$table = $db->getQueryRecord($sql); 

		if(!empty($table['table']['record'])) {
			return $table['table']['record'][0];
		}
		return false;
	}
	
	/*
	 *	取得特定國家相關資料
	 *	Add By Thomas 20141216
	 *	$countryid			int				國家編號	 	 
	 */
	public function getChannelList($countryid) {
	       
		global $config;
		 
		$db=new mysql($config["db2"]);
		$db->connect(); 
				
		$sql = "SELECT c.countryid, c.name as country_name, c.isocountrycode, c.countrycode ";
		$sql.= " , z.zoneid, z.name as zone_name ";
		$sql.= " ,ch.channelid, ch.name as channel_name ";
		$sql.= "  FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}country` c ";
		$sql.= "  JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}zone` z ON c.countryid=z.countryid AND c.switch='Y' and z.switch='Y' ";
		$sql.= "  JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel` ch ON z.zoneid=ch.zoneid AND z.switch='Y' and ch.switch='Y' ";
        $sql.= " WHERE 1=1 ";
		
		if(!empty($countryid)) {
		  $sql.= "   AND c.countryid=${countryid} ";
		}
		
		$sql.=" ORDER BY c.countryid asc, z.zoneid asc, ch.channelid asc ";
		
        $table = $db->getQueryRecord($sql); 
		if(!empty($table['table']['record']))
		{
			return $table['table']['record'];
		}
		return false;
	
	}
	
	/*
	 *	新增連線來源等資訊
	 *	Add By Thomas 2015/07/01 
	 *	$come_from			varchar				連線來源
	 *	$productid 			int					商品編號
	 *	$act				varchar				執行動作
	 *	$goto				varchar				前往連結
	 *	$memo				varchar				備註
	 *	$userid				int					會員編號
	 *	$useragent			varchar				連線種類
	 *	$intro_by			varchar				來源編號
	 */ 
	public function logAffiliate($come_from, $productid, $act, $goto, $memo, $userid='', $user_agent='', $intro_by='') {
	    
		global $config;	
		if($act=='PROD|VIEW') {
		    error_log("userid : ".$userid." ".$act." productid :".$productid);
    		return 1;
        }		
		$db=new mysql($config["db2"]);
		$db->connect(); 
		
		$sql=" INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` 
						SET come_from='{$come_from}',
						    intro_by='{$intro_By}',
							userid='{$userid}',
							productid='{$productid}',
							user_agent='{$user_agent}',
	                        act='{$act}',
							goto='{$goto}',
							memo='{$memo}',
							insertt=NOW(),
							switch='Y' ";
		return $db->query($sql);
	}
	
	/*
	 *	統計連線來源筆數
	 *	$userid				int					會員編號
	 */ 
	public function  countRecommandUsers($userid) {
		global $config;

		$db=new mysql($config["db2"]);
		$db->connect();
   
		$sql = "SELECT count(distinct(userid)) as cnt 
				 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
				WHERE act='REG' 
				  AND switch='Y' 
				  AND intro_by='{$userid}' ";

		$table = $db->getQueryRecord($sql); 
		if(!empty($table['table']['record'][0])) {
		   return $table['table']['record'][0]['cnt'];
		} else {
		   return false;
		}		   
	}

	/*
	 *	新增會員關注商品
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態	
	 */ 	
	public function createUserCollectData($userid, $productid, $prodtype, $switch) {
		global $config;
		$db2 = new mysql($config["db2"]);
		$db2->connect();
		$sql=" INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_collect_rt` 
			  SET userid='{$userid}',
				  productid='{$productid}',
				  prod_type='{$prodtype}',
				  switch='{$switch}',
				  insertt=NOW() ";	
		$db2->query($sql);
		$id=mysqli_insert_id($db2->_con);
		error_log("[m/createUserCollect] ucrid : ".$id);
		return $id;
	}

	/*
	 *	更新會員關注商品
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態	
	 */ 	
	public function updateUserCollectData($switch, $ucrid, $userid='', $productid='') {
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_collect_rt` 
				 SET switch='{$switch}' 
			   WHERE 1=1 ";
		if(!empty($ucrid)) {
		  $sql.= " AND ucrid='{$ucrid}' ";  
		}
		if(!empty($userid)) {
		  $sql.= " AND userid='{$userid}' ";  
		}
		if(!empty($productid)) {
		  $sql.= " AND productid='{$productid}' ";  
		}
		error_log("[m/user/updateUserCollect]:".$sql);		   
		return $db->query($sql);
	}

	/*
	 *	取得會員關注商品資料
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態	
	 */ 	
	public function getUserCollectData($userid, $productid='', $prodtype='', $switch='') {
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$query = "SELECT * FROM saja_user.saja_user_collect_rt rt WHERE 1=1 ";
		if(!empty($userid))
		   $query.=" AND userid='{$userid}' ";
		if(!empty($prodtype))
		   $query.=" AND prod_type='{$prodtype}' "; 
		if(!empty($switch))
		   $query.=" AND switch='{$switch}' "; 
		if(!empty($productid))
		   $query.=" AND productid='{$productid}' ";

		error_log("[m/user/getUserCollectList]:".$query);

		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}				
	}
	
	/*
	 *	取得會員關注清單
	 *	$userid				int					會員編號
	 *	$productid			int					商品編號
	 *	$prodtype			varchar				商品型態
	 *	$switch				varchar				資料狀態	
	 */ 
	public function getCollectProdList($userid, $productid='', $prodtype='', $switch='') {
	       
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$query = "SELECT rt.ucrid, rt.userid, rt.productid, rt.switch, p.name, p.retail_price, p.thumbnail_url, unix_timestamp(p.offtime) as offtime, 
		                 IFNULL(pt.filename,'') as filename, p.ptid 
				  FROM saja_user.saja_user_collect_rt rt 
				  JOIN saja_shop.saja_product p ON 
						rt.productid=p.productid
				  LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON 
						p.prefixid = pt.prefixid
						AND p.ptid = pt.ptid
						AND pt.switch = 'Y'	
				  WHERE 1=1 ";
		if(!empty($userid)) {
		   $query.=" AND rt.userid='{$userid}' ";
		}   
		if(!empty($prodtype)) {
		   $query.=" AND rt.prod_type='{$prodtype}' "; 
		}
		if(!empty($switch)) {
		   $query.=" AND rt.switch='{$switch}' "; 
		}
		if(!empty($productid)) {
		   $query.=" AND rt.productid='{$productid}' ";
		}
		error_log("[m/user/getCollectProdList]:".$query);
		
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		
		if(empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			
			$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);

			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			foreach($table['table']['record'] as $tk => $tv)
			{
				if(empty($tv['thumbnail_url']) && !empty($tv['filename'])) {
				   $table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$tv['filename'];
				}
			}			
			
			return $table['table'];
		}

	}
	
	/*
	 *	取得會員簡訊驗証資料
	 *	$arrCond			array			 相關參數
	 */ 
	public function getSmsAuthData($arrCond) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();
		$query = "SELECT * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
				  WHERE 1=1 ";
		if(!empty($arrCond['phone'])) {
		  $query.= " AND `phone`='{$arrCond['phone']}' ";
		}  
		if(!empty($arrCond['code'])) {
		  $query.= " AND `code`='{$arrCond['code']}' ";
		}
		if(!empty($arrCond['authid'])) {
		  $query.= " AND `authid`={$arrCond['authid']} "; 
		}
		if(!empty($arrCond['userid'])) {
		  $query.= " AND `userid`={$arrCond['userid']} "; 
		}
		if(!empty($arrCond['switch'])) {
		  $query.= " AND `switch`='{$arrCond['switch']}' "; 
		}
		if(!empty($arrCond['verified'])) {
		  $query.= " AND `verified`='{$arrCond['verified']}' ";
		}
		if(!empty($arrCond['user_type'])) {
		  $query.= " AND `user_type`='{$arrCond['user_type']}' ";				  
		}
		$query.=" ORDER BY authid desc ";

		error_log("[m/user/getSmsAuthData]:".$query);

		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'])) {
		   return $table['table']['record'];
		} else {
		   return false;
		}	
	}

	/*
	 *	新增會員簡訊驗証資料
	 *	$arrUpd				array			 新增內容
	 */ 	
	public function createSmsAuthData($arrUpd) {
		global $db,$config;

		$sql = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
		 SET `prefixid`='{$config['default_prefix_id']}', `insertt`=NOW()";
		if(!empty($arrUpd['code'])) {
			$sql.=",`code`='{$arrUpd['code']}' ";
		} 		   
		if(!empty($arrUpd['phone'])) {
			$sql.=", `phone`='{$arrUpd['phone']}' ";     
		}
		if(!empty($arrUpd['userid'])) {
			$sql.=", `userid`='{$arrUpd['userid']}' ";     
		}
		if(!empty($arrUpd['verified'])) {		   
			$sql.=", `verified`='{$arrUpd['verified']}' ";
		}
		if(!empty($arrUpd['user_type'])) {		   
			$sql.=", `user_type`='{$arrUpd['user_type']}' ";
		}
		error_log("[m/user/createSmsAuthData]:".$sql);		   
		$db->query($sql);
		$authid=mysqli_insert_id($db->_con);
		error_log("[m/user/createSmsAuthData]code:".$arrUpd['code'].", phone:".$arrUpd['phone'].", userid:".$arrUpd['userid'].", authid:".$authid);
		return $authid;
	}
	
	/*
	 *	刪除會員簡訊驗証資料
	 *	$arrCond				array			 刪除條件
	 */ 	
	public function delSmsAuthData($arrCond) {
		global $db,$config;
		$sql = "DELETE * FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` ";
		$sql.=" WHERE 1=1 ";
		if(!empty($arrCond['authid'])) {
			$sql.=" AND `authid`='{$arrCond['authid']}' ";     
		}
		if(!empty($arrCond['phone'])) {
			$sql.=" AND `phone`='{$arrCond['phone']}' ";     
		}
		if(!empty($arrCond['userid'])) {
			$sql.=" AND `userid`='{$arrCond['userid']}' ";     
		}
		if(!empty($arrCond['code'])) {
			$sql.=" AND `code`='{$arrCond['code']}' ";     
		}
		if(!empty($arrCond['verified'])) {
			$sql.=" AND `verified`='{$arrCond['verified']}' ";     
		}
		if(!empty($arrUpd['user_type'])) {		   
			$sql.=" AND `user_type`='{$arrUpd['user_type']}' ";
		}
		$ret=$db->query($sql);
		error_log("[m/user/delSmsAuthData]".$ret.":".$sql);
		return $ret;		   
    }

	/*
	 *	取得會員簡訊驗証資料
	 *	$arrUpd				array			 更新內容
	 *	$arrCond			array			 更新條件	 
	 */ 	
    public function updateSmsAuthData($arrUpd, $arrCond) {
		global $db,$config;
		$sql = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` 
				  SET `modifyt`=NOW() ";
		if(!empty($arrUpd['verified'])) {		   
			$sql.=", `verified`='{$arrUpd['verified']}' ";
		}
		if(!empty($arrUpd['code'])) {		   
			$sql.=", `code`='{$arrUpd['code']}' ";
		}
		if(!empty($arrUpd['phone'])) {		   
			$sql.=", `phone`='{$arrUpd['phone']}' ";
		}
		if(!empty($arrUpd['userid'])) {		   
			$sql.=", `userid`='{$arrUpd['userid']}' ";
		}
		if(!empty($arrUpd['user_type'])) {		   
			$sql.=", `user_type`='{$arrUpd['user_type']}' ";
		}		   
		$sql.=" WHERE 1=1 ";
		if(!empty($arrCond['authid'])) {
			$sql.=" AND `authid`='{$arrCond['authid']}' ";     
		}
		if(!empty($arrCond['phone'])) {
			$sql.=" AND `phone`='{$arrCond['phone']}' ";     
		}
		if(!empty($arrCond['userid'])) {
			$sql.=" AND `userid`='{$arrCond['userid']}' ";     
		}
		if(!empty($arrCond['code'])) {
			$sql.=" AND `code`='{$arrCond['code']}' ";     
		}
		if(!empty($arrCond['verified'])) {
			$sql.=" AND `verified`='{$arrCond['verified']}' ";     
		}
		if(!empty($arrCond['user_type'])) {		   
			$sql.=" AND `user_type`='{$arrCond['user_type']}' ";
		}
		error_log("[m/user/updateSmsAuthData]:".$sql);		   
		return $db->query($sql);
    }

	
	/* 
	 *	修改個人資料-頭像修改
	 *	$userid		int			會員編號
	 *	$picname	varchar		圖片檔名
 	 */	
	public function updateHeadImg($userid, $picname) {
	   global $config;
	   $db = new mysql($config["db2"]);
	   $db->connect();
	   $sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
				SET thumbnail_file ='{$picname}' 
				WHERE userid='{$userid}'";

	   error_log("[m/user/updateHeadImg]:".$sql);		   
	   return $db->query($sql);
	}
	
	/* 
	 *	取得用戶等級及經驗點數值
	 *	By Thomas 20160407
	 *	$userid		int			會員編號
 	 */		
	public function getUserLvlExpts($userid) {
		if(empty($userid)) {
			error_log("[m/user] empty userid !! ");
			return false;
		}
		global $config;
		$db = new mysql($config["db2"]);
		$db->connect();
		$sql = "  SELECT e.userid, e.total_expts, m.level, m.mdlid, m.title
				   FROM (SELECT userid, IFNULL(SUM(pts),0) as total_expts 
						   FROM saja_user.saja_user_expts 
						  WHERE switch='Y' and userid={$userid} ) e 
				   JOIN saja_medals_def m 
					 ON e.total_expts between m.min_expts and m.max_expts 
					AND m.switch='Y' AND m.type='E'
				   WHERE e.userid={$userid} ";
		error_log("[m/user/getUserExpts]:".$sql);
		$table = $db->getQueryRecord($sql); 
		if(!empty($table['table']['record'])) {
		   return $table['table']['record'][0];
		} else {
		   return false;
		}	   
	}
	
	/* 
	 *	修改卡包資訊
	 *	$arrCond		array			更新條件
	 *	$arrInfo		array			更新內容
 	 */		
	public function updCardPackInfo($arrCond, $arrInfo) {
		global $config, $db;
		$sql = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo` 
				  SET modifyt=NOW() ";
		for($idx=1;$idx<=8;++$idx) {
		   $sql.=",field".$idx."name='".$arrInfo['field'.$idx.'name']."'"; 
		}
		if(!empty($arrInfo['switch'])) {
		   $sql.=",switch='".$arrInfo['switch']."' ";
		}
		if(!empty($arrInfo['seq'])) {
		   $sql.=",seq=".$arrInfo['seq'];
		}
		$sql.=" WHERE 1=1 ";
		if(!empty($arrCond['ueid'])) {
		  $sql.=" AND ueid='".$arrCond['ueid']."' ";
		}
		if(!empty($arrCond['uecid'])) {
		  $sql.=" AND uecid='".$arrCond['uecid']."' "; 
		}
		if(!empty($arrCond['userid'])) {
		  $sql.=" AND userid='".$arrCond['userid']."' ";
		}
		if(!empty($arrCond['switch'])) {
		  $sql.=" AND switch='".$arrCond['switch']."' ";  
		}  
		error_log("[m/user/updCardPackInfo]:".$sql);
		$ret=$db->query($sql);
		if($ret==1) {
			error_log("[m/user/updCardPackInfo]: userid:".$arrCond['userid']." updated ueid:".$ueid);
		} 
		return $ret;
	}
	
	/* 
	 *	新增卡包資訊
	 *	$arrInfo		array			新增內容
 	 */		
	public function addCardPackInfo($arrInfo) {
		global $config, $db;
		$sql = " INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_extrainfo`  
						SET prefixid='saja', modifyt=NOW(), insertt=NOW() ";
		for($idx=1;$idx<=8;++$idx) {
		   $sql.=",field".$idx."name='".$arrInfo['field'.$idx.'name']."'"; 
		}
		if(!empty($arrInfo['uecid']))
		  $sql.=" ,uecid='".$arrInfo['uecid']."' "; 
		if(!empty($arrInfo['userid']))
		  $sql.=" ,userid='".$arrInfo['userid']."' ";
		if(!empty($arrInfo['switch']))
		  $sql.=" ,switch='".$arrInfo['switch']."' "; 
		  
		error_log("[m/user/addCardPackInfo]:".$sql);
		$ret = $db->query($sql);
		if($ret==1) {
			$ueid=mysqli_insert_id($db->_con);
			error_log("[m/user/addCardPackInfo]: userid:".$arrInfo['userid']." created ueid:".$ueid);
			return $ueid;
		} else {
			return $ret;
		}
	}
	
	/* 
	 *	會員中標商品List
	 *	$userid			int			會員編號
	 *	$productid		int			商品編號
 	 */		
	public function getUserAwardProdList($userid='', $productid='') {
		global $config, $db;
		$query = " SELECT pgp.*, p.retail_price, p.name, p.closed, p.thumbnail_url
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
					JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
					  ON pgp.productid=p.productid
					 AND pgp.switch='Y' 
				 WHERE 1=1 
					 AND p.closed='Y'
					 AND p.switch='Y' 
					 AND pgp.switch='Y' 
					 AND pgp.complete='Y' ";
		if(!empty($userid)) {
		 $query.=" AND pgp.userid='{$userid}' ";
		}	
		if(!empty($productid)) {
		 $query.=" AND pgp.productid='{$productid}' ";
		}	
		$query.=" ORDER BY p.offtime desc ";
		error_log("[m/user/getUserAwardProdList]:".$query);	
		$table = $db->getQueryRecord($query); 
		if(!empty($table['table']['record'])) {
		  return $table['table']['record'];
		} else {
		  return false;
		}		  
	}
	
	/* 
	 *	我的殺友清單
	 *	$userid		int			會員編號
	 *	$nickname	varchar		搜尋名稱
	 * 	$sIdx		int		 	目前頁碼
	 * 	$perpage	int 		筆數
 	 */	
	public function getFriendsList($userid, $nickname='', $sIdx='', $perpage='') {
		global $config, $db;
		
		//SQL指令 - 取得同推薦人的殺友清單
		$query = " SELECT ua.userid, up.nickname, up.thumbnail_file, up.thumbnail_url, up.gender, p.name as come_from 
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate` ua
					LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up 
					ON ua.userid = up.userid 
					LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` p 
					ON p.provinceid = up.provinceid 
					WHERE ua.intro_by = '{$userid}' 
					AND ua.userid <> '{$userid}' 
					AND ua.act='REG' ";
		
		if (!empty($nickname)){
			$query .= " AND up.nickname LIKE '%{$nickname}%'";
		}			
		
		$query .= " ORDER BY ua.userid ASC";

		error_log("[m/user/getFriendsList]:".$query);
		//總筆數
		$getnum = $db->getQueryRecord($query);
		$getnum	= count($getnum['table']['record']);
		$num = (!empty($getnum)) ? $getnum : 0;
		$perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
				
		
		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			if(!empty($perpage) && !empty($sIdx)) {			
				$query_limit.= " LIMIT ".$sIdx.",".$perpage;
			}else{				
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			foreach($table['table']['record'] as $tk => $tv) {			
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/".$tv['thumbnail_file'];
				} elseif(empty($tv['thumbnail_url']) && empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/_DefaultHeadImg.jpg";
				}
				
				unset($table['table']['record'][$tk]['thumbnail_file']);
			}
	
			return $table['table'];
		}
		
		return false;	
	}		
	
	/* 
	 *	修改会员数据
	 *	$arrCond		array			更新條件
	 *	$arrUpd			array			更新內容
 	 */		
	public function updUserData($arrUpd, $arrCond) {
	
		global $config, $db;	
		if(count($arrCond)<1 || count($arrUpd)<1) {
			return false;
		}		
        	
		$idx=0;
		$r=0;
		$query = "UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` SET prefixid='saja' ";
		if(!empty($arrUpd['name'])) {
		   $query.=", `name`='{$arrUpd['name']}' ";
		   $idx++;
		}
		if(!empty($arrUpd['passwd'])) {
		   $query.=", `passwd`='{$arrUpd['passwd']}' ";
		   $idx++;
		}
		if(!empty($arrUpd['exchangepasswd'])) {
		   $query.=", `exchangepasswd`='{$arrUpd['exchangepasswd']}' ";
		   $idx++;
		}
		
		$query.=" WHERE `prefixid` = '{$config['default_prefix_id']}' ";
		if(!empty($arrCond['userid'])) {
		   $query.=" AND `userid`='".$arrCond['userid']."' ";
		}
		if(!empty($arrCond['name'])) {
		   $query.=" AND `name`='".$arrCond['name']."' ";
		}
		
		try {
		   $r=$db->query($query);
		   error_log("[m/user/updUserData]:".$query."-->".$r);
        } catch (Exception $e) {
           error_log("[m/user/updUserData]:".$query."-->".$e->getMessage());
		   $r=-1;
        } finally {
            return $r;
        }		
	}
	
	/* 
	 *	修改個人資料-手機號碼修改
	 *	$userid		int			會員編號
	 *	$phone		varchar		手機號碼
 	 */	
	public function updUserProfileData($phone, $userid) {
	   global $config, $db;	
	   $r=0;
	   $sql=" UPDATE `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` 
				SET phone ='{$phone}' 
				WHERE userid='{$userid}'";
		try {
			$r=$db->query($sql);
			error_log("[m/user/updUserProfileData]:".$sql."-->".$r);	
		} catch (Exception $e) {
			error_log("[m/user/updUserProfileData]:".$sql."-->".$e->getMessage());
			$r=-1;
		} finally {
			return $r;
		}		
	}
	
	/* 
	 *	殺手榜
	 *	$type		int			排序方式
	 * 	$sIdx		int		 	目前頁碼
	 * 	$perpage	int 		分頁筆數
 	 */	
	public function getAssassinList($type, $sIdx='', $perpage='') {
		global $config, $db;
		
		error_log("[m/user/getAssassinList]: type:".$type.",sIdx:".$sIdx.",perpage:".$perpage);
		switch($type) {
			case 1 :$query = " SELECT up.userid, up.nickname, up.thumbnail_file, up.thumbnail_url, p.name as come_from, 
						  (select COUNT(pgp.userid) from `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp where pgp.userid = up.userid and pgp.switch='Y' ) as num_award,
					      '' as prod_name, '' as award_time
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up
					LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` p 
					ON p.provinceid = up.provinceid 
					GROUP BY up.userid 
					HAVING num_award > 2
					ORDER BY num_award DESC ";break;
			case 2 : $query = "SELECT up.userid, up.nickname, up.thumbnail_file, up.thumbnail_url, p.name as come_from, 
						           1 as num_award, pp.name as prod_name, pgp.insertt as award_time									  
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
		                 JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` pp
						   ON pgp.productid=pp.productid				
					     JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up
						   ON pgp.userid=up.userid
					LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` p 
					       ON up.provinceid=p.provinceid
                     WHERE pgp.insertt > NOW()-INTERVAL 15 DAY						   
				     ORDER BY pgp.insertt DESC ";break;
		}			
		
		//總筆數
		$getnum = $db->getQueryRecord("select count(*) as num from  (".$query." ) cnt ");
		$num = (!empty($getnum)) ? ($getnum['table']['record'][0]['num']) : 0;
		error_log("[m/user/getAssassinList]:".$query . $query_limit."-->".$num);
		if (empty($perpage)) {
		    $perpage = (!empty($_POST['perpage'])) ? $_POST['perpage'] : $_GET['perpage'];
		}
		if (empty($perpage)) {
			$perpage = $config['max_page'];
		}
		
		if($num) {
			//分頁資料
			$page = $db->recordPage($num, $this);
			if(!empty($perpage) && !empty($sIdx)) {			
				$query_limit.= " LIMIT ".$sIdx.",".$perpage;
			} else {				
				$query_limit = " LIMIT ". ($page["rec_start"]-1) .",". ($perpage);
			}
			//取得資料
			error_log("[m/user/getAssassinList]:".$query . $query_limit);
			$table = $db->getQueryRecord($query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}
		
		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			foreach($table['table']['record'] as $tk => $tv) {			
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/".$tv['thumbnail_file'];
				} elseif(empty($tv['thumbnail_url']) && empty($tv['thumbnail_file'])) {
					$table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.APP_DIR."/images/headimgs/_DefaultHeadImg.jpg";
				}
				
				unset($table['table']['record'][$tk]['thumbnail_file']);
			}
	
			return $table['table'];
		}
		
		return false;	
	}

	/*
	 *	商品訂單資料
	 *	$orderid	int			訂單編號	
	 */
	public function getOrderDetail($orderid='', $type) {
		global $config, $db;
		
		$query = " SELECT o.orderid, o.userid, o.status, o.type, o.num, o.esid, o.point_price, o.process_fee, o.total_fee, o.profit,
						  o.memo, o.confirm, oc.name, oc.address, oc.phone, oc.zip, oc.gender, o.insertt, os.outtime, os.outtype, os.outcode, 
						  os.outmemo, os.backtime, os.backmemo";
		if ($type == "saja") {
			$query .= " ,(select p.productid from `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON pgp.productid=p.productid AND p.switch='Y' where pgp.pgpid = o.pgpid and pgp.switch='Y' ) as prod_id
						,(select p.name from `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
						ON pgp.productid=p.productid AND p.switch='Y' where pgp.pgpid = o.pgpid and pgp.switch='Y' ) as prod_name
						"; 				  
		} else {
			$query .= " ,o.epid as prod_id, (select ep.name from `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_product` ep where ep.epid = o.epid and ep.switch='Y' ) as prod_name "; 				  
		}
		
		$query .= " FROM `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order` o
					LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_consignee` oc
						ON o.orderid=oc.orderid AND o.switch='Y'
					LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}order_shipping` os
						ON o.orderid=os.orderid AND os.switch='Y'	
					WHERE o.switch='Y' 
						AND oc.switch='Y'
						AND o.orderid = {$orderid}";
		error_log("[m/user/getOrderDetail]:".$query);	
		$table = $db->getQueryRecord($query); 
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}		  
	}

	/*
	 *	合格人數資料
	 *	$userid		int			會員編號	
	 */
	public function  RecommandDepositUsers($userid) {
		global $config;

		$db = new mysql($config["db2"]);
		$db->connect();
		
		if(!empty($userid)) {

			$sql = "SELECT userid, insertt 
					 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
					WHERE act='REG' 
					  AND switch='Y' 
					  AND intro_by='{$userid}' ";

			$table = $db->getQueryRecord($sql);
			
			$count = 0;
			
			foreach($table['table']['record'] as $tk => $tv){			
				if (!empty($tv['userid'])){
					$sql = "SELECT count(depositid) as num  
							 FROM `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}deposit`
							WHERE switch='Y' 
							  AND userid='{$tv['userid']}'
							  AND modifyt > '{$tv['insertt']}' ";

					$table2 = $db->getQueryRecord($sql);
					
					if ($table2['table']['record'][0]['num'] > 0){
						$count = $count + 1; 
					}
				} 
			}
			error_log("[m/user/RecommandDepositUsers] count :".$count);

			return $count;
		} else {
		   return false;
		}		   
	}

	/*
	 *	推薦人資料
	 *	$userid		int			會員編號	
	 */
	public function getAffiliateUser($userid) {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		if(!empty($userid)) {
			$sql = "SELECT intro_by, insertt 
					 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
					WHERE act='REG' 
					  AND switch='Y' 
					  AND userid='{$userid}' ";

			$table = $db->getQueryRecord($sql);
				
			if(!empty($table['table']['record'][0])) {
				return $table['table']['record'][0];
			} else {
			   return false;
			}
		}	
	}

	/*
	 *	會員微信openid資料
	 *	$userid		int			會員編號	
	 */
	public function getUserWeixinOpenID($userid) {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		if(!empty($userid)) {
			$sql = "SELECT uid 
					 FROM `{$config['db'][0]['dbname']}`.`v_user_profile_sso`
					WHERE sso_name='weixin'					
					  AND userid='{$userid}' ";

			$table = $db->getQueryRecord($sql);
				
			if(!empty($table['table']['record'][0])) {
				return $table['table']['record'][0];
			} else {
			   return false;
			}
		}	
	}

	/*
	 *	會員商戶資料
	 *	$userid		int			會員編號	
	 */
	public function get_user_enterprise_rt($userid) {
		global $config;
		
		$db=new mysql($config["db2"]);
		$db->connect();
		
		if(!empty($userid)) {
			$sql = "SELECT count(ueid) as num 
					 FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt`
					WHERE switch='Y' 
					  AND userid='{$userid}' ";

			$table = $db->getQueryRecord($sql);
				
			if(!empty($table['table']['record'][0])) {
				return $table['table']['record'][0];
			} else {
				return false;
			}
		}	
	}
	
}
?>
