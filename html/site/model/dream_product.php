<?php
/**
 * DreamProduct Model 模組
 */

class DreamProductModel {
    public $msg;
	public $sort_column = 'name|seq|modifyt';

    public function __construct() {
    }

	/**
     * Base Method : set_sort
     */
	public function set_sort() {
		global $status;

		$status["status"]["sort_path"] = '';
		$status["status"]["sort"] = '';
		$sub_sort_query = '';

		// 調整排序欄位請修改下列 Modify here to assign sort columns
		// $sort_column = "name|seq|modifyt";

		if($_GET) {
			foreach($_GET as $gk => $gv) {
				if (preg_match("/^sort_(". $this->sort_column .")/", $gk, $matches) && !empty($gv)) {
					$status["status"]["sort"][ "sort_".$matches[1] ] = $_GET[ "sort_".$matches[1] ];
					$status["status"]["sort_path"] .= "&sort_". $matches[1] ."=". $_GET[ "sort_".$matches[1] ];
				}
			}
		}

		if(is_array($status["status"]["sort"])) {
			$orders = array();
			foreach($status["status"]["sort"] as $sk => $sv) {
				if($sv != 'asc' && $sv != 'desc') {
					echo "Sort Params is wrong!!!"; exit  ;
				}
				$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
			}
			$sub_sort_query =  " ORDER BY " . implode(',', $orders);
		}

		return $sub_sort_query;
	}

	/**
     * Search Method : product_list_set_search
     */
	public function product_list_set_search() {
		global $status, $config;

		$rs = array();

		$rs['join_query'] = '';
		$status["status"]["search_path"] = '';
		$status["status"]["search"] = '';
		$rs['sub_search_query'] = ($_SESSION['m_prefix'] == 'm') ? "AND p.mob_type !='N' " : '';

		$rs['sub_search_query'] .= " AND p.closed = 'N'
			AND p.display = 'Y'
			AND p.switch = 'Y'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND (
				(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
				OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N')
			) ";
		if(!empty($_GET["pcid"])) {
			$status["status"]["search"]["pcid"] = $_GET["pcid"] ;
			$status["status"]["search_path"] .= "&pcid=".$_GET["pcid"] ;

			$rs['join_query'] .= "LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pc ON
			p.prefixid = pc.prefixid
			AND p.productid = pc.productid
			AND pc.`pcid` = '{$_GET["pcid"]}'
			AND pc.switch = 'Y' ";

			$rs['sub_search_query'] .= " AND pc.pcid IS NOT NULL
			";
		}

		return $rs;
	}

	//圓夢商品分類
	public function dream_product_category_list($pcid=null){
		global $config, $db;

		/*
		$rkey='product_category_list';
	    $redis = getRedis();
        $jsonStr = $redis->get($rkey);
		if(!empty($jsonStr)) {
		   return json_decode($jsonStr,true);
		}
		*/

		$query ="SELECT seq, pcid, name, thumbnail_url FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category`
		        WHERE `prefixid` = '{$config['default_prefix_id']}' ";
		if(!empty($pcid) ){
			$query .="AND `pcid` = {$pcid}";
		}
		
		$query .="	AND `layer` = 1
					AND `switch` = 'Y'
					AND `ptype` = 1
			   ORDER BY seq ";

		error_log("[dream_product_category_list]: ". $query);
		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])) {
			$results = $table['table']['record'];
			//$redis->set($rkey,json_encode($results));
			return $results;
		}
		return false;
	}

	public function dream_product_list($pcid, $p){
		global $config, $db, $product;

		//排序
		$set_sort = $this->set_sort();

		//SQL指令
		$query_count = " SELECT count(*) as num ";

		$query_record = "SELECT unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`, pt.filename as thumbnail
								, p.ptid, p.productid, p.name, p.ptid, p.offtime, p.is_flash, pc.pcid, p.limitid, p.lb_used ,
								'0' as is_bidwinner";

		$query = " FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p ";

		$query.= " LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt ON
			p.prefixid = pt.prefixid
			AND p.ptid = pt.ptid
			AND pt.switch = 'Y'
		LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pc ON
			p.prefixid = pc.prefixid
			AND p.productid = pc.productid
			AND pc.switch = 'Y'
		WHERE
			p.prefixid = '{$config['default_prefix_id']}'
			AND p.ptype = '1'
			AND p.is_flash != 'Y'
			AND p.closed = 'N'
			AND p.display = 'Y'
			AND p.switch = 'Y'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND (
				(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
				OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N'))
		";
		if(!empty($pcid) ) {
			$query.= " AND pc.`pcid` = '{$pcid}' ";
		}else{
			$query.= " AND p.`display_all` = 'Y' ";
		}

		$query .= ($set_sort) ? $set_sort : " GROUP BY p.productid ORDER BY p.offtime ASC, p.productid ASC, p.seq ASC";

		//總筆數
		$getnum = $db->getQueryRecord($query_count . $query);
		//$num = (!empty($getnum['table']['record'][0]['num']) ) ? $getnum['table']['record'][0]['num'] : 0;
		$num = count($getnum['table']['record'] > 0) ? count($getnum['table']['record']) : 0;

		// error_log("[m/dream_product_list] 1 :".$getnum['table']['record'][0]['num'] );

		if($num) {
			$config['max_page'] = 10;
			//分頁資料
			$page = $db->recordPage($num, $this);
			$start = ($p-1)*($config['max_page']);
			$query_limit = " LIMIT ". $start .",". $config['max_page'];
			
			error_log("[dream_product_list]: ". $query_record . $query . $query_limit);
			
			//取得資料
			$table = $db->getQueryRecord($query_record . $query . $query_limit);
		} else {
			$table['table']['record'] = '';
		}

		if(!empty($table['table']['record'])) {
			$table['table']['page'] = $page;
			
			foreach($table['table']['record'] as $tk => $tv) {
				$productid = $tv['productid'];
				$tv['offtime']=$tv['unix_offtime'];
				$table['table']['record'][$tk]['thumbnail'] = '';
				if(empty($tv['thumbnail_url']) && !empty($tv['thumbnail'])) {
					$table['table']['record'][$tk]['thumbnail_url']=BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['thumbnail'];
				}
				
				// Relation product_rule_rt 下標條件關聯
				$rule_rt = $this->get_product_rule_rt($productid);
				$table['table']['record'][$tk]['srid'] = $rule_rt['srid'];
				$table['table']['record'][$tk]['value'] = $rule_rt['value'];

				// 取得商品限定相關資料
				$product_limit = $this->get_product_limited($tv['limitid']);
				$table['table']['record'][$tk]['plname'] = $product_limit['plname'];
				$table['table']['record'][$tk]['mvalue'] = $product_limit['mvalue'];
				$table['table']['record'][$tk]['svalue'] = $product_limit['svalue'];
				$table['table']['record'][$tk]['kind'] = $product_limit['kind'];
				$table['table']['record'][$tk]['plpic'] = $product_limit['plpic'];
				$table['table']['record'][$tk]['colorcode'] = $product_limit['colorcode'];
				
				//目前中標
				$bidded = $product->get_bidded($productid);
				$num_bids_gap = $product->getNumBidsGap($productid);
				
				$srcip = $bidded['src_ip'];
				if(! empty($bidded['src_ip']) ){
					preg_match_all('/[0-9]+/i',$bidded['src_ip'], $src_ip);
					$src_ip[0][1] = "***";
					$src_ip[0][2] = "***";
					$srcip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				}
				
				$curr_winner_userid = empty($bidded['userid'])?"":$bidded['userid'];
				$table['table']['record'][$tk]['curr_winner_userid'] = $curr_winner_userid;
				$table['table']['record'][$tk]['bidded'] = empty($bidded['nickname'])?"":$bidded['nickname'];

				if($curr_winner_userid==""){
					$table['table']['record'][$tk]['bidded'] = "_無人得標_";
					//$table['table']['record'][$tk]['final_bid_price'] = 0;
					$table['table']['record'][$tk]['src_ip'] = '';
					$table['table']['record'][$tk]['curr_winner_comefrom'] = '';
					$table['table']['record'][$tk]['curr_winner_headimg'] = '';
					$table['table']['record'][$tk]['curr_winner_headimg_url'] = '';
				} else {
					$table['table']['record'][$tk]['src_ip'] = $srcip;

					if(strpos($srcip, ",")){
						$table['table']['record'][$tk]['src_ip'] = substr($srcip, 0, strpos($srcip,","));
					}
					$table['table']['record'][$tk]['curr_winner_comefrom'] = empty($bidded['comefrom'])?"":$bidded['comefrom'];
					$table['table']['record'][$tk]['curr_winner_headimg'] = $bidded['thumbnail_file'];
					$table['table']['record'][$tk]['curr_winner_headimg_url'] = empty($bidded['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bidded['thumbnail_url'];
				}
				//$table['table']['record'][$tk]['num_bids_gap'] = empty($num_bids_gap[0]['num'])?$get_product['saja_limit']:($get_product['saja_limit']-$num_bids_gap[0]['num']);

				$bid_winner = $this->get_bid_winner($productid);
				if (!empty($bid_winner)) {
					//目前得標者圖示
					// $user_thumbnail = empty($bid_winner['thumbnail_file']) ? $bid_winner['thumbnail_url'] : BASE_URL.HEADIMGS_DIR."/".$bid_winner['thumbnail_file'];
					// if (empty($user_thumbnail)){
						// if (!empty($bid_winner['shd_thumbnail_file'])){
								// $table['table']['record'][$tk]['thumbnail_url'] = IMG_URL.'/site/images/dream/products/'.$bid_winner['shd_thumbnail_file'];
						// }else{
							// $table['table']['record'][$tk]['thumbnail_url'] = BASE_URL.APP_DIR."/images".APP_DIR."/product/".$tv['thumbnail'];
						// }
					// }else{
						// $table['table']['record'][$tk]['thumbnail_url'] = $user_thumbnail;
					// }
					//$table['table']['record'][$tk]['is_bidwinner'] = 1;
				}
			}

			return $table;
		}

		return false;
	}

	public function get_dream_product_info($productid){
		global $config, $db;

		$query = " SELECT p.*,
				unix_timestamp(p.offtime) as offtime, unix_timestamp(p.offtime) as unix_offtime, unix_timestamp() as `now`,
				IFNULL(pt.filename,'') as thumbnail,
				IFNULL(pgp.userid,'') as final_winner_userid,
				IFNULL(pgp.price,0) as final_bid_price,
				pt2.filename as thumbnail2,
				pt3.filename as thumbnail3,
				pt4.filename as thumbnail4,
				IFNULL(rule.srid,'any_saja') as srid,
				IFNULL(rule.value,0) as srvalue,
				pc.name as c_name
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt
				ON p.prefixid = pt.prefixid
				AND p.ptid = pt.ptid
				AND pt.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt2
				ON p.prefixid = pt2.prefixid
				AND p.ptid2 = pt2.ptid
				AND pt2.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt3
				ON p.prefixid = pt3.prefixid
				AND p.ptid3 = pt3.ptid
				AND pt3.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_thumbnail` pt4
				ON p.prefixid = pt4.prefixid
				AND p.ptid4 = pt4.ptid
				AND pt4.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` pgp
				ON	p.productid = pgp.productid
				AND pgp.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt` rule
				ON p.prefixid=rule.prefixid
				AND p.productid = rule.productid
				AND rule.switch='Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category_rt` pcr
				ON p.prefixid=pcr.prefixid
				AND pcr.pcid <> 0
				AND p.productid = pcr.productid
				AND pcr.switch='Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_category` pc
				ON pc.prefixid=pcr.prefixid
				AND pc.pcid = pcr.pcid
				AND pc.switch='Y'
			WHERE
				p.prefixid = '{$config['default_prefix_id']}'
				AND p.productid = '{$productid}'
				AND p.switch = 'Y'
				AND p.display = 'Y'
				AND unix_timestamp() >= unix_timestamp(p.ontime)
		";

		$query = $query." LIMIT 1";
		$recArr = $db->getQueryRecord($query);

		if(!empty($recArr['table']['record'][0])) {
			//中标处理费 = 处理费% * 市價 * 兌換比率
			$recArr['table']['record'][0]['process_fee_ratio']=$recArr['table']['record'][0]['process_fee'];
			$process_fee = ( (float)$recArr['table']['record'][0]['process_fee']/100 ) * (float)$recArr['table']['record'][0]['retail_price']  * $config['sjb_rate'];
			$recArr['table']['record'][0]['process_fee'] = strval(round($process_fee, 0));

			//市價
			$recArr['table']['record'][0]['retail_price'] = str_replace(",","",(number_format($recArr['table']['record'][0]['retail_price']))); //number_format($recArr['table']['record'][0]['retail_price'], 2);

			//下标手续费
			$recArr['table']['record'][0]['saja_fee'] =  round($recArr['table']['record'][0]['saja_fee'], 0);

			//商品简介
			$description = (!empty($recArr['table']['record'][0]['description']) ) ? $recArr['table']['record'][0]['description'] : '';
			$recArr['table']['record'][0]['description'] = html_decode($description);

			$rule.='<p>◎ 下標前請先至殺友專區->我的帳號 填妥收件人資訊。</p>
			<p>◎ 本商品每次許願需支付殺價幣&nbsp; $'.$recArr['table']['record'][0]['saja_fee'].'/標。</p>
			<p>◎ 時間到時，出價最低且不與他人重複者得標。</p>
			<p>◎ 得標者需在3日內結帳，違者視同棄標。</p>
			<p>◎ 棄標者不得要求退回已支付手續費。</p>
			<p>◎ <span style="color: #E32A00">得標者須在結帳時支付得標處理費 $'.ceil($recArr['table']['record'][0]['process_fee']+$recArr['table']['record'][0]['checkout_money']).'</span>。</p>
			<p>◎ <span style="color: #E32A00">得標處理費可由已下標手續費100%全額折抵</span>。</p>
			<p>◎ <span style="color: #E32A00">未得標者不損失，手續費'.ceil($recArr['table']['record'][0]['bonus']).'%轉贈鯊魚點</span>。</p>
			<p>◎ 鯊魚點可至商城1:1使用&nbsp; 請安心下標。</p>';
			$recArr['table']['record'][0]['rule']  = (!empty($recArr['table']['record'][0]['rule']) ) ? $recArr['table']['record'][0]['rule'] : $rule;

			// Relation product_rule_rt 下標條件關聯
			$rule_rt = $this->get_product_rule_rt($productid);

			$recArr['table']['record'][0]['srid'] = $rule_rt['srid'];
			$recArr['table']['record'][0]['value'] = $rule_rt['value'];

			// 取得商品限定相關資料
			$product_limit = $this->get_product_limited($recArr['table']['record'][0]['limitid']);

			$recArr['table']['record'][0]['plname'] = $product_limit['plname'];
			$recArr['table']['record'][0]['mvalue'] = $product_limit['mvalue'];
			$recArr['table']['record'][0]['svalue'] = $product_limit['svalue'];
			$recArr['table']['record'][0]['kind'] = $product_limit['kind'];
			$recArr['table']['record'][0]['plpic'] = $product_limit['plpic'];
			$recArr['table']['record'][0]['colorcode'] = $product_limit['colorcode'];
			$recArr['table']['record'][0]['limit_group'] = $product_limit['limit_group'];


			$recArr['table']['record'][0]['product_offtime']=$recArr['table']['record'][0]['offtime'];
			$recArr['table']['record'][0]['offtime']=(int)$recArr['table']['record'][0]['unix_offtime'];
			// $recArr['table']['record'][0]['thumbnail']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail'];
            $recArr['table']['record'][0]['thumbnail']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail'];

			if (!empty($recArr['table']['record'][0]['thumbnail2'])) {
				// $recArr['table']['record'][0]['thumbnail2']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail2'];
				$recArr['table']['record'][0]['thumbnail2']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail2'];
			}
			if(!empty($recArr['table']['record'][0]['thumbnail3'])) {
				// $recArr['table']['record'][0]['thumbnail3']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail3'];
			    $recArr['table']['record'][0]['thumbnail3']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail3'];

			}
			if(!empty($recArr['table']['record'][0]['thumbnail4'])) {
				// $recArr['table']['record'][0]['thumbnail4']=BASE_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail4'];
				$recArr['table']['record'][0]['thumbnail4']=IMG_URL.APP_DIR.'/images/site/product/'.$recArr['table']['record'][0]['thumbnail4'];
			}

			// $title = (!empty($recArr['table']['record'][0]['c_name']) ) ? "夢想許願池(".$recArr['table']['record'][0]['c_name'].")" : '夢想許願池';
			$recArr['table']['record'][0]['title'] = $recArr['table']['record'][0]['name'];
			
		}else{
			return false;
		}

		return $recArr['table']['record'][0];
	}

	/*
	* 下標條件關聯
	*/
	public function get_product_rule_rt($productid)	{
        global $db, $config;

		$query ="SELECT pr.srid, pr.value
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_rule_rt` pr
		WHERE
			pr.prefixid = '{$config['default_prefix_id']}'
			AND pr.productid = '{$productid}'
			AND pr.switch = 'Y'
		";

		$recArr = $db->getQueryRecord($query);
		if(empty($recArr['table']['record'][0])) {
			$recArr['table']['record'][0]['srid'] = '';
			$recArr['table']['record'][0]['value'] = '';
		}

		return $recArr['table']['record'][0];
	}

	/*
	 *	限定條件關聯
	 *	$limitid				int					限定編號
	 */
	public function get_product_limited($limitid) {

        global $db, $config;

		$query ="SELECT pl.name as plname, pl.mvalue, pl.svalue, pl.kind, pl.thumbnail as plpic, c.colorcode, pl.limit_group
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product_limited` pl
		LEFT JOIN `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}color` c
			ON pl.colorid = c.cid
			AND c.used = 'Y'
			AND c.switch = 'Y'
		WHERE
			pl.plid = '{$limitid}'
			AND pl.used = 'Y'
			AND pl.switch = 'Y'
		";
		$recArr = $db->getQueryRecord($query);

		if(empty($recArr['table']['record'][0])) {
			$recArr['table']['record'][0]['plname'] = '';
			$recArr['table']['record'][0]['mvalue'] = '';
			$recArr['table']['record'][0]['svalue'] = '';
			$recArr['table']['record'][0]['kind'] = '';
			$recArr['table']['record'][0]['plpic'] = '';
			$recArr['table']['record'][0]['colorcode'] = '';
		}

		return $recArr['table']['record'][0];
    }

	/*
	 *	取得目前得標者資訊
	 */
	public function get_bid_winner($productid){

		global $db, $config;

        if(!empty($productid)) {
            $query ="SELECT sh.shid, sh.userid, sh.productid, sh.nickname, sh.price, sh.src_ip, sh.insertt, up.thumbnail_file, up.thumbnail_url, shd.thumbnail_filename as shd_thumbnail_file
			   FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
			LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up ON
				sh.prefixid = up.prefixid
				AND sh.userid = up.userid
				AND up.switch = 'Y'
			LEFT OUTER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` shd ON
				sh.shdid = shd.shdid
				AND shd.switch = 'Y'
			WHERE sh.prefixid = '{$config['default_prefix_id']}'
				AND sh.productid = '{$productid}'
				AND sh.switch = 'Y'
                AND sh.type = 'bid'
				AND sh.userid IS NOT NULL
				GROUP BY sh.price
				HAVING COUNT(sh.price) = 1
				ORDER BY sh.price ASC
				LIMIT 1 ";
            // error_log("[m/model/get_bidded] : ".$query);

			$recArr = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'][0] = '';
		}

		if(!empty($recArr['table']['record'][0]) && $recArr['table']['record'][0]['price']>0) {
			//error_log("[m/model/get_bidded] : ".json_encode($recArr['table']['record'][0]));
            return $recArr['table']['record'][0];
		}

		return false;

	}

	/*
	 *	取得最新五筆出價商品資料
	 */
	public function get_history_detail($productid){

		global $db, $config;

        if(!empty($productid)) {
            $query ="SELECT shdid, userid, productid, title, content, purchase_url, thumbnail_filename, thumbnail_name as pic_name, memo, insertt
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` as shd, 
					(SELECT MAX(shdid) as max_shdid FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` GROUP BY title) shd2
					WHERE shd.shdid = shd2.max_shdid
					AND productid = '{$productid}'
					AND switch = 'Y'
					ORDER BY shd.insertt DESC LIMIT 5 ";

            // error_log("[m/model/get_bidded] : ".$query);

			$recArr = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'] = array();
		}

		return $recArr['table']['record'];
	}

	/*
	 *	取得最新使用者最後出價商品資料
	 */
	public function get_user_history_detail($productid,$userid){

		global $db, $config;

        if(!empty($productid)&&!empty($userid)) {
            $query ="SELECT
						shdid,
						userid,
						productid,
						title,
						content,
						purchase_url,
						thumbnail_filename,
						thumbnail_name AS pic_name,
						memo,
						insertt
					FROM
						`{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` AS shd
					WHERE
					 productid = '{$productid}'
					AND userid  = '{$userid}'
					AND switch = 'Y'
					ORDER BY
						shd.insertt DESC
					LIMIT 1 ";
			$recArr = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'] = array();
		}

		return $recArr['table']['record'][0];
	}
	
	/*
	 *	取得特定出價商品資料
	 */
	public function get_history_detail_info($productid, $shdid){

		global $db, $config;

        if(!empty($productid)) {
            $query ="SELECT shdid, userid, productid, title, content, purchase_url, thumbnail_filename, thumbnail_name as pic_name, memo, insertt
					FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` as shd, 
					(SELECT MAX(shdid) as max_shdid FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history_detail` GROUP BY title) shd2
					WHERE shd.shdid = shd2.max_shdid
					AND productid = '{$productid}'
					AND shdid = '{$shdid}'
					AND switch = 'Y'
					ORDER BY shd.insertt DESC LIMIT 1 ";
			$recArr = $db->getQueryRecord($query);
		} else {
			$recArr['table']['record'] = array();
		}
		return $recArr['table']['record'][0];
	}
	
}
?>
