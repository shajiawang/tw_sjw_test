<?php
/**
 * Deposit Controller 儲值
 */

class Deposit2 
{
    public $controller = array();
	public $params = array();
    public $id;
	
	/**
     * Default home page.
     */
	public function home_ori()
	{
		
		global $tpl, $deposit;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		
		$get_drid_list = $deposit->row_drid_list();
		$tpl->assign('get_drid_list', $get_drid_list);
		
		if (!empty($get_drid_list)) {
			foreach ($get_drid_list as $key => $value)
			{
				$row_list[$value['drid']] = $deposit->row_list($value['drid']); 
				$tpl->assign('row_list', $row_list);
				
				foreach ($row_list[$value['drid']] as $rk => $rv) { 
					$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);
					if (!empty($get_scode_promote)) {
						$i = 1;
						$scode_promote_memo[$rv['driid']] = '';
						foreach ($get_scode_promote as $sk => $sv) {
							if ($sv['spid'] == 4) {
								$scode_promote_memo[$rv['driid']] .= $i.'. S碼 X '.$sv['num'].'<br>';
								$i++;
							}
							if ($sv['spid'] == 52) {
								$scode_promote_memo[$rv['driid']] .= $i.'. 限定S碼 X '.$sv['num'].'<br>';
								$i++;
							}
						}
					}
					else {
						$scode_promote_memo[$rv['driid']] .= "1. 無赠送任何东西";
					}
				}
				$tpl->assign('scode_promote_memo', $scode_promote_memo);
				
			}
		}
		$tpl->set_title('');
		$tpl->render("deposit","home2",true);
		
	}
	
	public function confirm()
	{
		global $tpl, $config, $deposit;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		
		$drid= $_GET['drid'];
		//充值项目ID
		//if(empty($_GET['driid'])) { }
		$row_list = $deposit->row_list($_GET['drid']); 
		$tpl->assign('row_list', $row_list);
		
		$deposit_rule = $deposit->deposit_rule($_GET['drid']);
		$tpl->assign('deposit_rule', $deposit_rule);
		
		foreach($row_list as $rk => $rv) {
				
			if((int)$rv['driid']==(int)$_GET["driid"] ) {
				//取得儲值點數
				$get_deposit['drid'] = $_GET['drid'];
				$get_deposit['driid'] = $rv['driid'];
				$get_deposit['name'] = $rv['name']; 
				$get_deposit['amount'] = $rv['amount'];
				$get_deposit['spoint'] = $rv['spoint'];
				continue;
			}
			
		}//endforeach;
			
		if($drid!=7) {	
			//新增deposit_history資訊
			$depositid = $deposit->add_deposit($_SESSION['auth_id'], $get_deposit['amount']);
			$spointid = $deposit->add_spoint($_SESSION['auth_id'], $get_deposit['spoint']);
			$dhid = $deposit->add_deposit_history($_SESSION['auth_id'], $_GET["driid"], $depositid, $spointid);
			$get_scode_promote = $deposit->get_scode_promote_rt($_GET['driid']);
			
			if (!empty($get_scode_promote)) {
				$i = 1;
				$spmemo = '';
				foreach ($get_scode_promote as $sk => $sv) {
					if ($sv['spid'] == 4) {
						$spmemo .= $i.". S碼 X ".$sv['num']."\n";
						$i++;
					}
					if ($sv['spid'] == 52) {
						$spmemo .= $i.". 限定S碼 X ".$sv['num']."\n";
						$i++;
					}
				}
			}
			else {
				$spmemo .= "1. 無赠送任何东西";
			}
			
			
			if ($_GET['drid'] == 4) {
				$banklist=array();
				$banklist['BOCOM']="交通银行";
				$banklist['CMB'] = "招商银行";
				$banklist['CCB'] = "建设银行";
				$banklist['SPDB'] = "浦发银行";
				$banklist['GDB'] = "广发银行";
				$banklist['PSDB'] = "邮政储蓄银行";
				$banklist['CIB'] = "兴业银行";
				$banklist['HXB'] = "华夏银行";
				$banklist['PAB'] = "平安银行";
				$banklist['BOS'] = "上海银行";
				$banklist['SRCB'] = "上海农商银行";
				$banklist['BCCB'] = "北京银行";
				$banklist['BRCB'] = "北京农商银行";
				$banklist['CEB'] = "光大银行";
				$banklist['ICBC'] = "工商银行(建议使用PC浏览)";
				$banklist['BOCSH'] = "中国银行(建议使用PC浏览)";
				$banklist['ABC'] = "农业银行(建议使用PC浏览)";
				$banklist['MBC'] = "民生银行(建议使用PC浏览)";
				$banklist['CNCB'] = "中信银行(建议使用PC浏览)";
				$banklist['OTHERS'] = "其它" ;
				
				//$banklist['BOC'] = "中国银行(大额)(建议使用PC浏览)";
				
				$tpl->assign('bank_list',$banklist);
			}
			
			//Create ordernumber
			$get_deposit['ordernumber'] = $dhid;
	    }
		
		//md5(merchantnumber+code+amount+ordernumber)
		//$get_deposit['hash'] = md5($config['alipay']['merchantnumber'] . $config['alipay']['code'] . sprintf("%d", $get_deposit['amount']) . $dhid);
		
		$tpl->assign('get_deposit', $get_deposit);
		$tpl->assign('get_scode_promote', $get_scode_promote);
		$tpl->assign('spmemo', $spmemo);
		$tpl->set_title('');
		$tpl->render("deposit","confirm2",true);
	}
	
		
	public function alipay_html()
	{
		global $tpl, $config, $deposit;
		
		$secretid = md5($_POST['out_trade_no'].$_POST['trade_no']);
		
		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		
		//充值项目ID
		if (($_POST['result'] == "success") && ($_POST['secretid'] == $secretid))
		{
			$alipay_array['out_trade_no'] = $_POST['out_trade_no'];
			$alipay_array['trade_no'] = $_POST['trade_no'];
			$alipay_array['result'] = $_POST['result'];
			//print_r($alipay_array);exit;
			$get_deposit_history = $deposit->get_deposit_history($alipay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);
			error_log("alipay:".date("H:i:s"));
			//print_R($get_deposit_history);exit;
			if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N') {
				$deposit->set_deposit_history($alipay_array, $get_deposit_history[0], "alipay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);
				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				//print_r($get_deposit_rule_item);echo '<br>';
				//$get_scode_promote = $deposit->get_scode_promote($get_deposit_rule_item[0]['spoint']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);
				
				if (!empty($get_scode_promote)) {
					foreach ($get_scode_promote  as $sk => $sv) {
						if ($sv['productid'] == 0) {
							$socde = $deposit->add_scode($_SESSION['auth_id'], $sv);
							$deposit->add_scode_history($_SESSION['auth_id'], $socde, $sv);
						}
						if ($sv['productid'] > 0 && !empty($sv['productid'])) {
							for ($i = 0; $i < $sv['num']; $i++)
							{
								$deposit->add_oscode($_SESSION['auth_id'], $sv);
							}
						}
					}
				}
				//$deposit->set_scode($_SESSION['auth_id'], $get_scode_promote['scode_sum']);
				//$deposit->set_scode_history($_SESSION['auth_id'], $get_scode_promote['scode_sum']);
				
				error_log("交易成功。");
				echo '<script>alert("充值成功!");window.location = "/site/member?channelid=1"</script>';
			}
			else if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y') {
				error_log("重覆充值。");
				echo '<script>alert("已重覆充值!");window.location = "/site/member?channelid=1"</script>';
			}
			else {
				error_log("充值失敗。");
				echo '<script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script>';
			}
		}
		else {
			error_log("充值失敗。");
			echo '<script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script>';
		}
		exit;
		
		/*
		
		$row_list = $deposit->row_list($_GET['drid']); 
		$tpl->assign('row_list', $row_list);
		
		foreach($row_list as $rk => $rv) {
			
			if((int)$rv['driid']==(int)$_GET["driid"] ) {
				//取得儲值點數
				$get_deposit['driid'] = $rv['driid'];
				$get_deposit['name'] = $rv['name']; 
				$get_deposit['amount'] = $rv['amount'];
				$get_deposit['ordernumber'] = $_GET['ordernumber'];
				$get_deposit['hash'] = md5($config['alipay']['merchantnumber'] . $config['alipay']['code'] . sprintf("%d", $get_deposit['amount']) . $get_deposit['ordernumber']);
				continue;
			}
		
		}//endforeach;
		
		$tpl->assign('get_deposit', $get_deposit);
		
		$tpl->set_title('');
		$tpl->render("deposit","alipay_html",true);
		*/
	}
	
	public function bankcomm_active_notify() {
	       
		   global $tpl, $config, $deposit;
		   set_status($this->controller);
		   $notifyMsg = $_POST['notifyMsg'];
		   $arr = preg_split("/\|{1,}/",$notifyMsg);
			$msg="";
			if(is_array($arr)) {
			    $size=count($arr);
			    for($i=0;$i<$size-1;$i++) {
				   if($i==$size-2) {
				      $msg.=base64_decode($arr[$i]);
				   } else {
			          $msg.=$arr[$i]."|";
				   }
			    }
		    }
		   error_log("bankcomm_active_notify from bankcomm : ".$msg);
		   echo ("1");
		   exit;
	} 
	
	public function bankcomm_html()
	{
		global $tpl, $config, $deposit;
		
		set_status($this->controller);
		
		    $notifyMsg = $_POST['notifyMsg'];
	        error_log("bankcomm_html from bankcomm : ".$notifyMsg);
            $arr = preg_split("/\|{1,}/",$notifyMsg);
			
			if(is_array($arr)) {
			   $merId=$arr[0];
			   $orderid=$arr[1];
			   $amount=$arr[2];
			   $curType=$arr[3];          // ex: CNY
			   $platformBatchNo=$arr[4];  // Usually leave blank
			   $merPayBatchNo=$arr[5];    // Optional, assign by merchant
			   $orderDate=$arr[6];        // ex: 20141015
			   $orderTime=$arr[7];        // ex: 095124 
			   $bankSerialNo=$arr[8];     // ex: C7D367EC
			   $tranResultCode=$arr[9];   // ex: 1 [success]
			   $feeSum=$arr[10];             // Total fee
			   $cardType=$arr[11];
			   $bankMono=$arr[12];
			   $errMsg=$arr[13];
			   $remoteAddr=$arr[14];
			   $refererAddr=$arr[15];
			   $merComment=base64_decode($arr[16]);
			   $signMsg=$arr[17];
		    }
			error_log("merComment is : ".$merComment);
			
			if($tranResultCode=='1') {
			    $bankcomm_array['out_trade_no'] = $orderid;
			    $bankcomm_array['trade_no'] =$bankSerialNo;
			    $bankcomm_array['amount'] = $amount;
			    $bankcomm_array['result'] = $tranResultCode;
			    $bankcomm_array['orderDate'] = $orderDate;
			    $bankcomm_array['orderTime'] = $orderTime;
			    $bankcomm_array['orderMono'] = $merComment;
			    			   
			    $get_deposit_history = $deposit->get_deposit_history($bankcomm_array['out_trade_no']);
			    $get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);
			    
			    if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N') {
			        $deposit->set_deposit_history($bankcomm_array, $get_deposit_history[0], "bankcomm");
				    $deposit->set_deposit($get_deposit_history[0]['depositid']);
				    $deposit->set_spoint($get_deposit_history[0]['spointid']);
				    $get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				    $get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);
				    
					if (!empty($get_scode_promote)) {
						foreach ($get_scode_promote  as $sk => $sv) {
							if ($sv['productid'] == 0) {
								$socde = $deposit->add_scode($_SESSION['auth_id'], $sv);
								$deposit->add_scode_history($_SESSION['auth_id'], $socde, $sv);
							}
							if ($sv['productid'] > 0 && !empty($sv['productid'])) {
								for ($i = 0; $i < $sv['num']; $i++)
								{
									$deposit->add_oscode($_SESSION['auth_id'], $sv);
								}
							}
						}
					}
					
				   /*
				   if ($get_deposit_rule_item[0]['spoint'] >= 50) {
					$get_scode_promote = $deposit->get_scode_promote($get_deposit_rule_item[0]['spoint'], "bankcomm");
					$socde = $deposit->add_scode($_SESSION['auth_id'], $get_scode_promote);
					$deposit->add_scode_history($_SESSION['auth_id'], $socde, $get_scode_promote);
				   }
				   */
			   }
			   error_log("bankcomm 充值成功。");
				echo '<script>alert("充值成功!");window.location = "/site/member?channelid=1"</script>';  
			} else {
			   error_log("bankcomm 充值失敗。");
				echo '<script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script>';
			}
        exit;
	}
	
	public function weixinpay_html()
	{
		global $tpl, $config, $deposit;
		
		$secretid = md5($_POST['orderid'].$_POST['amount']);
		error_log("orderid : ".$_POST['orderid']."/amount : ".$_POST['amount']."/secretid : ".$_POST['secretid']);
		//設定 Action 相關參數
		set_status($this->controller);
		//login_required();
		
		//充值项目ID
		if ($_POST['orderid'] && ($_POST['secretid'] == $secretid))
		{//error_log("secretid : ".$_POST['secretid']);
			//error_log("amount : ".$_POST['amount']);
			$weixinpay_array['out_trade_no'] = $_POST['orderid'];
			$weixinpay_array['amount'] = $_POST['amount'];
			//print_r($alipay_array);exit;
			$get_deposit_history = $deposit->get_deposit_history($weixinpay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);
			
			//print_R($get_deposit_history);exit;
			if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N') {
				$deposit->set_deposit_history($weixinpay_array, $get_deposit_history[0], "weixinpay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);
				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				//print_r($get_deposit_rule_item);echo '<br>';
				//$get_scode_promote = $deposit->get_scode_promote($get_deposit_rule_item[0]['spoint']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);
				
				if (!empty($get_scode_promote)) {
					foreach ($get_scode_promote  as $sk => $sv) {
						if ($sv['productid'] == 0) {
							$socde = $deposit->add_scode($_SESSION['auth_id'], $sv);
							$deposit->add_scode_history($_SESSION['auth_id'], $socde, $sv);
						}
						if ($sv['productid'] > 0 && !empty($sv['productid'])) {
							
							for ($i = 0; $i < $sv['num']; $i++)
							{
								$deposit->add_oscode($_SESSION['auth_id'], $sv);
							}
						}
					}
				}
				//$deposit->set_scode($_SESSION['auth_id'], $get_scode_promote['scode_sum']);
				//$deposit->set_scode_history($_SESSION['auth_id'], $get_scode_promote['scode_sum']);
				
				//回傳: 
				$ret['status'] = 200;
				error_log("微信支付交易成功。");
				//echo '<script>alert("充值成功!");window.location = "/site/member?channelid=1"</script>';
			}
			else if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y') {
				$ret['status'] = 2;
				error_log("微信支付重覆充值。");
				//echo '<script>alert("已重覆充值!");window.location = "/site/member?channelid=1"</script>';
			}
			else {
				$ret['status'] = 1;
				error_log("微信支付充值失敗1。");
				//echo '<script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script>';
			}
		}
		else {
			$ret['status'] = 1;
			error_log("微信支付充值失敗2。");
			//echo '<script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script>';
		}
		echo $ret['status'];
		exit;
		
	}
	
	public function alipay_complete()
	{
		global $tpl, $config, $deposit;
		/*
		//print_r($_POST['amount']);
		$row_list = $deposit->get_deposit($_GET['depositid']); 
		
		foreach($row_list as $rk => $rv) {
			
			if((int)$rv['depositid']==(int)$_GET["depositid"] ) {
				//取得儲值點數
				$get_deposit['depositid'] = $rv['depositid'];
				$get_deposit['paytitle'] = $rv['paytitle']; 
				$get_deposit['amount'] = $rv['amount'];
				continue;
			}
		
		}//endforeach;
		
		$tpl->assign('get_deposit', $get_deposit);

		//設定 Action 相關參數
		set_status($this->controller);
		//echo '<script>alert("充值完成!");window.location = "/site/member?channelid=1"</script>';
		*/
		$tpl->set_title('');
		$tpl->render("deposit","alipay_complete",true);
	}
	//test
	public function home()
	{
		global $tpl, $deposit;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		
		$get_drid_list = $deposit->row_drid_list_test();
		$tpl->assign('get_drid_list', $get_drid_list);
		
		if (!empty($get_drid_list)) {
			foreach ($get_drid_list as $key => $value)
			{
				$row_list[$value['drid']] = $deposit->row_list_test($value['drid']); 
				if(!$row_list[$value['drid']])
				   continue;
				$tpl->assign('row_list', $row_list);
				foreach ($row_list[$value['drid']] as $rk => $rv) { 
					$get_scode_promote = $deposit->get_scode_promote_rt_test($rv['driid']);
					if (!empty($get_scode_promote)) {
						$i = 1;
						$scode_promote_memo[$rv['driid']] = '';
						foreach ($get_scode_promote as $sk => $sv) {
							if ($sv['spid'] == 4) {
								$scode_promote_memo[$rv['driid']] .= $i.'. S碼 X '.$sv['num'].'<br>';
								$i++;
							}
							if ($sv['spid'] == 52) {
								$scode_promote_memo[$rv['driid']] .= $i.'. 限定S碼 X '.$sv['num'].'<br>';
								$i++;
							}
						}
					}
					else {
						$scode_promote_memo[$rv['driid']] .= "1. 無赠送任何东西";
					}
				}
				$tpl->assign('scode_promote_memo', $scode_promote_memo);
				
			}
		}
		
		$tpl->set_title('');
		$tpl->render("deposit","home2",true);
	}
	
	public function confirm2() {
		global $tpl, $config, $deposit;
		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		
		$drid=$_GET['drid'];
		
		//充值项目ID
		//if(empty($_GET['driid'])) { }
		$row_list = $deposit->row_list_test($_GET['drid']); 
		$tpl->assign('row_list', $row_list);
		
		$deposit_rule = $deposit->deposit_rule_test($_GET['drid']);
		$tpl->assign('deposit_rule', $deposit_rule);
		
		foreach($row_list as $rk => $rv) {
			
			if((int)$rv['driid']==(int)$_GET["driid"] ) {
				//取得儲值點數
				$get_deposit['drid'] = $_GET['drid'];
				$get_deposit['driid'] = $rv['driid'];
				$get_deposit['name'] = $rv['name']; 
				$get_deposit['amount'] = $rv['amount'];
				$get_deposit['spoint'] = $rv['spoint'];
				continue;
			}
		
		}//endforeach;
				
		if($drid==7) {
		    
			$get_deposit['drid']=$drid;
			
		} else if($drid!=7) {		
				//新增deposit_history資訊
				$depositid = $deposit->add_deposit($_SESSION['auth_id'], $get_deposit['amount']);
				$spointid = $deposit->add_spoint($_SESSION['auth_id'], $get_deposit['spoint']);
				$dhid = $deposit->add_deposit_history($_SESSION['auth_id'], $_GET["driid"], $depositid, $spointid);
				$get_scode_promote = $deposit->get_scode_promote_rt_test($_GET['driid']);
				
				if (!empty($get_scode_promote)) {
					$i = 1;
					$spmemo = '';
					foreach ($get_scode_promote as $sk => $sv) {
						if ($sv['spid'] == 4) {
							$spmemo .= $i.". S碼 X ".$sv['num']."\n";
							$i++;
						}
						if ($sv['spid'] == 52) {
							$spmemo .= $i.". 限定S碼 X ".$sv['num']."\n";
							$i++;
						}
					}
				}
				else {
					$spmemo .= "1. 無赠送任何东西";
				}
		
		
			if ($_GET['drid'] == 4) {
				$banklist=array();
				$banklist['BOCOM']="交通银行";
				$banklist['CMB'] = "招商银行";
				$banklist['CCB'] = "建设银行";
				$banklist['SPDB'] = "浦发银行";
				$banklist['GDB'] = "广发银行";
				$banklist['PSDB'] = "邮政储蓄银行";
				$banklist['CIB'] = "兴业银行";
				$banklist['HXB'] = "华夏银行";
				$banklist['PAB'] = "平安银行";
				$banklist['BOS'] = "上海银行";
				$banklist['SRCB'] = "上海农商银行";
				$banklist['BCCB'] = "北京银行";
				$banklist['BRCB'] = "北京农商银行";
				$banklist['CEB'] = "光大银行";
				$banklist['ICBC'] = "工商银行(建议使用PC浏览)";
				$banklist['BOCSH'] = "中国银行(建议使用PC浏览)";
				$banklist['ABC'] = "农业银行(建议使用PC浏览)";
				$banklist['MBC'] = "民生银行(建议使用PC浏览)";
				$banklist['CNCB'] = "中信银行(建议使用PC浏览)";
				$banklist['OTHERS'] = "其它" ;
				
				//$banklist['BOC'] = "中国银行(大额)(建议使用PC浏览)";
				
				$tpl->assign('bank_list',$banklist);
			}
			
			//Create ordernumber
			$get_deposit['ordernumber'] = $dhid;
		} 
		
		if($drid==6 || $drid==8) {  // ibon 
		   // currency is NTD ...
		   $arr_wherecond=array();
		   $arr_wherecond['depositid']=$depositid;
		   $arr_update=array();
		   $arr_update['currency']='NTD';
		   $deposit->update_deposit($arr_wherecond,$arr_update);
		}
		//md5(merchantnumber+code+amount+ordernumber)
		//$get_deposit['hash'] = md5($config['alipay']['merchantnumber'] . $config['alipay']['code'] . sprintf("%d", $get_deposit['amount']) . $dhid);
		
		$tpl->assign('get_deposit', $get_deposit);
		$tpl->assign('get_scode_promote', $get_scode_promote);
		$tpl->assign('spmemo', $spmemo);
		$tpl->set_title('');
		$tpl->render("deposit","confirm2",true); 
	}
	
	public function ibonpay() {
	       global $tpl, $config, $deposit;
		   set_status($this->controller);
		   login_required();
	 
	       $user_info=$_SESSION['user'];   
	       
           if(empty($payname)) {
              $payname=$user_info['profile']['nickname'];
           }			             			
			 
	       $success = 0;
		   $pay_info = Array(
			   "pay_type" => $_POST['pay_type'],	
			   "invoices" => $_POST['invoices'],				//是否捐贈發票
			   "amount" => $_POST['amount'],
			   "ordernumber" => $_POST['ordernumber'],
			   "merchantnumber" => $config['ibonpay']['merchantnumber'],
			   "paymenttype" => $config['ibonpay']['paymenttype'],
			   "transurl" => $config['ibonpay']['url_payment'],
			   "code" => $config['ibonpay']['tranCode'],
			   "nexturl" => $config['ibonpay']['nexturl'],
			   "payphone" => $user_info['profile']['phone'],
		       "payname" => $user_info['profile']['addressee'],
			   "duedate" => "",
			   "bankid" => "",
			   "paytitle" => "",
			   "paymemo" => "",
			   "returnvalue" => "1"
		   );
		   
		   $plaintext = $pay_info['merchantnumber'].
		                $pay_info['code'].
						$pay_info['amount'].
						$pay_info['ordernumber'];
		   error_log("[deposit] ibon plaintext : ".$plaintext);
		   
		   $hash = md5($plaintext);
		   error_log("Hash is :".$hash);
		   		   

		   $postdata = "merchantnumber=".$pay_info['merchantnumber'].
							"&ordernumber=".$pay_info['ordernumber'].
							"&amount=".$pay_info['amount'].
							"&paymenttype=".$pay_info['paymenttype'].
							"&bankid=".$pay_info['bankid'].
							"&paytitle=".urlencode($pay_info['paytitle']).
							"&paymemo=".urlencode($pay_info['paymemo']).
							"&payname=".urlencode($pay_info['payname']).
							"&payphone=".$pay_info['payphone'].
							"&duedate=".$pay_info['duedate'].
							"&returnvalue=".$pay_info['returnvalue'].
							"&hash=".$hash.
							"&nexturl=".urlencode($pay_info['nexturl']);
 		   
		   error_log("postdata :".$postdata);

		   $url = parse_url($pay_info['transurl']);

			$postdata = "POST ".$url['path']." HTTP/1.0\r\n".
						"Content-Type: application/x-www-form-urlencoded\r\n".
						"Host: ".$url['host']."\r\n".
						"Content-Length: ".strlen($postdata)."\r\n".
						"\r\n".
						$postdata;
						
			 
			$receivedata = "";
			$fp = fsockopen ($url['host'], $url['port'], $errno, $errstr, 90);
			if(!$fp)
			{ 
				echo "$errstr ($errno)<br>\n";
			}
			else
			{ 
				fputs ($fp, $postdata);
				do
				{ 
					if(feof($fp)) break;
					$tmpstr = fgets($fp,128);
					$receivedata = $receivedata.$tmpstr;
				}
				while(true);
				fclose ($fp);
			}
			
			$receivedata = str_replace("\r","",trim($receivedata));
			$isbody = false;
			$httpcode = null;
			$httpmessage = null;
			$result = "";
			$array1 = explode("\n",$receivedata);
			for($i=0;$i<count($array1);$i++)
			{
				if($i==0)
				{
				   $array2 = explode(" ",$array1[$i]);
				   $httpcode = $array2[1];
				   $httpmessage = $array2[2];
				}
				else if(!$isbody)
				{
				   if(strlen($array1[$i])==0) $isbody = true;
				}
				else
				{
				   $result = $result.$array1[$i];
				}
			}
			
			if($httpcode!="200")
			{
					if($httpcode=="404") 
					   echo "URL Not Found !!";
					else if($httpcode=="500") 
					   echo "Destination Server Error !!";
					else 
					   echo $httpmessage;
					return;
			}
				 
			$pay_result= Array();
			//-- 分割資料取參數
			$ary1 = explode("&",$result);
			for($i=0;$i<count($ary1);$i++)
			{
			  $ary2 = explode("=",$ary1[$i]);
			  $pay_result[$ary2[0]]=$ary2[1];
			}
            // $ary2 looks like :
			// Array ( [0] => rc [1] => 0 ) 
			// Array ( [0] => amount [1] => 2551 ) 
			// Array ( [0] => merchantnumber [1] => 456025 ) 
			// Array ( [0] => ordernumber [1] => 2228 ) 
			// Array ( [0] => paycode [1] => NW14103100190276 ) 
			// Array ( [0] => checksum [1] => 79c43472e3d3fd1c0fb509263ef7411c 
			
			// 將訂單號,paycode和checksum 等等暫時寫入saja_deposit_history
			$arr_wherecond['dhid']=$pay_result['ordernumber'];
			$arr_update['data']='{out_trade_no:"'.$pay_info['ordernumber']
			                   .'",trade_no:"'.$pay_result['paycode']
							   .'",amount:"'.$pay_info['amount'] 
							   .'",checksum:"'.$pay_result['checksum']
							   .'"}';
			
			$arr_update['data']="";
			$deposit->update_deposit_history($arr_wherecond,$arr_update);
			
		    $tpl->assign('pay_result',$pay_result);
			$tpl->assign('pay_info',$pay_info);
			$tpl->set_title('');
		    $tpl->render("deposit","ibonpay_result",true); 

	}
	
	public function ibonpay_success() {
	
	        error_log(var_dump($_POST));
			$my_Code = "gcxa3him";//藍新認證碼
			$my_Code2 = "sajo498";//殺價認證
			$my_num = "456025";//商家編號
			$send_jsp_url = "https://aquarius.neweb.com.tw:80/CashSystemFrontEnd/Payment";
			
			$ibonpay_array['tranCode']            = $config['ibonpay']['tranCode'];
			$ibonpay_array['merchantnumber']     = $_POST['merchantnumber'];
			$ibonpay_array['ordernumber']       = $_POST['ordernumber'];
			$ibonpay_array['amount']             = $_POST['amount'];
			$ibonpay_array['paymenttype']        = $_POST['paymenttype'];
			$ibonpay_array['serialnumber']       = $_POST['serialnumber'];
			$ibonpay_array['writeoffnumber']     = $_POST['writeoffnumber'];
			$ibonpay_array['timepaid']           = $_POST['timepaid'];
			$ibonpay_array['tel']                = $_POST['tel'];
			
			// 送交確認
			
			$get_deposit_history = $deposit->get_deposit_history($ibonpay_array['ordernumber']);
			$orderid=$get_deposit_history[0]['dhid'];
						
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);
			    
			if ($orderid && $get_deposit_id[0]['switch'] == 'N') {
				$deposit->set_deposit_history($ibonpay_array, $get_deposit_history[0], "ibonpay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);
				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);
				
				if (!empty($get_scode_promote)) {
				    $userid=$get_deposit_history[0]['userid'];
					foreach ($get_scode_promote  as $sk => $sv) {
						if ($sv['productid'] == 0) {
							$socde = $deposit->add_scode($userid, $sv);
							$deposit->add_scode_history($userid, $socde, $sv);
						}
						if ($sv['productid'] > 0 && !empty($sv['productid'])) {
							for ($i = 0; $i < $sv['num']; $i++)
							{
								$deposit->add_oscode($userid, $sv);
							}
						}
					}
					error_log("Userid:".$userid." Order ID:".$ibonpay_array['ordernumber']." amount:".$ibonpay_array['amount']." spoints:".$get_deposit_history[0]['spointid']." deposited OK !!"); 
				}
			}
			
			echo "1";
			exit;
	}
	
	public function hinetpts_exc() {
		
		   global $tpl, $config, $deposit;
		   set_status($this->controller);
		   login_required();
	 
	       $serial_no=addslashes($_POST['serial_no']);
		   if(empty($serial_no)) {
		      echo '<script>alert("序号不可為空白, 請重新輸入 !!");
			          window.location = "/site/deposit2/confirm2/?drid=7";
			      </script>';			  
			  exit;
		   }
		   if($serial_no!=$_POST['serial_no']) {
		      echo '<script>alert("序号格式錯誤, 請重新輸入 !!");
			          window.location = "/site/deposit2/confirm2/?drid=7";
			      </script>';			  
			  exit;
		   }  		   
		   
		   
		   $passwd=addslashes($_POST['passw']);
		   if(empty($passwd)) {
		      echo '<script>alert("密碼不可為空白, 請重新輸入 !!");
			         window.location = "/site/deposit2/confirm2/?drid=7";    
			        </script>';			  
			  exit;
		   }
		   if($passwd!=$_POST['passw']) {
		      echo '<script>alert("密碼格式錯誤, 請重新輸入 !!");
			          window.location = "/site/deposit2/confirm2/?drid=7";
			      </script>';			  
			  exit;
		   }
		   		   
		   $userid=$_SESSION['auth_id'];
		   $ch_serial=$deposit->get_ch_serial($serial_no);
		   
		   if(!$ch_serial) {
		       echo '<script>alert("序号錯誤, 請重新輸入 !!");
			           window.location = "/site/deposit2/confirm2/?drid=7";
			         </script>';
			   exit;
		   }
		   if($passwd!=$ch_serial['passw']) {
		      $arr_update=array();
		      $arr_update['userid']=$userid;
			  $arr_update['err_num']=$ch_serial['err_num']+1;
			  if($arr_update['err_num']<3) {
			     echo '<script>alert("密碼錯誤 : '.$arr_update['err_num'].' 次, 請重新輸入 !!");
				         window.location = "/site/deposit2/confirm2/?drid=7";
				       </script>'; 
			  } else {
			     $arr_update['switch']='L';
			     echo '<script>alert("密碼輸入錯誤 3 次,序号已鎖定暫停兌換,請洽客服人員 !!");
				         window.location = "/site/deposit2/confirm2/?drid=7";
				       </script>';
			  }
			  $deposit->update_ch_serial($serial_no, $arr_update);
			  exit;
		   } 
		   if($ch_serial['switch']=='L') {
		       echo '<script>alert("本序号已暫时停用,請洽客服人員 !!");
			         window.location = "/site/deposit2/confirm2/?drid=7";
			        </script>';
		       exit;
		   }
		   if($ch_serial['switch']=='Y') {
		       echo '<script>alert("本序号已使用過, 無法再兌換殺幣 !!");
			         window.location = "/site/deposit2/confirm2/?drid=7";
			        </script>';
		       exit;
		   }
		   if($passwd==$ch_serial['passw']) {
		      $arr_update=array();
			  $drid=$_POST['drid'];
			  $spoint=$ch_serial['point'];
			  
			  error_log("[hinetpst_exc] drid=${drid}, spoint=${spoint} ...");
			  			  
		      $deposit_rule = $deposit->deposit_rule_test($drid);
			  if($deposit_rule) {
		         $deposit_rule_item=$deposit->get_deposit_rule_item_by_drid_spoint($drid,$spoint);
				 if($deposit_rule_item) {
					$driid=$deposit_rule_item['driid'];
					
					error_log("[hinetpst_exc] driid=${driid} ...");
					
					// 產生訂單及儲值資料
					$depositid = $deposit->add_deposit($userid, $deposit_rule_item['amount']);
					$spointid = $deposit->add_spoint($userid,  $deposit_rule_item['spoint']);
					$dhid = $deposit->add_deposit_history($userid, $driid, $depositid, $spointid);
					
					error_log("[hinetpst_exc] dhid=${dhid}, spointid=${spointid}, depositid=${depositid} ...");
					
					// 修改訂單及儲值狀態
					$get_deposit_history = $deposit->get_deposit_history($dhid);
			        $get_deposit_id = $deposit->get_deposit_id($depositid);
					
					error_log("[hinetpts_exc] saja_deposit switch : ".$get_deposit_id[0]['switch']);
					
					if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N') {
					    
						$hinet_array['out_trade_no'] = $get_deposit_history[0]['dhid'];
			            $hinet_array['spoints'] = $spoint;
						$hinet_array['userid']=$userid;
						$hinet_array['serialno']=$serial_no;
						
						$deposit->set_deposit_history($hinet_array, $get_deposit_history[0], "hinet");
						
						$deposit->set_deposit($get_deposit_history[0]['depositid']);
						
						$deposit->set_spoint($get_deposit_history[0]['spointid']);
						
						$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
						$get_scode_promote = $deposit->get_scode_promote_rt_test($get_deposit_rule_item[0]['driid']);
						if (!empty($get_scode_promote)) {
							foreach ($get_scode_promote  as $sk => $sv) {
								if ($sv['productid'] == 0) {
									$socde = $deposit->add_scode($userid, $sv);
									$deposit->add_scode_history($userid, $socde, $sv);
								}
								if ($sv['productid'] > 0 && !empty($sv['productid'])) {
									for ($i = 0; $i < $sv['num']; $i++)
									{
										$deposit->add_oscode($userid, $sv);
									}
								}
							}
						}
						$arr_update=array();
						$arr_update['userid']=$userid;
						$arr_update['switch']='Y';
						$arr_update['dhid']=$dhid;
						$arr_update['modifyerid']=$userid;
						$deposit->update_ch_serial($serial_no,$arr_update);
						echo '<script>alert("兌換成功, 新增殺幣 '.$spoint.' 點 !!");window.location = "/site/member?channelid=1"</script>';
					}
				 }
			  }
			  
		   }
		   exit;
		   
	}
	// add end
}