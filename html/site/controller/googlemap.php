<?php
/*
 * GoogleMap Controller
 */

class GoogleMap {

  public $controller = array();
	public $params = array();
  public $id;
	public $userid = '';

	public function __construct() {
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
	}

	/*
  * Default home page.
  */
	public function home() {

		global $tpl, $googlemap;

		//設定 Action 相關參數
		set_status($this->controller);

		//資料
		$gmap = urldecode($_GET['k']);
		$tpl->assign('gmap', $gmap);

		mb_regex_encoding('UTF-8');
		mb_internal_encoding('UTF-8');
		$location_array = mb_split("到", mb_substr($_GET['k'], 1));
		$maplocation['from'] = $location_array[0];
		if(count($location_array) == 1){
			$mapstate = "loc";
		}else if(count($location_array) > 1){
			$mapstate = "nav";
			$maplocation['to'] = end($location_array);
			array_shift($location_array);		//去頭
			array_pop($location_array);			//去尾
			$maplocation['waypoint'] = implode(",", $location_array);
			if(!empty($maplocation['waypoint'])){
				$maplocation['waypoint'] = "waypoint: ['".$maplocation['waypoint']."'], ";
			}
		}else if(count($location_array) < 1){
			$mapstate = "";
			return_to('site/');
		}
		$tpl->assign('mapstate', $mapstate);
		$tpl->assign('maplocation', $maplocation);

		$tpl->set_title('');
		$tpl->render("googlemap","home",true);

	}

}
