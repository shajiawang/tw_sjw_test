<?php
/*
 *	Issues Controller 客服相關
 */
class Issues {

	public $controller = array();
	public $params = array();
	public $id;

	public function __construct() {

		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
		$this->datetime = date('Y-m-d H:i:s');

	}

	/*
	 *	頁面導項預設
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 */
	public function home() {

		global $tpl, $issues;

		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$status = empty($_POST['status']) ? htmlspecialchars($_GET['status']) : htmlspecialchars($_POST['status']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得客戶留言資料
		$get_list = $issues->get_Issues_List($userid, '');
		
		if(!empty($get_list['record'] ) ) {
			foreach($get_list['record'] as $rk => $rv) {
				if(!empty($rv['ispid']) ){
					$get_list['record'][$rk]['ispid'] = BASE_URL.ISSUESIMGS_DIR ."/{$rv['ispid']}";
				}
				
				if(!empty($rv['ispid2']) ){
					$get_list['record'][$rk]['ispid2'] = BASE_URL.ISSUESIMGS_DIR ."/{$rv['ispid2']}";
				}
				
				if(!empty($rv['ispid3']) ){
					$get_list['record'][$rk]['ispid3'] = BASE_URL.ISSUESIMGS_DIR ."/{$rv['ispid3']}";
				}
			}
		}

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('');
			$tpl->render("issues","home",true);
		}else{
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "MSG";
			$data['retObj']['data']= (!empty($get_list)) ? $get_list['record'] : new stdClass;
			$data['retObj']['page'] = (!empty($get_list['page'])) ? $get_list['page'] : new stdClass;
			echo json_encode($data);
		}

	}

	/*
	 *	客服留言回應清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$uiid	 	int			留言編號
	 *	$userid		int			會員編號
	 */
	public function issues_detail() {

		global $tpl, $issues, $member;

		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$uiid 		= empty($_POST['uiid']) ? htmlspecialchars($_GET['uiid']) : htmlspecialchars($_POST['uiid']);
		$userid 	= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$pt 		= empty($_POST['pt']) ? htmlspecialchars($_GET['pt']) : htmlspecialchars($_POST['pt']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//判斷資料是存在
		if(!empty($uiid)){
			$get_Issues = $issues->get_Issues_list($userid, $uiid);
			
			$get_Issues['record'][0]['thumbnail'] = IMG_URL.HEADIMGS_DIR ."/_DefaultHeadImg.jpg";
			$get_member = $member->get_info($userid);
			if($get_member){
				if(!empty($get_member['thumbnail_url'])){
					$get_Issues['record'][0]['thumbnail'] = $get_member['thumbnail_url'];
				}elseif(!empty($get_member['thumbnail_file'])){
					$get_Issues['record'][0]['thumbnail'] = BASE_URL.HEADIMGS_DIR ."/". $get_member['thumbnail_file'];
				}else{
					$get_Issues['record'][0]['thumbnail'] = IMG_URL.HEADIMGS_DIR ."/_DefaultHeadImg.jpg";
				}
			}
			
			//設定已讀
			if($get_Issues['record'][0]['status']=='2'){
				$data['status']=1;
				$get_Issues['record'][0]['status']=$data['status'];
				
				//更新讀取時間
				$issues->update_Issues($get_Issues['record'][0]['uiid'], $data);
				$issues->update_Issues_Reply($get_Issues['record'][0]['uiid'], $userid);
			}
			
			if($get_Issues['record'][0]['status']=='3'){
				$get_Issues['record'][0]["stat_text"] = '已結案';
			}
			
			if(!empty($get_Issues['record'][0]['ispid']) ){
				$get_Issues['record'][0]["ispid"] = BASE_URL.ISSUESIMGS_DIR .'/'. $get_Issues['record'][0]['ispid'];
			}
			
			if(!empty($get_Issues['record'][0]['ispid2']) ){
				$get_Issues['record'][0]["ispid2"] = BASE_URL.ISSUESIMGS_DIR .'/'. $get_Issues['record'][0]['ispid2'];
			}
			
			if(!empty($get_Issues['record'][0]['ispid3']) ){
				$get_Issues['record'][0]["ispid3"] = BASE_URL.ISSUESIMGS_DIR .'/'. $get_Issues['record'][0]['ispid3'];
			}
			
			//會員提問主檔
			$Issues = $get_Issues['record'][0];

			$get_Issues_reply = $issues->get_Issues_Detail($uiid, $userid, $pt);
			
			
			//留言回應清單
			$Issues_reply = $get_Issues_reply;
			
			if(!empty($get_Issues_reply ) ) {
				foreach($get_Issues_reply['record'] as $rk => $rv) {
					
					$Issues_reply['record'][$rk]['pt'] = $rv['uirid'];
					
					if(!empty($rv['rpfid']) ){
						$Issues_reply['record'][$rk]['rpfid'] = BASE_URL.ISSUESIMGS_DIR ."/{$rv['rpfid']}";
					}
					
					if($rv['adminid'] !== '0'){
						$Issues_reply['record'][$rk]['user_img'] = BASE_URL.IMG_DIR .'/bid-checkout-img2.png';
						$Issues_reply['record'][$rk]['user_name'] = '殺價王小編';
						$Issues_reply['record'][$rk]['bg_color'] = "#ffffff";
					}else{
						$Issues_reply['record'][$rk]['user_img'] = $Issues['thumbnail'];
						$Issues_reply['record'][$rk]['user_name'] = $Issues['username'];
						$Issues_reply['record'][$rk]['bg_color'] = "#ffe4a2";
					}
				}
				
				$reverse_issues_reply = array_reverse($Issues_reply['record']);
			}

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('Issues', $Issues);
				$tpl->assign('Issues_reply', $reverse_issues_reply);
				$tpl->set_title('');
				$tpl->render("issues","issues_detail",true);
			}else{
				$data['retCode'] = 1;
				$data['retMsg']  = "OK";
				$data['retType'] = "MSG";
				$data['retObj'] = $Issues;
				$data['retObj']['reply'] = (!empty($reverse_issues_reply)) ? $reverse_issues_reply : array();
				//error_log(json_encode($data));
				echo json_encode($data);
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('errmsg', '留言編號錯誤!!');
				$this->home();
			}else{
				$data['retCode'] = -1;
				$data['retMsg'] = "留言編號錯誤!!";
				$data['retType'] = "MSG";
				$data['retObj']= "";
				echo json_encode($data);
			}

		}

	}

	/*
	 *	新增客服留言
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$title	 	varchar		留言標題
	 *	$content	varchar		留言內容
	 *	$uicid		int			分類項目
 	 *	$kind		varchar		操作型態 (insertt:資料寫入)
	 */
	public function issues_add() {

		global $tpl, $issues;

		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$title 		= empty($_POST['title']) ? htmlspecialchars($_GET['title']) : htmlspecialchars($_POST['title']);
		$content 	= empty($_POST['content']) ? htmlspecialchars($_GET['content']) : htmlspecialchars($_POST['content']);
		$uicid 		= empty($_POST['uicid']) ? htmlspecialchars($_GET['uicid']) : htmlspecialchars($_POST['uicid']);
		$kind 		= empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$thumbnail1 = empty($_POST['thumbnail1']) ? htmlspecialchars($_GET['thumbnail1']) : htmlspecialchars($_POST['thumbnail1']);
		$thumbnail2	= empty($_POST['thumbnail2']) ? htmlspecialchars($_GET['thumbnail2']) : htmlspecialchars($_POST['thumbnail2']);
		$thumbnail3	= empty($_POST['thumbnail3']) ? htmlspecialchars($_GET['thumbnail3']) : htmlspecialchars($_POST['thumbnail3']);
		$info		= empty($_POST['info']) ? htmlspecialchars($_GET['info']) : htmlspecialchars($_POST['info']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		if(!empty($kind) && $kind == "insertt"){

			//判斷資料是存在
			if((!empty($content)) && (!empty($uicid)) && (!empty($userid)) ) {

				//問答資料
				$insertIssuesdata = array(
					'userid'		=> $userid,										//使用者編號
					'title'			=> $content,									//留言標題
					'content' 		=> $content, 									//留言內容
					'uicid'			=> $uicid,										//分類項目
					'status'      	=> 1,											//留言狀態
					'info'			=> $info,										//描述訂單編號或商品名稱
				);

				//新增資料
				$uiid = $issues->insert_Issues($insertIssuesdata);
				
				$pic = true;
				$udata = array();
				
				//上傳圖片
				if($json == 'Y'){
					if (!empty($thumbnail1) && !empty($uiid) ) {
						$pic = $this->issues_pic('ispid', $uiid, $thumbnail1);
					}
					
					if (!empty($thumbnail2) && !empty($uiid) ) {
						$pic = $this->issues_pic('ispid2', $uiid, $thumbnail2);
					}
					
					if (!empty($thumbnail3) && !empty($uiid) ) {
						$pic = $this->issues_pic('ispid3', $uiid, $thumbnail3);
					}
				}else{
					if (!empty($thumbnail1) ) {
						$udata['ispid'] = $thumbnail1;
					}
					
					if (!empty($thumbnail2) ) {
						$udata['ispid2'] = $thumbnail2;
					}
					
					if (!empty($thumbnail3) ) {
						$udata['ispid3'] = $thumbnail3;
					}
					
					if(!empty($uiid) && !empty($udata) ){
						$issues->update_Issues($uiid, $udata);
					}
				}
				
				if($pic == false){
					$data['retCode'] = -1;
					$data['retMsg'] = "圖片上傳失敗!";
					$data['retType'] = "MSG";
				} else {
					$data['retCode'] = 1;
					$data['retMsg']  = "問題已送出!!";
					$data['retType'] = "MSG";
				}

			}else{

				$data['retCode'] = -1;
				$data['retMsg'] = "資料有缺請檢查!!";
				$data['retType'] = "MSG";
			}
			
			echo json_encode($data);exit;

		}else{

			//取得客服分類資料
			$get_category_list = $issues->get_Issues_category();

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('uicid', $uicid);
				$tpl->assign('category_list', $get_category_list);
				$tpl->set_title('');
				$tpl->render("issues","issues_add",true);
			}else{
				$data['retCode'] = 1;
				$data['retMsg']  = "OK";
				$data['retType'] = "MSG";
				$data['retObj']['data'] = (!empty($get_category_list['record'])) ? $get_category_list['record'] : new stdClass;
				echo json_encode($data);
			}

		}
	}

	/*
	 *	新增客服留言回應
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$uiid	 	int			留言編號
	 *	$content	text		回應內容
	 *	$userid		int			分類項目
	 */
	public function issues_reply_add() {

		global $tpl, $issues;

		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$uiid 		= empty($_POST['uiid']) ? htmlspecialchars($_GET['uiid']) : htmlspecialchars($_POST['uiid']);
		$content 	= empty($_POST['content']) ? htmlspecialchars($_GET['content']) : htmlspecialchars($_POST['content']);
		$rpfid_img 	= empty($_POST['rpfid']) ? htmlspecialchars($_GET['rpfid']) : htmlspecialchars($_POST['rpfid']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		if((!empty($uiid)) && (!empty($userid))){

			$content_str = (!empty($content)) ? $content : '...';
			
			//留言回應資料
			$insertreplydata = array(
                        	'userid'	=> $userid,						//使用者編號
                        	'uiid'		=> $uiid,						//留言標題
                        	'content' 	=> $content_str, 				//留言內容
			);
			//新增
			$ins_uirid = $issues->insert_Issues_Reply($insertreplydata);

			//更新留言資料狀態
			$data['status']=1;
			$issues->update_Issues($uiid, $data);
			
			if (!empty($rpfid_img) && !empty($ins_uirid) ) {
				$filename = md5(date("YmdHis") ."issues") ."_". $ins_uirid .".png";
				
				//上傳圖片
				$thumbnail = $this->issues_upload_pic($filename, $rpfid_img);
				
				if($thumbnail == false){
					$data['retCode'] = -1;
					$data['retMsg'] = "圖片上傳失敗!";
					$data['retType'] = "MSG";
					echo json_encode($data);
				}else{
					$issues->update_reply_rpfid($ins_uirid, $filename);
				}
			}

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('msg', '留言完成!!');
				$this->home();
			}else{
				$data['retCode'] = 1;
				$data['retMsg']  = "OK";
				$data['retType'] = "MSG";
				echo json_encode($data);
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('msg', '留言編號錯誤!!');
				$tpl->set_title('');
				$tpl->render("issues","reply_add",true);
			}else{
				$data['retCode'] = -1;
				$data['retMsg'] = "留言編號錯誤!!";
				$data['retType'] = "MSG";
				echo json_encode($data);
			}
		}
	}
	
	/*
	*上傳圖片
	*/
	public function issues_pic($file_name, $uiid, $img){
		global $tpl, $issues;
		
		$file = md5(date("YmdHis") . $file_name) ."_". $uiid .".png";

		$pic = $this->issues_upload_pic($file, $img);
		
		if($pic==true){
			$udata[$file_name] = $file;
			$issues->update_Issues($uiid, $udata);
			
			return true;
		}else{
			return false;
		}
	}
	public function issues_upload_pic($filename, $img){
		//尋找檔案內是否含png檔頭
		$img_format_check = strpos($img,'data:image/png;base64,');

		//去除png檔頭取得純圖檔資料 需注意 data url 格式 與來源是否相符 ex:image/png
		$img = str_replace('data:image/png;base64,','', $img);
		//過濾圖檔資料裡的換行符號
		$img = preg_replace("/\s+/", '', $img);

		$img_data = base64_decode($img);//解base64碼
		
		$file = '/var/www/html/site/images/site/issues/'. $filename;
		
		//檔名 包含資料夾路徑 請記得此資料夾需 777 權限 方可寫入圖檔
		$res = file_put_contents($file, $img_data);
		$res2 = syncToS3($file, 's3://img.saja.com.tw','/site/images/site/issues/');

		if($res2){
			return true;
		}else{
			return false;;
		}
	}

	/*
	 *	客服留言分類清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 */
	public function issues_category_list() {

		global $tpl, $issues;

		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得客戶留言資料
		$get_category_list = $issues->get_Issues_category();

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('category_list', $get_category_list);
			$tpl->set_title('');
			$tpl->render("issues","add",true);
		}else{
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "MSG";
			$data['retObj']['data'] = (!empty($get_category_list['record'])) ? $get_category_list['record'] : new stdClass;
			$data['retObj']['page'] = (!empty($get_category_list['page'])) ? $get_category_list['page'] : new stdClass;
			echo json_encode($data);
		}

	}

}
?>
