<?php
/**
 * Contact Controller 聯絡我們
 */

class Contact {

  public $controller = array();
	public $params = array();
  public $id;
	public $userid = '';

	public function __construct() {
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
	}


	public function home() {

    global $tpl, $member, $user, $history, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("contact","home",true);

	}

}
