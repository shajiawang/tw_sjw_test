<?php
/*
 * Mall Controller 兌換商品
 */

include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR ."/wechat.class.php");
include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");
include_once(LIB_DIR."/helpers.php");

class Mall {

  public $controller = array();
	public $params = array();
  public $id;
	public $str;


	/*
   * Default home page.
   */
	public function home() {

    global $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$channelid = empty($_POST['channelid']) ? "" : htmlspecialchars($_POST['channelid']);

		if($json=='Y'){

			//商品分類
			$_category = $mall->product_category($channelid);

			$option = '';
			if($_category){
				foreach($_category as $pck1 => $pcv1){
					if($pcv1['layer'] == 1){
						$a_href = APP_DIR .'/mall/?'. $status['status']['args'] .'&'. $cdnTime .'&epcid='. $pcv1['epcid'];
						$option .= '<option value="'. $a_href .'">'. $pcv1['name'] .'</option>';
					}
				}
				$tpl->assign('cat_options', $option);
			}

		}else{
			//$_category = $mall->product_category_count(1);
			$_category = $this->web_category_list();
			$tpl->assign('category', $_category);
		}

		$use_type = 1;
		if($json=='Y'){
			$use_type = 2;
		}

		//商品列表
		$product_list = $mall->product_list($use_type);
		$tpl->assign('product_list', $product_list);

		if($json=='Y'){
			for($idx=0; $idx< count($product_list['table']['record']); ++$idx){
				$p = $product_list['table']['record'][$idx];
				$product_list['table']['record'][$idx]['offtime'] = (int)$p['offtime'];
			}

			foreach($_category as $tk => $tv){
				$cpl = $mall->product_list($use_type, $tv['epcid']);
				$_category[$tk]['product_list'] = $cpl['table']['record'];
			}

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['category_list']=$_category;
			
			echo json_encode($ret);
			exit;
		}

		if(empty($_GET['epcid'])){
			$epcid = 1;
		}else{
			$epcid = $_GET['epcid'];
		}

		$tpl->assign('page_content', $page_content);
		$tpl->assign('noback', 'Y');
		$tpl->set_title('');
		$tpl->render("mall","home",true);
	}
	
	/*
	 *	WEB版_分類列表清單
	 */
	private function web_category_list() {

		global $tpl, $mall;
		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$channelid = '';

		//商品分類
		$_category = $mall->product_category($channelid);

		//虛位以待
		$_syslist = $mall->get_sys();

		$description = (!empty($_syslist[0]['description']) ) ? $_syslist[0]['description'] : '';
		$_syslist[0]['description'] = html_decode($description);

		$use_type = 1;
		
		
		//取得2級分類
		foreach($_category as $tk => $tv){
			$_category[$tk]['product_list'] = array();
			$lvl = $mall->product_category_list(2, $tv['epcid']);
			if (!empty($lvl)){
				$_category[$tk]['level2_list'] = $lvl;
			}else{
				$_category[$tk]['level2_list'] = null;
			}
			if ($tv['epcid'] == "20") {
				$_category[$tk]['product_list'] = array( 
					array("offtime" => "1893743880",
						"epcid" => "20",
						"thumbnail" => "b6ad97f0c3efc3b06cc46f3fc0b44738.png",
						"epid" => "694",
						"name" => "ibon",
						"thumbnail_url" => "",
						"tx_url" => "https://www.saja.com.tw/site/mall/active/"
					)
				);
			}
		}
		
		return $_category;
		
		/*
		$ret = getRetJSONArray(1,'OK','LIST');
		$ret['retObj']['data']['category_list'] = $_category;
		$ret['retObj']['data']['sys_list'] = $_syslist;
		//echo json_encode($ret);
		return json_decode($ret);
		*/
	}


	/*
	 *	取得特定分類兌換商品清單
	 *	$channelid				varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$epcid				int						會員編號
	 *	$page	 			int						商品編號
	 */
	public function getExchangeProdList() {

		global  $mall,$config;

		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");

		$includeEpcids=null;
		$excludeEpcids=null;

		$channelid = empty($_POST['channelid']) ? htmlspecialchars($_GET['channelid']) : htmlspecialchars($_POST['channelid']);
		if(empty($channelid))
			$channelid=$config['channel'];

		$epcid = empty($_POST['epcid']) ? htmlspecialchars($_GET['epcid']) : htmlspecialchars($_POST['epcid']);
		
		$page = empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		if(empty($page))
			$page=0;

		$perPage = empty($_POST['perPage']) ? htmlspecialchars($_GET['perPage']) : htmlspecialchars($_POST['perPage']);
		if(empty($perPage))
			$perPage=99;

		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		if(empty($type))
			$type=0;

		// 只撈第三方服務的類型的兌換(category id=40)
		if($type==3){
			$includeEpcids=array();
			array_push($includeEpcids,$config['3rdparty_epcid']);
		}else{
			$excludeEpcids=array();
			array_push($excludeEpcids,$config['3rdparty_epcid']);
		}

		$use_type = 2;

		$product_list = $mall->getExchangeProdList($channelid,$page,$perPage,$includeEpcids,$excludeEpcids,$use_type);
		$ret=null;
		
		if($product_list){
			for($idx=0; $idx< count($product_list['table']['record']); ++$idx){
				$p = $product_list['table']['record'][$idx];
				$product_list['table']['record'][$idx]['offtime'] = (int)$p['unix_offtime'];
				if(empty($p['thumbnail_url']) && !empty($p['thumbnail'])){
				  $product_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$p['thumbnail'];
				}
			}
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']=$product_list['table']['record'];
		}else{
			$ret=getRetJSONArray(0,'No Data Found !!','MSG');
		}
		echo json_encode($ret);

		exit;

	}


	/*
	 * 設定分頁參數
	 */
	private function set_page($row_list, $page_path) {

    $table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';

		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;

  }


	// 兌換商品分類清單
	public function getExchangeCategoryList() {

		global $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		$layer=$_POST['layer'];

		if(empty($layer))
		  $layer=1;

		//商品分類
		$_category = $mall->product_category_list($_POST['layer']);
		$ret=null;

		if($_category){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']=$_category;
		}else{
			$ret=getRetJSONArray(0,'No Data Found !!','LIST');
		}

		echo json_encode($ret);

	}


	/*
	 *	兌換商品
	 *	$json				varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$userid				int						會員編號
	 *	$epid	 			int						商品編號
	 */
	public function check() {

    global $tpl, $mall, $member;

		//設定 Action 相關參數
		set_status($this->controller);

		$userid  = empty($_POST['auth_id']) ? ($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$epid 	 = empty($_POST['epid']) ? htmlspecialchars($_GET['epid']) : htmlspecialchars($_POST['epid']);
		$num 	 = empty($_POST['num']) ? htmlspecialchars($_GET['num']) : htmlspecialchars($_POST['num']);
		$eimoney = empty($_POST['eimoney']) ? htmlspecialchars($_GET['eimoney']) : htmlspecialchars($_POST['eimoney']);

		if(empty($epid)){
			return_to('site/mall');
		}else{
			//商品資料
			$get_product = $mall->get_info($epid);
			$get_product['point_price'] = sprintf("%d", $get_product['point_price']);
			$get_product['process_fee'] = sprintf("%d", $get_product['process_fee'])+$eimoney;
			$get_product['retail_price'] = sprintf("%d", $get_product['retail_price']);
			$tpl->assign('product', $get_product);

			//商品類別
			$get_product_category = $mall->get_product_category();
			$tpl->assign('product_category', $get_product_category);

			//會員鯊魚點餘額資料
			$get_bonus = $member->get_bonus($userid);
			$get_bonus['amount'] = sprintf("%d", $get_bonus['amount']);
			$tpl->assign('bonus', $get_bonus);

			//庫存
			if($get_product['eptype'] == 2 || $get_product['eptype'] == 3){
				$stock = $mall->get_exchange_card($get_product['epid']);
				//實際庫存 = (庫存數量 / 每組數量) - 預留庫存
				$stock = (floor($stock / $get_product['set_qty'])) - $get_product['reserved_stock'];
				if($stock <= 0){
					$stock = 0;
				}
			}else if($get_product['eptype'] == 4){
				//此類型為無序號商品，無須判斷庫存，顧庫存寫死為20
				$stock = '20';
			}else{
				$stock = $mall->get_stock($epid);
				//實際庫存 = 庫存數量 / 每組數量
				$stock = floor($stock / $get_product['set_qty']);
			}
			
			$tpl->assign('stock', $stock);
		}

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = (!empty($get_product)) ? $get_product : array();
			$ret['retObj']['bonus'] = (!empty($get_bonus)) ? $get_bonus : new stdClass;
			$ret['retObj']['stock'] = (!empty($stock)) ? $stock : new stdClass;
			$ret['retObj']['num'] = (!empty($num)) ? $num : new stdClass;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("mall","check",true);
		}

	}


	/*
	 *	兌換商品
	 *	$cfrom					varchar					來源
	 *	$epid					int						商品編號
	 *	$epcid					int						商品分類編號
	 *	$userid					int						會員編號
	 */
	public function exchange() {

		global $tpl, $mall, $user, $member;

		//設定 Action 相關參數
		set_status($this->controller);
		$cfrom = empty($_POST['cfrom']) ? htmlspecialchars($_GET['cfrom']) : htmlspecialchars($_POST['cfrom']);
		$epid = empty($_POST['epid']) ? htmlspecialchars($_GET['epid']) : htmlspecialchars($_POST['epid']);
		$epcid = empty($_POST['epcid']) ? htmlspecialchars($_GET['epcid']) : htmlspecialchars($_POST['epcid']);
		$userid = empty($_POST['auth_id']) ? ($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if(empty($epid)){
			return_to('site/mall');
		}else{

			if($user->is_logged && !empty($_SESSION['user']) ){
				//會員認證資料
				$get_auth = $user->validUserAuth($_SESSION['auth_id'], $_SESSION['user']['profile']['phone']);
				$tpl->assign('auth', $get_auth);
			}

			//會員鯊魚點餘額資料
			$get_spoint = $member->get_bonus($userid);
			$tpl->assign('spoint', $get_spoint);

			//商品資料
			$get_product = $mall->get_info($epid);

			//商品類別
			$get_product_category = $mall->get_product_category();
			$tpl->assign('product_category', $get_product_category);

			//訂單列表
			$order_exchange = $mall->order_exchange($userid, $epid);
			$tpl->assign('order_exchange', $order_exchange);

			//查詢卡包資料
			$user_extrainfoArr[1] = $user->get_user_extrainfo(1, $userid);	//查詢中油卡資料
			$user_extrainfoArr[2] = $user->get_user_extrainfo(2, $userid);	//查詢校園一卡通資料
			$user_extrainfoArr[3] = $user->get_user_extrainfo(3, $userid);	//查詢支付寶資料
			$user_extrainfoArr[4] = $user->get_user_extrainfo(4, $userid);	//查詢qq資料
			$tpl->assign('user_extrainfoArr', $user_extrainfoArr);

			//庫存
			if($get_product['eptype'] == 2 || $get_product['eptype'] == 3){
				$stock = $mall->get_exchange_card($get_product['epid']);
				//實際庫存 = (庫存數量 / 每組數量) - 預留庫存
				$stock = (floor($stock / $get_product['set_qty'])) - $get_product['reserved_stock'];
				if($stock <= 0){
					$stock = 0;
				}
			}else if($get_product['eptype'] == 4){
				//此類型為無序號商品，無須判斷庫存，顧庫存寫死為20
				$stock = '20';
			}else{
				$stock = $mall->get_stock($epid);
				//實際庫存 = 庫存數量 / 每組數量
				$stock = floor($stock / $get_product['set_qty']);
			}

			if($get_product['epid'] == '493'){
				$stock = 1;
			}

			$tpl->assign('stock', $stock);
			
			//加油卡特別顯示內容
			if(($epid == '689') && (empty($user_extrainfoArr[1]['field2name']))){
				$msg = '(含一次性卡片工本費及掛號郵資)';
				$eimoney = 130;
			}elseif(($epid == '689') && (!empty($user_extrainfoArr[1]['field2name']))){
				$msg = '(將充值至會員綁定的加油卡號)';
				$eimoney = 0;
			}else{
				$msg = '';
				$eimoney = 0;
			}

			$get_product['eimoney'] = $eimoney;
			if ($eimoney == 130){
				$get_product['process_fee'] = $get_product['process_fee'] + $eimoney;
			}
			$get_product['msg'] = $msg;
			$tpl->assign('product', $get_product);

		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','JSON');

			if($get_product){
				$ret['retObj']=array();
				$get_product['stock']=$stock;
				$ret['retObj']=$get_product;
			}
			echo json_encode($ret);
		}else{
			$meta['title']=$get_product['name'];
			$meta['description']='';

			if(!empty($get_product['thumbnail'])){
				$meta['image'] = BASE_URL."/site/images/site/product/".$get_product['thumbnail_file'];
			}elseif(!empty($get_product['thumbnail_url'])){
				$meta['image'] = $get_product['thumbnail_url'];
			}

			$tpl->assign('meta',$meta);
			$tpl->set_title('');

			if(empty($cfrom)){
				$tpl->render("mall","exchange",true);
			}else{
				$tpl->render2("mall","exchange2",true);
			}

		}
	}

	//產生qrcode
	public function qrcode() {

		global $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		if(empty($_GET['epid']) && empty($_GET['amount'])){
			echo "<script>alert('資料錯誤');location.href = '".BASE_URL.APP_DIR."/mall/';</script>";
			exit;
		}else{
			$qrcodeArr['epid'] = $_GET['epid'];
			$qrcodeArr['amount'] = $_GET['amount'];
			$tpl->assign('qrcodeArr', $qrcodeArr);
		}

		//改變訂單狀態
		$mall->set_order($_SESSION['auth_id']);

		$tpl->set_title('');
		$tpl->render("mall","qrcode",true);

	}


	//查詢兌換訂單
	public function check_qrcode() {

    global $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once(LIB_DIR."/convertString.ini.php");
		$this->str = new convertString();
		$r['msg'] = 'no';

		//訂單查詢
		$get_order = $mall->get_order($_POST['uid']);

		if($get_order['orderid']){
			$r['msg'] = 'yes';
			$r['data'] = $get_order['orderid']."_".date("YmdHis");
		}
		echo json_encode($r);

	}


	//紅利點數兌換
	public function qrcode_exchange() {

    global $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once(LIB_DIR."/convertString.ini.php");
		$this->str = new convertString();

		$dataArr = explode("_", $_GET['data']);

		//訂單資料
		$get_order = $mall->get_order($_SESSION['auth_id'], $dataArr[0]);
		$tpl->assign('order', $get_order);

		if($get_order['epid'] != 0){
			//產品資料
			$get_product = $mall->get_info($get_order['epid']);
			$tpl->assign('product', $get_product);
		}

		$tpl->set_title('');
		$tpl->render("mall","qrcode_exchange",true);

	}


	//確認紅利點數
	public function qrcode_confirm() {

		global $tpl, $mall, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once(LIB_DIR."/convertString.ini.php");
		$this->str = new convertString();

		$dataArr = explode("_", $_GET['data']);

		foreach ($dataArr as $key => $value){
			$d = explode("=", $value);
			$data[$d[0]] = $d[1];
		}

		if(strtotime("now") - strtotime($data['t']) <= 60){
			if($data['epid']){
				//查詢產品
				$productArr = $mall->get_info($data['epid']);
				$productArr['memo'] = '';
				$productArr['tx_data'] = '';

				//查詢庫存數量
				$num = $mall->stock_check($data['epid']);

				if($num <= 0){
					echo "<script>alert('庫存不足');location.href = '".BASE_URL.APP_DIR."/mall/';</script>";
				}

				$amount = $productArr['point_price'] + $productArr['process_fee'];
			}else{
				$amount = $data['amount'];
				$productArr['point_price'] = $data['amount'];
				$productArr['process_fee'] = 0;
			}

			$bonus = $mall->bonus_check($data['uid']);

			if($bonus - $amount < 0){
				echo "<script>alert('此會員紅利點數不足');location.href = '".BASE_URL.APP_DIR."/mall/';</script>";
			}

			$orderid = $mall->add_order($productArr, $data['uid']);
			$userArr = $user->get_user_profile($data['uid']);
			$mall->add_order_consignee($orderid, $userArr);

      if($orderid && $userArr['userid']){
				$data_complete = $orderid."_".$userArr['userid'];
				header("Location:".BASE_URL.APP_DIR."/mall/qrcode_complete/?data=".$data_complete);
			}

			exit;

		}else{
			echo "<script>alert('超過時間，請重新產生一組二維碼');location.href = '".BASE_URL.APP_DIR."/mall/';</script>";
		}

	}


	function qrcode_complete() {

    global $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once(LIB_DIR."/convertString.ini.php");
		$this->str = new convertString();

		$dataArr = explode("_", $_GET['data']);
		$tpl->assign('dataArr', $dataArr);

    //訂單資料
		$get_order = $mall->get_order($dataArr[1], $dataArr[0]);
		$tpl->assign('order', $get_order);

		if($get_order['epid'] != 0){
			//產品資料
			$get_product = $mall->get_info($get_order['epid']);
			$tpl->assign('product', $get_product);
		}

		$tpl->set_title('');
		$tpl->render("mall","qrcode_complete",true);

	}


	function qrcode_checkout() {

    global $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once(LIB_DIR."/convertString.ini.php");
		$this->str = new convertString();

		$exchange_bonus_Arr = $mall->get_exchange_bonus_history($_POST['orderid'], $_POST['userid']);

		$r['msg'] = 'no';

		if($exchange_bonus_Arr['orderid']){
			$r['msg'] = 'yes';
			$bonusid = $mall->add_bonus($exchange_bonus_Arr, $_SESSION['auth_id']);
			$mall->add_exchange_bonus_history($exchange_bonus_Arr, $_SESSION['auth_id'], $bonusid);
		}

		echo json_encode($r);

	}


	function timerefresh() {

		$r['t'] = date("YmdHis");
		echo json_encode($r);

	}


	function replyAndExit($retArr) {

		$json_str=json_encode($retArr);
		error_log($json_str);
		echo $json_str;
		exit;

	}


	function getSafeStr($str) {
		$safeStr=addslashes($str);
		if($safeStr!=$str){
		  error_log("[getSafeStr] ".$str."-->".$safeStr);
		}
		return $safeStr;
	}


	// Qrcode Tx For Web Client
	public function genUserTxQrcode2() {

		global $db, $config, $tpl, $mall, $member;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		date_default_timezone_set('Asia/Shanghai');

		// $commit_url=BASE_URL.APP_DIR."/mall/vendorCommitTx/";

		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$curr_bonus;
		$countryid ="";
		$nationphonecode="";
		$isocontrycode="";

		// 取得目前紅利點數等相關資訊
		$query=" SELECT up.userid, up.countryid, up.channelid, c.isocountrycode, c.countrycode as nationphonecode, sum(IFNULL(b.amount,0)) as curr_bonus ";
		$query.=" FROM saja_user.saja_user_profile up ";
		$query.=" LEFT JOIN saja_channel.saja_country c ";
		$query.=" ON up.switch='Y' AND c.switch='Y' AND up.countryid=c.countryid ";
		$query.=" LEFT JOIN saja_cash_flow.saja_bonus b ";
		$query.=" ON up.switch='Y' AND b.switch='Y' AND up.userid=b.userid ";
		$query.=" WHERE up.userid='${userid}' ";

		error_log($query);

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'][0]['userid'])){
		  $countryid = $table['table']['record'][0]['countryid'];
		  $channelid = $table['table']['record'][0]['channelid'];
		  $nationphonecode = $table['table']['record'][0]['nationphonecode'];
		  $isocountrycode = $table['table']['record'][0]['isocountrycode'];
		  $curr_bonus = $table['table']['record'][0]['curr_bonus'];
		  $qc_timestamp = microtime(true);
		  $qc_time=date("YmdHis",$qc_timestamp);
		  $checkcode = explode(".",$qc_timestamp);
		  $checkcode[1]=str_pad($checkcode[1],4,"0",STR_PAD_RIGHT);
		  $tx_code=$isocountrycode.".".$channelid.".".$userid.".".$qc_timestamp;
		}

		error_log("[genUserTxQrcode2] channelid :".$channelid);
		error_log("[genUserTxQrcode2] qc_time :".$qc_time);
		error_log("[genUserTxQrcode2] tx_code :".$tx_code);

		// 產生交易紀錄
		$sql=" INSERT INTO saja_exchange.saja_exchange_vendor_record
					   set prefixid='saja',
						   userid='${userid}',
						   tx_code='${tx_code}',
						   tx_status='1',
						   tx_type='web_tx',
						   qrcode_time='${qc_time}',
						   insertt='${qc_time}' ";
		error_log($sql);
		$db->query($sql);
		$evrid=$db->_con->insert_id;
		error_log("[genUserTxQrcode2]:".$sql."-->".$evrid);

		$tx_code_bar=$evrid." ".$checkcode[1]." ".$checkcode[0]." ".$userid." ".$channelid;
		$tx_code_md5 = md5($tx_code);
		$tx_code_base64=base64_encode($tx_code);

		error_log("[genUserTxQrcode2] tx_code_bar :".$tx_code_bar);
		error_log("[genUserTxQrcode2] tx_code_md5 :".$tx_code_md5);

		$txArr['userid']=$userid;
		$txArr['evrid']=$evrid;
		$txArr['qc_time']=$qc_time;
		$txArr['qc_timestamp']=$qc_timestamp;
		$txArr['expired_time']=$checkcode[0]+150;
		$txArr['channelid']=$channelid;
		$txArr['isocountrycode']=$isocountrycode;
		$txArr['nationphonecode']=$nationphonecode;
		$txArr['tx_code']=$tx_code;
		$txArr['tx_code_md5']=$tx_code_md5;
		$txArr['tx_code_bar']=$tx_code_bar;
		$txArr['curr_bonus']=$curr_bonus;
		$txArr['ClientType']="web";
		$txArr['tx_status']="1";

		$tpl->assign('txArr', $txArr);

		//紅利積點數量
		$get_bonus = $member->get_bonus($userid);
		$tpl->assign('bonus', $get_bonus);

		$tpl->render("mall","userTxQrcode2",true);
	}

	// For APP call
	public function genUserTxQrcode() {

		global $db, $config, $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);
		error_log("[genUserTxQrcode] post : ".json_encode($_POST));

		date_default_timezone_set('Asia/Shanghai');

		$userid = empty($_REQUEST['userid']) ? $_POST['auth_id'] : $_POST['userid'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if(empty($userid)){
			$userid = $_GET['auth_id'];
		}

		//檢查是否由app戳
		if($json!='Y'){
		  error_log("[genUserTxQrcode] Media Error !! ");
			echo json_encode(getRetJSONArray(-99,'Media Error','MSG'));
		  exit;
		}

		$curr_bonus;
		$countryid ="";
		$nationphonecode="";
		$isocontrycode="";

		//取得目前紅利點數等相關資訊
		$query=" SELECT up.userid, up.countryid, up.channelid, c.isocountrycode, c.countrycode as nationphonecode, sum(IFNULL(b.amount,0)) as curr_bonus
				   FROM saja_user.saja_user_profile up
			  LEFT JOIN saja_channel.saja_country c
						ON up.switch='Y' AND c.switch='Y' AND up.countryid=c.countryid
			  LEFT JOIN saja_cash_flow.saja_bonus b
						ON up.switch='Y' AND b.switch='Y' AND up.userid=b.userid
				  WHERE up.userid='${userid}'
			   ";

		error_log("[mall/genUserTxQrcode] query :".$query);

		$table = $db->getQueryRecord($query);

		//檢查用戶是否存在
		if(empty($table['table']['record'][0]['userid'])){
		  error_log("[genUserTxQrcode] User Error !! ");
		  echo json_encode(getRetJSONArray(-1,'用戶不存在 !!','MSG'));
		  exit;
		}

		//產生交易參數
		if(!empty($table['table']['record'][0]['userid'])){
		  $channelid = $table['table']['record'][0]['channelid'];
		  $countryid = $table['table']['record'][0]['countryid'];
		  $curr_bonus = $table['table']['record'][0]['curr_bonus'];
		  $qc_timestamp = microtime(true);        // unixtime 微秒 xxxxxxxxxx.zzzz ex:1547001297.3798
		  $qc_time=date("YmdHis",$qc_timestamp);  // yyyymmddhhiiss ex:20190109103457
		  $micro_ts = explode(".",$qc_timestamp); // 取microtime小數點後位數至矩陣 ["1547001297","3798"]
		  $micro_ts[1]=str_pad($micro_ts[1],4,"0",STR_PAD_RIGHT);  // 把microtime小數點後位數zzzz補零成4位
		  $tx_code_0=$countryid.".".$userid;
		}

		error_log("[mall/genUserTxQrcode] tx_code_0 :".$tx_code_0);

		//產生交易紀錄
		$sql=" INSERT INTO saja_exchange.saja_exchange_vendor_record
		               set prefixid='saja',
          				     userid='${userid}',
          					   tx_code='${tx_code_0}',
          					   tx_status='1',
          					   tx_type='app_tx',
          					   salt='{$micro_ts[1]}',
          					   qrcode_time='${qc_time}',
          					   insertt='${qc_time}'
         ";
		$db->query($sql);

		//取得交易紀錄id
		$evrid=$db->_con->insert_id;
		error_log("[genUserTxQrcode]:".$sql."-->".$evrid);

		//檢查交易紀錄是否已產生
		if($evrid>0){

			//組合交易代碼
			$tx_code_0.=".".$evrid;
			$tx_code=$tx_code_0.".".$micro_ts[1];

			//交易代碼寫入交易紀錄
			$sql=" UPDATE saja_exchange.saja_exchange_vendor_record
					      SET tx_code='{$tx_code}'
				      WHERE evrid={$evrid}
			          AND userid='{$userid}'
				        AND tx_status='1'
					      AND tx_type='app_tx' ";
			$ret=$db->query($sql);

			//檢查交易代碼寫入是否正確
			if($ret==1){

				//回傳app交易設定參數
				$txArr['evrid']=$evrid;
				$txArr['qc_ts']=intval($micro_ts[0]);                // timestamp
				$txArr['qc_expired_ts']=intval($micro_ts[0])+60;     // 限制時間60secs
				$txArr['curr_bonus']=sprintf("%1\$u", $curr_bonus);
				$txArr['ClientType']="app";
				$txArr['tx_status']="1";
				$txArr['tx_key']=$tx_code;

				$tx_code_md5 = md5($tx_code);
				error_log("[genUserTxQrcode] tx_code_md5 :".$tx_code_md5);

				$tx_key=base64_encode($evrid."|".$tx_code_md5);

				$txArr['url']="https://www.saja.com.tw/management/webtx/vendorConfirm2/?tx_key=".$tx_key;
				$txArr['thumbnail_url'] = BASE_URL.APP_DIR."/phpqrcode/?data=".$txArr['url'];

				$qc_timestamp = microtime(true);
				$checkcode = explode(".",$qc_timestamp);
				$checkcode[1]=str_pad($checkcode[1],4,"0",STR_PAD_RIGHT);

				$tx_code_bar=$evrid." ".$checkcode[1]." ".$checkcode[0]." ".$userid." ".$channelid;

				$txArr['tx_code_bar']=$tx_code_bar;
				$txArr['code_bar_url'] = BASE_URL.APP_DIR."/barcodegen/genbarcode.php?text=".$tx_code_bar;

				$ret=getRetJSONArray(1,'OK','JSON');

				if($txArr){
				  error_log("[c/mall/genUserTxQrcode] txArr : ".json_encode($txArr));
				  $ret['retObj']=array();
				  $ret['retObj']=$txArr;
				}

			}else{//交易代碼寫入失敗
				$ret=getRetJSONArray(-2,'TX Data ERROR','MSG');
			}
		}else{//交易紀錄新增失敗
		  $ret=getRetJSONArray(-3,'TX ID ERROR','MSG');
		}

		//輸出結果
		$r=json_encode($ret,JSON_UNESCAPED_SLASHES);
		error_log("[mall/genUserTxQrcode] : ".$r);
		echo ($r);
		exit;
	}

	//會員確認Qrcode交易
	public function userConfirmTx() {
		global $db, $config, $tpl, $mall, $enterprise;

		//設定 Action 相關參數
		login_required();
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');

		$evrid=empty($_GET['evrid'])?$_POST['evrid']:$_GET['evrid'];
		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		if(!empty($evrid) && !empty($userid)){
		  $arrCond=array();
		  $arrCond['evrid']=$evrid;
		  $arrCond['up.userid']=$userid;
		  $record=$mall->getQrcodeTxRecord($arrCond);
		  $src=$enterprise->getEnterpriseDetail($record['vendorid']);
		}
		error_log("[c/mall/userConfirmTx] src : ".json_encode($src));

		$record['thumbnail_url'] = $src['thumbnail_url'];
		$record['thumbnail_file'] = $src['thumbnail_file'];
		error_log("[c/mall/userConfirmTx] record : ".json_encode($record));

		foreach($record as $k=>$v){
		  if($record[$k]==null)
			$record[$k]="";
		}

		$record['tx_key']=base64_encode($record['evrid']."|".md5($record['tx_code']));
		unset($record['tx_code']);
		if($json=="Y"){
		  $ret=getRetJSONArray(1,'OK','JSON');
		  $ret['retObj']=array();
		  $ret['retObj']=$record;
		  echo json_encode($ret);
		}else{
		  $tpl->assign('record', $record);
		  $tpl->render("mall","userConfirmTx",true);
		}
	}

	// 取得QrcodeTx交易狀態
	public function getQrcodeTxStatus() {
		global $db, $config, $tpl, $mall;

		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');

		$evrid=empty($_GET['evrid'])?$_POST['evrid']:$_GET['evrid'];
		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		error_log("[getQrcodeTxStatus] evrid :".$evrid);

		if(!empty($evrid)){
		  $tx_status=$mall->getTxStatus($evrid);
		}
		error_log("[getQrcodeTxStatus] tx_status :".$tx_status);

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','JSON');
		  $ret['retObj']=array();
		  $ret['retObj']['evrid']=$evrid;
		  $ret['retObj']['TxStatus']=empty($tx_status)?"":$tx_status;
		  echo json_encode($ret);
		}else{
		  echo $tx_status;
		}

		exit;
	}


	function chkExpwd() {
		global $db, $config, $user;

		login_required();
		$expwd=$_POST['expwd'];
		$userid=$_POST['auth_id'];
		$ret=null;

		if(empty($expwd) || empty($userid)){
      echo json_encode(getRetJSONArray(-1,'DATA CHECK ERROR !!','MSG'));
		}else{
			$cs = new convertString();
			$get_user=$user->get_user($userid);
		  $exchangepasswd = $cs->strEncode($expwd, $config['encode_key']);
		  if($get_user){
        error_log("[c/chkExpwd]:".$exchangepasswd."-->".$get_user['exchangepasswd']);
        if($exchangepasswd==$get_user['exchangepasswd']){
          $ret=getRetJSONArray(1,'OK','MSG');
        }else{
          $ret=getRetJSONArray(-2,'兌換密碼錯誤 !!','MSG');
        }
			}else{
			  $ret=getRetJSONArray(0,'數據不存在或狀態錯誤 !!','MSG');
			}
		}
		echo json_encode($ret);

		exit;
	}


	//會員確認Qrcode交易
	function userCommitTx() {
		global $db, $config, $tpl, $mall, $enterprise, $user;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);
		date_default_timezone_set('Asia/Shanghai');

		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$expw=empty($_POST['expw'])?$_GET['expw']:$_POST['expw'];
		$userid=empty($_POST['auth_id'])?$_SESSION['auth_id']:$_POST['auth_id'];
		$evrid=empty($_POST['evrid'])?$_GET['evrid']:$_POST['evrid'];
		$bonus_noexpw=empty($_POST['bonus_noexpw'])?$_GET['bonus_noexpw']:$_POST['bonus_noexpw'];
		$bonus_total=empty($_POST['bonus_total'])?$_GET['bonus_total']:$_POST['bonus_total'];

		$arrCond=array();

		try {
		  //帳號檢核
		  if($userid!=$_SESSION['auth_id']){
			$ret=getRetJSONArray(-105,urlencode('會員數據錯誤 !!'),'MSG');
			echo json_encode($ret);
			exit;
		  }else{
			  error_log("[mall.userConfirmTx] userid : ".$userid." Check OK !!");
		  }

		  // 免密 兌換密碼檢核
		  if($bonus_total>$bonus_noexpw){
			$cs = new convertString();
			$exchangepasswd = $cs->strEncode($expw, $config['encode_key']);
			$query = "SELECT *
							FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
					   WHERE prefixid = '{$config['default_prefix_id']}'
						 AND userid = '{$userid}'
						 AND exchangepasswd = '{$exchangepasswd}'
						 AND switch = 'Y'
					   LIMIT 1
						 ";
			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'][0]['exchangepasswd'])){
				$ret=getRetJSONArray(-112,urlencode('兌換密碼錯誤 !!'),'MSG');
				echo json_encode($ret);
				exit;
			}else{
			  error_log("[mall.userConfirmTx] exchange pwd : ".$expw." Check OK !!");
			}
		  }

		  $arrCond['evrid']=$evrid;
		  $record=$mall->getQrcodeTxRecord($arrCond);
		  error_log("[mall.userConfirmTx] record : ".json_encode($record));

		  $retArr=array();

		  //確認資料存在
		  $retArr['evrid']=$evrid;
		  if(!$record){
			$ret=getRetJSONArray(-100,urlencode('數據不存在或狀態錯誤 !!'),'MSG');
			echo json_encode($ret);
			exit;
		  }else{
			error_log("[mall.userConfirmTx] evrid : ".$evrid." Check OK !!");
		  }

		  //確認交易狀態資料
		  if($record['tx_status']!=3){
			$ret=getRetJSONArray(-111,urlencode('事務數據狀態錯誤 !!'),'MSG');
			echo json_encode($ret);
			exit;
		  }else{
			error_log("[mall.userConfirmTx] tx_status : ".$tx_status." Check OK !!");
		  }

		  $arrCond['userid']=$userid;
		  $arrCond['tx_status']='3';

		  // 確認紅利點數
		  $require_bonus=$record['total_price'];
		  $curr_bonus = $mall->bonus_check($userid);
		  error_log("[mall.userConfirmTx] curr bonus : ".$curr_bonus.", required bonus :".$require_bonus);
		  if($curr_bonus<$require_bonus){
			$ret=getRetJSONArray(-104,urlencode('會員紅利點數不足 !!'),'MSG');
			echo json_encode($ret);
			exit;
		  }else{
			error_log("[mall.userConfirmTx] curr bonus check OK !!");
		  }

		  //取得企業資料
		  $enterpriseprofile = $enterprise->getEnterpriseDetail($record['vendorid']);
		  error_log("[c/mall/userCommitTx] enterprise profile :".json_encode($enterpriseprofile));

		  $profit_ratio = ($enterpriseprofile['profit_ratio']) ? (float)$enterpriseprofile['profit_ratio'] : 0;

		  // 取得使用者詳細資料
		  $userprofile = $user->get_user_profile($userid);
		  error_log("[c/mall/userCommitTx] user profile :".json_encode($userprofile));

		  //OK
		  //產生會員紅利點數支付紀錄
		  $query = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus` set
								`prefixid` = '{$config['default_prefix_id']}',
								`userid` = '{$userid}',
								`countryid` = '{$config['country']}',
								`behav` = 'user_qrcode_tx',
								`amount` = '-{$require_bonus}',
								`seq` = '0',
								`switch` = 'Y',
								`insertt` = now()";
		  error_log("[mall.userConfirmTx] pay bonus : ".$query);
		  $db->query($query);
		  $bonusid = $db->_con->insert_id;

		  $sys_profit = $require_bonus * ($profit_ratio/1000);
		  //產生商家紅利點數收取記錄
		  $insert = "insert into `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}bonus_store` set
								  `prefixid` = '{$config['default_prefix_id']}',
								  `bonusid` = '{$bonusid}',
								  `enterpriseid`='".$record['vendorid']."',
								  `esid`=(select esid from saja_user.saja_enterprise where enterpriseid='".$record['vendorid']."' ),
								  `countryid` = (select countryid from saja_user.saja_enterprise_profile where enterpriseid='".$record['vendorid']."' ),
								  `behav` = 'user_qrcode_tx',
								  `amount` = '{$require_bonus}',
								  `sys_profit` = '{$sys_profit}',
								  `seq` = '0',
								  `switch` = 'Y',
								  `insertt` = now()";
		  error_log("[mall.userConfirmTx] earn bonus : ".$insert);
		  $db->query($insert);

		  //修改交易紀錄
		  $arrUpd=array();
		  $tx_status='4';
		  $arrUpd['bonusid']=$bonusid;
		  $arrUpd['tx_status']=$tx_status;
		  $arrUpd['commit_time']=date('YmdHis');
		  $arrUpd['total_bonus']=$require_bonus - $sys_profit;


		  $retCode=$mall->updQrcodeTxRecord($arrUpd,$arrCond);

		  /*new 2016/12/22*/

		  //組合兌換資料
		  $productArr['esid'] = $enterpriseprofile['esid'];
		  $productArr['epid'] = 0;
		  $productArr['point_price'] = $require_bonus;
		  $productArr['process_fee'] = 0;
		  $productArr['memo'] = '鯊魚點支付：'.$require_bonus.'點';
		  $productArr['tx_data'] = '';
		  $productArr['confirm'] = 'Y';
		  $productArr['cost'] = 0;

		  //新增訂單
		  $orderid = $mall->add_order($productArr, $userid);
		  error_log("[c/mall/userCommitTx] orderid :".$orderid);

		  //新增訂單收件人
		  $consignee = $mall->add_order_consignee($orderid, $userprofile);
		  error_log("[c/mall/userCommitTx] consignee :".json_encode($consignee));
		  /*new 2016/12/22*/

		  $retArr=array();
		  if($retCode){

			/*new 2016/12/22*/
			//進行三級分潤
			$profit_ratio = ($enterpriseprofile['profit_ratio']) ? (float)$enterpriseprofile['profit_ratio'] : 0;

			$postdata['esid'] = $enterpriseprofile['esid'];
			$postdata['epid'] = 0;
			$postdata['amount'] = $sys_profit;
			$postdata['orderid'] = $orderid;
			$postdata['userid'] = $userid;

			$url = "http://www.saja.com.tw/site/mall/giftDistributionProfits";
			//設定送出方式-POST
			$stream_options = array(
									'http' => array (
											  'method' => "POST",
											  'content' => json_encode($postdata),
											  'header' => "Content-Type:application/json"
												)
			);
			$context  = stream_context_create($stream_options);
			// 送出json內容並取回結果
			$response = file_get_contents($url, false, $context);
			error_log("[ajax/mall/userCommitTx] response : ".$response);
			/*new 2016/12/22*/

			$retArr['retCode']='1';
			$retArr['retMsg']='交易完成, 使用鯊魚點數 : '.ceil($require_bonus).' 點!';
		  }

		}catch(Exception $e){
		  $retArr['retCode']=$e->getCode();
		  $retArr['retMsg']=$e->getMessage();
		}

		$ret=getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
		echo json_encode($ret);

		exit;
	}


	//逾時取消Qrcode交易
	function cancelQrcodeTx() {
		global $db, $config, $tpl, $mall;

		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');

		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$arrCond=array();
		$arrCond['evrid']=$_REQUEST['evrid'];

		$arrUpd=array();
		$arrUpd['tx_status']='0';

		$retCode=$mall->updQrcodeTxRecord($arrUpd, $arrCond);

		if($retCode){
		  $retArr['retCode']='1';
		  $retArr['retMsg']=urlencode('交易逾時取消 !!');
		}else{
		  $retArr['retCode']=$retCode;
		  $retArr['retMsg']=urlencode('交易取消失敗 !!');
		}

		$ret=getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
		echo json_encode($ret);

		exit;
	}


	//會員取消Qrcode交易
	function userCancelTx() {
		global $db, $config, $tpl, $mall;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');

		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];

		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$evrid=$_POST['evrid'];
		$arrCond=array();
		$arrCond['userid']=$userid;
		$arrCond['evrid']=$evrid;

		$arrUpd=array();
		$arrUpd['tx_status']='-1';
		$retCode=$mall->updQrcodeTxRecord($arrUpd, $arrCond);

		$retArr=array();
		$retArr['evrid']=$evrid;

		if($retCode){
		  $retArr['retCode']='1';
		  $retArr['retMsg']=urlencode('買方取消交易 !!');
		}else{
		  $retArr['retCode']=$retCode;
		  $retArr['retMsg']=urlencode('買方取消交易失敗 !!');
		}

		$ret=getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
		$ret['evrid'] = $retArr['evrid'];
		echo json_encode($ret);

		exit;
	}


	// 商家確認進行交易
	public function vendorCommitTx() {
		global $db, $config, $tpl, $mall;

		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');

		$json=empty($_POST['json'])?$_GET['json']:$_POST['json'];

		$tx_check_code=$_POST['tx_check_code'];
		$evrid=$this->getSafeStr($_POST['evrid']);
		$vendor_name=$this->getSafeStr($_POST['vendor_name']);
		$vendorid=$this->getSafeStr($_POST['vendorid']);
		$prod_pn=$this->getSafeStr($_POST['prod_pn']);
		$prod_name=$this->getSafeStr($_POST['prod_name']);
		$vendor_prodid=$this->getSafeStr($_POST['vendor_prodid']);
		$tx_quantity=$this->getSafeStr($_POST['tx_quantity']);
		$tx_currency=$this->getSafeStr($_POST['tx_currency']);
		$total_price=$this->getSafeStr($_POST['total_price']);
		$unit_price=$this->getSafeStr($_POST['unit_price']);
		$total_bonus=$this->getSafeStr($_POST['total_bonus']);
		$tx_type=$this->getSafeStr($_POST['tx_type']);
		$bg_notify_url=$this->getSafeStr($_POST['bg_notify_url']);
		$memo=$this->getSafeStr($_POST['memo']);
		$reqin_timestamp=time();
		$reqin_time=date('YmdHis',$reqin_timestamp);

		$retArr=array();

		if(empty($evrid)){
		  $ret=getRetJSONArray(-1,urlencode('交易序號缺誤 !!'),'MSG');
		  echo json_encode($ret);
		  exit;
		}

		$retArr['evrid']=$evrid;

		if(empty($tx_check_code)){
		  $ret=getRetJSONArray(-2,urlencode('交易驗證碼缺誤 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		$arrCond=array();
		$arrCond['evrid']=$evrid;
		$arrCond['tx_status']='1';
		$record=$mall->getQrcodeTxRecord($arrCond);

		if(empty($record['evrid'])){
		  $ret=getRetJSONArray(-100,urlencode('交易資料缺誤 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		// tx_check_code 驗證
		$tx_code=$record['tx_code'];
		error_log("[vendorCommitTx] tx_code : ".$tx_code);

		if($tx_check_code!=md5($tx_code)){
		  $ret=getRetJSONArray(-100,urlencode('交易碼驗證失敗 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		// 交易時效驗證
		$t=new DateTime($record['qrcode_time']);
		$qrcode_timestamp = $t->getTimestamp();

		if($reqin_timestamp-$qrcode_timestamp>63){
		  $ret=getRetJSONArray(-102,urlencode('商家確認時效已過 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		//各項資料驗證...ToDoList
		//商家名稱
		if(!empty($vendor_name)){
		  $arrUpd['vendor_name']=$vendor_name;
		}else{
		  $ret=getRetJSONArray(-4,urlencode('商家名稱缺誤 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		// 交易類別
		if(!empty($tx_type)){
		  $arrUpd['tx_type']=$tx_type;
		}else{
		  $ret=getRetJSONArray(-11,urlencode('交易類別缺誤 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		//商品交易數量
		if(!empty($tx_quantity)){
		  if(is_numeric($tx_quantity)){
			$arrUpd['tx_quantity']=$tx_quantity;
		  }else{

			$ret=getRetJSONArray(-108,urlencode('商品交易數量格式異常 !!'),'MSG');
			$ret['evrid'] = $retArr['evrid'];
			echo json_encode($ret);
			exit;
		  }
		}else{
		  $ret=getRetJSONArray(-7,urlencode('商品交易數量缺誤 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		//交易總金額
		if(!empty($total_price)){
		  if(is_numeric($total_price)){
			$arrUpd['total_price']=$total_price;
		  }else{
			$ret=getRetJSONArray(-109,urlencode('交易總金額格式異常 !!'),'MSG');
			$ret['evrid'] = $retArr['evrid'];
			echo json_encode($ret);
			exit;
		  }
		}else{
		  $ret=getRetJSONArray(-8,urlencode('交易總金額缺誤 !!'),'MSG');
		  echo json_encode($ret);
		  exit;
		}

		//所需紅利點數
		if(!empty($total_bonus)){
		  if(is_numeric($total_bonus)){
			$arrUpd['total_bonus']=$total_bonus;
		  }else{
			$ret=getRetJSONArray(-110,urlencode('所需紅利點數格式異常 !!'),'MSG');
			$ret['evrid'] = $retArr['evrid'];
			echo json_encode($ret);
			exit;
		  }
		}else{
		  $ret=getRetJSONArray(-9,urlencode('); !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		if(!empty($bg_notify_url)){
		  $arrUpd['bg_notify_url']=$bg_notify_url;
		}

		if(!empty($unit_price)){
		  $arrUpd['unit_price']=$unit_price;
		}
		if(!empty($vendorid)){
		  $arrUpd['vendorid']=$vendorid;
		}
		if(!empty($vendor_prodid)){
		  $arrUpd['vendor_prodid']=$vendor_prodid;
		}

		if(!empty($prod_pn)){
		  $arrUpd['prod_pn']=$prod_pn;
		}

		if(!empty($memo)){
		  $arrUpd['memo']=$memo;
		}

		// 驗證通過, 修改交易資料
		$arrUpd['keyin_time']=$reqin_time;
		$arrUpd['tx_status']='2';
		$retCode=$mall->updQrcodeTxRecord($arrUpd,$arrCond);
		if($retCode){
		  $ret=getRetJSONArray(1,urlencode('確認完成 !!'),'MSG');
		  $ret['evrid'] = $retArr['evrid'];
		  echo json_encode($ret);
		  exit;
		}

		exit;
	}


	public function getExchangeVendorRecord() {
		global $db, $config, $tpl, $mall;

		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');

		login_required();

		$evrid=$_REQUEST['evrid'];
		$json=$_REQUEST['json'];

		if(!empty($evrid)){
		  $table=$mall->getExchangeVendorRecord(array("evrid"=>$evrid));
		  if($table){
			error_log(json_encode($table));
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=$table[0];
			echo json_encode($ret);
		  }
		}

	}


	//
	public function getMallStoreList() {

    global $mall;

    $lat=$_POST['lat'];
    $lng=$_POST['lng'];

    $ret=getRetJSONArray(1,'OK','LIST');

    $arr1=array();
    $arr1['esid']=1;
    $arr1['emid']=6;
    $arr1['name']='上海巴根網絡科技有限公司';
    $arr1['address']='上海市徐匯區桂平路418號702-D室';
    $arr1['telno']='021-6115-2070#810';
    $arr1['mobile']='';
    $arr1['thumbnail_url']='https://mmbiz.qlogo.cn/mmbiz/9wsDib62U1qFCzUfy3NvCNf7d2lk38tJAOT9ZHokM6NlvyLU1J6484LqxMlJcuXFQ2k3VsWpbIYdlPZYktGt6hw/0?wx_fmt=jpeg';
    $arr1['url']='www.saja.com.tw';
    $arr1['email']='bargen@shajiawang.com';
    $arr1['start_time']='09:00';
    $arr1['off_time']='21:30';
    $arr1['lat']=31.25;
    $arr1['lng']=121.34;
    $arr1['distance']=0.35;

    $arr2=array();
    $arr2['esid']=2;
    $arr2['emid']=4;
    $arr2['name']='殺價王股份有限公司';
    $arr2['address']='北市和平東路3段255號7樓';
    $arr2['telno']='02-27380898';
    $arr2['mobile']='';
    $arr2['thumbnail_url']="https://mmbiz.qlogo.cn/mmbiz/9wsDib62U1qGAKl1mTPMz6fmLycH1hr6orOrufibvSCIeZJUh0KW18zN0bWvJ9svtS3dgvibIDfVeUILvsBiaQIMPQ/0?wx_fmt=jpeg";
    $arr2['url']='www.saja.com.tw';
    $arr2['email']='thomas.liu@saja.com.tw';
    $arr2['start_time']='09:00';
    $arr2['off_time']='18:00';
    $arr2['lat']=32;
    $arr2['lng']=122;
    $arr2['distance']=0.43;

    $list=array();
    array_push($list,$arr1);
    array_push($list,$arr2);

    $ret['retObj']['data']=$list;
    echo json_encode($ret);

    /*
    esid:商店編號, emid:所屬企業編號, name:商店名稱, address:地址,
    telno:市話號碼, mobile:手機號碼, thumbnail_url:圖片網址,url:官網網址, email:聯絡Email,
    start_time:營業起始時間,off_time:營業結束時間, lat:緯度,lng:經度,distance:距離
    */

	}


	public function getAds() {

    global $tpl, $mall;

    $acid=$_REQUEST['acid'];

    if(empty($acid))
      $acid='1';

    $showType=$_REQUEST['showType'];

    if(empty($showType))
      $showType='P';

    $adlist=$mall->getAds($acid,$showType);
    if($adlist){
      $ret=getRetJSONArray(1,'OK','LIST');
      $ret['retObj']['data']=$adlist['table']['record'];
    }else{
      $ret=getRetJSONArray(0,'No Data Found !!','MSG');

    }
    echo json_encode($ret);

    exit;

	}


	public function getFreeExchangeGiftNum() {
    global $mall;
    login_required();
    $ret=getRetJSONArray(1,'OK','JSON');
    $ret['retObj']=array("amount"=>"0.00");
    echo json_encode($ret);
    exit;

	}


	/*
	 *	兌換ofpay商品功能
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$type			varchar		ofpay
	 *	$uuid			int			會員編號&商品編號
	 *	$expw			int			兌換密碼
	 *	$tx_num			int			交易數量
	 *	$vndr_txid		int			ofpay端交易識別編號(UID)
	 *	$vndr_prodid	varchar		ofpay端產品編號
	 *	$total_bonus	int			總計使用鯊魚用點數
	 *	$sign			varchar		ofpay格式的簽名
	 */
	public function extExchange() {

		global $config, $tpl, $mall, $user, $member;

		$json			= empty($_POST['json']) ? $_GET['json'] : $_POST['json'];
		$type			= empty($_POST['type']) ? $_GET['type'] : $_POST['type'];
		$uuid			= empty($_POST['uuid']) ? $_GET['uuid'] : $_POST['uuid'];
		$expw			= empty($_POST['expw']) ? $_GET['expw'] : $_POST['expw'];
		$tx_num			= empty($_POST['tx_num']) ? $_GET['tx_num'] : $_POST['tx_num'];
		$vndr_txid		= empty($_POST['vndr_txid']) ? $_GET['vndr_txid'] : $_POST['vndr_txid'];
		$vndr_odrid		= empty($_POST['vndr_odrid']) ? $_GET['vndr_odrid'] : $_POST['vndr_odrid'];
		$vndr_prodid	= empty($_POST['vndr_prodid']) ? $_GET['vndr_prodid'] : $_POST['vndr_prodid'];
		$total_bonus	= empty($_POST['total_bonus']) ? $_GET['total_bonus'] : $_POST['total_bonus'];
		$sign			= empty($_POST['sign']) ? $_GET['sign'] : $_POST['sign'];

		//使用者在APP下接刷新資料使用
		if(!empty($_POST['uuid'])){
			$_SESSION['malldata'] = $_POST;
		}

		if(empty($_POST['uuid']) && !empty($_SESSION['malldata']['uuid'])){
			$json			= $_SESSION['malldata']['json'];
			$type			= $_SESSION['malldata']['type'];
			$uuid			= $_SESSION['malldata']['uuid'];
			$expw			= $_SESSION['malldata']['expw'];
			$tx_num			= $_SESSION['malldata']['tx_num'];
			$vndr_txid		= $_SESSION['malldata']['vndr_txid'];
			$vndr_odrid		= $_SESSION['malldata']['vndr_odrid'];
			$vndr_prodid	= $_SESSION['malldata']['vndr_prodid'];
			$total_bonus	= $_SESSION['malldata']['total_bonus'];
			$sign			= $_SESSION['malldata']['sign'];
		}

		$uedata = explode('|',$uuid);
		$userid = $uedata[0];
		$epid 	= $uedata[1];

		$str2	= substr($epid, 0, 3);
		error_log("[c/mall/extExchange] str2 :".$str2);
		if($str2 == "sbn"){
			$url = "http://www.sharebonus.com/get_product/exchange/?web=Y";
			redirect_post($url ,$_POST);
		}

		//取得使用者資料
		$userdata = $user->get_user($userid);

		error_log("[c/mall/extExchange] post data :".json_encode($_POST));
		error_log("[c/mall/extExchange] get data :".json_encode($_GET));

		//判斷使用者資料是否正確
		if((!empty($userid)) && ($userdata['userid'] == $userid)){

			$time_start = microtime(true);

			//取得兌換密碼
			$this->str = new convertString();
			$exchangepasswd = $this->str->strEncode($expw, $config['encode_key']);

			//判斷是否輸入兌換密碼
			if(empty($expw)){
				//訂單資料
        $order_data = array(
                  					'json'			=> $json,
                  					'type'			=> $type,
                  					'uuid'			=> $uuid,
                  					'tx_num'		=> $tx_num,
                  					'vndr_txid'		=> $vndr_txid,
                  					'vndr_odrid'	=> $vndr_odrid,
                  					'vndr_prodid'	=> $vndr_prodid,
                  					'total_bonus'	=> $total_bonus,
                  					'sign'			=> $sign
				);
				$tpl->assign('order', $order_data);
				$tpl->set_title('');

				$tpl->render("mall","exchangecode",true, '', 'Y');
				exit;

			}else{
				//判斷兌換密碼是否正確
				if((!empty($expw)) && ($userdata['exchangepasswd'] == $exchangepasswd)){

					//判斷兌換的相關資料是否正確
					if((!empty($type)) && (!empty($tx_num)) && (!empty($vndr_txid)) && (!empty($vndr_prodid)) && (!empty($total_bonus))){

						$time = date("YmdHi");

						if(empty($vndr_odrid)){
						  $localsign0="type=".$type."&uuid=".$uuid."&tx_num=".$tx_num."&vndr_txid=".$vndr_txid."&vndr_prodid=".$vndr_prodid."&total_bonus=".$total_bonus."&key=osfapjaay";
						}else{
						  $localsign0="type=".$type."&uuid=".$uuid."&tx_num=".$tx_num."&vndr_txid=".$vndr_txid."&vndr_odrid=".$vndr_odrid."&vndr_prodid=".$vndr_prodid."&total_bonus=".$total_bonus."&key=osfapjaay";
						}

						error_log("[c/mall/extExchange] input params :".$localsign0);
						$localsign = md5($localsign0);
						error_log("[c/mall/extExchange] local sign :".$localsign);
						error_log("[c/mall/extExchange] remote sign :".$sign);

						//判斷簽名是否正確
						if($sign == $localsign){

							//取得使用者詳細資料
							$userprofile = $user->get_user_profile($userid);
							error_log("[c/mall/extExchange] user profile :".json_encode($userprofile));

							//取得使用者鯊魚點數資料
							$get_spoint = $member->get_bonus($userid);

							error_log("[c/mall/extExchange] user spoint: ".(int)$get_spoint['amount']." <=> needed spoints: ".(int)$total_bonus);
							//判斷鯊魚點數餘額是否足夠使用
							if((int)$get_spoint['amount'] >= (int)$total_bonus){

								//組合兌換資料
								$productArr['esid'] = 7;
								$productArr['epid'] = $epid;
								$productArr['point_price'] = $total_bonus;
								$productArr['process_fee'] = 0;
								$formdata = empty($_POST) ? json_encode($_GET) : json_encode($_POST);
								$memo = json_decode($formdata,true);
								unset($memo['expw']);
								$productArr['num'] = $tx_num;
								$productArr['memo'] = '鯊魚商城點數使用：'.$total_bonus.'點';
								$productArr['tx_data'] = json_encode($memo);
								error_log("[1]".json_encode($productArr));
								//新增訂單
								$orderid = $mall->add_order($productArr, $userid);
								error_log("[c/mall/extExchange] orderid :".$orderid);

								//新增訂單收件人
								$consignee = $mall->add_order_consignee($orderid, $userprofile);
								error_log("[c/mall/extExchange] consignee :".json_encode($consignee));

								//兌換紅利計算
								$bonusdata['amount'] = $total_bonus;
								$bonusdata['orderid'] = $orderid;

								//扣除點數
								$bonusid = $mall->add_bonus($bonusdata, $userid);
								error_log("[c/mall/extExchange] bonusid :".json_encode($bonusid));

								//新增紅利點數歷史資料
								$history = $mall->add_exchange_bonus_history($bonusdata, $userid, $bonusid);
								error_log("[c/mall/extExchange] history :".json_encode($history));

								$ret = getRetJSONArray(1,'OK','JSON');
								$ret['sign'] = $localsign;
								$ret['userid'] = $userid;
								$ret['txid'] = $orderid;
								$ret['vndr_txid'] = $vndr_txid;

								$ofdata['retCode'] = "1";
								$ofdata['retMsg'] = "OK";
								$ofdata['userid'] = (string)$userid;
								$ofdata['txid'] = (string)$orderid;
								$ofdata['vndr_txid'] = $vndr_txid;
								if(!empty($vndr_odrid)){
									// ofpay新版系統
									$ofdata['total_bonus'] = $total_bonus;
									$ofdata['sign']	= md5("retCode=".$ofdata['retCode']."&retMsg=".$ofdata['retMsg']."&userid=".$ofdata['userid']."&txid=".$ofdata['txid']."&vndr_txid=".$ofdata['vndr_txid']."&total_bonus=".$ofdata['total_bonus']."&key=osfapjaay");
							  }else{
								  // ofpay舊版系統
									$ofdata['sign']	= md5("retCode=".$ofdata['retCode']."&retMsg=".$ofdata['retMsg']."&userid=".$ofdata['userid']."&txid=".$ofdata['txid']."&vndr_txid=".$ofdata['vndr_txid']."&key=osfapjaay");
								}
								error_log("[2]".json_encode($ofdata));
							}else{
								error_log("[c/mall/extExchange] spoint error :".$get_spoint['amount']);
								$ret = getRetJSONArray(-4,urlencode('鯊魚點數不足!!'),'MSG');
							}

						}else{
							error_log("[c/mall/extExchange] sign error :".$sign." -- localsign error :".$localsign);
							$ret = getRetJSONArray(-2,urlencode('簽名驗證錯誤!!'),'MSG');
						}

					}else{
						error_log("[c/mall/extExchange] postdata error :".json_encode($_POST));
						$ret = getRetJSONArray(-5,urlencode('查無事務數據 !!'),'MSG');
					}

				}else{
					error_log("[c/mall/extExchange] expw error :".$expw);
					$ret = getRetJSONArray(-3,urlencode('兌換密碼不正確!!'),'MSG');

					//訂單資料
					$order_data = array(
                  						'json'			=> $json,
                  						'type'			=> $type,
                  						'uuid'			=> $uuid,
                  						'tx_num'		=> $tx_num,
                  						'vndr_txid'		=> $vndr_txid,
                  						'vndr_odrid'	=> $vndr_odrid,
                  						'vndr_prodid'	=> $vndr_prodid,
                  						'total_bonus'	=> $total_bonus,
                  						'sign'			=> $sign,
                  						'retCode'		=> $ret['retCode'],
                  						'retMsg'		=> "兌換密碼不正確 !!"

					);

					$tpl->assign('order', $order_data);
					$tpl->set_title('');
					$tpl->render("mall","exchangecode",true, '', 'Y');
					exit;
				}
			}
		}else{
			error_log("[c/mall/extExchange] userid error :".$userid);
			$ret = getRetJSONArray(-1,urlencode('用戶不存在!!'),'MSG');
		}

		$ofpay_gw_url = OFPAY_GW_URL;

		error_log("[c/mall/extExchange] ofdata POST :".json_encode($ofdata));

		//設定送出方式-POST
		$stream_options = array(
                            'http' => array (
                                              'method' => "POST",
                                              'content' => json_encode($ofdata),
                                              // 'header' => "Content-Type: application/json\r\n" . "Accept: application/json\r\n"
                                              'header' => "Content-Type:application/json"
                                            )
		);
		$context  = stream_context_create($stream_options);
		error_log("[c/mall/extExchange] context :".$context);


		// 送出json內容並取回結果
		$response = file_get_contents($ofpay_gw_url, false, $context);
		// 讀取json內容
		$arr = json_decode($response,true);
		error_log("[c/mall/extExchange] ofpay Response[retCode] : ".$arr['retCode']);
    error_log("[c/mall/extExchange] ofpay Response[retMsg] : ".$arr['retMsg']);
		error_log("[c/mall/extExchange] ofpay Response : ".json_encode($arr));

		if(!empty($arr['retCode'])){
			//查詢紅利點數歷史資料
			$history = $mall->get_exchange_bonus_history($arr['txid'], $arr['userid']);

			//判斷紅利點數歷史資料是否存在
			if(!empty($history['ebhid'])){

				//判斷回覆結果調整訂單
				if($arr['retCode'] == 1){
					$switch = 'Y';
					$confirm = 'Y';
					$status = 1;

					//更改訂單狀態
					$orderid = $mall->set_order($arr['userid'], $arr['txid'], $switch, $confirm, $status);
					error_log("[c/mall/extExchange] Enable order :".json_encode($orderid));

				}else{

					$switch = 'N';
					$confirm = 'N';
					$status = 2;

					//取消訂單狀態
					$cencelorder = $mall->set_order($arr['userid'], $arr['txid'], $switch, $confirm, $status);
					error_log("[c/mall/extExchangeResult] Cancel order :".json_encode($cencelorder));

					//取消扣除點數
					$cenceldeposit = $mall->set_bonus($arr['userid'], $history['bonusid'], $switch);
					error_log("[c/mall/extExchangeResult] Cancel bonusid :".json_encode($history['bonusid']));

				}

				$msg_url = BASE_URL.APP_DIR."/mall/extExchangeMsg";
				error_log("[3]redirect to :".$msg_url);
				$arr['type'] = "ofpay";
				$arr['json'] = $json;
				redirect_post($msg_url, $arr);
				exit;

			}else{
				error_log("[c/mall/extExchangeResult] history error :".json_encode($history));
				$ret = getRetJSONArray(-6,urlencode('查無相關紀錄!!'),'MSG');
			}
		}
		if($ret['retCode'] < 0 || $ret['retCode']===0){
			$ret['type'] = $type;
			$ret['json'] = $json;
			$url = BASE_URL.APP_DIR."/mall/extExchangeMsg";
			redirect_post($url, $ret);
		}

		exit;

	}


	/*
	 *	ofpay回覆功能
	 *	$retCode 		varchar		回覆編碼
	 *	$retMsg			varchar		回覆訊息
	 *	$vendor			varchar		ofpay
	 *	$userid			int			會員編號
	 *	$txid			int			saja端交易識別編號(OrderID)
	 *	$vndr_txid		int			ofpay端交易識別編號(UID)
	 *	$sign			varchar		ofpay格式的簽名
	 */
	public function extExchangeResult() {

		global $config, $tpl, $mall, $user;

		$retCode	= empty($_POST['retCode']) ? $_GET['retCode'] : $_POST['retCode'];
		$retMsg		= empty($_POST['retMsg']) ? $_GET['retMsg'] : $_POST['retMsg'];
		$vendor		= empty($_POST['vendor']) ? $_GET['vendor'] : $_POST['vendor'];
		$userid		= empty($_POST['userid']) ? $_GET['userid'] : $_POST['userid'];
		$txid		= empty($_POST['txid']) ? $_GET['txid'] : $_POST['txid'];
		$vndr_txid	= empty($_POST['vndr_txid']) ? $_GET['vndr_txid'] : $_POST['vndr_txid'];
		$sign		= empty($_POST['sign']) ? $_GET['sign'] : $_POST['sign'];

		//判斷回覆資料是否正確
		if((!empty($retCode)) && (!empty($retMsg)) && (!empty($vendor)) && (!empty($txid)) && (!empty($vndr_txid)) && (!empty($sign))){

			$uuid = $userid."|".$txid;
			$time = date("YmdHi");
			$localsign = md5($uuid.md5($time."osfapjaay"));

			//判斷簽名是否正確
			if($sign == $localsign){

				//查詢紅利點數歷史資料
				$history = $mall->get_exchange_bonus_history($txid, $userid);

				//判斷紅利點數歷史資料是否存在
				if(!empty($history['ebhid'])){

					//判斷回覆結果調整訂單
					if($retCode == 1){

						$switch = 'Y';
						$confirm = 'Y';
						$status = 1;

						//更改訂單狀態
						$orderid = $mall->set_order($userid, $txid, $switch, $confirm, $status);
						error_log("[c/mall/extExchangeResult] order :".json_encode($orderid));

					}else{

						$switch = 'N';
						$confirm = 'N';
						$status = 2;

						//取消訂單狀態
						$cencelorder = $mall->set_order($userid, $txid, $switch, $confirm, $status);
						error_log("[c/mall/extExchangeResult] order :".json_encode($cencelorder));

						//取消扣除點數
						$cenceldeposit = $mall->set_bonus($userid, $history['bonusid'], $switch);
						error_log("[c/mall/extExchangeResult] bonusid :".json_encode($history['bonusid']));

					}

					$ret = getRetJSONArray(1,'OK','JSON');
					$ret['vendor'] = "ofpay";
					$ret['sign'] = $localsign;
					$ret['txid'] = $txid;
					$ret['vndr_txid'] = $vndr_txid;

				}else{
					error_log("[c/mall/extExchangeResult] history error :".json_encode($history));
					$ret = getRetJSONArray(-6,urlencode('查無相關紀錄!!'),'MSG');
				}

			}else{
				error_log("[c/mall/extExchangeResult] sign error :".$sign);
				$ret = getRetJSONArray(-2,urlencode('事務數據驗證錯誤!!'),'MSG');
			}

		}else{
			error_log("[c/mall/extExchangeResult] postdata error :".json_encode($_POST));
			$ret = getRetJSONArray(-1,urlencode('查無事務數據!!'),'MSG');
		}

		echo json_encode($ret);

		exit;

	}


	/*
	 *	ofpay回覆異常功能
	 *	$retCode 		varchar		回覆編碼
	 *	$retMsg			varchar		回覆訊息
	 *	$type			varchar		ofpay
	 */
	public function extExchangeMsg() {

		global $config, $tpl, $mall, $user;

		$retCode	= empty($_POST['retCode']) ? $_GET['retCode'] : $_POST['retCode'];
		$retMsg		= empty($_POST['retMsg']) ? $_GET['retMsg'] : $_POST['retMsg'];
		$type		= empty($_POST['type']) ? $_GET['type'] : $_POST['type'];
		$json		= empty($_POST['json']) ? $_GET['json'] : $_POST['json'];

		//判斷回覆資料是否正確
		if((!empty($retCode)) && (!empty($type))){

			switch($retCode){
				case 1: // 交易成功, 數據更新成功
      				   $retMsg="交易成功";
      				   break;
				case -1: // Saja的uuid異常
      				   $retMsg="用戶不存在!!";
      				   break;
				case -2: // Sign格式驗證錯誤
      				   $retMsg="簽名驗證錯誤 !!";
      				   break;
				case -3: // Saja的兌換密碼錯誤
      				   $retMsg="兌換密碼不正確 !!";
      				   break;
				case -4: // 紅利點數不夠
      				   $retMsg="鯊魚點數不足 !!";
      				   break;
				case -5: // 沒有編號txid(或vndr_txid)的資料
      				   $retMsg="查無事務數據 !!";
      				   break;
				default: // 回覆編碼異常
      				   $retMsg=$retMsg;
      				   if(empty($retMsg)){
      					   $retMsg = "回傳事務數據遺失!!!";
      				   }
      				   break;
			}
		}elseif((empty($retCode)) && (!empty($retMsg)) && (!empty($type))){
			error_log("[c/mall/extExchangeMsg] postdata error :".json_encode($_POST));
			$retCode = -7;
			$retMsg = $retMsg;
		}else{
			error_log("[c/mall/extExchangeMsg] postdata error :".json_encode($_POST));
			$retCode = -6;
			$retMsg = "回傳事務數據遺失!!!";
		}

		$tpl->assign('retCode',$retCode);
		$tpl->assign('retMsg',$retMsg);
		$tpl->assign('type','ofpay');
		$tpl->assign('json',$json);
		$tpl->set_title('');

		$tpl->render("mall","exchangemsg",true, '', 'Y');

	}


	/*
	 *	ofpay回覆異常功能
	 *	$retCode 		varchar		回覆編碼
	 *	$retMsg			varchar		回覆訊息
	 *	$type			varchar		ofpay
	 */
	public function refund() {

		global $config, $tpl, $mall, $user;

		$type			= empty($_POST['type']) ? $_GET['type'] : $_POST['type'];
		$txid			= empty($_POST['txid']) ? $_GET['txid'] : $_POST['txid'];
		$vndr_txid		= empty($_POST['vndr_txid']) ? $_GET['vndr_txid'] : $_POST['vndr_txid'];
		$bonus_refund	= empty($_POST['bonus_refund']) ? $_GET['bonus_refund'] : $_POST['bonus_refund'];
		$sign			= empty($_POST['sign']) ? $_GET['sign'] : $_POST['sign'];

		error_log("[c/mall/refund] :"."type=".$type."&txid=".$txid."&vndr_txid=".$vndr_txid."&bonus_refund=".$bonus_refund);

		//判斷回覆資料是否正確
		if((!empty($txid)) && (!empty($type)) && (!empty($vndr_txid)) && (!empty($bonus_refund)) && (!empty($sign))){

			$localsign = md5("type=".$type."&txid=".$txid."&vndr_txid=".$vndr_txid."&bonus_refund=".$bonus_refund."&key=osfapjaay");

			//判斷簽名是否正確
			if($sign == $localsign){

				//查詢訂單歷史資料
				$order = $mall->check_order($txid);

				//判斷紅利點數歷史資料是否存在
				if((!empty($order['orderid'])) && ($bonus_refund == $order['total_fee'])){

					//判斷訂單狀態是否符合退款條件
					if($order['status'] == 0 || $order['status'] == 3){

						$switch = 'Y';
						$confirm = 'Y';
						$status = 2;
						$tx_num = $order['num'];
						$total_bonus = $order['total_fee'];

						//取消訂單狀態
						$cencelorder = $mall->set_order($order['userid'], $txid, $switch, $confirm, $status);
						error_log("[c/mall/refund] orderid :".$txid);

						//兌換紅利計算
						$bonusdata['amount'] = -$total_bonus;
						$bonusdata['orderid'] = $txid;

						//扣除點數
						$bonusid = $mall->add_bonus($bonusdata, $order['userid']);
						error_log("[c/mall/refund] bonusid :".json_encode($bonusid));

						//新增紅利點數歷史資料
						$history = $mall->add_exchange_bonus_history($bonusdata, $order['userid'], $bonusid);
						error_log("[c/mall/refund] history :".json_encode($history));

						$ret = getRetJSONArray(1,'OK','JSON');

					}else{
						error_log("[c/mall/refund] history error :".json_encode($history));
						$ret = getRetJSONArray(-7,urlencode('訂單狀態不符合退款條件!!'),'MSG');
					}

				}else{
					error_log("[c/mall/refund] history error :".json_encode($history));
					$ret = getRetJSONArray(-6,urlencode('查無交易紀錄!!'),'MSG');
				}

			}else{
				error_log("[c/mall/refund] sign error :".$sign);
				$ret = getRetJSONArray(-2,urlencode('事務數據驗證錯誤!!'),'MSG');
			}

		}else{
			error_log("[c/mall/refund] postdata error :".json_encode($_POST));
			$ret = getRetJSONArray(-6,urlencode('回傳事務數據遺失!!'),'MSG');

		}

		$ret['type'] = $type;
		$ret['txid'] = $txid;
		$ret['vndr_txid'] = $vndr_txid;

		echo json_encode($ret);

		exit;

	}


	/*
	 *	跳轉ofpay服務
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$epid	 	int			商品編號
	 *  Update by Thomas 20161202 測試中
	*/
	public function extExchangeService() {

		global $config, $tpl, $mall, $user, $member;

		//設定 Action 相關參數
		set_status($this->controller);

		error_log("[c/mall/extExchangeService] POST DATA : ".json_encode($_POST));
		error_log("[c/mall/extExchangeService] GET DATA : ".json_encode($_GET));

		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$epid 	= empty($_POST['epid']) ? htmlspecialchars($_GET['epid']) : htmlspecialchars($_POST['epid']);
		$userid = empty($_POST['auth_id']) ? ($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		$method = "post";
		$user_phone  = "";
		$user_profile = "";

		if(!empty($userid)){
		   $arrCond=array("userid"=>$userid,
		                  "verified"=>"Y");
		   // 商家的saja_user.name不是數字
		   // 所以手機驗證要看saja_user.saja_user_sms_auth的phone欄位
		   $sms_auth=$user->getSmsAuthData($arrCond);
		   if($sms_auth && $sms_auth[0]['verified']=='Y' && is_numeric($sms_auth[0]['phone'])){
			   $user_phone = $sms_auth[0]['phone'];
			   $user_profile=$user->get_user_profile($userid);
		   }else{
			   // 未通過手機驗證 彈回 不可使用本服務
			   $ret = getRetJSONArray(-1,urlencode('請先進行手機號驗證 !'),'MSG');
			   echo json_encode($ret);
			   exit;
		   }
		}

		if($json!='Y')
		  login_required();

		//取得商品資料
		$ecproduct = $mall->get_info($epid);

		//判斷商品資料是否存在
		if((!empty($ecproduct['epid'])) && (!empty($ecproduct['tx_url']))){

			if($ecproduct['epid']=='592'){
			  // uptogo
			  $uuid = $user_phone."|".$epid;
        $s1=date("YmdHi")."uspatjoago";

        if($ecproduct['pass_sms_auth'] == 'Y' && $sms_auth[0]['verified']=='N'){
          echo "<script>alert('您的手機號尚未驗證 !!');location.href = '".BASE_URL.APP_DIR."/member/';</script>";
          exit;
        }

			}else{
			  // ofpay
			  $uuid = $userid."|nqc".$epid;
			  $s1=date("YmdHi")."sjw"."osfapjaay";
			}
			error_log("[c/mall/extExchangeService] uuid :".$uuid.", salt:".$s1);

			if($ecproduct['epid']=='592') {
			   $sign = md5($uuid."|".md5($s1));
			} else {
			   $sign = md5($uuid.md5($s1));
			   $data = array(
					'uuid'		=> $uuid,
					'sign'		=> $sign,
					'menu'		=> $ecproduct['tx_url']
				);
			}
			error_log("[c/mall/extExchangeService] sign :".$sign);

			$url = $ecproduct['tx_url'];
			$str_arr = getInbetweenStrings('{', '}', $ecproduct['tx_url']);

			foreach($str_arr as $key => $value){
				switch($value){
					case "uuid": 		//會員編號&商品編號
            					   $url = str_replace("{uuid}",$uuid,$url);
            					   break;
					case "sign": 		//ofpay格式的簽名
            					   $url = str_replace("{sign}",$sign,$url);
            					   break;
					case "userid":		//會員編號
            					   $url = str_replace("{userid}",$userid,$url);
            					   break;
					case "gender":		//會員性別
            					   $url = str_replace("{gender}",$user_profile['gender'],$url);
            					   break;
					case "nickname":		//會員暱稱
              					   $url = str_replace("{nickname}",$user_profile['nickname'],$url);
              					   break;
				}
			}

			// 特定網址限制form的method類型
			if(!empty($ecproduct["method_type"]))
			   $method=$ecproduct["method_type"];

			error_log("[c/mall/extExchangeService] method_type : ".$method);

			$post_url="";
			if(strtolower($method)=="post"){
			  $arrURI=parse_url($url);
        if(is_array($arrURI)){
          $post_url="http://".$arrURI['host'].$arrURI["path"];
				  $query_str=$arrURI["query"];
				  if(!empty($query_str)){
				    parse_str($query_str, $arrPost);
				  }
			  }
			}

      if($json != 'Y'){
      	$tpl->assign('url',$url);
      	$tpl->set_title('');
      	$tpl->render("mall","iframe",true);
      }else{
      	$ret = getRetJSONArray(1,'OK','MSG');
      	$ret['retObj']['url'] = (!empty($url)) ? $url : array();
      	echo json_encode($ret,JSON_UNESCAPED_SLASHES);
      	exit;
      }

		}else{
			error_log("[c/mall/extExchangeService] epid error :".$epid);
			$ret = getRetJSONArray(-1,urlencode('商品資料有誤!!'),'MSG');
			echo json_encode($ret);
			exit;

		}

	}


	/*
	 *	鯊魚點兌換殺價幣
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$amount	 	int			兌換金額
	 */
	public function exchangeBonusToSpoint() {

		global $db, $config, $tpl, $mall, $member, $user;

		login_required();

    //設定 Action 相關參數
		set_status($this->controller);

		$json 	= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$amount = empty($_POST['amount'])?$_GET['amount']:$_POST['amount'];
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		if($amount >= 100){

			//取得使用者鯊魚點數資料
			$get_spoint = $member->get_bonus($userid);

			//判斷鯊魚點數餘額是否足夠使用
			if((int)$get_spoint['amount'] >= (int)$total_bonus){

				$userprofile = $user->get_user_profile($userid);

				//組合兌換資料
				$productArr['esid'] = 1;
				$productArr['epid'] = 581;
				$productArr['point_price'] = $amount;
				$productArr['process_fee'] = $amount*0.1;
				$productArr['memo'] = '鯊魚點數兌換殺價幣：'.$total_bonus.'點';
				$productArr['tx_data'] = '';

				//新增訂單
				$orderid = $mall->add_order($productArr, $userid);
				error_log("[c/mall/exchangeBonusToSpoint] orderid :".$orderid);

				//新增訂單收件人
				$consignee = $mall->add_order_consignee($orderid, $userprofile);
				error_log("[c/mall/exchangeBonusToSpoint] consignee :".json_encode($consignee));

				//兌換紅利計算
				$bonusdata['amount'] = ($amount*1.1);
				$bonusdata['orderid'] = $orderid;

				//扣除點數
				$bonusid = $mall->add_bonus($bonusdata, $userid);
				error_log("[c/mall/exchangeBonusToSpoint] bonusid :".json_encode($bonusid));

				//新增紅利點數歷史資料
				$history = $mall->add_exchange_bonus_history($bonusdata, $userid, $bonusid);
				error_log("[c/mall/exchangeBonusToSpoint] history :".json_encode($history));

				//增加殺價幣
				$spoint = $mall->exchange_bonus($userid, $amount, 'TWD');
				error_log("[c/mall/exchangeBonusToSpoint] spoint :".json_encode($spoint));

				$retArr['retCode'] = 1;
				$retArr['retMsg'] = urlencode('兌換殺價幣成功 !!');

			}else{
				error_log("[c/mall/exchangeBonusToSpoint] spoint error :".$get_spoint['amount']);
				$retArr['retCode'] = -3;
				$retArr['retMsg'] = urlencode('鯊魚點數不足 !!');
			}
		}else{
			error_log("[c/mall/exchangeBonusToSpoint] spoint error :".$get_spoint['amount']);
			$retArr['retCode'] = -2;
			$retArr['retMsg'] = urlencode('兌換殺價幣小於100 !!');
		}

		if($json == 'Y'){
			$ret = getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
			echo json_encode($ret);
			exit;
		}else{
			if($retArr['retCode'] == 1){
				echo '<!DOCTYPE html><html><body><script>alert("兌換殺價幣成功!");window.location = "/site/history/"</script></body></html>';
			}else{
				$retArr['type'] = "saja";
				$retArr['json'] = $json;
				$url = BASE_URL.APP_DIR."/mall/extExchangeMsg";
				redirect_post($url, $retArr);
				exit;
			}
		}

	}


	/*
	 *	兌換商品3級分潤
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$epid		int			商品編號
	 *	$userid		int			會員編號
	 *	$orderid	int			訂單編號
	 *	$amount	 	int			利潤金額
	 *	$esid		int			商家編號
	 */
	public function giftDistributionProfits() {

		global $db, $config, $tpl, $mall, $user, $rule;

		//設定 Action 相關參數
		set_status($this->controller);

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$epid 		= empty($_POST['epid'])?$_GET['epid']:$_POST['epid'];
		$amount 	= empty($_POST['amount'])?$_GET['amount']:$_POST['amount'];
		$orderid 	= empty($_POST['orderid'])?$_GET['orderid']:$_POST['orderid'];
		$userid 	= empty($_SESSION['auth_id'])?$_POST['userid']:$_SESSION['auth_id'];
		$esid 		= empty($_POST['esid'])?$_GET['esid']:$_POST['esid'];


		$leveldata = $rule->get_profit_sharing_rule_item();
		error_log("[c/mall/giftDistributionProfits] leveldata :".json_encode($leveldata));

		$orderdata = $mall->check_order($orderid);
		error_log("[c/mall/giftDistributionProfits] orderdata :".json_encode($orderdata));

		$productdata = $mall->get_info($epid);
		error_log("[c/mall/giftDistributionProfits] productdata :".json_encode($productdata));

		if(empty($esid) && $epid != 0){
			$esid = $productdata['esid'];
		}

		$storedata = $mall->get_store($esid);
		error_log("[c/mall/giftDistributionProfits] storedata :".json_encode($storedata));

		$data['orderid'] = $orderid;
		$data['epid'] = $epid;

		try{
			//判斷分潤金額是否大於0
			if(($amount > 0) && (!empty($orderid)) && (!empty($userid))){

				$sixty_percent = $amount;

				if(!empty($leveldata[0]['ratio'])){
					//取得1級推薦人分潤
					$level1 = $user->getAffiliateUser($userid);

					if(!empty($level1['intro_by'])){
						$total = round($sixty_percent*($leveldata[0]['ratio']/100),4);
						//寫入推薦人分潤
						$spid = $mall->add_distribution_profits($level1['intro_by'], $total, 'feedback', $esid);
						error_log("[c/mall/giftDistributionProfits] spid :".$spid);

						//取得推薦人的微信openid
						$openid = $user->getUserWeixinOpenID($level1['intro_by']);

						//判斷推薦人的微信openid是否存在
						if(!empty($openid['uid'])){
							$msg = '推薦人回饋送 殺價幣：'.$total.' 點!!';
							$ret = sendWeixinMsg($openid['uid'], $msg, 'text');
						}

					}else{
						$total = 0;
					}
					//step1 第一級上層分潤
					$data['pscid'] = $leveldata[0]['pscid'];
					$data['profit'] = $total;
					$rule->insert_Profit_Sharing($data);
				}

				if(!empty($leveldata[1]['ratio'])){
					//取得2級推薦人分潤
					$level2 = $user->getAffiliateUser($level1['intro_by']);

					if(!empty($level2['intro_by'])){
						$total2 = round($sixty_percent*($leveldata[1]['ratio']/100),4);
						//寫入推薦人分潤
						$spid2 = $mall->add_distribution_profits($level2['intro_by'], $total2, 'feedback', $esid);
						error_log("[c/mall/giftDistributionProfits] spid2 :".$spid2);

						//取得推薦人的微信openid
						$openid2 = $user->getUserWeixinOpenID($level2['intro_by']);

						//判斷推薦人的微信openid是否存在
						if(!empty($openid2['uid'])){
							$msg = '推薦人回饋送 殺價幣：'.$total2.' 點!!';
							$ret = sendWeixinMsg($openid2['uid'], $msg, 'text');
						}

					}else{
						$total2 = 0;
					}
					//step2 第二級上層分潤
					$data['pscid'] = $leveldata[1]['pscid'];
					$data['profit'] = $total2;
					$rule->insert_Profit_Sharing($data);
			  }

				if(!empty($leveldata[2]['ratio'])){
					//取得業務資料
					$sales = $mall->get_introducer($storedata['enterpriseid']);

					if(!empty($sales['userid'])){
						$total3 = round($sixty_percent*($leveldata[2]['ratio']/100),4);
						//寫入業務分潤
						$spid3 = $mall->add_distribution_profits($sales['userid'], $total3, 'feedback', $esid);
						error_log("[c/mall/giftDistributionProfits] spid3 :".$spid3);

						//取得業務的微信openid
						$openid3 = $user->getUserWeixinOpenID($sales['userid']);

						//判斷業務的微信openid是否存在
						if(!empty($openid3['uid'])){
							$msg = '業務分潤 殺價幣：'.$total3.' 點!!';
							$ret = sendWeixinMsg($openid3['uid'], $msg, 'text');
						}

					}else{
						$total3 = 0;
					}
					//step3 業務分潤
					$data['pscid'] = $leveldata[2]['pscid'];
					$data['profit'] = $total3;
					$rule->insert_Profit_Sharing($data);
				}

				if(!empty($leveldata[3]['ratio'])){
					//step4 系統平臺分潤
					$data['pscid'] = $leveldata[3]['pscid'];
					$data['profit'] = $amount-($total+$total2+$total3);
					$rule->insert_Profit_Sharing($data);

					$msg = '系統平臺分潤 殺價幣：'.$data['profit'].' 點!!';
					$ret = sendWeixinMsg('oNmoZtxnMZIvr-gWe7A_JUGLt_Vs', $msg, 'text');
				}
				$retArr['retCode'] = 1;
				$retArr['retMsg'] = urlencode('分潤完成 !!');

			}else{
				error_log("[c/mall/giftDistributionProfits] data error :".json_encode($_POST));
				$retArr['retCode'] = '';
				$retArr['retMsg'] = urlencode('分潤資料異常 !!');
			}
		}catch (Exception $e){
			$retArr['retCode']=$e->getCode();
			$retArr['retMsg']=$e->getMessage();
		}

		$ret = getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
		echo json_encode($ret);

		exit;

	}


	/*
	 *	鯊魚點兌換
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$esid	 	int			店家編號
	 */
	public function topay() {

		global $db, $config, $tpl, $mall, $user, $member, $enterprise;

		//設定 Action 相關參數
		login_required();
		set_status($this->controller);

		$json 	= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$esid 	= empty($_POST['esid'])?$_GET['esid']:$_POST['esid'];
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		if(!empty($esid)){
			//取得店家資料
			$storeprofile = $mall->get_store($esid);
			error_log("[c/mall/topay] store profile :".json_encode($storeprofile));

			//取得企業資料
			$enterpriseprofile = $enterprise->getEnterpriseDetail($storeprofile['enterpriseid']);
			error_log("[c/mall/topay] enterprise profile :".json_encode($enterpriseprofile));

			//取得使用者鯊魚點餘額
			$userspointdata = $member->get_bonus($userid);
			error_log("[c/mall/topay] user spoint :".json_encode($userspointdata['amount']));

			//取得使用者免密額度
			$userdata = $user->get_user_profile($userid);
			error_log("[c/mall/topay] user spoint :".json_encode($userdata['bonus_noexpw']));
		}else{
			$retArr['retCode'] = '';
			$retArr['retMsg'] = 'QRcode異常，請重新掃描!!';
			$retArr['type'] = "saja";
			$retArr['json'] = $json;
			$url = BASE_URL.APP_DIR."/mall/extExchangeMsg";
			redirect_post($url, $retArr);
			exit;
		}

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['enterpriseprofile'] = $enterpriseprofile;
			$ret['retObj']['spoint'] = $userspointdata['amount'];
			$ret['retObj']['userprofile'] = $userdata;
			$ret['retObj']['json'] = $json;
			$ret['retObj']['esid'] = $esid;
			$ret['retObj']['userid'] = $userid;
		  echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('enterpriseprofile',$enterpriseprofile);
			$tpl->assign('spoint',$userspointdata['amount']);
			$tpl->assign('userprofile',$userdata);
			$tpl->assign('json',$json);
			$tpl->assign('esid',$esid);
			$tpl->assign('userid',$userid);
			$tpl->assign('tpfooter',1);
			$tpl->set_title('');
			$tpl->render("mall","topay",true);
		}

	}


	/*
	 *	鯊魚點消費確認
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$amount	 	int			兌換金額
	 *	$esid	 	int			店家編號
	 *	$expasswd	int			兌換密碼
	 */
	public function topayBonus() {

		global $db, $config, $tpl, $mall, $member, $user, $enterprise;

		//設定 Action 相關參數
		login_required();
		set_status($this->controller);

		$json 			= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$total 			= empty($_POST['total'])?$_GET['total']:$_POST['total'];
		$userid 		= empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$esid 			= empty($_POST['esid'])?$_GET['esid']:$_POST['esid'];
		$spoint 		= empty($_POST['spoint'])?$_GET['spoint']:$_POST['spoint'];
		$expw 		= empty($_POST['expw'])?$_GET['expw']:$_POST['expw'];

		error_log("[c/mall/topayBonus] POST data :".json_encode($_POST));

		//取得使用者鯊魚點數資料
		$get_spoint = $member->get_bonus($userid);

		//取得店家資料
		$storeprofile = $mall->get_store($esid);
		error_log("[c/mall/topayBonus] store profile :".json_encode($storeprofile));

		//取得企業資料
		$enterpriseprofile = $enterprise->getEnterpriseDetail($storeprofile['enterpriseid']);
		error_log("[c/mall/topayBonus] enterprise profile :".json_encode($enterpriseprofile));

		//判斷鯊魚點數餘額是否足夠使用
		if((int)$get_spoint['amount'] >= (int)$total){

			$userprofile = $user->get_user_profile($userid);

			//取得兌換密碼
			$get_user = $user->get_user($userid);

			$this->str = new convertString();
			$exchangepasswd = $this->str->strEncode($expw, $config['encode_key']);
			error_log("[c/mall/topayBonus] expasswd :".($get_user['exchangepasswd']));

			if($total > $userprofile['bonus_noexpw']){

				if($exchangepasswd !== $get_user['exchangepasswd']){
					$ret = getRetJSONArray(-2,'兌換密碼錯誤 !!','MSG');
					$ret['status']=-2;
					$ret['kind']='兌換密碼錯誤!!';
					echo json_encode($ret);
					exit;
				}

				error_log("[c/mall/topayBonus] exchangepasswd :".($get_user['exchangepasswd']));

			}

			//組合兌換資料
			$productArr['esid'] = $esid;
			$productArr['epid'] = 0;
			$productArr['point_price'] = $total;
			$productArr['process_fee'] = 0;
			$productArr['memo'] = '鯊魚點支付：'.$total.'點';
			$productArr['tx_data'] = '';
			$productArr['confirm'] = 'Y';

			//新增訂單
			$orderid = $mall->add_order($productArr, $userid);
			error_log("[c/mall/topayBonus] orderid :".$orderid);

			//新增訂單收件人
			$consignee = $mall->add_order_consignee($orderid, $userprofile);
			error_log("[c/mall/topayBonus] consignee :".json_encode($consignee));

			//紅利計算
			$bonusdata['amount'] = $total;
			$bonusdata['orderid'] = $orderid;
			$bonusdata['behav'] = 'user_qrcode_tx';
			//扣除點數
			$bonusid = $mall->add_bonus($bonusdata, $userid);
			error_log("[c/mall/topayBonus] bonusid :".json_encode($bonusid));

			//新增紅利點數歷史資料
			$history = $mall->add_exchange_bonus_history($bonusdata, $userid, $bonusid);
			error_log("[c/mall/topayBonus] history :".json_encode($history));

			//商家增加點數
			$bsid = $mall->add_bonus_store($bonusid, $esid, $storeprofile['enterpriseid'], $bonusdata['amount'], $total*$enterpriseprofile['profit_ratio']/1000);
			error_log("[c/mall/topayBonus] bsid :".json_encode($bsid));

			//新增商家紅利點數歷史資料
			$store_history = $mall->exchange_bonus_store_history($bsid, $orderid, $esid, $$storeprofile['enterpriseid'], $bonusdata['amount'], $enterpriseprofile['profit_ratio']);
			error_log("[c/mall/topayBonus] store_history :".json_encode($store_history));

			$qc_ts = microtime(true);                                     // 1427439827.2345
			$checkcode = explode(".",$qc_ts);                              // 1427439827.2345, $checkcode[0]=1427439827, $checkcode[1]=2345
			$qc_time = date("YmdHis",$checkcode[0]);                         // 1427439827轉換成20150327150347
			$checkcode[1] = str_pad($checkcode[1],4,"0",STR_PAD_RIGHT);      // 如果不足四位  右方補0 ex: 234=> 2340
			$tx_code = $checkcode[1].".".$userid;
			$total_bonus = $total - ($total*$enterpriseprofile['profit_ratio']/1000);
			// 產生交易紀錄
			$sql = "INSERT INTO saja_exchange.saja_exchange_vendor_record
                      set prefixid='saja',
                          userid='{$userid}',
                          vendorid='{$storeprofile['enterpriseid']}',
                          vendor_name='{$storeprofile['name']}',
                          tx_code='{$tx_code}',
                          tx_status='4',
                          qrcode_time='{$qc_time}',
                          tx_quantity='1',
                          bonusid='{$bonusid}',
                          total_price='{$total}',
                          total_bonus='{$total_bonus}',
                          commit_time='{$qc_time}',
                          insertt='{$qc_time}' ";
			error_log($sql);
			$db->query($sql);
			$evrid=$db->_con->insert_id;

			$postdata['esid'] = $esid;
			$postdata['epid'] = 0;
			$postdata['amount'] = $total*$enterpriseprofile['profit_ratio']/1000;
			$postdata['orderid'] = $orderid;
			$postdata['userid'] = $userid;

			$url = BASE_URL.APP_DIR."/mall/giftDistributionProfits";
			//設定送出方式-POST
			$stream_options = array(
                      				'http' => array (
                                    						'method' => "POST",
                                    						'content' => json_encode($postdata),
                                    						'header' => "Content-Type:application/json"
                                    				  )
			);
			$context  = stream_context_create($stream_options);
			// 送出json內容並取回結果
			$response = file_get_contents($url, false, $context);
			// 讀取json內容
			$arr = json_decode($response,true);
			error_log("[c/mall/topayBonus] arr : ".json_encode($arr));

			if(!empty($enterpriseprofile['contact_mobile'])){
				//判斷手機格式長度
				if(strlen($enterpriseprofile['contact_mobile'])==10 && strpos($enterpriseprofile['contact_mobile'],"0")===0){
					$msg = iconv("UTF-8","big5"," 收到 ".$productArr['memo']);
				}else{
					$msg = iconv("UTF-8","gb2312"," 收到 ".$productArr['memo']);
				}
				sendSMS($enterpriseprofile['contact_mobile'], $msg);
			}

			$retArr['retCode'] = 1;
			$retArr['retMsg'] = urlencode('兌換鯊魚點數成功 !!');

		}else{
			error_log("[c/mall/topayBonus] spoint error :".$get_spoint['amount']);
			$retArr['retCode'] = -4;
			$retArr['retMsg'] = urlencode('鯊魚點數不足 !!');
		}

		if($json == 'Y'){
			$ret = getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
			echo json_encode($ret);
			exit;
		}else{
			if($retArr['retCode'] == 1){
				$ret = getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
				$ret['status']=1;
				$ret['kind']='兌換鯊魚點數成功!!';
				$ret['orderid']=$orderid;
				echo json_encode($ret);
				exit;
			}else{
				$ret = getRetJSONArray($retArr['retCode'],$retArr['retMsg'],'MSG');
				echo json_encode($ret);
				exit;
			}
		}

	}


	/*
	 *	鯊魚點支付完成
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$esid	 	int			店家編號
	 */
	public function topayend() {

		global $db, $config, $tpl, $mall, $user, $member, $enterprise;

		//設定 Action 相關參數
		login_required();
		set_status($this->controller);

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$esid 		= empty($_POST['esid'])?$_GET['esid']:$_POST['esid'];
		$orderid 	= empty($_POST['orderid'])?$_GET['orderid']:$_POST['orderid'];
		$userid 	= empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		//取得店家資料
		$storeprofile = $mall->get_store($esid);
		error_log("[c/mall/topayend] store profile :".json_encode($storeprofile));

		//取得企業資料
		$enterpriseprofile = $enterprise->getEnterpriseDetail($storeprofile['enterpriseid']);
		error_log("[c/mall/topayend] enterprise profile :".json_encode($enterpriseprofile));

		//取得使用者鯊魚點餘額
		$userspointdata = $member->get_bonus($userid);
		error_log("[c/mall/topayend] user spoint :".json_encode($userspointdata['amount']));

		//取得使用者免密額度
		$userdata = $user->get_user_profile($userid);
		error_log("[c/mall/topayend] user spoint :".json_encode($userdata['bonus_noexpw']));

		$orderdata = $mall->get_order($userid, $orderid);
		error_log("[c/mall/topayend] order data :".json_encode($orderdata));

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['enterpriseprofile'] = $enterpriseprofile;
			$ret['retObj']['order'] = $orderdata;
			$ret['retObj']['userprofile'] = $userdata;
			$ret['retObj']['json'] = $json;
			$ret['retObj']['esid'] = $esid;
			$ret['retObj']['userid'] = $userid;
		    echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('enterpriseprofile',$enterpriseprofile);
			$tpl->assign('order',$orderdata);
			$tpl->assign('userprofile',$userdata);
			$tpl->assign('json',$json);
			$tpl->assign('esid',$esid);
			$tpl->assign('userid',$userid);
			$tpl->set_title('');
			$tpl->render("mall","topayend",true);
		}

	}


	/*
	 *	輸入兌換密碼
	 *	$epcid	 			int				瀏覽工具判斷 (Y:APP來源)
	 *	$userid				int				會員編號
	 *	$epid	 			int				商品編號
	 *	$esid	 			int				店家編號
	 *	$total	 			int				總金額
	 *	$bonus	 			int				兌換單價
	 *	$num	 			int				兌換數量
	 *	$all_bonus	 		int				鯊魚點餘額
	 *	$handling	 		int				處理費
	 *	$name	 			varchar			收件人名稱
	 *	$zip	 			varchar			收件人郵遞編號
	 *	$address	 		varchar			收件人地址
	 */
	public function exchangeCode() {

		global $db, $config, $tpl, $mall, $user, $member;

		//設定 Action 相關參數
		// login_required();
		set_status($this->controller);

		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		$data = array(
            			'epcid'		=> empty($_POST['epcid'])?$_GET['epcid']:$_POST['epcid'],
            			'epid' 		=> empty($_POST['epid'])?$_GET['epid']:$_POST['epid'],
            			'esid' 		=> empty($_POST['esid'])?$_GET['esid']:$_POST['esid'],
            			'total' 	=> empty($_POST['total'])?$_GET['total']:$_POST['total'],
            			'bonus' 	=> empty($_POST['bonus'])?$_GET['bonus']:$_POST['bonus'],
            			'num' 		=> empty($_POST['num'])?$_GET['num']:$_POST['num'],
            			'all_bonus' => is_null($_POST['all_bonus'])?$_GET['all_bonus']:$_POST['all_bonus'],
            			'handling' 	=> is_null($_POST['handling'])?$_GET['handling']:$_POST['handling'],
            			'name' 		=> empty($_POST['name'])?$_GET['name']:$_POST['name'],
            			'zip' 		=> empty($_POST['zip'])?$_GET['zip']:$_POST['zip'],
            			'address' 	=> empty($_POST['address'])?$_GET['address']:$_POST['address']
		);
		if($data['all_bonus'] > 0 && $data['all_bonus'] > $data['total']){
			//判斷資料是否存在
			if((!empty($data['total'])) && (!empty($data['num'])) && (!empty($data['epid'])) && (!empty($data['all_bonus']))){
				if($json == 'Y'){
					$ret=getRetJSONArray(1,'OK','MSG');
					$ret['retObj']['data'] = $data;
					echo json_encode($ret);
				}else{
					$tpl->assign('data',$data);
					$tpl->set_title('');
					$tpl->render("mall","code",true);
				}
			}else{
				if ($json == 'Y'){
					$ret=getRetJSONArray(-2,'兌換資料有誤','MSG');
					echo json_encode($ret);
				}else{
					echo '<!DOCTYPE html><html><body><script>alert("兌換資料有誤!!");window.location = "/site/mall/exchange/?epcid='.$data['epcid'].'&epid='.$data['epid'].'"</script></body></html>';
				}
			}
		}else{
			if($json == 'Y'){
				$ret=getRetJSONArray(-1,'鯊魚點餘額不足','MSG');
				echo json_encode($ret);
			}else{
				echo '<!DOCTYPE html><html><body><script>alert("鯊魚點餘額不足!!");window.location = "/site/mall/exchange/?epcid='.$data['epcid'].'&epid='.$data['epid'].'"</script></body></html>';
			}
		}

	}


	/*
	 *	兌換商品活動說明頁
	 */
	public function active() {

    global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		$tpl->set_title('');

		if($json == 'Y'){
			$tpl->render("mall","active",true,'',$json);
		}else{
			$tpl->render("mall","active",true);
		}

	}


	/*
	 *	銀行機構清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 */
	public function banklist() {

		global $db, $config, $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_GET['json']:$_POST['json'];

		//取得店家資料
		$banklist = $mall->BankList();

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $banklist;
		    echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('banklist',$banklist);
			$tpl->set_title('');
			$tpl->render("mall","banklist",true);
		}
	}


	/*
	 *	銀行機構銷帳說明
	 *	$json	 				varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$type	 				int						支付類型 (1:信用卡,2:房貸,3:車貸,4:信貸,5:學貸,6:電信費,7:宅公益,8:停車費,9:水費,10:電費)
	 *	$bankid	 				int						銀行機構編號	 
	 */
	public function bankdetail() {

		global $db, $config, $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$type = empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$bankid = empty($_POST['bankid'])?$_GET['bankid']:$_POST['bankid'];
		
		if($json == 'Y'){
			//判斷銀行編號&支付類型參數是否正確
			if (!empty($bankid) && !empty($type)){
				
				switch($type){
					case "1": 		//信用卡
						$kind = 1;
						break;
					case "2": 		//房貸
						$kind = 2;
						break;
					case "3":		//車貸
						$kind = 3;
						break;
					case "4":		//信貸
						$kind = 4;
						break;
					case "5":		//學貸
						$kind = 5;
						break;
					default: // 
						$kind = 1;
						break;
				}
				
				//取得該銀行相關類型訊息
				$bankdata = $mall->BankData($bankid, $kind);
			
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $bankdata;
			} else {
				$ret=getRetJSONArray(10001,'回傳資料有誤','MSG');
			}
			
		    echo json_encode($ret);
			exit;
		}
	}


	/*
	 *	貸款分類清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 */
	public function loan() {

		global $db, $config, $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_GET['json']:$_POST['json'];

		//取得店家資料
		$loanlist = $mall->loan();

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $loanlist;
		    echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('loanlist',$loanlist);
			$tpl->set_title('');
			$tpl->render("mall","loanlist",true);
		}

	}


	/*
	 *	生活繳費共用資料清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 */
	public function lifepay() {

		global $db, $config, $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_GET['json']:$_POST['json'];
		
		//銀行機構清單
		$data['banklist'] = $mall->BankList();
		//取得貸款分類資料
		$data['loanlist'] = $mall->loan();
		//取得電信公司資料
		$data['telcomlist'] = $mall->telecompany();
		//取得電信繳費分類資料
		$data['telpaylist'] = $mall->telpaytype();
		//繳費截止日期
		$workday = array();
		for ($i=1; $i < 31; $i++) { 
			if ( date("w",strtotime('+'.$i.' day'))!=0 && date("w",strtotime('+'.$i.' day'))!=6) {
				$workday[] = date( "Y-m-d",strtotime('+'.$i.' day') );
			}
		}
		
		$tel_endtime = $workday[7];
		$credit_endtime = $workday[10];
		$loan_endtime = $workday[10];

		$data['tel_endtime'] = $tel_endtime;
		$data['credit_endtime'] = $credit_endtime;
		$data['loan_endtime'] = $loan_endtime;
		
		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $data;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('lifepay',$data);
			$tpl->set_title('');
			$tpl->render("mall","lifepay",true);
		}

	}
	
	
	/*
	 *	Qrcode轉跳頁
	 */
	public function qrPage() {

		global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		$tpl->set_title('');

		if($json == 'Y'){
			$tpl->render("mall","qrpage",true,'',$json);
		}else{
			$tpl->render("mall","qrpage",true);
		}

	}
	
	
	/*
	 *	生活繳費商品清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 */
	public function lifepayprod() {

		global $db, $config, $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_GET['json']:$_POST['json'];
		
		//生活繳費商品清單
		$data = $mall->lifepay_product_list();
		
		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $data;
		    echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('lifepayprod',$data);
			$tpl->set_title('');
			$tpl->render("mall","lifepayprod",true);
		}

	}	

	/*
	 *	首頁分類列表清單
	 */
	public function category_list() {

		global $tpl, $mall;
		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$channelid = empty($_POST['channelid']) ? "" : htmlspecialchars($_POST['channelid']);

		//商品分類
		$_category = $mall->product_category($channelid);

		//虛位以待
		$_syslist = $mall->get_sys();

		$description = (!empty($_syslist[0]['description']) ) ? $_syslist[0]['description'] : '';
		$_syslist[0]['description'] = html_decode($description);

		$use_type = 1;
		if($json=='Y'){
			$use_type = 2;
		}
		
		//取得2級分類
		foreach($_category as $tk => $tv){
			$_category[$tk]['product_list'] = array();
			$lvl = $mall->product_category_list(2, $tv['epcid']);
			if (!empty($lvl)){
				$_category[$tk]['level2_list'] = $lvl;
			}else{
				$_category[$tk]['level2_list'] = null;
			}
			if ($tv['epcid'] == "20") {
				$_category[$tk]['product_list'] = array( 
					array("offtime" => "1893743880",
						"epcid" => "20",
						"thumbnail" => "b6ad97f0c3efc3b06cc46f3fc0b44738.png",
						"epid" => "694",
						"name" => "ibon",
						"thumbnail_url" => "",
						"tx_url" => "https://www.saja.com.tw/site/mall/active/"
					)
				);
			}
		}
		
		// foreach($_category as $tk => $tv){
		// 	$cpl = $mall->product_list($use_type, $tv['epcid']);
		// 	$_category[$tk]['product_list'] = $cpl['table']['record'];
		// }
		
		

		$ret=getRetJSONArray(1,'OK','LIST');
		$ret['retObj']['data']['category_list']=$_category;
		$ret['retObj']['data']['sys_list']=$_syslist;
		echo json_encode($ret);
		exit;

	}
	
	
	/*
	 *	第三層分類列表清單
	 *	$json	 		varchar			瀏覽工具判斷 (Y:APP來源)
	 *	$epcid	 		int				第二層分類編號
	 */
	public function category_level3_list() {

		global $tpl, $mall;
		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$epcid = empty($_POST['epcid']) ? "" : htmlspecialchars($_POST['epcid']);
		
		if (!empty($epcid)){
			//取得3級分類
			$lvl = $mall->product_category_list(3, $epcid);
			if (!empty($lvl)){
				$_category = $lvl;
			}else{
				$_category = array();
			}
			
			$ret=getRetJSONArray(1,'取得資料','LIST');
			$ret['retObj']['data']['category_list'] = $_category;
		} else {
			$ret=getRetJSONArray(10001,'分類參數錯誤','MSG');
			$ret['retObj']['data']['category_list'] = array();
		}
		
		echo json_encode($ret);
		exit;

	}	

/*
	 *	第N層分類列表清單
	 *	$json	 		varchar			瀏覽工具判斷 (Y:APP來源)
	 *	$epcid	 		int				第二層分類編號
	 *	$layer	 		int				第n層
	 */
	public function category_levels_list() {

		global $tpl, $mall;
		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$epcid = empty($_POST['epcid']) ? "" : htmlspecialchars($_POST['epcid']);
		$layer = empty($_POST['layer']) ? "" : htmlspecialchars($_POST['layer']);
		
		if (!empty($epcid) || !empty($layer)){
			//取得3級分類
			$lvl = $mall->product_category_list($layer, $epcid);
			if (!empty($lvl)){
				$_category = $lvl;
			}else{
				$_category = array();
			}
			
			$ret=getRetJSONArray(1,'取得資料','LIST');
			$ret['retObj']['data']['category_list'] = $_category;
		} else {
			$ret=getRetJSONArray(10001,'分類參數錯誤','MSG');
			$ret['retObj']['data']['category_list'] = array();
		}
		
		echo json_encode($ret);
		exit;

	}	


	/*
	 *	第二三層商品列表清單
	 *	$json	 				varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$epcid	 				int						第二層分類編號
	 */
	public function category_product_list() {

		global $tpl, $mall;
		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$epcid = empty($_POST['epcid']) ? "" : htmlspecialchars($_POST['epcid']);
		
		//取得2級分類
		$lvl2 = $mall->get_category_data(2, $epcid);
		
		$use_type = 1;
		if($json=='Y'){
			$use_type = 2;
		}
		
		if (!empty($epcid)){
			$_category = $epcid;
			//取得3級分類
			$lvl3 = $mall->product_category_list(3, $epcid);
			//設定查詢分類資料
			if (!empty($lvl)){
				
				foreach($lvl3 as $tk => $tv){
					$_category .= ','.$tv['epcid'];
				}
			}
			
			if (!empty($_category)){
				//取得相關分類商品清單
				$cpl = $mall->category_product_list($use_type, $_category);
				$_product = $cpl['table']['record'];
				if(empty($_product)){
					$_product = array();
				}
			} else {
				$_product = array();
			}
			
			$ret=getRetJSONArray(1,'取得資料','LIST');
			$ret['retObj']['data']['product_list'] = $_product;
		} else {
			$ret=getRetJSONArray(10001,'分類參數錯誤','MSG');
			$ret['retObj']['data']['product_list'] = array();
		}

		echo json_encode($ret);
		exit;

	}

	/*
	 *  貸款類別對應可使用銀行
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 */
	public function lifepay_bank() {

		global $db, $config, $tpl, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_GET['json']:$_POST['json'];

		//取得店家資料
		$banklist = $mall->BankList();

		if(!empty($banklist)){

			$item1['loan'] = 1;
			$item1['loan_name'] = '信用卡'; 

			$item2['loan'] = 2;
			$item2['loan_name'] = '房貸'; 

			$item3['loan'] = 3;
			$item3['loan_name'] = '車貸'; 

			$item4['loan'] = 4;
			$item4['loan_name'] = '信貸';

			$item5['loan'] = 5;
			$item5['loan_name'] = '學貸';

			foreach($banklist as $row){
				if($row['bank_switch1'] == 'Y'){
					$item1['bank_list'][] = array('bankid'=>$row['bankid'], 'bankname'=>$row['bankname']);
				}
				if($row['bank_switch2'] == 'Y'){
					$item2['bank_list'][] = array('bankid'=>$row['bankid'], 'bankname'=>$row['bankname']);
				}
				if($row['bank_switch3'] == 'Y'){
					$item3['bank_list'][] = array('bankid'=>$row['bankid'], 'bankname'=>$row['bankname']);
				}
				if($row['bank_switch4'] == 'Y'){
					$item4['bank_list'][] = array('bankid'=>$row['bankid'], 'bankname'=>$row['bankname']);
				}
				if($row['bank_switch5'] == 'Y'){
					$item5['bank_list'][] = array('bankid'=>$row['bankid'], 'bankname'=>$row['bankname']);
				}
			}
		}

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = array($item1, $item2, $item3, $item4, $item5);
		    echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('banklist',$banklist);
			$tpl->set_title('');
			$tpl->render("mall","banklist",true);
		}
	}
}
