<?php
/*
 * App Controller 功能串接
 */

include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR ."/helpers.php");
include_once(LIB_DIR ."/router.php");
include_once(LIB_DIR ."/dbconnect.php");
include_once(LIB_DIR ."/ini.php");

class AppExe {

	public $controller = array();
	public $params = array();
	public $id;

	//回傳:
	public $ret = array(
		'retCode'=>1,
		'retMsg'=>"OK",
		'retType'=>"LIST",
		'action_list'=>array(
				"type"=>5,
				"page"=>9,
				"msg"=>"NULL"
			)
		);

	//串接入口
	public function home() {
		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$this->json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$this->auth_id = empty($_REQUEST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_REQUEST['auth_id']);
		$act_type = (empty($_POST['act_type'])) ? htmlspecialchars($_GET['act_type']) : htmlspecialchars($_POST['act_type']);
		$this->activity_type = intval($act_type);


		//會員驗證
		$ret_auth = $this->valid_auth();

		//資安驗證
		$ret_tk = $this->valid_tk();

		if($ret_auth['status'] !==1){
			$ret = $ret_auth;
		}
		else if($ret_tk['status'] !==1){
			$ret = $ret_tk;
		}
		else {
			//執行功能導入
			$ret = $this->act_case();
		}

		// 回傳值:
		echo json_encode($ret);
	}

	//執行功能導入
	public function act_case() {
		global $tpl, $db, $config, $user;

		$ret = $this->ret;
		$today = date("Y-m-d H:i:s");

        error_log("[c/app_exe] activity_type : ".$this->activity_type);
		//導入執行
		switch ($this->activity_type) {

			case 9821:
				//2020 數位紅包
				//載入lib
				include_once(LIB_DIR ."/user_referral.php");

				//紅包送S碼
				$user_ref = new user_referral();
				$ret = $user_ref->red_scode($this->auth_id);
				break;

			case 8:
				//推薦送
				$intro_by = (empty($_POST['intro_by'])) ? htmlspecialchars($_GET['intro_by']) : htmlspecialchars($_POST['intro_by']);

				//載入lib
				include_once(LIB_DIR ."/user_referral.php");

				//推薦會員處理
				$user_ref = new user_referral();

				if ( $today >= '2019-11-28 15:00:00' ) {
					//推薦送 S碼
					$ret = $user_ref->gift_scode($this->auth_id, $intro_by);
				} else {
					//停用 推薦送幣 $ret = $user_ref->gift_invite($this->auth_id, $intro_by);
				}
				break;
			case 9:
				$intro_by = (empty($_POST['intro_by'])) ? htmlspecialchars($_GET['intro_by']) : htmlspecialchars($_POST['intro_by']);
				if (empty($intro_by)){
					$ret['status'] = -4;
					$ret['action_list']["msg"]="缺少商家參數 !";
				}else{
					$user_profile = $user->get_user_profile($this->auth_id);
					if ($user_profile['area']==''){
						$qrcode=array();
						$qrcode['type']=5;
						$qrcode['page']=10;
						$ret['action_list']=$qrcode;
						$ret['action_list']["msg"]="請先填寫收件人資訊 !";
					}else{
						$qrcode=array();
						$qrcode['type']=4;
						$qrcode['productid']=$intro_by;
						$qrcode['page']=20;
						$ret['action_list']=$qrcode;
					}

				}
				break;

			/*
			case 20:
				//備用: 掃碼開通 超級殺價券
				$var['scsn'] = (empty($_POST['scsn'])) ? htmlspecialchars($_GET['scsn']) : htmlspecialchars($_POST['scsn']);
				$var['scpw'] = (empty($_POST['scpw'])) ? htmlspecialchars($_GET['scpw']) : htmlspecialchars($_POST['scpw']);

				//載入lib
				include_once(LIB_DIR ."/scode.php");

				//開通處理
				$scode = new scode_lib();
				$ret = $scode->act_scode($this->auth_id, $var);
				break;
			*/

			default:
				$ret['status'] = -3;
				$ret['action_list']["msg"]="活動類型錯誤 !";
				break;
		}

		return $ret;
	}

	public function valid_tk() {
		$this->ret['status'] = 1;

		if($_REQUEST['saja_id']=="dev"){
		} else {
			if (MD5($this->auth_id .'|'. $_REQUEST['ts']) !== $_REQUEST['tk']){

				//回傳:
				$this->ret['status'] = -2;
				$this->ret['action_list']["msg"]="驗證碼錯誤 !";
			}
		}

		return $this->ret;
	}

	public function valid_auth() {
		$this->ret['status'] = 1;

		if (empty($this->auth_id) || (! is_numeric($this->auth_id)) ){

			//回傳:
			$this->ret['status'] = -1;
			$this->ret['action_list']["page"]=9;
			$this->ret['action_list']["msg"]="會員資料錯誤, 請重新登入 !";
		}

		return $this->ret;
	}

}
