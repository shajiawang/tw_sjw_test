<?php
/*
 * History Controller 交易紀錄
 */

class History {

	public $controller = array();
	public $params = array();
	public $id;
	public $userid = '';

	public function __construct() {
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
	}


	/*
	*	殺價幣紀錄清單
	*	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	*	$userid				int					會員編號
	*	$kind				varchar				顯示型態 (all:全部殺幣, get:取得殺幣, use:使用殺幣)
	*	$p					int					分頁編號
	*/
	public function home() {

		global $tpl, $history, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if($json == 'Y'){
			//殺價幣清單加載
			$get_list = $history->spoint_list($userid, $kind);
		}

		//涷結殺價幣數量
		// $get_fspoint_list = $history->saja_list($userid, true);
		$frozenspoint['frozen'] = 0;
		$frozenspoint['amount'] = 0;
		$frozenspoint['userid'] = $userid;

		// if(is_array($get_fspoint_list)){
			// foreach($get_fspoint_list['table']['record'] as $rk => $rv){
				// if($rv['closed'] == 'N'){
					// $spoint = $history->getSpointById($rv['spointid']);
					// $frozenspoint['frozen'] += abs($spoint);
					// $frozenspoint['amount'] += $rv['saja_fee'] * $rv['count'];
				// }
			// }
		// }
		$tpl->assign('frozenspoint', $frozenspoint);

		//判斷來源
		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			//判斷清單是否存在
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					/*
					for($i=0; $i<count($get_list['table']['record']);++$i){
						// $status=$get_list['table']['record'][$i]['status'];
						// $get_list['table']['record'][$i]['note']=$config['deposit_status_desc'][$status];
						$behav=$get_list['table']['record'][$i]['behav'];
						$get_list['table']['record'][$i]['note']=$config['deposit_status_desc'][$behav];
					}
					*/
					for($i=0; $i<count($get_list['table']['record']);++$i){
						// $status=$get_list['table']['record'][$i]['status'];
						// $get_list['table']['record'][$i]['note']=$config['deposit_status_desc'][$status];
						$behav=$get_list['table']['record'][$i]['behav'];
						$act_type=$get_list['table']['record'][$i]['activity_type'];
						// error_log("[history/home] behav : ".$behav.", activity_type:".$act_type);
						if($behav=='gift') {
							switch($act_type) {
                                case '1':   //老殺友回娘家
								     $get_list['table']['record'][$i]['note']="老殺友回娘家"; 
								     break;
								case '4':   //被推薦註冊成功
								     $get_list['table']['record'][$i]['note']="被推薦註冊成功"; 
								     break;
								case '6':   //過手機驗證
								     $get_list['table']['record'][$i]['note']="完成手機驗證"; 
								     break;
								case '18':   //推薦送
								case '8':   //推薦送
            				         $get_list['table']['record'][$i]['note']="推薦:".$get_list['table']['record'][$i]['new_user_name']."(".$get_list['table']['record'][$i]['memo'].")";      	     			
							         break; 
							    default:
								     $get_list['table']['record'][$i]['note']=$config['deposit_status_desc'][$behav];
									 break;
							}
						} elseif ($behav=='dream_reward') {
							$get_list['table']['record'][$i]['note']="推薦獎勵";
						} elseif ($behav=='user_saja') {
							$pname = $history->product_list($get_list['table']['record'][$i]['spointid'],'');
							if (!empty($pname)) {
								$pname = mb_substr($pname,0,15,"UTF-8");;
							}
							$get_list['table']['record'][$i]['note'] .= $config['deposit_status_desc'][$behav]." - ".$pname."..."; 
						} else {
                            $get_list['table']['record'][$i]['note']=$config['deposit_status_desc'][$behav];
						}
						
						//金額重組
						$total = round($get_list['table']['record'][$i]['amount']);
						if ($total > 0){
							$get_list['table']['record'][$i]['amount'] = '+'.number_format($total);
						} else {
							$get_list['table']['record'][$i]['amount'] = number_format($total);
						}
					}
					$ret['retObj']['data'] = $get_list['table']['record'];
				}
				$ret['retObj']['frozenspoint'] = $frozenspoint;
				$ret['retObj']['page'] = $get_list['table']['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['frozenspoint'] = $frozenspoint;
				$ret['retObj']['page'] = $page;
			}
		  echo json_encode($ret);
		  exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","home",true);
		}

	}


	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

		$table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';
		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;

	}


	/*
	 *	殺價紀錄清單
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$userid				int					會員編號
	 *	$closed				varchar				商品結標狀態(Y:已結標, N:競標中, NB:流標, NP:凍結殺幣, W:已結標並中標者)
	 *	$root				varchar				呼叫來源(wexin:小程式)
	 *	$kind				varchar				顯示型態 (all:全部, onbid:競標中, getbid:已得標)
	 *	$p					int					分頁編號
	 */
	public function saja() {

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);

		//殺價紀錄列表
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$root = empty($_POST['root']) ? htmlspecialchars($_GET['root']) : htmlspecialchars($_POST['root']);
		$closed = empty($_POST['closed']) ? htmlspecialchars($_GET['closed']) : htmlspecialchars($_POST['closed']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		login_required();

		// BM表示包月  目前未規劃  之後再調整
		if($json=='Y' && $closed=='BM'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']=array();
			$ret['retObj']['data'] = array();
			$ret['retObj']['page'] = array();
			echo json_encode($ret);
			exit;
		}

		if(!empty($kind)){
			switch($kind){
				case "all": 	//全部
					$closed = "";
					break;
				case "onbid":	//競標中
					$closed = "N";
					break;
				case "getbid":	//已結標&&中標者
					$closed = "W";
					break;

			}
			
			//取得殺價紀錄清單
			$get_list = $history->saja_list($userid, false, $closed, $root);
			$tpl->assign('row_list', $get_list);			
			
		}


		//涷結殺幣數量
		$get_fspoint_list = $history->saja_list($userid, true);
		$frozenspoint['frozen'] = 0;
		$frozenspoint['amount'] = 0;
		$frozenspoint['userid'] = $userid;
		if(is_array($get_fspoint_list)){
			foreach($get_fspoint_list['table']['record'] as $rk => $rv){
				if($rv['closed'] == 'N'){
					$spoint = $history->getSpointById($rv['spointid']);
					$frozenspoint['frozen'] += abs($spoint);
					$frozenspoint['amount'] += $rv['saja_fee'] * $rv['count'];
				}
			}
		}
		$tpl->assign('frozenspoint', $frozenspoint);

        $num_bid_of_limited_prod = $history->get_num_bid_of_limited($userid,['7']);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			//判斷清單是否存在
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
				  foreach($get_list['table']['record'] as $k=>$v){
						if(empty($v['thumbnail_url']) && !empty($v['filename'])){
							$get_list['table']['record'][$k]['thumbnail_url']=BASE_URL.APP_DIR.'/images/site/product/'.$v['filename'];
						}

						if($v['closed'] == 'Y'){
							$get_list['table']['record'][$k]['txtc'] = '';
							if($v['final_winner_userid'] == $userid){
								$get_list['table']['record'][$k]['bid_mod'] = '恭喜得標';
								$get_list['table']['record'][$k]['bid_status'] ='saja-status-img2'.'.png';
								$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userBidProductList/?productid='. $v['productid'];
								$get_list['table']['record'][$k]['sajago'] = '';

								$ontime = $v['offtime']+60*60*24*3;
								$havtime = $ontime - time();
								$get_list['table']['record'][$k]['closetime'] = $havtime;
							}else{
								$get_list['table']['record'][$k]['bid_mod'] = '已結標';
								$get_list['table']['record'][$k]['bid_status'] ='saja-status-img3'.'.png';
								$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userBidProductList/?productid='. $v['productid'];
								$get_list['table']['record'][$k]['sajago'] = '';
								$get_list['table']['record'][$k]['closetime'] = '';
							}
						}elseif($v['closed'] == 'N'){
							$get_list['table']['record'][$k]['txtc'] = 'style="color:red;"';
							$get_list['table']['record'][$k]['bid_mod'] = '競標中';
							$get_list['table']['record'][$k]['bid_status'] ='saja-status-img1'.'.png';
							$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userOnBidProductList/?productid='. $v['productid'];
							$get_list['table']['record'][$k]['sajago'] = BASE_URL.APP_DIR .'/product/saja/?productid='. $v['productid'];
							$get_list['table']['record'][$k]['closetime'] = '';
						}else{
							$get_list['table']['record'][$k]['txtc'] = '';
							$get_list['table']['record'][$k]['bid_mod'] = '流標';
							$get_list['table']['record'][$k]['bid_status'] ='saja-status-img3'.'.png';
							$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userBidProductList/?productid='. $v['productid'].'&type=NB';
							$get_list['table']['record'][$k]['sajago'] = '';
							$get_list['table']['record'][$k]['closetime'] = '';
						}
				  }
				  $ret['retObj']['data'] = $get_list['table']['record'];
				}

				$ret['retObj']['frozenspoint'] = $frozenspoint;
				$ret['retObj']['page'] = $get_list['table']['page'];

			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['frozenspoint'] = $frozenspoint;
				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;

		}else{
			$tpl->set_title('');
			$tpl->render("history","saja",true);
		}

	}


	/*
	*	殺價紀錄細節
	*	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	*	$userid				int					會員編號
	*	$productid			int					商品編號
	*	$root				varchar				呼叫來源(wexin:小程式)
	*/
	public function saja_detail(){

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);

		$productid=empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
    $root = empty($_POST['root']) ? htmlspecialchars($_GET['root']) : htmlspecialchars($_POST['root']);

		if(empty($root)){
			login_required();
		}

		if(empty($productid)){
			return_to('site/history/saja');
		}else{
			//殺價商品資料
			$get_list = $history->saja_detail($productid, $userid);
			$tpl->assign('row_list', $get_list);

			//設定分頁
			if(empty($get_list['table']['page']) ){
				$page_content = array();
			}else{
				$page_path = $tpl->variables['status']['status']['path'] .'&productid='. $productid;
				$page_content = $this->set_page($get_list, $page_path);
			}
			$tpl->assign('page_content', $page_content);
		}

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
		  if($get_list){
		    $ret['retObj']['data'] = $get_list['table']['record'];
		  }
		  echo json_encode($ret);
		  exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","saja_detail",true);
		}

	}


    /*
	*	鯊魚點使用清單
	*	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	*	$kind				varchar				顯示型態 (get:取得點數, use:使用點數)
	*	$p					int					分頁編號
	*/
	public function bonus() {

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//紅利積點列表
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
        error_log("[c/history/bonus] GET : ".json_encode($_GET));
        error_log("[c/history/bonus] POST : ".json_encode($_POST));
		
		if($json=='Y'){
			if (!empty($_POST['auth_id'])){
			  $this->userid=$_POST['auth_id'];
				$get_list = $history->bonus_list($this->userid, $kind);
			}else{
				$this->userid=$_SESSION['auth_id'];
				$get_list = $history->bonus_list($this->userid, $kind);
			}
		}else{
			$this->userid=$_SESSION['auth_id'];
			// $get_list = $history->bonus_list($this->userid);
			$get_list = array();
		}

		if(is_array($get_list['table']['record'])){

			$inbonus = 0;
			$outbonus = 0;

			foreach($get_list['table']['record'] as $key => $bonusArr){
				//20191004 leo add to unmark error_log($key."==".json_encode($bonusArr));
				$bonus_product = $history->get_bonus_product($this->userid, $bonusArr);
				$get_list['table']['record'][$key]['name'] = empty($bonus_product['name']) ? '' : $bonus_product['name'];
				$get_list['table']['record'][$key]['ex_num']=$bonus_product['ex_num'];
				$note="";
				if (($get_list['table']['record'][$key]['behav']=='user_qrcode_tx')&& ($get_list['table']['record'][$key]['amount']>0)){
					$get_list['table']['record'][$key]['name']=$history->get_es_name($get_list['table']['record'][$key]['bonusid']); 
				}				
                switch($bonusArr['behav']){
				  case 'ibon_exchange':
                                $get_list['table']['record'][$key]['evrid']="i".$bonus_product['ibonexcid'];
                                $note='ibon:';
                                $type='less';
                                $outbonus = $outbonus+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'order_refund':
                                $note='退費:';
                                $type='add';
                                $inbonus = $inbonus+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'user_exchange':
                                $get_list['table']['record'][$key]['evrid']="e".$bonus_product['orderid'];
                                $note='兌換:';
                                $type='less';
                                $outbonus = $outbonus+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'product_close':
					            error_log("[gg] bonusArr.seq=".$bonusArr['seq']);
								if($bonusArr['seq']=="78") {
								   $note='得標獲點:'; 	
								} else {
								   $note='結標轉紅利:';	
								}
                                $type='add';
                                $inbonus = $inbonus+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'user_qrcode_tx':
                                $get_list['table']['record'][$key]['evrid']="q".$get_list['table']['record'][$key]['evrid'];
                                $note='';//'掃碼支付';
                                if($get_list['table']['record'][$key]['amount']>0) {
								   $note='退費:';   	
								}
								$type=($kind=='use')?'less':'add';
                                $outbonus = $outbonus+1;
        						switch($get_list['table']['record'][$key]['tx_status']){
        							case -2:
        							$get_list['table']['record'][$key]['tx_status']='[商家取消]';
        							break;
        							case -1:
        							$get_list['table']['record'][$key]['tx_status']='[user取消]';
        							break;
        							case 4:
        							$get_list['table']['record'][$key]['tx_status']='[交易完成]';
        							break;
        							default:
        							$get_list['table']['record'][$key]['tx_status']='';
        							break;
        						}                                  
                                break;
					case 'other_exchange':
					case 'other_system_exchange':
                                $get_list['table']['record'][$key]['evrid']="o".$get_list['table']['record'][$key]['evrid'];
                                $note='第三方兌換:';
								$type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'order_close':
                                $note='得標獲點:';
                                // $note=$bonus_product['name'];
								$type='add';
                                $inbonus = $inbonus+1;
                                break;
					default :
					            $note=$bonusArr['behav'];
        						if($bonusArr['amount'] > 0){
        							$type='add';
        							$note='鯊魚點收入';
        							$inbonus = $inbonus+1;
        						}else{
        							$type='less';
        							$note='鯊魚點支出';
        							$outbonus = $outbonus+1;
        						}
        						break;
				}
				/*
				switch($bonusArr['behav']){
				  case 'ibon_exchange':
                                $get_list['table']['record'][$key]['evrid']="i".$bonus_product['ibonexcid'];
                                $note='ibon 兌換';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'order_refund':
                                $note='缺貨退費';
                                $type='add';
                                $inbonus = $inbonus+1;
                                break;
					case 'user_exchange':
                                $get_list['table']['record'][$key]['evrid']="e".$bonus_product['orderid'];
                                $note='兌換商品';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'product_close':
                                $note='未得標贈鯊魚點';
                                $type='add';
                                $inbonus = $inbonus+1;
                                break;
					case 'user_qrcode_tx':
                                $get_list['table']['record'][$key]['evrid']="q".$get_list['table']['record'][$key]['evrid'];
                                $note='支付';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'other_system_exchange':
                                $get_list['table']['record'][$key]['evrid']="o".$get_list['table']['record'][$key]['evrid'];
                                $note='第三方使用兌換';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					default :
					          $note=$bonusArr['behav'];
        						if($bonusArr['amount'] > 0){
        							$type='add';
        							$note='鯊魚點收入';
        							$inbonus = $inbonus+1;
        						}else{
        							$type='less';
        							$note='鯊魚點支出';
        							$outbonus = $outbonus+1;
        						}
				}
				*/
				// 2019/11/13 By Thomas
				// APP 直接顯示note 所以要先兜好
				//if($json=='Y'){
			    if(!empty($get_list['table']['record'][$key]['name'])) {
				   $note.=str_replace(': '.abs($bonusArr['amount']),'',$get_list['table']['record'][$key]['name']);	
				   //$note.= $get_list['table']['record'][$key]['tx_status'];
				}	
               //}
				
				$get_list['table']['record'][$key]['note']=$note;
				$get_list['table']['record'][$key]['type']=$type;
				$get_list['table']['record'][$key]['name']='';

			}
		}
		$tpl->assign('row_list', $get_list);
		$tpl->assign('inbonus', $inbonus);
		$tpl->assign('outbonus', $outbonus);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					$ret['retObj']['data'] = $get_list['table']['record'];
				}
				$ret['retObj']['page'] = $get_list['table']['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}

			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","bonus",true);
		}

	}
	
	
	/*
	*	鯊魚點使用清單
	*	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	*	$kind				varchar				顯示型態 (get:取得點數, use:使用點數)
	*	$p					int					分頁編號
	*/
	/*
	public function bonus() {

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//紅利積點列表
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if($json=='Y'){
			if (!empty($_POST['auth_id'])){
			  $this->userid=$_POST['auth_id'];
				$get_list = $history->bonus_list($this->userid, $kind);
			}else{
				$this->userid=$_SESSION['auth_id'];
				$get_list = $history->bonus_list($this->userid, $kind);
			}
		}else{
			$this->userid=$_SESSION['auth_id'];
			// $get_list = $history->bonus_list($this->userid);
			$get_list = array();
		}

		if(is_array($get_list['table']['record'])){

			$inbonus = 0;
			$outbonus = 0;

			foreach($get_list['table']['record'] as $key => $bonusArr){
				// error_log($key."==".json_encode($bonusArr));
				$bonus_product = $history->get_bonus_product($this->userid, $bonusArr);
				$get_list['table']['record'][$key]['name'] = empty($bonus_product['name']) ? '' : $bonus_product['name'];
				$get_list['table']['record'][$key]['ex_num']=$bonus_product['ex_num'];
				$note="";

				switch($bonusArr['behav']){
				  case 'ibon_exchange':
                                $get_list['table']['record'][$key]['evrid']="i".$bonus_product['ibonexcid'];
                                $note='ibon:';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'order_refund':
                                $note='退費:';
                                $type='add';
                                $inbonus = $inbonus+1;
                                break;
					case 'user_exchange':
                                $get_list['table']['record'][$key]['evrid']="e".$bonus_product['orderid'];
                                $note='兌換:';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'product_close':
					            // error_log("[gg] bonusArr.seq=".$bonusArr['seq']);
								$note='未得標轉贈:';	
								$type='add';
                                $inbonus = $inbonus+1;
                                break;
                                break;
					case 'user_qrcode_tx':
                                $get_list['table']['record'][$key]['evrid']="q".$get_list['table']['record'][$key]['evrid'];
                                $note='';//'掃碼支付';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'other_system_exchange':
                                $get_list['table']['record'][$key]['evrid']="o".$get_list['table']['record'][$key]['evrid'];
                                $note='第三方兌換:';
                                $type='less';
                                $outbonus = $outbonus+1;
                                break;
					case 'order_close':
                                $note='得標獲鯊魚點:';
                                $type='add';
                                $inbonus = $inbonus+1;
                                break;
					default :
					          $note=$bonusArr['behav'];
        						if($bonusArr['amount'] > 0){
        							$type='add';
        							$note='鯊魚點收入';
        							$inbonus = $inbonus+1;
        						}else{
        							$type='less';
        							$note='鯊魚點支出';
        							$outbonus = $outbonus+1;
        						}
				}
				
				// 2019/11/13 By Thomas
				// APP 直接顯示note 所以要先兜好
				if($json=='Y'){
				    if(!empty($get_list['table']['record'][$key]['name'])) {
					   $note.=$get_list['table']['record'][$key]['name'];	
					}
                }
				$get_list['table']['record'][$key]['note']=$note;
				$get_list['table']['record'][$key]['type']=$type;
			    // APP 直接顯示 $note  所以要先兜好
				
			}
		}
		$tpl->assign('row_list', $get_list);
		$tpl->assign('inbonus', $inbonus);
		$tpl->assign('outbonus', $outbonus);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					$ret['retObj']['data'] = $get_list['table']['record'];
				}
				$ret['retObj']['page'] = $get_list['table']['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}

			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","bonus",true);
		}

	}
    */

	// For APP usage
	public function getBonusList() {

		global $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$page=$_POST['page'];
		$perPage=$_POST['perPage'];
		$userid=empty($_POST['auth_id'])?$_SESSION['auth_id']:$_POST['auth_id'];

		//紅利積點列表
		$get_list = $history->get_bonus_list($userid);

		if(is_array($get_list['table']['record'])){
			foreach($get_list['table']['record'] as $key => $bonusArr){
				$note="";
				switch($bonusArr['behav']){
				    case 'ibon_exchange':
                                  $note='ibon 兌換';
                                  break;
					case 'order_refund':
                                  $note='缺貨退費';
                                  break;
					case 'user_exchange':
                                  $note='兌換商品';
                                  break;
					case 'product_close':
                                  $note='結標轉紅利';
                                  break;
					case 'user_qrcode_tx':
                                  $note='';//'掃碼支付';
                                  break;
					case 'other_system_exchange':
					case 'other_exchange':
                                  $note='第三方兌換';
                                  break;
					default :
					          $note=$bonusArr['behav'];
				}
				$get_list['table']['record'][$key]['note']=$note;
				$get_list['table']['record'][$key]['amount']=(int)$get_list['table']['record'][$key]['amount'];
				$bonus_product = $history->get_bonus_product($userid, $bonusArr);
				$get_list['table']['record'][$key]['name'] = $bonus_product['name'];
				$get_list['table']['record'][$key]['ex_num']=$bonus_product['ex_num'];
			}
		}else{
		  echo json_encode(getRetJSONArray(1,'OK','LIST'));
		  exit;
		}

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}

		if($get_list){
		   $ret=getRetJSONArray(1,'OK','LIST');
		   $ret['retObj']['data'] = $get_list['table']['record'];
		   $ret['retObj']['page'] = $get_list['table']['page'];
		   error_log(json_encode($ret));
		   echo json_encode($ret);
    }else{
      echo json_encode(getRetJSONArray(0,'NO DATA FOUND !!','MSG'));
    }

		exit;

	}


	/*
	*店點紀錄列表
	*/
	public function store() {

    global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$get_list = $history->store_list($this->userid);
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}
		$tpl->assign('page_content', $page_content);
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
			  $ret['retObj']['data'] = $get_list['table']['record'];
			  $ret['retObj']['page'] = $get_list['table']['page'];
      }
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","store",true);
		}

	}


	/*
	*中標紀錄
	*/
	public function bid() {

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//中標紀錄列表
		$get_list = $history->bid_list($this->userid);
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}
		$tpl->assign('page_content', $page_content);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
		  if($get_list){
				$ret['retObj']['data'] = $get_list['table']['record'];
				$ret['retObj']['page'] = $get_list['table']['page'];
		  }
		  echo json_encode($ret);
		  exit;
		}else{
		  $tpl->set_title('');
		  $tpl->render("history","bid",true);
		}

	}


	/*
  *  中標商品結帳頁
  *	$json						varchar					瀏覽工具判斷 (Y:APP來源)
  *	$phone						varchar					會員電話
  *	$userid						int						會員編號
  *	$productid					int						商品編號
  */
	public function bid_checkout() {

		global $tpl, $history, $config, $user, $member;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$productid = empty($_POST['productid'])?htmlspecialchars($_GET['productid']):htmlspecialchars($_POST['productid']);
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$phone = empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone']);
		$userid = (empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id']);
		$record = "";
		$get_auth = "";
		error_log("history/bid_checkout: phone_post:".$phone);
		$user_profile = $user->get_user_profile($userid);
		$tpl->assign('user_profile', $user_profile);

		if(empty($phone)){
		  $phone = $_SESSION['user']['profile']['phone'];
		}
		error_log("history/bid_checkout: phone_session:".$phone);

		// 判斷電話參數值是否符合要求
		if(empty($phone)){
			if($json=='Y') {
			  echo json_encode(getRetJSONArray(-1,'手機號不可為空 !','MSG'));
			  exit;
			}else{
			return_to('site/history/bid/?msgType=alert&errMSg='.urlencode('手機號不可為空 !'));
			}
		}elseif(!is_numeric($phone)){
		  if($json=='Y'){
			  echo json_encode(getRetJSONArray(-2,'手機號格式異常,請進行手機驗證 !','MSG'));
			  exit;
			}else{
				echo "<script>alert('手機號格式異常, 請進行手機驗證 !!');location.href = '".BASE_URL.APP_DIR."/verified/register_phone/';</script>";
				exit;
			}
		}

		// 判斷商品編號值是否存在
		if(empty($productid) ){
		  if($json=='Y'){
			  echo json_encode(getRetJSONArray(-3,'商品編號不可為空 !!','MSG'));
			  exit;
			}else{
    		return_to('site/history/bid/?msgType=alert&errMsg='.urlencode('商品編號不可為空 !!'));
			}
		}else{
			// 確認會員認證資料
			$get_auth = $user->validUserAuth($userid, $phone);
			$tpl->assign('auth', $get_auth);
			if(!$get_auth || $get_auth['verified']!='Y'){
			  if($json=='Y') {
					echo json_encode(getRetJSONArray(-4,'您的手機號尚未驗證 !!','MSG'));
					exit;
			  }else{
					echo "<script>alert('您的手機號尚未驗證 !!');location.href = '".BASE_URL.APP_DIR."/verified/register_phone/';</script>";
					exit;
			  }
			}

			// 取得中標相關資料
			$record = $history->bid_checkout($productid, $userid);
			if(!$record || $record['userid']!=$userid){
			  if($json=='Y') {
			    echo json_encode(getRetJSONArray(-5,'查不到您的中標紀錄 !!','MSG'));
			    exit;
			  }else{
          return_to('site/history/bid/?errMSg='.urlencode('查不到您的中標紀錄 !!'));
			    exit;
			  }
			}

			// 確認商品是否結帳
			if($record['complete']=='Y'){
			  if($json=='Y'){
			    echo json_encode(getRetJSONArray(-6,'商品已結帳 !!','MSG'));
			    exit;
			  }else{
          return_to('site/history/bid/?errMSg='.urlencode('商品已結帳 !!'));
				  exit;
			  }
			}

			// 得標價格
			$get_price = (float)$record['price'];
			$record['price'] = round($get_price, 2);

			// 中標處理費 = 市價 * 處理費% * 兌換比率
			$get_process_fee = ((float)$record['retail_price'] * ((float)$record['process_fee']/100) * $config['sjb_rate']) + $record['checkout_money'];
			$record['real_process_fee'] = round($get_process_fee, 2);

			// 費用總計
			$record['total'] = round($get_process_fee + $get_price, 2);

			if($record['checkout_type'] == 'pf'){
				$record['total'] = $get_process_fee;
			}elseif($record['checkout_type'] == 'bid'){
				$record['total'] = $get_price;
			}else{
				$record['total'] = $get_process_fee + $get_price;
			}

			if((int)$record['real_process_fee'] == 0 && $record['totalfee_type'] == 'B' && ($record['productid'] > 8504 && $record['productid'] < 8548)){
				$record['total'] = 0;
			}

			if ($record['is_discount']=='Y') {
				// 計算折抵金額 justice_lee 20190603
				switch ($record['totalfee_type']) {
					case 'A'://手續費+出價金額 
						$record['discount'] = 0;
						$record['discount'] += (int)$record['saja_total_price'];
						$record['discount'] += (int)$record['saja_fee']*(int)$record['saja_cnt'];
						break;
					case 'B'://出價金額
						$record['discount'] = (int)$record['saja_total_price'];
						break;
					case 'F'://手續費
						$record['discount'] = (int)$record['saja_fee']*(int)$record['saja_cnt'];
						break;
					default://免費
						$record['discount'] = 0;
						break;
				}
				$record['total'] -= $record['discount'];
				$record['total'] = ($record['total']<0)? 0 : $record['total'];
			}else{
				$record['discount'] = 0;
			}
			
			$tpl->assign('product', $record);
		}

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array();
			// 手機驗證資料
			$ret['retObj']['authid']=$get_auth['authid'];
			$ret['retObj']['phone']=$get_auth['phone'];
			$ret['retObj']['verified']=$get_auth['verified'];

			// 收件者資料
			$user_profile=$user->get_user_profile($userid);
			$ret['retObj']['zip']=$user_profile['area'];
			$ret['retObj']['address']=$user_profile['address'];
			$ret['retObj']['name']=$user_profile['addressee'];
			$ret['retObj']['rphone']=$user_profile['rphone'];

			// 會員現有殺幣數量
			$spoints=$member->get_spoint($userid);
			$ret['retObj']['spoints']=0;
			if($spoints){
			  $ret['retObj']['spoints']=round($spoints['amount'], 2);
			}

			// 商品紀錄
			$ret['retObj']['productid']=$record['productid'];
			$ret['retObj']['prodname']=$record['name'];
			$ret['retObj']['prodnum']=1;
			$ret['retObj']['thumbnail']=$record['thumbnail'];
			$ret['retObj']['thumbnail_url']=$record['thumbnail_url'];
			$ret['retObj']['real_process_fee']=$record['real_process_fee'];
			$ret['retObj']['price']=$record['price'];
			$ret['retObj']['checkout_type']=$record['checkout_type']; //得標結帳方式 justice_lee 20190621
			$ret['retObj']['is_discount']=$record['is_discount']; //是否折抵 justice_lee 20190617
			$ret['retObj']['discount']=$record['discount']; //折抵金額 justice_lee 20190603
			$ret['retObj']['pgpid']=$record['pgpid'];
			$ret['retObj']['total']=$record['total'];
      $ret['retObj']['offtime']=date('Y-m-d H:i',$record['offtime']);

			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","bid_checkout",true);
		}

	}


	/*
  *	中標商品結帳確認頁
  *	$json						varchar					瀏覽工具判斷 (Y:APP來源)
  *	$userid						int						會員編號
  *	$productid					int						商品編號
  *	$zip						varchar					收件人郵遞編號
  *	$address					varchar					收件人地址
  *	$name						varchar					收件人名稱
  *	$phone						varchar					收件人手機號碼
  *	$pgpid						int						得標紀錄編號
  */
	public function bid_confirm() {

		global $tpl, $history, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid = empty($_SESSION['auth_id']) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		$zip = empty($_POST['zip']) ? htmlspecialchars($_GET['zip']) : htmlspecialchars($_POST['zip']);
		$address = empty($_POST['address']) ? htmlspecialchars($_GET['address']) : htmlspecialchars($_POST['address']);
		$name = empty($_POST['name']) ? htmlspecialchars($_GET['name']) : htmlspecialchars($_POST['name']);
		$phone = empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone']);
		$pgpid = empty($_POST['pgpid']) ? htmlspecialchars($_GET['pgpid']) : htmlspecialchars($_POST['pgpid']);

		if(empty($productid) ){
			if($json == "Y"){
				$ret = getRetJSONArray(-10001,urlencode('資料有誤，請確認資料是否正確 !!'),'MSG');
				echo json_encode($ret);
				exit;
			}else{
				return_to('site/history/bid/');
			}
		}else{
			$record = $history->bid_checkout($productid, $userid);

			// 得標價格
			$get_price = (float)$record['price'] * $config['sjb_rate'];
			$record['price'] = round($get_price, 2);

			// 中標處理費 = 市價 * 處理費% * 兌換比率 + 得標處理金
			$get_process_fee = ((float)$record['retail_price'] * ((float)$record['process_fee']/100) * $config['sjb_rate']) + $record['checkout_money'];
			$record['real_process_fee'] = round($get_process_fee, 2);

			// 費用總計
			$record['total'] = round($get_process_fee + $get_price, 2);

			if($record['checkout_type'] == 'pf'){
				$record['total'] = $get_process_fee;
			}elseif($record['checkout_type'] == 'bid'){
				$record['total'] = $get_price;
			}else{
				$record['total'] = $get_process_fee + $get_price;
			}

			if((int)$record['real_process_fee'] == 0 && $record['totalfee_type'] == 'B' && ($record['productid'] > 8504 && $record['productid'] < 8548)){
				$record['total'] = 0;
			}

			if ($record['is_discount']=='Y') {
				// 計算折抵金額 justice_lee 20190603
				switch ($record['totalfee_type']) {
					case 'A'://手續費+出價金額 
						$record['discount'] = 0;
						$record['discount'] += (int)$record['saja_total_price'];
						$record['discount'] += (int)$record['saja_fee']*(int)$record['saja_cnt'];
						break;
					case 'B'://出價金額
						$record['discount'] = (int)$record['saja_total_price'];
						break;
					case 'F'://手續費
						$record['discount'] = (int)$record['saja_fee']*(int)$record['saja_cnt'];
						break;
					default://免費
						$record['discount'] = 0;
						break;
				}
				$record['total'] -= $record['discount'];
				$record['total'] = ($record['total']<0)? 0 : $record['total'];
			}else{
				$record['discount'] = 0;
			}
			

			// 取得收件人資料
			$userdata = $user->get_user_profile($userid);

			$tpl->assign('product', $record);
			$tpl->assign('user', $userdata);
		}

		if($json=='Y'){
			$ret = getRetJSONArray(1,'OK','JSON');

			if($record) {
				$ret['retObj'] = array();
				// 收件者資料
				$ret['retObj']['zip'] = $zip;
				$ret['retObj']['address'] = $address;
				$ret['retObj']['name'] = $name;
				$ret['retObj']['phone'] = $phone;
				$ret['retObj']['rphone'] = $phone;
				// 商品資料
				$ret['retObj']['productid']=$record['productid'];
				$ret['retObj']['prodname']=$record['name'];
				$ret['retObj']['prodnum']=1;
				$ret['retObj']['thumbnail']=$record['thumbnail'];
				$ret['retObj']['thumbnail_url']=$record['thumbnail_url'];
				$ret['retObj']['real_process_fee']=$record['real_process_fee'];
				$ret['retObj']['price']=$record['price'];
				$ret['retObj']['pgpid']=$record['pgpid'];
				$ret['retObj']['checkout_type']=$record['checkout_type']; //得標結帳方式 justice_lee 20190617
				$ret['retObj']['is_discount']=$record['is_discount']; //是否折抵 justice_lee 20190617
				$ret['retObj']['discount']=$record['discount']; //折抵金額 justice_lee 20190603
				$ret['retObj']['total']=$record['total'];
				$ret['retObj']['offtime']=date('Y-m-d H:i',$record['offtime']);
			}
		  echo json_encode($ret);
		}else{
	    $tpl->set_title('');
	    $tpl->render("history","bid_confirm",true);
		}

	}


	/*
	*殺價禮券列表
	*/
	public function coupon() {

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$get_list = $history->coupon_list($this->userid);
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}
		$tpl->assign('page_content', $page_content);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				for($idx=0; $idx< count($get_list['table']['record']); ++$idx){
					$p = $get_list['table']['record'][$idx];
					$get_list['table']['record'][$idx]['offtime'] = (int)$p['offtime'];
				}
			  $ret['retObj']['data'] = $get_list['table']['record'];
			  $ret['retObj']['page'] = $get_list['table']['page'];
			}
			echo json_encode($ret);
			exit;
		}else{
      $tpl->set_title('');
      $tpl->render("history","coupon",true);
		}

	}
	
	
    /*
	*	購物金使用清單
	*	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	*	$kind				varchar				顯示型態 (get:取得點數, use:使用點數)
	*	$p					int					分頁編號
	*/
	public function rebate() {

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//紅利積點列表
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
        error_log("[c/history/rebate] GET : ".json_encode($_GET));
        error_log("[c/history/rebate] POST : ".json_encode($_POST));
		
		if($json=='Y'){
			if (!empty($_POST['auth_id'])){
			  $this->userid=$_POST['auth_id'];
				$get_list = $history->rebate_list($this->userid, $kind);
			}else{
				$this->userid=$_SESSION['auth_id'];
				$get_list = $history->rebate_list($this->userid, $kind);
			}
		}else{
			$this->userid=$_SESSION['auth_id'];
			$get_list = array();
		}

		if(is_array($get_list['table']['record'])){

			$inrebate = 0;
			$outrebate = 0;

			foreach($get_list['table']['record'] as $key => $discountArr){
				$rebate_product = $history->get_rebate_product($this->userid, $rebateArr);
				$get_list['table']['record'][$key]['name'] = empty($rebate_product['name']) ? '' : $rebate_product['name'];
				$get_list['table']['record'][$key]['ex_num']=$rebate_product['ex_num'];
		
                switch($rebateArr['behav']){
					case 'order_refund':
                                $note='訂單退費:';
                                $type='add';
                                $inrebate = $inrebate+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'bid_close':
                                $note='結標轉入:';
                                $type='add';
                                $inrebate = $inrebate+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'user_exchange':
                                $get_list['table']['record'][$key]['evrid']="e".$rebate_product['orderid'];
                                $note='兌換折抵:';
                                $type='less';
                                $outrebate = $outrebate+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'user_deposit':
                                $note='使用者儲值:';
                                $type='add';
                                $inrebate = $inrebate+1;
                                $get_list['table']['record'][$key]['tx_status']='';
                                break;
					case 'system':
                                $note='系統轉入:';
								$type='add';
                                $inrebate = $inrebate+1;
                                break;
					case 'system_test':
                                $note='系統測試:';
								$type='add';
                                $inrebate = $inrebate+1;
                                break;
					case 'gift':
        						if($rebateArr['amount'] > 0){
        							$type='add';
        							$note='轉贈會員收入';
        							$inrebate = $inrebate+1;
        						}else{
        							$type='less';
        							$note='轉贈會員支出';
        							$outrebate = $outrebate+1;
        						}
                                $get_list['table']['record'][$key]['tx_status']='';								
                                break;
					default :
					            $note=$rebateArr['behav'];
        						if($rebateArr['amount'] > 0){
        							$type='add';
        							$note='購物金收入';
        							$inrebate = $inrebate+1;
        						}else{
        							$type='less';
        							$note='購物金支出';
        							$outrebate = $outrebate+1;
        						}
        						break;
				}

			    if(!empty($get_list['table']['record'][$key]['name'])) {
				   $note.=str_replace(': '.abs($rebateArr['amount']),'',$get_list['table']['record'][$key]['name']);	
				}	
				
				$get_list['table']['record'][$key]['note']=$note;
				$get_list['table']['record'][$key]['type']=$type;
				$get_list['table']['record'][$key]['name']='';

			}
		}
		$tpl->assign('row_list', $get_list);
		$tpl->assign('inrebate', $inrebate);
		$tpl->assign('outrebate', $outrebate);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					$ret['retObj']['data'] = $get_list['table']['record'];
				}
				$ret['retObj']['page'] = $get_list['table']['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}

			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","rebate",true);
		}

	}	


/*
	*	圓夢券使用清單
	*	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	*	$kind				varchar				顯示型態 (Y:已使用, use:未使用)
	*	$p					int					分頁編號
	*/
	public function dscode() {

		global $tpl, $history;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//紅利積點列表
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
        error_log("[c/history/rebate] GET : ".json_encode($_GET));
        error_log("[c/history/rebate] POST : ".json_encode($_POST));
		
		if($json=='Y'){
			if (!empty($_POST['auth_id'])){
			  $this->userid=$_POST['auth_id'];
				$get_list = $history->dscode_list($this->userid, $kind);
			}else{
				$this->userid=$_SESSION['auth_id'];
				$get_list = $history->dscode_list($this->userid, $kind);
			}
		}else{
			$this->userid=$_SESSION['auth_id'];
			$get_list = array();
		}

		if(is_array($get_list['table']['record'])){

			foreach($get_list['table']['record'] as $key => $discountArr){
		
                switch($discountArr['behav']){
					case 'user_regster':
                                $note='新戶註冊';
                                break;
					case 'bid_close':
                                $note='結標轉入:';
                                break;
					case 'user_exchange':
                                $note='會員兌換:';
                                break;
					case 'user_deposit':
                                $note='使用者儲值:';
                                break;
					case 'promote':
       							$note='行銷公關';
								break;
					case 'system':
                                $note='系統轉入:';
                                break;
					case 'system_test':
                                $note='系統測試:';
                                break;
					case 'gift':
        						if($discountArr['fromuserid'] > 0){
        							$note='轉贈收入';
        						}else{
									$note='轉贈支出';
        						}
                                break;
					default :
        						if($discountArr['fromuserid'] > 0){
        							$note='其它收入';
        						}else{
        							$note='其它支出';
        						}
        						break;
				}

				$get_list['table']['record'][$key]['note']=$note;

			}
		}
		$tpl->assign('row_list', $get_list);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					$ret['retObj']['data'] = $get_list['table']['record'];
				}
				$ret['retObj']['page'] = $get_list['table']['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}

			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("history","dscode",true);
		}

	}	

}
