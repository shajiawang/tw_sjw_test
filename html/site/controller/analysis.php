<?php
/*
 * Analysis Controller
 * 數據分析
 */

class analysis {

	public $controller = array();
	public $params = array();
  public $id;


	/*
   *分析統計報表
   */
  public function home() {

		global $db, $config;

		//設定 Action 相關參數
		set_status($this->controller);
		/*
		$arrAllowIP=[
                 '61.219.220.93',
				 '60.251.120.84',
                 '210.61.148.35'];
		*/
		// VPN 的 IP		 
		$arrAllowIP=['61.219.220.93','60.251.120.84','210.61.148.35'];
				 
        $ip=GetIP();
		if(!in_array($ip,$arrAllowIP)) {
			   die("Your IP : ".$ip." is not authorized !!");
			   exit();
		}
		



		$date = empty($_POST['date']) ? htmlspecialchars($_GET['date']) : htmlspecialchars($_POST['date']);

		$sdate = empty($_POST['sdate']) ? htmlspecialchars($_GET['sdate']) : htmlspecialchars($_POST['sdate']);
		$edate = empty($_POST['edate']) ? htmlspecialchars($_GET['edate']) : htmlspecialchars($_POST['edate']);

		if(empty($date) && empty($sdate) && empty($edate)) {
			    $date=date('Y-m-d');
				$sdate=$date;
				$edate=$date;
		} else if (!empty($sdate) && !empty($edate)){
				$sdate = $sdate;
				$edate = $edate;
		} else if (!empty($date)){
				$sdate = $date;
				$edate = $date;
		}else{		
				echo "日期未輸入，請輸入日期。";
				die();
		}

		echo"<style>
		       body{
		       	 font-size:24pt;
		       }
		       table, th, td {
             border: 1px solid black;
           }
					 table{
					 	 width: 60vw;
					 }
					 th, td{
					   font-size:24pt;
					 }
		     </style>";
    echo "統計區間: ".$sdate." ~ ".$edate;

		echo "</br>";
		echo "</br>";
    echo "商品下標狀況:";
		echo "</br>";
		$query ="SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(sh.`userid`) AS subtotal, sp.`name` AS product_name, sp.`saja_fee` AS saja_fee,COUNT(sh.`userid`)*sp.`saja_fee` AS saja_sum
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
		 LEFT join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` sp
		  ON sh.`productid` = sp.`productid`
			WHERE sh.`productid` 
			IN (SELECT `productid` FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
				  WHERE offtime >='{$sdate} 00:00:00' and offtime <='{$edate} 23:59:59' AND closed <> 'N' )
		 GROUP BY `pay`,`productid` ORDER BY productid,pay ASC";

		$table = $db->getQueryRecord($query);

		echo"<table>";

		echo"<tr>";
		echo"<th>商品編號</th>";
		echo"<th>商品名稱</th>";
		echo"<th>支付方式</th>";
		echo"<th>筆數</th>";
		echo"<th>下標手續費</th>";
		echo"<th>總下標手續費</th>";
		echo"</tr>";

    $paid_total_price = 0;
    $free_total_price = 0;
		foreach($table['table']['record'] as $tk => $tv)
		{

			echo "<tr>";
			echo "<th>".$table['table']['record'][$tk]['productid']."</th>";
      echo "<th>".$table['table']['record'][$tk]['product_name']."</th>";
      if($table['table']['record'][$tk]['pay'] == 'paid'){
				echo "<th>"."付費"."</th>";
			}else if($table['table']['record'][$tk]['pay'] == 'free'){
				echo "<th>"."免費"."</th>";
			}
			echo "<th>".$table['table']['record'][$tk]['subtotal']."</th>";
			echo "<th>".round($table['table']['record'][$tk]['saja_fee'])."</th>";
			echo "<th>".round($table['table']['record'][$tk]['saja_sum'])."</th>";
			echo "</tr>";

			if($table['table']['record'][$tk]['pay'] == 'paid'){
			  $paid_total_price = $paid_total_price + round($table['table']['record'][$tk]['saja_sum']);
			}else if($table['table']['record'][$tk]['pay'] == 'free'){
				$free_total_price = $free_total_price + round($table['table']['record'][$tk]['saja_sum']);
			}

		}
    echo "</table>";

		echo "付費 總下標手續費 合計: ".$paid_total_price." 元";
		echo "</br>";
    echo "免費 總下標手續費 合計: ".$free_total_price." 元";

		echo "</br>";
		echo "<hr/>";
		echo "</br>";

		$query ="SELECT *
		FROM (SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(userid) AS subtotal
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
		LEFT join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` sp
		 ON sh.`productid` = sp.`productid`
		WHERE sh.`productid` IN (
			SELECT `productid`
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
			WHERE offtime >='{$sdate} 00:00:00'
			and offtime <='{$edate} 23:59:59' AND closed <> 'N'
		) GROUP BY `userid`,`pay`
			ORDER BY `subtotal` desc) AS tb1
			";

		$table = $db->getQueryRecord($query);

    echo"殺價幣(依會員帳號統計 排序由下標筆數多到少)";
		echo"<table>";

		echo"<tr>";
		echo"<th>會員編號</th>";
		echo"<th>會員名稱</th>";
		echo"<th>使用時間</th>";
		echo"<th>支付方式</th>";
		echo"<th>筆數</th>";
		echo"</tr>";

    $paid_count = 0;
    $paid_total = 0;
		foreach($table['table']['record'] as $tk => $tv)
		{
      if($table['table']['record'][$tk]['pay'] == 'paid')
			{
			echo "<tr>";
			echo "<th>".$table['table']['record'][$tk]['userid']."</th>";
			echo "<th>".$table['table']['record'][$tk]['nickname']."</th>";
			echo "<th>".$table['table']['record'][$tk]['insertt']."</th>";
			if($table['table']['record'][$tk]['pay'] == 'paid'){
				echo "<th>"."付費"."</th>";
			}else if($table['table']['record'][$tk]['pay'] == 'free'){
				echo "<th>"."免費"."</th>";
			}
			echo "<th>".$table['table']['record'][$tk]['subtotal']."</th>";
			echo "</tr>";
			$paid_count = $paid_count + 1;
			$paid_total = $paid_total + $table['table']['record'][$tk]['subtotal'];
		  }

		}
    echo "</table>";
		echo "付費人數 合計: ".$paid_count." 筆";
		echo "</br>";
    echo "殺價幣 合計: ".$paid_total." 筆";

    echo "</br>";
		echo "<hr/>";
		echo "</br>";

		echo"殺價券+免下標手續費(依會員帳號統計 排序由下標筆數多到少)";
		echo"<table>";

		echo"<tr>";
		echo"<th>會員編號</th>";
		echo"<th>會員名稱</th>";
		echo"<th>使用時間</th>";
		echo"<th>支付方式</th>";
		echo"<th>筆數</th>";
		echo"</tr>";

    $free_count = 0;
    $free_total = 0;
		foreach($table['table']['record'] as $tk => $tv)
		{
			if($table['table']['record'][$tk]['pay'] == 'free')
			{
			echo "<tr>";
			echo "<th>".$table['table']['record'][$tk]['userid']."</th>";
			echo "<th>".$table['table']['record'][$tk]['nickname']."</th>";
			echo "<th>".$table['table']['record'][$tk]['insertt']."</th>";
			if($table['table']['record'][$tk]['pay'] == 'paid'){
				echo "<th>"."付費"."</th>";
			}else if($table['table']['record'][$tk]['pay'] == 'free'){
				echo "<th>"."免費"."</th>";
			}
			echo "<th>".$table['table']['record'][$tk]['subtotal']."</th>";
			echo "</tr>";
			$free_count = $free_count + 1;
			$free_total = $free_total + $table['table']['record'][$tk]['subtotal'];
			}
		}
		echo "</table>";
		echo "免費人數 合計: ".$free_count." 筆";
		echo "</br>";
    echo "殺價券+免下標手續費 合計: ".$free_total." 筆";

		echo "</br>";
		echo "<hr/>";
		echo "</br>";

		$query ="SELECT count(*) as people_count
		FROM (SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(userid) AS subtotal
		FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` sh
		LEFT join `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` sp
		 ON sh.`productid` = sp.`productid`
		WHERE sh.`productid` IN (
			SELECT `productid`
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
			WHERE offtime >='{$sdate} 00:00:00'
			and offtime <='{$edate} 23:59:59' AND closed <> 'N'
		  ) GROUP BY `userid`
			ORDER BY userid asc) AS tb1
			";

		$table = $db->getQueryRecord($query);

    $people_count = $table['table']['record'][0]['people_count'];

    echo "總下標人數: ".$people_count;
		echo "</br>";

    $total_count = ($paid_total+$free_total);
    echo "總下標筆數: ".$total_count;

		echo "</br>";
		echo "</br>";

		if($people_count > 0){
			$pay_ratio = round((($paid_count/$people_count)*100),2) ." %";
		}else{
			$pay_ratio = "0 %";
		}
    echo "下標付費人數比率: ".$pay_ratio;

		$query ="SELECT `up`.`userid`,`up`.`nickname`,`u`.`insertt`
		           FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
					LEFT JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` up
							     ON `u`.`userid` = `up`.`userid`
		          WHERE `u`.`insertt` >='{$sdate} 00:00:00'
			          AND `u`.`insertt` <='{$edate} 23:59:59'";

		$table = $db->getQueryRecord($query);

		echo "</br>";
		echo "<hr/>";
		echo "</br>";

		echo"新增會員統計";

		echo "</br>";
		echo "</br>";
		$new_member_count = 0;
		foreach($table['table']['record'] as $tk => $tv)
		{
			$new_member_count = $new_member_count + 1;
		}
		echo "新增會員人數: ".$new_member_count;
		echo "</br>";
		echo "新增會員清單: ";
		echo "</br>";
		echo"<table>";

		echo"<tr>";
		echo"<th>會員編號</th>";
		echo"<th>會員名稱</th>";
		echo"<th>新增時間</th>";
		echo"</tr>";

		foreach($table['table']['record'] as $tk => $tv)
		{
			echo "<tr>";
			echo "<th>".$table['table']['record'][$tk]['userid']."</th>";
			echo "<th>".$table['table']['record'][$tk]['nickname']."</th>";
			echo "<th>".$table['table']['record'][$tk]['insertt']."</th>";
			echo "</tr>";
		}
		echo "</table>";
		echo "</br>";

		return ;

	}

}
