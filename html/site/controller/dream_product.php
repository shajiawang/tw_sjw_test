<?php
/*
 * DreamProduct Controller 圓夢商品
 * API:1309 圓夢殺房首頁 house_list()
 * API:1308 圓夢殺房商品 house()
 */
class DreamProduct {

	public $controller = array();
	public $params = array();
	public $id;

	/*
   * Action Method : home
   */
	public function home() {

		global $tpl, $dream_product, $channel, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		//商品分類
		//$_category = $dream_product->product_category();
		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$type=$_REQUEST['type'];
		$cb=$_REQUEST['canbid'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		$rkey = "dream_product_category_list";
		$redis = getRedis();
		if($redis){
			$jsonStr = $redis->get($rkey);
			if(!empty($jsonStr)){
				$category_list = json_decode($jsonStr,true);
			}else{
				$category_list = $dream_product->dream_product_category_list();
				$redis->set($rkey,json_encode($category_list));
			}
		}else{
			$category_list = $dream_product->dream_product_category_list();
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['category_list'] = $category_list;
			echo json_encode($ret);
			exit;
		}else{
			//是否讓頁面顯示出價欄位
			if($cb!='N'){
				$cb='Y';
			}
			$tpl->assign('category_list', $category_list);
			$tpl->assign('canbid', $cb);
			$tpl->assign('noback', 'Y');
			$tpl->set_title('');
			$tpl->render("dream_product","home",true);
			$ret = getRetJSONArray(-1,'WRONG','MSG');
			echo json_encode($ret);
			exit;
		}
	}
	
	//API:1309 圓夢殺房首頁
	public function house_list(){
		global $tpl, $dream_product, $config, $user, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$pcid = '148'; //empty($_POST['pcid']) ? htmlspecialchars($_GET['pcid']) : htmlspecialchars($_POST['pcid']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if(empty($p) || $p <= 0){
			$p = 1;
		}

		//商品列表
		$product_list = $dream_product->dream_product_list($pcid, $p);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($product_list){
				$ret['retObj'] = $product_list;
				//最新下標
				$ret['retObj']['new_bid'] = array();

			}else{
				$ret['retCode'] = -1;
				$ret['retMsg'] = 'NO_DATA';
				$ret['retObj'] = array();
				$ret['retObj']['new_bid'] = array();

			}
			echo json_encode($ret);
			exit;
		}else{
			//是否讓頁面顯示出價欄位
			if($cb!='N'){
				$cb='Y';
			}
			$redis = getRedis();
			if($redis){
				$jsonStr = $redis->get($rkey);
				if(!empty($jsonStr)){
					$category_list = json_decode($jsonStr,true);
				}else{
					$category_list = $dream_product->dream_product_category_list();
					$redis->set($rkey,json_encode($category_list));
				}
			}else{
				$category_list = $dream_product->dream_product_category_list();
			}

			$tpl->assign('category_list', $category_list);
			$tpl->assign('canbid', $cb);
			$tpl->assign('noback', 'Y');
			$tpl->set_title('');
			$tpl->render("dream_product","house_list",true);
			//$ret = getRetJSONArray(-1,'WRONG','MSG');
			echo json_encode($ret);
			exit;
		}
	}

	//API:1308 圓夢殺房商品
	public function house() 
	{
		global $tpl, $dream_product, $product, $channel, $config, $user, $scodeModel, $oscode, $bid;
		
		login_required();
		//設定 Action 相關參數
		set_status($this->controller);

		$json = (empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid = (empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid = empty($_SESSION['auth_id'])?$_REQUEST['auth_id']:$_SESSION['auth_id'];
		$type = empty($_SESSION['auth_type'])?$_REQUEST['auth_type']:$_SESSION['auth_type'];
		$behav = $_REQUEST['behav'];
		$bid_a = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bida = empty($bid_a) ? '' : $bid_a;
		$bid_b = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$bidb = empty($bid_b) ? '' : $bid_b;
		$_auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$auto = empty($_auto) ? '' : $_auto;
		$paytype = 'single';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){
			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}
		//支付回傳資料
		$paydata = array(
			'bid'		=> $bidprice,	//起始價格
			'bida'		=> $bida,		//起始價格
			'bidb' 		=> $bidb,		//結束價格
			'type'		=> $paytype,	//下標類型
			'autobid'	=> $auto,		//自動下標
		);
		
		$ret=getRetJSONArray(1,'OK','JSON');
		$check=true;
		
		// 增加商品點擊數
		//$product->add_prod_click($productid,1);
		
		//收藏資料 : 20190508 暫時disable By Thomas
		// $collect = $user->getCollectProdList($userid, $productid, 'S', '');
		// $get_product['collect'] = $collect['record'][0];
			
		// 商品資料
		$get_product = $dream_product->get_dream_product_info($productid);
		
		if(empty($productid) || !$get_product){
			$check=false;
			if($json=='Y'){
				echo json_encode(getRetJSONArray(-1,'商品編號為空 !','MSG'));
				exit;
			}else{
				return_to('site/product');
			}
		}
		
		//用戶可否下標$ret['dream_enable']
		$get_user = $user->get_user($userid);
		$ret['dream_enable'] = $get_user['dream_enable'];
		$tpl->assign('dream_enable', $get_user['dream_enable']);
		if($get_user['dream_enable'] != 'Y'){
			$check=false;
			if($json=='Y'){
				echo json_encode(getRetJSONArray(-2, '此用戶不可下標 !', 'MSG'));
				exit;
			}
		}
		
		
		//生成商品頁內容
		if($check){
            $get_product['use_scode'] = false;
			$msg='';
			$get_product['now'] = (int)$get_product['now'];

			// 抓source IP
			$src_ip = getUserIP();

            // 抓user_agent
			// $user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			// $is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];
			
			//商品同意內容
			$get_product['agree'] = "本人已充分了解並同意「用戶使用規範」、「服務條款」、「下標規則」且同意本站變更、修改或終止該商品內容之權利。";
			$sgift=0;
			
			//取最新得標者
			$ret['bid_winner'] = array();
			$bid_winner = $dream_product->get_bid_winner($productid);
			if(!empty($bid_winner)){
				$ret['bid_winner'] = $bid_winner;
				$bid_winner['thumbnail_url'] = empty($bid_winner['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bid_winner['thumbnail_url'];
				$ret['bid_winner']['thumbnail_url'] = $bid_winner['thumbnail_url'];
			}else{
				$ret['bid_winner']=array("shid"=>"","userid"=>"","productid"=>"","nickname"=>"","price"=>"","src_ip"=>"","insertt"=>"","thumbnail_file"=>"","thumbnail_url"=>"","shd_thumbnail_file"=>"");
			}
			$tpl->assign('bid_winner', $bid_winner);

			//取最新五筆出價商品紀錄
			$ret['latest_bid'] = array();
			$latest_five_bid = $dream_product->get_history_detail($productid);
			if(!empty($latest_five_bid)){
				$ret['latest_bid'] = $latest_five_bid;
			}
			$tpl->assign('latest_bid', $latest_five_bid);

			//取得最近出價會員商品資料
			$ret['latest_user_bid'] = array();
			$latest_user_bid = $dream_product->get_user_history_detail($productid,$userid);
			if(!empty($latest_user_bid)){
				$ret['latest_user_bid'] = $latest_user_bid;
			}
			$tpl->assign('latest_user_bid', $latest_user_bid);
			
			
			if($json=='Y'){
                $ret['product_info'] = $get_product;
				$ret['paydata'] = $paydata;
            } else {
				if(!empty($get_product['thumbnail2'])){
					$meta['image'] = $get_product['thumbnail2'];
                }elseif (!empty($get_product['thumbnail_url'])){
					$meta['image'] = $get_product['thumbnail_url'];
                }
				$tpl->assign('paydata',$paydata);
				$tpl->assign('type',$type);
				$tpl->assign('sgift_num', $sgift);
            }
			
			// 可用殺價券數量
			$oscode_num =  $oscode->get_prod_oscode($productid, $userid);
			error_log("[c/product/saja] user: ${userid} available oscode for ".$productid." : ".$oscode_num);
			
			if($json=='Y'){
				$ret['oscode_num']=$oscode_num;
			}else{
				$tpl->assign('oscode_num', $oscode_num);
			}
			
			//S碼 現有可用總數
			$scode_num = $scodeModel->get_scode($userid);
			if($json=='Y'){
				$ret['scode_num']=$scode_num;
			}else{
				$tpl->assign('scode_num', $scode_num);
			}

            // S碼 已用總數
            $scode_use_num = abs($scodeModel->used_sum($userid));
            if($json=='Y'){
                $ret['scode_use_num']=$scode_use_num;
            }else{
                $tpl->assign('scode_use_num', $scode_use_num);
            }
			
			//目前中標
			$bidded = $product->get_bidded($productid);
			$num_bids_gap = $product->getNumBidsGap($productid);
			
			$srcip = $bidded['src_ip'];
			if(! empty($bidded['src_ip']) ){
				preg_match_all('/[0-9]+/i',$bidded['src_ip'], $src_ip);
				$src_ip[0][1] = "***";
				$src_ip[0][2] = "***";
				$srcip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				// error_log("[c/product/saja] src_ip: ".json_encode($all_src_ip));
			}
			
			if($json=='Y'){
                $ret['curr_winner_userid']=empty($bidded['userid'])?"":$bidded['userid'];
                $ret['bidded']=empty($bidded['nickname'])?"":$bidded['nickname'];

                if($ret['curr_winner_userid']==""){
                    $ret['bidded']="_無人得標_";
                    $ret['final_bid_price']=0;
                } else {
                    $ret['src_ip']= $srcip;

                    if(strpos($ret['src_ip'], ",")){
                        $ret['src_ip'] = substr($ret['src_ip'], 0, strpos($ret['src_ip'],","));
                    }
                    $ret['curr_winner_comefrom']=empty($bidded['comefrom'])?"":$bidded['comefrom'];
                    $ret['curr_winner_headimg']=$bidded['thumbnail_file'];
					$ret['curr_winner_headimg_url']=empty($bidded['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bidded['thumbnail_url'];
				}
                // error_log("[product/saja]num_bids_gap:".$num_bids_gap[0]['num']);
                $ret['num_bids_gap'] = empty($num_bids_gap[0]['num'])?$get_product['saja_limit']:($get_product['saja_limit']-$num_bids_gap[0]['num']);

            }else{
				$bidded['src_ip'] = $srcip;
				$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
				if(!empty($bidded['thumbnail_file'])){
					$bidded['curr_winner_headimg'] = BASE_URL.HEADIMGS_DIR .'/'. $bidded['thumbnail_file'];
				} elseif(!empty($bidded['thumbnail_url'])) {
					$bidded['curr_winner_headimg'] = $bidded['thumbnail_url'];
				} else {
					$bidded['curr_winner_headimg'] = BASE_URL.APP_DIR .'/static/img/rankman_1.png';
				}
				
			    $tpl->assign('sajabid', $bidded);
				$tpl->assign('canbid',$_REQUEST['canbid']);
                error_log("[product/saja]Winner:".$bid_name." of Prod:".$productid." From DB and sync to redis : ".$bid_name);
			}
			
		}
		
		$bidget_count = $user->pay_get_product_count($userid);
		$user_profile = $user->get_user_profile($userid);

		// 限定商品資格
		// checkCode=1 才可下標  // checkMsg 不可下標時顯示的訊息
		$retCheck=array();
		$retCheck['checkCode'] = 1;
		$retCheck['checkMsg'] = "";
		if ($get_product['limitid']=="7" || $get_product['limitid']=="21") {
			$retCheck = $product->new_hand_rule($productid,$userid,"1",$get_product['limitid']);
		}
		$err_limit = 0;
		if (!empty($get_product['limitid'])) {
			if ($get_product['kind'] == 'small') {
				$err_limit = ((int)$bidget_count>=(int)$get_product['mvalue'])? 1 : 0;
			}else if($get_product['kind'] == 'big'){
				$err_limit = ((int)$bidget_count<=(int)$get_product['svalue'])? 1 : 0;
			}
			if ($err_limit == 1) {
				$retCheck['checkCode'] = 3;
				$retCheck['checkMsg'] = "不符合{$get_product['plname']}商品資格";
			}
		}
		
		// 必須先填寫收件人姓名/郵遞區號/地址/電話
        if(empty($user_profile['address']) || 
		   empty($user_profile['addressee']) || 
		   empty($user_profile['area']) || 
		   empty($user_profile['rphone'])) {
			 $retCheck['checkCode'] = 4;
			 $retCheck['checkMsg'] = "下標前請先詳填收件人資料 !";   	
		}
		
		if($json=='Y'){
			$ret['retCheck'] = $retCheck;
			// error_log("[c/product/saja] retCheck (for APP) :".json_encode($retCheck));
			$ret['bidget_count'] = $bidget_count;
			
			echo json_encode($ret);
			exit;
		}else{
			
			$meta['title']=$get_product['name']." 搶標中 !!";
			$meta['description']='';
			
			$tpl->assign('meta',$meta);
			$tpl->assign('bidget_count', $bidget_count);
			$tpl->assign('retCheck', $retCheck);
			$tpl->assign('msg',$msg);
			$tpl->assign('product', $get_product);
			$tpl->set_title($get_product['name']);
			
			$tpl->render("dream_product","house",true);
		}
	}
  //圓夢商品 /*20201204 停用 */
	public function house_saja() {
		global $tpl, $dream_product, $product, $channel, $config, $user, $scodeModel, $oscode, $bid;
		login_required();
		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){

			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
			'bid'		=> $bidprice,			//起始價格
			'bida'		=> $bida,				//起始價格
			'bidb' 		=> $bidb,				//結束價格
			'type'		=> $paytype,			//下標類型
			'autobid'	=> $auto,				//自動下標
		);

		$tpl->assign('paydata',$paydata);

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid=(empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		$behav=$_REQUEST['behav'];

		//用戶可否下標 AARONFU
		$get_user = $user->get_user($userid);
		$tpl->assign('dream_enable',$get_user['dream_enable']);
		$tpl->assign('type',$type);

		$ret=null;
		if(empty($productid) ){
			if($json=='Y'){
				echo json_encode(getRetJSONArray(-3,'商品編號為空 !!','MSG'));
				exit;
			}else{
				return_to('site/product');
			}
		}else{
			// 商品資料
			$get_product = $dream_product->get_dream_product_info($productid);

			if(!$get_product){
                if($json=='Y'){
				  echo json_encode(getRetJSONArray(-4,'找不到商品資料 !!','MSG'));
				  exit;
				}
			}

			// 增加商品點擊數 By Thomas 20190508
            // 2020/01/31 暫停紀錄 By Thomas
			// $product->add_prod_click($productid,1);

		    //收藏資料 : 20190508 暫時disable By Thomas
			// $collect = $user->getCollectProdList($userid, $productid, 'S', '');
			// $get_product['collect'] = $collect['record'][0];

			$ret=getRetJSONArray(1,'OK','JSON');
            $get_product['use_scode'] = false;
			$msg='';
			$get_product['now'] = (int)$get_product['now'];

			// 抓source IP
			$src_ip = getUserIP();

            // 抓user_agent
			// $user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			// $is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if($json=='Y'){
                $ret['retObj']=$get_product;
            } else {
                if(!empty($get_product['thumbnail2'])){
				  $meta['image'] = $get_product['thumbnail2'];
                }elseif (!empty($get_product['thumbnail_url'])){
				  $meta['image'] = $get_product['thumbnail_url'];
                }
            }
			$sgift=0;
			if($json=='Y'){

            } else {
                $tpl->assign('sgift_num', $sgift);
            }

			/* Update By Thomas 2015214
			// 如果有經由qrcode過來 就贈送過殺價券
			if(!empty($userid) && !empty($productid) && $behav=="q"){
				$oscode_num0 = $scodeModel->get_oscode($productid, $userid);
				//查詢是否有贈送殺價券活動
				$scode_promote = $scodeModel->scode_promote($productid,'q');
				$info['spid'] = $scode_promote['spid'];
				$info['behav'] = 'qrcode_reg';
				$info['productid'] = $productid;
				$info['userid'] = $userid;
				$info['onum'] = $scode_promote['onum'];

                // 如果沒送過 且有活動 就贈送
				if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){
				  for($j = 0;$j < $info['onum']; ++$j){
					  $scodeModel->add_oscode($info);
				  }
				  $scodeModel->set_scode_promote($info);
				  $msg="恭喜您獲得 ".$info['onum']."張 ".$get_product['name']." 殺價券";
				}
				// Add End
			}
            */

			//取最新得標者
			$bid_winner = $dream_product->get_bid_winner($productid);
			if (!empty($bid_winner)){
				$bid_winner['thumbnail_url']=empty($bid_winner['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bid_winner['thumbnail_url'];
			}
			$tpl->assign('bid_winner', $bid_winner);

			//取最新五筆出價商品紀錄
			$latest_five_bid = $dream_product->get_history_detail($productid);
			$latest_bid = $latest_five_bid;
			$tpl->assign('latest_bid', $latest_bid);

			//取得最新使用者最後出價商品資料
			$latest_user_bid = $dream_product->get_user_history_detail($productid,$userid);
			$tpl->assign('latest_user_bid', $latest_user_bid);

			//目前中標
			$bidded = $product->get_bidded($productid);
			$num_bids_gap = $product->getNumBidsGap($productid);


			if($bidded['userid'] <> ""){
				preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
				$src_ip[0][1] = "***";
				$src_ip[0][2] = "***";
				$all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				error_log("[c/product/saja] src_ip: ".json_encode($all_src_ip));
				$bidded['src_ip'] = $all_src_ip;
			}
			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			// $redis = getRedis('','');
			// $redis->set('PROD:'.$productid.':BIDDER',$bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);
		}

		// Update By AARONFU 20190917
		$tpl->set_title($get_product['name']);

        if(!empty($msg))
			$tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);

		if(!empty($userid)){
			$bidget_count = $user->pay_get_product_count($userid);
			$user_profile=$user->get_user_profile($userid);
		}else{
			$bidget_count = 0;
		}

		// 限定商品資格
		// checkCode=1 才可下標
		// checkMsg 不可下標時顯示的訊息
		$retCheck=array();
		$retCheck['checkCode'] = 1;
		$retCheck['checkMsg'] = "";
		if ($get_product['limitid']=="7" || $get_product['limitid']=="21") {
			$retCheck = $product->new_hand_rule($productid,$userid,"1",$get_product['limitid']);
		}
		$err_limit = 0;
		if (!empty($get_product['limitid'])) {
			if ($get_product['kind'] == 'small') {
				$err_limit = ((int)$bidget_count>=(int)$get_product['mvalue'])? 1 : 0;
			}else if($get_product['kind'] == 'big'){
				$err_limit = ((int)$bidget_count<=(int)$get_product['svalue'])? 1 : 0;
			}
			if ($err_limit == 1) {
				$retCheck['checkCode'] = 3;
				$retCheck['checkMsg'] = "不符合{$get_product['plname']}商品資格";
			}
		}

		/*
		// Add By Thomas 2020/01/31
		// 必須先填寫收件人姓名/郵遞區號/地址/電話後　才能下標圓夢商品
        if(empty($user_profile['address']) ||
		   empty($user_profile['addressee']) ||
		   empty($user_profile['area']) ||
		   empty($user_profile['rphone'])) {
			 $retCheck['checkCode'] = 4;
			 $retCheck['checkMsg'] = "下標圓夢商品前請先詳填收件人資料 !";
		}*/

		$tpl->assign('bidget_count', $bidget_count);
		$tpl->assign('retCheck', $retCheck);
		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);

		if($json=='Y'){
			$ret['retObj']['dream_enable']=$get_user['dream_enable'];
			$x= json_encode($ret);
			echo $x;
			exit;
		}else{
			$tpl->render("dream_product","house_saja",true);
		}

	}
	//圓夢商品
	public function saja() {
		global $tpl, $dream_product, $product, $channel, $config, $user, $scodeModel, $oscode, $bid;
		login_required();
		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){

			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
			'bid'		=> $bidprice,			//起始價格
			'bida'		=> $bida,				//起始價格
			'bidb' 		=> $bidb,				//結束價格
			'type'		=> $paytype,			//下標類型
			'autobid'	=> $auto,				//自動下標
		);

		$tpl->assign('paydata',$paydata);

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid=(empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		$behav=$_REQUEST['behav'];

		//用戶可否下標 AARONFU
		$get_user = $user->get_user($userid);
		$tpl->assign('dream_enable',$get_user['dream_enable']);
		$tpl->assign('type',$type);
				
		$ret=null;
		if(empty($productid) ){
			if($json=='Y'){
				echo json_encode(getRetJSONArray(-3,'商品編號為空 !!','MSG'));
				exit;
			}else{
				return_to('site/product');
			}
		}else{
			// 商品資料
			$get_product = $dream_product->get_dream_product_info($productid);

			if(!$get_product){
                if($json=='Y'){
				  echo json_encode(getRetJSONArray(-4,'找不到商品資料 !!','MSG'));
				  exit;
				}
			}
			
			// 增加商品點擊數 By Thomas 20190508
            // 2020/01/31 暫停紀錄 By Thomas
			// $product->add_prod_click($productid,1);

		    //收藏資料 : 20190508 暫時disable By Thomas
			// $collect = $user->getCollectProdList($userid, $productid, 'S', '');
			// $get_product['collect'] = $collect['record'][0];

			$ret=getRetJSONArray(1,'OK','JSON');
            $get_product['use_scode'] = false;
			$msg='';
			$get_product['now'] = (int)$get_product['now'];

			// 抓source IP
			$src_ip = getUserIP();

            // 抓user_agent
			// $user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			// $is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if($json=='Y'){
                $ret['retObj']=$get_product;
            } else {
                if(!empty($get_product['thumbnail2'])){
				  $meta['image'] = $get_product['thumbnail2'];
                }elseif (!empty($get_product['thumbnail_url'])){
				  $meta['image'] = $get_product['thumbnail_url'];
                }
            }
			$sgift=0;
			if($json=='Y'){

            } else {
                $tpl->assign('sgift_num', $sgift);
            }

			/* Update By Thomas 2015214
			// 如果有經由qrcode過來 就贈送過殺價券
			if(!empty($userid) && !empty($productid) && $behav=="q"){
				$oscode_num0 = $scodeModel->get_oscode($productid, $userid);
				//查詢是否有贈送殺價券活動
				$scode_promote = $scodeModel->scode_promote($productid,'q');
				$info['spid'] = $scode_promote['spid'];
				$info['behav'] = 'qrcode_reg';
				$info['productid'] = $productid;
				$info['userid'] = $userid;
				$info['onum'] = $scode_promote['onum'];

                // 如果沒送過 且有活動 就贈送
				if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){
				  for($j = 0;$j < $info['onum']; ++$j){
					  $scodeModel->add_oscode($info);
				  }
				  $scodeModel->set_scode_promote($info);
				  $msg="恭喜您獲得 ".$info['onum']."張 ".$get_product['name']." 殺價券";
				}
				// Add End
			}
            */
			
			//取最新得標者
			$bid_winner = $dream_product->get_bid_winner($productid);
			if (!empty($bid_winner)){
				$bid_winner['thumbnail_url']=empty($bid_winner['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bid_winner['thumbnail_url'];
			}
			$tpl->assign('bid_winner', $bid_winner);
			
			//取最新五筆出價商品紀錄
			$latest_five_bid = $dream_product->get_history_detail($productid);
			$latest_bid = $latest_five_bid;
			$tpl->assign('latest_bid', $latest_bid);

			//取得最新使用者最後出價商品資料
			$latest_user_bid = $dream_product->get_user_history_detail($productid,$userid);
			$tpl->assign('latest_user_bid', $latest_user_bid);
			
			//目前中標
			$bidded = $product->get_bidded($productid);
			$num_bids_gap = $product->getNumBidsGap($productid);


			if($bidded['userid'] <> ""){
				preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
				$src_ip[0][1] = "***";
				$src_ip[0][2] = "***";
				$all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				error_log("[c/product/saja] src_ip: ".json_encode($all_src_ip));
				$bidded['src_ip'] = $all_src_ip;
			}
			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			// $redis = getRedis('','');
			// $redis->set('PROD:'.$productid.':BIDDER',$bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);
		}

		// Update By AARONFU 20190917
		$tpl->set_title($get_product['name']);

        if(!empty($msg))
			$tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);

		if(!empty($userid)){
			$bidget_count = $user->pay_get_product_count($userid);
			$user_profile=$user->get_user_profile($userid);
		}else{
			$bidget_count = 0;
		}

		// 限定商品資格
		// checkCode=1 才可下標 
		// checkMsg 不可下標時顯示的訊息
		$retCheck=array();
		$retCheck['checkCode'] = 1;
		$retCheck['checkMsg'] = "";
		if ($get_product['limitid']=="7" || $get_product['limitid']=="21") {
			$retCheck = $product->new_hand_rule($productid,$userid,"1",$get_product['limitid']);
		}
		$err_limit = 0;
		if (!empty($get_product['limitid'])) {
			if ($get_product['kind'] == 'small') {
				$err_limit = ((int)$bidget_count>=(int)$get_product['mvalue'])? 1 : 0;
			}else if($get_product['kind'] == 'big'){
				$err_limit = ((int)$bidget_count<=(int)$get_product['svalue'])? 1 : 0;
			}
			if ($err_limit == 1) {
				$retCheck['checkCode'] = 3;
				$retCheck['checkMsg'] = "不符合{$get_product['plname']}商品資格";
			}
		}
		
		// Add By Thomas 2020/01/31 
		// 必須先填寫收件人姓名/郵遞區號/地址/電話後　才能下標圓夢商品
        if(empty($user_profile['address']) || 
		   empty($user_profile['addressee']) || 
		   empty($user_profile['area']) || 
		   empty($user_profile['rphone'])) {
			 $retCheck['checkCode'] = 4;
			 $retCheck['checkMsg'] = "下標圓夢商品前請先詳填收件人資料 !";   	
		}

		$tpl->assign('bidget_count', $bidget_count);
		$tpl->assign('retCheck', $retCheck);
		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);

		if($json=='Y'){
			$ret['retObj']['dream_enable']=$get_user['dream_enable'];
			$x= json_encode($ret);
			echo $x;
			exit;
		}else{
			$tpl->render("dream_product","saja",true);
		}

	}

	//商品清單
	public function product_list(){
		global $tpl, $dream_product, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$pcid = empty($_POST['pcid']) ? htmlspecialchars($_GET['pcid']) : htmlspecialchars($_POST['pcid']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if(empty($pcid)){
			$ret = getRetJSONArray(-1,'NO_PCID','MSG');
			echo json_encode($ret);
			exit;
		}

		if(empty($p) || $p <= 0){
			$p = 1;
		}

		$product_list = $dream_product->dream_product_list($pcid, $p);

		if($product_list){
			if($json=='Y'){
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product_list;
			}else{
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product_list['table']['record'];
				$ret['retObj']['page'] = $product_list['table']['page'];
				unset($product_list['table']['page']);
			}
		}else{
			if($json=='Y'){
				$ret=getRetJSONArray(1,'NO_DATA','MSG');
				$ret['retObj']['data']['table']['record'] = array();
				$ret['retObj']['data']['table']['page'] = '';
			}else{
				$ret=getRetJSONArray(1,'NO_DATA','MSG');
				$ret['retObj']['data'] = array();
				$ret['retObj']['page'] = '';
			}
		}
		echo json_encode($ret);
		exit;
	}
	
	//商品資料
	public function product_info(){
		global $tpl, $dream_product, $config, $user, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);
		//用戶可否下標或兌換 AARONFU
		$get_user = $user->get_user($userid);
		$tpl->assign('dream_enable',$get_user['dream_enable']);
		if(empty($userid)){
			$ret = getRetJSONArray(-1,'NO_AUTH_ID','MSG');
			echo json_encode($ret);
			exit;
		}

		if(empty($productid)){
			$ret = getRetJSONArray(-2,'NO_PRODUCTID','MSG');
			echo json_encode($ret);
			exit;
		}

		//取圓夢商品資訊
		$product_info = $dream_product->get_dream_product_info($productid);
		$product_info['agree'] = "上傳商品價值不得大於該檔圓夢金額、違者視同放棄";
		$ret=getRetJSONArray(1,'OK','LIST');
		if($product_info == false){
			$ret['retObj']['product_info'] = array();
		}else{
			$ret['retObj']['product_info'] = $product_info;

			// checkCode=1 才可下標 
			// checkMsg 不可下標時顯示的訊息
			$retCheck['checkCode'] = 1;
			$retCheck['checkMsg'] = "";
			$bidget_count = $user->pay_get_product_count($userid);

			// 新手限定一次一種商品
			if ($product_info['limitid']=="7" || $product_info['limitid']=="21") {
				$retCheck = $product->new_hand_rule($productid,$userid,"1", $product_info['limitid']);
			}
			// 限定商品資格
			$err_limit = 0;
			if (!empty($product_info['limitid'])) {
				if ($product_info['kind'] == 'small') {
					$err_limit = ($bidget_count>=$product_info['mvalue'])? 1 : 0;
				}else if($product_info['kind'] == 'big'){
					$err_limit = ($bidget_count<=$product_info['svalue'])? 1 : 0;
				}
				if ($err_limit == 1) {
					$retCheck['checkCode'] = 3;
					$retCheck['checkMsg'] = "不符合{$product_info['plname']}商品資格";
				}
			}
			
			$ret['retCheck'] = $retCheck;

			//取最新得標者
			$bid_winner = $dream_product->get_bid_winner($productid);
			if(!empty($bid_winner)){
				$ret['retObj']['bid_winner'] = $bid_winner;
				$ret['retObj']['bid_winner']['thumbnail_url']=empty($bid_winner['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bid_winner['thumbnail_url'];
			}else{
				$ret['retObj']['bid_winner'] = (object)array();
			}

			//取最新五筆出價商品紀錄
			$latest_five_bid = $dream_product->get_history_detail($productid);
			if(!empty($latest_five_bid)){
				$ret['retObj']['latest_bid'] = $latest_five_bid;
			}else{
				$ret['retObj']['latest_bid'] = array();
			}

			//取得最新使用者最後出價商品資料
			$latest_user_bid = $dream_product->get_user_history_detail($productid,$userid);
			if(!empty($latest_user_bid)){
				$ret['retObj']['latest_user_bid'] = $latest_user_bid;
			}else{
				$ret['retObj']['latest_user_bid'] = array();
			}
		}

		if($json == 'Y'){
			$ret['retObj']['dream_enable']=$get_user['dream_enable'];
			echo json_encode($ret);
		}else{
			$tpl->assign('product_info', $product_info);
			$tpl->assign('bid_winner', $bid_winner);
			$tpl->assign('latest_bid', $latest_five_bid);
			$tpl->assign('latest_user_bid', $latest_user_bid);
			$tpl->assign('noback', 'Y');
			$tpl->set_title('');
			$tpl->render("dream_product","saja",true);
		}
	}

	//下標殺價商品的資料
	public function sajabid(){

		global $tpl, $member, $product, $scodeModel, $user, $oscode, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){
			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
			'bid'		=> $bidprice,			//起始價格
			'bida'		=> $bida,				//起始價格
			'bidb' 		=> $bidb,				//結束價格
			'type'		=> $paytype,			//下標類型
			'autobid'	=> $auto,				//自動下標
		);

		$tpl->assign('paydata',$paydata);

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid=(empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		
		$dpname = empty($_POST['dpname']) ? htmlspecialchars($_GET['dpname']) : htmlspecialchars($_POST['dpname']);
		$dpcontent = empty($_POST['dpcontent']) ? htmlspecialchars($_GET['dpcontent']) : htmlspecialchars($_POST['dpcontent']);
		$dpaddress = empty($_POST['dpaddress']) ? htmlspecialchars($_GET['dpaddress']) : htmlspecialchars($_POST['dpaddress']);
		$dppic = empty($_POST['thumbnail']) ? htmlspecialchars($_GET['thumbnail']) : htmlspecialchars($_POST['thumbnail']);

		$ret=null;
		if(empty($productid) ){
			return_to('site/dream_product');
		}else{

			//商品資料
			$get_product = $product->getProductById($productid);

		  //收藏資料
			$collect = $user->getCollectProdList($userid, $productid, 'S', '');
			$get_product['collect'] = $collect['record'][0];

			$ret=getRetJSONArray(1,'OK','JSON');
			$get_product['use_scode'] = false;
			$msg='';
			$get_product['offtime'] = (int)$get_product['offtime'];
			$get_product['now'] = (int)$get_product['now'];
			$get_product['dpname'] = $dpname;
			$get_product['dpcontent'] = $dpcontent;
			$get_product['dpaddress'] = $dpaddress;
			$get_product['dppic'] = $dppic;
			
			// 抓source IP
			$src_ip = getUserIP();

			// 抓user_agent
			$user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			$is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if(!empty($get_product['thumbnail'])){
				$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail'];
			}elseif(!empty($get_product['thumbnail_url'])){
				$meta['image'] = $get_product['thumbnail_url'];
			}

			$sgift=0;
			$tpl->assign('sgift_num', $sgift);

			//殺幣數量
			$get_spoint = $member->get_spoint($userid);

			if(!$get_spoint){
				$get_spoint=array("userid"=>$userid, "amount"=>0);
			}else{
				$get_spoint['amount'] = sprintf("%1\$u",$get_spoint['amount']);
			}
			$tpl->assign('spoint', $get_spoint);

			//目前中標
			$bidded=$this->getCurrWinnerInfo($productid);
			$num_bids_gap = $product->getNumBidsGap($productid);

			if($bidded['userid'] <> ""){
				preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
				$src_ip[0][1] = "***";
				$src_ip[0][2] = "***";
				$all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				$bidded['src_ip'] = $all_src_ip;
			}

			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			$redis = getRedis('','');
			$redis->set('PROD:'.$productid.':BIDDER',$bid_name);
			$tpl->assign('bidded', $bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);

		}

		$tpl->set_title('');

		if(!empty($msg))
			$tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);

		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);

		if($json == 'Y'){
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LSIT";
			$data['retObj']['product'] = (!empty($get_product)) ? $get_product : new stdClass;
			$data['retObj']['paydata'] = (!empty($paydata)) ? $paydata : new stdClass;
			$data['retObj']['spoint'] = (!empty($get_spoint)) ? $get_spoint : new stdClass;
			echo json_encode($data);
		}else{
			$tpl->render("dream_product","sajabid",true);
		}

	}

	//下標殺價商品支付
	public function sajatopay() {

		global $tpl, $product, $scodeModel, $user, $oscode, $bid, $member;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$bida = empty($_POST['bida']) ? htmlspecialchars($_GET['bida']) : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? htmlspecialchars($_GET['bidb']) : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? htmlspecialchars($_GET['auto']) : $_POST['auto'];
		$paytype = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : $_POST['type'];
		$count = empty($_POST['count']) ? htmlspecialchars($_GET['count']) : $_POST['count'];
		$fee_all = empty($_POST['fee_all']) ? htmlspecialchars($_GET['fee_all']) : $_POST['fee_all'];
		$total_all = empty($_POST['total_all']) ? htmlspecialchars($_GET['total_all']) : $_POST['total_all'];
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : $_POST['productid'];
		$userid = empty($_SESSION['auth_id']) ? $_POST['auth_id']:$_SESSION['auth_id'];
		$type = empty($_SESSION['auth_type']) ? $_POST['auth_type']:$_SESSION['auth_type'];
		$behav = $_REQUEST['behav'];
		$bidprice = '';
		$channelid = empty($_POST['channelid']) ? $_GET['channelid'] : $_POST['channelid'];
		$dpname = empty($_POST['dpname']) ? htmlspecialchars($_GET['dpname']) : htmlspecialchars($_POST['dpname']);
		$dpcontent = empty($_POST['dpcontent']) ? htmlspecialchars($_GET['dpcontent']) : htmlspecialchars($_POST['dpcontent']);
		$dpaddress = empty($_POST['dpaddress']) ? htmlspecialchars($_GET['dpaddress']) : htmlspecialchars($_POST['dpaddress']);
		$dppic = empty($_POST['dppic']) ? htmlspecialchars($_GET['dppic']) : htmlspecialchars($_POST['dppic']);

		
		if($json != 'Y'){
			$udata = empty($_POST) ? $_GET : $_POST;
		}else{
			$paytype = empty($_POST['paytype']) ? htmlspecialchars($_GET['paytype']) : $_POST['paytype'];

			$udata = array(
              				'productid'		=> $productid,
              				'bida'			=> $bida,
              				'bidb' 			=> $bidb,
              				'total_all' 	=> $total_all,
              				'count' 		=> $count,
              				'type'			=> $paytype,
              				'fee_all'		=> $fee_all,
              				'json'			=> $json,
              				'fun'			=> 'product',
              				'act'			=> 'sajatopay',
              				'channelid'		=> $channelid
			);
		}

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){
			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
                			'bid'		=> $bidprice,			//起始價格
                			'bida'		=> $bida,				//起始價格
                			'bidb' 		=> $bidb,				//結束價格
                			'type'		=> $paytype,			//下標類型
                			'autobid'	=> $auto,				//自動下標
		);
		error_log("[c/product/saja] paydata : ".json_encode($paydata));

		$tpl->assign('paydata',$paydata);

		$ret=null;
		if(empty($productid) ){
			return_to('site/product');
		}else{

			//S碼 現有可用總數
			$scode_num = $scodeModel->get_scode($userid);
			// $scode_num = 5;
			$tpl->assign('scode_num', $scode_num);

			// S碼 已用總數
			$scode_use_num = abs($scodeModel->used_sum($userid));
			if($json=='Y'){
				$ret['retObj']['scode_use_num']=$scode_use_num;
			}else{
				$tpl->assign('scode_use_num', $scode_use_num);
			}

			//商品資料
			// $get_product = $product->get_info($productid);
			$get_product = $product->getProductById($productid);

			$ret=getRetJSONArray(1,'OK','JSON');
			$get_product['use_scode'] = false;
			$msg='';
			$get_product['offtime'] = (int)$get_product['offtime'];
			$get_product['now'] = (int)$get_product['now'];
			$get_product['dpname'] = $dpname;
			$get_product['dpcontent'] = $dpcontent;
			$get_product['dpaddress'] = $dpaddress;
			$get_product['dppic'] = $dppic;
			
			// 抓source IP
			$src_ip = getUserIP();

			// 抓user_agent
			$user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			$is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if(!empty($get_product['thumbnail'])){
			  $meta['image'] = IMG_URL.APP_DIR.'/images/site/product/'.$get_product['thumbnail'];
			}elseif (!empty($get_product['thumbnail_url'])){
			  $meta['image'] = $get_product['thumbnail_url'];
			}

			$sgift=0;
			$tpl->assign('sgift_num', $sgift);

			// 可用殺價券數量
			$oscode_num =  $oscode->get_prod_oscode($productid, $userid);
			if(!$oscode_num){
			  $oscode_num=0;
			}
			$tpl->assign('oscode_num', $oscode_num);

			//殺幣數量
			$get_spoint = $member->get_spoint($userid);
			if(!$get_spoint){
			  $get_spoint=array("userid"=>$this->userid, "amount"=>0);
			}else{
				$get_spoint['amount'] = sprintf("%1\$u",$get_spoint['amount']);
			}
			$tpl->assign('spoint', $get_spoint);

			//目前中標
			$bidded=$this->getCurrWinnerInfo($productid);
			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			$redis = getRedis('','');
			$redis->set('PROD:'.$productid.':BIDDER',$bid_name);
			$tpl->assign('bidded', $bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);

		}
		$tpl->set_title('');

		if(!empty($msg))
		  $tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);
		$udata['payall'] = getBidTotalFee($get_product, $paytype, $bida, $bida, $bidb, 1);
		$tpl->assign('topay',$udata);
		$tpl->assign('type',$type);
		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);
		error_log("[c/product/saja] udata : ".json_encode($udata));

		if($json == "Y"){
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LSIT";
			$data['retObj']['product']= (!empty($get_product)) ? $get_product : new stdClass;
			$data['retObj']['paydata'] = (!empty($paydata)) ? $paydata : new stdClass;
			$data['retObj']['spoint'] = (!empty($get_spoint)) ? $get_spoint : new stdClass;
			$data['retObj']['scode_num'] = $scode_num;
			$data['retObj']['oscode_num'] = $oscode_num;
			$data['retObj']['topay'] = (!empty($udata)) ? $udata : new stdClass;
			echo json_encode($data);
		}else{
			$tpl->render("dream_product","sajatopay",true);
		}

	}

	//取得得標者
	public function getCurrWinnerInfo($productid) {

        global $product, $dream_product;
        // $CurrWinnerInfo=$product->get_bidded($productid);
		$CurrWinnerInfo = $dream_product->get_bid_winner($productid);

        if(!empty($CurrWinnerInfo['userid'])){
            if(empty($CurrWinnerInfo['thumbnail_url']) && !empty($CurrWinnerInfo['thumbnail_file'])){
                $CurrWinnerInfo['thumbnail_file']=BASE_URL.APP_DIR.'/images/site/headimgs/'.$CurrWinnerInfo['thumbnail_file'];
            }
            return $CurrWinnerInfo;
        }else{
            return false;
        }

	}

    //取得得標者
	public function winner() {
        global $tpl, $product, $user;

		//設定 Action 相關參數
		set_status($this->controller);

        $productid=htmlspecialchars($_GET['productid']);
		if(empty($productid)){
		   $productid=htmlspecialchars($_POST['productid']);
		}

        $r=array('name'=>'',
                 'comefrom'=>'',
                 'productid'=>'',
                 'second'=>0,
                 'status'=>1, //狀態碼(1:更新得標者, 2:無人得標)
				 'thumbnail_file'=>'',
				 'thumbnail_url'=>''
                );

        if(empty($productid)) {
           echo json_encode($r);
           return ;
        }

        $r['productid'] = $productid;
        $redis = getRedis('','','');
        if($redis) {
            if($redis->exists("PROD:".$productid.":BIDDER")) {
               // redis key 存在 -> 即使撈出來的值是空的(表示目前沒有人得標) 也直接回傳
               	$rdata = $redis->get("PROD:".$productid.":BIDDER");
				$winnerdata = json_decode($rdata,TRUE);
				$r['name'] = $winnerdata['name'];
				$r['thumbnail_file'] = $winnerdata['thumbnail_file'];
				$r['thumbnail_url'] = $winnerdata['thumbnail_url'];
				$r['status'] = $winnerdata['status'];
         
                error_log("[c/product/winner] redis PROD:".$productid.":BIDDER->".$r['name'].":STATUS->".$r['status']);
            } else {
               // redis key 不存在 -> 去DB撈並更新redis後回傳
               $bidded = $product->get_bidded($productid);
               error_log("[c/product/winner] bidded : ".json_encode($bidded));
               if($bidded && $bidded['shid']>0) {
					if($bidded['userid'] != 0) {
						$userdata = $user->get_user_profile($bidded['userid']);
						$r['thumbnail_file'] = $userdata['thumbnail_file'];
						$r['thumbnail_url'] = $userdata['thumbnail_url'];
					}					
                    if($bidded['src_ip']) {
                       $r['comefrom'] = maskIP($bidded['src_ip']);
                    }
                    if (empty($bidded['nickname'])) {
	                	$r['name'] = urldecode($bidded['nickname']);
	                	$r['status'] = 2; //狀態碼(1:更新得標者, 2:無人得標)
               	 	}
                    // $r['name'] = empty($bidded['nickname'])?' ':urldecode($bidded['nickname']);
                    $r['name'] = urldecode($bidded['nickname']);
               }
               $redis->set("PROD:".$productid.":BIDDER",json_encode($r));
            }
        } else {
            //目前得標
            $bidded = $product->get_bidded($productid);
            if($bidded && $bidded['shid']>0) {
				if($bidded['userid'] != 0) {
					$userdata = $user->get_user_profile($bidded['userid']);
					$r['thumbnail_file'] = $userdata['thumbnail_file'];
					$r['thumbnail_url'] = $userdata['thumbnail_url'];
				} 				
                if($bidded['src_ip']) {
                   $r['comefrom'] = maskIP($bidded['src_ip']);
                }
                if (empty($bidded['nickname'])) {
                	$r['name'] = urldecode($bidded['nickname']);
                	$r['status'] = 2; //狀態碼(1:更新得標者, 2:無人得標)
                }
            }
            error_log("[c/product/winner] DB PROD:".$productid.":BIDDER->".$r['name'].":STATUS->".$r['status']);
        }
		$r['second'] = date("s");
		echo json_encode($r);
	}

	//出價商品紀錄
	public function get_history_detail_info(){
		
        global $tpl, $dream_product, $product, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$shdid = empty($_POST['shdid']) ? htmlspecialchars($_GET['shdid']) : htmlspecialchars($_POST['shdid']);

		//取特定出價商品紀錄
		$ret = $dream_product->get_history_detail_info($productid, $shdid);

		echo json_encode($ret);
		exit;
	}
}
