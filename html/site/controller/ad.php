<?php
/**
 * Ad Controller 廣告相關
 */

class Ad 
{
    public $controller = array();
	public $params = array();
    public $id;
	public $userid = '';
	
	public function __construct() {
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
	}
	
	/**
     * Default home page.
     */
	public function home()
	{
		global $tpl, $ad;

		$kind 	= empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);

		//設定 Action 相關參數
		set_status($this->controller);

		//建立Redis連線
		$redis=getRedis();
		$redis=false;
		if($redis){
			//取得廣告資料
			$rdata='';
			$rkey = "SiteAdBannerList".$kind;
			$rdata = $redis->get($rkey);
			if($rdata) {
			   $ad_list = json_decode($rdata,TRUE);   	
			} else {
			   $ad_list = $ad->get_ad_list($kind, 'P');
               if(!empty($ad_list) && is_array($ad_list)) {
				   
				//計算即將下架的廣告秒數
				foreach($ad_list as $res){
					$offtime_array[] = strtotime($res['offtime']);
				}
				//resdis有效時間(秒))
				$rexpire_time = min($offtime_array) - strtotime('now');

				$redis->set($rkey, json_encode($ad_list));
				$redis->expire($rkey, $rexpire_time);
               }  			   
			}
		} else {
			//取得廣告資料
			$ad_list = $ad->get_ad_list($kind, 'P');
        }
		
		// Add By Thomas 2020/01/21
		// 如果有$_SESSION["auth_id"], 網址內也有指定 userid=$_USER_ID, 則將$_USER_ID代換為$_SESSION["auth_id"]
		$userid=$_SESSION["auth_id"];
		if(!empty($userid)) {
			foreach($ad_list as $res){
				   $res['action']= 	str_replace("$_USER_ID",$userid,$res['action']);
				   $res['action_a']= str_replace("$_USER_ID",$userid,$res['action_a']);
				   $res['action_i']=  str_replace("$_USER_ID",$userid,$res['action_i']);
				   $res['action_list']['url']= str_replace("$_USER_ID",$userid, $res['action_list']['url']);
			}
	    }

		$data['retCode'] = 1;
		$data['retMsg'] = "取得資料 !!";
		$data['retType'] = "MSG";
		$data['retObj']['data'] = $ad_list;
		error_log("[c/ad/home] ret : ".json_encode($ad_list));
		echo json_encode($data);
	}
	
	
	/*
     * 首頁彈窗
     */
	 public function popups()
	{
		global $tpl, $ad;

		//設定 Action 相關參數
		set_status($this->controller);

		//建立Redis連線
		$redis=getRedis();
		$data=array();
		$ad_list="[]";
		if($redis){
			//取得廣告資料
			$rdata='';
			$rkey = "SiteHomeAdPopupsList";
			$rdata = $redis->get($rkey);
			if($rdata) {
			   $ad_list = json_decode($rdata,TRUE);   	
			} else {
			   $ad_list = $ad->get_ad_popups_list(5, 'P');
               if(!empty($ad_list) && is_array($ad_list)) {
				   
				//計算即將下架的廣告秒數
				foreach($ad_list as $res){
					$offtime_array[] = strtotime($res['offtime']);
				}
				//resdis有效時間(秒))
				$rexpire_time = min($offtime_array) - strtotime('now');

				$redis->set($rkey, json_encode($ad_list));
				$redis->expire($rkey, $rexpire_time);
               }  			   
			}
		} else {
			//取得廣告資料
			$ad_list = $ad->get_ad_popups_list(5, 'P');
        }

        // Add By Thomas 20191127
		// 多組廣告資料時, array做隨機排列後回傳給APP (APP僅取第一筆顯示)
		if(is_array($ad_list) && count($ad_list)>1) {
		   shuffle($ad_list);	
		}
		
		$show = getRandomNum()%3+1;
		// 100% 出現
		$show=3;
		if($show==3) {
		  $data['retCode'] = 1;
		  $data['retMsg'] = "OK";
		  $data['retType'] = "MSG";	
		  $data['retObj']['data'] = $ad_list;  
		} else {
		  $data['retCode'] = 1;
		  $data['retMsg'] = "OK";
		  $data['retType'] = "MSG";	
		  $data['retObj']['data'] = "[]";  	
		}
		echo json_encode($data);
	}

    /**
     * 鯊魚商城廣告
     */
	public function mallad()
	{
		global $tpl, $ad;

		$arrCond=array();
		$arrCond['acid'] 	= empty($_POST['acid']) ? htmlspecialchars($_GET['acid']) : htmlspecialchars($_POST['acid']);
		$arrCond['adid'] 	= empty($_POST['adid']) ? htmlspecialchars($_GET['adid']) : htmlspecialchars($_POST['adid']);
		$arrCond['esid'] 	= empty($_POST['esid']) ? htmlspecialchars($_GET['esid']) : htmlspecialchars($_POST['esid']);
		$arrCond['epcid'] 	= empty($_POST['epcid']) ? htmlspecialchars($_GET['epcid']) : htmlspecialchars($_POST['epcid']);
		//設定 Action 相關參數
		set_status($this->controller);
		
		$ad_list = $ad->get_mallad_list($arrCond);

		$data['retCode'] = 1;
		$data['retMsg'] = "OK";
		$data['retType'] = "MSG";
		$data['retObj']['data'] = $ad_list;
		echo json_encode($data);
	}	
	
	/*
	public function popups()
	{
		global $tpl, $ad;

		//設定 Action 相關參數
		set_status($this->controller);

        $data=array();
		$show = getRandomNum()%4+1;
		if($show==3)  {
			//建立Redis連線
			$redis=getRedis();
			if($redis){
				//取得廣告資料
				$rdata='';
				$rkey = "SiteHomeAdPopupsList";
				$rdata = $redis->get($rkey);
				if($rdata) {
				   $ad_list = json_decode($rdata,TRUE);   	
				} else {
				   $ad_list = $ad->get_ad_popups_list(5, 'P');
				   if(!empty($ad_list) && is_array($ad_list)) {
					   
					//計算即將下架的廣告秒數
					foreach($ad_list as $res){
						$offtime_array[] = strtotime($res['offtime']);
					}
					//resdis有效時間(秒))
					$rexpire_time = min($offtime_array) - strtotime('now');

					$redis->set($rkey, json_encode($ad_list));
					$redis->expire($rkey, $rexpire_time);
				   }  			   
				}
			} else {
				//取得廣告資料
				$ad_list = $ad->get_ad_popups_list(5, 'P');
			}
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "MSG";
			$data['retObj']['data'] = $ad_list;
		} else {
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "MSG";
			$data['retObj']['data'] = "[]";
		}
		echo json_encode($data);
	}	
	*/

}