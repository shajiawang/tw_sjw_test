<?php
/*
 * Oscode Controller 限定S碼紀錄
 */

include_once(BASE_DIR ."/model/oscode.php");
include_once(LIB_DIR."/helpers.php");

class Oscode {

  public $controller = array();
	public $params = array();
  public $id;
	public $userid = '';

	public function __construct() {

    global $tpl, $oscModel;

		$this->userid = (empty($_SESSION['auth_id'])) ? $_POST['auth_id']:$_SESSION['auth_id'];
		$oscModel = new OscodeModel;
	}


	/*
   * Default home page.
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$type				varchar				動作型態 (now:可用總數, accept:收取總數, active:序號收取總數, used:已用總數, expired:過期總數)
	 *	$kind				varchar				顯示型態 (get:取得殺價卷, use:使用殺價卷)
	 *	$p					int					分頁編號
   */
	public function home() {

    global $tpl, $oscModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

    $json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
    $type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
    $kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
    $p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if($json=='Y'){

			if(empty($kind)){
				$ret;
				switch ($type){
					case 'now':
        						 $ret=getRetJSONArray(1, 'OK', 'JSON');
        						 $ret['retObj']=array("type"=>"now","amount"=>$oscModel->get_oscode($this->userid));
        						 break;
					case 'accept':
            						 $ret=getRetJSONArray(1, 'OK', 'JSON');
            						 $ret['retObj']=array("type"=>"accept","amount"=>$oscModel->oscode_accept_sum($this->userid));
            						 break;
					case 'active':
            						 $ret=getRetJSONArray(1, 'OK', 'JSON');
            						 $ret['retObj']=array("type"=>"active","amount"=>$oscModel->oscode_active_sum($this->userid));
            						 break;
					case 'used':
          						 $ret=getRetJSONArray(1, 'OK','JSON');
          						 $ret['retObj']=array("type"=>"used","amount"=>$oscModel->oscode_used_sum($this->userid));
          						 break;
					case 'expired':
            						 $ret=getRetJSONArray(1, 'OK', 'JSON');
            						 $ret['retObj']=array("type"=>"expired","amount"=>$oscModel->get_oscode_expired($this->userid));
            						 break;
					default:
      						$ret=getRetJSONArray(1, 'OK', 'LIST');
      						$ret['retObj']['data'] = array();
      						$ret['retObj']['page'] = array();

      						$page['rec_start'] = 0;
      						$page['totalcount'] = 0;
      						$page['totalpages'] = 1;
      						$page['perpage'] = 50;
      						$page['page'] = 1;
      						$page['item'] = array(array("p" => 1 ));

      						$ret['retObj']['page'] = $page;
      						$ret['getcount'] = 0;
      						$ret['usecount'] = 0;
      						break;
        }

			}else{
				// 取得殺價卷
				$get_list = $oscModel->get_oscode_list($this->userid);
				// 使用殺價卷
				$use_list = $oscModel->get_oscode_expired_list($this->userid);

				if($kind == "get"){
					//現有可用清單
					$scode_info	= $get_list;
				}elseif ($kind == "use"){
					//過期清單
					$scode_info = $use_list;

					foreach($scode_info['table']['record'] as $tk => $tv){
						if($tv['used'] == "N"){
							$scode_info['table']['record'][$tk]['used'] = "O";
						}
					}
				}else{
					$scode_info = "";
				}

				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['getcount'] = empty($get_list['table']['page']['totalcount']) ? 0 : $get_list['table']['page']['totalcount'];
				$ret['usecount'] = empty($use_list['table']['page']['totalcount']) ? 0 : $use_list['table']['page']['totalcount'];
				//判斷清單是否存在
				if($scode_info){
					if((empty($p)) || ($p=='') || ($p < 0) || ($p > $scode_info['table']['page']['totalpages'])){
						$ret['retObj']['data'] = array();
					}else{
						$ret['retObj']['data'] = $scode_info['table']['record'];
					}
					$ret['retObj']['page'] = $scode_info['table']['page'];
				}else{
					$ret['retObj']['data'] = array();

					$page['rec_start'] = 0;
					$page['totalcount'] = 0;
					$page['totalpages'] = 1;
					$page['perpage'] = 50;
					$page['page'] = 1;
					$page['item'] = array(array("p" => 1 ));

					$ret['retObj']['page'] = $page;
				}

			}
			echo json_encode($ret);
			exit;

		}else{

			//現有可用清單
			$now_list = $oscModel->get_oscode_list($this->userid);
			$scode_info['now_list'] = $now_list['table']['record'];
			$scode_info['now_list_count'] = empty($now_list['table']['page']['total']) ? 0 : $now_list['table']['page']['total'];

			//過期清單
			$expired_list = $oscModel->get_oscode_expired_list($this->userid);
			$scode_info['expired_list'] = $expired_list['table']['record'];
			$scode_info['expired_list_count'] =  empty($expired_list['table']['page']['total']) ? 0 : $expired_list['table']['page']['total'];

			$tpl->assign('scode_info', $scode_info);
			$tpl->set_title('');
			$tpl->render("oscode","home",true);
		}

	}


	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

    $table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';

		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;

  }


	/*
	 * S碼-收取明細
	 */
	public function serial() {

    global $tpl, $oscModel;

		//設定 Action 相關參數
		set_status($this->controller);

    login_required();

		$get_list = $oscModel->oscode_serial_list($this->userid);
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}

		$tpl->assign('page_content', $page_content);
		$tpl->set_title('');
		$tpl->render("oscode","serial",true);

	}


	/*
	 * 限定S碼-收取明細(Frank.Kao -- 11/13)
	 */
	public function accept() {

    global $tpl, $oscModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$this->userid=empty($_POST['auth_id'])?$_SESSION['auth_id']:$_POST['auth_id'];

		$json = empty($_POST['json']) ? htmlspecialchars($_POST['json']) : htmlspecialchars($_POST['json']);
		error_log("json:".$json);

    if($json=='Y'){
		  // 僅統計可使用的殺價券數量
		  $get_list = $oscModel->oscode_available_list($this->userid);
		}else{
		  $get_list = $oscModel->oscode_accept($this->userid);
		}

		if($get_list){
			for($idx=0; $idx< count($get_list['table']['record']); ++$idx){
				$p = $get_list['table']['record'][$idx];
				$get_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$p['filename'];
				error_log("[c/oscode/accept]:".$get_list['table']['record'][$idx]['thumbnail_url']);
				$get_list['table']['record'][$idx]['offtime'] = (int)$p['offtime'];
			}
		}

		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
      if($get_list){
			  $ret['retObj']['data'] = $get_list['table']['record'];
			  $ret['retObj']['page'] = $get_list['table']['page'];
		  }
		  echo json_encode($ret);
		}else{
		  $tpl->assign('page_content', $page_content);
		  $tpl->set_title('');
		  $tpl->render("oscode","accept",true);
		}

	}


	/*
	 * 限定S碼-收取明細列表
	 */
	public function acceptlist() {

    global $tpl, $oscModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$productid=$_GET['productid'];

    if(empty($productid))
		  $productid=$_POST['productid'];

		$spid=$_GET['spid'];

    if(empty($spid))
		  $spid=$_POST['spid'];

		$get_list = $oscModel->oscode_accept_list($this->userid, $productid, $spid);
		$tpl->assign('row_list', $get_list);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
      if(!$get_list){
        $ret['retObj']['data'] = new stdClass();
        $ret['retObj']['page'] = new stdClass();
      }else{
        $ret['retObj']['data'] = $get_list['table']['record'];
        $ret['retObj']['page'] = $get_list['table']['page'];
      }
      echo json_encode($ret);
      exit;
		}else{
		  $tpl->assign('page_content', $page_content);
		  $tpl->set_title('');
		  $tpl->render("oscode","acceptlist",true);
		}

	}


	/*
	 * S碼-使用明細
	 */
	public function used() {

    global $tpl, $oscModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$get_list = $oscModel->oscode_used_list($this->userid);
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
			if(empty($get_list)){
			  $ret['retObj']['data'] = new stdClass();
			  $ret['retObj']['page'] = new stdClass();
		  }else{
			  $ret['retObj']['data'] = $get_list['table']['record'];
			  $ret['retObj']['page'] = $get_list['table']['page'];
		  }
			echo json_encode($ret);
      exit;
		}else{
			$tpl->assign('page_content', $page_content);
			$tpl->set_title('');
			$tpl->render("oscode","used",true);
		}

	}


	public function getNumOfAvailOscode() {

    global $oscModel;

		login_required();

		$productid=empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_POST['userid']) ? ($_SESSION['auth_id']) : htmlspecialchars($_POST['userid']);

		if(empty($userid)){
		  echo json_encode(getRetJSONArray(-1,"EMPTY USERID !!","MSG"));
		  exit;
		}

		if(empty($productid)){
		  echo json_encode(getRetJSONArray(-2,"EMPTY PRODUCTID !!","MSG"));
		  exit;
		}

		$ret=getRetJSONArray(1,'OK','JSON');
		$num=$oscModel->get_prod_oscode($productid, $userid);
		$ret['retObj']=array("userid"=>$userid, "amount"=>$num);
		echo json_encode($ret);

    exit;

	}


	/*
   * 	取得送殺價卷項目 - 廣告專用
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$userid				varchar				會員編號
   */
	public function getAdOscodeItem() {

    global $tpl, $oscModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

    $json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
    $userid = $this->userid;

		if(!empty($userid)){

			// 取得殺價卷活動
			$get_list = $oscModel->get_ad_oscode();

			// 統計當天已領取多少次數活動
			$get_user_conut = $oscModel->get_ad_oscode_user($userid);

			$ret = getRetJSONArray(1,'OK','MSG');

			if(!empty($get_list[0])){
				$ret['retObj']['spid'] = $get_list[0]['spid'];			// 檔次編號
				$ret['retObj']['name'] = $get_list[0]['name'];			// 檔次名稱
				$ret['retObj']['onum'] = $get_list[0]['onum'];			// 每次發行組數
			}

			$ret['retObj']['getcount'] = empty($get_user_conut['total_amount']) ? 0 : $get_user_conut['total_amount'];
			$ret['retObj']['totalcount'] = 10;							// 會員每日可取得組數

    }else{
			$ret=getRetJSONArray(-10001,'請先登入','MSG');
		}
		echo json_encode($ret);

		exit;

	}


	/*
   * 	送殺價卷 - 廣告專用
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$userid				varchar				會員編號
   */
	public function sendAdOscode() {

		global $tpl, $oscModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

    $json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
    $spid = empty($_POST['spid']) ? htmlspecialchars($_GET['spid']) : htmlspecialchars($_POST['spid']);

    $userid = $this->userid;

		if(!empty($userid)){

			if(!empty($spid)){

				// 統計當天已領取多少次數活動
				$get_user_conut = $oscModel->get_ad_oscode_user($userid);

				// 判斷當天已領取次數是否小於10次
				if($get_user_conut['total_amount'] < 10){

					// 查詢殺價卷
					$get_ad_scode = $oscModel->get_ad_scode($spid);

					if(!empty($get_ad_scode['spid'])){

            if($get_ad_scode['onum']>0){
              for($j=0;$j<$get_ad_scode['onum'];++$j){
								// 領取殺價卷
								$get_oscode = $oscModel->send_oscode($get_ad_scode,$userid);
							}
						}

						if(!empty($get_oscode) && ($get_oscode > 0)){
							$ret = getRetJSONArray(1,'OK','MSG');
							$ret['retObj']['oscodeid'] = $get_oscode;
						}else{
							$ret = getRetJSONArray(-10005,'領取失敗','MSG');
						}

					}else{
						$ret = getRetJSONArray(-10004,'領取失敗，此卷已送完','MSG');
					}

				}else{
					$ret = getRetJSONArray(-10003,'今天已到領取上限','MSG');
				}

			}else{
				$ret = getRetJSONArray(-10002,'領取序號有誤','MSG');
			}

		}else{
			$ret = getRetJSONArray(-10001,'請先登入','MSG');
		}
		echo json_encode($ret);

		exit;

	}


	/*
     * 	送殺價卷 - 付費下槱滿20次送卷
	 *	$productid			varchar				商品編號
     */
	public function sendBidOscode() {

		global $tpl, $oscModel, $bid;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		
		//取得當下時間
		$nowdate = date("Y-m-d");	//取得當前日期
		$nowh = date(H);	//取得當前小時
		// print_r($nowdate);exit;
		
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		
		//取得該商品付費下標滿20標的會員清單
		$get_bid_list = $bid->get_bid_user_list($productid);
		// print_r($get_bid_list);exit; 
		
		//判斷是有有可送卷清單&日期是否為2019-12-31
		if(!empty($get_bid_list) && (strtotime($nowdate) <= strtotime('2020-01-01'))){
			//統計送卷人數
			$count=0;
			//執行會員清單
			foreach($get_bid_list as $rk => $rv){
				//取得判斷是否有送過卷
				$get_send_list = $oscModel->get_oscode_lsit($rv['userid'],$productid);
				
				if(empty($get_send_list)){
					//取得該會員最近付費下標時間
					$get_history = $bid->get_bid_user_new($rv['userid'],$productid);
					$kind = date('H', strtotime($get_history['insertt']));
					// print_r($kind. '<br/>');
					
					//判斷送卷時段
					switch($kind){
						case ($kind < 22 ): //全部5張
							$num = 5;
							break;
						case ($kind < 23): //全部3張
							$num = 3;
							break;
						case ($kind < 24): //全部2張
							$num = 2;
							break;
						default: 
							$num = 0;
							break; 
					}
					
					//執行送卷
					$oscModel->send_oscode_lsit($rv['userid'],$productid,$num);
					$count++;
				}else{
					// $ret = getRetJSONArray(1,'已過送卷','MSG');
				}
				
			}
			$ret = getRetJSONArray(1,'送卷執行完成!! 送出人數 '.$count. '人','MSG');
			
		}else{
			$ret = getRetJSONArray(1,'無法執行送卷!!','MSG');
		}
		
		echo json_encode($ret);
		exit;
	}
	
	
}
