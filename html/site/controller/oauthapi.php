<?php
/*
 * Oauthapi Controller
 */

class Oauthapi {

	public $controller = array();
	public $params = array();
  public $id;


	/*
   * Default home page.
   */
	public function home() {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("user","home",true);

	}


  public function r() {

		global $db, $config, $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$data=array();
		// 從_to參數去判斷撈GET還是POST
		$_to = $_GET['_to'];

		if(empty($_to)){
		  $_to=$_POST['_to'];
		  $state=$_POST;
		}else{
		  $state=$_GET;
		}

		unset($state['fun']);
		unset($state['act']);
		unset($state['oauthapi/r/']);
		$browserType = "";

		if(!empty($state['_authby'])){
		  $browserType=$state['_authby'];
		  unset($state['_authby']);
		}else{
		  $browserType=browserType();
		}

		foreach($state as $k=>$v){
		  if(is_numeric($k))
		    unset($state[$k]);
		}

        $base_url = OAUTH_HOST;
		$state['callback_url']= BASE_URL.APP_DIR."/oauthapi/regLogin";
		
		error_log("[c/oauthapi/r] BASE_URL : ".$base_url);
		error_log("[c/oauthapi/r] callback_url : ".$state['callback_url']);
		error_log("[c/oauthapi/r] state : ".json_encode($state));
		// $callback_url :是第三方授權做完之後回殺價王系統 做登入動作(建帳號, 塞session)的函式
		// $_to  : 是塞session登入完成之後要跳轉到哪一頁
		switch($browserType){

	    case 'line' :
			          $url=$base_url.APP_DIR."/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
	                  error_log("[c/oauthapi/r] Line auth url :".$url);
	                  header("Location:".$url);
	                  break;

	    case 'weixin': // 委託大陸主機做微信認證
	                  $url="https://www.shajiawang.com/wx_auth_tw.php?state=".base64_encode(json_encode($state));
					  // $url=$base_url.APP_DIR."/oauth/weixin/getWxOauthCode.php?state=".base64_encode(json_encode($state));
	                  error_log("[oauthapi/r] Weixin auth url :".$url);
	                  header("Location:".$url);
	                  break;
	    case 'fb' :	             
                     $url="https://oauth.saja.vip/site/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
	                 error_log("[c/oauthapi/r] FB auth url :".$url);
	                 header("Location:".$url);
	                 break;

	    default :    
                     /* line oauth
                     $url=$base_url.APP_DIR."/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
	                 error_log("[c/oauthapi/r] Line auth url :".$url);
	                 header("Location:".$url);
	                 break;
					 */
					 $url="https://oauth.saja.vip/site/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
	                 error_log("[c/oauthapi/r] FB auth url :".$url);
	                 header("Location:".$url);
	                 break;
		  /* 導去登入頁
		                  // $url=$base_url.APP_DIR."/member/userlogin/";
						  $url=$base_url.APP_DIR."/member/userlogin/?state=".base64_encode(json_encode($state));
		                  header("Location: ".$url);
		                  break;
		  */
		}

		return ;

  }


	public function regLogin() {

		global $db, $config, $tpl, $statisticsModel;

		$auth_by	= empty($_POST['auth_by']) ? htmlspecialchars($_GET['auth_by']) : htmlspecialchars($_POST['auth_by']);
		$sso_uid	= empty($_POST['sso_uid']) ? htmlspecialchars($_GET['sso_uid']) : htmlspecialchars($_POST['sso_uid']);

		// $data是將授權資料(json string) 先urlencode, 再作base64_encode編碼後再傳過來的
		$data 		= empty($_POST['data']) ? htmlspecialchars($_GET['data']) : htmlspecialchars($_POST['data']);

		// $state是額外參數, 也是先urlencode, 再作base64_encode編碼後再傳過來
		$state   	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);

		error_log("[c/oauthapi/regLogin] data : ".$data);
		error_log("[c/oauthapi/regLogin] state : ".urldecode(base64_decode($state)));

		// 將授權資料轉成array
		$sso_data = urldecode(base64_decode($data));
		error_log("[c/oauthapi/regLogin] decoded sso_data : ".$sso_data);

		$sso_data = json_decode($sso_data, true);
		$arrState = json_decode(urldecode(base64_decode($state)), true);
		$user_src=$arrState['user_src'];
		$ts=str_replace(".","",microtime(true));
		$arrSSO = array();

		switch($auth_by){
		  case 'fb':
					        $arrSSO = array(
					          'auth_by' 		=> $auth_by,
					          'sso_data' 		=> urldecode(base64_decode($data)),
					          'state' 		=> $state,
					          'nickname' 		=> emoji_remove($sso_data['name']),
					          'headimgurl'    => $sso_data['picture']['url'],
					          'sso_name' 		=> "fb",
					          'phone'         => "fb".$ts,
					          'type' 			=> "sso",
					          'json'          => "Y",
					          'gender'        => "N/A",
					          'sso_uid' 		=> $sso_uid,
					          'sso_uid2' 		=> "",
					          'user_src'      => $user_src
					        );
					        break;
      case 'line':
									unset($sso_data['statusMessage']);
									$sso_data['displayName']=emoji_remove($sso_data['displayName']);
									$arrSSO = array(
									  'auth_by' 		=> $auth_by,
									  'sso_data' 		=> urlencode(json_encode($sso_data)),
									  'state' 		=> $state,
									  'nickname' 		=> $sso_data['displayName'],
									  'headimgurl'    => $sso_data['pictureUrl'],
									  'sso_name' 		=> "line",
									  'phone'         => "ln".$ts,
									  'type' 			=> "sso",
									  'json'          => "Y",
									  'gender'        => "N/A",
									  'sso_uid' 		=> $sso_uid,
									  'sso_uid2' 		=> "",
									  'user_src'      => $user_src
									);
									break;
      case 'weixin' :
											$arrSSO = array(
												'auth_by' 		=> $auth_by,
												'sso_data' 		=> urldecode(base64_decode($data)),
												'state' 		=> $state,
												'nickname' 		=> emoji_remove($sso_data['nickname']),
												'headimgurl'    => $sso_data['headimgurl'],
												'sso_name' 		=> "weixin",
												'phone'         => "wx".$ts,
												'type' 			=> "sso",
												'json'          => "Y",
												'gender'        => "N/A",
												'sso_uid' 		=> $sso_uid,
												'sso_uid2' 		=> $sso_data['unionid'],
												'user_src'      => $user_src
											);
			                break;
      default : break;
		}
		error_log("[c/oauthapi/regLogin] oauth data : ".json_encode($arrSSO));

		// helpers.php 的getUserBySSO
		if($auth_by=="weixin"){
		  $get_user = getUserBySSO($sso_uid, $auth_by, $sso_data['unionid']);
		}else{
		  $get_user = getUserBySSO($sso_uid, $auth_by, '');
		}
		error_log("[c/oauthapi/regLogin] get_user : ".json_encode($get_user));

		if(empty($get_user)){

		  $url = BASE_URL.APP_DIR."/ajax/user_register.php";
		  //設定送出方式-POST
		  $stream_options = array(
			'http' => array (
												'method' => "POST",
												'content' => json_encode($arrSSO),
												'header' => "Content-Type:application/json"
											 )
			);
		  $context = stream_context_create($stream_options);

		  // 送出json內容並取回結果
		  $response = file_get_contents($url, false, $context);

		  error_log("[c/oauthapi/regLogin] response : ".$response);

		  // 讀取json內容
		  $arr = json_decode($response,true);

			if($arr['retCode'] == 1){
			  $get_user = getUserBySSO($sso_uid, $auth_by, '');
			}
		}
		// helpers.php 的setLogin
		setLogin($get_user);

		//WEB登入成功記錄user使用裝置
		if(!empty($get_user['userid'])) {

				//參數設定
				$device = 'web';//裝置類型

				//查詢是否已有記錄
				$user_device_info = $statisticsModel->get_user_device($get_user['userid']);

				if($user_device_info == "FALSE") {//無紀錄新增

						$result = $statisticsModel->insert_user_device($get_user['userid'], $device);

				}else{//有紀錄更新

						$result = $statisticsModel->update_user_device($get_user['userid'], $device);

				}
		}

		error_log("[c/oauthapi/regLogin] _to :".$arrState['_to']);
		$gotourl=$arrState['_to'];
		$_redirect = true;
		
		if(!empty($gotourl)){
			$goto_url=BASE_URL.$gotourl;
			
			if( $arrState['_to'] !== "/site/member/" ){
				//轉給第三方的參數
				$goto_url = $gotourl;
				$arrState['saja_auth'] = $get_user['userid'];
				$arrState['saja_secret'] = md5($get_user['userid'] . $get_user['name']);
				$arrState['phone'] = $get_user['phone'];
				//$_redirect = false;
			}
			
		}else{
			// 預設去會員中心
			$goto_url=BASE_URL.APP_DIR."/member/";
		}
		unset($arrState['_to']);
		$build_query = $goto_url ."?". http_build_query($arrState);
		
		error_log("[c/oauthapi] location : ". $build_query);
		
		if($_redirect){
			header("Location: ". $build_query);
		}
		exit ;
  }


	public function qqcallback() {
		global $db, $config, $usermodel;

		//QQ：2845249491
		//密碼：xumu4hUF
		// require_once("/var/www/html/site/oauth/QQAPI/comm/config.php");
    // require_once(CLASS_PATH."QC.class.php");

		include_once("/var/www/html".APP_DIR."/oauth/QQAPI/qqConnectAPI.php");
		$qc = new QC();
		$qc->qq_callback();
		$token = $qc->get_openid();
    error_log("[oauthapi.qqcallback] The open_id is :".$token);

		$_SESSION['sso']["provider"] = '';
		$_SESSION['sso']["identifier"] = '';

		if($token){
			$_SESSION['token'] = $token;
			$uid = $token;
			$get_user = $this->get_user($uid, 'qq');

			if(!$get_user){

				$_SESSION['sso']["provider"] = 'qq';
				$_SESSION['sso']["identifier"] = $uid;
				error_log("Not get User !!");
				error_log($_SERVER['HTTP_HOST']);
				header("Location:".BASE_URL.APP_DIR."/user/sso_register/");
				/*
				echo '<form name="ssoform" action="'.BASE_URL.'/site/user/sso_register/" method="post">';
				echo '<input type="hidden" name="provider" value="qq">';
				echo '<input type="hidden" name="identifier" value="'.$uid.'">';
				echo '</form>';
				echo '<script>document.forms.ssoform.submit();</script>';
				*/
				// header("Location:".BASE_URL."/site/user/sso_register/?provider=qq&identifier=".$uid);
			}else{
				error_log("Get User :".$get_user['userid']);
				$this->set_login($get_user);

				//授權完成
				header("Location:".BASE_URL.APP_DIR."/");
			}
		}else{
			//'授權失敗!!'
			header("Location:".BASE_URL.APP_DIR."/");
		}
		die();

	}


	public function sinacallback() {

		global $db, $config, $usermodel;

		include_once("/var/www/html".APP_DIR."/oauth/SinaAPI/saetv2.ex.class.php");
		$o = new SaeTOAuthV2( WB_AKEY , WB_SKEY );

		if(isset($_REQUEST['code'])){
			$keys = array();
			$keys['code'] = $_REQUEST['code'];
			$keys['redirect_uri'] = WB_CALLBACK_URL;
			try{
				$token = $o->getAccessToken( 'code', $keys ) ;
			}catch (OAuthException $e){
			}
		}

		$_SESSION['sso']["provider"] = '';
		$_SESSION['sso']["identifier"] = '';

		if($token){
			$_SESSION['token'] = $token;
			$uid = $token["uid"];
			$get_user = $this->get_user($uid, 'sina');

			if(!$get_user){
				$_SESSION['sso']["provider"] = 'sina';
				$_SESSION['sso']["identifier"] = $uid;

				header("location: ".APP_DIR."/user/sso_register");
				die();
			}else{
				$this->set_login($get_user);

				//授權完成
				header("location: ".APP_DIR."/");
				die();
			}
		}else{
			//'授權失敗!!'
			header("location: ".APP_DIR."/");
			die();
		}

	}


	public function weixincallback() {

		global $db, $config, $usermodel;

		include_once("/var/www/html".APP_DIR."/oauth/WeixinAPI/WeixinChat.class.php");

		$options['appid'] = APP_ID;
		$options['appsecret'] = APP_SECRET;

		$wx_user = new WeixinChat($options);

		$url=APP_DIR."/";
		$token = '';
		$code = $_REQUEST['code'];

		if(isset($code) && !empty($code)){
		  error_log("[oauthapi/weixincallback] code=".$code);
			try{
				$token = $wx_user->getSnsAccessToken($code);
			}catch (OAuthException $e){
				echo "[oauthapi/weixincallback] exception：", $e->getMessage();
			}
		}

		session_start();
		$_SESSION['sso']["provider"] = '';
		$_SESSION['sso']["identifier"] = '';

		error_log("[oauthapi/weixincallback] token=".json_encode($token));

		if(!empty($token)){
			$_SESSION['token'] = $token;
			$openid = $token["openid"];
			$unionid= $token["unionid"];
			$_SESSION['sso']['openid'] = $openid;
			$_SESSION['sso']['unionid'] = $unionid;

			$get_user = $this->get_user($openid, 'weixin', $unionid);

			if(!$get_user){
				$u = (isset($_GET['u'])) ? $_GET['u'] : '';
				$_SESSION['sso']["provider"] = 'weixin';
				$_SESSION['sso']["identifier"] = $openid;
				$user_array = $wx_user->getSnsUserInfo($token["access_token"], $openid);
				if($user_array){
					error_log("[oauthapi/weixincallback] wx_user_array=".json_encode($user_array));
					$_SESSION['sso']["openid"] = $uid;
					$_SESSION['sso']["unionid"] = $user_array['unionid'];
					$_SESSION['sso']['headimgurl']= $user_array['headimgurl'];
					$_SESSION['sso']['nickname'] = addslashes($user_array['nickname']);
					$_SESSION['sso']['sex'] = $user_array['sex'];
					$_SESSION['sso']['language']=$user_array['language'];
					$_SESSION['sso']['city']=$user_array['city'];
					$_SESSION['sso']['province']=$user_array['province'];
					$_SESSION['sso']['country']=$user_array['country'];
					$_SESSION['sso']['subscribe']=$user_array['subscribe'];
					$_SESSION['sso']['sso_data']=$user_array;
					$_SESSION['reg_type']='sso';
					error_log("[oauthapi/weixincallback] redir to : ".APP_DIR."/user/sso_register?u=".$u);
					header("location: ".APP_DIR."/user/sso_register?u=".$u);
					die();
				}else{
				  error_log("[oauthapi/weixincallback] Cannot find wx userinfo of ".$openid."( unionid:".$unionid." )");
				  die();
				}
			}else{
				$this->set_login($get_user);
				$url = APP_DIR."/product/";

				error_log("[oauthapi/weixincallback] state=".$url);
				//授權完成
				header("location: ".$url);
				die();
			}
		}else{
			//'授權失敗!!'
			header("location: ".$url);
			die();
		}

	}


/*
	 *	line登入回傳
	 *	$auth_by		varchar			第三方類型格式 line
	 *	$sso			varchar			第三方會員編碼
	 *	$state			varchar			第三方加密編碼
	 *	$data			varchar			第三方會員資料
	 */
	public function linecallback() {

		global $db, $config, $usermodel ;

		$auth_by	= empty($_POST['auth_by']) ? htmlspecialchars($_GET['auth_by']) : htmlspecialchars($_POST['auth_by']);
		$sso 		= empty($_POST['sso_uid']) ? htmlspecialchars($_GET['sso_uid']) : htmlspecialchars($_POST['sso_uid']);
		$state 		= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);

		$data 		= empty($_POST['data']) ? htmlspecialchars($_GET['data']) : htmlspecialchars($_POST['data']);
		$data       = urldecode(base64_decode($data));
		$data =     remove_emoji($data);

		error_log("[c/oauthapi/linecallback] callback data : ".$data);
		$userdata 	= json_decode($data, true);

		$linedata = array(
											'auth_by' 		=> $auth_by,
											'sso_data' 		=> $data,
											'state' 		=> $state,
											'nickname' 		=> remove_emoji($userdata['displayName']),
											'headimgurl' 	=> $userdata['pictureUrl'],
											'sso_name' 		=> "line",
											'gender' 		=> "",
											'type' 			=> "sso",
											'sso_uid' 		=> $userdata['userId'],
											'sso_uid2' 		=> ""
		);

		$get_user = $this->get_user($linedata['sso_uid'], 'line', '');

    $arrState = json_decode(urldecode(base64_decode($state)), true);
    $gotourl=APP_DIR."/member/?";
    /* ex: gotourl="/site/product/saja/"
           productid=8395
           user_src=xxx
           ==> $gotourl="/site/product/saja/?productid=8395&user_src=xxx"
    */
    if(!empty($arrState['_to'])){
      $gotourl=$arrState['_to']."?";
      unset($arrState['_to']);
    }

		if(!empty($arrState['_vcburl'])){
      $gotourl=$arrState['_vcburl'];
      unset($arrState['_vcburl']);
    }

    if(is_array($arrState)){
     unset($arrState['callback_url']);
     $gotourl.=http_build_query($arrState);
    }
    error_log("[oauthapi] gotourl: ".$gotourl);

		//判斷是否註冊過
		if(empty($get_user)){
			//進行註冊
			$url = BASE_URL.APP_DIR."/ajax/user_register.php";
			//設定送出方式-POST
			$stream_options = array(
															'http' => array (
																	'method' => "POST",
																	'content' => json_encode($linedata),
																	'header' => "Content-Type:application/json"
															)
			);
			$context = stream_context_create($stream_options);

			// 送出json內容並取回結果
			$response = file_get_contents($url, false, $context);
			error_log("[oauthapi/linecallback] response : ".$response);
            // 讀取json內容
			$arr = json_decode($response,true);
			error_log("[oauthapi/linecallback] arr : ".json_encode($arr));

			if($arr['retCode'] == 1 || $arr['status']==200 || $arr['status']==16){
				$get_user2 = $this->get_user($linedata['sso_uid'], $linedata['auth_by'], '');
				if(!empty($get_user2)){
					$this->set_login($get_user2);
          echo "<script>location.href = '".BASE_URL.APP_DIR."/member/';</script>";

				}else{
					echo "<script>alert('註冊失敗 !!');location.href = '".BASE_URL.APP_DIR."';</script>";
				}
			}
		}else{
			error_log("[c/oauthapi/linecallback] user : ".json_encode($get_user));
      $this->set_login($get_user);
      header("Location:".BASE_URL.$gotourl);
		}

	}


	public function weixinbinding() {

		global $db, $config, $usermodel;

		include_once("/var/www/html".APP_DIR."/oauth/WeixinAPI/WeixinChat.class.php");

		$options['appid'] = APP_ID;
		$options['appsecret'] = APP_SECRET;

		$wx_user = new WeixinChat($options);

		if(isset($_REQUEST['code'])){
			$code = $_REQUEST['code'];
			$token = '';
			try{
				$token = $wx_user->getSnsAccessToken($code);
			}catch (OAuthException $e){
				echo "Caught exception：", $e->getMessage();
			}
		}

		if($token){
			$user['uid'] = $token["openid"];
			$user['userid'] = $_GET['userid'];
			$user['uid2'] = '';
			$user['sso_data'] = '';
			error_log("[oauthapi/weixinbinding] ".$user['userid']."<==>".$user['uid']);
			$get_sso = $this->get_sso($user['uid'], 'weixin');
			error_log($get_sso['ssoid']);
			if(!$get_sso['ssoid']){
				$this->set_sso($user, 'weixin');
				//綁定完成
				$this->alert("綁定完成！");
				exit;

			}else{
				//已綁定過
				$this->alert("已綁定過！");
				exit;
			}
		}else{
			//'授權失敗!!'
			header("location: ".APP_DIR."/");
			die();
		}

	}


	public function set_login($user) {

		global $db, $config, $usermodel;

		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';

		$query = "SELECT *
								FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							 WHERE `prefixid` = '{$config['default_prefix_id']}'
								 AND `userid` = '{$user['userid']}'
								 AND `switch` =  'Y'
								";
		$table = $db->getQueryRecord($query);

		$user['profile'] = $table['table']['record'][0];
		$_SESSION['user'] = $user;

		// Set session and cookie information.
		$_SESSION['auth_id'] = $user['userid'];
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = md5($user['userid'] . $user['name']);
		setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", md5($user['userid'] . $user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);

		// Set local publiciables with the user's info.
		if(isset($usermodel)){
			$usermodel->user_id = $user['userid'];
			$usermodel->name = $user['profile']['nickname'];
			$usermodel->email = '';
			$usermodel->ok = true;
			$usermodel->is_logged = true;
		}

	}


	public function get_user($uid, $type, $uid2) {

		global $db, $config;

		$query = "SELECT u.*, s.ssoid, s.name as provider_name, s.uid as provider_uid, s.uid2 as union_id, s.switch as sso_switch
								FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso` s
		 LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt` us ON
									   s.prefixid = us.prefixid
									   AND s.ssoid = us.ssoid
									   AND us.switch = 'Y'
		 LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u ON
									   us.prefixid = u.prefixid
									   AND us.userid = u.userid
									   AND u.switch = 'Y'
							 WHERE s.prefixid = '{$config['default_prefix_id']}'
								 AND s.name = '{$type}'
								 AND s.switch = 'Y'
								 AND u.userid IS NOT NULL
		";

		if(!empty($uid) && !empty($uid2)){
			$query .= "AND (s.uid = '{$uid}' OR s.uid2 = '{$uid2}')";
		}

		if(!empty($uid) && empty($uid2)){
			$query .= "AND s.uid = '{$uid}'";
		}

		if(empty($uid) && !empty($uid2)){
			$query .= "AND s.uid2 = '{$uid}'";
		}
		error_log("[c/oauthapi/get_user] : ".$query);

		if(empty($uid) && empty($uid2)){
			$table = '';
		}else{
			$table = $db->getQueryRecord($query);
		}

		if(empty($table['table']['record']) ){
			return false;
		}elseif($table['table']['record'][0]['switch'] != 'Y' || $table['table']['record'][0]['sso_switch'] != 'Y'){
			return false;
		}else{
			return $table['table']['record'][0];
		}

	}


	public function get_sso($uid, $type) {

		global $db, $config;

		$query = "select ssoid FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
						   where prefixid = '{$config['default_prefix_id']}'
							   AND name = '{$type}'
							   AND uid = '{$uid}'
							   AND switch = 'Y'
		";
		error_log("[oauthapi/get_sso]:".$query);
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'][0]) ){
			return false;
		}else{
			return $table['table']['record'][0];
		}

	}


	public function set_sso($user, $type) {

		global $db, $config;

		$query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
										  set
								      prefixid = '{$config['default_prefix_id']}',
											name = '{$type}',
											uid = '{$user['uid']}',
											uid2 = '{$user['uid2']}',
											sso_data = '{$user['sso_data']}',
											seq = '0',
											switch = 'Y',
											insertt = NOW()
		";
		$db->query($query);
		$ssoid = $db->_con->insert_id;

    $query = "insert into `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sso_rt`
											set
											prefixid = '{$config['default_prefix_id']}',
											userid = '{$user['userid']}',
											ssoid = '{$ssoid}',
											seq = '0',
											switch = 'Y',
											insertt = NOW()
		";
		$db->query($query);

    return $ssoid;

	}

  // Add By Thomas 20180803 給GamaPay綁定用的
  public function bindPayAccount() {

		global $db, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$sso_name	= empty($_POST['sso_name']) ? htmlspecialchars($_GET['sso_name']) : htmlspecialchars($_POST['sso_name']);
		$uid 	= empty($_POST['uid']) ? htmlspecialchars($_GET['uid']) : htmlspecialchars($_POST['uid']);
		$uid2 	= empty($_POST['uid2']) ? htmlspecialchars($_GET['uid2']) : htmlspecialchars($_POST['uid2']);

		$ret =getRetJSONArray(0,'INVALID INPUT !!','');
		if(empty($sso_name) || empty($uid) || empty($uid2)){
		   error_log("[c/oauthapi/bindPayAccount] ret : ".json_encode($ret));
		   echo  json_encode($ret);
		   return ;
		}

		// 檢查是否已綁定
		$arrCond = array();
		$arrCond['name']=$sso_name;
		$arrCond['uid']=$uid;
		$arrCond['uid2']=$uid2;

		$arr = $user->get_sso_data($arrCond);
		if($arr && $arr[0]['ssoid']>0) {
		  $ret =getRetJSONArray(-1,'此帳號已綁定過','');
		  error_log("[c/oauthapi/bindPayAccount] ret : ".json_encode($ret));
		  echo  json_encode($ret);
		  return ;
		}

		// 新增綁定資料
		$arrCond['userid']=$_SESSION['auth_id'];

		$ssoid = $this->set_sso($arrCond, $sso_name);
		if($ssoid>0){
		  $ret =getRetJSONArray(1,'OK','');
		  $ret['ssoid']=$ssoid;
		}
		error_log("[c/oauthapi/bindPayAccount] ret : ".json_encode($ret));
		echo json_encode($ret);

		return;

  }

   // Add By Thomas 20180803 給GamaPay解除綁定用的
  public function unbindPayAccount() {

		global $db, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$sso_name = empty($_POST['sso_name']) ? htmlspecialchars($_GET['sso_name']) : htmlspecialchars($_POST['sso_name']);
		$uid 	= empty($_POST['uid']) ? htmlspecialchars($_GET['uid']) : htmlspecialchars($_POST['uid']);
		$uid2 	= empty($_POST['uid2']) ? htmlspecialchars($_GET['uid2']) : htmlspecialchars($_POST['uid2']);
		$userid = $_SESSION['auth_id'];

		if(empty($sso_name) && empty($uid) && empty($userid) && empty($uid2)){
		  $ret =getRetJSONArray(0,'無效的輸入資料','');
		  error_log("[c/oauthapi/unbindPayAccount] ret : ".json_encode($ret));
		  echo  json_encode($ret);
		  return ;
		}

		// 檢查是否有綁定資料
		$ssoid="";
		$arrCond = array();
		$arrCond['name']=$sso_name;
		$arrCond['uid']=$uid;
		$arrCond['uid2']=$uid2;
		$arr = $user->get_sso_data($arrCond);
		if($arr && $arr[0]['ssoid']>0){
		  $ssoid=$arr[0]['ssoid'];
		}else{
		  $ret =getRetJSONArray(-1,'查不到綁定帳號資料','');
		  error_log("[c/oauthapi/unbindPayAccount] ret : ".json_encode($ret));
		  echo  json_encode($ret);
		  return ;
		}
		error_log("[c/oauthapi/unbindPayAccount] ssoid to unset : ".$ssoid);
		if($ssoid>0){
		  if($user->delete_sso_rt($ssoid)){
		     if($user->delete_sso($ssoid)){
		       $ret = getRetJSONArray(1,'OK','');
		       $ret['ssoid']=$ssoid;
		     }else{
		       $ret = getRetJSONArray(-2,'綁定帳號資料刪除失敗','');
		       $ret['ssoid']=$ssoid;
		     }
		  }else{
		    $ret = getRetJSONArray(-3,'關連帳號資料刪除失敗','');
		    $ret['ssoid']=$ssoid;
		  }
		}
		error_log("[c/oauthapi/unbindPayAccount] ret : ".json_encode($ret));
		echo json_encode($ret);

		return;

  }

	public function alert($message) {
		echo "<script language='javascript'>alert('".$message."');location.href='".APP_DIR."/';</script>";
		return false;
	}

	/*
	 * 第三方認證登入
	 * $uid		varchar		OPEN ID
	 * $uid2	varchar		微信UNION ID
	 * $type	varchar		登入類型
	 */
	public function appLogin() {

		global $db, $tpl, $config;

		$uid 		= empty($_GET['uid']) ? $_POST['uid'] : $_GET['uid'];
    $uid2 		= empty($_GET['uid2']) ? $_POST['uid2'] : $_GET['uid2'];
		$type 		= empty($_GET['type']) ? $_POST['type'] : $_GET['type'];
		$intro_by 	= empty($_GET['intro_by']) ? $_POST['intro_by'] : $_GET['intro_by'];
		$typedesc 	= "";

		error_log("[oauthapi/appLogin] ".json_encode($_POST));

		switch($type){
			case "weixin":
										$typedesc="微信";

										//使用openid取得使用者資料
										$table = $this->get_user($uid, $type, $uid2);
										error_log("[oauthapi/appLogin] get_user : ".json_encode($table));

										if((empty($table['union_id'])) && (!empty($uid2)) && (strlen($uid2)>16)){
											$query = "update `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
																   set uid2 = '{$uid2}'
																 WHERE prefixid = '{$config['default_prefix_id']}'
																   AND ssoid = '{$table['ssoid']}'
																   AND uid = '{$uid}'
											";
											$db->query($query);
										}

										//取得使用者暱稱
										if(!empty($table['userid'])){
											$query = "select nickname FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
																 where prefixid = '{$config['default_prefix_id']}'
																   AND userid = '{$table['userid']}'
																   AND switch = 'Y'
											";
											$tablepr = $db->getQueryRecord($query);
											$table['nickname'] = $tablepr['table']['record'][0]['nickname'];
										}else{

											if(!empty($uid) || !empty($uid2)){
												$userdata = $this->getWeixinUserData($uid);
												error_log("[oauthapi/appLogin] : ".json_encode($userdata));

												$userdata['province'] = strtoupper($userdata['province']);

												if(!empty($userdata['errcode']) && $userdata['errcode']>0){
												  $ret=getRetJSONArray(1,'無'.$typedesc.'綁定資料, 請先以微信註冊!!','MSG');
												  echo json_encode($ret);
												  exit;
												}
												//取得省份及地區位置
												$query = "SELECT pr.provinceid, pr.regionid, re.countryid
																		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}province` pr
															 left join `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}region` re
																			on pr.prefixid = re.prefixid
																		 and pr.regionid = re.regionid
																		 and pr.switch = 'Y'
																	 WHERE pr.prefixid = '{$config['default_prefix_id']}'
																		 AND pr.switch = 'Y'
																		 AND (pr.`name` = '{$userdata['province']}' OR pr.code = '{$userdata['province']}')
																	";
												$place = $db->getQueryRecord($query);

												//判斷是否有省份及區域資料,若無資料則使用系統預設
												if(!empty($place)){
													$provinceid = $place['table']['record'][0]['provinceid'];
													$regionid = $place['table']['record'][0]['regionid'];
													$countryid = $place['table']['record'][0]['countryid'];
												}else{
													$countryid = $config['country'];
													$regionid = $config['region'];
													$provinceid = $config['province'];
													$channelid = $config['channel'];
												}

												//取得城巿相關資料
												$query = "SELECT *
																		FROM `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel`
																	 WHERE prefixid = '{$config['default_prefix_id']}'
																		 AND provinceid = '{$provinceid}'
																		 AND name = '{$userdata['city']}'
																		 AND switch = 'Y'
												";
												$place2 = $db->getQueryRecord($query);

												//判斷城巿若無資料則新增
												if(empty($place2['table']['record'][0]['channelid'])){
													$query = "INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}channel`
													             SET	`prefixid`='{$config['default_prefix_id']}',
														                `countryid`='{$countryid}',
																						`provinceid`='{$provinceid}',
																						`name`='{$city}',
																						`description`='{$province}全區',
																						`seq`=0,
																						`switch`='Y',
																						`insertt`=NOW()
													";

													$db->query($query);
													$channelid = $db->_con->insert_id;
												}else{
													$channelid = $table2['table']['record'][0]['channelid'];
												}

												//時間序碼
												//$num = time();
												$num = microtime(true)*10000;
												//使用者登入帳號
												$user_name = "weixin_".$num;

												include_once(LIB_DIR ."/convertString.ini.php");
												$this->str = new convertString();
												//設定密碼
												$user_passwd = $this->str->strEncode(substr($num, -6), $config['encode_key']);
												//設定兌換密碼
												$user_exchangepasswd = $this->str->strEncode(substr($num, -6), $config['encode_key']);

												//寫入USER帳密資料
												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
																		 SET `prefixid`='{$config['default_prefix_id']}',
																			 	 `name`='{$user_name}',
																			 	 `passwd`='{$user_passwd}',
																				 `exchangepasswd`='{$user_exchangepasswd}',
																			 	 `email`='',
																				 `insertt`=NOW()
												";
												$db->query($query);
												$userid = $db->_con->insert_id;

												//設定性別
												if($userdata['sex'] == 1){
													$gender = 'male';
												}else{
													$gender = 'female';
												}

												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
																		 SET `prefixid`='{$config['default_prefix_id']}',
																				 `userid`='{$userid}',
																				 `nickname`='{$userdata['nickname']}',
																				 `gender`='{$gender}',
																				 `countryid`='{$countryid}',
																				 `regionid`='{$regionid}',
																				 `provinceid`='{$provinceid}',
																				 `channelid`='{$channelid}',
																				 `thumbnail_url`='{$userdata['headimgurl']}',
																				 `area`='',
																				 `address`='',
																				 `addressee`='',
																				 `phone`='{$user_name}',
																				 `src_from` = 'WEIXINAPP',
																				 `insertt`=NOW()
												";
												$db->query($query);

												$user['uid'] = $userdata["openid"];
												$user['userid'] = $userid;
												$user['uid2'] = $userdata['unionid'];
												$user['sso_data'] = json_encode($userdata);
												//新增SSO相關資料
												$this->set_sso($user, 'weixin');

												//新增手機驗證相關資料
												$shuffle = get_shuffle();
												$checkcode = substr($shuffle, 0, 6);

												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
																		 SET `prefixid`='{$config['default_prefix_id']}',
																				 `phone`='{$user_name}',
																				 `userid`='{$userid}',
																				 `code`='{$checkcode}',
																				 `verified`='N',
																				 `insertt`=NOW()
												";
												$db->query($query);

												// 記錄連線來源 - 抓來源IP
												if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
													$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
													$ip = $temp_ip[0];
												}else{
													$ip = $_SERVER['REMOTE_ADDR'];
												}
												$goto='';

												//新增推薦人相關資
												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
																		 SET `come_from`='{$ip}',
																				 `intro_by`='{$intro_by}',
																				 `act`='REG',
																				 `productid`='',
																				 `userid`='{$userid}',
																				 `goto`='{$goto}',
																				 `memo`='{$user['sso_data']}',
																				 `user_agent`='WEIXIN',
																				 `insertt`=NOW(),
																				 `modifyt`=NOw()
												";
												$db->query($query);

												$table['userid'] = $userid;
												$table['name'] = $user_name;
												$table['email'] = '';
												$table['nickname'] = $userdata['nickname'];
											}
										}
                    break;
      case "qq":
										$typedesc="qq";
										break;
			case "weibo":
									  $typedesc="微博";
									  break;
		}

		error_log("[oauthapi/appLogin] user data :".json_encode($table));

		if(empty($table) || empty($table['userid'])){
			$ret = getRetJSONArray(1,'FAIL'.$typedesc.'綁定資料 !!','MSG');
		}else{
			$ret = getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array();
			$ret['retObj']['auth_id'] = $table['userid'];
			$ret['retObj']['auth_loginid'] = $table['name'];
			$ret['retObj']['auth_email'] = $table['email'];
			$ret['retObj']['auth_secret'] = md5($table['userid'].$table['name']);
			$ret['retObj']['auth_nickname']=$table['nickname'];
		}

		$x=json_encode($ret);
		error_log("[appLogin] : ".$x);
		echo $x;

		exit;

	}


  /*
	 * Web App第三方認證登入 (Client已有userinfo)
	 * $uid		varchar		OPEN ID
	 * $uid2	varchar		微信UNION ID
	 * $type	varchar		登入類型
	 */

  public function WebAppLogin() {

		global $db, $tpl, $config;

		$uid 		= empty($_GET['uid']) ? $_POST['uid'] : $_GET['uid'];
    $uid2 		= empty($_GET['uid2']) ? $_POST['uid2'] : $_GET['uid2'];
		$type 		= empty($_GET['type']) ? $_POST['type'] : $_GET['type'];
		$city       = empty($_GET['city']) ? $_POST['city'] : $_GET['city'];
		$sex       = empty($_GET['sex']) ? $_POST['sex'] : $_GET['sex'];
		$nickname       = empty($_GET['nickname']) ? $_POST['nickname'] : $_GET['nickname'];
		$thumbnail_url       = empty($_GET['thumbnail_url']) ? $_POST['thumbnail_url'] : $_GET['thumbnail_url'];
		$intro_by 	= empty($_GET['intro_by']) ? $_POST['intro_by'] : $_GET['intro_by'];
		$typedesc 	= "";

		error_log("[oauthapi/WebAppLogin] ".json_encode($_POST));

		switch($type){
			case "weixin":
			              $typedesc="微信";

										//使用openid或unionid取得使用者資料

										$table = $this->get_user($uid, $type, $uid2);
										error_log("[oauthapi/WebAppLogin] get_user : ".json_encode($table));

										if((empty($table['union_id'])) && (!empty($uid2)) && (strlen($uid2)>16)){
											$query = "update `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}sso`
														       set uid2 = '{$uid2}'
														     WHERE prefixid = '{$config['default_prefix_id']}'
														       AND ssoid = '{$table['ssoid']}'
														       AND uid = '{$uid}' ";
											$db->query($query);
										}

										if(!empty($table['userid'])){
											$query = "select nickname FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
											           where prefixid = '{$config['default_prefix_id']}'
												           AND userid = '{$table['userid']}'
												           AND switch = 'Y'
											";
											$tablepr = $db->getQueryRecord($query);
											if($tablepr && $tablepr['table']['record'][0]){
											  $table['nickname'] = $tablepr['table']['record'][0]['nickname'];
											  $_SESSION['user']['profile'] = $tablepr['table']['record'][0];
											}
											$ret = getRetJSONArray(1,'OK','JSON');
											$ret['retObj']=array();
											$ret['retObj']['auth_id'] = $table['userid'];
											$ret['retObj']['auth_loginid'] = $table['name'];
											$ret['retObj']['auth_email'] = $table['email'];
											$ret['retObj']['auth_secret'] = md5($table['userid'].$table['name']);
											$ret['retObj']['auth_nickname']=$table['nickname'];

											// Set session and cookie information.
											$_SESSION['auth_id'] = $table['userid'];
											$_SESSION['auth_email'] = $table['email'];
											$_SESSION['auth_secret'] = md5($table['userid'].$table['name']);
											$_SESSION['auth_loginid'] = $table['name'];
											$_SESSION['auth_nickname'] = $table['profile']['nickname'];
											$_SESSION['auth_type'] = $table['user_type'];

											setcookie("auth_id", $table['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
											setcookie("auth_email", $table['email'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
											setcookie("auth_secret", md5($table['userid'].$table['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);
											setcookie("auth_loginid", $table['name'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
											setcookie("auth_nickname", $table['profile']['nickname'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
											setcookie("auth_type", $table['user_type'], time()+60*60*24*30, "/", COOKIE_DOMAIN);

											$x=json_encode($ret);
											error_log("[WebAppLogin] : ".$x);
											echo $x;
											exit;

										}else{

											if(!empty($uid) || !empty($uid2)){

												//設定性別
												if($sex == 1 || $sex=='男' || $sex=='男'){
													$gender = 'male';
												}else{
													$gender = 'female';
												}

												$userdata=array(
																				'gender'=>$gender,
																				'nickname'=>$nickname,
																				'openid'=>$uid,
																				'unionid'=>$uid2,
																				'nickname'=>$nickname,
																				'headimgurl'=>$thumbnail_url
												);

												$countryid = $config['country'];
												$regionid = $config['region'];
												$provinceid = $config['province'];
												$channelid = $config['channel'];

												// //取得省份及地區位置 (改用 helpers.php裏的function)
												if(!empty($userdata['province'])){
												  $province_data=getProvinceData(array("name"=>$userdata['province']));
												  if($province_data['table']['record']){
												    $provinceid=$province_data['table']['record'][0]['provinceid'];
												  }
												}
												if(!empty($userdata['city'])){
												  $channel_data = getChannelData(array("name"=>$userdata['city']));
												  if($channel_data['table']['record']){
													  $channelid=$channel_data['table']['record'][0]['channelid'];
												  }
												}

												//時間序碼
												$num = microtime(true)*10000;
												//使用者登入帳號
												$user_name = "weixin_".$num;

												include_once(LIB_DIR ."/convertString.ini.php");
												$this->str = new convertString();
												//設定密碼
												$user_passwd = $this->str->strEncode(substr($num, -6), $config['encode_key']);
												//設定兌換密碼
												$user_exchangepasswd = $this->str->strEncode(substr($num, -6), $config['encode_key']);

												//寫入USER帳密資料
												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
												             SET `prefixid`='{$config['default_prefix_id']}',
																				 `name`='{$user_name}',
																				 `passwd`='{$user_passwd}',
																				 `exchangepasswd`='{$user_exchangepasswd}',
																				 `email`='',
																				 `user_type`='P',
																				 `insertt`=NOW()
												";
												$db->query($query);
												$userid = $db->_con->insert_id;

												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
												             SET `prefixid`='{$config['default_prefix_id']}',
																				 `userid`='{$userid}',
																				 `nickname`='{$userdata['nickname']}',
																				 `gender`='{$userdata['gender']}',
																				 `countryid`='{$countryid}',
																				 `regionid`='{$regionid}',
																				 `provinceid`='{$provinceid}',
																				 `channelid`='{$channelid}',
																				 `thumbnail_url`='{$userdata['headimgurl']}',
																				 `area`='',
																				 `address`='',
																				 `addressee`='',
																				 `phone`='{$user_name}',
																				 `src_from` = 'WEIXINAPP',
																				 `insertt`=NOW()
												";
												$db->query($query);

												$user['uid'] = $userdata["openid"];
												$user['userid'] = $userid;
												$user['uid2'] = $userdata['unionid'];
												$user['sso_data'] = json_encode($userdata);
												//新增SSO相關資料
												$this->set_sso($user, 'weixin');

												//新增手機驗證相關資料
												$shuffle = get_shuffle();
												$checkcode = substr($shuffle, 0, 6);

												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth`
																		 SET `prefixid`='{$config['default_prefix_id']}',
																				 `phone`='{$user_name}',
																			 	 `userid`='{$userid}',
																				 `code`='{$checkcode}',
																				 `verified`='N',
																				 `insertt`=NOW()
												";
												$db->query($query);

												// 記錄連線來源 - 抓來源IP
												if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
													$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
													$ip = $temp_ip[0];
												}else{
													$ip = $_SERVER['REMOTE_ADDR'];
												}
												$goto='';

												//新增推薦人相關資
												$query = "INSERT INTO `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
												             SET `come_from`='{$ip}',
																				 `intro_by`='{$intro_by}',
																				 `act`='REG',
																				 `productid`='',
																				 `userid`='{$userid}',
																				 `goto`='{$goto}',
																				 `memo`='{$user['sso_data']}',
																				 `user_agent`='WEIXIN',
																				 `insertt`=NOW(),
																				 `modifyt`=NOw()
												";
												$db->query($query);

												$table['userid'] = $userid;
												$table['name'] = $user_name;
												$table['email'] = '';
												$table['nickname'] = $userdata['nickname'];

												// Set session and cookie information.
												$_SESSION['auth_id'] = $table['userid'] ;
												$_SESSION['auth_email'] = $table['email'];
												$_SESSION['auth_secret'] = md5($table['userid'].$table['name']);
												$_SESSION['auth_loginid'] = $table['name'];
												$_SESSION['auth_nickname'] = $table['nickname'];
												$_SESSION['auth_type'] = 'sso';

												setcookie("auth_id", $table['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
												setcookie("auth_email", $table['email'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
												setcookie("auth_secret", md5($table['userid'].$table['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);
												setcookie("auth_loginid", $table['name'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
												setcookie("auth_nickname", $table['profile']['nickname'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
												setcookie("auth_type", $table['user_type'], time()+60*60*24*30, "/", COOKIE_DOMAIN);

											}
										}
                    break;
      case "qq":
								$typedesc="qq";
								break;
			case "weibo":
								   $typedesc="微博";
								   break;
		}

		error_log("[oauthapi/WebAppLogin] user data :".json_encode($table));
		if(empty($table) || empty($table['userid'])){
			$ret = getRetJSONArray(0,'無'.$typedesc.'綁定資料 !!','MSG');
		}
		$x=json_encode($ret);
		error_log("[WebAppLogin] : ".$x);
		echo $x;

		exit;

	}

	//第三方用戶資料查詢 - weixin
	public function getWeixinUserData($id='') {

		global $db, $config;

    $uid = empty($id) ? $_GET['uid'] : $id;
		$lang = empty($_GET['lang']) ? "zh_CN":$_GET['lang'];
		error_log("[getWeixinUserData] open ID : ".$uid);

		//取得weixin的token資料
		$tokendata = json_decode(file_get_contents("https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=wxbd3bb123afa75e56&secret=367c4259039bf38a65af9b93d92740ae"), true);
		$token = $tokendata['access_token'];

		$openid = $uid;

		//取得用戶資料
		$weixindata = file_get_contents("https://api.weixin.qq.com/cgi-bin/user/info?access_token=".$token."&openid=".$openid."&lang=".$lang);
		error_log("[c/oauthapi/getWeixinUserData]".$weixindata);
		$userdata = json_decode($weixindata, true);
		return $userdata;

		exit;

	}


	public function use_line_login() {

   	$state=array("callback_url"=>BASE_URL.APP_DIR."/oauthapi/linecallback/");
    $state['productid']=htmlspecialchars(empty($_POST['productid'])?$_GET['productid']:$_POST['productid']);
    $state['_to']=htmlspecialchars(empty($_POST['_to'])?$_GET['_to']:$_POST['_to']);
    $state['user_src']=empty($_POST['user_src'])?$_GET['user_src']:$_POST['user_src'];
    error_log('[c/oauthapi/use_line_login] state : '.json_encode($state));
		$url=BASE_URL.APP_DIR."/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
		error_log('[c/oauthapi/use_line_login] url : '.$url);
		header("Location:".$url);

		exit;

	}

	// 作fb認證, 並把抓到的fb user profile 傳給callback_url
	public function use_fb_login() {

		$state=array("callback_url"=>BASE_URL.APP_DIR."/oauthapi/user_register.php");
		$url=BASE_URL.APP_DIR."/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
		error_log('[c/oauthapi/use_fb_login] url : '.$url);
		header("Location:".$url);

		exit;

	}

  // 發放token
  public function genToken($vdrid='', $vuid='') {

		global $db, $config, $helpers, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$userid=$_SESSION['auth_id'];

		if(empty($vdrid))
		  $vdrid=$_REQUEST['vdrid'];

		if(empty($vuid))
		  $vuid=$_REQUEST['vuid'];

		$ret = getRetJSONArray('1','OK','JSON');

		if(empty($userid) || empty($_SESSION['auth_id'])){
		  $ret['retCode']=-1;
		  $ret['retMsg']='EMPTY SAJA USER';
		  echo json_encode($ret);
		  exit;
		}

		if(empty($vdrid)){
		  $ret['retCode']=-2;
		  $ret['retMsg']='EMPTY VENDOR';
		  echo json_encode($ret);
		  exit;
		}

		if(empty($vuid)){
		  $ret['retCode']=-3;
		  $ret['retMsg']='EMPTY VENDOR USERID';
		  echo json_encode($ret);
		  exit;
		}

		error_log("[c/oauthapi/genToken] session : ".$_SESSION['auth_id']);
		error_log("[c/oauthapi/genToken] vdrid: ".$vdrid." <=>vuid: ".$vuid);

		$token = "";
		// 以$userid, $vdrid 檢查是否已有token存在
		$arrSSOList = $user->get_user_sso_list($userid, $vdrid);
		if($arrSSOList){
		  // 如果有則直接回傳
		  $token = $arrSSOList[0]['uid'];
		}else{
		  $oriStr = $vdrid."|".$userid;
		  error_log("[c/oauthapi] ori str : ".$oriStr);
		  $token = strtolower(md5($oriStr));
		  // 將token 寫入 sso, sso_rt
		  $ssoid = $user->create_sso($vdrid, $token,'','');

			if($ssoid>0){
		    $sso_rt_id = $user->create_sso_rt($userid, $ssoid);
		  }

		}
		$ret['retObj']=array("token"=>$token,"vdrid"=>$vdrid, "vuid"=>$vuid);
		echo json_encode($ret);

		return;

  }

}
