<?php
/*
 * Bid Controller //閃殺最新成交(殺價得標、流標)
 */

include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR."/helpers.php");

class Kusobid {

	public $controller = array();
	public $params = array();
	public $id;

  /*
   *	最新成交清單列表
   *	$type			varchar			成交狀態
   * 	$genHTML		varchar			建立新頁參數
   */
	public function home() {

		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		//活動代碼
		$hostid=htmlspecialchars($_REQUEST['hostid']);

		//成交狀態
		$type = empty($_POST['type'])?htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$tpl->assign('type', $type);
		$page_number = empty($_POST['p'])?htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
		
		$product_list=null;
		
		//判斷成交狀態參數
		if (!empty($hostid)) {	//得標
			//取得成交清單，目前只顯示最近六小時內的資料
			$product_list = $bid->flash_closed_product($page_number, $hostid);
		}else{	//流標
		  //$product_list = $bid->product_cancelled_list();
		}

		if($product_list) {
			for($idx=0; $idx< count($product_list['table']['record']); ++$idx) {
				$p=$product_list['table']['record'][$idx];
				
				//閃殺商品
				if(!empty($p['thumbnail'])) {
					$product_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$p['thumbnail'];
					$product_list['table']['record'][$idx]['offtime']=(int)$p['offtime'];
					$product_list['table']['record'][$idx]['now']=(int)$p['now'];
				}
			}
		}

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if($json=='Y') {
		  $ret=getRetJSONArray(1,'OK','LIST');
		  if((empty($p)) || ($p=='') || ($p < 0) || ($p > $product_list['table']['page']['totalpages'])){
			$ret['retObj']['data'] = '';
		  }else{
			$ret['retObj']['data'] = $product_list['table']['record'];
			$ret['retObj']['page'] = $product_list['table']['page'];
		  }
		  echo json_encode($ret);
		  exit;
		}

		$tpl->assign('product_list', $product_list);

		//設定分頁
		if(empty($product_list['table']['page']) ){

		  $page_content = array();

		}else{

		  $page_path = $tpl->variables['status']['status']['path'];
		  $page_content = $this->set_page($product_list, $page_path);

		}

		$meta['title']="最新得標";
		$meta['description']='';
		$tpl->assign('meta', $meta);

		$tpl->assign('footer','N');
		$tpl->assign('hostid', $hostid);
		$tpl->assign('page_content', $page_content);
		$tpl->set_title('閃殺最新得標');
		$tpl->render("kusosaja","kusobid",true,'',$json);
	}

	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

		$table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'].'&type='.$_GET['type'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'].'&type='.$_GET['type'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';

		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;
	}


	//最新成交(殺價中標、流標)商品內頁
	public function detail() {

		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = htmlentities($_GET['type']);
		$productid=htmlentities($_GET['productid']);
		$channelid=htmlentities($_GET['channelid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		if(empty($productid)){
			echo '<script>alert("商品編號錯誤!");</script>';
		} else {
			//Generate Static Html Page
			$DetailHtml = $this->BidDetailHtml();

			$product = $bid->get_info($productid);

			if(!empty($product['thumbnail'])){
				$product['img_src'] = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}elseif(!empty($product['thumbnail_url'])){
				$product['img_src'] = $product['thumbnail_url'];
			}

		}

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		// 檢查用戶是否有下標
		$tpl->assign('user_bid_count', $this->user_bid_count($userid, $productid));
		$tpl->assign('type', $type);
		$tpl->assign('product', $product);
		$tpl->assign('productid', $productid);
		$tpl->assign('channelid', $channelid);
		$tpl->assign('navbar_html', $DetailHtml['navbar']);
		$tpl->assign('bidSelf_html', $DetailHtml['bidSelf']);
		$tpl->assign('bidList_html', $DetailHtml['bidList']);
		$tpl->set_title('閃殺最新得標');

		$meta['title']=$product['name'];
		$meta['description']='';
		$meta['image'] = $product['img_src'];
		$tpl->assign('footer','N');
		$tpl->assign('meta',$meta);
		$tpl->render("kusosaja","bid_detail",true,'',$json);
	}
	
	private function BidDetailHtml() {

		global $tpl, $bid, $member;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$startprice = empty($_GET['startprice']) ? 1 : htmlspecialchars($_GET['startprice']);
		
		//活動代碼
		$hostid=htmlspecialchars($_REQUEST['hostid']);
		
		//商品資料
		$product = $bid->get_info($productid);
		

		//判斷若得標商品無資料,則直接查詢商品資料
		if (empty($product['productid'])){
			$product = $bid->get_cancelled_info($productid);
		}

		//下標人數&下標金額
		//$get_bid_Arr = $bid->get_bid_info($productid);
		if (!empty($productid) ){
			$_bid_Arr = $bid->get_onbid_list($productid, '', $startprice);
			$get_bid_Arr = empty($_bid_Arr['table']['record']) ? null : $_bid_Arr['table']['record'];
		}
		
		$i = 0;
		$user_bid_count = 0;
		if(is_array($get_bid_Arr)){
			foreach($get_bid_Arr as $key => $value){
				$bid_info[$key]['price'] = $value['price'];
				$bid_info[$key]['num'] = $value['count'];
			}
			
			$user_bid_count = count($bid_info); //總數
		}
			
		$range = 1000;	//區間
		$user_bid_max = round($bid->user_bid_max('', $productid));	//下標最大金額
		$bid_count = ceil($user_bid_max / $range);	//區間數
		$output_push = array( 0=>array('start'=>1, 'end'=>$range) );	//儲存各區間
		$html['navbar']= '';
			
		if($bid_count >1){
			$html['navbar'].='<!--  navbar 區間  --><div class="collapse navbar-collapse" id="myNavbar"><ul class="nav navbar-nav">';
				
				for($i=0; $i < $bid_count; $i++){                                        
					//$remainder = $i % 3;
					$bid_start = ($i==0) ? 1 : ($i * $range)+1;
					$bid_end = ($i+1) * $range;
					$output_push[$i] = array('start'=>$bid_start, 'end'=>$bid_end);
					
					$link = BASE_URL.APP_DIR .'/kusobid/detail/?productid='. $productid .'&startprice='. $bid_start .'&hostid='. $hostid .'&type=';
					
					$act = (intval($startprice)==intval($bid_start)) ? ' active' : ' ';
					$done_str = (($bid_start < $startprice-($range*2)) ) ? ' li_show' : ' '; 
					$html['navbar'].='<li class="'. $done_str .'"><a class="'. $act .'" href="javascript:void(0)" onclick="location.replace(\''. $link .'\')">'. $bid_start.' - '. $bid_end .'</a></li>';
				}
			
			$html['navbar'].='</ul></div>';
		}
		
		//會員圖示
		$get_member = $member->get_info($product['userid']); 
		
		if(!empty($get_member['thumbnail_file'])){
            $userImg = BASE_URL .'/site/images/headimgs/'. $get_member['thumbnail_file'];
		}elseif(!empty($get_member['thumbnail_url'])){
			$userImg = $get_member['thumbnail_url'];
		} else {
			$userImg = APP_DIR .'/static/img/bid-member.jpg?_t='.time();
		}
		
		$html['bidSelf']= '<!--  成交商品資訊  -->'.
			'<input id="allbidcount" type="hidden" value="'.$user_bid_count.'" >'.
			'<ul class="listsimg-box-group ">'.
			'<li class="listsimg-box">'.
			'<div class="listsimg-box-solid">'.
			'<div class="listsimg-header">';

		$html['bidSelf'].= '<div class="d-flex align-items-center">'.
              '<div class="bid-user d-inlineflex align-items-center mr-auto">'.
              '<div class="bid-img">'.
              '<img class="img-fluid" src="'. $userImg .'">'.
              '</div>'.
              '<div class="bid-userName">';
      
        $html['bidSelf'].= urldecode($product['nickname']).
                '</div>'.
                '</div>'.
                '<div class="bid-price">'.
                '<span>得標金額</span><span>NT</span><span>'. $product['price'].'</span><span>元</span>'.
                '</div>';
		$html['bidSelf'].= '</div>';

		$html['bidSelf'].= '</div>'.
				'<div class="listsimg-contant d-flex align-items-start">'.
				'<div class="listimg_img">'.
				'<i class="d-flex align-items-center">';

		//殺戮商品
			if(!empty($product['thumbnail'])) {
				$img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}elseif(!empty($product['thumbnail_url'])){
				$img_src = $product['thumbnail_url'];
			}

		$html['bidSelf'].= '<img src="'.$img_src.'">'.
            '</i>'.
            '</div>'.
            '<div class="listimg-info">'.
            '<p class="pro-name">'.$product['name'].'</p>'.
            '<p class="pro-price">'.
            '<span>官方售價：NT </span><span>'.$product['retail_price'].'</span><span> 元</span>'.
            '</p>'.
            '</div>'.
            '</div>'.
            '<div class="listimg-time d-flex">'.
            '<div class="ml-auto">'.
            '<span>'. $product['insertt'].'</span>'.
            '</div>'.
            '</div>'.
			'</div>'.
            '</li>'.
            '</ul>';
		
		//競標資訊
		if(is_array($bid_info)){
		$html['bidList']= '<!--  競標資訊  --><ul>';
			
			foreach($bid_info as $rk => $rv){
			
			if ($rv['num']==1 && $product['price'] == $rv['price']){
			$html['bidList'].= '<li class="bidList d-flex listMark">';
			}else{
			$html['bidList'].= '<li class="bidList d-flex">';
			}
			
			$html['bidList'].= '<div class="list-data">'.round($rv['price']).'</div><div class="list-data">'.$rv['num'].'</div>';
			$html['bidList'].= '</li>';
			
			}
		
		$html['bidList'].= '</ul>';
		}
		
		return $html;
	}
	
	public function user_bid_count($userid, $productid) {
		global $tpl, $bid, $product;

		  //設定 Action 相關參數
		  set_status($this->controller);

		  if(!empty($userid) && !empty($productid)) {
		    return $bid->user_bid_count($userid, $productid);
		  }
	    return 0;
	}

}
