<?php
/*
 * Deposit Controller 儲值
 */

include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/helpers.php");
require_once(LIB_DIR."/WxpayAPI/WxPay.JsApiPay.php");

class Deposit3 {
  public $controller = array();
	public $params = array();
  public $id;
	public $_ARR_ALLOW_IP=['150.116.207.59','61.219.220.92','61.219.220.93','60.251.120.79','60.251.120.80','210.65.89.147'];
  public $_SALT='sJw#333';


  /*
  * Default home page.
  */
	public function home() {

		global $tpl, $config, $deposit, $product, $member, $user;

    //設定 Action 相關參數
		set_status($this->controller);
		
		$dhid = 152;
		$get_deposit['amount'] = 500;
		$chkStr='saja#333';
		$cs = new convertString();
		$enc_chkStr=$cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
		echo $enc_chkStr;
		echo '<br>';
		$dec_chkStr = $cs->strDecode($enc_chkStr, $config['encode_key'],$config['encode_type']);
		echo $dec_chkStr;
	//var_dump($config["encode_key"]);
		exit;
		//login_required();
		/*
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		//取得支付項目
		$get_drid_list = $deposit->row_drid_list($json);
		$tpl->assign('get_drid_list', $get_drid_list);

		//取得殺價幣餘額
		$get_spoint = $member->get_spoint($userid);

    if(!$get_spoint){
      $get_spoint=array("userid"=>$userid, "amount"=>0);
    }

		$get_spoint['amount'] = sprintf("%1\$u", $get_spoint['amount']);
		$tpl->assign('spoint', $get_spoint);

		//會員卡包資料
		$user_extrainfo_category = $user->check_user_extrainfo_category();

		if(is_array($user_extrainfo_category)){
			foreach ($user_extrainfo_category as $key => $value){
				if($value['uecid'] == 8){
					if($json!='Y'){
						$user_extrainfo[$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}else{
						$user_extrainfo['a'.$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}
				}
			}
    }

		$tpl->assign('user_extrainfo', $user_extrainfo);


    // 20180809 By Thomas 取得該支付的綁定虛擬帳號
    $arrPayAcct=array();
		if(!empty($get_drid_list)){

			foreach ($get_drid_list as $key => $value){

        if($json != 'Y') {
					// 取得支付項目細項
					$row_list[$value['drid']] = $deposit->row_list($value['drid']);
					$tpl->assign('row_list', $row_list);
				} else {
					// API結構輸出變更專用
					$row_list2 = $deposit->row_list($value['drid']);
				}

        // 20180809 By Thomas 取得該用戶在該支付項目的虛擬帳號 (以 sso_deposit_rule.act欄位值為關連)
        if($value['act']) {
           $sso_row = $user->get_user_sso_list($userid,$value['act']);
           if($sso_row) {
              // ex : $arrPayVendor['gama']=....
              $arrPayAcct[$value['act']] =$sso_row;
           }
        }

				if($json != 'Y'){

					foreach ($row_list[$value['drid']] as $rk => $rv){

            $get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);

						if(!empty($get_scode_promote)){

              $i = 1;
							$scode_promote_memo[$rv['driid']] = array();

							foreach ($get_scode_promote as $sk => $sv) {

								if($sv['num']>0) {
								   $scode_promote_memo[$rv['driid']][$sk]['num'].= $sv['num'].' ';
								}else{
								   $scode_promote_memo[$rv['driid']][$sk]['num'].=$i.". ".$sv['name'].'<br>';
								}

								$scode_promote_memo[$rv['driid']][$sk]['offtime'].=$sv['offtime'].' ';
								$i++;
							}

					  }else{
							$scode_promote_memo[$rv['driid']][0]['num'] .= "無贈送任何東西";
							$scode_promote_memo[$rv['driid']][0]['offtime'] .= "";
					  }
          }

				  $tpl->assign('scode_promote_memo', $scode_promote_memo);

        }else{

  				foreach ($row_list2 as $rk => $rv){
  					$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);
  					if(!empty($get_scode_promote)){
  						$i = 1;
  						foreach($get_scode_promote as $sk => $sv){
  							if($sv['num']>0){
   								 $row_list2[$rk]['scode_promote_memo'][$sk]['num']=$sv['name'].' '.$sv['num'].' 張';
  							   $row_list2[$rk]['scode_promote_memo'][$sk]['offtime'] .= $sv['offtime'].' ';
  							}else{
  							   $row_list2[$rk]['scode_promote_memo'][$sk]['num']=$sv['name'];
  							   $row_list2[$rk]['scode_promote_memo'][$sk]['offtime'] .= $sv['offtime'].' ';
  							}
  							$i++;
  						}
  					}else{
  						$row_list2[$rk]['scode_promote_memo'][0]['num'] = "無贈送任何東西";
  						$row_list2[$rk]['scode_promote_memo'][0]['offtime'] .= "";
  					}
  				}
        }

			  $get_drid_list[$key]['row_list'] = $row_list2;
      }

      // 用戶支付項目的虛擬帳號
      $tpl->assign('pay_account_list', $arrPayAcct);

    }
		*/
		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_drid_list;
			$ret['retObj']['spoint'] = $get_spoint;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('footer','N');
			$tpl->set_title('');
			$tpl->render("deposit","home3",true);
		}

  }


	public function is_test_id($userid) {

    if($userid==1705 || $userid==996 || $userid==997 || $userid==9 || $userid==28 || $userid==116 || $userid==1 || $userid==1476 || $userid==2186 || $userid==1271) {
      return true;
    }else{
      return false;
    }

	}


	public function confirm() {

		global $tpl, $config, $deposit, $product, $member, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		//login_required();
		echo 8888;exit;
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid=empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
    $driid=empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
    $userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

    error_log("[c/deposit] drid:".$drid." , driid:".$driid." , userid:".$userid);

		/*
		//殺幣數量
		$get_spoint = $member->get_spoint($userid);
    if(!$get_spoint){
      $get_spoint=array("userid"=>$userid, "amount"=>0);
    }
		$tpl->assign('spoint', $get_spoint);

		//充值項目ID
		$row_list = $deposit->row_list($drid);
		$tpl->assign('row_list', $row_list);

		$deposit_rule = $deposit->deposit_rule($drid);
		$tpl->assign('deposit_rule', $deposit_rule);

		foreach($row_list as $rk => $rv){

			if((int)$rv['driid']==(int)($driid)){

        //取得儲值點數
				$get_deposit['drid'] = $drid;
				$get_deposit['driid'] = $rv['driid'];
				$get_deposit['name'] = $rv['name'];
				$get_deposit['amount'] = floatval($rv['amount']);
				$get_deposit['spoint'] = $rv['spoint'];
				break;

			}else{
			  continue;
			}

		} //endforeach;

		if($drid=='7' || $drid=='9') { //Hinet & 阿拉訂 : 點數兌換時才生訂單

			$get_deposit['drid']=$drid;

		}else if($drid != '7' && $drid !='9'){

			//新增deposit_history資訊
			$currenty=$config['currency'];
			if($drid=='6' || $drid=='8' || $drid=='10'){
			  $currency="NTD";
			}else{
        $currency="RMB";
      }

			if($this->is_test_id($userid)){
			   if($currency=="NTD"){
			     $get_deposit['amount']=2;
			   }else if($currency=="RMB"){
			     $get_deposit['amount']=0.01;
        }
			}

			$depositid = $deposit->add_deposit($userid, $get_deposit['amount'],$currency);
			$spointid = $deposit->add_spoint($userid, $get_deposit['spoint']);
			$dhid = $deposit->add_deposit_history($userid, $driid, $depositid, $spointid);
			$get_scode_promote = $deposit->get_scode_promote_rt($driid);

      if(!empty($get_scode_promote)){

        $i = 1;
				$spmemo = '';
				$spmemototal = 0;

        foreach ($get_scode_promote as $sk => $sv){

          $get_product_info = $product->get_info($sv['productid']);
					$spmemo.='<li class="d-flex align-items-center">
                    <div class="de_title"><p>送 殺價券</p><p>( '.$get_product_info['name'].' )</p></div><div class="de_point">';
					$spmemoapp[$sk]['name'] = "送 殺價券".$get_product_info['name'];

          if(!empty($sv['num'])){
					  $spmemo.=$sv['num'].' 張';
						$spmemoapp[$sk]['num'] = $sv['num'].' 張';
						$spmemototal = ($spmemototal+($sv['num']*$get_product_info['saja_fee']));
					}

					if(!empty($get_product_info['productid'])) {
  				  $spmemo.='</div></li>';
					}

          $i++;

			  }

      }else{

				$spmemo .= "<li>無贈送任何東西</li>";
				$spmemoapp[0]['name'] = "無贈送任何東西";
				$spmemoapp[0]['num'] = " 0 張";

			}

			if($drid== 4){
				$banklist=array();
				$banklist['BOCOM']="交通銀行";
				$banklist['CMB'] = "招商銀行";
				$banklist['CCB'] = "建設銀行";
				$banklist['SPDB'] = "浦發銀行";
				$banklist['GDB'] = "廣發銀行";
				$banklist['PSDB'] = "郵政儲蓄銀行";
				$banklist['CIB'] = "興業銀行";
				$banklist['HXB'] = "華夏銀行";
				$banklist['PAB'] = "平安銀行";
				$banklist['BOS'] = "上海銀行";
				$banklist['SRCB'] = "上海農商銀行";
				$banklist['BCCB'] = "北京銀行";
				$banklist['BRCB'] = "北京農商銀行";
				$banklist['CEB'] = "光大銀行";
				$banklist['ICBC'] = "工商銀行(建議使用PC流覽)";
				$banklist['BOCSH'] = "中國銀行(建議使用PC流覽)";
				$banklist['ABC'] = "農業銀行(建議使用PC流覽)";
				$banklist['MBC'] = "民生銀行(建議使用PC流覽)";
				$banklist['CNCB'] = "中信銀行(建議使用PC流覽)";
				$banklist['OTHERS'] = "其它" ;

				$tpl->assign('bank_list',$banklist);
			}

			//Create ordernumber
			$get_deposit['ordernumber'] = $dhid;

      // 如果是其他支付,需要抓其他支付的虛擬帳號
      if($drid=='10'){
        $arrSSO = $user->get_user_sso_list($userid);
        if($arrSSO && $arrSSO[0]){
					$tpl->assign('vendor_pay', $arrSSO);
        }
      }

    }
		*/
		$chkStr=$dhid."|".$get_deposit['amount'];
		$cs = new convertString();
		$enc_chkStr=$cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_deposit;
			$ret['retObj']['deposit_rule'] = $deposit_rule;
			$ret['retObj']['spmemo'] = $spmemoapp;
			$ret['retObj']['spmemototal'] = $spmemototal;
			$ret['retObj']['vendor_pay'] = $arrSSO;
			$ret['retObj']['bank_list'] = $banklist;
			$ret['retObj']['chkStr'] = $enc_chkStr;
			$ret['retObj']['htmlurl'] = $config[$deposit_rule[0]['act']]['htmlurl'];
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('chkStr',$enc_chkStr);
			$tpl->assign('get_deposit', $get_deposit);
			$tpl->assign('get_scode_promote', $get_scode_promote);
			$tpl->assign('spmemo', $spmemo);
			$tpl->assign('spmemototal', $spmemototal);
			$tpl->set_title('');

			$tpl->render("deposit","confirm",true);
		}

	}


	public function alipay_app() {

    global $tpl, $config, $deposit;

	}


	public function alipay_html() {

		global $tpl, $config, $deposit;

		$secretid = md5($_POST['out_trade_no'].$_POST['trade_no']);

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//充值項目ID
		if(($_POST['result'] == "success") && ($_POST['secretid'] == $secretid)){

			$alipay_array['out_trade_no'] = $_POST['out_trade_no'];  // deposit_history id
			$alipay_array['trade_no'] = $_POST['trade_no'];
			$alipay_array['result'] = $_POST['result'];

			$get_deposit_history = $deposit->get_deposit_history($alipay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);
			error_log("alipay:".date("H:i:s"));

			if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){

				$userid=$get_deposit_history[0]['userid'];

				$deposit->set_deposit_history($alipay_array, $get_deposit_history[0], "alipay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);

				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);

				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

        if(!empty($get_scode_promote)){
					foreach($get_scode_promote  as $sk => $sv){
						if($sv['productid'] == 0){
							$scode = $deposit->add_scode($userid, $sv);
							$deposit->add_scode_history($userid, $scode, $sv);
						}
						// 殺價券
						if(!empty($sv['productid']) && $sv['productid'] > 0){
					    if($this->is_house_promote_effective($sv['productid'])){

                // 判定是否符合萬人殺房資格, 符合者送殺價券
							  $this->register_house_promote($sv['productid'],$userid);

                for($i = 0; $i < $sv['num']; $i++){
								   $deposit->add_oscode($userid, $sv);
							  }

                // 推薦者送殺價券
							  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
						  }else{
								error_log("[c/deposit/alipay_html] sv : ".$sv['num']);
								// 不是房子(2854), 就直接送殺價券
							  for($i = 0; $i < $sv['num']; $i++){
								  $deposit->add_oscode($userid, $sv);
							  }
						  }
					  }
				  }
        }

				//首次儲值送推薦人2塊
				$this->add_deposit_to_user_src($userid);
				error_log("支付寶交易成功。");
				echo '<!DOCTYPE html><html><body><script>alert("充值成功!");window.location = "/site/member/";</script></body></html>';

      }else if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y'){
				error_log("支付寶交易 - 已充值成功。");
				echo '<!DOCTYPE html><html><body><script>alert("充值成功!");window.location = "/site/member/";</script></body></html>';
			}else{
				error_log("支付寶充值失敗。");
				echo '<!DOCTYPE html><html><body><script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script></body></html>';
			}

    }else{
			error_log("充值失敗。");
			echo '<!DOCTYPE html><html><body><script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script></body></html>';
		}

		exit;

	}


	// 背景通知 form bankcomm
	public function bankcomm_active_notify() {

	  global $tpl, $config, $deposit;

    set_status($this->controller);
    $notifyMsg = $_POST['notifyMsg'];
    $arr = preg_split("/\|{1,}/",$notifyMsg);
    $msg="";

		if(is_array($arr)){

      $size=count($arr);

      for($i=0;$i<$size-1;$i++){
		    if($i==$size-2){
		      $msg.=base64_decode($arr[$i]);
		    }else{
	        $msg.=$arr[$i]."|";
		    }
	    }

    	$merId=$arr[0];
			$orderid=$arr[1];
			$amount=$arr[2];
			$curType=$arr[3];          // ex: CNY
			$platformBatchNo=$arr[4];  // Usually leave blank
			$merPayBatchNo=$arr[5];    // Optional, assign by merchant
			$orderDate=$arr[6];        // ex: 20141015
			$orderTime=$arr[7];        // ex: 095124
			$bankSerialNo=$arr[8];     // ex: C7D367EC
			$tranResultCode=$arr[9];   // ex: 1 [success]
			$feeSum=$arr[10];             // Total fee
			$cardType=$arr[11];
			$bankMono=$arr[12];
			$errMsg=$arr[13];
			$remoteAddr=$arr[14];
			$refererAddr=$arr[15];
			$merComment=base64_decode($arr[16]);

    }

		error_log("bankcomm_active_notify from bankcomm : ".$msg);
	  error_log("transResultCode : ".$tranResultCode);

    if($tranResultCode=='1'){
      $bankcomm_array['out_trade_no'] = $orderid;
      $bankcomm_array['trade_no'] =$bankSerialNo;
      $bankcomm_array['amount'] = $amount;
      $bankcomm_array['result'] = $tranResultCode;
      $bankcomm_array['orderDate'] = $orderDate;
      $bankcomm_array['orderTime'] = $orderTime;
      $bankcomm_array['orderMono'] = $merComment;

      $get_deposit_history = $deposit->get_deposit_history($orderid);
      $get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

			if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'N'){

				$userid = $get_deposit_history[0]['userid'];

				$deposit->set_deposit_history($bankcomm_array, $get_deposit_history[0], "bankcomm");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);
				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				$arr_cond=array();
				$arr_update=array();
				$arr_cond['dhid']=$orderid;
				$arr_update['modifiertype']="system";
				$arr_update['modifiername']="bankcomm_active_notify";
				$deposit->update_deposit_history($arr_cond,$arr_update);

				if(!empty($get_scode_promote)){
			    foreach ($get_scode_promote  as $sk => $sv){
            if ($sv['productid'] == 0){
			 		    $scode = $deposit->add_scode($userid, $sv);
			 		    $deposit->add_scode_history($userid, $scode, $sv);
			 	    }
			 	    if ($sv['productid'] > 0 && !empty($sv['productid'])){
		 	        if($this->is_house_promote_effective($sv['productid'])){
							  // 判定是否符合萬人殺房資格, 符合者送殺價券
							  $this->register_house_promote($sv['productid'],$userid);
                for ($i = 0; $i < $sv['num']; $i++){
								   $deposit->add_oscode($userid, $sv);
							  }
							  // 推薦者送殺價券
							  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
              }else{
                // 不是房子(2854), 就直接送殺價券
							  for($i = 0; $i < $sv['num']; $i++){
								  $deposit->add_oscode($userid, $sv);
							  }
						  }
			 	    }
			    }
        }

			}else if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'Y'){
			   error_log("[deposit] Order id : ".$orderid." has been deposited OK , nothing to do !!");
			}

	  }else{
	       error_log("[deposit] Order id : ".$orderid." is failed , please check with bankcomm !!");
	  }
		echo '1';
		exit;

	}


	// 同步通知 form bankcomm
	public function bankcomm_html(){

		global $tpl, $config, $deposit;

		set_status($this->controller);

		//判斷來源是否為APP,若是則導向銀聯APP的專屬function
		if ($_POST['json'] == 'Y'){
			$this->bankcomm_app();
			exit;
		}

    $notifyMsg = $_POST['notifyMsg'];
    error_log("bankcomm_html from bankcomm : ".$notifyMsg);
    $arr = preg_split("/\|{1,}/",$notifyMsg);

		if(is_array($arr)){
		   $merId=$arr[0];
		   $orderid=$arr[1];
		   $amount=$arr[2];
		   $curType=$arr[3];          // ex: CNY
		   $platformBatchNo=$arr[4];  // Usually leave blank
		   $merPayBatchNo=$arr[5];    // Optional, assign by merchant
		   $orderDate=$arr[6];        // ex: 20141015
		   $orderTime=$arr[7];        // ex: 095124
		   $bankSerialNo=$arr[8];     // ex: C7D367EC
		   $tranResultCode=$arr[9];   // ex: 1 [success]
		   $feeSum=$arr[10];          // Total fee
		   $cardType=$arr[11];
		   $bankMono=$arr[12];
		   $errMsg=$arr[13];
		   $remoteAddr=$arr[14];
		   $refererAddr=$arr[15];
		   $merComment=base64_decode($arr[16]);
		   $signMsg=$arr[17];
	  }
		error_log("merComment is : ".$merComment);
		error_log("transResultCode : ".$tranResultCode);

    if($tranResultCode=='1'){
	    $bankcomm_array['out_trade_no'] = $orderid;
	    $bankcomm_array['trade_no'] =$bankSerialNo;
	    $bankcomm_array['amount'] = $amount;
	    $bankcomm_array['result'] = $tranResultCode;
	    $bankcomm_array['orderDate'] = $orderDate;
	    $bankcomm_array['orderTime'] = $orderTime;
	    $bankcomm_array['orderMono'] = $merComment;

	    $get_deposit_history = $deposit->get_deposit_history($bankcomm_array['out_trade_no']);
	    $get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

		  if (!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'N'){

			  $userid=$get_deposit_history[0]['userid'];

        $deposit->set_deposit_history($bankcomm_array, $get_deposit_history[0], "bankcomm");
        $deposit->set_deposit($get_deposit_history[0]['depositid']);
        $deposit->set_spoint($get_deposit_history[0]['spointid']);
        $get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
        $get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				if(!empty($get_scode_promote)){
			    foreach($get_scode_promote  as $sk => $sv){
						if ($sv['productid'] == 0) {
					        $scode = $deposit->add_scode($userid, $sv);
					        $deposit->add_scode_history($userid, $scode, $sv);
					  }
						if ($sv['productid'] > 0 && !empty($sv['productid'])){
			        if($this->is_house_promote_effective($sv['productid'])){
							  // 判定是否符合萬人殺房資格, 符合者送殺價券
							  $this->register_house_promote($sv['productid'],$userid);
							  for ($i = 0; $i < $sv['num']; $i++) {
								   $deposit->add_oscode($userid, $sv);
							  }
							  // 推薦者送殺價券
							  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
							  // 不是房子(2854), 就直接送殺價券
							  for ($i = 0; $i < $sv['num']; $i++) {
								   $deposit->add_oscode($userid, $sv);
							  }
						  }
					  }
			    }
				}

				error_log("銀聯交易成功。");
			  echo '<!DOCTYPE html><html><body><script>alert("充值成功!");window.location = "/site/product/";</script></body></html>';

      }else if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'Y'){
			  error_log("[deposit] Order id : ".$orderid." has been deposited OK , nothing to do !!");
			}

		}else{
		  error_log("銀聯交易失敗。");
			echo '<!DOCTYPE html><html><body><script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script></body></html>';
		}

    exit;

	}


	public function weixinpay_html(){

		global $tpl, $config, $deposit;

		error_log("[c/deposit/weixinpay_html] POST : ".json_encode($_POST));
		error_log("[c/deposit/weixinpay_html] GET : ".json_encode($_GET));
		error_log("[c/deposit/weixinpay_html] orderid : ".$_POST['orderid'].", amount : ".$_POST['amount'].", secretid : ".$_POST['secretid'].", productid : ".$_POST['productid']);

    //設定 Action 相關參數
		set_status($this->controller);

		//判斷來源是否為APP,若是則導向微信APP的專屬function
		if ($_POST['json'] == 'Y'){
			$this->weixinpay_app();
			exit;
		}

		//充值項目ID
		if($_POST['orderid'] && ($_POST['secretid'] == $secretid)){

			$weixinpay_array['out_trade_no'] = $_POST['orderid'];
			$weixinpay_array['amount'] = $_POST['amount'];

			$get_deposit_history = $deposit->get_deposit_history($weixinpay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

			if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){
			  // 交易金額比對
				error_log("[weixinpay_html] : ".floatval($_POST['amount'])."<==>".floatval($get_deposit_id[0]['amount']));
				if(floatval($_POST['amount'])!=floatval($get_deposit_id[0]['amount'])){
				  $ret['status'] = 3;
				  error_log("[weixinpay_html] 微信支付事務數據異常。");
				  echo $ret['status'];
				  exit;
				}
				$deposit->set_deposit_history($weixinpay_array, $get_deposit_history[0], "weixinpay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);

				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);
				error_log("[c/deposit/weixinpay_html] get_scode_promote : ".json_encode($get_scode_promote));

				$userid=$get_deposit_history[0]['userid'];

				if(!empty($get_scode_promote)){
					foreach($get_scode_promote  as $sk => $sv){
						if ($sv['productid'] > 0 && !empty($sv['productid'])){
							if($this->is_house_promote_effective($sv['productid'])){
							  // 判定是否符合萬人殺房資格, 符合者送殺價券
							  $this->register_house_promote($sv['productid'],$userid);
							  for ($i = 0; $i < $sv['num']; $i++) {
								   $deposit->add_oscode($userid, $sv);
							  }
							  // 推薦者送殺價券
							  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
  						  // 不是房子(2854), 就直接送殺價券
  						  for ($i = 0; $i < $sv['num']; $i++) {
  							   $deposit->add_oscode($userid, $sv);
  						  }
								error_log("[c/deposit/weixinpay_html] sv : ".$sv['num']);
							}
						}
					}
				}

				//首次儲值送推薦人2塊
				$this->add_deposit_to_user_src($userid);

				//回傳:
				$ret['status'] = 200;
				error_log("[weixinpay_html]微信支付交易-成功。");

			}else if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y'){
				$ret['status'] = 2;
				error_log("[weixinpay_html]微信支付重複充值。");
			}else{
				$ret['status'] = 1;
				error_log("[weixinpay_html]微信支付充值失敗1。");
			}

    }else{
			$ret['status'] = 1;
			error_log("[weixinpay_html]微信支付充值失敗2。");
		}

    echo $ret['status'];
		exit;

	}


	public function alipay_complete() {

		global $tpl, $config, $deposit;

		$tpl->set_title('');
		$tpl->render("deposit","alipay_complete",true);

	}


	public function ibonpay() {

    global $tpl, $config, $deposit;

    set_status($this->controller);
    login_required();

	  $user_info=$_SESSION['user'];

    if(empty($payname)){
      $payname=$user_info['profile']['nickname'];
    }

    $success = 0;

    $pay_info = Array(
                      "transurl" => $config['ibonpay']['url_payment'],
                      "code" => $config['ibonpay']['code'],
                      "ordernumber" => $_POST['ordernumber'],
                      "merchantnumber" => $config['ibonpay']['merchantnumber'],
                      "paymenttype" => $config['ibonpay']['paymenttype'],
                      "amount" => $_POST['amount'],
                      "duedate" => "",
                      "payname" => $user_info['profile']['addressee'],
                      "payphone" => $user_info['profile']['phone'],
                      "nexturl" => $config['ibonpay']['nexturl'],
                      "returnvalue" => "1",
                      "reprint" => "1",
                      "bankid" => "",
                      "paytitle" => "",
                      "paymemo" => ""
    );

		$plaintext = $pay_info['merchantnumber'].
    $pay_info['code'].
    $pay_info['amount'].
    $pay_info['ordernumber'];
    error_log("[ibonpay] ibon plaintext : ".$plaintext);

    $hash = md5($plaintext);
    error_log("[ibonpay] Hash is :".$hash);


    $postdata = "merchantnumber=".$pay_info['merchantnumber'].
          	    "&ordernumber=".$pay_info['ordernumber'].
          	    "&amount=".$pay_info['amount'].
          	    "&paymenttype=".$pay_info['paymenttype'].
          	    "&bankid=".$pay_info['bankid'].
          	    "&paytitle=".urlencode($pay_info['paytitle']).
          	    "&paymemo=".urlencode($pay_info['paymemo']).
          	    "&payname=".urlencode($pay_info['payname']).
          	    "&payphone=".$pay_info['payphone'].
          	    "&duedate=".$pay_info['duedate'].
          	    "&returnvalue=".$pay_info['returnvalue'].
          	    "&hash=".$hash.
          	    "&nexturl=".urlencode($pay_info['nexturl']);

		$url = parse_url($pay_info['transurl']);

    $postdata = "POST ".$url['path']." HTTP/1.0\r\n".
          			"Content-Type: application/x-www-form-urlencoded\r\n".
          			"Host: ".$url['host']."\r\n".
          			"Content-Length: ".strlen($postdata)."\r\n".
          			"\r\n".
          			$postdata;

		$receivedata = "";
		$fp = fsockopen ($url['host'], $url['port'], $errno, $errstr, 90);

		if(!$fp){
			echo "$errstr ($errno)<br>\n";
		}else{
			fputs ($fp, $postdata);
			do{
				if(feof($fp)) break;
				$tmpstr = fgets($fp,128);
				$receivedata = $receivedata.$tmpstr;
			}while(true);
			fclose ($fp);
		}

  	$receivedata = str_replace("\r","",trim($receivedata));
  	$isbody = false;
  	$httpcode = null;
  	$httpmessage = null;
  	$result = "";
  	$array1 = explode("\n",$receivedata);
		for($i=0;$i<count($array1);$i++){
			if($i==0){
			   $array2 = explode(" ",$array1[$i]);
			   $httpcode = $array2[1];
			   $httpmessage = $array2[2];
			}else if(!$isbody){
			   if(strlen($array1[$i])==0) $isbody = true;
			}else{
			   $result = $result.$array1[$i];
			}
		}

		if($httpcode!="200"){
  		if($httpcode=="404")
  		  error_log ("URL Not Found !!");
  		else if($httpcode=="500")
  		  error_log("Destination Server Error !!");
  		else
  		  error_log($httpmessage);
  		return;
		}

		$pay_result= Array();
		//-- 分割資料取參數
		$ary1 = explode("&",$result);
		for($i=0;$i<count($ary1);$i++){
		  $ary2 = explode("=",$ary1[$i]);
		  $pay_result[$ary2[0]]=$ary2[1];
		}
    // $ary2 looks like :
		// Array ( [0] => rc [1] => 0 )
		// Array ( [0] => amount [1] => 2551 )
		// Array ( [0] => merchantnumber [1] => 456025 )
		// Array ( [0] => ordernumber [1] => 2228 )
		// Array ( [0] => paycode [1] => NW14103100190276 )
		// Array ( [0] => checksum [1] => 79c43472e3d3fd1c0fb509263ef7411c

		// 將訂單號,paycode和checksum 等等暫時寫入saja_deposit_history
		$arr_wherecond['dhid']=$pay_info['ordernumber'];
		$arr_update['data']=json_encode($pay_result);
		error_log($arr_update['data']);
		$deposit->update_deposit_history($arr_wherecond,$arr_update);

    $tpl->assign('pay_result',$pay_result);
    $tpl->assign('pay_info',$pay_info);
    $tpl->set_title('');
    $tpl->render("deposit","ibonpay_result",true);

	}


	public function ibonpay_success() {

    global $tpl, $config, $deposit;
    set_status($this->controller);

    error_log("[ibonpay_success] Referer :".$_SERVER['HTTP_REFERER']);
    if(md5("www.saja.com.tw")!=$_SERVER['HTTP_REFERER']) {
      echo("-99=NOT FROM SAJA");
      error_log("Request is not from www.saja.com.tw, Rejected !!");
      exit;
    }

		// 資料確認
		$orderid=$_POST['ordernumber'];
		error_log("[ibonpay_success] Checking orderid: ".$orderid);
		$get_deposit_history = $deposit->get_deposit_history($orderid);

    if(empty($get_deposit_history)){
		  error_log("Orderid: ".$orderid." data not found !!");
			echo "-1=DATA NOT FOUND";
			exit;
		}

		if(empty($get_deposit_history[0]['dhid'])) {
		  error_log("Orderid: ".$orderid." error !!");
			echo "-2=ERROR ORDER ID";
			exit;
		}


		$spointid=$get_deposit_history[0]['spointid'];

		$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

    if(!empty($get_deposit_id[0]['amount'])){
		  error_log("The Amount of deposit id: ".$get_deposit_history[0]['depositid']." is: ".$get_deposit_id[0]['amount']);
			if($get_deposit_id[0]['amount']!=$_POST['amount']){
				error_log("Not identical to request: ".$_POST['amount']." !!");
				echo "-3=AMOUNT NOT IDENTICAL";
				exit;
			}
		}

		$amount=$get_deposit_id[0]['amount'];

		foreach($_POST as $key=>$value){
		  $ibonpay_array[$key]=$value;
		}
		/*
		$ibonpay_array['merchantnumber']     = $_POST['merchantnumber']; // merchantnumber => 456025
		$ibonpay_array['out_trade_no']       = $orderid;  				 // ordernumber => 2469
		$ibonpay_array['amount']             = $amount;   				 // amount => 35
		$ibonpay_array['paymenttype']        = $_POST['paymenttype'];    // paymenttype => IBON, FAMIPORT ...
		$ibonpay_array['serialnumber']       = $_POST['serialnumber'];  //  serialnumber => 10463556
		$ibonpay_array['writeoffnumber']     = $_POST['writeoffnumber'];  // writeoffnumber => 041110FU994161(User收據上的序號)
		$ibonpay_array['timepaid']           = $_POST['timepaid'];
		$ibonpay_array['tel']                = $_POST['tel'];
		*/
		if($get_deposit_id[0]['switch']=='N'){
			$deposit->set_deposit_history($ibonpay_array, $get_deposit_history[0], "ibonpay");
			$deposit->set_deposit($get_deposit_history[0]['depositid']);
			$deposit->set_spoint($get_deposit_history[0]['spointid']);
			$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
			$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

			if (!empty($get_scode_promote)){
				$userid=$get_deposit_history[0]['userid'];

				foreach ($get_scode_promote  as $sk => $sv){
					if($sv['productid'] == 0){
						$scode = $deposit->add_scode($userid, $sv);
						$deposit->add_scode_history($userid, $scode, $sv);
					}
					if($sv['productid'] > 0 && !empty($sv['productid'])){
						if($this->is_house_promote_effective($sv['productid'])){
  					  // 判定是否符合萬人殺房資格, 符合者送殺價券
  					  $this->register_house_promote($sv['productid'],$userid);
  					  for($i = 0; $i < $sv['num']; $i++){
  						  $deposit->add_oscode($userid, $sv);
  					  }
  					  // 推薦者送殺價券
  					  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
						}else{
  					  //不是房子(2854), 就直接送殺價券
  					  for ($i = 0; $i < $sv['num']; $i++) {
  						   $deposit->add_oscode($userid, $sv);
  					  }
						}
					}
				}
			}

			error_log("[ibonpay_success] ibon success notify : ".json_encode($ibonpay_array));
			echo "1=OK";

		}else if($get_deposit_id[0]['switch'] == 'Y'){
			error_log("Order id: ".$orderid." had been enabled , cannot be enable again !!");
			echo "-4=DUPLICATED DEPOSIT";
		}else{
		  error_log("Order id: ".$orderid." status is unknown !!");
			echo "-5=UNKNOW DEPOSIT STATUS";
		}

	  exit;

	}


	public function alading_exc() {

    global $tpl, $config, $deposit;

    set_status($this->controller);
    login_required();

    $this->hinetpts_exc();

	}


	public function hinetpts_exc() {

    global $tpl, $config, $deposit;
    set_status($this->controller);
    login_required();

    $drid=$_POST['drid'];
    $userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

    if($drid=='7'){
      $ptype=$config['hinet']['paymenttype'];
      $curr_type='NTD';
    }else if($drid=='9'){
      $ptype=$config['alading']['paymenttype'];
      $curr_type='RMB';
    }

    $serial_no=addslashes($_POST['serial_no']);
    if(empty($serial_no)){
      echo '<script>alert("序號不可為空白, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      exit;
    }
    if($serial_no!=$_POST['serial_no']){
      echo '<script>alert("序號格式錯誤, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      exit;
    }


    $passwd=addslashes($_POST['passw']);
    if(empty($passwd)){
      echo '<script>alert("密碼不可為空白, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      exit;
    }
    if($passwd!=$_POST['passw']){
      echo '<script>alert("密碼格式錯誤, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      exit;
    }

    $ch_serial=$deposit->get_ch_serial($serial_no);

    if(!$ch_serial){
       echo '<script>alert("序號錯誤, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
       exit;
    }

    if($passwd!=$ch_serial['passw']){
      $arr_update=array();
      $arr_update['userid']=$userid;
      $arr_update['err_num']=$ch_serial['err_num']+1;
      if($arr_update['err_num']<3){
       echo '<script>alert("密碼錯誤 : '.$arr_update['err_num'].' 次, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      }else{
       $arr_update['switch']='L';
       echo '<script>alert("密碼輸入錯誤 3 次,序號已鎖定暫停兌換,請洽客服人員 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      }
      $deposit->update_ch_serial($serial_no, $arr_update);
      exit;
    }

    if($ch_serial['switch']=='L'){
      echo '<script>alert("本序號已暫停兌換,請洽客服人員 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      exit;
    }
    if($ch_serial['switch']=='Y'){
      echo '<script>alert("本序號已使用過, 無法再兌換殺幣 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
      exit;
    }
    if($passwd==$ch_serial['passw']){

      $arr_update=array();
      $spoint=$ch_serial['point'];
      error_log("[c/hinetpts_exc] drid=${drid}, spoint=${spoint} ...");

      $deposit_rule = $deposit->deposit_rule_test($drid);
      if($deposit_rule){
        $deposit_rule_item=$deposit->get_deposit_rule_item_by_drid_spoint($drid,$spoint);
        if($deposit_rule_item){
      	  $driid=$deposit_rule_item['driid'];
          error_log("[c/hinetpst_exc] driid=${driid} ...");

        	// 產生訂單及儲值資料
        	$depositid = $deposit->add_deposit($userid, $deposit_rule_item['amount'],$curr_type);
        	$spointid = $deposit->add_spoint($userid,  $deposit_rule_item['spoint']);
        	$dhid = $deposit->add_deposit_history($userid, $driid, $depositid, $spointid);

        	error_log("[hinetpst_exc] dhid=${dhid}, spointid=${spointid}, depositid=${depositid} ...");

          // 修改訂單及儲值狀態
          $get_deposit_history = $deposit->get_deposit_history($dhid);
          $get_deposit_id = $deposit->get_deposit_id($depositid);

          error_log("[c/hinetpts_exc] saja_deposit switch : ".$get_deposit_id[0]['switch']);

      	  if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){
            $hinet_array['out_trade_no'] = $get_deposit_history[0]['dhid'];
            $hinet_array['spoints'] = $spoint;
            $hinet_array['userid']=$userid;
            $hinet_array['serialno']=$serial_no;

            $deposit->set_deposit_history($hinet_array, $get_deposit_history[0], $ptype);

            $deposit->set_deposit($get_deposit_history[0]['depositid']);

            $deposit->set_spoint($get_deposit_history[0]['spointid']);

        		$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
        		$get_scode_promote = $deposit->get_scode_promote_rt_test($get_deposit_rule_item[0]['driid']);

            if(!empty($get_scode_promote)){
        			foreach($get_scode_promote  as $sk => $sv){
        				if($sv['productid'] == 0){
        					$scode = $deposit->add_scode($userid, $sv);
        					$deposit->add_scode_history($userid, $scode, $sv);
        				}
        				if($sv['productid'] > 0 && !empty($sv['productid'])){
        					if($this->is_house_promote_effective($sv['productid'])){
        					  // 判定是否符合萬人殺房資格, 符合者送殺價券
        					  $this->register_house_promote($sv['productid'],$userid);
        					  for($i = 0; $i < $sv['num']; $i++){
        						  $deposit->add_oscode($userid, $sv);
        					  }
        					  // 推薦者送殺價券
        					  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
        					}else{
        					  // 不是房子(2854), 就直接送殺價券
        					  for ($i = 0; $i < $sv['num']; $i++) {
        						   $deposit->add_oscode($userid, $sv);
        					  }
        					}
        				}
        			}
        	  }
        		$arr_update=array();
        		$arr_update['userid']=$userid;
        		$arr_update['switch']='Y';
        		$arr_update['dhid']=$dhid;
        		$arr_update['memo']=$ptype;
        		$arr_update['modifyerid']=$userid;
        		$deposit->update_ch_serial($serial_no,$arr_update);
        		echo '<script>alert("兌換成功, 已新增殺幣 '.$spoint.' 點 !!");window.location = "/site/member?channelid=1"</script>';
      	  }
        }
      }
    }

    exit;

	}


	/*
	 *	信用卡支付送出
	 *	$userid					int						會員編號
	 *	$json					varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$drid					int						儲值方式編號
	 *	$driid					int						儲值方案編號
	 *	$amount					int						充值金額
	 *	$ordernumber			varchar					訂單編號
	 *	$spoint					int						充值點數
	 *	$chkStr					varchar					加密簽名
	 */
	public function twcreditcard_pay() {

		global $tpl, $config, $deposit;

		set_status($this->controller);
		login_required();

		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_GET['json'])?htmlspecialchars($_POST['json']):htmlspecialchars($_GET['json']);
		$drid = empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
		$driid = empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
		$amount = empty($_GET['amount'])?htmlspecialchars($_POST['amount']):htmlspecialchars($_GET['amount']);
		$spoint = empty($_GET['spoint'])?htmlspecialchars($_POST['spoint']):htmlspecialchars($_GET['spoint']);
		$chkStr = empty($_GET['chkStr'])?htmlspecialchars($_POST['chkStr']):htmlspecialchars($_GET['chkStr']);
		$ordernumber = empty($_GET['ordernumber'])?htmlspecialchars($_POST['ordernumber']):htmlspecialchars($_GET['ordernumber']);

		// 檢查交易金額是否正確
		if(floatval($amount)<0){
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10001,'交易金額錯誤','json');
				echo json_encode($ret);
			}else{
				die('<script>alert("交易金額錯誤!!");history.back();</script>');
				exit;
			}
		}

		// 檢查是否正確
		if(empty($chkStr)){
			if($json == 'Y'){
				$ret = getRetJSONArray(-10002,'加密簽名資料錯誤','json');
				echo json_encode($ret);
			}else{
				die('<script>alert("加密簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}

		$pay_info=array();
		$pay_info['web']=$config['creditcard']['merchantnumber'];
		$pay_info['MN']= floatval($amount);
		$pay_info['Td']=$ordernumber;
		$pay_info['sna']=urlencode($_SESSION['user']['profile']['nickname']."(".$userid.")");
		$pay_info['sdt']=$_SESSION['user']['profile']['phone'];

    if(!is_numeric($pay_info['sdt']))
        $pay_info['sdt']='1234567890';

    $pay_info['email']=$_SESSION['user']['email'];
		$pay_info['note1']="{userid:".$userid.",OrderId:".$ordernumber.",Amount:".floatval($amount)."}";
		$pay_info['note2']="saja_tw";
		$pay_info['name']=$_SESSION['user']['profile']['nickname'];

    $pay_info['OrderInfo']=urlencode("OrderId:".$pay_info['Td'].
        										",Name:".$_SESSION['user']['profile']['nickname'].
        										",Userid:".$userid.
        										",Amount:".$pay_info['MN'].
        										",Spts:".$spoint);

    $pay_info['Card_Type']=$config['creditcard']['Card_Type'];
		$chkvalue_ori=$pay_info['web'].$config['creditcard']['code'].$pay_info['MN'];
		$pay_info['ChkValue']=strtoupper(sha1($chkvalue_ori));
		$pay_info['web_app_android'] = $config['creditcard']['merchantnumber_app'];	//app android商家編號
		$pay_info['web_app_ios'] = $config['creditcard']['merchantnumber_app'];	//app ios商家編號
		$pay_info['TransPwd'] = $config['creditcard']['code'];	//交易密碼
		$pay_info['PayType'] = "credit";	//android付款方式 credit: 信用卡 MIT_AppPayment: 感應收款 paycode: 超商代碼繳費 barcode: 超商條碼繳費 atm_account: ATM轉帳繳款
		$pay_info['RSTransactionPayType'] = "RSTransactionPayTypeCredit";	//ios付款方式 RSTransactionPayTypeCredit：信用卡收款 RSTransactionPayTypeMIT：感應收款 RSTransactionPayTypePaycode：超商代碼收款 RSTransactionPayTypeBarcode：超商條碼收款 RSTransactionPayTypeATMAccount：ATM轉帳收款

    $pay_info['Term'] = "";	//web,android分期期數
		$pay_info['RSTransactionInstallmentTermValue'] = $config['creditcard']['RSTransactionInstallmentTermValue'];//ios分期期數 RSTransactionInstallmentTermNone：不分期 RSTransactionInstallmentTerm3：3期 RSTransactionInstallmentTerm6：6期 RSTransactionInstallmentTerm12：12期 RSTransactionInstallmentTerm18：18期 RSTransactionInstallmentTerm24：24期

    $pay_info['isproduction_android'] = 'true';	//android是否為正式環境（true為正式環境，false為測試環境)
    $pay_info['isproduction_ios'] = 'YES';	//ios是否為正式環境（YES為正式環境，NO為測試環境)
		$pay_info['needSign_android'] = 'false';	//android是否需要簽名面版(true為需要,false為不需要)
		$pay_info['needSign_ios'] = 'NO';	//ios是否需要簽名面版（YES為需要，NO為不需要）
		$pay_info['timeoutSec_android'] = 500;	//android限制信用卡輸入畫面的輸入時間（秒）
		$pay_info['timeoutSec_ios'] = 500;	//ios限制信用卡輸入畫面的輸入時間（秒）
		$pay_info['orderNoChk_android'] = 'true';	//android是否加強訂單編號重複檢查（true為一律檢查，false為第二次送出交易時檢查）
		$pay_info['isCheckOrderNo_ios'] = 'YES';	//ios檢查自訂訂單編號是否重複(YES為檢查，NO為不檢查)
		$pay_info['Language_android'] = 1;	//android交易畫面的語系設定 0：手機預設，1：繁體中文，2：英文，3：簡體中文
		$pay_info['RSTransactionLanguage_ios'] = 'RSTransactionLanguageZhHant';	//ios交易畫面的語系設定，(若沒有進行設定則會自動選擇繁體中文)：RSTransactionLanguageAuto：自動切換。 RSTransactionLanguageEn：英文 RSTransactionLanguageZhHant：繁體中 RSTransactionLanguageZhHans： 簡體中文

    // for測試環境
    if($_SESSION['auth_id']=='1705'){
      $pay_info['isproduction_ios'] = 'NO';
      $pay_info['isproduction_android'] = 'false';
    }
    if($pay_info['isproduction_ios']=='NO'){
      $pay_info['web_app_ios'] = $config['creditcard']['merchantnumber_app_test'];
    }
    if($pay_info['isproduction_android']=='false'){
      $pay_info['web_app_android'] = $config['creditcard']['merchantnumber_app_test'];
    }

		$cs=new convertString();
		$chkStr=$cs->strDecode($chkStr,$config["encode_key"],$config["encode_type"]);
		error_log("[c/deposit/twcreditcard_pay] chkStr : ".$chkStr);
		$chkArr=explode("|",$chkStr);

		if(is_array($chkArr)){

      $chk_orderid=$chkArr[0];
			$chk_amount=$chkArr[1];

			if(floatval($chk_amount)!=floatval($amount)){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10003,'交易金額不符','MSG');
					echo json_encode($ret);
				}else{
					die('<script>alert("交易金額不符 !!");history.back();</script>');
					exit;
				}
			}

      if($chk_orderid!=$ordernumber){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10004,'訂單編號不符!!','MSG');
					echo json_encode($ret);
				}else{
					die('<script>alert("訂單編號不符 !!");history.back();</script>');
					exit;
				}
			}

    }else{
			if($json == 'Y'){
				$ret = getRetJSONArray(-10005,'簽名資料錯誤 !!','MSG');
				echo json_encode($ret);
			}else{
				die('<script>alert("簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}

		$get_deposit_history=$deposit->get_deposit_history($pay_info['Td']);

		if(!empty($get_deposit_history[0]['dhid'])){

			$arr_cond=array();
			$arr_cond['dhid']=$pay_info['Td'];
			$arr_data=array();
			$arr_data['out_trade_no']=$pay_info['Td'];
			$arr_data['userid']=$userid;
			$arr_data['amount']= intval($pay_info['MN']);
			$arr_data['timepaid']=date('YmdHis');
			$arr_data['phone']=$pay_info['sdt'];
			$arr_data['paymenttype']=$config['creditcard']['paymenttype'];
			$arr_date['ChkValue']=$pay_info['ChkValue'];

			$arr_update['data']=json_encode($arr_data);
			$arr_update['modifierid']=$userid;
			$arr_update['modifiername']=$_SESSION['user']['profile']['nickname'];
			$arr_update['modifiertype']='User';

			$deposit->update_deposit_history($arr_cond, $arr_update);

			if($json=='Y'){

				$data['PayType'] = $pay_info['PayType']; //andriod付款方式
				$data['RSTransactionPayType'] = $pay_info['RSTransactionPayType']; //ios付款方式

				$data['web_android'] = $pay_info['web_app_android']; //app 商家編號
				$data['web_ios'] = $pay_info['web_app_ios']; //app 商家編號

				$data['TransPwd'] = $pay_info['TransPwd']; //交易密碼
				$data['MN'] = $pay_info['MN']; //交易金額

				$data['Term'] = $pay_info['Term']; //android分期期數
				$data['RSTransactionInstallmentTermValue'] =$pay_info['RSTransactionInstallmentTermValue'];//ios分期期數

				$data['OrderInfo'] = urldecode($pay_info['OrderInfo']); //交易內容
				$data['Td'] = "SJO".$pay_info['Td']; //商家訂單編號
				$data['CustomerName'] = urldecode($pay_info['name']); //消費者姓名
				$data['Mobile'] = $pay_info['sdt']; //消費者電話
				$data['email'] = $pay_info['email']; //消費者email
				$data['note1'] = $pay_info['note1']; //備註1
				$data['note2'] = $pay_info['note2']; //備註2

				$data['isproduction_android'] = $pay_info['isproduction_android']; //android是否為正式環境
				$data['isproduction_ios'] = $pay_info['isproduction_ios']; //ios是否為正式環境
				$data['needSign_android'] = $pay_info['needSign_android']; //android是否需要簽名面版
				$data['needSign_ios'] = $pay_info['needSign_ios']; //ios是否需要簽名面版
				$data['timeoutSec_android'] = $pay_info['timeoutSec_android']; //android限制信用卡輸入畫面的輸入時間（秒）
				$data['timeoutSec_ios'] = $pay_info['timeoutSec_ios']; //ios限制信用卡輸入畫面的輸入時間（秒）
				$data['orderNoChk_android'] = $pay_info['orderNoChk_android']; //android是否加強訂單編號重複檢查
				$data['isCheckOrderNo_ios'] = $pay_info['isCheckOrderNo_ios']; //ios檢查自訂訂單編號是否重複
				$data['Language_android'] = $pay_info['Language_android']; //android交易畫面的語系設定
				$data['RSTransactionLanguage_ios'] = $pay_info['RSTransactionLanguage_ios']; //ios交易畫面的語系設定

				$ret = getRetJSONArray(1,'SUCCESS','LIST');
        $ret['retObj']['data'] = $data;
        error_log("[c/deposit/tw_credit_pay]: ".json_encode($ret));
				echo json_encode($ret);

      }else{

				$submit='<body onload="document.form1.submit();" >';
				$submit.='<form name="form1" action="'.$config['creditcard']['url_payment'].'" method="POST">';
				$submit.='<input type="hidden" name="web" value="'.$config['creditcard']['merchantnumber'].'" />';
				$submit.='<input type="hidden" name="MN" value="'. intval($pay_info['MN']).'" />';
				$submit.='<input type="hidden" name="OrderInfo" value="'.$pay_info['OrderInfo'].'" />';
				$submit.='<input type="hidden" name="Td" value="SJO'.$pay_info['Td'].'" />';
				$submit.='<input type="hidden" name="sna" value="'.$pay_info['sna'].'" />';
				$submit.='<input type="hidden" name="sdt" value="'.$pay_info['sdt'].'" />';
				$submit.='<input type="hidden" name="email" value="'.$pay_info['email'].'" />';
				$submit.='<input type="hidden" name="note1" value="'.$pay_info['note1'].'" />';
				$submit.='<input type="hidden" name="note2" value="'.$pay_info['note2'].'" />';
				$submit.='<input type="hidden" name="Card_Type" value="'.$pay_info['Card_Type'].'" />';
				$submit.='<input type="hidden" name="ChkValue" value="'.$pay_info['ChkValue'].'" />';
				$submit.='</form>';
				$submit.='</body>';
				echo($submit);

      }

		}else{
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10006,'充值程式異常','MSG');
				echo json_encode($ret);
			}else{
				echo '<script>alert("充值程式異常!");window.location = "/site/deposit/"</script>';
			}
		}

		exit;

	}


	public function twcreditcard_pay_result() {

	  global $tpl, $config, $deposit;
		set_status($this->controller);

    $errcode = $_POST['errcode'];
    $showview = htmlspecialchars(($_POST['showview']) != "") ? htmlspecialchars($_POST['showview']) : TRUE;

    error_log("[twcreditcard_pay_result] post data : ".json_encode($_POST));

    if($errcode=='00'){
      $ret = $this->twcreditcard_pay_success("sync",$showview);	  // sync=同步

			  if($showview == 'TRUE'){
				  if($ret=='0000'){
					  echo '<script>alert("充值成功!");window.location = "/site/member?channelid=1"</script>';
				  }else{
					  echo '<script>alert("充值失敗 : ['.$ret.']");window.location = "/site/member?channelid=1"</script>';
				  }
			  }else{
				  echo $ret;
			  }

    }else{
		  $this->twcreditcard_pay_failed($showview);
		}

		exit;

	}


	public function twcreditcard_pay_success($callType="async", $showview) {   // async=異步

		global $tpl, $config, $deposit;
		set_status($this->controller);

		if($callType=="async"){
			// 讓sync的先跑完 ...
			for($i=0;$i<=100000;++$i){
		        continue;
			}
		}
		error_log("[twcreditcard_pay_success] SendType=".$callType);
		$result_array=array();
		foreach($_POST as $key => $value){
	    error_log("[twcreditcard_pay_success] ${key} = ${value}");
			$result_array[$key]=(string)$value;
		}

		if(!empty($result_array['Td'])){

      // 比對訂單編號是否有前綴sbo如果是移除前綴
			if(preg_match("/SJO/i", $result_array['Td'])){//有前綴
			  $result_array['Td'] = str_replace("SJO","",$result_array['Td']);
			}

		  $get_deposit_history=$deposit->get_deposit_history($result_array['Td']);
			if($get_deposit_history){
				$userid=$get_deposit_history[0]['userid'];
			  $orderid=$get_deposit_history[0]['dhid'];
			  $depositid=$get_deposit_history[0]['depositid'];
				error_log("[twcreditcard_pay_success] userid:".$userid.", orderid:".$orderid." depositid:".$depositid);

        if(!empty($depositid)){
					$get_deposit=$deposit->get_deposit_id($depositid);

          if($get_deposit){
						$amount= round(floatval($get_deposit[0]['amount']));
						error_log("[twcreditcard_pay_success]amount:".round($get_deposit[0]['amount'])."==>".$amount);
						$TransID=$result_array['buysafeno']; //紅陽交易編號
						if($TransID==''){
              $TransID=$result_array['BuySafeNo'];//（相容信用卡交易參數）
            }
						$my_chkvalue=$result_array['web'].
            $config['creditcard']['code'].
            $TransID.
            $amount.
            $result_array['errcode'];
						error_log("[twcreditcard_pay_success] My Ori ChkValue=".$my_chkvalue);
						$my_chkvalue=strtoupper(sha1($my_chkvalue));
						error_log("[twcreditcard_pay_success] My ChkValue=".$my_chkvalue);

				    if($my_chkvalue==$result_array['ChkValue']){
              // 驗證ok, 修改訂單及加入贈送
              $switch=$get_deposit[0]['switch'];
              $status=$get_deposit_history[0]['status'];
              $spointid=$get_deposit_history[0]['spointid'];
              $driid=$get_deposit_history[0]['driid'];
              $nickname=$get_deposit_history[0]['modifiername'];
              error_log("[twcreditcard_pay_result] orderid: ".$orderid."/status: ".$status."/switch: ".$switch);
              if($orderid && $switch == 'N'){
                // 將json形式的資訊轉為Associated array
                $arr_data=json_decode($get_deposit_history[0]['data'],true);
                // 添加資訊到data欄位
                $arr_data['out_trade_no']=$result_array['Td'];
                $arr_data['vendor_no']=$result_array['buysafeno'];
                $arr_data['merchant_id']=$result_array['web'];
                $arr_data['amount']=$result_array['MN'];
                $arr_data['ApproveCode']=$result_array['ApproveCode'];
                $arr_data['Card_NO']=$result_array['Card_NO'];
                $arr_data['Card_Type']=$result_array['Card_Type'];
                $arr_data['SendType']=$result_array['SendType'];
                $arr_data['errcode']=$result_array['errcode'];

                if($callType=="sync"){
                  $arr_data['modifierid']=$userid;
                  $arr_data['modifiername']=$nickname;
                  $arr_data['modifiertype']="User";
                }else if($callType=="async"){
                  $arr_data['modifierid']="0";
                  $arr_data['modifiername']="twcreditcard_pay_success";
                  $arr_data['modifiertype']="System";
                }

                $deposit->set_deposit_history($arr_data, $get_deposit_history[0], $config['creditcard']['paymenttype']);

                if($result_array['errcode']=='00'){  // 不管異步同步, 回傳errorcode='00' 才表示成功
                	$deposit->set_deposit($depositid);
                	$deposit->set_spoint($spointid);
                	$get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
                	error_log("[c/deposit/twcreditcard_pay_success] driid : ".$driid);
                	if(!empty($get_deposit_rule_item[0]['driid'])){
                		$get_scode_promote = $deposit->get_scode_promote_rt($driid);
                		if(!empty($get_scode_promote)){
                			foreach($get_scode_promote  as $sk => $sv){
                				if(!empty($sv['productid']) && $sv['productid'] > 0){
                					for($i = 0; $i < $sv['num']; $i++){
                						 $deposit->add_oscode($userid, $sv);
                					}
                				}
                			}
                		}
                		$ret="0000";
                		//首次儲值送推薦人2塊
                		$this->add_deposit_to_user_src($userid);
                	}
                	// 開立電子發票
                	$Invoice = $this->createInvoice($depositid, $userid);
                	$ret="0000";
                }
              }else{
                error_log("Duplicated Update, do nothing !!");
                $ret="0000";
              }

            }else{
				      $ret="Data Consistency Check Error !!";
					  }
			    }

				}else{
				  $ret="Empty deposit id of ".$orderid." !!";
				}

			}else{
			  $ret="Cannot find the data of ".$orderid." !!";
			}

		}else{
		  $ret="Empty order id !!";
		}

    error_log($ret);

		if($callType=="async"){
		  echo "0000";
		  exit;
		}else{
		  // if $callType="sync"
		  return $ret;
		}

	}


  public function twcreditcard_pay_failed($showview) {

    $errmsg=urldecode($_POST['errmsg']);

    if($ShowView == 'TRUE'){
      echo '<script>alert("充值失敗['.$errmsg.']");</script>';
      return;
    }else{
      //紅陽app異步回傳
      echo "0000";
      return;
    }

  }


  public function gama_pay_result() {

    global $tpl, $config, $deposit;
    set_status($this->controller);

    foreach($_POST as $key => $value){
      $result_array[$key]=$value;
    }

    if($result_array['ResultCode']=='0000'){
      $this->gama_pay_success($result_array);
    }else{
      $this->gama_pay_failed($result_array);
    }

    exit;

  }


  /*
   回傳例
   {
   "ResultCode":"0000","ResultMessage":"成功",
   "TransactionID":"20180808100084723873",
   "MerchantOrderID":"7940",
   "GamaPayAccount":"167b34b2-f300-4038-9819-631d9d2dac4b",
   "MerchantID":"2052",
   "SubMerchantID":"0",
   "MerchantAccount":"0916587443",
   "TransAmount":2.0,
   "CurrencyCode":"TWD",
   "TransType":10,
   "TransFinallyAmount":2.0,
   "TransFee":0.0,
   "ReceiverLoginID":"saja",
   "ReceiverName":"殺價王",
   "PerformanceBondDateTime":null,
   "LoveCode":null,"CarrierID":null,"StatusCode":100,
   "TransDate":"2018-08-08T16:36:33",
   "CreatedDate":"2018-08-08T16:36:33",
   "MAC":"aOMHwIsZBHDy9v+ReCiSvu5sy1W8Ro3LP1RgDTPHYvM="
   }
  */
  public function gama_pay_success($arrRet) {

    global $tpl, $config, $deposit;
    set_status($this->controller);

    error_log("[c/deposit/gama_pay_success] response : ".json_encode($arrRet));

	  $ret=array();
    $ret['retCode']=$arrRet['ResultCode'];
    $ret['retMsg']=$arrRet['ResultMessage'];
    // 非成功狀態
    if($arrRet['ResultCode']!='0000'){
      error_log("[c/deposit/gama_pay_success] : ".json_encode($ret));
      echo json_encode($ret);
      return;
    }

		if(!empty($arrRet['MerchantOrderID']) && !empty($arrRet['TransAmount'])){

      $dhid=$arrRet['MerchantOrderID'];
      $TransAmount=$arrRet['TransAmount'];
      $get_deposit_history=$deposit->get_deposit_history($dhid);

      if($get_deposit_history){
        $userid=$get_deposit_history[0]['userid'];
        $orderid=$get_deposit_history[0]['dhid'];
        $depositid=$get_deposit_history[0]['depositid'];
				error_log("[c/deposit/gama_pay_success] userid:".$userid.", orderid:".$orderid.", depositid:".$depositid);
				if(!empty($depositid)){
					$get_deposit=$deposit->get_deposit_id($depositid);
					if($get_deposit){
						error_log("[c/deposit/gama_pay_success] amount : ".round($get_deposit[0]['amount'])."<==> TransAmount :".round($TransAmount));
					    if(round($get_deposit[0]['amount'])==round($TransAmount) ){
                // 驗證ok, 修改訂單及加入贈送
                $switch=$get_deposit[0]['switch'];
                $status=$get_deposit_history[0]['status'];
                $spointid=$get_deposit_history[0]['spointid'];
                $driid=$get_deposit_history[0]['driid'];
                $nickname=$get_deposit_history[0]['modifiername'];
                error_log("[c/deposit/gama_pay_success] orderid: ".$orderid."/status: ".$status."/switch: ".$switch);
                if ($orderid && $switch == 'N'){
                  $arr_cond=array();
                  $arr_cond['dhid']=$dhid;
                  $arr_cond['switch']="Y";

                  $arr_upd=array();
                  $arr_upd['data']=json_encode($_POST);
                  $arr_upd['status']='deposit';
                  $arr_upd['modifierid']=$userid;
                  $arr_upd['modifiertype']="User";

                  $updateH = $deposit->update_deposit_history($arr_cond, $arr_upd);

								  if($updateH==1){
									  // 儲值生效
                    $deposit->set_deposit($depositid);
                    // 殺幣生效
                    $deposit->set_spoint($spointid);

                    $get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
                    error_log("[c/deposit/gama_pay_success] driid : ".$driid);

                    if(!empty($get_deposit_rule_item[0]['driid'])){
  										$get_scode_promote = $deposit->get_scode_promote_rt($driid);
  										if (!empty($get_scode_promote)){
  											foreach ($get_scode_promote  as $sk => $sv){
  												if (!empty($sv['productid']) && $sv['productid'] > 0){
  													for ($i = 0; $i < $sv['num']; $i++){
  														 $deposit->add_oscode($userid, $sv);
  													}
  												}
  											}
  										}
  										// 首次儲值送推薦人2塊
  										$this->add_deposit_to_user_src($userid);
  										// 開立電子發票
  										$this->createInvoice($depositid, $userid);
                                            $ret['retCode']=1;
  										$ret['retMsg']='OK';
									  }
								  }

							  }else{
								  $ret['retCode']=-2;
                  $ret['retMsg']="Duplicated Update, do nothing !!";
							  }

						  }else{
					      $ret['retCode']=-3;
                $ret['retMsg']="Data Consistency Check Error !!";
						  }
			      }

				  }else{
				    $ret['retCode']=-4;
            $ret['retMsg']="Empty deposit id of ".$orderid." !!";
				  }

			 }else{
			   $ret['retCode']=-5;
         $ret['retMsg']="Cannot find the data of ".$orderid." !!";
			 }

		}else{
		  $ret['retCode']=-5;
      $ret['retMsg']="Empty order id !!";
		}

		error_log("[c/deposit/gama_pay_success] ret : ".json_encode($ret));
		echo json_encode($ret);
		return;

  }


  public function gama_pay_failed($arrRet) {

    error_log("[c/deposit/gama_pay_failed] response : ".json_encode($arrRet));
    $ret = array("retCode"=>-99, "retMsg"=>$arrRet['ResultMsg']);
    error_log("[c/deposit/gama_pay_failed] ret : ".json_encode($ret));
    echo json_encode($ret);
    return;

  }

  public function is_house_promote_effective($productid) {

    global $config, $deposit;
    $ret=false;

    // 是否為編號2854產品
    error_log("[is_house_promote_effective] productid : ".$productid);
    if($productid!='2854') {
      $ret=false;
      return $ret;
    }

    // 人數是否已滿
    $cnt=$deposit->countPassphrase($productid,'','Y');
	  if($cnt){
      error_log("[is_house_promote_effective] now total :".$cnt['now_total']);
      if($cnt['now_total']<33333) {
        $ret=true;
      }else if($cnt['now_total']>=33333){
        $ret=false;
      }
    }else{
      // query error
      error_log("[is_house_promote_effective] empty passphrase :".$productid);
      $ret=false;
	  }
    error_log("[is_house_promote_effective] Effective :".$ret);
    return $ret;

  }


  public function register_house_promote($productid,$userid) {

    global $config, $deposit;

    $cnt=$deposit->countPassphrase($productid,$userid,'');

    if($cnt){
      $user_now_total=$cnt['now_total'];
      error_log("[register_house_promote] now total :".$cnt['now_total']);
    }
    // 是否有啟動
    if($user_now_total>0){
      error_log("[register_house_promote] update passphrase");
      $deposit->updatePassphrase($productid,$userid,'Y');
    }else{
      error_log("[register_house_promote] create passphrase");
      $deposit->createPassphrase($productid,$userid,'Y','','');
    }
    return;

  }


	// usage http://www.shajiawang.com/site/deposit/add_oscode_to_user_test?uid=&pid=&num=&spid=&behav=
  public function add_oscode_to_user_test() {

    global $config, $deposit;

    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
  		$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
  		$ip = $temp_ip[0];
  	}else{
  		$ip = $_SERVER['REMOTE_ADDR'];
  	}

  	if(strpos($ip,'61.219.220.')==false){
  	  die('You have no permission to add !!');
  	  exit;
  	}

  	for($i=0;$i<$_GET['num'];++$i){
      $deposit->add_oscode2($_GET['uid'], $_GET['pid'], $_GET['spid'],  $_GET['behav']);
  	}

  	error_log("add ".$_GET['num']." oscodes to userid : ".$_GET['uid'].", spid:".$_GET['spid'].",behav:".$_GET['behav']);
  	die("add ".$_GET['num']." oscodes to userid : ".$_GET['uid'].", spid:".$_GET['spid'].",behav:".$_GET['behav']);

  }

	// 充值成功時送推薦人及TA殺價券 (只一次)
	public function add_oscode_to_user_src($userid, $productid, $num) {

    global $config, $deposit;

  	$passphrase=$deposit->getPassphraseInfo($productid,$userid,'Y');

  	if($passphrase!=false){

  		// TA 充值滿500,再送6張給TA
  		if($passphrase['userid']>0){
  			$oscode_gived_to_ta=$deposit->get_oscode('160',$passphrase['userid'], 2854);
  			error_log("[c/deposit]oscode_gived_to_ta: ".$oscode_gived_to_ta['cnt']);
  			if($oscode_gived_to_ta['cnt']==0){
  				for ($i = 0; $i < 6; $i++){
  					 $deposit->add_oscode2($passphrase['userid'], $productid, '160', 'b');
  				}
  			}
  		}

  		// TA 充值滿500, 另外送3張給推薦人
	    if($passphrase['user_src']>0){
	      $oscode_gived_to_src=$deposit->get_oscode('161',$passphrase['user_src'],2854);
		    error_log("[c/deposit]oscode_gived_to_src: ".$oscode_gived_to_src['cnt']);
		    if($oscode_gived_to_src['cnt']==0){
		      for($i = 0; $i < 3; $i++){
				    $deposit->add_oscode2($passphrase['user_src'], $productid, '161', 'c');
			    }
		    }
	    }

  	}

  	return ;

	}

	/*
	 *	儲值規則項目清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$drid		int			儲值規則編號
	 *	$driid		int			儲值規則項目編號
	 */
	public function getRuleItemList() {

		global $config, $tpl, $rule, $deposit;

		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid	 = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid	 = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);

		//設定 Action 相關參數
		set_status($this->controller);

		//取得儲值清單資料
		$rule_list = $rule->get_Rule_List($drid);

		for ($i=0;$i<count($rule_list['record']);$i++){

			//判斷儲值規則資料是否存在
			if(!empty($rule_list['record'][$i]['drid'])){
				// 20160607 暫時跳過銀聯
				if($rule_list['record'][$i]['drid']=='4'){
				  unset($rule_list['record'][$i]);
					continue;
				}

				//增加圖示的絕對路徑
				$rule_list['record'][$i]['logo'] = IMG_URL.APP_DIR."/images/site/deposit/".$rule_list['record'][$i]['logo'];
				$rule_list['record'][$i]['banner'] = IMG_URL.APP_DIR."/images/site/deposit/".$rule_list['record'][$i]['banner'];

				//取得儲值規則項目資料
				$rule_item_list = $rule->get_Rule_Item($rule_list['record'][$i]['drid'], $driid);

				foreach ($rule_item_list['record'] as $rk => $rv){
					// 取得項目贈送資料
					$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);

					if(!empty($get_scode_promote)){
						foreach ($get_scode_promote as $sk => $sv){
						  $rule_item_list['record'][$rk]['freegoods'][$sk]['sprid'].= $sv['sprid'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['spid'].= $sv['spid'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['amount'].= $sv['amount'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['seq'].= $sv['seq'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['name'].= $sv['name'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['num'].= $sv['num'];
						}
					}else{
						$rule_item_list['record'][$rk]['freegoods'] = array();
					}
				}

				//判斷儲值規則項目是否存在,加入到儲值規則清單
				$rule_list['record'][$i]['item'] = $rule_item_list['record'];

      }

		}

		//判斷是否為網頁介面
		if($json != 'Y') {
			$tpl->assign('row_list', $rule_list);
		}else{
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "JSON";
			$data['retObj']['data'] = $rule_list['record'];
			echo json_encode($data);
		}

	}


	public function promote_evt() {

    set_status($this->controller);
    $client=$_POST;

    $tpl->assign('client', $client);
    $tpl->set_title('');
    $tpl->render("deposit","promite_evt",true);

	}


	public function confirm_app($drid, $driid, $data, $kind) {

    global $tpl, $config, $deposit, $product, $rule;

		//設定 Action 相關參數
		set_status($this->controller);

		$dhid = 0;
		error_log("[c/deposit/confirm_app] data : ".json_encode($data));

		//新增deposit_history資訊
		$currenty=$config['currency'];
		if($drid=='6' || $drid=='8') {
		  $currency="NTD";
		} else {
		  $currency="RMB";
		}

		//取得儲值規則項目資料
		$rule_item_list = $rule->get_Rule_Item('', $_POST['driid']);
 		$get_deposit['amount'] = floatval($rule_item_list['record'][0]['amount']);
		$get_deposit['spoint'] = $rule_item_list['record'][0]['spoint'];

		if ($kind == "weixin"){
			$get_deposit['amount'] = floatval($data['total_fee']);
			$get_deposit['spoint'] = $data['total_fee'];
		}

		$userid = empty($_POST['auth_id']) ? $data['userid'] : htmlspecialchars($_POST['auth_id']);

		$depositid = $deposit->add_deposit($userid, $get_deposit['amount'], $currency);
		$spointid = $deposit->add_spoint($userid, $get_deposit['spoint']);
		$dhid = $deposit->add_deposit_history($userid, $driid, $depositid, $spointid);

		return $dhid;
	}

	// 小程式用 獲取prepay_id
	public function getWxUnifiedOrder() {

    global $config;

    error_log("[c/deposit/getWxUnifiedOrder] input data : ".json_encode($_REQUEST));
    $userid=$_REQUEST['userid'];
    $productid=$_REQUEST['productid'];
    $body=$_REQUEST['body'];
    $total_fee=$_REQUEST['total_fee']*100;
    $total=$_REQUEST['total_fee'];
    $openid=$_REQUEST['openid'];
    $kind="weixin";
    $trade_type='JSAPI';
    $notify_url=BASE_URL.APP_DIR."/deposit/weixinpay_app";

    // 取得訂單號
    $out_trade_no = $this->confirm_app(5,0,$_REQUEST,$kind);

    $ret="";
    $tools = new JsApiPay();
    try {
      $input = new WxPayUnifiedOrder();
      $input->SetBody($body);
      $input->SetOut_trade_no($out_trade_no);
      $input->SetTotal_fee($total_fee);
      $input->SetNotify_url($notify_url);
      $input->SetTrade_type($trade_type);
      $input->SetOpenid($openid);

      $order = WxPayApi::unifiedOrder($input);
      error_log("[c/deposit/getWxUnifiedOrder] order data : ".json_encode($order));

      $ret = $tools->GetJsApiParameters($order);
    }catch (Exception $e){
      error_log($e->getMessage());
    }finally{
      error_log("[c/deposit/getWxUnifiedOrder] data for wxpay : ".$ret);
      echo $ret;
    }

	}


	// 取得充值贈品List及dhid
	public function getDepositConfirmData() {

    global $config, $deposit, $rule;

		login_required();

		$drid = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);

		// 取得dhid
		$dhid = $this->confirm_app($drid,$driid);
		error_log("[c/deposit/getDepositConfirmData] Get dhid : ".$dhid);

		if(!$dhid){
		  echo json_encode(getRetJSONArray(-2,'系統忙碌中,請稍候再試!!','MSG'));
      exit;
		}

		$code = 1;
		$msg = "OK";

		//充值方式為微信則傳值至微信
		if ($drid == 5){
			//取得儲值規則項目資料
			$rule_item_list = $rule->get_Rule_Item('', $driid);
			//隨機產生變數字串
			$str=$this->getWXNonceStr(32);
			//組合xml格式內容
			$notify_url = BASE_URL.APP_DIR."/deposit/weixinpay_app";

			$out_trade_no = $dhid;
			$total_fee = $rule_item_list['record'][0]['amount']*100;
			if($_SESSION['auth_id']=='116')
			  $total_fee = 1;
			$body = $rule_item_list['record'][0]['description'];

			$stringA = "appid=wxa8ae20850962b5e0&body=".$body."&mch_id=1340460501&nonce_str=".$str."&notify_url=".$notify_url."&out_trade_no=".$out_trade_no."&spbill_create_ip=121.40.19.35&total_fee=".$total_fee."&trade_type=APP";
			$stringSignTemp = $stringA."&key=8d806a7a258bab43991bdf637568a7e1";
			error_log("[c/deposit/getDepositConfirmData] String For Uniformorder : ".$stringSignTemp);
			$sign = strtoupper(MD5($stringSignTemp));

			$xml = '<?xml version="1.0" encoding="UTF-8"?>
    					<xml>
    						<appid>wxa8ae20850962b5e0</appid>
    						<body>'.$body.'</body>
    						<mch_id>1340460501</mch_id>
    						<nonce_str>'.$str.'</nonce_str>
    						<notify_url>'.$notify_url.'</notify_url>
    						<out_trade_no>'.$out_trade_no.'</out_trade_no>
    						<spbill_create_ip>121.40.19.35</spbill_create_ip>
    						<total_fee>'.$total_fee.'</total_fee>
    						<trade_type>APP</trade_type>
    						<sign>'.$sign.'</sign>
    					</xml>';

      //XML送出路徑
			$url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';

			$post_data = array(
                  				"xml" => $xml,
                  			);

      //設定送出方式-POST
			$stream_options = array(
                      				'http' => array (
                                    						'method' => "POST",
                                    						'content' => $xml,
                                    						'timeout' => 5,
                                    						'header' => "Content-Type: text/xml; charset=utf-8"
                                    				  )
			);

			$context  = stream_context_create($stream_options);
			//送出XML內容並取回結果
			$response = file_get_contents($url, null, $context);
			//讀取xml內容
			$xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);

			//加入dhid至物件中
			$xml->dhid = $dhid;

      // Test
			$xml->nonce_str =$this->getWXNonceStr(32);
			$trspay = $this->app_payment($xml->nonce_str, $xml->prepay_id);

			if(!empty($trspay)){
				$xml->sign = $trspay['sign'];
				$xml->timestamp = $trspay['timestamp'];
			}

			//依回傳結果設定回傳至APP的訊息
			if ($xml->return_code == 'SUCCESS'){
				if($xml->result_code == 'SUCCESS'){
					$code = 1;
					$msg = "OK";
				}else{
					$code = $config['weixinpay']['errorcode'][$xml->err_code];
					$msg = $xml->err_code_des;
				}
			}else{
				$code = -3;
				$msg = $xml->return_msg;
			}

			//轉換xml格式
			$xml = json_decode(json_encode($xml));

		}

		$ret=getRetJSONArray($code, $msg,'JSON');
		$ret['retObj']=array();
		//判斷是否為weixin支付
		if ($drid == 5){
			$ret['retObj'] = $xml;
		}else{
			$ret['retObj']['dhid']=$dhid;
		}
		error_log("[c/deposit/getDepositConfirmData] return : ".json_encode($ret));
		echo json_encode($ret);
		exit;

	}


	// 取得支付寶支付(appbsl)
	public function getAliPayOrderForWebApp() {

    global $config, $deposit, $rule;
    error_log("[getAliPayOrderForWebApp] POST IN : ".json_encode($_POST));
    error_log("[getAliPayOrderForWebApp] GET IN : ".json_encode($_GET));
    login_required();

		include_once(LIB_DIR."/alipay/sdk/AopSdk.php");

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
    $drid 		= empty($_POST['drid'])?$_GET['drid']:$_POST['drid'];
    $driid 		= empty($_POST['drid'])?$_GET['driid']:$_POST['driid'];
    $bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
    $bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
    $type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
    $total		= empty($_POST['total'])?$_GET['total']:$_POST['total'];
    $autobid 	= empty($_POST['autobid'])?$_GET['autobid']:$_POST['autobid'];
    $productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];
    $userid		= empty($_SESSION['auth_id'])?$_REQUEST['userid']:$_SESSION['auth_id'];
    $chkStr     = $_REQUEST['chkStr'];

		//賣家支付寶帳戶
		$seller_email = $_POST['WIDseller_email'];

		//商戶訂單號
		$out_trade_no = $_POST['WIDout_trade_no'];

		//訂單名稱
		$subject = $_POST['WIDsubject'];

		//付款金額
		$total_fee = $_POST['WIDtotal_fee'];

		$passback_params=array();
		$passback_params['orderid']=$out_trade_no;
		$passback_params['bida']=$bida;
		$passback_params['bidb']=$bidb;
		$passback_params['type']=$type;   // pay_type (single or lazy ...)
		$passback_params['autobid']=$autobid;
		$passback_params['productid']=$productid;
		$passback_params['userid']=$userid;
		$passback_params['pay_amount']=$total_fee;
		$passback_params['chkStr']=$chkStr;

		$biz_content=array(true);
		$biz_content['body']='殺價王-下標'.$total_fee.' 元';
		$biz_content['out_trade_no']=$out_trade_no;
		$biz_content['subject']=$subject;
		$biz_content['seller_email']=$seller_email;
		$biz_content['total_amount']=$total_fee;
		$biz_content['timeout_express']='5m';
		$biz_content['product_code']='QUICK_MSECURITY_PAY';
		$biz_content['passback_params']=urlencode(json_encode($passback_params));


		$aop = new AopClient;
		// Production
		$aop->appId="2016050401363208";
    $aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
		$aop->rsaPrivateKeyFilePath = LIB_DIR.'/alipay/gen_Alipay_private_key_2048.pem';
    $aop->signType= "RSA2";

		$request = new AlipayTradeAppPayRequest();
	  $request->setBizContent(json_encode($biz_content));
		$request->setNotifyUrl(BASE_URL.APP_DIR."/lib/alipay/webapp_notify_url.php");
		$response = $aop->sdkExecute($request);
		if(!$response) {
		  $ret=getRetJSONArray('0', 'FAILED','MSG');
		  echo trim(json_encode($ret));
		  exit;
		}
		$ret=getRetJSONArray('1', 'OK','JSON');
		$ret['retObj']=$response;
		error_log("[getAliPayOrderForWebApp] return : ".json_encode($ret));
		echo trim(json_encode($ret));
		exit;

	}


	// 取得微信支付預訂單資料(appbsl)
	public function getWxUnifiedOrderForWebApp() {

    global $config, $deposit, $rule;

		error_log("[getWxUnifiedOrderForWebApp] POST IN : ".json_encode($_POST));
		error_log("[getWxUnifiedOrderForWebApp] GET IN : ".json_encode($_GET));

    login_required();
		include_once(LIB_DIR."/weixinpay/WxPayHelper/WxPayHelper.php");

    $prodid=$_REQUEST['productid'];
    $bida=$_REQUEST['bida'];
    $bidb=$_REQUEST['bidb'];
    $autobid=$_REQUEST['autobid'];
    $drid = $_REQUEST['drid'];
    $driid = $_REQUEST['driid'];
    $amount=0;
    $total_fee=0;
    $body ='';
    $dhid=$_REQUEST['orderid'];
    $notify_url = BASE_URL.APP_DIR."/deposit/weixinpay_app";

    if($autobid=='Y'){
      $notify_url=BASE_URL.APP_DIR.'/product/sajatopay';
    }

		$code = 1;
		$msg = "OK";

		// 有傳orderid , 則用傳入的orderid, 否則另外生一個dhid當作orderid
		$dhid=$_REQUEST['orderid'];
		if(empty($dhid)){
		  $dhid = $this->confirm_app($drid,$driid);
		  error_log("[c/deposit/getWxUnifiedOrderForWebApp] Get orderid : ".$dhid);
		}
		if(!$dhid){
		  echo json_encode(getRetJSONArray(-2,'系統忙碌中,請稍候再試!!','MSG'));
      exit;
		}

		//有傳$driid  則依$driid取得儲值規則項目中的金額以及敘述
		if(!empty($driid)){
		   $rule_item_list = $rule->get_Rule_Item('', $driid);
		   if($rule_item_list['record'][0]){
		      $amount=$rule_item_list['record'][0]['amount'];
		      $total_fee = $rule_item_list['record'][0]['amount']*100;
		      $body = ($rule_item_list['record'][0]['description']);
		   }
		}else{
		  $amount=$_REQUEST['amount'];
		  $total_fee=$_REQUEST['amount']*100;
      $body = ($_REQUEST['body']);
		}
		if(empty($body)){
		  $body=('殺價王-下標 : '.$amount.' 元');
		}
	  if($_SESSION['auth_id']==116 || $_SESSION['auth_id']==28 || $_SESSION['auth_id']==1777 || $_SESSION['auth_id']==9){
		  $amount=0.01;
			$total_fee=1;
		}
		$secretid=md5($dhid.$amount);
		error_log("[getWxUnifiedOrderForWebApp] orderid:${dhid} ,amount: ${amount} ,secretid:${secretid}");

		// 使用統一支付介面，獲取prepay_id
		$ufo = new UnifiedOrder(array("appid"=>"wxa8ae20850962b5e0", "mchid"=>"1340460501","key"=>"8d806a7a258bab43991bdf637568a7e1"));
		$ufo->setParameter('notify_url',$notify_url);
		$ufo->setParameter('out_trade_no',$dhid);
		$ufo->setParameter('total_fee',$total_fee);
		$ufo->setParameter('body',$body);
		$ufo->setParameter('trade_type','APP');

		$response=$ufo->getResult();
		error_log("[lib/weixinpay/webapp_call] response:".json_encode($response));

		if($response['return_code']=='SUCCESS'){
		  if($response['result_code']=='SUCCESS'){
        $code = 1;
        $msg = "OK";
        $response['nonce_str'] =$this->getWXNonceStr(32);
        $response['amount'] = "".$amount;
        $response['orderid'] = "".$dhid;
        $response['secretid'] = "".$secretid;
			  $trspay = $this->app_payment($response['nonce_str'],$response['prepay_id']);
			  if(!empty($trspay)){
				  $response['sign'] = $trspay['sign'];
				  $response['timestamp'] = $trspay['timestamp'];
			  }
		  }else{
			  $code = $config['weixinpay']['errorcode'][$response['err_code']];
			  $msg = $response['err_code_des'];
			  $response['prepay_id']='';
			  $response['timestamp']='';
		  }
		}
		$ret=getRetJSONArray($code, $msg,'JSON');
		$ret['retObj']=$response;
		error_log("[c/deposit/getWxUnifiedOrderForWebApp] return : ".json_encode($ret));
		echo trim(json_encode($ret));

    exit;

	}


	function app_payment($nonce_str, $prepay_id, $appid=''){

		$str = $nonce_str;
		$package = "Sign=WXPay";
		$prepayid = $prepay_id;
		$timestamp = "".time();
		$payment = "";

		if(empty($appid))
		   $appid="wxa8ae20850962b5e0";

		if ((!empty($nonce_str)) && (!empty($prepay_id))){
			$stringA = "appid=".$appid.
			           "&noncestr=".$str.
			           "&package=".$package.
    					   "&partnerid=1340460501".
    					   "&prepayid=".$prepayid.
    					   "&timestamp=".$timestamp;
			$stringSignTemp = $stringA."&key=8d806a7a258bab43991bdf637568a7e1";
			error_log("[c/deposit/app_payment] String For WXPay: ".$stringSignTemp);
			$sign = strtoupper(MD5($stringSignTemp));

			$payment['sign'] = $sign;
			$payment['timestamp'] = $timestamp;
		}

		return $payment;

	}


	public function getWXNonceStr($len=32) {

    $nstr = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";

    for( $i = 0; $i < $len; $i++ ){
			$nstr.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
		}

		return $nstr;

	}


	public function AlipayPaySuccessForBid() {

    global $tpl, $config, $deposit, $rule;

    error_log("[AlipayPaySuccessForBid] POST : ".json_encode($_POST));

    $ret=getRetJSONArray('1', 'OK','JSON');
    error_log("[AlipayPaySuccessForBid] return : ".json_encode($ret));
    echo trim(json_encode($ret));

    exit;

	}


	/*
	 *	微信儲值APP
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$driid		int			儲值規則項目編號
	 */
	public function weixinpay_app() {
		global $tpl, $config, $deposit, $rule;

		$input = file_get_contents("php://input"); //接收POST數據
		$xml = simplexml_load_string($input, 'SimpleXMLElement', LIBXML_NOCDATA); //提取POST數據為simplexml對象

		$orderid = (string)$xml->out_trade_no;
		$amount = (string)$xml->total_fee;
		$appid = (string)$xml->appid;
		$mchid = (string)$xml->mch_id;

		if(($appid != 'wxbd3bb123afa75e56') && ($appid != 'wxa8ae20850962b5e0') && ($appid!='wx054ee14ea7e1d53b')){
			$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
            				<note>
            				<return_code><![CDATA[FAIL]</return_code>
            				<return_msg><![CDATA[微信應用ID異常。]</return_msg>
            				</note>";
			print_r($myXMLData);
			exit;
		}

		if(($mchid != '10011676') && ($mchid != '1340460501')){
			$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
                    <note>
                    <return_code><![CDATA[FAIL]</return_code>
                    <return_msg><![CDATA[微信商戶號異常。]</return_msg>
                    </note>";
			print_r($myXMLData);
			exit;
		}

		$secretid = md5($orderid.$amount);
		error_log("[c/deposit/weixinpay_app] orderid : ".$orderid.", amount : ".$amount.", secretid : ".$secretid);

    //設定 Action 相關參數
		set_status($this->controller);

		if($orderid){

			$weixinpay_array['out_trade_no'] = $orderid;
			$weixinpay_array['amount'] = $amount;

			$get_deposit_history = $deposit->get_deposit_history($weixinpay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

	    error_log("[c/deposit/weixinpay_app] : ".($amount)."<==>".($get_deposit_id[0]['amount']*100));
			if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){
			  // 交易金額比對
				if($amount!=($get_deposit_id[0]['amount']*100)){
				  error_log("[c/deposit/weixinpay_app] : ".floatval($amount)."<==>".(floatval($get_deposit_id[0]['amount'])));
				  $ret['status'] = 3;
				  $myXMLData ="<?xml version='1.0' encoding='UTF-8'?>
					<note>
					<return_code><![CDATA[FAIL]</return_code>
					<return_msg><![CDATA[微信支付-訂單數據異常。]</return_msg>
					</note>";
				  error_log("微信支付-訂單數據異常。");
				  echo $ret['status'];
		      exit;
				}
				$deposit->set_deposit_history($weixinpay_array, $get_deposit_history[0], "weixinpay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);

				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				$userid=$get_deposit_history[0]['userid'];

				if(!empty($get_scode_promote)){

					foreach($get_scode_promote  as $sk => $sv){

						if($sv['productid'] > 0 && !empty($sv['productid'])){
							if($this->is_house_promote_effective($sv['productid'])){
								// 判定是否符合萬人殺房資格, 符合者送殺價券
								$this->register_house_promote($sv['productid'],$userid);
								for($i = 0; $i < $sv['num']; $i++){
								  $deposit->add_oscode($userid, $sv);
								}
								// 推薦者送殺價券
								$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
								// 不是房子(2854), 就直接送殺價券
								for($i = 0; $i < $sv['num']; $i++){
								   $deposit->add_oscode($userid, $sv);
								}
							}
						}
					}
				}

				//首次儲值送推薦人 2 點殺價幣
				$this->add_deposit_to_user_src($userid);

				//回傳:
				// $ret=getRetJSONArray(1,'OK','LIST');
				// $ret['status'] = 200;
				// $ret['retObj']['dhid'] = $dhid;
				// $ret['retObj']['userid'] = $userid;
				// $ret['retObj']['amount'] = str_replace(",","",(number_format($rule_item_list[0]['amount'], 2)));
				// $ret['retObj']['spoint'] = str_replace(",","",(number_format($rule_item_list[0]['spoint'], 2)));
				// $ret['retObj']['secretid'] = md5($dhid."|".$userid."|".$rule_item_list[0]['spoint']);
				$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
            					<note>
            					<return_code><![CDATA[SUCCESS]</return_code>
            					<return_msg><![CDATA[OK]</return_msg>
            					</note>";
				error_log("微信支付交易-成功。");
			}else if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y'){

				$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
            					<note>
            					<return_code><![CDATA[FAIL]</return_code>
            					<return_msg><![CDATA[微信支付重複充值。]</return_msg>
            					</note>";
				error_log("微信支付重複充值。");
			}else{

				$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
            					<note>
            					<return_code><![CDATA[FAIL]</return_code>
            					<return_msg><![CDATA[微信支付充值失敗1。]</return_msg>
            					</note>";
				error_log("微信支付充值失敗1。");
			}
		}else{

			$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
            				<note>
            				<return_code><![CDATA[FAIL]</return_code>
            				<return_msg><![CDATA[微信支付充值失敗2。]</return_msg>
            				</note>";
			error_log("微信支付充值失敗2。");
		}

		print_r($myXMLData);
		exit;

	}


	/*
	 *	銀聯儲值APP 同步通知 form bankcomm
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$driid		int			儲值規則項目編號
	 */
	public function bankcomm_app() {

    global $tpl, $config, $deposit;

		set_status($this->controller);

    $notifyMsg = $_POST['notifyMsg'];
    error_log("bankcomm_html from bankcomm : ".$notifyMsg);
    $arr = preg_split("/\|{1,}/",$notifyMsg);

		if(is_array($arr)){
      $merId=$arr[0];
      $orderid=$arr[1];
      $amount=$arr[2];
      $curType=$arr[3];          // ex: CNY
      $platformBatchNo=$arr[4];  // Usually leave blank
      $merPayBatchNo=$arr[5];    // Optional, assign by merchant
      $orderDate=$arr[6];        // ex: 20141015
      $orderTime=$arr[7];        // ex: 095124
      $bankSerialNo=$arr[8];     // ex: C7D367EC
      $tranResultCode=$arr[9];   // ex: 1 [success]
      $feeSum=$arr[10];          // Total fee
      $cardType=$arr[11];
      $bankMono=$arr[12];
      $errMsg=$arr[13];
      $remoteAddr=$arr[14];
      $refererAddr=$arr[15];
      $merComment=base64_decode($arr[16]);
      $signMsg=$arr[17];
	  }
		error_log("merComment is : ".$merComment);
		error_log("transResultCode : ".$tranResultCode);
		if($tranResultCode=='1'){
	    $bankcomm_array['out_trade_no'] = $orderid;
	    $bankcomm_array['trade_no'] =$bankSerialNo;
	    $bankcomm_array['amount'] = $amount;
	    $bankcomm_array['result'] = $tranResultCode;
	    $bankcomm_array['orderDate'] = $orderDate;
	    $bankcomm_array['orderTime'] = $orderTime;
	    $bankcomm_array['orderMono'] = $merComment;

	    $get_deposit_history = $deposit->get_deposit_history($bankcomm_array['out_trade_no']);
	    $get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

		  if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'N'){

			  $userid=$get_deposit_history[0]['userid'];

        $deposit->set_deposit_history($bankcomm_array, $get_deposit_history[0], "bankcomm");
        $deposit->set_deposit($get_deposit_history[0]['depositid']);
        $deposit->set_spoint($get_deposit_history[0]['spointid']);
        $get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
        $get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				if(!empty($get_scode_promote)){
				  foreach($get_scode_promote  as $sk => $sv){
						if($sv['productid'] > 0 && !empty($sv['productid'])){
			        if($this->is_house_promote_effective($sv['productid'])){
							  // 判定是否符合萬人殺房資格, 符合者送殺價券
							  $this->register_house_promote($sv['productid'],$userid);
							  for($i = 0; $i < $sv['num']; $i++){
								  $deposit->add_oscode($userid, $sv);
							  }
							  // 推薦者送殺價券
							  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
					    }else{
							  // 不是房子(2854), 就直接送殺價券
							  for($i = 0; $i < $sv['num']; $i++){
								   $deposit->add_oscode($userid, $sv);
							  }
					    }
					  }
				  }
				}

				error_log("銀聯交易成功。");
				$ret=getRetJSONArray(1,'OK','JSON');
				$ret['retObj']=array();
				$ret['retObj']['dhid'] = $get_deposit_history[0]['dhid'];
				$ret['retObj']['userid'] = $userid;
				$ret['retObj']['amount'] = str_replace(",","",(number_format($get_deposit_rule_item['table']['record'][0]['amount'], 2)));
				$ret['retObj']['spoint'] = str_replace(",","",(number_format($get_deposit_rule_item['table']['record'][0]['spoint'], 2)));
				$ret['retObj']['secretid'] = md5($get_deposit_history['table']['record'][0]['dhid']."|".$userid."|".$get_deposit_rule_item['table']['record'][0]['spoint']);
				echo json_encode($ret);
				exit;
      }else if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'Y'){
			  error_log("[deposit] Order id : ".$orderid." has been deposited OK , nothing to do !!");
				$ret=getRetJSONArray(-1,'error','MSG');
				echo json_encode($ret);
				exit;
			}
		}else{
		  error_log("銀聯交易失敗。");
			$ret=getRetJSONArray(-1,'error','MSG');
			echo json_encode($ret);
			exit;
		}

    exit;

	}


	/*
	 *	充值成功時送推薦人2點殺價幣
	 *	$userid		int		會員編號
	 */
	public function add_deposit_to_user_src($userid) {

		global $config, $deposit, $user;

		$deposit_conut = $deposit->get_Deposit_Count($userid);

		error_log("[c/deposit/add_deposit_to_user_src] deposit_conut : ".json_encode($deposit_conut));

		//判斷是否第一次充值
		if($deposit_conut['num'] == 1){

			//取得推薦人的會員編號
			$gived_to_src = $user->getAffiliateUser($userid);

			error_log("[c/deposit/add_deposit_to_user_src] gived_to_src : ".json_encode($gived_to_src));

			if(!empty($gived_to_src['intro_by'])){

				//贈送2塊給推薦人
				$gived_deposit = $deposit->add_Gived_Deposit($gived_to_src['intro_by'], 2, 'TWD');

				//取得推薦人的微信openid
				$openid = $user->getUserWeixinOpenID($gived_to_src['intro_by']);

				//判斷推薦人的微信openid是否存在
				if (!empty($openid['uid'])){
					$msg = '恭喜您 有新合格人數產生，已贈送您殺價幣 2 點!!';
					$ret = sendWeixinMsg($openid['uid'], $msg, 'text');
				}
				error_log("[c/deposit/add_deposit_to_user_src] gived_deposit : ".json_encode($gived_deposit));

			}

		}

		return ;

	}


	public function alipayconfirm() {

		global $tpl, $config, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$drid=$_GET['drid'];
		$driid=$_GET['driid'];
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		$tpl->assign('drid',$drid);
		$tpl->assign('driid', $driid);
		$tpl->set_title('');

		$tpl->render("deposit","confirm3",true,'','Y');
	}


	/*
	 *	支付下標手續費選擇
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$bida			int			起始價格
	 *	$bidb			int			結束價格
	 *	$type			varchar		下標類型
	 *	$productid	 	int			商品編號
	 */
	public function paychoose() {

		global $tpl, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
		$bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
		$type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];

		error_log("[c/deposit/paychoose] post data : ".json_encode($_POST));

		if( (!empty($type)) && ($bida > 0) && (!empty($productid)) ){

			//取得殺價商品資料
			$get_product_info = $product->get_info($productid);
			error_log("[c/deposit/paychoose] product:".json_encode($get_product_info));

			$total = getBidTotalFee($get_product_info,$type,$bida,$bida,$bidb);
			//支付資料
			$paydata = array(
                				'productid'		=> $productid,										//商品編號
                				'bida'			=> $bida,											//起始價格
                				'bidb' 			=> $bidb, 											//結束價格
                				'type'			=> $type,											//下標類型
                				'total'			=> $total,											//總金額
                			);

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('paydata', $paydata);
				$tpl->set_title('');
				$tpl->render("deposit","paychoose",true);
			}else{
				$ret = getRetJSONArray(1,"OK",'LIST');
				$ret['retObj']['paydata'] = (!empty($paydata)) ? $paydata : array();
				echo json_encode((object)$ret);
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y') {
				echo"<script>alert('請確認下標資料 !!');history.go(-1);</script>";
			}else{
				$ret = getRetJSONArray(-1,"ERROR",'MSG');
				echo json_encode($ret);
			}

		}

	}


	/*
	 *	當下支付下標手續費
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$bida			int			起始價格
	 *	$bidb			int			結束價格
	 *	$type			varchar		下標類型
	 *	$userid			int			會員編號
	 *	$productid	 	int			商品編號
	 */
	public function onlinepay() {

		global $tpl, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$count	 	= empty($_POST['count'])?$_GET['count']:$_POST['count'];
		$bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
		$bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
		$type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];
		$userid 	= empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		error_log("[c/deposit/onlinepay] post data : ".json_encode($_POST));

		if( (!empty($type)) && ($bida > 0) && (!empty($productid)) ){

			//取得殺價商品資料
			$get_product_info = $product->get_info($productid);
			error_log("[c/deposit/onlinepay] product:".json_encode($get_product_info));

			$get_drid_list = $deposit->row_drid_list();
			error_log("[c/deposit/onlinepay] get_drid_list:".json_encode($get_drid_list));

			$fee = $get_product_info['saja_fee'];

			$total=getBidTotalFee($get_product_info,$type,$bida,$bida,$bidb);
			if($_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='1777' || $_SESSION['auth_id']=='2525') {
			   $total=0.01;
			}
			//支付資料
			$paydata = array(
                				'userid'		=> $userid,											//使用者編號
                				'productid'		=> $productid,										//商品編號
                				'bida'			=> $bida,											//起始價格
                				'bidb' 			=> $bidb, 											//結束價格
                				'type'			=> $type,											//下標類型
                				'count'			=> $count,											//下標數
                				'fee'			=> $fee,											//手續費
                				'total'			=> $total,											//總金額
                			);

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('paydata', $paydata);
				$tpl->assign('get_product_info', $get_product_info);
				$tpl->assign('get_drid_list', $get_drid_list);
				$tpl->set_title('');
				$tpl->render("deposit","onlinepay",true);
			}else{
				$ret = getRetJSONArray(1,"OK",'LIST');
				$ret['retObj']['paydata'] = (!empty($paydata)) ? $paydata : array();
				$ret['retObj']['product'] = (!empty($get_product_info)) ? $get_product_info : array();
				$ret['retObj']['paykind'] = (!empty($get_drid_list)) ? $get_drid_list : array();
				echo json_encode((object)$ret);
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y'){
				echo"<script>alert('請確認下標資料 !!');history.go(-1);</script>";
			}else{
				$ret = getRetJSONArray(-1,"ERROR",'MSG');
				echo json_encode($ret);
			}

		}

	}


	/*
	 *	當下支付下標手續費確認
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$bida			int			起始價格
	 *	$bidb			int			結束價格
	 *	$type			varchar		下標類型
	 *	$autobid		varchar		自動下標
	 *	$userid			int			會員編號
	 *	$productid	 	int			商品編號
	 */
	public function onlinpay_confirm() {

		global $tpl, $config, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

    error_log("[onlinpay_confirm] POST IN : ".json_encode($_POST));
		error_log("[onlinpay_confirm] GET IN : ".json_encode($_GET));

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$drid 		= empty($_POST['drid'])?$_GET['drid']:$_POST['drid'];
		$bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
		$bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
		$type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$total		= empty($_POST['total'])?$_GET['total']:$_POST['total'];
		$autobid 	= empty($_POST['autobid'])?$_GET['autobid']:$_POST['autobid'];
		$productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];
		$userid		= empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];


		if($type == "single"){
			$bidb = 0;
		}

		//支付類型
		$deposit_rule = $deposit->deposit_rule($drid);
		error_log("[c/deposit/onlinepayconfirm] deposit_rule : ".json_encode($deposit_rule));

		//支付資料
		$paydata = array(
                			'userid'		=> $userid,											//使用者編號
                			'productid'		=> $productid,										//商品編號
                			'bida'			=> $bida,											//起始價格
                			'bidb' 			=> $bidb, 											//結束價格
                			'type'			=> $type,											//下標類型
                			'total'			=> $total,											//總金額
                			'autobid'		=> $autobid,										//自動下標
                		);

		error_log("[c/deposit/onlinepayconfirm] paydata : ".json_encode($paydata));

		//取得儲值點數
		$get_deposit['drid'] = $drid;
		$get_deposit['driid'] = 0;
		$get_deposit['name'] = '下標手續費支付';
		$get_deposit['amount'] = $total;
		$get_deposit['spoint'] = $total;

		if($_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='1777' || $_SESSION['auth_id']=='2525' ) {
		  $get_deposit['amount'] = 0.01;
		}

		if($drid =='7' || $drid =='9') {
			$get_deposit['drid']=$drid;
		}else if ($drid != '7' && $drid != '9'){
			//新增deposit_history資訊
			$currenty = $config['currency'];
			if($drid=='6' || $drid=='8'){
			  $currency="NTD";
			  $get_deposit['amount'] = $total * 5;
			}else{
			  $currency="RMB";
			}

			//寫入下標儲值資料
			$depositid = $deposit->add_deposit($userid, $get_deposit['amount'],$currency);
			$spointid = $deposit->add_spoint($userid, $get_deposit['spoint']);
			$dhid = $deposit->add_deposit_history($userid, 0, $depositid, $spointid);

			if($drid == 4){
				$banklist=array();
				$banklist['BOCOM']="交通銀行";
				$banklist['CMB'] = "招商銀行";
				$banklist['CCB'] = "建設銀行";
				$banklist['SPDB'] = "浦發銀行";
				$banklist['GDB'] = "廣發銀行";
				$banklist['PSDB'] = "郵政儲蓄銀行";
				$banklist['CIB'] = "興業銀行";
				$banklist['HXB'] = "華夏銀行";
				$banklist['PAB'] = "平安銀行";
				$banklist['BOS'] = "上海銀行";
				$banklist['SRCB'] = "上海農商銀行";
				$banklist['BCCB'] = "北京銀行";
				$banklist['BRCB'] = "北京農商銀行";
				$banklist['CEB'] = "光大銀行";
				$banklist['ICBC'] = "工商銀行(建議使用PC流覽)";
				$banklist['BOCSH'] = "中國銀行(建議使用PC流覽)";
				$banklist['ABC'] = "農業銀行(建議使用PC流覽)";
				$banklist['MBC'] = "民生銀行(建議使用PC流覽)";
				$banklist['CNCB'] = "中信銀行(建議使用PC流覽)";
				$banklist['OTHERS'] = "其它" ;

				$tpl->assign('bank_list',$banklist);
			}

			$get_deposit['ordernumber'] = $dhid;

		}

		//判斷是否為網頁介面
		if($json != 'Y'){
			$chkStr=$dhid."|".$get_deposit['amount'];

			$cs = new convertString();
			$enc_chkStr = $cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
			error_log("[c/deposit/onlinepayconfirm] ori chkStr : ".$chkStr);
			error_log("[c/deposit/onlinepayconfirm] encode chkStr ".$enc_chkStr);
			$tpl->assign('paydata', $paydata);
			$tpl->assign('autobid', $autobid);
			$tpl->assign('bida', $bida);
			$tpl->assign('bidb', $bidb);
			$tpl->assign('bidtype', $type);
			$tpl->assign('productid', $productid);
			$tpl->assign('userid', $userid);
			$tpl->assign('total', $total);
			$tpl->assign('deposit_rule', $deposit_rule);
			$tpl->assign('chkStr',$enc_chkStr);
			$tpl->assign('get_deposit', $get_deposit);
			$tpl->set_title('');
			$tpl->render("deposit","onlinepayconfirm",true);
		}else{
			$ret = getRetJSONArray(1,"OK",'LIST');
			$ret['retObj']['paydata'] = (!empty($paydata)) ? $paydata : array();
			$ret['retObj']['chkStr'] = (!empty($chkStr)) ? $chkStr : array();
			$ret['retObj']['deposit'] = (!empty($get_deposit)) ? $get_deposit : array();
			$ret['retObj']['paykind'] = (!empty($deposit_rule)) ? $deposit_rule : array();
			echo json_encode((object)$ret);
		}

	}


	/*
	 *	取得充值贈品List及dhid
	 */
	public function getDepositConfirm() {

    global $config, $deposit, $rule;

		login_required();

		$drid = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);

		$dhid = $this->confirm_app($drid,$driid);
		error_log("[c/deposit/getDepositConfirmData] Get dhid : ".$dhid);
		if(!$dhid){
		  echo json_encode(getRetJSONArray(-2,'系統忙碌中,請稍候再試!!','MSG'));
      exit;
		}
		$code = 1;
		$msg = "OK";

		//充值方式為微信則傳值至微信
		if($drid == 5){
			//取得儲值規則項目資料
			$rule_item_list = $rule->get_Rule_Item('', $driid);
			//隨機產生變數字串
			$str=$this->getWXNonceStr(32);
			//組合xml格式內容
			$notify_url = "http://www.saja.com/site/deposit/weixinpay_app";

			$out_trade_no = $dhid;
			$total_fee = $rule_item_list['record'][0]['amount']*100;
			if($_SESSION['auth_id']=='116')
			  $total_fee = 1;
			$body = $rule_item_list['record'][0]['description'];

			$stringA = "appid=wxa8ae20850962b5e0&body=".$body."&mch_id=1340460501&nonce_str=".$str."&notify_url=".$notify_url."&out_trade_no=".$out_trade_no."&spbill_create_ip=121.40.19.35&total_fee=".$total_fee."&trade_type=APP";
			$stringSignTemp = $stringA."&key=8d806a7a258bab43991bdf637568a7e1";
			error_log("[c/deposit/getDepositConfirmData] String For Uniformorder : ".$stringSignTemp);
			$sign = strtoupper(MD5($stringSignTemp));

			$xml = '<?xml version="1.0" encoding="UTF-8"?>
    					<xml>
    						<appid>wxa8ae20850962b5e0</appid>
    						<body>'.$body.'</body>
    						<mch_id>1340460501</mch_id>
    						<nonce_str>'.$str.'</nonce_str>
    						<notify_url>'.$notify_url.'</notify_url>
    						<out_trade_no>'.$out_trade_no.'</out_trade_no>
    						<spbill_create_ip>121.40.19.35</spbill_create_ip>
    						<total_fee>'.$total_fee.'</total_fee>
    						<trade_type>APP</trade_type>
    						<sign>'.$sign.'</sign>
    					</xml>';
			//XML送出路徑
			$url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';

			$post_data = array(
                  				"xml" => $xml,
                  			);
			//設定送出方式-POST
			$stream_options = array(
                      				'http' => array (
                                    						'method' => "POST",
                                    						'content' => $xml,
                                    						'timeout' => 5,
                                    						'header' => "Content-Type: text/xml; charset=utf-8"
                                    				  )
			);

			$context  = stream_context_create($stream_options);
			//送出XML內容並取回結果
			$response = file_get_contents($url, null, $context);
			//讀取xml內容
			$xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);

			//加入dhid至物件中
			$xml->dhid = $dhid;
			// Test
			$xml->nonce_str =$this->getWXNonceStr(32);
			$trspay = $this->app_payment($xml->nonce_str, $xml->prepay_id);

			if(!empty($trspay)){
				$xml->sign = $trspay['sign'];
				$xml->timestamp = $trspay['timestamp'];
			}

			//依回傳結果設定回傳至APP的訊息
			if ($xml->return_code == 'SUCCESS'){
				if($xml->result_code == 'SUCCESS'){
					$code = 1;
					$msg = "OK";
				}else{
					$code = $config['weixinpay']['errorcode'][$xml->err_code];
					$msg = $xml->err_code_des;
				}
			}else{
				$code = -3;
				$msg = $xml->return_msg;
			}

			//轉換xml格式
			$xml = json_decode(json_encode($xml));

		}

		$ret=getRetJSONArray($code, $msg,'JSON');
		$ret['retObj']=array();
		//判斷是否為weixin支付
		if($drid == 5){
			$ret['retObj'] = $xml;
		}else{
			$ret['retObj']['dhid']=$dhid;
		}
		error_log("[c/deposit/getDepositConfirmData] return : ".json_encode($ret));
		echo json_encode($ret);

    exit;

	}


	/*
	 * 創建發票單（用途 : 兜成發票編號並回寫充值資料訂單）
	 * @param int $depositid    未開發票的單號（儲值帳目id）
	 */
	public function createInvoice($depositid='', $userid='') {

		global $config, $deposit, $invoice;

    if(empty($userid))
      $userid = empty($_SESSION['auth_id'])?htmlspecialchars($_REQUEST['userid']):$_SESSION['auth_id'];

    if(empty($depositid))
      $depositid = htmlspecialchars($_REQUEST['depositid']);

    error_log("[c/deposit/createInvoice] userid:${userid}, depositid:${depositid} ");

    //判斷是否有發票編號
		$depositData= $deposit->getDepositHistoryForInvoice($depositid, $userid);

	  if(!$depositData){
      //資料不存在
      $ret = array(
                    "retCode" 	=> -1,
                    "retMsg" 	=> "資料不存在"
      );
	  }else{

      if($depositData['switch'] != 'Y'){
        // 充值是否完成
        $ret = array(
                      "retCode" 	=> -2,
                      "retMsg" 	=> "此筆充值尚未完成"
        );
      }else if ($depositData['invoiceno'] != ''){ //發票號碼是否為空
        //invoiceno不為空值
        $ret = array(
                      "retCode" 	=> -3,
                      "retMsg" 	=> "發票號碼已存在"
        );
      }else{
        //當儲存資料有值時，取modifyt欄位
        $year = date('Y', strtotime($depositData['insertt']));
        $date = date('m', strtotime($depositData['insertt']));

        //取得發票字軌資料
        $invoice_rail = $deposit->getInvoiceRail($year,$date);

        if(!$invoice_rail){
          //發票字軌資料不存在
          $ret = array(
                        "retCode" 	=> -3,
                        "retMsg" 	=> "發票字軌資料不存在"
          );
        }else{

          // 發票號碼
          // 20181219 : 配號數前面有0的, 轉成integer時會被忽略掉  所以算完之後要再補零到8位
          $invoiceno = $invoice_rail['rail_prefix'] . str_pad(($invoice_rail['seq_start'] + $invoice_rail['used']),8,'0',STR_PAD_LEFT);

          /* 發票資料
          $arrInvoice = array();
          $arrInvoice['invoiceno']=$invoiceno;
          $arrInvoice['invoice_datetime']=date('Y-m-d H:i:s');
          $arrInvoice['buyer_name']=$userid;
          $arrInvoice['userid']=$userid;
          $arrInvoice['random_number']=$random_code ;
          $arrInvoice['total_amt'] =round($depositData['amount']);
          $arrInvoice['sales_amt']=round($depositData['amount']/1.05);
          $arrInvoice['tax_amt'] = round($arrInvoice['total_amt']-$arrInvoice['sales_amt']);
          $arrInvoice['railid']=$invoice_rail['railid'];
          $arrInvoice['donate_mark']="0";
          $arrInvoice['nopban']="";
          $arrInvoice['upload']="N";
          $arrInvoice['carrier_type']="";
          $arrInvoice['carrier_id1']="";
          $arrInvoice['carrier_id2']="";
          $arrInvoice['utype']="S";
          $arrInvoice['is_winprize']="N";
          */

          $arrInvoice = $invoice->genDefaultInvoiceArr($userid, round($depositData['amount']), '','S');
		      $arrInvoice['invoiceno']=$invoiceno;
          $arrInvoice['userid']=$userid;
          $arrInvoice['railid']=$invoice_rail['railid'];
          $arrInvoice['upload']="N";

          $siid=$invoice->addInvoiceData($arrInvoice);

      		if($siid>0){
      		  $arrInvoiceItem = array();
            $arrInvoiceItem['siid']=$siid;
            $arrInvoiceItem['seq_no']=1;
            $arrInvoiceItem['unitprice']= round($depositData['amount']);
            $arrInvoiceItem['quantity']=1;
            $arrInvoiceItem['amount']=$arrInvoiceItem['unitprice']*$arrInvoiceItem['quantity'];
            $arrInvoiceItem['description']="系統使用費";
            $sipid = $invoice->addInvoiceProdItem($arrInvoiceItem);
      		}


      		error_log("[c/deposit/createInvoice] invoiceno:".$invoiceno."--siid:".$siid."--sipid:".$sipid);
      		//將 發票號碼 和 隨機號碼 回寫到表 saja_deposit

          $update_invoice = $deposit->updateDepositHistoryInvoice($userid,$depositid,$invoiceno);

          if($update_invoice != 1){
            //寫入表saja_deposit失敗
            $ret = array(
                          "retCode" 	=> -4,
                          "retMsg" 	=> "寫入失敗"
            );
          }else{
            //將used+1回寫到saja_invoice_rail中
            $update_invoice_rail = $deposit->updateInvoiceRail($invoice_rail['railid'],$invoice_rail['rail_prefix'],'');

            if($update_invoice_rail != 1){
              //used累加 1失敗
              $ret = array(
                            "retCode" 	=> -4,
                            "retMsg" 	=> "累加1失敗"
              );
            }else{
              //取表invoice_rail中的資料
              $invoice_rail = $deposit->getInvoiceRail($year,$date);

              //如果加1之後檢查used是否等於total, 是則表示號碼用完,將switch改成"E"之後不再使用
              if($invoice_rail['used'] == $invoice_rail['total']){
                  $update_invoice_rail_switch = $deposit->updateInvoiceRail($invoice_rail['railid'],$invoice_rail['rail_prefix'],1);
                  $ret = array(
                                "retCode" 	=> 1,
                                "retMsg" 	=> "OK"
                  );
              }else{
                $ret = array(
                              "retCode" 	=> 1,
                              "retMsg" 	=> "OK"
                );
              }
            }
          }
        }
      }
	  }

    $ret['retObj']['depositid'] = (!is_null($depositid)) ? $depositid : "";
    $ret['retObj']['invoiceno'] = (!is_null($invoiceno)) ? $invoiceno : "";
    $ret['retObj']['random_code'] = (!is_null($random_code)) ? $random_code : "";
    error_log("[c/deposit/createInvoice]".json_encode($ret));
    return json_encode($ret);

	}


  /*
  *	接收遠端request, 呼叫內部function加殺價幣
  */
  function addSajaPoints() {
    global $config, $deposit, $invoice;

    $amount = htmlspecialchars($_REQUEST['amount']);
    $userid = htmlspecialchars($_REQUEST['userid']);
    $tx_data = htmlspecialchars($_REQUEST['tx_data']);
    $drid = htmlspecialchars($_REQUEST['drid']);

    // 如果不開發票  可以塞任何值給這個參數(inv)
    $inv = htmlspecialchars($_REQUEST['inv']);

    // 簽名(非_ARR_ALLOW_IP允許IP傳入  須驗簽)
    $sign = htmlspecialchars($_REQUEST['sign']);

    $ip=GetIP();
    error_log("[c/deposit/addSajaPoints] request from : ".$ip);
    error_log("[c/deposit/addSajaPoints] data : ".$userid."|".$amount."|".$drid."|".$tx_data);
    error_log("[c/deposit/addSajaPoints] inv : ".$inv);

    $arrData = array("retCode"=>0,"retMsg"=>"NO_DATA", "userid"=>"", "amount"=>"");

    if(empty($userid)){
      $arrData['retCode']=-1;
      $arrData['retMsg']="UNKNOWN_USERID";
      echo  json_encode($arrData);
      exit;
    }
    $arrData["userid"]=$userid;

    if($amount<=0){
      $arrData['retCode']=-2;
      $arrData['retMsg']="ERROR_DEPOSIT_AMT";
      echo  json_encode($arrData);
      exit;
    }
    $arrData["amount"]=$amount;

    // sign 格式 md5($userid."|".$amount."|".$timestamp) ;(到秒)
    // 如果非允許ip  就要驗簽
    if(!in_array($ip,$this->_ARR_ALLOW_IP)){
       $arrSignParams=[$userid,$amount,$this->_SALT];
       error_log("[c/deposit] addSajaPoints : ".json_encode($arrSignParams));
       if(chkSign($arrSignParams, $sign)==false){
          $arrData['retCode']=-3;
          $arrData['retMsg']="SIGN_CHECK_FAILED";
          echo  json_encode($arrData);
          exit;
       }
    }

    $driid=99;   // 暫定為非限定金額儲值
    $depositid = $deposit->add_deposit($arrData['userid'], $arrData['amount'],'','Y');
    if($depositid && $depositid>0){
      if($drid>0){
        $arrCond=array("drid"=>$drid, "amount"=>$amount);
        // 該充值金額是否符合儲值項目規定金額
        $dritem = $deposit->getDepositRuleItems($arrCond," driid desc ");
        if(is_array($dritem) && $dritem[0]['driid']>0){
          $driid=$dritem[0]['driid'];
          // 該充值方式是否有送殺價券活動
          $ScodePromote = $deposit->getScodePromoteData(array("driid"=>$driid));
          if($ScodePromote){
            // 送殺價券
            $num = $ScodePromote[0]['num'];
            $productid=$ScodePromote[0]['productid'];
            $spid = $ScodePromote[0]['spid'];
            if($num>0 && $productid>0 && $spid>0){
              error_log("[c/deposit/addSajaPoints] spid ".$spid." : add ".$num." oscodes of productid : ".$productid );
              for($i=1;$i<=$num;++$i){
                 $deposit->add_oscode2($userid,$productid,$spid,"user_deposit");
              }
            }
          }else{
            // 儲值額度對應不到活動
            error_log("[c/deposit/addSajaPoint] No oscode promote for driid : ".$driid);
          }
        }else{
          // 儲值額度非限定金額
          error_log("[c/deposit/addSajaPoint] No deposit rule item for amount : ".$amount);
          $driid=99;
        }
      }else{
        // 未知的儲值方式
        $arrData['retCode']=-3;
        $arrData['retMsg']="UNKNOWN_DRID";
      }

      $arrData['depositid']=$depositid;
      $arrData['driid']=$driid;
      $arrData['status']='deposit';
      $arrData['data']=$tx_data;
      $arrData['inserttype']='User';
      $arrData['insertid']=$userid;
      $arrData['modifierid']=$userid;
      $arrData['modifiertype']='User';
      $arrData['switch']='Y';

      if($driid==99){
        // 非限定金額儲值, 不加點也不開發票
        $arrData['spointid']=0;
        $dhid =$deposit->addDepositHistory($arrData);
        $arrData["retCode"]=1;
        $arrData["retMsg"]="DEPOSIT_AMT_NOT_MATCH";
      }else if($driid!=99){
        // 必須扣除5%的發票稅
        // 故殺幣額度要從金額 / 1.05
        $spointid = $deposit->add_spoint($arrData['userid'], round($arrData['amount']/1.05), 'Y');
        if($spointid && $spointid>0){
          $arrData['spointid']= $spointid;
          $dhid =$deposit->addDepositHistory($arrData);
          if($dhid && $dhid>0){
            if(empty($inv)){
              $Invoice = $this->createInvoice($depositid, $userid);
              if($Invoice){
                 $arrInvoice = json_decode($Invoice,TRUE);
                 $arrData["invoiceno"]=$arrInvoice['retObj']['invoiceno'];
              }
            }
            $arrData["retCode"]=1;
            $arrData["retMsg"]="OK";
          }
        }
      }
    }else{
      // 新增儲值紀錄失敗
      $arrData['retCode']=-10;
      $arrData['retMsg']="DEPOSIT_FAILED";
    }

    error_log("[c/deposit/addSajaPoints] : ".json_encode($arrData));
    echo json_encode($arrData);
    return;

  }

	/*
	 *	充值說明頁
	 */
	public function payfaq() {

		global $tpl, $config, $deposit, $user, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid=empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
    $driid=empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
    $userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		//會員卡包資料
		$user_extrainfo_category = $user->check_user_extrainfo_category();

		if(is_array($user_extrainfo_category)){
			foreach ($user_extrainfo_category as $key => $value){
				if($value['uecid'] == 8){
					if($json!='Y'){
						$user_extrainfo[$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}else{
						$user_extrainfo['a'.$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}
				}
			}
		}

		//充值項目ID
		$row_list = $deposit->row_list($drid);
		$tpl->assign('row_list', $row_list);

		$deposit_rule = $deposit->deposit_rule($drid);
		$tpl->assign('deposit_rule', $deposit_rule);

		foreach($row_list as $rk => $rv){

			if((int)$rv['driid']==(int)($driid)){
				//取得儲值點數
				$get_deposit['drid'] = $drid;
				$get_deposit['driid'] = $rv['driid'];
				$get_deposit['name'] = $rv['name'];
				$get_deposit['amount'] = floatval($rv['amount']);
				$get_deposit['spoint'] = $rv['spoint'];

				break;
			}else{
			    continue;
			}

		} //endforeach;

		if($drid=='7' || $drid=='9'){ //Hinet & 阿拉訂 : 點數兌換時才生訂單

			$get_deposit['drid']=$drid;

		}else if ($drid != '7' && $drid !='9'){

			//新增deposit_history資訊
			$currenty=$config['currency'];
			if($drid=='6' || $drid=='8' || $drid=='10') {
			  $currency="NTD";
			}else{
        $currency="RMB";
      }

			$get_scode_promote = $deposit->get_scode_promote_rt($driid);
			if (!empty($get_scode_promote)){
				$i = 1;
				$spmemo = '';
				$spmemototal = 0;
				$spmemoapp = '';

				foreach($get_scode_promote as $sk => $sv){
					$get_product_info = $product->get_info($sv['productid']);
					$spmemoapp[$sk]['name'] = "送 殺價券".$get_product_info['name'];

					$spmemo.='<li class="d-flex align-items-center">
                    <div class="de_title"><p>送 殺價券</p><p>( '.$get_product_info['name'].' )</p></div><div class="de_point">';
          if(!empty($sv['num'])){
					  $spmemo.=$sv['num'].' 張';
						$spmemoapp[$sk]['num'] = $sv['num'].' 張';
						$spmemototal = ($spmemototal+($sv['num']*$get_product_info['saja_fee']));
					}

					if(!empty($get_product_info['productid'])){
  					$spmemo.='</div></li>';
					}
				  $i++;
				}
			}else{
				$spmemo .= "<li>無贈送任何東西</li>";
				$spmemoapp[0]['name'] = "無贈送任何東西";
				$spmemoapp[0]['num'] = " 0 張";
			}
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_deposit;
			$ret['retObj']['spmemo'] = $spmemoapp;
			$ret['retObj']['user_extrainfo'] =  $user_extrainfo['a8']['field1name'];
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('drid',$drid);
			$tpl->assign('driid', $driid);
			$tpl->assign('get_deposit', $get_deposit);
			$tpl->assign('get_scode_promote', $get_scode_promote);
			$tpl->assign('spmemo', $spmemo);
			$tpl->assign('spmemototal', $spmemototal);
			$tpl->assign('user_extrainfo', $user_extrainfo);
			$tpl->set_title('');
			$tpl->render("deposit","payfaq",true);
		}

	}

}
