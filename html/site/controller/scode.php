<?php
/*
 * Scode Controller S碼紀錄
 */

class Scode {

  public $controller = array();
	public $params = array();
  public $id;
	public $userid = '';

	public function __construct() {
		$this->userid = (empty($_SESSION['auth_id']) ) ? $_POST['auth_id'] : $_SESSION['auth_id'];
	}


	/*
   * Default home page.
   */
	public function home() {

    global $tpl, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$status=(empty($_POST['status'])) ? htmlspecialchars($_GET['status']) : htmlspecialchars($_POST['status']);

		//S碼 現有可用總數
		$scode_info['now'] = $scodeModel->get_scode($this->userid);

		//S碼 收取總數
		$scode_info['accept'] = $scodeModel->accept_sum($this->userid);

		//S碼 收取且過期總數
		$scode_info['expired'] = $scodeModel->expired_sum($this->userid);

		//S碼 已用總數
		$tpl->assign('retCode', $status);
		$tpl->assign('scode_info', $scode_info);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $scode_info;
			echo json_encode($ret);
		}else{
			$tpl->assign('header','Y');
			$tpl->assign('footer','N');
			$tpl->set_title('');
			$tpl->render("scode","home",true);
		}

	}


	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

    $table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';

		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;

  }


	/*
	* S碼-收取明細
	*/
	public function accept() {

    global $tpl, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$get_list = $scodeModel->accept_list($this->userid);
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}

		$tpl->assign('page_content', $page_content);
		$tpl->set_title('');

		$tpl->render("scode","accept",true);

	}


	/*
	 * S碼-使用明細
	 */
	public function used() {

    global $tpl, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);

    login_required();

		$get_list = $scodeModel->used_list($this->userid);
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}

		$tpl->assign('page_content', $page_content);
		$tpl->set_title('');

		$tpl->render("scode","used",true);

	}


	/*
     *	S碼相關清單
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$kind				varchar				顯示型態 (get:取得殺價卷, use:使用殺價卷)
	 *	$p					int					分頁編號
     */
	public function scode_list() {

    global $tpl, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = 1;//empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		error_log('[c/scode/scode_list] json : '.$json.', kind : '.$kind.', p : '.$p);
		
		//20200121 數位紅包 AARONFU
		include_once(LIB_DIR ."/user_referral.php");
		$today = date("Y-m-d H:i:s");
		
		//紅包送S碼 $user_ref = new user_referral();
		if($today >= "2020-01-22 12:00:00") {
			//01/31 停用
			//$u_ret = $user_ref->red_scode($this->userid);
		}

		// S碼 現有可用總數
		$scode_info['now'] = $scodeModel->get_scode($this->userid);

		// S碼 收取資料
		$scode_info['accept'] = $scodeModel->scode_list($this->userid, $kind);
		
		if ($kind=='use') {
			$scode_info['accept'] = $scodeModel->use_scode_list($this->userid);
		}

		// S碼 已用總數
		$scode_info['used'] = abs($scodeModel->used_sum($this->userid));

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['getcount'] = empty($scode_info['now']) ? 0 : $scode_info['now'];
			$ret['usecount'] = empty($scode_info['used']) ? 0 : $scode_info['used'];

			// 判斷清單是否存在
			if($scode_info){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $scode_info['accept']['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
					$page['rec_start'] = 0;
					$page['totalcount'] = 0;
					$page['totalpages'] = 1;
					$page['perpage'] = 50;
					$page['page'] = 1;
					$page['item'] = array(array("p" => 1 ));
				}else{
					$ret['retObj']['data'] = $scode_info['accept']['table']['record'];
					$ret['retObj']['page'] = $scode_info['accept']['table']['page'];
				}

			} else {
				$ret['retObj']['data'] = array();

				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('scode_info', $scode_info);
			$tpl->set_title('');
			$tpl->render("scode","scodelist",true);
		}

	}


	/*
     *	發送超級殺價券
	 *	$kind				varchar				發送種類 scode:超級殺價卷)
	 *	$num				int					發送張數
	 *	$userteam			int					發送組別 (0 1 2)
	 *	$pushid				int					推播發送編號 
     */
	public function scode_add($kind,$num,$userteam,$pushid,$userid) {

		global $tpl, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);
		// login_required();

		$kind = empty($kind) ? htmlspecialchars($_GET['kind']):$kind;
		$num = empty($num) ? htmlspecialchars($_GET['num']):$num;
		$userteam = empty($userteam) ? htmlspecialchars($_GET['userteam']):$userteam;
		$pushid = empty($pushid) ? htmlspecialchars($_GET['pushid']):$pushid;
		$userid = empty($userid) ? htmlspecialchars($_GET['userid']):$userid;
		
		error_log('[c/scode/scode_add] kind0 : '.$kind.', num : '.$num.', userteam : '.$userteam.', userid : '.$userid);
		if(!empty($kind) && !empty($num) && !empty($pushid) && !empty($userid)){
			
			error_log('[c/scode/scode_add] kind : '.$kind.', num : '.$num.', userteam : '.$userteam);
			// 發送超級殺價券
			$scode = $scodeModel->add_scode($kind,$num,$userteam,$pushid,$userid);

			error_log('[c/scode/scode_add] scode : '.json_encode($scode));
		}else{
			error_log('[c/scode/scode_add] kind2 : '.$kind.', num : '.$num.', userteam : '.$userteam);
		}
	}


}
