<?php
/*
 *	Invoice Controller 電子發票相關功能
 */

class Invoice {

  public $controller = array();
	public $params = array();
  public $id;

	public function __construct() {

		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
    $this->datetime = date('Y-m-d H:i:s');

  }


	/*
	 *	發票功能清單
	 *	$kind 					varchar					使用功能
	 *	$idate					date					期別 ex:20180708
	 *	$invoiceno				varchar					發票號碼
	 *	$type					varchar					發票更新類型 up:已上傳成功 wait:已取過待上傳
	 */
	public function home() {

		global $config, $tpl;

		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$idate = empty($_POST['idate']) ? htmlspecialchars($_GET['idate']) : htmlspecialchars($_POST['idate']);
		$invoiceno = empty($_POST['invoiceno']) ? htmlspecialchars($_GET['invoiceno']) : htmlspecialchars($_POST['invoiceno']);
		$allowanceno = empty($_POST['allowanceno']) ? htmlspecialchars($_GET['allowanceno']) : htmlspecialchars($_POST['allowanceno']);
		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);

		//設定 Action 相關參數
		set_status($this->controller);

        error_log("[c/invoice/home] kind:${kind} ");
		switch($kind){
			case 'C0401': // C0401 平臺存證開立發票
            			  $this->Invoice($idate);
            		    break;
			case 'C0401_1': // C0401 平臺存證開立發票
            			  $this->Invoice1($invoiceno);
            		    break;
			case 'C0501': // C0501 平臺存證作廢發票
            			  $this->CancelInvoice($invoiceno);
            			  break;
			case 'C0701': // C0701 註銷發票訊息存證
            			  $this->VoidInvoice($invoiceno);
            			  break;
			case 'D0401': // D0401 平臺存證開立折讓證明單
            			  // $this->Allowance($invoiceno);
						  $this->Allowance($allowanceno);
            			  break;
			case 'D0501': // D0501 平臺存證作廢折讓證明單
            			  $this->CancelAllowance($allowanceno);
            			  break;
			case 'E0402': // E0402 空白未使用字軌檔
						  // idate='20180910';
						  $this->BranchTrackBlank($idate);
						  break;
			case 'upload':
						  $this->InvoiceUpResult($invoiceno,$type);
						  break;
			case 'batch_upd_inv':
					  $jstrInvoices = urldecode($_REQUEST['invoicenos']);
					  $this->BatchUpdateInvoicesUploadStatus($jstrInvoices);
					  break;
			case 'batch_cancel_inv':
					  $jstrInvoices = urldecode($_REQUEST['invoicenos']);
					  $this->BatchUpdateInvoicesCancelStatus($jstrInvoices);
					  break;
			case 'batch_upd_alwn':
					  $jstrAllowancenos = urldecode($_REQUEST['allowancenos']);
					  $this->BatchUpdateAllowanceUploadStatus($jstrAllowancenos);
					  break;
			case 'get_prize':  // 撈最新得獎發票資料
			          $invTerm=htmlspecialchars($_REQUEST['invTerm']);
					  $this->genInvoicePrizeData($invTerm);
					  break;
			case 'chk_prize':  // 發票兌獎
			          $invTerm=htmlspecialchars($_REQUEST['invTerm']);
					  $this->chkInvoicePrizeData($invTerm);
					  break;
			default: // C0401 平臺存證開立發票
					  $this->Invoice($date);
			          break;
		}

	}


	/*
	 *	C0401 平臺存證開立發票
         <?xml version="1.0" encoding="UTF-8"?>
					<Invoice xsi:schemaLocation="urn:GEINV:eInvoiceMessage:C0401:3.1 C0401.xsd" xmlns="urn:GEINV:eInvoiceMessage:C0401:3.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
						<Main>
							<InvoiceNumber>'.$InvoiceNumber.'</InvoiceNumber><!-- 發票號碼 -->
							<InvoiceDate>'.$InvoiceDate.'</InvoiceDate><!-- 發票日期 -->
							<InvoiceTime>'.$InvoiceTime.'</InvoiceTime><!-- 發票時間 -->

							<Seller><!-- 賣方 -->
								<Identifier>'.$SellerIdentifier.'</Identifier>
								<Name>'.$SellerName.'</Name>
								<Address>'.$SellerAddress.'</Address>
								<TelephoneNumber>'.$SellerTelephoneNumber.'</TelephoneNumber>
							</Seller>

							<Buyer><!-- 買方 -->
								<Identifier>'.$BuyerIdentifier.'</Identifier>
								<Name>'.$BuyerName.'</Name>
							</Buyer>

							<InvoiceType>'.$InvoiceType.'</InvoiceType><!-- 發票類別 -->
							<DonateMark>'.$DonateMark.'</DonateMark><!-- 捐贈註記 -->
							<CarrierType>'.$CarrierType.'</CarrierType><!-- 載具類別號碼 -->
							<CarrierId1>'.$CarrierId1.'</CarrierId1><!-- 載具顯碼Id -->
							<CarrierId2>'.$CarrierId2.'</CarrierId2><!-- 載具顯碼Id2 -->
							<PrintMark>'.$PrintMark.'</PrintMark><!-- Y/N，PrintMark 為 Y 時載具類別號碼,載具顯碼 ID,載具隱碼 ID 必須為空白，捐贈註記必為 0 -->
							<NPOBAN>'.$NPOBAN.'</NPOBAN><!-- 發票捐贈對象 -->
							<RandomNumber>'.$RandomNumber.'</RandomNumber><!-- 發票防偽隨機碼 -->
						</Main>

						<Details><!-- 明細資料-->
							<ProductItem><!-- 至少一筆 -->
								<Description>'.$Description.'</Description><!-- 品名 -->
								<Quantity>'.$Quantity.'</Quantity><!-- 數量 -->
								<UnitPrice>'.$UnitPrice.'</UnitPrice><!-- 單價 -->
								<Amount>'.$Amount.'</Amount><!-- 金額 -->
								<SequenceNumber>'.$SequenceNumber.'</SequenceNumber><!-- 明細排列序號 -->
							</ProductItem>
						</Details>

						<Amount><!-- 彙總資料 -->
							<SalesAmount>'.$SalesAmount.'</SalesAmount><!-- 銷售額合計(新台幣) -->
							<FreeTaxSalesAmount>'.$FreeTaxSalesAmount.'</FreeTaxSalesAmount><!-- 免銷售額合計(新台幣) -->
							<ZeroTaxSalesAmount>'.$ZeroTaxSalesAmount.'</ZeroTaxSalesAmount><!-- 零銷售額合計(新台幣) -->
							<TaxType>'.$TaxType.'</TaxType><!-- 課稅別 -->
							<TaxRate>'.$TaxRate.'</TaxRate><!-- 稅率 -->
							<TaxAmount>'.$TaxAmount.'</TaxAmount><!-- 營業稅 -->
							<TotalAmount>'.$TotalAmount.'</TotalAmount><!-- 總計 -->
						</Amount>
	*/
	public function Invoice($idate) {

		global $config, $tpl, $deposit, $invoice;

		//設定 Action 相關參數
		set_status($this->controller);

		//取得需上傳發票資料
    $Invoice_list = $invoice->getInvoiceListForUpload($idate);
		$ret = "";
    error_log("[c/invoice/Invoice]:".json_encode($Invoice_list));

	if(count($Invoice_list) > 0){
		$ret = $Invoice_list;
	}else{
		$ret = array();
	}
	echo json_encode($ret);
	exit;

    if(!empty($Invoice_list['invoiceno'])){

			$date = date('Ymd', strtotime(substr($Invoice_list['invoice_datetime'],0 ,10)));

			$InvoiceNumber = $Invoice_list['invoiceno']; 							// 發票號碼
			$InvoiceDate = $date; 													// 發票日期
			$InvoiceTime = substr($Invoice_list['invoice_datetime'],11 ,8); 		// 發票時間

			$SellerIdentifier = $Invoice_list['seller_id'];						// 賣方統編
			$SellerName = $Invoice_list['seller_name']; 						// 賣方名稱
			$SellerAddress = $Invoice_list['seller_addr']; 					// 賣方地址
			$SellerTelephoneNumber = $Invoice_list['seller_telno'];	           // 賣方電話

			$BuyerIdentifier = $Invoice_list['buyer_id']; 						// 買方統編
			$BuyerName = $Invoice_list['buyer_name']; 								// 買方名稱

            $RandomNumber = $Invoice_list['random_number']; 	                // 發票防偽隨機碼
			$InvoiceType = $Invoice_list['invoice_type']; 					    // 發票類別
			$DonateMark = $Invoice_list['donate_mark']; 						// 捐贈註記
			$PrintMark = $Invoice_list['print_mark']; 							// 載具類別號碼
			$NPOBAN = $Invoice_list['npoban'];  								// 發票捐贈對象

			if(!empty($NPOBAN)) {
				$DonateMark='1';
				$PrintMark = 'N';
			}
			/*
			if($DonateMark == 1){
				$NPOBAN = "00968326"; 											// 發票捐贈對象
			}else{
				$NPOBAN = "";
			}
			*/
			/*
          載具類別號碼為 6 碼，編碼規則第 1 碼為行業別(英數)，第 2 碼為載具類別(英數)，後 4 碼為流水號
          共通性載具類別號碼說明如下:
          1. 手機條碼為 3J0002
          2. 自然人憑證條碼為 CQ0001
		  // 20200512 : 新增殺價王會員載具
          3. 殺價王會員載具 EG0544
	         $CarrierType='EG0544';
	         $CarrierId1= 'SJ'.str_pad($Invoice_list['userid'],4,"0",STR_PAD_LEFT);
	         $CarrierId2= str_pad($Invoice_list['userid'],4,"0",STR_PAD_LEFT);
	   */
			$CarrierType='';
			$CarrierId1='';
			$CarrierId2='';

			if (!empty($Invoice_list['phonecode'])){
				$CarrierType='3J0002';
				$CarrierId1=$Invoice_list['phonecode'];
				$CarrierId1=$Invoice_list['phonecode'];
			} elseif (!empty($Invoice_list['npcode'])){
				$CarrierType='CQ0001';
				$CarrierId1=$Invoice_list['npcode'];
				$CarrierId1=$Invoice_list['npcode'];
			} else {
				$CarrierType=$Invoice_list['carrier_type'];
				$CarrierId1=$Invoice_list['carrier_id1'];
				$CarrierId1=$Invoice_list['carrier_id2'];
			}

			$Description = $Invoice_list['description'];						// 品名
			$UnitPrice = $Invoice_list['unitprice']; 							// 單價
			$Quantity = $Invoice_list['quantity']; 								// 數量
			$SequenceNumber = $Invoice_list['seq_no']; 							// 明細排列序號

			$SalesAmount = round($Invoice_list['sales_amt']);									// 銷售額合計(新台幣)
			$FreeTaxSalesAmount = round($Invoice_list['free_tax_sales_amt']); 												// 免銷售額合計(新台幣)
			$ZeroTaxSalesAmount = round($Invoice_list['zero_tax_sales_amt']);                // 零銷售額合計(新台幣)
			/*
          // 課稅別
          1：應稅
          2：零稅率
          3：免稅
          4：應稅(特種稅率)
          9：混合應稅與免稅或零稅率(限訊息 C0401 使用)
      */
      $TaxType = $Invoice_list['tax_type'];
			$TaxRate = $Invoice_list['tax_rate']; 														// 稅率
			$TaxAmount = round($Invoice_list['tax_amt']);								// 營業稅
			$TotalAmount = round($Invoice_list['total_amt']);										// 總計
      $Amount=round($Quantity*$UnitPrice);

			$ret = '<?xml version="1.0" encoding="UTF-8"?>
                      <Invoice xmlns="urn:GEINV:eInvoiceMessage:C0401:3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:GEINV:eInvoiceMessage:C0401:3.2 C0401.xsd">
                          <Main>
                              <InvoiceNumber>'.$InvoiceNumber.'</InvoiceNumber>
                              <InvoiceDate>'.$InvoiceDate.'</InvoiceDate>
                              <InvoiceTime>'.$InvoiceTime.'</InvoiceTime>
                              <Seller>
                                  <Identifier>'.$SellerIdentifier.'</Identifier>
                                  <Name>'.$SellerName.'</Name>
                                  <Address>'.$SellerAddress.'</Address>
                                  <TelephoneNumber>'.$SellerTelephoneNumber.'</TelephoneNumber>
                              </Seller>
                              <Buyer>
                                  <Identifier>'.$BuyerIdentifier.'</Identifier>
                                  <Name>'.$BuyerName.'</Name>
                              </Buyer>
                              <InvoiceType>'.$InvoiceType.'</InvoiceType>
                              <DonateMark>'.$DonateMark.'</DonateMark>
                              <CarrierType>'.$CarrierType.'</CarrierType>
                              <CarrierId1>'.$CarrierId1.'</CarrierId1>
                              <CarrierId2>'.$CarrierId2.'</CarrierId2>
                              <PrintMark>'.$PrintMark.'</PrintMark>
                              <NPOBAN>'.$NPOBAN.'</NPOBAN>
                              <RandomNumber>'.$RandomNumber.'</RandomNumber>
                          </Main>
                          <Details>
                              <ProductItem>
                                  <Description>'.$Description.'</Description>
                                  <Quantity>'.$Quantity.'</Quantity>
                                  <UnitPrice>'.sprintf("%1\$.4f",$UnitPrice).'</UnitPrice>
                                  <Amount>'.$Amount.'</Amount>
                                  <SequenceNumber>'.$SequenceNumber.'</SequenceNumber>
                              </ProductItem>
                          </Details>
                          <Amount>
                              <SalesAmount>'.$SalesAmount.'</SalesAmount>
                              <FreeTaxSalesAmount>'.$FreeTaxSalesAmount.'</FreeTaxSalesAmount>
                              <ZeroTaxSalesAmount>'.$ZeroTaxSalesAmount.'</ZeroTaxSalesAmount>
                              <TaxType>'.$TaxType.'</TaxType>
                              <TaxRate>'.$TaxRate.'</TaxRate>
                              <TaxAmount>'.$TaxAmount.'</TaxAmount>
                              <TotalAmount>'.$TotalAmount.'</TotalAmount>
                          </Amount>
                      </Invoice>';
		}else{
      $ret = "NO_DATA";
		}
      error_log("[c/invoice/Invoice] xml : ".$ret);
      echo $ret;

	}


	// 撈單筆發票資料
	public function Invoice1($invoiceno) {

		global $config, $tpl, $invoice;

		//設定 Action 相關參數
		set_status($this->controller);

		//取得需上傳發票資料
		$InvoiceData = $invoice->getInvoiceDetailByNo($invoiceno);
		$ret = "";
		error_log("[c/invoice/Invoice1]:".json_encode($InvoiceData));


		if(!empty($InvoiceData['invoiceno'])){

			$date = date('Ymd', strtotime(substr($InvoiceData['invoice_datetime'],0 ,10)));

			$InvoiceNumber = $InvoiceData['invoiceno']; 							// 發票號碼
			$InvoiceDate = $date; 													// 發票日期
			$InvoiceTime = substr($InvoiceData['invoice_datetime'],11 ,8); 		// 發票時間

			$SellerIdentifier = $InvoiceData['seller_id'];						// 賣方統編
			$SellerName = $InvoiceData['seller_name']; 						// 賣方名稱
			$SellerAddress = $InvoiceData['seller_addr']; 					// 賣方地址
			$SellerTelephoneNumber = $InvoiceData['seller_telno'];	           // 賣方電話

			$BuyerIdentifier = $InvoiceData['buyer_id']; 						// 買方統編
			$BuyerName = $InvoiceData['buyer_name']; 								// 買方名稱

            $RandomNumber = $InvoiceData['random_number']; 	                // 發票防偽隨機碼
			$InvoiceType = $InvoiceData['invoice_type']; 					    // 發票類別
			$DonateMark = $InvoiceData['donate_mark']; 						// 捐贈註記
			$PrintMark = $InvoiceData['print_mark']; 							// 載具類別號碼
			$NPOBAN = $Invoice_list['npoban'];								// 發票捐贈對象

            if(!empty($NPOBAN)) {
				$DonateMark='1';
				$PrintMark = 'N';
			}
			/*
			if($DonateMark == 1){
				$NPOBAN = "00968326"; 											// 發票捐贈對象
			}else{
				$NPOBAN = "";
			}
			*/
			/*
          載具類別號碼為 6 碼，編碼規則第 1 碼為行業別(英數)，第 2 碼為載具類別(英數)，後 4 碼為流水號
          共通性載具類別號碼說明如下:
          1. 手機條碼為 3J0002
          2. 自然人憑證條碼為 CQ0001
		   3. 殺價王會員載具 EG0544
	         $CarrierType='EG0544';
	         $CarrierId1= 'SJ'.str_pad($Invoice_list['userid'],4,"0",STR_PAD_LEFT);
	         $CarrierId2= str_pad($Invoice_list['userid'],4,"0",STR_PAD_LEFT);
      */
            $CarrierType='';
			$CarrierId1='';
			$CarrierId2='';

			if (!empty($Invoice_list['phonecode'])){
				$CarrierType='3J0002';
				$CarrierId1=$Invoice_list['phonecode'];
				$CarrierId1=$Invoice_list['phonecode'];
			} elseif (!empty($Invoice_list['npcode'])){
				$CarrierType='CQ0001';
				$CarrierId1=$Invoice_list['npcode'];
				$CarrierId1=$Invoice_list['npcode'];
			} else {
				$CarrierType=$Invoice_list['carrier_type'];
				$CarrierId1=$Invoice_list['carrier_id1'];
				$CarrierId1=$Invoice_list['carrier_id2'];
			}

			$Description = $InvoiceData['description'];						// 品名
			$UnitPrice = $InvoiceData['unitprice']; 							// 單價
			$Quantity = $InvoiceData['quantity']; 								// 數量
			$SequenceNumber = $InvoiceData['seq_no']; 							// 明細排列序號

			$SalesAmount = round($InvoiceData['sales_amt']);									// 銷售額合計(新台幣)
			$FreeTaxSalesAmount = round($InvoiceData['free_tax_sales_amt']); 												// 免銷售額合計(新台幣)
			$ZeroTaxSalesAmount = round($InvoiceData['zero_tax_sales_amt']);                // 零銷售額合計(新台幣)
			/*
          // 課稅別
          1：應稅
          2：零稅率
          3：免稅
          4：應稅(特種稅率)
          9：混合應稅與免稅或零稅率(限訊息 C0401 使用)
      */
      $TaxType = $InvoiceData['tax_type'];
			$TaxRate = $InvoiceData['tax_rate']; 														// 稅率
			$TaxAmount = round($InvoiceData['tax_amt']);								// 營業稅
			$TotalAmount = round($InvoiceData['total_amt']);										// 總計
      $Amount=round($Quantity*$UnitPrice);

			$ret = '<?xml version="1.0" encoding="UTF-8"?>
                      <Invoice xmlns="urn:GEINV:eInvoiceMessage:C0401:3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="urn:GEINV:eInvoiceMessage:C0401:3.2 C0401.xsd">
                          <Main>
                              <InvoiceNumber>'.$InvoiceNumber.'</InvoiceNumber>
                              <InvoiceDate>'.$InvoiceDate.'</InvoiceDate>
                              <InvoiceTime>'.$InvoiceTime.'</InvoiceTime>
                              <Seller>
                                  <Identifier>'.$SellerIdentifier.'</Identifier>
                                  <Name>'.$SellerName.'</Name>
                                  <Address>'.$SellerAddress.'</Address>
                                  <TelephoneNumber>'.$SellerTelephoneNumber.'</TelephoneNumber>
                              </Seller>
                              <Buyer>
                                  <Identifier>'.$BuyerIdentifier.'</Identifier>
                                  <Name>'.$BuyerName.'</Name>
                              </Buyer>
                              <InvoiceType>'.$InvoiceType.'</InvoiceType>
                              <DonateMark>'.$DonateMark.'</DonateMark>
                              <CarrierType>'.$CarrierType.'</CarrierType>
                              <CarrierId1>'.$CarrierId1.'</CarrierId1>
                              <CarrierId2>'.$CarrierId2.'</CarrierId2>
                              <PrintMark>'.$PrintMark.'</PrintMark>
                              <NPOBAN>'.$NPOBAN.'</NPOBAN>
                              <RandomNumber>'.$RandomNumber.'</RandomNumber>
                          </Main>
                          <Details>
                              <ProductItem>
                                  <Description>'.$Description.'</Description>
                                  <Quantity>'.$Quantity.'</Quantity>
                                  <UnitPrice>'.sprintf("%1\$.4f",$UnitPrice).'</UnitPrice>
                                  <Amount>'.$Amount.'</Amount>
                                  <SequenceNumber>'.$SequenceNumber.'</SequenceNumber>
                              </ProductItem>
                          </Details>
                          <Amount>
                              <SalesAmount>'.$SalesAmount.'</SalesAmount>
                              <FreeTaxSalesAmount>'.$FreeTaxSalesAmount.'</FreeTaxSalesAmount>
                              <ZeroTaxSalesAmount>'.$ZeroTaxSalesAmount.'</ZeroTaxSalesAmount>
                              <TaxType>'.$TaxType.'</TaxType>
                              <TaxRate>'.$TaxRate.'</TaxRate>
                              <TaxAmount>'.$TaxAmount.'</TaxAmount>
                              <TotalAmount>'.$TotalAmount.'</TotalAmount>
                          </Amount>
                      </Invoice>';
		}else{
		$ret = "NO_DATA";
		}
		error_log("[c/invoice/Invoice1] xml : ".$ret);
		echo $ret;

	}


	/*
	 *	C0501 平臺存證作廢發票
	 */
	/*
	public function CancelInvoice($invoiceno) {

		global $config, $tpl, $deposit;

		//設定 Action 相關參數
		set_status($this->controller);

		// 取得發票號充值相關資料
		$checkInvoice = $deposit->getInvoiceByNo($invoiceno,'use');

		// 確認發票號是否使用
		if(!empty($checkInvoice['depositid'])){

			$CancelInvoiceNumber = $checkInvoice['invoiceno']; 			// 發票號碼
			$InvoiceDate = substr($checkInvoice['insertt'],0 ,10); 		// 發票日期
			$BuyerId = $config['invoice']['BuyerId'];					// 買方統編
			$SellerId = $config['invoice']['SellerId'];					// 賣方統編
			$CancelDate = date('Ymd');			 						// 作廢日期
			$CancelTime = date('H:i:s'); 								// 作廢時間
			$CancelReason = '重複開立'; 								// 作廢原因

			echo '<?xml version="1.0" encoding="UTF-8"?>
  					<CancelInvoice xsi:schemaLocation="urn:GEINV:eInvoiceMessage:C0501:3.1 C0501.xsd" xmlns="urn:GEINV:eInvoiceMessage:C0501:3.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  						<CancelInvoiceNumber>'.$CancelInvoiceNumber.'</CancelInvoiceNumber>
  						<InvoiceDate>'.$InvoiceDate.'</InvoiceDate>
  						<BuyerId>'.$BuyerId.'</BuyerId>
  						<SellerId>'.$SellerId.'</SellerId>
  						<CancelDate>'.$CancelDate.'</CancelDate>
  						<CancelTime>'.$CancelTime.'</CancelTime>
  						<CancelReason>'.$CancelReason.'</CancelReason>
  						<!-- Remark></Remark -->
  					</CancelInvoice>';
		}else{
			echo "作廢失敗，發票未變更 !!";
		}

	}
    */
    /*
	 *	C0501 平臺存證作廢發票
	 */
	public function CancelInvoice($invoiceno) {

		global $config, $tpl, $invoice;

		//設定 Action 相關參數
		set_status($this->controller);

		// 取得發票號充值相關資料
		$checkInvoice = $invoice->getInvoiceByNo($invoiceno,'');

		// 確認發票號是否使用
		if(!empty($checkInvoice['invoiceno']) && $checkInvoice['is_void']!='Y'){

			$CancelInvoiceNumber = $checkInvoice['invoiceno']; 			// 發票號碼
			$InvoiceDate = substr($checkInvoice['insertt'],0 ,10); 		// 發票日期
			$InvoiceDate = str_replace("-","",$InvoiceDate);
			$BuyerId = $config['invoice']['BuyerId'];					// 買方統編
			$SellerId = $config['invoice']['SellerId'];					// 賣方統編
			$CancelDate = date('Ymd');			 						// 作廢日期
			$CancelTime = date('H:i:s'); 								// 作廢時間
			$CancelReason = '資料錯誤'; 								// 作廢原因

			echo '<?xml version="1.0" encoding="UTF-8"?>
  					<CancelInvoice xsi:schemaLocation="urn:GEINV:eInvoiceMessage:C0501:3.2 C0501.xsd" xmlns="urn:GEINV:eInvoiceMessage:C0501:3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  						<CancelInvoiceNumber>'.$CancelInvoiceNumber.'</CancelInvoiceNumber>
  						<InvoiceDate>'.$InvoiceDate.'</InvoiceDate>
  						<BuyerId>'.$BuyerId.'</BuyerId>
  						<SellerId>'.$SellerId.'</SellerId>
  						<CancelDate>'.$CancelDate.'</CancelDate>
  						<CancelTime>'.$CancelTime.'</CancelTime>
  						<CancelReason>'.$CancelReason.'</CancelReason>
  					</CancelInvoice>';
		} else if($checkInvoice['is_void']=='Y') {
		    echo "INVOICE_IS_VOID";
		} else {
			echo "NO_DATA";
		}

	}

	/*
	 *	C0701 註銷發票訊息存證
	 */
	public function VoidInvoice($invoiceno)	{

		global $config, $tpl, $invoice;

		//設定 Action 相關參數
		set_status($this->controller);

		// 取得發票號充值相關資料
		$checkInvoice = $invoice->getInvoiceByNo($invoiceno,'');

		// 確認發票號是否使用
		if(!empty($checkInvoice['invoiceno'])){
			// 確認發票號是否使用
			$VoidInvoiceNumber = $checkInvoice['invoiceno']; 				// 發票號碼
			$InvoiceDate = substr($checkInvoice['insertt'],0 ,10); 			// 發票日期
			$InvoiceDate = str_replace("-","",$InvoiceDate);
			$BuyerId = $config['invoice']['BuyerId'];						// 買方統編
			$SellerId = $config['invoice']['SellerId'];						// 賣方統編
			$VoidDate = date('Ymd');			 							// 註銷日期
			$VoidTime = date('H:i:s'); 										// 註銷時間
			$VoidReason = '資料錯誤';	 									// 註銷原因

			echo '<?xml version="1.0" encoding="UTF-8"?>
					<VoidInvoice xsi:schemaLocation="urn:GEINV:eInvoiceMessage:C0701:3.2 C0701.xsd" xmlns="urn:GEINV:eInvoiceMessage:C0701:3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
						<VoidInvoiceNumber>'.$VoidInvoiceNumber.'</VoidInvoiceNumber>
						<InvoiceDate>'.$InvoiceDate.'</InvoiceDate>
						<BuyerId>'.$BuyerId.'</BuyerId>
						<SellerId>'.$SellerId.'</SellerId>
						<VoidDate>'.$VoidDate.'</VoidDate>
						<VoidTime>'.$VoidTime.'</VoidTime>
						<VoidReason>'.$VoidReason.'</VoidReason>
					</VoidInvoice>';
		}else{
				echo "NO_DATA";
		}
        return;
	}


	// php 5.4 以前不支援 microtime(true) 只好手動
	public function microtime_float() {

		list($usec, $sec) = explode(" ", microtime());
		$ret = (float)$usec + (float)$sec;
		$ret = str_replace(".","",$ret);
		return substr($ret,0,12);

	}


 	/*
	 *	D0401 平臺存證開立折讓證明單(單張發票)
	 */
	public function Allowance($allowance_no='') {

		global $config, $tpl, $deposit, $invoice;

		//設定 Action 相關參數
		set_status($this->controller);

    $arrCond=array();
	if(!empty($allowance_no)) {
	   $arrCond['allowance_no']=$allowance_no;
	}
    $arrCond['switch']='Y';
    $arrCond['upload']='N';
    // 依新增時間昇冪排列
    $arrCond['_ORDERBY']=' insertt asc ';
    // 每次只取一筆未上傳的
    $arrCond['_LIMIT']=' 0,1 ';
    $ret = $invoice->getAllowanceList($arrCond);
    $xml="NO_DATA";
    if($ret && $ret[0]['salid']>0){
      $allowance=$ret[0];
      // 從 $allowance['salid']抓items
      $arrCond=array();
      $arrCond['salid']=$allowance['salid'];
      $arrCond['switch']='Y';
      $arrCond['_ORDERBY']=' seq_no asc ';
      $arrItems=$invoice->getAllowanceItemList($arrCond);
      if($arrItems && $arrItems[0]['saliid']>0){
        $TaxAmount=0;
        $TotalAmount=0;

        // Main Block
        $AllowanceNumber = $allowance['allowance_no'];				                                            // 折讓證明單號碼 改用原發票號碼
        $AllowanceDate = $allowance['allowance_date']; 															// 折讓證明單日期
        $AllowanceDate = str_replace("-","",$AllowanceDate);
		// $AllowanceDate='20200331';
        $SellerIdentifier = $config['invoice']['SellerId'];										// 賣方統編
        $SellerName = $config['invoice']['SellerName']; 										// 賣方名稱
        $SellerAddress = $config['invoice']['SellerAddress'];									// 賣方地址
        $SellerTelephoneNumber = $config['invoice']['SellerTelephoneNumber']; 					// 賣方電話
        $BuyerIdentifier = $config['invoice']['BuyerId'];										// 買方統編
        $BuyerName = $allowance['invoice_buyer_name']; 													// 買方名稱
        $AllowanceType = 2; 																	// 折讓種類 1:買方開立折讓證明單 2:賣方折讓證明通知單

        $xml= '<?xml version="1.0" encoding="UTF-8"?>
                <Allowance xsi:schemaLocation="urn:GEINV:eInvoiceMessage:D0401:3.2 D0401.xsd" xmlns="urn:GEINV:eInvoiceMessage:D0401:3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                    <Main>
                        <AllowanceNumber>'.$AllowanceNumber.'</AllowanceNumber>
                        <AllowanceDate>'.$AllowanceDate.'</AllowanceDate>
                        <Seller>
                            <Identifier>'.$SellerIdentifier.'</Identifier>
                            <Name>'.$SellerName.'</Name>
                            <Address>'.$SellerAddress.'</Address>
                            <TelephoneNumber>'.$SellerTelephoneNumber.'</TelephoneNumber>
                        </Seller>
                        <Buyer>
                            <Identifier>'.$BuyerIdentifier.'</Identifier>
                            <Name>'.$BuyerName.'</Name>
                        </Buyer>
                        <AllowanceType>'.$AllowanceType.'</AllowanceType>
                    </Main>
                <Details>';
        foreach($arrItems as $item){
          $AllowanceSequenceNumber=$item['seq_no'];
          $Quantity = $item['quantity'];
          $UnitPrice = $item['unitprice'];	 												// 單價
          $TaxType = $item['tax_type'];
          $OriginalInvoiceDate = $item['ori_invoice_date'];			 				        // 原發票日期
          $OriginalInvoiceDate = str_replace("-","",$OriginalInvoiceDate);
          $OriginalInvoiceNumber = $item['ori_invoiceno'];
          $OriginalDescription = $item['ori_description'];
          $Amount=$item['amount'];
          $Tax=$item['tax'];
          $xml.='<ProductItem>
                      <OriginalInvoiceDate>'.$OriginalInvoiceDate.'</OriginalInvoiceDate>
                      <OriginalInvoiceNumber>'.$OriginalInvoiceNumber.'</OriginalInvoiceNumber>
                      <OriginalDescription>'.$OriginalDescription.'</OriginalDescription>
                      <Quantity>'.$Quantity.'</Quantity>
                      <UnitPrice>'.$UnitPrice.'</UnitPrice>
                      <Amount>'.($Amount-$Tax).'</Amount>
                      <Tax>'.$Tax.'</Tax>
                      <AllowanceSequenceNumber>'.$AllowanceSequenceNumber.'</AllowanceSequenceNumber>
                      <TaxType>'.$TaxType.'</TaxType>
                  </ProductItem>';

        }


        $TotalAmount = $allowance['allowance_amt'];
        $TaxAmount= $allowance['allowance_tax_amt'];	// 金額合計

        $xml.='</Details>
                <Amount>
                    <TaxAmount>'.$TaxAmount.'</TaxAmount>
                    <TotalAmount>'.($TotalAmount-$TaxAmount).'</TotalAmount>
                </Amount>
              </Allowance>';
		error_log("[c/invoice/Allowance] xml : ".$xml);
        echo $xml;
      }else{
        echo "NO_DETAIL_DATA";
      }

    }else{
      echo "NO_DATA";
    }

	}

	/*
	 *	D0501 平臺存證作廢折讓證明單
	 */
	public function CancelAllowance($allowanceno)	{

		global $config, $tpl, $invoice;

		//設定 Action 相關參數
		set_status($this->controller);

		if(empty($allowanceno)) {
		   	echo "NO_DATA";
			return;
		}
        $allowanceDataList = $invoice->getAllowanceList(array("allowance_no"=>$allowanceno));
		if(count($allowanceDataList)>0) {
		   $allowanceData=$allowanceDataList[0];
		   if($allowanceData && $allowanceData['allowance_no']>0) {
			   echo '<?xml version="1.0" encoding="UTF-8"?><CancelAllowance xsi:schemaLocation="urn:GEINV:eInvoiceMessage:D0501:3.2 D0501.xsd" xmlns="urn:GEINV:eInvoiceMessage:D0501:3.2" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  						<CancelAllowanceNumber>'.$allowanceData['allowance_no'].'</CancelAllowanceNumber>
  						<AllowanceDate>'.str_replace("-","",$allowanceData['allowance_date']).'</AllowanceDate>
						<BuyerId>'.$allowanceData['invoice_buyer_id'].'</BuyerId>
  						<SellerId>25089889</SellerId>
  						<CancelDate>'.date('Ymd').'</CancelDate>
  						<CancelTime>'.date('H:i:s').'</CancelTime>
  						<CancelReason>申報金額錯誤</CancelReason>
  						<Remark></Remark>
  					</CancelAllowance>';

		   }
	    } else{
		   echo "NO_DATA";
		}
		return;
		/* 取得發票號充值相關資料
		$checkInvoice = $deposit->getInvoiceByNo($invoiceno,'use');

		// 確認發票號是否使用
		if(!empty($checkInvoice['depositid'])){

			$CancelAllowanceNumber = '';				 											// 作廢折讓證明單號碼
			$AllowanceDate = date('Ymd'); 														// 折讓證明單日期
			$SellerId = $config['invoice']['SellerId']; 							// 賣方統一編號
			$BuyerId = $config['invoice']['BuyerId']; 								// 買方統一編號
			$CancelDate = date('Ymd');												 				// 折讓證明單作廢日期
			$CancelTime = date('H:i:s');															// 折讓證明單作廢時間
			$CancelReason = '作廢折讓';																// 折讓證明單作廢原因
			$Remark = '';		 																	       // 備註

			echo '<?xml version="1.0" encoding="UTF-8"?>
  					<CancelAllowance xsi:schemaLocation="urn:GEINV:eInvoiceMessage:D0501:3.1 D0501.xsd" xmlns="urn:GEINV:eInvoiceMessage:D0501:3.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  						<CancelAllowanceNumber>'.$CancelAllowanceNumber.'</CancelAllowanceNumber>
  						<AllowanceDate>'.$AllowanceDate.'</AllowanceDate>
  						<BuyerId>'.$BuyerId.'</BuyerId>
  						<SellerId>'.$SellerId.'</SellerId>
  						<CancelDate>'.$CancelDate.'</CancelDate>
  						<CancelTime>'.$CancelTime.'</CancelTime>
  						<CancelReason>'.$CancelReason.'</CancelReason>
  						<Remark>'.$Remark.'</Remark>
  					</CancelAllowance>';
		}else{
			echo "作廢折讓失敗，作廢折讓未開立 !!";
		}
        */
	}


	/*
	 *	E0402 空白未使用字軌檔
	 */
	public function BranchTrackBlank($idate) {

		global $config, $tpl, $deposit;

		//設定 Action 相關參數
		set_status($this->controller);

    error_log("[c/invoice/BranchTrackBlank] idate : ".$idate);

    $InvoiceYear = substr($idate,0 ,4); 								 // 年份
		if(empty($InvoiceYear)){
      echo "發票年份不可為空白 !";
      return;
    }

    $InvoiceMonth = substr($idate,4 ,2); 								 // 開始月
    if(empty($InvoiceMonth)){
      echo "發票起始月份不可為空白 !";
      return;
    }
    if(intval($InvoiceMonth) % 2!=1){
      echo "發票起始月份需為單月 !";
      return;
    }

		$InvoiceMonth2 = substr($idate,6 ,2);                 // 結束月
    if(empty($InvoiceMonth2)){
      echo "發票迄止月份不可為空白 !";
      return;
    }
    if(intval($InvoiceMonth2) % 2!=0){
      echo "發票迄止月份需為雙月 !";
      return;
    }

		// 取得發票字軌相關資料
		$value = $deposit->getInvoiceRail($InvoiceYear,$InvoiceMonth2,'B');

		if(!empty($value)){
      if($value['switch']!='B'){
        echo "字軌資料狀態異常 !!";
        return;
      }
      if($value['used'] >= $value['total']){
        echo "字軌已用完 !!";
        return;
      }
      $HeadBan = $config['invoice']['SellerId'];		 						// 總公司統一編號
      $BranchBan = $HeadBan;		 																// 分支機構統一編號
      $InvoiceType = '07';
      $InvoiceTrack = $value['rail_prefix'];
      $YearMonth = ($InvoiceYear-1911).$InvoiceMonth2;
      $InvoiceBeginNo = str_pad($value['seq_start'] + $value['used'],8,'0',STR_PAD_LEFT);
      $InvoiceEndNo = str_pad($value['seq_start'] + $value['total'] - 1,8,'0',STR_PAD_LEFT);
      echo '<?xml version="1.0" encoding="UTF-8"?>
            <BranchTrackBlank xsi:schemaLocation="urn:GEINV:eInvoiceMessage:E0402:3.1 E0402.xsd" xmlns="urn:GEINV:eInvoiceMessage:E0402:3.1" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
                <Main>
                    <HeadBan>'.$HeadBan.'</HeadBan>
                    <BranchBan>'.$BranchBan.'</BranchBan>
                    <InvoiceType>'.$InvoiceType.'</InvoiceType>
                    <YearMonth>'.$YearMonth.'</YearMonth>
                    <InvoiceTrack>'.$InvoiceTrack.'</InvoiceTrack>
                </Main>
                <Details> <BranchTrackBlankItem>
                            <InvoiceBeginNo>'.$InvoiceBeginNo.'</InvoiceBeginNo>
                            <InvoiceEndNo>'.$InvoiceEndNo.'</InvoiceEndNo>
                          </BranchTrackBlankItem>
                </Details>
            </BranchTrackBlank>';
            // 狀態改為'E'

      return;

    }else{
      echo "該期無未使用字軌 !!";
    }

	}


	/*
	 *	發票上傳成功更新
	 */
	public function InvoiceUpResult($invoiceno, $type) {

		global $config, $tpl, $deposit, $invoice;

		// 設定 Action 相關參數
		set_status($this->controller);

		// 取得發票號充值相關資料
        $checkInvoice = $invoice->getInvoiceByNo($invoiceno);
        error_log("[c/invoice/InvoiceUpResult] : ".json_encode($checkInvoice));
		// 確認發票號是否使用
		if(!empty($checkInvoice['invoiceno'])){
			if($checkInvoice['upload']!='Y'){
				// 指定修改條件
				$arrCond=array();
				$arrCond['invoiceno']=$invoiceno;
				$arrCond['upload']=$type;
				// 指定修改內容
				$arrUpd=array();
				if($type=='N'){
				   // 將未上傳改成上傳中
				   $arrUpd['upload']='P';
				}else if($type=='P'){
				   // 將上傳中改成已上傳
				   $arrUpd['upload']='Y';
				}
				$invoice->updateInvoice($arrUpd,$arrCond);
				$data['retCode'] = 1;
				$data['retMsg'] = "OK";
				$data['retType'] = "MSG";
			}else{
				$data['retCode'] = -2;
				$data['retMsg'] = "發票已上傳過 !!";
				$data['retType'] = "MSG";
			}
		}else{
			$data['retCode'] = -3;
			$data['retMsg'] = "更新失敗，發票未變更 !!";
			$data['retType'] = "MSG";
		}
		echo json_encode($data);

	}


  /*
	 *	發票上傳成功更新
	 */
	public function AllowanceUpResult($allowance_no='', $upload='') {

		global $config, $tpl, $invoice;

		// 設定 Action 相關參數
		set_status($this->controller);

    if(empty($allowance_no))
      $allowance_no=htmlspecialchars($_REQUEST['allowance_no']);
    if(empty($upload))
      $upload=htmlspecialchars($_REQUEST['upload']);
    if(empty($upload))
      $upload='P';
    $arrRet = array("retCode"=>0, "retMsg"=>"NO_DATA");
    if(empty($allowance_no)){
      error_log("[c/invoice/AllowanceUpResult]:".json_encode($ret));
      echo json_encode($arrRet);
      return;
    }
    $arrRet['allowance_no']=$allowance_no;
    $arrRet['upload']=$upload;
    $arrCond=array("switch"=>"Y", "allowance_no"=>$allowance_no);
		$arrUpd=array("upload"=>$upload);
    $arrRet['retCode'] = $invoice->updateAllowance($arrUpd,$arrCond);
    if($arrRet['retCode']==1){
      $arrRet['retMsg']="OK";
    }else{
      $arrRet['retMsg']="ERROR";
    }
    echo json_encode($arrRet);
    return ;

	}


	/*
	 *	發票資料內容輸出 - 電子發票頁面專用
	 *	$json							varchar							瀏覽工具判斷 (Y:APP來源)
	 *	$invoiceno						varchar							發票號碼
	 */
	public function InvoiceDataOut() {

		global $config, $tpl, $invoice;

		// 設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$invoiceno = empty($_POST['invoiceno']) ? htmlspecialchars($_GET['invoiceno']) : htmlspecialchars($_POST['invoiceno']);
		$APP = empty($_POST['APP']) ? htmlspecialchars($_GET['APP']) : htmlspecialchars($_POST['APP']);
		$tpl->assign('APP', $APP);

		// 判斷發票號碼是否為空值
		if(!empty($invoiceno)){
			// 取得發票資料
			$checkInvoice = $invoice->getInvoiceDetailByNo($invoiceno,'');

			// 取得發票字軌資料
			$InvoiceDate = $invoice->getInvoiceRailById($checkInvoice['railid']);

			// 一維條碼資料組合
			$tx_code_bar = ($InvoiceDate['year']-1911).$InvoiceDate['end_month'].$checkInvoice['invoiceno'].$checkInvoice['random_number'];

			// 左側二維碼資料組成
			$hex_sales_amt = base_convert(round($checkInvoice['sales_amt']),10,16);
			$amount = str_pad($hex_sales_amt,8,'0',STR_PAD_LEFT);						// 銷售額

			$hex_total_amt = base_convert(round($checkInvoice['total_amt']),10,16);
			$total = str_pad($hex_total_amt,8,'0',STR_PAD_LEFT);						  // 總計額

			$InvoiceDateTime = Date("Y-m-d", strtotime($checkInvoice['invoice_datetime']));
			$InvoiceYear = substr($InvoiceDateTime,0 ,4); 										                  // 年份
			$InvoiceMonth = substr($InvoiceDateTime,5 ,2);                                     	// 月
			$InvoiceDay = substr($InvoiceDateTime,8 ,2);                                     	 // 日
			$SellerId = $config['invoice']['SellerId']; 												               // 賣方統一編號
			$BuyerId = $checkInvoice['BuyerId']; 													               // 買方統一編號

			$date = ($InvoiceYear-1911).$InvoiceMonth.$InvoiceDay;

			$key = $config['invoice']['key'];
			$aeskey = $this->getEncrypt(($checkInvoice['invoiceno'].$checkInvoice['random_number']),$key);

			$tx_code_qrcl0 = $checkInvoice['invoiceno']."|".$date."|".$checkInvoice['random_number']."|".$amount."|".$total."|".$BuyerId."|".$SellerId."|".$aeskey."|".':**********:1:1:0';

			$tx_code_qrcl = $checkInvoice['invoiceno'].$date.$checkInvoice['random_number'].$amount.$total.$BuyerId.$SellerId.$aeskey.':**********:1:1:0';

			error_log("[c/invoice/InvoiceDataOut] tx_code_qrcl0 : ".$tx_code_qrcl0);
			error_log("[c/invoice/InvoiceDataOut] tx_code_qrcl : ".$tx_code_qrcl);
			// 右側二維碼資料組成
			$tx_code_qrcr = '**系統使用費:1:'.round($checkInvoice['sales_amt']);

			$data['retCode'] = 1;
			$data['retMsg'] = "ok !!";
			$data['retType'] = "MSG";
			$data['SellerName'] = $config['invoice']['SellerName'];
			$data['year'] = $InvoiceDate['year']-1911;
			$data['start_month'] = $InvoiceDate['start_month'];
			$data['end_month'] = $InvoiceDate['end_month'];
			$data['invoiceno'] = $checkInvoice['invoiceno'];
			$data['date'] = Date("Y-m-d", strtotime($checkInvoice['invoice_datetime']));
			$data['time'] = Date("H:i:s", strtotime($checkInvoice['invoice_datetime']));
			$data['random_code'] = $checkInvoice['random_number'];
			$data['amount'] = round($checkInvoice['total_amt']);
			$data['SellerId'] = $SellerId;
			$data['is_winprize'] = $checkInvoice['is_winprize'];
			$data['tx_code_bar'] = $tx_code_bar;
			$data['tx_code_qrcl'] = $tx_code_qrcl;
			$data['tx_code_qrcr'] = $tx_code_qrcr;

			$data['buyer_id'] = $checkInvoice['buyer_id'];
			$data['buyer_name'] = $checkInvoice['buyer_name'];
			$data['donate_mark'] = $checkInvoice['donate_mark'];
			$data['carrier_type'] = $checkInvoice['carrier_type'];
			$data['carrier_id1'] = $checkInvoice['carrier_id1'];
			$data['carrier_id2'] = $checkInvoice['carrier_id2'];
			$data['phonecode'] = $checkInvoice['phonecode'];
			$data['npcode'] = $checkInvoice['npcode'];
			$data['donate_mark'] = $checkInvoice['donate_mark'];
			$data['npoban'] = $checkInvoice['npoban'];
			$data['description'] = $checkInvoice['description'];
			$data['quantity'] = $checkInvoice['quantity'];
			$data['unitprice'] = $checkInvoice['unitprice'];
			$data['tax_rate'] = $checkInvoice['tax_rate'];
			$data['tax_amt'] = $checkInvoice['tax_amt'];
			$data['sales_amt'] = $checkInvoice['sales_amt'];


			if ($checkInvoice['print_mark'] == "Y"){
				$data['print_mark'] = "是";
			}else{
				$data['print_mark'] = "否";
			}

			if (!empty($checkInvoice['phonecode'])){
				$data['carrier_kind'] = "手機條碼";
				$data['carrier_code'] = $checkInvoice['phonecode'];
			}elseif (!empty($checkInvoice['npcode'])){
				$data['carrier_kind'] = "自然人憑證條碼";
				$data['carrier_code'] = $checkInvoice['npcode'];
			}else{
				$data['carrier_kind'] = "會員載具";
				$data['carrier_code'] = $checkInvoice['carrier_id1'];
			}

			if ($checkInvoice['buyer_id'] == "00000000"){
				$data['invoice_kind'] = "電子發票 - 個人";
			}else{
				$data['invoice_kind'] = "電子發票 - 公司";
			}

			if ($checkInvoice['donate_mark'] == "1"){
				$data['invoice_type'] = "已捐贈";
				$data['invoiceno'] = substr_replace($data['invoiceno'], '***', 7, 3);
			}else{
				if ((int)$InvoiceDate['end_month'] >= (int)date('m') && (int)$InvoiceDate['year'] >= (int)date('Y')){
					$data['invoice_type'] = "未開獎";
				}else{
					if ($checkInvoice['is_winprize'] == "Y"){
						$winprize_detail = "";

						//Y檔
						if(empty($data['carrier_code']) && $checkInvoice['print_mark'] == "Y"){
							$winprize_detail = " - 已索取紙本電子發票";
						}
						//A檔
						if(empty($checkInvoice['carrier_id1']) && $data['carrier_kind'] == "會員載具" && $checkInvoice['print_mark'] == "N"){
							$winprize_detail = " - 於".($InvoiceDate['end_month']+1)."/10前寄出";
						}
						//X檔
						if(!empty($data['carrier_code']) && $data['carrier_kind'] != "會員載具"){
							$winprize_detail = " - (詳註1)";
						}
						//Z檔
						if(!empty($checkInvoice['carrier_id1']) && !empty($data['carrier_type'])){
							$winprize_detail = " - (詳註2)";
						}

						$data['invoice_type'] = "已中獎 ".$winprize_detail;
					}else{
						$data['invoice_type'] = "未中獎";
					}
				}
			}

		}else{
			$data['retCode'] = -10001;
			$data['retMsg'] = "發票號碼錯誤，查無相關資料 !!";
			$data['retType'] = "MSG";
		}

		if($json == 'Y'){
			echo json_encode($data);
		}else{
			$tpl->assign('invoice_data', $data);
			$tpl->set_title('');
			if($APP == 'Y'){
				$tpl->render("invoice","home",true,false,$APP);
			}else{
				$tpl->render("invoice","home",true);
			}
		}

	}


	/*
	 *	發票資料加密專用(不可用)
	 *	$sStr						varchar						加密資料
	 *	$sKey						varchar						密鑰

	public function getEncrypt($sStr, $sKey) {

    $iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return base64_encode(
            mcrypt_encrypt(
            	MCRYPT_RIJNDAEL_128,
            	$sKey,
            	$sStr,
            	MCRYPT_MODE_ECB,
            	$iv
            )
		);

	}
	*/

	/*
	 *	發票資料加密用
	 *	$_str						varchar						加密資料
	 *	$_key						varchar						密鑰
	    來源 :
		https://www.facebook.com/notes/%E6%A5%8A%E5%B0%8F%E9%9B%84/%E8%B2%A1%E6%94%BF%E9%83%A8%E9%9B%BB%E5%AD%90%E7%99%BC%E7%A5%A8qrcode-aes%E5%8A%A0%E5%AF%86%E6%96%B9%E5%BC%8F-php%E7%89%88/10153229860846149/
	*/
	public function getEncrypt($_str, $_key) {
			 $sKey = hex2bin($_key);
			 $iv = base64_decode("Dt8lyToo17X/XkXaQvihuA==");
			 $aes_data = $_str; //發票號碼10碼+隨機碼4碼
			 $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $sKey, $this->pkcs5_pad($aes_data, 16), MCRYPT_MODE_CBC, $iv);
			 $aes_data_str = base64_encode($encrypted);
			 return $aes_data_str;
	}

	public function pkcs5_pad($text, $blocksize) {
		$pad = $blocksize - (strlen($text) % $blocksize);
		return $text . str_repeat(chr($pad), $pad);
	}


  public function unitTest() {

    global $invoice, $config;

    /*
    $arrPost=array();
    $arrPost['userid']=4052;
    $arrPost['amt']=180;
    $arrPost['json']='Y';
    $ret = postReq(BASE_URL.APP_DIR."/invoice/createAllowance/",$arrPost);
    */

    $arrPost=array();

    $arrPost['json']='Y';
    $arrPost['userid']=4068;
    $arrPost['total_amt']=152;
	$arrPost['utype']='S';
    $arrPost['invoice_uts']=time();

    $arrPost['items']=array();

    $arrProdItem=array();
    $arrProdItem["seq_no"]=1;
    $arrProdItem["description"]="1商品名稱";
    $arrProdItem["quantity"]=1;
    $arrProdItem["amount"]=15;
    $arrProdItem["unitprice"]=15;

    $arrPost['items'][0]=$arrProdItem;

    $arrProcessItem=array();
    $arrProcessItem["seq_no"]=2;
    $arrProcessItem["description"]="2.商品得標處理費";
    $arrProcessItem["quantity"]=1;
    $arrProcessItem["amount"]=130;
    $arrProcessItem["unitprice"]=137;
    $arrPost['items'][1]=$arrProcessItem;

    $ret = postReq(BASE_URL.APP_DIR."/invoice/createSalesInvoice/",$arrPost);
    error_log("[c/invoice/unitTest] ret : ".json_encode($ret));
    echo json_encode($ret);
    return $ret;

  }


  //折讓
  // $userid 欲折讓的用戶id
  // $amt 欲折讓的金額
	public function  createAllowance($userid='', $amt='') {

    global $config, $invoice;

    // 設定 Action 相關參數
    set_status($this->controller);
    $ret=getRetJSONArray("0","NO_DATA","JSON");

    error_log("[c/invoice/createAllowance] POST: ".json_encode($_POST));
    error_log("[c/invoice/createAllowance] GET: ".json_encode($_GET));

    $jstrAlwn = $_REQUEST['jstrAlwn'];
    if(!empty($jstrAlwn)){
      $arrAlwn = json_decode($jstrAlwn,TRUE);
      if(!is_array($arrAlwn)){
        return $ret;
      }
      $userid=$arrAlwn['userid'];
      $amt=$arrAlwn['amount'];
    }else{
      $userid=htmlspecialchars($_REQUEST['userid']);
      $amt=htmlspecialchars($_REQUEST['amount']);
    }

    if(empty($userid)){
      $ret['retCode']=-1;
      $ret['retMsg']='EMPTY_USERID';
      echo json_encode($ret);
      return;
    }

    if(empty($amt) || $amt<=0){
      $ret['retCode']=-3;
      $ret['retMsg']='ERROR_ALLOWANCE_AMT';
      echo json_encode($ret);
      return;
    }

    $invoice_list = array();
    // 取得用戶有折讓中的發票 (依發票時間 asc & 可折讓金額 asc 排列)
    $invoice_list_in_allowance = $invoice->getInvoiceListInAllowance($userid,'S');
    if($invoice_list_in_allowance){
      foreach($invoice_list_in_allowance as $item){
          $invoice_list[]=$item;
      }
    }

    // 取得用戶未折讓的發票, (依發票時間 asc & 可折讓金額 asc 排列)
    $invoice_list_no_allowance = $invoice->getInvoiceListNotInAllowance($userid,'S');
    if($invoice_list_no_allowance){
      foreach($invoice_list_no_allowance as $item){
        $invoice_list[]=$item;
      }
    }

    // 保留次序  將兩批資料合併(已折讓過的發票在前面)
    if(!$invoice_list || count($invoice_list)<1){
      $ret['retCode']=-10;
      $ret['retMsg']="No Invoice For Allowance !!";
      echo json_encode($ret);
      return;
    }

    $allowance_amt=$amt;
    $arrAllowanceItem=array();
    foreach($invoice_list as $item){
      if($amt>0){
        $allowanceItem=array();
        $allowanceItem['tax_type']='1';
        $allowanceItem['ori_invoice_date']=$item['invoice_datetime'];
        $allowanceItem['ori_invoiceno']=$item['invoiceno'];
        // 折讓明細目前只有一項  先寫死
        $allowanceItem['quantity']=1;
        $allowanceItem['ori_description']=$item['description'];
        // 發票剩餘可折讓金額
        $allowable_allowance_amt = $item['total_amt']-$item['allowance_amt'];
        if($allowable_allowance_amt<=0)
           continue;
        if($amt>=$allowable_allowance_amt){
          // 當折讓金額>=發票可折讓金額時 發票全部折讓掉
          $item['allowance_amt']=$allowable_allowance_amt;
          $allowanceItem['amount']=$allowable_allowance_amt;
          $allowanceItem['tax']=round($allowable_allowance_amt*$item['tax_rate']);
          $allowanceItem['unitprice']=$allowable_allowance_amt;
          $amt-=$allowable_allowance_amt;
        }else if($amt<$allowable_allowance_amt){
          // 當 剩餘折讓金額<發票可折讓金額時 發票僅部分折讓
          $item['allowance_amt']=$amt;
          $allowanceItem['amount']=$amt;
          $allowanceItem['unitprice']=$amt;
          $allowanceItem['tax']=round($amt*$item['tax_rate']);
          $allowanceItem['unitprice']=$amt;
          $amt=0;
        }
        array_push($arrAllowanceItem,$allowanceItem);
      }
    }
    if($amt>0){
      $ret['retCode']=-11;
      $ret['retMsg']="Not Enough Invoice Amount For Allowance !!";
      echo json_encode($ret);
      return;
    }

    // 統計折讓稅額
    $allowance_tax_amt = 0;
    foreach($arrAllowanceItem as $item){
      $allowance_tax_amt+=$item['tax'];
    }


    // 新增allowance主檔
    $arrAllowance = array();
    $arrAllowance['allowance_no']=time();
    $arrAllowance['userid']=$userid;
    $arrAllowance['allowance_date']=date('Ymd');
    $arrAllowance['invoice_buyer_id']='00000000';
    $arrAllowance['invoice_buyer_name']=$userid;
    $arrAllowance['allowance_amt']=$allowance_amt;
    $arrAllowance['allowance_tax_amt']=$allowance_tax_amt;
    /*
    1:買方開立折讓證明單
    2:賣方開立折讓證明通知單
    */
    $arrAllowance['allowance_type']='2';
    $salid = $invoice->createAllowance($arrAllowance);
    if($salid){
      // 新增allowance明細
      $arrAllowance['salid']= $salid;
      $seq_no=1;
      foreach($arrAllowanceItem as $item){
        $item['salid']=$salid;
        $item['seq_no']=str_pad($seq_no,3,"0",STR_PAD_LEFT);
        $saliid = $invoice->createAllowanceItem($item);
        if($saliid>0){
          ++$seq_no;
          // 修改發票資料的allowance_amt
          $invoiceData = $invoice->getInvoiceByNo($item['ori_invoiceno']);
          if($invoiceData){
            $addAmt = $invoiceData['allowance_amt']+$item['amount'];
            $invoice->updateInvoice(array("allowance_amt"=>$addAmt), array("invoiceno"=>$item['ori_invoiceno']));
          }
          $ret['retCode']=1;
          $ret['retMsg']='OK';
          $ret['retObj']=$arrAllowance;

        }
      }
    }
    echo json_encode($ret);

    return;

  }


  // 開立非充值(沒有depositid)的發票
  /*
	輸入 :
	json="Y"
    userid 用戶編號
    utype="S"
    total_amt 總金額(含稅)
    sales_amt 銷售額

	items[$i]['seq_no'] 序號
	items[$i]['siid']   發票主檔流水號
	items[$i]['unitprice'] 單價
	items[$i]['quantity']   數量
	items[$i]['amount']   (含稅)單項金額
	items[$i]['description']  品名
 */
  public  function createSalesInvoice($arrInv='') {

    global $config, $invoice;

    $ret =array("retCode"=>0, "retMsg"=>"NO_DATA");
    $json=htmlspecialchars($_POST["json"]);
    error_log("[c/invoice/createSalesInvoice]  POST : ".json_encode($_POST));

    if(empty($_POST)){
      $ret["retCode"]=-1;
      $ret["retMsg"]="EMPTY_INVOICE_RAWDATA";
      $this->reply($ret,$json);
      return;
    }

    $json="Y";

    if(empty($_POST['userid'])){
      $ret["retCode"]=-2;
      $ret["retMsg"]="EMPTY_UERID";
      $this->reply($ret,$json);
      return;
    }
    $userid=htmlspecialchars($_POST['userid']);

    $utype=htmlspecialchars($_POST['utype']);
	if(empty($utype)) {
	   $utype="S";
	}

    $total_amt=htmlspecialchars($_POST['total_amt']);
    $sales_amt=htmlspecialchars($_POST['sales_amt']);
    if(empty($total_amt) && empty($sales_amt)){
      $ret["retCode"]=-3;
      $ret["retMsg"]="EMPTY_AMOUNT";
      $this->reply($ret,$json);
      return;
    }

    $arrItems = $_POST['items'];
    error_log("[c/createSalesInvoice] arrItems : ".json_encode($arrItems));
    if(!is_array($arrItems) || count($arrItems)<=0){
      $ret["retCode"]=-6;
      $ret["retMsg"]="ERROR_DETAIL_ITEMS";
      $this->reply($ret,$json);
      return;
    }
	//2020-09-10 增加發票時間
	$invoice_datetime = $_POST['items'][0]['invoice_datetime'];
    $arrInvoice = $invoice->genDefaultInvoiceArr($userid, $total_amt, $sales_amt, $utype, '', $invoice_datetime);
    if(!is_array($arrInvoice)){
      $ret["retCode"]=-7;
      $ret["retMsg"]="ERROR_INVOICE_DATA";
      $this->reply($ret,$json);
      return;
    }
    $arrInvoice['upload']="N";
    $arrInvoice['userid']=$userid;
    $rail = "";
	error_log("[c/createSalesInvoice] arrInvoice : ".json_encode($arrInvoice));
    if(!empty($arrInvoice['invoice_datetime'])){
      // 產生發票配號  並與所使用的字軌一起回傳
      $rail = $invoice->genInvoiceNo($arrInvoice['invoice_datetime']);
      if($rail['railid']>0 && $rail['invoiceno']){

        // 字軌資料 PK
        $arrInvoice['railid']=$rail['railid'];

        // 發票配號
        $arrInvoice['invoiceno']=$rail['invoiceno'];

        if($arrInvoice['invoiceno']){
          $ret['invoiceno']=$arrInvoice['invoiceno'];
          // 新增發票資料
          $siid=$invoice->addInvoiceData($arrInvoice);

          // 發票銷項明細資料
          if($siid>0){
            for($i=0; $i<count($arrItems);++$i){

              $arrItems[$i]['siid']=$siid;
              $sipid = $invoice->addInvoiceProdItem($arrItems[$i]);
            }
            //將used+1回寫到saja_invoice_rail中後
            $update_rail = $invoice->updateInvoiceRail($rail['railid'],$rail['rail_prefix'],'');

            //檢查該發票字軌編號是否用完
            $rail = $invoice->getInvoiceRailById($rail['railid']);
            if($rail && $rail['used']>=$rail['total']){
              $update_invoice_rail_switch = $invoice->updateInvoiceRail($rail['railid'],$rail['rail_prefix'],'E');
            }
            $ret['retObj']['invoiceno']=$arrInvoice['invoiceno'];
            $ret['retObj']['random_code']=$arrInvoice['random_number'];
            $ret['retObj']['siid']=$siid;
            $ret['retCode']=1;
            $ret['retMsg']="OK";
          }
        }else{
          $ret['retCode']=-6;
          $ret['retMsg']="ERROR_INVOICE_TRACK";
        }
      }else{
        $ret['retCode']=-7;
        $ret['retMsg']="EMPTY_INVOICE_NO";
      }
    }
    error_log("[c/invoice/createSalesInvoice] : ".json_encode($ret));
    $this->reply($ret,$json);

    return;

  }


  public function reply($arrRet, $json) {

    if($json=='Y'){
      echo json_encode($arrRet);
      return;
    }else{
      return $ret;
    }

  }

  public function invoice_complete(){

	global $config, $invoice;

	$invoiceno_arr = htmlspecialchars($_POST['invoiceno_arr']);
	$idate = htmlspecialchars($_POST['idate']);

	//整合服務平台回覆已上傳成功的發票
	$invoiceno = json_decode(urldecode($invoiceno_arr), true);

	//殺價王上傳成功但尚未更新已完成的發票
	$invoice_list = $invoice->getInvoiceListForComplete($idate);

	error_log("[c/invoice/Invoice]:".json_encode($invoice_list));

	if(count($invoice_list) > 0){

		$i = 0;
		foreach($invoice_list as $row){

			if(in_array($row['invoiceno'], $invoiceno)){
				$invoice->updateInvoiceComplete($row['siid']);
				$i++;
			}
		}

		echo '總共更新 : '.$i.'筆';
	}else{
		echo '沒有需要更新的發票';
	}
	exit;
  }

  // 批次修改發票上傳狀態
  public function BatchUpdateInvoicesUploadStatus($jstrInvoicenos) {

	     global $config, $invoice;

		 $ret = array('retCode'=>0, 'retMsg'=>"NO_DATA");
		 error_log("[c/invoice/BatchUpdateInvoicesUploadStatus] invoicenos :".$jstrInvoicenos);
		 if($jstrInvoicenos) {
		    $arrInvoicenos = json_decode($jstrInvoicenos);
		    if(is_array($arrInvoicenos) && count($arrInvoicenos)>0) {
			   $arrCond = array("invoicenos"=>$arrInvoicenos, "switch"=>"Y", "upload"=>"P");
			   $arrUpd = array("upload"=>"Y");
			   error_log("[c/invoice/BatchUpdateInvoicesUploadStatus] invoicenos : ".json_encode($arrInvoicenos));
			   $num = $invoice->updateInvoice($arrUpd, $arrCond);
			   if($num>0) {
				  $ret['retCode']=1;
                  $ret['retMsg']='OK';
                  $ret['updnum']=$num;
			   }
		    }
		 }
	     $this->reply($ret,"Y");
  }

  // 批次修改作廢發票上傳狀態
  public function BatchUpdateInvoicesCancelStatus($jstrInvoicenos) {

	     global $config, $invoice;

		 $ret = array('retCode'=>0, 'retMsg'=>"NO_DATA");
		 error_log("[c/invoice/BatchUpdateInvoicesCancelStatus] invoicenos :".$jstrInvoicenos);
		 if($jstrInvoicenos) {
		    $arrInvoicenos = json_decode($jstrInvoicenos);
		    if(is_array($arrInvoicenos) && count($arrInvoicenos)>0) {
			   $arrCond = array("invoicenos"=>$arrInvoicenos, "switch"=>"Y", "is_void"=>"N");
			   $arrUpd = array("is_void"=>"Y");
			   $num = $invoice->updateInvoice($arrUpd, $arrCond);
			   if($num>0) {
				  $ret['retCode']=1;
                  $ret['retMsg']='OK';
                  $ret['updnum']=$num;
			   }
		    }
		 }
	     $this->reply($ret,"Y");
  }

  // 批次修改折讓單上傳狀態
  public function BatchUpdateAllowanceUploadStatus($jstrAllowancenos) {

	     global $config, $invoice;

		 $ret = array('retCode'=>0, 'retMsg'=>"NO_DATA");
		 // error_log("[c/invoice/BatchUpdateAllowanceUploadStatus] allowancenos :".$jstrAllowancenos);
		 if($jstrAllowancenos) {
		    $arrAllowancenos = json_decode($jstrAllowancenos);
		    if(is_array($arrAllowancenos) && count($arrAllowancenos)>0) {
			   $arrCond = array("allowancenos"=>$arrAllowancenos, "switch"=>"Y", "upload"=>"P");
			   $arrUpd = array("upload"=>"Y");
			   error_log("[c/invoice/BatchUpdateAllowanceUploadStatus] allowancenos : ".json_encode($arrAllowancenos));
			   $num = $invoice->updateAllowance($arrUpd, $arrCond);
			   if($num>0) {
				  $ret['retCode']=1;
                  $ret['retMsg']='OK';
                  $ret['updnum']=$num;
			   }

		    }
		 }
	     $this->reply($ret,"Y");
  }

    /* 取得中獎發票號碼並存入local DB :saja_cash_flow.saja_invoice_prize
  // $invTerm為該期發票的民國年+雙月份 (ex: 10808)
  public function getInvoicePrize($invTerm='') {

	     global $config, $invoice;
		 if(empty($invTerm)) {
			$m=date("n");
			if($m%2==0) {
				// 偶數月 : 要取上一期
				$year= date("Y",strtotime("-2 month"));
				$month= date("m",strtotime("-2 month"));
			} else if($m%2>0) {
				// 奇數月 : 要取上個月
				$year= date("Y",strtotime("-1 month"));
				$month= date("m",strtotime("-1 month"));
			}
			$invTerm=str_pad($year-1911,3,"0",STR_PAD_LEFT).str_pad($month,2,"0",STR_PAD_LEFT);
		 }

		 $arrParams['invTerm']=$invTerm;
	     $arrParams['action']="QryWinningList";
	     $arrParams['version']="0.2";
	     $arrParams['UUID']=$_SERVER['SERVER_ADDR'];
	     $arrParams['appID']="EINV4202004216892";
		 $url = "https://api.einvoice.nat.gov.tw/PB2CAPIVAN/invapp/InvApp";

	     $ch = curl_init();
		 curl_setopt($ch, CURLOPT_POST, 1);
		 curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
		 curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
		 curl_setopt($ch, CURLOPT_URL, $url );
		 curl_setopt($ch, CURLOPT_HEADER, false);
		 curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arrParams));
		 curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
		 $response=curl_exec($ch);
		 curl_close($ch);
         $ret=array("retCode"=>0, "retMsg"=>"NO_DATA");
		 if($response) {
            $arrPrize = json_decode($response,true);
			if(is_array($arrPrize)) {

                unset($arrPrize['v']);
				unset($arrPrize['timeStamp']);
				unset($arrPrize['msg']);
				unset($arrPrize['code']);

                $arrPrize['year']=intval(substr($arrPrize['invoYm'],0,3))+1911;
                $arrPrize['end_month']=intval(substr($arrPrize['invoYm'],-2));
                $arrPrize['end_month']=	str_pad($arrPrize['end_month'],2,"0",STR_PAD_LEFT);
                $arrPrize['start_month']=intval($arrPrize['end_month'])-1;
				$arrPrize['start_month']=str_pad($arrPrize['start_month'],2,"0",STR_PAD_LEFT);

				// 檢查是否已有資料
				$chk=$invoice->getInvoicePrize(array("invoYm"=>$arrPrize['invoYm'],"switch"=>"Y"));
				if($chk && $chk[0]['sipid']>0) {
					 $ret['retCode']=-2;
					 $ret['retMsg']="DUPLICATE";
					 $ret['retObj']=$arrPrize;
				} else {
					$num_row = $invoice->createInvoicePrize($arrPrize);
					if($num_row) {
					   $ret['retCode']=1;
					   $ret['retMsg']="OK";
					   $ret['retObj']=$arrPrize;
					}
				}
			}
         }
         echo json_encode($ret);
	}
	*/

	// 呼叫電子發票應用服務API 取得中獎發票資訊
	public function getInvoicePrizeData($invTerm='') {

		   global $config, $invoice;

			if(empty($invTerm)) {
				$m=date("n");
				if($m%2==0) {
					// 偶數月 : 要取上一期
					$year= date("Y",strtotime("-2 month"));
					$month= date("m",strtotime("-2 month"));
				} else if($m%2>0) {
					// 奇數月 : 要取上個月
					$year= date("Y",strtotime("-1 month"));
					$month= date("m",strtotime("-1 month"));
				}
				$invTerm=str_pad($year-1911,3,"0",STR_PAD_LEFT).str_pad($month,2,"0",STR_PAD_LEFT);
			 }

			 $arrParams['invTerm']=$invTerm;
			 $arrParams['action']="QryWinningList";
			 $arrParams['version']="0.2";
			 $arrParams['UUID']=$_SERVER['SERVER_ADDR'];
			 $arrParams['appID']="EINV4202004216892";
			 $url = "https://api.einvoice.nat.gov.tw/PB2CAPIVAN/invapp/InvApp";

			 $ch = curl_init();
			 curl_setopt($ch, CURLOPT_POST, 1);
			 curl_setopt($ch,CURLOPT_SSL_VERIFYHOST,0);
			 curl_setopt($ch,CURLOPT_SSL_VERIFYPEER,0);
			 curl_setopt($ch, CURLOPT_URL, $url );
			 curl_setopt($ch, CURLOPT_HEADER, false);
			 curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arrParams));
			 curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
			 $response=curl_exec($ch);
			 curl_close($ch);
			 if($response) {
                return json_decode($response,true);
			 }
			 return false;
	}

	// 取得中獎發票號碼後存入local DB :saja_cash_flow.saja_invoice_prizeno
    // $invTerm為該期發票的民國年+雙月份 (ex: 10808)
    public function genInvoicePrizeData($invTerm='') {

		    global $config, $invoice;

		    $ret=array("retCode"=>0, "retMsg"=>"NO_DATA");

			if(empty($invTerm)) {
			  $m=date("n");
			  if($m%2==0) {
				// 偶數月 : 要取上一期
				$year= date("Y",strtotime("-2 month"));
				$month= date("m",strtotime("-2 month"));
			  } else if($m%2>0) {
				// 奇數月 : 要取上個月
				$year= date("Y",strtotime("-1 month"));
				$month= date("m",strtotime("-1 month"));
			  }
			  $invTerm=str_pad($year-1911,3,"0",STR_PAD_LEFT).str_pad($month,2,"0",STR_PAD_LEFT);
		    }

           // 取得中獎發票號碼
		   $arrPrize = $this->getInvoicePrizeData($invTerm);
			if(is_array($arrPrize)) {
				$ret['retCode']=1;
				$ret['retMsg']="OK";
				$ret['retObj']=$arrPrize;
				foreach($arrPrize as $k=>$v) {
					$arrNew=array();
					$arrNew['invoYm']=$arrPrize['invoYm'];
					$arrNew['year']=intval(substr($arrPrize['invoYm'],0,3))+1911;
					$arrNew['end_month']=intval(substr($arrPrize['invoYm'],-2));
					$arrNew['start_month']=intval($arrNew['end_month'])-1;
					$arrNew['end_month']=str_pad($arrNew['end_month'],2,"0",STR_PAD_LEFT);
					$arrNew['start_month']=str_pad($arrNew['start_month'],2,"0",STR_PAD_LEFT);
					if(strpos($k,"PrizeNo")>0 && !empty($v)) {
						$arrNew['prize_desc']=$k;
						$arrNew['prize_no']=$v;
						$invoice->createInvoicePrizeno($arrNew);
					} else if(strpos($k,"PrizeAmt")>0 && !empty($v)) {
						$arrNew['prize_desc']=$k;
						$arrNew['prize_amt']=$v;
						$invoice->createInvoicePrizeno($arrNew);
					}
				}

			}
			echo json_encode($ret);
	}

	public function chkInvoicePrizeData($invTerm='') {

		   global $config, $invoice;

		   $ret=array("retCode"=>0, "retMsg"=>"NO_DATA");
		   if(empty($invTerm)) {
			  $ret['retCode']=-1;
              $ret['retMsg']="請傳入 invoTerm=民國年+偶數月份 (ex:10808) ";
              echo json_encode($ret);
              exit;
		   }

		   $yyyy=substr($invTerm,0,3)+1911;
		   $mm=substr($invTerm,-2);

		   $arrPrize=array();
		   $arrPrize0=$invoice->getPrizeOfExactlyDigit($yyyy,$mm, 8);
		   if(is_array($arrPrize0)) {
			  $arrPrize=array_merge($arrPrize,$arrPrize0);
		   }

		   $arrPrize1=$invoice->getPrizeOfPartialDigit($yyyy,$mm, 3);
		   if(is_array($arrPrize1)) {
			  $arrPrize=array_merge($arrPrize,$arrPrize1);
		   }

		   if($arrPrize && count($arrPrize)>0) {
			  $arrCond=array("switch"=>"Y","is_void"=>"N");
			  foreach($arrPrize as $inv) {
			     $arrCond["invoicenos"][]=$inv['invoiceno'];
			  }
			  if(count($arrCond["invoicenos"])>0 && !empty( $arrCond["invoicenos"][0])) {
				 $arrUpd=array("is_winprize"=>"Y");
				 $invoice->updateInvoice($arrUpd,$arrCond);
			  }
			  $ret['retCode']=1;
	          $ret['retMsg']='OK';
			  $ret['retObj']=$arrPrize;
		   }
		   echo json_encode($ret);
	}

	// 歸戶相關api
	// 從大平台過來小平台歸戶
	public function AggregateByEinv() {
		   global $config, $invoice, $tpl;

			//設定 Action 相關參數
		    set_status($this->controller);

		    date_default_timezone_set('Asia/Taipei');

			// echo "[c/invoice/AggregateByEinv] POST : ".json_encode($_POST);

			error_log("[c/invoice/AggregateByEinv] POST : ".json_encode($_POST));
			error_log("[c/invoice/AggregateByEinv] GET : ".json_encode($_GET));

			$ban=$_POST['ban'];
			$token=$_POST['token'];

			if(empty($ban) || empty($token)) {
			   echo "empty input !!";
               return;
			}

			//production
			// $callback_url="https://einvoice.nat.gov.tw/APCONSUMER/BTC101I/";
			// $callback_url="https://einvoice.nat.gov.tw/APCONSUMER/BTC103I/";

			//test
			// $callback_url="https://wwwtest.einvoice.nat.gov.tw/APCONSUMER/BTC101I/";
			$callback_url="https://wwwtest.einvoice.nat.gov.tw/APCONSUMER/BTC101I/";

			$arrPost=array("token"=>$token);
			$nonce=substr(md5($config['invoice']['SellerId']."~".$config['invoice']['CarrierType']."~".time()),0,16);
			$arrPost['nonce']=$nonce;

			error_log("[c/AggregateByEinv] send post : ".json_encode($arrPost));

			$data = json_encode($arrPost);
			$curl = curl_init($callback_url);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0); // 對認證證書來源的檢查
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0); // 從證書中檢查SSL加密算法是否存在
			curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
			$response = curl_exec($curl);
			curl_close($curl);
			/*
			$stream_options = array(
                'http' => array (
                        'method' => "POST",
                        'content' => json_encode($arrPost),
                        'header' => "Content-Type:application/json"
                )
            );
            $context  = stream_context_create($stream_options);
            // 送出json內容並取回結果
            $response = file_get_contents($callback_url, false, $context);
			*/
            error_log("[c/invoice/AggregateByEinv] response : ".$response);
			if($response) {
			   $arrRet = json_decode($response,TRUE);
   			   if(is_array($arrRet)) {
				   // 驗nonce
				   if($arrRet['nonce']!=$nonce) {
					  echo "SIGN CHECK FAILED !!";
				      return;
				   }
				   // nonce驗簽通過  token失敗
				   if($arrRet['token_flag']!='Y') {
                      echo $arrRet['err_msg'];
					  return;
                   }
				   // nonce驗簽通過  token成功
                   if($arrRet['token_flag']=='Y') {
                      $tpl->set_title('');
		              $tpl->render("invoice","AggregateLogin",true);
					  return;
                   }
			   } else {
				  echo "einvoice double-check failed !!";
			   }
			} else {
			   echo "einvoice empty response !!";
			}
			return;

	}


	public function AggregateBySaja() {

		    global $config, $invoice;

			//設定 Action 相關參數
		    set_status($this->controller);

		    date_default_timezone_set('Asia/Taipei');

			error_log("[invoice/AggregateBySaja] : ".json_encode($_POST));

			$back_url=$_POST['back_url'];
			$card_type=$_POST['card_type'];
			$card_no2=$_POST['card_no2'];
			$card_no1=$_POST['card_no1'];
			$card_ban=$_POST['card_ban'];
			$token=$_POST['token'];

			$now=time();

			// 驗證token
			$ret="N";
			for($t=$now-20;$t<=$now+10;++$t) {
			    // error_log(base64_decode($card_type)."~".base64_decode($card_no1)."~".$t);
				if($token==md5(base64_decode($card_type)."~".base64_decode($card_no1)."~".$t)) {
				   $ret="Y";
                   break;
                }
			}
			echo $ret;
            return;
	}

	// 小平台向大平台歸戶  發起驗證
	public function Aggregate() {

		    global $config, $invoice, $tpl;

			//設定 Action 相關參數
		    set_status($this->controller);

		    date_default_timezone_set('Asia/Taipei');

			error_log("[c/invoice/Aggregate] : ".json_encode($_POST));
			$userid = $_SESSION['auth_id'];
			if (empty($_SESSION['auth_id'])){
				$userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);
			}
			if(empty($userid)) {
			   echo "FAILED !!";
               exit;
			}

			// test data
			$card_type=$config['invoice']['CarrierType'];
			$card_ban=$config['invoice']['SellerId'];
			$card_no1="SJ".$userid;
			$card_no2=$userid;

			$back_url='https://www.saja.com.tw/site/invoice/AggregateResult/';
			// $back_url='http://test.shajiawang.com/site/invoice/AggregateResult/';
			// $action='https://wwwtest.einvoice.nat.gov.tw/APMEMBERVAN/membercardlogin';
      $action='https://wwwtest.einvoice.nat.gov.tw/APCONSUMER/BTC103I/';

			$ori_token=$card_type."~".$card_no1."~".time();
			error_log("[c/invoice/Aggregate] ori_token:".$ori_token);
			$token=md5($ori_token);

			$tpl->assign('action',$action);
			$tpl->assign('card_ban',$card_ban);
		    $tpl->assign('card_no1',base64_encode($card_no1));
		    $tpl->assign('card_no2',base64_encode($card_no2));
		    $tpl->assign('card_type',base64_encode($card_type));
		    $tpl->assign('back_url',$back_url);
		    $tpl->assign('token',$token);
			$tpl->set_title('');
		    $tpl->render("invoice","Aggregate",true);

	}

	// 小平台向大平台歸戶  接收回覆
	public function AggregateResult() {

		    global $config, $invoice, $tpl, $user;

			//設定 Action 相關參數
		    set_status($this->controller);

		    date_default_timezone_set('Asia/Taipei');

			$ret="N";

			error_log("[invoice/AggregateResult] : ".json_encode($_POST));
			//  {"card_ban":"25089889","card_no1":"U0oxNzA1","card_no2":"U0oxNzA1","card_type":"RUcwNTQ0","rtn_flag":"Y"}
			$card_type=htmlspecialchars($_POST['card_type']);
			$card_no2=htmlspecialchars($_POST['card_no2']);
			$card_no1=htmlspecialchars($_POST['card_no1']);
			$card_ban=htmlspecialchars($_POST['card_ban']);
			$rtn_flag=htmlspecialchars($_POST['rtn_flag']);

			if("Y"==$rtn_flag) {
			   $userid=intval(base64_decode($card_no2));
			   if(!empty($userid)) {
					$arrCond=array("userid"=>$userid);
					$arrUpd=array();
					$arrUpd["carrier_aggregate"]="Y";
					$arrUpd["carrier_id1"]=base64_decode($card_no1);
					$arrUpd["carrier_id2"]=base64_decode($card_no2);
					$arrUpd["carrier_type"]=base64_decode($card_type);
					$user->updUserProfile($arrUpd, $arrCond);
               }
			}
			echo $rtn_flag;
			return;
	}



	public function check_phonecode() {

		global $config, $invoice, $tpl, $user;

		$code = empty($_POST['barCode']) ? htmlspecialchars($_GET['barCode']) : htmlspecialchars($_POST['barCode']);
		$appid = empty($_POST['appid']) ? htmlspecialchars($_GET['appid']) : htmlspecialchars($_POST['appid']);
		$TxID = empty($_POST['TxID']) ? htmlspecialchars($_GET['TxID']) : htmlspecialchars($_POST['TxID']);
		$action = empty($_POST['action']) ? htmlspecialchars($_GET['action']) : htmlspecialchars($_POST['action']);
		$version = empty($_POST['version']) ? htmlspecialchars($_GET['version']) : htmlspecialchars($_POST['version']);

		$code2 = str_replace("+",'%2B',$code);

		//設定 Action 相關參數
		set_status($this->controller);

		date_default_timezone_set('Asia/Taipei');
		// $url = "https://www-vc.einvoice.nat.gov.tw/BIZAPIVAN/biz";
		$url = "https://api.saja.com.tw/api/einvoice/";
		$data = array(
			'version' 		=> "1.0",
			'action' 		=> "bcv",
			'barCode' 		=> $code2,
			'TxID' 			=> date('YmdHis'),
			'appId'      	=> $config['invoice']['appId']
		);

		//設定送出方式-POST
		$stream_options = array(
			'http' => array (
				'method' => "POST",
				'header' => "Content-Type:application/x-www-form-urlencoded",
				'content' => http_build_query($data)
			 )
		);
		$context = stream_context_create($stream_options);
		// 送出json內容並取回結果
		$response = file_get_contents($url, false, $context);
		// 讀取json內容
		// $arr = json_decode($response,true);
		error_log("[invoice/check_phonecode] : ".($response));

		echo $response;
		exit;
	}
}
?>
