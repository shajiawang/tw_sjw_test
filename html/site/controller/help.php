<?php
/*
 *  Help Controller
 */
class Help {

	public $controller = array();
	public $params = array();
	public $id;

	public function __construct() {

		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
		$this->datetime = date('Y-m-d H:i:s');

	}

/* 	public function home() {

		global $tpl, $testeason;

		$app = empty($_POST['APP']) ? htmlspecialchars($_GET['APP']) : htmlspecialchars($_POST['APP']);
		$tpl->assign('APP', $app);

		//設定 Action 相關參數
		set_status($this->controller);

		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("help","home",true,false,$app);

	} */
	
	
	public function home() {

		global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['APP']) ? htmlspecialchars($_GET['APP']) : htmlspecialchars($_POST['APP']);

		//幫助中心分類
		$_category = $faq->help_category();
		foreach($_category as $tk => $tv){
			$table = $faq->help_list($tv['hcid']);
			$_category[$tk]['help_list'] = $table;
		}
		
		
		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['category_list']=$_category;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('help_category', $_category);
		}
		
		$tpl->assign('noback', 'Y');
		$tpl->set_title('幫助中心');
		$tpl->render("help","help_list",true,false,$app);

	}	

}

?>
