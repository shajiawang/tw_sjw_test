<?php

class closeproduct {

	public $controller = array();
	public $params = array();
	public $id;


	public function home() {

		global $db, $config;
		
	}

}
?>
<?php
/*
	if($_SESSION['auth_id'] != '1705'){
		echo '<script>
			alert("請登入1705帳號");
			window.location="'.BASE_URL.'";
			</script>';
		//header('Location:'.BASE_URL.APP_DIR);
	}
	*/
	global $db, $config;

	//設定 Action 相關參數
	set_status($this->controller);

	$query ="SELECT *
			FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` p
			WHERE
			p.closed = 'N'
			AND p.is_flash != 'Y'
			AND p.display = 'Y'
			AND p.switch = 'Y'
			AND unix_timestamp() >= unix_timestamp(p.ontime)
			AND (
				(unix_timestamp(p.offtime) > 0 AND unix_timestamp() < unix_timestamp(p.offtime))
				OR (unix_timestamp(p.offtime) = 0 AND p.locked = 'N'))
			ORDER BY p.offtime ASC, p.productid ASC, p.seq ASC";

	$table = $db->getQueryRecord($query);
	
?>
<style>
	body{
		font-size:24pt;
	}
	table, th, td {
		border: 1px solid black;
	}
	table{
		width: 60vw;
	}
	th, td{
		font-size:24pt;
	}
</style>
<script src='https://code.jquery.com/jquery-3.4.1.min.js'></script>

<h3>要結標的商品</h3>
<select name="productid" id="productid" style="font-size:24pt">
		<?php
		
		foreach($table['table']['record'] as $row){
		?>
			<option id="<?php echo $row['productid']?>" value="<?php echo $row['productid']?>"><?php echo $row['name']?></option>
			<?php
		}
		?>
	</select>
	<br><br><br>
	<div>
		<input type="text" id="password" style="font-size:24pt" placeholder="請輸入密碼">
	</div>
	<br><br>
	<div>
		<button id="button" style="font-size:24pt" onclick="send()">確認</button>
	</div>

<script type="text/javascript">
	function send(){

		var msg = "確定要手動結標:" + $('#productid').val(); 
		if (confirm(msg)==true){ 
			$.ajax({
				url: "<?php echo APP_DIR;?>/instantkill/close_bid",
				data:{productid : $('#productid').val() , password : $('#password').val()},
				dataType: "JSON",
				type: "POST",
				contentType : "application/x-www-form-urlencoded; charset=utf-8",
				success: function(data) {
					console.log(data);
					if(data.retCode == 1 || data.retCode == -5){
						alert($('#' + $('#productid').val()).text() + '結標完成');
						window.location.reload();
					}else{
						alert('操作失敗，錯誤代碼:' + data.retCode + ',錯誤訊息:' + data.retMsg);
					}
				}
			});			
			
		}else{ 
			return false; 
		}
	}
</script>