<?php
/*
 * Bid Controller //最新成交(殺價得標、流標)
 */

include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR."/helpers.php");

class Bid {

  public $controller = array();
	public $params = array();
  public $id;


	/*
	 *	最新成交清單列表
	 *	$type			varchar			成交狀態
   * 	$genHTML		varchar			建立新頁參數
   */
   /*
	public function home() {

		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_POST['type'])?htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$tpl->assign('type', $type);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];


		//建立bid info HTML靜態頁面
		$genHTML=$_GET['genHTML'];
		$product_list=null;
		//判斷成交狀態參數
		if (empty($type) || $type == 'deal') {	//得標

      //取得成交清單，目前只顯示金額大於500的資料
			$product_list = $bid->product_list();
      if($genHTML=='QazwSxedC') {
        foreach($product_list['table']['record'] as $product) {
			    echo "productid : ".$product['productid']."<br>";
          $_GET['productid']=$product['productid'];
          $this->genBidDetailHtml();
			  }
			  exit;
      }

		}else{	//流標

      $product_list = $bid->product_cancelled_list();

    }

		if($product_list) {

      for($idx=0; $idx< count($product_list['table']['record']); ++$idx) {
        $p=$product_list['table']['record'][$idx];
				if(empty($p['thumbnail_url']) && !empty($p['thumbnail'])) {
          $product_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$p['thumbnail'];
				  $product_list['table']['record'][$idx]['offtime']=(int)$p['offtime'];
				  $product_list['table']['record'][$idx]['now']=(int)$p['now'];
        }
		  }

    }

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if($json=='Y') {

			$ret=getRetJSONArray(1,'OK','LIST');
			if((empty($p)) || ($p=='') || ($p < 0) || ($p > $product_list['table']['page']['totalpages'])){
				$ret['retObj']['data'] = '';
			}else{
				$ret['retObj']['data'] = $product_list['table']['record'];
				$ret['retObj']['page'] = $product_list['table']['page'];
			}
			echo json_encode($ret);
			exit;

		}

		$tpl->assign('product_list', $product_list);

		//設定分頁
		if(empty($product_list['table']['page']) ){

			$page_content = array();

		}else{

			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($product_list, $page_path);

		}

		$meta['title']="最新得標";
		$meta['description']='';
		$tpl->assign('meta',$meta);

		$tpl->assign('page_content', $page_content);
		$tpl->set_title('');
		$tpl->render("bid","home",true);

	}
*/

  /*
   *	最新成交清單列表
   *	$type			varchar			成交狀態
   * 	$genHTML		varchar			建立新頁參數
   */
  public function home() {

    global $tpl, $bid, $product;

    //設定 Action 相關參數
    set_status($this->controller);

    //成交狀態
    $type = empty($_POST['type'])?htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
    $tpl->assign('type', $type);
    $userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

    $page_number = empty($_POST['p'])?htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);


    //建立bid info HTML靜態頁面
    $genHTML=$_GET['genHTML'];
    $product_list=null;
    //判斷成交狀態參數
    if (empty($type) || $type == 'deal') {	//得標

      //取得成交清單，目前只顯示金額大於98的資料
      //$product_list = $bid->product_list();
      $product_list = $bid->recently_closed_product_list($page_number);

      if($genHTML=='QazwSxedC') {
        foreach($product_list['table']['record'] as $product) {
          echo "productid : ".$product['productid']."<br>";
          $_GET['productid']=$product['productid'];
          $this->genBidDetailHtml();
        }
        exit;
      }

    }else{	//流標

      $product_list = $bid->product_cancelled_list();

    }

    if($product_list) {

      for($idx=0; $idx< count($product_list['table']['record']); ++$idx) {
        $p=$product_list['table']['record'][$idx];
        if(empty($p['thumbnail_url']) && !empty($p['thumbnail'])) {
          $product_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$p['thumbnail'];
          $product_list['table']['record'][$idx]['offtime']=(int)$p['offtime'];
          $product_list['table']['record'][$idx]['now']=(int)$p['now'];
        }
      }

    }

    $json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
    $p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

    if($json=='Y') {

      $ret=getRetJSONArray(1,'OK','LIST');
      if((empty($p)) || ($p=='') || ($p < 0) || ($p > $product_list['table']['page']['totalpages'])){
        $ret['retObj']['data'] = '';
      }else{
        $ret['retObj']['data'] = $product_list['table']['record'];
        $ret['retObj']['page'] = $product_list['table']['page'];
      }
      echo json_encode($ret);
      exit;

    }

    $tpl->assign('product_list', $product_list);

    //設定分頁
    if(empty($product_list['table']['page']) ){

      $page_content = array();

    }else{

      $page_path = $tpl->variables['status']['status']['path'];
      $page_content = $this->set_page($product_list, $page_path);

    }

    $meta['title']="最新得標";
    $meta['description']='';
    $tpl->assign('meta',$meta);

    $tpl->assign('page_content', $page_content);
    $tpl->set_title('');
    $tpl->render("bid","home",true);

  }

	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

		$table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'].'&type='.$_GET['type'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'].'&type='.$_GET['type'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';

		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;

  }


	//最新成交(殺價中標、流標)商品
  public function detail() {

		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = htmlentities($_GET['type']);
		$productid=htmlentities($_GET['productid']);
		$channelid=htmlentities($_GET['channelid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		if(empty($productid)){
			return_to('site/bid');
		}else{
			if(file_exists('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html')){
         // Static bid detail page file exists
			   //error_log("[bid/detail] Show /var/www/html".APP_DIR."/static/html/BidDetail_".$productid.".html ");
      }else{
				// Static bid detail page does not exist, generate Static Html Page
				$this->genBidDetailHtml();
      }

      $product = $bid->get_info($productid);

      if(!empty($product['thumbnail'])){
				$product['img_src'] = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}elseif(!empty($product['thumbnail_url'])){
				$product['img_src'] = $product['thumbnail_url'];
			}

    }

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

    // 檢查用戶是否有下標
		$tpl->assign('user_bid_count', $this->user_bid_count($userid, $productid));
		$tpl->assign('type', $type);
		$tpl->assign('product', $product);
		$tpl->assign('productid', $productid);
		$tpl->assign('channelid', $channelid);

		if ($type == 'NB'){
			$tpl->set_title('流標商品紀錄');
		}else{
			$tpl->set_title('');
		}

		$meta['title']=$product['name'];
		$meta['description']='';
		$meta['image'] = $product['img_src'];
		$tpl->assign('meta',$meta);
		$tpl->render("bid","detail",true,'',$json);

	}


  public function genBidDetailHtml() {

		global $tpl, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$productid=empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$channelid=empty($_GET['channelid']) ? htmlspecialchars($_POST['channelid']) : htmlspecialchars($_GET['channelid']);
		// 由scheduler 呼叫的
		$bg=empty($_GET['bg']) ? htmlspecialchars($_POST['bg']) : htmlspecialchars($_GET['bg']);

		$ret="OK";

		//成交狀態
		if (empty($type) || $type == 'deal') {
      //商品資料
			$product = $bid->get_info($productid);

      //判斷若得標商品無資料,則直接查詢商品資料
			if (empty($product['productid'])){
				$product = $bid->get_cancelled_info($productid);
			}

			//下標人數&下標金額
			$get_bid_Arr = $bid->get_bid_info($productid);
			$i = 0;

			if(is_array($get_bid_Arr)){
			  foreach($get_bid_Arr as $key => $value){
          if($value['price'] <= $product['price']){
  				  $bid_info[$key] = $get_bid_Arr[$key];
  			  }else{
  				  if($i < 10){
  					  $bid_info[$key] = $get_bid_Arr[$key];
  					  $i++;
  				  }
  			  }
			  }
		  }

		}else{

      //商品資料
			$product = $bid->get_cancelled_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_cancelled_bid_info($productid);

		}

		$html= '<div class="article">'.
           '<!--  成交商品資訊  -->'.
			     '<input id="allbidcount" type="hidden" value="'.count($bid_info).'" >'.
           '<ul class="listsimg-box-group bidDetail">'.
           '<li class="listsimg-box">'.
           '<div class="listsimg-header">';

    if($type == '' || $type == 'deal'){

      $html.= '<div class="d-flex align-items-center">'.
              '<div class="bid-user d-inlineflex align-items-center mr-auto">'.
              '<div class="bid-img">'.
              '<img class="img-fluid" src="'.APP_DIR.'/static/img/bid-success.png">'.
              '</div>'.
              '<div class="bid-userName">';
      if(!empty($product['nickname'])){
        $html.= urldecode($product['nickname']).
                '</div>'.
                '</div>'.
                '<div class="bid-price">'.
                '<span>得標金額</span><span>NT</span><span>'. $product['price'].'</span><span>元</span>'.
                '</div>';
      }else{
        $html.= '無人得標'.
                '</div>'.
                '</div>';
      }
      $html.= '</div>';

    }else{

      $html.= '<div class="d-flex align-items-center">'.
              '<div class="nobid-reason"><span>流標原因：</span><span>'.$product['memo'].'</span></div>'.
              '</div>';

    }

    $html.= '</div>'.
            '<div class="listsimg-contant d-flex align-items-start">'.
            '<div class="listimg_img">'.
            '<i class="d-flex align-items-center">';

  	//商品圖示
    if(!empty($product['thumbnail'])) {
      $img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
  	}elseif(!empty($product['thumbnail_url'])){
  	  $img_src = $product['thumbnail_url'];
  	}

    $html.= '<img src="'.$img_src.'">'.
            '</i>'.
            '</div>'.
            '<div class="listimg-info">'.
            '<p class="pro-name">'.$product['name'].'</p>'.
            '<p class="pro-price">'.
            '<span>官方售價：NT </span><span>'.$product['retail_price'].'</span><span> 元</span>'.
            '</p>'.
            '</div>'.
            '</div>'.
            '<div class="listimg-time d-flex">'.
            '<div class="ml-auto">'.
            '<span>'. $product['insertt'].'</span>'.
            '</div>'.
            '</div>'.
            '</li>'.
            '</ul>';

    //競標資訊
    if(is_array($bid_info)){
      $html.= '<!--  競標資訊  -->'.
              '<ul class="bidList-group">'.
              '<div class="bidList-header d-flex">'.
              '<div class="header-title">下標金額 (元)</div>'.
              '<div class="header-title">下標人數 (人)</div>'.
              '</div>';
      foreach($bid_info as $rk => $rv){
        if ($product['price'] == $rv['price']){
          $html.= '<li class="bidList d-flex listMark">'.
                  '<div class="list-data">'.round($rv['price']).' 元</div>'.
                  '<div class="list-data">'.$rv['num'].' 人</div>'.
                  '</li>';
        }else{
          $html.= '<li class="bidList d-flex">'.
                  '<div class="list-data">'.round($rv['price']).'</div>'.
                  '<div class="list-data">'.$rv['num'].'</div>'.
                  '</li>';
        }
      }
      $html.= '</ul>';
    }

    $html.= '</div><!-- /article -->';

    try {
         // /static/html目錄必須設定為setGid
  			 $fp = fopen('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html', 'w+');
  			 fwrite($fp, $html);
  			 fclose($fp);
    }catch(Exception $e){
  			 error_log($e->getMessage());
  			 $ret=$e->getMessage();
  	}finally{
  	  if($bg=='Y'){
  		  echo $ret;
  		}else{
  		  return $ret;
  		}
  	}

  }


	//下標金標數統計
	public function getBidHistory() {

		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		$productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid		= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$type 		= empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$page 	 	= empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$perpage 	= empty($_POST['perpage']) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);

		//取得商品的相關資料
		$proinfo = $product->get_info($productid);

		//判斷此商品的下標狀態是否在 closed=’Y’ (已結標) 或 closed=’NB’ (流標)
		if (($proinfo['closed'] == 'Y') || ($proinfo['closed'] == 'NB')){

			if ($type == 'A'){
				//取得該商品的使用者下標資料
				$bid_check = $bid->getUserSajaHistory($userid, $productid);
				error_log("[bid/getBidHistory] uid = ".$userid." pid = ".$productid);

				//判斷是否有下標過此商品
				if (!empty($bid_check)) {
					//取得下標金標數統計
					$bid_list = $bid->get_bid_list($productid, $type, $page, $perpage);
				}else{
					$bid_list = array();
				}
			}else{
				$bid_list = $bid->get_bid_list($productid, $type);
			}

			error_log("[bid/getBidHistory] type = ".$type);

			if($json=='Y') {
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = (!empty($bid_list['record'])) ? $bid_list['record'] : array();
				$ret['retObj']['page'] = (!empty($bid_list['page'])) ? $bid_list['page'] : new stdClass;
				echo json_encode($ret);
				exit;
			}else{
				$tpl->assign('bid_list', $bid_list);

				//設定分頁
				if(empty($bid_list['table']['page']) ){
					$page_content = array();
				}else{
					$page_path = $tpl->variables['status']['status']['path'];
					$page_content = $this->set_page($bid_list, $page_path);
				}
				$tpl->assign('page_content', $page_content);
				$tpl->set_title('');
				$tpl->render("bid","getBidHistory",true);
			}

		}else{
			if($json=='Y') {
				$ret=getRetJSONArray(-1,'ERROR','MSG');
				echo json_encode($ret);
				exit;
			}else{
				$tpl->assign('type', $type);
				$tpl->assign('productid', $productid);
				$tpl->set_title('');
				$tpl->render("bid","home",true);
			}
		}

	}


	public function user_bid_count($userid, $productid) {
    global $tpl, $bid, $product;

		  //設定 Action 相關參數
		  set_status($this->controller);

		  if(!empty($userid) && !empty($productid)) {
		    return $bid->user_bid_count($userid, $productid);
		  }
	    return 0;
	}


	/*
	 *	下標清單
	 *	$type					varchar				狀態
	 *	$page					int					頁碼
	 *	$perpage				int					每頁個數
	 *	$productid				int					商品編號
	 *	$channelid				varchar
	 *	$userid					int					會員編號
	 */
	public function bidlist() {

		global $tpl, $bid ;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$page = empty($_GET['p'])? htmlspecialchars($_POST['p']) : htmlspecialchars($_GET['p']);
		$perpage = empty($_GET['perpage'])? htmlspecialchars($_POST['perpage']) : htmlspecialchars($_GET['perpage']);
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$channelid = empty($_GET['channelid']) ? htmlspecialchars($_POST['channelid']) : htmlspecialchars($_GET['channelid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		// 由scheduler 呼叫的
		$bg=empty($_GET['bg']) ? htmlspecialchars($_POST['bg']) : htmlspecialchars($_GET['bg']);

		$ret="OK";

		//判斷是否有登入會員
		// login_required();

		//成交狀態
		if (empty($type) || $type == 'deal') {
			//商品資料
			$product = $bid->get_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_bid_list($productid, 'A', $page, $perpage);
		} elseif ($type == 'NB') {
			//商品資料
			$product = $bid->get_cancelled_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_bid_list($productid, 'A', $page, $perpage);
		} else {
			//商品資料
			$product = $bid->get_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_bid_list($productid, 'A', $page, $perpage);
			error_log("[bid/bidlist] product = ".json_encode($product));
		}

		//設定分頁
		if(empty($bid_info['page']) ) {
			$page_content = array();
		} else {
			$content['table']['record'] = $bid_info['record'];
			$content['table']['page'] = $bid_info['page'];
			$page_path = $tpl->variables['status']['status']['path']."&productid=".$productid."&perpage=".$perpage;
			$page_content = $this->set_page($content, $page_path);
		}

		if ($json=='Y') {
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $product;
			$ret['retObj']['user_bid_count'] = $bid->user_bid_count('', $productid);
			$ret['retObj']['user_bid_max'] = round($bid->user_bid_max('', $productid));

			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('user_bid_count', $bid->user_bid_count('', $productid));
			$tpl->assign('user_bid_max', round($bid->user_bid_max('', $productid)));
			$tpl->assign('type', $type);
			$tpl->assign('productid', $productid);
			$tpl->assign('channelid', $channelid);
			$tpl->assign('product', $product);
			$tpl->assign('bid_info', $bid_info['record']);
			$tpl->assign('page_content', $page_content);

			$meta['title']=$product['name'];
			$meta['description']='';
			if(!empty($product['thumbnail'])) {
				$meta['image'] = BASE_URL."/site/images/site/product/".$product['thumbnail'];
			} elseif (!empty($product['thumbnail_url'])) {
				$meta['image'] = $product['thumbnail_url'];
			}
			$tpl->assign('meta',$meta);

			$tpl->set_title('');
			$tpl->render("bid","detail_all",true);
		}
	}


	/*
	 *	個人競標中商品相關出價資料
	 *	$json	 				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 */
	public function userOnBidProductList() {

		global $tpl,$bid,$product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//判斷是否有登入會員
		login_required();

		if(empty($productid) || empty($userid)){
			return_to('site/bid');
		} else {
			//取得商品資料
			$product = $product->getProductById($productid);
			//取得下標最大金額
			$user_bid_max = $bid->user_bid_max($userid, $productid);
			//會員最後下標時間
			$user_bid_last = $bid->user_bid_last($userid, $productid);

			if ($json=='Y') {
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product;
				$ret['retObj']['user_bid_count'] = $this->user_bid_count($userid, $productid);
				$ret['retObj']['user_bid_max'] = $user_bid_max;
				$ret['retObj']['user_bid_last'] = $user_bid_last;
				echo json_encode($ret);
				exit;
			} else {
				$tpl->assign('user_bid_count', $this->user_bid_count($userid, $productid));
				$tpl->assign('user_bid_max', $user_bid_max);
				$tpl->assign('user_bid_last', $user_bid_last);
				$tpl->assign('product', $product);
				$tpl->set_title('');
				$tpl->render("bid","onbid",true);
			}
		}
	}


	/*
	 *	個人競標中商品出價紀錄
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 */
	public function getUserOnBidList() {

		global $tpl,$bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id']) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		$startprice = empty($_GET['startprice']) ? htmlspecialchars($_POST['startprice']) : htmlspecialchars($_GET['startprice']);

		//判斷是否有登入會員
		login_required();

		if (!empty($productid) && !empty($userid) && !empty($startprice)){
			//下標次數&下標金額
			$bid_info = $bid->get_onbid_list($productid, $userid, $startprice);

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $bid_info['table']['record'];
			// $ret['retObj']['page'] = $bid_info['table']['page'];
		}else{
			$ret=getRetJSONArray(-1,urlencode('資料有誤，請確認資料是否正確 !!'),'MSG');
		}
		echo json_encode($ret);
		exit;
	}


	/*
	 *	全商品出價紀錄
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 *	$startprice				int					開始下標價位
	 */
	public function getBidList() {

		global $tpl,$bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$startprice = empty($_GET['startprice']) ? htmlspecialchars($_POST['startprice']) : htmlspecialchars($_GET['startprice']);

		//判斷是否有登入會員
		// login_required();

		if (!empty($productid) && !empty($startprice)){
			//下標次數&下標金額
			$bid_info = $bid->get_onbid_list($productid, '', $startprice);

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $bid_info['table']['record'];
		}else{
			$ret=getRetJSONArray(-1,urlencode('資料有誤，請確認資料是否正確 !!'),'MSG');
		}
		echo json_encode($ret);
		exit;
	}


	/*
	 *	成功統計相關資料
	 *	$json	 				varchar				瀏覽工具判斷 (Y:APP來源)
   *	$reflash	 		varchar				Redis暫存更新 (Y:更新)
	 */
	public static function getBidAllTotalCount() {

		global $tpl, $bid;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$refresh = empty($_POST['refresh']) ? htmlspecialchars($_GET['refresh']) : htmlspecialchars($_POST['refresh']);

		//設定 Action 相關參數
		set_status($controller);

		//取得成功人數與成功節省金額

		//建立Redis連線
		$redis=getRedis();

		if($redis){//如果redis存在使用redis

			//成功人數
			$rdata = ''; //redis data 暫存預設
			$rkey="bid_user_count"; //設定redis key 名稱
			$rdata = $redis->get($rkey); //取得redis key對映值

			if(!empty($rdata) && $refresh != 'Y'){          //有redis值且不需刷新時用redis值
				$bid_user_count = $rdata;
				$bid_user_count_refreshed ='N';//成功人數cache刷新資料旗標
			}else{                      //沒值或要刷新時重新取值後存入redis
				$bid_user_count = $bid->bid_user_count(); //直接取得成功人數值
				$redis->set($rkey,$bid_user_count); //存入redis
				$bid_user_count_refreshed ='Y';//成功人數cache刷新資料旗標
			}

			//成功節省金額
			$rdata = ''; //redis data 暫存預設
			$rkey="bid_money_count"; //設定redis key 名稱
			$rdata = $redis->get($rkey); //取得redis key對映值

			if(!empty($rdata) && $refresh != 'Y'){         //有redis值且不需刷新時用redis值
				$bid_money_count = $rdata;
				$bid_money_count_refreshed ='N';//成功節省金額cache刷新資料旗標
			}else{                      //沒值或要刷新時重新取值後存入redis
				$bid_money_count = $bid->bid_money_count(); //直接取得成功人數值
				$redis->set($rkey,$bid_money_count); //存入redis
				$bid_money_count_refreshed ='Y';//成功節省金額cache刷新資料旗標
			}

			//輸出使用cache資料旗標
			$cached ='Y';

		}else{ //如果redis不存在直接取值

			//成功人數
			$bid_user_count = $bid->bid_user_count();
			//成功節省金額
			$bid_money_count = $bid->bid_money_count();
			//輸出無使用cache資料旗標
			$cached ='N';

		}
	
		//成功人數固定加110
		$bid_user_count = $bid_user_count + 163333;
		//成功節省金額固定加1700萬
		$bid_money_count = $bid_money_count + 17000000 + 250000000;

		$data['retCode'] = 1;
		$data['retMsg'] = "取得資料 !!";
		$data['retType'] = "LSIT";
		$data['cached'] = $cached;
		$data['bid_user_count_refreshed'] = $bid_user_count_refreshed;
		$data['bid_money_count_refreshed'] = $bid_money_count_refreshed;
		$data['retObj']['bid_user_start_text'] = "已有";
		$data['retObj']['bid_user_count'] = (int)$bid_user_count;
		$data['retObj']['bid_user_end_text'] = "人成功殺價節省";
		$data['retObj']['bid_money_start_text'] = "NT";
		$data['retObj']['bid_money_count'] = (int)$bid_money_count;
		$data['retObj']['bid_money_end_text'] = "";
		$data['retObj']['nowmicrotime'] = microtime(true);
		echo json_encode($data);

	}


	/*
	 *	個人商品相關出價資料
	 *	$json	 				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 *	$type					varchar				結標狀態
	 */
	public function userBidProductList() {

		global $tpl,$bid,$product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);

		//判斷是否有登入會員
		login_required();

		if(empty($productid) || empty($userid)){
			return_to('site/bid');
		} else {
			//取得商品資料
			if ($type == 'NB'){
				$product = $bid->get_cancelled_info($productid);
			}else{
				$product = $bid->get_info($productid);
			}
			//取得下標最大金額
			$user_bid_max = $bid->user_bid_max($userid, $productid);
			//會員最後下標時間
			$user_bid_last = $bid->user_bid_last($userid, $productid);

			if ($json=='Y') {
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product;
				$ret['retObj']['user_bid_count'] = $this->user_bid_count($userid, $productid);
				$ret['retObj']['user_bid_max'] = $user_bid_max;
				$ret['retObj']['user_bid_last'] = $user_bid_last;
				echo json_encode($ret);
				exit;
			} else {
				$tpl->assign('user_bid_count', $this->user_bid_count($userid, $productid));
				$tpl->assign('user_bid_max', $user_bid_max);
				$tpl->assign('user_bid_last', $user_bid_last);
				$tpl->assign('product', $product);
				$tpl->assign('type', $type);
				$tpl->set_title('');
				$tpl->render("bid","bidclose",true);
			}
		}
	}


	/*
	 *	特定商品出價紀錄
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 *	$startprice				int					開始下標價位
	 */
	public function getUserBidList() {

		global $tpl,$bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$startprice = empty($_GET['startprice']) ? htmlspecialchars($_POST['startprice']) : htmlspecialchars($_GET['startprice']);

		//判斷是否有登入會員
		login_required();

		if (!empty($productid) && !empty($startprice)){
			//下標次數&下標金額
			$bid_info = $bid->get_onbid_list($productid, $userid, $startprice);

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $bid_info['table']['record'];
		}else{
			$ret=getRetJSONArray(-1,urlencode('資料有誤，請確認資料是否正確 !!'),'MSG');
		}
		echo json_encode($ret);
		exit;
	}
	
	// 最新成交清單列表
	public  function recently_closed_product_list() {
	         global $bid;
			 set_status($this->controller);
			 $reload = htmlspecialchars($_REQUEST['reload']);
			 if($reload!='Y')
			    $reload='N';
			 $rkey = "recently_closed_product_list";
			 $arr = '';
			 $ret = '';
			 $redis = getRedis();
			 if($reload=='N') {
			    if($redis) {
				   $ret=$redis->get($rkey); 
				   if(!empty($ret)) {
					  return $ret;
				   }
			    }
		     }
			 $arr = $bid->recently_closed_product_list(''); 
			 if(!empty($arr) && is_array($arr)) {
			    $ret = json_encode($arr);
		     } 
		     if($redis) {
			    $redis->set($rkey,$ret);   
		     }
		     return $ret;             
    }

}
