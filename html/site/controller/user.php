<?php
/*
 * User Controller 會員相關
 */

include_once(LIB_DIR."/convertString.ini.php");

class User {

	public $controller = array();
	public $params = array();
  public $id;

	/*
   * Default home page.
   */
	public function home() {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("user","home",true);

	}


	//會員收件資訊
	public function profile_confirm() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$tpl->set_title('');
		$tpl->render("user","profile_confirm",true);

	}


	//會員收件資訊
	public function profile() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		//會員個人資料
		$user_profile = $user->get_user_profile($userid);

		//推薦人資料
		$user_referral = $user->getAffiliateUser($userid);
		if(!empty($user_referral['intro_by']) ){
			$user_profile['intro_by'] = $user_referral['intro_by'];
			$user_referral_profile = $user->get_user_profile($user_referral['intro_by']);
			$user_profile['intro_name'] = empty($user_referral_profile['nickname']) ? '' : " ({$user_referral_profile['nickname']})";
		} else {
			$user_profile['intro_by'] = '';
			$user_profile['intro_name'] = '';
		}


		//國家
		$get_country = $channel->get_country();
		//區域
		$get_region = $channel->get_region();
		//省巿
		$get_province = $channel->get_province();
		//縣區
		$get_channel = $channel->get_channel();

		$place = array(
			'country' => $get_country,
			'region' =>	$get_region,
			'province' => $get_province,
			'channel' => $get_channel
		);

		//會員卡包資料
		$user_extrainfo_category = $user->check_user_extrainfo_category();

		if(is_array($user_extrainfo_category)){
			foreach($user_extrainfo_category as $key => $value){
				if($json!='Y'){
					$user_extrainfo[$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
				}else{
					$user_extrainfo['a'.$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
				}
			}

		}
		//取得會員信用卡清單
		$user_card = $user->getUserCreditCardTokenList($userid);

		//會員認證資料
		$get_auth = $user->validSMSAuth($_SESSION['auth_id']);

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('user_profile', $user_profile);
			$tpl->assign('user_enterprise_rt', $user_enterprise_rt);
			$tpl->assign('countryArr', $get_country);
			$tpl->assign('regionArr', $get_region);
			$tpl->assign('provinceArr', $get_province);
			$tpl->assign('channelArr', $get_channel);
			$tpl->assign('user_extrainfo_category', $user_extrainfo_category);
			$tpl->assign('user_extrainfo', $user_extrainfo);
			$tpl->assign('auth', $get_auth);
			$tpl->set_title('');
			$tpl->render("user","profile",true);
		}else{
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";
			$data['retObj']['data']= (!empty($user_profile)) ? $user_profile : new stdClass;
			$data['retObj']['place']= (!empty($place)) ? $place : new stdClass;
			$data['retObj']['user_extrainfo_category']= (!empty($user_extrainfo_category)) ? $user_extrainfo_category : array();
			$data['retObj']['user_extrainfo']= (!empty($user_extrainfo)) ? $user_extrainfo : new stdClass;
			$data['retObj']['user_card']= (!empty($user_card)) ? $user_card : array();
			echo json_encode($data);
		}

	}


	// 會員修改資訊-暱稱
	public function uname() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$user_profile = $user->get_user_profile($_SESSION['auth_id']);
		$tpl->assign('user_profile', $user_profile);
		$tpl->set_title('');
		$tpl->assign("useview","uname");
		$tpl->render("user","profile_edit",true);

	}


	// 會員修改資訊-分區
	public function uarea() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//區域
		$get_country = $channel->get_country();
		$tpl->assign('countryArr', $get_country);
		$get_region = $channel->get_region();
		$tpl->assign('regionArr', $get_region);
		$get_province = $channel->get_province();
		$tpl->assign('provinceArr', $get_province);
		$get_channel = $channel->get_channel();
		$tpl->assign('channelArr', $get_channel);

		$tpl->set_title('');
		$tpl->assign("useview","uarea");
		$tpl->render("user","profile_edit",true);

	}


	// 會員修改資訊-登錄密碼
	public function upwd() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$tpl->set_title('');
		$tpl->assign("useview","upwd");
		$tpl->render("user","profile_edit",true);

	}


	// 會員修改資訊-兌換密碼
	public function uepwd() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$tpl->set_title('');
		$tpl->assign("useview","uepwd");
		$tpl->render("user","profile_edit",true);

	}


	// 會員修改資訊-小額免密
	public function unopwd() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$user_profile = $user->get_user_profile($_SESSION['auth_id']);
		$tpl->assign('user_profile', $user_profile);
		$tpl->set_title('');
		$tpl->assign("useview","unopwd");
		$tpl->render("user","profile_edit",true);

	}


	// 會員修改資訊-忘記兌換密碼
	public function ufpwd() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//會員認證資料
		$get_auth = $user->validSMSAuth($_SESSION['auth_id']);
		$tpl->assign('auth', $get_auth);

		$tpl->set_title('');
		$tpl->assign("useview","ufpwd");
		$tpl->render("user","profile_edit",true);

	}


	// 會員修改資訊-卡包
	public function upackage() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$user_extrainfo_category = $user->check_user_extrainfo_category();
		$tpl->assign('user_extrainfo_category', $user_extrainfo_category);

		if(is_array($user_extrainfo_category)){
			foreach($user_extrainfo_category as $key => $value){
				$user_extrainfo[$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
			}
			$tpl->assign('user_extrainfo', $user_extrainfo);
		}

		$tpl->set_title('');
		$tpl->assign("useview","upackage");
		$tpl->render("user","profile_edit",true);

	}


	/*
	 *	會員修改資訊-收件人資訊
	 *	$json	 			varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function uaddress() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		$user_profile = $user->get_user_profile($userid);

		$tpl->assign('user_profile', $user_profile);
		$tpl->set_title('');
		$tpl->assign("useview","uaddress");
		$tpl->render("user","profile_edit",true);

	}


	//QQ登入
	public function qqapi() {

		include_once("/var/www/html/site/oauth/QQAPI/qqConnectAPI.php");

		$qc = new QC();
    $qc->qq_login();

	}


	//微博登入
	public function sinaapi() {

		include_once("/var/www/html/site/oauth/SinaAPI/saetv2.ex.class.php");

		$o = new SaeTOAuthV2( WB_AKEY , WB_SKEY );
		$code_url = $o->getAuthorizeURL( WB_CALLBACK_URL ); var_dump($code_url);

		header("Location:{$code_url}");

	}


	//微信登入
	public function weixinapi() {

		include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");

		$options['appid'] = APP_ID;
		$options['appsecret'] = APP_SECRET;
		$scope = 1;

		if(!empty($_REQUEST['scope'])){
		  $scope=$_REQUEST['scope'];
		}

		$state=STATE;
		if(!empty($_REQUEST['url'])){
		  $state=$_REQUEST['url'];
		}

		$u = (isset($_REQUEST['u'])) ? "/?u=".$_REQUEST['u'] : '';
		error_log("[user/weixinapi] u=".$u);

		$state=empty($u)?'':"u=".$u;

		$wxchat = new WeixinChat($options);
		$code_url = $wxchat->redirectGetOauthCode(WX_CALLBACK_URL.$u, $scope, $state);
		error_log("[user/weixinapi] code_url: ".$code_url);
		header("Location:{$code_url}");

	}


	//微信綁定API
	public function weixinbindingapi() {

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");

		$options['appid'] = APP_ID;
		$options['appsecret'] = APP_SECRET;
		$scope = 1;

		$wxchat = new WeixinChat($options);
		$code_url = $wxchat->redirectGetOauthCode(WX_BINDING_URL."/?userid=".$_SESSION['auth_id'], $scope, STATE);

		var_dump($code_url);

		header("Location:{$code_url}");

	}


	// 小程式用: 由code換取openid
  public function getWxUserOpenidByCode() {

		global $user;

		$code=$_REQUEST['code'];

		$url = "https://api.weixin.qq.com/sns/jscode2session?";
		$url.="appid=wx054ee14ea7e1d53b";
		$url.="&secret=463dd4e9be21d1281f9d5f9a2df38600";
		//$url.="appid=wxbd3bb123afa75e56";
		//$url.="&secret=367c4259039bf38a65af9b93d92740ae";
		$url.="&js_code=".$code;
		$url.="&grant_type=authorization_code";

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		// Set so curl_exec returns the result instead of outputting it.
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// Get the response and close the channel.
		$response = curl_exec($ch);
		curl_close($ch);

		error_log("[getWxUserOpenidByCode]:".$response);
		echo $response;
		exit;

	}


	// 小程式用: 取得並更新access_token
    public function getWxLaAccessToken() {

			global $db, $config;

			$db = new mysql($config["db"]);
			$db->connect();
			$table=$db->getQueryRecord("SELECT * FROM saja_channel.saja_weixin_settings WHERE switch='Y' AND name='access_token_LA' order by offtime desc LIMIT 1 ");

			if($table){
			  $restTime=strtotime($table['table']['record'][0]['offtime'])-time();

				error_log("offtime-time=".strtotime($table['table']['record'][0]['offtime'])."-".time()."=".$restTime);

				if($restTime>0){
				  $ret= json_encode(array("access_token"=>$table['table']['record'][0]['code'],
				                          "expires_in"=>$restTime));
				  error_log("[getWxLaAccessToken] reply access_token_LA :".$ret);
				  echo $ret;
				  exit;
				}

			}

			$url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
			$url.="&appid=wx054ee14ea7e1d53b";
			$url.="&secret=463dd4e9be21d1281f9d5f9a2df38600";

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			// Get the response and close the channel.
			$response = curl_exec($ch);
			error_log("[getWxLaAccessToken]: ".$response);
			curl_close($ch);

			$jdata=json_decode($response,true);
			if(!empty($jdata['access_token']) && $jdata['expires_in']>0){
			  $sql = "UPDATE saja_channel.saja_weixin_settings
			             SET code='{$jdata['access_token']}' , ontime=NOW(), offtime=(NOW()+INTERVAL 7170 SECOND)
			           WHERE switch='Y' AND name='access_token_LA' ";
			  error_log("[getWxLaAccessToken] update access_token_LA :".$sql);
			  $db->query($sql);
			}

			echo $response;
			exit;

	}


	public function sendWxLaTemplateMsg() {

		global $user, $config;

		/*
		 {"touser":"oZ1L60LZLIuqvk0k9XjHD1eGAPxU"
		,"template_id":"_Ez-T8fO9d7B1F3bsOtikiGpmIKgcdsx0dV7sGGGEOc"
		,"page":"....."
		,"form_id":"the formId is a mock one"
		,"data":{"keyword1":{"value":"\u66c9\u8fea\u7b52\u4ed4\u7c73\u7cd5","color":"#173177"},"keyword2":{"value":"0.24","color":"#173177"},"keyword3":{"value":"2017-03-01 18:13:19","color":"#173177"},"keyword4":{"value":"1849","color":"#173177"}}
		,"emphasis_keyword":"keyword2.DATA"
		,"access_token":"qRKe2QTm_hQ5sRHEoy4vWtlGrHj9Ox3Z9EztzykRwR6AladcymWlKW_KhuVmv5gz5smduYy3MelvAqx3yE51x-YeDUN-lSfzd9OcF0Bad3QJfLi50W1p4eTwIjAZ63okWIPcAJAGSZ"}
		*/

		error_log("[c/user/sendWxLaTemplageMsg] POST:".json_encode($_POST));

		if(!empty($_POST['touser']) && !empty($_POST['form_id'])
		&& !empty($_POST['access_token']) && !empty($_POST['template_id'])){
		  $context = array (
												'http' => array (
													                'method' => "POST",
																				  'header' => "User-Agent: Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US) \r\n Accept: */*",
																				  'content' => json_encode($_POST)
																			  )
		  );
		  $url='https://api.weixin.qq.com/cgi-bin/message/wxopen/template/send?access_token='.$_POST['access_token'];
			$stream_context = stream_context_create ( $context );
			$data = file_get_contents( $url, FALSE, $stream_context );
			error_log("[sendWxLaTemplateMsg] ret : ".$data);
			return $data;
		}
		exit;

	}


	public function sso_register() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$u = (isset($_REQUEST['u']) ) ? $_REQUEST['u'] : '';
		$tpl->assign('user_src', $u);

		$tpl->assign('gotourl', '/site/');

		// redirect from /wx_auth.php with json data format
		$wx_auth_jdata=$_REQUEST['jdata'];

		error_log("[c/user/sso_register] wx_auth_jdata=".$wx_auth_jdata);

		if(!empty($wx_auth_jdata)){
			$tpl->assign('jdata',$wx_auth_jdata);
			// $arr=json_decode($wx_auth_jdata,true);
			$arr=array();
			$arrPair=explode("|",$wx_auth_jdata);
			if(is_array($arrPair)){
				foreach($arrPair as $v) {
					$arrKV=explode(":",$v);
						if(is_array($arrKV)){
							error_log("[c/sso_register] set ".$arrKV[0]."=".$arrKV[1]);
							$tpl->assign($arrKV[0], $arrKV[1]);
						}
				}
			}
		}
		//
		$tpl->set_title('');
		$tpl->render("user","sso_register",true);

	}


	// 一般註冊
	public function register() {

		global $tpl, $user, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$u = (isset($_GET['u']) ) ? $_GET['u'] : '';
		$tpl->assign('user_src', $u);

		$tpl->set_title('');
		$tpl->render("user","register",true);
		// $tpl->render("mall","qrpage",true);

	}

	// 一般註冊
	public function aregister() {

		global $tpl, $user, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$u = (isset($_GET['u']) ) ? $_GET['u'] : '';
		$tpl->assign('user_src', $u);

		$tpl->set_title('');
		$tpl->render("user","aregister",true);

	}


	// 閃殺註冊
	public function flash_register() {

		global $tpl, $user, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("user","flash_register",true);

	}


	public function uni_register() {

		global $tpl, $user, $config;
		//設定 Action 相關參數
		set_status($this->controller);


		// 推薦人
		$user_src = $_REQUEST['user_src'];
		if(empty($user_src))
		  $user_src = $_REQUEST['u'];

		error_log("[user/uni_register] user_src : ".$user_src);
		$state['user_src']=$user_src;
		$state['callback_url']=BASE_URL.APP_DIR."/user/regLogin/";
		$browserType =  $this->browserType();
		switch($browserType){
		  case 'weixin':
		                $url=BASE_URL.APP_DIR."/oauth/weixin/getFbOauthCode.php?state=".base64_encode(json_encode($state));
		                error_log("[user/uni_register] Weixin auth url :".$url);
		                header("Location:".$url);
		                break;
		  case 'fb' :
		                $url=BASE_URL.APP_DIR."/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
		                error_log("[user/uni_register] FB auth url :".$url);
		                header("Location:".$url);
		                break;
		  case 'line' :
		                $url=BASE_URL.APP_DIR."/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
		                error_log("[user/uni_register] Line auth url :".$url);
		                header("Location:".$url);
		                break;
		  default:
			        break;
		}
    return ;

  }


	// 回傳用戶目前所用的瀏覽媒體
	public function browserType() {

		$agent = strtolower($_SERVER['HTTP_USER_AGENT']);
		error_log("[user/browserType] user agent:".$agent);
		$btype="browser";

		if(strpos($agent, 'micromessenger')>0 ){
		  $btype="weixin";
		}else if (strpos($agent, 'line')>0 ){
		  $btype="line";
		}else if (strpos($agent, 'fban')>0 || strpos($agent, 'fbav')>0 || strpos($agent, 'facebook')>0){
		  $btype="fb";
		}else{
		  $btype="line";
		}
		error_log("[user/browserType] browserType:".$btype);
		return $btype;

	}


  public function regLogin() {

		global $db, $config, $tpl, $usermodel, $broadcast;

		$auth_by	= empty($_POST['auth_by']) ? htmlspecialchars($_GET['auth_by']) : htmlspecialchars($_POST['auth_by']);
		$sso_uid	= empty($_POST['sso_uid']) ? htmlspecialchars($_GET['sso_uid']) : htmlspecialchars($_POST['sso_uid']);
		$state   	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
		$data 		= empty($_POST['data']) ? htmlspecialchars($_GET['data']) : htmlspecialchars($_POST['data']);
		$sso_data 	= json_decode(urldecode(base64_decode($data)), true);

		error_log("[c/user/regLogin] state : ".urldecode(base64_decode($state)));
		$arrState =  json_decode(urldecode(base64_decode($state)), true);
		$user_src=$arrState['user_src'];
		$ts=str_replace(".","",microtime(true));
		$arrSSO = array();
    switch($auth_by){
			case 'line':
								    $arrSSO = array(
								      'auth_by' 		=> $auth_by,
								      'sso_data' 		=> urldecode(base64_decode($data)),
								      'state' 		=> $state,
								      'nickname' 		=> $sso_data['displayName'],
								      'headimgurl'    => $sso_data['pictureUrl'],
								      'sso_name' 		=> "line",
								      'phone'         => "line_".$ts,
								      'type' 			=> "sso",
								      'json'          => "Y",
								      'gender'        => "N/A",
								      'sso_uid' 		=> $sso_uid,
								      'sso_uid2' 		=> "",
								      'user_src'      => $user_src
								    );
								    break;
			case 'fb':
							    $arrSSO = array(
							      'auth_by' 		=> $auth_by,
							      'sso_data' 		=> urldecode(base64_decode($data)),
							      'state' 		=> $state,
							      'nickname' 		=> $sso_data['name'],
							      'headimgurl' => $sso_data['picture']['url'],
							      'sso_name' 		=> "fb",
							      'phone'         => "fb_".$ts,
							      'type' 			=> "sso",
							      'json'          => "Y",
							      'gender'        => "N/A",
							      'sso_uid' 		=> $sso_uid,
							      'sso_uid2' 		=> "",
							      'user_src'      => $user_src
							    );
							    break;
    }
		error_log("[c/user/regLogin] oauth data : ".json_encode($arrSSO));

		$get_user = $broadcast->get_user($sso_uid, $auth_by, '');
		error_log("[c/user/regLogin] get_user : ".json_encode($get_user));
    if(empty($get_user)){

			$url = BASE_URL.APP_DIR."/ajax/user_register.php";
			//設定送出方式-POST
			$stream_options = array(
															'http' => array (
																								'method' => "POST",
																								'content' => json_encode($arrSSO),
																								'header' => "Content-Type:application/json"
																								)
			);
      $context = stream_context_create($stream_options);

      // 送出json內容並取回結果
      $response = file_get_contents($url, false, $context);

      error_log("[c/user/regLogin] response : ".$response);

      // 讀取json內容
      $arr = json_decode($response,true);

			if($arr['retCode'] == 1){
			  $get_user = $broadcast->get_user($sso_uid, $auth_by, '');
			}
    }
    $this->set_login($get_user);
    header("Location:".BASE_URL.APP_DIR."/member");
    return ;

  }


  public function set_login($user) {

		global $db, $config, $usermodel;

		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';

		$query = "SELECT *
								FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							 WHERE `prefixid` = '{$config['default_prefix_id']}'
								 AND `userid` = '{$user['userid']}'
								 AND `switch` =  'Y'
		";
		error_log("[c/user/set_login]Query User : ".$query);
		$table = $db->getQueryRecord($query);

		$user['profile'] = $table['table']['record'][0];
		$_SESSION['user'] = $user;

		// Set session and cookie information.
		$_SESSION['auth_id'] = $user['userid'];
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = md5($user['userid'] . $user['name']);
		setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", md5($user['userid'] . $user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);

		// Set local publiciables with the user's info.
		if(isset($usermodel)){
			$usermodel->user_id = $user['userid'];
			$usermodel->name = $user['profile']['nickname'];
			$usermodel->email = '';
			$usermodel->ok = true;
			$usermodel->is_logged = true;
		}
    return ;

	}


	/*
	 * APP & Weixin & 重新發送的手機驗證用
	 * $phone		varchar		手機號碼
	 * $userid		int			會員編號
	 * @ App & "重新發送" 只傳phone值, weixin會傳送phone值及userid值
	 */
	public function sendPhoneChkcode() {

		global $user, $config;

		$ret = null;
		$phone = empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$user_type=empty($_POST['user_type']) ? htmlspecialchars($_GET['user_type']) : htmlspecialchars($_POST['user_type']);
		$client_type=empty($_POST['client_type']) ? htmlspecialchars($_GET['client_type']) : htmlspecialchars($_POST['client_type']);

		if(empty($user_type))
		  $user_type="P";
		error_log("[c/user/sendPhoneChkcode] receive data :".json_encode($_POST));

		//判斷手機號是否有值
		if(empty($phone)){
			$ret = getRetJSONArray(1,"手機號碼不能為空白 !!",'-2');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = $phone;
			error_log(json_encode($ret));
			echo json_encode($ret);
			exit;
		}

		//判斷手機號格式是否正確
		if(!preg_match("/^[0-9]{10,15}$/", $phone)){
			$ret = getRetJSONArray(1,"手機號碼必須為數字 !!",'-3');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = $phone;
			error_log(json_encode($ret));
			echo json_encode($ret);
			exit;
		}

		// 檢查該手機號是否已被使用
		$user_data=$user->get_user('',$phone,$user_type);
		if(is_array($user_data) && $user_data && $user_data[0]['userid']>0){
			// 手機號已驗證通過-彈回
			$ret = getRetJSONArray(1,"此手機號碼已驗證過!",'-4');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = "";
			error_log(json_encode($ret));
			echo json_encode($ret);
			exit;
		}else if($user_data && $user_data['userid']>0){
      // 手機號已驗證通過-彈回
			$ret = getRetJSONArray(1,"此手機號碼已驗證過!!",'-5');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = "";
			error_log(json_encode($ret));
			echo json_encode($ret);
			exit;
    }

		//產生手機驗證碼
		$shuffle = get_shuffle();
		$code = substr($shuffle, 0, 6);

		//判斷手機格式長度
		if(strlen($phone)==10 && strpos($phone,"0")===0){
			if($client_type=='WX_LA'){
			  // 從微信小程式來
			  $msg = iconv("UTF-8","big5", "[殺價王]手機驗證碼為:".$code);
			}else{
			  $msg = iconv("UTF-8","big5",$code.":手機驗證碼[殺價王].");
			}
		}else if(strlen($phone)>10){
		  if($client_type=='WX_LA'){
				// 從微信小程式來
				$msg = iconv("UTF-8","gb2312","[殺價王]手機驗證碼為:".$code);
			}else{
				// 從其他地方來
				$msg = iconv("UTF-8","gb2312",$code.":手機驗證碼[殺價王].");
			}
		}

		//設定查詢條件陣列
		$arrUpd=array();
		$arrCond=array();

		// 判斷是否有會員編號,如果未傳入　則是新註冊時(包括重新發送)的手機驗證
		if(empty($userid)){
			// 未註冊用戶  做手機驗證
			$arrCond['phone']=$phone;
			$arrCond['user_type']=$user_type;
			$sms_auth_data=$user->getSmsAuthData($arrCond);

			if($sms_auth_data && $sms_auth_data[0]['authid']>0){

			  // 有$phone的驗證資料=> 不管是否已驗證通過　都改code重新發送
				$arrUpd['code']=$code;
				$arrUpd['verified']='N';

				//更新手機驗證資料
				$user->updateSmsAuthData($arrUpd,$arrCond);

				// 發送簡訊(09XXXXXXXX 台灣, 其他->中國)
				sendSMS($phone, $msg);

				$ret = getRetJSONArray(1,"已發送驗證碼至您的手機!",'1');
				$ret['retObj']=array();
				$ret['retObj']['phone']=$phone;
				error_log("[c/user/sendPhoneChkcode] Resent code : ".json_encode($ret));
				echo json_encode($ret);
				exit;
		  }else{
        $arrUpd['phone']=$phone;
				$arrUpd['code']=$code;
				$arrUpd['user_type']=$userid;

				//新增手機驗證資料
				$user->createSmsAuthData($arrUpd);

				// 發送簡訊
				sendSMS($phone, $msg);

				$ret=getRetJSONArray(1,"已發送驗證碼至您的手機!!",'1');
				$ret['retObj']=array();
				$ret['retObj']['phone'] = $phone;
				error_log("[c/user/sendPhoneChkcode] Sent New code : ".json_encode($ret));
				echo json_encode($ret);
				exit;
		  }

		}else if(!empty($userid)){
	    // 已註冊用戶 做手機驗證
	    // $arrCond['phone'] = $phone;
	    $arrCond['user_type'] = $user_type;
			$arrCond['userid'] = $userid;
			$sms_auth_data=$user->getSmsAuthData($arrCond);

			// 重發手機驗證碼
			if($sms_auth_data && $sms_auth_data[0]['authid']>0){
			  // 有$userid的驗證資料=> 不管是否已驗證通過(phone是數字或含文字)
				// 都改code重新發送
				$arrUpd['phone']=$phone;
				$arrUpd['code']=$code;
				$arrUpd['verified']='N';

				//更新手機驗證資料
				$user->updateSmsAuthData($arrUpd,$arrCond);

				// 發送簡訊(09XXXXXXXX 台灣, 其他->中國)
				sendSMS($phone, $msg);

				$ret = getRetJSONArray(1,"已重發驗證碼至您的手機 !",'1');
				$ret['retObj']=array();
				$ret['retObj']['phone'] = $phone;
				error_log("[c/user/sendPhoneChkcode] Resent code : ".json_encode($ret));
				echo json_encode($ret);
				exit;
			}else{
			  // 沒有$phone的驗證資料=> 第一次驗證 新建一筆驗證資料並發送
				//驗証碼
     	  $arrUpd['phone']=$phone;
				$arrUpd['code']=$code;
				$arrUpd['userid']=$userid;
				$arrUpd['user_type']=$user_type;
				//新增手機驗證資料
				$user->createSmsAuthData($arrUpd);

				// 發送簡訊
				sendSMS($phone, $msg);

				$ret=getRetJSONArray(1,"已發送驗證碼至您的手機!!",'1');
				$ret['retObj']=array();
				$ret['retObj']['phone'] = $phone;
				error_log("[c/user/sendPhoneChkcode] Sent New code : ".json_encode($ret));
				echo json_encode($ret);
				exit;
			}
		}

  }


	/*
	 * 確認手機驗證碼是否正確
	 * $phone		varchar		手機號碼
	 * $chkcode		varchar		驗證碼
	 * $secretid	varchar		檢查碼
	 * $userid		int			會員編號
	 */
	public function verifyPhoneChkcode() {

	  global $user, $config;
		$ret = null;
		$phone = $_POST['phone'];
		$chkcode = $_POST['chkcode'];
		$secretid = $_POST['secretid'];
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$client_type=empty($_POST['client_type']) ? htmlspecialchars($_SESSION['client_type']) : htmlspecialchars($_POST['client_type']);

		//檢查手機號碼是否有值
		if(empty($phone)){
			$ret = getRetJSONArray(1,"手機號碼不能為空白 !!",'-1');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = "";
			error_log("[c/user/veriftPhoneChkcode] err:".json_encode($ret));
			echo json_encode($ret);
			exit;
		}

		//檢查手機號碼格式
		if(!preg_match("/^[0-9]{10,15}$/", $phone)){
			$ret = getRetJSONArray(1,"手機號碼必須為數字 !!",'MSG');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = $phone;
			error_log("[c/user/veriftPhoneChkcode] err:".json_encode($ret));
			echo json_encode($ret);
			exit;
		}

		//檢查驗證碼是否有值
		if(empty($chkcode)){
			$ret = getRetJSONArray(1,"驗證碼不能為空白 !!",'-3');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = $phone;
			error_log("[c/user/veriftPhoneChkcode] err:".json_encode($ret));
			echo json_encode($ret);
			exit;
		}

		//檢查驗證碼格式
		if(!preg_match("/^[0-9]{6}$/", $chkcode)){
			$ret = getRetJSONArray(1,"驗證碼必須為數字 !!",'-4');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = $phone;
			error_log("[c/user/veriftPhoneChkcode] err:".json_encode($ret));
			echo json_encode($ret);
			exit;
		}

		//確認檢查碼
		if(MD5($phone."|".$chkcode)!=$secretid){
			$ret = getRetJSONArray(1,"檢核資料異常 !!",'-5');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = $phone;
			error_log("[c/user/veriftPhoneChkcode] err:".json_encode($ret));
			echo json_encode($ret);
			exit;
		}

		//組合查詢條件
		$arrCond = array("phone"=>$phone, "code"=>$chkcode);
		//取得是否有相關的驗證資料
		$data = $user->getSmsAuthData($arrCond);
		error_log("[c/user/veriftPhoneChkcode]:".json_encode($data));

		if($data){

			if(!empty($userid)){
				// 表示是以微信方式註冊, saja_user.name不是手機號的,
				// 驗證通過後要把saja_user.name改為手機號
				// 20170222 saja_user登入和兌換密碼都改成手機末六碼
		    $cvtr=new convertString();
		    $expasswd=$cvtr->strEncode(substr($phone,-6), $config['encode_key']);
		    $passwd=$expasswd;
		    $userUpd=array("name"=>$phone, "passwd"=>$passwd, "exchangepasswd"=>$expasswd);
		    $userCond=array("userid"=>$userid);
		    $r1=$user->updUserData($userUpd,$userCond);

				//修改使用者的聯絡用手機號
				$r2=$user->updUserProfileData($phone, $userid);
			}

			$arrUpd = array("verified"=>"Y");

			if(!empty($userid)){
				$arrUpd['userid']=$userid;
			}

			//更新手機驗證資料
			$upd = $user->updateSmsAuthData($arrUpd,$arrCond);

			if($upd){
				$ret=getRetJSONArray(1,"OK","1");
				$ret['retObj']=array();
				$ret['retObj']['phone'] = $phone;
				$ret['retObj']['chkcode'] = $chkcode;
				$ret['retObj']['secretid'] = md5($phone."|".$chkcode);
			}else{
				$ret = getRetJSONArray(1,"資料異動失敗 !!",'-6');
				$ret['retObj']=array();
			  $ret['retObj']['phone'] = $phone;
			}

		}else{
			$ret = getRetJSONArray(1,"驗證碼錯誤 !!",'-7');
			$ret['retObj']=array();
			$ret['retObj']['phone'] = $phone;
		}
		$r = json_encode($ret);
		error_log("[c/user/verifyPhoneChkcode]".$r);
		echo ($r);
		exit;

	}


	//昵稱 手機檢查
	public function chk_nick_phone() {

		global $db, $config;

		$r['err'] = '';

		if(empty($_POST['phone'])){
			//'請填寫手機號碼'
			// -10022
			$r['err'] = 3;
		}else{

			$db2 = new mysql($config["db2"]);
			$db2->connect();

			$query = "SELECT u.*, usa.code
									FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile` u
			 LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa ON
												u.prefixid = usa.prefixid
												AND u.userid = usa.userid
												AND usa.switch = 'Y'
								 WHERE
												u.`prefixid` = '{$config['default_prefix_id']}'
												AND u.`phone` = '{$_POST['phone']}'
												AND u.switch = 'Y'
			";
		  $table = $db2->getQueryRecord($query);

			if(empty($table['table']['record']) ){
				//'昵稱或手機不正確'
				// -10023
				$r['err'] = 4;
			}else{
				$uid = $table['table']['record'][0]['userid'];
				$code = $table['table']['record'][0]['code'];
				$user_phone = $table['table']['record'][0]['phone'];
				$user_phone_0 = substr($user_phone, 0, 1);
				$user_phone_1 = substr($user_phone, 1, 1);

				if($user_phone_0=='0' && $user_phone_1=='9'){
					$phone = $user_phone;
					$area = 1;
				}else{
					$phone = $user_phone;
					$area = 2;
				}

				if(! $this->mk_pass($uid, $phone, $code, "password", $area) ){
					//'此昵稱或手機不正確'
					// -10024
					$r['err'] = 5;
				}
			}
		}
		return $r;

	}


	public function forget() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("user","forget",true);

	}


	public function aforget() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("user","aforget",true);

	}

	public function forgetnickname() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("user","forgetnickname",true);
	}


	/*
	 *	殺手榜
	 *	$type		int		排序方式 1 (依中標次數降冪) / 2 (依中標時間降冪排列)
	 *	$page		int		頁碼
	 *	$perpage	int		分頁筆數
	 */
	public function getAssassinList() {

		// TodoList
		global $tpl, $user;
		//設定 Action 相關參數
		set_status($this->controller);

		$type	 = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$page 	 = empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$perpage = empty($_POST['perpage']) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);

		if(empty($perpage))
		  $perpage=20;

		if(empty($page))
		  $page=1;

		if($page>3)
		  $page=3;

		$assassin_list = $user->getAssassinList($type, ($page-1)*$perpage, $perpage);

		$ret = getRetJSONArray(1,'OK','LIST');
		$ret['retObj']['data'] = (!empty($assassin_list['record'])) ? $assassin_list['record'] : array();
		$ret['retObj']['page'] = (!empty($assassin_list['page'])) ? $assassin_list['page'] : new stdClass;

		echo json_encode($ret);
		exit;

	}


	// 地區選單List
	public function getChannelList() {

		$LANG=strtolower($_POST['lang']);
		error_log("[c/getChannelList] lang:".$LANG);

		$ret=getRetJSONArray(1,'OK',"LIST");
		$ret['lang']=$LANG;

		if(empty($LANG) || $LANG=='zh_cn'){
		  // [channelid, Cname, Ename, Longitude, Latitude]
			$area=array(
									 array("channelid"=>"1","cname"=>"上海","ename"=>"Shanghai","long"=>"121.26","lat"=>"31.12")
									,array("channelid"=>"2","cname"=>"北京","ename"=>"Beijing","long"=>"116.28","lat"=>"39.54")
									,array("channelid"=>"52","cname"=>"廣州","ename"=>"Guangzhou","long"=>"113.18","lat"=>"23.10")
									,array("channelid"=>"37","cname"=>"重慶","ename"=>"Chungking","long"=>"106.33","lat"=>"29.33")
									,array("channelid"=>"48","cname"=>"深圳","ename"=>"Shenzhen","long"=>"114.05","lat"=>"22.32")
									,array("channelid"=>"57","cname"=>"成都","ename"=>"Chengdu","long"=>"104.04","lat"=>"30.39")
								 );
		}else if($LANG=='zh_tw'){
			$area=array(
									 array("channelid"=>"1","cname"=>"上海","ename"=>"Shanghai","long"=>"121.26","lat"=>"31.12")
									,array("channelid"=>"2","cname"=>"北京","ename"=>"Beijing","long"=>"116.28","lat"=>"39.54")
									,array("channelid"=>"52","cname"=>"廣州","ename"=>"Guangzhou","long"=>"113.18","lat"=>"23.10")
									,array("channelid"=>"37","cname"=>"重慶","ename"=>"Chungking","long"=>"106.33","lat"=>"29.33")
									,array("channelid"=>"48","cname"=>"深圳","ename"=>"Shenzhen","long"=>"114.05","lat"=>"22.32")
									,array("channelid"=>"57","cname"=>"成都","ename"=>"Chengdu","long"=>"104.04","lat"=>"30.39")
								 );
		}

		if($area){
		  $ret['retCode']=1;
		  $ret['retObj']=$area;
		}else{
		  $ret['retCode']=0;
		  $ret['retMsg']='No Data Found !!';
		}
		echo json_encode($arr);
    exit;

	}


	// 設定收藏/取消收藏
	public function setCollectProd() {

		global $user;

		login_required();

		$userid=$_POST['auth_id'];
		$productid=$_POST['productid'];
		$prodtype=$_POST['prod_type'];
		$switch=$_POST['switch'];
		$ret=null;

		if(empty($userid)){
		  $ret=getRetJSONArray(-1,'EMPTY_USERID','MSG');
		  echo json_encode($ret);
		  exit;
		}

		if(empty($productid)){
		  $ret=getRetJSONArray(-2,'EMPTY_PRODUCTID','MSG');
		  echo json_encode($ret);
		  exit;
		}

		if(empty($prodtype)){
		  $ret=getRetJSONArray(-3,'EMPTY_PROD_TYPE','MSG');
		  echo json_encode($ret);
      exit;
		}

		if(empty($switch)){
		  $ret=getRetJSONArray(-4,'EMPTY_COLLECT_STATUS','MSG');
		  echo json_encode($ret);
		  exit;
		}

		$data=$user->getUserCollectData($userid, $productid, $prodtype,'');
		error_log($data[0]['ucrid']);

		$r=false;
		if($data[0]['ucrid']>0){
		  $r=$user->updateUserCollectData($switch, $data[0]['ucrid']);
		}else{
		  $r=$user->createUserCollectData($userid, $productid, $prodtype, $switch);
		}

		if($r==true || $r==1 ){
		  $ret=getRetJSONArray(1,'OK','JSON');
		  $data=$user->getUserCollectData($userid, $productid, $prodtype, $switch);
		  $ret['retObj']=$data[0];
		}else{
		  $ret=getRetJSONArray(-1,'UPDATE FAILED!!','MSG');
		}
	  echo json_encode($ret);
		exit;

	}


	// 取得收藏清單
	public function getCollectProdList() {

		global $user;

		login_required();

		$ret=null;
		$userid=$_POST['auth_id'];
		$prodtype=$_POST['prod_type'];
		$switch=$_POST['switch'];

		if(empty($userid)){
			$ret=getRetJSONArray(-1,'EMPTY_USERID','MSG');
			echo json_encode($ret);
			exit;
		}

		if(empty($prodtype)){
		  $ret=getRetJSONArray(-3,'EMPTY_PROD_TYPE','MSG');
		  echo json_encode($ret);
		  exit;
		}

		if(empty($switch)){
		  $switch='Y';
		}

		$ret=getRetJSONArray(1,'OK','LIST');
		$list = $user->getCollectProdList($userid, '', $prodtype, $switch);

		if($list){

			for($idx=0; $idx< count($list['record']); ++$idx){
				$p = $list['record'][$idx];
				$list['record'][$idx]['offtime'] = (int)$p['offtime'];
			}

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $list['record'];
			$ret['retObj']['page'] = $list['page'];

		}
		$x=json_encode($ret);
		error_log("[c/getCollectProdList] : ".$x);
		echo $x;
		exit;

	}


  /*
	 *	登入頁選項功能判斷
	 */
	public function login() {

		global $tpl, $user, $config;

		// 設定 Action 相關參數
		set_status($this->controller);

		// 回傳用戶目前所用的瀏覽媒體
		$agent = strtolower($_SERVER['HTTP_USER_AGENT']);

    $btype = "browser";
		$showtype = empty($_POST['showtype']) ? htmlspecialchars($_GET['showtype']) : $_POST['showtype'];

		if(empty($showtype)){
			if(strpos($agent, 'micromessenger')>0 ){
				$btype="weixin";
			}else if (strpos($agent, 'line')>0 ){
				$btype="line";
			}else if (strpos($agent, 'fban')>0 || strpos($agent, 'fbav')>0 || strpos($agent, 'facebook')>0){
				$btype="fb";
			}else{
				$btype="";
			}
		}else{
			$btype = $showtype;
		}

		$line = 'N';
		$fb = 'N';
		$weixin = 'N';

		switch($btype){
			case 'line':
									$line = 'Y';
									break;
			case 'fb':
									$fb = 'Y';
									break;
			case 'weixin':
										$weixin = 'Y';
										break;
			case 'noline':
										$fb = 'Y';
										$weixin = 'Y';
										break;
			case 'nofb':
									$line = 'Y';
									$weixin = 'Y';
									break;
			case 'noweixin':
											$line = 'Y';
											$fb = 'Y';
											break;
			default :
								$fb = 'Y';
								$line = 'Y';
								$weixin = 'Y';
								break;
		}

		$ret = getRetJSONArray(1,'OK','JSON');
		$ret['retObj'] = array();
		$ret['retObj']['fb'] = $fb;
		$ret['retObj']['line'] = $line;
		$ret['retObj']['weixin'] = $weixin;
        // 暫時關掉關掉weixin登入
        // $ret['retObj']['weixin'] = 'N';
		echo json_encode($ret);
		exit;

	}


	/*
	 *	修改個人資料-頭像修改
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$pic		varchar		圖片
 	 */
	public function setHeadImg() {

		global $tpl, $user, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_POST['auth_id']) ? htmlspecialchars($_GET['userid']) : htmlspecialchars($_POST['auth_id']);
		$pic 		= empty($_POST['pic']) ? htmlspecialchars($_GET['pic']) : $_POST['pic'];

		//判斷資料是存在
		if((!empty($userid))){

			//判斷圖片資料是否存在
			if(!empty($pic)){

				$image = base64_decode($pic);
				$subName = 'jpg';//副檔名
				$picname = $userid.'-'.time().'.'.$subName;

				//上傳圖片
				$file_path = $config["path_site"]."/images/headimgs/";
				$file_path = $file_path . basename($picname);

				$pic = str_replace('data:image/png;base64,', '', $pic);
				$pic = str_replace(' ', '+', $pic);
				$data = base64_decode($pic);
				$success = file_put_contents($file_path, $data);

				//頭像修改資料
				$updata = $user->updateHeadImg($userid, $picname);

			}

			//取得用戶相關資料
			$userdata = $user->get_user_profile($userid);

			//判斷是否為網頁介面
			if($json != 'Y'){
				$this->home();
			}else{
				$ret=getRetJSONArray(1,'OK','JSON');
				$ret['retObj']=array();
				$ret['retObj']['fname'] = $picname;
				$ret['retObj']['thumbnail_url'] = empty($userdata['thumbnail_url']) ? BASE_URL.APP_DIR."/images/headimgs/".$userdata['thumbnail_file'] : $userdata['thumbnail_url'];
				$ret['retObj']['userid'] = $userid;
				echo json_encode($ret);
				exit;
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('errmsg', '資料有缺請檢查!!');
				$tpl->set_title('');
				$tpl->render("user","home",true);
			}else{
				$ret=getRetJSONArray(-1,'error','MSG');
				echo json_encode($data);
				exit;
			}

		}

	}

	public function setPersionID(){
		global $tpl, $user, $config;
		$ret=array();
		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$personId 	= empty($_POST['personId']) ? htmlspecialchars($_GET['personId']) : htmlspecialchars($_POST['personId']);
		$applyYyymmdd 	= empty($_POST['applyYyymmdd']) ? htmlspecialchars($_GET['applyYyymmdd']) : htmlspecialchars($_POST['applyYyymmdd']);
		$applyCode 	= empty($_POST['applyCode']) ? htmlspecialchars($_GET['applyCode']) : htmlspecialchars($_POST['applyCode']);
		$issueSiteId 	= empty($_POST['issueSiteId']) ? htmlspecialchars($_GET['issueSiteId']) : htmlspecialchars($_POST['issueSiteId']);
		$_ret=0;
		$err_msg=array();
		if (!(id_card($personId))){
			$_ret=-1;
			array_push($err_msg,"無效的身分證字號");
		}
		if (!((is_numeric($applyYyymmdd))&&(strlen($applyYyymmdd)==7))){
			$_ret=-2;
			array_push($err_msg,"無效的證件日期");
		}
		if (! (in_array($applyCode, array('0','1','2','3','4')))){
			$_ret=-3;
			array_push($err_msg,"無效的證件狀態");
		}
		if (! (in_array($issueSiteId, array('10001','10002','10003','10004','10005','10006','10007','10008','10009','10010','10011','10012','10013','10014','10015','10016','10017','10018','10020','09007','09020','63000','64000','65000','66000','67000','68000')))){
			$_ret=-4;
			array_push($err_msg,"無效的發證區");
		}

        $fields = array(
          'personId'  => $personId,
          'applyYyymmdd'    => $applyYyymmdd,
          'applyCode'        => $applyCode,
          'issueSiteId'        => $issueSiteId
        );
		//修改會員名稱
		if ($_ret==0){
			if ($user->check_id_verified($personId)){
				$ret['retCode']=1;
				$ret['action_list']['type']=5;
				$ret['action_list']['msg']='此身份證號碼已驗證過';
				$ret['action_list']['page']=$target_page;
			}else{
				$result=json_decode(id_verify($personId,$applyYyymmdd,$applyCode,$issueSiteId),true);
				if (empty($result['responseData']['checkIdCardApply'])){
					$_ret=0;
				}else{
					$_ret=$result['responseData']['checkIdCardApply'];
					if ($_ret=='1') {
						$user->set_idnum($personId,$userid);
					}
				}
				$target_page=0;
				switch($_ret){
					case '0':
						$ret['msg'] = '伺服器發生問題，請連絡客服人員' ;
						break;
					case '1':
						$ret['msg'] = '身份證字號驗證成功';
						$target_page=10;
						break;
					case '2':
						$ret['msg'] = '身分證字號 '.$personId.' 目前驗證資料錯誤次數已達 1 次，請確認您所輸入的欄位。' ;
						break;
					case '3':
						$ret['msg'] = '身分證字號 '.$personId.' 目前驗證資料錯誤次數已達 2 次，請確認您所輸入的欄位。' ;
						break;
					case '4':
					case '5':
					case '6':
					case '7':
					case '8':
						$ret['msg'] = '身分證字號 '.$personId.' 目前驗證資料錯誤次數已達 3 次，請連絡客服人員' ;
						break;
				}
				$ret['retCode']=1;
				$ret['action_list']['type']=5;
				$ret['action_list']['msg']=$ret['msg'];
				$ret['action_list']['page']=$target_page;
			}
		}else{
			$ret['retCode']=1;
			$ret['action_list']['type']=5;
			$ret['action_list']['msg']=implode(",", $err_msg);
			$ret['action_list']['page']=0;
		}
		die(json_encode($ret));
	}

	// 取得用戶等級及經驗值
	// Add By Thomas 20160407
	public function getUserLvlExpts() {

		global $user;
		login_required();
		$ret=null;
		$userid=$_POST['auth_id'];

		if(empty($userid)){
		  $ret=getRetJSONArray(-1,'EMPTY USERID','MSG');
		  echo json_encode($ret);
		  exit;
		}

		$get_level_pts=$user->getUserLvlExpts($userid);

		if($get_level_pts){
		  $ret=getRetJSONArray(1,'OK','JSON');
		  $ret['retObj']=$get_level_pts;
		}else{
			$ret=getRetJSONArray(1,'OK','JSON');
		  $ret['retObj']=array("userid"=>$userid, "total_pts"=>"0","level"=>"0","mdlid"=>"0","title"=>"");
		}
		echo json_encode($ret);
		exit;

	}


	// 修改卡包資訊
	public function updCardPackInfo() {

		global $config, $user;
		login_required();

		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$ueid=empty($_POST['ueid'])?$_GET['ueid']:$_POST['ueid'];
		$uecid=empty($_POST['uecid'])?$_GET['uecid']:$_POST['uecid'];

		if(empty($userid)){
		  echo json_encode(getRetJSONArray(-1,'會員編號不可為空 !!','JSON'));
		  exit;
		}

		$arrInfo=array();
		$arrInfo['switch']=empty($_POST['switch'])?$_GET['switch']:$_POST['switch'];
		$arrInfo['seq']=empty($_POST['seq'])?$_GET['seq']:$_POST['seq'];
		$arrInfo['uecid']=$uecid;
		$arrInfo['userid']=$userid;

		for($idx=1; $idx<=8;++$idx){
		  $arrInfo['field'.$idx.'name']=$_POST['field'.$idx.'name'];
		}

		$ret=null;
		if(!empty($ueid)){
		  // 如果有$ueid=> 修改
			$arrCond=array();
			$arrCond['uecid']=$uecid;
			$arrCond['userid']=$userid;
			$arrCond['ueid']=$ueid;
		  $r=$user->updCardPackInfo($arrCond,$arrInfo);

			if($r==1){
			  $ret=getRetJSONArray(1,'OK','JSON');
			}else{
			  echo json_encode(getRetJSONArray($r,'卡包修改失敗 !!','JSON'));
			}

		  $ret['retObj']=array("ueid"=>$ueid);
		  echo json_encode($ret);
		}else{
		  // 如果沒有$ueid=>新增,回傳$ueid
			if(empty($uecid)){
			  echo json_encode(getRetJSONArray(-2,'卡包類別不可為空 !!','JSON'));
			  exit;
			}
			$ueid=$user->addCardPackInfo($arrInfo);
			if($ueid>0){
			  $ret=getRetJSONArray(1,'OK','JSON');
			  $ret['retObj']=array("ueid"=>$ueid);
			  echo json_encode($ret);
			}else{
			  $ret=getRetJSONArray($r,'卡包添加失敗 !!','JSON');
			  $ret['retObj']=array("ueid"=>"");
			  echo json_encode($ret);
			}
		}
		exit;

	}


	// 取得User中標商品
	public function getUserAwardProdList() {

		global $config, $user;

		login_required();

		$json  = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$productid= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : $_POST['productid'];

		$get_list=$user->getUserAwardProdList($userid, $productid);

		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');

			if($get_list){
			  $ret['retObj']['data']=$get_list;
			}

		}else{

		  if($get_list){
		    $ret=$get_list;
		  }

		}
		echo json_encode($ret);
		exit;

	}


	// 附近, 充值送...等等活動
	public function event() {

		global $tpl, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$json  = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$type= empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$lat= empty($_POST['lat']) ? htmlspecialchars($_GET['lat']) : htmlspecialchars($_POST['lat']);
		$lng= empty($_POST['lng']) ? htmlspecialchars($_GET['lng']) : htmlspecialchars($_POST['lng']);

		error_log("[c/user/event]=> ".json_encode($_POST));
		echo json_encode($_POST);

	}


	/*
	 *	取得我的殺友清單
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid			int			會員編號
	 */
	public function getIntroList() {

		global $config, $user;

		//判斷是否登入
		login_required();

		$json		= empty($_POST['json']) ? $_GET['json'] : $_POST['json'];
		$userid		= empty($_POST['userid']) ? $_GET['userid'] : $_POST['userid'];
		$nickname  	= empty($_POST['nickname']) ? htmlspecialchars($_GET['nickname']) : htmlspecialchars($_POST['nickname']);
		$page 	 	= empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$perpage 	= empty($_POST['perpage']) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);

		error_log("[c/getIntroList]: nickname :".$nickname);

		if($nickname=='*')
		    $nickname='';

		if(!empty($userid)){
			//取得殺友清單
			$friend_list = $user->getFriendsList($userid, $nickname, $page, $perpage);

			$ret = getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = (!empty($friend_list['record'])) ? $friend_list['record'] : array();
			$ret['retObj']['page'] = (!empty($friend_list['page'])) ? $friend_list['page'] : new stdClass;
		}else{
			$ret=getRetJSONArray(-1,'會員編號錯誤','MSG');
		}
		echo json_encode($ret);
		exit;

	}


	/*
	 *	商品訂單資料
	 *	$json		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$orderid	int			訂單編號
	 *	$type		varchar		訂單類別
	 */
	public function getOrderDetail() {

		// TodoList
		global $tpl, $user;
		//設定 Action 相關參數
		set_status($this->controller);

		$json	 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$orderid 	= empty($_POST['orderid']) ? htmlspecialchars($_GET['orderid']) : htmlspecialchars($_POST['orderid']);
		$type 		= empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);

		if(!empty($orderid)){

			$order = $user->getOrderDetail($orderid, $type);

			$ret = getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = (!empty($order)) ? $order : array();

		}else{
			$ret=getRetJSONArray(-1,'訂單編號','MSG');
		}
		echo json_encode($ret);
		exit;

	}


	public function getPhoneAddress($phone=''){
		global $config, $helpers;

		if(empty($phone))
		  $phone=$_REQUEST['phone'];

		if(empty($phone)){
			echo json_encode(getRetJSONArray(-1,'EMPTY_PHONE_NUMBER','MSG'));
			return;
		}

		$arr=getPhoneAddress($phone);

		if($arr['retCode']=='1'){
			$ret=getRetJSONArray($arr['retCode'],$arr['retMsg'],'JSON');
			$ret['retObj']=$arr;
		}else{
		  $ret=getRetJSONArray($arr['retCode'],$arr['retMsg'],'MSG');
		}
		echo json_encode($ret);
		exit;

	}


	/*
	 *	修改會員頭像
	 *	$userid				int					會員編號
	 *	$pic				varchar				圖片
 	 */
	public function edit_user_logo() {

		global $tpl, $user, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$pic 	= empty($_POST['ThumbnailFile']) ? htmlspecialchars($_GET['ThumbnailFile']) : $_POST['ThumbnailFile'];

		//判斷是否有登入會員
		login_required();

		//判斷資料是存在
		if((!empty($userid))){
			//判斷圖片資料是否存在
			if(!empty($pic)){
				//頭像修改資料
				$updata = $user->updateHeadImg($userid, $pic);
			}
			//取得用戶相關資料
			// $userdata = $user->get_user_profile($userid);
			$ret=getRetJSONArray(1,'頭像更新完成','JSON');
			$ret['retObj']=array();
			$ret['retObj']['fname'] = $pic;
			echo json_encode($ret);
			exit;
		}else{
			$ret=getRetJSONArray(-1,'頭像更新失敗','MSG');
			echo json_encode($data);
			exit;
		}

	}


	/*
	 *	確認手機是否驗證過
	 *	$phone 				varchar				手機號碼
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function checkPhoneSms() {

		global $tpl, $user, $config;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$phone = empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		login_required();

		//判斷是否帶入手機參數
		if(empty($phone)){
			$ret=getRetJSONArray(-1,'手機號碼異常','MSG');
			echo json_encode($ret);
			exit;
		}else{
			//取得手機號的驗證資料
			$data = $user->getPhoneSms($phone, $userid);

			if(!empty($data)){
				$ret=getRetJSONArray(-1,'此手機號碼已驗證過','MSG');
				echo json_encode($ret);
			}else{
				$ret=getRetJSONArray(1,'此手機號碼可驗證','MSG');
				echo json_encode($ret);
			}
			exit;
		}

	}

    // 綁定第三方(支付, 登入)帳號
	public function oauth_list() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$sso_list=$user->get_user_sso_list($_SESSION['auth_id']);

		if($sso_list){
			  $arrSSO=array();
			  foreach($sso_list as $row){
				$arrSSO[$row['name']]=$row;
			  }
		}
		$tpl->set_title('');
		$tpl->assign('sso_list',$arrSSO);
		$tpl->render("user","oauth_list",true);

	}


	// 支付帳號綁定
	public function bind_oauth() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$sso_name = empty($_POST['sso_name']) ? htmlspecialchars($_GET['sso_name']) : htmlspecialchars($_POST['sso_name']);
		$tpl->assign('sso_name', $sso_name);
		switch ($sso_name){
			case 'gama':
							  $tpl->assign("act","bind");
							  $tpl->render("user","inputGamaPayId",true);
							  break;
			default: break;
		}

		return ;

	}


	// 解除綁定支付帳號
	public function unbind_oauth() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$sso_name = empty($_POST['sso_name']) ? htmlspecialchars($_GET['sso_name']) : htmlspecialchars($_POST['sso_name']);
		$uid=empty($_POST['uid']) ? htmlspecialchars($_GET['uid']) : htmlspecialchars($_POST['uid']);
		$tpl->assign('sso_name', $sso_name);

		switch ($sso_name){
				case 'gama':
									$tpl->assign('GamaPayID', $uid);
									$tpl->assign("act","unbind");
									$tpl->render("user","inputGamaPayId",true);
									break;
				default: break;
		}

		return ;

	}


	public function inputVerifyCode() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$act = empty($_POST['act']) ? htmlspecialchars($_GET['act']) : htmlspecialchars($_POST['act']);
		$token=empty($_POST['Token']) ? htmlspecialchars($_GET['Token']) : htmlspecialchars($_POST['Token']);
		$gamapayid=empty($_POST['GamaPayID']) ? htmlspecialchars($_GET['GamaPayID']) : htmlspecialchars($_POST['GamaPayID']);
		$merchantaccount=empty($_POST['MerchantAccount']) ? htmlspecialchars($_GET['MerchantAccount']) : htmlspecialchars($_POST['MerchantAccount']);

		if(empty($merchantaccount))
		  $merchantaccount=$_SESSION['user']['name'];

		error_log("[c/user] Token:".$token." ,GamaPayID:".$gamapayid." ,MerchantAccount:".$merchantaccount);

		$tpl->set_title('');
		$tpl->assign('act',$act);
		$tpl->assign('Token', $token);
		$tpl->assign('GamaPayID', $gamapayid);
		$tpl->assign('MerchantAccount', $merchantaccount);
		$tpl->render("user","inputVerifyCode",true);

	}


	// Add By Thomas 20181122
	// (用login帳號) 查詢用戶紅利點數
	public function getBonus($loginid='', $passwd='') {

		global $user, $member;

		$ret = array('retCode'=>'0',
		             'retMsg'=>'EMPTY');
		if(empty($loginid))
		  $loginid=empty($_POST['acct'])?htmlspecialchars($_GET['acct']):htmlspecialchars($_POST['acct']);

		if(empty($loginid)){
			echo json_encode($ret);
			return;
		}

		$userData = $user->get_user('',$loginid,'P');

		if($userData && $userData['userid']){

			$ret['retCode']='1';
		  $ret['retMsg']='OK';
		  $temp=$member->get_bonus($userData['userid']);

			if(is_array($temp))
		    $ret['retObj']['amount']=floor($temp['amount']);

		  unset($ret['retObj']['userid']);

		}

		echo json_encode($ret);
		return;

	}


	/*
	 *	會員信用卡代碼維護功能 (新增)
	 *	$json							varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$action							varchar				請求類型 (new：新增信用卡代碼 delete：刪除信用卡代碼 suspend：暫停信用卡代碼使用 re-enable：恢復信用卡代碼使用)
	 *	$username						varchar				持卡人姓名
	 *	$cardbank						varchar				發卡行名稱
	 *	$card1							varchar				信用卡最前4碼
	 *	$card2							varchar				信用卡前4碼
	 *	$card3							varchar				信用卡後4碼
	 *	$card4							varchar				信用卡最後4碼
	 *	$CVV2							varchar				信用卡安全碼
	 *	$expiration_month				varchar				有效月份
	 *	$expiration_year				varchar				有效年份
	 */
	public function user_creditcard_token_add() {

		global $tpl, $config, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$action = empty($_POST['action']) ? htmlspecialchars($_GET['action']) : htmlspecialchars($_POST['action']);
		$username = empty($_POST['username']) ? htmlspecialchars($_GET['username']) : htmlspecialchars($_POST['username']);
		$cardbank = empty($_POST['cardbank']) ? htmlspecialchars($_GET['cardbank']) : htmlspecialchars($_POST['cardbank']);
		$card1 = empty($_POST['card1']) ? htmlspecialchars($_GET['card1']) : htmlspecialchars($_POST['card1']);
		$card2 = empty($_POST['card2']) ? htmlspecialchars($_GET['card2']) : htmlspecialchars($_POST['card2']);
		$card3 = empty($_POST['card3']) ? htmlspecialchars($_GET['card3']) : htmlspecialchars($_POST['card3']);
		$card4 = empty($_POST['card4']) ? htmlspecialchars($_GET['card4']) : htmlspecialchars($_POST['card4']);
		$CVV2 = empty($_POST['CVV2']) ? htmlspecialchars($_GET['CVV2']) : htmlspecialchars($_POST['CVV2']);
		$expiration_month = empty($_POST['expiration_month']) ? htmlspecialchars($_GET['expiration_month']) : htmlspecialchars($_POST['expiration_month']);
		$expiration_year = empty($_POST['expiration_year']) ? htmlspecialchars($_GET['expiration_year']) : htmlspecialchars($_POST['expiration_year']);
		error_log("[user_creditcard_token_add]:".json_encode($_POST));

		if(empty($action)){
			$ret = getRetJSONArray(-1,'請求類型錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		if(empty($card1)|| empty($card2) || empty($card3) || empty($card4)){
			$ret = getRetJSONArray(-2,'信用卡卡號錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		if(empty($CVV2)){
			$ret = getRetJSONArray(-3,'信用卡安全碼錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		if(empty($expiration_month)){
			$ret = getRetJSONArray(-4,'信用卡有效月份錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		if(empty($expiration_year)){
			$ret = getRetJSONArray(-5,'信用卡有效年份錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		if(empty($username)){
			$ret = getRetJSONArray(-6,'持卡人姓名錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}

		$isProduction = false; //是否為正式平台（true為正式平台，false為測試平台）
		try {

			$paymentURL = '';
			if ($isProduction) {
				$paymentURL = 'https://www.esafe.com.tw/serviceAPP/Api/Token_Request.ashx'; //傳送網址（正式）
			} else {
				$paymentURL = 'https://test.esafe.com.tw/serviceAPP/Api/Token_Request.ashx'; //傳送網址（測試）
			}

			$merchantID = $config['creditcard']['merchantnumber_test']; //商家代碼
			$password = $config['creditcard']['code']; //交易密碼
			//帳號與金鑰設定（用於信用卡代碼交易）
			$key = $config['creditcard']['key']; 						//金鑰
			$encIV = $config['creditcard']['encIV']; 					//IV

			$timestamp = gmdate('U'); //目前時間Unix Time Stamp (UTC時間)

			$curl = curl_init('https://lookup.binlist.net/'.$card1.$card2);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $requestStr);

			//商店主機端的TLS/SSL協定至少支援TLS 1.1/1.2其中一種
			//OpenSSL需版本1.0.1以上才支援TLS 1.1/1.2
			//curl版本為7.34.0以上時為強制支援TLS 1.1/1.2
			//如有必要時，請使用以下其中一行設定
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_1); //可將CURL_SSLVERSION_TLSv1_1改為數字5
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); //可將CURL_SSLVERSION_TLSv1_2改為數字6

			//執行送出請求
			$returnCard = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			//if ( $status != 201 ) {
			//    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
			//}
			curl_close($curl);
			$carddata = json_decode($returnCard, true);
			// print_r($carddata);

			$tokenRequest=array('action'=>'','merchantID'=>'','cardData'=>'','chkValue'=>'','lang'=>''); //傳入資料（Token請求）
			$cardData=array('timestamp'=>'','userID'=>'','cardHolder'=>'','cardNo'=>'','expiryDate'=>'','CVV2'=>'','paymentToken'=>'','verificationCode'=>'','tokenExpiryDate'=>''); //傳入資料（卡片資料）
			$cardData['timestamp'] = $timestamp;
			$cardData['userID'] = $userid;
			// $cardData['cardHolder'] = 'joelin';
			// $cardData['cardNo'] = 5428210015691234;
			// $cardData['expiryDate'] = '0825';
			// $cardData['CVV2'] = '888';
			$cardData['cardHolder'] = $username;
			$cardData['cardNo'] = $card1.$card2.$card3.$card4;
			$cardData['expiryDate'] = $expiration_month.$expiration_year;
			$cardData['CVV2'] = $CVV2;
			$cardData['paymentToken'] = '';
			$cardData['verificationCode'] = '';
			$cardData['tokenExpiryDate'] = '';
			// print_r($cardData);
			$json = json_encode_fix($cardData);
			//在加密時,會出現"+"被替換成空白的問題,所以需將空白替換回來
			$json = str_replace(" ","+",$json);
			$tokenRequest['action'] = 'new'; //新增

			$tokenRequest['merchantID'] = $merchantID;
			$tokenRequest['cardData'] = dataEncrypt($json, $key, $encIV);
			$tokenRequest['chkValue'] = SHA256Encrypt($merchantID . $password . $tokenRequest['action'] . $json);
			$tokenRequest['lang'] = 'tw';

			$requestStr = json_encode_fix($tokenRequest);
			// print_r($requestStr);
			//在加密時,會出現"+"被替換成空白的問題,所以需將空白替換回來
			$requestStr = str_replace(" ","+",$requestStr);

            error_log("[user/user_creditcard_token_add] requestStr : ${requestStr}");
			//送出請求
			$curl = curl_init($paymentURL);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $requestStr);

			//商店主機端的TLS/SSL協定至少支援TLS 1.1/1.2其中一種
			//OpenSSL需版本1.0.1以上才支援TLS 1.1/1.2
			//curl版本為7.34.0以上時為強制支援TLS 1.1/1.2
			//如有必要時，請使用以下其中一行設定
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_1); //可將CURL_SSLVERSION_TLSv1_1改為數字5
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); //可將CURL_SSLVERSION_TLSv1_2改為數字6

			//執行送出請求
			$returnStr = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			//if ( $status != 201 ) {
			//    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
			//}
			curl_close($curl);

            error_log("[user/user_creditcard_token_add] returnStr : ".$returnStr);
			
			//解析資料
			if (strpos($returnStr,'tokenData') > 0) {
				//不用事先定義回傳資料的JSON結構
				$responseData=json_decode_fix($returnStr);

                error_log("[user/user_creditcard_token_add] responseData : ".json_encode($responseData));
				//檢查回覆是否有錯誤訊息
				if (!isset($responseData->errorMessage)) {
					throw new Exception('回覆格式錯誤');
					$ret=getRetJSONArray(-7,'回覆格式錯誤','MSG');
				} elseif ($responseData->errorMessage != "") {
					throw new Exception($responseData->errorMessage);
					$ret=getRetJSONArray(-8,$responseData->errorMessage,'MSG');
				}

				//查看tokenData內容
				if ((isset($responseData->tokenData)) && ($responseData->tokenData != '')) {
					$responseData->tokenData = dataDecrypt($responseData->tokenData, $key, $encIV);
					//檢查碼驗證
					if ($responseData->chkValue != SHA256Encrypt($merchantID . $password . $tokenRequest['action'] . $responseData->tokenData)) {
						throw new Exception('資料驗證錯誤');
					}

					$returnStr .= "\r\n" . "\r\n" . "tokenData: " . "\r\n" . $responseData->tokenData; //組成欲顯示的內容（非必要，請改成您希望的工作）

					//將responseData.tokenData物件化以便存取
					//不用事先定義回傳資料$tokenData的JSON結構
					$tokenData=json_decode_fix($responseData->tokenData);
					//物件化後，$tokenData->paymentToken即為信用卡代碼，更多參數說明請參考技術手冊
					//在這裡加入您要處理的程式（例如儲存回傳的資料）
					$tokenDataObj = json_decode($responseData->tokenData, true);

					$tokenDataObj['expiration_month'] = $expiration_month;
					$tokenDataObj['expiration_year'] = $expiration_year;
					$tokenDataObj['cardbank'] = $carddata['bank']['name'];
					$tokenDataObj['cardtype'] = $carddata['scheme'];
					// 寫入信用卡代碼資料
					$card = $user->addUserCreditCardToken($userid, $tokenDataObj);

					$ret=getRetJSONArray(1,'交易碼維護成功','MSG');
					$ret['retObj']['data']['cardtype'] = $tokenDataObj['cardtype'];
					$ret['retObj']['data']['expiration_month'] = $tokenDataObj['expiration_month'];
					$ret['retObj']['data']['expiration_year'] = $tokenDataObj['expiration_year'];
					$ret['retObj']['data']['cardbank'] = $tokenDataObj['cardbank'];
					$ret['retObj']['data']['card4'] = $card4;

				} else {
					throw new Exception('資料錯誤');
					$ret=getRetJSONArray(-9,'資料錯誤','MSG');
				}

			} else {
				throw new Exception('回覆格式錯誤');
				$ret=getRetJSONArray(-7,'回覆格式錯誤','MSG');
			}

		} catch (Exception $ex) {
			// processMonitor($requestStr, 'error: ' . $ex->getMessage() . "\r\n\r\n" . $returnStr); //顯示錯誤訊息（非必要，請改成您希望的工作）
			error_log("[c/user/user_creditcard_token_add] error : ".$ex->getMessage());
			$ret=getRetJSONArray(-10,'交易碼維護失敗','MSG');
		}

		echo json_encode($ret);
		//processMonitor($requestStr, $returnStr); //顯示送出與傳送內容（非必要，請改成您希望的工作）
		exit();
	}


	/*
	 *	會員信用卡代碼維護功能 (刪除)
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$action				varchar				請求類型 (new：新增信用卡代碼 delete：刪除信用卡代碼 suspend：暫停信用卡代碼使用 re-enable：恢復信用卡代碼使用)
	 *	$ucid				int					會員信用卡編號
	 */
	public function user_creditcard_token_del() {

		global $tpl, $config, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$action = empty($_POST['action']) ? htmlspecialchars($_GET['action']) : htmlspecialchars($_POST['action']);
		$ucid = empty($_POST['ucid']) ? htmlspecialchars($_GET['ucid']) : htmlspecialchars($_POST['ucid']);

		if(empty($action)){
			$ret = getRetJSONArray(-1,'請求類型錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		if(empty($ucid)){
			$ret = getRetJSONArray(-2,'會員信用卡流水編號錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		//取得信用卡代碼資料
		$usercard = $user->getUserCreditCardToken($userid,$ucid);

		$isProduction = false; //是否為正式平台（true為正式平台，false為測試平台）
		try {

			$paymentURL = '';
			if ($isProduction) {
				$paymentURL = 'https://www.esafe.com.tw/serviceAPP/Api/Token_Request.ashx'; //傳送網址（正式）
			} else {
				$paymentURL = 'https://test.esafe.com.tw/serviceAPP/Api/Token_Request.ashx'; //傳送網址（測試）
			}

			$merchantID = $config['creditcard']['merchantnumber_test']; //商家代碼
			$password = $config['creditcard']['code']; //交易密碼
			//帳號與金鑰設定（用於信用卡代碼交易）
			$key = $config['creditcard']['key']; 						//金鑰
			$encIV = $config['creditcard']['encIV']; 					//IV

			$timestamp = gmdate('U'); //目前時間Unix Time Stamp (UTC時間)

			$tokenRequest=array('action'=>'','merchantID'=>'','cardData'=>'','chkValue'=>'','lang'=>''); //傳入資料（Token請求）
			$cardData=array('timestamp'=>'','userID'=>'','cardHolder'=>'','cardNo'=>'','expiryDate'=>'','CVV2'=>'','paymentToken'=>'','verificationCode'=>'','tokenExpiryDate'=>''); //傳入資料（卡片資料）
			$cardData['timestamp'] = $timestamp;
			$cardData['userID'] = $userid;
			$cardData['cardHolder'] = '';
			$cardData['cardNo'] = '';
			$cardData['expiryDate'] = '';
			$cardData['CVV2'] = '';
			$cardData['paymentToken'] = $usercard['paymentToken'];
			$cardData['verificationCode'] = $usercard['verificationCode'];
			$cardData['tokenExpiryDate'] = $usercard['tokenExpiryDate'];
			$json = json_encode_fix($cardData);
			//在加密時,會出現"+"被替換成空白的問題,所以需將空白替換回來
			$json = str_replace(" ","+",$json);
			$tokenRequest['action'] = 'delete';

			$tokenRequest['merchantID'] = $merchantID;
			$tokenRequest['cardData'] = dataEncrypt($json, $key, $encIV);
			$tokenRequest['chkValue'] = SHA256Encrypt($merchantID . $password . $tokenRequest['action'] . $json);
			$tokenRequest['lang'] = 'tw';

			$requestStr = json_encode_fix($tokenRequest);
			// print_r($requestStr);
			//在加密時,會出現"+"被替換成空白的問題,所以需將空白替換回來
			$requestStr = str_replace(" ","+",$requestStr);


			//送出請求
			$curl = curl_init($paymentURL);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $requestStr);

			//商店主機端的TLS/SSL協定至少支援TLS 1.1/1.2其中一種
			//OpenSSL需版本1.0.1以上才支援TLS 1.1/1.2
			//curl版本為7.34.0以上時為強制支援TLS 1.1/1.2
			//如有必要時，請使用以下其中一行設定
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_1); //可將CURL_SSLVERSION_TLSv1_1改為數字5
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); //可將CURL_SSLVERSION_TLSv1_2改為數字6

			//執行送出請求
			$returnStr = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			//if ( $status != 201 ) {
			//    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
			//}
			curl_close($curl);

			//解析資料
			if (strpos($returnStr,'tokenData') > 0) {
				//不用事先定義回傳資料的JSON結構
				$responseData=json_decode_fix($returnStr);

				//檢查回覆是否有錯誤訊息
				if (!isset($responseData->errorMessage)) {
					throw new Exception('回覆格式錯誤');
					$ret=getRetJSONArray(-7,'回覆格式錯誤','MSG');
				} elseif ($responseData->errorMessage != "") {
					throw new Exception($responseData->errorMessage);
					$ret=getRetJSONArray(-8,$responseData->errorMessage,'MSG');
				}

				//查看tokenData內容
				if ((isset($responseData->tokenData)) && ($responseData->tokenData != '')) {
					$responseData->tokenData = dataDecrypt($responseData->tokenData, $key, $encIV);
					//檢查碼驗證
					if ($responseData->chkValue != SHA256Encrypt($merchantID . $password . $tokenRequest['action'] . $responseData->tokenData)) {
						throw new Exception('資料驗證錯誤');
						$ret=getRetJSONArray(-11,'資料驗證錯誤','MSG');
					}
					$returnStr .= "\r\n" . "\r\n" . "tokenData: " . "\r\n" . $responseData->tokenData; //組成欲顯示的內容（非必要，請改成您希望的工作）

					//將responseData.tokenData物件化以便存取
					//不用事先定義回傳資料$tokenData的JSON結構
					$tokenData=json_decode_fix($responseData->tokenData);
					//物件化後，$tokenData->paymentToken即為信用卡代碼，更多參數說明請參考技術手冊
					//在這裡加入您要處理的程式（例如儲存回傳的資料）
					$tokenDataObj = json_decode($responseData->tokenData, true);

					$tokenDataObj['expiration_month'] = $expiration_month;
					$tokenDataObj['expiration_year'] = $expiration_year;
					$tokenDataObj['cardbank'] = $carddata['bank']['name'];
					$tokenDataObj['cardtype'] = $carddata['scheme'];
					// echo "eee</br>";
					// print_r($tokenDataObj);
					$card = $user->delUserCreditCardToken($userid,$ucid);
					$ret=getRetJSONArray(1,'交易碼維護成功','MSG');
				} else {
					throw new Exception('資料錯誤');
					$ret=getRetJSONArray(-9,$responseData->errorMessage,'MSG');
				}

			} else {
				throw new Exception('回覆格式錯誤');
				$ret=getRetJSONArray(-7,'回覆格式錯誤','MSG');
			}

		} catch (Exception $ex) {
			// processMonitor($requestStr, 'error: ' . $ex->getMessage() . "\r\n\r\n" . $returnStr); //顯示錯誤訊息（非必要，請改成您希望的工作）
			error_log("[c/user/user_creditcard_token_del] error : ".$ex->getMessage());
			$ret=getRetJSONArray(-10,'交易碼維護失敗','MSG');
		}

		echo json_encode($ret);
		// processMonitor($requestStr, $returnStr); //顯示送出與傳送內容（非必要，請改成您希望的工作）
		exit();
	}



	/*
	 *	會員信用卡代碼維護功能 (修改)
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$action				varchar				請求類型 (new：新增信用卡代碼 delete：刪除信用卡代碼 suspend：暫停信用卡代碼使用 re-enable：恢復信用卡代碼使用 edit:修改預設卡片)
	 *	$ucid				int					會員信用卡編號
	 */
	public function user_creditcard_token_edit() {

		global $tpl, $config, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$action = empty($_POST['action']) ? htmlspecialchars($_GET['action']) : htmlspecialchars($_POST['action']);
		$ucid = empty($_POST['ucid']) ? htmlspecialchars($_GET['ucid']) : htmlspecialchars($_POST['ucid']);

		if(empty($action)){
			$ret = getRetJSONArray(-1,'請求類型錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		if(empty($ucid)){
			$ret = getRetJSONArray(-2,'會員信用卡流水編號錯誤!!','MSG');
			echo json_encode($ret);
			exit;
		}
		//取得信用卡代碼資料
		$usercard = $user->getUserCreditCardToken($userid,$ucid);

		try {
			$card = $user->editUserCreditCardToken($userid,$ucid);
			$ret=getRetJSONArray(1,'信用卡預設修改成功','MSG');
		} catch (Exception $ex) {
			$ret=getRetJSONArray(-3,'信用卡預設修改失敗','MSG');
		}

		echo json_encode($ret);
		exit();
	}


	/*
	 *	會員信用卡代碼維護功能 (圖文說明)
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$sysid				int					系統訊息編號
	 */
	public function user_creditcard_content() {

		global $tpl, $config, $user, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$sysid = empty($_POST['sysid']) ? htmlspecialchars($_GET['sysid']) : htmlspecialchars($_POST['sysid']);

		$syslist = $mall->get_sys($sysid);

		$description = (!empty($syslist[0]['description']) ) ? $syslist[0]['description'] : '';
		$description = str_replace(array("\r\n\t&nbsp;","\r","\n","\t","&nbsp;"), '', (trim(strip_tags($description))));
		$syslist[0]['description'] = html_decode($description);

		$ret = getRetJSONArray(1,'OK','MSG');
		$ret['retObj']['data'] = (!empty($syslist)) ? $syslist : array();
		echo json_encode($ret);
		exit();
	}


	/*
	 *	會員相關公告
	 *	$newsid 			int					資料編號
     *	$public				varchar				公開模式 (Y:顯示在首頁 N:站內用公告)
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function usernews() {

		global $tpl, $user, $config;
		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$newsid = empty($_POST['newsid']) ? htmlspecialchars($_GET['newsid']) : htmlspecialchars($_POST['newsid']);
		$public = empty($_POST['public']) ? htmlspecialchars($_GET['public']) : htmlspecialchars($_POST['public']);

        error_log("[c/usernews] : ".json_);
		//取得用戶條款的前言說明
		$data = array();
		/*
		$preface=$user->getNews('1','Y');
		if($preface) {
		   array_push($data, $preface[0]);	
		}
		*/
		//取得用戶條款的資料
		$news = $user->getNews($newsid,$public);
		if($news) {
		   foreach($news as $v) {
		      array_push($data, $v);
           }		   
		}

		if(!empty($data)){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['page_title']="會員約定條款修定通知";
			$ret['retObj']['bottom_btn_title']="我已閱讀並同意上述所有條款";
			$ret['retObj']['data'] = (!empty($data)) ? $data : array();
			// error_log(json_encode($data));
			echo json_encode($ret);
		}else{
			$ret=getRetJSONArray(0,'NO_DATA_FOUND','MSG');
			$ret['retObj']['page_title']="會員約定條款修定通知";
			$ret['retObj']['bottom_btn_title']="我已閱讀並同意上述所有條款";
			echo json_encode($ret);
		}
		exit;

	}
	
	// 會員修改資訊-條款
	public function uagreement() {

		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();
		//成交狀態

		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$agree = empty($_POST['agree']) ? htmlspecialchars($_GET['agree']) : htmlspecialchars($_POST['agree']);
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//判斷是否有登入會員
		login_required();
		$result=array();
		if( empty($userid)){
			$result['retCode']=-1;
			$result['retMsg']='查無相關使用者記錄';
		} else {
			$rowdata = $user->update_agreement($userid,$agree);
			if ($rowdata==1 ){
				$result['retCode']=1;
				$result['retMsg']='更新成功';
			} else{
				$result['retCode']=$rowdata;
				$result['retMsg']='更新失敗';
			}

		}
		echo json_encode($result);
		exit;
	}


	/*
	 *	會員載具功能
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$sysid				int					系統訊息編號
	 */
	public function user_carrier() {

		global $tpl, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		//會員個人資料
		$user_profile = $user->get_user_carrier($userid);
		$user_profile['content'] = '<div class="carrier-content">1.您可將殺價王會員載具歸戶至「手機條碼」或「自然人憑證」。</div>
			<div class="carrier-content">2.未歸戶的用戶：我們將於每期發票開獎後進行對獎（已索取／已捐贈／已作廢／打統編發票除外），並發推播通知中獎用戶，並在次月10日前將中獎紙本發票以掛號寄出以便您至各行庫領獎。</div>
			<div class="carrier-content">3.已歸戶的用戶：將由「財政部電子發票整合平台」對獎並通知中獎，您可至「財政部電子發票服務整合平台」查詢發票資料，並可設定將獎金匯入您指定的銀行帳號。</div>
			<div class="carrier-content">4.如欲將殺價王會員載具歸戶，請先至「財政部電子發票服務整合平台」申請「手機條碼」或以「自然人憑證註冊」。</div>
			<div class="carrier-content">5.財政部電子發票服務整合平台<a href="https://www.einvoice.nat.gov.tw/"><font style="color:#0000FF">www.einvoice.nat.gov.tw</font></a></div>
			<div class="carrier-content">6.財稅中心24小時免費客服電話 0800-521-988</div>';
		
		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('user_profile', $user_profile);
			$tpl->set_title('');
			$tpl->render("user","carrier",true);
		}else{
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";
			$data['retObj']['data']= (!empty($user_profile)) ? $user_profile : new stdClass;
			echo json_encode($data);
		}
	}	

	/*
	 *	會員手機條碼
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$sysid				int					系統訊息編號
	 */
	public function user_phonecode() {

		global $tpl, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		//會員個人資料
		$user_profile = $user->get_user_carrier($userid);
		
		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('user_profile', $user_profile);
			$tpl->set_title('');
			$tpl->render("user","phonecode",true);
		}else{
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";
			$data['retObj']['data']= (!empty($user_profile)) ? $user_profile : new stdClass;
			echo json_encode($data);
		}
	}	

	/*
	 *	會員手機條碼
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$sysid				int					系統訊息編號
	 */
	public function user_npcode() {

		global $tpl, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		//會員個人資料
		$user_profile = $user->get_user_carrier($userid);
		
		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('user_profile', $user_profile);
			$tpl->set_title('');
			$tpl->render("user","npcode",true);
		}else{
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";
			$data['retObj']['data']= (!empty($user_profile)) ? $user_profile : new stdClass;
			echo json_encode($data);
		}
	}


	/*
	 *	會員發票設定
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$sysid				int					系統訊息編號
	 */
	public function user_invoice() {

		global $tpl, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);

		$donate[0]['code'] ="856203";
		$donate[1]['code'] ="321";
		$donate[2]['code'] ="0770223";
		$donate[3]['code'] ="7505";
		$donate[4]['code'] ="172";
		$donate[5]['code'] ="978";
		$donate[6]['code'] ="8850";
		
		$donate[0]['name'] ="台灣幸福狗流浪中途之家";
		$donate[1]['name'] ="財團法人中華民國唐氏症基金會";
		$donate[2]['name'] ="財團法人台北市基督徒救世會社會福利事業基金會";
		$donate[3]['name'] ="中華社會福利聯合勸募協會";
		$donate[4]['name'] ="臺北巿流浪猫保護協會";
		$donate[5]['name'] ="台灣之心愛護動物協會";
		$donate[6]['name'] ="社法人中華民國身障關懷協會";

		//會員個人資料
		$user_profile = $user->get_user_carrier($userid);
		$user_profile['content'] = '<div style="margin:1rem;color:#FF0000;font-size:1.1rem;">*依規定公司發票開立不能改為個人發票<br><br>*電子發票選擇公司類型，需填寫統一編號及發票抬頭</div>';
		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('donate', $donate);
			$tpl->assign('user_profile', $user_profile);
			$tpl->set_title('');
			$tpl->render("user","invoice",true);
		}else{
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";
			$data['retObj']['data']= (!empty($user_profile)) ? $user_profile : new stdClass;
			echo json_encode($data);
		}
	}

	/*  function test	/* //20201201leo
	 *	會員信用卡代碼維護功能 (圖文說明)
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$sysid				int					系統訊息編號
	 */
	public function test() {
		error_reporting(E_ALL); ini_set("display_errors", 1);
		global $tpl, $config, $user, $mall;

		//設定 Action 相關參數
		set_status($this->controller);

		//login_required();
		/*
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$depositid = empty($_POST['depositid']) ? htmlspecialchars($_GET['depositid']) : htmlspecialchars($_POST['depositid']);
		$amount = empty($_POST['amount']) ? htmlspecialchars($_GET['amount']) : htmlspecialchars($_POST['amount']);
		$rtn=couponreward($userid,$depositid,$amount,10,5);
		$ret = getRetJSONArray(1,'OK','MSG');
		$ret['retObj']['data'] = (!empty($rtn)) ? $rtn : array();
		echo json_encode($ret);*/
		//$user->set_idnum("H123456789",5014);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$intro_by = empty($_POST['intro_by']) ? htmlspecialchars($_GET['intro_by']) : htmlspecialchars($_POST['intro_by']);
		$user->setUserAffiliate($userid,$intro_by);
		//var_dump( decrypt($_GET['idnum']));
		//var_dump( decrypt('et+ESMS1wnvf0wff88n+pnhE2bayJY08EJ3oCJeUhYY='));
	}

	// 殺房活動註冊
	public function register2() {

		global $tpl, $user, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$user_src = empty(htmlspecialchars($_REQUEST['user_src'])) ? htmlspecialchars($_REQUEST['user_src']) : '';
		
		if($_REQUEST['user_src']!=htmlspecialchars($_REQUEST['user_src'])) {
		   $tpl->set_title('');
		   $tpl->render("home","",true);
           return;		   
		}
		$tpl->assign('user_src', $user_src);

		$tpl->set_title('');
		$tpl->render("user","register2",true);
		// $tpl->render("mall","qrpage",true);

	}

		// 殺房活動註冊
	public function test1() {

		global $tpl, $user, $oscode,$db,$faq,$product,$push;

		//設定 Action 相關參數
		set_status($this->controller);
		//$user_src = empty(htmlspecialchars($_REQUEST['user_src'])) ? htmlspecialchars($_REQUEST['user_src']) : '';
		/*
		$oscode-> insert_bid_oscode(97120,97115,303,'5014');


		//$db = new mysql($config["db"]);
		//$db->connect();
		//echo $db->getFields("saja_user","saja_user","insertt","userid",1705);
		//var_dump( $db->getFields("saja_user","saja_user_profile","nickname,gender","userid",1705));

		$_category = $faq->help_category();
		var_dump($_category);
		foreach($_category as $tk => $tv){
			$table = $faq->help_list($tv['hcid']);
			var_dump($table);
			$_category[$tk]['help_list'] = $table;
		}
		*/
		//$product->reset_oscode_sets_info();
		$userid=1705;

		$product->reset_tmpl_product_redis();
		//$last3buy=$product->getLast3buy($userid);
		$schedule=($product->pickup_promote($userid));
		var_dump($schedule);
		$product->genProdsFromTmpl($schedule,$userid);

		//echo phpinfo();
		// Manager Class
		//$manager = new MongoDB\Driver\Manager(MONGO);
		//$collection_name ="hrlog";
		// Query Class
		//$query = new MongoDB\Driver\Query(array('userid' => 47));

		// Output of the executeQuery will be object of MongoDB\Driver\Cursor class
		//$cursor = $manager->executeQuery(MONGO_DB.".$collection_name", $query);

		// Convert cursor to Array and print result
		//print_r($cursor->toArray());
		//var_dump($push->getNotifyCnt($userid));

	}
}
