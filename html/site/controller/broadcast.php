<?php
/**
 * User Controller
 *
 * This controller contains all the actions the user may perform that deals with their
 * account.
 */

include_once(LIB_DIR ."/convertString.ini.php");

class Broadcast {

	public $controller = array();
	public $params = array();
	public $id;

	/*
	 * Default home page.
	 */
	public function home($lbid='', $lbuserid='', $user_src='') {

		global $db, $config, $tpl, $broadcast;
		//設定 Action 相關參數
		set_status($this->controller);

		// 直播間編號
		if(empty($lbid)){
		  $lbid = htmlspecialchars($_REQUEST['lbid']);
		}

		// 直播主編號
		if(empty($lbuserid)){
		  $lbuserid = htmlspecialchars($_REQUEST['lbuserid']);
		}

		// 推薦人的userid
		if(empty($user_src)){
		  $user_src = htmlspecialchars($_REQUEST['user_src']);
		}

		error_log("[c/broadcast/home] userid : ".$_SESSION['auth_id']);
		if(!empty($_SESSION['auth_id'])){
		   if(!empty($lbuserid) || !empty($lbid)){
		     $this->play($lbid, $lbuserid, $user_src);
		   }else{
		     $this->lb_list();
		   }
		   return;
		}

		$browserType=browserType();
		$base_url=BASE_URL;
		$state['callback_url']=$base_url.APP_DIR."/broadcast/regLogin/";
		$state['user_src']=$user_src;
		$state['lbid']=$lbid;
		$state['lbuserid']=$lbuserid;

		switch($browserType){
		  case 'fb' :
		                $url=$base_url."/site/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
		                error_log("[oauthapi/auth] FB auth url :".$url);
		                header("Location:".$url);
		                break;
		  case 'line' :
		                $url=$base_url."/site/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
		                error_log("[oauthapi/auth] Line auth url :".$url);
		                header("Location:".$url);
		                break;
		  default: break;
		}

		return ;

	}


	/*
	 *	直播大廳(list)
	 */
	public function lb_list() {

		global $db, $config, $tpl, $broadcast;

		//設定 Action 相關參數
		set_status($this->controller);

		$live_broadcast_list = $broadcast->getLiveBroadcast('','');

		$tpl->set_title('');
		$tpl->assign('live_broadcast_list',$live_broadcast_list);
		$tpl->assign('footer','Y');
		$tpl->render("broadcast","home",true);
	}


	public function test() {

		$tpl->set_title('');
		$tpl->render("broadcast","test",true);

    }


	/*
	 *	註冊登入
	 */
	public function regLogin() {

		global $db, $config, $tpl, $usermodel, $broadcast;

		$auth_by	= empty($_POST['auth_by']) ? htmlspecialchars($_GET['auth_by']) : htmlspecialchars($_POST['auth_by']);
		$sso_uid	= empty($_POST['sso_uid']) ? htmlspecialchars($_GET['sso_uid']) : htmlspecialchars($_POST['sso_uid']);
		$state   	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
		$data 		= empty($_POST['data']) ? htmlspecialchars($_GET['data']) : htmlspecialchars($_POST['data']);
		$sso_data 	= json_decode(urldecode(base64_decode($data)), true);

		error_log("[c/regLogin] state : ".urldecode(base64_decode($state)));
		$arrState =  json_decode(urldecode(base64_decode($state)), true);
		$lbid=$arrState['lbid'];
		$lbuserid=$arrState['lbuserid'];
		$user_src=$arrState['user_src'];
		$ts=str_replace(".","",microtime(true));
		$arrSSO = array();

		switch($auth_by){
			case 'line':
			  $arrSSO = array(
									      'auth_by' 		=> $auth_by,
									      'sso_data' 		=> urldecode(base64_decode($data)),
									      'state' 		=> $state,
									      'nickname' 		=> $sso_data['displayName'],
									      'headimgurl'    => $sso_data['pictureUrl'],
									      'sso_name' 		=> "line",
									      'phone'         => "ln".$ts,
									      'type' 			=> "sso",
									      'json'          => "Y",
									      'gender'        => "N/A",
									      'sso_uid' 		=> $sso_uid,
									      'sso_uid2' 		=> "",
									      'user_src'      => $user_src
			                 );
			 break;
			case 'fb':
			  $arrSSO = array(
									      'auth_by' 		=> $auth_by,
									      'sso_data' 		=> urldecode(base64_decode($data)),
									      'state' 		=> $state,
									      'nickname' 		=> $sso_data['name'],
									      'headimgurl' => $sso_data['picture']['url'],
									      'sso_name' 		=> "fb",
									      'phone'         => "fb".$ts,
									      'type' 			=> "sso",
									      'json'          => "Y",
									      'gender'        => "N/A",
									      'sso_uid' 		=> $sso_uid,
									      'sso_uid2' 		=> "",
									      'user_src'      => $user_src
			                 );
			  break;
		}
		error_log("[c/broadcast/regLogin] oauth data : ".json_encode($arrSSO));

		$get_user = $broadcast->get_user($sso_uid, $auth_by, '');
		error_log("[c/broadcast/regLogin] get_user : ".json_encode($get_user));

		if(empty($get_user)){

        $url = BASE_URL.APP_DIR."/ajax/user_register.php";

				//設定送出方式-POST
        $stream_options = array(
								'http' => array (
									'method' => "POST",
									'content' => json_encode($arrSSO),
									'header' => "Content-Type:application/json"
								)
                            );

        $context = stream_context_create($stream_options);

        // 送出json內容並取回結果
        $response = file_get_contents($url, false, $context);

        error_log("[broadcast/regLogin] response : ".$response);

        // 讀取json內容
        $arr = json_decode($response,true);

		    if($arr['retCode'] == 1){
          $get_user = $broadcast->get_user($sso_uid, $auth_by, '');
        }

		}

		$this->set_login($get_user);

		$tpl->assign('oauth', $arrSSO);

		if(empty($lbid) && empty($lbuserid)) {
			// 如果已登入但沒有指定直播主和直播間編號
		   $this->lb_list();
		} else {
		   $this->play($lbid, $lbuserid, $user_src);
		}

		return ;
	}

	
	/*
	 *	登入設定
	 */
	public function set_login($user){

		global $db, $config, $usermodel;

		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';

		$query = "SELECT *
							FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							WHERE `prefixid` = '{$config['default_prefix_id']}'
								AND `userid` = '{$user['userid']}'
								AND `switch` =  'Y'
		";
		error_log("[c/broadcast/set_login]Query User : ".$query);
		$table = $db->getQueryRecord($query);

		$user['profile'] = $table['table']['record'][0];
		$_SESSION['user'] = $user;

		// Set session and cookie information.
		$_SESSION['auth_id'] = $user['userid'];
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = md5($user['userid'] . $user['name']);
		setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", md5($user['userid'] . $user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);

		// Set local publiciables with the user's info.
		if(isset($usermodel)){
			$usermodel->user_id = $user['userid'];
			$usermodel->name = $user['profile']['nickname'];
			$usermodel->email = '';
			$usermodel->ok = true;
			$usermodel->is_logged = true;
		}

	}


	/*
	 *	 進入指定直播間 ($lbid)
	 */
	public function play($lbid='', $lbuserid='', $user_src='') {

		global $db, $config, $tpl, $broadcast;
		set_status($this->controller);

		if(empty($_SESSION['auth_id'])) {
			$this->home($lbid, $lbuserid, $user_src);
			return;
		}

		if(empty($lbid))
		 $lbid = empty($_POST['lbid']) ? htmlspecialchars($_GET['lbid']) : htmlspecialchars($_POST['lbid']);

		if(empty($lbuserid))
		 $lbuserid =  empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);

		if(empty($user_src))
		 $user_src =  empty($_POST['user_src']) ? htmlspecialchars($_GET['user_src']) : htmlspecialchars($_POST['user_src']);

		// 如果沒有指定直播主或直播間編號  就回到大廳
		if(empty($lbid) && empty($lbuserid)){
		  $this->home('','',$user_src);
		  return ;
		}

		error_log("[c/broadcast/play] lbid:".$lbid." , lbuserid:".$lbuserid." , user_src:".$user_src);

		$list = $broadcast->getLiveBroadcast($lbid,$lbuserid);
		if($list[0] && $list[0]['lbid']>0) {

			$arrBroadcast= $list[0];
		  $lbid=$arrBroadcast['lbid'];
		  $lbuserid=$arrBroadcast['lbuserid'];

			if($lbid) {
		      // Nothing to do now ...
		  }

		  if($lbuserid){
		    $arrPromote = $broadcast->getScodePromoteList($lbuserid);

				if($arrPromote && $arrPromote[0]) {
					// 商品編號
					$arrBroadcast['productid']= $arrPromote[0]['productid'];

					// 商品名稱
					$arrBroadcast['product_name']= $arrPromote[0]['product_name'];

					// 活動編號
					$arrBroadcast['spid']= $arrPromote[0]['spid'];

					// 活動名稱
					$arrBroadcast['spname']= $arrPromote[0]['name'];

					// 活動說明
					$arrBroadcast['spdesc']= $arrPromote[0]['description'];
		    }

		  }

		}else{
		  $tpl->assign('footer','Y');
		  $tpl->set_title('');
		  $tpl->render("broadcast","home",true);
		}

		error_log("[c/broadcast/play] broadcast_data : ".json_encode($arrBroadcast));
		$tpl->assign('footer','Y');
		$tpl->assign('live_broadcast_data',$arrBroadcast);
		$tpl->set_title('');
		$tpl->render("broadcast","play",true);
		return ;
	}


	/*
	 *	新增殺價券資料
	 */
	public function give_oscode() {

		global $db, $config, $broadcast, $product;

		$lbid = empty($_POST['lbid']) ? htmlspecialchars($_GET['lbid']) : htmlspecialchars($_POST['lbid']);
		$lbuserid =  empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);
		$productid =  empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=$_SESSION['auth_id'];

		if(empty($lbid) && empty($lbuserid) && empty($productid)) {
			$ret = getRetJSONArray(0,'無效的參數 !!','MSG');
			echo json_encode($ret);
		  return false;
		}

		$arrPromotes=$broadcast->getScodePromoteList($lbuserid);
		$retMsg ="";
		$num_received_oscode=0;
		$arrProductids=array();

		if($arrPromotes){
			error_log("[c/broadcast/give_oscode] : ".json_encode($arrPromotes));
			foreach($arrPromotes as $promote){
				if($promote['lbuserid']==$lbuserid){
				$spid = $promote['spid'];
					$onum = $promote['onum'];
					$productid=$promote['productid'];

					if(empty($productid)) {
					  error_log("[c/give_oscode] empty productid !! ");
					  continue;
					}

					// 檢查是否領過殺價券
					$received = $broadcast->getUserOscode($userid,$spid);
					error_log("[c/give_oscode] received oscode for productid ${productid} : ".$received[0]['count']);

						if($received[0]['count']>0) {
							$ret = array();
							$ret['retCode']=-1;
							$ret['retMsg']= "您已領取過殺價券 !!";
							echo json_encode($ret);
							return;
							continue;
						}

					if(true){
						if($onum>0){

						for($j=0;$j<$onum;++$j){
						  $broadcast->insertUserOscode($userid, $spid, $productid, $lbuserid);
						}

						if($spid>0){
						  $update = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
										SET amount=amount+1 ,scode_sum=scode_sum+".$onum."
									  WHERE spid='".$spid."'";
						  $ret=$db->query($update);
						  error_log("[c/give_oscode] update : ".$update."-->".$ret);
						  $num_received_oscode+=$onum;
						}

						array_push($arrProductids,$productid);

						}

						$ret = array();
						$ret['retCode']=1;
						$ret['retMsg']= "成功領取 ".$onum." 張殺價券 !!";
						$ret['prodids']=$arrProductids;
					}

				}

			}

		}else{

			$ret = array();
			$ret['retCode']=0;
			$ret['retMsg']= "本活動已結束 !!";

		}

		echo json_encode($ret);
		return;

	}
  
  
	/*
	 *	直播間資料
	 */
	public function lb_room() {

		global $db, $config, $tpl, $broadcast;

		//設定 Action 相關參數
		set_status($this->controller);

		$live_broadcast_list = $broadcast->getLiveBroadcastRoom('','','');

		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['retObj']=array();
		$ret['retObj']['data']=$live_broadcast_list;
		echo json_encode($ret,JSON_UNESCAPED_SLASHES);
		exit;
	}  
  

}
