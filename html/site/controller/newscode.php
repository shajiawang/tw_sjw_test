<?php
/*
 * Newscode Controller
 */

class Newscode {
  public $controller = array();
	public $params = array();
  public $id;
	public $userid = '';

	public function __construct() {
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
	}


	/*
   * Default home page.
   */
	public function home() {

		global $tpl;


    //設定 Action 相關參數
		set_status($this->controller);

		$to = "_to=/site/scode/";
		$url = BASE_URL.APP_DIR."/oauthapi/r/?_authby=line&".$to;
		error_log("[newscode/home] url :".$url);
		header("Location:".$url);
		break;

	}

}
