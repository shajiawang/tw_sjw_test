<?php

class About {

	public $controller = array();
	public $params = array();
	public $id;

	public function __construct() {

		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
		$this->datetime = date('Y-m-d H:i:s');

	}

	/*
	 *	關於殺價首頁
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 */
	public function home()	{

		global $tpl, $showoff;

		//設定 Action 相關參數
		set_status($this->controller);

		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("about","home",true);
	}

	public function privacy() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$noheader = htmlspecialchars($_REQUEST['json']);
		
		$data = $user->getNews(1,'N');
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->assign('data', $data[0]['description']);
		$tpl->render("about","privacy",true,false,$noheader);
	}

	public function service() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$noheader = htmlspecialchars($_REQUEST['json']);
		
		$data = $user->getNews(2,'N');
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->assign('data', $data[0]['description']);
		$tpl->render("about","service",true,false,$noheader);
	}

	public function usage() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$noheader = htmlspecialchars($_REQUEST['json']);
		
		$data = $user->getNews(3,'N');
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->assign('data', $data[0]['description']);
		$tpl->render("about","usage",true,false,$noheader);
	}

}
?>
