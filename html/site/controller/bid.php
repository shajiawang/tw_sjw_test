<?php
/*
 * Bid Controller //最新成交(殺價得標、流標)
 */

include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/websocket/WebsocketClient.php");
include_once(LIB_DIR."/helpers.php");

class Bid {

	public $controller = array();
	public $params = array();
	public $id;

  /*
   *	最新成交清單列表
   *	$type			varchar			成交狀態
   * 	$genHTML		varchar			建立新頁參數
   */
	public function home() 
	{
    global $tpl, $bid, $product;

    //設定 Action 相關參數
    set_status($this->controller);

    //成交狀態
    $type = empty($_POST['type'])?htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
    $tpl->assign('type', $type);
    $userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
    $keyword=empty($_POST['keyword'])?htmlspecialchars($_GET['keyword']) : htmlspecialchars($_POST['keyword']);
    $ymd=empty($_POST['ymd'])?htmlspecialchars($_GET['ymd']) : htmlspecialchars($_POST['ymd']);
    $page_number = empty($_POST['p'])?htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
    $ctype 	= (empty($_POST['ctype'])) ? htmlspecialchars($_GET['ctype']) : htmlspecialchars($_POST['ctype']);
    //建立bid info HTML靜態頁面
    $genHTML=$_GET['genHTML'];
    $product_list=null;
    //判斷成交狀態參數
    if (empty($type) || $type == 'deal') {	//得標

      //取得成交清單，目前只顯示金額大於98的資料
      //$product_list = $bid->product_list();
	  $product_list = $bid->recently_closed_product_list($page_number,$keyword,$ymd,$ctype);
      if($genHTML=='QazwSxedC') {
        foreach($product_list['table']['record'] as $product) {
          echo "productid : ".$product['productid']."<br>";
          $_GET['productid']=$product['productid'];
          $this->genBidDetailHtml();
        }
        exit;
      }

    }else{	//流標

      $product_list = $bid->product_cancelled_list();

    }

    if($product_list) {

		for($idx=0; $idx< count($product_list['table']['record']); ++$idx) {
			$p=$product_list['table']['record'][$idx];
			//圓夢商品
			if($p['ptype'] == 1){
				//撈取得標者上傳的商品圖檔
				$history_detail = $bid->get_history_detail($p['productid'], $p['userid'], (int)$p['price']);
				if($history_detail){
					$product_list['table']['record'][$idx]['name']= lan_str('DREAM').lan_str('PRODUCTS') ."(". $history_detail['title'] .")"; //"圓夢商品(".$history_detail['title'].")";
					$product_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/dream/products/".$history_detail['shd_thumbnail_filename'];
				}else{
					$product_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$p['thumbnail'];
				}
				$product_list['table']['record'][$idx]['thumbnail'] = '';
				$product_list['table']['record'][$idx]['offtime']=(int)$p['offtime'];
				$product_list['table']['record'][$idx]['now']=(int)$p['now'];
			//殺戮商品
			}else{
				if(!empty($p['thumbnail'])) {
					$product_list['table']['record'][$idx]['thumbnail_url']=BASE_URL.APP_DIR."/images/site/product/".$p['thumbnail'];
					$product_list['table']['record'][$idx]['offtime']=(int)$p['offtime'];
					$product_list['table']['record'][$idx]['now']=(int)$p['now'];
				}
			}
		}

    }

    $json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
    $p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

    if($json=='Y') {
      $ret=getRetJSONArray(1,'OK','LIST');
      if((empty($p)) || ($p=='') || ($p < 0) || ($p > $product_list['table']['page']['totalpages'])){
        $ret['retObj']['data'] = '';
      }else{
        $ret['retObj']['data'] = $product_list['table']['record'];
        $ret['retObj']['page'] = $product_list['table']['page'];
      }
      echo json_encode($ret);
      exit;

    }

    $tpl->assign('product_list', $product_list);

    //設定分頁
    if(empty($product_list['table']['page']) ){

      $page_content = array();

    }else{

      $page_path = $tpl->variables['status']['status']['path'];
      $page_content = $this->set_page($product_list, $page_path);

    }

    $meta['title']= lan_str('NEW_GETBID'); //"最新得標";
    $meta['description']='';
    $tpl->assign('meta',$meta);

    $tpl->assign('page_content', $page_content);
    $tpl->set_title('');
    $tpl->render("bid","home",true);

  }

	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

		$table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'].'&type='.$_GET['type'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'].'&type='.$_GET['type'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';

		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			//$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >'. lan_str('STEP_PAGE', 0, $pv['p']) .'</option>';
		}

		return $page_content;

  }

  //取得最新得標的分類項
  public function getCategory(){
  	$result=array();
  	$ret=array();
/*  	$ret[]=array('CategoryID'=>'0','CategoryName'=>'大檔');
  	$ret[]=array('CategoryID'=>'1','CategoryName'=>'圓夢');
  	$ret[]=array('CategoryID'=>'2','CategoryName'=>'新手');
  	$ret[]=array('CategoryID'=>'3','CategoryName'=>'老手');
  	$ret[]=array('CategoryID'=>'4','CategoryName'=>'閃殺');*/
  	$ret[]=array('CategoryID'=>'0','CategoryName'=>'按時間');
  	$ret[]=array('CategoryID'=>'1','CategoryName'=>'按金額');
  	$result['retCode']=1;
  	$result['retObj']=$ret;
  	echo json_encode($result);
  	die();
  }

	//最新成交(殺價中標、流標)商品
  public function detail() {

		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = htmlentities($_GET['type']);
		$productid=htmlentities($_GET['productid']);
		$channelid=htmlentities($_GET['channelid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		if(empty($productid)){
			return_to('site/bid');
		} else {
				if(file_exists('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html')){
					// Static bid detail page file exists
					//error_log("[bid/detail] Show /var/www/html".APP_DIR."/static/html/BidDetail_".$productid.".html ");
				}else{
					// Static bid detail page does not exist, generate Static Html Page
					//$this->genBidDetailHtml();
				}


			//Generate Static Html Page
			$DetailHtml = $this->BidDetailHtml();
			var_dump($productid,$_GET['productid']);
			$product = $bid->get_info($productid);
			var_dump($product['chainstatus']);
			if ($product['chainstatus']=='Y') $this->transToBlockChain();
			if(!empty($product['thumbnail'])){
				$product['img_src'] = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}elseif(!empty($product['thumbnail_url'])){
				$product['img_src'] = $product['thumbnail_url'];
			}

		}

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$etherscan_prefix=(strpos($_SERVER['SERVER_NAME'],'saja.com.tw')>0)?'https://etherscan.io/tx/':'https://rinkeby.etherscan.io/tx/';
		if ($product['tx_hash']!='') $product['tx_hash'] = $etherscan_prefix.$product['tx_hash'];

		// 檢查用戶是否有下標
		$tpl->assign('user_bid_count', $this->user_bid_count($userid, $productid));
		$tpl->assign('type', $type);
		$tpl->assign('product', $product);
		$tpl->assign('productid', $productid);
		$tpl->assign('channelid', $channelid);
		$tpl->assign('navbar_html', $DetailHtml['navbar']);
		$tpl->assign('bidSelf_html', $DetailHtml['bidSelf']);
		$tpl->assign('bidList_html', $DetailHtml['bidList']);
		$tpl->set_title('');

		$meta['title']=$product['name'];
		$meta['description']='';
		$meta['image'] = $product['img_src'];
		$tpl->assign('meta',$meta);
		$tpl->render("bid","bid_detail",true,'',$json);
	}

	public function dailyBlockChain(){
		global $tpl, $bid, $member;
		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$i = 0;
		$user_bid_count = 0;
		$bid_list=array();
		$targetlist=array();
		//cntdate如果沒有參數就抓當天的
		$cntdate=htmlspecialchars($_REQUEST['cntdate']);
		if ($cntdate=='') $cntdate=date("Y-m-d",strtotime("-1 day"));
		$ckproduct=$bid->getDailybidded($cntdate);
		for ($k=0;$k<sizeof($ckproduct);$k++){
			//商品資料
			$productid=$ckproduct[$k]['productid'];
			$targetlist[]=$productid;
			$product = $bid->get_info($productid);
			//判斷若得標商品無資料,則直接查詢商品資料
			if (empty($product['productid'])){
				die();
			}else{
				//下標人數&下標金額
				if (!empty($productid) ){
					$_bid_Arr = $bid->get_onbid_list($productid, '', -1);
					$get_bid_Arr = empty($_bid_Arr['table']['record']) ? null : $_bid_Arr['table']['record'];
				}
				if(is_array($get_bid_Arr)){
					foreach($get_bid_Arr as $key => $value){
						$bid_info[$key]['price'] = $value['price'];
						$bid_info[$key]['num'] = $value['count'];
						$bid_list[]=(array($ckproduct[$k]['productid']."_".round($value['price'])=>$value['count']*1));
					}
					//$user_bid_count += count($bid_info); //總數
				}
			}
		}
		if (sizeof($ckproduct)>0){
			$user_bid_count = count($bid_info);

			/*
			測試鍊和正式鋉to_Address
			api domain
			api url
	        api key 都和正式的不同誤混用
			*/
			$rtn=array();
			$assets=array();
			$assets['use_ipfs']=true;
			$assets['to_address']= "0x6475aE9D4f7336a279Ad0e9E8abE2d728d21802a";
			$assets['contract_address']= "0x5bCf93AE0E3dA550daeDc39Fdc5353ddd5Cf3C8c";
			$assets['name']=$cntdate."得標商品競價明細";
			$assets['description']= "商品編號_下標金額:組數";
			$assets['image_url']='http://test.shajiawang.com/site/images/site/product/a3967663e7bfd1d2a4af3d2661785d93.png';
			$assets['background_color']='ffffff';
			$attributes_map=array();
			$attributes_map['winnerName']='';
			$attributes_map['productName']=$cntdate."得標商品競價明細";
			$attributes_map['bidPrice']=0;
			$attributes_map['productId']=0;
			$attributes_map['winnerUserId']=0;
			$attributes_map['onTime']=$cntdate." 00:00:00";
			$attributes_map['offTime']=$cntdate." 23:59:59";
			$attributes_map['bidDetail']=json_encode($bid_list);
			$assets['attributes_map']=$attributes_map;
			$rtn['assets']=array($assets);
			if (file_exists( '/opt/php_errors.log' )){
				$fp = fopen('/opt/json'.$cntdate.'.json', 'w');
			}else{
				$fp = fopen('/tmp/json'.$cntdate.'.json', 'w');
			}
			fwrite($fp, json_encode($rtn));
			fclose($fp);
			$data = $this->httpRequest('http://api.saja.com.tw/api/blockchain/bh.php', ($rtn));
			//var_dump($data);
			$bid->updateBlockChain(0,'S',$data['token_id'],implode(",",$targetlist),$data['tx_hash'],$data['ipfs_hash']);
		}
	}

	private function transToBlockChain() {
		global $tpl, $bid, $member;
		//設定 Action 相關參數
		set_status($this->controller);
		//避免在上鏈未完成前被重覆呼叫先mark為執行中

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$bid->updateBlockChain($productid,'Q',0);
		$startprice = empty($_GET['startprice']) ? 1 : htmlspecialchars($_GET['startprice']);

		//商品資料
		$product = $bid->get_info($productid);


		//判斷若得標商品無資料,則直接查詢商品資料
		if (empty($product['productid'])){
			die();
			//$product = $bid->get_cancelled_info($productid);
		}

		//下標人數&下標金額
		//$get_bid_Arr = $bid->get_bid_info($productid);
		if (!empty($productid) ){
			$_bid_Arr = $bid->get_onbid_list($productid, '', -1);
			$get_bid_Arr = empty($_bid_Arr['table']['record']) ? null : $_bid_Arr['table']['record'];
		}

		$i = 0;
		$user_bid_count = 0;
		$bid_list=array();
		if(is_array($get_bid_Arr)){
			foreach($get_bid_Arr as $key => $value){
				$bid_info[$key]['price'] = $value['price'];
				$bid_info[$key]['num'] = $value['count'];
				$bid_list[]=(array(round($value['price'])=>$value['count']*1));
			}

			$user_bid_count = count($bid_info); //總數
		}
		//商品圖示
		//圓夢商品
		if($product['ptype'] == 1){
			//撈取得標者上傳的商品圖檔
			$history_detail = $bid->get_history_detail($productid, $product['userid'], (int)$product['price']);
			if($history_detail){
				$product['name'] = lan_str('DREAM').lan_str('PRODUCTS') ."(". $history_detail['title'] .")";//"圓夢商品(".$history_detail['title'].")";
				$img_src = BASE_URL.APP_DIR."/images/dream/products/".$history_detail['shd_thumbnail_filename'];
			}else{
				$img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}
		}else{
		//殺戮商品
			if(!empty($product['thumbnail'])) {
				$img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}elseif(!empty($product['thumbnail_url'])){
				$img_src = $product['thumbnail_url'];
			}
		}
		/*
		測試鍊和正式鋉to_Address
		api domain
		api url
        api key 都和正式的不同誤混用
		*/
		var_dump(385);
		$rtn=array();
		$assets=array();
		$assets['use_ipfs']=true;
		//$assets['to_address']= "0x5912fC17aA79d5e38D6B189fEE6e68176b6C17F5";
		//
		$assets['to_address']= "0x6475aE9D4f7336a279Ad0e9E8abE2d728d21802a";
		$assets['contract_address']= "0x5bCf93AE0E3dA550daeDc39Fdc5353ddd5Cf3C8c";
		$assets['name']=$product['name'];
		$assets['description']= $product['name'];
		$assets['image_url']=$img_src;
		$assets['background_color']='ffffff';
		$attributes_map=array();
		$attributes_map['winnerName']='';
		$attributes_map['productName']=$product['name'];
		$attributes_map['bidPrice']=$product['price'];
		$attributes_map['productId']=$product['productid'];
		$attributes_map['winnerUserId']=$product['userid'];
		$attributes_map['onTime']=$product['ontime'];
		$attributes_map['offTime']=date("Y-m-d H:i:s",$product['offtime']);
		$attributes_map['bidDetail']=json_encode($bid_list);
		$assets['attributes_map']=$attributes_map;
		$rtn['assets']=array($assets);
		var_dump(408);
		$fp = fopen('/tmp/json'.$product['productid'].'.json', 'w');
		fwrite($fp, json_encode($rtn));
		fclose($fp);
		$data = $this->httpRequest('http://api.saja.com.tw/api/blockchain/bh.php', ($rtn));
		$bid->updateBlockChain($productid,'S',$data['token_id'],'',$data['tx_hash'],$data['ipfs_hash']);
		error_log("[bid/updateBlockChain]".json_encode($data));
		var_dump(414);
		//$bid->updateBlockChain($productid,'Q',$data['assets'][0]['token_id']);
		/*$data = $this->httpRequest('https://api.rinkeby.forge.lootex.dev/v1/contracts/0xb45e04e782726bd497dbe46255512f682bb5bc76/assets/batch', json_encode($rtn));
		$bid->updateBlockChain($productid,'Q',$data['assets'][0]['token_id']);*/

	}

	private function httpRequest($api, $data) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $api);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		curl_close($ch);
		return json_decode($result, true);
	}

	private function BidDetailHtml() {

		global $tpl, $bid, $member;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$startprice = empty($_GET['startprice']) ? 1 : htmlspecialchars($_GET['startprice']);

		//商品資料
		$product = $bid->get_info($productid);


		//判斷若得標商品無資料,則直接查詢商品資料
		if (empty($product['productid'])){
			$product = $bid->get_cancelled_info($productid);
		}

		//下標人數&下標金額
		//$get_bid_Arr = $bid->get_bid_info($productid);
		if (!empty($productid) ){
			$_bid_Arr = $bid->get_onbid_list($productid, '', $startprice);
			$get_bid_Arr = empty($_bid_Arr['table']['record']) ? null : $_bid_Arr['table']['record'];
		}

		$i = 0;
		$user_bid_count = 0;
		if(is_array($get_bid_Arr)){
			foreach($get_bid_Arr as $key => $value){
				/*
				if($value['price'] <= $product['price']){ $bid_info[$key] = $get_bid_Arr[$key]; }
				else{
					if($i < 10){ $bid_info[$key] = $get_bid_Arr[$key]; $i++; }
				}*/
				$bid_info[$key]['price'] = $value['price'];
				$bid_info[$key]['num'] = $value['count'];
			}

			$user_bid_count = count($bid_info); //總數
		}

		$range = 500;	//區間
		$user_bid_max = round($bid->user_bid_max('', $productid));	//下標最大金額
		$bid_count = ceil($user_bid_max / $range);	//區間數
		$output_push = array( 0=>array('start'=>1, 'end'=>$range) );	//儲存各區間
		$html['navbar']= '';

		if($bid_count >1){
			$html['navbar'].='<!--  navbar 區間  --><div class="collapse navbar-collapse" id="myNavbar"><ul class="nav navbar-nav">';

				for($i=0; $i < $bid_count; $i++){
					//$remainder = $i % 3;
					$bid_start = ($i==0) ? 1 : ($i * $range)+1;
					$bid_end = ($i+1) * $range;
					$output_push[$i] = array('start'=>$bid_start, 'end'=>$bid_end);

					$link = BASE_URL.APP_DIR .'/bid/detail/?productid='. $productid .'&startprice='. $bid_start .'&type=';

					$act = (intval($startprice)==intval($bid_start)) ? ' active' : ' ';
					$done_str = (($bid_start < $startprice-($range*2)) ) ? ' li_show' : ' ';
					$html['navbar'].='<li class="'. $done_str .'"><a class="'. $act .'" href="javascript:void(0)" onclick="location.replace(\''. $link .'\')">'. $bid_start.' - '. $bid_end .'</a></li>';
				}

			$html['navbar'].='</ul></div>';
		}


			/*
			if(!empty($type) && $type =='NG'){
					var_dump($html['navbar']);
					exit;
			}
			*/


		//會員圖示
		$get_member = $member->get_info($product['userid']);

		if(!empty($get_member['thumbnail_file'])){
            $userImg = BASE_URL .'/site/images/headimgs/'. $get_member['thumbnail_file'];
		}elseif(!empty($get_member['thumbnail_url'])){
			$userImg = $get_member['thumbnail_url'];
		} else {
			$userImg = APP_DIR .'/static/img/bid-member.jpg';
		}

		$html['bidSelf']= '<!--  成交商品資訊  -->'.
			'<input id="allbidcount" type="hidden" value="'.$user_bid_count.'" >'.
			'<ul class="listsimg-box-group ">'.
			'<li class="listsimg-box">'.
			'<div class="listsimg-box-solid">'.
			'<div class="listsimg-header">';

		$html['bidSelf'].= '<div class="d-flex align-items-center">'.
              '<div class="bid-user d-inlineflex align-items-center mr-auto">'.
              '<div class="bid-img">'.
              '<img class="img-fluid" src="'. $userImg .'">'.
              '</div>'.
              '<div class="bid-userName">';

        $html['bidSelf'].= urldecode($product['nickname']).
                '</div>'.
                '</div>'.
                '<div class="bid-price">'.
                '<span>得標金額</span><span>NT</span><span>'. $product['price'].'</span><span>元</span>'.
                '</div>';
		$html['bidSelf'].= '</div>';

		$html['bidSelf'].= '</div>'.
				'<div class="listsimg-contant d-flex align-items-start">'.
				'<div class="listimg_img">'.
				'<i class="d-flex align-items-center">';

		//商品圖示
		//圓夢商品
		if($product['ptype'] == 1){
			//撈取得標者上傳的商品圖檔
			$history_detail = $bid->get_history_detail($productid, $product['userid'], (int)$product['price']);
			if($history_detail){
				$product['name'] = lan_str('DREAM').lan_str('PRODUCTS') ."(". $history_detail['title'] .")"; //"圓夢商品(".$history_detail['title'].")";
				$img_src = BASE_URL.APP_DIR."/images/dream/products/".$history_detail['shd_thumbnail_filename'];
			}else{
				$img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}
		}else{
		//殺戮商品
			if(!empty($product['thumbnail'])) {
				$img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
			}elseif(!empty($product['thumbnail_url'])){
				$img_src = $product['thumbnail_url'];
			}
		}
  	$hashstr='';
  	if ($product['tx_hash']!=''){
		$etherscan_prefix=(strpos($_SERVER['SERVER_NAME'],'saja.com.tw')>0)?'https://etherscan.io/tx/':'https://rinkeby.etherscan.io/tx/';
		if ($product['tx_hash']!='') $product['tx_hash'] = $etherscan_prefix.$product['tx_hash'];

		$hashstr="<div style='line-height:3rem;' onclick='javascript:window.open(\"".$product['tx_hash']."\",\"_blank\");'>Ethereum區塊鏈記錄</div>";

  	}
		$html['bidSelf'].= '<img src="'.$img_src.'">'.
            '</i>'.
            '</div>'.
            '<div class="listimg-info">'.
            '<p class="pro-name">'.$product['name'].'</p>'.
            '<p class="pro-price">'.
            '<span>官方售價：NT </span><span>'.$product['retail_price'].'</span><span> 元</span>'.
            '</p>'.$hashstr.
            '</div>'.
            '</div>'.
            '<div class="listimg-time d-flex">'.
            '<div class="ml-auto">'.
            '<span>'. $product['insertt'].'</span>'.
            '</div>'.
            '</div>'.
			'</div>'.
            '</li>'.
            '</ul>';


		//競標資訊
		if(is_array($bid_info)){
		$html['bidList']= '<!--  競標資訊  --><ul>';

			foreach($bid_info as $rk => $rv){

			if ($rv['num']==1 && $product['price'] == $rv['price']){
			$html['bidList'].= '<li class="bidList d-flex listMark">';
			}else{
			$html['bidList'].= '<li class="bidList d-flex">';
			}

			$html['bidList'].= '<div class="list-data">'.round($rv['price']).'</div><div class="list-data">'.$rv['num'].'</div>';
			$html['bidList'].= '</li>';

			}

		$html['bidList'].= '</ul>';
		}

		return $html;
	}


  public function genBidDetailHtml() {

		global $tpl, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$productid=empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$channelid=empty($_GET['channelid']) ? htmlspecialchars($_POST['channelid']) : htmlspecialchars($_GET['channelid']);
		// 由scheduler 呼叫的
		$bg=empty($_GET['bg']) ? htmlspecialchars($_POST['bg']) : htmlspecialchars($_GET['bg']);

		$ret="OK";

		//成交狀態
		if (empty($type) || $type == 'deal') {
      //商品資料
			$product = $bid->get_info($productid);

      //判斷若得標商品無資料,則直接查詢商品資料
			if (empty($product['productid'])){
				$product = $bid->get_cancelled_info($productid);
			}

			//下標人數&下標金額
			$get_bid_Arr = $bid->get_bid_info($productid);
			$i = 0;

			if(is_array($get_bid_Arr)){
				foreach($get_bid_Arr as $key => $value){
					if($value['price'] <= $product['price']){
						$bid_info[$key] = $get_bid_Arr[$key];
					}else{
						if($i < 10){
							$bid_info[$key] = $get_bid_Arr[$key];
							$i++;
						}
					}
				}
			}

		}else{

      //商品資料
			$product = $bid->get_cancelled_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_cancelled_bid_info($productid);

		}

		$html= '<div class="article">'.
			'<!--  成交商品資訊  -->'.
			'<input id="allbidcount" type="hidden" value="'.count($bid_info).'" >'.
			'<ul class="listsimg-box-group bidDetail">'.
			'<li class="listsimg-box">'.
			'<div class="listsimg-box-solid">'.
			'<div class="listsimg-header">';

    if($type == '' || $type == 'deal' ){

		$html.= '<div class="d-flex align-items-center">'.
              '<div class="bid-user d-inlineflex align-items-center mr-auto">'.
              '<div class="bid-img">'.
              '<img class="img-fluid" src="'.APP_DIR.'/static/img/bid-member.jpg">'.
              '</div>'.
              '<div class="bid-userName">';

	if(!empty($product['nickname'])){
        $html.= urldecode($product['nickname']).
                '</div>'.
                '</div>'.
                '<div class="bid-price">'.
                '<span>得標金額</span><span>NT</span><span>'. $product['price'].'</span><span>元</span>'.
                '</div>';
      }else{
        $html.= '無人得標'.
                '</div>'.
                '</div>';
      }
      $html.= '</div>';

    }else{

      $html.= '<div class="d-flex align-items-center">'.
              '<div class="nobid-reason"><span>流標原因：</span><span>'.$product['memo'].'</span></div>'.
              '</div>';

    }

    $html.= '</div>'.
            '<div class="listsimg-contant d-flex align-items-start">'.
            '<div class="listimg_img">'.
            '<i class="d-flex align-items-center">';

  	//商品圖示
    if(!empty($product['thumbnail'])) {
      $img_src = BASE_URL.APP_DIR.'/images/site/product/'.$product['thumbnail'];
  	}elseif(!empty($product['thumbnail_url'])){
  	  $img_src = $product['thumbnail_url'];
  	}

    $html.= '<img src="'.$img_src.'">'.
            '</i>'.
            '</div>'.
            '<div class="listimg-info">'.
            '<p class="pro-name">'.$product['name'].'</p>'.
            '<p class="pro-price">'.
            '<span>官方售價：NT </span><span>'.$product['retail_price'].'</span><span> 元</span>'.
            '</p>'.
            '</div>'.
            '</div>'.
            '<div class="listimg-time d-flex">'.
            '<div class="ml-auto">'.
            '<span>'. $product['insertt'].'</span>'.
            '</div>'.
            '</div>'.
			'</div>'.
            '</li>'.
            '</ul>';

    //競標資訊
    if(is_array($bid_info)){
      $html.= '<!--  競標資訊  -->'.
              '<ul class="bidList-group">'.
              '<div class="bidList-header d-flex">'.
              '<div class="header-title">下標金額 (元)</div>'.
              '<div class="header-title">下標人數 (人)</div>'.
              '</div>';
      foreach($bid_info as $rk => $rv){
        if ($product['price'] == $rv['price']){
          $html.= '<li class="bidList d-flex listMark">'.
                  '<div class="list-data">'.round($rv['price']).' 元</div>'.
                  '<div class="list-data">'.$rv['num'].' 人</div>'.
                  '</li>';
        }else{
          $html.= '<li class="bidList d-flex">'.
                  '<div class="list-data">'.round($rv['price']).'</div>'.
                  '<div class="list-data">'.$rv['num'].'</div>'.
                  '</li>';
        }
      }
      $html.= '</ul>';
    }

    $html.= '</div><!-- /article -->';

    try {
         // /static/html目錄必須設定為setGid
  			 $fp = fopen('/var/www/html'.APP_DIR.'/static/html/BidDetail_'.$productid.'.html', 'w+');
  			 fwrite($fp, $html);
  			 fclose($fp);
    }catch(Exception $e){
  			 error_log($e->getMessage());
  			 $ret=$e->getMessage();
  	}finally{
  	  if($bg=='Y'){
  		  echo $ret;
  		}else{
  		  return $ret;
  		}
  	}

  }


	//下標金標數統計
	public function getBidHistory() {

		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		$productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid		= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$type 		= empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$page 	 	= empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$perpage 	= empty($_POST['perpage']) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);

		//取得商品的相關資料
		$proinfo = $product->get_info($productid);

		//判斷此商品的下標狀態是否在 closed=’Y’ (已結標) 或 closed=’NB’ (流標)
		if (($proinfo['closed'] == 'Y') || ($proinfo['closed'] == 'NB')){

			if ($type == 'A'){
				//取得該商品的使用者下標資料
				$bid_check = $bid->getUserSajaHistory($userid, $productid);
				error_log("[bid/getBidHistory] uid = ".$userid." pid = ".$productid);

				//判斷是否有下標過此商品
				if (!empty($bid_check)) {
					//取得下標金標數統計
					$bid_list = $bid->get_bid_list($productid, $type, $page, $perpage);
				}else{
					$bid_list = array();
				}
			}else{
				$bid_list = $bid->get_bid_list($productid, $type);
			}

			error_log("[bid/getBidHistory] type = ".$type);

			if($json=='Y') {
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = (!empty($bid_list['record'])) ? $bid_list['record'] : array();
				$ret['retObj']['page'] = (!empty($bid_list['page'])) ? $bid_list['page'] : new stdClass;
				echo json_encode($ret);
				exit;
			}else{
				$tpl->assign('bid_list', $bid_list);

				//設定分頁
				if(empty($bid_list['table']['page']) ){
					$page_content = array();
				}else{
					$page_path = $tpl->variables['status']['status']['path'];
					$page_content = $this->set_page($bid_list, $page_path);
				}
				$tpl->assign('page_content', $page_content);
				$tpl->set_title('');
				$tpl->render("bid","getBidHistory",true);
			}

		}else{
			if($json=='Y') {
				$ret=getRetJSONArray(-1,'ERROR','MSG');
				echo json_encode($ret);
				exit;
			}else{
				$tpl->assign('type', $type);
				$tpl->assign('productid', $productid);
				$tpl->set_title('');
				$tpl->render("bid","home",true);
			}
		}

	}


	public function user_bid_count($userid, $productid) {
    global $tpl, $bid, $product;

		  //設定 Action 相關參數
		  set_status($this->controller);

		  if(!empty($userid) && !empty($productid)) {
		    return $bid->user_bid_count($userid, $productid);
		  }
	    return 0;
	}


	/*
	 *	下標清單
	 *	$type					varchar				狀態
	 *	$page					int					頁碼
	 *	$perpage				int					每頁個數
	 *	$productid				int					商品編號
	 *	$channelid				varchar
	 *	$userid					int					會員編號
	 */
	public function bidlist() {

		global $tpl, $bid ;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$type = empty($_GET['type'])? htmlspecialchars($_POST['type']) : htmlspecialchars($_GET['type']);
		$page = empty($_GET['p'])? htmlspecialchars($_POST['p']) : htmlspecialchars($_GET['p']);
		$perpage = empty($_GET['perpage'])? htmlspecialchars($_POST['perpage']) : htmlspecialchars($_GET['perpage']);
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$channelid = empty($_GET['channelid']) ? htmlspecialchars($_POST['channelid']) : htmlspecialchars($_GET['channelid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		// 由scheduler 呼叫的
		$bg=empty($_GET['bg']) ? htmlspecialchars($_POST['bg']) : htmlspecialchars($_GET['bg']);

		$ret="OK";

		//判斷是否有登入會員
		// login_required();

		//成交狀態
		if (empty($type) || $type == 'deal') {
			//商品資料
			$product = $bid->get_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_bid_list($productid, 'A', $page, $perpage);
		} elseif ($type == 'NB') {
			//商品資料
			$product = $bid->get_cancelled_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_bid_list($productid, 'A', $page, $perpage);
		} else {
			//商品資料
			$product = $bid->get_info($productid);
			//下標人數&下標金額
			$bid_info = $bid->get_bid_list($productid, 'A', $page, $perpage);
			error_log("[bid/bidlist] product = ".json_encode($product));
		}

		//圓夢商品
		if($product['ptype'] == 1){
			//撈取得標者上傳的商品圖檔
			$history_detail = $bid->get_history_detail($productid, $product['userid'], (int)$product['price']);
			if($history_detail){
				$product['name'] = lan_str('DREAM').lan_str('PRODUCTS') ."(". $history_detail['title'] .")"; //"圓夢商品(".$history_detail['title'].")";
				$product['thumbnail_url'] = BASE_URL.APP_DIR."/images/dream/products/".$history_detail['shd_thumbnail_filename'];
			}else{
				$product['thumbnail_url'] = BASE_URL.APP_DIR."/images/site/product/".$product['thumbnail'];
			}
			$product['thumbnail'] = '';
		}else{
		//殺戮商品
			if(!empty($product['thumbnail'])){
				$product['thumbnail_url'] = BASE_URL.APP_DIR."/images/site/product/".$product['thumbnail'];
			}
		}

		if(!empty($product['m_thumbnail_file'])){
			$product['m_thumbnail_url'] = IMG_URL.HEADIMGS_DIR."/".$product['m_thumbnail_file'];
		}

		if(empty($product['m_thumbnail_file']) && empty($product['m_thumbnail_url'])){
			$product['m_thumbnail_url'] = IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg";
		}

		//設定分頁
		if(empty($bid_info['page']) ) {
			$page_content = array();
		} else {
			$content['table']['record'] = $bid_info['record'];
			$content['table']['page'] = $bid_info['page'];
			$page_path = $tpl->variables['status']['status']['path']."&productid=".$productid."&perpage=".$perpage;
			$page_content = $this->set_page($content, $page_path);
		}

		if ($json=='Y') {
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $product;
			$ret['retObj']['user_bid_count'] = $bid->user_bid_count('', $productid);
			$ret['retObj']['user_bid_max'] = round($bid->user_bid_max('', $productid));

			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('user_bid_count', $bid->user_bid_count('', $productid));
			$tpl->assign('user_bid_max', round($bid->user_bid_max('', $productid)));
			$tpl->assign('type', $type);
			$tpl->assign('productid', $productid);
			$tpl->assign('channelid', $channelid);
			$tpl->assign('product', $product);
			$tpl->assign('bid_info', $bid_info['record']);
			$tpl->assign('page_content', $page_content);

			$meta['title']=$product['name'];
			$meta['description']='';
			if(!empty($product['thumbnail'])) {
				$meta['image'] = BASE_URL."/site/images/site/product/".$product['thumbnail'];
			} elseif (!empty($product['thumbnail_url'])) {
				$meta['image'] = $product['thumbnail_url'];
			}
			$tpl->assign('meta',$meta);

			$tpl->set_title('');
			$tpl->render("bid","detail_all",true);
		}
	}


	/*
	 *	個人競標中商品相關出價資料
	 *	$json	 				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 */
	public function userOnBidProductList() {

		global $tpl,$bid,$product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//判斷是否有登入會員
		login_required();

		if(empty($productid) || empty($userid)){
			return_to('site/bid');
		} else {
			//取得商品資料
			$product = $product->getProductById($productid);
			//取得下標最大金額
			$user_bid_max = $bid->user_bid_max($userid, $productid);
			//會員最後下標時間
			$user_bid_last = $bid->user_bid_last($userid, $productid);

			if ($json=='Y') {
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product;
				$ret['retObj']['user_bid_count'] = $this->user_bid_count($userid, $productid);
				$ret['retObj']['user_bid_max'] = $user_bid_max;
				$ret['retObj']['user_bid_last'] = $user_bid_last;
				echo json_encode($ret);
				exit;
			} else {
				$tpl->assign('user_bid_count', $this->user_bid_count($userid, $productid));
				$tpl->assign('user_bid_max', $user_bid_max);
				$tpl->assign('user_bid_last', $user_bid_last);
				$tpl->assign('product', $product);
				$tpl->set_title('');
				$tpl->render("bid","onbid",true);
			}
		}
	}


	/*
	 *	個人競標中商品出價紀錄
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 */
	public function getUserOnBidList() {

		global $tpl,$bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id']) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		$startprice = empty($_GET['startprice']) ? htmlspecialchars($_POST['startprice']) : htmlspecialchars($_GET['startprice']);

		//判斷是否有登入會員
		login_required();

		if (!empty($productid) && !empty($userid) && !empty($startprice)){
			//下標次數&下標金額
			$bid_info = $bid->get_onbid_list($productid, $userid, $startprice);

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $bid_info['table']['record'];
			// $ret['retObj']['page'] = $bid_info['table']['page'];
		}else{
			$ret=getRetJSONArray(-1,urlencode('資料有誤，請確認資料是否正確 !!'),'MSG');
		}
		echo json_encode($ret);
		exit;
	}


	/*
	 *	全商品出價紀錄
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 *	$startprice				int					開始下標價位
	 */
	public function getBidList() {

		global $tpl,$bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$startprice = empty($_GET['startprice']) ? htmlspecialchars($_POST['startprice']) : htmlspecialchars($_GET['startprice']);

		//判斷是否有登入會員
		// login_required();

		if (!empty($productid) && !empty($startprice)){
			//下標次數&下標金額
			$bid_info = $bid->get_onbid_list($productid, '', $startprice);

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $bid_info['table']['record'];
		}else{
			$ret=getRetJSONArray(-1,urlencode('資料有誤，請確認資料是否正確 !!'),'MSG');
		}
		echo json_encode($ret);
		exit;
	}


	/*
	 *	成功統計相關資料
	 *	$json	 				varchar				瀏覽工具判斷 (Y:APP來源)
   *	$reflash	 		varchar				Redis暫存更新 (Y:更新)
	 */
	public static function getBidAllTotalCount() {

		global $tpl, $bid;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$refresh = empty($_POST['refresh']) ? htmlspecialchars($_GET['refresh']) : htmlspecialchars($_POST['refresh']);

		//設定 Action 相關參數
		set_status($controller);

		//取得成功人數與成功節省金額

		//建立Redis連線
		$redis=getRedis();

		if($redis){//如果redis存在使用redis

			//成功人數
			$rdata = ''; //redis data 暫存預設
			$rkey="bid_user_count"; //設定redis key 名稱
			$rdata = $redis->get($rkey); //取得redis key對映值

			if(!empty($rdata) && $refresh != 'Y'){          //有redis值且不需刷新時用redis值
				$bid_user_count = $rdata;
				$bid_user_count_refreshed ='N';//成功人數cache刷新資料旗標
			}else{                      //沒值或要刷新時重新取值後存入redis
				$bid_user_count = $bid->bid_user_count(); //直接取得成功人數值
				$redis->set($rkey,$bid_user_count); //存入redis
				$bid_user_count_refreshed ='Y';//成功人數cache刷新資料旗標
			}

			//成功節省金額
			$rdata = ''; //redis data 暫存預設
			$rkey="bid_money_count"; //設定redis key 名稱
			$rdata = $redis->get($rkey); //取得redis key對映值

			if(!empty($rdata) && $refresh != 'Y'){         //有redis值且不需刷新時用redis值
				$bid_money_count = $rdata;
				$bid_money_count_refreshed ='N';//成功節省金額cache刷新資料旗標
			}else{                      //沒值或要刷新時重新取值後存入redis
				$bid_money_count = $bid->bid_money_count(); //直接取得成功人數值
				$redis->set($rkey,$bid_money_count); //存入redis
				$bid_money_count_refreshed ='Y';//成功節省金額cache刷新資料旗標
			}

			//輸出使用cache資料旗標
			$cached ='Y';

		}else{ //如果redis不存在直接取值

			//成功人數
			$bid_user_count = $bid->bid_user_count();
			//成功節省金額
			$bid_money_count = $bid->bid_money_count();
			//輸出無使用cache資料旗標
			$cached ='N';

		}

		//成功人數固定加110
		$bid_user_count = $bid_user_count + 163333;
		//成功節省金額固定加1700萬
		$bid_money_count = $bid_money_count + 17000000 + 250000000;

		$data['retCode'] = 1;
		$data['retMsg'] = "取得資料 !!";
		$data['retType'] = "LSIT";
		$data['cached'] = $cached;
		$data['bid_user_count_refreshed'] = $bid_user_count_refreshed;
		$data['bid_money_count_refreshed'] = $bid_money_count_refreshed;
		$data['retObj']['bid_user_start_text'] = "已有";
		$data['retObj']['bid_user_count'] = (int)$bid_user_count;
		$data['retObj']['bid_user_end_text'] = "人成功圓夢";
		$data['retObj']['bid_money_start_text'] = "圓夢金額 NT";
		$data['retObj']['bid_money_count'] = (int)$bid_money_count;
		$data['retObj']['bid_money_end_text'] = "";
		$data['retObj']['nowmicrotime'] = microtime(true);
		echo json_encode($data);

	}


	/*
	 *	個人商品相關出價資料
	 *	$json	 				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 *	$type					varchar				結標狀態
	 */
	public function userBidProductList() {

		global $tpl,$bid,$product;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);

		//判斷是否有登入會員
		login_required();

		if(empty($productid) || empty($userid)){
			return_to('site/bid');
		} else {
			//取得商品資料
			if ($type == 'NB'){
				$product = $bid->get_cancelled_info($productid);
			}else{
				$product = $bid->get_info($productid);
			}
			//取得下標最大金額
			$user_bid_max = $bid->user_bid_max($userid, $productid);
			//會員最後下標時間
			$user_bid_last = $bid->user_bid_last($userid, $productid);

			if ($json=='Y') {
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product;
				$ret['retObj']['user_bid_count'] = $this->user_bid_count($userid, $productid);
				$ret['retObj']['user_bid_max'] = $user_bid_max;
				$ret['retObj']['user_bid_last'] = $user_bid_last;
				echo json_encode($ret);
				exit;
			} else {
				$tpl->assign('user_bid_count', $this->user_bid_count($userid, $productid));
				$tpl->assign('user_bid_max', $user_bid_max);
				$tpl->assign('user_bid_last', $user_bid_last);
				$tpl->assign('product', $product);
				$tpl->assign('type', $type);
				$tpl->set_title('');
				$tpl->render("bid","bidclose",true);
			}
		}
	}


	/*
	 *	特定商品出價紀錄
	 *	$productid				int					商品編號
	 *	$userid					int					會員編號
	 *	$startprice				int					開始下標價位
	 */
	public function getUserBidList() {

		global $tpl,$bid;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$productid = empty($_GET['productid']) ? htmlspecialchars($_POST['productid']) : htmlspecialchars($_GET['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$startprice = empty($_GET['startprice']) ? htmlspecialchars($_POST['startprice']) : htmlspecialchars($_GET['startprice']);

		//判斷是否有登入會員
		login_required();

		if (!empty($productid) && !empty($startprice)){
			//下標次數&下標金額
			$bid_info = $bid->get_onbid_list($productid, $userid, $startprice);

			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $bid_info['table']['record'];
		}else{
			$ret=getRetJSONArray(-1,urlencode('資料有誤，請確認資料是否正確 !!'),'MSG');
		}
		echo json_encode($ret);
		exit;
	}

	// 最新成交清單列表
	public  function recently_closed_product_list() {
	         global $bid;
			 set_status($this->controller);
			 $reload = htmlspecialchars($_REQUEST['reload']);
			 if($reload!='Y')
			    $reload='N';
			 $rkey = "recently_closed_product_list";
			 $arr = '';
			 $ret = '';
			 $redis = getRedis();
			 if($reload=='N') {
			    if($redis) {
				   $ret=$redis->get($rkey);
				   if(!empty($ret)) {
					  return $ret;
				   }
			    }
		     }
			 $arr = $bid->recently_closed_product_list('','','','');
			 if(!empty($arr) && is_array($arr)) {
			    $ret = json_encode($arr);
		     }
		     if($redis) {
			    $redis->set($rkey,$ret);
		     }
		     return $ret;
    }

}
