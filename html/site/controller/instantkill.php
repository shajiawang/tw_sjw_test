<?php
/*
 * instantkill Controller 閃殺
 */

include_once(LIB_DIR ."/convertString.ini.php");

class instantkill {

	public $controller = array();
	public $params = array();
	public $id;


	/*
	 * 閃殺入口.
	 */
  public function home($productid='', $user_src='') {

		//商品編號
		$token	= empty($_POST['token']) ? htmlspecialchars($_GET['token']) : htmlspecialchars($_POST['token']);

    //$this -> user_bidding_room($token);
		$this -> play(8143);
		return;

		global $db, $config, $tpl, $broadcast;

		//設定 Action 相關參數
		set_status($this->controller);

		// 產品編號
		if(empty($productid)){
		  $productid  = htmlspecialchars($_REQUEST['productid']);
		}

		// 推薦人的userid
		if(empty($user_src)){
		  $user_src  = htmlspecialchars($_REQUEST['user_src']);
		}

		//登入id寫入log
		error_log("[instantkill/home] auth_id : ".$_SESSION['auth_id']);

		//**已登入**

		//導向閃殺頁面
		if(!empty($_SESSION['auth_id'])){

			if(empty($productid)){
	      $this->prod_list();      //沒有指定商品編號
			}else{
			  $this->play($productid); //有指定商品編號
			}
			return;

		}

		//**未登入**

		//取得瀏覽器類型(用helper內的 function browserType)
		$browserType=browserType();

		//設定第三方驗證返回網址
		$base_url=BASE_URL;
		$state['callback_url'] = $base_url.APP_DIR."/instantkill/regLogin/";

		//設定第三方驗證返回參數
		$state['user_src'] = $user_src;
		$state['productid'] = $productid;

    //依瀏覽器類型導向不同第三方登入入口
		switch($browserType){

			case 'fb' :   $url=$base_url."/site/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
		                error_log("[instantkill/home] FB auth url :".$url);
		                header("Location:".$url);
		                break;

			case 'line' : $url=$base_url."/site/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
		                error_log("[instantkill/home] Line auth url :".$url);
		                header("Location:".$url);
		                break;

			default :     echo "請使用fb或line掃碼登入使用!!";
			              break;

		}

		return ;

	}


	/*
	 *	註冊使用者
	 */
  public function regLogin() {

		global $db, $config, $tpl, $usermodel, $broadcast;

    $auth_by	= empty($_POST['auth_by']) ? htmlspecialchars($_GET['auth_by']) : htmlspecialchars($_POST['auth_by']);
    $sso_uid	= empty($_POST['sso_uid']) ? htmlspecialchars($_GET['sso_uid']) : htmlspecialchars($_POST['sso_uid']);
    $state   	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
    $data 		= empty($_POST['data']) ? htmlspecialchars($_GET['data']) : htmlspecialchars($_POST['data']);
    $sso_data 	= json_decode(urldecode(base64_decode($data)), true);

    error_log("[instantkill/regLogin] state : ".urldecode(base64_decode($state)));

    $arrState =  json_decode(urldecode(base64_decode($state)), true);
    $lbid=$arrState['lbid'];
    $lbuserid=$arrState['lbuserid'];
    $user_src=$arrState['user_src'];

		//讀取伺服器當前的 Unix 時間戳以微秒數(電話後方唯一碼)
		$ts = str_replace(".","",microtime(true));

    // 組合需要參數
		switch($auth_by){
			case 'line': $arrSSO = array(
											              'auth_by' 		=> $auth_by,
											              'sso_data' 		=> urldecode(base64_decode($data)),
											              'state' 		=> $state,
											              'nickname' 		=> $sso_data['displayName'],
											              'headimgurl'    => $sso_data['pictureUrl'],
											              'sso_name' 		=> "line",
											              'phone'         => "line_".$ts,
											              'type' 			=> "sso",
											              'json'          => "Y",
											              'gender'        => "N/A",
											              'sso_uid' 		=> $sso_uid,
											              'sso_uid2' 		=> "",
											              'user_src'      => $user_src
											            );
			            break;

			case 'fb':  $arrSSO = array(
											              'auth_by' 		=> $auth_by,
											              'sso_data' 		=> urldecode(base64_decode($data)),
											              'state' 		=> $state,
											              'nickname' 		=> $sso_data['name'],
											              'headimgurl' => $sso_data['picture']['url'],
											              'sso_name' 		=> "fb",
											              'phone'         => "fb_".$ts,
											              'type' 			=> "sso",
											              'json'          => "Y",
											              'gender'        => "N/A",
											              'sso_uid' 		=> $sso_uid,
											              'sso_uid2' 		=> "",
											              'user_src'      => $user_src
											            );
			            break;

			default :   $arrSSO = array();
			            break;
		}
    error_log("[instantkill/regLogin] oauth data : ".json_encode($arrSSO));

    //查詢是否已註冊
    $get_user = $broadcast->get_user($sso_uid, $auth_by, '');
    error_log("[instantkill/regLogin] get_user : ".json_encode($get_user));

		//未註冊直接幫他註冊
    if(empty($get_user)){

      $url = BASE_URL.APP_DIR."/ajax/user_register.php";
      //設定送出方式-POST
      $stream_options = array('http' =>
			                         array (
																	     'method' => "POST",
																			 'content' => json_encode($arrSSO),
																			 'header' => "Content-Type:application/json"
                                     )
                              );
      $context = stream_context_create($stream_options);

      // 送出json內容並取回結果
      $response = file_get_contents($url, false, $context);
      error_log("[instantkill/regLogin] response : ".$response);

      // 讀取json內容
      $arr = json_decode($response,true);

			//註冊成功 取得使用者user sso data
	    if($arr['retCode'] == 1){
        $get_user = $broadcast->get_user($sso_uid, $auth_by, '');
      }

	  }

		//使用者登入寫入session & cookie)
    $this->set_login($get_user);

    //傳送參數至頁面
    $tpl->assign('oauth', $arrSSO);

    //導向閃殺頁面
		if(empty($productid)){
			$this->prod_list();      //沒有指定商品編號
		}else{
			$this->play($productid); //有指定商品編號
		}
		return;

	}


	/*
	 *  閃殺地點清單
	 *	$auth_id  string		會員編號 (需登入 登入後系統自動抓session內auth_id值)
	 *	$date     string    查詢日期(格式:2018-11-22) (預設當天不需送參)
	 *	$json     string    輸出選擇 (Y:json N:網頁)
	 */
	public function choice_location() {

		global $db, $config, $tpl;

		//測試商家輸入
		$_SESSION['auth_id'] = "1705";
		//閃殺日期(2018-11-22)
		$date	= empty($_POST['date']) ? htmlspecialchars($_GET['date']) : htmlspecialchars($_POST['date']);
		//json輸出(Y:N)
		$json	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//設定 Action 相關參數
		set_status($this->controller);

		//檢查是否已登入
		if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']!= ""){

			//設定查詢商家
			$store_id = $_SESSION['auth_id'];

		}else{

			if($json == 'Y'){ //json輸出
				$data['retCode'] = -1;
				$data['retMsg'] = "請先登入";
				$data['retType'] = "MSG";
				echo json_encode($data,JSON_UNESCAPED_UNICODE);
			}else{
				echo "請先登入";
			}

			return;
		}

		//抓取系統時間
		$t=time();

		//如未輸入日期預設為當天
		if(empty($date) || $date == ""){
			$date = date("Y-m-d",$t);
		}
		$sdate = $date.' 00:00:00';//開始時間
		$edate = $date.' 23:59:59';//結束時間

    //如未輸入json參數預設為否
		if($json != 'Y'){
			$json = 'N';
		}

		//取得商家未結標閃殺商品資料
		$sql = "SELECT flash_loc
							FROM  `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
							WHERE `prefixid` = '{$config['default_prefix_id']}'
							  AND `vendorid` = '{$store_id}'
								AND `is_flash` = 'Y'
								AND `closed` = 'N'
								AND `ontime` >= '{$sdate}'
								AND `ontime` <= '{$edate}'
								AND `switch` =  'Y'
							GROUP BY `flash_loc`
							ORDER BY `ontime` ASC
					 ";

		error_log("[instantkill/show_room] bidding item info sql : ".$sql);

		$query = $db->getQueryRecord($sql);

		if($json == 'Y'){ //json輸出
			$data['locatios'] = $query['table']['record'];
			$data['auth_id']= $_SESSION['auth_id'];
			$data['date'] = $date;
			$data['retCode'] = 1;
			$data['retMsg'] = "查詢完成";
			$data['retType'] = "JSON";
			echo json_encode($data,JSON_UNESCAPED_UNICODE);
			return;
		}else{ //網頁輸出
			$tpl->set_title('');
			$tpl->assign('header','Y');
			$tpl->assign('footer','N');
			$tpl->assign('locatios',$query['table']['record']);
			$tpl->assign('auth_id',$_SESSION['auth_id']);
			$tpl->assign('date',$date);
			$tpl->render("instantkill","choice_location",true);
		}

	}


	/*
	 *  閃殺大廳
	 *	$auth_id   string		會員編號 (需登入 登入後系統自動抓session內auth_id值)
	 *	$date      string   查詢日期(格式:2018-11-22) (預設當天不需送參)
	 *  $flash_loc string   查詢閃殺地點
	 *	$json      string   輸出選擇 (Y:json N:網頁)
	 */
	public function show_room() {

		global $db, $config, $tpl;

		//測試商家輸入
		$_SESSION['auth_id'] = 1705;
		//閃殺日期
		$date = empty($_POST['date']) ? htmlspecialchars($_GET['date']) : htmlspecialchars($_POST['date']);
		//閃殺地點
		$flash_loc = empty($_POST['flash_loc']) ? htmlspecialchars($_GET['flash_loc']) : htmlspecialchars($_POST['flash_loc']);
		//json輸出(Y:N)
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//設定 Action 相關參數
		set_status($this->controller);

		//檢查是否已登入
		if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']!= ""){

			//設定查詢商家
			$store_id = $_SESSION['auth_id'];

		}else{

			if($json == 'Y'){ //json輸出
				$data['retCode'] = -1;
				$data['retMsg'] = "請先登入";
				$data['retType'] = "MSG";
				echo json_encode($data,JSON_UNESCAPED_UNICODE);
			}else{
				echo "請先登入";
			}

			return;
		}

		//抓取系統時間
		$t=time();
		$nowtime = date("Y-m-d H:i:s",$t);

		//如未輸入日期預設為當天
		if(empty($date) || $date == ""){
			$date = date("Y-m-d",$t);
		}
		$sdate = $date.' 00:00:00';//開始時間
		$edate = $date.' 23:59:59';//結束時間

		//檢查閃殺地點是否已輸入
		if(empty($flash_loc)){

			if($json == 'Y'){ //json輸出
				$data['retCode'] = -2;
				$data['retMsg'] = "請先選擇地點";
				$data['retType'] = "MSG";
				echo json_encode($data,JSON_UNESCAPED_UNICODE);
			}else{
				echo "請先選擇地點";
			}

			return;
		}

		//如未輸入json參數預設為否
		if($json != 'Y'){
			$json = 'N';
		}

		//取得商家未結標閃殺商品資料
		$sql = "SELECT *
							FROM  `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product`
							WHERE `prefixid` = '{$config['default_prefix_id']}'
							  AND `vendorid` = '{$store_id}'
								AND `is_flash` = 'Y'
								AND `closed` = 'N'
								AND `ontime` >= '{$sdate}'
								AND `ontime` <= '{$edate}'
								AND `flash_loc` = '{$flash_loc}'
								AND `switch` =  'Y'
							ORDER BY `ontime` ASC
					 ";

		//error_log("[instantkill/show_room] bidding item info sql : ".$sql);

		$query = $db->getQueryRecord($sql);

		//未結標商品總數
		$total_product_count = count($query['table']['record']);

		//取得商家最近一檔未結標閃殺商品資料
		$product_data = $query['table']['record'][0];

		//echo json_encode($product_data);
		//return;

		//閃殺限定時間
		$time_limit = 180;

		//傳送參數編碼
		//$arrPost = base64_encode(json_encode(array('store_id' => $store_id,'date' => $date,'flash_loc' => $flash_loc)));
		$arrPost = base64_encode("1705|2018-11-22|第一場");

		//設定第三方驗證返回網址
		$qr_value = BASE_URL.APP_DIR."/instantkill/?token=".$arrPost;

		if($json == 'Y'){ //json輸出 productid
			$data['flash_loc'] = $flash_loc;
			$data['productid'] = $product_data['productid'];
			$data['thumbnail_url'] = $product_data['thumbnail_url'];
			$data['product_name'] = $product_data['name'];
			$data['retail_price'] = $product_data['retail_price'];
			$data['time_limit'] = $time_limit;
			$data['now_time'] = $nowtime;
			$data['total_product_count'] = $total_product_count;
			$data['qr_value'] = $qr_value;
			$data['retCode'] = 1;
			$data['retMsg'] = "查詢完成";
			$data['retType'] = "JSON";
			echo json_encode($data,JSON_UNESCAPED_UNICODE);
		}else{ //網頁輸出
			$tpl->set_title('');
			$tpl->assign('header','N');
			$tpl->assign('footer','N');
			$tpl->assign('flash_loc',$flash_loc); //閃殺地點
			$tpl->assign('productid',$product_data['productid']); //商品編號
			$tpl->assign('thumbnail_url',$product_data['thumbnail_url']); //商品圖
			$tpl->assign('product_name',$product_data['name']); //商品名稱
			$tpl->assign('retail_price',$product_data['retail_price']); //商品市價
			$tpl->assign('time_limit',$time_limit);  //限時x秒
			$tpl->assign('now_time',$nowtime);  //現在時間
			$tpl->assign('total_product_count',$total_product_count);  //未結標商品總數
			$tpl->assign('qr_value',$qr_value);  //qrcode值
			$tpl->render("instantkill","show_room",true);
		}

		return;

	}


  /*
	 * 取得商品現在得標者資料
	 *	$productid		string		商品編號
	 */
	function get_winner_now() {

    global $db, $config;

		//商品編號
    $productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);

    //取得商品現在得標者紀錄
		$sql = "SELECT *
	          FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
	          WHERE `prefixid` = '{$config['default_prefix_id']}'
							AND `productid` = '{$productid}'
							AND `type` = 'bid'
							AND `switch` = 'Y'
							GROUP BY `price`
							HAVING COUNT(*) = 1
							ORDER BY `price` ASC
							LIMIT 1
		       ";

    $query = $db->getQueryRecord($sql);

		//計算得標紀錄對應下標比數
		$record_count = count($query['table']['record']);

		if($record_count < 1){
			$data['retCode'] = -2;
			$data['retMsg'] = "尚無得標者";
			$data['retType'] = "MSG";

			echo json_encode($data,JSON_UNESCAPED_UNICODE);

			return;
		}

		//得標者紀錄
		$winner_data = $query['table']['record'][0];

		//取得得標者profile
		$sql = "SELECT *
						FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
						WHERE `prefixid` = '{$config['default_prefix_id']}'
							AND `userid` = '{$winner_data['userid']}'
							AND `switch` = 'Y'
					 ";

		$query = $db->getQueryRecord($sql);

		//得標者profile
		$winner_profile = $query['table']['record'][0];

    $data['shid'] = $winner_data['shid'];
		$data['productid'] = $winner_data['productid'];
		$data['type'] = $winner_data['type'];
		$data['userid'] = $winner_data['userid'];
		$data['nickname'] = $winner_profile['nickname'];
		$data['thumbnail_file'] = $winner_profile['thumbnail_file'];
		$data['thumbnail_url'] = $winner_profile['thumbnail_url'];
		$data['retCode'] = 1;
		$data['retMsg'] = "查詢完成";
		$data['retType'] = "JSON";

    echo json_encode($data,JSON_UNESCAPED_UNICODE);

		return;

	}

	/*
	 *  商品手動結標
	 *	$productid		string		商品編號
	 */
	public function close_bid() {

    global $db, $config;

		//商品編號
    $productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
	$password = empty($_POST['password']) ? htmlspecialchars($_GET['password']) : htmlspecialchars($_POST['password']);

	//取得商家id
		//$_SESSION['auth_id'] = "1705";
		$auth_id = $_SESSION['auth_id'];

    //特定商家可使用單一商品結標功能
		if($auth_id != "1705"){
			//$sec_key = md5(rand(100000000,999999999));
			if($password == "qazwsxedc"){
				$sec_key = md5($password);
				$auth_id = 'saja';
			}else{
				$sec_key = md5(rand(100000000,999999999));
			}
		}else{
			$auth_id = 'saja';
			$sec_key = md5("qazwsxedc");
		}

		//組合結標參數
    $arrPost = array('auth_id' => $auth_id,'productid' => $productid,'sec_key' => $sec_key);

		//執行結標
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, BASE_URL.APP_DIR."/script.php");
		curl_setopt($ch, CURLOPT_POST, true); // 啟用POST
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($arrPost));
		$result_data=curl_exec($ch);
		curl_close($ch);

		//過濾結標回傳中 #!/usr/bin/php (linux執行前綴)
		$result_data = str_replace("#!/usr/bin/php","",$result_data);
		//回傳結果加入productid
		$data = json_decode($result_data,TRUE);

		//結標成功或流標，更新商品下架時間
		if($data['retCode'] == 1 || $data['retCode'] == -5){
			$query = "UPDATE `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}product` set 
			`offtime` = NOW()
			WHERE `productid` = '{$productid}'";
			$res = $db->query($query);
		}

		$data['productid'] = $productid;
		echo json_encode($data,JSON_UNESCAPED_UNICODE);
  }


	/*
	 *  顯示最終得標者頁
	 *	$flash_loc		string		閃殺地點
	 *	$productid		string		商品編號
	 */
	public function show_final_winner(){

    global $db, $config, $tpl;

    //閃殺地點
		$flash_loc	= empty($_POST['flash_loc']) ? htmlspecialchars($_GET['flash_loc']) : htmlspecialchars($_POST['flash_loc']);

		//商品編號
		$productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);

		$tpl->set_title('');
		$tpl->assign('header','N');
		$tpl->assign('footer','N');

    $tpl->assign('flash_loc',$flash_loc); //商品編號

		$tpl->assign('productid',$productid); //商品編號

		$tpl->render("instantkill","show_final_winner",true);
	}

	/*
	 * 取得商品最終得標者資料
	 *	$productid		string		商品編號
	 */
	function get_winner_final() {

    global $db, $config;

		//商品編號
    $productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);

    //取得商品現在得標者紀錄
		$sql = "SELECT *
	          FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}pay_get_product` AS `p`
						INNER JOIN `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history` AS `h`
						ON `p`.`productid` = `h`.`productid` AND `p`.`userid` = `h`.`userid` AND `p`.`price` = `h`.`price`
	          WHERE `p`.`prefixid` = '{$config['default_prefix_id']}'
							AND `p`.`productid` = '{$productid}'
		       ";

    $query = $db->getQueryRecord($sql);

		//計算得標紀錄對應下標筆數
		$record_count = count($query['table']['record']);

		if($record_count < 1){
			$data['retCode'] = -2;
			$data['retMsg'] = "查無結標紀錄";
			$data['retType'] = "MSG";

			echo json_encode($data,JSON_UNESCAPED_UNICODE);

			return;
		}

    if($record_count > 1){
			$data['retCode'] = -3;
			$data['retMsg'] = "結標紀錄有誤 非最低唯一";
			$data['retType'] = "MSG";

			echo json_encode($data,JSON_UNESCAPED_UNICODE);

			return;
		}

		//得標者紀錄
		$winner_data = $query['table']['record'][0];

		// echo 	json_encode($winner_data);
		// die();

		//取得得標者profile
		$sql = "SELECT *
						FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
						WHERE `prefixid` = '{$config['default_prefix_id']}'
							AND `userid` = '{$winner_data['userid']}'
							AND `switch` = 'Y'
					 ";

		$query = $db->getQueryRecord($sql);

		//得標者profile
		$winner_profile = $query['table']['record'][0];

    $data['shid'] = $winner_data['shid'];
		$data['productid'] = $winner_data['productid'];
		$data['type'] = $winner_data['type'];
		$data['userid'] = $winner_data['userid'];
		$data['nickname'] = $winner_profile['nickname'];
		$data['thumbnail_file'] = $winner_profile['thumbnail_file'];
		$data['thumbnail_url'] = $winner_profile['thumbnail_url'];
		$data['price'] = $winner_data['price'];
		$data['retCode'] = 1;
		$data['retMsg'] = "查詢完成";
		$data['retType'] = "JSON";

    echo json_encode($data,JSON_UNESCAPED_UNICODE);

		return;

	}


	/*
	 *  用戶下標頁
	 *	$productid		string		商品編號
	 */
	public function user_bidding_room($token) {

    global $db, $config, $tpl;

		$value = base64_decode($token);

		$values = explode("|", $value);
	  $storeid = $values[0];
		$date = $values[1];
		$flash_loc = $values[2];

		$tpl->set_title('');
		$tpl->assign('header','Y');
		$tpl->assign('footer','N');

		$tpl->render("instantkill","user_bidding_room",true);

	}


	/*
	 *  取得用戶下標紀錄
	 *	$productid		string		商品編號
	 */
	public function get_user_bid_record() {

		global $db, $config;

		//商品編號
		$productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);

		//測試用戶id
	  $_SESSION['auth_id'] = "1705";

		//檢查是否已登入
		if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']!= ""){

			//設定查詢用戶帳號
			$auth_id = $_SESSION['auth_id'];

		}else{

			$data['retCode'] = -1;
			$data['retMsg'] = "請先登入";
			$data['retType'] = "MSG";
			echo json_encode($data,JSON_UNESCAPED_UNICODE);

			return;
		}

		//取得用戶下標紀錄
		$sql = "SELECT `shid`,`price`,`insertt`
						FROM `{$config['db'][4]['dbname']}`.`{$config['default_prefix']}history`
						WHERE `prefixid` = '{$config['default_prefix_id']}'
							AND `productid` = '{$productid}'
							AND `userid` = '{$auth_id}'
							AND `type` = 'bid'
							AND `switch` = 'Y'
					 ";

		$query = $db->getQueryRecord($sql);

    //設定輸出資料
		$data['productid'] = $productid;
		$data['userid'] = $auth_id;
		$data['recodes'] = $query['table']['record'];

		//計算下標筆數
		$record_count = count($query['table']['record']);

		if($record_count < 1){
			$data['retCode'] = -2;
			$data['retMsg'] = "查無下標紀錄";
			$data['retType'] = "MSG";
		}else{
			$data['retCode'] = 1;
			$data['retMsg'] = "查詢完成";
			$data['retType'] = "JSON";
		}

    echo json_encode($data,JSON_UNESCAPED_UNICODE);
		return;

	}


	// 閃殺商品頁面
	/*
	 * QRcode導到此, 存入$_SESSION['canbid']='Y' 再導去頁面
	 */
	public function new_play($productid='') {

		global $tpl, $product, $scodeModel, $user, $oscode, $bid;

    //設定 Action 相關參數
		set_status($this->controller);

		if(empty($productid)){
		  $productid=htmlspecialchars($_REQUEST['productid']);
		}
		$userid=$_SESSION['auth_id'];
		$errMsg='';
		$errAct='';
		$oscode_num =0;
		error_log("[c/kusosaja/play] userid : ".$userid);
		$get_product = "";

		//檢查是否登入
		if(!empty($userid) && $userid>0){
      //取得商品資訊
			if(empty($productid)){//無商品id
				$plist=$product->product_flash_list_all('');
				if($plist && count($plist['table']['record'])>0){
					$get_product = $plist['table']['record'][0];
					$productid= $get_product['productid'];
				}
			}else{//有商品id
				$get_product=$product->get_info($productid);
			}
			//檢查有無商品紀錄
		  if(!empty($get_product) && !empty($get_product['productid'])){
			  $tpl->assign('canbid','Y');

				//檢查是否登入
				if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']>0){
					//查詢是否有贈送過殺價券
					$oscode_num0 = $scodeModel->get_oscode($productid, $_SESSION['auth_id']);

					//查詢是否有贈送殺價券活動
					$scode_promote = $scodeModel->scode_promote($productid);
					$info['spid'] = $scode_promote['spid'];
					$info['behav'] = '';
					$info['productid'] = $productid;
					$info['userid'] = $_SESSION['auth_id'];
					$info['onum'] = $scode_promote['onum'];


					// 如果沒送過且有活動就贈送
					if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){

						for($j = 0;$j < $info['onum']; ++$j){
							$scodeModel->add_oscode($info);
						}

						$scodeModel->set_scode_promote($info);
						$errMsg="恭喜您獲得 ".$info['onum']."張本商品殺價券";

					}

					// 可用殺價券數量
					$oscode_num =  $oscode->get_prod_oscode($productid, $_SESSION['auth_id']);
					$tpl->assign('oscode_num', $oscode_num);
			  }

				$pay_type = $get_product['pay_type'];
				$bid_price=0;
				$bid_count=0;

				if(!empty($userid) && $userid>0){
					$saja_record=$bid->getUserSajaHistory($userid,$productid,' price ASC ',1);
					if($saja_record){
						error_log("[c/kusosaja] saja_record : ".json_encode($saja_record));
						$bid_price=$saja_record[0]['price'];
						$bid_count=count($saja_record);
					}
				}
				$tpl->assign('bid_price', $bid_price);
				$tpl->assign('bid_count', $bid_count);

				//Add By Thomas 20150721 for LINE 分享
				if($get_product['is_flash']=='Y'){
					$meta['title']=$get_product['name']." 閃殺搶標中 !!";
					$meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
				}else{
					$meta['title']=$get_product['name']." 搶標中 !!";
					$meta['description']='';
				}

				// 商品圖檔
				if(!empty($get_product['thumbnail2'])){
					$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail2'];
				}elseif(!empty($get_product['thumbnail_url'])){
					$meta['image'] = $get_product['thumbnail_url'];
				}
				// error_log("[] thumbnail2 : ".$get_product['thumbnail2']);
				$tpl->assign('product', $get_product);
				$tpl->assign('meta',$meta);
			}else{
				$errAct="window.location.href='/site/product/?type=flash&canbid=N';";
				$errMsg='沒有這件商品 !!';
			}
		}else{
			$errAct="window.location.href='/site';";
			$errMsg='請登入享受更多優惠 !!';
		}
		$tpl->assign('errMsg',$errMsg);
		$tpl->assign('errAct',$errAct);
		$tpl->assign('canbid','Y');
		$tpl->assign('header','Y');
		$tpl->assign('footer','N');
		$tpl->set_title('');
		$tpl->render("product","kuso_saja",true);

	}


  // 固定導到最早結標的閃殺商品
	/*
	 *	QRcode導到此, 存入$_SESSION['canbid']='Y' 再導去頁面
	 *	http://www.shajiawang.com/wx_auth.php?jdata=gotourl:/site/product/top_kuso_saja
	 */
	public function play($productid='8143') {

    global $tpl, $product, $scodeModel, $user, $oscode, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

    $_SESSION['auth_id']=1705;

		if(empty($productid)){
      $productid=htmlspecialchars($_REQUEST['productid']);
    }

		$userid=$_SESSION['auth_id'];
    $errMsg='';
	  $errAct='';
	  $oscode_num =0;
	  error_log("[instantkill/play] auth_id : ".$userid);

	  if(!empty($userid) && $userid>0){

			if(empty($productid)){
		    $plist=$product->product_flash_list_all('');
		  }else{
        $plist['table']['record'][0]=$product->get_info($productid);
		  }

			if(!empty($plist['table']['record']) && !empty($plist['table']['record'][0]['productid'])){

				$tpl->assign('canbid','Y');
			  $productid= $plist['table']['record'][0]['productid'];

			  if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']>0){

					//查詢是否有贈送過殺價券
				  $oscode_num0 = $scodeModel->get_oscode($productid, $_SESSION['auth_id']);

				  //查詢是否有贈送殺價券活動
				  $scode_promote = $scodeModel->scode_promote($productid);
				  $info['spid'] = $scode_promote['spid'];
				  $info['behav'] = '';
				  $info['productid'] = $productid;
				  $info['userid'] = $_SESSION['auth_id'];
				  $info['onum'] = $scode_promote['onum'];

				  // 如果沒送過且有活動就贈送
				  if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){

						for($j = 0;$j < $info['onum']; ++$j){
					    $scodeModel->add_oscode($info);
					  }

						$scodeModel->set_scode_promote($info);

						$errMsg="恭喜您獲得 ".$info['onum']."張本商品殺價券";

					}

				  // 可用殺價券數量
				  $oscode_num =  $oscode->get_prod_oscode($productid, $_SESSION['auth_id']);
				  $tpl->assign('oscode_num', $oscode_num);

			  }

			  //商品資料
			  $get_product = $product->get_info($productid);
			  $pay_type = $get_product['pay_type'];
			  $tpl->assign('product', $get_product);

			  // 店點資料(目前未使用)
			  $sgift=0;
			  $tpl->assign('sgift_num', $sgift);

			  //目前中標
			  $redis = getRedis('','');
			  // $redis->connect('127.0.0.1');
			  $bid_name=$redis->get('PROD:'.$productid.':BIDDER');

			  if(empty($bid_name)){
			    $bidded = $product->get_bidded($productid);
				  $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '無';
				  $redis->set('PROD:'.$productid.':BIDDER',$bid_name);
				  error_log("[instantkill/play] Winner :".$bid_name." of Prod:".$productid." From DB and sync to redis : ".$bid_name);
			  }else{
				  error_log("[instantkill/play] Winner: ".$bid_name." of Prod:".$productid." From redis");
			  }
			  $tpl->assign('bidded', $bid_name);

			  // 取得該用戶的(最低)出價
			  $bid_price=0;

			  if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']>0){
			    $saja_record=$bid->getUserSajaHistory($_SESSION['auth_id'],$productid,' price ASC ',1);
				  if($saja_record!=false){
				    $bid_price=$saja_record[0]['price'];
				  }
			  }

			  $tpl->assign('bid_price', $bid_price);

		    //Add By Thomas 20150721 for LINE 分享
		    if($get_product['is_flash']=='Y'){
			    $meta['title']=$get_product['name']." 閃殺搶標中 !!";
			    $meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
		    }else{
			    $meta['title']=$get_product['name']." 搶標中 !!";
			    $meta['description']='';
		    }

			  // 商品圖檔
		    if(!empty($get_product['thumbnail'])){
				  $meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail'];
		    }elseif (!empty($get_product['thumbnail_url'])){
				  $meta['image'] = $get_product['thumbnail_url'];
		    }

			  $tpl->assign('meta',$meta);
		  }else{
		    $errAct="window.location.href='/site/product/?type=flash&canbid=N';";
		    $errMsg='沒有這件商品 !!';
		  }
	  }else{
	    $errAct="window.location.href='/site';";
	    $errMsg='請登入享受更多優惠 !!';
	  }

		$tpl->assign('errMsg',$errMsg);
	  $tpl->assign('errAct',$errAct);
    $tpl->assign('canbid','Y');
	  $tpl->set_title('');
	  $tpl->render("product","kuso_saja",true);

  }


	/*
	 * 使用者登入寫入session & cookie)
	 */
  public function set_login($user) {

		global $db, $config, $usermodel;

		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';

		//取得使用者profile
		$query = "SELECT *
							FROM  `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_profile`
							WHERE `prefixid` = '{$config['default_prefix_id']}'
								AND `userid` = '{$user['userid']}'
								AND `switch` =  'Y'
							";

		error_log("[instantkill/set_login] Query User : ".$query);

		$table = $db->getQueryRecord($query);

		$user['profile'] = $table['table']['record'][0];

		//使用者profile 寫入session
		$_SESSION['user'] = $user;

		// Set session and cookie information.
		$_SESSION['auth_id'] = $user['userid'];
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = md5($user['userid'] . $user['name']);
		setcookie("auth_id", $user['userid'], time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()+60*60*24*30, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", md5($user['userid'] . $user['name']), time()+60*60*24*30, "/", COOKIE_DOMAIN);

		// Set local publiciables with the user's info.
		if(isset($usermodel)){
			$usermodel->user_id = $user['userid'];
			$usermodel->name = $user['profile']['nickname'];
			$usermodel->email = '';
			$usermodel->ok = true;
			$usermodel->is_logged = true;
		}

  }


  // 新增殺價券資料
  public function give_oscode() {

		global $db, $config, $broadcast, $product;

		$lbid = empty($_POST['lbid']) ? htmlspecialchars($_GET['lbid']) : htmlspecialchars($_POST['lbid']);
		$lbuserid =  empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);
		$productid =  empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=$_SESSION['auth_id'];

		if(empty($lbid) && empty($lbuserid) && empty($productid)){
		  $ret = getRetJSONArray(0,'無效的參數 !!','MSG');
		  echo json_encode($ret);
		  return false;
		}

		$arrPromotes=$broadcast->getScodePromoteList($lbuserid);
		$retMsg ="";
		$num_received_oscode=0;
		$arrProductids=array();

		if($arrPromotes){
      error_log("[instantkill/give_oscode] arrPromotes : ".json_encode($arrPromotes));
      foreach($arrPromotes as $promote){
        if( $promote['lbuserid']==$lbuserid){

					$spid = $promote['spid'];
			    $onum = $promote['onum'];
			    $productid=$promote['productid'];

          if(empty($productid)){
            error_log("[instantkill/give_oscode] empty productid !! ");
            continue;
          }

          // 檢查是否領過殺價券
          $received = $broadcast->getUserOscode($userid,$spid);
          error_log("[instantkill/give_oscode] received oscode for productid ${productid} : ".$received[0]['count']);

					if($received[0]['count']>0){
						$ret = array();
						$ret['retCode']=-1;
						$ret['retMsg']= "您已領取過殺價券 !!";
						echo json_encode($ret);
						return;
						continue;
          }

          // $product = $product->get_info($productid);
          if(true){
            if($onum>0){
              for($j=0;$j<$onum;++$j){
                $broadcast->insertUserOscode($userid, $spid, $productid, $lbuserid);
              }
              if($spid>0){
                $update = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
                              SET amount=amount+1 ,scode_sum=scode_sum+".$onum."
                            WHERE spid='".$spid."'";
                $ret=$db->query($update);
                error_log("[instantkill/give_oscode] update : ".$update."-->".$ret);
                $num_received_oscode+=$onum;
              }
              array_push($arrProductids,$productid);
            }
            $ret = array();
            $ret['retCode']=1;
            $ret['retMsg']= "成功領取 ".$onum." 張殺價券 !!";
            $ret['prodids']=$arrProductids;
          }
        }
      }
    }else{

			$ret = array();
			$ret['retCode']=0;
			$ret['retMsg']= "本活動已結束 !!";

    }
    echo json_encode($ret);
    return;

  }

}
