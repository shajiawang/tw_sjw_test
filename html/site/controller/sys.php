<?php
/*
 * Sys Controller 系統記錄
 */

class Sys {

  public $controller = array();
	public $params = array();

	public function __construct() {
		$this->datetime = date('Y-m-d H:i:s');
	}


	/*
   * Default home page.
   */
	public function home() {

    global $tpl;

		$tpl->set_title('');
		$tpl->render("sys","home",true);

	}


	/*
   * 	log 顯示
	 *	$page		int		頁碼
	 *	$perpage	int		分頁筆數
   */
	public function showlog() {

		global $tpl, $sys;

		$json	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$page 	 = empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$perpage = empty($_POST['perpage']) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);

		//取得LOG資料
		$log_list = $sys->log_list();

		if($json == 'Y'){
			$ret = getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = (!empty($log_list)) ? $log_list : array();
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('list', $log_list);
			$tpl->set_title('');
			$tpl->render("sys","home",true,'','Y');
		}

	}


	/*
   * 	log 顯示
	 *	$msg		varchar			app訊息
	 *	$client		varchar			客戶端訊息
   */
	public function log() {

		global $tpl, $sys;

		$msg 	= (empty($_GET['msg'])) ? $_POST['msg'] : $_GET['msg'];
		$client = (empty($_GET)) ? $_POST : $_GET;

		//判斷APP訊息是否存在
		if(!empty($msg)){
			//寫入log
			$logid = $sys->add_Log($msg, json_encode($client));
			$ret = getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array();
			$ret['retObj']['logid'] = $logid;
		}else{
			$ret = getRetJSONArray(-1,'log資料錯誤!!','MSG');
		}

		echo json_encode($ret);
		exit;
	}


	public function getLocation() {

    $location=$_REQUEST['location'];
    error_log("[c/sys/getLocation] location:".$location);
    // 跟騰訊地圖服務申請的密鑰
    $key='5QEBZ-AJQCJ-XPYFW-FGHGD-OAHLQ-REFWQ';

    if(!empty($location) && strpos($location,",")>0) {
      $url='http://apis.map.qq.com/ws/geocoder/v1/?location='.$location.'&key='.$key;
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL,$url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);//不輸出內容
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
      $result =  curl_exec($ch);
      curl_close ($ch);
      error_log("[c/sys/getLocation] result:".$result);
      echo $result;
      return;
    }else{
      echo '{"status": -1,"message": "EMPTY"}';
    }

	}


	public function showMsg() {

		global $tpl, $sys, $product, $mall, $member;

		//設定 Action 相關參數
		set_status($this->controller);
		$status = _v('status');

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$code = empty($_POST['code']) ? htmlspecialchars($_GET['code']) : htmlspecialchars($_POST['code']);
		$msg = empty($_POST['msg']) ? htmlspecialchars($_GET['msg']) : htmlspecialchars($_POST['msg']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$kid = empty($_POST['kid']) ? htmlspecialchars($_GET['kid']) : htmlspecialchars($_POST['kid']);
		$obida = empty($_POST['obida']) ? htmlspecialchars($_GET['obida']) : htmlspecialchars($_POST['obida']);
		$obidb = empty($_POST['obidb']) ? htmlspecialchars($_GET['obidb']) : htmlspecialchars($_POST['obidb']);
		$obid = empty($_POST['obid']) ? htmlspecialchars($_GET['obid']) : htmlspecialchars($_POST['obid']);
		$total = empty($_POST['total']) ? htmlspecialchars($_GET['total']) : htmlspecialchars($_POST['total']);

		if($kind == "product"){
			$content = $product->getProductById($kid);
			$data = array(
            				'code'		=> $code,
            				'msg'		=> $msg,
            				'kind'		=> $kind,
            				'kid'		=> $kid,
            				'content'	=> $content,
            				'obida'		=> $obida,
            				'obidb'		=> $obidb,
            				'obid'		=> $obid,
            				'total'		=> $total
			);
		}elseif ($kind == "mall"){
			$content = $mall->get_info($kid);
			$data = array(
            				'code'		=> $code,
            				'msg'		=> $msg,
            				'kind'		=> $kind,
            				'kid'		=> $kid,
            				'content'	=> $content,
            				'total'		=> $total
			);
		}else{
			$content = $product->getProductById($kid);
			$data = array(
            				'code'		=> $code,
            				'msg'		=> $msg,
            				'kind'		=> $kind,
            				'kid'		=> $kid,
            				'content'	=> $content,
            				'obida'		=> $obida,
            				'obidb'		=> $obidb,
            				'obid'		=> $obid,
            				'total'		=> $total
			);
		}

		if($json == 'Y'){
			$ret = getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = (!empty($data)) ? $data : array();
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('data', $data);
			$tpl->set_title('');
			$tpl->render("sys","msg",true);
		}

	}


	public function showview() {

		global $tpl, $sys, $product, $mall, $member;

		//設定 Action 相關參數
		set_status($this->controller);
		$status = _v('status');

		$tpl->assign('data', $data);
		$tpl->set_title('');
		$tpl->render("sys","msg",true);

	}


	public function showiframe() {

		global $tpl, $sys, $product, $mall, $member;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("sys","iframe",true);

	}


	/*
	 *	APP版本控管
	 *	$type					varchar					手機類型
	 *	$osversion				varchar					系統版本號
	 *	$appversion				varchar					app版本號
	 */
	public function app() {

		global $tpl, $sys;
		
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$osversion = empty($_POST['osversion']) ? htmlspecialchars($_GET['osversion']) : htmlspecialchars($_POST['osversion']);
		$appversion = empty($_POST['appversion']) ? htmlspecialchars($_GET['appversion']) : htmlspecialchars($_POST['appversion']);

		if(!empty($type) && ($type == 'ios' || $type == 'android')) {
			
			$content = $sys->app_os_list($type);

			if($osversion != $content['osversion']) {
				$data['exchange']	= true;
			} else {
				$data['exchange']	= false;
			}

			$data = array(
				'osversion'		=> $content['osversion'],
				'appversion'	=> $content['appversion'],
				'kind'			=> 1,
				'uri'			=> 'market://details?id=tw.com.saja.userapp',
			);

	
			if($appversion < $content['appversion']) {
				$data['exchange']	= true;
				$data['content']	= '殺價王推出新版本\n請下載更新！';
			} else {
				$data['exchange']	= false;
				$data['content']	= '不需要更新版本';
			}

			if($json == 'Y'){
				$ret = getRetJSONArray(1,'OK','MSG');
				$ret['retObj']['data'] = (!empty($data)) ? $data : array();
				echo json_encode($ret);
				exit;
			}
		}
		exit;
	}
    
    // Add By thomas 20190423
    // 將DB內特定商品的 下標金額 - 出價數 sync到Cache
    public function sync_bid_to_cache() {
           
           $prodid = empty($_POST['prodid']) ? htmlspecialchars($_GET['prodid']) : htmlspecialchars($_POST['prodid']);
                      
           if(empty($prodid)) {
              echo getRetJSONArray(0,'NO_DATA','MSG'); 
              return ;              
           }
           
           global $bid;
           // 取得該商品之下標價格與記錄
           $arr = $bid->get_bid_price_stats($prodid);
           if($prodid && $arr) 
           {
              $redis = getBidCache();
              if($redis) {
                 $key = 'PROD:RANK:'.$prodid;
                 // For single instance
                 // $redis->delete($key);
                 // For Cluster
                 $redis->del($key);
                 for($idx=0;$idx<count($arr);++$idx) {
                     if(intval($arr[$idx]['price']==0)) {
                        continue;   
                     }
                     $arr[$idx]['price']=str_pad(intval($arr[$idx]['price']),12,"0",STR_PAD_LEFT);
                     $redis->zIncrBy($key, $arr[$idx]['cnt'], $arr[$idx]['price']);                     
                 }
              }
              $ret = getRetJSONArray(1,'OK','PROD:RANK:'.$prodid);             
           } else {
              $ret = getRetJSONArray(0,'NO_DATA_FOUND','PROD:RANK:'.$prodid);       
           } 
           echo json_encode($ret);
           $ret['retObj']= $arr;
           error_log(json_encode($ret));           
           return;
    }
	
	
	/*
	 *	系統訊息內容
	 *	$json	 				varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$id						int						系統訊息編號
	 *	$filtration				varchar					內文過濾html格式 (Y:是 N:否)
	 */
	public function sysmsg() {

		global $tpl, $sys, $mall;
		
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$id = empty($_POST['id']) ? htmlspecialchars($_GET['id']) : htmlspecialchars($_POST['id']);
		$filtration = empty($_POST['filtration']) ? htmlspecialchars($_GET['filtration']) : htmlspecialchars($_POST['filtration']);
		
		if (!empty($id)) {
			//取得系統訊息
			$syslist = $mall->get_sys($id);
			//過濾內容格式
			if ($filtration == 'Y'){
				$description = (!empty($syslist[0]['description']) ) ? $syslist[0]['description'] : '';
				$description = str_replace(array("\r\n\t&nbsp;","\r","\n","\t","&nbsp;"), '', (trim(strip_tags($description))));
				$syslist[0]['description'] = html_decode($description);
			}
		}
		
		$ret = getRetJSONArray(1,'OK','MSG');
		$ret['retObj']['data']['syslist'] = (!empty($syslist)) ? $syslist : array();
		echo json_encode($ret);
		exit;
	}

	
	
	public function setCache() {
		   global $tpl;
		   if(!empty($_SERVER["HTTP_CLIENT_IP"])){
				$cip = $_SERVER["HTTP_CLIENT_IP"];
			}
			elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
				$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
			}
			elseif(!empty($_SERVER["REMOTE_ADDR"])){
				$cip = $_SERVER["REMOTE_ADDR"];
			}
			else{
				$cip = "unknown";
			}
			if($cip!="211.75.238.38" && $cip!="150.116.207.59" && $cip!="210.61.148.35" &&  $cip!="60.251.120.84") {
			   exit;	
			}
		   $key=htmlspecialchars($_REQUEST['k']);
		   $value=htmlspecialchars($_REQUEST['v']);
		   $redis = new Redis();
           $redis->connect('sajacache01', 6379);
		   $ret=array();
		   $ret["retCode"]=$redis->set($key, $value);
		   $ret["retMsg"]="OK";
		   $ret['retObj']=array(
		      "key"=>$key,
			  "value"=>$redis->get($key)
		   );
		   echo json_encode($ret);
		   return;
	}
	
	public function delCache(){
		    global $tpl;
		   if(!empty($_SERVER["HTTP_CLIENT_IP"])){
				$cip = $_SERVER["HTTP_CLIENT_IP"];
			}
			elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
				$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
			}
			elseif(!empty($_SERVER["REMOTE_ADDR"])){
				$cip = $_SERVER["REMOTE_ADDR"];
			}
			else{
				$cip = "unknown";
			}
			if($cip!="211.75.238.38" && $cip!="150.116.207.59" && $cip!="210.61.148.35" &&  $cip!="60.251.120.84") {
			   exit;	
			}
		   $key=htmlspecialchars($_REQUEST['k']);
		   $redis = new Redis();
           $redis->connect('sajacache01', 6379);
		   $ret=array();
		   $ret["retCode"]=$redis->del($key);
		   $ret["retMsg"]="OK";
		   $ret['retObj']=array(
		      "key"=>$key,
			  "value"=>$redis->get($key)
		   );
		   echo json_encode($ret);
		   return;
		
	}
	
	public function getCache() {
		   global $tpl;
			if(!empty($_SERVER["HTTP_CLIENT_IP"])){
				$cip = $_SERVER["HTTP_CLIENT_IP"];
			}
			elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
				$cip = $_SERVER["HTTP_X_FORWARDED_FOR"];
			}
			elseif(!empty($_SERVER["REMOTE_ADDR"])){
				$cip = $_SERVER["REMOTE_ADDR"];
			}
			else{
				$cip = "unknown";
			}
			if($cip!="211.75.238.38" && $cip!="150.116.207.59" && $cip!="210.61.148.35" &&  $cip!="60.251.120.84") {
			   exit;	
			}
		   $key=htmlspecialchars($_REQUEST['k']);
		   if(empty($key))
			  $key="*.*.*.*";
		   $redis = new Redis();
           $redis->connect('sajacache01', 6379);
		   /*
		   $arr = $redis->keys($key);
		   foreach($arr as $k) {
			    echo $k."->".$redis->get($k)."<br>" ; 
		   */
		   $ret=array();
		   $ret["retCode"]=1;
		   $ret["retMsg"]="OK";
		   $arr = $redis->keys($key);
		   foreach($arr as $k) {
			   $ret['retObj'][$k]=$redis->get($k);   
		   }
		   echo json_encode($ret);
		   return;
	}


	/*
	 *	系統訊息內容
	 *	$os_type	 				varchar					瀏覽工具判斷
	 */
	public function appload() {
		
		global $tpl, $sys, $mall;
		
		$os_type= htmlspecialchars($_REQUEST['os_type']);
		
		//底部選單
		if($os_type=="iOS") {
			$icon['account_off'] = IMG_URL.APP_DIR."/static/img/appload/ios/account_off.png";
			$icon['account_on'] = IMG_URL.APP_DIR."/static/img/appload/ios/account_on.png";
			$icon['auction_off'] = IMG_URL.APP_DIR."/static/img/appload/ios/auction_off.png";
			$icon['auction_on'] = IMG_URL.APP_DIR."/static/img/appload/ios/auction_on.png";
			$icon['dream_off'] = IMG_URL.APP_DIR."/static/img/appload/ios/dream_off.png";
			$icon['dream_on'] = IMG_URL.APP_DIR."/static/img/appload/ios/dream_on.png";
			$icon['home_off'] = IMG_URL.APP_DIR."/static/img/appload/ios/home_off.png";
			$icon['home_on'] = IMG_URL.APP_DIR."/static/img/appload/ios/home_on.png";
			$icon['shop_off'] = IMG_URL.APP_DIR."/static/img/appload/ios/shop_off.png";
			$icon['shop_on'] = IMG_URL.APP_DIR."/static/img/appload/ios/shop_on.png";
		} else if($os_type=="android") {
			$icon['account_off'] = IMG_URL.APP_DIR."/static/img/appload/android/account_off.png";
			$icon['account_on'] = IMG_URL.APP_DIR."/static/img/appload/android/account_on.png";
			$icon['auction_off'] = IMG_URL.APP_DIR."/static/img/appload/android/auction_off.png";
			$icon['auction_on'] = IMG_URL.APP_DIR."/static/img/appload/android/auction_on.png";
			$icon['dream_off'] = IMG_URL.APP_DIR."/static/img/appload/android/dream_off.png";
			$icon['dream_on'] = IMG_URL.APP_DIR."/static/img/appload/android/dream_on.png";
			$icon['home_off'] = IMG_URL.APP_DIR."/static/img/appload/android/home_off.png";
			$icon['home_on'] = IMG_URL.APP_DIR."/static/img/appload/android/home_on.png";
			$icon['shop_off'] = IMG_URL.APP_DIR."/static/img/appload/android/shop_off.png";
			$icon['shop_on'] = IMG_URL.APP_DIR."/static/img/appload/android/shop_on.png";
		}
		
		//首頁上方選單
		$icon['newuser'] = IMG_URL.APP_DIR."/static/img/homeMenu/i1.png";
		$icon['saja'] = IMG_URL.APP_DIR."/static/img/homeMenu/i2.png";
		$icon['newbid'] = IMG_URL.APP_DIR."/static/img/homeMenu/i3.png";
		$icon['sajaking'] = IMG_URL.APP_DIR."/static/img/homeMenu/i4.png";
		
		$ret = getRetJSONArray(1,'OK','MSG');
		$ret['retObj']['data']['icon'] = (!empty($icon)) ? $icon : array();
		$ret['retObj']['data']['wordcolor'] = '#F9A823';
		$ret['retObj']['data']['wordcolor-index'] = '#000000';
		// $ret['retObj']['data']['wordcolor'] = '#FF0000';
		echo json_encode($ret);
		exit;
	}
	
	
	/*
	 *	捐贈單位
	 */
	public function donatelist() {
		
		global $tpl, $sys;
	
		$donate[0]['code'] ="856203";
		$donate[1]['code'] ="321";
		$donate[2]['code'] ="0770223";
		$donate[3]['code'] ="7505";
		$donate[4]['code'] ="172";
		$donate[5]['code'] ="978";
		$donate[6]['code'] ="8850";
		
		$donate[0]['name'] ="台灣幸福狗流浪中途之家";
		$donate[1]['name'] ="財團法人中華民國唐氏症基金會";
		$donate[2]['name'] ="財團法人台北市基督徒救世會社會福利事業基金會";
		$donate[3]['name'] ="中華社會福利聯合勸募協會";
		$donate[4]['name'] ="臺北巿流浪猫保護協會";
		$donate[5]['name'] ="台灣之心愛護動物協會";
		$donate[6]['name'] ="社法人中華民國身障關懷協會";
		
		$ret = getRetJSONArray(1,'OK','LIST');
		$ret['retObj']['data']['donate'] = (!empty($donate)) ? $donate : array();
		echo json_encode($ret);
		exit;
	}	


	/*
	 *	國家資料
	 */
	public function country() {
		global $tpl, $channel;
		
		//判斷是否為APP
		$json 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		//國家
		$get_country = $channel->get_country();

        // add 身分驗證規則說明文字
        $rule_note = " 身分驗證說明:\n
   1. 國家/地區及手機號為必填\n
   2. 推薦人ID為選填, 您可於登入後至殺友專區填寫推薦人\n
   3. 證號末6碼為選填, 您可於登入後至殺友專區->我的帳號進行驗證\n
   4. 您可依身分選擇填寫下列證號末6碼:\n
      -身分證\n
      -居留證\n
      -護照\n
      -公司登記證明文件\n
   5. 得標會員須出示符合上述末6碼之證件正本予殺價王驗證身分,未符合者將取消其得標資格,並沒收下標之殺價幣/殺價券\n";		
		if($json=='Y') {
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";
			$data['retObj']['data']= (!empty($get_country)) ? $get_country : new stdClass;
			$data['retObj']['note']= $rule_note;
			echo json_encode($data);
        } else {
		    echo "";	
		}
	}

}
