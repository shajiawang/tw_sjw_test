<?php
/*
 * Member Controller 會員
 */

include_once(BASE_DIR ."/model/oscode.php");
include_once(LIB_DIR."/convertString.ini.php");

class Member {

	public $controller = array();
	public $params = array();
	public $id;
	public $userid = '';
	public $ret='';
	public $json = '';

	public function __construct() {
		global $oscode;
		$this->userid = (empty($_SESSION['auth_id']) ) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		$oscode=new OscodeModel;
	}

	/*
	 * Default home page.
	 */
	public function home() {

		global $db, $tpl, $member, $user, $history, $mall, $config, $deposit, $oscode, $scodeModel,$push;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		include_once(LIB_DIR ."/convertString.ini.php");
		$this->str = new convertString();

		//20200121 數位紅包 AARONFU
		include_once(LIB_DIR ."/user_referral.php");
		$today = date("Y-m-d H:i:s");

		//紅包送S碼 //$user_ref = new user_referral();
		if($today >= "2020-01-21 18:00:00") {
			//01/31 停用
			//$u_ret = $user_ref->red_scode($this->userid);
		}

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$os_type=(empty($_POST['os_type'])) ? htmlspecialchars($_GET['os_type']) : htmlspecialchars($_POST['os_type']);

		if($json=='Y'){
			$this->userid = (empty($_POST['auth_id']) ) ? $_GET['auth_id'] : $_POST['auth_id'];
			$get_member = $member->get_info($this->userid);
			if($get_member){
				if(empty($get_member['thumbnail_url'])){
					if(empty($get_member['thumbnail_file'])){
						$get_member['thumbnail_file']="_DefaultHeadImg.jpg";
					}
				}

			}else{
				$get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg";
			}

		}else{
		   $get_member = $member->get_info($this->userid);
		}

		$tpl->assign('member', $get_member);

		//會員認證資料
		$phone=$get_member['name'];

		if(empty($phone))
			$phone=$get_member['phone'];

		if(empty($phone))
			$phone=$_SESSION['user']['profile']['phone'];

		$get_auth = $user->validUserAuth($this->userid, $phone);
		$tpl->assign('auth', $get_auth);

		//殺幣數量
		$get_spoint = $member->get_spoint($this->userid);
		if(!$get_spoint){
			$get_spoint=array("userid"=>$this->userid, "amount"=>0);
		}
		$tpl->assign('spoint', $get_spoint);

		//凍結殺幣數量
		$get_list = $history->saja_list($this->userid, true, 'NP');
		$row_list['frozen'] = 0;
		$row_list['userid'] = $this->userid;
		if(is_array($get_list)){
			foreach($get_list['table']['record'] as $rk => $rv){
				if($rv['closed'] == 'N'){
					$spoint = $history->getSpointById($rv['spointid']);
					$row_list['frozen'] += abs($spoint);
					$row_list['amount'] += $rv['saja_fee'] * $rv['count'];
				}
			}
		}
		$tpl->assign('row_list', $row_list);

		//店點數量紀錄(尚未使用)
		$get_gift['userid']=$this->userid;
		$get_gift['amount']=0;
		$tpl->assign('gift', $get_gift);

		//紅利積點數量
		$get_bonus = $member->get_bonus($this->userid);
			// if($get_bonus['amount']<0) {
			//    $get_bonus['amount']=0;
			// }
		$tpl->assign('bonus', $get_bonus);

		//殺價券數量
		$get_oscode=$oscode->get_oscode($this->userid);
		$tpl->assign('oscode',$get_oscode);

		// S碼 現有可用總數
		$get_scode = $scodeModel->get_scode($this->userid);
		$tpl->assign('scode',$get_scode);
		
		if(empty($get_scode) && $os_type=="android") {
		   $get_scode=" 0";
		}

		//購物金數量
		$get_rebate = $member->get_rebate($this->userid);
		if(!$get_rebate){
			$get_rebate=array("userid"=>$this->userid, "amount"=>0);
		}
		$tpl->assign('rebate', $get_rebate);

		//圓夢卷數量
		$get_dscode = $member->get_dscode($this->userid);
		if(!$get_dscode){
			$get_dscode=array("userid"=>$this->userid, "amount"=>0);
		}
		$tpl->assign('dscode', $get_dscode);

		//有返饋圓夢卷數量
		$get_dscode2 = $member->get_dscode($this->userid,'Y');
		if(!$get_dscode2){
			$get_dscode2=array("userid"=>$this->userid, "amount"=>0);
		}
		$tpl->assign('dscode2', $get_dscode2);
		
		//無返饋圓夢卷數量
		$get_dscode3 = $member->get_dscode($this->userid,'N');
		if(!$get_dscode3){
			$get_dscode3=array("userid"=>$this->userid, "amount"=>0);
		}
		$tpl->assign('dscode3', $get_dscode3);

		//msgstatus
		$get_notifycnt = $push->getNotifyCnt($this->userid);


		$get_notify_yes=array("userid"=>$this->userid, "amount"=>$get_notifycnt['yes_cnt']);
		$get_notify_no=array("userid"=>$this->userid, "amount"=>$get_notifycnt['no_cnt']);
		$tpl->assign('yes_cnt', $get_notify_yes);
		$tpl->assign('no_cnt', $get_notify_no);

		// S碼 已用總數
		$get_scode_use = abs($scodeModel->used_sum($this->userid));
		$tpl->assign('scode_use',$get_scode_use);

		//推薦送提示
		$act_affiliate_msg = '';//$this->act_affiliate($this->userid);
		$tpl->assign('act_affiliate_msg', $act_affiliate_msg);


		//上一層推薦人
		$get_member_data['intro_parent_id'] = '';
		$get_member_data['intro_parent_name'] = '';

		//推薦人
		$get_member_data['intro_by'] = '';
		$get_member_data['intro_name'] = '';

		//推薦人資料
		$user_referral = $user->getAffiliateUser($this->userid);

		if(!empty($user_referral['intro_by']) ){
			//推薦人
			$get_member_data['intro_by'] = $user_referral['intro_by'];
			$get_member_data['intro_name'] = $user_referral['nickname'];

			//上一層推薦人
			$user_referral_2 = $user->affiliateParents($user_referral['intro_by']);
			$get_member_data['intro_parent_id'] = $user_referral_2['intro_by'];
			$get_member_data['intro_parent_name'] = $user_referral_2['nickname'];
		}

		$get_member_data['userid']=$get_member['userid'];
		$get_member_data['name']=$get_member['name'];
		$get_member_data['nickname']=$get_member['nickname'];
		$get_member_data['thumbnail_file']=$get_member['thumbnail_file'];
		$get_member_data['thumbnail_url']=$get_member['thumbnail_url'];
		$get_member_data['spoint']=($get_spoint['amount'])?sprintf("%1\$d",$get_spoint['amount']):"0";
		$get_member_data['store_gift']=($get_gift['amount'])?$get_gift['amount']:"0";
		$get_member_data['bonus']=($get_bonus['amount'])?sprintf("%1\$d",$get_bonus['amount']):"0";
		$get_member_data['frozen_spoints']=($row_list['frozen'])?"".sprintf("%1\$d",$row_list['frozen']):"0";
		$get_member_data['rebate']=($get_rebate['amount'])?sprintf("%1\$d",$get_rebate['amount']):"0";
		
		$get_member_data['dscode']=($get_dscode['amount'])?sprintf("%1\$d",$get_dscode['amount']):"0";
		$get_member_data['dscode2']=($get_dscode2['amount'])?sprintf("%1\$d",$get_dscode2['amount']):"0";
		$get_member_data['dscode3']=($get_dscode3['amount'])?sprintf("%1\$d",$get_dscode3['amount']):"0";
		$get_member_data['yes_cnt']="".$get_notify_yes['amount'];
		$get_member_data['no_cnt']="".$get_notify_no['amount'];

		$get_member_data['oscode']="".$get_oscode;
		$get_member_data['scode']="".$get_scode;
		$get_member_data['scode_use']="".$get_scode_use;
		$get_member_data['verified']=$get_auth['verified'];
		$get_member_data['id_varifyied']=$get_member['id_varifyied'];
		$get_member_data['act_affiliate_msg']=$get_auth['act_affiliate_msg'];

		// 暫時使用 web的方式充值
		$get_member_data['deposit']="web";
		$tpl->assign('member_data', $get_member_data);


		$ret=getRetJSONArray(1,'OK','JSON');
		$ret['retObj']=array();
		$ret['retObj']=$get_member_data;

		if($json=='Y') {
			echo json_encode($ret,JSON_UNESCAPED_SLASHES);
			exit;
		}else{
			$tpl->assign('ret', $ret);
			$tpl->assign('noback', 'Y');
			$tpl->set_title('');
			$tpl->render("member","home",true);
		}

	}

	private function act_affiliate($userid) {
		global $db, $config, $usermodel;

		//推薦送
		$act_affiliate_msg = '';

		$query_Affiliate = "SELECT `intro_by`,`promoteid`, count(*) u_cnt
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_affiliate`
			WHERE `switch` = 'Y'
				AND `userid` = '{$userid}'
				AND `promoteid` ='8'
				AND `act` = 'REG'
				AND `insertt` > '2019-09-24 12:00:00' ";
		$table_Affiliate = $db->getQueryRecord($query_Affiliate);

		if ( $table_Affiliate['table']['record'][0]['u_cnt']!=0 ) {

			//驗證是否領取
			$query = "SELECT count(*) ah_cnt
				FROM  `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}activity_history` ah
				WHERE ah.switch = 'Y'
					AND ah.userid = '{$userid}'
					AND ah.activity_type='8'";
			$table3 = $db->getQueryRecord($query);
			if ( $table3['table']['record'][0]['ah_cnt']==0 ) {
				$act_affiliate_msg = '(完成驗證立即送 100 殺幣)';
			}
		}

		return $act_affiliate_msg;
	}

	/*
	 * 推薦好友清單
	 */
	public function referral(){
		global $db, $tpl, $config, $user;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);


		//處理狀態
		$status = '';
		if($kind=='all'){
			$status = '';
		}
		/*
		elseif($kind=='process'){
			$status = '0';
		}elseif($kind=='close'){
			$status = '1';
		}*/

		//清單
		$get_list = $user->getReferralList($this->userid, '', $p);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');

			if($get_list){

				if(($p < 0) || ($p > $get_list['table']['page']['totalpages']) ){
					$ret['retObj']['data'] = '';
				}else{
					$ret['retObj']['data']=$get_list['table']['record'];
				}

				$ret['retObj']['page']=$get_list['table']['page'];

			} else {

				$ret['retObj']['data']=array();
				$ret['retObj']['page']=new stdClass();

				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}

			echo json_encode($ret);
			exit;

		} else {
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('推薦好友清單');
			$tpl->render("member","referral",true);
		}
	}


	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

		$table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';
		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;

  }


	//手機驗證碼
	public function check_sms() {

		global $tpl, $mall, $member;

		$ep=(empty($_POST['ep']))?$_GET['ep']:$_POST['ep'];

    if($ep == 'Y'){
			enterprise_login_required();
		}else{
			login_required();
		}

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("member","check_sms",true);

	}


	/*
	 *	訂單紀錄清單
	 *	$json						varchar							瀏覽工具判斷 (Y:APP來源)
	 *	$kind						varchar							訂單狀態
	 */
	public function order() {

		global $tpl, $mall;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if($kind=='all'){
			$status = '';
		}elseif($kind=='process'){
			$status = '0';
		}elseif($kind=='close'){
			$status = '1';
		}else{
			$status = '';
		}

		//紀錄清單
		$get_list = $mall->order_list($this->userid, '', $status);

		if(!empty($get_list['table']['record'])) {
			foreach($get_list['table']['record'] as $tk => $tv) {
				$get_list['table']['record'][$tk]['modifyt'] = $tv['insertt'];
				// Add By Thomas 2020/01/02 使用貨幣及點數的敘述
				switch($tv['type']) {
				    case "saja":
					     $get_list['table']['record'][$tk]['curr_type_desc']="使用殺價幣 : ".intval($get_list['table']['record'][$tk]['total_fee'])."枚";
						 break;
                    case "exchange":
                    case "lifepay":
                    case "user_qrcode_tx":
                    default :
					     $get_list['table']['record'][$tk]['curr_type_desc']="使用鯊魚點 : ".intval($get_list['table']['record'][$tk]['total_fee'])."點";
                         break;
				}
			}
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = '';
				}else{
					$ret['retObj']['data']=$get_list['table']['record'];
				}
				$ret['retObj']['page']=$get_list['table']['page'];
			}else{
				$ret['retObj']['data']=array();
				$ret['retObj']['page']=new stdClass();

				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('');
			$tpl->render("member","order",true);
		}

	}

	/*
	 *	商城購買清單
	 *	$json						varchar							瀏覽工具判斷 (Y:APP來源)
	 *	$type						int								兌換商品類別 (1:生活代繳, 2:一般商品)
	 *	$p							int								頁碼
	 */
	public function exchange_order() {

		global $tpl, $mall;
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		switch ($type) {
			case '1':
				$type = "lifepay";
				break;
			case '2':
				$type = "exchange";
				break;
			default:
				$type = "";
				break;
		}

		//紀錄清單
		$get_list = $mall->exchange_order_list($this->userid, $type);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = '';
				}else{
					$ret['retObj']['data']=$get_list['table']['record'];
				}
				$ret['retObj']['page']=$get_list['table']['page'];
			}else{
				$ret['retObj']['data']=array();
				$ret['retObj']['page']=new stdClass();

				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;
		}

	}


	/*
	 *	取得訂單列表
	 *	$json						varchar							瀏覽工具判斷 (Y:APP來源)
	 *	$userid						int								會員編號
	 *	$type						varchar							訂單類型
	 *	$status						varchar							訂單狀態
	 */
	public function getOrderList() {

		global $tpl, $mall;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_POST['json']:$_POST['json'];
		$userid = $_POST['auth_id'];
		$type = $_POST['type'];
		$status = $_POST['status'];

		//紀錄清單
		$get_list = $mall->order_list($userid, $type, $status);
		$tpl->assign('row_list', $get_list);

		if($get_list){
		  $ret=getRetJSONArray(1,'OK','LIST');
		  $ret['retObj']['data']=$get_list['table']['record'];
		  $ret['retObj']['page']=$get_list['table']['page'];
		}else{
		  $ret=getRetJSONArray(0,'No Data Found !!','LIST');
		  $ret['retObj']['data']=array();
		  $ret['retObj']['page']=new stdClass();
		}
		echo json_encode($ret);
		exit;

	}


	public function order_detail() {
		global $tpl, $mall, $member;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("member","order_detail",true);
	}


	public function news() {

		global $tpl, $member;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//紀錄清單
		$get_list = $member->news_list();
		$tpl->assign('row_list', $get_list);

		//設定分頁
		if(empty($get_list['table']['page']) ){
			$page_content = array();
		}else{
			$page_path = $tpl->variables['status']['status']['path'];
			$page_content = $this->set_page($get_list, $page_path);
		}
		$tpl->assign('page_content', $page_content);

		$tpl->set_title('');
		$tpl->render("member","news",true);

	}


	public function news_detail() {

		global $tpl, $member;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$detail = $member->news_detail();
		$tpl->assign('detail', $detail);

		$tpl->set_title('');
		$tpl->render("member","news_detail",true);

	}

	public function edit() {

		global $tpl, $member;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		if($_POST){

      if($member->update($_POST)) $tpl->set_msg($member->msg, $member->ok);
			  $tpl->set_msg($member->msg, $member->ok);

		}

		$tpl->assign('user_email',$user->email);
		$tpl->assign('user_name',$user->name);
		$tpl->set_title('');
		$tpl->render("member","edit",true);

	}


	// For JSON reply only
	public function getAvilSpoints() {

		global $member;

		login_required();

		$userid=$_SESSION['auth_id'];

		if(empty($userid))
		  $userid=$_POST['auth_id'];

		$ret=null;

		if(empty($userid)){
		  $ret=getRetJSONArray(-1,'EMPTY USERID !!','MSG');
		}else{
		  $get_spoint = $member->get_spoint($userid);
		  if($get_spoint){
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>$get_spoint['amount']);
		  }else{
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>0.0);
		  }
		}
		echo json_encode($ret);

	}


	public function getSpoints() {

		global $member;

		login_required();

		$userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$ret=null;

		if(empty($userid)){
		  $ret=getRetJSONArray(-1,'EMPTY USERID !!','MSG');
		}else{
		  $get_spoint = $member->get_spoint($userid);
		  if(!empty($get_spoint['amount'])){
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>$get_spoint['amount']);
		  }else{
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>0.0);
		  }
		}
		echo json_encode($ret);

	}


	// For JSON reply only
	public function getAvailBonus() {

		global $member;

		login_required();

		$userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		error_log("auth_id->".$userid);
		$ret=null;

		if(empty($userid)){
		  $ret=getRetJSONArray(-1,'EMPTY USERID !!','MSG');
		}else{
		  $get_bonus=$member->get_bonus($userid);
		  if($get_bonus){
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>$get_bonus['amount']);
		  }else{
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>0.00);
		  }
		}
		echo json_encode($ret);

	}

	// 取得購物金 For JSON reply only
	public function getAvailRebate() {

		global $member;

		login_required();

		$userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		error_log("auth_id->".$userid);
		$ret=null;

		if(empty($userid)){
		  $ret=getRetJSONArray(-1,'EMPTY USERID !!','MSG');
		}else{
		  $get_rebate=$member->get_rebate($userid);
		  if($get_rebate){
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>$get_rebate['amount']);
		  }else{
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array("userid"=>$userid, "amount"=>0.00);
		  }
		}
		echo json_encode($ret);

	}

	// 取得卡包資訊
	public function getCardPackInfo() {

		global $user ;

		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);

		$ret=null;
		if(!empty($userid)){
		  //卡包資訊
			$user_extrainfo_category = $user->get_user_extrainfo_category();

			if(is_array($user_extrainfo_category)){

				if($json == 'Y'){
					$user_extrainfo=$user->get_user_extrainfo2($userid);
					foreach($user_extrainfo as $key => $value){
						if(empty($value['userid'])){
						  $user_extrainfo[$key]['userid'] = $userid;
						}
					}
				}else{
					$user_extrainfo=array();
					foreach($user_extrainfo_category as $key => $value){
						$info=$user->get_user_extrainfo($value['uecid'],$userid);
						if($info){
						  array_push($user_extrainfo,$info);
						}
					}
				}
				$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']=	$user_extrainfo;
			}
			echo json_encode($ret);
		}

		exit;

	}


	public function getMedalsList() {

		global $member;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$mdlid=empty($_POST['mdlid'])?$_GET['mdlid']:$_POST['mdlid'];
		$type=empty($_POST['type'])?$_GET['type']:$_POST['type'];

		$arrCond=array();

		if(!empty($mdlid)){
		  $arrCond['mdlid']=$mdlid;
		}

		if(!empty($type)){
		  $arrCond['type']=$type;
		}

		$get_list=$member->getMedalsList($arrCond);

		if($get_list){
		  foreach($get_list as $k=>$v){
			error_log(json_encode($v));
			$get_list[$k]['thumbnail_url_n']=BASE_URL.MEDALIMGS_DIR."/".$v['thumbnail_url_n'];
			$get_list[$k]['thumbnail_url_y']=BASE_URL.MEDALIMGS_DIR."/".$v['thumbnail_url_y'];
		  }
		  $ret=getRetJSONArray(1,'OK','LIST');
		  $ret['retObj']['data']=$get_list;
		}else{
		  $ret=getRetJSONArray(0,'No Data Found !!','LIST');
		}

		echo json_encode($ret);

		exit;

	}


	public function getUserMedalsList() {

    global $member;

    login_required();

    $json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
    $userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
    $mdlid=empty($_POST['mdlid'])?$_GET['mdlid']:$_POST['mdlid'];
    $type=empty($_POST['type'])?$_GET['type']:$_POST['type'];

    $arrCond=array();

    if(!empty($userid)){
      $arrCond['userid']=$userid;
    }

    if(!empty($mdlid)){
      $arrCond['mdlid']=$mdlid;
    }

    if(!empty($type)){
      $arrCond['type']=$type;
    }

    $get_list=$member->getUserMedalsList($arrCond);

    if($get_list){
      $ret=getRetJSONArray(1,'OK','LIST');
      $ret['retObj']['data']=$get_list;
    }else{
      $ret=getRetJSONArray(0,'No Data Found !!','LIST');
    }

    echo json_encode($ret);

    exit;

	}


	/*
	 *	店點明細
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 */
	public function getStoreGiftList() {

		global $tpl, $member;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
 	  $userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得曬單清單資料
		$get_list = $member->getStoreGiftList($userid);

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('');
			$tpl->render("member","getStoreGiftList",true);
		}else{
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = (!empty($get_list['record'])) ? $get_list['record'] : array();
			$ret['retObj']['page'] = (!empty($get_list['page'])) ? $get_list['page'] : new stdClass();
			echo json_encode($ret);
		}

	}


	/*
	 *	會員店點點數
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$esid		int			兌換中心分店編號
	 *	$page		int			頁碼
	 *	$perpage	int			會員編號
	 */
	public function getStoreGiftSum() {

		global $tpl, $member;

		$json 	 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$esid 	 	= empty($_POST['esid']) ? htmlspecialchars($_GET['esid']) : htmlspecialchars($_POST['esid']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得曬單清單資料
		$get_list = $member->getStoreGiftSum($userid, $esid);

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('');
			$tpl->render("member","getStoreGiftSum",true);
		}else{
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = (!empty($get_list['record'])) ? $get_list['record'] : array();
			$ret['retObj']['page'] = (!empty($get_list['page'])) ? $get_list['page'] : new stdClass();
			echo json_encode($ret);
		}

	}

	//20191119 AARON 改架構
	public function shareQrcode() {
		global $tpl, $member;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once(LIB_DIR ."/convertString.ini.php");
		$this->str = new convertString();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$this->userid = empty($_REQUEST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_REQUEST['auth_id']);
		error_log("[c/member/shareQrcode]: params : ".json_encode($_POST));

		if($json=='Y'){
			$get_member = $member->get_info($_POST['auth_id']);
			if($get_member){
				if(empty($get_member['thumbnail_url'])){
				  if(empty($get_member['thumbnail_file'])){
					 $get_member['thumbnail_file']="_DefaultHeadImg.jpg";
				  }
				}
				$get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/".$get_member['thumbnail_file'];
			}else{
				$get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg";
			}

		}else{
			$get_member = $member->get_info($this->userid);
		}

		//呼朋引伴推薦QRcode url
		$get_member['user_src'] = base64_encode($this->str->encryptAES128($config['encode_key'], $this->userid.'&&1') );
		//$share_url = BASE_URL . APP_DIR .'/user/register/?u='. $get_member['user_src'] .'&';
		//$get_member['share_url'] = $share_url;

		//微信呼朋引伴推薦推薦Qrcode URL
		$get_member['user_src2'] = $this->userid;
		$weixin_url = BASE_URL . APP_DIR .'/user/uni_register/?u='. $get_member['user_src2'];

		//WEB版 qrcode_url
		$get_member['share_url'] = $weixin_url;
		$qrcode_url["act_type"] = 8;
		$qrcode_url["intro_by"] = $this->userid;
		$get_member['weixin_url'] = json_encode($qrcode_url);

		if($json=='Y'){
		  $get_member['share_url'] = $weixin_url;
		  $get_member['qr_thumbnail_url'] = BASE_URL . APP_DIR .'/phpqrcode/?data='.$get_member['share_url'];
		}

		$today = date("Y-m-d H:i:s");

		//回傳:
		$ret['retCode']=1;
		$ret['retMsg']="OK";
		$ret['retObj']=array();
		$ret['retObj']=$get_member;
		$ret['retType']="LIST";
		$ret['action_data']["act_type"]=8;
		$ret['action_data']["intro_by"]=$this->userid;
		$ret['usetid']="我的ID: ".$this->userid;
		$ret['note']='<p><strong>邀請好友步驟</strong></p>
					  <p>1. 請好友下載，且安裝殺價王APP</p>
					  <p>2. 請好友註冊，並通過手機號驗證</p>
					  <p>3. 請好友至殺友專區 > 邀請好友，開啟掃描器</p>
					  <p>4. 請好友掃描上方您的二維碼，即可完成推薦</p>
					  <p>========================================</p>';

		if ( $today >= '2019-11-22 10:00:00' && $today < '2019-11-28 15:00:00') {
			$ret['note'] .='<p>推薦人可獲得殺價幣 50 枚</p>';
			$ret['note'] .='<p>被推薦用戶也可獲殺價幣 50 枚</p>';
		} else if($today >= '2019-11-28 15:00:00') {
			$ret['note'] .='<p>推薦人可獲得超級殺價券 2 張</p>';
			$ret['note'] .='<p>被推薦用戶也可獲得超級殺價券 1 張</p>';
            $ret['note'] .='<p>領取後可至殺友專區查看</p>';
			$ret['note'] .='<p>有效期限為<font color="red">領取後72小時內</font></p>';
		}

		if($json=='Y'){
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('ret', $ret);
			$tpl->assign('member', $get_member);
			$tpl->set_title('');
			$tpl->render("member","shareQrcode",true);
		}

	}
	/*
	public function shareQrcode() {
		global $tpl, $member;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		include_once(LIB_DIR ."/convertString.ini.php");
		$this->str = new convertString();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if($json=='Y'){
      $this->userid = $_POST['auth_id'];
      $get_member = $member->get_info($_POST['auth_id']);
      if($get_member){
        if(empty($get_member['thumbnail_url'])){
          if(empty($get_member['thumbnail_file'])){
        	 $get_member['thumbnail_file']="_DefaultHeadImg.jpg";
          }
        }
        $get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/".$get_member['thumbnail_file'];
      }else{
        $get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg";
      }

		}else{
		  $this->userid=$_SESSION['auth_id'];
		  error_log("[c/member] userid (Web) : ".$this->userid);
		  $get_member = $member->get_info($this->userid);
		}

		//呼朋引伴推薦QRcode url
		$get_member['user_src'] = base64_encode($this->str->encryptAES128($config['encode_key'], $this->userid.'&&1') ); //var_dump($u);string(24) "kgl1YRLcAQH9xlFIhbIEwQ=="
		$share_url = BASE_URL . APP_DIR .'/user/register/?u='. $get_member['user_src'] .'&';
		$get_member['share_url'] = $share_url;

		//微信呼朋引伴推薦推薦Qrcode URL
		$get_member['user_src2'] = $this->userid;
		$weixin_url = BASE_URL . APP_DIR .'/user/uni_register/?u='. $get_member['user_src2'] ;
		$get_member['weixin_url'] = $weixin_url;

		if($json=='Y'){
		  $get_member['share_url'] = $weixin_url;
		  $get_member['qr_thumbnail_url'] = BASE_URL.APP_DIR.'/phpqrcode/?data='.$get_member['share_url'];
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array();
			$ret['retObj']=$get_member;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('member', $get_member);
			$tpl->set_title('');
			$tpl->render("member","shareQrcode",true);
		}

	}
	*/


	// 會員登入
	public function userlogin() {

		global $tpl, $user;

			//設定 Action 相關參數
			set_status($controller);

		error_log("[c/member/userlogin] GET params : ".json_encode($_GET));
		error_log("[c/member/userlogin] POST params : ".json_encode($_POST));

		// 20181206 Add By Thomas
		$arrParams = array();
			if(count($_GET)>0){
		  $arrParams=$_GET;
		}else if(count($_POST)>0){
		  $arrParams=$_POST;
		}
		$_to="";
		if(!empty($arrParams)){
		  unset($arrParams['member/userlogin/']);
		  unset($arrParams['act']);
		  unset($arrParams['fun']);
		  error_log("[c/member/userlogin] params : ".json_encode($arrParams));

		  /* 入參說明
			 登入完成後要去的地方
			 _to=/site/deposit

			 APP localStorage 的$auth_id, $auth_loginid, $auth_secret
			 _auth=base64_encode($auth_id."|".$auth_loginid."|".$auth_secret)
		  */
		  if(is_array($arrParams) && count($arrParams)>0){
			if(array_key_exists("_to",$arrParams) && !empty($arrParams['_to']) ){
			  $_to=http_build_query($arrParams);
			}else{
			  $_to.="_to=".APP_DIR."/member/&".http_build_query($arrParams);
			}

			// 如果傳入 _auth 則自動作登入動作(給APP 跳Webview時用)
			if(array_key_exists("_auth",$arrParams) && !empty($arrParams['_auth'])){
			  $_auth=base64_decode($arrParams['_auth']);
			  // $_auth格式  $auth_id|$auth_loginid|$auth_secret
			  $arrAuth=explode("|",$_auth);
			  // error_log("[c/member/userlogin] arrAuth : ".json_encode($arrAuth));
			  error_log("[c/member/userlogin] auth_id : ".$arrAuth[0]);
			  error_log("[c/member/userlogin] auth_loginid : ".$arrAuth[0]);
			  error_log("[c/member/userlogin] : ".MD5($arrAuth[0].$arrAuth[1])."--".$arrAuth[2]);

			  if(MD5($arrAuth[0].$arrAuth[1])==$arrAuth[2]){
				// 驗證通過則做自動登入
				$get_user = $user->get_user($arrAuth[0]);
				if($get_user && setLogin($get_user)){
				  header("Location:".$arrParams['_to']);
				  return;
				}else{
				  header("Location:".APP_DIR);
				  return;
				}
			  }else{
				header("Location:".APP_DIR);
				return;
			  }
			}
		  }
		}else{
		  $_to="_to=".APP_DIR."/member/";
		}
		$tpl->assign('_to',$_to);
		$tpl->assign('_params',$arrParams);
		// Add End

		$admin 	        = empty($_POST['admin']) ? htmlspecialchars($_GET['admin']) : htmlspecialchars($_POST['admin']);
		$proxy_url 		= empty($_POST['proxy_url']) ? htmlspecialchars($_GET['proxy_url']) : htmlspecialchars($_POST['proxy_url']);
		$state 			= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
		$client_id 		= empty($_POST['client_id']) ? htmlspecialchars($_GET['client_id']) : htmlspecialchars($_POST['client_id']);
		$oauth_token 	= empty($_POST['oauth_token']) ? htmlspecialchars($_GET['oauth_token']) : htmlspecialchars($_POST['oauth_token']);
		$oauth_verifier = empty($_POST['oauth_verifier']) ? htmlspecialchars($_GET['oauth_verifier']) : htmlspecialchars($_POST['oauth_verifier']);

		if((!empty($admin)) && ($admin == "adminlogin")){
			$tpl->set_title('');
			$tpl->render("site","login",true);
		}else{
			$tpl->set_title('');
			$tpl->render("site","login2",true);
		}

	}

	// 喬遷活動專用會員登入
	public function auserlogin() {

		global $tpl, $user;

		//設定 Action 相關參數
		set_status($controller);

		error_log("[c/member/auserlogin] GET params : ".json_encode($_GET));
		error_log("[c/member/auserlogin] POST params : ".json_encode($_POST));

		// 20181206 Add By Thomas
		$arrParams = array();
			if(count($_GET)>0){
		  $arrParams=$_GET;
		}else if(count($_POST)>0){
		  $arrParams=$_POST;
		}
		$_to="";
		if(!empty($arrParams)){
		  unset($arrParams['member/auserlogin/']);
		  unset($arrParams['act']);
		  unset($arrParams['fun']);
		  error_log("[c/member/auserlogin] params : ".json_encode($arrParams));

		  /* 入參說明
			 登入完成後要去的地方
			 _to=/site/deposit

			 APP localStorage 的$auth_id, $auth_loginid, $auth_secret
			 _auth=base64_encode($auth_id."|".$auth_loginid."|".$auth_secret)
		  */
		  if(is_array($arrParams) && count($arrParams)>0){
			if(array_key_exists("_to",$arrParams) && !empty($arrParams['_to']) ){
			  $_to=http_build_query($arrParams);
			}else{
			  $_to.="_to=".APP_DIR."/member/&".http_build_query($arrParams);
			}

			// 如果傳入 _auth 則自動作登入動作(給APP 跳Webview時用)
			if(array_key_exists("_auth",$arrParams) && !empty($arrParams['_auth'])){
			  $_auth=base64_decode($arrParams['_auth']);
			  // $_auth格式  $auth_id|$auth_loginid|$auth_secret
			  $arrAuth=explode("|",$_auth);
			  // error_log("[c/member/userlogin] arrAuth : ".json_encode($arrAuth));
			  error_log("[c/member/auserlogin] auth_id : ".$arrAuth[0]);
			  error_log("[c/member/auserlogin] auth_loginid : ".$arrAuth[0]);
			  error_log("[c/member/auserlogin] : ".MD5($arrAuth[0].$arrAuth[1])."--".$arrAuth[2]);

			  if(MD5($arrAuth[0].$arrAuth[1])==$arrAuth[2]){
				// 驗證通過則做自動登入
				$get_user = $user->get_user($arrAuth[0]);
				if($get_user && setLogin($get_user)){
				  header("Location:".$arrParams['_to']);
				  return;
				}else{
				  header("Location:".APP_DIR);
				  return;
				}
			  }else{
				header("Location:".APP_DIR);
				return;
			  }
			}
		  }
		}else{
		  $_to="_to=".APP_DIR."/shareoscode/one_click_promote_scode_oauth_return_data_process/";
		}
		$tpl->assign('_to',$_to);
		$tpl->assign('_params',$arrParams);
		// Add End

		$admin 	        = empty($_POST['admin']) ? htmlspecialchars($_GET['admin']) : htmlspecialchars($_POST['admin']);
		$proxy_url 		= empty($_POST['proxy_url']) ? htmlspecialchars($_GET['proxy_url']) : htmlspecialchars($_POST['proxy_url']);
		$state 			= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
		$client_id 		= empty($_POST['client_id']) ? htmlspecialchars($_GET['client_id']) : htmlspecialchars($_POST['client_id']);
		$oauth_token 	= empty($_POST['oauth_token']) ? htmlspecialchars($_GET['oauth_token']) : htmlspecialchars($_POST['oauth_token']);
		$oauth_verifier = empty($_POST['oauth_verifier']) ? htmlspecialchars($_GET['oauth_verifier']) : htmlspecialchars($_POST['oauth_verifier']);

		$tpl->set_title('');
		$tpl->render("site","aclogin",true);

	}

	// 會員登入
	public function login3rd() {

		global $tpl, $user;

			//設定 Action 相關參數
			set_status($controller);

		error_log("[c/member/userlogin] GET params : ".json_encode($_GET));
		error_log("[c/member/userlogin] POST params : ".json_encode($_POST));

		// 20181206 Add By Thomas
		$arrParams = array();
			if(count($_GET)>0){
		  $arrParams=$_GET;
		}else if(count($_POST)>0){
		  $arrParams=$_POST;
		}
		$_to="";

		if(!empty($arrParams)){
		  unset($arrParams['member/login3rd/']);
		  unset($arrParams['act']);
		  unset($arrParams['fun']);

		  /* 入參說明
			 登入完成後要去的地方
			 _to=/site/deposit

			 APP localStorage 的$auth_id, $auth_loginid, $auth_secret
			 _auth=base64_encode($auth_id."|".$auth_loginid."|".$auth_secret)
		  */
		  if(is_array($arrParams) && count($arrParams)>0){
			if(array_key_exists("_to",$arrParams) && !empty($arrParams['_to']) ){
			  //$_to=http_build_query($arrParams);
			  $_to=$arrParams['_to'];
			}else{
			  $_to.="_to=".APP_DIR."/member/&".http_build_query($arrParams);
			}
			error_log("[c/member/userlogin] params : ".$_to);

			// 如果傳入 _auth 則自動作登入動作(給APP 跳Webview時用)
			if(array_key_exists("_auth",$arrParams) && !empty($arrParams['_auth'])){
			  $_auth=base64_decode($arrParams['_auth']);
			  // $_auth格式  $auth_id|$auth_loginid|$auth_secret
			  $arrAuth=explode("|",$_auth);
			  // error_log("[c/member/userlogin] arrAuth : ".json_encode($arrAuth));
			  error_log("[c/member/userlogin] auth_id : ".$arrAuth[0]);
			  error_log("[c/member/userlogin] auth_loginid : ".$arrAuth[0]);
			  error_log("[c/member/userlogin] : ".MD5($arrAuth[0].$arrAuth[1])."--".$arrAuth[2]);

			  if(MD5($arrAuth[0].$arrAuth[1])==$arrAuth[2]){
				// 驗證通過則做自動登入
				$get_user = $user->get_user($arrAuth[0]);
				if($get_user && setLogin($get_user)){
				  header("Location:".$arrParams['_to']);
				  return;
				}else{
				  header("Location:".APP_DIR);
				  return;
				}
			  }else{
				header("Location:".APP_DIR);
				return;
			  }
			}
		  }
		}else{
		  $_to="_to=".APP_DIR."/member/";
		}
		$tpl->assign('_to',$_to);
		$tpl->assign('_params',$arrParams);
		// Add End

			$admin 	        = empty($_POST['admin']) ? htmlspecialchars($_GET['admin']) : htmlspecialchars($_POST['admin']);
			$proxy_url 		= empty($_POST['proxy_url']) ? htmlspecialchars($_GET['proxy_url']) : htmlspecialchars($_POST['proxy_url']);
			$state 			= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
			$client_id 		= empty($_POST['client_id']) ? htmlspecialchars($_GET['client_id']) : htmlspecialchars($_POST['client_id']);
			$oauth_token 	= empty($_POST['oauth_token']) ? htmlspecialchars($_GET['oauth_token']) : htmlspecialchars($_POST['oauth_token']);
			$oauth_verifier = empty($_POST['oauth_verifier']) ? htmlspecialchars($_GET['oauth_verifier']) : htmlspecialchars($_POST['oauth_verifier']);

		$tpl->set_title('');
		$tpl->render("site","login_3rd",true);
	}

	/*
	 *	轉贈 取得餘額 Step1
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 */
	public function get_gift_counts() {
		global $tpl, $member,$deposit,$scodeModel,$oscModel;
		$oscModel = new OscodeModel;
		//$scodeModel = new scodeModel;
		$json 	 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		//判斷是否有登入會員
		login_required();
		//設定 Action 相關參數
		set_status($this->controller);

		//判斷是否為網頁介面
		if($json != 'Y'){
			//$tpl->assign('row_list', $get_list);
			//$tpl->set_title('');
			//$tpl->render("member","getStoreGiftSum",true);
		}else{
			$ret=getRetJSONArray(1,'OK','LIST');
			$retbate=($member->getRebate($userid));
			$couponY=($member->get_dscode($userid,'Y'));
			$couponN=($member->get_dscode($userid,'N'));
			$scode=($scodeModel->get_scode($userid));
			
			$oscode=$oscModel->get_oscode_list($userid);
			$spoint=($member->get_spoint($userid));
			$rebateable=($deposit->get_deposit_summy($userid)*1>0)?true:false;
			$ret['retObj']['rebate'] =$retbate['amount']*1;
			$ret['retObj']['couponY'] =$couponY['amount']*1;
			$ret['retObj']['couponN'] =$couponN['amount']*1;
			$ret['retObj']['spoint'] =$spoint['amount']*1;
			$ret['retObj']['scode'] =$scode;
			$ret['retObj']['oscode'] =$oscode['table']['record'];	
			$ret['retObj']['rebateable'] =$rebateable;
			unset($ret['retObj']['data']);
			echo json_encode($ret);
		}
	}

		/*
	 *	轉贈 取得餘額 Step1
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 */
	public function transfer_gift() {
		//error_reporting(E_ALL); ini_set("display_errors", 1);
		global $db,$tpl, $member,$config;
		$json 	 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$qty = empty($_POST['qty']) ? htmlspecialchars($_GET['qty']) : htmlspecialchars($_POST['qty']);
		$gift_type = empty($_POST['gift_type']) ? htmlspecialchars($_GET['gift_type']) : htmlspecialchars($_POST['gift_type']);
		$receiver  =empty($_POST['receiver']) ? htmlspecialchars($_GET['receiver']) : htmlspecialchars($_POST['receiver']);
		$productid  =empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		//判斷是否有登入會員
		login_required();
		//設定 Action 相關參數
		set_status($this->controller);

		//判斷是否為網頁介面
		if($json != 'Y'){
			//$tpl->assign('row_list', $get_list);
			//$tpl->set_title('');
			//$tpl->render("member","getStoreGiftSum",true);
		}else{
			$receiver=explode(",",$receiver);
			$ret=getRetJSONArray(1,'OK','LIST');
			include_once(LIB_DIR ."/convertString.ini.php");
			$this->str = new convertString();
			$expw= empty($_POST['expw']) ? $_GET['expw'] : $_POST['expw'];
			$exchangepasswd = $this->str->strEncode($expw, $config['encode_key']);

			$ret['err'] = '';

			$query = "SELECT *
			FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user`
			WHERE
				prefixid = '{$config['default_prefix_id']}'
				AND userid = '{$userid}'
				AND exchangepasswd = '{$exchangepasswd}'
				AND switch = 'Y'
			LIMIT 1
			";

			$table = $db->getQueryRecord($query);
			if(empty($table['table']['record'][0])) {
				//'兌換密碼錯誤'
				$ret['retCode']=50001;
				$ret['retMsg']='兌換密碼錯誤 !!';
				$ret['err'] = 5;
			} else {
				error_log("1459 $gift_type");
				switch ($gift_type){
					case 'rebate':
						$rtn=$member->transRebate($userid,$receiver,$qty);
					break;
					case 'couponY':
						$rtn=$member->transCoupon($userid,$receiver,$qty,'Y');
					break;
					case 'couponN':
						$rtn=$member->transCoupon($userid,$receiver,$qty,'N');
					break;
					case 'spoint':
						$rtn=$member->transSpoint($userid,$receiver,$qty);
					break;	
					case 'scode':
						$title_txt="超級殺價券";
						$rtn=$member->transScode($userid,$receiver,$qty);
					break;
					case 'oscode':
						$title_txt="殺價券";
						$rtn=$member->transOscode($userid,$receiver,$qty,$productid);
					break;																
				}
				if ($rtn<0){
					$ret['retCode']=60001;
					$ret['retMsg']=($gift_type=='rebate')?'餘額不足 !!':'剩餘張數不足!!';
					$ret['err'] = 6;
				}else{
					/*測站不加
					//推播
					$inp=array();
					$inp['title'] = "贈送{$title_txt}通知";
					$inp['productid'] = 0;
					$inp['action'] = 4;
					$inp['url_title'] = '';
					$inp['url'] = '';
					$inp['sender'] = '1';
					$inp['page'] = 28;
					$inp['target'] = 'saja';
					
					for ($i=0;$i<sizeof($receiver);$i++){
						$inp['sendto'] = $receiver[$i];
						$inp['body'] = "好友會員:{$userid}，贈送給:{$receiver[$i]} {$title_txt}: {$qty} 已設定完成";
						$rtn=sendFCM($inp);
					}	
					*/			
					unset($ret['retObj']);
				}
			}
			echo json_encode($ret);
		}
	}

    /*
	 *	轉贈 取得
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 */
	public function get_receivers() {
		global $tpl, $member,$user;
		$json 	 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid 	= empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);

		$receiver  =empty($_POST['receiver']) ? htmlspecialchars($_GET['receiver']) : htmlspecialchars($_POST['receiver']);
		//判斷是否有登入會員
		login_required();
		//設定 Action 相關參數
		set_status($this->controller);

		//判斷是否為網頁介面
		if($json != 'Y'){
			//$tpl->assign('row_list', $get_list);
			//$tpl->set_title('');
			//$tpl->render("member","getStoreGiftSum",true);
		}else{
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']=$user->getUsersSinfo(explode(",",$receiver));
			echo json_encode($ret);
		}
	}


	//購物金單頁
	public function rebate() {
		global $tpl, $config, $deposit,$member;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		//紅利積點數量
		$get_bonus = $member->get_bonus($this->userid);
			// if($get_bonus['amount']<0) {
			//    $get_bonus['amount']=0;
			// }
		$tpl->assign('bonus', $get_bonus);		
		//購物金數量
		$get_rebate = $member->get_rebate($this->userid);
		if(!$get_rebate){
			$get_rebate=array("userid"=>$this->userid, "amount"=>0);
		}
		$tpl->assign('rebate', $get_rebate);
		$tpl->set_title('購物金');
		$tpl->render("member","rebate",true);
	}

	//殺價卷單頁
	public function codes() {
		global $tpl, $config, $oscode,$scodeModel;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		//殺價券數量
		$get_oscode=$oscode->get_oscode($this->userid);
		$tpl->assign('oscode',$get_oscode);

		// S碼 現有可用總數
		$get_scode = $scodeModel->get_scode($this->userid);
		$tpl->assign('scode',$get_scode);
		$tpl->set_title('殺價卷');
		$tpl->render("member","codes",true);
	}
	//填寫推薦人資訊
	public function set_affiliate() {
		global $tpl,  $user;
		$userid = empty($_SESSION['auth_id'])?htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		error_log("auth_id->".$userid);
		login_required();
		//推薦人資料
		$user_referral = $user->getAffiliateUser($userid);

		if(!empty($user_referral['intro_by']) ){
			header("location:/site/user/profile/".date("YmdHis"));
			die();
		}else{
			//設定 Action 相關參數
			set_status($this->controller);
			$tpl->assign('userid',$userid);
			$tpl->set_title('填寫推薦人');
			$tpl->render("member","set_affiliate",true);			
		}

	}	

	//購物金輸入受贈方/數量 
	public function rebate_cho_receiver() {
		global $tpl, $config, $deposit,$member;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		
		//購物金數量
		$get_rebate = $member->get_rebate($this->userid);
		if(!$get_rebate){
			$get_rebate=array("userid"=>$this->userid, "amount"=>0);
		}
		$tpl->assign('rebate', $get_rebate['amount']);
		$tpl->assign('userid', $userid);
		$tpl->set_title('轉贈購物金');
		$tpl->render("member","rebate_cho_receiver",true);
	}

	//購物金輸入交易密碼 
	public function rebate_confirm() {
		global $tpl, $config, $deposit,$member;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$receiver = empty($_POST['receiver']) ? htmlspecialchars($_GET['receiver']) : htmlspecialchars($_POST['receiver']);
		$qty = empty($_POST['qty']) ? htmlspecialchars($_GET['qty']) : htmlspecialchars($_POST['qty']);
		//購物金數量
		$get_rebate = $member->get_rebate($this->userid);
		if(!$get_rebate){
			$get_rebate=array("userid"=>$this->userid, "amount"=>0);
		}

		$get_member = $member->get_info($receiver);
		if($get_member){
			if(empty($get_member['thumbnail_url'])){
				if(empty($get_member['thumbnail_file'])){
					$get_member['thumbnail_file']="_DefaultHeadImg.jpg";
				}
			}

		}else{
			$get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg";
		}
		$tpl->assign('member', $get_member);

		$tpl->assign('rebate', $get_rebate['amount']);
		$tpl->assign('userid', $userid);
		$tpl->assign('receiver', $receiver);
		$tpl->assign('qty', $qty);
		$tpl->set_title('轉贈確認');
		$tpl->render("member","rebate_confirm",true);
	}	
	
	//殺幣輸入受贈方/數量 
	public function spoint_cho_receiver() {
		global $tpl, $config, $deposit,$member;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		//殺幣數量
		$get_spoint = $member->get_spoint($this->userid);
		if(!$get_spoint){
			$get_spoint=array("userid"=>$this->userid, "amount"=>0);
		}	

		$tpl->assign('spoint', $get_spoint['amount']);
		$tpl->assign('userid', $userid);
		$tpl->set_title('轉贈殺價幣');
		$tpl->render("member","spoint_cho_receiver",true);
	}

	//殺幣輸入交易密碼 
	public function spoint_confirm() {
		global $tpl, $config, $deposit,$member;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$receiver = empty($_POST['receiver']) ? htmlspecialchars($_GET['receiver']) : htmlspecialchars($_POST['receiver']);
		$qty = empty($_POST['qty']) ? htmlspecialchars($_GET['qty']) : htmlspecialchars($_POST['qty']);
		//殺幣數量
		$get_spoint = $member->get_spoint($this->userid);
		if(!$get_spoint){
			$get_spoint=array("userid"=>$this->userid, "amount"=>0);
		}	

		$get_member = $member->get_info($receiver);
		if($get_member){
			if(empty($get_member['thumbnail_url'])){
				if(empty($get_member['thumbnail_file'])){
					$get_member['thumbnail_file']="_DefaultHeadImg.jpg";
				}
			}

		}else{
			$get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg";
		}
		$tpl->assign('member', $get_member);

		$tpl->assign('spoint', $get_spoint['amount']);
		$tpl->assign('userid', $userid);
		$tpl->assign('receiver', $receiver);
		$tpl->assign('qty', $qty);
		$tpl->set_title('轉贈確認');
		$tpl->render("member","spoint_confirm",true);
	}
	
	//超級殺價券輸入受贈方/數量 
	public function scode_cho_receiver() {
		global $tpl, $config, $scodeModel,$member;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		//超級殺價券數量
		$get_scode = $scodeModel->get_scode($this->userid);
		$tpl->assign('scode',$get_scode);
		$tpl->assign('userid', $userid);
		$tpl->set_title('轉贈超級殺價券');
		$tpl->render("member","scode_cho_receiver",true);
	}

	//超級殺價券輸入交易密碼 
	public function scode_confirm() {
		global $tpl, $config, $scodeModel,$member;
		set_status($this->controller);
		login_required();
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$receiver = empty($_POST['receiver']) ? htmlspecialchars($_GET['receiver']) : htmlspecialchars($_POST['receiver']);
		$qty = empty($_POST['qty']) ? htmlspecialchars($_GET['qty']) : htmlspecialchars($_POST['qty']);
		//超級殺價券數量
		$get_scode = $scodeModel->get_scode($this->userid);
		$tpl->assign('scode',$get_scode);

		$get_member = $member->get_info($receiver);
		if($get_member){
			if(empty($get_member['thumbnail_url'])){
				if(empty($get_member['thumbnail_file'])){
					$get_member['thumbnail_file']="_DefaultHeadImg.jpg";
				}
			}

		}else{
			$get_member['thumbnail_url']=IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg";
		}
		$tpl->assign('member', $get_member);
		$tpl->assign('userid', $userid);
		$tpl->assign('receiver', $receiver);
		$tpl->assign('qty', $qty);
		$tpl->set_title('轉贈確認');
		$tpl->render("member","scode_confirm",true);
	}		
}
