<?php
/*
 * Deposit Controller 儲值
 */
 
include_once(LIB_DIR."/convertString.ini.php");
include_once(LIB_DIR."/helpers.php");
require_once(LIB_DIR."/WxpayAPI/WxPay.JsApiPay.php");

class Deposit {
	public $controller = array();
	public $params = array();
	public $id;
	public $_ARR_ALLOW_IP=['210.71.254.62','203.66.45.133','211.75.238.38','60.251.120.84','210.65.89.147','210.61.2.243'];
	public $_SALT='sJw#333';


	/*
	 * Default home page.
	 */
	public function home() {

		global $tpl, $deposit, $product, $member, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

        // 未手機驗證者不給充值
		$arrCond=array("userid"=>$userid, "switch"=>"Y");
		$auth = $user->getSmsAuthData($arrCond);
		error_log("[c/deposit/home] check if user ${userid} phone is validate : ".$auth[0]['verified']);
		if($auth[0]['verified']!='Y') {
		   $_able["deposit_enable"]='N';
		}

		// 檢查用戶60分鐘內是否充值超過3次
		$gap_mins=60;
		$gap_limit=3;
		$ban =  $this->chk_swipe_credit_count($userid,$gap_mins,$gap_limit);


		error_log("[c/deposit/home] check swipe creditcard ${gap_limit} times in last ${gap_mins} mins for ${userid} : ".$ban);
		if($ban==true) {
		    $_able["deposit_enable"]='N';

			// 鎖住下標兌換
			set_bid_able($userid,"deposit_enable","N");
			set_bid_able($userid,"exchange_enable","N");
			set_bid_able($userid,"bid_enable","N");
			set_bid_able($userid,"dream_enable","N");

			//  推播通知
			$inp=array();
			$inp['title']="用戶 ${userid} 頻繁充值警示 [".date("Y-m-d H:i:s")."]";
			$inp['body']=" 用戶 ${userid} 於過去 ${gap_mins} 內刷卡超過 ${gap_limit} 次  已先暫停資格 !!";
			$inp['productid']='';
			$inp['action']="";
			$inp['url_title']="";
			$inp['url']="";
			$inp['sender']="99";
			$inp['sendto']="26958,28,2788,1705,4068";
			$inp['page']=0;
			$inp['target']='saja';
			$rtn=sendFCM($inp);
		}

		//查詢儲值功能狀態 AARONFU
		$_able = query_bid_able($userid);
		if($_able["deposit_enable"] !=='Y') {
			if($json == 'Y'){
				$ret = array('retCode'=>1,
					'retMsg'=>"OK",
					'action_list'=>array(
							"type"=>5,
							"page"=>9,
							"msg"=>"儲值功能鎖定中,請洽客服"
						)
					);
				echo json_encode($ret);
				exit;
			} else {
				return_to('site/member');
			}
		}

		$tpl->assign('app', $app);


		/* 取得一小時內儲值超過3次的名單
		$table=$deposit->count_swipe_creditcard(60,3);
		if($table) {

		   // 檢查該用戶是否在名單中, 有的話就不傳
		   foreach($table['table']['record'] as $item) {
              if($item['userid']==$userid && $item['cnt']>3) {
				 error_log("[c/deposit/home] ${userid} swipe ".$item['cnt']." times.");
				 $_able["deposit_enable"]='N';
				 set_bid_able($userid,"deposit_enable","N");
				 set_bid_able($userid,"exchange_enable","N");
				 set_bid_able($userid,"bid_enable","N");
				 set_bid_able($userid,"dream_enable","N");

				 //  推播通知
				 $inp=array();
				 $inp['title']="用戶 ${userid} 頻繁充值提醒 [".date("Y-m-d H:i:s")."]";
				 $inp['body']=" 用戶 : ${userid} 被發現於過去1小時內刷卡超過3次  已先暫停資格 !!";
				 $inp['productid']='';
				 $inp['action']="";
				 $inp['url_title']="";
				 $inp['url']="";
				 $inp['sender']="99";
				 $inp['sendto']="26958,28,2788,1705";
				 $inp['page']=0;
				 $inp['target']='saja';
				 $rtn=sendFCM($inp);
                 break;
			  }
		   }
		}
		*/
		//取得支付項目
		$get_drid_list = $deposit->row_drid_list($json, $_able["deposit_enable"]);
		if($userid !=='1705' && !empty($get_drid_list) ){
			foreach ($get_drid_list as $key => $value){
				if($value['drid']=='26'){ unset($get_drid_list[$key]); }
			}
		}

		$tpl->assign('get_drid_list', $get_drid_list);

		//取得殺價幣餘額
		$get_spoint = $member->get_spoint($userid);

		if(!$get_spoint){
			$get_spoint=array("userid"=>$userid, "amount"=>0);
		}

		$get_spoint['amount'] = sprintf("%1\$u", $get_spoint['amount']);
		$tpl->assign('spoint', $get_spoint);

		//會員卡包資料
		$user_extrainfo_category = $user->check_user_extrainfo_category();

		if(is_array($user_extrainfo_category)){
			foreach ($user_extrainfo_category as $key => $value){
				if($value['uecid'] == 8){
					if($json!='Y' || $app == 'Y'){
						$user_extrainfo[$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}else{
						$user_extrainfo['a'.$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}
				}
			}
		}

		$tpl->assign('user_extrainfo', $user_extrainfo);


		// 20180809 By Thomas 取得該支付的綁定虛擬帳號
		$arrPayAcct=array();
		if(!empty($get_drid_list)){
			// 統計儲值次數
			$deposit_conut = $deposit->get_Deposit_Count($userid);

			//判斷是否第一次充值
			if($deposit_conut['num'] > 0 ){
				$firstdeposit = 'N';
			} else{
				$firstdeposit = 'Y';
			}

			//重機首充儲值送活動，活動內容寫死 20190910
			if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00'){
				$firstdeposit = 'N';
			}

			//取得首儲送的倍數
			$deposit_times = $deposit->getDepositTimes();
			$tpl->assign('times', $deposit_times);

			foreach ($get_drid_list as $key => $value){

				if($json != 'Y' || $app == 'Y') {
					// 取得支付項目細項
					$row_list[$value['drid']] = $deposit->row_list($value['drid']);
					$tpl->assign('row_list', $row_list);
				} else if($json == 'Y') {
					// API結構輸出變更專用
					$row_list2 = $deposit->row_list($value['drid']);
				}

				// 20180809 By Thomas 取得該用戶在該支付項目的虛擬帳號 (以 sso_deposit_rule.act欄位值為關連)
				if($value['act']) {
					$sso_row = $user->get_user_sso_list($userid,$value['act']);
					if($sso_row) {
						// ex : $arrPayVendor['gama']=....
						$arrPayAcct[$value['act']] =$sso_row;
					}
				}

				if($json != 'Y' || $app == 'Y'){

					foreach ($row_list[$value['drid']] as $rk => $rv){

						$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);

						if(!empty($get_scode_promote)){
							$i = 1;
							$scode_promote_memo[$rv['driid']] = array();
							foreach ($get_scode_promote as $sk => $sv) {
								if($sv['num']>0) {
									$scode_promote_memo[$rv['driid']][$sk]['name'].= $sv['name'];
									$scode_promote_memo[$rv['driid']][$sk]['num'].= $sv['num'].' ';
									if ($firstdeposit == 'N'){
										$scode_promote_memo[$rv['driid']][$sk]['firstdeposit_text'].= '';
									}else{
										$scode_promote_memo[$rv['driid']][$sk]['firstdeposit_text'].= 'x'.$deposit_times['times'];
									}
								}else{
								   $scode_promote_memo[$rv['driid']][$sk]['num'].=$i.". ".$sv['name'].'<br>';
								}

								$scode_promote_memo[$rv['driid']][$sk]['offtime'].=$sv['offtime'].' ';
								$i++;
							}
						}else{
							$scode_promote_memo[$rv['driid']][0]['num'] .= "無贈送任何東西";
							$scode_promote_memo[$rv['driid']][0]['offtime'] .= "";
						}
						//重機首充儲值送活動，活動內容寫死 20190910
						if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00' && $deposit_conut['num'] == 0){
							switch($rv['driid']){
								//300
								case 55:
								case 107:
									$scode_promote_memo[$rv['driid']][0]['firstdeposit_text'].= '';
									$scode_promote_memo[$rv['driid']][0]['name'] = "首次儲值加碼送重機卷";
									$scode_promote_memo[$rv['driid']][0]['num'] = "1";
								break;
								//2000
								case 56:
								case 173:
									$scode_promote_memo[$rv['driid']][0]['firstdeposit_text'].= '';
									$scode_promote_memo[$rv['driid']][0]['name'] = "首次儲值加碼送重機卷";
									$scode_promote_memo[$rv['driid']][0]['num'] = "7";
								break;
								//10000
								case 59:
								case 110:
									$scode_promote_memo[$rv['driid']][0]['firstdeposit_text'].= '';
									$scode_promote_memo[$rv['driid']][0]['name'] = "首次儲值加碼送重機卷";
									$scode_promote_memo[$rv['driid']][0]['num'] = "34";
								break;
								//30000
								case 60:
								case 111:
									$scode_promote_memo[$rv['driid']][0]['firstdeposit_text'].= '';
									$scode_promote_memo[$rv['driid']][0]['name'] = "首次儲值加碼送重機卷";
									$scode_promote_memo[$rv['driid']][0]['num'] = "101";
								break;
								//50000
								case 176:
								case 182:
									$scode_promote_memo[$rv['driid']][0]['firstdeposit_text'].= '';
									$scode_promote_memo[$rv['driid']][0]['name'] = "首次儲值加碼送重機卷";
									$scode_promote_memo[$rv['driid']][0]['num'] = "167";
								break;
								//100000
								case 179:
								case 185:
									$scode_promote_memo[$rv['driid']][0]['firstdeposit_text'].= '';
									$scode_promote_memo[$rv['driid']][0]['name'] = "首次儲值加碼送重機卷";
									$scode_promote_memo[$rv['driid']][0]['num'] = "334";
								break;
							}
							$scode_promote_memo[$rv['driid']][0]['offtime'] = '';
						}
					}

					$tpl->assign('scode_promote_memo', $scode_promote_memo);
					$tpl->assign('firstdeposit', $firstdeposit);

				}else if($json == 'Y'){

					foreach ($row_list2 as $rk => $rv){
						$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);
						if(!empty($get_scode_promote)){
							$i = 1;
							foreach($get_scode_promote as $sk => $sv){
								if($sv['num']>0){
									if ($firstdeposit == 'N'){
										$row_list2[$rk]['scode_promote_memo'][$sk]['firstdeposit_text']= '';
										$row_list2[$rk]['scode_promote_memo'][$sk]['name']=$sv['name'];
										$row_list2[$rk]['scode_promote_memo'][$sk]['num']=$sv['num'].' 張';
									}else{
										$row_list2[$rk]['scode_promote_memo'][$sk]['firstdeposit_text']= 'x'.$deposit_times['times'];
										$row_list2[$rk]['scode_promote_memo'][$sk]['name']=$sv['name'];
										$row_list2[$rk]['scode_promote_memo'][$sk]['num']=$sv['num'].' 張';
									}
									// add thumbnail by Thomas 20190712
									if(!empty($sv['thumbnail'])) {
									   $row_list2[$rk]['scode_promote_memo'][$sk]['thumbnail']=$sv['thumbnail'];
									} else if(!empty($sv['thumbnail_url'])) {
									   $row_list2[$rk]['scode_promote_memo'][$sk]['thumbnail']=$sv['thumbnail_url'];
									}
									// add End
									$row_list2[$rk]['scode_promote_memo'][$sk]['offtime'] .= $sv['offtime'].' ';
								}else{
									$row_list2[$rk]['scode_promote_memo'][$sk]['name']=$sv['name'];
									$row_list2[$rk]['scode_promote_memo'][$sk]['num']= '0 張';
									$row_list2[$rk]['scode_promote_memo'][$sk]['offtime'] .= $sv['offtime'].' ';
								}
								$i++;
							}
						}else{
							// $row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
							// $row_list2[$rk]['scode_promote_memo'][0]['name'] = "無贈送任何東西";
							// $row_list2[$rk]['scode_promote_memo'][0]['num'] = '0 張';
							// $row_list2[$rk]['scode_promote_memo'][0]['offtime'] .= '';
							$row_list2[$rk]['scode_promote_memo'] = array();
						}
						//重機首充儲值送活動，活動內容寫死 20190910
						if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00' && $deposit_conut['num'] == 0){
							switch($rv['driid']){
								//300
								case 55:
								case 107:
									$row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
									$row_list2[$rk]['scode_promote_memo'][0]['name'] = "首次儲值加碼送重機卷";
									$row_list2[$rk]['scode_promote_memo'][0]['num'] = "1 張";
								break;
								//2000
								case 56:
								case 173:
									$row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
									$row_list2[$rk]['scode_promote_memo'][0]['name'] = "首次儲值加碼送重機卷";
									$row_list2[$rk]['scode_promote_memo'][0]['num'] = "7 張";
								break;
								//10000
								case 59:
								case 110:
									$row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
									$row_list2[$rk]['scode_promote_memo'][0]['name'] = "首次儲值加碼送重機卷";
									$row_list2[$rk]['scode_promote_memo'][0]['num'] = "34 張";
								break;
								//30000
								case 60:
								case 111:
									$row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
									$row_list2[$rk]['scode_promote_memo'][0]['name'] = "首次儲值加碼送重機卷";
									$row_list2[$rk]['scode_promote_memo'][0]['num'] = "101 張";
								break;
								//50000
								case 176:
								case 182:
									$row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
									$row_list2[$rk]['scode_promote_memo'][0]['name'] = "首次儲值加碼送重機卷";
									$row_list2[$rk]['scode_promote_memo'][0]['num'] = "167 張";
								break;
								//100000
								case 179:
								case 185:
									$row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
									$row_list2[$rk]['scode_promote_memo'][0]['name'] = "首次儲值加碼送重機卷";
									$row_list2[$rk]['scode_promote_memo'][0]['num'] = "334 張";
								break;
							}
							$row_list2[$rk]['scode_promote_memo'][0]['offtime'] = '';
						}
					}
				}

				$get_drid_list[$key]['row_list'] = empty($row_list2) ? array() : $row_list2;
			}

			// 用戶支付項目的虛擬帳號
			$tpl->assign('pay_account_list', $arrPayAcct);

		}
		if ($app == 'Y') {
			if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00' && $deposit_conut['num'] == 0){
				$tpl->assign('firstdeposit','Y');
			}
			$tpl->assign('header','N');
			$tpl->set_title('');
			$tpl->render("deposit","home",true,false,$app);
		}else if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_drid_list;
			$ret['retObj']['spoint'] = $get_spoint;
			$ret['retObj']['firstdeposit'] = $firstdeposit;
			if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00' && $deposit_conut['num'] == 0){
				$ret['retObj']['firstdeposit'] = 'Y';
			}
			$ret['retObj']['times'] = empty($deposit_times) ? null : $deposit_times;
			echo json_encode($ret);
			exit;
		}else if($json != 'Y'){
			if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00' && $deposit_conut['num'] == 0){
				$tpl->assign('firstdeposit','Y');
			}
			$tpl->assign('footer','N');
			$tpl->set_title('');
			$tpl->render("deposit","home",true);
		}

	}

	/*
	 * Temp home page.
	 */
	public function home_temp() {

		global $tpl, $deposit, $product, $member, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		$json='';

		//取得支付項目
		$get_drid_list = $deposit->row_drid_list($json);
		$tpl->assign('get_drid_list', $get_drid_list);

		//取得殺價幣餘額
		$get_spoint = $member->get_spoint($userid);

		if(!$get_spoint){
			$get_spoint=array("userid"=>$userid, "amount"=>0);
		}

		$get_spoint['amount'] = sprintf("%1\$u", $get_spoint['amount']);
		$tpl->assign('spoint', $get_spoint);

		//會員卡包資料
		$user_extrainfo_category = $user->check_user_extrainfo_category();

		if(is_array($user_extrainfo_category)){
			foreach ($user_extrainfo_category as $key => $value){
				if($value['uecid'] == 8){
					if($json!='Y'){
						$user_extrainfo[$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}else{
						$user_extrainfo['a'.$value['uecid']] = $user->get_user_extrainfo($value['uecid']);
					}
				}
			}
		}

		$tpl->assign('user_extrainfo', $user_extrainfo);


		// 20180809 By Thomas 取得該支付的綁定虛擬帳號
		$arrPayAcct=array();
		if(!empty($get_drid_list)){
			// 統計儲值次數
			$deposit_conut = $deposit->get_Deposit_Count($userid);

			//判斷是否第一次充值
			if($deposit_conut['num'] > 0 ){
				$firstdeposit = 'N';
			} else{
				$firstdeposit = 'Y';
			}

			foreach ($get_drid_list as $key => $value){

				if($json != 'Y') {
					// 取得支付項目細項
					$row_list[$value['drid']] = $deposit->row_list($value['drid']);
					$tpl->assign('row_list', $row_list);
				} else {
					// API結構輸出變更專用
					$row_list2 = $deposit->row_list($value['drid']);
				}

				// 20180809 By Thomas 取得該用戶在該支付項目的虛擬帳號 (以 sso_deposit_rule.act欄位值為關連)
				if($value['act']) {
					$sso_row = $user->get_user_sso_list($userid,$value['act']);
					if($sso_row) {
						// ex : $arrPayVendor['gama']=....
						$arrPayAcct[$value['act']] =$sso_row;
					}
				}

				if($json != 'Y'){

					foreach ($row_list[$value['drid']] as $rk => $rv){

						$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);

						if(!empty($get_scode_promote)){
							$i = 1;
							$scode_promote_memo[$rv['driid']] = array();
							foreach ($get_scode_promote as $sk => $sv) {
								if($sv['num']>0) {
									$scode_promote_memo[$rv['driid']][$sk]['name'].= $sv['name'];
									$scode_promote_memo[$rv['driid']][$sk]['num'].= $sv['num'].' ';
									if ($firstdeposit == 'N'){
										$scode_promote_memo[$rv['driid']][$sk]['firstdeposit_text'].= '';
									}else{
										$scode_promote_memo[$rv['driid']][$sk]['firstdeposit_text'].= 'x2';
									}
								}else{
								   $scode_promote_memo[$rv['driid']][$sk]['num'].=$i.". ".$sv['name'].'<br>';
								}

								$scode_promote_memo[$rv['driid']][$sk]['offtime'].=$sv['offtime'].' ';
								$i++;
							}
						}else{
							$scode_promote_memo[$rv['driid']][0]['num'] .= "無贈送任何東西";
							$scode_promote_memo[$rv['driid']][0]['offtime'] .= "";
						}
					}

					$tpl->assign('scode_promote_memo', $scode_promote_memo);
					$tpl->assign('firstdeposit', $firstdeposit);

				}else{

					foreach ($row_list2 as $rk => $rv){
						$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);
						if(!empty($get_scode_promote)){
							$i = 1;
							foreach($get_scode_promote as $sk => $sv){
								if($sv['num']>0){
									if ($firstdeposit == 'N'){
										$row_list2[$rk]['scode_promote_memo'][$sk]['firstdeposit_text']= '';
										$row_list2[$rk]['scode_promote_memo'][$sk]['name']=$sv['name'];
										$row_list2[$rk]['scode_promote_memo'][$sk]['num']=$sv['num'].' 張';
									}else{
										$row_list2[$rk]['scode_promote_memo'][$sk]['firstdeposit_text']= 'x2';
										$row_list2[$rk]['scode_promote_memo'][$sk]['name']=$sv['name'];
										$row_list2[$rk]['scode_promote_memo'][$sk]['num']=$sv['num'].' 張';
									}
									// add thumbnail by Thomas 20190712
									if(!empty($sv['thumbnail'])) {
									   $row_list2[$rk]['scode_promote_memo'][$sk]['thumbnail']=$sv['thumbnail'];
									} else if(!empty($sv['thumbnail_url'])) {
									   $row_list2[$rk]['scode_promote_memo'][$sk]['thumbnail']=$sv['thumbnail_url'];
									}
									// add End
									$row_list2[$rk]['scode_promote_memo'][$sk]['offtime'] .= $sv['offtime'].' ';
								}else{
									$row_list2[$rk]['scode_promote_memo'][$sk]['name']=$sv['name'];
									$row_list2[$rk]['scode_promote_memo'][$sk]['num']= '0 張';
									$row_list2[$rk]['scode_promote_memo'][$sk]['offtime'] .= $sv['offtime'].' ';
								}
								$i++;
							}
						}else{
							$row_list2[$rk]['scode_promote_memo'][0]['firstdeposit_text']= '';
							$row_list2[$rk]['scode_promote_memo'][0]['name'] = "無贈送任何東西";
							$row_list2[$rk]['scode_promote_memo'][0]['num'] = '0 張';
							$row_list2[$rk]['scode_promote_memo'][0]['offtime'] .= '';
						}
					}
				}

				$get_drid_list[$key]['row_list'] = $row_list2;
			}

			// 用戶支付項目的虛擬帳號
			$tpl->assign('pay_account_list', $arrPayAcct);

		}

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_drid_list;
			$ret['retObj']['spoint'] = $get_spoint;
			$ret['retObj']['firstdeposit'] = $firstdeposit;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('footer','N');
			$tpl->assign('header','N');
			$tpl->set_title('');
			$tpl->render("deposit","home",true);
		}

	}


	public function is_test_id($userid) {

		if($userid==1705 || $userid==996 || $userid==997 || $userid==9 || $userid==28 || $userid==116 || $userid==1 || $userid==1476 || $userid==2186 || $userid==1271) {
		  return true;
		}else{
		  return false;
		}

	}


	public function confirm() {

		global $tpl, $config, $deposit, $product, $member, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid=empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
		$driid=empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$app = empty($_POST['app']) ? htmlspecialchars($_GET['app']) : htmlspecialchars($_POST['app']);
		$tpl->assign('app', $app);

		error_log("[c/deposit] drid:".$drid." , driid:".$driid." , userid:".$userid);

		//殺幣數量
		$get_spoint = $member->get_spoint($userid);
		if(!$get_spoint){
			$get_spoint=array("userid"=>$userid, "amount"=>0);
		}
		$tpl->assign('spoint', $get_spoint);

		//充值項目ID
		$row_list = $deposit->row_list($drid);
		$tpl->assign('row_list', $row_list);

		$deposit_rule = $deposit->deposit_rule($drid);
		$tpl->assign('deposit_rule', $deposit_rule);

		foreach($row_list as $rk => $rv){

			if((int)$rv['driid']==(int)($driid)){

				//取得儲值點數
				$get_deposit['drid'] = $drid;
				$get_deposit['driid'] = $rv['driid'];
				$get_deposit['name'] = $rv['name'];
				$get_deposit['amount'] = floatval($rv['amount']);
				$get_deposit['spoint'] = $rv['spoint'];
				break;

			}else{
			  continue;
			}

		} //endforeach;

		//取得首儲送的倍數
		$deposit_times = $deposit->getDepositTimes();

		if($drid=='7' || $drid=='9') { //Hinet & 阿拉訂 : 點數兌換時才生訂單

			$get_deposit['drid']=$drid;

		}else if($drid != '7' && $drid !='9'){

			//新增deposit_history資訊
			$currenty=$config['currency'];
			if($drid=='6' || $drid=='8' || $drid=='10'){
				$currency="NTD";
			}

			// if($this->is_test_id($userid)){
				// $get_deposit['amount']=2;
			// }
			$ip = GetIP();

			$depositid = $deposit->add_deposit($userid, $get_deposit['amount'],$currency);
			$spointid = $deposit->add_spoint($userid, $get_deposit['spoint']);
			$dhid = $deposit->add_deposit_history($userid, $driid, $depositid, $spointid, $ip);
			$get_scode_promote = $deposit->get_scode_promote_rt($driid);

			if(!empty($get_scode_promote)){

				// 統計儲值次數
				$deposit_conut = $deposit->get_Deposit_Count($userid);

				//判斷是否第一次充值
				if($deposit_conut['num'] > 0){
					$firstdeposit = 'N';
				} else{
					$firstdeposit = 'Y';
				}

				//重機首充儲值送活動，活動內容寫死 20190910
				if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00'){
					$firstdeposit = 'N';
				}

				$i = 1;
				$spmemo = '';
				$spmemototal = 0;

				foreach ($get_scode_promote as $sk => $sv){

					$get_product_info = $product->get_info($sv['productid']);
					$spmemo.='<li class="d-flex align-items-center">
					<div class="de_title"><p>送 殺價券</p><p>( '.$get_product_info['name'].' )</p></div><div class="de_point">';
					$spmemoapp[$sk]['name'] = "送 殺價券".$get_product_info['name'];

					if(!empty($sv['num'])){
						if ($firstdeposit == 'N'){
							$spmemo.=$sv['num'].' 張';
							$spmemoapp[$sk]['num'] = $sv['num'].' 張';
						}else{
							$spmemo.=($sv['num']*$deposit_times['times']).' 張';
							$spmemoapp[$sk]['num'] = ($sv['num']*$deposit_times['times']).' 張';
						}
						$spmemototal = ($spmemototal+($sv['num']*$get_product_info['saja_fee']));
					}

					if(!empty($get_product_info['productid'])) {
						$spmemo.='</div></li>';
					}

					$i++;

				}

			}else{

				$spmemo .= "<li>無贈送任何東西</li>";
				$spmemoapp = array();
				// $spmemoapp[0]['name'] = "無贈送任何東西";
				// $spmemoapp[0]['num'] = " 0 張";

			}

			//重機首充儲值送活動，活動內容寫死 20190910
			if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00' && $deposit_conut['num'] == 0){
				$spmemo = '';
				$spmemototal = 0;
				$spmemo.='<li class="d-flex align-items-center">
				<div class="de_title"><p>送 殺價券</p><p>( Suzuki SV 650 )</p></div><div class="de_point">';
				$spmemoapp[0]['name'] = "送 殺價券".'Suzuki SV 650';
				switch($driid){
					//300
					case 55:
					case 107:
						$spmemo.='1 張';
						$spmemoapp[0]['num'] = '1 張';
					break;
					//2000
					case 56:
					case 173:
						$spmemo.='7 張';
						$spmemoapp[0]['num'] = '7 張';
					break;
					//10000
					case 59:
					case 110:
						$spmemo.='34 張';
						$spmemoapp[0]['num'] = '34 張';
					break;
					//30000
					case 60:
					case 111:
						$spmemo.='101 張';
						$spmemoapp[0]['num'] = '101 張';
					break;
					//50000
					case 176:
					case 182:
						$spmemo.='167 張';
						$spmemoapp[0]['num'] = '167 張';
					break;
					//100000
					case 179:
					case 185:
						$spmemo.='334 張';
						$spmemoapp[0]['num'] = '334 張';
					break;
				}
			}

			if($drid== 4){
				$banklist=array();
				$banklist['BOCOM']="交通銀行";
				$banklist['CMB'] = "招商銀行";
				$banklist['CCB'] = "建設銀行";
				$banklist['SPDB'] = "浦發銀行";
				$banklist['GDB'] = "廣發銀行";
				$banklist['PSDB'] = "郵政儲蓄銀行";
				$banklist['CIB'] = "興業銀行";
				$banklist['HXB'] = "華夏銀行";
				$banklist['PAB'] = "平安銀行";
				$banklist['BOS'] = "上海銀行";
				$banklist['SRCB'] = "上海農商銀行";
				$banklist['BCCB'] = "北京銀行";
				$banklist['BRCB'] = "北京農商銀行";
				$banklist['CEB'] = "光大銀行";
				$banklist['ICBC'] = "工商銀行(建議使用PC流覽)";
				$banklist['BOCSH'] = "中國銀行(建議使用PC流覽)";
				$banklist['ABC'] = "農業銀行(建議使用PC流覽)";
				$banklist['MBC'] = "民生銀行(建議使用PC流覽)";
				$banklist['CNCB'] = "中信銀行(建議使用PC流覽)";
				$banklist['OTHERS'] = "其它" ;

				$tpl->assign('bank_list',$banklist);
			}

			//Create ordernumber
			$get_deposit['ordernumber'] = $dhid;
			
			//20200121 AARONFU
			$get_deposit['api_url'] = $deposit_rule[0]['notify_url'];

			// 如果是其他支付,需要抓其他支付的虛擬帳號
			if($drid=='10'){
				$arrSSO = $user->get_user_sso_list($userid);
				if($arrSSO && $arrSSO[0]){
					$tpl->assign('vendor_pay', $arrSSO);
				}
			}

		}

		$chkStr=$dhid."|".$get_deposit['amount'];
		$cs = new convertString();
		$enc_chkStr=$cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);

		if ($app=="Y") {
			$tpl->assign('chkStr',$enc_chkStr);
			$tpl->assign('get_deposit', $get_deposit);
			$tpl->assign('get_scode_promote', $get_scode_promote);
			$tpl->assign('spmemo', $spmemo);
			$tpl->assign('spmemototal', $spmemototal);
			$tpl->assign('times', $deposit_times);
			$tpl->assign('header','N');
			$tpl->set_title('');
			$tpl->render("deposit","confirm",true,false,$app);
		}else if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_deposit;
			$ret['retObj']['deposit_rule'] = $deposit_rule;
			$ret['retObj']['spmemo'] = $spmemoapp;
			$ret['retObj']['spmemototal'] = $spmemototal;
			$ret['retObj']['vendor_pay'] = $arrSSO;
			$ret['retObj']['bank_list'] = $banklist;
			$ret['retObj']['chkStr'] = $enc_chkStr;
			$ret['retObj']['htmlurl'] = $config[$deposit_rule[0]['act']]['htmlurl'];
			$ret['retObj']['times'] = empty($deposit_times) ? null : $deposit_times;
			echo json_encode($ret);
			exit;
		}else if($json!='Y'){
			$tpl->assign('chkStr',$enc_chkStr);
			$tpl->assign('get_deposit', $get_deposit);
			$tpl->assign('get_scode_promote', $get_scode_promote);
			$tpl->assign('spmemo', $spmemo);
			$tpl->assign('spmemototal', $spmemototal);
			$tpl->assign('times', $deposit_times);
			$tpl->set_title('');

			$tpl->render("deposit","confirm",true);
		}

	}


	public function alipay_html() {

		global $tpl, $config, $deposit;

		$secretid = md5($_POST['out_trade_no'].$_POST['trade_no']);

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		//充值項目ID
		if(($_POST['result'] == "success") && ($_POST['secretid'] == $secretid)){

			$alipay_array['out_trade_no'] = $_POST['out_trade_no'];  // deposit_history id
			$alipay_array['trade_no'] = $_POST['trade_no'];
			$alipay_array['result'] = $_POST['result'];

			$get_deposit_history = $deposit->get_deposit_history($alipay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);
			error_log("alipay:".date("H:i:s"));

			if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){

				$userid=$get_deposit_history[0]['userid'];

				$deposit->set_deposit_history($alipay_array, $get_deposit_history[0], "alipay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);

				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);

				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				if(!empty($get_scode_promote)){
					foreach($get_scode_promote  as $sk => $sv){
						if($sv['productid'] == 0){
							$scode = $deposit->add_scode($userid, $sv);
							$deposit->add_scode_history($userid, $scode, $sv);
						}
						// 殺價券
						if(!empty($sv['productid']) && $sv['productid'] > 0){
							if($this->is_house_promote_effective($sv['productid'])){

								// 判定是否符合萬人殺房資格, 符合者送殺價券
								$this->register_house_promote($sv['productid'],$userid);

								for($i = 0; $i < $sv['num']; $i++){
								   $deposit->add_oscode($userid, $sv);
								}

								// 推薦者送殺價券
								$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
								error_log("[c/deposit/alipay_html] sv : ".$sv['num']);
								// 不是房子(2854), 就直接送殺價券
								for($i = 0; $i < $sv['num']; $i++){
									$deposit->add_oscode($userid, $sv);
								}
							}
						}
					}
				}

				//首次儲值送推薦人2塊
				$this->add_deposit_to_user_src($userid);
				error_log("支付寶交易成功。");
				echo '<!DOCTYPE html><html><body><script>alert("充值成功!");window.location = "/site/member/";</script></body></html>';

			}else if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y'){
				error_log("支付寶交易 - 已充值成功。");
				echo '<!DOCTYPE html><html><body><script>alert("充值成功!");window.location = "/site/member/";</script></body></html>';
			}else{
				error_log("支付寶充值失敗。");
				echo '<!DOCTYPE html><html><body><script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script></body></html>';
			}

		}else{
			error_log("充值失敗。");
			echo '<!DOCTYPE html><html><body><script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script></body></html>';
		}
		exit;

	}


	// 背景通知 form bankcomm
	public function bankcomm_active_notify() {

		global $tpl, $config, $deposit;

		set_status($this->controller);
		$notifyMsg = $_POST['notifyMsg'];
		$arr = preg_split("/\|{1,}/",$notifyMsg);
		$msg="";

		if(is_array($arr)){

			$size=count($arr);

			for($i=0;$i<$size-1;$i++){
				if($i==$size-2){
				  $msg.=base64_decode($arr[$i]);
				}else{
				$msg.=$arr[$i]."|";
				}
			}

			$merId=$arr[0];
			$orderid=$arr[1];
			$amount=$arr[2];
			$curType=$arr[3];          // ex: CNY
			$platformBatchNo=$arr[4];  // Usually leave blank
			$merPayBatchNo=$arr[5];    // Optional, assign by merchant
			$orderDate=$arr[6];        // ex: 20141015
			$orderTime=$arr[7];        // ex: 095124
			$bankSerialNo=$arr[8];     // ex: C7D367EC
			$tranResultCode=$arr[9];   // ex: 1 [success]
			$feeSum=$arr[10];             // Total fee
			$cardType=$arr[11];
			$bankMono=$arr[12];
			$errMsg=$arr[13];
			$remoteAddr=$arr[14];
			$refererAddr=$arr[15];
			$merComment=base64_decode($arr[16]);

		}

		error_log("bankcomm_active_notify from bankcomm : ".$msg);
		error_log("transResultCode : ".$tranResultCode);

		if($tranResultCode=='1'){
			$bankcomm_array['out_trade_no'] = $orderid;
			$bankcomm_array['trade_no'] =$bankSerialNo;
			$bankcomm_array['amount'] = $amount;
			$bankcomm_array['result'] = $tranResultCode;
			$bankcomm_array['orderDate'] = $orderDate;
			$bankcomm_array['orderTime'] = $orderTime;
			$bankcomm_array['orderMono'] = $merComment;

			$get_deposit_history = $deposit->get_deposit_history($orderid);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

				if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'N'){

					$userid = $get_deposit_history[0]['userid'];

					$deposit->set_deposit_history($bankcomm_array, $get_deposit_history[0], "bankcomm");
					$deposit->set_deposit($get_deposit_history[0]['depositid']);
					$deposit->set_spoint($get_deposit_history[0]['spointid']);
					$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
					$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

					$arr_cond=array();
					$arr_update=array();
					$arr_cond['dhid']=$orderid;
					$arr_update['modifiertype']="system";
					$arr_update['modifiername']="bankcomm_active_notify";
					$deposit->update_deposit_history($arr_cond,$arr_update);

					if(!empty($get_scode_promote)){
						foreach ($get_scode_promote  as $sk => $sv){
							if ($sv['productid'] == 0){
								$scode = $deposit->add_scode($userid, $sv);
								$deposit->add_scode_history($userid, $scode, $sv);
							}

							if ($sv['productid'] > 0 && !empty($sv['productid'])){
								if($this->is_house_promote_effective($sv['productid'])){
									// 判定是否符合萬人殺房資格, 符合者送殺價券
									$this->register_house_promote($sv['productid'],$userid);
									for ($i = 0; $i < $sv['num']; $i++){
										$deposit->add_oscode($userid, $sv);
									}
									// 推薦者送殺價券
									$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
								}else{
									// 不是房子(2854), 就直接送殺價券
									for($i = 0; $i < $sv['num']; $i++){
										$deposit->add_oscode($userid, $sv);
									}
								}
							}
						}
					}

				}else if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'Y'){
				   error_log("[deposit] Order id : ".$orderid." has been deposited OK , nothing to do !!");
				}

		}else{
		   error_log("[deposit] Order id : ".$orderid." is failed , please check with bankcomm !!");
		}
		echo '1';
		exit;

	}


	// 同步通知 form bankcomm
	public function bankcomm_html(){

		global $tpl, $config, $deposit;

		set_status($this->controller);

		//判斷來源是否為APP,若是則導向銀聯APP的專屬function
		if ($_POST['json'] == 'Y'){
			$this->bankcomm_app();
			exit;
		}

		$notifyMsg = $_POST['notifyMsg'];
		error_log("bankcomm_html from bankcomm : ".$notifyMsg);
		$arr = preg_split("/\|{1,}/",$notifyMsg);

		if(is_array($arr)){
		   $merId=$arr[0];
		   $orderid=$arr[1];
		   $amount=$arr[2];
		   $curType=$arr[3];          // ex: CNY
		   $platformBatchNo=$arr[4];  // Usually leave blank
		   $merPayBatchNo=$arr[5];    // Optional, assign by merchant
		   $orderDate=$arr[6];        // ex: 20141015
		   $orderTime=$arr[7];        // ex: 095124
		   $bankSerialNo=$arr[8];     // ex: C7D367EC
		   $tranResultCode=$arr[9];   // ex: 1 [success]
		   $feeSum=$arr[10];          // Total fee
		   $cardType=$arr[11];
		   $bankMono=$arr[12];
		   $errMsg=$arr[13];
		   $remoteAddr=$arr[14];
		   $refererAddr=$arr[15];
		   $merComment=base64_decode($arr[16]);
		   $signMsg=$arr[17];
		}
		error_log("merComment is : ".$merComment);
		error_log("transResultCode : ".$tranResultCode);

		if($tranResultCode=='1'){
			$bankcomm_array['out_trade_no'] = $orderid;
			$bankcomm_array['trade_no'] =$bankSerialNo;
			$bankcomm_array['amount'] = $amount;
			$bankcomm_array['result'] = $tranResultCode;
			$bankcomm_array['orderDate'] = $orderDate;
			$bankcomm_array['orderTime'] = $orderTime;
			$bankcomm_array['orderMono'] = $merComment;

			$get_deposit_history = $deposit->get_deposit_history($bankcomm_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

			if (!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'N'){

				$userid=$get_deposit_history[0]['userid'];

				$deposit->set_deposit_history($bankcomm_array, $get_deposit_history[0], "bankcomm");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);
				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				if(!empty($get_scode_promote)){
					foreach($get_scode_promote  as $sk => $sv){
						if ($sv['productid'] == 0) {
							$scode = $deposit->add_scode($userid, $sv);
							$deposit->add_scode_history($userid, $scode, $sv);
						}
						if ($sv['productid'] > 0 && !empty($sv['productid'])){
							if($this->is_house_promote_effective($sv['productid'])){
								// 判定是否符合萬人殺房資格, 符合者送殺價券
								$this->register_house_promote($sv['productid'],$userid);
								for ($i = 0; $i < $sv['num']; $i++) {
								   $deposit->add_oscode($userid, $sv);
								}
								// 推薦者送殺價券
								$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
								// 不是房子(2854), 就直接送殺價券
								for ($i = 0; $i < $sv['num']; $i++) {
								   $deposit->add_oscode($userid, $sv);
								}
							}
						}
					}
				}

				error_log("銀聯交易成功。");
				echo '<!DOCTYPE html><html><body><script>alert("充值成功!");window.location = "/site/product/";</script></body></html>';

			}else if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'Y'){
				error_log("[deposit] Order id : ".$orderid." has been deposited OK , nothing to do !!");
			}

		}else{
		  error_log("銀聯交易失敗。");
			echo '<!DOCTYPE html><html><body><script>alert("充值失敗!");window.location = "/site/member?channelid=1"</script></body></html>';
		}

		exit;

	}


	public function weixinpay_html(){

		global $tpl, $config, $deposit;

		error_log("[c/deposit/weixinpay_html] POST : ".json_encode($_POST));
		error_log("[c/deposit/weixinpay_html] GET : ".json_encode($_GET));
		error_log("[c/deposit/weixinpay_html] orderid : ".$_POST['orderid'].", amount : ".$_POST['amount'].", secretid : ".$_POST['secretid'].", productid : ".$_POST['productid']);

		//設定 Action 相關參數
		set_status($this->controller);

		//判斷來源是否為APP,若是則導向微信APP的專屬function
		if ($_POST['json'] == 'Y'){
			$this->weixinpay_app();
			exit;
		}

		//充值項目ID
		if($_POST['orderid'] && ($_POST['secretid'] == $secretid)){

			$weixinpay_array['out_trade_no'] = $_POST['orderid'];
			$weixinpay_array['amount'] = $_POST['amount'];

			$get_deposit_history = $deposit->get_deposit_history($weixinpay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

			if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){
			  // 交易金額比對
				error_log("[weixinpay_html] : ".floatval($_POST['amount'])."<==>".floatval($get_deposit_id[0]['amount']));
				if(floatval($_POST['amount'])!=floatval($get_deposit_id[0]['amount'])){
					$ret['status'] = 3;
					error_log("[weixinpay_html] 微信支付事務數據異常。");
					echo $ret['status'];
					exit;
				}
				$deposit->set_deposit_history($weixinpay_array, $get_deposit_history[0], "weixinpay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);

				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);
				error_log("[c/deposit/weixinpay_html] get_scode_promote : ".json_encode($get_scode_promote));

				$userid=$get_deposit_history[0]['userid'];

				if(!empty($get_scode_promote)){
					foreach($get_scode_promote  as $sk => $sv){
						if ($sv['productid'] > 0 && !empty($sv['productid'])){
							if($this->is_house_promote_effective($sv['productid'])){
								// 判定是否符合萬人殺房資格, 符合者送殺價券
								$this->register_house_promote($sv['productid'],$userid);
								for ($i = 0; $i < $sv['num']; $i++) {
								   $deposit->add_oscode($userid, $sv);
								}
							  // 推薦者送殺價券
							  $this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
								// 不是房子(2854), 就直接送殺價券
								for ($i = 0; $i < $sv['num']; $i++) {
									$deposit->add_oscode($userid, $sv);
								}
								error_log("[c/deposit/weixinpay_html] sv : ".$sv['num']);
							}
						}
					}
				}

				//首次儲值送推薦人2塊
				$this->add_deposit_to_user_src($userid);

				//回傳:
				$ret['status'] = 200;
				error_log("[weixinpay_html]微信支付交易-成功。");

			}else if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y'){
				$ret['status'] = 2;
				error_log("[weixinpay_html]微信支付重複充值。");
			}else{
				$ret['status'] = 1;
				error_log("[weixinpay_html]微信支付充值失敗1。");
			}

		}else{
			$ret['status'] = 1;
			error_log("[weixinpay_html]微信支付充值失敗2。");
		}

		echo $ret['status'];
		exit;

	}


	public function alipay_complete() {

		global $tpl, $config, $deposit;

		$tpl->set_title('');
		$tpl->render("deposit","alipay_complete",true);

	}


	public function ibonpay() {

		global $tpl, $config, $deposit;

		set_status($this->controller);
		login_required();

		$user_info=$_SESSION['user'];

		if(empty($payname)){
		  $payname=$user_info['profile']['nickname'];
		}
		$success = 0;

		$pay_info = Array(
			"transurl" => $config['ibonpay']['url_payment'],
			"code" => $config['ibonpay']['code'],
			"ordernumber" => $_POST['ordernumber'],
			"merchantnumber" => $config['ibonpay']['merchantnumber'],
			"paymenttype" => $config['ibonpay']['paymenttype'],
			"amount" => $_POST['amount'],
			"duedate" => "",
			"payname" => $user_info['profile']['addressee'],
			"payphone" => $user_info['profile']['phone'],
			"nexturl" => $config['ibonpay']['nexturl'],
			"returnvalue" => "1",
			"reprint" => "1",
			"bankid" => "",
			"paytitle" => "",
			"paymemo" => ""
		);

		$plaintext = $pay_info['merchantnumber'].
		$pay_info['code'].
		$pay_info['amount'].
		$pay_info['ordernumber'];
		error_log("[ibonpay] ibon plaintext : ".$plaintext);

		$hash = md5($plaintext);
		error_log("[ibonpay] Hash is :".$hash);


		$postdata = "merchantnumber=".$pay_info['merchantnumber'].
				"&ordernumber=".$pay_info['ordernumber'].
				"&amount=".$pay_info['amount'].
				"&paymenttype=".$pay_info['paymenttype'].
				"&bankid=".$pay_info['bankid'].
				"&paytitle=".urlencode($pay_info['paytitle']).
				"&paymemo=".urlencode($pay_info['paymemo']).
				"&payname=".urlencode($pay_info['payname']).
				"&payphone=".$pay_info['payphone'].
				"&duedate=".$pay_info['duedate'].
				"&returnvalue=".$pay_info['returnvalue'].
				"&hash=".$hash.
				"&nexturl=".urlencode($pay_info['nexturl']);

		$url = parse_url($pay_info['transurl']);

		$postdata = "POST ".$url['path']." HTTP/1.0\r\n".
					"Content-Type: application/x-www-form-urlencoded\r\n".
					"Host: ".$url['host']."\r\n".
					"Content-Length: ".strlen($postdata)."\r\n".
					"\r\n".
					$postdata;

		$receivedata = "";
		$fp = fsockopen ($url['host'], $url['port'], $errno, $errstr, 90);

		if(!$fp){
			echo "$errstr ($errno)<br>\n";
		}else{
			fputs ($fp, $postdata);
			do{
				if(feof($fp)) break;
				$tmpstr = fgets($fp,128);
				$receivedata = $receivedata.$tmpstr;
			}while(true);
			fclose ($fp);
		}

		$receivedata = str_replace("\r","",trim($receivedata));
		$isbody = false;
		$httpcode = null;
		$httpmessage = null;
		$result = "";
		$array1 = explode("\n",$receivedata);
		for($i=0;$i<count($array1);$i++){
			if($i==0){
			   $array2 = explode(" ",$array1[$i]);
			   $httpcode = $array2[1];
			   $httpmessage = $array2[2];
			}else if(!$isbody){
			   if(strlen($array1[$i])==0) $isbody = true;
			}else{
			   $result = $result.$array1[$i];
			}
		}

		if($httpcode!="200"){
		if($httpcode=="404")
		  error_log ("URL Not Found !!");
		else if($httpcode=="500")
		  error_log("Destination Server Error !!");
		else
		  error_log($httpmessage);
		return;
		}

		$pay_result= Array();
		//-- 分割資料取參數
		$ary1 = explode("&",$result);
		for($i=0;$i<count($ary1);$i++){
		  $ary2 = explode("=",$ary1[$i]);
		  $pay_result[$ary2[0]]=$ary2[1];
		}
		// $ary2 looks like :
		// Array ( [0] => rc [1] => 0 )
		// Array ( [0] => amount [1] => 2551 )
		// Array ( [0] => merchantnumber [1] => 456025 )
		// Array ( [0] => ordernumber [1] => 2228 )
		// Array ( [0] => paycode [1] => NW14103100190276 )
		// Array ( [0] => checksum [1] => 79c43472e3d3fd1c0fb509263ef7411c

		// 將訂單號,paycode和checksum 等等暫時寫入saja_deposit_history
		$arr_wherecond['dhid']=$pay_info['ordernumber'];
		$arr_update['data']=json_encode($pay_result);
		error_log($arr_update['data']);
		$deposit->update_deposit_history($arr_wherecond,$arr_update);

		$tpl->assign('pay_result',$pay_result);
		$tpl->assign('pay_info',$pay_info);
		$tpl->set_title('');
		$tpl->render("deposit","ibonpay_result",true);

	}


	public function ibonpay_success() {

		global $tpl, $config, $deposit;
		set_status($this->controller);

		error_log("[ibonpay_success] Referer :".$_SERVER['HTTP_REFERER']);
		if(md5("www.saja.com.tw")!=$_SERVER['HTTP_REFERER']) {
			echo("-99=NOT FROM SAJA");
			error_log("Request is not from www.saja.com.tw, Rejected !!");
			exit;
		}

		// 資料確認
		$orderid=$_POST['ordernumber'];
		error_log("[ibonpay_success] Checking orderid: ".$orderid);
		$get_deposit_history = $deposit->get_deposit_history($orderid);

		if(empty($get_deposit_history)){
			error_log("Orderid: ".$orderid." data not found !!");
			echo "-1=DATA NOT FOUND";
			exit;
		}

		if(empty($get_deposit_history[0]['dhid'])) {
		  error_log("Orderid: ".$orderid." error !!");
			echo "-2=ERROR ORDER ID";
			exit;
		}


		$spointid=$get_deposit_history[0]['spointid'];

		$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

		if(!empty($get_deposit_id[0]['amount'])){
			error_log("The Amount of deposit id: ".$get_deposit_history[0]['depositid']." is: ".$get_deposit_id[0]['amount']);
			if($get_deposit_id[0]['amount']!=$_POST['amount']){
				error_log("Not identical to request: ".$_POST['amount']." !!");
				echo "-3=AMOUNT NOT IDENTICAL";
				exit;
			}
		}

		$amount=$get_deposit_id[0]['amount'];

		foreach($_POST as $key=>$value){
			$ibonpay_array[$key]=$value;
		}
		/*
		$ibonpay_array['merchantnumber']     = $_POST['merchantnumber']; // merchantnumber => 456025
		$ibonpay_array['out_trade_no']       = $orderid;  				 // ordernumber => 2469
		$ibonpay_array['amount']             = $amount;   				 // amount => 35
		$ibonpay_array['paymenttype']        = $_POST['paymenttype'];    // paymenttype => IBON, FAMIPORT ...
		$ibonpay_array['serialnumber']       = $_POST['serialnumber'];  //  serialnumber => 10463556
		$ibonpay_array['writeoffnumber']     = $_POST['writeoffnumber'];  // writeoffnumber => 041110FU994161(User收據上的序號)
		$ibonpay_array['timepaid']           = $_POST['timepaid'];
		$ibonpay_array['tel']                = $_POST['tel'];
		*/
		if($get_deposit_id[0]['switch']=='N'){
			$deposit->set_deposit_history($ibonpay_array, $get_deposit_history[0], "ibonpay");
			$deposit->set_deposit($get_deposit_history[0]['depositid']);
			$deposit->set_spoint($get_deposit_history[0]['spointid']);
			$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
			$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

			if (!empty($get_scode_promote)){
				$userid=$get_deposit_history[0]['userid'];

				foreach ($get_scode_promote  as $sk => $sv){
					if($sv['productid'] == 0){
						$scode = $deposit->add_scode($userid, $sv);
						$deposit->add_scode_history($userid, $scode, $sv);
					}
					if($sv['productid'] > 0 && !empty($sv['productid'])){
						if($this->is_house_promote_effective($sv['productid'])){
							// 判定是否符合萬人殺房資格, 符合者送殺價券
							$this->register_house_promote($sv['productid'],$userid);
							for($i = 0; $i < $sv['num']; $i++){
								$deposit->add_oscode($userid, $sv);
							}
							// 推薦者送殺價券
							$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
						}else{
							//不是房子(2854), 就直接送殺價券
							for ($i = 0; $i < $sv['num']; $i++) {
								$deposit->add_oscode($userid, $sv);
							}
						}
					}
				}
			}

			error_log("[ibonpay_success] ibon success notify : ".json_encode($ibonpay_array));
			echo "1=OK";

		}else if($get_deposit_id[0]['switch'] == 'Y'){
			error_log("Order id: ".$orderid." had been enabled , cannot be enable again !!");
			echo "-4=DUPLICATED DEPOSIT";
		}else{
			error_log("Order id: ".$orderid." status is unknown !!");
			echo "-5=UNKNOW DEPOSIT STATUS";
		}
		exit;

	}


	public function alading_exc() {

		global $tpl, $config, $deposit;

		set_status($this->controller);
		login_required();

		$this->hinetpts_exc();

	}


	public function hinetpts_exc() {

		global $tpl, $config, $deposit;
		set_status($this->controller);
		login_required();

		$drid=$_POST['drid'];
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		if($drid=='7'){
			$ptype=$config['hinet']['paymenttype'];
			$curr_type='NTD';
		}else if($drid=='9'){
			$ptype=$config['alading']['paymenttype'];
			$curr_type='RMB';
		}

		$serial_no=addslashes($_POST['serial_no']);
		if(empty($serial_no)){
			echo '<script>alert("序號不可為空白, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			exit;
		}
		if($serial_no!=$_POST['serial_no']){
			echo '<script>alert("序號格式錯誤, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			exit;
		}


		$passwd=addslashes($_POST['passw']);
		if(empty($passwd)){
			echo '<script>alert("密碼不可為空白, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			exit;
		}
		if($passwd!=$_POST['passw']){
			echo '<script>alert("密碼格式錯誤, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			exit;
		}

		$ch_serial=$deposit->get_ch_serial($serial_no);

		if(!$ch_serial){
			echo '<script>alert("序號錯誤, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			exit;
		}

		if($passwd!=$ch_serial['passw']){
			$arr_update=array();
			$arr_update['userid']=$userid;
			$arr_update['err_num']=$ch_serial['err_num']+1;
			if($arr_update['err_num']<3){
				echo '<script>alert("密碼錯誤 : '.$arr_update['err_num'].' 次, 請重新輸入 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			}else{
				$arr_update['switch']='L';
				echo '<script>alert("密碼輸入錯誤 3 次,序號已鎖定暫停兌換,請洽客服人員 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			}
			$deposit->update_ch_serial($serial_no, $arr_update);
			exit;
		}

		if($ch_serial['switch']=='L'){
			echo '<script>alert("本序號已暫停兌換,請洽客服人員 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			exit;
		}
		if($ch_serial['switch']=='Y'){
			echo '<script>alert("本序號已使用過, 無法再兌換殺幣 !!");window.location = "/site/deposit/confirm/?drid='.$drid.'";</script>';
			exit;
		}
		if($passwd==$ch_serial['passw']){

			$arr_update=array();
			$spoint=$ch_serial['point'];
			error_log("[c/hinetpts_exc] drid=${drid}, spoint=${spoint} ...");

			$deposit_rule = $deposit->deposit_rule_test($drid);
			if($deposit_rule){
				$deposit_rule_item=$deposit->get_deposit_rule_item_by_drid_spoint($drid,$spoint);
				if($deposit_rule_item){
					$driid=$deposit_rule_item['driid'];
					error_log("[c/hinetpst_exc] driid=${driid} ...");

					// 產生訂單及儲值資料
					$depositid = $deposit->add_deposit($userid, $deposit_rule_item['amount'],$curr_type);
					$spointid = $deposit->add_spoint($userid,  $deposit_rule_item['spoint']);
					$dhid = $deposit->add_deposit_history($userid, $driid, $depositid, $spointid);

					error_log("[hinetpst_exc] dhid=${dhid}, spointid=${spointid}, depositid=${depositid} ...");

					// 修改訂單及儲值狀態
					$get_deposit_history = $deposit->get_deposit_history($dhid);
					$get_deposit_id = $deposit->get_deposit_id($depositid);

					error_log("[c/hinetpts_exc] saja_deposit switch : ".$get_deposit_id[0]['switch']);

					if ($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){
						$hinet_array['out_trade_no'] = $get_deposit_history[0]['dhid'];
						$hinet_array['spoints'] = $spoint;
						$hinet_array['userid']=$userid;
						$hinet_array['serialno']=$serial_no;

						$deposit->set_deposit_history($hinet_array, $get_deposit_history[0], $ptype);

						$deposit->set_deposit($get_deposit_history[0]['depositid']);

						$deposit->set_spoint($get_deposit_history[0]['spointid']);

						$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
						$get_scode_promote = $deposit->get_scode_promote_rt_test($get_deposit_rule_item[0]['driid']);

						if(!empty($get_scode_promote)){
							foreach($get_scode_promote  as $sk => $sv){
								if($sv['productid'] == 0){
									$scode = $deposit->add_scode($userid, $sv);
									$deposit->add_scode_history($userid, $scode, $sv);
								}
								if($sv['productid'] > 0 && !empty($sv['productid'])){
									if($this->is_house_promote_effective($sv['productid'])){
										// 判定是否符合萬人殺房資格, 符合者送殺價券
										$this->register_house_promote($sv['productid'],$userid);
										for($i = 0; $i < $sv['num']; $i++){
											$deposit->add_oscode($userid, $sv);
										}
										// 推薦者送殺價券
										$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
									}else{
										// 不是房子(2854), 就直接送殺價券
										for ($i = 0; $i < $sv['num']; $i++) {
										   $deposit->add_oscode($userid, $sv);
										}
									}
								}
							}
						}
						$arr_update=array();
						$arr_update['userid']=$userid;
						$arr_update['switch']='Y';
						$arr_update['dhid']=$dhid;
						$arr_update['memo']=$ptype;
						$arr_update['modifyerid']=$userid;
						$deposit->update_ch_serial($serial_no,$arr_update);
						echo '<script>alert("兌換成功, 已新增殺幣 '.$spoint.' 點 !!");window.location = "/site/member?channelid=1"</script>';
					}
				}
			}
		}
		exit;

	}


	/*
	 *	信用卡支付送出
	 *	$userid					int						會員編號
	 *	$json					varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$drid					int						儲值方式編號
	 *	$driid					int						儲值方案編號
	 *	$amount					int						充值金額
	 *	$ordernumber			varchar					訂單編號
	 *	$spoint					int						充值點數
	 *	$chkStr					varchar					加密簽名
	 *	$paymentTokenuse		varchar					快速結帳使用
	 *	$ucid					int						信用卡流水編號
	 */
	public function twcreditcard_pay() {

		global $tpl, $config, $deposit, $user;

		set_status($this->controller);
		// login_required();
		$auth_id = empty($_GET['auth_id'])?htmlspecialchars($_POST['auth_id']):htmlspecialchars($_GET['auth_id']);
		$userid = empty($_SESSION['auth_id'])?$auth_id:$_SESSION['auth_id'];
		$json = empty($_GET['json'])?htmlspecialchars($_POST['json']):htmlspecialchars($_GET['json']);
		$drid = empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
		$driid = empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
		$amount = empty($_GET['amount'])?htmlspecialchars($_POST['amount']):htmlspecialchars($_GET['amount']);
		$spoint = empty($_GET['spoint'])?htmlspecialchars($_POST['spoint']):htmlspecialchars($_GET['spoint']);
		$chkStr = empty($_GET['chkStr'])?htmlspecialchars($_POST['chkStr']):htmlspecialchars($_GET['chkStr']);
		$ordernumber = empty($_GET['ordernumber'])?htmlspecialchars($_POST['ordernumber']):htmlspecialchars($_GET['ordernumber']);
		$paymentTokenuse = empty($_GET['paymentTokenuse'])?htmlspecialchars($_POST['paymentTokenuse']):htmlspecialchars($_GET['paymentTokenuse']);
		$runAt = empty($_GET['runAt'])?htmlspecialchars($_POST['runAt']):htmlspecialchars($_GET['runAt']);
		$ucid = empty($_GET['ucid'])?htmlspecialchars($_POST['ucid']):htmlspecialchars($_GET['ucid']);

		if ($runAt == 'APP'){
			$chkStr=$ordernumber."|".$amount;
			$cs = new convertString();
			$chkStr=$cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
		}

		error_log("[c/deposit/twcreditcard_pay] userid : ".$userid);
		// 檢查交易金額是否正確
		if(floatval($amount)<0){
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10001,'交易金額錯誤','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("交易金額錯誤!!");history.back();</script>');
				exit;
			}
		}

		// 檢查是否正確
		if(empty($chkStr)){
			if($json == 'Y'){
				$ret = getRetJSONArray(-10002,'加密簽名資料錯誤','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("加密簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}

		$pay_info=array();
		$pay_info['web']=$config['creditcard']['merchantnumber'];
		$pay_info['MN']= floatval($amount);
		$pay_info['Td']=$ordernumber;
		$pay_info['sna']=urlencode($_SESSION['user']['profile']['nickname']."(".$userid.")");
		$pay_info['sdt']=$_SESSION['user']['profile']['phone'];

		if(!is_numeric($pay_info['sdt'])){
			$pay_info['sdt']='';
		}

		$pay_info['email']=empty($_SESSION['user']['email'])? "" : $_SESSION['user']['email'];
		$pay_info['note1']="{userid:".$userid.",OrderId:".$ordernumber.",Amount:".floatval($amount)."}";
		$pay_info['note2']="saja_tw";
		$pay_info['name']=$_SESSION['user']['profile']['nickname'];

		$pay_info['OrderInfo']=urlencode("OrderId:".$pay_info['Td'].
												",Name:".$_SESSION['user']['profile']['nickname'].
												",Userid:".$userid.
												",Amount:".$pay_info['MN'].
												",Spts:".$spoint);

		$pay_info['Card_Type']=$config['creditcard']['Card_Type'];
		$chkvalue_ori=$pay_info['web'].$config['creditcard']['code'].$pay_info['MN'];
		$pay_info['ChkValue']=strtoupper(sha1($chkvalue_ori));
		$pay_info['web_app_android'] = $config['creditcard']['merchantnumber_app'];	//app android商家編號
		$pay_info['web_app_ios'] = $config['creditcard']['merchantnumber_app'];	//app ios商家編號
		$pay_info['TransPwd'] = $config['creditcard']['code'];	//交易密碼
		$pay_info['PayType'] = "credit";	//android付款方式 credit: 信用卡 MIT_AppPayment: 感應收款 paycode: 超商代碼繳費 barcode: 超商條碼繳費 atm_account: ATM轉帳繳款
		$pay_info['RSTransactionPayType'] = "RSTransactionPayTypeCredit";	//ios付款方式 RSTransactionPayTypeCredit：信用卡收款 RSTransactionPayTypeMIT：感應收款 RSTransactionPayTypePaycode：超商代碼收款 RSTransactionPayTypeBarcode：超商條碼收款 RSTransactionPayTypeATMAccount：ATM轉帳收款

		$pay_info['Term'] = "";	//web,android分期期數
		$pay_info['RSTransactionInstallmentTermValue'] = $config['creditcard']['RSTransactionInstallmentTermValue'];//ios分期期數 RSTransactionInstallmentTermNone：不分期 RSTransactionInstallmentTerm3：3期 RSTransactionInstallmentTerm6：6期 RSTransactionInstallmentTerm12：12期 RSTransactionInstallmentTerm18：18期 RSTransactionInstallmentTerm24：24期

		$pay_info['isproduction_android'] = 'true';	//android是否為正式環境（true為正式環境，false為測試環境)
		$pay_info['isproduction_ios'] = 'YES';	//ios是否為正式環境（YES為正式環境，NO為測試環境)
		$pay_info['needSign_android'] = 'false';	//android是否需要簽名面版(true為需要,false為不需要)
		$pay_info['needSign_ios'] = 'NO';	//ios是否需要簽名面版（YES為需要，NO為不需要）
		$pay_info['timeoutSec_android'] = 500;	//android限制信用卡輸入畫面的輸入時間（秒）
		$pay_info['timeoutSec_ios'] = 500;	//ios限制信用卡輸入畫面的輸入時間（秒）
		$pay_info['orderNoChk_android'] = 'true';	//android是否加強訂單編號重複檢查（true為一律檢查，false為第二次送出交易時檢查）
		$pay_info['isCheckOrderNo_ios'] = 'YES';	//ios檢查自訂訂單編號是否重複(YES為檢查，NO為不檢查)
		$pay_info['Language_android'] = 1;	//android交易畫面的語系設定 0：手機預設，1：繁體中文，2：英文，3：簡體中文
		$pay_info['RSTransactionLanguage_ios'] = 'RSTransactionLanguageZhHant';	//ios交易畫面的語系設定，(若沒有進行設定則會自動選擇繁體中文)：RSTransactionLanguageAuto：自動切換。 RSTransactionLanguageEn：英文 RSTransactionLanguageZhHant：繁體中 RSTransactionLanguageZhHans： 簡體中文

		// for測試環境
		if($_SESSION['auth_id']=='1705'){
		  $pay_info['isproduction_ios'] = 'NO';
		  $pay_info['isproduction_android'] = 'false';
		}
		if($pay_info['isproduction_ios']=='NO'){
		  $pay_info['web_app_ios'] = $config['creditcard']['merchantnumber_app'];
		}
		if($pay_info['isproduction_android']=='false'){
		  $pay_info['web_app_android'] = $config['creditcard']['merchantnumber_app'];
		}

		$cs=new convertString();
		$chkStr=$cs->strDecode($chkStr,$config["encode_key"],$config["encode_type"]);
		error_log("[c/deposit/twcreditcard_pay] chkStr : ".$chkStr);
		$chkArr=explode("|",$chkStr);

		if(is_array($chkArr)){

			$chk_orderid=$chkArr[0];
			$chk_amount=$chkArr[1];

			if(floatval($chk_amount)!=floatval($amount)){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10003,'交易金額不符','MSG');
					echo json_encode($ret);
					exit;
				}else{
					die('<script>alert("交易金額不符 !!");history.back();</script>');
					exit;
				}
			}

			if($chk_orderid!=$ordernumber){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10004,'訂單編號不符!!','MSG');
					echo json_encode($ret);
					exit;
				}else{
					die('<script>alert("訂單編號不符 !!");history.back();</script>');
					exit;
				}
			}

		}else{
			if($json == 'Y'){
				$ret = getRetJSONArray(-10005,'簽名資料錯誤 !!','MSG');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}

		$get_deposit_history=$deposit->get_deposit_history($pay_info['Td']);

		if(!empty($get_deposit_history[0]['dhid'])){

			$arr_cond=array();
			$arr_cond['dhid']=$pay_info['Td'];
			$arr_data=array();
			$arr_data['out_trade_no']=$pay_info['Td'];
			$arr_data['userid']=$userid;
			$arr_data['amount']= intval($pay_info['MN']);
			$arr_data['timepaid']=date('YmdHis');
			$arr_data['phone']=$pay_info['sdt'];
			$arr_data['paymenttype']=$config['creditcard']['paymenttype'];
			$arr_date['ChkValue']=$pay_info['ChkValue'];

			$arr_update['data']=json_encode($arr_data);
			$arr_update['modifierid']=$userid;
			$arr_update['modifiername']=$_SESSION['user']['profile']['nickname'];
			$arr_update['modifiertype']='User';

			$deposit->update_deposit_history($arr_cond, $arr_update);

			// $user_token = $user->get_user_token($userid);
			// error_log("[c/deposit/tw_credit_pay] user_token: ".json_encode($user_token));

			//取得信用卡代碼資料
			$user_token = $user->getUserCreditCardToken($userid,$ucid);

			$merchantID = $pay_info['web']; //商家代號（信用卡）（可登入商家專區至「服務設定」中查詢Buysafe服務的代碼）
			$transPassword = $pay_info['TransPwd']; //交易密碼（可登入商家專區至「密碼修改」處設定，此密碼非後台登入密碼）
			$isProduction = true; //是否為正式平台（true為正式平台，false為測試平台）
			$MN = $pay_info['MN'];
			$Term = $pay_info['Term'];
			$ChkValue = $this->getChkValue($web . $transPassword . $MN . $Term); //交易檢查碼（SHA1雜湊值並轉成大寫）

			//信用卡代碼交易增加參數
			$var_userID = $userid; 										//會員帳號
			if ($paymentTokenuse != 'N'){
				$var_paymentToken = $user_token['paymentToken']; 		//信用卡代碼，第一次交易為空字串，取得代碼後第二次交易即可帶入信用卡代碼
			}else{
				$var_paymentToken = ''; 								//信用卡代碼，第一次交易為空字串，取得代碼後第二次交易即可帶入信用卡代碼
			}
			$var_paymentData = ''; 										//代碼認證資料，第一次交易為空字串，取得代碼後第二次交易由下面的程式組成實際內容

			//帳號與金鑰設定（用於信用卡代碼交易）
			$key = $config['creditcard']['key']; 						//金鑰
			$encIV = $config['creditcard']['encIV']; 					//IV

			if ($var_paymentToken != "") 								//有信用卡代碼時的交易參數
			{
				$verificationCode = (string)$user_token['verificationCode']; 	//代碼認證資料（可由第一次交易產生信用卡代碼時同時取得）
				$tokenExpiryDate = $user_token['tokenExpiryDate']; 		//代碼有效期限（可由第一次交易產生信用卡代碼時同時取得）

				//先取得通行碼
				$passcode = ''; 										//通行碼(下方函數取得)
				$passcode = $this->getPasscode($isProduction, $merchantID, $transPassword, $key, $encIV, $var_userID, $var_paymentToken, $verificationCode, $tokenExpiryDate, $MN);
				// error_log("[c/deposit/tw_credit_pay] passcode: ".$passcode);

				if ($passcode=='') {
					exit('取得通行碼失敗'); //取得通行碼失敗，應中止並檢查錯誤為何（通常為某參數數值錯誤）
				}

				//組成paymentData內容
				$paymentData=array('passcode'=>'','userID'=>'','verificationCode'=>'','tokenExpiryDate'=>'','price'=>''); //傳入資料（交易內容）
				$paymentData['passcode'] = $passcode;
				$paymentData['userID'] = $var_userID;
				$paymentData['verificationCode'] = $verificationCode;
				$paymentData['tokenExpiryDate'] = $tokenExpiryDate;
				$paymentData['price'] = $MN; //交易金額（同前面設定）

				//加密
				$var_paymentData = $this->json_encode_fix($paymentData);
				//verificationCode在加密時,會出現"+"被替換成空白的問題,所以需將空白替換回來
				$var_paymentData = str_replace(" ","+",$var_paymentData);
				// error_log("[c/deposit/ ] var_paymentData: ".$var_paymentData);

				$var_paymentData = $this->dataEncrypt($var_paymentData, $key, $encIV);
			}
			error_log("[c/deposit/twcreditcard_pay] ucid : ".$ucid);

			if($json=='Y' && empty($ucid)){

				$data['PayType'] = $pay_info['PayType']; //andriod付款方式
				$data['RSTransactionPayType'] = $pay_info['RSTransactionPayType']; //ios付款方式

				$data['web_android'] = $pay_info['web_app_android']; //app 商家編號
				$data['web_ios'] = $pay_info['web_app_ios']; //app 商家編號

				$data['TransPwd'] = $pay_info['TransPwd']; //交易密碼
				$data['MN'] = $pay_info['MN']; //交易金額

				$data['Term'] = $pay_info['Term']; //android分期期數
				$data['RSTransactionInstallmentTermValue'] =$pay_info['RSTransactionInstallmentTermValue'];//ios分期期數
				$data['OrderInfo'] = urldecode($pay_info['OrderInfo']); //交易內容
				$data['Td'] = "SJO".$pay_info['Td']; //商家訂單編號
				$data['CustomerName'] = urldecode($pay_info['name']); //消費者姓名
				$data['Mobile'] = $pay_info['sdt']; //消費者電話
				$data['email'] = $pay_info['email']; //消費者email
				$data['note1'] = $pay_info['note1']; //備註1
				$data['note2'] = $pay_info['note2']; //備註2

				$data['isproduction_android'] = $pay_info['isproduction_android']; //android是否為正式環境
				$data['isproduction_ios'] = $pay_info['isproduction_ios']; //ios是否為正式環境
				$data['needSign_android'] = $pay_info['needSign_android']; //android是否需要簽名面版
				$data['needSign_ios'] = $pay_info['needSign_ios']; //ios是否需要簽名面版
				$data['timeoutSec_android'] = $pay_info['timeoutSec_android']; //android限制信用卡輸入畫面的輸入時間（秒）
				$data['timeoutSec_ios'] = $pay_info['timeoutSec_ios']; //ios限制信用卡輸入畫面的輸入時間（秒）
				$data['orderNoChk_android'] = $pay_info['orderNoChk_android']; //android是否加強訂單編號重複檢查
				$data['isCheckOrderNo_ios'] = $pay_info['isCheckOrderNo_ios']; //ios檢查自訂訂單編號是否重複
				$data['Language_android'] = $pay_info['Language_android']; //android交易畫面的語系設定
				$data['RSTransactionLanguage_ios'] = $pay_info['RSTransactionLanguage_ios']; //ios交易畫面的語系設定

				$data['Card_Type'] = $pay_info['Card_Type']; //交易類別
				$data['userID'] = $userid; //會員帳號
				$data['paymentToken'] = $paymentToken; //信用卡代碼
				$data['paymentData'] = $paymentData; //代碼認證資料

				$data['url_payment'] = $config['creditcard']['url_payment'];
				$data['web'] = $config['creditcard']['merchantnumber'];
				$data['sna'] = $pay_info['sna'];
				$data['sdt'] = $pay_info['sdt'];
				$data['ChkValue'] = $pay_info['ChkValue'];

				$ret = getRetJSONArray(1,'SUCCESS','LIST');
				$ret['retObj']['data'] = $data;
				error_log("[c/deposit/tw_credit_pay]: ".json_encode($ret));
				echo json_encode($ret);

			}else{

				$submit='<body onload="document.form1.submit();" >';
				$submit.='<form name="form1" action="'.$config['creditcard']['url_payment'].'" method="POST">';
				$submit.='<input type="hidden" name="web" value="'.$config['creditcard']['merchantnumber'].'" />';
				$submit.='<input type="hidden" name="MN" value="'. intval($pay_info['MN']).'" />';
				$submit.='<input type="hidden" name="OrderInfo" value="'.$pay_info['OrderInfo'].'" />';
				$submit.='<input type="hidden" name="Td" value="SJO'.$pay_info['Td'].'" />';
				$submit.='<input type="hidden" name="sna" value="'.$pay_info['sna'].'" />';
				$submit.='<input type="hidden" name="sdt" value="'.$pay_info['sdt'].'" />';
				$submit.='<input type="hidden" name="email" value="'.$pay_info['email'].'" />';
				$submit.='<input type="hidden" name="note1" value="'.$pay_info['note1'].'" />';
				$submit.='<input type="hidden" name="note2" value="'.$pay_info['note2'].'" />';
				$submit.='<input type="hidden" name="Card_Type" value="'.$pay_info['Card_Type'].'" />';
				$submit.='<input type="hidden" name="ChkValue" value="'.$pay_info['ChkValue'].'" />';
				$submit.='<input type="hidden" name="userID" value="'.$userid.'" />';
				$submit.='<input type="hidden" name="paymentToken" value="'.$var_paymentToken.'" />';
				$submit.='<input type="hidden" name="paymentData" value="'.$var_paymentData.'" />';
				$submit.='</form>';
				$submit.='</body>';
				echo($submit);

			}

		}else{
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10006,'充值程式異常','MSG');
				echo json_encode($ret);
				exit;
			}else{
				echo '<script>alert("充值程式異常!");window.location = "/site/deposit/"</script>';
			}
		}

		exit;

	}

	public function twcreditcard_pay_result() {

	  global $tpl, $config, $deposit;
		set_status($this->controller);

		$errcode = $_POST['errcode'];
		$showview = htmlspecialchars(($_POST['showview']) != "") ? htmlspecialchars($_POST['showview']) : TRUE;

		error_log("[twcreditcard_pay_result] post data : ".json_encode($_POST));

		if($errcode=='00'){
			$ret = $this->twcreditcard_pay_success("sync",$showview);	  // sync=同步

			if($showview == 'TRUE'){
				if($ret=='0000'){
					echo '<script>alert("充值成功!");window.location = "/site/member?channelid=1"</script>';
				}else{
					echo '<script>alert("充值失敗 : ['.$ret.']");window.location = "/site/member?channelid=1"</script>';
				}
			}else{
				echo $ret;
			}

		}else{
			$this->twcreditcard_pay_failed($showview);
		}

		exit;
	}

	public function twcreditcard_pay_success($callType="async", $showview) {   // async=異步

		global $tpl, $config, $deposit, $user;
		set_status($this->controller);

		if($callType=="async"){
			// 讓sync的先跑完 ...
			for($i=0;$i<=100000;++$i){
				continue;
			}
		}

		error_log("[twcreditcard_pay_success] SendType=".$callType);
		$result_array=array();
		foreach($_POST as $key => $value){
			error_log("[twcreditcard_pay_success] ${key} = ${value}");
			$result_array[$key]=(string)$value;
		}

		//信用卡代碼交易回傳參數（首次交易為使用信用卡代碼時）
		$tokenData = urldecode($this->getPostData('tokenData')); //代碼資料

		if(!empty($result_array['Td'])){

			// 比對訂單編號是否有前綴sbo如果是移除前綴
			if(preg_match("/SJO/i", $result_array['Td'])){//有前綴
			  $result_array['Td'] = str_replace("SJO","",$result_array['Td']);
			}

			$get_deposit_history=$deposit->get_deposit_history($result_array['Td']);
			if($get_deposit_history){
				$userid=$get_deposit_history[0]['userid'];
				$orderid=$get_deposit_history[0]['dhid'];
				$depositid=$get_deposit_history[0]['depositid'];
				error_log("[twcreditcard_pay_success] userid:".$userid.", orderid:".$orderid." depositid:".$depositid);

				if(!empty($depositid)){
					$get_deposit=$deposit->get_deposit_id($depositid);

					if($get_deposit){
						$amount= round(floatval($get_deposit[0]['amount']));
						error_log("[twcreditcard_pay_success]amount:".round($get_deposit[0]['amount'])."==>".$amount);
						$TransID=$result_array['buysafeno']; //紅陽交易編號
						if($TransID==''){
							$TransID=$result_array['BuySafeNo'];//（相容信用卡交易參數）
						}
						$my_chkvalue=$result_array['web'].
									$config['creditcard']['code'].
									$TransID.
									$amount.
									$result_array['errcode'];
						error_log("[twcreditcard_pay_success] My Ori ChkValue=".$my_chkvalue);
						$my_chkvalue=strtoupper(sha1($my_chkvalue));
						error_log("[twcreditcard_pay_success] My ChkValue=".$my_chkvalue);

						if($my_chkvalue==$result_array['ChkValue']){
							// 驗證ok, 修改訂單及加入贈送
							$switch=$get_deposit[0]['switch'];
							$status=$get_deposit_history[0]['status'];
							$spointid=$get_deposit_history[0]['spointid'];
							$driid=$get_deposit_history[0]['driid'];
							$nickname=$get_deposit_history[0]['modifiername'];
							error_log("[twcreditcard_pay_success] orderid: ".$orderid."/status: ".$status."/switch: ".$switch);

							//帳號與金鑰設定（用於信用卡代碼交易）
							$key = $config['creditcard']['key']; 					//金鑰
							$encIV = $config['creditcard']['encIV']; 				//IV
							//判斷信用卡是否有資料
							if ($tokenData != '') {

								try {
									$tokenData = $this->dataDecrypt($tokenData, $key, $encIV);
								} catch (Exception $ex) {
									exit('解密失敗');
								}
								error_log("[twcreditcard_pay_success] tokenData: ".$tokenData);

								//將tokenData物件化以便存取
								$tokenDataObj= $this->json_decode_fix($tokenData);
								$verification = json_decode($tokenData, true);
								//物件化後，$tokenDataObj->paymentToken為信用卡代碼，更多參數說明請參考技術手冊
								//其中信用卡代碼、代碼認證資料、信用卡代碼有效期限、信用卡卡號末四碼均為必須妥善儲存保管的資料（下次交易均會用到）
								//以下為debug用，正式環境應刪除
								// error_log("[twcreditcard_pay_success] verificationCode :".$tokenDataObj->verificationCode." <===> ".$verification['verificationCode']);
								// $tokenData_decoded = "<br>timestamp: " . $tokenDataObj->timestamp . "<br>userID: " . $tokenDataObj->userID . "<br>paymentToken: " . $tokenDataObj->paymentToken . "<br>verificationCode: " . $tokenDataObj->verificationCode . "<br>tokenExpiryDate: " . $tokenDataObj->tokenExpiryDate . "<br>last4Cardno: " . $tokenDataObj->last4Cardno;

								//寫入信用卡資料
								// $card = $user->updUserProfileCreditCardToken($userid, json_encode($tokenDataObj));
								if (!empty($verification['paymentToken']) && !empty($verification['last4Cardno'])){
									$card = $user->addUserCreditCardToken($userid, $verification);
									// error_log("[twcreditcard_pay_success] card: ".$card);
								}
							}

							if($orderid && $switch == 'N'){
								// 將json形式的資訊轉為Associated array
								$arr_data=json_decode($get_deposit_history[0]['data'],true);
								// 添加資訊到data欄位
								$arr_data['out_trade_no']=$result_array['Td'];
								$arr_data['vendor_no']=$result_array['buysafeno'];
								$arr_data['merchant_id']=$result_array['web'];
								$arr_data['amount']=$result_array['MN'];
								$arr_data['ApproveCode']=$result_array['ApproveCode'];
								$arr_data['Card_NO']=$result_array['Card_NO'];
								$arr_data['Card_Type']=$result_array['Card_Type'];
								$arr_data['SendType']=$result_array['SendType'];
								$arr_data['errcode']=$result_array['errcode'];

								if($callType=="sync"){
									$arr_data['modifierid']=$userid;
									$arr_data['modifiername']=$nickname;
									$arr_data['modifiertype']="User";
								}else if($callType=="async"){
									$arr_data['modifierid']="0";
									$arr_data['modifiername']="twcreditcard_pay_success";
									$arr_data['modifiertype']="System";
								}

								$deposit->set_deposit_history($arr_data, $get_deposit_history[0], $config['creditcard']['paymenttype']);

								if($result_array['errcode']=='00'){  // 不管異步同步, 回傳errorcode='00' 才表示成功
									$deposit->set_deposit($depositid);
									$deposit->set_spoint($spointid);
									$get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
									$deposit_conut = $deposit->get_Deposit_Count($userid);
									error_log("[c/deposit/twcreditcard_pay_success] driid : ".$driid);
									if(!empty($get_deposit_rule_item[0]['driid'])){
										$get_scode_promote = $deposit->get_scode_promote_rt($driid);
										//重機首充儲值送活動，活動內容寫死 20190910
										if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00'  && $deposit_conut['num'] == 1){
											switch($driid){
												//300
												case 55:
												case 107:
													$cnt = 1;
												break;
												//2000
												case 56:
												case 173:
													$cnt = 7;
												break;
												//10000
												case 59:
												case 110:
													$cnt = 34;
												break;
												//30000
												case 60:
												case 111:
													$cnt = 101;
												break;
												//50000
												case 176:
												case 182:
													$cnt = 167;
												break;
												//100000
												case 179:
												case 185:
													$cnt = 334;
												break;
											}
											for($i = 0; $i < $cnt; $i++){
												$sv['productid'] = 10044;
												$sv['spid'] = 0;
												$deposit->add_oscode($userid, $sv);
											}
										}else{
											if(!empty($get_scode_promote)){
												foreach($get_scode_promote  as $sk => $sv){
													if(!empty($sv['productid']) && $sv['productid'] > 0){
														for($i = 0; $i < $sv['num']; $i++){
															$deposit->add_oscode($userid, $sv);
														}
													}
												}
											}
											$ret="0000";
											//判斷是否第一次充值
											if($deposit_conut['num'] == 1){

												$get_scode_promote = $deposit->get_scode_promote_rt($driid);
												if(!empty($get_scode_promote)){
													$deposit_times = $deposit->getDepositTimes();
													error_log("[twcreditcard_pay_success] times: ".json_encode($deposit_times));

													if ($deposit_times['times'] > 1) {
														//首儲送的倍數跑迴圈
														for($t = 0; $t < (int)$deposit_times['times']-1; $t++){
															if ($t < (int)$deposit_times['times']) {
																foreach($get_scode_promote  as $sk => $sv){
																	if(!empty($sv['productid']) && $sv['productid'] > 0){
																		for($i = 0; $i < $sv['num']; $i++){
																			$deposit->add_oscode($userid, $sv);
																		}
																	}
																}
															}
														}
													}
												}

											}
										}
										// 首次儲值送推薦人2塊
										// $this->add_deposit_to_user_src($userid);
									}

									// 開立電子發票
									$Invoice = $this->createInvoice($depositid, $userid);
									$ret="0000";

								}

							}else{
								error_log("Duplicated Update, do nothing !!");
								$ret="0000";
							}

						}else{
							$ret="Data Consistency Check Error !!";
						}
					}

				}else{
					$ret="Empty deposit id of ".$orderid." !!";
				}

			}else{
				$ret="Cannot find the data of ".$orderid." !!";
			}

		}else{
			$ret="Empty order id !!";
		}

		error_log($ret);

		// Add By Thomas 2020/01/19
		// 檢查用戶60分鐘內是否充值超過3次
		$ban =  $this->chk_swipe_credit_count($userid,60,3);
		error_log("[c/deposit/home] check swipe creditcard 3 times in last 60 mins for ${userid} : ".$ban);
		if($ban==true) {
			// 鎖住下標/兌換/充值等功能
			set_bid_able($userid,"deposit_enable","N");
			set_bid_able($userid,"exchange_enable","N");
			set_bid_able($userid,"bid_enable","N");
			set_bid_able($userid,"dream_enable","N");

			//  推播通知
			$inp=array();
			$inp['title']="用戶 ${userid} 頻繁充值 [".date("Y-m-d H:i:s")."]";
			$inp['body']=" 用戶 ${userid} 於過去 ${mins} 內刷卡超過 ${limit} 次 已先暫停下標/兌換/充值資格 !!";
			$inp['productid']='';
			$inp['action']="";
			$inp['url_title']="";
			$inp['url']="";
			$inp['sender']="99";
			$inp['sendto']="26958,28,2788,1705,4068";
			$inp['page']=0;
			$inp['target']='saja';
			$rtn=sendFCM($inp);
		}

		if($callType=="async"){
			echo "0000";
			exit;
		}else{
			// if $callType="sync"
			return $ret;
		}

	}

	public function twcreditcard_pay_failed($showview) {

		$errmsg=urldecode($_POST['errmsg']);

		if($ShowView == 'TRUE'){
			echo '<script>alert("充值失敗['.$errmsg.']");</script>';
			return;
		}else{
			//紅陽app異步回傳
			echo "0000";
			return;
		}

	}



	/*
	 *	綠界信用卡支付送出
	 *	$userid					int						會員編號
	 *	$json					varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$drid					int						儲值方式編號
	 *	$driid					int						儲值方案編號
	 *	$amount					int						充值金額
	 *	$ordernumber			varchar					訂單編號
	 *	$spoint					int						充值點數
	 *	$chkStr					varchar					加密簽名
	 *	$paymentTokenuse		varchar					快速結帳使用
	 *	$ucid					int						信用卡流水編號
	 */
	public function eccreditcard_pay() {

		global $tpl, $config, $deposit, $user;

		set_status($this->controller);
		// login_required();
		$auth_id = empty($_GET['auth_id'])?htmlspecialchars($_POST['auth_id']):htmlspecialchars($_GET['auth_id']);
		$userid = empty($_SESSION['auth_id'])?$auth_id:$_SESSION['auth_id'];
		$json = empty($_GET['json'])?htmlspecialchars($_POST['json']):htmlspecialchars($_GET['json']);
		$drid = empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
		$driid = empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
		$amount = empty($_GET['amount'])?htmlspecialchars($_POST['amount']):htmlspecialchars($_GET['amount']);
		$spoint = empty($_GET['spoint'])?htmlspecialchars($_POST['spoint']):htmlspecialchars($_GET['spoint']);
		$chkStr = empty($_GET['chkStr'])?htmlspecialchars($_POST['chkStr']):htmlspecialchars($_GET['chkStr']);
		$ordernumber = empty($_GET['ordernumber'])?htmlspecialchars($_POST['ordernumber']):htmlspecialchars($_GET['ordernumber']);
		$paymentTokenuse = empty($_GET['paymentTokenuse'])?htmlspecialchars($_POST['paymentTokenuse']):htmlspecialchars($_GET['paymentTokenuse']);
		$runAt = empty($_GET['runAt'])?htmlspecialchars($_POST['runAt']):htmlspecialchars($_GET['runAt']);
		$ucid = empty($_GET['ucid'])?htmlspecialchars($_POST['ucid']):htmlspecialchars($_GET['ucid']);

		if ($runAt == 'APP'){
			$chkStr=$ordernumber."|".$amount;
			$cs = new convertString();
			$chkStr=$cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
		}

		error_log("[c/deposit/eccreditcard_pay] userid : ".$userid);
		// 檢查交易金額是否正確
		if(floatval($amount)<0){
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10001,'交易金額錯誤','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("交易金額錯誤!!");history.back();</script>');
				exit;
			}
		}

		// 檢查是否正確
		if(empty($chkStr)){
			if($json == 'Y'){
				$ret = getRetJSONArray(-10002,'加密簽名資料錯誤','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("加密簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}

		$pay_info=array();
		$pay_info['web']=$config['creditcard']['merchantnumber'];
		$pay_info['MN']= floatval($amount);
		$pay_info['Td']=$ordernumber;
		$pay_info['sna']=urlencode($_SESSION['user']['profile']['nickname']."(".$userid.")");
		$pay_info['sdt']=$_SESSION['user']['profile']['phone'];

		if(!is_numeric($pay_info['sdt'])){
			$pay_info['sdt']='';
		}

		$pay_info['email']=empty($_SESSION['user']['email'])? "" : $_SESSION['user']['email'];
		$pay_info['note1']="{userid:".$userid.",OrderId:".$ordernumber.",Amount:".floatval($amount)."}";
		$pay_info['note2']="saja_tw";
		$pay_info['name']=$_SESSION['user']['profile']['nickname'];

		$pay_info['OrderInfo']=urlencode("OrderId:".$pay_info['Td'].
												",Name:".$_SESSION['user']['profile']['nickname'].
												",Userid:".$userid.
												",Amount:".$pay_info['MN'].
												",Spts:".$spoint);

		$cs=new convertString();
		$chkStr=$cs->strDecode($chkStr,$config["encode_key"],$config["encode_type"]);
		$chkArr=explode("|",$chkStr);

		if(is_array($chkArr)){

			$chk_orderid=$chkArr[0];
			$chk_amount=$chkArr[1];

			if(floatval($chk_amount)!=floatval($amount)){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10003,'交易金額不符','MSG');
					echo json_encode($ret);
					exit;
				}else{
					die('<script>alert("交易金額不符 !!");history.back();</script>');
					exit;
				}
			}

			if($chk_orderid!=$ordernumber){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10004,'訂單編號不符!!','MSG');
					echo json_encode($ret);
					exit;
				}else{
					die('<script>alert("訂單編號不符 !!");history.back();</script>');
					exit;
				}
			}

		}else{
			if($json == 'Y'){
				$ret = getRetJSONArray(-10005,'簽名資料錯誤 !!','MSG');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}

		$get_deposit_history=$deposit->get_deposit_history($pay_info['Td']);

		if(!empty($get_deposit_history[0]['dhid'])){

			$arr_cond=array();
			$arr_cond['dhid']=$pay_info['Td'];
			$arr_data=array();
			$arr_data['out_trade_no']=$pay_info['Td'];
			$arr_data['userid']=$userid;
			$arr_data['amount']= intval($pay_info['MN']);
			$arr_data['timepaid']=date('YmdHis');
			$arr_data['phone']=$pay_info['sdt'];
			$arr_data['paymenttype']=$config['creditcard']['paymenttype'];
			$arr_date['ChkValue']=$pay_info['ChkValue'];

			$arr_update['data']=json_encode($arr_data);
			$arr_update['modifierid']=$userid;
			$arr_update['modifiername']=$_SESSION['user']['profile']['nickname'];
			$arr_update['modifiertype']='User';

			$deposit->update_deposit_history($arr_cond, $arr_update);

			// print_r($pay_info);exit;
				
			//載入SDK(路徑可依系統規劃自行調整)
			include(LIB_DIR .'/ECPay.Payment.Integration.php');
			try {
		
				$obj = new ECPay_AllInOne();

				//服務參數
				$obj->ServiceURL  = $config['creditcard']['ServiceURL'];                          //服務位置
				$obj->HashKey     = $config['creditcard']['HashKey'];                             //測試用Hashkey，請自行帶入ECPay提供的HashKey
				$obj->HashIV      = $config['creditcard']['HashIV'];                              //測試用HashIV，請自行帶入ECPay提供的HashIV
				$obj->MerchantID  = $config['creditcard']['MerchantID'];                          //測試用MerchantID，請自行帶入ECPay提供的MerchantID
				$obj->EncryptType = $config['creditcard']['EncryptType'];                         //CheckMacValue加密類型，請固定填入1，使用SHA256加密

				//基本參數(請依系統規劃自行調整)
				$MerchantTradeNo = $pay_info['Td'];
				$obj->Send['StoreID']         	= "SJO";                                     //特店旗下店舖代號
				$obj->Send['ReturnURL']         = $config['creditcard']['ReturnURL'];        //付款完成通知回傳的網址
				$obj->Send['MerchantTradeNo']   = $MerchantTradeNo;                          //訂單編號
				$obj->Send['MerchantTradeDate'] = date('Y/m/d H:i:s');                       //交易時間
				$obj->Send['TotalAmount']       = $pay_info['MN'];                           //交易金額
				$obj->Send['TradeDesc']         = "購買殺價幣".(int)$spoint ;                //交易描述
				$obj->Send['ChoosePayment']     = ECPay_PaymentMethod::Credit ;              //付款方式:Credit
				$obj->Send['IgnorePayment']     = ECPay_PaymentMethod::GooglePay ;           //不使用付款方式:GooglePay
				$obj->Send['ClientBackURL']     = $config['creditcard']['ClientBackURL'];    //Client端返回特店的按鈕連結
				$obj->Send['OrderResultURL']    = $config['creditcard']['OrderResultURL'];   //Client 端回傳付款結果網址

				//訂單的商品資料
				array_push(
					$obj->Send['Items'], 
					array(
						'Name' => "購買殺價幣", 
						'Price' => (int)$spoint,
						'Currency' => "點", 
						'Quantity' => (int) "1",
						'URL' => "dedwed"
					)
				);
				
				//Credit信用卡分期付款延伸參數(可依系統需求選擇是否代入)
				//以下參數不可以跟信用卡定期定額參數一起設定
				$obj->SendExtend['CreditInstallment'] = $config['creditcard']['CreditInstallment'] ;            //分期期數，預設0(不分期)，信用卡分期可用參數為:3,6,12,18,24
				$obj->SendExtend['InstallmentAmount'] = $config['creditcard']['InstallmentAmount'] ;            //使用刷卡分期的付款金額，預設0(不分期)
				$obj->SendExtend['Redeem']            = $config['creditcard']['Redeem'];                        //是否使用紅利折抵，預設false
				$obj->SendExtend['UnionPay']          = $config['creditcard']['UnionPay'];                      //是否為聯營卡，預設false;

				//產生訂單(auto submit至ECPay)
				$obj->CheckOut();

			} catch (Exception $e) {
				echo $e->getMessage();
			}
		
		}else{
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10006,'充值程式異常','MSG');
				echo json_encode($ret);
				exit;
			}else{
				echo '<script>alert("充值程式異常!");window.location = "/site/deposit/"</script>';
			}
		}
		exit;
	}

	/*
	 *	綠界信用卡支付回傳
	 */
	public function eccreditcard_pay_result() {

	  global $tpl, $config, $deposit;
		set_status($this->controller);

		$errcode = $_POST['RtnCode'];
		$showview = htmlspecialchars(($_POST['showview']) != "") ? htmlspecialchars($_POST['showview']) : TRUE;

		error_log("[eccreditcard_pay_result] post data : ".json_encode($_POST));

		if($errcode=='1'){
			$ret = $this->eccreditcard_pay_success("sync",$showview);	  // sync=同步

			if($showview == 'TRUE'){
				if($ret=='1|OK'){
					echo '<script>alert("充值成功!");window.location = "/site/member?channelid=1"</script>';
				}else{
					echo '<script>alert("充值失敗 : ['.$ret.']");window.location = "/site/member?channelid=1"</script>';
				}
			}else{
				echo $ret;
			}

		}else{
			$this->eccreditcard_pay_failed($showview);
		}

		exit;
	}

	/*
	 *	綠界信用卡支付回傳成功
	 */
	public function eccreditcard_pay_success($callType="async", $showview) {   // async=異步

		global $tpl, $config, $deposit, $user;
		set_status($this->controller);

		if($callType=="async"){
			// 讓sync的先跑完 ...
			for($i=0;$i<=100000;++$i){
				continue;
			}
		}

		error_log("[eccreditcard_pay_success] SendType=".$callType);
		$result_array=array();
		foreach($_POST as $key => $value){
			error_log("[eccreditcard_pay_success] ${key} = ${value}");
			$result_array[$key]=(string)$value;
		}
		// $ret= '1|OK';

		if(!empty($result_array['MerchantTradeNo'])){

			// 比對訂單編號是否有前綴sbo如果是移除前綴
			if(preg_match("/SJO/i", $result_array['MerchantTradeNo'])){//有前綴
			  $result_array['MerchantTradeNo'] = str_replace("SJO","",$result_array['MerchantTradeNo']);
			}

			$get_deposit_history=$deposit->get_deposit_history($result_array['MerchantTradeNo']);
			if($get_deposit_history){
				$userid=$get_deposit_history[0]['userid'];
				$orderid=$get_deposit_history[0]['dhid'];
				$depositid=$get_deposit_history[0]['depositid'];
				error_log("[eccreditcard_pay_success] userid:".$userid.", orderid:".$orderid." depositid:".$depositid);

				if(!empty($depositid)){
					$get_deposit=$deposit->get_deposit_id($depositid);

					if($get_deposit){
						$amount= round(floatval($get_deposit[0]['amount']));
						error_log("[eccreditcard_pay_success]amount:".round($get_deposit[0]['amount'])."==>".$amount);
						
						include(LIB_DIR .'/ECPay.Payment.Integration.php');
						
						try {
					
							$obj = new ECPay_AllInOne();

							//服務參數
							$obj->ServiceURL  = $config['creditcard']['ServiceURL'];                          //服務位置
							$obj->HashKey     = $config['creditcard']['HashKey'];                             //測試用Hashkey，請自行帶入ECPay提供的HashKey
							$obj->HashIV      = $config['creditcard']['HashIV'];                              //測試用HashIV，請自行帶入ECPay提供的HashIV
							$obj->MerchantID  = $config['creditcard']['MerchantID'];                          //測試用MerchantID，請自行帶入ECPay提供的MerchantID
							$obj->EncryptType = $config['creditcard']['EncryptType'];                         //CheckMacValue加密類型，請固定填入1，使用SHA256加密

							//產生訂單(auto submit至ECPay)
							$my_chkvalue = $obj->CheckOutFeedback();

						} catch (Exception $e) {
							echo $e->getMessage();
						}
						
						error_log("[eccreditcard_pay_success] My Ori ChkValue=".$result_array['CheckMacValue']);
						error_log("[eccreditcard_pay_success] My ChkValue=".json_encode($my_chkvalue));

						if($my_chkvalue['arErrors'] == 0){
							// 驗證ok, 修改訂單及加入贈送
							$switch=$get_deposit[0]['switch'];
							$status=$get_deposit_history[0]['status'];
							$spointid=$get_deposit_history[0]['spointid'];
							$driid=$get_deposit_history[0]['driid'];
							$nickname=$get_deposit_history[0]['modifiername'];
							error_log("[eccreditcard_pay_success] orderid: ".$orderid."/status: ".$status."/switch: ".$switch);


							if($orderid && $switch == 'N'){
								// 將json形式的資訊轉為Associated array
								$arr_data=json_decode($get_deposit_history[0]['data'],true);
								// 添加資訊到data欄位
								$arr_data['MerchantID']=$result_array['MerchantID'];
								$arr_data['MerchantTradeNo']=$result_array['MerchantTradeNo'];
								$arr_data['StoreID']=$result_array['StoreID'];
								$arr_data['TradeNo']=$result_array['TradeNo'];
								$arr_data['TradeAmt']=$result_array['TradeAmt'];
								$arr_data['PaymentDate']=$result_array['PaymentDate'];
								$arr_data['PaymentType']=$result_array['PaymentType'];
								$arr_data['PaymentTypeChargeFee']=$result_array['PaymentTypeChargeFee'];
								$arr_data['TradeDate']=$result_array['TradeDate'];
								$arr_data['SimulatePaid']=$result_array['SimulatePaid'];
								$arr_data['CheckMacValue']=$result_array['CheckMacValue'];
								$arr_data['RtnMsg']=$result_array['RtnMsg'];
								$arr_data['RtnCode']=$result_array['RtnCode'];
								$arr_data['out_trade_no']=$result_array['MerchantTradeNo'];

								if($callType=="sync"){
									$arr_data['modifierid']=$userid;
									$arr_data['modifiername']=$nickname;
									$arr_data['modifiertype']="User";
								}else if($callType=="async"){
									$arr_data['modifierid']="0";
									$arr_data['modifiername']="eccreditcard_pay_success";
									$arr_data['modifiertype']="System";
								}

								$deposit->set_deposit_history($arr_data, $get_deposit_history[0], $config['creditcard']['paymenttype']);

								if($result_array['RtnCode']=='1'){  // 不管異步同步, 回傳errorcode='1' 才表示成功
									$deposit->set_deposit($depositid);
									$deposit->set_spoint($spointid);
									$get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
									$deposit_conut = $deposit->get_Deposit_Count($userid);
									error_log("[c/deposit/eccreditcard_pay_success] driid : ".$driid);
									if(!empty($get_deposit_rule_item[0]['driid'])){
										$get_scode_promote = $deposit->get_scode_promote_rt($driid);
										//重機首充儲值送活動，活動內容寫死 20190910
										if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00'  && $deposit_conut['num'] == 1){
											switch($driid){
												//300
												case 55:
												case 107:
													$cnt = 1;
												break;
												//2000
												case 56:
												case 173:
													$cnt = 7;
												break;
												//10000
												case 59:
												case 110:
													$cnt = 34;
												break;
												//30000
												case 60:
												case 111:
													$cnt = 101;
												break;
												//50000
												case 176:
												case 182:
													$cnt = 167;
												break;
												//100000
												case 179:
												case 185:
													$cnt = 334;
												break;
											}
											for($i = 0; $i < $cnt; $i++){
												$sv['productid'] = 10044;
												$sv['spid'] = 0;
												$deposit->add_oscode($userid, $sv);
											}
										}else{
											if(!empty($get_scode_promote)){
												foreach($get_scode_promote  as $sk => $sv){
													if(!empty($sv['productid']) && $sv['productid'] > 0){
														for($i = 0; $i < $sv['num']; $i++){
															$deposit->add_oscode($userid, $sv);
														}
													}
												}
											}
											$ret="0000";
											//判斷是否第一次充值
											if($deposit_conut['num'] == 1){

												$get_scode_promote = $deposit->get_scode_promote_rt($driid);
												if(!empty($get_scode_promote)){
													$deposit_times = $deposit->getDepositTimes();
													error_log("[eccreditcard_pay_success] times: ".json_encode($deposit_times));

													if ($deposit_times['times'] > 1) {
														//首儲送的倍數跑迴圈
														for($t = 0; $t < (int)$deposit_times['times']-1; $t++){
															if ($t < (int)$deposit_times['times']) {
																foreach($get_scode_promote  as $sk => $sv){
																	if(!empty($sv['productid']) && $sv['productid'] > 0){
																		for($i = 0; $i < $sv['num']; $i++){
																			$deposit->add_oscode($userid, $sv);
																		}
																	}
																}
															}
														}
													}
												}

											}
										}
										// 首次儲值送推薦人2塊
										// $this->add_deposit_to_user_src($userid);
									}

									// 開立電子發票
									$Invoice = $this->createInvoice($depositid, $userid);
									$ret="1|OK";
								}

							}else{
								error_log("Duplicated Update, do nothing !!");
								$ret="1|OK";
							}

						}else{
							$ret="Data Consistency Check Error !!";
						}
					}

				}else{
					$ret="Empty deposit id of ".$orderid." !!";
				}

			}else{
				$ret="Cannot find the data of ".$orderid." !!";
			}

		}else{
			$ret="Empty order id !!";
		}

		error_log($ret);

		// Add By Thomas 2020/01/19
		// 檢查用戶60分鐘內是否充值超過3次
		// $ban =  $this->chk_swipe_credit_count($userid,60,3);
		error_log("[c/deposit/home] check swipe creditcard 3 times in last 60 mins for ${userid} : ".$ban);
		if($ban==true) {
			// 鎖住下標/兌換/充值等功能
			// set_bid_able($userid,"deposit_enable","N");
			// set_bid_able($userid,"exchange_enable","N");
			// set_bid_able($userid,"bid_enable","N");
			// set_bid_able($userid,"dream_enable","N");

			//  推播通知
			// $inp=array();
			// $inp['title']="用戶 ${userid} 頻繁充值 [".date("Y-m-d H:i:s")."]";
			// $inp['body']=" 用戶 ${userid} 於過去 ${mins} 內刷卡超過 ${limit} 次 已先暫停下標/兌換/充值資格 !!";
			// $inp['productid']='';
			// $inp['action']="";
			// $inp['url_title']="";
			// $inp['url']="";
			// $inp['sender']="99";
			// $inp['sendto']="26958,28,2788,1705,4068";
			// $inp['page']=0;
			// $inp['target']='saja';
			// $rtn=sendFCM($inp);
		}

		if($callType=="async"){
			echo "1|OK";
			exit;
		}else{
			// if $callType="sync"
			return $ret;
		}

	}

	/*
	 *	綠界信用卡支付回傳失敗
	 */
	public function eccreditcard_pay_failed($showview) {

		$errmsg=urldecode($_POST['errmsg']);

		if($ShowView == 'TRUE'){
			echo '<script>alert("充值失敗['.$errmsg.']");</script>';
			return;
		}else{
			//紅陽app異步回傳
			echo "0000";
			return;
		}

	}



	/*
	 *	台灣 Pay 支付
	 */
	public function taiwanpay() {
		global $tpl, $config, $deposit, $user;

		set_status($this->controller);
		//login_required();
		error_log("[taiwanpay] post : ".json_encode($_POST));
		error_log("[taiwanpay] get : ".json_encode($_GET));
		
		$auth_id = empty($_GET['auth_id'])?htmlspecialchars($_POST['auth_id']):htmlspecialchars($_GET['auth_id']);
		$var['userid'] = empty($_SESSION['auth_id'])?$auth_id:$_SESSION['auth_id'];
		$var['json'] = empty($_GET['json'])?htmlspecialchars($_POST['json']):htmlspecialchars($_GET['json']);
		$var['drid'] = empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
		$var['driid'] = empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
		$var['amount'] = empty($_GET['amount'])?htmlspecialchars($_POST['amount']):htmlspecialchars($_GET['amount']);
		$var['chkStr'] = empty($_GET['chkStr'])?htmlspecialchars($_POST['chkStr']):htmlspecialchars($_GET['chkStr']);
		$var['ordernumber'] = empty($_GET['ordernumber'])?htmlspecialchars($_POST['ordernumber']):htmlspecialchars($_GET['ordernumber']);
		
		//載入lib
		include_once(LIB_DIR ."/taiwanpay_process.php");
		
		//支付處理
		$twp = new taiwan_pay();
		$ret = $twp->home($var);
		
		echo json_encode($ret);
		exit;
	}
	
	
	public function taiwanpay_result() {
		global $tpl, $config, $deposit, $user;
		
		set_status($this->controller);
		//error_log("[taiwanpay_result] post : ".json_encode($_POST));
		error_log("[taiwanpay_result] get : ".json_encode($_GET));

		//$var['userid'] = empty($_GET['auth_id'])?htmlspecialchars($_POST['auth_id']):htmlspecialchars($_GET['auth_id']);
		//$var['json'] = empty($_GET['json'])?htmlspecialchars($_POST['json']):htmlspecialchars($_GET['json']);
		$var['acqBank'] = empty($_GET['acqBank'])?htmlspecialchars($_POST['acqBank']):htmlspecialchars($_GET['acqBank']);
		$var['cardPlan'] = empty($_GET['cardPlan'])?htmlspecialchars($_POST['cardPlan']):htmlspecialchars($_GET['cardPlan']);
		$var['merchantId'] = empty($_GET['merchantId'])?htmlspecialchars($_POST['merchantId']):htmlspecialchars($_GET['merchantId']);
		$var['orderNumber'] = empty($_GET['orderNumber'])?htmlspecialchars($_POST['orderNumber']):htmlspecialchars($_GET['orderNumber']);
		$var['responseCode'] = empty($_GET['responseCode'])?htmlspecialchars($_POST['responseCode']):htmlspecialchars($_GET['responseCode']);
		$var['terminalId'] = empty($_GET['terminalId'])?htmlspecialchars($_POST['terminalId']):htmlspecialchars($_GET['terminalId']);
		$var['verifyCode'] = empty($_GET['verifyCode'])?htmlspecialchars($_POST['verifyCode']):htmlspecialchars($_GET['verifyCode']);
		
		//載入lib
		include_once(LIB_DIR ."/taiwanpay_process.php");
		
		//支付處理
		$twp = new taiwan_pay();
		$ret = $twp->result($var);
		
		//echo json_encode($ret);
		exit;
	}


	public function gama_pay_result() {

		global $tpl, $config, $deposit;
		set_status($this->controller);

		foreach($_POST as $key => $value){
			$result_array[$key]=$value;
		}

		if($result_array['ResultCode']=='0000'){
			$this->gama_pay_success($result_array);
		}else{
			$this->gama_pay_failed($result_array);
		}

		exit;

	}


	/*
		回傳例
		{
		"ResultCode":"0000","ResultMessage":"成功",
		"TransactionID":"20180808100084723873",
		"MerchantOrderID":"7940",
		"GamaPayAccount":"167b34b2-f300-4038-9819-631d9d2dac4b",
		"MerchantID":"2052",
		"SubMerchantID":"0",
		"MerchantAccount":"0916587443",
		"TransAmount":2.0,
		"CurrencyCode":"TWD",
		"TransType":10,
		"TransFinallyAmount":2.0,
		"TransFee":0.0,
		"ReceiverLoginID":"saja",
		"ReceiverName":"殺價王",
		"PerformanceBondDateTime":null,
		"LoveCode":null,"CarrierID":null,"StatusCode":100,
		"TransDate":"2018-08-08T16:36:33",
		"CreatedDate":"2018-08-08T16:36:33",
		"MAC":"aOMHwIsZBHDy9v+ReCiSvu5sy1W8Ro3LP1RgDTPHYvM="
		}
	*/
	public function gama_pay_success($arrRet) {

		global $tpl, $config, $deposit;
		set_status($this->controller);

		error_log("[c/deposit/gama_pay_success] response : ".json_encode($arrRet));
		$ret=array();
		$ret['retCode']=$arrRet['ResultCode'];
		$ret['retMsg']=$arrRet['ResultMessage'];
		// 非成功狀態
		if($arrRet['ResultCode']!='0000'){
			error_log("[c/deposit/gama_pay_success] : ".json_encode($ret));
			echo json_encode($ret);
			return;
		}

		if(!empty($arrRet['MerchantOrderID']) && !empty($arrRet['TransAmount'])){
			$dhid=$arrRet['MerchantOrderID'];
			$TransAmount=$arrRet['TransAmount'];
			$get_deposit_history=$deposit->get_deposit_history($dhid);

			if($get_deposit_history){
				$userid=$get_deposit_history[0]['userid'];
				$orderid=$get_deposit_history[0]['dhid'];
				$depositid=$get_deposit_history[0]['depositid'];
				error_log("[c/deposit/gama_pay_success] userid:".$userid.", orderid:".$orderid.", depositid:".$depositid);
				if(!empty($depositid)){
					$get_deposit=$deposit->get_deposit_id($depositid);
					if($get_deposit){
						error_log("[c/deposit/gama_pay_success] amount : ".round($get_deposit[0]['amount'])."<==> TransAmount :".round($TransAmount));
						if(round($get_deposit[0]['amount'])==round($TransAmount) ){
							// 驗證ok, 修改訂單及加入贈送
							$switch=$get_deposit[0]['switch'];
							$status=$get_deposit_history[0]['status'];
							$spointid=$get_deposit_history[0]['spointid'];
							$driid=$get_deposit_history[0]['driid'];
							$nickname=$get_deposit_history[0]['modifiername'];
							error_log("[c/deposit/gama_pay_success] orderid: ".$orderid."/status: ".$status."/switch: ".$switch);
							if ($orderid && $switch == 'N'){
								$arr_cond=array();
								$arr_cond['dhid']=$dhid;
								$arr_cond['switch']="Y";

								$arr_upd=array();
								$arr_upd['data']=json_encode($_POST);
								$arr_upd['status']='deposit';
								$arr_upd['modifierid']=$userid;
								$arr_upd['modifiertype']="User";

								$updateH = $deposit->update_deposit_history($arr_cond, $arr_upd);

								if($updateH==1){
									// 儲值生效
									$deposit->set_deposit($depositid);
									// 殺幣生效
									$deposit->set_spoint($spointid);

									$get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
									error_log("[c/deposit/gama_pay_success] driid : ".$driid);

									if(!empty($get_deposit_rule_item[0]['driid'])){
													$get_scode_promote = $deposit->get_scode_promote_rt($driid);
													if (!empty($get_scode_promote)){
														foreach ($get_scode_promote  as $sk => $sv){
															if (!empty($sv['productid']) && $sv['productid'] > 0){
																for ($i = 0; $i < $sv['num']; $i++){
																	 $deposit->add_oscode($userid, $sv);
																}
															}
														}
													}
													// 首次儲值送推薦人2塊
													$this->add_deposit_to_user_src($userid);
													// 開立電子發票
													$this->createInvoice($depositid, $userid);
														$ret['retCode']=1;
													$ret['retMsg']='OK';
												  }
											  }

										  }else{
												$ret['retCode']=-2;
												$ret['retMsg']="Duplicated Update, do nothing !!";
										  }

						}else{
							$ret['retCode']=-3;
							$ret['retMsg']="Data Consistency Check Error !!";
						}
					}

				}else{
					$ret['retCode']=-4;
					$ret['retMsg']="Empty deposit id of ".$orderid." !!";
				}

			}else{
				$ret['retCode']=-5;
				$ret['retMsg']="Cannot find the data of ".$orderid." !!";
			}

		}else{
			$ret['retCode']=-5;
			$ret['retMsg']="Empty order id !!";
		}

		error_log("[c/deposit/gama_pay_success] ret : ".json_encode($ret));
		echo json_encode($ret);
		return;

	}


	public function gama_pay_failed($arrRet) {

		error_log("[c/deposit/gama_pay_failed] response : ".json_encode($arrRet));
		$ret = array("retCode"=>-99, "retMsg"=>$arrRet['ResultMsg']);
		error_log("[c/deposit/gama_pay_failed] ret : ".json_encode($ret));
		echo json_encode($ret);
		return;

	}


	/*
	 *	超商代收（條碼）支付送出
	 *	$userid					int						會員編號
	 *	$json					varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$drid					int						儲值方式編號
	 *	$driid					int						儲值方案編號
	 *	$amount					int						充值金額
	 *	$ordernumber			varchar					訂單編號
	 *	$spoint					int						充值點數
	 *	$chkStr					varchar					加密簽名
	 *	$paymentTokenuse		varchar					快速結帳使用
	 *	$ucid					int						信用卡流水編號
	 */
	public function payment24_pay() {

		global $tpl, $config, $deposit, $user;

		set_status($this->controller);
		// login_required();
		$auth_id = empty($_GET['auth_id'])?htmlspecialchars($_POST['auth_id']):htmlspecialchars($_GET['auth_id']);
		$userid = empty($_SESSION['auth_id'])?$auth_id:$_SESSION['auth_id'];
		$json = empty($_GET['json'])?htmlspecialchars($_POST['json']):htmlspecialchars($_GET['json']);
		$drid = empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
		$driid = empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
		$amount = empty($_GET['amount'])?htmlspecialchars($_POST['amount']):htmlspecialchars($_GET['amount']);
		$spoint = empty($_GET['spoint'])?htmlspecialchars($_POST['spoint']):htmlspecialchars($_GET['spoint']);
		$chkStr = empty($_GET['chkStr'])?htmlspecialchars($_POST['chkStr']):htmlspecialchars($_GET['chkStr']);
		$ordernumber = empty($_GET['ordernumber'])?htmlspecialchars($_POST['ordernumber']):htmlspecialchars($_GET['ordernumber']);
		$paymentTokenuse = empty($_GET['paymentTokenuse'])?htmlspecialchars($_POST['paymentTokenuse']):htmlspecialchars($_GET['paymentTokenuse']);
		$runAt = empty($_GET['runAt'])?htmlspecialchars($_POST['runAt']):htmlspecialchars($_GET['runAt']);
		$ucid = empty($_GET['ucid'])?htmlspecialchars($_POST['ucid']):htmlspecialchars($_GET['ucid']);

		if ($runAt == 'APP'){
			$chkStr=$ordernumber."|".$amount;
			$cs = new convertString();
			$chkStr=$cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
		}

		error_log("[c/deposit/payment24_pay] userid : ".$userid);
		// 檢查交易金額是否正確
		if(floatval($amount)<0){
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10001,'交易金額錯誤','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("交易金額錯誤!!");history.back();</script>');
				exit;
			}
		}

		// 檢查是否正確
		if(empty($chkStr)){
			if($json == 'Y'){
				$ret = getRetJSONArray(-10002,'加密簽名資料錯誤','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("加密簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}


		//會員個人資料
		$user_profile = $user->get_user_profile($userid);

		$pay_info=array();
		$pay_info['web'] = $config['payment']['merchantnumber'];
		$pay_info['MN'] = floatval($amount);
		$pay_info['Td'] = $ordernumber; //商家訂單編號
		$pay_info['sna'] = urlencode($_SESSION['user']['profile']['nickname']."(".$userid.")"); //消費者姓名
		$pay_info['sdt'] = empty($_SESSION['user']['profile']['phone'])? $user_profile['phone']: $_SESSION['user']['profile']['phone']; //消費者電話（不可有特殊符號）

		if(!is_numeric($pay_info['sdt'])){
			$pay_info['sdt']='';
		}

		$pay_info['email']=empty($_SESSION['user']['email'])? "" : $_SESSION['user']['email']; //消費者Email
		$pay_info['note1']="{userid:".$userid.",OrderId:".$ordernumber.",Amount:".floatval($amount)."}";//備註1（自行應用）
		$pay_info['note2']="saja_tw"; //備註2（自行應用）
		$pay_info['name']=empty($_SESSION['user']['profile']['nickname'])? $user_profile['nickname']: $_SESSION['user']['profile']['nickname'];

		$pay_info['OrderInfo']=urlencode("OrderId:".$pay_info['Td'].
												",Name:".$_SESSION['user']['profile']['nickname'].
												",Userid:".$userid.
												",Amount:".$pay_info['MN'].
												",Spts:".$spoint);


		// $pay_info['Card_Type']=$config['payment']['Card_Type'];
		$chkvalue_ori=$pay_info['web'].$config['payment']['code'].$pay_info['MN'];
		$pay_info['ChkValue']=strtoupper(sha1($chkvalue_ori));
		$pay_info['web_app_android'] = $config['payment']['merchantnumber_app'];	//app android商家編號
		$pay_info['web_app_ios'] = $config['payment']['merchantnumber_app'];	//app ios商家編號
		$pay_info['TransPwd'] = $config['payment']['code'];	//交易密碼
		$pay_info['PayType'] = "barcode";	//android付款方式 credit: 信用卡 MIT_AppPayment: 感應收款 paycode: 超商代碼繳費 barcode: 超商條碼繳費 atm_account: ATM轉帳繳款
		$pay_info['RSTransactionPayType'] = "RSTransactionPayTypeBarcode";	//ios付款方式 RSTransactionPayTypeCredit：信用卡收款 RSTransactionPayTypeMIT：感應收款 RSTransactionPayTypePaycode：超商代碼收款 RSTransactionPayTypeBarcode：超商條碼收款 RSTransactionPayTypeATMAccount：ATM轉帳收款

		$pay_info['Term'] = "";	//web,android分期期數
		$pay_info['RSTransactionInstallmentTermValue'] = $config['payment']['RSTransactionInstallmentTermValue'];//ios分期期數 RSTransactionInstallmentTermNone：不分期 RSTransactionInstallmentTerm3：3期 RSTransactionInstallmentTerm6：6期 RSTransactionInstallmentTerm12：12期 RSTransactionInstallmentTerm18：18期 RSTransactionInstallmentTerm24：24期

		$pay_info['isproduction_android'] = 'true';	//android是否為正式環境（true為正式環境，false為測試環境)
		$pay_info['isproduction_ios'] = 'YES';	//ios是否為正式環境（YES為正式環境，NO為測試環境)
		$pay_info['needSign_android'] = 'false';	//android是否需要簽名面版(true為需要,false為不需要)
		$pay_info['needSign_ios'] = 'NO';	//ios是否需要簽名面版（YES為需要，NO為不需要）
		$pay_info['timeoutSec_android'] = 500;	//android限制信用卡輸入畫面的輸入時間（秒）
		$pay_info['timeoutSec_ios'] = 500;	//ios限制信用卡輸入畫面的輸入時間（秒）
		$pay_info['orderNoChk_android'] = 'true';	//android是否加強訂單編號重複檢查（true為一律檢查，false為第二次送出交易時檢查）
		$pay_info['isCheckOrderNo_ios'] = 'YES';	//ios檢查自訂訂單編號是否重複(YES為檢查，NO為不檢查)
		$pay_info['Language_android'] = 1;	//android交易畫面的語系設定 0：手機預設，1：繁體中文，2：英文，3：簡體中文
		$pay_info['RSTransactionLanguage_ios'] = 'RSTransactionLanguageZhHant';	//ios交易畫面的語系設定，(若沒有進行設定則會自動選擇繁體中文)：RSTransactionLanguageAuto：自動切換。 RSTransactionLanguageEn：英文 RSTransactionLanguageZhHant：繁體中 RSTransactionLanguageZhHans： 簡體中文

		// for測試環境
		if($_SESSION['auth_id']=='1705'){
			$pay_info['isproduction_ios'] = 'NO';
			$pay_info['isproduction_android'] = 'false';
		}
		if($pay_info['isproduction_ios']=='NO'){
			$pay_info['web_app_ios'] = $config['payment']['merchantnumber_app_test'];
		}
		if($pay_info['isproduction_android']=='false'){
			$pay_info['web_app_android'] = $config['payment']['merchantnumber_app_test'];
		}
		$pay_info['DueDate'] = date('Ymd', strtotime("+1 days")); //繳款期限，最長 180 天(YYYYMMDD)
		$pay_info['UserNo'] = $userid;//用戶編號
		$pay_info['BillDate'] = date('Ymd');//列帳日期
		$pay_info['ProductName1'] = '殺價幣儲值-'.$driid;//產品名稱
		$pay_info['ProductPrice1'] = floatval($amount);//產品單價
		$pay_info['ProductQuantity1'] = 1; //產品數量
		$pay_info['AgencyType'] = '';//1 條碼、2 虛擬帳號
		$pay_info['AgencyBank'] = ''; // 1：中國信託銀行
		$pay_info['CargoFlag'] = ''; //空白 or 0 不需搭配物流、1 搭配物流
		$pay_info['StoreID'] = ''; //空白(紅陽端提供選擇) or 參考emap_711
		$pay_info['StoreName'] = ''; //空白(紅陽端提供選擇) or 參考emap_711
		$pay_info['BuyerCid'] = ''; //買方統一編號
		$pay_info['DonationCode'] = ''; //捐贈碼

		$cs=new convertString();
		$chkStr=$cs->strDecode($chkStr,$config["encode_key"],$config["encode_type"]);
		error_log("[c/deposit/payment24_pay] chkStr : ".$chkStr);
		$chkArr=explode("|",$chkStr);

		if(is_array($chkArr)){

			$chk_orderid=$chkArr[0];
			$chk_amount=$chkArr[1];

			if(floatval($chk_amount)!=floatval($amount)){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10003,'交易金額不符','MSG');
					echo json_encode($ret);
					exit;
				}else{
					die('<script>alert("交易金額不符 !!");history.back();</script>');
					exit;
				}
			}

			if($chk_orderid!=$ordernumber){
				if($json == 'Y'){
					$ret = getRetJSONArray(-10004,'訂單編號不符!!','MSG');
					echo json_encode($ret);
					exit;
				}else{
					die('<script>alert("訂單編號不符 !!");history.back();</script>');
					exit;
				}
			}

		}else{
			if($json == 'Y'){
				$ret = getRetJSONArray(-10005,'簽名資料錯誤 !!','MSG');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("簽名資料錯誤 !!");history.back();</script>');
				exit;
			}
		}

		$get_deposit_history=$deposit->get_deposit_history($pay_info['Td']);

		if(!empty($get_deposit_history[0]['dhid'])){

			$arr_cond=array();
			$arr_cond['dhid']=$pay_info['Td'];
			$arr_data=array();
			$arr_data['out_trade_no']=$pay_info['Td'];
			$arr_data['userid']=$userid;
			$arr_data['amount']= intval($pay_info['MN']);
			$arr_data['timepaid']=date('YmdHis');
			$arr_data['phone']=$pay_info['sdt'];
			$arr_date['ChkValue']=$pay_info['ChkValue'];

			$arr_update['data']=json_encode($arr_data);
			$arr_update['modifierid']=$userid;
			$arr_update['modifiername']=$_SESSION['user']['profile']['nickname'];
			$arr_update['modifiertype']='User';

			$deposit->update_deposit_history($arr_cond, $arr_update);
			// error_log("[c/deposit/payment24_pay] user_token: ".json_encode($user_token));

			$merchantID = $pay_info['web'];  //商家代號（超商代收（條碼））（可登入商家專區至「服務設定」中查詢24payment服務的代碼）
			$transPassword = $pay_info['TransPwd']; //交易密碼（可登入商家專區至「密碼修改」處設定，此密碼非後台登入密碼）
			$isProduction = false; //是否為正式平台（true為正式平台，false為測試平台）
			$MN = $pay_info['MN'];
			$Term = $pay_info['Term'];
			$ChkValue = $this->getChkValue($web . $transPassword . $MN); //交易檢查碼（SHA1雜湊值並轉成大寫）

			if ($json == 'Y'){

				$data['PayType'] = $pay_info['PayType']; //andriod付款方式
				$data['RSTransactionPayType'] = $pay_info['RSTransactionPayType']; //ios付款方式

				$data['web_android'] = $pay_info['web_app_android']; //app 商家編號
				$data['web_ios'] = $pay_info['web_app_ios']; //app 商家編號

				$data['TransPwd'] = $pay_info['TransPwd']; //交易密碼
				$data['MN'] = $pay_info['MN']; //交易金額

				$data['Term'] = $pay_info['Term']; //android分期期數
				$data['RSTransactionInstallmentTermValue'] =$pay_info['RSTransactionInstallmentTermValue'];//ios分期期數
				$data['OrderInfo'] = urldecode($pay_info['OrderInfo']); //交易內容
				$data['Td'] = "SJO".$pay_info['Td']; //商家訂單編號
				$data['CustomerName'] = urldecode($pay_info['name']); //消費者姓名
				$data['Mobile'] = $pay_info['sdt']; //消費者電話
				$data['email'] = $pay_info['email']; //消費者email
				$data['note1'] = $pay_info['note1']; //備註1
				$data['note2'] = $pay_info['note2']; //備註2

				$data['DueDate'] = $pay_info['DueDate']; //繳款期限，最長 180 天(YYYYMMDD)
				$data['UserNo'] = $pay_info['UserNo'];//用戶編號
				$data['BillDate'] = $pay_info['BillDate'];//列帳日期
				$data['ProductName'] = $pay_info['ProductName1'];//產品名稱
				$data['ProductPrice'] = $pay_info['ProductPrice1'];//產品單價
				$data['ProductQuantity'] = $pay_info['ProductQuantity1']; //產品數量
				$data['AgencyType'] = $pay_info['AgencyType'];//1 條碼、2 虛擬帳號
				$data['AgencyBank'] = $pay_info['AgencyBank']; // 1：中國信託銀行
				$data['CargoFlag'] = $pay_info['CargoFlag']; //空白 or 0 不需搭配物流、1 搭配物流
				$data['StoreID'] = $pay_info['StoreID']; //空白(紅陽端提供選擇) or 參考emap_711
				$data['StoreName'] = $pay_info['StoreName']; //空白(紅陽端提供選擇) or 參考emap_711
				$data['BuyerCid'] = $pay_info['BuyerCid']; //買方統一編號
				$data['DonationCode'] = $pay_info['DonationCode']; //捐贈碼

				$data['isproduction_android'] = $pay_info['isproduction_android']; //android是否為正式環境
				$data['isproduction_ios'] = $pay_info['isproduction_ios']; //ios是否為正式環境
				$data['needSign_android'] = $pay_info['needSign_android']; //android是否需要簽名面版
				$data['needSign_ios'] = $pay_info['needSign_ios']; //ios是否需要簽名面版
				$data['timeoutSec_android'] = $pay_info['timeoutSec_android']; //android限制信用卡輸入畫面的輸入時間（秒）
				$data['timeoutSec_ios'] = $pay_info['timeoutSec_ios']; //ios限制信用卡輸入畫面的輸入時間（秒）
				$data['orderNoChk_android'] = $pay_info['orderNoChk_android']; //android是否加強訂單編號重複檢查
				$data['isCheckOrderNo_ios'] = $pay_info['isCheckOrderNo_ios']; //ios檢查自訂訂單編號是否重複
				$data['Language_android'] = $pay_info['Language_android']; //android交易畫面的語系設定
				$data['RSTransactionLanguage_ios'] = $pay_info['RSTransactionLanguage_ios']; //ios交易畫面的語系設定


				$ret = getRetJSONArray(1,'SUCCESS','LIST');
				$ret['retObj']['data'] = $data;
				// Add By Thomas 2019/12/26 超商條碼繳費說明寫死for android
				$ret['retObj']['user_extrainfo_remark'] = '<div style="margin: 1rem; font-family:sans-serif;">
			                                               <br>1.繳費後約<font color="red"><b> 5 個工作日</b></font>（銀行遇假日不作業）入帳。</br>
						                                   <div>';
				error_log("[c/deposit/payment24_pay]: ".json_encode($ret));
				echo json_encode($ret);
			}else{

				$submit='<body onload="document.form1.submit();" >';
				$submit.='<form name="form1" action="'.$config['payment']['url_payment'].'" method="POST">';
				$submit.='<input type="hidden" name="web" value="'.$config['payment']['merchantnumber'].'" />';
				$submit.='<input type="hidden" name="MN" value="'. intval($pay_info['MN']).'" />';
				$submit.='<input type="hidden" name="OrderInfo" value="'.$pay_info['OrderInfo'].'" />';
				$submit.='<input type="hidden" name="Td" value="SJO'.$pay_info['Td'].'" />';
				$submit.='<input type="hidden" name="sna" value="'.$pay_info['sna'].'" />';
				$submit.='<input type="hidden" name="sdt" value="'.$pay_info['sdt'].'" />';
				$submit.='<input type="hidden" name="email" value="'.$pay_info['email'].'" />';
				$submit.='<input type="hidden" name="note1" value="'.$pay_info['note1'].'" />';
				$submit.='<input type="hidden" name="note2" value="'.$pay_info['note2'].'" />';
				$submit.='<input type="hidden" name="DueDate" value="'.$pay_info['DueDate'].'">';
				$submit.='<input type="hidden" name="UserNo" value="'.$pay_info['UserNo'].'">';
				$submit.='<input type="hidden" name="BillDate" value="'.$pay_info['BillDate'].'">';
				$submit.='<input type="hidden" name="ProductName1" value="'.$pay_info['ProductName1'].'">';
				$submit.='<input type="hidden" name="ProductPrice1" value="'.$pay_info['ProductPrice1'].'">';
				$submit.='<input type="hidden" name="ProductQuantity1" value="'.$pay_info['ProductQuantity1'].'">';
				$submit.='<input type="hidden" name="AgencyType" value="'.$pay_info['AgencyType'].'"/>';
				$submit.='<input type="hidden" name="AgencyBank" value="'.$pay_info['AgencyBank'].'"/>';
				$submit.='<input type="hidden" name="CargoFlag" value="'.$pay_info['CargoFlag'].'">';
				$submit.='<input type="hidden" name="StoreID" value="'.$pay_info['StoreID'].'">';
				$submit.='<input type="hidden" name="StoreName" value="'.$pay_info['StoreName'].'">';
				$submit.='<input type="hidden" name="BuyerCid" value="'.$pay_info['BuyerCid'].'">';
				$submit.='<input type="hidden" name="DonationCode" value="'.$pay_info['DonationCode'].'">';
				$submit.='<input type="hidden" name="ChkValue" value="'.$pay_info['ChkValue'].'">';
				$submit.='</form>';
				$submit.='</body>';
				echo($submit);
			}
		}else{
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10006,'儲值程式異常','MSG');
				echo json_encode($ret);
				exit;
			}else{
				echo '<script>alert("儲值程式異常!");window.location = "/site/deposit/"</script>';
			}
		}

		exit;
	}


	/*
	 *	超商代收（條碼）交易完成
	 *	$userid					int						會員編號
	 *	$json					varchar					瀏覽工具判斷 (Y:APP來源)
	 *	$drid					int						儲值方式編號
	 *	$driid					int						儲值方案編號
	 *	$amount					int						充值金額
	 *	$ordernumber			varchar					訂單編號
	 *	$spoint					int						充值點數
	 *	$chkStr					varchar					加密簽名
	 *	$paymentTokenuse		varchar					快速結帳使用
	 *	$ucid					int						信用卡流水編號
	 */
	public function payment24_pay_receive() {

		global $tpl, $config, $deposit, $user;

		set_status($this->controller);
		// login_required();

		$merchantID = $config['payment']['merchantnumber']; //商家代號（超商代收（條碼））（可登入商家專區至「服務設定」中查詢24payment服務的代碼）
		$transPassword = $config['payment']['code'];

		$buysafeno = $this->getPostData('buysafeno'); //紅陽交易編號
		$web = $this->getPostData('web'); //商家代號
		if ($merchantID != $web) {
			exit('商家代號錯誤');
		}
		$UserNo = $this->getPostData('UserNo'); //用戶編號
		$Td = $this->getPostData('Td'); //商家訂單編號
		$MN = $this->getPostData('MN'); //交易金額
		$note1 = urldecode($this->getPostData('note1')); //備註1
		$note2 = urldecode($this->getPostData('note2')); //備註2
		$SendType = $this->getPostData('SendType'); //傳送方式，1:背景傳送、2:網頁傳送、3:金流模組系統間回傳
		$BarcodeA = empty($this->getPostData('BarcodeA'))? $this->getPostData('barcodeA') : $this->getPostData('BarcodeA'); //超商第一段條碼
		$BarcodeB = empty($this->getPostData('BarcodeB'))? $this->getPostData('barcodeB') : $this->getPostData('BarcodeB'); //超商第二段條碼
		$BarcodeC = empty($this->getPostData('BarcodeC'))? $this->getPostData('barcodeC') : $this->getPostData('BarcodeC'); //超商第三段條碼
		$PostBarcodeA = $this->getPostData('PostBarcodeA'); //郵局第一段條碼
		$PostBarcodeB = $this->getPostData('PostBarcodeB'); //郵局第二段條碼
		$PostBarcodeC = $this->getPostData('PostBarcodeC'); //郵局第三段條碼
		$EntityATM = empty($this->getPostData('EntityATM'))? $this->getPostData('ATMAccount') : $this->getPostData('EntityATM'); //ATM轉帳帳號
		$BankCode = empty($this->getPostData('BankCode'))? $this->getPostData('ATMBankID') : $this->getPostData('BankCode'); //ATM轉帳銀行代碼
		$BankName = $this->getPostData('BankName'); //ATM轉帳分行名稱
		$StoreType = $this->getPostData('StoreType'); //物流狀態代碼
		$StoreMsg = urldecode($this->getPostData('StoreMsg')); //物流狀態解釋
		$ChkValue = $this->getPostData('ChkValue'); //交易檢查碼（SHA1雜湊值）

		//自行設計更新資料庫訂單狀態
		if (!empty($StoreType) && $ChkValue == $this->getChkValue($web . $transPassword . $buysafeno . $StoreType)) {
			//物流狀態回傳
			switch ($StoreType) {
				case '101':
					//表示貨抵達取貨門市
					break;
				case '1010':
					//表示取貨完成
					break;
				case '1B1B':
					//表示欲退貨或退貨取貨完成
					break;
			}
			exit;
		} elseif (empty($StoreType) && $ChkValue == $this->getChkValue($web . $transPassword . $buysafeno . $MN . $EntityATM)) {
			//交易結果回傳

			if(preg_match("/SJO/i", $Td)){//有前綴
			  $Td = str_replace("SJO","",$Td);
			}

			$arr_cond=array();
			$arr_cond['dhid']=$Td ;

			$arr_update=array();

			$redata = array(
				'UserNo' => $UserNo, //用戶編號
				'Td' => $Td, //商家訂單編號
				'MN' => $MN, //交易金額
				'note1' => $note1, //備註1
				'note2' => $note2, //備註2
				'SendType' => $SendType, //傳送方式，1:背景傳送、2:網頁傳送、3:金流模組系統間回傳
				'BarcodeA' => $BarcodeA, //超商第一段條碼
				'BarcodeB' => $BarcodeB, //超商第二段條碼
				'BarcodeC' => $BarcodeC, //超商第三段條碼
				'PostBarcodeA' => $PostBarcodeA, //郵局第一段條碼
				'PostBarcodeB' => $PostBarcodeB, //郵局第二段條碼
				'PostBarcodeC' => $PostBarcodeC, //郵局第三段條碼
				'EntityATM' => $EntityATM, //ATM轉帳帳號
				'BankCode' => $BankCode, //ATM轉帳銀行代碼
				'BankName' => $BankName, //ATM轉帳分行名稱
				'StoreType' => $StoreType, //物流狀態代碼
				'StoreMsg' => $StoreMsg, //物流狀態解釋
				'ChkValue' => $ChkValue //交易檢查碼（SHA1雜湊值）
			);
			$arr_update['memo'] = json_encode($redata);
			$deposit->update_deposit_history($arr_cond, $arr_update);


			//接收「背景回傳確認通知」後需回傳0000，不可包含HTML
			if ($SendType == '1') {
				echo '0000';
				exit();
			}else{
				$tpl->assign('redata',$redata);
				$tpl->assign('footer','N');
				$tpl->set_title('');
				$tpl->render("deposit","paybarcode",true);
			}
		} else {
			exit('交易檢查碼錯誤');
		}



	}


	/*
	 *	超商代收（條碼）交易結果回傳
	 */
	public function payment24_pay_result() {

	  global $tpl, $config, $deposit;
		set_status($this->controller);

		$errcode = $_POST['errcode'];
		$showview = htmlspecialchars(($_POST['showview']) != "") ? htmlspecialchars($_POST['showview']) : TRUE;

		error_log("[payment24_pay_result] post data : ".json_encode($_POST));

		if($errcode=='00'){
			$ret = $this->payment24_pay_success("sync",$showview);	  // sync=同步

			  if($showview == 'TRUE'){
				  if($ret=='0000'){
					  echo '<script>alert("交易成功!");window.location = "/site/member?channelid=1"</script>';
				  }else{
					  echo '<script>alert("交易失敗 : ['.$ret.']");window.location = "/site/member?channelid=1"</script>';
				  }
			  }else{
				  echo $ret;
			  }

		}else{
		    // $this->twcreditcard_pay_failed($showview);
		    $this->payment24_pay_failed($showview);
		}

		exit;
	}


	/*
	 *	超商代收（條碼）交易結果回傳成功
	 */
	public function payment24_pay_success($callType="async", $showview) {   // async=異步

		global $tpl, $config, $deposit, $user;
		set_status($this->controller);

		error_log("[payment24_pay_success] SendType=".$callType);

		$result_array=array();
		foreach($_POST as $key => $value){
			error_log("[payment24_pay_success] ${key} = ${value}");
			$result_array[$key]=(string)$value;
		}
		error_log("[payment24_pay_success] result_array = ".json_encode($result_array));

		if(!empty($result_array['Td'])){

			// 比對訂單編號是否有前綴sbo如果是移除前綴
			if(preg_match("/SJO/i", $result_array['Td'])){//有前綴
			  $result_array['Td'] = str_replace("SJO","",$result_array['Td']);
			}

			error_log("[payment24_pay_success] Td=".$result_array['Td']);

			$get_deposit_history=$deposit->get_deposit_history($result_array['Td']);
			if($get_deposit_history){
				$userid=$get_deposit_history[0]['userid'];
				$orderid=$get_deposit_history[0]['dhid'];
				$depositid=$get_deposit_history[0]['depositid'];
				error_log("[payment24_pay_success] userid:".$userid.", orderid:".$orderid." depositid:".$depositid);

				if(!empty($depositid)){
					$get_deposit=$deposit->get_deposit_id($depositid);

					if($get_deposit){
						$amount= round(floatval($get_deposit[0]['amount']));
						error_log("[payment24_pay_success]amount:".round($get_deposit[0]['amount'])."==>".$amount);
						$TransID=$result_array['buysafeno']; //紅陽交易編號
						if($TransID==''){
							$TransID=$result_array['BuySafeNo'];//（相容信用卡交易參數）
						}

						$my_chkvalue = $this->getChkValue($result_array['web'] . $config['payment']['code'] . $result_array['buysafeno'] . $result_array['MN'] . $result_array['errcode'] . $result_array['CargoNo']);
						error_log("[payment24_pay_success] My ChkValue=".$my_chkvalue);

						if($my_chkvalue==$result_array['ChkValue']){
							// 驗證ok, 修改訂單及加入贈送
							$switch=$get_deposit[0]['switch'];
							$status=$get_deposit_history[0]['status'];
							$spointid=$get_deposit_history[0]['spointid'];
							$driid=$get_deposit_history[0]['driid'];
							$nickname=$get_deposit_history[0]['modifiername'];
							error_log("[payment24_pay_success] orderid: ".$orderid."/status: ".$status."/switch: ".$switch);

							if($orderid && $switch == 'N'){
								// 將json形式的資訊轉為Associated array
								$arr_data=json_decode($get_deposit_history[0]['data'],true);
								// 添加資訊到data欄位
								$arr_data=$result_array;

								if($callType=="sync"){
									$arr_data['modifierid']=$userid;
									$arr_data['modifiername']=$nickname;
									$arr_data['modifiertype']="User";
								}else if($callType=="async"){
									$arr_data['modifierid']="0";
									$arr_data['modifiername']="payment24_pay_success";
									$arr_data['modifiertype']="System";
								}

								$deposit->set_deposit_history($arr_data, $get_deposit_history[0], $config['payment']['paymenttype']);

								if($result_array['errcode']=='00'){  // 不管異步同步, 回傳errorcode='00' 才表示成功
									$deposit->set_deposit($depositid);
									$deposit->set_spoint($spointid);
									$get_deposit_rule_item = $deposit->get_deposit_rule_item($driid);
									$deposit_conut = $deposit->get_Deposit_Count($userid);
									error_log("[c/deposit/payment24_pay_success] driid : ".$driid);
									if(!empty($get_deposit_rule_item[0]['driid'])){
										$get_scode_promote = $deposit->get_scode_promote_rt($driid);
										//重機首充儲值送活動，活動內容寫死 20190910
										if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00'  && $deposit_conut['num'] == 1){
											switch($driid){
												//300
												case 55:
												case 107:
													$cnt = 1;
												break;
												//2000
												case 56:
												case 173:
													$cnt = 7;
												break;
												//10000
												case 59:
												case 110:
													$cnt = 34;
												break;
												//30000
												case 60:
												case 111:
													$cnt = 101;
												break;
												//50000
												case 176:
												case 182:
													$cnt = 167;
												break;
												//100000
												case 179:
												case 185:
													$cnt = 334;
												break;
											}
											for($i = 0; $i < $cnt; $i++){
												$sv['productid'] = 10044;
												$sv['spid'] = 0;
												$deposit->add_oscode($userid, $sv);
											}
										}else{
											if(!empty($get_scode_promote)){
												foreach($get_scode_promote  as $sk => $sv){
													if(!empty($sv['productid']) && $sv['productid'] > 0){
														for($i = 0; $i < $sv['num']; $i++){
															$deposit->add_oscode($userid, $sv);
														}
													}
												}
											}
											$ret="0000";
											//判斷是否第一次充值
											if($deposit_conut['num'] == 1){

												$get_scode_promote = $deposit->get_scode_promote_rt($driid);
												if(!empty($get_scode_promote)){
													$deposit_times = $deposit->getDepositTimes();
													error_log("[payment24_pay_success] times: ".json_encode($deposit_times));

													if ($deposit_times['times'] > 1) {
														//首儲送的倍數跑迴圈
														for($t = 0; $t < (int)$deposit_times['times']-1; $t++){
															if ($t < (int)$deposit_times['times']) {
																foreach($get_scode_promote  as $sk => $sv){
																	if(!empty($sv['productid']) && $sv['productid'] > 0){
																		for($i = 0; $i < $sv['num']; $i++){
																			$deposit->add_oscode($userid, $sv);
																		}
																	}
																}
															}
														}
													}
												}

											}
										}
										// 首次儲值送推薦人2塊
										// $this->add_deposit_to_user_src($userid);
									}

									// 開立電子發票
									$Invoice = $this->createInvoice($depositid, $userid);

									// 入帳時推播通知用戶
									$inp=array();
									$inp['title']="殺價幣入帳通知";
									$inp['body']="您先前繳費購買的殺價幣已入帳, 請至殺友專區查看~ ";
									$inp['productid']="";
									$inp['action']="4";
									$inp['url_title']="";
									$inp['url']="";
									$inp['sender']=99;
									$inp['sendto']=$userid;
									$inp['page']=9;
									$inp['target']='saja';
									$rtn=sendFCM($inp);

									$ret="0000";

								}

							}else{
								error_log("Duplicated Update, do nothing !!");
								$ret="0000";
							}

						}else{
							$ret="Data Consistency Check Error !!";
						}
					}

				}else{
					$ret="Empty deposit id of ".$orderid." !!";
				}

			}else{
				$ret="Cannot find the data of ".$orderid." !!";
			}

		}else{
			$ret="Empty order id !!";
		}

		error_log($ret);

		if($callType=="async"){
			echo "0000";
			exit;
		}else{
			// if $callType="sync"
			return $ret;
		}

	}

   public function testFCM() {
	       global $tpl, $config, $deposit, $user;
		   set_status($this->controller);
		   $inp=array();
			$inp['title']="殺價幣入帳通知";
			$inp['body']="您先前繳費購買的殺價幣已入帳, 請至殺友專區查看~ ";
			$inp['productid']="";
			$inp['action']="4";
			$inp['url_title']="";
			$inp['url']="";
			$inp['sender']=99;
			$inp['sendto']=1705;
			$inp['page']=9;
			$inp['target']='saja';
			$rtn=sendFCM($inp);
			echo json_encode($inp);
	}

	/*
	 *	超商代收（條碼）交易結果回傳失敗
	 */
	public function payment24_pay_failed($showview) {

		$errmsg=urldecode($_POST['errmsg']);

		if($ShowView == 'TRUE'){
			echo '<script>alert("儲值失敗['.$errmsg.']");</script>';
			return;
		}else{
			//紅陽app異步回傳
			echo "0000";
			return;
		}

	}


	public function is_house_promote_effective($productid) {

		global $config, $deposit;
		$ret=false;

		// 是否為編號2854產品
		error_log("[is_house_promote_effective] productid : ".$productid);
		if($productid!='2854') {
			$ret=false;
			return $ret;
		}

		// 人數是否已滿
		$cnt=$deposit->countPassphrase($productid,'','Y');
		if($cnt){
			error_log("[is_house_promote_effective] now total :".$cnt['now_total']);
			if($cnt['now_total']<33333) {
				$ret=true;
			}else if($cnt['now_total']>=33333){
				$ret=false;
			}
		}else{
			// query error
			error_log("[is_house_promote_effective] empty passphrase :".$productid);
			$ret=false;
		}
		error_log("[is_house_promote_effective] Effective :".$ret);
		return $ret;

	}


	public function register_house_promote($productid,$userid) {

		global $config, $deposit;

		$cnt=$deposit->countPassphrase($productid,$userid,'');

		if($cnt){
			$user_now_total=$cnt['now_total'];
			error_log("[register_house_promote] now total :".$cnt['now_total']);
		}
		// 是否有啟動
		if($user_now_total>0){
			error_log("[register_house_promote] update passphrase");
			$deposit->updatePassphrase($productid,$userid,'Y');
		}else{
			error_log("[register_house_promote] create passphrase");
			$deposit->createPassphrase($productid,$userid,'Y','','');
		}
		return;

	}


	// usage http://www.shajiawang.com/site/deposit/add_oscode_to_user_test?uid=&pid=&num=&spid=&behav=
	public function add_oscode_to_user_test() {

		global $config, $deposit;

		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
			$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
			$ip = $temp_ip[0];
		}else{
			$ip = $_SERVER['REMOTE_ADDR'];
		}

		if(strpos($ip,'61.219.220.')==false){
			die('You have no permission to add !!');
			exit;
		}

		for($i=0;$i<$_GET['num'];++$i){
			$deposit->add_oscode2($_GET['uid'], $_GET['pid'], $_GET['spid'],  $_GET['behav']);
		}

		error_log("add ".$_GET['num']." oscodes to userid : ".$_GET['uid'].", spid:".$_GET['spid'].",behav:".$_GET['behav']);
		die("add ".$_GET['num']." oscodes to userid : ".$_GET['uid'].", spid:".$_GET['spid'].",behav:".$_GET['behav']);

	}


	// 充值成功時送推薦人及TA殺價券 (只一次)
	public function add_oscode_to_user_src($userid, $productid, $num) {

		global $config, $deposit;

		$passphrase=$deposit->getPassphraseInfo($productid,$userid,'Y');

		if($passphrase!=false){

			// TA 充值滿500,再送6張給TA
			if($passphrase['userid']>0){
				$oscode_gived_to_ta=$deposit->get_oscode('160',$passphrase['userid'], 2854);
				error_log("[c/deposit]oscode_gived_to_ta: ".$oscode_gived_to_ta['cnt']);
				if($oscode_gived_to_ta['cnt']==0){
					for ($i = 0; $i < 6; $i++){
						 $deposit->add_oscode2($passphrase['userid'], $productid, '160', 'b');
					}
				}
			}

			// TA 充值滿500, 另外送3張給推薦人
			if($passphrase['user_src']>0){
			  $oscode_gived_to_src=$deposit->get_oscode('161',$passphrase['user_src'],2854);
				error_log("[c/deposit]oscode_gived_to_src: ".$oscode_gived_to_src['cnt']);
				if($oscode_gived_to_src['cnt']==0){
				  for($i = 0; $i < 3; $i++){
						$deposit->add_oscode2($passphrase['user_src'], $productid, '161', 'c');
					}
				}
			}

		}

		return ;

	}


	/*
	 *	儲值規則項目清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$drid		int			儲值規則編號
	 *	$driid		int			儲值規則項目編號
	 */
	public function getRuleItemList() {

		global $config, $tpl, $rule, $deposit;

		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid	 = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid	 = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);

		//設定 Action 相關參數
		set_status($this->controller);

		//取得儲值清單資料
		$rule_list = $rule->get_Rule_List($drid);

		for ($i=0;$i<count($rule_list['record']);$i++){

			//判斷儲值規則資料是否存在
			if(!empty($rule_list['record'][$i]['drid'])){
				// 20160607 暫時跳過銀聯
				if($rule_list['record'][$i]['drid']=='4'){
					unset($rule_list['record'][$i]);
					continue;
				}

				//增加圖示的絕對路徑
				$rule_list['record'][$i]['logo'] = IMG_URL.APP_DIR."/images/site/deposit/".$rule_list['record'][$i]['logo'];
				$rule_list['record'][$i]['banner'] = IMG_URL.APP_DIR."/images/site/deposit/".$rule_list['record'][$i]['banner'];

				//取得儲值規則項目資料
				$rule_item_list = $rule->get_Rule_Item($rule_list['record'][$i]['drid'], $driid);

				foreach ($rule_item_list['record'] as $rk => $rv){
					// 取得項目贈送資料
					$get_scode_promote = $deposit->get_scode_promote_rt($rv['driid']);

					if(!empty($get_scode_promote)){
						foreach ($get_scode_promote as $sk => $sv){
						  $rule_item_list['record'][$rk]['freegoods'][$sk]['sprid'].= $sv['sprid'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['spid'].= $sv['spid'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['amount'].= $sv['amount'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['seq'].= $sv['seq'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['name'].= $sv['name'];
							$rule_item_list['record'][$rk]['freegoods'][$sk]['num'].= $sv['num'];
						}
					}else{
						$rule_item_list['record'][$rk]['freegoods'] = array();
					}
				}

				//判斷儲值規則項目是否存在,加入到儲值規則清單
				$rule_list['record'][$i]['item'] = $rule_item_list['record'];

			}

		}

		//判斷是否為網頁介面
		if($json != 'Y') {
			$tpl->assign('row_list', $rule_list);
		}else{
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "JSON";
			$data['retObj']['data'] = $rule_list['record'];
			echo json_encode($data);
		}

	}


	public function promote_evt() {

		set_status($this->controller);
		$client=$_POST;

		$tpl->assign('client', $client);
		$tpl->set_title('');
		$tpl->render("deposit","promite_evt",true);

	}


	public function confirm_app($drid, $driid, $data, $kind) {

		global $tpl, $config, $deposit, $product, $rule;

		//設定 Action 相關參數
		set_status($this->controller);

		$dhid = 0;
		error_log("[c/deposit/confirm_app] data : ".json_encode($data));

		//新增deposit_history資訊
		$currenty=$config['currency'];
		if($drid=='6' || $drid=='8') {
			$currency="NTD";
		} else {
			$currency="RMB";
		}

		//取得儲值規則項目資料
		$rule_item_list = $rule->get_Rule_Item('', $_POST['driid']);
		$get_deposit['amount'] = floatval($rule_item_list['record'][0]['amount']);
		$get_deposit['spoint'] = $rule_item_list['record'][0]['spoint'];

		if ($kind == "weixin"){
			$get_deposit['amount'] = floatval($data['total_fee']);
			$get_deposit['spoint'] = $data['total_fee'];
		}

		$userid = empty($_POST['auth_id']) ? $data['userid'] : htmlspecialchars($_POST['auth_id']);

		$depositid = $deposit->add_deposit($userid, $get_deposit['amount'], $currency);
		$spointid = $deposit->add_spoint($userid, $get_deposit['spoint']);
		$dhid = $deposit->add_deposit_history($userid, $driid, $depositid, $spointid);

		return $dhid;
	}


	// 小程式用 獲取prepay_id
	public function getWxUnifiedOrder() {

		global $config;

		error_log("[c/deposit/getWxUnifiedOrder] input data : ".json_encode($_REQUEST));
		$userid=$_REQUEST['userid'];
		$productid=$_REQUEST['productid'];
		$body=$_REQUEST['body'];
		$total_fee=$_REQUEST['total_fee']*100;
		$total=$_REQUEST['total_fee'];
		$openid=$_REQUEST['openid'];
		$kind="weixin";
		$trade_type='JSAPI';
		$notify_url=BASE_URL.APP_DIR."/deposit/weixinpay_app";

		// 取得訂單號
		$out_trade_no = $this->confirm_app(5,0,$_REQUEST,$kind);

		$ret="";
		$tools = new JsApiPay();
		try {
			$input = new WxPayUnifiedOrder();
			$input->SetBody($body);
			$input->SetOut_trade_no($out_trade_no);
			$input->SetTotal_fee($total_fee);
			$input->SetNotify_url($notify_url);
			$input->SetTrade_type($trade_type);
			$input->SetOpenid($openid);

			$order = WxPayApi::unifiedOrder($input);
			error_log("[c/deposit/getWxUnifiedOrder] order data : ".json_encode($order));

			$ret = $tools->GetJsApiParameters($order);
		}catch (Exception $e){
			error_log($e->getMessage());
		}finally{
			error_log("[c/deposit/getWxUnifiedOrder] data for wxpay : ".$ret);
			echo $ret;
		}

	}


	// 取得充值贈品List及dhid
	public function getDepositConfirmData() {

		global $config, $deposit, $rule;

		login_required();

		$drid = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);

		// 取得dhid
		$dhid = $this->confirm_app($drid,$driid);
		error_log("[c/deposit/getDepositConfirmData] Get dhid : ".$dhid);

		if(!$dhid){
			echo json_encode(getRetJSONArray(-2,'系統忙碌中,請稍候再試!!','MSG'));
			exit;
		}

		$code = 1;
		$msg = "OK";

		//充值方式為微信則傳值至微信
		if ($drid == 5){
			//取得儲值規則項目資料
			$rule_item_list = $rule->get_Rule_Item('', $driid);
			//隨機產生變數字串
			$str=$this->getWXNonceStr(32);
			//組合xml格式內容
			$notify_url = BASE_URL.APP_DIR."/deposit/weixinpay_app";

			$out_trade_no = $dhid;
			$total_fee = $rule_item_list['record'][0]['amount']*100;
			if($_SESSION['auth_id']=='116')
				$total_fee = 1;
			$body = $rule_item_list['record'][0]['description'];

			$stringA = "appid=wxa8ae20850962b5e0&body=".$body."&mch_id=1340460501&nonce_str=".$str."&notify_url=".$notify_url."&out_trade_no=".$out_trade_no."&spbill_create_ip=121.40.19.35&total_fee=".$total_fee."&trade_type=APP";
			$stringSignTemp = $stringA."&key=8d806a7a258bab43991bdf637568a7e1";
			error_log("[c/deposit/getDepositConfirmData] String For Uniformorder : ".$stringSignTemp);
			$sign = strtoupper(MD5($stringSignTemp));

			$xml = '<?xml version="1.0" encoding="UTF-8"?>
						<xml>
							<appid>wxa8ae20850962b5e0</appid>
							<body>'.$body.'</body>
							<mch_id>1340460501</mch_id>
							<nonce_str>'.$str.'</nonce_str>
							<notify_url>'.$notify_url.'</notify_url>
							<out_trade_no>'.$out_trade_no.'</out_trade_no>
							<spbill_create_ip>121.40.19.35</spbill_create_ip>
							<total_fee>'.$total_fee.'</total_fee>
							<trade_type>APP</trade_type>
							<sign>'.$sign.'</sign>
						</xml>';

			//XML送出路徑
			$url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';

			$post_data = array(
								"xml" => $xml,
							);

			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
							'method' => "POST",
							'content' => $xml,
							'timeout' => 5,
							'header' => "Content-Type: text/xml; charset=utf-8"
							)
			);

			$context  = stream_context_create($stream_options);
			//送出XML內容並取回結果
			$response = file_get_contents($url, null, $context);
			//讀取xml內容
			$xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);

			//加入dhid至物件中
			$xml->dhid = $dhid;

			// Test
			$xml->nonce_str =$this->getWXNonceStr(32);
			$trspay = $this->app_payment($xml->nonce_str, $xml->prepay_id);

			if(!empty($trspay)){
				$xml->sign = $trspay['sign'];
				$xml->timestamp = $trspay['timestamp'];
			}

			//依回傳結果設定回傳至APP的訊息
			if ($xml->return_code == 'SUCCESS'){
				if($xml->result_code == 'SUCCESS'){
					$code = 1;
					$msg = "OK";
				}else{
					$code = $config['weixinpay']['errorcode'][$xml->err_code];
					$msg = $xml->err_code_des;
				}
			}else{
				$code = -3;
				$msg = $xml->return_msg;
			}

			//轉換xml格式
			$xml = json_decode(json_encode($xml));

		}

		$ret=getRetJSONArray($code, $msg,'JSON');
		$ret['retObj']=array();
		//判斷是否為weixin支付
		if ($drid == 5){
			$ret['retObj'] = $xml;
		}else{
			$ret['retObj']['dhid']=$dhid;
		}
		error_log("[c/deposit/getDepositConfirmData] return : ".json_encode($ret));
		echo json_encode($ret);
		exit;

	}


	// 取得支付寶支付(appbsl)
	public function getAliPayOrderForWebApp() {

		global $config, $deposit, $rule;
		error_log("[getAliPayOrderForWebApp] POST IN : ".json_encode($_POST));
		error_log("[getAliPayOrderForWebApp] GET IN : ".json_encode($_GET));
		login_required();

		include_once(LIB_DIR."/alipay/sdk/AopSdk.php");

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$drid 		= empty($_POST['drid'])?$_GET['drid']:$_POST['drid'];
		$driid 		= empty($_POST['drid'])?$_GET['driid']:$_POST['driid'];
		$bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
		$bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
		$type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$total		= empty($_POST['total'])?$_GET['total']:$_POST['total'];
		$autobid 	= empty($_POST['autobid'])?$_GET['autobid']:$_POST['autobid'];
		$productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];
		$userid		= empty($_SESSION['auth_id'])?$_REQUEST['userid']:$_SESSION['auth_id'];
		$chkStr     = $_REQUEST['chkStr'];

		//賣家支付寶帳戶
		$seller_email = $_POST['WIDseller_email'];

		//商戶訂單號
		$out_trade_no = $_POST['WIDout_trade_no'];

		//訂單名稱
		$subject = $_POST['WIDsubject'];

		//付款金額
		$total_fee = $_POST['WIDtotal_fee'];

		$passback_params=array();
		$passback_params['orderid']=$out_trade_no;
		$passback_params['bida']=$bida;
		$passback_params['bidb']=$bidb;
		$passback_params['type']=$type;   // pay_type (single or lazy ...)
		$passback_params['autobid']=$autobid;
		$passback_params['productid']=$productid;
		$passback_params['userid']=$userid;
		$passback_params['pay_amount']=$total_fee;
		$passback_params['chkStr']=$chkStr;

		$biz_content=array(true);
		$biz_content['body']='殺價王-下標'.$total_fee.' 元';
		$biz_content['out_trade_no']=$out_trade_no;
		$biz_content['subject']=$subject;
		$biz_content['seller_email']=$seller_email;
		$biz_content['total_amount']=$total_fee;
		$biz_content['timeout_express']='5m';
		$biz_content['product_code']='QUICK_MSECURITY_PAY';
		$biz_content['passback_params']=urlencode(json_encode($passback_params));


		$aop = new AopClient;
		// Production
		$aop->appId="2016050401363208";
		$aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
		$aop->rsaPrivateKeyFilePath = LIB_DIR.'/alipay/gen_Alipay_private_key_2048.pem';
		$aop->signType= "RSA2";

		$request = new AlipayTradeAppPayRequest();
		$request->setBizContent(json_encode($biz_content));
		$request->setNotifyUrl(BASE_URL.APP_DIR."/lib/alipay/webapp_notify_url.php");
		$response = $aop->sdkExecute($request);
		if(!$response) {
			$ret=getRetJSONArray('0', 'FAILED','MSG');
			echo trim(json_encode($ret));
			exit;
		}
		$ret=getRetJSONArray('1', 'OK','JSON');
		$ret['retObj']=$response;
		error_log("[getAliPayOrderForWebApp] return : ".json_encode($ret));
		echo trim(json_encode($ret));
		exit;

	}


	// 取得微信支付預訂單資料(appbsl)
	public function getWxUnifiedOrderForWebApp() {

		global $config, $deposit, $rule;

		error_log("[getWxUnifiedOrderForWebApp] POST IN : ".json_encode($_POST));
		error_log("[getWxUnifiedOrderForWebApp] GET IN : ".json_encode($_GET));

		login_required();
		include_once(LIB_DIR."/weixinpay/WxPayHelper/WxPayHelper.php");

		$prodid=$_REQUEST['productid'];
		$bida=$_REQUEST['bida'];
		$bidb=$_REQUEST['bidb'];
		$autobid=$_REQUEST['autobid'];
		$drid = $_REQUEST['drid'];
		$driid = $_REQUEST['driid'];
		$amount=0;
		$total_fee=0;
		$body ='';
		$dhid=$_REQUEST['orderid'];
		$notify_url = BASE_URL.APP_DIR."/deposit/weixinpay_app";

		if($autobid=='Y'){
			$notify_url=BASE_URL.APP_DIR.'/product/sajatopay';
		}

		$code = 1;
		$msg = "OK";

		// 有傳orderid , 則用傳入的orderid, 否則另外生一個dhid當作orderid
		$dhid=$_REQUEST['orderid'];
		if(empty($dhid)){
			$dhid = $this->confirm_app($drid,$driid);
			error_log("[c/deposit/getWxUnifiedOrderForWebApp] Get orderid : ".$dhid);
		}
		if(!$dhid){
			echo json_encode(getRetJSONArray(-2,'系統忙碌中,請稍候再試!!','MSG'));
			exit;
		}

		//有傳$driid  則依$driid取得儲值規則項目中的金額以及敘述
		if(!empty($driid)){
		   $rule_item_list = $rule->get_Rule_Item('', $driid);
			if($rule_item_list['record'][0]){
				$amount=$rule_item_list['record'][0]['amount'];
				$total_fee = $rule_item_list['record'][0]['amount']*100;
				$body = ($rule_item_list['record'][0]['description']);
			}
		}else{
			$amount = $_REQUEST['amount'];
			$total_fee = $_REQUEST['amount']*100;
			$body = ($_REQUEST['body']);
		}
		if(empty($body)){
			$body=('殺價王-下標 : '.$amount.' 元');
		}
		if($_SESSION['auth_id']==116 || $_SESSION['auth_id']==28 || $_SESSION['auth_id']==1777 || $_SESSION['auth_id']==9){
			$amount=0.01;
			$total_fee=1;
		}
		$secretid=md5($dhid.$amount);
		error_log("[getWxUnifiedOrderForWebApp] orderid:${dhid} ,amount: ${amount} ,secretid:${secretid}");

		// 使用統一支付介面，獲取prepay_id
		$ufo = new UnifiedOrder(array("appid"=>"wxa8ae20850962b5e0", "mchid"=>"1340460501","key"=>"8d806a7a258bab43991bdf637568a7e1"));
		$ufo->setParameter('notify_url',$notify_url);
		$ufo->setParameter('out_trade_no',$dhid);
		$ufo->setParameter('total_fee',$total_fee);
		$ufo->setParameter('body',$body);
		$ufo->setParameter('trade_type','APP');

		$response=$ufo->getResult();
		error_log("[lib/weixinpay/webapp_call] response:".json_encode($response));

		if($response['return_code']=='SUCCESS'){
			if($response['result_code']=='SUCCESS'){
				$code = 1;
				$msg = "OK";
				$response['nonce_str'] =$this->getWXNonceStr(32);
				$response['amount'] = "".$amount;
				$response['orderid'] = "".$dhid;
				$response['secretid'] = "".$secretid;
				$trspay = $this->app_payment($response['nonce_str'],$response['prepay_id']);
				if(!empty($trspay)){
					$response['sign'] = $trspay['sign'];
					$response['timestamp'] = $trspay['timestamp'];
				}
			}else{
				$code = $config['weixinpay']['errorcode'][$response['err_code']];
				$msg = $response['err_code_des'];
				$response['prepay_id']='';
				$response['timestamp']='';
			}
		}
		$ret=getRetJSONArray($code, $msg,'JSON');
		$ret['retObj']=$response;
		error_log("[c/deposit/getWxUnifiedOrderForWebApp] return : ".json_encode($ret));
		echo trim(json_encode($ret));
		exit;

	}


	function app_payment($nonce_str, $prepay_id, $appid=''){

		$str = $nonce_str;
		$package = "Sign=WXPay";
		$prepayid = $prepay_id;
		$timestamp = "".time();
		$payment = "";

		if(empty($appid))
		   $appid="wxa8ae20850962b5e0";

		if ((!empty($nonce_str)) && (!empty($prepay_id))){
			$stringA = "appid=".$appid.
					   "&noncestr=".$str.
					   "&package=".$package.
						   "&partnerid=1340460501".
						   "&prepayid=".$prepayid.
						   "&timestamp=".$timestamp;
			$stringSignTemp = $stringA."&key=8d806a7a258bab43991bdf637568a7e1";
			error_log("[c/deposit/app_payment] String For WXPay: ".$stringSignTemp);
			$sign = strtoupper(MD5($stringSignTemp));

			$payment['sign'] = $sign;
			$payment['timestamp'] = $timestamp;
		}

		return $payment;

	}


	public function getWXNonceStr($len=32) {

		$nstr = "";
		$chars = "abcdefghijklmnopqrstuvwxyz0123456789";

		for( $i = 0; $i < $len; $i++ ){
			$nstr.= substr($chars, mt_rand(0, strlen($chars)-1), 1);
		}

		return $nstr;

	}


	public function AlipayPaySuccessForBid() {

		global $tpl, $config, $deposit, $rule;

		error_log("[AlipayPaySuccessForBid] POST : ".json_encode($_POST));

		$ret=getRetJSONArray('1', 'OK','JSON');
		error_log("[AlipayPaySuccessForBid] return : ".json_encode($ret));
		echo trim(json_encode($ret));

		exit;

	}


	/*
	 *	微信儲值APP
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$driid		int			儲值規則項目編號
	 */
	public function weixinpay_app() {
		global $tpl, $config, $deposit, $rule;

		$input = file_get_contents("php://input"); //接收POST數據
		$xml = simplexml_load_string($input, 'SimpleXMLElement', LIBXML_NOCDATA); //提取POST數據為simplexml對象

		$orderid = (string)$xml->out_trade_no;
		$amount = (string)$xml->total_fee;
		$appid = (string)$xml->appid;
		$mchid = (string)$xml->mch_id;

		if(($appid != 'wxbd3bb123afa75e56') && ($appid != 'wxa8ae20850962b5e0') && ($appid!='wx054ee14ea7e1d53b')){
			$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
							<note>
							<return_code><![CDATA[FAIL]</return_code>
							<return_msg><![CDATA[微信應用ID異常。]</return_msg>
							</note>";
			print_r($myXMLData);
			exit;
		}

		if(($mchid != '10011676') && ($mchid != '1340460501')){
			$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
					<note>
					<return_code><![CDATA[FAIL]</return_code>
					<return_msg><![CDATA[微信商戶號異常。]</return_msg>
					</note>";
			print_r($myXMLData);
			exit;
		}

		$secretid = md5($orderid.$amount);
		error_log("[c/deposit/weixinpay_app] orderid : ".$orderid.", amount : ".$amount.", secretid : ".$secretid);

		//設定 Action 相關參數
		set_status($this->controller);

		if($orderid){

			$weixinpay_array['out_trade_no'] = $orderid;
			$weixinpay_array['amount'] = $amount;

			$get_deposit_history = $deposit->get_deposit_history($weixinpay_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

			error_log("[c/deposit/weixinpay_app] : ".($amount)."<==>".($get_deposit_id[0]['amount']*100));
			if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'N'){
			  // 交易金額比對
				if($amount!=($get_deposit_id[0]['amount']*100)){
					error_log("[c/deposit/weixinpay_app] : ".floatval($amount)."<==>".(floatval($get_deposit_id[0]['amount'])));
					$ret['status'] = 3;
					$myXMLData ="<?xml version='1.0' encoding='UTF-8'?>
						<note>
						<return_code><![CDATA[FAIL]</return_code>
						<return_msg><![CDATA[微信支付-訂單數據異常。]</return_msg>
						</note>";
					error_log("微信支付-訂單數據異常。");
					echo $ret['status'];
					exit;
				}
				$deposit->set_deposit_history($weixinpay_array, $get_deposit_history[0], "weixinpay");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);

				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				$userid=$get_deposit_history[0]['userid'];

				if(!empty($get_scode_promote)){

					foreach($get_scode_promote  as $sk => $sv){

						if($sv['productid'] > 0 && !empty($sv['productid'])){
							if($this->is_house_promote_effective($sv['productid'])){
								// 判定是否符合萬人殺房資格, 符合者送殺價券
								$this->register_house_promote($sv['productid'],$userid);
								for($i = 0; $i < $sv['num']; $i++){
								  $deposit->add_oscode($userid, $sv);
								}
								// 推薦者送殺價券
								$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
								// 不是房子(2854), 就直接送殺價券
								for($i = 0; $i < $sv['num']; $i++){
								   $deposit->add_oscode($userid, $sv);
								}
							}
						}
					}
				}

				//首次儲值送推薦人 2 點殺價幣
				$this->add_deposit_to_user_src($userid);

				//回傳:
				// $ret=getRetJSONArray(1,'OK','LIST');
				// $ret['status'] = 200;
				// $ret['retObj']['dhid'] = $dhid;
				// $ret['retObj']['userid'] = $userid;
				// $ret['retObj']['amount'] = str_replace(",","",(number_format($rule_item_list[0]['amount'], 2)));
				// $ret['retObj']['spoint'] = str_replace(",","",(number_format($rule_item_list[0]['spoint'], 2)));
				// $ret['retObj']['secretid'] = md5($dhid."|".$userid."|".$rule_item_list[0]['spoint']);
				$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
								<note>
								<return_code><![CDATA[SUCCESS]</return_code>
								<return_msg><![CDATA[OK]</return_msg>
								</note>";
				error_log("微信支付交易-成功。");
			}else if($get_deposit_history[0]['dhid'] && $get_deposit_id[0]['switch'] == 'Y'){

				$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
								<note>
								<return_code><![CDATA[FAIL]</return_code>
								<return_msg><![CDATA[微信支付重複充值。]</return_msg>
								</note>";
				error_log("微信支付重複充值。");
			}else{

				$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
								<note>
								<return_code><![CDATA[FAIL]</return_code>
								<return_msg><![CDATA[微信支付充值失敗1。]</return_msg>
								</note>";
				error_log("微信支付充值失敗1。");
			}
		}else{

			$myXMLData = "<?xml version='1.0' encoding='UTF-8'?>
							<note>
							<return_code><![CDATA[FAIL]</return_code>
							<return_msg><![CDATA[微信支付充值失敗2。]</return_msg>
							</note>";
			error_log("微信支付充值失敗2。");
		}

		print_r($myXMLData);
		exit;

	}


	/*
	 *	銀聯儲值APP 同步通知 form bankcomm
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$driid		int			儲值規則項目編號
	 */
	public function bankcomm_app() {

		global $tpl, $config, $deposit;

		set_status($this->controller);

		$notifyMsg = $_POST['notifyMsg'];
		error_log("bankcomm_html from bankcomm : ".$notifyMsg);
		$arr = preg_split("/\|{1,}/",$notifyMsg);

		if(is_array($arr)){
			$merId=$arr[0];
			$orderid=$arr[1];
			$amount=$arr[2];
			$curType=$arr[3];          // ex: CNY
			$platformBatchNo=$arr[4];  // Usually leave blank
			$merPayBatchNo=$arr[5];    // Optional, assign by merchant
			$orderDate=$arr[6];        // ex: 20141015
			$orderTime=$arr[7];        // ex: 095124
			$bankSerialNo=$arr[8];     // ex: C7D367EC
			$tranResultCode=$arr[9];   // ex: 1 [success]
			$feeSum=$arr[10];          // Total fee
			$cardType=$arr[11];
			$bankMono=$arr[12];
			$errMsg=$arr[13];
			$remoteAddr=$arr[14];
			$refererAddr=$arr[15];
			$merComment=base64_decode($arr[16]);
			$signMsg=$arr[17];
		}
		error_log("merComment is : ".$merComment);
		error_log("transResultCode : ".$tranResultCode);
		if($tranResultCode=='1'){
			$bankcomm_array['out_trade_no'] = $orderid;
			$bankcomm_array['trade_no'] =$bankSerialNo;
			$bankcomm_array['amount'] = $amount;
			$bankcomm_array['result'] = $tranResultCode;
			$bankcomm_array['orderDate'] = $orderDate;
			$bankcomm_array['orderTime'] = $orderTime;
			$bankcomm_array['orderMono'] = $merComment;

			$get_deposit_history = $deposit->get_deposit_history($bankcomm_array['out_trade_no']);
			$get_deposit_id = $deposit->get_deposit_id($get_deposit_history[0]['depositid']);

			if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'N'){

				$userid=$get_deposit_history[0]['userid'];
				$deposit->set_deposit_history($bankcomm_array, $get_deposit_history[0], "bankcomm");
				$deposit->set_deposit($get_deposit_history[0]['depositid']);
				$deposit->set_spoint($get_deposit_history[0]['spointid']);
				$get_deposit_rule_item = $deposit->get_deposit_rule_item($get_deposit_history[0]['driid']);
				$get_scode_promote = $deposit->get_scode_promote_rt($get_deposit_rule_item[0]['driid']);

				if(!empty($get_scode_promote)){
					foreach($get_scode_promote  as $sk => $sv){
						if($sv['productid'] > 0 && !empty($sv['productid'])){
							if($this->is_house_promote_effective($sv['productid'])){
								// 判定是否符合萬人殺房資格, 符合者送殺價券
								$this->register_house_promote($sv['productid'],$userid);
								for($i = 0; $i < $sv['num']; $i++){
									$deposit->add_oscode($userid, $sv);
								}
								// 推薦者送殺價券
								$this->add_oscode_to_user_src($userid, $sv['productid'],$sv['amount']/500);
							}else{
								// 不是房子(2854), 就直接送殺價券
								for($i = 0; $i < $sv['num']; $i++){
								   $deposit->add_oscode($userid, $sv);
								}
							}
						}
					}
				}

				error_log("銀聯交易成功。");
				$ret=getRetJSONArray(1,'OK','JSON');
				$ret['retObj']=array();
				$ret['retObj']['dhid'] = $get_deposit_history[0]['dhid'];
				$ret['retObj']['userid'] = $userid;
				$ret['retObj']['amount'] = str_replace(",","",(number_format($get_deposit_rule_item['table']['record'][0]['amount'], 2)));
				$ret['retObj']['spoint'] = str_replace(",","",(number_format($get_deposit_rule_item['table']['record'][0]['spoint'], 2)));
				$ret['retObj']['secretid'] = md5($get_deposit_history['table']['record'][0]['dhid']."|".$userid."|".$get_deposit_rule_item['table']['record'][0]['spoint']);
				echo json_encode($ret);
				exit;
			}else if(!empty($get_deposit_history[0]['dhid']) && $get_deposit_id[0]['switch'] == 'Y'){
				error_log("[deposit] Order id : ".$orderid." has been deposited OK , nothing to do !!");
				$ret=getRetJSONArray(-1,'error','MSG');
				echo json_encode($ret);
				exit;
			}
		}else{
			error_log("銀聯交易失敗。");
			$ret=getRetJSONArray(-1,'error','MSG');
			echo json_encode($ret);
			exit;
		}

		exit;

	}


	/*
	 *	充值成功時送推薦人2點殺價幣
	 *	$userid		int		會員編號
	 */
	public function add_deposit_to_user_src($userid) {

		global $config, $deposit, $user;

		$deposit_conut = $deposit->get_Deposit_Count($userid);

		error_log("[c/deposit/add_deposit_to_user_src] deposit_conut : ".json_encode($deposit_conut));

		//判斷是否第一次充值
		if($deposit_conut['num'] == 1){

			//取得推薦人的會員編號
			$gived_to_src = $user->getAffiliateUser($userid);

			error_log("[c/deposit/add_deposit_to_user_src] gived_to_src : ".json_encode($gived_to_src));

			if(!empty($gived_to_src['intro_by'])){
				$ip = GetIP();
				//贈送2塊給推薦人
				$gived_deposit = $deposit->add_Gived_Deposit($gived_to_src['intro_by'], 2, 'TWD', $ip);

				//取得推薦人的微信openid
				$openid = $user->getUserWeixinOpenID($gived_to_src['intro_by']);

				//判斷推薦人的微信openid是否存在
				if (!empty($openid['uid'])){
					$msg = '恭喜您 有新合格人數產生，已贈送您殺價幣 2 點!!';
					$ret = sendWeixinMsg($openid['uid'], $msg, 'text');
				}
				error_log("[c/deposit/add_deposit_to_user_src] gived_deposit : ".json_encode($gived_deposit));

			}

		}

		return ;

	}


	public function alipayconfirm() {

		global $tpl, $config, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$drid=$_GET['drid'];
		$driid=$_GET['driid'];
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		$tpl->assign('drid',$drid);
		$tpl->assign('driid', $driid);
		$tpl->set_title('');

		$tpl->render("deposit","confirm3",true,'','Y');
	}


	/*
	 *	支付下標手續費選擇
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$bida			int			起始價格
	 *	$bidb			int			結束價格
	 *	$type			varchar		下標類型
	 *	$productid	 	int			商品編號
	 */
	public function paychoose() {

		global $tpl, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
		$bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
		$type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];

		error_log("[c/deposit/paychoose] post data : ".json_encode($_POST));

		if( (!empty($type)) && ($bida > 0) && (!empty($productid)) ){

			//取得殺價商品資料
			$get_product_info = $product->get_info($productid);
			error_log("[c/deposit/paychoose] product:".json_encode($get_product_info));

			$total = getBidTotalFee($get_product_info,$type,$bida,$bida,$bidb);
			//支付資料
			$paydata = array(
								'productid'		=> $productid,										//商品編號
								'bida'			=> $bida,											//起始價格
								'bidb' 			=> $bidb, 											//結束價格
								'type'			=> $type,											//下標類型
								'total'			=> $total,											//總金額
							);

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('paydata', $paydata);
				$tpl->set_title('');
				$tpl->render("deposit","paychoose",true);
			}else{
				$ret = getRetJSONArray(1,"OK",'LIST');
				$ret['retObj']['paydata'] = (!empty($paydata)) ? $paydata : array();
				echo json_encode((object)$ret);
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y') {
				echo"<script>alert('請確認下標資料 !!');history.go(-1);</script>";
			}else{
				$ret = getRetJSONArray(-1,"ERROR",'MSG');
				echo json_encode($ret);
			}

		}

	}


	/*
	 *	當下支付下標手續費
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$bida			int			起始價格
	 *	$bidb			int			結束價格
	 *	$type			varchar		下標類型
	 *	$userid			int			會員編號
	 *	$productid	 	int			商品編號
	 */
	public function onlinepay() {

		global $tpl, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$count	 	= empty($_POST['count'])?$_GET['count']:$_POST['count'];
		$bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
		$bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
		$type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];
		$userid 	= empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		error_log("[c/deposit/onlinepay] post data : ".json_encode($_POST));

		if( (!empty($type)) && ($bida > 0) && (!empty($productid)) ){

			//取得殺價商品資料
			$get_product_info = $product->get_info($productid);
			error_log("[c/deposit/onlinepay] product:".json_encode($get_product_info));

			$get_drid_list = $deposit->row_drid_list();
			error_log("[c/deposit/onlinepay] get_drid_list:".json_encode($get_drid_list));

			$fee = $get_product_info['saja_fee'];

			$total=getBidTotalFee($get_product_info,$type,$bida,$bida,$bidb);
			if($_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='1777' || $_SESSION['auth_id']=='2525') {
			   $total=0.01;
			}
			//支付資料
			$paydata = array(
								'userid'		=> $userid,											//使用者編號
								'productid'		=> $productid,										//商品編號
								'bida'			=> $bida,											//起始價格
								'bidb' 			=> $bidb, 											//結束價格
								'type'			=> $type,											//下標類型
								'count'			=> $count,											//下標數
								'fee'			=> $fee,											//手續費
								'total'			=> $total,											//總金額
							);

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('paydata', $paydata);
				$tpl->assign('get_product_info', $get_product_info);
				$tpl->assign('get_drid_list', $get_drid_list);
				$tpl->set_title('');
				$tpl->render("deposit","onlinepay",true);
			}else{
				$ret = getRetJSONArray(1,"OK",'LIST');
				$ret['retObj']['paydata'] = (!empty($paydata)) ? $paydata : array();
				$ret['retObj']['product'] = (!empty($get_product_info)) ? $get_product_info : array();
				$ret['retObj']['paykind'] = (!empty($get_drid_list)) ? $get_drid_list : array();
				echo json_encode((object)$ret);
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y'){
				echo"<script>alert('請確認下標資料 !!');history.go(-1);</script>";
			}else{
				$ret = getRetJSONArray(-1,"ERROR",'MSG');
				echo json_encode($ret);
			}

		}

	}


	/*
	 *	當下支付下標手續費確認
	 *	$json	 		varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$bida			int			起始價格
	 *	$bidb			int			結束價格
	 *	$type			varchar		下標類型
	 *	$autobid		varchar		自動下標
	 *	$userid			int			會員編號
	 *	$productid	 	int			商品編號
	 */
	public function onlinpay_confirm() {

		global $tpl, $config, $deposit, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		error_log("[onlinpay_confirm] POST IN : ".json_encode($_POST));
		error_log("[onlinpay_confirm] GET IN : ".json_encode($_GET));

		$json 		= empty($_POST['json'])?$_GET['json']:$_POST['json'];
		$drid 		= empty($_POST['drid'])?$_GET['drid']:$_POST['drid'];
		$bida	 	= empty($_POST['bida'])?$_GET['bida']:$_POST['bida'];
		$bidb	 	= empty($_POST['bidb'])?$_GET['bidb']:$_POST['bidb'];
		$type	 	= empty($_POST['type'])?$_GET['type']:$_POST['type'];
		$total		= empty($_POST['total'])?$_GET['total']:$_POST['total'];
		$autobid 	= empty($_POST['autobid'])?$_GET['autobid']:$_POST['autobid'];
		$productid	= empty($_POST['productid'])?$_GET['productid']:$_POST['productid'];
		$userid		= empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];


		if($type == "single"){
			$bidb = 0;
		}

		//支付類型
		$deposit_rule = $deposit->deposit_rule($drid);
		error_log("[c/deposit/onlinepayconfirm] deposit_rule : ".json_encode($deposit_rule));

		//支付資料
		$paydata = array(
							'userid'		=> $userid,											//使用者編號
							'productid'		=> $productid,										//商品編號
							'bida'			=> $bida,											//起始價格
							'bidb' 			=> $bidb, 											//結束價格
							'type'			=> $type,											//下標類型
							'total'			=> $total,											//總金額
							'autobid'		=> $autobid,										//自動下標
						);

		error_log("[c/deposit/onlinepayconfirm] paydata : ".json_encode($paydata));

		//取得儲值點數
		$get_deposit['drid'] = $drid;
		$get_deposit['driid'] = 0;
		$get_deposit['name'] = '下標手續費支付';
		$get_deposit['amount'] = $total;
		$get_deposit['spoint'] = $total;

		if($_SESSION['auth_id']=='9' || $_SESSION['auth_id']=='116' || $_SESSION['auth_id']=='28' || $_SESSION['auth_id']=='1777' || $_SESSION['auth_id']=='2525' ) {
		  $get_deposit['amount'] = 0.01;
		}

		if($drid =='7' || $drid =='9') {
			$get_deposit['drid']=$drid;
		}else if ($drid != '7' && $drid != '9'){
			//新增deposit_history資訊
			$currenty = $config['currency'];
			if($drid=='6' || $drid=='8'){
			  $currency="NTD";
			  $get_deposit['amount'] = $total * 5;
			}else{
			  $currency="RMB";
			}

			//寫入下標儲值資料
			$depositid = $deposit->add_deposit($userid, $get_deposit['amount'],$currency);
			$spointid = $deposit->add_spoint($userid, $get_deposit['spoint']);
			$dhid = $deposit->add_deposit_history($userid, 0, $depositid, $spointid);

			if($drid == 4){
				$banklist=array();
				$banklist['BOCOM']="交通銀行";
				$banklist['CMB'] = "招商銀行";
				$banklist['CCB'] = "建設銀行";
				$banklist['SPDB'] = "浦發銀行";
				$banklist['GDB'] = "廣發銀行";
				$banklist['PSDB'] = "郵政儲蓄銀行";
				$banklist['CIB'] = "興業銀行";
				$banklist['HXB'] = "華夏銀行";
				$banklist['PAB'] = "平安銀行";
				$banklist['BOS'] = "上海銀行";
				$banklist['SRCB'] = "上海農商銀行";
				$banklist['BCCB'] = "北京銀行";
				$banklist['BRCB'] = "北京農商銀行";
				$banklist['CEB'] = "光大銀行";
				$banklist['ICBC'] = "工商銀行(建議使用PC流覽)";
				$banklist['BOCSH'] = "中國銀行(建議使用PC流覽)";
				$banklist['ABC'] = "農業銀行(建議使用PC流覽)";
				$banklist['MBC'] = "民生銀行(建議使用PC流覽)";
				$banklist['CNCB'] = "中信銀行(建議使用PC流覽)";
				$banklist['OTHERS'] = "其它" ;

				$tpl->assign('bank_list',$banklist);
			}

			$get_deposit['ordernumber'] = $dhid;

		}

		//判斷是否為網頁介面
		if($json != 'Y'){
			$chkStr=$dhid."|".$get_deposit['amount'];

			$cs = new convertString();
			$enc_chkStr = $cs->strEncode($chkStr, $config['encode_key'],$config['encode_type']);
			error_log("[c/deposit/onlinepayconfirm] ori chkStr : ".$chkStr);
			error_log("[c/deposit/onlinepayconfirm] encode chkStr ".$enc_chkStr);
			$tpl->assign('paydata', $paydata);
			$tpl->assign('autobid', $autobid);
			$tpl->assign('bida', $bida);
			$tpl->assign('bidb', $bidb);
			$tpl->assign('bidtype', $type);
			$tpl->assign('productid', $productid);
			$tpl->assign('userid', $userid);
			$tpl->assign('total', $total);
			$tpl->assign('deposit_rule', $deposit_rule);
			$tpl->assign('chkStr',$enc_chkStr);
			$tpl->assign('get_deposit', $get_deposit);
			$tpl->set_title('');
			$tpl->render("deposit","onlinepayconfirm",true);
		}else{
			$ret = getRetJSONArray(1,"OK",'LIST');
			$ret['retObj']['paydata'] = (!empty($paydata)) ? $paydata : array();
			$ret['retObj']['chkStr'] = (!empty($chkStr)) ? $chkStr : array();
			$ret['retObj']['deposit'] = (!empty($get_deposit)) ? $get_deposit : array();
			$ret['retObj']['paykind'] = (!empty($deposit_rule)) ? $deposit_rule : array();
			echo json_encode((object)$ret);
		}

	}


	/*
	 *	取得充值贈品List及dhid
	 */
	public function getDepositConfirm() {

		global $config, $deposit, $rule;

		login_required();

		$drid = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);

		$dhid = $this->confirm_app($drid,$driid);
		error_log("[c/deposit/getDepositConfirmData] Get dhid : ".$dhid);
		if(!$dhid){
			echo json_encode(getRetJSONArray(-2,'系統忙碌中,請稍候再試!!','MSG'));
			exit;
		}
		$code = 1;
		$msg = "OK";

		//充值方式為微信則傳值至微信
		if($drid == 5){
			//取得儲值規則項目資料
			$rule_item_list = $rule->get_Rule_Item('', $driid);
			//隨機產生變數字串
			$str=$this->getWXNonceStr(32);
			//組合xml格式內容
			$notify_url = "http://www.saja.com/site/deposit/weixinpay_app";

			$out_trade_no = $dhid;
			$total_fee = $rule_item_list['record'][0]['amount']*100;
			if($_SESSION['auth_id']=='116')
				$total_fee = 1;
			$body = $rule_item_list['record'][0]['description'];

			$stringA = "appid=wxa8ae20850962b5e0&body=".$body."&mch_id=1340460501&nonce_str=".$str."&notify_url=".$notify_url."&out_trade_no=".$out_trade_no."&spbill_create_ip=121.40.19.35&total_fee=".$total_fee."&trade_type=APP";
			$stringSignTemp = $stringA."&key=8d806a7a258bab43991bdf637568a7e1";
			error_log("[c/deposit/getDepositConfirmData] String For Uniformorder : ".$stringSignTemp);
			$sign = strtoupper(MD5($stringSignTemp));

			$xml = '<?xml version="1.0" encoding="UTF-8"?>
						<xml>
							<appid>wxa8ae20850962b5e0</appid>
							<body>'.$body.'</body>
							<mch_id>1340460501</mch_id>
							<nonce_str>'.$str.'</nonce_str>
							<notify_url>'.$notify_url.'</notify_url>
							<out_trade_no>'.$out_trade_no.'</out_trade_no>
							<spbill_create_ip>121.40.19.35</spbill_create_ip>
							<total_fee>'.$total_fee.'</total_fee>
							<trade_type>APP</trade_type>
							<sign>'.$sign.'</sign>
						</xml>';
			//XML送出路徑
			$url = 'https://api.mch.weixin.qq.com/pay/unifiedorder';

			$post_data = array(
								"xml" => $xml,
							);
			//設定送出方式-POST
			$stream_options = array(
				'http' => array (
					'method' => "POST",
					'content' => $xml,
					'timeout' => 5,
					'header' => "Content-Type: text/xml; charset=utf-8"
				)
			);

			$context  = stream_context_create($stream_options);
			//送出XML內容並取回結果
			$response = file_get_contents($url, null, $context);
			//讀取xml內容
			$xml = simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA);

			//加入dhid至物件中
			$xml->dhid = $dhid;
			// Test
			$xml->nonce_str =$this->getWXNonceStr(32);
			$trspay = $this->app_payment($xml->nonce_str, $xml->prepay_id);

			if(!empty($trspay)){
				$xml->sign = $trspay['sign'];
				$xml->timestamp = $trspay['timestamp'];
			}

			//依回傳結果設定回傳至APP的訊息
			if ($xml->return_code == 'SUCCESS'){
				if($xml->result_code == 'SUCCESS'){
					$code = 1;
					$msg = "OK";
				}else{
					$code = $config['weixinpay']['errorcode'][$xml->err_code];
					$msg = $xml->err_code_des;
				}
			}else{
				$code = -3;
				$msg = $xml->return_msg;
			}

			//轉換xml格式
			$xml = json_decode(json_encode($xml));

		}

		$ret=getRetJSONArray($code, $msg,'JSON');
		$ret['retObj']=array();
		//判斷是否為weixin支付
		if($drid == 5){
			$ret['retObj'] = $xml;
		}else{
			$ret['retObj']['dhid']=$dhid;
		}
		error_log("[c/deposit/getDepositConfirmData] return : ".json_encode($ret));
		echo json_encode($ret);

		exit;

	}


	/*
	 *	創建發票單（用途 : 兜成發票編號並回寫充值資料訂單）
	 *	@param int $depositid    未開發票的單號（儲值帳目id）
	 *  @param int $userid    會員編號
	 *  僅開放相同controller內部呼叫
	 *  遠端須透過api createInvoiceViaSign,通過驗簽後 才能內部呼叫此function
	 */
	public function createInvoice($depositid='', $userid='') {

		global $config, $deposit, $invoice;
		/*
		if(empty($userid))
		  $userid = empty($_SESSION['auth_id'])?htmlspecialchars($_REQUEST['userid']):$_SESSION['auth_id'];
		if(empty($depositid))
		  $depositid = htmlspecialchars($_REQUEST['depositid']);
		*/
		$ret = array();
		error_log("[c/deposit/createInvoice] userid:${userid}, depositid:${depositid} ");

		if(empty($userid)) {
			$userid = $_SESSION['auth_id'];
		}
		if(empty($userid)) {
			$ret['retCode']=-7;
			$ret['retMsg']="NO_USERID";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		}
		if(empty($depositid)) {
			$ret['retCode']=-8;
			$ret['retMsg']="NO_DEPOSITID";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		}

		//判斷是否有發票編號
		$depositData= $deposit->getDepositHistoryForInvoice($depositid, $userid);

		if(!$depositData){
			//資料不存在
			$ret = array(
						"retCode" 	=> -1,
						"retMsg" 	=> "資料不存在"
			);
		}else{

			if($depositData['switch'] != 'Y'){
				// 充值是否完成
				$ret = array(
							  "retCode" 	=> -2,
							  "retMsg" 	=> "此筆充值尚未完成"
				);
			}else if ($depositData['invoiceno'] != ''){ //發票號碼是否為空
				//invoiceno不為空值
				$ret = array(
							  "retCode" 	=> -3,
							  "retMsg" 	=> "發票號碼已存在"
				);
			}else{
				//當儲存資料有值時，取modifyt欄位
				$year = date('Y', strtotime($depositData['insertt']));
				$date = date('m', strtotime($depositData['insertt']));

				//取得發票字軌資料
				$invoice_rail = $deposit->getInvoiceRail($year,$date);

				if(!$invoice_rail){
				  //發票字軌資料不存在
				  $ret = array(
								"retCode" 	=> -4,
								"retMsg" 	=> "發票字軌資料不存在"
				  );
			}else{

					// 發票號碼
					// 20181219 : 配號數前面有0的, 轉成integer時會被忽略掉  所以算完之後要再補零到8位
					$invoiceno = $invoice_rail['rail_prefix'] . str_pad(($invoice_rail['seq_start'] + $invoice_rail['used']),8,'0',STR_PAD_LEFT);

					/* 發票資料
					$arrInvoice = array();
					$arrInvoice['invoiceno']=$invoiceno;
					$arrInvoice['invoice_datetime']=date('Y-m-d H:i:s');
					$arrInvoice['buyer_name']=$userid;
					$arrInvoice['userid']=$userid;
					$arrInvoice['random_number']=$random_code ;
					$arrInvoice['total_amt'] =round($depositData['amount']);
					$arrInvoice['sales_amt']=round($depositData['amount']/1.05);
					$arrInvoice['tax_amt'] = round($arrInvoice['total_amt']-$arrInvoice['sales_amt']);
					$arrInvoice['railid']=$invoice_rail['railid'];
					$arrInvoice['donate_mark']="0";
					$arrInvoice['nopban']="";
					$arrInvoice['upload']="N";
					$arrInvoice['carrier_type']="";
					$arrInvoice['carrier_id1']="";
					$arrInvoice['carrier_id2']="";
					$arrInvoice['utype']="S";
					$arrInvoice['is_winprize']="N";
					*/

					$arrInvoice = $invoice->genDefaultInvoiceArr($userid, round($depositData['amount']), '','S');
					$arrInvoice['invoiceno']=$invoiceno;
					$arrInvoice['userid']=$userid;
					$arrInvoice['railid']=$invoice_rail['railid'];
					$arrInvoice['upload']="N";

					$siid=$invoice->addInvoiceData($arrInvoice);

					if($siid>0){
						$arrInvoiceItem = array();
						$arrInvoiceItem['siid']=$siid;
						$arrInvoiceItem['seq_no']=1;
						$arrInvoiceItem['unitprice']= round($depositData['amount']);
						$arrInvoiceItem['quantity']=1;
						$arrInvoiceItem['amount']=$arrInvoiceItem['unitprice']*$arrInvoiceItem['quantity'];
						$arrInvoiceItem['description']="系統使用費";
						$sipid = $invoice->addInvoiceProdItem($arrInvoiceItem);
					}


					error_log("[c/deposit/createInvoice] invoiceno:".$invoiceno."--siid:".$siid."--sipid:".$sipid);
					//將 發票號碼 和 隨機號碼 回寫到表 saja_deposit

					$update_invoice = $deposit->updateDepositHistoryInvoice($userid,$depositid,$invoiceno);

					if($update_invoice != 1){
						//寫入表saja_deposit失敗
						$ret = array(
									  "retCode" 	=> -5,
									  "retMsg" 	=> "寫入失敗"
						);
					}else{
						//將used+1回寫到saja_invoice_rail中
						$update_invoice_rail = $deposit->updateInvoiceRail($invoice_rail['railid'],$invoice_rail['rail_prefix'],'');

						if($update_invoice_rail != 1){
						  //used累加 1失敗
						  $ret = array(
										"retCode" 	=> -6,
										"retMsg" 	=> "累加1失敗"
						  );
						}else{
							//取表invoice_rail中的資料
							$invoice_rail = $deposit->getInvoiceRail($year,$date);

							//如果加1之後檢查used是否等於total, 是則表示號碼用完,將switch改成"E"之後不再使用
							if($invoice_rail['used'] == $invoice_rail['total']){
								$update_invoice_rail_switch = $deposit->updateInvoiceRail($invoice_rail['railid'],$invoice_rail['rail_prefix'],1);
								$ret = array(
									"retCode" 	=> 1,
									"retMsg" 	=> "OK"
								);
							}else{
								$ret = array(
									"retCode" 	=> 1,
									"retMsg" 	=> "OK"
								);
							}
						}
					}
				}
			}
		}

		$ret['retObj']['depositid'] = (!is_null($depositid)) ? $depositid : "";
		$ret['retObj']['invoiceno'] = (!is_null($invoiceno)) ? $invoiceno : "";
		$ret['retObj']['random_code'] = (!is_null($random_code)) ? $random_code : "";
		error_log("[c/deposit/createInvoice]".json_encode($ret));
		return json_encode($ret);

	}


	/*
	 *	驗簽後呼叫開儲值發票
	 *  @param int $userid     會員編號
	 *	@param int $depositid  未開發票的單號（儲值帳目id）
	 */
	public function createInvoiceViaSign() {

		global $config, $deposit, $invoice;
		$ret=array();
		$ret['retCode']=0;
		$ret['retMsg']="NO_DATA";

		$userid = htmlspecialchars($_REQUEST['userid']);
		if(empty($userid)) {
			$ret['retCode']=-1;
			$ret['retMsg']="NO_USERID";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		}
		$depositid = htmlspecialchars($_REQUEST['depositid']);
		if(empty($depositid)) {
			$ret['retCode']=-2;
			$ret['retMsg']="NO_DEPOSITID";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		}
		$sign = htmlspecialchars($_REQUEST['sign']);
		if(empty($sign)) {
			$ret['retCode']=-3;
			$ret['retMsg']="NO_SIGN";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		}
		$ts = htmlspecialchars($_REQUEST['ts']);
		if(empty($ts))
		   $ts=time();
		if(!is_numeric($ts)) {
		   $ret['retCode']=-4;
		   $ret['retMsg']="TS_ERROR";
		   error_log(json_encode($ret));
		   return json_encode($ret);
		   exit;
		}
		$chkOK=false;
		for($t=$ts-5; $t<=$ts+5; ++$t) {
			$genSign=($t."|".$userid."|".$depositid."|sjW333-_@");
			// error_log("[c/deposit/createInvoiceViaSign] chkSign : ".$genSign);
			if($sign==MD5($genSign)) {
				$chkOK=true;
				break;
			}
		}
		if(!$chkOK) {
			$ret['retCode']=-5;
			$ret['retMsg']="SIGN_CHK_FAILED";
			error_log(json_encode($ret));
			return json_encode($ret);
			exit;
		} else {
			return $this->createInvoice($depositid, $userid);
		}
	}


	/*
	 *	接收遠端request, 呼叫內部function加殺價幣
	 */
	function addSajaPoints() {
		global $config, $deposit, $invoice;

		$amount = htmlspecialchars($_REQUEST['amount']);
		$userid = htmlspecialchars($_REQUEST['userid']);
		$tx_data = htmlspecialchars($_REQUEST['tx_data']);
		$drid = htmlspecialchars($_REQUEST['drid']);

		// 如果不開發票  可以塞任何值給這個參數(inv)
		$inv = htmlspecialchars($_REQUEST['inv']);

		// 簽名(非_ARR_ALLOW_IP允許IP傳入  須驗簽)
		$sign = htmlspecialchars($_REQUEST['sign']);

		$ip=GetIP();
		error_log("[c/deposit/addSajaPoints] request from : ".$ip);
		error_log("[c/deposit/addSajaPoints] data : ".$userid."|".$amount."|".$drid."|".$tx_data);
		error_log("[c/deposit/addSajaPoints] inv : ".$inv);

		$arrData = array("retCode"=>0,"retMsg"=>"NO_DATA", "userid"=>"", "amount"=>"");

		if(empty($userid)){
			$arrData['retCode']=-1;
			$arrData['retMsg']="UNKNOWN_USERID";
			echo  json_encode($arrData);
			exit;
		}
		$arrData["userid"]=$userid;

		if($amount<=0){
			$arrData['retCode']=-2;
			$arrData['retMsg']="ERROR_DEPOSIT_AMT";
			echo  json_encode($arrData);
			exit;
		}
		$arrData["amount"]=$amount;

		// sign 格式 md5($userid."|".$amount."|".$timestamp) ;(到秒)
		// 如果非允許ip  就要驗簽
		if(!in_array($ip,$this->_ARR_ALLOW_IP)){
			$arrSignParams=[$userid,$amount,$this->_SALT];
			error_log("[c/deposit] addSajaPoints : ".json_encode($arrSignParams));
			if(chkSign($arrSignParams, $sign)==false){
				$arrData['retCode']=-3;
				$arrData['retMsg']="SIGN_CHECK_FAILED";
				echo  json_encode($arrData);
				exit;
			}
		}

		$driid=99;   // 暫定為非限定金額儲值
		$depositid = $deposit->add_deposit($arrData['userid'], $arrData['amount'],'','Y');
		$spoint=0;
		if($depositid && $depositid>0){
			if($drid>0){
			$arrCond=array("drid"=>$drid, "amount"=>$amount);
			// 該充值金額是否符合儲值項目規定金額
			$dritem = $deposit->getDepositRuleItems($arrCond," driid desc ");
			if(is_array($dritem) && $dritem[0]['driid']>0){
				$driid=$dritem[0]['driid'];
				// 找得到儲值金額對應匯入的殺價幣
				$spoint=$dritem[0]['spoint'];
				// 該充值方式是否有送殺價券活動
				$get_scode_promote = $deposit->get_scode_promote_rt($driid);
				//充值次數
				$deposit_conut = $deposit->get_Deposit_Count($userid);
				//重機首充儲值送活動，活動內容寫死 20190910
				if(date('Y-m-d H:i:s') >= '2019-09-10 10:00:00' && date('Y-m-d H:i:s') < '2019-09-11 22:03:00'  && $deposit_conut['num'] == 1){
					switch($driid){
						//300
						case 55:
						case 107:
							$cnt = 1;
						break;
						//2000
						case 56:
						case 173:
							$cnt = 7;
						break;
						//10000
						case 59:
						case 110:
							$cnt = 34;
						break;
						//30000
						case 60:
						case 111:
							$cnt = 101;
						break;
						//50000
						case 176:
						case 182:
							$cnt = 167;
						break;
						//100000
						case 179:
						case 185:
							$cnt = 334;
						break;
					}
					for($i = 0; $i < $cnt; $i++){
						$sv['productid'] = 10044;
						$sv['spid'] = 0;
						$deposit->add_oscode($userid, $sv);
					}
				}else{
					if(!empty($get_scode_promote)){
						// 送殺價券
						foreach($get_scode_promote  as $sk => $sv){
							if(!empty($sv['productid']) && $sv['productid'] > 0){
								for($i = 0; $i < $sv['num']; $i++){
									$deposit->add_oscode($userid, $sv);
								}
							}
						}

						//判斷是否第一次充值
						if($deposit_conut['num'] == 1){

							$get_scode_promote = $deposit->get_scode_promote_rt($driid);
							if(!empty($get_scode_promote)){

								$deposit_times = $deposit->getDepositTimes();
								error_log("[eccreditcard_pay_success] times: ".json_encode($deposit_times));

								if ($deposit_times['times'] > 1) {
									//首儲送的倍數跑迴圈
									for($t = 0; $t < (int)$deposit_times['times']-1; $t++){
										if ($t < (int)$deposit_times['times']) {
											foreach($get_scode_promote  as $sk => $sv){
												if(!empty($sv['productid']) && $sv['productid'] > 0){
													for($i = 0; $i < $sv['num']; $i++){
														$deposit->add_oscode($userid, $sv);
													}
												}
											}
										}
									}
								}

							}

						}
					}else{
						// 儲值額度對應不到活動
						error_log("[c/deposit/addSajaPoint] No oscode promote for driid : ".$driid);
					}
				}
			}else{
				// 儲值額度非限定金額
				error_log("[c/deposit/addSajaPoint] No deposit rule item for amount : ".$amount);
				$driid=99;
			}
			}else{
				// 未知的儲值方式
				$arrData['retCode']=-3;
				$arrData['retMsg']="UNKNOWN_DRID";
			}

			$arrData['depositid']=$depositid;
			$arrData['driid']=$driid;
			$arrData['status']='deposit';
			$arrData['data']=$tx_data;
			$arrData['inserttype']='User';
			$arrData['insertid']=$userid;
			$arrData['modifierid']=$userid;
			$arrData['modifiertype']='User';
			$arrData['switch']='Y';
			$arrData['src_ip']=GetIP();

			// 如果能從儲值規則項目中找到所加的殺幣額度就用, 如果找不到就用金額/1.05
			// $arrData['spoint']=$spoint>0?$spoint:round($arrData['amount']/1.05));
			// 如果能從儲值規則項目中對應到所加的殺幣額度就用, 如果找不到就用 0 ,寧願事後加, 也得避免多給
			$arrData['spoint']=$spoint;

			if($driid==99){
				// 非限定金額儲值, 不加點也不開發票
				$arrData['spointid']=0;
				$dhid =$deposit->addDepositHistory($arrData);
				$arrData["retCode"]=1;
				$arrData["retMsg"]="DEPOSIT_AMT_NOT_MATCH";
			}else if($driid!=99){

				$spointid = $deposit->add_spoint($arrData['userid'], $arrData['spoint'], 'Y');
				// $spointid = $deposit->add_spoint($arrData['userid'], round($arrData['amount']/1.05), 'Y');
				if($spointid && $spointid>0){
					$arrData['spointid']= $spointid;
					$dhid =$deposit->addDepositHistory($arrData);
					if($dhid && $dhid>0){
						if(empty($inv)){
							$Invoice = $this->createInvoice($depositid, $userid);
							if($Invoice){
								$arrInvoice = json_decode($Invoice,TRUE);
								$arrData["invoiceno"]=$arrInvoice['retObj']['invoiceno'];
							}
						}
						$arrData["retCode"]=1;
						$arrData["retMsg"]="OK";
					}
				}
			}
		}else{
			// 新增儲值紀錄失敗
			$arrData['retCode']=-10;
			$arrData['retMsg']="DEPOSIT_FAILED";
		}

		error_log("[c/deposit/addSajaPoints] : ".json_encode($arrData));
		echo json_encode($arrData);
		return;

	}


	/*
	 *	充值說明頁
	 */
	public function payfaq() {

		global $tpl, $config, $deposit, $user, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid=empty($_GET['drid'])?htmlspecialchars($_POST['drid']):htmlspecialchars($_GET['drid']);
		$driid=empty($_GET['driid'])?htmlspecialchars($_POST['driid']):htmlspecialchars($_GET['driid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];

		//會員卡包資料
		if ($drid == "17") {
			$user_extrainfo_bankname = "Maybank";
			$user_extrainfo_bankpic = "bank_logo/logo-maybank.png";
			$user_extrainfo_text = array("Account name：Bargain Unique sdn bhd", "Account number：514-049-021-451");
			//$user_extrainfo_remark = '<div style="margin: 1rem;"><p>1.注意：請匯款項目<strong style="color: #f13d3d;"><font color="#F13D3D">指定金額。</font></strong></p><p style="margin-top:10px"><font color="#F13D3D"><strong>2.使用外幣購買無法即時入帳，須等待1～3個工作天，敬請見諒！</strong></font></p><p style="margin-top:10px"><a class="a_line" href="https://line.me/R/ti/p/%40fgz1243t">如完成匯款，請洽Line@官方客服</a></p></div>';
			$user_extrainfo_remark = '<div style="margin: 1rem;"><p>1.注意：請匯款項目<strong style="color: #f13d3d;"><font color="#F13D3D">指定金額，且必須備註：殺價王ID＆暱稱</font></strong>。</p><p style="margin-top:10px">2.付款金額/ID/暱稱 錯誤，將無法入帳。</p><p style="margin-top:10px">3.外幣購買，會於三個工作天內完成。</p><p style="margin-top:10px"><a class="a_line" href="https://line.me/R/ti/p/%40fgz1243t">如有問題，請洽Line@官方客服</a></p></div>';

		}else if($drid == "20"){
			$user_extrainfo_bankname = "微信支付";
			$user_extrainfo_bankpic = "wexinpay.jpg";
			$user_extrainfo_text = array();
			// $user_extrainfo_remark = '<div style="margin: 1rem;"><p>1.請使用<strong style="color: #f13d3d;"><font color="#F13D3D">微信支付</font></strong>掃碼器，即可付款成功。</p><p style="margin-top:10px"><font color="#F13D3D"><strong>2.使用外幣購買無法即時入帳，須等待1～3個工作天，敬請見諒！</strong></font></p><p style="margin-top:10px"><a class="a_line" href="https://line.me/R/ti/p/%40fgz1243t">如完成匯款，請洽Line@官方客服</a></p></div>';
			$user_extrainfo_remark = '<div style="margin: 1rem;"><p>1.微信支付時<strong style="color: #f13d3d;"><font color="#F13D3D">必須備註：殺價王ID＆暱稱</font></strong>。</p><p style="margin-top:10px">2.付款金額/ID/暱稱 錯誤，將無法入帳。</p><p style="margin-top:10px">3.外幣購買，會於三個工作天內完成。</p><p style="margin-top:10px"><a class="a_line" href="https://line.me/R/ti/p/%40fgz1243t">如有問題，請洽Line@官方客服</a></p></div>';
		}else if($drid == "23"){
			$user_extrainfo_bankname = "";
			$user_extrainfo_bankpic = "";
			$user_extrainfo_text = array();
			// $user_extrainfo_remark = '<div style="margin: 1rem;"><p>1.請截圖後，使用任一港幣e-Banking掃碼器，即可付款成功。</p><p style="margin-top:10px"><font color="#F13D3D"><strong>2.使用外幣購買無法即時入帳，須等待1～3個工作天，敬請見諒！</strong></font></p><p style="margin-top:10px"><a class="a_line" href="https://line.me/R/ti/p/%40fgz1243t">如完成匯款，請洽Line@官方客服</a></p></div>';
			$user_extrainfo_remark = '<div style="margin: 1rem;"><p>1.e-Banking支付時<strong style="color: #f13d3d;"><font color="#F13D3D">必須備註：殺價王ID＆暱稱</font></strong>。</p><p style="margin-top:10px">2.付款金額/ID/暱稱 錯誤，將無法入帳。</p><p style="margin-top:10px">3.外幣購買，會於三個工作天內完成。</p><p style="margin-top:10px"><a class="a_line" href="https://line.me/R/ti/p/%40fgz1243t">如有問題，請洽Line@官方客服</a></p></div>';

		}else if($drid == "8"){
			$user_extrainfo_bankname =  '';
			$user_extrainfo_bankpic =  '';
			$user_extrainfo_text = array();
			$user_extrainfo_remark = '';
		} else if($drid=="11") {
			$user_extrainfo_remark = '<div style="margin: 1rem; font-family:sans-serif;">
			                          <br>1.繳費後約<font color="red"><b> 5 個工作日</b></font>（銀行遇假日不作業）入帳。</br>
						              <div>';

		}else{
			$user_extrainfo_category = $user->check_user_extrainfo_category();
			if(is_array($user_extrainfo_category)){
				foreach ($user_extrainfo_category as $key => $value){
					if($value['uecid'] == 8){
						$extrainfo = $user->get_user_extrainfo($value['uecid']);
						$user_extrainfo_bankname =  '第一商業銀行';
						$user_extrainfo_bankpic =  'bank_logo/logo-firstbank.png';
						$user_extrainfo_text = array("銀行總代號：007", "個人專屬繳費帳號：".$extrainfo['field1name']);
                        $user_extrainfo_remark = '<div style="margin: 1rem; font-family:sans-serif;">
						                         <br>1.每個帳號擁有各自專屬繳費號碼，請勿匯入他人專屬繳費的帳戶中。</br>';
					    $user_extrainfo_remark.='<br>2.若使用<font color="red"><b>第一銀行「臨櫃」/「實體ATM」</b></font>繳款，請務必使用「<font color="red"><b>繳費</b></font>」功能，其他銀行不在此限。</br>
												 <br>3.金額請輸入「總計」金額，勿自行扣除手續費，以免系統無法自動核銷帳並入帳。</br>
                                                 <br>4.繳款金額與繳款流程操作無誤，系統將於15~20分鐘自動入帳，請至殺價幣獲得紀錄查看。</br>
                                                 <br>5.若操作有誤或繳款金額有誤，則需要人工入帳，請客服Line@saja 並提供您繳費收據與會員編號。</br>
												 <br>6.人工入帳作業處理流程需3~5個工作天不含假日，入帳金額將扣除人工處理手續費每筆殺價幣15元。</div>';
					}
				}
			}
		}

		if($json!='Y'){
			$user_extrainfo['bankname'] = $user_extrainfo_bankname;
			$user_extrainfo['bankpic'] = $user_extrainfo_bankpic;
			$user_extrainfo['text'] = $user_extrainfo_text;
			$user_extrainfo['remark'] = $user_extrainfo_remark;
		}

		//充值項目ID
		$row_list = $deposit->row_list($drid);
		$tpl->assign('row_list', $row_list);

		$deposit_rule = $deposit->deposit_rule($drid);
		$tpl->assign('deposit_rule', $deposit_rule);

		foreach($row_list as $rk => $rv){

			if((int)$rv['driid']==(int)($driid)){
				//取得儲值點數
				$get_deposit['drid'] = $drid;
				$get_deposit['driid'] = $rv['driid'];
				$get_deposit['name'] = $rv['name'];
				$get_deposit['amount'] = floatval($rv['amount']);
				$get_deposit['spoint'] = $rv['spoint'];
				$get_deposit['mark'] = $deposit_rule[0]['mark'];
				$get_deposit['drid_name'] = $deposit_rule[0]['name'];

				$code1 = (!empty($rv['code1']))?$rv['code1']:"";
				$codeformat1 = (!empty($rv['codeformat1']))?$rv['codeformat1']:"";
				$code2 = (!empty($rv['code2']))?$rv['code2']:"";
				$codeformat2 = (!empty($rv['codeformat2']))?$rv['codeformat2']:"";
				$get_deposit['qrcodeformat'] = array(
					array("code"=>$code1 , "type"=>$codeformat1),
					array("code"=>$code2 , "type"=>$codeformat2)
				);

				break;
			}else{
				continue;
			}

		} //endforeach;

		if($drid=='7' || $drid=='9'){ //Hinet & 阿拉訂 : 點數兌換時才生訂單

			$get_deposit['drid']=$drid;

		}else if ($drid != '7' && $drid !='9'){

			//新增deposit_history資訊
			$currenty=$config['currency'];
			if($drid=='6' || $drid=='8' || $drid=='10') {
				$currency="NTD";
			}else{
				$currency="RMB";
			}

			$get_scode_promote = $deposit->get_scode_promote_rt($driid);
			if (!empty($get_scode_promote)){
				$i = 1;
				$spmemo = '';
				$spmemototal = 0;
				$spmemoapp = '';

				// 統計儲值次數
				$deposit_conut = $deposit->get_Deposit_Count($userid);

				//判斷是否第一次充值
				if($deposit_conut['num'] > 0 ){
					$firstdeposit = 'N';
				} else{
					$firstdeposit = 'Y';
				}

				foreach($get_scode_promote as $sk => $sv){
					$get_product_info = $product->get_info($sv['productid']);
					$spmemoapp[$sk]['name'] = "送 殺價券".$get_product_info['name'];

					$spmemo.='<li class="d-flex align-items-center">
					<div class="de_title"><p>送 殺價券</p><p>( '.$get_product_info['name'].' )</p></div><div class="de_point">';
					if(!empty($sv['num'])){
						if ($firstdeposit == 'N'){
							$spmemo.=$sv['num'].' 張';
							$spmemoapp[$sk]['num'] = $sv['num'].' 張';
						}else{
							$spmemo.=($sv['num']*2).' 張';
							$spmemoapp[$sk]['num'] = ($sv['num']*2).' 張';
						}
						$spmemototal = ($spmemototal+($sv['num']*$get_product_info['saja_fee']));
					}

					if(!empty($get_product_info['productid'])){
						$spmemo.='</div></li>';
					}
				  $i++;
				}
			}else{
				$spmemo .= "<li>無贈送任何東西</li>";
				$spmemoapp = array();
				// $spmemoapp[0]['name'] = "無贈送任何東西";
				// $spmemoapp[0]['num'] = " 0 張";
			}
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_deposit;
			$ret['retObj']['spmemo'] = $spmemoapp;
			$ret['retObj']['user_extrainfo_text'] = $user_extrainfo_text;
			$ret['retObj']['user_extrainfo_remark'] = $user_extrainfo_remark;
			$ret['retObj']['user_extrainfo_bankname'] =  $user_extrainfo_bankname;
			$ret['retObj']['user_extrainfo_bankpic'] =  $user_extrainfo_bankpic;
			$ret['retObj']['user_extrainfo'] =  $extrainfo['field1name'];
			$ret['retObj']['user_extrainfo_bankid'] =  '007';
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('drid',$drid);
			$tpl->assign('driid', $driid);
			$tpl->assign('get_deposit', $get_deposit);
			$tpl->assign('get_scode_promote', $get_scode_promote);
			$tpl->assign('spmemo', $spmemo);
			$tpl->assign('spmemototal', $spmemototal);
			$tpl->assign('user_extrainfo', $user_extrainfo);
			$tpl->set_title('');

			if ($deposit_rule[0]['type'] == "2") {
				$tpl->render("deposit","payfaq",true);
			}else if($deposit_rule[0]['type'] == "3"){
				$tpl->render("deposit","payqrcode",true);
			}

		}

	}



	/*
	 *	取得儲值待繳費清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$drid		int			儲值規則編號
	 *	$driid		int			儲值規則項目編號
	 *	$status		int			儲值資料狀態
	 *	$p			int			分頁編號
	 */
	public function getDepositPayList() {

		global $config, $tpl, $rule, $deposit;

		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid	 = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid	 = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);
		$userid  = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$status  = empty($_POST['status']) ? htmlspecialchars($_GET['status']) : htmlspecialchars($_POST['status']);
		$p 		 = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		//取得儲值規則資料
		$rule_data = $deposit->deposit_rule($drid);

		//判斷是否為待繳費項目
		if (!empty($rule_data[0]) && $rule_data[0]['type'] != 5){
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10001,'儲值規則查無資料','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("儲值規則查無資料!!");history.back();</script>');
				exit;
			}
		}

		//取得儲值待繳費項目資料
		$item_list = $deposit->row_list($drid);

		if (empty($item_list)){
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10002,'儲值規則項目查無資料','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("儲值規則項目查無資料!!");history.back();</script>');
				exit;
			}
		}

		foreach($item_list as $rk => $rv){
			$itemdata[$rk] = $rv['driid'];
		}
		$itemid = implode(',', $itemdata);

		//取得儲值待繳費清單資料
		$get_history_list = $deposit->get_deposit_history_by_driid($itemid, $status);

		//判斷是否為網頁介面
		if($json != 'Y') {
			$tpl->assign('row_list', $get_history_list['table']['record']);
			$tpl->set_title('');
			$tpl->render("deposit","paylsit",true);
		}else{
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_history_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_history_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					$ret['retObj']['data'] = $get_history_list['table']['record'];
				}
				$ret['retObj']['page'] = $get_history_list['table']['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;
		}

	}



	/*
	 *	取得儲值待繳費內容
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$drid		int			儲值規則編號
	 *	$driid		int			儲值規則項目編號
	 *	$dhid		int			儲值明細編號
	 */
	public function getDepositPayDetail() {

		global $config, $tpl, $deposit;

		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$drid	 = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid	 = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);
		$userid  = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$dhid	 = empty($_POST['dhid']) ? htmlspecialchars($_GET['dhid']) : htmlspecialchars($_POST['dhid']);

		//設定 Action 相關參數
		set_status($this->controller);

		login_required();

		error_log("[deposit/getDepositPayDetail] drid:${drid}, driid:${driid}, userid:${userid}, dhid:${dhid} ");

		// 比對訂單編號是否有前綴sbo如果是移除前綴
		if(preg_match("/SJO/i", $dhid)){//有前綴
		  $dhid = str_replace("SJO","",$dhid);
		}

		//取得儲值待繳費項目資料
		$get_history = $deposit->get_deposit_history($dhid);


		if (empty($get_history[0])){
			if ($json == 'Y'){
				$ret = getRetJSONArray(-10001,'查無相關儲值資料','json');
				echo json_encode($ret);
				exit;
			}else{
				die('<script>alert("查無相關資料!!");history.back();</script>');
				exit;
			}
		}

		//判斷是否為網頁介面
		if($json != 'Y') {
			$tpl->assign('row_list', $get_history[0]);
			$tpl->set_title('');
			$tpl->render("deposit","paydetail",true);
		}else{
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $get_history[0];
			// 條碼原始字串清除 "*" 符號
			$ret['retObj']['data']['memo'] = str_replace("*","",$ret['retObj']['data']['memo']);
			// Add By Thomas 2019/12/18 條碼繳費的注意事項
			$ret['retObj']['data']['memo']['note0']="<br>1.請至 7-11/全家/萊爾富/OK 便利超商出示條碼並繳費。
                                                     <br>2.繳費完成後‚請向便利超商索取繳費證明單並核對金額 (超商不開立發票)。
                                                     <br>3.如上述條碼無法掃瞄‚ 請改輸入條碼下方英數字。
													 <br>4.因應超商及銀行作業時間, 繳費後約<font color='red' size='1.5rem'><b> 5 個工作日</b></font>入帳。";
			error_log("[c/deposit/getDepositPayDetail] ret : ".json_encode($ret));
			echo json_encode($ret);
			exit;
		}

	}


	/**
	 * 檢查交易檢查碼是否正確（SHA1雜湊值）
	 */
	public function getChkValue($string) {
		return strtoupper(sha1($string));
	}


	//取得交易用通行碼（取自token/passcode範例）
	public function getPasscode($isProduction, $merchantID, $password, $key, $encIV, $userID, $paymentToken, $verificationCode, $tokenExpiryDate, $price) {
		$requestStr = ''; //傳送內容
		$returnStr = ''; //回覆內容
		$passcode = ''; //通行碼

		try {
			$paymentURL = '';
			if ($isProduction) {
				$paymentURL = 'https://www.esafe.com.tw/serviceAPP/Api/Passcode_Request.ashx'; //傳送網址（正式）
			}
			else {
				$paymentURL = 'https://test.esafe.com.tw/serviceAPP/Api/Passcode_Request.ashx'; //傳送網址（測試）
			}

			//傳送欄位設定（請參考技術手冊）
			$timestamp = gmdate('U'); //目前時間Unix Time Stamp (UTC時間)

			$passcodeRequest=array('merchantID'=>'','tokenData'=>'','chkValue'=>'','lang'=>''); //傳入資料（passcode請求）
			$tokenData=array('timestamp'=>'','userID'=>'','paymentToken'=>'','verificationCode'=>'','tokenExpiryDate'=>'','price'=>''); //傳入資料（Token資料）
			$tokenData['timestamp'] = $timestamp;
			$tokenData['userID'] = $userID;
			$tokenData['paymentToken'] = $paymentToken;
			$tokenData['verificationCode'] = $verificationCode;
			$tokenData['tokenExpiryDate'] = $tokenExpiryDate;
			$tokenData['price'] = $price;

			$json = $this->json_encode_fix($tokenData);

			$passcodeRequest['merchantID'] = $merchantID;
			$passcodeRequest['tokenData'] = $this->dataEncrypt($json, $key, $encIV);
			$passcodeRequest['chkValue'] = $this->SHA256Encrypt($merchantID . $password . $json);
			$passcodeRequest['lang'] = 'tw';

			$requestStr = $this->json_encode_fix($passcodeRequest);
			//送出請求
			$curl = curl_init($paymentURL);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-type: application/json"));
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_POSTFIELDS, $requestStr);

			//商店主機端的TLS/SSL協定至少支援TLS 1.1/1.2其中一種
			//OpenSSL需版本1.0.1以上才支援TLS 1.1/1.2
			//curl版本為7.34.0以上時為強制支援TLS 1.1/1.2
			//如有必要時，請使用以下其中一行設定
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_1); //可將CURL_SSLVERSION_TLSv1_1改為數字5
			//curl_setopt($curl, CURLOPT_SSLVERSION, CURL_SSLVERSION_TLSv1_2); //可將CURL_SSLVERSION_TLSv1_2改為數字6

			//執行送出請求
			$returnStr = curl_exec($curl);
			$status = curl_getinfo($curl, CURLINFO_HTTP_CODE);
			//if ( $status != 201 ) {
			//    die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
			//}
			curl_close($curl);

			//解析資料
			if (strpos($returnStr,'passcodeData') > 0) {
				//不用事先定義回傳資料的JSON結構
				$responseData=$this->json_decode_fix($returnStr);

				//檢查回覆是否有錯誤訊息
				if (!isset($responseData->errorMessage)) {
					throw new Exception('回覆格式錯誤');
				}
				elseif ($responseData->errorMessage != "") {
					throw new Exception($responseData->errorMessage);
				}

				//查看tokenData內容
				if ((isset($responseData->passcodeData)) && ($responseData->passcodeData != '')) {

					try {
						$responseData->passcodeData = $this->dataDecrypt($responseData->passcodeData, $key, $encIV);
					} catch (Exception $ex) {
						throw new Exception($ex->getMessage());
					}

					//檢查碼驗證
					if ($responseData->chkValue != $this->SHA256Encrypt($merchantID . $password . $responseData->passcodeData)) {
						throw new Exception('資料驗證錯誤');
					}

					//將responseData.passcodeData物件化以便存取
					//不用事先定義回傳資料$passcodeData的JSON結構
					$passcodeData=$this->json_decode_fix($responseData->passcodeData);

					//物件化後，$passcodeData->passcode即為通行碼，此為使用於交易時的passcode參數，更多參數說明請參考技術手冊
					$passcode = $passcodeData->passcode;

				}
				else {
					throw new Exception('資料錯誤');
				}
			}
			else {
				throw new Exception('回覆格式錯誤');
			}

		} catch (Exception $ex) {
			//發生錯誤，詳情見$ex->getMessage()（建議將錯誤原因儲存，以便了解原因與解決問題）

		}

		return $passcode;
	}


	// AES加密（資料加解密用）
	public function dataEncrypt($plainText, $key, $iv) {
		$outputData='';
		try {
			$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			//padding處理
			$len = strlen($plainText);
			$padding = $block - ($len % $block);
			$plainText .= str_repeat(chr($padding),$padding);

			$key=base64_decode($key);
			$iv=base64_decode($iv);
			$outputData=mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $key, $plainText, MCRYPT_MODE_CBC, $iv);
			if ($outputData!='') {$outputData=base64_encode($outputData);}
		} catch (Exception $ex) {
			//加密異常
			throw new Exception('資料錯誤');
			//return $ex->getMessage(); //回傳錯誤訊息
		}
		return $outputData;
	}


	// AES解密（資料加解密用）
	public function dataDecrypt($encryptedTextData, $key, $iv) {
		$outputData='';
		try {
			$encryptedTextData=base64_decode($encryptedTextData);
			$block = mcrypt_get_block_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC);
			$key=base64_decode($key);
			$iv=base64_decode($iv);

			$outputData=mcrypt_decrypt(MCRYPT_RIJNDAEL_128, $key, $encryptedTextData, MCRYPT_MODE_CBC, $iv);
			if ($outputData=='') {return '';} //解密異常，回傳空值
			//padding處理
			$len=strlen($outputData);
			$pad = ord($outputData[$len - 1]);
			$outputData=substr($outputData, 0, $len-$pad);
		} catch (Exception $ex) {
			//解密異常
			throw new Exception('資料錯誤');
			//return $ex->getMessage(); //回傳錯誤訊息
		}
		return $outputData;
	}


	// SHA256加密
	public function SHA256Encrypt($plainText) {
		return hash('sha256', $plainText, false);
	}


	//JSON解碼
	public function json_decode_fix($json, $assoc = false) {
		$json = str_replace(array("\n","\r"),"",$json);
		$json = preg_replace('/([{,]+)(\s*)([^"]+?)\s*:/','$1"$3":',$json);
		$json = preg_replace('/(,)\s*}$/','}',$json);
		return json_decode($json,$assoc);
	}


	//JSON編碼
	public function json_encode_fix($json) {
		return preg_replace("/\\\\u([a-f0-9]{4})/e", "iconv('UCS-4LE','UTF-8',pack('V', hexdec('U$1')))", json_encode($json));
	}


	public function getPostData($key) {
		return array_key_exists($key, $_POST) ? $_POST[$key] : '';
	}

	// 查詢 $mins 時間內刷卡超過 $limit 的資料
	public function swipe_credit() {
		   global $config, $tpl, $deposit;

		   $mins=htmlspecialchars($_REQUEST['mins']);
		   $userid=htmlspecialchars($_REQUEST['userid']);
		   $limit=htmlspecialchars($_REQUEST['limit']);
		   if(empty($limit))
			  $limit=3;
		   if(empty($mins))
			  $mins=60;
		   $ret = array();
		   $ret['retCode']=1;
		   $ret['retMsg']='OK';
		   $table= $deposit->count_swipe_creditcard($mins, $limit);
		   if($table) {
			  $ret['retObj']=$table['table']['record'];
			  if($userid>0) {
				  foreach($table['table']['record'] as $v) {
					   if($v['userid']=$userid) {
                          $ret['retObj']=$v;
                          break;
                       }
				  }
			  }
		   }
		   echo json_encode($ret);
		   return;
	}

	public function chk_swipe_credit_count($userid, $mins, $limit) {
		   global $config, $tpl, $deposit;

		   if(empty($mins))
		      $mins=htmlspecialchars($_REQUEST['mins']);
		   if(empty($userid))
		      $userid=htmlspecialchars($_REQUEST['userid']);
		   if(empty($limit))
		      $limit=htmlspecialchars($_REQUEST['limit']);

		    $ret =false;
	        $table=$deposit->count_swipe_creditcard($mins,$limit);
			if($table) {
			   // 檢查該用戶是否在名單中
			   foreach($table['table']['record'] as $item) {
				  if($item['userid']==$userid) {
					 error_log("[c/deposit/home] ${userid} swipe ".$item['cnt']." times.");
					 $ret = true;
					 break;
				  }
			   }
			}
            return $ret;
	}

}
