<?php
/*
 * Dream Controller 殺價商品
 */
class Dream {

	public $controller = array();
	public $params = array();
	public $id;

	/*
   * Action Method : home
   */
	public function home() {

    global $tpl, $dream, $channel, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		//商品分類
		$_category = $dream->product_category();
		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$type=$_REQUEST['type'];
		$cb=$_REQUEST['canbid'];

		$viewflag='';
		if(!empty($_REQUEST['viewflag'])){
		  $viewflag=$_REQUEST['viewflag'];
		}

		$category_list = $dream->product_category_list();
		$tpl->assign('category_list', $category_list);


		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['category_list'] = $category_list;
			echo json_encode($ret);
			exit;
		}

		// 是否讓頁面顯示出價欄位
		if($cb!='N'){
          $cb='Y';
        }
		$tpl->assign('canbid', $cb);

		$tpl->assign('noback', 'Y');
		$tpl->set_title('');
		$tpl->render("dream","home",true);

	}


	/*
	 * 設定分頁參數
	 */
	private function set_page($row_list, $page_path){

    $table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';

    foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;

  }


	public function getProdInfo() {

    global $dream;
    //商品資料
    $ret=null;
    $dreamid=$_POST['productid'];

    if(empty($dreamid)){
      $ret=getRetJSONArray(0,'商品編號為空 !!','MSG');
      echo json_encode($ret);
      exit;
    }

    $get_product = $dream->get_info($dreamid);

    if($get_product){
      $get_product['offtime'] = (int)$get_product['offtime'];
      $get_product['now'] = (int)$get_product['now'];
      $ret=getRetJSONArray(1,'OK','JSON');
      $ret['retObj']=$get_product;
    }else{
      $ret=getRetJSONArray(0,'找不到商品資料 !!','MSG');
    }
    echo json_encode($ret);

    exit;

	}


	// test
	public function test(){
		global $tpl, $dream, $scodeModel, $user, $oscode, $bid;
		echo browserType();
	}


	//殺價商品的資料
	public function saja() {

		global $tpl, $dream, $scodeModel, $user, $oscode, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){

			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
			'bid'		=> $bidprice,			//起始價格
			'bida'		=> $bida,				//起始價格
			'bidb' 		=> $bidb,				//結束價格
			'type'		=> $paytype,			//下標類型
			'autobid'	=> $auto,				//自動下標
		);

		$tpl->assign('paydata',$paydata);

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$dreamid=(empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		$behav=$_REQUEST['behav'];

		//用戶可否下標或兌換 AARONFU
		$get_user = $user->get_user($userid);
		$tpl->assign('bid_enable',$get_user['bid_enable']);
		$tpl->assign('type',$type);

		$ret=null;
		if(empty($dreamid) ){
			if($json=='Y'){
				echo json_encode(getRetJSONArray(-3,'商品編號為空 !!','MSG'));
				exit;
			}else{
				return_to('site/dream');
			}
		}else{
			
			/* add By Thomas 2019-10-22 臨時加入
			  disable By Thomas 20191024 17:25
			$today = date("Y-m-d H:i:s");
			if ($today>'2019-10-22 22:33:00' && $dreamid=="12000") {
                $dreamid="12351";
			}
            // add End
            */

            // 增加商品點擊數 By Thomas 20190508
            $dream->add_prod_click($dreamid,1);

			// 商品資料
			$get_product = $dream->getProductById($dreamid);

			if(!$get_product){
                if($json=='Y'){
				  echo json_encode(getRetJSONArray(-4,'找不到商品資料 !!','MSG'));
				  exit;
				}
			}

		    //收藏資料 : 20190508 暫時disable By Thomas
			// $collect = $user->getCollectProdList($userid, $dreamid, 'S', '');
			// $get_product['collect'] = $collect['record'][0];

			$ret=getRetJSONArray(1,'OK','JSON');
            $get_product['use_scode'] = false;
			$msg='';
			// $get_product['offtime'] = (int)$get_product['offtime'];
			$get_product['now'] = (int)$get_product['now'];

			// 抓source IP
			if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$src_ip = $temp_ip[0];
			}else{
				$src_ip = $_SERVER['REMOTE_ADDR'];
			}

            // 抓user_agent
			// $user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			// $is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			//商品同意內容
			$get_product['agree'] = "本人已充分了解並同意「用戶使用規範」、「服務條款」、「下標規則」且同意本站變更、修改或終止該商品內容之權利。";
			
			if($json=='Y'){
                $ret['retObj']=$get_product;
            } else {
                if(!empty($get_product['thumbnail2'])){
				  $meta['image'] = $get_product['thumbnail2'];
                }elseif (!empty($get_product['thumbnail_url'])){
				  $meta['image'] = $get_product['thumbnail_url'];
                }
            }
			$sgift=0;
			if($json=='Y'){

            } else {
                $tpl->assign('sgift_num', $sgift);
            }

			/* Update By Thomas 2015214
			// 如果有經由qrcode過來 就贈送過殺價券
			if(!empty($userid) && !empty($dreamid) && $behav=="q"){
				$oscode_num0 = $scodeModel->get_oscode($dreamid, $userid);
				//查詢是否有贈送殺價券活動
				$scode_promote = $scodeModel->scode_promote($dreamid,'q');
				$info['spid'] = $scode_promote['spid'];
				$info['behav'] = 'qrcode_reg';
				$info['productid'] = $dreamid;
				$info['userid'] = $userid;
				$info['onum'] = $scode_promote['onum'];

                // 如果沒送過 且有活動 就贈送
				if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){
				  for($j = 0;$j < $info['onum']; ++$j){
					  $scodeModel->add_oscode($info);
				  }
				  $scodeModel->set_scode_promote($info);
				  $msg="恭喜您獲得 ".$info['onum']."張 ".$get_product['name']." 殺價券";
				}
				// Add End
			}
			*/

			// 可用殺價券數量
			$oscode_num =  $oscode->get_prod_oscode($dreamid, $userid);
			error_log("[c/product/saja] user: ${userid} available oscode for ".$dreamid." : ".$oscode_num);

            if($json=='Y'){
				$ret['retObj']['oscode_num']=$oscode_num;
			}else{
				$tpl->assign('oscode_num', $oscode_num);
			}

			//S碼 現有可用總數
			$scode_num = $scodeModel->get_scode($userid);
			if($json=='Y'){
				$ret['retObj']['scode_num']=$scode_num;
			}else{
				$tpl->assign('scode_num', $scode_num);
			}

            // S碼 已用總數
            $scode_use_num = abs($scodeModel->used_sum($userid));
            if($json=='Y'){
                $ret['retObj']['scode_use_num']=$scode_use_num;
            }else{
                $tpl->assign('scode_use_num', $scode_use_num);
            }

			//目前得標
			// $bidded=$this->getCurrWinnerInfo($dreamid);
			$bidded = $dream->get_bidded($dreamid);
			$num_bids_gap = $dream->getNumBidsGap($dreamid);

			if($json=='Y'){

                $ret['retObj']['curr_winner_userid']=empty($bidded['userid'])?"":$bidded['userid'];
                $ret['retObj']['bidded']=empty($bidded['nickname'])?"":$bidded['nickname'];

                if($ret['retObj']['curr_winner_userid']==""){
                    $ret['retObj']['bidded']="_無人得標_";
                    $ret['retObj']['final_bid_price']=0;
                } else {
                    preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
                    $src_ip[0][1] = "***";
                    $src_ip[0][2] = "***";
                    $all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
                    // error_log("[c/product/saja] src_ip: ".json_encode($all_src_ip));

                    $ret['retObj']['src_ip']=empty($bidded['src_ip']) ? "" : $all_src_ip;

                    if(strpos($ret['retObj']['src_ip'], ",")){
                        $ret['retObj']['src_ip'] = substr($ret['retObj']['src_ip'], 0, strpos($ret['retObj']['src_ip'],","));
                    }
                    $ret['retObj']['curr_winner_comefrom']=empty($bidded['comefrom'])?"":$bidded['comefrom'];
                    $ret['retObj']['curr_winner_headimg']=$bidded['thumbnail_file'];
					$ret['retObj']['curr_winner_headimg_url']=empty($bidded['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bidded['thumbnail_url'];
				}
                // error_log("[product/saja]num_bids_gap:".$num_bids_gap[0]['num']);
                $ret['retObj']['num_bids_gap'] = empty($num_bids_gap[0]['num'])?$get_product['saja_limit']:($get_product['saja_limit']-$num_bids_gap[0]['num']);

            }else{
				if($bidded['userid'] <> ""){
					preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
					$src_ip[0][1] = "***";
					$src_ip[0][2] = "***";
					$all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
					error_log("[c/product/saja] src_ip: ".json_encode($all_src_ip));
					$bidded['src_ip'] = $all_src_ip;
				}
			    $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
				
				if(!empty($bidded['thumbnail_file'])){
					$bidded['curr_winner_headimg'] = BASE_URL.HEADIMGS_DIR .'/'. $bidded['thumbnail_file'];
				} elseif(!empty($bidded['thumbnail_url'])) {
					$bidded['curr_winner_headimg'] = $bidded['thumbnail_url'];
				} else {
					$bidded['curr_winner_headimg'] = BASE_URL.APP_DIR .'/static/img/rankman_1.png';
				}
				
			    // $redis = getRedis('','');
                // $redis->set('PROD:'.$dreamid.':BIDDER',$bid_name);
                $tpl->assign('sajabid', $bidded);
                error_log("[product/saja]Winner:".$bid_name." of Prod:".$dreamid." From DB and sync to redis : ".$bid_name);
			}

			if($json=='Y'){

			}else{
			  $tpl->assign('canbid',$_REQUEST['canbid']);
			}

		}

		$tpl->set_title('');

        if(!empty($msg))
			$tpl->assign('msg',$msg);

		// error_log("[c/product/saja] productid: ".$dreamid.", is_flash : ".$get_product['is_flash']);
		$tpl->assign('product', $get_product);

		if(!empty($userid)){
			$bidget_count = $user->pay_get_product_count($userid);
		}else{
			$bidget_count = 0;
		}
		
		// 限定商品資格
		// checkCode=1 才可下標 
		// checkMsg 不可下標時顯示的訊息
		$retCheck['checkCode'] = 1;
		$retCheck['checkMsg'] = "";
		if ($get_product['limitid']=="7" || $get_product['limitid']=="21") {
			$retCheck = $dream->new_hand_rule($dreamid,$userid,'0',$get_product['limitid']);
		}
		/*
		if ($get_product['limitid']=="8") {
			$retCheck['checkMsg'] = "哈雷俱樂部活動場專用"."\n"."限花蓮現場殺友下標"."\n"."得標者需在活動現場當場領取"."\n"."否則視同棄標";
		}
		*/
		$err_limit = 0;
		if (!empty($get_product['limitid'])) {
			if ($get_product['kind'] == 'small') {
				$err_limit = ((int)$bidget_count>=$get_product['mvalue'])? 1 : 0;
			}else if($get_product['kind'] == 'big'){
				$err_limit = ((int)$bidget_count<=$get_product['svalue'])? 1 : 0;
			}
			if ($err_limit == 1) {
				$retCheck['checkCode'] = 3;
				$retCheck['checkMsg'] = "不符合{$get_product['plname']}商品資格";
			}
		}
		
		// Add By Thomas 20191024
		/* 檢查是否雙11商品
		$arrProdCates=$dream->get_product_category($dreamid);
		if($arrProdCates && is_array($arrProdCates)) {
		   foreach($arrProdCates as $k=>$v) {
			   if($v['pcid']=='111') {   
			      $retCheck['checkCode'] = 1;
				  $retCheck['checkMsg'] = " 1.本商品每人限得標一次\n（同一手機驗證, 同一收貨地址之帳號視為同一人）\n2.得標者需親至活動現場領取，違反者視為無條件棄標 ~";
				  $today = date('m-d H:i:s');
				  $limit_date="11-11 00:00:00";
			      if($today<$limit_date) {
					 $retCheck['checkCode'] = 11;
					 $retCheck['checkMsg'] = "11/11後才能下標喔 ~";
				  }
			   }
		   }
		}
		*/
		
		$ret['retCheck'] = $retCheck;
		// error_log("[c/product/saja] retCheck (for APP) :".json_encode($retCheck));
		
		if($json=='Y'){
			$ret['retObj']['bidget_count'] = $bidget_count;
		}else{
			$tpl->assign('bidget_count', $bidget_count);
			$tpl->assign('retCheck', $retCheck);
		}

		if($get_product['is_flash']=='Y'){
			//Add By Thomas 20150721 for LINE 分享
			$meta['title']=$get_product['name']." 閃殺搶標中 !!";
			$meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
			if($json=='Y'){

			}else{
				$tpl->assign('meta',$meta);
			}
			$bid_price=0;
			$canbid='N';
			
			if(!empty($userid) && $userid>0){
				$saja_record=$bid->getUserSajaHistory($userid,$dreamid,' price ASC ',1);
				if($saja_record!=false){
					$bid_price=$saja_record[0]['price'];
					$canbid='N';
				}else{
					$canbid='Y';
				}
				error_log("[c\product\saja] User ".$userid." bid price for productid ".$dreamid." is : ".$bid_price);
				if($json=='Y'){
					$ret['retObj']['bid_price'] = $bid_price;
				}else{
					$tpl->assign('canbid', $canbid);
					$tpl->assign('bid_price', $bid_price);
					$meta['bid_price']=$bid_price;
				}
			}

			if($json=='Y'){
				echo json_encode($ret);
				exit;
			}else{
				//檢查殺價券
				$tpl->render("product","kuso_saja",true);
				exit;
			}

		}else{
			  $meta['title']=$get_product['name']." 搶標中 !!";
			  $meta['description']='';
			  $tpl->assign('meta',$meta);

			  if($json=='Y'){
				$ret['retObj']['bid_enable']= $get_user['bid_enable'];
				$x= json_encode($ret);

				echo $x;
				exit;
			  }else{
				$tpl->render("dream","saja",true);
			  }

		}

	}


	public function getCurrWinnerInfo($dreamid) {

        global $dream;
        $CurrWinnerInfo=$dream->get_bidded($dreamid);
        if(!empty($CurrWinnerInfo['userid'])){
            if(empty($CurrWinnerInfo['thumbnail_url']) && !empty($CurrWinnerInfo['thumbnail_file'])){
                $CurrWinnerInfo['thumbnail_file']=BASE_URL.APP_DIR.'/images/site/headimgs/'.$CurrWinnerInfo['thumbnail_file'];
            }
            return $CurrWinnerInfo;
        }else{
            return false;
        }

	}


	//殺價商品的資料
	public function adview() {

    global $tpl, $dream, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);

		$dreamid=$_REQUEST['productid'];

		if(empty($dreamid)){
			$plist=$dream->product_flash_list_all('');
			if(!empty($plist['table']['record'])){
			  $dreamid= $plist['table']['record'][0]['productid'];
			}
		}

		//商品資料
		$get_product = $dream->get_info($dreamid);
		$tpl->assign('product', $get_product);

		//目前中標
		$redis = getRedis('','');
    $bid_name=$redis->get('PROD:'.$dreamid.':BIDDER');

		if(empty($bid_name)){
      $bidded = $dream->get_bidded($dreamid);
      $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '無';
      $redis->set('PROD:'.$dreamid.':BIDDER',$bid_name);
      error_log("[adview]Winner:".$bid_name." of Prod:".$dreamid." From DB and sync to redis : ".$bid_name);
		}else{
		  error_log("[adview]Winner:".$bid_name." of Prod:".$dreamid." From redis");
		}

		$tpl->assign('bidded', $bid_name);

		//Add By Thomas 20150721 for LINE 分享
		if($get_product['is_flash']=='Y'){
      $meta['title']=$get_product['name']." 閃殺搶標中 !!";
      $meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
		}else{
		  $meta['title']=$get_product['name']." 搶標中 !!";
		  $meta['description']='';
		}

		if(!empty($get_product['thumbnail'])){
			$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail'];
		}elseif (!empty($get_product['thumbnail_url'])){
			$meta['image'] = $get_product['thumbnail_url'];
		}

		$showQrcode=$dream->show_flash_qrcode();
		error_log("Show Qrcode : ".$showQrcode);
		$tpl->assign('qrcode',$showQrcode);
		$tpl->assign('header','N');
		$tpl->assign('footer','N');
		$tpl->assign('meta',$meta);
		$tpl->set_title('');
		$tpl->render("product","adview",true);

	}


	public function adctrl() {

    global $tpl, $dream, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);

		$plist=$dream->product_flash_list_all('');
		$prod_sel_list=array();

		if(!empty($plist['table']['record'])){
		  foreach($plist['table']['record'] as $prod){
		    $prod_sel_list[$prod['productid']]=$prod['name']." (".$prod['productid'].")";
		  }
		}

		$dreamid=$_REQUEST['productid'];
		if(empty($dreamid)){
			if(!empty($plist['table']['record'])){
			  $dreamid= $plist['table']['record'][0]['productid'];
			}
		}

		//商品資料
		$get_product = $dream->get_info($dreamid);
		$tpl->assign('product', $get_product);

		//目前中標
		$bid_name='';

		if(empty($bid_name)){
		  $bidded = $dream->get_bidded($dreamid);
		  $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '_無_';
		  error_log("[adview]Winner:".$bid_name." of Prod:".$dreamid." From DB ");
		}else{
		  error_log("[adview]Winner:".$bid_name." of Prod:".$dreamid." From redis");
		}

		$tpl->assign('bidded', $bid_name);

		//Add By Thomas 20150721 for LINE 分享
		if($get_product['is_flash']=='Y'){
		  $meta['title']=$get_product['name']." 閃殺搶標中 !!";
		  $meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
		}else{
		  $meta['title']=$get_product['name']." 搶標中 !!";
		  $meta['description']='';
		}

		if(!empty($get_product['thumbnail2'])){
			$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail2'];
		}elseif (!empty($get_product['thumbnail_url'])){
			$meta['image'] = $get_product['thumbnail_url'];
		}

		$showQrcode=$dream->show_flash_qrcode();
		error_log("Show Qrcode : ".$showQrcode);
		$tpl->assign('qrcode',$showQrcode);
		$tpl->assign('prod_sel_list',$prod_sel_list);
		$tpl->assign('option',$option);
		$tpl->assign('header','Y');
		$tpl->assign('footer','N');
		$tpl->assign('meta',$meta);
		$tpl->set_title('');
		$tpl->render("product","adctrl",true);

	}


	// 固定導到最早結標的閃殺商品
	/*
	 *  QRcode導到此, 存入$_SESSION['canbid']='Y' 再導去頁面
	 *  http://www.shajiawang.com/wx_auth.php?jdata=gotourl:/site/product/top_kuso_saja
	 */
	public function top_kuso_saja() {

	  global $tpl, $dream, $scodeModel, $user, $oscode, $bid;

	  set_status($this->controller);

	  $userid=$_SESSION['auth_id'];
    $errMsg='';
		$errAct='';
		$oscode_num =0;
		error_log("[c/top_kuso_saja] userid : ".$userid);

		if(!empty($userid) && $userid>0){

      if(empty($_REQUEST['productid'])){
			  $plist=$dream->product_flash_list_all('');
			}else{
	      $plist['table']['record'][0]=$dream->get_info($_REQUEST['productid']);
			}

      if(!empty($plist['table']['record']) && !empty($plist['table']['record'][0]['productid'])){
			  $tpl->assign('canbid','Y');
				$dreamid= $plist['table']['record'][0]['productid'];

        if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']>0){

          //查詢是否有贈送過殺價券
          $oscode_num0 = $scodeModel->get_oscode($dreamid, $_SESSION['auth_id']);

          //查詢是否有贈送殺價券活動
          $scode_promote = $scodeModel->scode_promote($dreamid);
          $info['spid'] = $scode_promote['spid'];
          $info['behav'] = '';
          $info['productid'] = $dreamid;
          $info['userid'] = $_SESSION['auth_id'];
          $info['onum'] = $scode_promote['onum'];

          // 如果沒送過且有活動就贈送
          if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){
           for($j = 0;$j < $info['onum']; ++$j){
             $scodeModel->add_oscode($info);
           }
           $scodeModel->set_scode_promote($info);
           $errMsg="恭喜您獲得 ".$info['onum']."張本商品殺價券";
          }

          // 可用殺價券數量
          $oscode_num =  $oscode->get_prod_oscode($dreamid, $_SESSION['auth_id']);
          $tpl->assign('oscode_num', $oscode_num);
        }

        //商品資料
        $get_product = $dream->get_info($dreamid);
        $pay_type = $get_product['pay_type'];
        $tpl->assign('product', $get_product);

        // 店點資料(目前未使用)
        $sgift=0;
        $tpl->assign('sgift_num', $sgift);

        //目前中標
        $redis = getRedis('','');
        $bid_name=$redis->get('PROD:'.$dreamid.':BIDDER');

        if(empty($bid_name)){
          $bidded = $dream->get_bidded($dreamid);
          $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '無';
          $redis->set('PROD:'.$dreamid.':BIDDER',$bid_name);
          error_log("[top_kuso_saja]Winner:".$bid_name." of Prod:".$dreamid." From DB and sync to redis : ".$bid_name);
        }else{
          error_log("[top_kuso_saja]Winner:".$bid_name." of Prod:".$dreamid." From redis");
        }
        $tpl->assign('bidded', $bid_name);

        // 取得該用戶的(最低)出價
        $bid_price=0;
        if(!empty($_SESSION['auth_id']) && $_SESSION['auth_id']>0) {
          $saja_record=$bid->getUserSajaHistory($_SESSION['auth_id'],$dreamid,' price ASC ',1);
          if($saja_record!=false){
            $bid_price=$saja_record[0]['price'];
          }
        }
				$tpl->assign('bid_price', $bid_price);

        //Add By Thomas 20150721 for LINE 分享
        if($get_product['is_flash']=='Y'){
          $meta['title']=$get_product['name']." 閃殺搶標中 !!";
          $meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
        }else{
          $meta['title']=$get_product['name']." 搶標中 !!";
          $meta['description']='';
        }
        // 商品圖檔
        if(!empty($get_product['thumbnail'])){
          $meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail'];
        }elseif (!empty($get_product['thumbnail_url'])){
          $meta['image'] = $get_product['thumbnail_url'];
        }
        $tpl->assign('meta',$meta);

			}else{
        $errAct="window.location.href='/site/product/?type=flash&canbid=N';";
        $errMsg='沒有這件商品 !!';
			}

		}else{
      $errAct="window.location.href='/site';";
      $errMsg='請登入享受更多優惠 !!';
		}

		$tpl->assign('errMsg',$errMsg);
		$tpl->assign('errAct',$errAct);
		$tpl->set_title('');
		$tpl->render("product","kuso_saja",true);

	}


	/*
	 *	今日必殺商品清單
	 *	$json					varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$p						int					分頁編號
	 */
	public function today() {

		global $tpl, $dream;

		$json = (empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$p 	= (empty($_POST['p'])) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		//設定 Action 相關參數
		set_status($this->controller);

		$today = $dream->get_today();

		if($json != 'Y'){
			$tpl->assign('today_product', $today['record']);
			$tpl->set_title('');
			$tpl->render("product","today",true);
		}else{
			$ret=getRetJSONArray(1,'OK','LIST');

			if($today){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $today['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					$ret['retObj']['data'] = $today['record'];
				}
				$ret['retObj']['nowmicrotime'] = microtime(true);
				$ret['retObj']['page'] = $today['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));
				$ret['retObj']['data'] = array();
				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);

			exit;

		}

	}

	//閃殺活動
	public function flash() {

    global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		//跑馬燈
		$marquee = $dream->get_marquee();
		$tpl->assign('marquee', $marquee);

		$tpl->set_title('');
		$tpl->render("product","flash",true);

	}

    //取得得標者
	public function winner() {
        global $tpl, $dream, $user;

		//設定 Action 相關參數
		set_status($this->controller);
        // date_default_timezone_set('Asia/Shanghai');

        $dreamid=htmlspecialchars($_GET['productid']);
		if(empty($dreamid)){
		   $dreamid=htmlspecialchars($_POST['productid']);
		}

        $r=array('name'=>'',
                 'comefrom'=>'',
                 'productid'=>'',
                 'second'=>0,
                 'status'=>1, //狀態碼(1:更新得標者, 2:無人得標)
				 'thumbnail_file'=>'',
				 'thumbnail_url'=>''
                );

        if(empty($dreamid)) {
           echo json_encode($r);
           return ;
        }

        $r['productid'] = $dreamid;
        $redis = getRedis('','','');
        if($redis) {
            if($redis->exists("PROD:".$dreamid.":BIDDER")) {
               // redis key 存在 -> 即使撈出來的值是空的(表示目前沒有人得標) 也直接回傳
               	$rdata = $redis->get("PROD:".$dreamid.":BIDDER");
				$winnerdata = json_decode($rdata,TRUE);
				$r['name'] = $winnerdata['name'];
				$r['thumbnail_file'] = $winnerdata['thumbnail_file'];
				$r['thumbnail_url'] = $winnerdata['thumbnail_url'];
				$r['status'] = $winnerdata['status'];

                error_log("[c/product/winner] redis PROD:".$dreamid.":BIDDER->".$r['name'].":STATUS->".$r['status']);
            } else {
               // redis key 不存在 -> 去DB撈並更新redis後回傳
               $bidded = $dream->get_bidded($dreamid);
               error_log("[c/product/winner] bidded : ".json_encode($bidded));
               if($bidded && $bidded['shid']>0) {
					if($bidded['userid'] != 0) {
						$userdata = $user->get_user_profile($bidded['userid']);
						$r['thumbnail_file'] = $userdata['thumbnail_file'];
						$r['thumbnail_url'] = $userdata['thumbnail_url'];
					}
                    if($bidded['src_ip']) {
                       $r['comefrom'] = maskIP($bidded['src_ip']);
                    }
                    if (empty($bidded['nickname'])) {
	                	$r['name'] = urldecode($bidded['nickname']);
	                	$r['status'] = 2; //狀態碼(1:更新得標者, 2:無人得標)
               	 	}
                    // $r['name'] = empty($bidded['nickname'])?' ':urldecode($bidded['nickname']);
                    $r['name'] = urldecode($bidded['nickname']);
               }
               $redis->set("PROD:".$dreamid.":BIDDER",json_encode($r));
               error_log("[c/product/winner] DB->redis PROD:".$dreamid.":BIDDER->".$r['name'].":STATUS->".$r['status']);
            }
        } else {
            //目前得標
            $bidded = $dream->get_bidded($dreamid);
            if($bidded && $bidded['shid']>0) {
				if($bidded['userid'] != 0) {
					$userdata = $user->get_user_profile($bidded['userid']);
					$r['thumbnail_file'] = $userdata['thumbnail_file'];
					$r['thumbnail_url'] = $userdata['thumbnail_url'];
				}
                if($bidded['src_ip']) {
                   $r['comefrom'] = maskIP($bidded['src_ip']);
                }
                if (empty($bidded['nickname'])) {
                	$r['name'] = urldecode($bidded['nickname']);
                	$r['status'] = 2; //狀態碼(1:更新得標者, 2:無人得標)
                }
            }
            error_log("[c/product/winner] DB PROD:".$dreamid.":BIDDER->".$r['name'].":STATUS->".$r['status']);
        }
		$r['second'] = date("s");
		echo json_encode($r);
	}

    //取得閃殺得標者(只撈DB)
	public function kusosaja_winner() {

        global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);
        date_default_timezone_set('Asia/Taipei');

        $dreamid=htmlspecialchars($_GET['productid']);
		if(empty($dreamid)){
		   $dreamid=htmlspecialchars($_POST['productid']);
		}

        $r=array('name'=>'',
                 'comefrom'=>'',
                 'productid'=>'',
                 'second'=>0
                );

        if(empty($dreamid)) {
           echo json_encode($r);
           return ;
        }

        $r['productid'] = $dreamid;
        //目前得標
        $bidded = $dream->get_bidded($dreamid);
        if($bidded && $bidded['shid']>0) {
           $r['name'] = empty($bidded['nickname'])?' ':urldecode($bidded['nickname']);
        }
        error_log("[c/product/kusosaja_winner] DB PROD:".$dreamid.":BIDDER->".$r['name']);
       	$r['second'] = date("s");
		echo json_encode($r);
	}

	public function oscode() {

		global $tpl, $dream, $scodeModel, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$user_src=htmlspecialchars($_REQUEST['user_src']);
		$dreamid=htmlspecialchars($_REQUEST['productid']);
		$channelid=htmlspecialchars($_REQUEST['channelid']);

		$userid=$_SESSION['auth_id'];

		if(empty($userid))
			$userid=htmlspecialchars($_POST['auth_id']);

		// 用helper的GetIP()
		$src_ip = GetIP();

		// 用helper的browserType()
		$browserType=browserType();

		$base_url=BASE_URL;

		if(empty($userid)){

      error_log("[c/product/oscode] empty userid, productid: ".$dreamid);
      $state['callback_url']=$base_url.APP_DIR."/product/oauthCallback/";
      $state['productid']=$dreamid;
      $state['user_src']=$user_src;

      switch($browserType){
        case 'line':
                    $url=$base_url.APP_DIR."/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
                    error_log("[product/oscode] line auth url :".$url);
                    header("Location:".$url);
                    return;
                    break;
        case 'fb':
                  $url=$base_url.APP_DIR."/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
                  error_log("[product/oscode] fb auth url :".$url);
                  header("Location:".$url);
                  return;
                  break;
        case 'weixin':
                      $url=$base_url."/wx_auth.php?jdata=".urlencode("gotourl:/site/product/oscode/|productid:".$dreamid."|user_src:".$user_src);
                      error_log("[product/oscode] weixin auth url :".$url);
                      header("Location: ".$url);
                      return;
                      break;
        default :
                $url=$base_url.APP_DIR."/member/userlogin/?gotourl=/site/product/oscode/&productid=".$dreamid."&user_src=".$user_src;
                header("Location: ".$url);
                return;
                break;
      }

		}else{
			// 指定用戶登入
			error_log("[c/product/oscode] logined !! userid: ".$userid.", productid: ".$dreamid);
			$user->logAffiliate($src_ip, $dreamid,'FLASH|LOGIN','http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'] , $_SERVER['HTTP_USER_AGENT'], $_SESSION['auth_id'],strtoupper($browserType),'');

			//查詢是否有贈送過殺價券
			$oscode_num = $scodeModel->get_oscode($dreamid, $userid);

			//查詢是否有贈送活動
			$scode_promote = $scodeModel->scode_promote($dreamid);

			$info['spid'] = $scode_promote['spid'];
			$info['behav'] = '';
			$info['productid'] = $dreamid;
			$info['userid'] = $userid;
			$info['onum'] = $scode_promote['onum'];

			if($info['onum'] > 0 && ($oscode_num == 0 || $oscode_num==false)){
				for($j = 0;$j < $info['onum']; ++$j){
					$scodeModel->add_oscode($info);
				}
				$scodeModel->set_scode_promote($info);
				echo "<script>alert('恭喜您獲得 ".$info['onum']."張本商品殺價券');</script>";
			}
			echo "<script>location.href='".BASE_URL . APP_DIR . "/product/saja/?productid=".$info['productid']."&canbid=Y';</script>";
		}
	}

	public function oauthCallback() {

		global $db, $config, $tpl, $usermodel, $broadcast;

		$auth_by	= empty($_POST['auth_by']) ? htmlspecialchars($_GET['auth_by']) : htmlspecialchars($_POST['auth_by']);
		$sso_uid	= empty($_POST['sso_uid']) ? htmlspecialchars($_GET['sso_uid']) : htmlspecialchars($_POST['sso_uid']);
		$state   	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
		$data 		= empty($_POST['data']) ? htmlspecialchars($_GET['data']) : htmlspecialchars($_POST['data']);
		$sso_data 	= json_decode(urldecode(base64_decode($data)), true);

		// 撈用戶資料, 沒有的話就新建一個
		$user = $this->regUser($auth_by, $sso_uid, $state, $data, $sso_data);

		// 將用戶設定成登入狀態後導到商品頁
		if($user){
		  $usermodel = setLogin($user, $usermodel);
		  $arrState = json_decode(urldecode(base64_decode($state)), true);
		  $dreamid=$arrState['productid'];
		  echo "<script>location.href='".BASE_URL . APP_DIR . "/product/saja/?productid=".$dreamid."&canbid=Y';</script>";
		}else{
		  echo "<script>";
		  echo "alert('登入失敗 !!');";
		  echo "location.href='".BASE_URL.APP_DIR."';";
		  echo "</script>";
		}
	}


    /*
     *	接收oauthCallback的入參, 以檢查第三方資料是否已有對應用戶, 有則撈取  無則新增後傳回
	 */
	public function regUser($auth_by='', $sso_uid='', $state='', $data='', $sso_data='') {

		global $db, $config, $tpl, $usermodel, $broadcast;

		error_log("[c/product/regUser] state : ".urldecode(base64_decode($state)));
		$arrState =  json_decode(urldecode(base64_decode($state)), true);
		$dreamid=$arrState['productid'];
		$user_src=$arrState['user_src'];
		$ts=str_replace(".","",microtime(true));
		  $arrSSO = array();
		switch($auth_by){
      case 'line':
                  $arrSSO = array(
                                  'auth_by' 		=> $auth_by,
                                  'sso_data' 		=> urldecode(base64_decode($data)),
                                  'state' 		=> $state,
                                  'nickname' 		=> $sso_data['displayName'],
                                  'headimgurl'    => $sso_data['pictureUrl'],
                                  'sso_name' 		=> "line",
                                  'phone'         => "line_".$ts,
                                  'type' 			=> "sso",
                                  'json'          => "Y",
                                  'gender'        => "N/A",
                                  'sso_uid' 		=> $sso_uid,
                                  'sso_uid2' 		=> "",
                                  'user_src'      => $user_src
                  );
                  break;
      case 'fb':
                $arrSSO = array(
                                'auth_by' 		=> $auth_by,
                                'sso_data' 		=> urldecode(base64_decode($data)),
                                'state' 		=> $state,
                                'nickname' 		=> $sso_data['name'],
                                'headimgurl' => $sso_data['picture']['url'],
                                'sso_name' 		=> "fb",
                                'phone'         => "fb_".$ts,
                                'type' 			=> "sso",
                                'json'          => "Y",
                                'gender'        => "N/A",
                                'sso_uid' 		=> $sso_uid,
                                'sso_uid2' 		=> "",
                                'user_src'      => $user_src
                );
                break;
    }
    error_log("[c/product/regUser] oauth data : ".json_encode($arrSSO));

    $get_user = $broadcast->get_user($sso_uid, $auth_by, '');

    if(empty($get_user)){
      // 用ajax/user_register.php 進行註冊
      // 並送推薦人和TA殺價券
      $url = BASE_URL.APP_DIR."/ajax/user_register.php";
      //設定送出方式-POST
      $stream_options = array(
                              'http' => array (
                                                'method' => "POST",
                                                'content' => json_encode($arrSSO),
                                                'header' => "Content-Type:application/json"
                                              )
      );
      $context = stream_context_create($stream_options);

      // 送出json內容並取回結果
      $response = file_get_contents($url, false, $context);

      error_log("[c/product/regUser] response : ".$response);

      // 讀取json內容
      $arr = json_decode($response,true);

      if($arr['retCode'] == 1){
        $get_user = getUserBySSO($sso_uid, $auth_by, '');
      }
    }

    if(empty($get_user)){
      $get_user=false;
    }

		error_log("[c/product/regUser] regUser : ".json_encode($get_user));
		return $get_user;

	}


	/*  ad broadcast */
	public function getBroadcastInfo() {

    global $tpl, $dream;

    set_status($this->controller);

    $latitude=$_REQUEST['lat'];
    $longitude=$_REQUEST['lng'];
    error_log("[c\product\getBroadcastInfo] Latitude:".$latitude."|Longitide:".$longitude);
    $broadcast_info=$dream->getBroadcastInfo( $latitude, $longitude);

    if(!empty($broadcast_info)){
      echo json_encode($broadcast_info);
    }
    exit;

	}


	// For APP, 取得廣告banner資料
	public function getAdList() {

    global $dream;

    set_status($this->controller);

    $type=$_POST['type'];
    $limit=$_POST['limit'];

    if(empty($limit)){
      $limit=5;
    }

    if($type==2 || $type==3){
      $limit=1;
    }

    $_POST['perpage']=$limit;
    error_log("[c/product/getAdList] {type:".$type.",limit:".$limit."}");
    $ret=null;

    $get_list = $dream->getAdBannerList($type, $limit);
    if($get_list){
      $ret=getRetJSONArray(1,'OK','LIST');
      $ret['retObj']['data']=$get_list['record'];
      $ret['retObj']['page']=$get_list['page'];
    }else{
      $ret=getRetJSONArray(0,'No Data Found!!','LIST');
      $ret['retObj']['data']=array();
      $ret['retObj']['page']=new stdClass();
    }
    echo json_encode($ret);

    exit;

	}


	public function getBidWinnerInfo($dreamid='') {

    global $tpl, $dream;

    $ret=array();
    $ret['productid']=$dreamid;

    if(empty($dreamid)){
      $ret['productid']=$_REQUEST['productid'];
    }

    $ret['userid']='';

    if(empty($ret['productid'])){
      echo json_encode($ret);
    }

    $record=$dream->getBidWinnerInfo($ret['productid']);
    if($record!=false){
      $ret['userid'] = $record[0]['userid'];
      $ret['nickname'] = $record[0]['nickname'];
      $ret['productid'] = $record[0]['productid'];
      $ret['src_ip'] = $record[0]['src_ip'];
      $ret['prodname'] = $record[0]['prodname'];
      $ret['price'] = sprintf("%01.2f",$record[0]['price']);
      $ret['curr_winner_comefrom'] = $record[0]['comefrom'];
      $ret['openid'] = $record[0]['openid'];
      $ret['unionid'] = $record[0]['unionid'];
    }
    echo json_encode($ret);

	}

	/*
	 *	商品分類清單
	 */
	public function getSajaProdCateList() {

		global $dream;

		$list = $dream->product_category_list();
		$ret = null;

		if($list){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']=$list;
			$ret['retObj']['page']=new stdClass();
		}else{
			$ret=getRetJSONArray(0,'No Data Found !!','LIST');
			$ret['retObj']['data']=new stdClass();
			$ret['retObj']['page']=new stdClass();
		}

		echo json_encode($ret);
		exit;

	}


	public function getSajaProdList() {

    global $tpl, $dream;

		$page=$_POST['page'];

    if(empty($page))
			$page=0;
		else
			$page=$page-1;

		if($page<0)
			$page=0;

		$perpage=$_POST['perpage'];

		if(empty($perpage)){
		  $perpage=10;
		}

		$sIdx=$perpage*($page);

		$lprice=$_POST['lprice'];
		if(empty($lprice))
			$lprice=2000;

		$type = empty($_POST['type']) ? $_GET['type'] : $_POST['type'];

		if(empty($_POST['pcid'])){
			$pcid = '';
		}else{
			$pcid = $_POST['pcid'];
		}

		error_log("[c/product/getSajaProdList] pcid:".$pcid.",type:".$type.",perpage:".$perpage.",page:".$page);

		$ProdList=array();

		switch($type){
			case 1: // 大檔商品
    			   $ProdList=$dream->getProdList($sIdx,$perpage,2000,' p.retail_price desc ',$pcid);
    			   break;
			case 2: // 即將結標商品
    			   $ProdList=$dream->getProdList(0,$perpage,'',' p.offtime asc ',$pcid);
    			   break;
			case 3: // 今日必殺(APP)
    			   $ProdList=$dream->get_today_app($pcid);
    			   break;
			default: // 一般的殺價商品清單
      			   $ProdList=$dream->getProdList($sIdx,100,''," p.seq, p.offtime ASC ",$pcid);
      			   break;
		}

		$ret=null;
		if($ProdList){

			for($idx=0; $idx< count($ProdList['record']); ++$idx){
				$p = $ProdList['record'][$idx];
				$ProdList['record'][$idx]['offtime'] = (int)$p['offtime'];
				if(empty($p['thumbnail_url']) && !empty($p['filename']) && ($p['ptid'] != 0)){
				  $ProdList['record'][$idx]['thumbnail_url']=IMG_URL.APP_DIR."/images/site/product/".$p['filename'];
				}
				if(empty($p['filename'])){
					$ProdList['record'][$idx]['filename']=substr($p['thumbnail_url'], 51, -1).'g';
				}
			}

		  $ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $ProdList['record'];
			$ret['retObj']['page'] = $ProdList['page'];

		}else{

		  $ret=getRetJSONArray(0,'No Data Found !!','LIST');
			$ret['retObj']['data'] = array();
			$ret['retObj']['page'] = new stdClass();

		}

		echo json_encode($ret);

		exit;

	}


	public function getMoneySaved() {

		global $dream;

		$arr_amount=$dream->getMoneySaved();
		$ret = getRetJSONArray();

		if($arr_amount){
			$ret['retCode']=1;
			$ret['retMsg']='OK';
			$ret['retObj']=$arr_amount[0];
		}else{
			$ret['retCode']=0;
			$ret['retMsg']='No Data Found !!';
			$ret['retObj']=array("amount"=>0);
		}
		echo json_encode($ret);
		exit;

	}


	public function weixinSDK() {

		global $tpl, $dream;

		$wurl=(empty($_POST['wurl'])) ? ($_GET['wurl']) : ($_POST['wurl']);

		error_log("[site/product/weixinSDK] from : ".$_SERVER['HTTP_REFERER']);
		error_log("[site/product/weixinSDK] wurl : ".$wurl);

		//設定 Action 相關參數
		set_status($this->controller);
		include_once("/var/www/html/site/oauth/WeixinAPI/WeixinChat.class.php");
		include_once("/var/www/html/site/oauth/WeixinAPI/jssdk.php");

		$jssdk = new JSSDK(APP_ID, APP_SECRET);
		$signPackage = $jssdk->getSignPackage($wurl);

		echo json_encode($signPackage);

		exit;

	}


	/*
	 * 2017/01/11 joelin
   * 小程式閃殺
   */
	public function saja_flash_weixin() {

    global $tpl, $dream, $scodeModel;

		//設定 Action 相關參數
		set_status($this->controller);

		$dreamid=$_REQUEST['productid'];

    if(empty($dreamid) ){
			$plist=$dream->product_flash_list_all('');
			if(!empty($plist['table']['record'])){
			  $dreamid= $plist['table']['record'][0]['productid'];
			}
		}

		//商品資料
		$get_product = $dream->get_info($dreamid);

		//目前中標
		$redis = getRedis('','');
    $bid_name=$redis->get('PROD:'.$dreamid.':BIDDER');

		if(empty($bid_name)){
      $bidded = $dream->get_bidded($dreamid);
      $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '無';
      $redis->set('PROD:'.$dreamid.':BIDDER',$bid_name);
      error_log("[saja_flash_weixin]Winner:".$bid_name." of Prod:".$dreamid." From DB and sync to redis : ".$bid_name);
		}else{
		  error_log("[saja_flash_weixin]Winner:".$bid_name." of Prod:".$dreamid." From redis");
		}

		//Add By Thomas 20150721 for LINE 分享
		if($get_product['is_flash']=='Y'){
      $meta['title']=$get_product['name']." 閃殺搶標中 !!";
      $meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
		}else{
      $meta['title']=$get_product['name']." 搶標中 !!";
      $meta['description']='';
		}

		$showQrcode = $dream->show_flash_qrcode();
		error_log("Show Qrcode : ".$showQrcode);
		$get_product['showQrcode'] = $showQrcode;
		$get_product['bidded'] = $bid_name;

		$ret['retCode'] = 1;
		$ret['retMsg'] = "OK";
		$ret['retType'] = "LIST";
		$ret['retObj'] = $get_product;

    echo json_encode($ret);

	}


	//殺價商品的資料 - 小程式專用 2017-01-26
	public function saja2() {

		global $tpl, $dream, $scodeModel, $user, $oscode, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){

			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		$json = (empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$dreamid = (empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);

		if(empty($dreamid)){
		  $dreamid = $_POST['productid'];
		}

		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		$behav=$_REQUEST['behav'];

		error_log("[c/product/saja2] {productid:".$dreamid.",behav:".$behav.",type:".$type.",userid:".$userid."}");

		$ret=null;
		if(empty($dreamid) ){
			echo json_encode(getRetJSONArray(-3,'商品編號為空 !!','MSG'));
			exit;
		}else{

			//商品資料
			$get_product = $dream->get_info2($dreamid);

			if(!$get_product){
				echo json_encode(getRetJSONArray(1,'找不到商品資料 !!','MSG'));
				exit;
			}

			$ret=getRetJSONArray(1,'OK','JSON');
		  $get_product['use_scode'] = false;
			$msg='';
			$get_product['offtime'] = (int)$get_product['offtime'];
			$get_product['now'] = (int)$get_product['now'];

			error_log("[c/product/saja2] time:".date('Y-m-d H:i:s',$get_product['offtime']));

			// 抓source IP
			if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$src_ip = $temp_ip[0];
			}else{
				$src_ip = $_SERVER['REMOTE_ADDR'];
			}
			// 抓user_agent
			$user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			$is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			//APP上 中標處理費用是以百分比顯示
			$get_product['bonus']=str_replace(",","",$get_product['bonus']);
			$idx_dot=strpos($get_product['bonus'],".");

			if($idx_dot>0){
				$get_product['bonus']=substr($get_product['bonus'],0,$dot_idx);
			}

			error_log("[c/product/saja2] 2 {'bonus_type':'".$get_product['bonus_type']."','bonus':'".$get_product['bonus']."'}");

			//取得商品圖示清單
			$get_pic = $dream->getProductThumbnailList($dreamid);
			$tmp_thumbnail_url=$get_product['thumbnail_url'];
			$tmp_thumbnail=$get_product['thumbnail'];
			$get_product['thumbnail_urls']=array();

			if(!empty($tmp_thumbnail_url)){
				$get_product['thumbnail_urls'][0]=$tmp_thumbnail_url;
			}else{
				for ($k=0;$k<count($get_pic);$k++){
					$get_product['thumbnail_urls'][$k]=IMG_URL.APP_DIR.'/images/site/product/'.$get_pic[$k]['filename'];
				}
			}

			unset($get_product['thumbnail_url']);
			unset($get_product['thumbnail']);

			$get_product['abstract']=" 一分錢起標, 出價最低且唯一者得標, 搶標中!! ";
			$ret['retObj']=$get_product;
			$sgift=0;

			// Update By Thomas 2015214
			// 如果有經由qrcode過來 就贈送過殺價券
			if(!empty($userid) && !empty($dreamid) && $behav=="q"){
				$oscode_num0 = $scodeModel->get_oscode($dreamid, $userid);
				//查詢是否有贈送殺價券活動
				$scode_promote = $scodeModel->scode_promote($dreamid,'q');
				$info['spid'] = $scode_promote['spid'];
				$info['behav'] = 'qrcode_reg';
				$info['productid'] = $dreamid;
				$info['userid'] = $userid;
				$info['onum'] = $scode_promote['onum'];
				// 如果沒送過 且有活動 就贈送
				if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){
				  for($j = 0;$j < $info['onum']; ++$j) {
					  $scodeModel->add_oscode($info);
				  }
				  $scodeModel->set_scode_promote($info);
				  $msg="恭喜您獲得 ".$info['onum']."張 ".$get_product['name']." 殺價券";
				}
				// Add End
			}

			// 可用殺價券數量
			$oscode_num =  $oscode->get_prod_oscode($dreamid, $userid);
			error_log("[c/product/saja2] available oscode_num for ".$dreamid." : ".$oscode_num);
			$ret['retObj']['oscode_num']=$oscode_num;

			//目前中標
			$bidded=$this->getCurrWinnerInfo($dreamid);
			$num_bids_gap = $dream->getNumBidsGap($dreamid);
			$ret['retObj']['curr_winner_userid']=empty($bidded['userid'])?"":$bidded['userid'];
			$ret['retObj']['bidded']=empty($bidded['nickname'])?"":$bidded['nickname'];

      if($ret['retObj']['curr_winner_userid']==""){

        $ret['retObj']['bidded']="目前沒有人得標";
				$ret['retObj']['final_bid_price']=0;

      }else{

        $ret['retObj']['src_ip']=empty($bidded['src_ip'])?"":$bidded['src_ip'];
				if(strpos ($ret['retObj']['src_ip'], ",")){
					$ret['retObj']['src_ip'] = substr($ret['retObj']['src_ip'], 0, strpos($ret['retObj']['src_ip'],","));
				}
				$ret['retObj']['curr_winner_comefrom'] = empty($bidded['comefrom'])?"":$bidded['comefrom'];
				$ret['retObj']['curr_winner_headimg'] = empty($bidded['thumbnail_url'])?IMG_URL.HEADIMGS_DIR."/_DefaultHeadImg.jpg":$bidded['thumbnail_url'];

			}
			error_log("[product/saja2] num_bids_gap:".$num_bids_gap[0]['num']);
			$ret['retObj']['num_bids_gap'] = empty($num_bids_gap[0]['num'])?$get_product['saja_limit']:($get_product['saja_limit']-$num_bids_gap[0]['num']);

		}

		error_log("[c/product/saja2] productid: ".$dreamid.", is_flash : ".$get_product['is_flash']);

		if($get_product['is_flash']=='Y'){

      //Add By Thomas 20150721 for LINE 分享
	    $meta['title']=$get_product['name']." 閃殺搶標中 !!";
	    $meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];

	    $bid_price=0;
	    $canbid='N';

      if(!empty($userid) && $userid>0){
  			$saja_record=$bid->getUserSajaHistory($userid,$dreamid,' price ASC ',1);

        if($saja_record!=false){
  				$bid_price=$saja_record[0]['price'];
  				$canbid='N';
  			}else{
  				$canbid='Y';
  			}

  			error_log("[c\product\saja2] User ".$userid." bid price for productid ".$dreamid." is : ".$bid_price);

  			if($json=='Y') {
  				$ret['retObj']['bid_price']=$bid_price;
  			}else{
  				$tpl->assign('canbid', $canbid);
  				$tpl->assign('bid_price', $bid_price);
  				$meta['bid_price']=$bid_price;
  			}
	    }
	    echo json_encode($ret);
		  exit;

		}else{
	    $meta['title']=$get_product['name']." 搶標中 !!";
	    $meta['description']='';
			$x = json_encode($ret);
			error_log("[c/product/saja2] : ".$x);
			echo $x;
			exit;
		}

	}


	public function getProdDesc() {

		global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		$type = $_REQUEST['type'];
		$dreamid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_POST['userid']) ? htmlspecialchars($_GET['userid']) : htmlspecialchars($_POST['userid']);

		$dream_content = $dream->get_info($dreamid);

		$ret=getRetJSONArray(1,'OK','LIST');
		$ret['retObj'] = $dream_content['description2'];
		echo json_encode($ret);

		exit;

	}


	public function getWxLaProdShareQrcode() {

    global $config;

    $path=$_REQUEST['path'];
    $width=$_REQUEST['width'];
    $token=$_REQUEST['access_token'];
    $dreamid=$_REQUEST['productid'];

    if(empty($token) || empty($path)){
      echo "{'retCode':0, 'retMsg':'Empty Data !!'}";
      exit;
    }

    if(empty($width)){
      $width=360;
    }

    $img_path="/var/www/html/site/images/wx_la/".$dreamid."_qrcode.jpg";

    if(file_exists($img_path)){
      error_log("[getWxLaProdShareQrcode] file exists : ".$img_path);
      $ret=getRetJSONArray(1,'OK','JSON');
      $ret['retObj']=array();
      $ret['retObj']['img_url']='https://www.shajiawang.com/site/images/wx_la/'.$dreamid."_qrcode.jpg";
      echo json_encode($ret);
      exit;
    }

    $url='https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode?access_token='.$token;
    $ch = curl_init();
    $data_arr = array('path'=>$path,'width'=>$width);
    $data_json = json_encode($data_arr);
    error_log("[getWxLaProdShareQrcode] : ".$data_json);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS,$data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $response  = curl_exec($ch);
    curl_close($ch);

    if(file_put_contents($img_path,$response)){
      $ret=getRetJSONArray(1,'OK','JSON');
      $ret['retObj']=array();
      $ret['retObj']['img_url']='https://www.shajiawang.com/site/images/wx_la/'.$dreamid."_qrcode.jpg";
    }else{
      $ret=getRetJSONArray(-1,'Image Save Failed !!','MSG');
    }
    echo json_encode($ret);
    exit;
	}


	//下標殺價商品的資料
	public function sajabid(){

		global $tpl, $member, $dream, $scodeModel, $user, $oscode, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){
			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
                			'bid'		=> $bidprice,			//起始價格
                			'bida'		=> $bida,				//起始價格
                			'bidb' 		=> $bidb,				//結束價格
                			'type'		=> $paytype,			//下標類型
                			'autobid'	=> $auto,				//自動下標
		);

		$tpl->assign('paydata',$paydata);

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$dreamid=(empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		$behav=$_REQUEST['behav'];
		$tpl->assign('type',$type);

		$ret=null;
		if(empty($dreamid) ){
			return_to('site/product');
		}else{

			//商品資料
			$get_product = $dream->getProductById($dreamid);

		  //收藏資料
			$collect = $user->getCollectProdList($userid, $dreamid, 'S', '');
			$get_product['collect'] = $collect['record'][0];

			$ret=getRetJSONArray(1,'OK','JSON');
			$get_product['use_scode'] = false;
			$msg='';
			$get_product['offtime'] = (int)$get_product['offtime'];
			$get_product['now'] = (int)$get_product['now'];

			// 抓source IP
			if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$src_ip = $temp_ip[0];
			}else{
				$src_ip = $_SERVER['REMOTE_ADDR'];
			}

			// 抓user_agent
			$user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			$is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if(!empty($get_product['thumbnail'])){
				$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail'];
			}elseif(!empty($get_product['thumbnail_url'])){
				$meta['image'] = $get_product['thumbnail_url'];
			}

			$sgift=0;
			$tpl->assign('sgift_num', $sgift);

			//殺幣數量
			$get_spoint = $member->get_spoint($userid);

			if(!$get_spoint){
				$get_spoint=array("userid"=>$userid, "amount"=>0);
			}else{
				$get_spoint['amount'] = sprintf("%1\$u",$get_spoint['amount']);
			}
			$tpl->assign('spoint', $get_spoint);

			//目前中標
			$bidded=$this->getCurrWinnerInfo($dreamid);
			$num_bids_gap = $dream->getNumBidsGap($dreamid);

			if($bidded['userid'] <> ""){
				preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
				$src_ip[0][1] = "***";
				$src_ip[0][2] = "***";
				$all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				$bidded['src_ip'] = $all_src_ip;
			}

			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			$redis = getRedis('','');
			$redis->set('PROD:'.$dreamid.':BIDDER',$bid_name);
			$tpl->assign('bidded', $bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);

		}

		$tpl->set_title('');

    if(!empty($msg))
		  $tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);

		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);

		if($json == 'Y'){
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LSIT";
			$data['retObj']['product'] = (!empty($get_product)) ? $get_product : new stdClass;
			$data['retObj']['paydata'] = (!empty($paydata)) ? $paydata : new stdClass;
			$data['retObj']['spoint'] = (!empty($get_spoint)) ? $get_spoint : new stdClass;
			echo json_encode($data);
		}else{
			$tpl->render("dream","sajabid",true);
		}

	}


	//下標殺價商品支付
	public function sajatopay() {

    global $tpl, $dream, $scodeModel, $user, $oscode, $bid, $member;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$bida = empty($_POST['bida']) ? htmlspecialchars($_GET['bida']) : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? htmlspecialchars($_GET['bidb']) : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? htmlspecialchars($_GET['auto']) : $_POST['auto'];
		$paytype = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : $_POST['type'];
		$count = empty($_POST['count']) ? htmlspecialchars($_GET['count']) : $_POST['count'];
		$fee_all = empty($_POST['fee_all']) ? htmlspecialchars($_GET['fee_all']) : $_POST['fee_all'];
		$total_all = empty($_POST['total_all']) ? htmlspecialchars($_GET['total_all']) : $_POST['total_all'];
		$dreamid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : $_POST['productid'];
		$userid = empty($_SESSION['auth_id']) ? $_POST['auth_id']:$_SESSION['auth_id'];
		$type = empty($_SESSION['auth_type']) ? $_POST['auth_type']:$_SESSION['auth_type'];
		$behav = $_REQUEST['behav'];
		$bidprice = '';
		$channelid = empty($_POST['channelid']) ? $_GET['channelid'] : $_POST['channelid'];

		if($json != 'Y'){
			$udata = empty($_POST) ? $_GET : $_POST;
		}else{
			$paytype = empty($_POST['paytype']) ? htmlspecialchars($_GET['paytype']) : $_POST['paytype'];

			$udata = array(
              				'productid'		=> $dreamid,
              				'bida'			=> $bida,
              				'bidb' 			=> $bidb,
              				'total_all' 	=> $total_all,
              				'count' 		=> $count,
              				'type'			=> $paytype,
              				'fee_all'		=> $fee_all,
              				'json'			=> $json,
              				'fun'			=> 'product',
              				'act'			=> 'sajatopay',
              				'channelid'		=> $channelid
			);
		}

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){
			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
                			'bid'		=> $bidprice,			//起始價格
                			'bida'		=> $bida,				//起始價格
                			'bidb' 		=> $bidb,				//結束價格
                			'type'		=> $paytype,			//下標類型
                			'autobid'	=> $auto,				//自動下標
		);
		error_log("[c/product/saja] paydata : ".json_encode($paydata));

		$tpl->assign('paydata',$paydata);

		$ret=null;
		if(empty($dreamid) ){
			return_to('site/product');
		}else{

			//S碼 現有可用總數
			$scode_num = $scodeModel->get_scode($userid);
			// $scode_num = 5;
			$tpl->assign('scode_num', $scode_num);

			// S碼 已用總數
			$scode_use_num = abs($scodeModel->used_sum($userid));
			if($json=='Y'){
				$ret['retObj']['scode_use_num']=$scode_use_num;
			}else{
				$tpl->assign('scode_use_num', $scode_use_num);
			}

			//商品資料
			// $get_product = $dream->get_info($dreamid);
			$get_product = $dream->getProductById($dreamid);

			$ret=getRetJSONArray(1,'OK','JSON');
			$get_product['use_scode'] = false;
			$msg='';
			$get_product['offtime'] = (int)$get_product['offtime'];
			$get_product['now'] = (int)$get_product['now'];

			// 抓source IP
			if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$src_ip = $temp_ip[0];
			}else{
				$src_ip = $_SERVER['REMOTE_ADDR'];
			}

			// 抓user_agent
			$user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			$is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if(!empty($get_product['thumbnail'])){
			  $meta['image'] = IMG_URL.APP_DIR.'/images/site/product/'.$get_product['thumbnail'];
			}elseif (!empty($get_product['thumbnail_url'])){
			  $meta['image'] = $get_product['thumbnail_url'];
			}

			$sgift=0;
			$tpl->assign('sgift_num', $sgift);

			// 可用殺價券數量
			$oscode_num =  $oscode->get_prod_oscode($dreamid, $userid);
			if(!$oscode_num){
			  $oscode_num=0;
			}
			$tpl->assign('oscode_num', $oscode_num);

			//殺幣數量
			$get_spoint = $member->get_spoint($userid);
			if(!$get_spoint){
			  $get_spoint=array("userid"=>$this->userid, "amount"=>0);
			}else{
				$get_spoint['amount'] = sprintf("%1\$u",$get_spoint['amount']);
			}
			$tpl->assign('spoint', $get_spoint);

			//目前中標
			$bidded=$this->getCurrWinnerInfo($dreamid);
			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			$redis = getRedis('','');
			$redis->set('PROD:'.$dreamid.':BIDDER',$bid_name);
			$tpl->assign('bidded', $bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);

		}
		$tpl->set_title('');

		if(!empty($msg))
		  $tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);
		$udata['payall'] = getBidTotalFee($get_product, $paytype, $bida, $bida, $bidb, 1);
		$tpl->assign('topay',$udata);
		$tpl->assign('type',$type);
		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);
		error_log("[c/product/saja] udata : ".json_encode($udata));

		if($json == "Y"){
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LSIT";
			$data['retObj']['product']= (!empty($get_product)) ? $get_product : new stdClass;
			$data['retObj']['paydata'] = (!empty($paydata)) ? $paydata : new stdClass;
			$data['retObj']['spoint'] = (!empty($get_spoint)) ? $get_spoint : new stdClass;
			$data['retObj']['scode_num'] = $scode_num;
			$data['retObj']['oscode_num'] = $oscode_num;
			$data['retObj']['topay'] = (!empty($udata)) ? $udata : new stdClass;
			echo json_encode($data);
		}else{
			$tpl->render("dream","sajatopay",true);
		}

	}


	/*
	 *	商品關鍵字查詢
	 *	$prodKeyword			varchar				查詢關鍵字
	 *	$prodType				varchar				商品類別
	 *	$prodClose				varchar				商品下標狀態 (Y:已結標,N:競標中,NB:流標)
	 *	$json					varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$p						int					分頁編號
	 */
	public function getProductSearch() {

		global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		$prodKeyword 	= (empty($_POST['prodKeyword'])) ? htmlspecialchars($_GET['prodKeyword']) : htmlspecialchars($_POST['prodKeyword']);
		$prodType 		= (empty($_POST['prodType'])) ? htmlspecialchars($_GET['prodType']) : htmlspecialchars($_POST['prodType']);
		$prodClose 		= (empty($_POST['prodClose'])) ? htmlspecialchars($_GET['prodClose']) : htmlspecialchars($_POST['prodClose']);
		$json			= (empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$p 				= (empty($_POST['p'])) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if(!empty($prodKeyword)){
			//商品資料
			$get_product = $dream->getProdSearch($prodKeyword, $prodType, 'N', $sIdx='', $perPage='');

			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "JSON";
			$data['retObj']['data'] = $get_product['record'];
			$data['retObj']['page'] = $get_product['page'];

			if($get_product){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_product['page']['totalpages'])){
					$data['retObj']['data'] = array();
				}else{
					$data['retObj']['data'] = $get_product['record'];
				}
				$data['retObj']['nowmicrotime'] = microtime(true);
				$data['retObj']['page'] = $get_product['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));
				$data['retObj']['data'] = array();
				$data['retObj']['nowmicrotime'] = microtime(true);
				$data['retObj']['page'] = $page;
			}

		}else{
			$get_product = "";

			$page['rec_start'] = 0;
			$page['totalcount'] = 0;
			$page['totalpages'] = 1;
			$page['perpage'] = 50;
			$page['page'] = 1;
			$page['item'] = array(array("p" => 1 ));

			$data['retCode'] = -1;
			$data['retMsg'] = "請輸入關鍵字 !!";
			$data['retType'] = "JSON";
			$data['retObj']['data'] = array();
			$data['retObj']['nowmicrotime'] = microtime(true);
			$data['retObj']['page'] = $page;

		}

		$tpl->assign('product_list', $get_product);
		$tpl->assign('prodKeyword', $prodKeyword);

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->set_title('');
			$tpl->render("product","search",true);
		}else{
			echo json_encode($data);
			exit;
		}

	}


	/*
	 *	分類商品查詢
	 *	$pcid					int					分類編號
	 *	$json					varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function getCategoryProduct() {

		global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		$pcid 	= (empty($_POST['pcid'])) ? htmlspecialchars($_GET['pcid']) : htmlspecialchars($_POST['pcid']);
		$json	= (empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$p 		= (empty($_POST['p'])) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
		$userid = empty($_SESSION['auth_id']) ? $_POST['auth_id'] : $_SESSION['auth_id'];
		
		
		$get_product = $dream->getCategoryProductList($selected_channelid, $storeid, $exclude_storeids, $pcid, $p, $userid);

		if($get_product){

			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";

			if ($p=="all") {
				$data['retObj']['data'] = $get_product['table']['record'];
			}else if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_product['table']['page']['totalpages'])){
				$data['retObj']['data'] = array();
			}else{
				$data['retObj']['data'] = $get_product['table']['record'];
			}
			$data['retObj']['nowmicrotime'] = microtime(true);
			$data['retObj']['page'] = $get_product['table']['page'];
		}else{
			$page['rec_start'] = 0;
			$page['totalcount'] = 0;
			$page['totalpages'] = 1;
			$page['perpage'] = 50;
			$page['page'] = 1;
			$page['item'] = array(array("p" => 1 ));

			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LIST";
			$data['retObj']['nowmicrotime'] = microtime(true);
			$data['retObj']['page'] = $page;
		}

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->set_title('');
			$tpl->render("product","search",true);
		}else{
			echo json_encode($data);
			exit;
		}

	}

	/******/
    public function inx_product_list() {
           global $tpl, $dream;
           //設定 Action 相關參數
		   set_status($this->controller);
           $reload = htmlspecialchars($_REQUEST['reload']);
		   if($reload!='Y')
			  $reload='N';
           $arr = '';
		   $ret = '';
		   $rkey = "inx_product_list";
		   $redis = getRedis();
		   if($reload=='N') {
			  if($redis) {
				  $ret=$redis->get($rkey);
				  if(!empty($ret)) {
					 return $ret;
				  }
			  }
		   }
		   $arr = $dream->inx_product_list();
		   if(!empty($arr) && is_array($arr)) {
			  $ret = json_encode($arr);
		   }
		   if($redis) {
			  $redis->set($rkey,$ret);
		   }
		   return $ret;
    }

	public function getSiteHomeAdBannerList() {
		global $tpl, $dream, $ad;
		//設定 Action 相關參數
		set_status($this->controller);
		$reload = htmlspecialchars($_REQUEST['reload']);
		if($reload!='Y')
			$reload='N';
		$arr = '';
		$ret = '';
		$rkey = "SiteAdBannerList1";
		$redis = getRedis();
		if($reload=='N') {
			if($redis) {
				$ret=$redis->get($rkey);
				if(!empty($ret)) {
					return $ret;
				}
			}
		}
		// $arr = $dream->getAdBannerList(4,5,'P');
		$arr = $ad->get_ad_list(1, 'P');
		if(!empty($arr) && is_array($arr)) {
			$ret = json_encode($arr);
		}
		if($redis) {
			//計算即將下架的廣告秒數
			foreach($arr as $res){
				$offtime_array[] = strtotime($res['offtime']);
			}

			//resdis有效時間(秒))
			$rexpire_time = min($offtime_array) - strtotime('now');

			$redis->set($rkey,$ret);
			$redis->expire($rkey, $rexpire_time);
		}
		return $ret;
    }


	/*
	 *	熱門商品列表清單
	 */
    public function hot_product_list() {
		global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		$reload = htmlspecialchars($_REQUEST['reload']);
		if($reload != 'Y'){
			$reload = 'N';
		}
		//建立Redis連線
		$redis=getRedis();

		if($redis){

			//商品列表
			$rdata='';
			$rkey = "hot_product_list";
			$rdata = $redis->get($rkey);

			if($rdata && $reload != 'Y') {
			   $dream_list = json_decode($rdata,TRUE);
			} else {
			   $dream_list = $dream->hot_product_list();
               if(!empty($dream_list) && is_array($dream_list)) {
                  $redis->set($rkey, json_encode($dream_list));
               }
			}

		} else {
			//商品列表
			$dream_list = $dream->hot_product_list();
        }

		if ($dream_list == false){
			$dream_list = array();
		}

		// return $ret;
		$data['retCode'] = 1;
		$data['retMsg'] = "取得資料 !!";
		$data['retType'] = "MSG";
		// $data['retObj']['data'] = $dream_list;
		$data['retObj']['data'] = $dream_list['record'];
		$data['retObj']['page'] = $dream_list['page'];
		$data['retObj']['nowmicrotime'] = microtime(true);

		echo json_encode($data);
		exit;
    }


    // 增加商品點擊數
    public function add_prod_click($dreamid='', $num='',$json='') {
		global $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		if(empty($json)) {
			$json=htmlspecialchars($_REQUEST['json']);
		}
		if(empty($dreamid)) {
			$dreamid=htmlspecialchars($_REQUEST['productid']);
		}
		if(empty($num)) {
			$num=htmlspecialchars($_REQUEST['num']);
		}

		$ret =array();
		if(empty($dreamid)) {
			$ret['retCode'] = 0;
			$ret['retMsg'] = "NO_DATA";
			if($json=='Y') {
			   echo json_encode($ret);
			   return;
			} else {
			   return $ret;
			}
		}

		// if(!is_numeric($num)) {
			$num=1;
		// }

		$ret['add_num']= $dream->add_prod_click($dreamid,$num);
		$ret['retCode'] = 1;
		$ret['retMsg'] = "OK";
		if($json=='Y') {
			echo json_encode($ret);
			return ;
		} else {
			return $ret;
		}
    }


	/*
	 *	取得競標中商品某會員下標金額統計清單
	 *	$userid						int					會員編號
	 *	$dreamid					int					商品編號
	 *	$p							int					頁碼
	 *	$perpage					int					每頁筆數
	 */
    public function get_user_onbid_list() {
		global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = (empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$dreamid = (empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$p = (empty($_POST['p'])) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
		$perpage = (empty($_POST['perpage'])) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);
		$type = (empty($_POST['type'])) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);

		if(empty($type) || $type == 0){
			$type = 1;
		}

		//商品列表
		$dream_list = $dream->get_user_onbid_list($dreamid, $userid, $perpage, $type);
		
		if (empty($dream_list['table']['record'])){
			$dream_list['table']['record'][0]['price'] = '';
			$dream_list['table']['record'][0]['count'] = '';
			
			$page['rec_start'] = 0;
			$page['totalcount'] = 0;
			$page['totalpages'] = 1;
			$page['perpage'] = 50;
			$page['page'] = 1;
			$page['item'] = array(array("p" => 1 ));
			$dream_list['table']['page'] = $page;
		}
		
		// return $ret;
		$data['retCode'] = 1;
		$data['retMsg'] = "取得資料 !!";
		$data['retType'] = "MSG";
		$data['retObj']['data'] = $dream_list['table'];

		echo json_encode($data);
		exit;
    }


	/*
	 *	直播商品列表清單
	 */
    public function lb_product_list() {
		global $tpl, $dream;

		//設定 Action 相關參數
		set_status($this->controller);

		$reload = htmlspecialchars($_REQUEST['reload']);

		//建立Redis連線
		$redis=getRedis();

		if($redis){

			// 商品列表
			$rdata='';
			$rkey = "lb_product_list";
			$rdata = $redis->get($rkey);

			if($rdata) {
			   $dream_list = json_decode($rdata,TRUE);
			} else {
			   $dream_list = $dream->lb_product_list();
               if(!empty($dream_list) && is_array($dream_list)) {
                  $redis->set($rkey, json_encode($dream_list));
               }
			}

		} else {
			//商品列表
			$dream_list = $dream->lb_product_list();
        }

		if ($dream_list == false){
			$dream_list = array();
		}

		// return $ret;
		$data['retCode'] = 1;
		$data['retMsg'] = "取得資料 !!";
		$data['retType'] = "MSG";
		$data['retObj']['data'] = $dream_list;
		$data['retObj']['nowmicrotime'] = microtime(true);

		echo json_encode($data);
		exit;
    }

		/*
		 *	取得現正直播下標商品
		 */
	 	public function get_lb_product_now() {
				global $tpl, $dream;

				//設定 Action 相關參數
				set_status($this->controller);

				//商品列表
				$dream_list = $dream->lb_product_list_now();

				//$dream_list[0]['unix_now'] = 1564495201;

				if ($dream_list == false){
						$data['retCode'] = 1;
						$data['retMsg'] = "今日商品已全數標售完畢 !!";
						$data['retType'] = "MSG";
						$data['retObj']['next_start_in'] = 0;
						$data['retObj']['product_end_in'] = 0;
						$data['retObj']['product_data'] = NULL;
				}else if($dream_list[0]['unix_now'] >= $dream_list[0]['unix_ontime']){

						//商品結束倒數幾秒
						$dream_end_in = $dream_list[0]['unix_offtime'] - $dream_list[0]['unix_now'];

						$data['retCode'] = 2;
						$data['retMsg'] = "商品下標中 !!";
						$data['retType'] = "MSG";
						$data['retObj']['next_start_in'] = 0;
						$data['retObj']['product_end_in'] = $dream_end_in;
						$data['retObj']['product_data'] = $dream_list[0];
				}else if($dream_list[0]['unix_now'] < $dream_list[0]['unix_ontime']){

						//商品開始倒數幾秒
						$next_start_in = $dream_list[0]['unix_ontime'] - $dream_list[0]['unix_now'];

						$data['retCode'] = 3;
						$data['retMsg'] = "商品即將於 ".$next_start_in." 秒後開始下標 !!";
						$data['retType'] = "MSG";
						$data['retObj']['next_start_in'] = $next_start_in;
						$data['retObj']['product_end_in'] = 0;
						$data['retObj']['product_data'] = $dream_list[0];
				}

				echo json_encode($data,JSON_UNESCAPED_UNICODE);
				exit;
		}
}
