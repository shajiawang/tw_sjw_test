<?php
/*
 * Dream_history Controller 交易紀錄
 */

class DreamHistory {
	public $controller = array();
	public $params = array();
	public $id;
	public $userid = '';

	public function __construct() {
		$this->userid = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
	}

	/*
	 *	圓夢紀錄清單
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$userid				int					會員編號
	 *	$kind				varchar				顯示型態 (all:全部, onbid:競標中, getbid:已得標)
	 *	$p					int					分頁編號
	 */
	public function history_list() {

		global $tpl, $dream_history;

		//設定 Action 相關參數
		set_status($this->controller);

		//殺價紀錄列表
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		login_required();
		//20191004 leo add to fix PHP Warning:  Invalid argument supplied for foreach() in /var/www/html/site/view/dream_history/saja.php on line 9
		$kind=(empty($kind))?'all':$kind;
		if(!empty($kind)){
			switch($kind){
				case "all": 	//全部
					$closed = "";
					break;
				case "onbid":	//競標中
					$closed = "N";
					break;
				case "getbid":	//已結標&&中標者
					$closed = "W";
					break;
			}
			//取得殺價紀錄清單
			$get_list = $dream_history->history_list($userid, $closed);

			$tpl->assign('row_list', $get_list);			
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			//判斷清單是否存在
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					foreach($get_list['table']['record'] as $k=>$v){
						if(empty($v['thumbnail_url']) && !empty($v['filename'])){
							$get_list['table']['record'][$k]['thumbnail_url']=BASE_URL.APP_DIR.'/images/site/product/'.$v['filename'];
						}

						if($v['closed'] == 'Y'){
							$get_list['table']['record'][$k]['txtc'] = '';
							if($v['final_winner_userid'] == $userid){
								$get_list['table']['record'][$k]['bid_mod'] = '恭喜得標';
								$get_list['table']['record'][$k]['bid_status'] ='saja-status-img2'.'.png';
								$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userBidProductList/?productid='. $v['productid'];
								$get_list['table']['record'][$k]['sajago'] = '';

								$ontime = $v['offtime']+60*60*24*3;
								$havtime = $ontime - time();
								$get_list['table']['record'][$k]['closetime'] = $havtime;
							}else{
								$get_list['table']['record'][$k]['bid_mod'] = '已結標';
								$get_list['table']['record'][$k]['bid_status'] ='saja-status-img3'.'.png';
								$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userBidProductList/?productid='. $v['productid'];
								$get_list['table']['record'][$k]['sajago'] = '';
								$get_list['table']['record'][$k]['closetime'] = '';
							}
						}elseif($v['closed'] == 'N'){
							$get_list['table']['record'][$k]['txtc'] = 'style="color:red;"';
							$get_list['table']['record'][$k]['bid_mod'] = '競標中';
							$get_list['table']['record'][$k]['bid_status'] ='saja-status-img1'.'.png';
							$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userOnBidProductList/?productid='. $v['productid'];
							$get_list['table']['record'][$k]['sajago'] = BASE_URL.APP_DIR .'/dream_product/saja/?productid='. $v['productid'];
							$get_list['table']['record'][$k]['closetime'] = '';
						}else{
							$get_list['table']['record'][$k]['txtc'] = '';
							$get_list['table']['record'][$k]['bid_mod'] = '流標';
							$get_list['table']['record'][$k]['bid_status'] ='saja-status-img3'.'.png';
							$get_list['table']['record'][$k]['href'] = BASE_URL.APP_DIR .'/bid/userBidProductList/?productid='. $v['productid'].'&type=NB';
							$get_list['table']['record'][$k]['sajago'] = '';
							$get_list['table']['record'][$k]['closetime'] = '';
						}
					}
					$ret['retObj']['data'] = $get_list['table']['record'];
				}

				$ret['retObj']['page'] = $get_list['table']['page'];
			}else{
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			$tpl->render("dream_history","history_list",true);
		}
	}

		/*
		*	個人商品相關出價資料
		*	$json	 				varchar				瀏覽工具判斷 (Y:APP來源)
		*	$productid				int					商品編號
		*	$userid					int					會員編號
		*	$type					varchar				1.個人下標紀錄 2.所有會員下標紀錄 (預設:1)
		*	$orderby				varchar				排序 1.數字順 2.數字逆 3.時間順 4.時間逆
		*	$p						int					頁碼
		*	$perpage				int					每頁筆數
		*/
		public function history_detail(){
			global $tpl, $dream_history;

			//設定 Action 相關參數
			set_status($this->controller);

			$json = (empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
			$productid = (empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
			$userid = (empty($_SESSION['auth_id']))?$_POST['auth_id']:$_SESSION['auth_id'];
			$p = (empty($_POST['p'])) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
			$perpage = (empty($_POST['perpage'])) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);
			$type = (empty($_POST['type'])) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
			$orderby = (empty($_POST['orderby'])) ? htmlspecialchars($_GET['orderby']) : htmlspecialchars($_POST['orderby']);

			if(empty($type) || $type == 0){
				$type = 1;
			}

			if(empty($orderby) || $orderby == 0){
				$orderby = 1;
			}

			//商品列表
			$product_list = $dream_history->history_detail($productid, $userid, $perpage, $type, $orderby);
			
			if (empty($product_list['table']['record'])){
				$product_list['table']['record'][0]['price'] = '';
				$product_list['table']['record'][0]['count'] = '';
				
				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 20;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));
				$product_list['table']['page'] = $page;
			}
			
			// return $ret;
			$data['retCode'] = 1;
			$data['retMsg'] = "取得資料 !!";
			$data['retType'] = "MSG";
			$data['retObj']['data'] = $product_list['table'];

			echo json_encode($data);
			exit;
		}
	}