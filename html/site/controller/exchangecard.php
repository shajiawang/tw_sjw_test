<?php
/*
 * Exchangecard Controller 殺價商品
 */

class Exchangecard {

	public $controller = array();
	public $params = array();
	public $id;

	/*
   * Action Method : home
   */
	public function home() {

    global $tpl, $config;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$this->userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);

		if($json=='Y'){

			if(empty($this->userid)){
				echo json_encode(getRetJSONArray(-1,'NO_USER_DATA','MSG'));
				exit();
			}
			
			$ret['retCode'] = '1';
			$ret['retMsg'] = 'OK';
			$ret['retType'] = 'LIST';
			$ret['retObj']['category_list'] = array(
				array(
					'seq'		=> '0',
					'eccid'		=> '0',
					'name'		=> '票卷/卡片'
				),array(
					'seq'		=> '1',
					'eccid'		=> '1',
					'name'		=> '已使用'
				)
			);

			echo json_encode($ret);
			exit;
		}
	}
	
	/*
   * Action Method : exchange_card_list
   */
	public function exchange_card_list(){

		global $tpl, $config, $member, $exchangecard;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$eccid = empty($_POST['eccid']) ? htmlspecialchars($_GET['eccid']) : htmlspecialchars($_POST['eccid']);
		$page_number = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
		$this->userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);

		if($json=='Y'){

			if(empty($this->userid)){
				echo json_encode(getRetJSONArray(-1,'NO_USER_DATA','MSG'));
				exit();
			}

			$get_member = $member->get_info($this->userid);
			//檢查會員資料
			if(empty($get_member)){
				echo json_encode(getRetJSONArray(-1,'AUTH_ID_ERROR','MSG'));
				exit();

			}else{
				if (empty($eccid)) {
					$eccid='0';
				}
				//取兌換卡片記錄
				$card = $exchangecard->exchange_card_list($this->userid, $eccid, $page_number);

				$data = getRetJSONArray(1,'OK','LIST');

				if(!empty($card)){
					$data['retObj']['data'] = $card['table']['record'];
					$data['retObj']['page'] = $card['table']['page'];
				}else{
					$page['rec_start'] = 0;
					$page['totalcount'] = 0;
					$page['totalpages'] = 1;
					$page['perpage'] = 50;
					$page['page'] = 1;
					$page['item'] = array(array("p" => 1 ));
		
					$data['retObj']['data'] = array();
					$data['retObj']['page'] = $page;
				}
				$data['retObj']['nowmicrotime'] = microtime(true);
			}

			echo json_encode($data);
			exit;
		}
	}

	/*
   * Action Method : exchange_card_list
   */
	public function exchange_card_info(){

		global $tpl, $config, $member, $exchangecard;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$cid = empty($_POST['cid']) ? htmlspecialchars($_GET['cid']) : htmlspecialchars($_POST['cid']);
		$orderid = empty($_POST['orderid']) ? htmlspecialchars($_GET['orderid']) : htmlspecialchars($_POST['orderid']);
		$this->userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);

		if($json=='Y'){

			if(empty($cid)){
				echo json_encode(getRetJSONArray(-1,'NO_CID_DATA','MSG'));
				exit();
			}

			if(empty($orderid)){
				echo json_encode(getRetJSONArray(-1,'NO_ORDERID_DATA','MSG'));
				exit();
			}

			if(empty($this->userid)){
				echo json_encode(getRetJSONArray(-1,'NO_USER_DATA','MSG'));
				exit();
			}

			$get_member = $member->get_info($this->userid);
			//檢查會員資料
			if(empty($get_member)){
				echo json_encode(getRetJSONArray(-1,'AUTH_ID_ERROR','MSG'));
				exit();

			}else{

				//取兌換卡片詳細資料
				if ($cid == -1) {
					$card_info = $exchangecard->exchange_card_info_orderid($this->userid, $orderid);
				}else{
					$card_info = $exchangecard->exchange_card_info($this->userid, $cid);
				}
				
				if(!empty($card_info)){
					$data = getRetJSONArray(1,'OK','LIST');
					$data['retObj'] = $card_info['table']['record'][0];
				}else{
					$data = getRetJSONArray(-1,'NO_CID_DATA','MSG');
				}
			}

			echo json_encode($data);
			exit;
		}
	}

	/*
   * Action Method : exchange_card_list
   */
	public function update_card_invalid(){

		global $tpl, $config, $member, $exchangecard;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$cid = empty($_POST['cid']) ? htmlspecialchars($_GET['cid']) : htmlspecialchars($_POST['cid']);
		$this->userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);

		if($json=='Y'){

			if(empty($cid)){
				echo json_encode(getRetJSONArray(-1,'NO_CID_DATA','MSG'));
				exit();
			}

			if(empty($this->userid)){
				echo json_encode(getRetJSONArray(-1,'NO_USER_DATA','MSG'));
				exit();
			}

			$get_member = $member->get_info($this->userid);
			//檢查會員資料
			if(empty($get_member)){
				echo json_encode(getRetJSONArray(-1,'AUTH_ID_ERROR','MSG'));
				exit();

			}else{
				//取兌換卡片詳細資料
				$card_info = $exchangecard->exchange_card_info($this->userid, $cid);

				if(!empty($card_info)){
					
					//將卡片更新為失效
					$res = $exchangecard->update_exchange_card($this->userid, $cid);

					if($res){
						$data = getRetJSONArray(1,'UPDATE_SUCCESS','MSG');
					}else{
						$data = getRetJSONArray(-1,'UPDATE_FAILED','MSG');
					}

				}else{
					$data = getRetJSONArray(-1,'CARD_NOT_EXIST','MSG');
				}
				$data['retObj']['nowmicrotime'] = microtime(true);
			}

			echo json_encode($data);
			exit;
		}
	}
}
