<?php
/*
 * Share_oscode Controller 分享送殺價卷
 */

class LbOscode {

	public $controller = array();
	public $params = array();
	public $id;

	/*
	 * 首頁-領殺價卷說明頁.
	 */
	public function home() {

		global $db, $config, $tpl, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		//推薦人
		$user_src = empty($_POST['user_src']) ? htmlspecialchars($_GET['user_src']) : htmlspecialchars($_POST['user_src']);
		//商品編號
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		//推薦來源
		$behav = empty($_POST['behav']) ? htmlspecialchars($_GET['behav']) : htmlspecialchars($_POST['behav']);
		//直播主編號
		$lbuserid = empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);

		//返設定回控制器與方法位置
		$to = "/site/shareoscode/oauth_return_data_process";

		//贈送管道(活動)
		$promote_channel = "product";

		//組合傳送參數
		$state = $user_src."|".$productid."|".$behav."|".$lbuserid."|".$promote_channel;

		//傳送參數編碼
		$state = base64_encode(json_encode($state));

		//取得商品資料
		$product_data = $product->getProductById($productid);

		$product_pic = $product_data['thumbnail2'];//"https://www.saja.com.tw/site/images/site/product/6f4e660f6c5c1fb32db98c985fc0c572.png";
		$product_name = $product_data['name'];//"無敵破壞王電影票一張";
		$oscode_num = "1";
		$product_rule = $product_data['rule'];//"此下標券只可使用在此檔商品！";
		
		$ret=getRetJSONArray(1,'OK','LIST');
		
		$ret['retObj']['data']['to'] = $to;
		$ret['retObj']['data']['state'] = $state;
		$ret['retObj']['data']['product_pic'] = $product_pic;
		$ret['retObj']['data']['product_name'] = $product_name;
		$ret['retObj']['data']['oscode_num'] = $oscode_num;
		$ret['retObj']['data']['product_rule'] = $product_rule;
		
		echo json_encode($ret);
		exit;		
	}


	/*
	 * 活動首頁-活動說明頁.
	 */
	public function activityhome() {

		global  $tpl, $share;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		//推薦人
		$user_src	= empty($_POST['user_src']) ? htmlspecialchars($_GET['user_src']) : htmlspecialchars($_POST['user_src']);
		//直播主編號
		$lbuserid	= empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);
		//推薦來源
		$behav	= empty($_POST['behav']) ? htmlspecialchars($_GET['behav']) : htmlspecialchars($_POST['behav']);

		if(empty($behav)||$behav ==""){
			$behav = 'lb';
		}

		//頁面自動開啟fb分享頁面(Y:開啟 N:不開啟)
		//$open_fb_share	= empty($_POST['open_fb_share']) ? htmlspecialchars($_GET['open_fb_share']) : htmlspecialchars($_POST['$open_fb_share']);

		//使用者帳號
		$user_auth_id = $_SESSION['auth_id'];

		//取得當前系統時間(timestamp格式)
		$nowtime_timestamp = time();
		//取得今日日期(date格式)
		$today = date("Y-m-d",$nowtime_timestamp);

		//**當天活動結束改為顯示隔天活動功能**
		// //取得明天日期(date格式)
		// $today_temp = date_create($today);
		// date_add($today_temp,date_interval_create_from_date_string("1 days"));
		// $nextday = date_format($today_temp,"Y-m-d");
		//
		// //今日活動結束時間(date格式)
		// $activity_endtime = $today." 23:00:00";
		// //今日活動結束時間(timestamp格式)
		// $activity_endtime_timestamp=strtotime($activity_endtime);
		//
		// //判斷今日活動時間是否已結束如已結束顯示明天商品
		// if($nowtime_timestamp < $activity_endtime_timestamp){
		// 	//活動結標開始時間
		// 	$sdate = $today;
		// 	//活動結標結束時間
		// 	$edate = $today;
		// }else{
		// 	//活動結標開始時間
		// 	$sdate = $nextday;
		// 	//活動結標結束時間
		// 	$edate = $nextday;
		// }

		//**顯示今天到4天後的活動功能**

		//取得4天後日期(date格式)
		$today_temp = date_create($today);
		date_add($today_temp,date_interval_create_from_date_string("4 days")); //設定結束日期 今天加幾天
		$last_date = date_format($today_temp,"Y-m-d");

		//設定顯示開始結束日期
		$sdate = "";//$today;
		$edate = $last_date;

		//取得活動檔次目錄與商品清單
		$promote_menu = json_encode($share->getLbScodePromoteMenu($sdate,$edate,$lbuserid));

		//取得使用者已領過殺價卷商品編號與張數
		$user_already_taken_oscode = json_encode($share->getUserAlreadyTakenOscode($user_auth_id,$behav,$sdate,$edate));

		$ret = getRetJSONArray(1,'OK','LIST');
		
		$ret['retObj']['data']['user_auth_id'] = $user_auth_id;
		$ret['retObj']['data']['user_src'] = $user_src;
		$ret['retObj']['data']['behav'] = $behav;
		$ret['retObj']['data']['lbuserid'] = $lbuserid;
		$ret['retObj']['data']['promote_menu'] = $promote_menu;
		$ret['retObj']['data']['user_already_taken_oscode'] = $user_already_taken_oscode;
		
		echo json_encode($ret);
		exit;
	}


	/*
	 * 取得活動殺價券
	 */
	public function getActivityPromoteScode() {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		//推薦人
		$user_src = empty($_POST['user_src']) ? htmlspecialchars($_GET['user_src']) : htmlspecialchars($_POST['user_src']);
		//商品編號
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		//推薦來源
		$behav = empty($_POST['behav']) ? htmlspecialchars($_GET['behav']) : htmlspecialchars($_POST['behav']);
		//直播主編號
		$lbuserid = empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);
		//返設定回控制器與方法位置
		$to = "/site/shareoscode/oauth_return_data_process";

		//贈送管道(活動)
		$promote_channel = "activity";

		//組合傳送參數
		$state = $user_src."|".$productid."|".$behav."|".$lbuserid."|".$promote_channel;

		//傳送參數編碼
		$state = base64_encode(json_encode($state));

		$send_url = BASE_URL.APP_DIR."/oauthapi/r/?_to=".$to."&state=".$state;

		header('Location: '.$send_url);
		return;

	}


	/*
	 * 第三方登入回參
	 */
	public function oauth_return_data_process(){

		global $tpl, $share;

		//設定 Action 相關參數
		set_status($this->controller);

		//取得傳送參數
		$state	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);

		//傳送參數解碼
		$state_decode = json_decode(base64_decode($state));

		$state_arr = explode("|", $state_decode);

		$user_src = $state_arr[0];//推薦人
		$productid = $state_arr[1];//商品編號
		$behav = $state_arr[2];//推薦來源
		$lbuserid = $state_arr[3];//直播主編號
		$promote_channel = $state_arr[4];	//贈送管道(活動)

		if($behav == "lb"){
			$get_oscode_result = $this->give_broadcast_oscode($user_src,$productid,$lbuserid);
		}else{
			echo "無此分享類別";
			return;
		}

		$get_oscode_result['behav'] = $behav;
		$get_oscode_result['userid'] = $_SESSION['auth_id'];
		$get_oscode_result['lbuserid'] = $lbuserid;
		$get_oscode_result['user_src'] = $user_src;

		if($promote_channel == "product"){//單一商品結束頁
			$this->endpage($get_oscode_result,"N");
		}else if($promote_channel == "activity"){//活動清單結束頁
			$this->endpage2($get_oscode_result,"N");
		}else{
			echo json_encode($get_oscode_result);
		}

		return;

	}

	/*
	 * 直播送殺價卷
	 */
	public function give_broadcast_oscode($user_src,$productid,$lbuserid) {

		global $db, $config, $broadcast, $product;

		//設定 Action 相關參數
		set_status($this->controller);
		$userid = $_SESSION['auth_id'];

		//未登入
		if(empty($userid)){
			$ret['retCode']=-2;
			$ret['retMsg']= "未登入請先登入 !!";
			$ret['num'] = '0';									// 送卷數量
			$ret['productid'] = $productid;  					// 商品編號
			$ret['prodname'] = "";								// 商品名稱
			$ret['atname'] = "";								// 活動名稱
			return $ret;
		}

		//檢查參數是否傳送
		if(empty($lbid) && empty($lbuserid) && empty($productid)){
			$ret['retCode']=-3;
			$ret['retMsg']= "無效的參數 !!";
			$ret['num'] = '0';											// 送卷數量
			$ret['productid'] = $productid;  								// 商品編號
			$ret['prodname'] = "";				// 商品名稱
			$ret['atname'] = "";						// 活動名稱
		}else{
			// 直播殺價卷活動資料取得
			$arrPromotes = $broadcast->getScodePromoteList($lbuserid, $productid);
			$num_received_oscode = 0;
			$arrProductids = array();

			// 判斷是否有直播殺價卷活動資料
			if($arrPromotes[0]){
				$spid = $arrPromotes[0]['spid'];
				$onum = $arrPromotes[0]['onum'];
				$productid = $arrPromotes[0]['productid'];

				// 檢查是否領過(lb直播)送的殺價券
				$received = $broadcast->getUserOscode($userid,$spid,'lb');

				if($received[0]['count']>0){
					 $ret['retCode']=-1;
					 $ret['retMsg']= "您已領取過殺價券 !!";
					 $ret['num'] = '0';											// 送卷數量
					 $ret['productid'] = $productid;  								// 商品編號
					 $ret['prodname'] = $arrPromotes[0]['product_name'];				// 商品名稱
					 $ret['atname'] = $arrPromotes[0]['name'];						// 活動名稱
				}else{
					if($onum>0){
						//直播主分享送卷
						for($j=0;$j<$onum;++$j){
							$broadcast->insertUserOscode($userid, $spid, $productid, $lbuserid,'lb',0);
						}
						if($spid>0){
							$spupdate = $broadcast->updateScodePromote($onum, $spid);
						}
						//使用者二次分享送卷
						if($user_src !="" && ($user_src != $userid)){
							for($k=0;$k<$onum;++$k){
								$broadcast->insertUserOscode($user_src, $spid, $productid, $lbuserid,'lb',$userid);
							}
							if($spid>0){
								$spupdate = $broadcast->updateScodePromote($onum, $spid);
							}
						}

					}

					$ret['retCode'] = 1;
					$ret['retMsg'] = "成功領取 ".$onum." 張殺價券 !!";
					$ret['num'] = $onum;											// 送卷數量
					$ret['productid'] = $productid;  								// 商品編號
					$ret['prodname'] = $arrPromotes[0]['product_name'];				// 商品名稱
					$ret['atname'] = $arrPromotes[0]['name'];						// 活動名稱
				}
			}else{
				$ret = array();
				$ret['retCode'] = 0;
				$ret['retMsg'] = "查無此活動 !!";
				$ret['num'] = '0';													// 送卷數量
				$ret['productid'] = $productid;  									// 商品編號
				$ret['prodname'] = $arrPromotes[0]['product_name'];					// 商品名稱
				$ret['atname'] = $arrPromotes[0]['name'];							// 活動名稱
			}
		}
		return $ret;
	}


	/*
	 * 殺價卷贈送結果
	 */
	public function endpage($get_oscode_result,$json = 'N') {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$data['behav'] = $get_oscode_result['behav'];
		$data['retCode'] = $get_oscode_result['retCode'];
		$data['retMsg'] = $get_oscode_result['retMsg'];
		$data['oscode_num'] = $get_oscode_result['num'];
		$data['product_id'] = $get_oscode_result['productid'];
		$data['product_name'] = $get_oscode_result['prodname'];
		$data['product_pic'] = "";
		$data['activity_name'] = $get_oscode_result['atname'];
		$data['lbuserid'] = $get_oscode_result['lbuserid'];
		$data['userid'] = $get_oscode_result['userid'];
		$data['user_src'] = $get_oscode_result['user_src'] ;

		//fb分享參數寫入給直播下標結果頁顯示
		$_SESSION['fb_share_link_data']['userid'] = $data['userid'];
		$_SESSION['fb_share_link_data']['productid'] = $data['product_id'];
		$_SESSION['fb_share_link_data']['behav'] = $data['behav'];
		$_SESSION['fb_share_link_data']['lbuserid'] = $data['lbuserid'];

		echo json_encode($data,JSON_UNESCAPED_UNICODE);
		exit;
	}


	/*
	 * 殺價卷贈送結果
	 */
	public function endpage2($get_oscode_result,$json = 'N') {

		global $tpl, $product, $share;

		//設定 Action 相關參數
		set_status($this->controller);

		$data = array();

		$data['behav'] = $get_oscode_result['behav'];
		$data['retCode'] = $get_oscode_result['retCode'];
		$data['retMsg'] = $get_oscode_result['retMsg'];
		$data['oscode_num'] = $get_oscode_result['num'];
		$data['product_id'] = $get_oscode_result['productid'];
		$data['product_name'] = $get_oscode_result['prodname'];

		//取得商品資料
		$product_data = $product->getProductById($data['product_id'],"","N");

		// echo json_encode($product_data);
		// die();

		$data['product_pic'] = $product_data['thumbnail2'];//"https://www.saja.com.tw/site/images/site/product/6f4e660f6c5c1fb32db98c985fc0c572.png";
		$data['product_offtime'] = $product_data['product_offtime'];
		$data['product_display'] = $product_data['display'];
		$data['product_closed'] = $product_data['closed'];

		$data['activity_name'] = $get_oscode_result['atname'];
		$data['lbuserid'] = $get_oscode_result['lbuserid'];
		$data['userid'] = $get_oscode_result['userid'];
		$data['user_src'] = $get_oscode_result['user_src'] ;

		//取得當前系統時間(timestamp格式)
		$nowtime_timestamp = time();
		//取得今日日期(date格式)
		$today = date("Y-m-d",$nowtime_timestamp);

		//**當天活動結束改為顯示隔天活動功能**
		// //取得明天日期(date格式)
		// $today_temp = date_create($today);
		// date_add($today_temp,date_interval_create_from_date_string("1 days"));
		// $nextday = date_format($today_temp,"Y-m-d");
		//
		// //今日活動結束時間(date格式)
		// $activity_endtime = $today." 23:00:00";
		// //今日活動結束時間(timestamp格式)
		// $activity_endtime_timestamp=strtotime($activity_endtime);
		//
		// //判斷今日活動時間是否已結束如已結束顯示明天商品
		// if($nowtime_timestamp < $activity_endtime_timestamp){
		// 	//活動結標開始時間
		// 	$sdate = $today;
		// 	//活動結標結束時間
		// 	$edate = $today;
		// }else{
		// 	//活動結標開始時間
		// 	$sdate = $nextday;
		// 	//活動結標結束時間
		// 	$edate = $nextday;
		// }

		//**顯示今天到4天後的活動功能**

		//取得4天後日期(date格式)
		$today_temp = date_create($today);
		date_add($today_temp,date_interval_create_from_date_string("4 days")); //設定結束日期 今天加幾天
		$last_date = date_format($today_temp,"Y-m-d");

		//設定顯示開始結束日期
		$sdate = "";//$today;
		$edate = $last_date;

		//取得活動檔次目錄與商品清單
		$data['promote_menu'] = json_encode($share->getLbScodePromoteMenu($sdate,$edate,$data['lbuserid']));

		//取得使用者已領過殺價卷商品編號與張數
		$data['$user_already_taken_oscode'] = json_encode($share->getUserAlreadyTakenOscode($data['userid'],$data['behav'],$sdate,$edate));
		//echo $user_already_taken_oscode;

		// echo json_encode($data);
		// die();

		//fb分享參數寫入給直播下標結果頁顯示
		$_SESSION['fb_share_link_data']['userid'] = $data['userid'];
		$_SESSION['fb_share_link_data']['productid'] = $data['product_id'];
		$_SESSION['fb_share_link_data']['behav'] = $data['behav'];
		$_SESSION['fb_share_link_data']['lbuserid'] = $data['lbuserid'];

		echo json_encode($data,JSON_UNESCAPED_UNICODE);
		exit;
	}


	/*
	 * 檢查系統時間(輸出db與ap時間)
	 */
	public function time_check() {

		global $db, $config;
		//取得db時間
		$query = "SELECT NOW() as db_time";
		$table = $db->getQueryRecord($query);
		//輸出db時間
		echo "db_time:".$table['table']['record'][0]['db_time']."</br>";

		//取得當前系統時間(timestamp格式)
		$nowtime_timestamp = time();
		//取得今日日期(date格式)
		$today = date("Y-m-d H:i:s",$nowtime_timestamp);
		//輸出ap時間
		echo "ap_time:".$today."</br>";

		return;

	}


	/*
	 * 工程測試用(傳送參數解碼顯示)
	 */
	public function trans_value_decode_eng_test() {

		$state = 'Inw4NTc4fGxifHxhY3Rpdml0eSI%3D';//無
		//$state = 'Inw4NTc4fGxifDJ8YWN0aXZpdHki';//有

		//傳送參數解碼
		//echo $state_decode = json_decode(base64_decode($state));
		echo $state_decode = (base64_decode($state));
		echo "</br>";
		$state_arr = explode("|", $state_decode);

		echo $user_src = $state_arr[0];//推薦人
		echo "</br>";
		echo $productid = $state_arr[1];//商品編號
		echo "</br>";
		echo $behav = $state_arr[2];//推薦來源
		echo "</br>";
		echo $lbuserid = $state_arr[3];//直播主編號
		echo "</br>";
		echo $promote_channel = $state_arr[4];	//贈送管道(活動)
		echo "</br>";

		return;
	}


	/*
	 * 一鍵取得活動殺價券
	 */
	public function one_click_promote_scode() {

		global $tpl;

		//返設定回控制器與方法位置
		$to = "/site/shareoscode/one_click_promote_scode_oauth_return_data_process";

		//傳送參數編碼
		$state = base64_encode(json_encode($state));

		//導向登入
		$send_url = BASE_URL.APP_DIR."/oauthapi/r/?_to=".$to;
		//echo  $send_url;
		header('Location: '.$send_url);

	}


	/*
	 * 一鍵取得活動殺價券接第三方登入回參處理
	 */
	public function one_click_promote_scode_oauth_return_data_process() {

		global  $tpl, $share;

		//設定 Action 相關參數
		set_status($this->controller);

		//頁面顯示輸出列印css設定
		echo"<style>
		       body{
		       	 font-size:24pt;
		       }
		       table, th, td {
             border: 1px solid black;
           }
					 table{
					 	 width: 100vw;
					 }
					 th, td{
					   font-size:24pt;
					 }
		     </style>";

		//檢查是否登入
		if(!empty($_SESSION['auth_id'])){

			//輸出使用者代號
			echo "使用者代碼:   ".$_SESSION['auth_id']."</br>";
			echo "</br>";

			$user_src = "";//推薦人
			$lbuserid = '2';//直播主編號

			//取得當前系統時間(timestamp格式)
			$nowtime_timestamp = time();
			//取得今日日期(date格式)
			$today = date("Y-m-d",$nowtime_timestamp);

			//**當天活動結束改為顯示隔天活動功能**
			// //取得明天日期(date格式)
			// $today_temp = date_create($today);
			// date_add($today_temp,date_interval_create_from_date_string("1 days"));
			// $nextday = date_format($today_temp,"Y-m-d");
			//
			// //今日活動結束時間(date格式)
			// $activity_endtime = $today." 23:00:00";
			// //今日活動結束時間(timestamp格式)
			// $activity_endtime_timestamp=strtotime($activity_endtime);
			//
			// //判斷今日活動時間是否已結束如已結束顯示明天商品
			// if($nowtime_timestamp < $activity_endtime_timestamp){
			// 	//活動結標開始時間
			// 	$sdate = $today;
			// 	//活動結標結束時間
			// 	$edate = $today;
			// }else{
			// 	//活動結標開始時間
			// 	$sdate = $nextday;
			// 	//活動結標結束時間
			// 	$edate = $nextday;
			// }

			//**顯示今天到4天後的活動功能**

			//取得4天後日期(date格式)
			$today_temp = date_create($today);
			date_add($today_temp,date_interval_create_from_date_string("4 days")); //設定結束日期 今天加幾天
			$last_date = date_format($today_temp,"Y-m-d");

			//設定顯示開始結束日期
			$sdate = "";//$today;
			$edate = $last_date;


			//手動設定商品清單與總數
			// $num = 8;//商品總數
			// $productid['1']= 8568;//第一檔商品
			// $productid['2']= 8570;//第二檔商品
			// $productid['3']= 8573;//第三檔商品
			// $productid['4']= 8575;//第四檔商品
			// $productid['5']= 8578;//第五檔商品
			// $productid['6']= 8580;//第六檔商品
			// $productid['7']= 8583;//第七檔商品
			// $productid['8']= 8585;//第八檔商品

			//自動取得商品清單清單與總數
			//取得活動檔次目錄與商品清單
			$promote_menu = $share->getLbScodePromoteMenu($sdate,$edate,$lbuserid);
			//echo json_encode($promote_menu);
			//活動總檔次計算
			$activity_count = count($promote_menu);
			//echo $activity_count;

			//取得所有活動商品編號
			foreach ($promote_menu as $key => $value) {
				$productid[$key] = $promote_menu[$key]['productid'];
			}
			//echo json_encode($productid);

			//商品總數設定
			$num = $activity_count;


			//表單輸出
			echo"<table>";

			echo"<tr>";
			echo"<th>項目</th>";
			echo"<th>商品編號</th>";
			echo"<th>商品名稱</th>";
			echo"<th>數量</th>";
			echo"<th>贈送結果</th>";
			echo"</tr>";

			for($i=0;$i<$num;$i++){
				//送券
				$get_oscode_result = $this->give_broadcast_oscode($user_src,$productid[$i],$lbuserid);
				//結果列印
				echo "<tr><th>".($i+1)."</th><th>".$get_oscode_result['productid']."</th><th>".$get_oscode_result['prodname']."</th><th>".$get_oscode_result['num']."</th><th>".$get_oscode_result['retMsg']."</th></tr>";

			}

			echo "</table>";
			return;
		}

		echo"未登入 請點擊下列按鈕再試一次!!"."</br>";
		echo"<input type=\"button\" value=\"再試一次\" onclick=\"location.href='".BASE_URL.APP_DIR."/shareoscode/one_click_promote_scode'\">";
		return;

	}


	/*
	 * 分享登入
	 */
	public function share_login() {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		//推薦人
		$user_src	= empty($_POST['user_src']) ? htmlspecialchars($_GET['user_src']) : htmlspecialchars($_POST['user_src']);
		//推薦來源
		$behav	= empty($_POST['behav']) ? htmlspecialchars($_GET['behav']) : htmlspecialchars($_POST['behav']);
		//直播主編號
		$lbuserid	= empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);

		//返設定回控制器與方法位置
		$to = "/site/shareoscode/share_login_oauth_return_data_process";

		//組合傳送參數
		$state = $user_src."|".$behav."|".$lbuserid;

		//傳送參數編碼
		$state_json_encode =json_encode($state);
		$state_base64_encode = base64_encode($state_json_encode);

		//導向登入
		$send_url = BASE_URL.APP_DIR."/oauthapi/r/?_to=".$to."&state=".$state_base64_encode;

		header('Location: '.$send_url);
		return;

	}


	/*
	 * 分享登入第三方回參處理
	 */
	public function share_login_oauth_return_data_process() {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		//取得傳送參數
		$state	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);

		//傳送參數解碼
		$state_base64_decode = base64_decode($state);
		$state_json_decode = json_decode($state_base64_decode);

		// echo $state_base64_decode."</br>";

		//取得參數
		$state_arr = explode("|", $state_json_decode);
		$user_src = $state_arr[0];//推薦人
		$behav = $state_arr[1];//推薦來源
		$lbuserid = $state_arr[2];//直播主編號

		//回活動頁
		$send_url = BASE_URL.APP_DIR."/shareoscode/activityhome/?user_src=".$user_src."&behav=".$behav."&lbuserid=".$lbuserid;

		header('Location: '.$send_url);

		return;

	}


	/*
	 * 通關密語送券登入
	 */
	public function share_magic_word_login() {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		//推薦人
		$user_src	= empty($_POST['user_src']) ? htmlspecialchars($_GET['user_src']) : htmlspecialchars($_POST['user_src']);
		//推薦來源
		$behav	= empty($_POST['behav']) ? htmlspecialchars($_GET['behav']) : htmlspecialchars($_POST['behav']);
		//直播主編號
		$lbuserid	= empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);

		//返設定回控制器與方法位置
		$to = "/site/shareoscode/share_login_oauth_return_data_process";

		//組合傳送參數
		$state = $user_src."|".$behav."|".$lbuserid;

		//echo $state."</br>";

		//傳送參數編碼
		$state_json_encode =json_encode($state);
		$state_base64_encode = base64_encode($state_json_encode);

		//導向登入
		$send_url = BASE_URL.APP_DIR."/oauthapi/r/?_to=".$to."&state=".$state_base64_encode;

		//echo $send_url."</br>";

		header('Location: '.$send_url);
		return;

	}


	/*
	 * 通關密語送券登入第三方回參處理
	 */
	public function share_magic_word_login_oauth_return_data_process() {

		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		//取得傳送參數
		$state	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);

		//傳送參數解碼
		$state_base64_decode = base64_decode($state);
		$state_json_decode = json_decode($state_base64_decode);

		// echo $state_base64_decode."</br>";

		//取得參數
		$state_arr = explode("|", $state_json_decode);
		$user_src = $state_arr[0];//推薦人
		$behav = $state_arr[1];//推薦來源
		$lbuserid = $state_arr[2];//直播主編號

		//回活動頁
		$send_url = BASE_URL.APP_DIR."/shareoscode/activityhome/?user_src=".$user_src."&behav=".$behav."&lbuserid=".$lbuserid."&open_fb_share=Y";

		//echo $send_url."</br>";

		header('Location: '.$send_url);

		return;

	}

}
