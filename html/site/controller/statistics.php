<?php
/*
 * Statistics Controller
 * 數據分析
 */

class statistics {

	public $controller = array();
	public $params = array();
  public $id;

	/*
	*
	*/
  public function home() {
	}

	/*
	* 記錄user使用裝置
	*	$auth_id			int			會員編號
	*	$device		chr			裝置類型
	*/
	public function set_user_device() {

			global $db, $config, $statisticsModel;

			//設定 Action 相關參數
			set_status($this->controller);

			$userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);
			$device = empty($_POST['device']) ? htmlspecialchars($_GET['device']) : htmlspecialchars($_POST['device']);

			if(empty($userid) || empty($device)) {
				$ret['retCode']=-1;
				$ret['retMsg']= "帳號或裝置類型未傳送";
				$ret['retType']= "MSG";
				echo json_encode($ret, JSON_UNESCAPED_UNICODE);
				return;
			}

			$user_device_info = $statisticsModel->get_user_device($userid);

			if($user_device_info == "FALSE") {

					$result = $statisticsModel->insert_user_device($userid, $device);
					if($result == 'TRUE'){
						$ret['retCode']= 1;
						$ret['retMsg']= "新增成功";
						$ret['retType']= "MSG";
					}else{
						$ret['retCode']= -2;
						$ret['retMsg']= "無此裝置類型無法新增";
						$ret['retType']= "MSG";
					}
			}else{

					$result = $statisticsModel->update_user_device($userid, $device);
					if($result == 'TRUE'){
						$ret['retCode']= 2;
						$ret['retMsg']= "記錄已更新";
						$ret['retType']= "MSG";
					}else{
						$ret['retCode']= -3;
						$ret['retMsg']= "無此裝置類型無法更新";
						$ret['retType']= "MSG";
					}
			}

			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			return ;

	}

	/*
	* 取得user使用裝置統計資料
	*/
	public function get_user_device_count() {

			global $db, $config, $statisticsModel;

			//設定 Action 相關參數
			set_status($this->controller);

			$user_device_count = $statisticsModel->count_user_device();

			if($user_device_count == "FALSE") {

					$ret['retCode']= -1;
					$ret['retMsg']= "無使用者裝置記錄";

			}else{

					$ret['retCode']= 1;
					$ret['retMsg']= "統計完成";
					$ret['retType']= "MSG";
					$ret['retObj']= $user_device_count;
			}

			echo json_encode($ret, JSON_UNESCAPED_UNICODE);
			return ;

	}

	/*
	* 紀錄老人回娘家活動來源
	*/
	public function set_activity_source(){

		global $db, $config, $statisticsModel;

		//設定 Action 相關參數
		set_status($this->controller);

		$source = empty($_POST['source']) ? htmlspecialchars($_GET['source']) : htmlspecialchars($_POST['source']);
		
		if(empty($source)){
			$source = 'banner';
		}

		if(preg_match("/^09[0-9]{8}$/", $source)) {
			$source_num = 2;
			$phone = $source;
		}else if($source == 'banner'){
			$source_num = 1;
			$phone = 0;
		}else if($source == 'line'){
			$source_num = 3;
			$phone = 0;
		}else if($source == 'fb'){
			$source_num = 4;
			$phone = 0;
		}else{
			$source_num = 1;
			$phone = 0;
		}

		$statisticsModel->set_activity_source($source_num, $phone);

		echo json_encode(getRetJSONArray(1,'SUCCESS','MSG'));
		exit();
	}

}
