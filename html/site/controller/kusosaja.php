<?php
/*
 * Kusosaja Controller
 */

include_once(LIB_DIR ."/convertString.ini.php");
include_once(LIB_DIR."/vendor/autoload.php"); // 載入wss client 套件(composer)
class Kusosaja {

	public $controller = array();
	public $params = array();
	public $id;


	/*
   * Default home page. vendorid
   */
  public function home($productid='', $user_src='',$hostid='') {

		global $db, $config, $tpl, $broadcast;
		//設定 Action 相關參數
		set_status($this->controller);

		// 產品編號
		if(empty($productid)){
		  $productid  = htmlspecialchars($_REQUEST['productid']);
		}
		if(!empty($productid))
           $state['productid']=$productid;

		//閃殺群組 (可當商店id)
		if(empty($hostid)){
		  $hostid  = htmlspecialchars($_REQUEST['hostid']);
		}
		if(!empty($hostid))
           $state['hostid']=$hostid;

		// 推薦人的userid
		if(empty($user_src)){
		  $user_src  = htmlspecialchars($_REQUEST['user_src']);
		}
		if(!empty($user_src))
		   $state['user_src']=$user_src;

		error_log("[c/kusosaja/home] userid : ".$_SESSION['auth_id']);
		if(!empty($_SESSION['auth_id'])){
		  $this->play($productid, $_REQUEST['hostid']);
		  return;
		}

		// 用helper內的 function browserType
		$browserType=browserType();
		$base_url=BASE_URL;
		$state['callback_url']=$base_url.APP_DIR."/kusosaja/regLogin/";

		// 抓來源IP
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
			$state['ip'] = $temp_ip[0];
		} else {
			$state['ip'] = $_SERVER['REMOTE_ADDR'];
		}

		switch($browserType){
		  case 'weixin':
		  case 'fb' :
		            $url="https://oauth.saja.vip/site/oauth/fb/getFbOauthCode.php?state=".base64_encode(json_encode($state));
	                error_log("[c/oauthapi/r] FB auth url :".$url);
					header("Location:".$url);
		            break;
		  case 'line' :
		  default :
		            $url=$base_url."/site/oauth/line/getLineOauthCode.php?state=".base64_encode(json_encode($state));
		            error_log("[kusosaja/home] Line auth url :".$url);
					header("Location:".$url);
	                break;
        }
        return ;
  }


  public function regLogin() {

		global $db, $config, $tpl, $usermodel, $broadcast;

		$auth_by	= empty($_POST['auth_by']) ? htmlspecialchars($_GET['auth_by']) : htmlspecialchars($_POST['auth_by']);
		$sso_uid	= empty($_POST['sso_uid']) ? htmlspecialchars($_GET['sso_uid']) : htmlspecialchars($_POST['sso_uid']);
		$state   	= empty($_POST['state']) ? htmlspecialchars($_GET['state']) : htmlspecialchars($_POST['state']);
		$data 		= empty($_POST['data']) ? htmlspecialchars($_GET['data']) : htmlspecialchars($_POST['data']);
		$sso_data 	= json_decode(urldecode(base64_decode($data)), true);

		error_log("[c/kusosaja/regLogin] data : ".urldecode(base64_decode($data)));
		$arrState =  json_decode(urldecode(base64_decode($state)), true);
		$user_src=$arrState['user_src'];
		$productid=$arrState['productid'];
		$hostid=$arrState['hostid'];
		$ts=str_replace(".","",microtime(true));
		$arrSSO = array();
		switch($auth_by){
				case 'fb':
									 $arrSSO = array(
																  'auth_by' 		=> $auth_by,
																  'sso_data' 		=> urldecode(base64_decode($data)),
																  'state' 		=> $state,
																  'nickname' 		=> addslashes($sso_data['name']),
																  'headimgurl'    => ($sso_data['picture']['url']),
																  'sso_name' 		=> "fb",
																  'phone'         => "fb".$ts,
																  'type' 			=> "sso",
																  'json'          => "Y",
																  'gender'        => "N/A",
																  'sso_uid' 		=> $sso_uid,
																  'sso_uid2' 		=> "",
																  'user_src'      => $user_src
										);
										break;
				case 'line':
									   unset($sso_data['statusMessage']);
									   $sso_data['displayName']=emoji_remove($sso_data['displayName']);
									   $arrSSO = array(
																	  'auth_by' 		=> $auth_by,
																	  'sso_data' 		=> urlencode(json_encode($sso_data)),
																	  'state' 		=> $state,
																	  'nickname' 		=> $sso_data['displayName'],
																	  'headimgurl'    => $sso_data['pictureUrl'],
																	  'sso_name' 		=> "line",
																	  'phone'         => "ln".$ts,
																	  'type' 			=> "sso",
																	  'json'          => "Y",
																	  'gender'        => "N/A",
																	  'sso_uid' 		=> $sso_uid,
																	  'sso_uid2' 		=> "",
																	  'user_src'      => $user_src
										);
										break;
				case 'weixin' :
											 $sso_data['nickname']=emoji_remove($sso_data['nickname']);
											 $arrSSO = array(
											  'auth_by' 		=> $auth_by,
											  'sso_data' 		=> urlencode(json_encode($sso_data)),
											  'state' 		=> $state,
											  'nickname' 		=> $sso_data['nickname'],
											  'headimgurl'    => $sso_data['headimgurl'],
											  'sso_name' 		=> "weixin",
											  'phone'         => "wx".$ts,
											  'type' 			=> "sso",
											  'json'          => "Y",
											  'gender'        => "N/A",
											  'sso_uid' 		=> $sso_uid,
											  'sso_uid2' 		=> $sso_data['unionid'],
											  'user_src'      => $user_src
											);
											break;
				default : break;
		}
		error_log("[c/kusosaja/regLogin] oauth data : ".json_encode($arrSSO));

		// helpers.php 的getUserBySSO
		$get_user = getUserBySSO($sso_uid, $auth_by, '');
		error_log("[c/kusosaja/regLogin] get_user : ".json_encode($get_user));

			if(empty($get_user)){

				$url = BASE_URL.APP_DIR."/ajax/user_register.php";
				//設定送出方式-POST
				$stream_options = array(
																'http' => array (
																'method' => "POST",
																'content' => json_encode($arrSSO),
																'header' => "Content-Type:application/json"
																)
				);
				$context = stream_context_create($stream_options);

				// 送出json內容並取回結果
				$response = file_get_contents($url, false, $context);

				error_log("[broadcast/regLogin] response : ".$response);

				// 讀取json內容
				$arr = json_decode($response,true);

				if($arr['retCode'] == 1){
				  $get_user = getUserBySSO($sso_uid, $auth_by, '');
				}

			}

			// 查驗來源IP
			if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$ip = $temp_ip[0];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}

			$_SESSION['auth_pass'] = false;

			// helpers.php 的setLogin
			setLogin($get_user);

			if($arrState['ip'] !== $ip){
				$url = BASE_URL.APP_DIR."/kusosaja/?hostid={$hostid}";
				header("Location:". $url);
				exit;
			}else{
				$tpl->assign('oauth', $arrSSO);
				$this->play($productid, $hostid);
				return ;
			}
    }

  /*
  public function adctrl() {

		global $tpl, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		$plist=$product->product_flash_list_all('');
		$prod_sel_list=array();
		if(!empty($plist['table']['record'])){
		  foreach($plist['table']['record'] as $prod){
		    $prod_sel_list[$prod['productid']]=$prod['name']." (".$prod['productid'].")";
		  }
		}

		$hostid=$_REQUEST['hostid'];

		$productid=$_REQUEST['productid'];
		if(empty($productid)){
			if(!empty($plist['table']['record'])){
			  $productid= $plist['table']['record'][0]['productid'];
			}
		}

		//商品資料
		$get_product = $product->get_info($productid);
		$tpl->assign('product', $get_product);

		//目前中標
    $bid_name='';
    if(empty($bid_name)){
		  $bidded = $product->get_bidded($productid);
		  $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '_無_';
		  error_log("[adview]Winner:".$bid_name." of Prod:".$productid." From DB ");
		}

		$tpl->assign('bidded', $bid_name);

		if(!empty($get_product['thumbnail2'])){
			$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail2'];
		}elseif (!empty($get_product['thumbnail_url'])){
			$meta['image'] = $get_product['thumbnail_url'];
		}

		$tpl->assign('qrcode',"Y");
		$tpl->assign('prod_sel_list',$prod_sel_list);
		$tpl->assign('hostid',$hostid);
		$tpl->assign('productid',$productid);
		$tpl->assign('header','Y');
		$tpl->assign('footer','N');
		$tpl->set_title('');
		$tpl->render("kusosaja","adctrl",true);

	}
  */

  public function adctrl() {

		global $tpl, $product;

		//設定 Action 相關參數
		set_status($this->controller);

        $hostid=htmlspecialchars($_REQUEST['hostid']);

		$plist=$product->product_flash_list_all($hostid);
		$prod_sel_list=array();
		if(!empty($plist['table']['record'])){
		  foreach($plist['table']['record'] as $prod){
		    $prod_sel_list[$prod['productid']]=$prod['name']." (".$prod['productid'].")";
		  }
		}

		$productid=$_REQUEST['productid'];
		if(empty($productid)){
			if(!empty($plist['table']['record'])){
			  $productid= $plist['table']['record'][0]['productid'];
			}
		}

		//商品資料
		$get_product = $product->get_info($productid);
		$tpl->assign('product', $get_product);

		//目前中標
		$bid_name='';
		if(empty($bid_name)){
		  $bidded = $product->get_bidded($productid);
		  $bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '_無_';
		  error_log("[adview]Winner:".$bid_name." of Prod:".$productid." From DB ");
		}

		$tpl->assign('bidded', $bid_name);

		if(!empty($get_product['thumbnail2'])){
			$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail2'];
		}elseif (!empty($get_product['thumbnail_url'])){
			$meta['image'] = $get_product['thumbnail_url'];
		}

		$tpl->assign('qrcode',"Y");
		$tpl->assign('qrcode_url', urlencode(BASE_URL."/site/kusosaja/?hostid=".$hostid));
		$tpl->assign('prod_sel_list',$prod_sel_list);
		$tpl->assign('hostid',$hostid);
		$tpl->assign('productid',$productid);
		$tpl->assign('header','Y');
		$tpl->assign('footer','N');
		$tpl->assign('noback','Y');
		$tpl->assign('adctrl','Y');

		$tpl->set_title('閃殺後台');
		$tpl->render("kusosaja","adctrl",true);

	}

  // 閃殺大廳(list)
  public function ks_prod_list() {

		global $db, $config, $tpl, $broadcast;

		//設定 Action 相關參數
		set_status($this->controller);

	}


  // 閃殺商品頁面
	/*
	 *  QRcode導到此, 存入$_SESSION['canbid']='Y' 再導去頁面
	 */
	public function play($productid='', $hostid='') {

		global $tpl, $product, $scodeModel, $user, $oscode, $bid;

		set_status($this->controller);
		$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
		if(empty($productid)){
			$productid=htmlspecialchars($_REQUEST['productid']);
		}
		$userid=$_SESSION['auth_id'];
		$errMsg='';
		$errAct='';
		error_log("[c/kusosaja/play] userid : ".$userid);
		$get_product = "";
		if(!empty($userid) && $userid>0){
			if(empty($productid)){
			    $plist=$product->product_flash_list_all($hostid);
				if($plist && count($plist['table']['record'])>0){
				  $get_product = $plist['table']['record'][0];
				  $productid= $get_product['productid'];
				}
			}else{
				$get_product=$product->get_info($productid);
			}

			// 取得 hostid 資料
			$host_id = empty($_REQUEST['hostid']) ? $get_product['vendorid'] : htmlspecialchars($_REQUEST['hostid']);

			if(!empty($get_product) && !empty($get_product['productid'])){
				// 取得該用戶的出價資料
				$user_limit = $get_product['user_limit'];

				if(!$user_limit || $user_limit<=0)
				$user_limit=5;

				$tpl->assign("user_limit",$user_limit);

				$bid_count=0;
				if(!empty($userid) && $userid>0){
					$saja_record=$bid->getUserSajaHistory($userid,$productid,' insertt ASC ',$user_limit);
					if($saja_record){
						error_log("[c/kusosaja] saja_record : ".json_encode($saja_record));
						// $bid_price=$saja_record[0]['price'];
						$bid_count=count($saja_record);
						$tpl->assign("saja_record",$saja_record);
					}
				}
				//吐閃殺記錄的API BY json=Y
				if ((!empty($_REQUEST['json']))&&($_REQUEST['json']=='Y')){
					$rtn=array();
					$rtn['retCode']=1;
					$rtn['data']=$saja_record;
					echo json_encode($rtn);
					die();
				}
				$tpl->assign('bid_count', $bid_count);

				//Add By Thomas 20150721 for LINE 分享
				if($get_product['is_flash']=='Y'){
					$meta['title']=$get_product['name']." 閃殺搶標中 !!";
					$meta['description']='閃殺活動地點 : '.$get_product['flash_loc'];
				}else{
					$meta['title']=$get_product['name']." 搶標中 !!";
					$meta['description']='';
				}

				// 商品圖檔
				if(!empty($get_product['thumbnail2'])) {
					$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail2'];
				}elseif(!empty($get_product['thumbnail_url'])){
					$meta['image'] = $get_product['thumbnail_url'];
				}
				error_log("[c/kusosaja] product : ".json_encode($get_product));

				$tpl->assign('product', $get_product);
				$tpl->assign('meta',$meta);
			}else{
			  $errAct="window.location.href='/site/product/?type=flash&canbid=N';";
			  $errMsg='沒有這件商品 !!';
			}
		}else{
		  $errAct="window.location.href='/site';";
		  $errMsg='請登入享受更多優惠 !!';
		}
		$tpl->assign('wss_url',$wss_url);
		$tpl->assign('hostid', $host_id);
		$tpl->assign('errMsg',$errMsg);
		$tpl->assign('errAct',$errAct);
		$tpl->assign('header','Y');
		$tpl->assign('footer','N');
		$tpl->assign('noback','Y');
		$tpl->assign('download_app','Y');
		$tpl->set_title('殺價王-閃殺活動');
		$tpl->render("product","kuso_saja",true);
	}


  // 新增殺價券資料
  public function give_oscode() {

		global $db, $config, $broadcast, $product;

		$lbid = empty($_POST['lbid']) ? htmlspecialchars($_GET['lbid']) : htmlspecialchars($_POST['lbid']);
		$lbuserid =  empty($_POST['lbuserid']) ? htmlspecialchars($_GET['lbuserid']) : htmlspecialchars($_POST['lbuserid']);
		$productid =  empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=$_SESSION['auth_id'];

		if(empty($lbid) && empty($lbuserid) && empty($productid)){
		  $ret = getRetJSONArray(0,'無效的參數 !!','MSG');
		  echo json_encode($ret);
		  return false;
		}

		$arrPromotes=$broadcast->getScodePromoteList($lbuserid);
		$retMsg ="";
		$num_received_oscode=0;
		$arrProductids=array();

		if($arrPromotes){
		  error_log("[c/broadcast/give_oscode] : ".json_encode($arrPromotes));
		  foreach($arrPromotes as $promote){
		    if($promote['lbuserid']==$lbuserid){
		      $spid = $promote['spid'];
		      $onum = $promote['onum'];
		      $productid=$promote['productid'];

          if(empty($productid)){
            error_log("[c/give_oscode] empty productid !! ");
            continue;
          }

          // 檢查是否領過殺價券
          $received = $broadcast->getUserOscode($userid,$spid);
          error_log("[c/give_oscode] received oscode for productid ${productid} : ".$received[0]['count']);
          if($received[0]['count']>0){
						$ret = array();
						$ret['retCode']=-1;
						$ret['retMsg']= "您已領取過殺價券 !!";
						echo json_encode($ret);
						return;
						continue;
          }

          // $product = $product->get_info($productid);
          if(true){
            if($onum>0){
              for($j=0;$j<$onum;++$j){
                $broadcast->insertUserOscode($userid, $spid, $productid, $lbuserid);
              }
              if($spid>0){
                $update = "UPDATE `{$config['db'][1]['dbname']}`.`{$config['default_prefix']}scode_promote`
                              SET amount=amount+1 ,scode_sum=scode_sum+".$onum."
                            WHERE spid='".$spid."'";
                $ret=$db->query($update);
                error_log("[c/give_oscode] update : ".$update."-->".$ret);
                $num_received_oscode+=$onum;
              }
              array_push($arrProductids,$productid);
            }
            $ret = array();
            $ret['retCode']=1;
            $ret['retMsg']= "成功領取 ".$onum." 張殺價券 !!";
            $ret['prodids']=$arrProductids;
          }
		    }
		  }
		}else{
			$ret = array();
			$ret['retCode']=0;
			$ret['retMsg']= "本活動已結束 !!";
		}
		echo json_encode($ret);

		return;

  }


  // 設定倒數時間
  function setSecToClose($productid='', $secToClose='') {

		global $db, $config,  $product;

		$ret=array("retCode"=>0, "retMsg"=>"NO_PROD_DATA");

		if(empty($productid)){
		  $productid=htmlspecialchars($_REQUEST['productid']);
		}

		if(empty($productid)){
		  echo json_encode($ret);
		  return;
		}

		if(empty($secToClose))
		  $secToClose=htmlspecialchars($_REQUEST['secToClose']);

		if(empty($secToClose))
		  $secToClose=30;

		$ret['productid']=$productid;

		$offtime_ts=time()+$secToClose;
		$offtime=date ("Y-m-d H:i:s", $offtime_ts);

		$arrUpd=array("closed"=>"N", "offtime"=>$offtime);
		$arrCond=array("productid"=>$productid,"switch"=>"Y");

		$ret['retCode']=$product->updateProduct($arrUpd, $arrCond);

		if($ret['retCode']==1){
		  $ret['retMsg']=$offtime;
		  $ret['offtime_ts']=$offtime_ts;
			// websocket更新得標人員
			$ts = time();
			$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
			$client = new WebSocket\Client($wss_url);
			$ary_bidder = array('actid'		=> 'BID_CLOSE',
								'name' 		=> "",
								'productid' => "{$productid}",
								'ts'		=> $ts,
								'status'	=> 3,
								'sign'		=> MD5($ts."|sjW333-_@"),
								'thumbnail_file' => "" ,
								'thumbnail_url'	=> ""
						);
			$client->send(json_encode($ary_bidder));
		}else{
		  $ret['retCode']=-1;
		  $ret['retMsg']="UPDATE_ERROR";
		}
		echo json_encode($ret);

		return;

  }

  // 閃殺結標
  function closeProduct($productid='') {

	  global $db, $config,  $product;

	  $ret=array();
	  if(empty($productid)){
	    $productid=htmlspecialchars($_REQUEST['productid']);
	  }
	  if(empty($productid)){
      $ret["retCode"]=-1;
      $ret["retMsg"]="EMPTY_PRODUCTID";
      echo json_encode($ret);
      return;
	  }
	  $arrCloseCond=array();
	  $arrCloseCond['switch']='Y';
	  $arrCloseCond['is_flash']='Y';
	  //$arrCloseCond['closed']='N';
	  $arrCloseCond['display']='Y';
	  $arrCloseCond['_on_sale']=TRUE;
	  $arrCloseCond['productid']=$productid;

    $table = $product->getProducts($arrCloseCond);
		if(!$table || empty($table[0]['productid'])){
     	$ret["retCode"]=0;
      $ret["retMsg"]="NO_PRODUCT_TO_CLOSE";
      echo json_encode($ret);
      return;
		}

    $prod = $table[0];

    $ret['retCode']=1;
    $ret['retMsg']="OK";

    //取得得標者資料
    $recArr = $product->getWinner($prod['productid']);

    $arrCond=array('productid'=>$prod['productid'], "switch"=>"Y");
    $arrUpd=array();
    if(!$recArr || empty($recArr[0]['userid'])){
      //流標
      error_log("[c/kusosaja/closeProduct] No unique bid : ".$prod['productid']);
      // 修改商品狀態為流標
      $arrUpd=array("closed"=>"NB");
      $product->updateProduct($arrUpd, $arrCond);

      // 將流標資料塞入回傳json
      $ret['retCode']=1;
      $ret['retMsg']="NOBODY_WIN";
      $ret['userid']="";
      $ret['productid']=$productid;
      $ret['price']=0;
      $ret['nickname']="";
    }else{
      // 結標
      $productid=$prod['productid'];
      $userid=$recArr[0]['userid'];
	  $price=$recArr[0]['price'];
	  $nickname=$recArr[0]['nickname'];
      error_log("[c/kusosaja/closeProduct] winner of ${productid} is : ${userid} ");
      // 紀錄得標者資料
      $product->createPayGetData($productid, $userid, $price, $nickname);

	  // 修改商品狀態為已結標
	  /*
      $arrUpd=array("closed"=>"Y");
      $product->updateProduct($arrUpd, $arrCond);
	  */
      // 重新取得得標者狀態
      $winner = $product->getBidWinnerInfo($productid);
      // 將得標者資料塞入回傳json
      $ret['retCode']=1;
      $ret['userid']=$userid;
      $ret['productid']=$productid;
      $ret['price']=round($price);
      $ret['nickname']=$winner[0]['nickname'];
    }
    //發送結標完成資訊出去
    $ts = time();
	$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
	$client = new WebSocket\Client($wss_url);
	$ary_bidder = array('actid'		=> 'BID_CLOSED',
						'name' 		=> "",
						'nickname'	=> "{$ret['nickname']}",
						'productid' => "{$productid}",
						'userid'	=> "{$ret['userid']}",
						'ts'		=> $ts,
						'price'		=> "{$ret['price']}",
						'sign'		=> MD5($ts."|sjW333-_@")
				);
	$client->send(json_encode($ary_bidder));
	  error_log("[c/kusosaja/closeProduct] : ".json_encode($ret));
    echo json_encode($ret);

    return ;

	}

	public function kusosaja_check(){
		global $tpl, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		login_required();

		$tpl->set_title('');
		$tpl->render("product","kuso_saja_check",true);

	}


	// 驗證通關密碼
	function check_pass() {
		global $db, $config,  $product;

		$productid = htmlspecialchars($_REQUEST['productid']);
		$hostid = htmlspecialchars($_REQUEST['hostid']);
		$pass = htmlspecialchars($_REQUEST['pass']);

		$ret=array("retCode"=>0, "retMsg"=>"ERROR_DATA");
		$_SESSION['auth_pass'] = false;

		if(empty($productid) || empty($hostid)){
			echo json_encode($ret); exit;
		}

		if(($hostid=='1' && $pass=='2888') || ($hostid=='2' && $pass=='5133') ||
		($hostid=='3' && $pass=='333') ||($hostid=='4' && $pass=='333') ){
			$ret['retCode']=1;
			$ret['retMsg']="OK";

			$_SESSION['auth_pass'] = $hostid;
			//$this->start_session('auth_pass', $hostid, 300);
			setcookie('auth_pass', $hostid, time() + 3600, '/', COOKIE_DOMAIN);
		} else {
			$ret['retCode']=-1;
			$ret['retMsg']="PASS ERROR";
		}
		echo json_encode($ret); exit;

	}

	function start_session($id='PHPSESSID', $val=0, $expire=0)
	{
		if (empty($_COOKIE[$id])) {
			session_set_cookie_params($expire, '/', COOKIE_DOMAIN);
			session_start();
		} else {
			setcookie($id, $val, time() + $expire, '/', COOKIE_DOMAIN);
		}
	}

}
