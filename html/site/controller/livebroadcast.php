<?php
/*
 * Livebroadcast Controller 直播頁
 */

class Livebroadcast {

	public $controller = array();
	public $params = array();
	public $id;

	/*
   * 首頁-領殺價卷說明頁.
   */
	public function home() {

		global $db, $config, $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->render2("livebroadcast","home",false);
	}

	public function nonlive() {

		global $db, $config, $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$json	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		$tpl->assign('json',$json);
		$tpl->render2("livebroadcast","nonlive",false);
	}


	public function chattest() {

		global $db, $config, $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->render2("livebroadcast","chattest",false);
	}

	public function chatview(){
		
		global $db, $config, $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->render2("livebroadcast","chatview",false);

	}

	public function yttest() {

		global $db, $config, $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->render2("livebroadcast","yttest",false);
	}

}
