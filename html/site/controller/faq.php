<?php
/**
 * Faq Controller 新手教學
 */

class Faq {

	public $controller = array();
	public $params = array();
	public $id;
	public $userid = '';

	public function __construct() {
		
		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
		$this->datetime = date('Y-m-d H:i:s');
		
	}


	/*
	  * Default home page.
	  * 20191105 AARON 修正
	  */
	public function home() {

		global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['APP']) ? htmlspecialchars($_GET['APP']) : htmlspecialchars($_POST['APP']);

		//分類
		$_category = $faq->faq_category();
		foreach($_category as $tk => $tv){
			$table = $faq->faq_list_a($tv['fcid']);
			$_category[$tk]['faq_list'] = $table;
		}
		
		/*
		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['category_list']=$_category;
			echo json_encode($ret);
			exit;
		}else{*/
			$tpl->assign('faq_category', $_category);
		//}
		
		$tpl->assign('noback', 'Y');
		$tpl->set_title('新手教學');
		$tpl->render("faq","faq_list",true,false,$app);
	}


	
	
	/*
	public function home() {
		global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$app = empty($_POST['APP']) ? htmlspecialchars($_GET['APP']) : htmlspecialchars($_POST['APP']);
		$tpl->assign('APP', $app);

		if(empty($userid)){
		  if(file_exists(HTML_FILE_DIR.'/faq/faq_nologin.php')){
			  $tpl->assign('static_html',HTML_FILE_DIR.'/faq/faq_nologin.php');
			  $tpl->set_title('');
			  $tpl->render("faq","home",true,false,$app);
			}else{
			  //列表
				$faq_list = $faq->faq_list('a');
				$tpl->assign('row_list', $faq_list);

				if(is_array($faq_list)){
					foreach ($faq_list as $fcid => $value){
						if(is_array($value['menu'])){
							foreach ($value['menu'] as $faqid => $value1){
								$faq_detail[$faqid] = $faq->faq_detail($faqid);
								$tpl->assign('detail', $faq_detail);
							}
						}
					}
				}
				$tpl->set_title('');
				$tpl->render("faq","home",true,false,$app);
			}
		}else if(!empty($userid)){
			if(file_exists(HTML_FILE_DIR.'/faq/faq.html')){
			  $tpl->assign('static_html',HTML_FILE_DIR.'/faq/faq.html');
			  $tpl->set_title('');
			  $tpl->render("faq","home",true,false,$app);

			}else{
				//列表
				$faq_list = $faq->faq_list('a');
				$tpl->assign('row_list', $faq_list);

				if(is_array($faq_list)){
					foreach($faq_list as $fcid => $value){
						if(is_array($value['menu'])){
							foreach ($value['menu'] as $faqid => $value1){
								$faq_detail[$faqid] = $faq->faq_detail($faqid);
								$tpl->assign('detail', $faq_detail);
							}
						}
					}
				}
				$tpl->set_title('');
				$tpl->render("faq","home",true,false,$app);
			}
		}
	}*/
	


	public function getQkStartItems() {

    global $tpl, $faq;

    $lang=$_POST['lang'];
    $ret=getRetJSONArray(1,'OK','LIST');
    $faq=array();
    $faq['url']='http://img.shajiawang.com/site/static/html/qkstart.html';
    $faq['items'][0]['title']='新手必看';
    $faq['items'][0]['items']=array();
    $faq['items'][1]['title']='殺價競價相關';
    $faq['items'][1]['items']=array();
    $faq['items'][2]['title']='中標後續相關';
    $faq['items'][2]['items']=array();
    $faq['items'][3]['title']='鯊魚點兌換相關';
    $faq['items'][3]['items']=array();
    $faq['items'][4]['title']='殺友帳號相關';
    $faq['items'][4]['items']=array();
    $faq['items'][5]['title']='殺價券與閃殺';
    $faq['items'][5]['items']=array();
    $faq['items'][6]['title']='疑難排解';
    $faq['items'][6]['items']=array();
    $faq['items'][7]['title']='殺價王使用者服務協定';
    $faq['items'][7]['items']=array();

    $subtitle=array();
    $s_0_0=array('name'=>'CCTV報導殺價王 (請在WIFI環境下觀看)','loc'=>'s_0_0');
    $s_0_1=array('name'=>'快速上手教學影片 (請在WIFI環境下觀看)','loc'=>'s_0_1');
    array_push($faq[0]['items'],$s_0_0,$s_0_1);
    $s_1_0=array('name'=>'如何進行殺價?','loc'=>'s_1_0');
    $s_1_1=array('name'=>'順位提示功能是什麼?','loc'=>'s_1_1');
    $s_1_2=array('name'=>'出價條件/結標條件說明','loc'=>'s_1_2');
    $s_1_3=array('name'=>'出價方式說明','loc'=>'s_1_3');
    $s_1_4=array('name'=>'殺價幣說明','loc'=>'s_1_4');
    $s_1_5=array('name'=>'凍結殺價幣','loc'=>'s_1_5');
    array_push($faq[1]['items'],$s_1_0,$s_1_1,$s_1_2,$s_1_3,$s_1_4,$s_1_5);
    $s_2_0=array('name'=>'中標後結帳手續說明','loc'=>'s_2_0');
    $s_2_1=array('name'=>'中標曬單','loc'=>'s_2_1');
    $s_2_2=array('name'=>'退貨流程','loc'=>'s_2_2');
    array_push($faq[2]['items'],$s_2_0,$s_2_1,$s_2_2);
    $s_3_0=array('name'=>'鯊魚點說明與規範','loc'=>'s_3_0');
    $s_3_1=array('name'=>'鯊魚點兌換注意事項','loc'=>'s_3_1');
    array_push($faq[3]['items'],$s_3_0,$s_3_1);
    $s_4_0=array('name'=>'忘記登陸密碼','loc'=>'s_4_0');
    $s_4_1=array('name'=>'忘記兌換密碼','loc'=>'s_4_1');
    $s_4_2=array('name'=>'封鎖帳號相關','loc'=>'s_4_2');
    array_push($faq[4]['items'],$s_4_0,$s_4_1,$s_4_2);
    $s_5_0=array('name'=>'殺價券說明','loc'=>'s_5_0');
    $s_5_1=array('name'=>'閃殺活動說明','loc'=>'s_5_1');
    array_push($faq[5]['items'],$s_5_0,$s_5_1);
    $s_6_0=array('name'=>'中標商品可否換或變現?','loc'=>'s_6_0');
    $s_6_1=array('name'=>'商品相關問題','loc'=>'s_6_1');
    $s_6_2=array('name'=>'中標者之顯示','loc'=>'s_6_2');
    $s_6_3=array('name'=>'殺價王會不會作弊?','loc'=>'s_6_3');
    $s_6_4=array('name'=>'各類名詞解釋','loc'=>'s_6_4');
    array_push($faq[6]['items'],$s_6_0,$s_6_1,$s_6_2,$s_6_3,$s_6_4);
    $s_7_0=array('name'=>'殺價王使用者服務協定','loc'=>'s_7_0');
    array_push($faq[7]['items'],$s_7_0);

    $ret['retObj']['data']= $faq;
    echo json_encode($ret);
    exit;

	}


	public function detail() {

		global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		if(empty($_GET['fid']) ){
			return_to('site/faq');
		}else{
			//資料
			$get_faq = $faq->faq_detail($_GET['fid']);
			$tpl->assign('detail', $get_faq);
		}

		$tpl->set_title('');
		$tpl->render("faq","detail",true);

	}


	public function servicepolicy() {

    global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("faq","servicepolicy",true);

	}


	public function privatepolicy() {

    global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("faq","privatepolicy",true);

	}


	public function weixinfaq() {

    global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$fid = empty($_POST['fid']) ? htmlspecialchars($_GET['fid']) : htmlspecialchars($_POST['fid']);

		if(empty($fid)){
			$get_faq = '';
		}else{
			//資料
			$get_faq = $faq->faq_detail($fid);
		}
		$ret=getRetJSONArray(1,'OK','LIST');
		$ret['retObj']['data'] = $get_faq;
		echo json_encode($ret);
		exit;

	}

	public function getSajaRuleDesc() {

		global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$type = $_REQUEST['type'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_POST['userid']) ? htmlspecialchars($_GET['userid']) : htmlspecialchars($_POST['userid']);

		$get_faq = $faq->faq_detail(668);

		$ret=getRetJSONArray(1,'OK','LIST');
		//$ret['retObj'] = 'This is saja rule of productid : '.$productid;
		$ret['retObj'] = $get_faq['description'];
		echo json_encode($ret);
		exit;

	}

}
