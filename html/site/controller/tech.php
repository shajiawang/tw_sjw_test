<?php
/*
 * Tech Controller 殺價密技
 */

class Tech {

	public $controller = array();
	public $params = array();
	public $id;
	public $userid = '';

	public function __construct() {
		
		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
		$this->datetime = date('Y-m-d H:i:s');
		
	}


	/*
	  * Default home page.
	  * 20191105 AARON 修正
	  */
	public function home() {

		global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$cdnTime = date("YmdHis");
		$status = _v('status');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$app = empty($_POST['APP']) ? htmlspecialchars($_GET['APP']) : htmlspecialchars($_POST['APP']);

		//分類
		$_category = $faq->tech_category();
		foreach($_category as $tk => $tv){
			$table = $faq->tech_list($tv['fcid']);
			$_category[$tk]['tech_list'] = $table;
		}
		
		/*
		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['category_list']=$_category;
			echo json_encode($ret);
			exit;
		}else{*/
			$tpl->assign('tech_category', $_category);
		//}
		
		$tpl->assign('noback', 'Y');
		$tpl->set_title('王者秘笈');
		$tpl->render("tech","tech_list",true,false,$app);
	}


	/*
	public function home() {
    global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		$app = empty($_POST['APP']) ? htmlspecialchars($_GET['APP']) : htmlspecialchars($_POST['APP']);
		$tpl->assign('APP', $app);

		if(file_exists(HTML_FILE_DIR.'/tech/tech.html')){
		  error_log("[c/tech] tech.html...");
		  $tpl->assign('static_html',HTML_FILE_DIR.'/tech/tech.html');
		  $tpl->set_title('');
		  $tpl->render("tech","home",true,false,$app);
		}else{

      //列表
			$get_list = $faq->faq_list('b');
			$tpl->assign('row_list', $get_list);

			if(is_array($get_list)){
				foreach($get_list as $fcid => $value){
					if(is_array($value['menu'])){
						foreach ($value['menu'] as $faqid => $value1){
							$get_detail[$faqid] = $faq->faq_detail($faqid);
							$tpl->assign('detail', $get_detail);
						}
					}
				}
			}
			error_log("[c/tech] view/tech/home...");
			$tpl->set_title('');
			$tpl->render("tech","home",true,false,$app);
		}

	}
	*/


	public function detail() {

    global $tpl, $faq;

		//設定 Action 相關參數
		set_status($this->controller);

		if(empty($_GET['fid']) ){
			return_to('site/tech');
		}else{
			//資料
			$get_faq = $faq->faq_detail($_GET['fid']);
			$tpl->assign('detail', $get_faq);
		}

		$tpl->set_title('');
		$tpl->render("tech","detail",true);

	}
	
	public function test() {
		global $tpl, $config;
		error_log("[c/Tech/test]".json_encode(query_bid_able("1705")));	
	}

}
