<?php
/*
 * Showoff Controller
 */

class Showoff {

  public $controller = array();
	public $params = array();
  public $id;

	public function __construct() {

		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
    $this->datetime = date('Y-m-d H:i:s');

  }


	/*
	 *	曬單清單首頁
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$p			int			頁碼
	 *	$perpage	int			會員編號
	 */
	public function home() {

		global $tpl, $showoff;

		$json 	 	= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid  	= empty($_SESSION['auth_id']) ? htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$page 	 	= empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$perpage 	= empty($_POST['perpage']) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得曬單清單資料
		$showoff_list = $showoff->get_Showoff_List('', '', '', $page, $perpage);

		for($i=0;$i<count($showoff_list['record']);$i++){

			//判斷曬單資料是否存在
			if(!empty($showoff_list['record'][$i]['shofid'])){
				//取得曬單圖片資料
				$showoff_pics_list = $showoff->get_Showoff_Pics($showoff_list['record'][$i]['shofid']);

				for ($k=0;$k<3;$k++){

					//判斷圖片是否存在,加入到曬單清單
					if(!empty($showoff_pics_list[$k]['shofpid'])){
						$showoff_list['record'][$i]['pic'.($k+1)] = IMG_URL.APP_DIR."/images/showoff/".$showoff_pics_list[$k]['thumbnail_url'];
					}else{
						$showoff_list['record'][$i]['pic'.($k+1)] = "";
					}

				}
			}

		}

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('row_list', $showoff_list);
			$tpl->set_title('');
			$tpl->render("showoff","home",true);
		}else{
			$ret = getRetJSONArray(1,"OK",'LIST');
			$ret['retObj']['data'] = (!empty($showoff_list['record'])) ? $showoff_list['record'] : array();
			$ret['retObj']['page'] = (!empty($showoff_list['page'])) ? $showoff_list['page'] : new stdClass;
			echo json_encode((object)$ret);
		}

	}


	/*
	 *	曬單清單首頁
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$shofid		int			曬單編號
	 */
	public function Showoff_detail(){

		global $tpl, $showoff;

		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid  = empty($_SESSION['auth_id']) ? htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$shofid  = empty($_POST['shofid']) ? htmlspecialchars($_GET['shofid']) : htmlspecialchars($_POST['shofid']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得曬單清單資料
		$showoff_detail = $showoff->get_Showoff_Detail($shofid);

		//取得曬單圖片資料
		$showoff_pics_list = $showoff->get_Showoff_Pics($showoff_detail[0]['shofid']);

		for($k=0;$k<3;$k++){

			//判斷圖片是否存在,加入到曬單清單
			if(!empty($showoff_pics_list[$k]['shofpid'])){
				$showoff_detail[0]['pic'.($k+1)] = IMG_URL.APP_DIR."/images/showoff/".$showoff_pics_list[$k]['thumbnail_url'];
			}else{
				$showoff_detail[0]['pic'.($k+1)] = "";
			}

		}

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('row_detail', $showoff_detail);
			$tpl->set_title('');
			$tpl->render("showoff","detail",true);
		}else{
			$ret = getRetJSONArray(1,"OK",'JSON');
			$ret['retObj']=array();
			$ret['retObj']['data'] = (!empty($showoff_detail[0])) ? $showoff_detail[0] : new stdClass;
			echo json_encode($ret);
		}

	}


	/*
	 *	新增曬單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$productid	int			商品編號
	 *	$title	 	varchar		留言標題
	 *	$content	varchar		留言內容
	 *	$pic1		varchar		圖片1
	 *	$pic2		varchar		圖片2
	 *	$pic3		varchar		圖片3
	 */
	public function Showoff_add() {

		global $tpl, $config, $user, $showoff;

		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid  	= empty($_SESSION['auth_id']) ? htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$productid 	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$title 		= empty($_POST['title']) ? htmlspecialchars($_GET['title']) : htmlspecialchars($_POST['title']);
		$content 	= empty($_POST['content']) ? htmlspecialchars($_GET['content']) : htmlspecialchars($_POST['content']);
		$pic1 		= empty($_POST['pic1']) ? htmlspecialchars($_GET['pic1']) : $_POST['pic1'];
		$pic2 		= empty($_POST['pic2']) ? htmlspecialchars($_GET['pic2']) : $_POST['pic2'];
		$pic3 		= empty($_POST['pic3']) ? htmlspecialchars($_GET['pic3']) : $_POST['pic3'];

		//判斷是否有登入會員
		if($json!="Y")
		  login_required();

	  error_log("[c/showoff/Showoff_add] json:". $json);
    error_log("[c/showoff/Showoff_add] title:". $title);
    error_log("[c/showoff/Showoff_add] content:". $content);
    error_log("[c/showoff/Showoff_add] userid:".$userid);

    if(empty($title))
      $title=$productid;

		//判斷資料是存在
		if((!empty($title)) && (!empty($content)) && (!empty($userid))){

      //取得曬單清單資料
			$showoff_list = $showoff->get_Showoff_have($productid);
			error_log("[c/showoff/Showoff_add] showoff_list :".json_encode($showoff_list));
			error_log("[c/showoff/Showoff_add] count :".$showoff_list[0]['num']);

			//判斷是否有重覆曬單商品
			if($showoff_list[0]['num'] == 0){

				//取得商品得標資料
				$bid_product = $user->getUserAwardProdList($userid, $productid);
				error_log("[c/showoff/Showoff_add] bid_product :".json_encode($bid_product));

				//判斷此使用者是否為商品得標者
				if(!empty($bid_product[0])){

					//組合曬單資料
					$insertShowoffdata = array(
                          						'userid'		=> $userid,											//使用者編號
                          						'productid'		=> $productid,										//商品編號
                          						'title'			=> $title,											//曬單標題
                          						'content' 		=> $content, 										//曬單內容
                          						'seq'			=> 0,												//排序
                          						'switch'      	=> 1												//狀態
					);

					//新增曬單資料
					$insdata = $showoff->insert_Showoff($insertShowoffdata);

					for ($i=1;$i<4;$i++){

						//判斷圖片資料是否存在
						if(!empty(${"pic".$i})){

							$image = base64_decode($pic);
							$subName = 'jpg';//副檔名
							$picname = $userid.'-'.$i.time().'.'.$subName;

							//上傳圖片
							$file_path = $config["path_site"]."/images/showoff/";
							$file_path = $file_path . basename($picname);

							$pic = str_replace('data:image/png;base64,', '', ${"pic".$i});
							$pic = str_replace(' ', '+', $pic);
							$data = base64_decode($pic);
							$success = file_put_contents($file_path, $data);

							//組合曬單資料
							$insertpicsdata = array(
                      								'shofid'			=> $insdata,							//使用者編號
                      								'ori_fname'			=> $picname,							//圖片名稱
                      								'thumbnail_url'		=> $picname,							//圖片路徑
                      								'seq'				=> $i,									//排序
                      								'switch'      		=> 'Y'									//狀態
							);

							//新增曬單資料
							$inspicsdata = $showoff->insert_Showoff_Pics($insertpicsdata);
						}

					}

					//判斷是否為網頁介面
					if($json != 'Y'){
						$this->home();
					}else{
						$ret = getRetJSONArray(1,"OK",'MSG');
						echo json_encode($ret);
					}

				}else{

					//判斷是否為網頁介面
					if($json != 'Y'){
						$tpl->assign('errmsg', '不是此商品得標者!!');
						$tpl->set_title('');
						$tpl->render("showoff","add",true);
					}else{
						$ret = getRetJSONArray(-3,'不是此商品得標者','MSG');
						echo json_encode($ret);
					}

				}

			}else{

				//判斷是否為網頁介面
				if($json != 'Y'){
					$tpl->assign('errmsg', '此商品已經曬單!!');
					$tpl->set_title('');
					$tpl->render("showoff","add",true);
				}else{
					$ret = getRetJSONArray(-2,'此商品已經曬單','MSG');
					echo json_encode($ret);
				}

			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('errmsg', '資料有缺請檢查!!');
				$tpl->set_title('');
				$tpl->render("showoff","add",true);
			}else{
				$ret = getRetJSONArray(-1,"ERROR",'MSG');
				echo json_encode($ret);
			}

		}

	}


	/*
	 *	修改曬單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$showfid	int			曬單編號
	 *	$userid		int			會員編號
	 *	$seq		int			排序
	 *	$switch		int			狀態
	 *	$pic1		varchar		圖片1
	 *	$pic2		varchar		圖片2
	 *	$pic3		varchar		圖片3
	 */
	public function Showoff_edit() {

		global $tpl, $config, $showoff;

		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$showfid 	= empty($_POST['showfid']) ? htmlspecialchars($_GET['showfid']) : htmlspecialchars($_POST['showfid']);
		$userid  	= empty($_SESSION['auth_id']) ? htmlspecialchars($_POST['auth_id']) : htmlspecialchars($_SESSION['auth_id']);
		$seq 		= empty($_POST['seq']) ? htmlspecialchars($_GET['seq']) : htmlspecialchars($_POST['seq']);
		$switch 	= empty($_POST['switch']) ? htmlspecialchars($_GET['switch']) : htmlspecialchars($_POST['switch']);
		$pic1 		= empty($_POST['pic1']) ? htmlspecialchars($_GET['pic1']) : htmlspecialchars($_POST['pic1']);
		$pic2 		= empty($_POST['pic2']) ? htmlspecialchars($_GET['pic2']) : htmlspecialchars($_POST['pic2']);
		$pic3 		= empty($_POST['pic3']) ? htmlspecialchars($_GET['pic3']) : htmlspecialchars($_POST['pic3']);

		//判斷是否有登入會員
		login_required();

		if((!empty($showfid)) && (!empty($content)) && (!empty($userid))){

			//組合曬單資料
			$updateshowiffdata = array(
                          				'seq'		=> $seq,						//排序
                          				'switch'    => $switch,						//狀態
                          				'modifyt'	=> $this->datetime				//修改時間
			);
			//新增曬單資料
			$insdata = $showoff->update_Showoff($shofid, $updateshowiffdata);

			//判斷圖片資料是否皆不存在
			if((!empty($_FILES['pic1']['name'])) && (!empty($_FILES['pic2']['name'])) && (!empty($_FILES['pic3']['name'])) ){

				echo "無修改圖片";

			}else{

				//刪除舊有圖片資料
				$deldata = $showoff->delete_Showoff_Pics($shofid);

				for($i=1;$i<4;$i++){

					if(!empty(${"pic".$i})){

						$deldata = $showoff->delete_Showoff_Pics($shofid);

						$image = base64_decode($pic);
						$subName = 'jpg';//副檔名
						$picname = $userid.'-'.$i.time().'.'.$subName;

						//上傳圖片
						$file_path = $config["path_site"]."/images/showoff/";
						$file_path = $file_path . basename($picname);

						$pic = str_replace('data:image/png;base64,', '', ${"pic".$i});
						$pic = str_replace(' ', '+', $pic);
						$data = base64_decode($pic);
						$success = file_put_contents($file_path, $data);

						//組合曬單資料
						$insertpicsdata = array(
                      							'shofid'			=> $shofid,											//使用者編號
                      							'ori_fname'			=> $picname,										//圖片名稱
                      							'thumbnail_url'		=> $picname,										//圖片路徑
                      							'seq'				=> $i,												//排序
                      							'switch'      		=> 'Y'												//狀態
						);

						//新增曬單資料
						$insdata = $showoff->insert_Showoff_Pics($insertpicsdata);
					}
				}
			}

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('msg', '曬單修改完成!!');
				$this->home();
			}else{
				$ret = getRetJSONArray(1,"OK",'MSG');
				echo json_encode($ret);
			}

		}else{

			//判斷是否為網頁介面
			if($json != 'Y'){
				$tpl->assign('msg', '資料有缺請檢查!!');
				$tpl->set_title('');
				$tpl->render("showoff","reply_add",true);
			}else{
				$ret = getRetJSONArray(-1,"ERROR",'MSG');
				echo json_encode($ret);
			}

		}

	}

}
?>
