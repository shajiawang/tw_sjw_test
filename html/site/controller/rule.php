<?php
/*
 * Rule Controller
 */

class Rule {

  public $controller = array();
	public $params = array();
  public $id;

	public function __construct() {

		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
    $this->datetime = date('Y-m-d H:i:s');

  }

	/*
	 *	儲值清單
	 */
	public function home() {

		global $tpl;

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("index",true);

	}


	/*
	 *	儲值規則清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$drid		int			儲值規則編號
	 */
	public function rule_list()	{

		global $tpl, $rule;

		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid  = empty($_POST['userid']) ? htmlspecialchars($_GET['userid']) : htmlspecialchars($_POST['userid']);
		$drid	 = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);

    //判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得儲值清單資料
		$rule_list = $rule->get_Rule_List($drid);

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('row_list', $rule_list);
		}else{
			$data['retCode'] = 1;
			$data['retMsg'] = "取得資料 !!";
			$data['retType'] = "JSON";
			$data['retObj'] = $rule_list;
			echo json_encode($data);
		}

	}


	/*
	 *	儲值規則項目清單
	 *	$json	 	varchar		瀏覽工具判斷 (Y:APP來源)
	 *	$userid		int			會員編號
	 *	$drid		int			儲值規則編號
	 *	$driid		int			儲值規則項目編號
	 */
	public function rule_item_list() {

		global $tpl, $rule;

		$json 	 = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$userid  = empty($_POST['userid']) ? htmlspecialchars($_GET['userid']) : htmlspecialchars($_POST['userid']);
		$drid	 = empty($_POST['drid']) ? htmlspecialchars($_GET['drid']) : htmlspecialchars($_POST['drid']);
		$driid	 = empty($_POST['driid']) ? htmlspecialchars($_GET['driid']) : htmlspecialchars($_POST['driid']);

		//判斷是否有登入會員
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//取得儲值規則項目資料
		$rule_item_list = $rule->get_Rule_Item($drid, $driid);

		//判斷是否為網頁介面
		if($json != 'Y'){
			$tpl->assign('row_list', $rule_item_list);
		}else{
			$data['retCode'] = 1;
			$data['retMsg'] = "取得資料 !!";
			$data['retType'] = "JSON";
			$data['retObj'] = $rule_item_list;
			echo json_encode($data);
		}

	}

}
?>
