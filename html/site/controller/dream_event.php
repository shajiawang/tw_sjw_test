<?php
/*
 * DreamProduct Controller 圓夢商品
 * 供 WEB版 Landing page 使用
 */
class DreamEvent {

	public $controller = array();
	public $params = array();
	public $id;

	/*
   * Action Method : home
   */
	public function home() {

		global $tpl, $dream_event, $dream_product, $channel, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		//商品分類
		//$_category = $dream_product->product_category();
		date_default_timezone_set('Asia/Shanghai');
		$cdnTime = date("YmdHis");
		$status = _v('status');
		$type=$_REQUEST['type'];
		$cb=$_REQUEST['canbid'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		
		//廣告流量來源參數 Ad=0 (預設為0)
		$ad_income = empty($_GET['Ad']) ? 0 : htmlspecialchars($_GET['Ad']);

		$rkey = "dream_product_category_list";
		$redis = getRedis();
		if($redis){
			$jsonStr = $redis->get($rkey);
			if(!empty($jsonStr)){
				$category_list = json_decode($jsonStr,true);
			}else{
				$category_list = $dream_product->dream_product_category_list();
				$redis->set($rkey,json_encode($category_list));
			}
		}else{
			$category_list = $dream_product->dream_product_category_list();
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['category_list'] = $category_list;
			echo json_encode($ret);
			exit;
		}else{
			//是否讓頁面顯示出價欄位
			if($cb!='N'){
				$cb='Y';
			}
			$tpl->assign('category_list', $category_list);
			$tpl->assign('canbid', $cb);
			$tpl->assign('noback', 'Y');
			$tpl->set_title('');
			//$tpl->render("dream_enent","home",true);
			//$ret = getRetJSONArray(-1,'WRONG','MSG');
			//echo json_encode($ret);
			//exit;
		}
		
		//$meta['title']="最新得標";
		//$meta['description']='';
		//$tpl->assign('meta',$meta);

		$tpl->set_title('');
		$tpl->render("dream_event","home",true);
	}

	//商品內頁
	public function saja() {

		global $tpl, $dream_product, $product, $channel, $config, $user, $scodeModel, $oscode, $bid;
		login_required();
		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){

			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
			'bid'		=> $bidprice,			//起始價格
			'bida'		=> $bida,				//起始價格
			'bidb' 		=> $bidb,				//結束價格
			'type'		=> $paytype,			//下標類型
			'autobid'	=> $auto,				//自動下標
		);

		$tpl->assign('paydata',$paydata);

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid=(empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		$behav=$_REQUEST['behav'];

		$tpl->assign('type',$type);

		$ret=null;
		if(empty($productid) ){
			if($json=='Y'){
				echo json_encode(getRetJSONArray(-3,'商品編號為空 !!','MSG'));
				exit;
			}else{
				return_to('site/product');
			}
		}else{

            // 增加商品點擊數 By Thomas 20190508
            $product->add_prod_click($productid,1);

			// 商品資料
			$get_product = $dream_product->get_dream_product_info($productid);

			if(!$get_product){
                if($json=='Y'){
				  echo json_encode(getRetJSONArray(-4,'找不到商品資料 !!','MSG'));
				  exit;
				}
			}

		    //收藏資料 : 20190508 暫時disable By Thomas
			// $collect = $user->getCollectProdList($userid, $productid, 'S', '');
			// $get_product['collect'] = $collect['record'][0];

			$ret=getRetJSONArray(1,'OK','JSON');
            $get_product['use_scode'] = false;
			$msg='';
			$get_product['now'] = (int)$get_product['now'];

			// 抓source IP
			if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$src_ip = $temp_ip[0];
			}else{
				$src_ip = $_SERVER['REMOTE_ADDR'];
			}

            // 抓user_agent
			// $user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			// $is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if($json=='Y'){
                $ret['retObj']=$get_product;
            } else {
                if(!empty($get_product['thumbnail2'])){
				  $meta['image'] = $get_product['thumbnail2'];
                }elseif (!empty($get_product['thumbnail_url'])){
				  $meta['image'] = $get_product['thumbnail_url'];
                }
            }
			$sgift=0;
			if($json=='Y'){

            } else {
                $tpl->assign('sgift_num', $sgift);
            }

			// Update By Thomas 2015214
			// 如果有經由qrcode過來 就贈送過殺價券
			if(!empty($userid) && !empty($productid) && $behav=="q"){
				$oscode_num0 = $scodeModel->get_oscode($productid, $userid);
				//查詢是否有贈送殺價券活動
				$scode_promote = $scodeModel->scode_promote($productid,'q');
				$info['spid'] = $scode_promote['spid'];
				$info['behav'] = 'qrcode_reg';
				$info['productid'] = $productid;
				$info['userid'] = $userid;
				$info['onum'] = $scode_promote['onum'];

                // 如果沒送過 且有活動 就贈送
				if($info['onum'] > 0 && ($oscode_num0 == 0 || $oscode_num0==false)){
				  for($j = 0;$j < $info['onum']; ++$j){
					  $scodeModel->add_oscode($info);
				  }
				  $scodeModel->set_scode_promote($info);
				  $msg="恭喜您獲得 ".$info['onum']."張 ".$get_product['name']." 殺價券";
				}
				// Add End
			}

			//取最新得標者
			$bid_winner = $dream_product->get_bid_winner($productid);
			$tpl->assign('bid_winner', $bid_winner);
			
			//取最新五筆出價商品紀錄
			$latest_five_bid = $dream_product->get_history_detail($productid);
			$latest_bid = $latest_five_bid;
			$tpl->assign('latest_bid', $latest_bid);
			
			//目前中標
			$bidded = $product->get_bidded($productid);
			$num_bids_gap = $product->getNumBidsGap($productid);


			if($bidded['userid'] <> ""){
				preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
				$src_ip[0][1] = "***";
				$src_ip[0][2] = "***";
				$all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				error_log("[c/product/saja] src_ip: ".json_encode($all_src_ip));
				$bidded['src_ip'] = $all_src_ip;
			}
			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			// $redis = getRedis('','');
			// $redis->set('PROD:'.$productid.':BIDDER',$bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);
		}

		$tpl->set_title('');

        if(!empty($msg))
			$tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);

		if(!empty($userid)){
			$bidget_count = $user->pay_get_product_count($userid);
		}else{
			$bidget_count = 0;
		}


		$tpl->assign('bidget_count', $bidget_count);
		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);

		if($json=='Y'){
			$x= json_encode($ret);
			echo $x;
			exit;
		}else{
			$tpl->render("dream_event","saja",true);
		}

	}

	//商品清單
	public function product_list(){
		global $tpl, $dream_product, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$pcid = empty($_POST['pcid']) ? htmlspecialchars($_GET['pcid']) : htmlspecialchars($_POST['pcid']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if(empty($pcid)){
			$ret = getRetJSONArray(-1,'NO_PCID','MSG');
			echo json_encode($ret);
			exit;
		}

		if(empty($p) || $p <= 0){
			$p = 1;
		}

		$product_list = $dream_product->dream_product_list($pcid, $p);

		if($product_list){
			if($json=='Y'){
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product_list;
			}else{
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = $product_list['table']['record'];
				$ret['retObj']['page'] = $product_list['table']['page'];
				unset($product_list['table']['page']);
			}
		}else{
			if($json=='Y'){
				$ret=getRetJSONArray(1,'NO_DATA','MSG');
				$ret['retObj']['data']['table']['record'] = array();
				$ret['retObj']['data']['table']['page'] = '';
			}else{
				$ret=getRetJSONArray(1,'NO_DATA','MSG');
				$ret['retObj']['data'] = array();
				$ret['retObj']['page'] = '';
			}
		}
		echo json_encode($ret);
		exit;
	}
	
	//商品資料
	public function product_info(){
		global $tpl, $dream_product, $config, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid = empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid = empty($_POST['auth_id']) ? htmlspecialchars($_GET['auth_id']) : htmlspecialchars($_POST['auth_id']);

		if(empty($userid)){
			$ret = getRetJSONArray(-1,'NO_AUTH_ID','MSG');
			echo json_encode($ret);
			exit;
		}

		if(empty($productid)){
			$ret = getRetJSONArray(-2,'NO_PRODUCTID','MSG');
			echo json_encode($ret);
			exit;
		}

		//取圓夢商品資訊
		$product_info = $dream_product->get_dream_product_info($productid);
		$ret=getRetJSONArray(1,'OK','LIST');
		if($product_info == false){
			$ret['retObj']['product_info'] = array();
		}else{
			$ret['retObj']['product_info'] = $product_info;

			//取最新得標者
			$bid_winner = $dream_product->get_bid_winner($productid);
			if(!empty($bid_winner)){
				$ret['retObj']['bid_winner'] = $bid_winner;
			}else{
				$ret['retObj']['bid_winner'] = (object)array();
			}

			//取最新五筆出價商品紀錄
			$latest_five_bid = $dream_product->get_history_detail($productid);
			if(!empty($bid_winner)){
				$ret['retObj']['latest_bid'] = $latest_five_bid;
			}else{
				$ret['retObj']['latest_bid'] = array();
			}
		}

		if($json == 'Y'){
			echo json_encode($ret);
		}else{
			$tpl->assign('product_info', $product_info);
			$tpl->assign('bid_winner', $bid_winner);
			$tpl->assign('latest_bid', $latest_five_bid);
			$tpl->assign('noback', 'Y');
			$tpl->set_title('');
			$tpl->render("dream_event","saja",true);
		}
	}

	//下標商品的資料
	public function sajabid(){

		global $tpl, $member, $product, $scodeModel, $user, $oscode, $bid;

		//設定 Action 相關參數
		set_status($this->controller);

		$bida = empty($_POST['bida']) ? $_GET['bida'] : $_POST['bida'];
		$bidb = empty($_POST['bidb']) ? $_GET['bidb'] : $_POST['bidb'];
		$auto = empty($_POST['auto']) ? $_GET['auto'] : $_POST['auto'];
		$paytype = '';
		$bidprice = '';

		if((!empty($auto)) && (!empty($bida)) && ($bida > 0)){
			if(($bidb < $bida) && ($bidb == 0)){
				$paytype = "single";
				$bidprice = $bida;
				$bida = '';
				$bidb = '';
			}else{
				$bidprice = '';
				$paytype = "range";
			}
		}

		//支付完成回傳資料
		$paydata = array(
			'bid'		=> $bidprice,			//起始價格
			'bida'		=> $bida,				//起始價格
			'bidb' 		=> $bidb,				//結束價格
			'type'		=> $paytype,			//下標類型
			'autobid'	=> $auto,				//自動下標
		);

		$tpl->assign('paydata',$paydata);

		$json=(empty($_POST['json'])) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$productid=(empty($_POST['productid'])) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid=empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$type=empty($_SESSION['auth_type'])?$_POST['auth_type']:$_SESSION['auth_type'];
		
		$dpname = empty($_POST['dpname']) ? htmlspecialchars($_GET['dpname']) : htmlspecialchars($_POST['dpname']);
		$dpcontent = empty($_POST['dpcontent']) ? htmlspecialchars($_GET['dpcontent']) : htmlspecialchars($_POST['dpcontent']);
		$dpaddress = empty($_POST['dpaddress']) ? htmlspecialchars($_GET['dpaddress']) : htmlspecialchars($_POST['dpaddress']);
		$dppic = empty($_POST['thumbnail']) ? htmlspecialchars($_GET['thumbnail']) : htmlspecialchars($_POST['thumbnail']);

		$ret=null;
		if(empty($productid) ){
			return_to('site/dream_product');
		}else{

			//商品資料
			$get_product = $product->getProductById($productid);

		  //收藏資料
			$collect = $user->getCollectProdList($userid, $productid, 'S', '');
			$get_product['collect'] = $collect['record'][0];

			$ret=getRetJSONArray(1,'OK','JSON');
			$get_product['use_scode'] = false;
			$msg='';
			$get_product['offtime'] = (int)$get_product['offtime'];
			$get_product['now'] = (int)$get_product['now'];
			$get_product['dpname'] = $dpname;
			$get_product['dpcontent'] = $dpcontent;
			$get_product['dpaddress'] = $dpaddress;
			$get_product['dppic'] = $dppic;
			
			// 抓source IP
			if(!empty($_SERVER['HTTP_X_FORWARDED_FOR'])){
				$temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
				$src_ip = $temp_ip[0];
			}else{
				$src_ip = $_SERVER['REMOTE_ADDR'];
			}

			// 抓user_agent
			$user_agent=$_SERVER['HTTP_USER_AGENT'];

			// 判斷是否微信
			$is_wx=strpos($user_agent, 'MicroMessenger');

			//下標支付方式: 1.殺幣(a) 2.殺幣+S碼+限定S碼(b) 3.殺幣+限定S碼(c) 4.S碼+限定S碼(d) 5.限定S碼(e)
			$pay_type = $get_product['pay_type'];

			if(!empty($get_product['thumbnail'])){
				$meta['image'] = IMG_URL . APP_DIR .'/images/site/product/'.$get_product['thumbnail'];
			}elseif(!empty($get_product['thumbnail_url'])){
				$meta['image'] = $get_product['thumbnail_url'];
			}

			$sgift=0;
			$tpl->assign('sgift_num', $sgift);

			//殺幣數量
			$get_spoint = $member->get_spoint($userid);

			if(!$get_spoint){
				$get_spoint=array("userid"=>$userid, "amount"=>0);
			}else{
				$get_spoint['amount'] = sprintf("%1\$u",$get_spoint['amount']);
			}
			$tpl->assign('spoint', $get_spoint);

			//目前中標
			$bidded=$this->getCurrWinnerInfo($productid);
			$num_bids_gap = $product->getNumBidsGap($productid);

			if($bidded['userid'] <> ""){
				preg_match_all('/[0-9]+/i',$bidded['src_ip'],$src_ip);
				$src_ip[0][1] = "***";
				$src_ip[0][2] = "***";
				$all_src_ip = $src_ip[0][0].".".$src_ip[0][1].".".$src_ip[0][2].".".$src_ip[0][3];
				$bidded['src_ip'] = $all_src_ip;
			}

			$bid_name = (!empty($bidded['nickname']) ) ? $bidded['nickname'] : '';
			$redis = getRedis('','');
			$redis->set('PROD:'.$productid.':BIDDER',$bid_name);
			$tpl->assign('bidded', $bid_name);
			$tpl->assign('sajabid', $bidded);
			$tpl->assign('canbid',$_REQUEST['canbid']);

		}

		$tpl->set_title('');

		if(!empty($msg))
			$tpl->assign('msg',$msg);

		$tpl->assign('product', $get_product);

		$meta['title']=$get_product['name']." 搶標中 !!";
		$meta['description']='';
		$tpl->assign('meta',$meta);

		if($json == 'Y'){
			$data=array();
			$data['retCode'] = 1;
			$data['retMsg'] = "OK";
			$data['retType'] = "LSIT";
			$data['retObj']['product'] = (!empty($get_product)) ? $get_product : new stdClass;
			$data['retObj']['paydata'] = (!empty($paydata)) ? $paydata : new stdClass;
			$data['retObj']['spoint'] = (!empty($get_spoint)) ? $get_spoint : new stdClass;
			echo json_encode($data);
		}else{
			$tpl->render("dream_event","sajabid",true);
		}

	}

	


	public function getCurrWinnerInfo($productid) {

        global $product, $dream_product;
        // $CurrWinnerInfo=$product->get_bidded($productid);
		$CurrWinnerInfo = $dream_product->get_bid_winner($productid);

        if(!empty($CurrWinnerInfo['userid'])){
            if(empty($CurrWinnerInfo['thumbnail_url']) && !empty($CurrWinnerInfo['thumbnail_file'])){
                $CurrWinnerInfo['thumbnail_file']=BASE_URL.APP_DIR.'/images/site/headimgs/'.$CurrWinnerInfo['thumbnail_file'];
            }
            return $CurrWinnerInfo;
        }else{
            return false;
        }

	}


}
