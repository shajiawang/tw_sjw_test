<?php
/*
 * Verified Controller 驗證
 */

class Verified {

  public $controller = array();
	public $params = array();
  public $id;

	public function __construct() {

		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];
    $this->datetime = date('Y-m-d H:i:s');

  }


	/*
	 *	確認驗證碼
	 *	$phone 				varchar				手機號碼
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function register_captcha() {

		global $tpl, $verifiedphone;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$phone = empty($_POST['phone']) ? htmlspecialchars($_GET['phone']) : htmlspecialchars($_POST['phone']);

		$tpl->assign('json',$json);
		$tpl->assign('phone',$phone);

		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("site","register_captcha",true);

	}


	/*
	 *	確認手機號碼
	 */
  public function register_phone() {

		global $tpl, $verifiedphone;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("site","register_phone",true);

	}

}
?>
