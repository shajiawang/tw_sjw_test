<?php
/**
 * Enterprise Controller 商家
 */
include_once("/var/www/html/site/lib/vendor/autoload.php"); // 載入wss client 套件(composer)
class Enterprise {
	public $ES_SALT='sJw#333';
	public $controller = array();
	public $params = array();
	public $enterpriseid = '';

	public function __construct() {
		$this->enterpriseid = (empty($_SESSION['sajamanagement']['enterprise']['enterpriseid']) ) ? $_POST['enterpriseid'] : $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
	}

	/*
	* Default home page.
	*	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	*/
	public function home() {
		global $tpl, $enterprise, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		enterprise_login_required();
		include_once(LIB_DIR ."/convertString.ini.php");
		$this->str = new convertString();

		//取得商戶關聯的會員編號
		$userenterprise = $enterprise->getEnterpriseProfile($this->enterpriseid);
		//商家資訊
		error_log("[enterprise.home] enterpriseid : ".$this->enterpriseid);

		//呼朋引伴推薦QRcode url
		$user_src = base64_encode($this->str->encryptAES128($config['encode_key'],$userenterprise['userid'].'&&1')); //var_dump($u);string(24) "kgl1YRLcAQH9xlFIhbIEwQ=="
		$share_url = BASE_URL . APP_DIR .'/user/uni_register/?u='.$userenterprise['userid'];

		$user = $user->get_user_profile($userenterprise['userid']);
		$tpl->assign('nickname', $user['nickname']);

		//商家綁定會員資訊
		error_log("[enterprise.home] nickname : ".$user['nickname']);
		error_log("[enterprise.home] userid&&1 : ".$userenterprise['userid'].'&&1');
		error_log("[enterprise.home] user_src : ".$user_src);

		$total_net_bonus = $enterprise->getEnterpriseTotalBonus($this->enterpriseid);

		// 鯊魚點數
		$tpl->assign('total_net_bonus', $total_net_bonus);

		$tpl->assign('share_url', $share_url);

		if($json=='Y'){
			$userenterprise['share_url']=$share_url;
			$userenterprise['user_src']=$user_src;
			$userenterprise['spoint']=($total_net_bonus)?sprintf("%1\$u",$total_net_bonus):"0";

			$ret=getRetJSONArray(1,'OK','JSON');
			$ret['retObj']=array();
			$ret['retObj']=$userenterprise;
			echo json_encode($ret,JSON_UNESCAPED_SLASHES);
			exit;
		}else{
			$tpl->set_title('商家首頁');
			$tpl->render("enterprise","home",true);
		}
	}


	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path) {

		$table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';
		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;
	}


	//商家登入頁
	public function login() {
		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);

		$_SESSION['sajamanagement']['enterprise'] = '';
		setcookie("enterpriseid", "", time() - 3600, "/", COOKIE_DOMAIN);

		// 如同時具有user及商家身分, 須先清除user session 及 cookie
		// 暫不清除
		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';
		setcookie("auth_id", '', time()-3600, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()-3600, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", '', time()-3600, "/", COOKIE_DOMAIN);

		$tpl->set_title('商家登入');
		$tpl->render("enterprise","login",true);
	}
	public function preload(){
		global  $enterprise;
		if($_REQUEST['json']=='Y') {
		  	$result=array();
		  	$ret=array();
		  	$ret[]=array('api_id'=>0,'version'=>$enterprise->getOsVer());
		  	//上線後看商家版後台首頁廣告banner的最新的insertt
		  	$ret[]=array('api_id'=>1,'version'=>'1575270438');
		  	$result['retCode']=1;
		  	$result['retObj']=$ret;
		  	echo json_encode($result);
		  	die();
		}
	}

	public function dashboard(){
		global $db, $config, $enterprise;
		$result=array();
		enterprise_login_required();
		$cntdate = empty($_REQUEST['cntdate']) ? date("Y-m-d"): htmlspecialchars($_REQUEST['cntdate']);
		try{
			//單日收益
			$sum_rec= number_format(round($enterprise->bonus_rec($this->enterpriseid,$cntdate,'sum')));
            //單日訂單數
			$count_rec= number_format($enterprise->bonus_rec($this->enterpriseid,$cntdate,'count'));
			//目前的總點數
			$now_sp=number_format($enterprise->getEnterpriseTotalBonus($this->enterpriseid));
			$result['retCode']=1;
			$result['sum_rec']=(is_null($sum_rec))?0:$sum_rec;
			$result['count_rec']=(is_null($count_rec))?0:$count_rec;
			$result['now_sp']=$now_sp;
		}catch(Exception $e ){
			$result['retCode']=$e->getCode();
			$result['retMsg']=$e->getMessage();
		}
	  	echo json_encode($result);
	  	die();
	}

    //首頁廣告先寫死之後要改按後台
	public function ad(){
		global $db, $tpl, $config, $enterprise;
		if ($_REQUEST['json']=='Y'){
			$result=array();
		  	$ret=array();

		  	$action=array("type"=>"2","url_title"=>"","url"=>"http://evt.saja.vip/dlapp.html","imgurl"=>"https://s3.hicloud.net.tw/evt.saja.vip/images/saja_bid_car_step.png","page"=>"1","productid"=>"");
		  	$ret[]=array('ad_id'=>1,'thumbnail'=>'https://s3.hicloud.net.tw/img.saja.com.tw/site/images/site/ad/19cd37aca04057daef8dc3a5cbc3aaa1.png','action_list'=>$action);

		  	$action=array("type"=>"2","url_title"=>"","url"=>"http://bit.ly/2CAKyUT","imgurl"=>"https://s3.hicloud.net.tw/evt.saja.vip/images/saja_bid_car_step.png","page"=>"1","productid"=>"");
		  	$ret[]=array('ad_id'=>2,'thumbnail'=>'https://s3.hicloud.net.tw/img.saja.com.tw/site/images/site/ad/71d115272760294b95839bd1e3ae3e0e.png','action_list'=>$action);


		  	$result['retCode']=1;
		  	$result['retObj']=$ret;
		  	echo json_encode($result);
		  	die();
		}
	}
	//商家qrcode登入頁
	public function loginto() {
		global $tpl;

		//設定 Action 相關參數
		set_status($this->controller);
		error_log("[c/enterprise/loginto] : ".json_encode($_REQUEST));
		if(empty($this->enterpriseid)){
			$tpl->assign('REQUEST_DATA', $_REQUEST);

			$tpl->set_title('');
			$tpl->render("enterprise","loginto",true);
		}else{
		  $action = $_REQUEST['_tourl'];
			if(empty($action)){
			  $action="/site/enterprise/home";
			}
			echo '<!DOCTYPE html>';
			echo '<html>';
			echo '<body>';
			echo '<form name="login" id="login" action="'.$action.'" method="post">';
			if(is_array($_REQUEST)){
				foreach ($_REQUEST as $key => $value){
					echo '<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
				}
			}
			echo '</form>';
			echo '<script>document.forms.login.submit();</script>';
			echo '</body>';
			echo '</html>';
		}

		exit;
	}


	//商家qrcode登入處理頁
	public function login_action() {
		global $db, $tpl, $config, $enterprise;

		//設定 Action 相關參數
		set_status($this->controller);

		$query = "SELECT *,est.name as exchange_store_name,est.name as name FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise` e
		            JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` ep
				          ON e.enterpriseid=ep.enterpriseid
				        JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_shop` es
				          ON e.enterpriseid=es.enterpriseid
		 LEFT OUTER JOIN `{$config['db'][3]['dbname']}`.`{$config['default_prefix']}exchange_store` est
				          ON e.esid=est.esid
		           WHERE e.prefixid = '{$config['default_prefix_id']}'
			           AND e.loginname = '{$_POST['loginname']}'
			           AND e.switch = 'Y'
		           LIMIT 1
		";
		error_log("[c/enterprise/login_action]:".$query);
		$table = $db->getQueryRecord($query);

		if(empty($table['table']['record'])){
			//'帳號不存在'
			echo '<!DOCTYPE html><html><body><script>alert("帳號不存在");</script>';
			echo '<form name="login_action" id="login_action" action="/site/enterprise/loginto" method="post">';
			if (is_array($_REQUEST)){
				foreach($_REQUEST as $key => $value){
					echo '<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
				}
			}
			echo '</form>';
			echo '<script>document.forms.login_action.submit();</script>';
			echo '</body>';
			echo '</html>';
		}else{
			$_SESSION['sajamanagement']['enterprise'] = '';

			include_once(LIB_DIR ."/convertString.ini.php");
			$this->str = new convertString();

			$enterprisedata = $table['table']['record'][0];
			$passwd = $this->str->strEncode($_POST['passwd'], $config['encode_key']);

			if($enterprisedata['passwd'] === $passwd){

				if(!empty($enterprisedata['thumbnail_file']))
				  $enterprisedata['thumbnail']=BASE_URL."/management/images/headimgs/".$enterprisedata['thumbnail_file'];
				else
				  $enterprisedata['thumbnail']=$enterprisedata['thumbnail_url'];

				error_log("[c/enterprise/loginto] thumbnail : ".$enterprisedata['thumbnail']);

				$query2 = "SELECT u.userid, usa.verified FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user` u
					LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_enterprise_rt` uer
								       ON u.userid=uer.userid
					LEFT OUTER JOIN `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}user_sms_auth` usa
								       ON u.userid=usa.userid
						        WHERE u.prefixid = '{$config['default_prefix_id']}'
							        AND uer.enterpriseid = '{$enterprisedata['enterpriseid']}'
							        AND u.switch = 'Y'
						        LIMIT 1
				";
				$table2 = $db->getQueryRecord($query2);

				$enterprisedata['sms'] = $table2['table']['record'][0]['verified'];
				$userid = $table2['table']['record'][0]['userid'];

				$_SESSION['auth_id'] = $userid;
				$_SESSION['sajamanagement']['enterprise'] = $enterprisedata;
				setcookie("auth_id", $userid, time()+60*60*24*30, "/", COOKIE_DOMAIN);
				setcookie("enterpriseid", $enterprisedata['enterpriseid'], time()+86400, "/", COOKIE_DOMAIN);

				$action = $_REQUEST['_tourl'];
				if(empty($action)){
				  $action="/site/enterprise/home";
				}
				echo '<!DOCTYPE html>';
				echo '<html>';
				echo '<body>';
				echo '<form name="login_action" id="login_action" action="'.$action.'" method="post">';
				if(is_array($_REQUEST)){
					foreach($_REQUEST as $key => $value){
						echo '<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
					}
				}
				echo '</form>';
				echo '<script>document.forms.login_action.submit();</script>';
				echo '</body>';
				echo '</html>';
			}else{
				//'密碼不正確'
				echo '<!DOCTYPE html><html><body><script>alert("密碼不正確");</script>';
				echo '<form name="login_action" id="login_action" action="loginto" method="post">';
				if(is_array($_REQUEST)){
					foreach($_REQUEST as $key => $value){
						echo '<input type="hidden" name="'.$key.'" id="'.$key.'" value="'.$value.'" />';
					}
				}
				echo '</form>';
				echo '<script>document.forms.login_action.submit();</script>';
				echo '</body>';
				echo '</html>';
			}
		}

		exit;
	}


	/*
	 *	商家登出功能
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function logout() {
		global $tpl;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$enterpriseid = empty($_POST['enterpriseid']) ? htmlspecialchars($_GET['enterpriseid']) : htmlspecialchars($_POST['enterpriseid']);
		// clear enterprise session and cookie
		$_SESSION['sajamanagement']['enterprise'] = '';
		setcookie("enterpriseid", "", time() - 3600, "/", COOKIE_DOMAIN);

		// clear user session and cookie information.
		$_SESSION['user'] = '';
		$_SESSION['auth_id'] = '';
		$_SESSION['auth_email'] = '';
		$_SESSION['auth_secret'] = '';
		setcookie("auth_id", '', time()-3600, "/", COOKIE_DOMAIN);
		setcookie("auth_email", '', time()-3600, "/", COOKIE_DOMAIN);
		setcookie("auth_secret", '', time()-3600, "/", COOKIE_DOMAIN);

		if($json=='Y'){
			enterprise_log($enterpriseid,'user','logout','app');
			$ret = getRetJSONArray(1,'登出成功','MSG');
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('');
			enterprise_log($enterpriseid,'user','logout','web');
			$tpl->render("enterprise","login",true);
		}
	}


	/*
	 *	商家點數交易清單功能
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 *	$p					int					分頁編號
	 */
	public function storebonuslist() {
		global $tpl, $enterprise;

		//設定 Action 相關參數
		set_status($this->controller);
		enterprise_login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);
		$ctype = empty($_REQUEST['ctype']) ? 0 : htmlspecialchars($_REQUEST['ctype']);
		error_log("[enterprise.storebonuslist] enterpriseid : ".$this->enterpriseid);
		$store_bonus_list = $enterprise->store_bonus_list($this->enterpriseid,$ctype);
		//判斷來源
		if($json=='Y'){
		  $ret=getRetJSONArray(1,'OK','LIST');
		  //判斷清單是否存在
		  if($store_bonus_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $store_bonus_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = array();
				}else{
					$ret['retObj']['data'] = $store_bonus_list['table']['record'];
				}
				$ret['retObj']['page'] = $store_bonus_list['table']['page'];
      }else{
        $page['rec_start'] = 0;
        $page['totalcount'] = 0;
        $page['totalpages'] = 1;
        $page['perpage'] = 50;
        $page['page'] = 1;
        $page['item'] = array(array("p" => 1 ));
        $ret['retObj']['data'] = array();
        $ret['retObj']['page'] = $page;
		  }
		  echo json_encode($ret);
		  exit;
		}else{
			$tpl->assign('store_bonus_list', $store_bonus_list);
			$tpl->set_title('交易明細');
			$tpl->render("enterprise","storebonuslist",true);
		}
	}


	/*
	 *	商戶資料功能
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function member() {
		global $tpl, $enterprise;

		//設定 Action 相關參數
		set_status($this->controller);
		enterprise_login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		error_log("[enterprise.member] enterpriseid : ".$this->enterpriseid);
		//取得商家企業資料
		$member_data = $enterprise->getEnterpriseDetail($this->enterpriseid);
		//取得商家會員資料
		$user = $enterprise->getEnterpriseUser($this->enterpriseid);
		if($user['verified'] == 'Y'){
			$_SESSION['sajamanagement']['enterprise']['sms'] = $user['verified'];
		}
		if($json == 'Y'){
			$ret = getRetJSONArray(1,'取得資料','LIST');
			$ret['retObj'] = $member_data;
			$ret['retObj']['userid'] = $user['userid'];
			$ret['retObj']['verified'] = $user['verified'];
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('商戶資料');
			$tpl->assign('member_data',$member_data);
			$tpl->render("enterprise","member",true);
		}
	}


	public function member_register() {
		global $tpl, $enterprise, $user, $channel;

		//設定 Action 相關參數
		set_status($this->controller);
		include_once(LIB_DIR ."/convertString.ini.php");
		$this->str = new convertString();

		$userid = empty($_POST['auth_id']) ? ($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$u = (isset($_GET['user_src']) ) ? $_GET['user_src'] : '';

		if(!empty($u)){
			$user_src = $u;
		}else{
			$user_src = $userid;
		}

		$channel = $channel->get_channel();
		$user = $user->get_user_profile($user_src);

		$tpl->assign('channel', $channel);
		$tpl->assign('nickname', $user['nickname']);
		$tpl->assign('user_src', $user_src);
		$tpl->assign('userid', $user_src);
		$tpl->render("enterprise","register",true);
	}


	/*
	 *	商戶資料修改功能
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function member_update() {
		global $tpl, $enterprise;

		//設定 Action 相關參數
		set_status($this->controller);

		//$userid = empty($_POST['auth_id']) ? ($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		enterprise_login_required();
		//取得會員關聯的商戶編號
		$userenterprise = $enterprise->getUserEnterpriseE($enterpriseid);
		//var_dump($userid);
		//die();
		$enterpriseid = empty($this->enterpriseid) ? $userenterprise['enterpriseid'] : $this->enterpriseid;
		error_log("[enterprise.member] enterpriseid : ".$this->enterpriseid);

		$enterprisedata = $enterprise->getEnterpriseDetail($enterpriseid);

		if(!empty($userid) && empty($this->enterpriseid)){
			$_SESSION['sajamanagement']['enterprise'] = $enterprisedata;
			setcookie("enterpriseid", $enterprisedata['enterpriseid'], time()+86400, "/", COOKIE_DOMAIN);
		}

		//取得企業詳細資料
		$enterprise_profile = $enterprise->getEnterpriseProfile($enterpriseid);

		//取得企業帳戶資料
		$enterprise_account = $enterprise->getEnterpriseAccount($enterpriseid);

		//取得企業營業資料
		$enterprise_shop = $enterprise->getEnterpriseShop($enterpriseid);

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $enterprisedata;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('商戶信息');
			$tpl->assign('enterprise',$enterprisedata);
			$tpl->assign('enterprise_profile',$enterprise_profile);
			$tpl->assign('enterprise_account',$enterprise_account);
			$tpl->assign('enterprise_shop',$enterprise_shop);

			$tpl->render("enterprise","member_update",true);
		}
	}


	/*
	 *	商戶提現功能
	 *	$json				varchar				瀏覽工具判斷 (Y:APP來源)
	 */
	public function cash() {
		global $tpl, $enterprise;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//設定 Action 相關參數
		set_status($this->controller);
		enterprise_login_required();


		//取得企業帳戶資料
		$enterprise_account = $enterprise->getEnterpriseAccount($this->enterpriseid);

		//取得鯊魚點數
		$total_net_bonus = $enterprise->getEnterpriseTotalBonus($this->enterpriseid);

		error_log("[enterprise.cash] enterpriseid : ".$this->enterpriseid);

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $enterprise_account;
			$ret['retObj']['data']['bonus'] = $total_net_bonus;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('商戶資料');
			$tpl->assign('member_data',$enterprise_account);
			$tpl->assign('total_net_bonus',$total_net_bonus);
			$tpl->render("enterprise","cash",true);
		}
	}


	public function storeproduct() {
		global $tpl, $enterprise, $product;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$flash = empty($_POST['flash']) ? htmlspecialchars($_GET['flash']) : htmlspecialchars($_POST['flash']);

		//設定 Action 相關參數
		set_status($this->controller);
		enterprise_login_required();

		//取得企業資料
		$member_data = $enterprise->getEnterpriseDetail($this->enterpriseid);
		error_log("[enterprise.storeproduct] storeid : ".$member_data['storeid']);
		//取得商家商品資料
		$product_data = $product->enterprise_product_list('', $member_data['storeid'], $flash);
		error_log("[enterprise.storeproduct] product_data : ".json_encode($product_data));

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['enterprise'] = $member_data;
			$ret['retObj']['data']['product_data'] = $product_data;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('商戶資料');
			$tpl->assign('enterprise',$member_data);
			$tpl->assign('product_data',$product_data);
			$tpl->render("enterprise","storeproduct",true);
		}
	}


	public function storeproduct_add() {
		global $tpl, $enterprise;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//設定 Action 相關參數
		set_status($this->controller);
		enterprise_login_required();

		//取得企業資料
		$member_data = $enterprise->getEnterpriseDetail($this->enterpriseid);
		error_log("[enterprise.storeproduct_add] enterpriseid : ".$this->enterpriseid);

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['enterprise'] = $member_data;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('商戶資料');
			$tpl->assign('member_data',$member_data);
			$tpl->render("enterprise","storeproduct_add",true);
		}
	}


	public function storeproduct_update() {
		global $tpl, $enterprise, $product;

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$pid = empty($_POST['pid']) ? htmlspecialchars($_GET['pid']) : htmlspecialchars($_POST['pid']);
		error_log("[enterprise.storeproduct_update] post : ".json_encode($_GET));

		//設定 Action 相關參數
		set_status($this->controller);
		enterprise_login_required();

		//取得企業資料
		$member_data = $enterprise->getEnterpriseDetail($this->enterpriseid);

		//取得商品資料
		$product_data = $product->enterprise_product_detail($pid);

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data']['enterprise'] = $member_data;
			$ret['retObj']['data']['product'] = $product_data;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('商戶資料');
			$tpl->assign('member_data',$member_data);
			$tpl->assign('product_data',$product_data);
			$tpl->render("enterprise","storeproduct_update",true);
		}
	}


	public function getAvailChannelList() {
		global $channel;

		$country=$REQUEST['countryid'];
		$provinceid=$REQUEST['provinceid'];
		$result=$channel->get_channel();
		$ret=getRetJSONArray(1,'OK','LIST');
		$ret['retObj']['data'] = $result;
		echo json_encode($ret);
		exit;
	}


	/*
	* about menu
	*/
	public function about() {
		global $tpl, $enterprise, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		enterprise_login_required();

		include_once(LIB_DIR ."/convertString.ini.php");
		$this->str = new convertString();

		// 鯊魚點數
		$tpl->set_title('');
		$tpl->render("enterprise","about",true);
	}


	public function shareQrcode() {
		global $tpl, $enterprise, $user;

		//設定 Action 相關參數
		set_status($this->controller);

		enterprise_login_required();

		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);

		include_once(LIB_DIR ."/convertString.ini.php");
		$this->str = new convertString();

		//取得商戶關聯的會員編號
		$userenterprise = $enterprise->getEnterpriseDetail($this->enterpriseid);
		//商家資訊

		//呼朋引伴推薦QRcode url
		$user_src = base64_encode($this->str->encryptAES128($config['encode_key'],$userenterprise['userid'].'&&1')); //var_dump($u);string(24) "kgl1YRLcAQH9xlFIhbIEwQ=="
		$share_url = BASE_URL . APP_DIR .'/user/uni_register/?u='.$userenterprise['userid'];

		$tpl->assign('kind', $kind);
		$tpl->assign('share_url', $share_url);
		$tpl->set_title('');
		$tpl->render("enterprise","shareQrcode",true);
	}



/*
public function getBidHistory() {
		global $tpl, $bid, $product;

		//設定 Action 相關參數
		set_status($this->controller);

		$productid	= empty($_POST['productid']) ? htmlspecialchars($_GET['productid']) : htmlspecialchars($_POST['productid']);
		$userid		= empty($_POST['auth_id']) ? htmlspecialchars($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$type 		= empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$json 		= empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$page 	 	= empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$perpage 	= empty($_POST['perpage']) ? htmlspecialchars($_GET['perpage']) : htmlspecialchars($_POST['perpage']);

		//取得商品的相關資料
		$proinfo = $product->get_info($productid);

		//判斷此商品的下標狀態是否在 closed=’Y’ (已結標) 或 closed=’NB’ (流標)
		if (($proinfo['closed'] == 'Y') || ($proinfo['closed'] == 'NB')){

			if ($type == 'A'){
				//取得該商品的使用者下標資料
				$bid_check = $bid->getUserSajaHistory($userid, $productid);
				error_log("[bid/getBidHistory] uid = ".$userid." pid = ".$productid);

				//判斷是否有下標過此商品
				if (!empty($bid_check)) {
					//取得下標金標數統計
					$bid_list = $bid->get_bid_list($productid, $type, $page, $perpage);
				}else{
					$bid_list = array();
				}
			}else{
				$bid_list = $bid->get_bid_list($productid, $type);
			}

			error_log("[bid/getBidHistory] type = ".$type);

			if($json=='Y') {
				$ret=getRetJSONArray(1,'OK','LIST');
				$ret['retObj']['data'] = (!empty($bid_list['record'])) ? $bid_list['record'] : array();
				$ret['retObj']['page'] = (!empty($bid_list['page'])) ? $bid_list['page'] : new stdClass;
				echo json_encode($ret);
				exit;
			}else{
				$tpl->assign('bid_list', $bid_list);

				//設定分頁
				if(empty($bid_list['table']['page']) ){
					$page_content = array();
				}else{
					$page_path = $tpl->variables['status']['status']['path'];
					$page_content = $this->set_page($bid_list, $page_path);
				}
				$tpl->assign('page_content', $page_content);
				$tpl->set_title('');
				$tpl->render("bid","getBidHistory",true);
			}

		}else{
			if($json=='Y') {
				$ret=getRetJSONArray(-1,'ERROR','MSG');
				echo json_encode($ret);
				exit;
			}else{
				$tpl->assign('type', $type);
				$tpl->assign('productid', $productid);
				$tpl->set_title('');
				$tpl->render("bid","home",true);
			}
		}

	}

	http://test.shajiawang.com/site/phpqrcode/?data=
	http://test.shajiawang.com/site/enterprise/vendorConfirm/?tx_key=ODcwMnwyMzY4OTc3MTA4MmUzM2MxZjQxZGM5MThiMjM4NjI0Zg==
*/

	/*
	 *	商家確認交易
	 *	$json 	varchar 	瀏覽工具判斷 (Y:APP來源)
	 */
	public function vendorConfirm() {

		$cdnTime = date("YmdHis");
		global $tpl, $enterprise, $user;

		//設定 Action 相關參數
		set_status($this->controller);
		enterprise_login_required();

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$auth_id = empty($_POST['auth_id']) ? ($_SESSION['auth_id']) : htmlspecialchars($_POST['auth_id']);
		$etprs_id=empty($_SESSION['sajamanagement']['enterprise']['enterpriseid']) ? $_REQUEST['enterpriseid'] : $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
        $ret=array();
		if(($_REQUEST['tx_status']=='-2')){
			$sql=" UPDATE saja_exchange.saja_exchange_vendor_record SET txt_status='-2',modifyt=NOW() where evrid='".htmlspecialchars($_REQUEST['evrid'])."' ";
			$ts = $cdnTime;

			$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
			$client = new WebSocket\Client($wss_url);
			error_log(763);
			$ary_bidder = array('actid'		=> 'B2C_Vendor_Cancel',
								'evrid' 		=> $_REQUEST['evrid'],
								'productid' => "{$product['productid']}",
								'ts'		=> $cdnTime,
								'sign'		=> MD5($ts."|sjW333-_@")
						);
			error_log(json_encode($ary_bidder));
			error_log($wss_url);
			$client->send(json_encode($ary_bidder));

			if ((!(empty($_POST['json'])))&&($_POST['json']=='Y')){
				$ret['retCode']=1;
				$ret['nextpage']=BASE_URL.'/site/enterprise/?'.$cdnTime;
				echo json_encode($ret);
			}else{
				header('Location:'.BASE_URL.'/site/enterprise/?'.$cdnTime);
			}
			die();
		}
		//取得商家關聯參數
		if(!empty($_REQUEST['tx_key'])){
			$tx_key=base64_decode($_REQUEST['tx_key']);

			$arrTxKey=explode("|",$tx_key);
			$evrid=$arrTxKey[0];
			$tx_code_md5=$arrTxKey[1];

			error_log("[enterprise/vendorConfirm] evrid : ".$evrid);
			error_log("[enterprise/vendorConfirm] tx_code_md5 : ".$tx_code_md5);
		}


		if(empty($evrid)) {
			if($json == 'Y'){
				$ret=getRetJSONArray(10007, '交易序號有誤 !!', 'MSG');
				echo json_encode($ret);
				exit;
			}else{
				$tpl->assign('MSG', '交易序號有誤 !!');
			}
		}

		/*

		//取得會員關聯的商戶編號
		$userenterprise = $enterprise->getUserEnterprise($enterprise_userid);

		$enterpriseid = empty($this->enterpriseid) ? $userenterprise['enterpriseid'] : $this->enterpriseid;
		error_log("[enterprise.member] enterpriseid : ".$this->enterpriseid);

		$enterprisedata = $enterprise->getEnterpriseDetail($enterpriseid);

		if(!empty($enterprise_userid) && empty($this->enterpriseid)){
			$_SESSION['sajamanagement']['enterprise'] = $enterprisedata;
			setcookie("enterpriseid", $enterprisedata['enterpriseid'], time()+86400, "/", COOKIE_DOMAIN);
		}

		//取得企業詳細資料
		$enterprise_profile = $enterprise->getEnterpriseProfile($enterpriseid);

		//取得企業帳戶資料
		$enterprise_account = $enterprise->getEnterpriseAccount($enterpriseid);

		//取得企業營業資料
		$enterprise_shop = $enterprise->getEnterpriseShop($enterpriseid);

		if($json == 'Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			$ret['retObj']['data'] = $enterprisedata;
			echo json_encode($ret);
			exit;
		}else{
			$tpl->set_title('商戶信息');
			$tpl->assign('enterprise',$enterprisedata);
			$tpl->assign('enterprise_profile',$enterprise_profile);
			$tpl->assign('enterprise_account',$enterprise_account);
			$tpl->assign('enterprise_shop',$enterprise_shop);

			$tpl->render("enterprise","member_update",true);
		}*/

		$record = $enterprise->getQrcodeTxRecord($evrid);
		error_log("[enterprise/vendorConfirm] QrcodeTxRecord : ".json_encode($record));


		/*
		   $arrUpd=array();
		   $tx_status=$this->io->input["post"]["tx_status"];
		   if($tx_status=='-2') {
				$arrUpd['tx_status']=$tx_status;
				$arrUpd['keyin_time']=$cdnTime;
				$retCode=updQrcodeTxRecord($arrUpd,$arrCond,$this->model);
				if($retCode) {
				   $retArr['retCode']='-2';
				   $retArr['retMsg']=('交易已取消 !!');
					if($_REQUEST['json'] == 'Y') {
						$ret['retCode'] = 10006;
						$ret['retMsg'] = '交易已取消 !!';
						$ret['retType'] = 'MSG';
						echo json_encode($ret);
						exit;
					}else{
					   // $WSClient=new Client($this->config->wss_url);
					   // $WSClient->send("NTFY|".$evrid."|".$tx_status);
					   replyAndExit($retArr);
					}
				}
			}
			*/



			/*
			if(empty($record['evrid'])) {
				$retArr['retCode']=-100;
				$retArr['retMsg']=('交易資料有缺 !!');
				if($_REQUEST['json'] == 'Y') {
					$ret['retCode'] = 10005;
					$ret['retMsg'] = '交易資料有缺 !!';
					$ret['retType'] = 'MSG';
					echo json_encode($ret);
					exit;
				}else{
					replyAndExit($retArr);
				}
			}

		   // 验证时间
		   $keyin_ts = time();
		   if($keyin_ts-strtotime($record['qrcode_time'])>180) {
			  $retArr['retCode']=-101;
			  $retArr['retMsg']=('消費者交易時效過期 !!');
				if($_REQUEST['json'] == 'Y') {
					$ret['retCode'] = 10001;
					$ret['retMsg'] = '消費者交易時效過期 !!';
					$ret['retType'] = 'MSG';
					echo json_encode($ret);
					exit;
				}else{
					replyAndExit($retArr);
					exit;
				}
		   }

		   // 交易码验证
		   if($tx_code_md5!=md5($record['tx_code'])) {
			  $retArr['retCode']=-102;
			  $retArr['retMsg']=('交易碼驗證失敗 !!');
				if($_REQUEST['json'] == 'Y') {
					$ret['retCode'] = 10002;
					$ret['retMsg'] = '交易碼驗證失敗 !!';
					$ret['retType'] = 'MSG';
					echo json_encode($ret);
					exit;
				}else{
					replyAndExit($retArr);
				}
		   }


		SELECT ep.enterpriseid, ep.companyname, ep.marketingname, ep.thumbnail_file, ep.thumbnail_url, ep.name, ep.uniform, ep.phone, ep.fax, ep.countryid, ep.channelid, ep.area, ep.cityid, ep.address, ep.email
					FROM `{$config['db'][0]['dbname']}`.`{$config['default_prefix']}enterprise_profile` ep
				  WHERE ep.prefixid = '{$config['default_prefix_id']}'
					AND ep.enterpriseid = '{$eid}'
					AND ep.switch = 'Y'
					LIMIT 1
		*/

		//帶出商家名稱
		$query = "SELECT * , st.name as marketingname
				FROM `saja_user`.`saja_enterprise_profile` p
				JOIN `saja_user`.`saja_enterprise_shop` s
				  ON p.enterpriseid = s.enterpriseid
				JOIN `saja_user`.`saja_enterprise` e
				  ON p.enterpriseid = e.enterpriseid
		   LEFT OUTER JOIN `saja_exchange`.`saja_exchange_store` st
				  ON e.esid = st.esid
			   WHERE p.prefixid = 'saja'
			     AND p.enterpriseid = '".$enterprise_userid."'
			     AND p.switch = 'Y'
				 AND s.switch = 'Y'
				 AND e.switch = 'Y'
				 AND st.switch = 'Y'
				 ";
		   //error_log($query);
		   //$result = $this->model->getQueryRecord($query);
		   /*
		   if(empty($result['table']['record'][0]['enterpriseid'])) {
			  $retArr['retCode']=-4;
			  $retArr['retMsg']=('商家資料有誤 !!');
				if($_REQUEST['json'] == 'Y') {
					$ret['retCode'] = 10004;
					$ret['retMsg'] = '商家資料有誤 !!';
					$ret['retType'] = 'MSG';
					echo json_encode($ret);
					exit;
				}else{
					replyAndExit($retArr);
				}
		   }*/

		   //$enterprise=$result['table']['record'][0];

		//取得企業詳細資料
		$enterprise_profile = $enterprise->getEnterpriseProfile($etprs_id);

		//取得企業營業資料
		$enterprise_shop = $enterprise->getTxEnterpriseShop($etprs_id);


		   $arrUpd['vendorid']=$etprs_id;
		   $arrUpd['tx_status']='2';
		   $arrUpd['keyin_time']=$cdnTime;
		   //$retCode=updQrcodeTxRecord($arrUpd,$arrCond,$this->model);

			$txArr=array();
			$txArr['evrid']=$record['evrid'];
			$txArr['userid']=$record['userid'];
			$txArr['vendorid']=$enterprise_profile['enterpriseid'];
			$txArr['companyname']=$enterprise_profile['companyname'];
			$txArr['vendor_name']=$enterprise_shop['name'];
			$txArr['tx_status']='2';
			$txArr['tx_currency']=$record['tx_currency'];
			$txArr['keyin_time']=$arrUpd['keyin_time'];
			$txArr['tx_code']=$record['tx_code'];
			$txArr['tx_code_md5']=$tx_code_md5;
			$txArr['ClientType']='web';
			if ((!(empty($_POST['json'])))&&($_POST['json']=='Y')) $txArr['ClientType']='app';
			if(!empty($enterprise_profile['thumbnail_file']))
				$txArr['thumbnail']=BASE_URL."/management/images/headimgs/".$enterprise_profile['thumbnail_file'];
			else
				$txArr['thumbnail']=$enterprise_profile['thumbnail_url'];

			$tpl->assign('txArr',$txArr);
			error_log("[enterprise/vendorConfirm] txArr : ".json_encode($txArr));

			/*
			if($_REQUEST['json'] == 'Y') {
				$ret['retCode'] = 1;
				$ret['retMsg'] = '資料取得';
				$ret['type'] = 'LSIT';
				$ret['data'] = $txArr;
				echo json_encode($ret);
				exit;
			}else{
				$this->tplVar('txArr' , $txArr) ;
				$this->display();
			}
			*/
		$ts = $cdnTime;

		$wss_url=(BASE_URL=='https://www.saja.com.tw')?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
		$client = new WebSocket\Client($wss_url);
		error_log(991);
		$ary_bidder = array('actid'		=> 'B2C_Vendor_Processing',
							'evrid' 		=> $record['evrid'],
							'productid' => "{$product['productid']}",
							'ts'		=> $cdnTime,
							'sign'		=> MD5($ts."|sjW333-_@")
					);
		error_log(json_encode($ary_bidder));
		error_log($wss_url);
		$client->send(json_encode($ary_bidder));
		$tpl->set_title('商家確認交易');
		if ((!(empty($_POST['json'])))&&($_POST['json']=='Y')){
			$ret['retCode']=1;
			$ret['retObj']=$txArr;
			echo json_encode($ret);
		}else{
			$tpl->render("enterprise", "vendorConfirm", true);
		}
		die();

	}


	/*
	function replyAndExit($retArr) {
	   $json_str=json_encode($retArr);
	   error_log("[vendorConfirm2] ".$json_str);
	   echo $retArr['retMsg'];
	   exit;
	}
	function getSafeStr($str) {
		 $safeStr=addslashes($str);
		 if($safeStr!=$str) {
			error_log("[getSafeStr2] ".$str."-->".$safeStr);
		 }
		 return $safeStr;
	}
	function getQrcodeTxRecord($arrCond,$db) {

		if(count($arrCond)>0) {
		   $query=" SELECT evr.*, sum(IFNULL(b.amount,0)) as curr_bonus ";
		   $query.=" FROM saja_user.saja_user_profile up ";
		   $query.=" LEFT JOIN saja_cash_flow.saja_bonus b ";
		   $query.=" ON up.switch='Y' and b.switch='Y'  AND up.userid=b.userid ";
		   $query.=" LEFT JOIN saja_exchange.saja_exchange_vendor_record evr ";
		   $query.=" ON up.switch='Y' and evr.switch='Y' AND up.userid=evr.userid ";
		   $query.=" WHERE 1=1 ";

		   foreach($arrCond as $key => $value) {
			  $query.=(" AND ".$key."='".addslashes($value)."'");
		   }

		   error_log("[vendorConfirm2]".$query);
		   $table = $db->getQueryRecord($query);

		   if(!empty($table['table']['record'][0]['evrid'])) {
			  return $table['table']['record'][0];
		   }
	   }
	   return false;
	}
	// 修改Qrcode交易資料
	function updQrcodeTxRecord($arrUpd,$arrCond,$db) {

		if(empty($arrUpd)) {
		  return false;
		}
		$ret=false;
		$sql=" UPDATE saja_exchange.saja_exchange_vendor_record SET modifyt=NOW() ";
		foreach($arrUpd as $key => $value) {
		   $sql.=(" ,".$key."='".addslashes($value)."'");
		}

		$sql.=" WHERE 1=1 ";
		if(count($arrCond)>0) {
		   foreach($arrCond as $key => $value) {
			   $sql.=(" AND ".$key."='".addslashes($value)."'");
		   }
		}
		error_log("[vendorConfirm2] ".$sql);
		$ret = $db->query($sql);
		return $ret;
	}
	*/
	//20191121
		public function genVenderTxQrcode() {
		global $db, $config,  $mall,$tpl, $enterprise;
		//設定 Action 相關參數
		set_status($this->controller);
		error_log("[genVenderTxQrcode]");
		date_default_timezone_set('Asia/Taipei');
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$etprs_id=$_REQUEST['enterpriseid'];
		$ret['retCode'] = 0;
		$ret['retMsg'] = "參數錯誤";
		if (!(empty($etprs_id))){
			$qrcode=array();
			$qrcode['act_type']='9';
			$qrcode['intro_by']=$etprs_id;
			//$qrcode['url']=BASE_URL.APP_DIR."/phpqrcode/?data=".json_encode($qrcode);
			//echo json_encode($qrcode);
				$ret['retCode'] = 1;
				$ret['retMsg'] = '資料取得';
				$ret['retObj'] = $qrcode;
				echo json_encode($ret);
		}
		die();
	}

	public function rollBackTx(){

		global $db, $config, $tpl, $mall, $member, $user, $enterprise;
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$enterpriseid =empty($_SESSION['sajamanagement']['enterprise']['enterpriseid']) ? $_REQUEST['enterpriseid'] : $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
		$evrid=empty($_POST['evrid']) ? htmlspecialchars($_GET['evrid']) : htmlspecialchars($_POST['evrid']);
		if ((!(empty($json)))&& ($json=='Y')){
			enterprise_login_required();
		}
		$rtn=$enterprise->rollBackTx($enterpriseid,$evrid);
		//var_dump($rtn);
		echo json_encode($rtn);
		die();
	}
	public function withDraw(){

		global $db, $config, $tpl, $mall, $member, $user, $enterprise;
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$enterpriseid =empty($_SESSION['sajamanagement']['enterprise']['enterpriseid']) ? $_REQUEST['enterpriseid'] : $_SESSION['sajamanagement']['enterprise']['enterpriseid'];
		$amount=empty($_POST['amount']) ? htmlspecialchars($_GET['amount']) : htmlspecialchars($_POST['amount']);
		if ((is_numeric($amount))&&($amount>0)){
		}else{
			$ret=array();
			$qrcode=array();
			$qrcode['type']=5;
			$qrcode['page']=0;
			$qrcode['msg']="提現金額只接受大於零的數字 !";
			$ret['retCode']=1;
			$ret['action_list']=$qrcode;

			echo json_encode($ret);
			die();
		}
		if ((!(empty($json)))&& ($json=='Y')){
			enterprise_login_required();
		}
		$rtn=$enterprise->withDraw($enterpriseid,$amount);
		echo json_encode($rtn);
		die();
	}
}
