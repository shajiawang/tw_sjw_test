<?php

/*
 * Push Controller 推播
 */

class Push {
	public $controller = array();
	public $params = array();
	public $id;

	/*
	* home
	*/
	public function home() {

		global $db, $config;

		//設定 Action 相關參數
		set_status($this->controller);
		
		//手動排程推播
		$this->push_by_hand();
		
		//每日自動發推播送超殺券
		$this->push_scode();
	}

	/*
	* near_closed 快要結標
	*/
	public function near_closed($mins=30){

		global $db, $config, $push;
		//設定 Action 相關參數
		set_status($this->controller);

		$redis = getRedis('','');
		$today = date("Y-m-d");
		// $redis->del($today.'_near_close_pushed');
		$pushed_product_id = $redis->get($today.'_near_close_pushed');

		$product_list = $push->get_product_list($mins,$pushed_product_id);
		$push_msg_list = $push->get_auto_push_msg("1");
		$title = $push_msg_list[0]['title'];
		$body = $push_msg_list[0]['body'];
		$action = $push_msg_list[0]['action'];
		$page = $push_msg_list[0]['page'];
		$new_pushed_product_id = array();

		if (!empty($product_list)) {
			foreach ($product_list as $key => $value) {
				$token[0] = $value['token'];
				$product_name = '《'.$value['product_name'].'》';
				$nickname = $value['nickname'];
				$productid = $value['productid'];
				$page = $value['page'];
				$messege = str_replace("{productname}", $product_name, $body);
				$messege = str_replace("{nickname}", $nickname, $messege);
				$new_pushed_product_id[] = $productid;

				$action_list = array(
					'type'		=> $action,
					'url_title'	=> "",
					'url'		=> "",
					'imgurl'	=> "",
					'page'		=> $page,
					'productid'	=> $productid
				);
				$action_list = json_encode($action_list);

				$msg = array(
					'title'		=> $title,
					'body' 		=> $messege,
					'vibrate'	=> 1,
					'sound'		=> 1
				);

				$data = array(
					'action'	=> $action,
					'productid'	=> $productid,
					'url_title'	=> "",
					'url'		=> "",
					'action_list'=> "{$action_list}"
				);

				$this->fcm_broadcast_send($token, $msg, $data);
			}
			$new_pushed_product_id = array_unique($new_pushed_product_id);
			$new_pushed_product_id = implode(",", $new_pushed_product_id);
			if (empty($pushed_product_id)) {
				$redis->set($today.'_near_close_pushed',$new_pushed_product_id);
			}else{
				$redis->set($today.'_near_close_pushed',$pushed_product_id.",".$new_pushed_product_id);
			}
		}
	}

	/*
	* after_saja 下完標推播母程式
	*/
	public function after_saja(){
		global $db, $config, $push;

		$this->winner_killed();
		$this->become_winner();
	}

	/*
	* winner_killed 目前得標被蓋過
	*/
	public function winner_killed(){
		global $db, $config, $push;

		$ori_userid = $_POST['ori_userid'];
		$new_userid = $_POST['new_userid'];
		$productid = $_POST['productid'];

		if (!empty($ori_userid)&&($new_userid!=$ori_userid)) {
			$push_msg_list = $push->get_auto_push_msg("2");
			$token = $push->get_token($ori_userid);
			$product_name = $push->get_productname($productid);
			$product_name = '《'.$product_name.'》';
			$title = $push_msg_list[0]['title'];
			$body = $push_msg_list[0]['body'];
			$action = $push_msg_list[0]['action'];
			$page = $push_msg_list[0]['page'];

			$messege = str_replace("{productname}", $product_name, $body);

			$action_list = array(
					'type'		=> $action,
					'url_title'	=> "",
					'url'		=> "",
					'imgurl'	=> "",
					'page'		=> $page,
					'productid'	=> $productid
				);
			$action_list = json_encode($action_list);

			$msg = array(
				'title'		=> $title,
				'body' 		=> $messege,
				'vibrate'	=> 1,
				'sound'		=> 1
			);

			// 新舊規格一起傳
			if ($action == "4") {
				switch ($page) {
					case '1':
						$action = "saja";
						break;
					case '4':
						$action = "index";
						break;
					case '5':
						$action = "saja_history";
						break;
				}
			}

			$data = array(
				'action'	=> $action,
				'productid'	=> $productid,
				'url_title'	=> "",
				'url'		=> "",
				'action_list'=> "{$action_list}"
			);

			$this->fcm_broadcast_send($token, $msg, $data);

			echo json_encode(array("推播完成"),JSON_UNESCAPED_UNICODE ) ;
		}else{
			echo json_encode(array("沒有原得標者"),JSON_UNESCAPED_UNICODE ) ;
		}
	}

    /*
	* get_broadcast_history 取得目標被推播記錄
	*/
	public function broadcast_history(){
		global $db, $config, $push;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態

		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$page = empty($_POST['page']) ? htmlspecialchars($_GET['page']) : htmlspecialchars($_POST['page']);
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//判斷是否有登入會員
		login_required();
		$result=array();
		if( empty($userid)){
			$result['retCode']=-1;
			$result['retMsg']='查無相關記錄';
		} else {
			$rowdata = $push->get_broadcast_history($userid,$page);
			$result['retCode']=1;
			if (empty($rowdata['table']['record'])){
				$result['retMsg']='查無相關記錄';
			} else{
				$result['retMsg']='OK';
				$result['retObj']['data']=$rowdata['table']['record'];
				$result['retObj']['page']=$rowdata['table']['page'];
			}
		}
		echo json_encode($result);
	}
    /*
	* get_broadcast_history 取得目標被推播記錄
	*/
	public function broadcast_history_detail(){
		global $db, $config, $push;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$pushid = empty($_SESSION['pushid'])?$_REQUEST['pushid']:$_SESSION['pushid'];
		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
        $push_type = empty($_SESSION['push_type'])?$_REQUEST['push_type']:$_SESSION['push_type'];
		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		//判斷是否有登入會員
		login_required();
		$result=array();
		if( empty($pushid)){
			$result['retCode']=-1;
			$result['retMsg']='查無相關記錄201';
		} else {
			$rowdata = $push->get_broadcast_history_detail($pushid,$userid,$push_type);
			$result['retCode']=1;
			if (empty($rowdata['table']['record'])){
				$result['retMsg']='查無相關記錄';
			} else{
				$action_list=array();
				$action_list['type']=$rowdata['table']['record'][0]['action'];
				$action_list['productid']=$rowdata['table']['record'][0]['productid'];
				$action_list['url_title']=$rowdata['table']['record'][0]['url_title'];
				$action_list['url']=$rowdata['table']['record'][0]['url'];
				$action_list['page']=$rowdata['table']['record'][0]['page'];
				$action_list['msg']=$rowdata['table']['record'][0]['body'];
				$result['retMsg']='OK';
				$result['retObj']['data']=$rowdata['table']['record'][0];
				$result['retObj']['action_list']=$action_list;
			}
		}
		echo json_encode($result);
	}
    /*
	* update_broadcast_history_status 更新推播記錄狀態
	*/
	public function broadcast_history_status(){
		global $db, $config, $push;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$result=array();
        $acp_method=array('POST','DELETE');
        //login_required();
        if (in_array($_SERVER['REQUEST_METHOD'], $acp_method)){
	        $pushid = empty($_SESSION['pushid'])?$_REQUEST['pushid']:$_SESSION['pushid'];
			$userid = empty($_SESSION['auth_id'])?$_REQUEST['auth_id']:$_SESSION['auth_id'];
	        $push_type = empty($_SESSION['push_type'])?$_REQUEST['push_type']:$_SESSION['push_type'];
	        $json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
			if( empty($pushid)){
				$result['retCode']=-1;
				$result['retMsg']='更新失敗';
			} else {
				$rowdata = $push->update_broadcast_history_status($pushid,$userid,$push_type,$_SERVER['REQUEST_METHOD']);
				$result['retCode']=1;
				if ($rowdata){
					$result['retMsg']='OK';
				} else{
					$result['retCode']=-3;
					$result['retMsg']='更新失敗';
				}
			}
        }else{
			$result['retCode']=-2;
			$result['retMsg']='只接受DELETE跟POST';
        }
		echo json_encode($result);
		die();
	}

	/*
	* become_winner 成為目前得標者
	*/
	public function become_winner(){
		global $db, $config, $push;

		$productid = $_POST['productid'];
		$ori_userid = $_POST['ori_userid'];
		$new_userid = $_POST['new_userid'];

		if (!empty($new_userid)&&($new_userid!=$ori_userid)) {

			$push_msg_list = $push->get_auto_push_msg("3");
			$token = $push->get_token($new_userid);
			$product_name = $push->get_productname($productid);
			$product_name = '《'.$product_name.'》';
			$nickname = $push->get_nickname($new_userid);
			$title = $push_msg_list[0]['title'];
			$body = $push_msg_list[0]['body'];
			$action = $push_msg_list[0]['action'];
			$page = $push_msg_list[0]['page'];

			$messege = str_replace("{productname}", $product_name, $body);
			$messege = str_replace("{nickname}", $nickname, $messege);

			$action_list = array(
					'type'		=> $action,
					'url_title'	=> "",
					'url'		=> "",
					'imgurl'	=> "",
					'page'		=> $page,
					'productid'	=> $productid
				);
			$action_list = json_encode($action_list);

			$msg = array(
				'title'		=> $title,
				'body' 		=> $messege,
				'vibrate'	=> 1,
				'sound'		=> 1
			);

			// 新舊規格一起傳
			if ($action == "4") {
				switch ($page) {
					case '1':
						$action = "saja";
						break;
					case '4':
						$action = "index";
						break;
					case '5':
						$action = "saja_history";
						break;
				}
			}

			$data = array(
				'action'	=> $action,
				'productid'	=> $productid,
				'url_title'	=> "",
				'url'		=> "",
				'action_list'=> "{$action_list}"
			);

			$this->fcm_broadcast_send($token, $msg, $data);

			echo json_encode(array("推播完成"),JSON_UNESCAPED_UNICODE ) ;
		}else{
			echo json_encode(array("沒有得標者"),JSON_UNESCAPED_UNICODE ) ;
		}

	}

	/*
	* bid_winner 得標通知
	*/
	public function bid_winner(){
		global $db, $config, $push;

		$productid = $_POST['productid'];
		if (empty($productid)) {
			echo json_encode(array("沒有productid"),JSON_UNESCAPED_UNICODE ) ;
			exit();
		}

		$push_msg_list = $push->get_auto_push_msg("4");
		$title = $push_msg_list[0]['title'];
		$body = $push_msg_list[0]['body'];
		$action = $push_msg_list[0]['action'];
		$page = $push_msg_list[0]['page'];

		$winner = $push->get_winner($productid);
		if (!empty($winner)) {
			$token = array();
			$nickname = $winner[0]['nickname'];
			$product_name = $winner[0]['product_name'];
			$product_name = '《'.$product_name.'》';
			$messege = str_replace("{productname}", $product_name, $body);
			$messege = str_replace("{nickname}", $nickname, $messege);

			$action_list = array(
					'type'		=> $action,
					'url_title'	=> "",
					'url'		=> "",
					'imgurl'	=> "",
					'page'		=> $page,
					'productid'	=> $productid
				);
			$action_list = json_encode($action_list);

			$msg = array(
				'title'		=> $title,
				'body' 		=> $messege,
				'vibrate'	=> 1,
				'sound'		=> 1
			);

			// 新舊規格一起傳
			if ($action == "4") {
				switch ($page) {
					case '1':
						$action = "saja";
						break;
					case '4':
						$action = "index";
						break;
					case '5':
						$action = "saja_history";
						break;
				}
			}

			$data = array(
				'action'	=> $action,
				'productid'	=> $productid,
				'url_title'	=> "",
				'url'		=> "",
				'action_list'=> "{$action_list}"
			);

			foreach ($winner as $key => $value) {
				$token[] = $value['token'];
			}
			$token = array_chunk($token, 1000);
			$push->record_push_msg($productid,$title,$messege,$winner[0]['userid']);
			$this->fcm_broadcast_send($token, $msg, $data);

			echo json_encode(array("推播完成"),JSON_UNESCAPED_UNICODE ) ;
			exit();
		}else{
			echo json_encode(array("沒有得標者"),JSON_UNESCAPED_UNICODE ) ;
			exit();
		}
	}

	/*
	* push_by_hand 手動排程推播
	*/
	public function push_by_hand(){
		global $db, $config, $push;
		//設定 Action 相關參數
		set_status($this->controller);

		$push_msg_list = $push->get_hand_push_msg();
		$new_pushed_id = array();

		if (!empty($push_msg_list)) {
			foreach ($push_msg_list as $key => $value) {
				$pushid 		= $value['pushid'];
				$action 		= $value['action'];
				$title 			= $value['title'];
				$body 			= $value['body'];
				$product_name 	= '《'.$value['product_name'].'》';
				$productid 		= $value['productid'];
				$url_title 		= $value['url_title'];
				$url 			= $value['url'];
				$page 			= $value['page'];
				$userid 		= $value['sendto'];
				$messege 		= str_replace("{productname}", $product_name, $body);
				$ids 			= array();

				$new_pushed_id[] = $pushid;

				$action_list = array(
						'type'		=> $action,
						'url_title'	=> $url_title,
						'url'		=> $url,
						'imgurl'	=> '',
						'page'		=> $page,
						'productid'	=> $productid
					);
				$action_list = json_encode($action_list);

				$msg = array(
					'title'		=> $title,
					'body' 		=> $messege,
					'vibrate'	=> 1,
					'sound'		=> 1
				);

				// 新舊規格一起傳
				if ($action == "4") {
					switch ($page) {
						case '1':
							$action = "saja";
							break;
						case '4':
							$action = "index";
							break;
						case '5':
							$action = "saja_history";
							break;
					}
				}

				$data = array(
					'action'	=> $action,
					'productid'	=> $productid,
					'url_title'	=> $url_title,
					'url'		=> $url,
					'action_list'=> "{$action_list}"
				);

				$token = $push->get_token($userid);
				if (!$token) {
					echo json_encode(array("推播失敗:查無token"),JSON_UNESCAPED_UNICODE ) ;
					exit();
				}else{
					$ids = $token;
				}
				$this->fcm_broadcast_send($ids, $msg, $data);
			}
			
			

			if ($push->used_hand_push_msg($new_pushed_id)){
				$new_pushed_id = implode($new_pushed_id, ',');
				echo json_encode(array("推播完成 id:{$new_pushed_id}"),JSON_UNESCAPED_UNICODE ) ;
				exit();
			}else{
				echo json_encode(array("推播失敗"),JSON_UNESCAPED_UNICODE ) ;
				exit();
			}

		}else{
			echo json_encode(array("推播失敗"),JSON_UNESCAPED_UNICODE ) ;
			exit();
		}
	}

    /*
	* goods_arrival 推播
	*/
	public function goods_arrival(){
		global $db, $config, $push;
		//設定 Action 相關參數
		set_status($this->controller);

		$push_msg_list = $_POST;
		$new_pushed_id = array();
		if (!empty($push_msg_list)) {
			$action 		= $_POST['action'];
			$title 			= $_POST['title'];
			$body 			= $_POST['body'];
			$productid 		= $_POST['productid'];
			$url_title 		= $_POST['url_title'];
			$url 			= $_POST['url'];
			$page 			= $_POST['page'];
			$userid 		= $_POST['sendto'];
			$messege 		=  $body;
			$ids 			= array();

			$new_pushed_id[] = $pushid;

			$action_list = array(
					'type'		=> $action,
					'url_title'	=> $url_title,
					'url'		=> $url,
					'imgurl'	=> '',
					'page'		=> $page,
					'productid'	=> $productid
				);
			$action_list = json_encode($action_list);

			$msg = array(
				'title'		=> $title,
				'body' 		=> $messege,
				'vibrate'	=> 1,
				'sound'		=> 1
			);

			$data = array(
				'action'	=> $action,
				'productid'	=> $productid,
				'url_title'	=> $url_title,
				'url'		=> $url,
				'action_list'=> "{$action_list}"
			);
			$token = $push->get_token($userid);
			if (!$token) {
				echo "推播失敗:查無token";
				//echo json_encode(array("推播失敗:查無token"),JSON_UNESCAPED_UNICODE ) ;
				exit();
			}else{
				$ids = $token;
			}
			$this->fcm_broadcast_send($ids, $msg, $data);
			$push->record_push_msg($productid,$title,$messege,$userid);
		}else{
			echo "推播失敗";
			//echo json_encode(array("推播失敗"),JSON_UNESCAPED_UNICODE ) ;
			exit();
		}
	}
	
	/*
	* 20201225 push_scode 排程推播送券
	*/
	public function push_scode(){
		global $db, $config, $push, $scodeModel, $user;
		//設定 Action 相關參數
		set_status($this->controller);
		set_time_limit(0);

		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$push_msg_list = '';
		$new_pushed_id = array();
		$chk = true;
		//error_log("[push_scode]userid: ". $userid );
		
		//if($userid=='38802'){
			$push_msg_list = $push->get_push_scode_msg();
			error_log("[push_scode]: ". json_encode($push_msg_list) );
		//}

		if (!empty($push_msg_list)) {

			foreach ($push_msg_list as $key => $value) {
				$pushid 		= $value['pushid'];
				$ontime 		= strtotime($value['ontime']); //開始日
				$pmt_sendtime	= $value['sendtime'];
				$time_serial 	= date('Y-m-d '. $pmt_sendtime);
				$push_serial 	= $value['push_serial'] .'_'. strtotime($time_serial); //推播序號
				$ttype 			= $value['ttype']; //券種: S超殺券, O限殺券
				$qty 			= $value['qty']; //贈送組數
				
				//用戶分群數
				$usergroup 		= empty($value['usergroup']) ? 0 : intval($value['usergroup']);

				//今日第x群
				$tday = time();
				$user_str = '';

				if($tday < $ontime || $usergroup==0){
					$chk = false;
				} else {
					$user_divisor_day = floor( ($tday - $ontime)/86400 );
					$user_remainder = $user_divisor_day % $usergroup;

					//分組取會員ID, 每次取1萬筆
					$user_str = $push->getUseridByMod($user_remainder, $usergroup, $push_serial);
					
					if(empty($user_str)){
						//更新發送時間 
						$push->used_push_scode($value['push_serial'], $time_serial);
					}
				}

				$title 			= $value['title'];
				$body 			= $value['body'];
				$action 		= $value['action'];
				$page 			= $value['page'];
				$url_title 		= $value['url_title'];
				$url 			= $value['url'];
				$productid 		= $value['productid'];
				$product_name 	= '《'.$value['product_name'].'》';
				$sendto_user 	= empty($value['sendto']) ? '' : $value['sendto'];

				//本次分組會員ID
				$userid_str = empty($user_str) ? $sendto_user : $user_str .",". $sendto_user;
				//error_log("[userid_str]: ". json_encode($userid_str) );
				
				$ids_arr = array();
				if(!empty($userid_str) ){
					//發送超級殺價券
					$scodeModel->add_scode('scode', $qty, $push_serial, '', $userid_str);
					
					$user_arr = explode(",", $userid_str); 
					$ids_arr = array_chunk($user_arr, 500);
					$chk = true;
					
				}else{
					$chk = false;
					echo json_encode(array("推播失敗:查無會員ID"),JSON_UNESCAPED_UNICODE ) ;
					exit();
				}
				
				$ids = '';
				error_log("[push_scode]chk: {$chk}, ids: ". $ids);
				//echo 'ids_arr:'; var_dump($ids_arr); echo '<br>';

				if($chk){
					//推播
					$messege_body = str_replace("{productname}", $product_name, $body);
					
					$inp=array();
					$inp['title'] = $title .'_'. date('m-d');
					$inp['body'] = $messege_body;
					$inp['productid'] = $productid;
					$inp['action'] = $action;
					$inp['url_title'] = $url_title;
					$inp['url'] = $url;
					$inp['sender'] = '1';
					$inp['page'] = $page;
					$inp['target'] = 'saja';
					
					foreach ($ids_arr as $rk => $rv) {
						$inp['sendto'] = implode(",", $rv);
						//echo "sendto {$rk}:"; var_dump($inp['sendto']); echo '<br><br>';
						error_log("[push_scode]sendto: ". $rk);
						
						//發推播
						$rtn=sendFCM($inp);
						
						//殺價券生效
						$scodeModel->update_scode_promote($push_serial, $inp['sendto']);
					}
					
				}
				
			}
			
			echo json_encode(array("推播完成 id:{$push_serial}"),JSON_UNESCAPED_UNICODE ) ;

		}else{
			echo json_encode(array("無推播資料"),JSON_UNESCAPED_UNICODE ) ;
		}
		exit();
	}
	
	/*
	* 20201225 push_scode 排程推播送券
	*/
	public function push_scode2(){
		global $db, $config, $push, $scodeModel, $user;
		//設定 Action 相關參數
		set_status($this->controller);

		$userid = empty($_SESSION['auth_id'])?$_POST['auth_id']:$_SESSION['auth_id'];
		$push_msg_list = '';
		$new_pushed_id = array();
		$chk = true;
		error_log("[push_scode]userid: ". $userid );
		
		if($userid=='38802' || $userid=='1705'){
			$push_msg_list = $push->get_push_scode_msg();
			error_log("[push_scode2]: ". json_encode($push_msg_list) );
		}

		if (!empty($push_msg_list)) {
			foreach ($push_msg_list as $key => $value) {
				$pushid 		= $value['pushid'];
				$ontime 		= strtotime($value['ontime']); //開始日
				
				$push_serial 	= $value['push_serial'] .'_'. date('Y-m-d'); //推播序號
				$ttype 			= $value['ttype']; //券種: S超殺券, O限殺券
				$qty 			= $value['qty']; //贈送組數
				//用戶分群數
				$usergroup 		= empty($value['usergroup']) ? 0 : intval($value['usergroup']);
				
				//今日第x群
				$tday = time();
				$user_str = '';
		
				if($tday < $ontime || $usergroup==0){
					$chk = false;
				} else {
					$user_divisor_day = floor( ($tday - $ontime)/86400 );
					$user_remainder = $user_divisor_day % $usergroup;
					//echo 'user_remainder:'; var_dump($user_remainder); echo '<br>';
					
					//分組取會員ID
					$user_str = $user->getModUserid($user_remainder, $usergroup);
				}
				
				$title 			= $value['title'];
				$body 			= $value['body'];
				$action 		= $value['action'];
				$page 			= $value['page'];
				$url_title 		= $value['url_title'];
				$url 			= $value['url'];
				$productid 		= $value['productid'];
				$product_name 	= '《'.$value['product_name'].'》';
				$sendto_user 	= empty($value['sendto']) ? '' : $value['sendto'];
				$pmt_sendtime	= $value['sendtime'];
				
				//本次分組會員ID
				$userid_str = empty($user_str) ? $sendto_user : $user_str .",". $sendto_user;
				//error_log("[userid_str]: ". json_encode($userid_str) );
				$ids = array();
				
				if(!empty($userid_str) ){
					//發送超級殺價券
					// $scodeModel->add_scode('scode', $qty, $push_serial, '', $userid_str);
					$token = $push->get_token($userid_str);
					if (!$token) {
						$chk = false;
						echo json_encode(array("推播失敗:查無token"),JSON_UNESCAPED_UNICODE ) ;
						exit();
					}else{
						$ids = $token;
						$chk = true;
					}
				}else{
					$chk = false;
					echo json_encode(array("推播失敗:查無token"),JSON_UNESCAPED_UNICODE ) ;
					exit();
				}
				
				$messege = str_replace("{productname}", $product_name, $body);

				$action_list = array(
						'type'		=> $action,
						'url_title'	=> $url_title,
						'url'		=> $url,
						'imgurl'	=> '',
						'page'		=> $page,
						'productid'	=> $productid
					);
				$action_list = json_encode($action_list);

				$msg = array(
					'title'		=> $title,
					'body' 		=> $messege,
					'vibrate'	=> 1,
					'sound'		=> 1
				);

				$data = array(
					'action'	=> $action,
					'productid'	=> $productid,
					'url_title'	=> $url_title,
					'url'		=> $url,
					'action_list'=> "{$action_list}"
				);
				
				error_log("[push_scode]chk: {$chk} ,msg: ". json_encode($msg) );

				if($chk){
					//推播
					// $this->fcm_broadcast_send255($ids, $msg, $data);
					
					$new_pushed_id[] = $push_serial;
					$nexttime = date('Y-m-d') .' '. $pmt_sendtime;
					$push->used_push_scode2($value['push_serial']);
					echo json_encode(array("推播完成 id:{$push_serial}"),JSON_UNESCAPED_UNICODE ) ;
				}
			}

		}else{
			echo json_encode(array("無推播資料"),JSON_UNESCAPED_UNICODE ) ;
		}
		exit();
	}
	
	/* firebase 推播傳送對外api
	   ids: 逗號分隔的tokens, ( id1,id2,id3 )
	   idtype : 預設是userid, 如果要直接傳入手機token, 則 idtype=tk
	*/
	public function send($ids='', $title='', $msg='', $idtype='') {
		   
		   global $db, $config, $push, $user;
		   //設定 Action 相關參數
		   set_status($this->controller);
		   
		   if(empty($ids)) {
			  $ids=htmlspecialchars($_REQUEST['ids']); 
		   }
		   if(empty($msg)) {
			  $msg=htmlspecialchars($_REQUEST['msg']); 
		   }
		   if(empty($title)) {
			  $title=htmlspecialchars($_REQUEST['title']); 
		   }
		   if(empty($idtype)) {
			  $idtype=htmlspecialchars($_REQUEST['idtype']); 
		   }
		   // 預設是userid
		   if(empty($idtype)) {
			  $idtype="user"; 
		   }
		   
		    $action="4";
			$productid="";
			$url_title="";
			$url="";
			$page="";
			
			// 新舊規格一起傳
			if ($action == "4") {
				switch ($page) {
					case '1':
						$action = "saja";
						break;
					case '4':
						$action = "index";
						$page='4';
						break;
					case '5':
						$action = "saja_history";
						break;
				}
			}
			
		   
		    $action_list = array(
					'type'		=> $action,
					'url_title'	=> $url_title,
					'url'		=> $url,
					'imgurl'	=> '',
					'page'		=> $page,
					'productid'	=> $productid
				);
			$action_list = json_encode($action_list);

		    $content = array(
				'title'		=> $title,
				'body' 		=> $msg,
				'vibrate'	=> "",
				'sound'		=> ""
		    );	

			$data = array(
				'action'	=> $action,
				'productid'	=> $productid,
				'url_title'	=> $url_title,
				'url'		=> $url,
				'action_list'=> "{$action_list}",
			);
		   
		    if(empty($ids)|| empty($msg) || empty($title)){
				echo json_encode(array("retCode"=>0,"retMsg"=>"NOT ENOUGH DATA TO NOTIFY"));
				return ;
		    }
		    // $arr_ids=explode(",",$ids);
		    // error_log("[c/push/send] : ".json_encode($arr_ids));
		    // 預設是userid
		    $token = "";
		    if($idtype=="user") {
			  $token = $push->get_token($ids);    
		    } else {
              $token = $ids; 
            }			   
		    $this->fcm_broadcast_send($token, $content, $data);
	}

	/* firebase 推播傳送模組
			$title : 標題
			$body : 內文
			$ids: 接收者array of registrationIds, ['id1','id2','id3'...]
			$vibrate : 振動(0:無/1:有)
			$sound : 題示音(0:無/1:有)
	*/
	private function fcm_broadcast_send($ids='', $msg=array(), $data=array()) {

			global $db, $config;

			//設定 Action 相關參數
			set_status($this->controller);

			//參數未傳送返回false
			if(empty($ids)||empty($msg)||empty($data)){
				return FALSE;
			}

			// API access key from Google API's Console
			define( 'API_ACCESS_KEY', 'AAAAbCNeUVQ:APA91bGcY4z5iUZyWFxirakTqx6h5XR1-f9K-XMNuiUsUuqx9S2Js4Sk4KSywwlaUD4YjcAqIlLbkQQ289G_vI996dZDOU32Bu9XPGBxI7vWrHtOqTUg_rqP1iN0Nu4RxF2AswUfg93U' );

			$headers = array(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);

            // 切成1000筆的array chunk $ids_chunk = array_chunk($ids,1000,false);
			            
			// 每次丟1000個id的array 去發推播 
			foreach ($ids as $key => $value) {
				//接收者token $ids
				error_log("[c/fcm_broadcast_send] count(ids) of ${key} : ".count($value));
				$fields = array(
					'registration_ids' 	=> $value,
					'notification'		=> $msg,
					'data'				=> $data
				);
				

				if($data['act']=='no'){ }else{
					$ch = curl_init();
					curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
					curl_setopt( $ch,CURLOPT_POST, true );
					curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
					curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
					curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
					curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
					$result = curl_exec( $ch );
					error_log("[c/fcm_broadcast_send] result of ${key} : ".json_encode($result));
					curl_close( $ch );
				}
			}
	}

	/*
	* clear_notify_status 全部標示成己讀
	* @done int 1:read/0:unread
	*/
	public function clear_notify_status(){
		global $db, $config, $push;

		//設定 Action 相關參數
		set_status($this->controller);

		//成交狀態
		$result=array();
        $acp_method=array('POST');
        login_required();
        if (in_array($_SERVER['REQUEST_METHOD'], $acp_method)){
			$userid = empty($_SESSION['auth_id'])?$_REQUEST['auth_id']:$_SESSION['auth_id'];
			$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
			$done = isset($_POST['done']) ? htmlspecialchars($_POST['done']):1;
			$rowdata = $push->clrNotify($userid,$done);
			$result['retCode']=1;
			if ($rowdata){
				$result['retMsg']='OK';
			} else{
				$result['retCode']=-3;
				$result['retMsg']='更新失敗';
			}
        }else{
			$result['retCode']=-2;
			$result['retMsg']='只接受POST';
        }
		echo json_encode($result);
		die();
	}
}

?>
