<?php
/*
 * Bbc Controller 殺殿(B2B2C) API 規劃如下:

N01 殺殿申請 https://docs.sharebonus.info/web/#/6?page_id=701
/site/bbc/apply

N02 殺殿專區首頁 https://docs.sharebonus.info/web/#/6?page_id=702
/site/bbc/home

N03 殺殿商品清單 https://docs.sharebonus.info/web/#/6?page_id=703
/site/bbc/product

N04 新增殺品 https://docs.sharebonus.info/web/#/6?page_id=704
/site/bbc/product_add

N05 檢視殺品 https://docs.sharebonus.info/web/#/6?page_id=705
/site/bbc/product_view
*/
 
include_once(LIB_DIR ."/convertString.ini.php");

class Bbc {

	public $controller = array();
	public $params = array();
	public $id;
	public $userid = '';
	public $ret='';
	public $json = '';

	public function __construct() {
		global $config;
		
		$this->str = new convertString();
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		$this->userid = (empty($_SESSION['auth_id']) ) ? $_REQUEST['auth_id'] : $_SESSION['auth_id'];
		$this->json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);

		$this->ret = array(
			'retCode'=>1,
			'retMsg'=>"OK",
			'retObj'=>array()
		);

		//殺殿申請物件
		include_once(LIB_DIR ."/bbc_apply.php"); //載入lib
		$this->aO = new bbc_apply_lib();
		
		//殺殿首頁物件
		include_once(LIB_DIR ."/bbc_home.php"); //載入lib
		$this->hO = new bbc_home_lib();
		
		//殺品處理物件
		include_once(LIB_DIR ."/bbc_product.php"); //載入lib
		$this->pdO = new bbc_product_lib();
	}

	/*
	 * N01 殺殿申請
	 */
	public function apply() {
		global $db, $config, $tpl, $member, $user;

		login_required();
		//資安查驗 $chk_auth = chk_auth();

		//參數陣列
		$var['json'] = $this->json;
		$var['userid'] = $this->userid;
		
        error_log("[bbc/apply] post : ".json_encode($_POST));
		error_log("[bbc/apply] get : ".json_encode($_GET));
		$ret = $this->ret;

		//帶入該項分類ID $ccid = empty($_REQUEST['ccid']) ? '' : htmlspecialchars($_REQUEST['ccid']);
		$chk_auth['retCode']=1;
		
		//帶入參數
		$this->aO->_v = $var;
		
		//分類資訊
		//$cate_info = $this->pdO->getCateInfo($var['pcid']);
		//$var['cate_info'] = $cate_info['CateInfo'];
		//$field = "`productid`, `name`, `description` title, `rule`, `ptid_url`,`media_url`";
		$ProdList = array(); //$this->pdO->getProdList($field, $ccid);
		$row_list = array_merge($chk_auth, $ProdList);
		
		if($json=='Y'){
			$ret['retObj']['data'] = $row_list['record'];
			$ret['retObj']['page'] = $row_list['page'];
			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('row_list', $row_list);
			$tpl->assign('ret', $ret);
			$tpl->set_title('殺殿申請');
			$tpl->render("bbc","apply", true);
		}

	}
	
	/*
	 * N02 殺殿專區首頁
	 */
	public function home() {
		global $db, $config, $tpl, $member, $user;

		login_required();
		//資安查驗 $chk_auth = chk_auth();

		//參數陣列
		$var['json'] = $this->json;
		$var['userid'] = $this->userid;
		
        error_log("[bbc/home] post : ".json_encode($_POST));
		error_log("[bbc/home] get : ".json_encode($_GET));
		$ret = $this->ret;

		//帶入該項分類ID $ccid = empty($_REQUEST['ccid']) ? '' : htmlspecialchars($_REQUEST['ccid']);
		$chk_auth['retCode']=1;
		
		//帶入參數
		$this->hO->_v = $var;
		
		//分類資訊
		//$cate_info = $this->pdO->getCateInfo($var['pcid']);
		//$var['cate_info'] = $cate_info['CateInfo'];
		//$field = "`productid`, `name`, `description` title, `rule`, `ptid_url`,`media_url`";
		$ProdList = array(); //$this->pdO->getProdList($field, $ccid);
		$row_list = array_merge($chk_auth, $ProdList);
		
		if($json=='Y'){
			$ret['retObj']['data'] = $row_list['record'];
			$ret['retObj']['page'] = $row_list['page'];
			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('row_list', $row_list);
			$tpl->assign('ret', $ret);
			$tpl->set_title('殺殿專區首頁');
			$tpl->render("bbc","home", true);
		}

	}
	
	/*
	 * N03 殺殿商品清單
	 */
	public function product() {
		global $db, $config, $tpl, $member, $user;

		login_required();
		//資安查驗 $chk_auth = chk_auth();

		//參數陣列
		$var['json'] = $this->json;
		$var['userid'] = $this->userid;
		
        error_log("[bbc/product] post : ".json_encode($_POST));
		error_log("[bbc/product] get : ".json_encode($_GET));
		$ret = $this->ret;

		//帶入該項分類ID $ccid = empty($_REQUEST['ccid']) ? '' : htmlspecialchars($_REQUEST['ccid']);
		$chk_auth['retCode']=1;
		
		//帶入參數
		$this->pdO->_v = $var;
		
		//分類資訊
		//$cate_info = $this->pdO->getCateInfo($var['pcid']);
		//$var['cate_info'] = $cate_info['CateInfo'];
		//$field = "`productid`, `name`, `description` title, `rule`, `ptid_url`,`media_url`";
		$ProdList = array(); //$this->pdO->getProdList($field, $ccid);
		$row_list = array_merge($chk_auth, $ProdList);
		
		if($json=='Y'){
			$ret['retObj']['data'] = $row_list['record'];
			$ret['retObj']['page'] = $row_list['page'];
			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('row_list', $row_list);
			$tpl->assign('ret', $ret);
			$tpl->set_title('殺殿商品清單');
			$tpl->render("bbc","product", true);
		}

	}
	
	/*
	 * N04 新增殺品
	 */
	public function product_add() {
		global $db, $config, $tpl, $member, $user;

		login_required();
		//資安查驗 $chk_auth = chk_auth();

		//參數陣列
		$var['json'] = $this->json;
		$var['userid'] = $this->userid;
		
        error_log("[bbc/product_add] post : ".json_encode($_POST));
		error_log("[bbc/product_add] get : ".json_encode($_GET));
		$ret = $this->ret;

		//帶入該項分類ID $ccid = empty($_REQUEST['ccid']) ? '' : htmlspecialchars($_REQUEST['ccid']);
		$chk_auth['retCode']=1;
		
		//帶入參數
		$this->pdO->_v = $var;
		
		//分類資訊
		//$cate_info = $this->pdO->getCateInfo($var['pcid']);
		//$var['cate_info'] = $cate_info['CateInfo'];
		//$field = "`productid`, `name`, `description` title, `rule`, `ptid_url`,`media_url`";
		$ProdList = array(); //$this->pdO->getProdList($field, $ccid);
		$row_list = array_merge($chk_auth, $ProdList);
		
		if($json=='Y'){
			$ret['retObj']['data'] = $row_list['record'];
			$ret['retObj']['page'] = $row_list['page'];
			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('row_list', $row_list);
			$tpl->assign('ret', $ret);
			$tpl->set_title('新增殺品');
			$tpl->render("bbc","product_add", true);
		}

	}
	
	/*
	 * N05 檢視殺品
	 */
	public function product_view() {
		global $db, $config, $tpl, $member, $user;

		login_required();
		//資安查驗 $chk_auth = chk_auth();

		//參數陣列
		$var['json'] = $this->json;
		$var['userid'] = $this->userid;
		
        error_log("[bbc/product_view] post : ".json_encode($_POST));
		error_log("[bbc/product_view] get : ".json_encode($_GET));
		$ret = $this->ret;

		//帶入該項分類ID $ccid = empty($_REQUEST['ccid']) ? '' : htmlspecialchars($_REQUEST['ccid']);
		$chk_auth['retCode']=1;
		
		//帶入參數
		$this->pdO->_v = $var;
		
		//分類資訊
		//$cate_info = $this->pdO->getCateInfo($var['pcid']);
		//$var['cate_info'] = $cate_info['CateInfo'];
		//$field = "`productid`, `name`, `description` title, `rule`, `ptid_url`,`media_url`";
		$ProdList = array(); //$this->pdO->getProdList($field, $ccid);
		$row_list = array_merge($chk_auth, $ProdList);
		
		if($json=='Y'){
			$ret['retObj']['data'] = $row_list['record'];
			$ret['retObj']['page'] = $row_list['page'];
			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('row_list', $row_list);
			$tpl->assign('ret', $ret);
			$tpl->set_title('檢視殺品');
			$tpl->render("bbc","product_view", true);
		}

	}
	
	
	
	
	
	
	
	
	
	
	
	

	/*
	 * 推薦好友清單
	 */
	public function _referral(){
		global $db, $tpl, $config, $user;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);


		//處理狀態
		$status = '';
		if($kind=='all'){
			$status = '';
		}
		/*
		elseif($kind=='process'){
			$status = '0';
		}elseif($kind=='close'){
			$status = '1';
		}*/

		//清單
		$get_list = $user->getReferralList($this->userid, '', $p);

		if($json=='Y'){
			echo json_encode($ret);
			exit;
		} else {
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('推薦好友清單');
			$tpl->render("bbc","referral",true);
		}
	}

	/*
	 *	訂單紀錄清單
	 *	$json						varchar							瀏覽工具判斷 (Y:APP來源)
	 *	$kind						varchar							訂單狀態
	 */
	public function _order() {

		global $tpl, $mall;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$kind = empty($_POST['kind']) ? htmlspecialchars($_GET['kind']) : htmlspecialchars($_POST['kind']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		if($kind=='all'){
			$status = '';
		}elseif($kind=='process'){
			$status = '0';
		}elseif($kind=='close'){
			$status = '1';
		}else{
			$status = '';
		}

		//紀錄清單
		$get_list = $mall->order_list($this->userid, '', $status);

		if(!empty($get_list['table']['record'])) {
			foreach($get_list['table']['record'] as $tk => $tv) {
				
				//20210120 AARONFU 
				//訂單狀態:0處理中,1配送中,2退費,3已發送(結標取得殺價券 殺價幣等等),4已到貨,5已退費,6不出貨,7.已自動發送(票券),8.支付處理
				switch($tv['status']) {
				    case "1": 
						$status_str = '配送中'; break;
					case "4": 
						$status_str = '已到貨'; break;
                    case "2": 
					case "5": 
						$status_str = '已退費'; break;
					case "6": 
						$status_str = '不出貨'; break;
                    case "3":
					case "7": 
						$status_str = '已發送'; break;
					case "8": 
						$status_str = '付款中'; break;
					default :
					    $status_str = '處理中'; break;
				}
				$status_color = ($tv['status']=='3' || $tv['status']=='4' || $tv['status']=='7')?'green':'yellow';
				$get_list['table']['record'][$tk]['status_color'] = $status_color;
				$get_list['table']['record'][$tk]['status'] = $status_str;
				
				//支付標籤由 API 提供
				//'rebate_text' = '購物金折抵' => 對應 rebate_pay
				//'cash_text' = '現金支付(或信用卡支付)' => 對應 cash_pay
				//'bonus_text' = '鯊魚點支付'=> 對應 bonus_pay >0
				//付款記錄
				$get_pay = $mall->get_order_log($tv['orderid']);
				$pay_drid = empty($get_pay['drid'])? 0 : $get_pay['drid'];
				$get_list['table']['record'][$tk]['drid'] = $pay_drid;
				$get_list['table']['record'][$tk]['rebate_text'] = '購物金折抵';
				$get_list['table']['record'][$tk]['cash_text'] = ($pay_drid=='31')? '信用卡支付' : '現金支付';
				$get_list['table']['record'][$tk]['bonus_text'] = '鯊魚點支付';
			
				
				// Add By Thomas 2020/01/02 使用貨幣及點數的敘述
				$get_list['table']['record'][$tk]['modifyt'] = $tv['insertt'];
				switch($tv['type']) {
				    case "saja":
					    $get_list['table']['record'][$tk]['curr_type_desc']="使用殺價幣 : ".intval($get_list['table']['record'][$tk]['total_fee'])."枚";
						break;
                    case "exchange":
					case "tryexchange":
                    case "lifepay":
                    case "user_qrcode_tx":
                    default :
					     $get_list['table']['record'][$tk]['curr_type_desc']="使用鯊魚點 : ".intval($get_list['table']['record'][$tk]['total_fee'])."點";
                         break;
				}
			}
		}

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = '';
				}else{
					$ret['retObj']['data']=$get_list['table']['record'];
				}
				$ret['retObj']['page']=$get_list['table']['page'];
			}else{
				$ret['retObj']['data']=array();
				$ret['retObj']['page']=new stdClass();

				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('');
			$tpl->render("member","order",true);
		}

	}

	/*
	 *	商城購買清單
	 *	$json						varchar							瀏覽工具判斷 (Y:APP來源)
	 *	$type						int								兌換商品類別 (1:生活代繳, 2:一般商品)
	 *	$p							int								頁碼
	 */
	public function _exchange_order()
	{
		global $tpl, $mall;
		
		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json']) ? htmlspecialchars($_GET['json']) : htmlspecialchars($_POST['json']);
		$type = empty($_POST['type']) ? htmlspecialchars($_GET['type']) : htmlspecialchars($_POST['type']);
		$p = empty($_POST['p']) ? htmlspecialchars($_GET['p']) : htmlspecialchars($_POST['p']);

		switch ($type) {
			case '1':
				$type = "lifepay";
				break;
			case '2':
				$type = "exchange";
				break;
			default:
				$type = "";
				break;
		}

		//紀錄清單
		$get_list = $mall->exchange_order_list($this->userid, $type);

		if($json=='Y'){
			$ret=getRetJSONArray(1,'OK','LIST');
			if($get_list){
				if((empty($p)) || ($p=='') || ($p < 0) || ($p > $get_list['table']['page']['totalpages'])){
					$ret['retObj']['data'] = '';
				}else{
					$ret['retObj']['data']=$get_list['table']['record'];
				}
				$ret['retObj']['page']=$get_list['table']['page'];
			}else{
				$ret['retObj']['data']=array();
				$ret['retObj']['page']=new stdClass();

				$page['rec_start'] = 0;
				$page['totalcount'] = 0;
				$page['totalpages'] = 1;
				$page['perpage'] = 50;
				$page['page'] = 1;
				$page['item'] = array(array("p" => 1 ));

				$ret['retObj']['page'] = $page;
			}
			echo json_encode($ret);
			exit;
		}else{
			$tpl->assign('row_list', $get_list);
			$tpl->set_title('商品兌換清單');
			$tpl->render("member","exchange_order",true);
		}

	}

	/*
	 *	取得訂單列表
	 *	$json						varchar							瀏覽工具判斷 (Y:APP來源)
	 *	$userid						int								會員編號
	 *	$type						varchar							訂單類型
	 *	$status						varchar							訂單狀態
	 */
	public function _getOrderList()
	{
		global $tpl, $mall;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$json = empty($_POST['json'])?$_POST['json']:$_POST['json'];
		$userid = $_POST['auth_id'];
		$type = $_POST['type'];
		$status = $_POST['status'];

		//紀錄清單
		$get_list = $mall->order_list($userid, $type, $status);
		$tpl->assign('row_list', $get_list);

		if($get_list){
		  $ret=getRetJSONArray(1,'OK','LIST');
		  $ret['retObj']['data']=$get_list['table']['record'];
		  $ret['retObj']['page']=$get_list['table']['page'];
		}else{
		  $ret=getRetJSONArray(0,'No Data Found !!','LIST');
		  $ret['retObj']['data']=array();
		  $ret['retObj']['page']=new stdClass();
		}
		echo json_encode($ret);
		exit;
	}

	public function _order_detail()
	{
		global $tpl, $mall, $member;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		$tpl->set_title('');
		$tpl->render("member","order_detail",true);
	}

	public function _edit()
	{
		global $tpl, $member;

		login_required();

		//設定 Action 相關參數
		set_status($this->controller);

		if($_POST){
			if($member->update($_POST)) $tpl->set_msg($member->msg, $member->ok);
			$tpl->set_msg($member->msg, $member->ok);
		}

		$tpl->assign('user_email',$user->email);
		$tpl->assign('user_name',$user->name);
		$tpl->set_title('');
		$tpl->render("member","edit",true);
	}
	
	/*
	* 設定分頁參數
	*/
	private function set_page($row_list, $page_path)
	{
		$table_page = $row_list['table']['page'];

		//前一頁
		$page_content['prevhref'] = $page_path .'&p='. $table_page['previouspage'];

		//後一頁
		$page_content['nexthref'] = $page_path .'&p='. $table_page['nextpage'];

		//當前頁
		$page_content['thispage'] = (int)$table_page['thispage'];

		//總頁數
		$page_content['lastpage'] = (int)$table_page['lastpage'];

		$page_content['change'] = '';
		foreach($table_page['item'] as $pk => $pv){
			$selected = ($pv['p']==$_GET['p']) ? 'selected' : '';
			$page_content['change'] .= '<option value="'. $pv['p'] .'" '. $selected .' >第 '. $pv['p'] .' 頁</option>';
		}

		return $page_content;
	}
	
}
