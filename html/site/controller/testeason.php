<?php
/*
*   Eason 測試用
*/
class Testeason {

    public $controller = array();
	public $params = array();
    public $id;
 
	public function __construct() {
	
		$this->UserID = (empty($_SESSION['auth_id']) ) ? '' : $_SESSION['auth_id'];	
        $this->datetime = date('Y-m-d H:i:s');

    }	
	
	
	public function register_captcha()	{
		
		global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("site","register_captcha",true);
	}
    
    public function register_phone()	{
		
		global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("site","register_phone",true);
	}
    
    public function home() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("issues","issues_add2",true);
    }
    public function ad() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("ad","home",true);
    }

    public function user_cf() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("mall","userConfirmTx",true);
    }
    
    public function instant_home() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("instantkill","instant_home",true);
        $tpl->render("instantkill","choice_location",true);
    }
    
    public function instantkill_home() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
        $tpl->render("instantkill","choice_location",true);
    }
    public function instant_import() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("instant","instant_import",true);
    }
    public function show_room() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("instant","show_room",true);
    }
    public function instant_winner() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("instant","instant_winner",true);
    }
    
    public function user_instant_bind() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("instant","user_instant_bind",true);
    }
    public function instant_user_result() {
        global $tpl, $testeason;
		
		//設定 Action 相關參數
		set_status($this->controller);
		
		//判斷是否為網頁介面
		$tpl->set_title('');
		$tpl->render("instant","instant_user_result",true);
    }
}
?>