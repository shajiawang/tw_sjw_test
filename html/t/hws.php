<?php

// HiCloud 
// http://hws.hicloud.hinet.net/hws-doc/zh_TW/rest/tutorial/howto-gen-signature.html

// hash_hmac
// https://blog.csdn.net/dengjiexian123/article/details/53313913

function base64UrlEncode($str) {
     $find = array("+", "/","=");
     $replace = array("*", "-","");
     return str_replace($find, $replace, base64_encode($str));
}

$url="https://hws.hicloud.hinet.net/cloud_hws/api/hws/";

$arrHWS=array();
$arrHWS['action']="describeInstances";
$arrHWS['instanceId']="BV55011241002B";
$arrHWS['chtAuthType']="hwspass";
$arrHWS['accessKey']="SE41NTAxMTI0MTE1NTE3OTYzMDk5MTY";
$arrHWS['version']='2018-12-12';
date_default_timezone_set("UTC");
$arrHWS['expires']=date('Y-m-d\TH:i:s\Z', time()+900);

ksort($arrHWS);

echo "<br>2.".http_build_query($arrHWS);

$strHWS = strtolower(http_build_query($arrHWS));

// echo "<br>3.".$strHWS;

$strHWS  = hash_hmac("sha1", $strHWS, "Y2JlMzJjMzMxOTliNDUwOGE3NDkwZjFmOTQ0ZjQ3Zjk");

echo "<br>4.".$strHWS;

$arrHWS['signature'] = base64UrlEncode($strHWS);

$url.="?".http_build_query($arrHWS);
echo "<br>".$url;

// echo file_get_contents($url);


/*
驗證碼(signature)：
signature=D6VJcOhTgolniL30uJNjtKYB2NI
驗證碼產生的方式如下：

以UTF-8格式將指令內容(command string)進行編碼。
將CaaS / CVPC API呼叫中的指令內容(command string)以”&”符號分隔，可得到多組鍵值/參數對應。
action=runInstances
version=2013-03-29
imageId=hi-olajtpss
instanceType=HC1.S.LINUX
monitoringEnabled=false
instanceName=haha
count=1
accessKey=U0U0MU5UQXhNREF3TVRFek5qSTVPRFkxTURneU1UWT0
expires=2013-03-29T17:50:04Z

依據鍵值進行參數排序，重新組合指令內容。(這邊採用自然排序法，即是以字元符號進行排序)，排序結果如下：
accessKey=U0U0MU5UQXhNREF3TVRFek5qSTVPRFkxTURneU1UWT0&action=runInstances
&chtAuthType=hwspass&count=1&expires=2013-03-29T17:50:04Z&imageId=hi-olajtpss&instanceName=haha&instanceType=HC1.S.LINUX&monitoringEnabled=false&version
=2013-03-29

將指令內容中的大寫均轉換為小寫，如下：
accesskey=u0u0mu5uqxhnref3tvrfek5qstvprfkxturneu1uwt0&action=runinstances&count=1
&chtauthtype=hwspass&expires=2013-03-29T17:50:04Z&imageid=hi-olajtpss&instancename=haha&instancetype=hc1.s.linux&monitoringenabled=false&version=2013-03-29

搭配Secret Key並使用HMAC SHA-1加密演算法將指令內容進行加密，進一步透過Base64編碼方式將加密結果進行編碼，
而編碼結果中可能包含URL之三個特殊字元("+", "/", "=")，取代此三個特殊字元後即為驗證碼(signature)，

取代原則如下表1.，驗證碼格式如下：signature=D6VJcOhTgolniL30uJNjtKYB2NI。

表1.

原   取代
"+"	 "*"
"/"	 "-"
"="	 ""(空字元)
 

將驗證碼串於服務平台網址及指令內容後面，即是具驗證功能的CaaS / CVPC API 呼叫。
    https://hws.hicloud.hinet.net/cloud_hws/api/hws/?action=runInstances
    &version=2013-03-29&chtAuthType=hwspass
    &imageId=hi-olajtpss&instanceType=HC1.S.LINUX&monitoringEnabled=false&instanceName=haha
    &count=1&accessKey=U0U0MU5UQXhNREF3TVRFek5qSTVPRFkxTURneU1UWT0&expires=2013-03-29T17:50:04Z
    &signature=D6VJcOhTgolniL30uJNjtKYB2NI
*/
?>