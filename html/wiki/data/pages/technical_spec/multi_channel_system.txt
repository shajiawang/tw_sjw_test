====== Muti Channel ======

===== Diagram =====
{{:technical_spec:muti_channel.jpg|}}

===== Description =====
   * Channel : 頻道
   * Country : 國家
   * Language : 使用語系 , 會因為國家而有所不同.
   * Channel AD : 頻道廣告系統 , 一套廣告系統可以同時存在多個頻道也可以只在某一個頻道.
   * Channel News : 頻道新聞系統 , 一套新聞系統可以同時存在多個頻道也可以只在某一個頻道.
   * Channel Saja Store : 頻道殺價系統 , 一個商店可以同時存在多個頻道也可以只在一個頻道
   * Channel Exchange Mall : 頻道兌換商城 , 一個兌換商城可以同時存在多個頻道也可以只在一個頻道.
   * Saja Store : 殺價商店.
   * Exchange Mall : 兌換商城.

===== Database =====
  * saja_channel - 經銷商
  * saja_channel_exchange_mall_rt - 經銷商兌換中心關聯
  * saja_channel_store_rt - 經銷商分館關聯
  * saja_country - 國家
  * saja_country_language_rt - 國家語言關聯
  * saja_exchange_mall - 兌換中心
  * saja_exchange_product - 兌換商品
  * saja_language - 語言
  * saja_store - 分館
  * saja_store_product_rt - 競標商品

===== Database Schema =====
==== saja_channel ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| channelid | int(10) unsigned |  | NO | PRI |  | auto_increment | 經銷商id |
| countryid | varchar(8) | utf8_general_ci | NO |  |  |  | 國家id |
| name | varchar(16) | utf8_general_ci | NO |  |  |  | 經銷商名稱（英文） |
| description | varchar(16) | utf8_general_ci | NO |  |  |  | 經銷商敘述（中文） |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_channel_exchange_mall_rt ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| channelid | int(10) unsigned |  | NO | PRI |  |  | 經銷商id |
| emid | int(10) unsigned |  | NO | PRI |  |  | 兌換中心id |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_channel_store_rt ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| channelid | int(10) unsigned |  | NO | PRI |  |  | 經銷商id |
| storeid | int(10) unsigned |  | NO | PRI |  |  | 分館id |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_country ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| countryid | varchar(8) | utf8_general_ci | NO | PRI |  |  | 國家id |
| name | varchar(16) | utf8_general_ci | NO |  |  |  | 國家名稱（英文） |
| description | varchar(16) | utf8_general_ci | NO |  |  |  | 國家敘述（中文） |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_country_language_rt ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| countryid | varchar(8) | utf8_general_ci | NO | PRI |  |  | 國家id |
| langid | varchar(8) | utf8_general_ci | NO | PRI |  |  | 語言id |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_exchange_mall ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| emid | int(10) unsigned |  | NO | PRI |  | auto_increment | 兌換中心id |
| name | varchar(16) | utf8_general_ci | NO |  |  |  | 兌換中心名稱 |
| description | varchar(16) | utf8_general_ci | NO |  |  |  | 兌換中心敘述 |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_exchange_product ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| epid | int(10) unsigned |  | NO | PRI |  | auto_increment | 兌換商品id |
| emid | int(10) unsigned |  | NO |  |  |  | 兌換中心id |
| name | varchar(16) | utf8_general_ci | NO |  |  |  | 兌換商品名稱 |
| description | varchar(16) | utf8_general_ci | NO |  |  |  | 兌換商品敘述 |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_language ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| langid | int(10) unsigned |  | NO | PRI |  |  | 語言id |
| name | varchar(8) | utf8_general_ci | NO |  |  |  | 語言名稱 |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_store ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| storeid | int(10) unsigned |  | NO | PRI |  | auto_increment | 分館id |
| name | varchar(16) | utf8_general_ci | NO |  |  |  | 分館名稱（英文） |
| description | varchar(16) | utf8_general_ci | NO |  |  |  | 分館敘述（中文） |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |

==== saja_store_product_rt ====
^ Field ^ Type ^ Collation ^ Null ^ Key ^ Default ^ Extra ^ Comment ^
| prefixid | varchar(8) | utf8_general_ci | NO | PRI |  |  |  |
| storeid | int(10) unsigned |  | NO | PRI |  |  | 分館id |
| productid | int(10) unsigned |  | NO | PRI |  |  | 競標商品id |
| name | varchar(16) | utf8_general_ci | NO |  |  |  | 競標商品名稱 |
| description | varchar(16) | utf8_general_ci | NO |  |  |  | 競標商品敘述 |
| seq | int(10) unsigned |  | NO |  |  |  |  |
| switch | enum('Y','N') | utf8_general_ci | NO |  | Y |  |  |
| insertt | datetime |  | NO |  |  |  |  |
| insertt | datetime |  | NO |  |  |  |  |


~~DISCUSSION~~