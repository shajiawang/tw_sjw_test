function RecommendProduct() {
  this.index = 1,
  this.maxBlocks = 4,
  this.blocks = $('.js_recommend_area .item_block'),
  this.itemBlockCount = this.blocks.length,
  this.maxIndex = Math.ceil(this.itemBlockCount / this.maxBlocks)
}
$(function () {
  var recommendProduct = new RecommendProduct;
  recommendProduct.resize(),
  recommendProduct.showArrow(),
  $(window).resize(function () {
    recommendProduct.resize()
  }),
  $('#recommend_prev').click(function () {
    recommendProduct.movePrev()
  }),
  $('#recommend_next').click(function () {
    recommendProduct.moveNext()
  })
}),
RecommendProduct.prototype.showArrow = function () {
  this.blocks.length <= 4 && $('.js_btn_reload').addClass('hide')
},
RecommendProduct.prototype.resize = function () {
  $('#recommend_prev:visible').length && this.showBlocks()
},
RecommendProduct.prototype.moveNext = function () {
  this.maxIndex > 1 && (this.maxIndex > this.index ? this.index++ : this.index = 1, this.showBlocks())
},
RecommendProduct.prototype.movePrev = function () {
  this.maxIndex > 1 && (this.index > 1 ? this.index-- : this.index = this.maxIndex, this.showBlocks())
},
RecommendProduct.prototype.showBlocks = function () {
  var index = this.index,
  maxBlocks = this.maxBlocks;
  $.each(this.blocks, function (key, ele) {
    key >= (index - 1) * maxBlocks && index * maxBlocks - 1 >= key ? $(ele).removeClass('hide')  : $(ele).addClass('hide')
  })
};
