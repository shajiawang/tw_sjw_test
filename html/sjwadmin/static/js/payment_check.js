var ordersAlert = function (content, time) {
  'undefined' == typeof time && (time = 2000),
  $.dialog({
    animation: 'opacity',
    closeAnimation: 'none',
    animationSpeed: 1000,
    title: ' ',
    content: '<span class="content_dialog_inner">' + content + '</span>',
    icon: 'icon-notice',
    theme: 'waca_confirm',
    keyboardEnabled: !0,
    onOpen: function () {
      var that = this;
      setTimeout(function () {
        that.close()
      }, time)
    }
  })
},
setErrorElementOffset = function (checkResult, element) {
  return 'undefined' == typeof error_element_offset ? !1 : (0 == checkResult && error_element_offset.push(parseInt(element.offset().top)), !0)
},
slideToErrorField = function () {
  if ('undefined' == typeof error_element_offset) return !1;
  var min_offset = Math.min.apply(null, error_element_offset);
  isFinite(min_offset) && $('html,body').animate({
    scrollTop: min_offset
  }, 'fast')
},
checkStocks = function (is_init) {
  //console.log(locale);
  $.post(locale + '/compareStockAjax', {
  }, function (data) {
    var check = !0,
    count = 0;
    $('.js_quantity_area').removeClass('form_valid');
    for (var stock_key in data.not_enough_stocks) {
      var keys = stock_key.split('_'),
      stock = data.not_enough_stocks[stock_key].stock.split('_');
      if (data.not_enough_stocks[stock_key].is_addprice) var qty_element = $('.js_cart_list_add_price_quantity[data-standard-id=' + keys[0] + '][data-add-price-standard-id=' + keys[1] + ']');
       else var qty_element = $('.js_cart_list_quantity[data-standard-id=' + keys[0] + ']');
      if (qty_element.parent().parent('.js_quantity_area').addClass('form_valid'), $('option', qty_element).remove(), 0 == stock[0]) qty_element.append('<option value=0>' + Lang.get('frontend.product.btn.soldout') + '</option>').next().text(Lang.get('frontend.product.btn.soldout'));
       else {
        qty_element.append($('<option value=0>' + Lang.get('frontend.product.option.select') + '</option>')).next().text(Lang.get('frontend.product.option.select')),
        stock[0] = parseInt(stock[0]),
        stock[1] = parseInt(stock[1]);
        for (var i = stock[0]; i <= stock[1]; i++) qty_element.append('<option value=' + i + '>' + i + '</option>')
      }
      check = !1,
      count++,
      setErrorElementOffset(!1, qty_element)
    }
    count > 0 && ordersAlert(Lang.get('frontend.errormsg.stock.not_enough', {
      count: count
    })),
    slideToErrorField(),
    !is_init && check ? $.post(blacklist_url, {
      purchaser_email: $('#purchaser_email').val(),
      purchaser_phone: $('#purchaser_phone').val()
    }, function (result) {
      if ('1' == result.status) {
        var recipient_id = null === recipient_selected_data.get() ? 0 : recipient_selected_data.get().id,
        order_form = $('#order_form');
        $('<input>').attr({
          type: 'hidden',
          name: 'recipient_id',
          value: recipient_id
        }).appendTo(order_form),
        601 != $('#payment').val() ? order_form.submit()  : stripe_pay()
      } else 'email' == result.check_type ? ($('#purchaser_email_area').addClass('form_valid').removeClass('form_success'), setErrorElementOffset(!1, $('#purchaser_email_area')), $('#order_submit').prop('disabled', !1))  : $.confirm({
        backgroundDismiss: !1,
        content: '<span class="content_confirm_inner">' + result.message + '</span>',
        confirmButtonClass: 'btn_confirm btn_sm',
        cancelButtonClass: 'btn_cancel btn_sm',
        confirmButton: Lang.get('frontend.contact.title.mail.message'),
        closeIcon: !1,
        icon: 'icon-notice',
        title: ' ',
        theme: 'waca_confirm',
        cancelButton: Lang.get('frontend.other.btn.cancel'),
        keyboardEnabled: !0,
        confirm: function () {
          return location.href = contact_url,
          !0
        },
        cancel: function () {
          $('#order_submit').prop('disabled', !1)
        }
      })
    }, 'json')  : $('#order_submit').prop('disabled', !1)
  })
},
validatorResult = function () {
  for (var error_message = '', i = 0; i < validator_error.length; i++) error_message += validator_error[i] + '<br/>';
  validator_error.length > 0 && ordersAlert(error_message, 5000)
};
validatorResult();
var emptyFields = function (fields) {
  for (var i = 0; i < fields.length; i++) $(fields[i]).val('')
},
checkAllpayCVS = function () {
  return emptyFields(['.js_receiver_city',
  '.js_receiver_district',
  '#receiver_address']),
  '' == $('#CVSStoreID').val() ? (ordersAlert(Lang.get('frontend.errormsg.order.allpay.store.isRequired')), !1)  : !0
};
$(function () {
  function checkValidCompanyUid(uid) {
    if (!/^\d{8}$/.test(uid)) return !1;
    for (var validateOperator = [
      1,
      2,
      1,
      2,
      1,
      2,
      4,
      1
    ], companyUidArray = uid.split('').map(function (num) {
      return parseInt(num)
    }), sum = 0, calculate = function (product) {
      var ones = product % 10,
      tens = (product - ones) / 10;
      return ones + tens
    }, i = 0; i < validateOperator.length; i++) sum += calculate(companyUidArray[i] * validateOperator[i]);
    return sum % 10 === 0 || 7 === companyUidArray[6] && (sum + 1) % 10 === 0
  }
  var checkShipping = function () {
    var shipping = $('#shipping'),
    shipping_id = '-1' != shipping.val().indexOf('custom') ? '311' : shipping.val(),
    check = !0;
    $('#shipping_area').removeClass('form_valid');
    var check_shipping = !1;
    if ('' != shipping_options && $.each(shipping_options, function (key, val) {
      shipping.val() == val.id && (check_shipping = !0)
    }), !check_shipping && 'none' != shipping.val() || '' == shipping_options) return $('#shipping_area').addClass('form_valid'),
    ordersAlert(Lang.get('frontend.errormsg.cart.noShipping'), 1200),
    !1;
    switch (shipping_id) {
      case 'none':
        $('#shipping_area').addClass('form_valid'),
        check = !1;
        break;
      case '111':
      case '121':
      case '222':
        checkReceiverAddress() || (check = !1),
        checkField('#receiver_city_area', '.js_receiver_city', '') || (check = !1),
        emptyFields(['#int_receiver_address',
        '#int_receiver_city',
        '#int_receiver_district',
        '#int_receiver_zip']);
        break;
      case '131':
        checkField('#code_7-11_area', '#code_7-11', '') || (check = !1),
        checkField('#name_7-11_area', '#name_7-11', '') || (check = !1),
        emptyFields(['.js_receiver_city',
        '.js_receiver_district',
        '#receiver_address',
        '#int_receiver_address',
        '#int_receiver_city',
        '#int_receiver_district',
        '#int_receiver_zip']);
        break;
      case '211':
        emptyFields(['.js_receiver_city',
        '.js_receiver_district',
        '#receiver_address',
        '#int_receiver_address',
        '#int_receiver_city',
        '#int_receiver_district',
        '#int_receiver_zip']),
        checkField('#store_input_area', '#store_name', '') || (check = !1);
        break;
      case '221':
        emptyFields(['.js_receiver_city',
        '.js_receiver_district',
        '#receiver_address',
        '#int_receiver_address',
        '#int_receiver_city',
        '#int_receiver_district',
        '#int_receiver_zip']),
        '' == $('input[name=\'shipping_info[221][system]\']').val() && (ordersAlert(Lang.get('frontend.errormsg.order.allpay.store.isRequired')), check = !1);
        break;
      case '231':
        emptyFields(['.js_receiver_city',
        '.js_receiver_district',
        '.js_receiver_zip']),
        checkReceiverIntAddress(!1) || (check = !1),
        checkField('#receiver_int_country_area', '#int_receiver_country', 'none') || (check = !1);
        break;
      case '242':
      case '243':
      case '244':
      case '245':
      case '246':
      case '247':
        checkAllpayCVS() || (check = !1),
        emptyFields(['.js_receiver_city',
        '.js_receiver_district',
        '#receiver_address',
        '#int_receiver_address',
        '#int_receiver_city',
        '#int_receiver_district',
        '#int_receiver_zip']);
        break;
      case '311':
        var shipping_code = shipping.val();
        if ('undefined' != typeof shipping_options[shipping_code] && '1' == shipping_options[shipping_code].setting.has_address) if ('1' == shipping_options[shipping_code].setting.in_taiwan) emptyFields(['#int_receiver_address',
        '#int_receiver_city',
        '#int_receiver_district',
        '#int_receiver_zip']),
        checkReceiverAddress() || (check = !1),
        checkField('#receiver_city_area', '.js_receiver_city', '') || (check = !1),
        checkField('#receiver_district_area', '.js_receiver_district', '') || (check = !1);
         else if (emptyFields(['.js_receiver_city',
        '.js_receiver_district',
        '.js_receiver_zip']), checkReceiverIntAddress(!1) || (check = !1), checkField('#receiver_address_country_area', '#receiver_country_sel', '')) {
          var receiver_country = $.trim($('#receiver_country_sel').val()),
          check_country = !1;
          if ($.each(JSON.parse(country_list), function (key, val) {
            val.country_id == receiver_country && (check_country = !0, $('#receiver_country').val(val.country_id))
          }), !check_country) return $('#receiver_address_country_area').addClass('form_valid').removeClass('form_success').find('.help_block').eq(0).find('span').text(Lang.get('frontend.errormsg.address.country.format')),
          setErrorElementOffset(!1, $('#receiver_address_country_area')),
          !1
        } else check = !1;
         else emptyFields(['.js_receiver_city',
        '.js_receiver_district',
        '#receiver_address',
        '#int_receiver_address',
        '#int_receiver_city',
        '#int_receiver_district',
        '#int_receiver_zip'])
    }
    return setErrorElementOffset(check, $('#shipping_area')),
    check
  },
  checkInvoice = function () {
    if ('111' == $('#payment').val() || '141' == $('#payment').val() || 0 == has_invoice) return !0;
    var check = !0;
    switch ($('#orders_invoice').val()) {
      case '1':
        switch ($('#carrier_type').val()) {
          case '0':
            checkField('#carrier_no_mobile_area', '#carrier_no_mobile', '') || (check = !1),
            checkCarrierNo('#carrier_no_mobile') || (check = !1),
            checkCheckBox('#electronic_return', Lang.get('frontend.errormsg.order.invoice.electronic_return.isRequired', {
              shops_name: shops_name
            })) || (check = !1);
            var carrier_no_mobile = $('#carrier_no_mobile').val();
            carrier_no_mobile.match(/^\/+[0-9A-Za-z+-.]{7}$/) || ($('#carrier_no_mobile_area').addClass('form_valid'), setErrorElementOffset(!1, $('#carrier_no_mobile')), check = !1),
            $('#hard_copy').prop('checked', !1),
            $('#invoice_address').val('');
            break;
          case '1':
            checkField('#carrier_no_nature_area', '#carrier_no_nature', '') || (check = !1),
            checkCarrierNo('#carrier_no_nature') || (check = !1),
            checkCheckBox('#electronic_return', Lang.get('frontend.errormsg.order.invoice.electronic_return.isRequired', {
              shops_name: shops_name
            })) || (check = !1);
            var carrier_no_nature = $('#carrier_no_nature').val();
            carrier_no_nature.match(/^[0-9A-Za-z]{16}$/) || ($('#carrier_no_nature_area').addClass('form_valid'), setErrorElementOffset(!1, $('#carrier_no_nature')), check = !1),
            $('#hard_copy').prop('checked', !1),
            $('#invoice_address').val('');
            break;
          case '2':
            checkCheckBox('#electronic_return', Lang.get('frontend.errormsg.order.invoice.electronic_return.isRequired', {
              shops_name: shops_name
            })) || (check = !1),
            $('#hard_copy').is(':checked') && (checkField('#invoice_address_area', '#invoice_address', '') || (check = !1));
            break;
          case '3':
            checkField('#carrier_no_easycard_area', '#carrier_no_easycard', '') || (check = !1),
            checkCarrierNo('#carrier_no_easycard') || (check = !1),
            checkCheckBox('#electronic_return', Lang.get('frontend.errormsg.order.invoice.electronic_return.isRequired', {
              shops_name: shops_name
            })) || (check = !1);
            var carrier_no_easycard = $('#carrier_no_easycard').val();
            carrier_no_easycard.match(/^[0-9]{9}$/) || ($('#carrier_no_easycard_area').addClass('form_valid'), setErrorElementOffset(!1, $('#carrier_no_easycard')), check = !1),
            $('#hard_copy').prop('checked', !1),
            $('#invoice_address').val('');
            break;
          case '4':
            $('#hard_copy').is(':checked') && (checkField('#invoice_address_area', '#invoice_address', '') || (check = !1)),
            'Pay2Go' == invoice_type && (checkField('#invoice_address_area', '#invoice_address', '') || (check = !1)),
            checkCheckBox('#electronic_return', Lang.get('frontend.errormsg.order.invoice.electronic_return.isRequired', {
              shops_name: shops_name
            })) || (check = !1)
        }
        break;
      case '2':
        checkField('#company_title_area', '#company_title', '') || (check = !1),
        checkCompanyUid() || (check = !1),
        'Pay2Go' == invoice_type && (checkField('#invoice_address_area', '#invoice_address', '') || (check = !1));
        break;
      case '3':
        checkCheckBox('#electronic_return', Lang.get('frontend.errormsg.order.invoice.electronic_return.isRequired', {
          shops_name: shops_name
        })) || (check = !1)
      }
      return setErrorElementOffset(check, $('#carrier_type_area')),
      check
  },
  checkCarrierNo = function (field) {
    return $.trim($(field).val()) == $.trim($(field + '_confirm').val()) ? ($(field + '_confirm_area').removeClass('form_valid'), !0)  : ($(field + '_confirm_area').addClass('form_valid'), setErrorElementOffset(!1, $(field)), !1)
},
checkMaskingAddress = function (address) {
  return null === recipient_selected_data.get() ? !1 : address === recipient_selected_data.get().address
},
checkReceiverAddress = function () {
var receiver_address = $.trim($('#receiver_address').val()),
error_msg = [
  Lang.get('frontend.errormsg.address.isRequired'),
  Lang.get('frontend.errormsg.address.format')
],
error_txt_block = $('#receiver_address_area').find('.help_block').eq(0).find('span');
return '' == receiver_address ? ($('#receiver_address_area').addClass('form_valid').removeClass('form_success'), error_txt_block.text(error_msg[0]), setErrorElementOffset(!1, $('#receiver_address_area')), !1)  : /^0|1|2|3|4|5|6|7|8|9|０|１|２|３|４|５|６|７|８|９|一|二|三|四|五|六|七|八|九|十$/.test(receiver_address) || checkMaskingAddress(receiver_address) ? ($('#receiver_address_area').addClass('form_success').removeClass('form_valid'), !0)  : ($('#receiver_address_area').addClass('form_valid').removeClass('form_success'), error_txt_block.text(error_msg[1]), setErrorElementOffset(!1, $('#receiver_address_area')), !1)
},
checkReceiverIntAddress = function (only_check) {
var int_receiver_address = $.trim($('#int_receiver_address').val()),
int_receiver_district = ($.trim($('#int_receiver_city').val()), $.trim($('#int_receiver_district').val())),
int_receiver_zip = $.trim($('#int_receiver_zip').val()),
error_address_elm = $('#receiver_int_address_area'),
error_city_elm = $('#receiver_int_city_area'),
error_district_elm = $('#receiver_int_district_area'),
error_zip_elm = $('#receiver_int_zip_area'),
error_address_txt_block = error_address_elm.find('.help_block').eq(0).find('span'),
error_district_txt_block = (error_city_elm.find('.help_block').eq(0).find('span'), error_district_elm.find('.help_block').eq(0).find('span')),
error_zip_txt_block = error_zip_elm.find('.help_block').eq(0).find('span'),
error = !1;
return only_check || '' != int_receiver_address ? /^0|1|2|3|4|5|6|7|8|9|０|１|２|３|４|５|６|７|８|９|一|二|三|四|五|六|七|八|九|十$/.test(int_receiver_address) || checkMaskingAddress(int_receiver_address) ? error_address_elm.addClass('form_success').removeClass('form_valid')  : (error_address_elm.addClass('form_valid').removeClass('form_success'), error_address_txt_block.text(Lang.get('frontend.errormsg.address.format')), setErrorElementOffset(!1, error_address_elm), error = !0)  : (error_address_elm.addClass('form_valid').removeClass('form_success'), error_address_txt_block.text(Lang.get('frontend.errormsg.address.isRequired')), setErrorElementOffset(!1, error_address_elm), error = !0),
only_check || '' != int_receiver_district ? error_district_elm.addClass('form_success').removeClass('form_valid')  : (error_district_elm.addClass('form_valid').removeClass('form_success'), error_district_txt_block.text(Lang.get('frontend.errormsg.address.city.isRequired')), setErrorElementOffset(!1, error_district_elm), error = !0),
only_check || '' != int_receiver_zip ? error_zip_elm.addClass('form_success').removeClass('form_valid')  : (error_zip_elm.addClass('form_valid').removeClass('form_success'), error_zip_txt_block.text(Lang.get('frontend.errormsg.address.zip.isRequired')), setErrorElementOffset(!1, error_zip_elm), error = !0),
!error
},
checkField = function (area, field, check_val) {
return !$('#same_info').is(':checked') || '#receiver_name' != field && '#receiver_phone' != field ? 0 == order_email && '#purchaser_email' == field ? ($(area).removeClass('form_valid'), !0)  : $.trim($(field).val()) == check_val ? ($(area).addClass('form_valid'), setErrorElementOffset(!1, $(field)), !1)  : ($(area).removeClass('form_valid'), !0)  : ($(area).removeClass('form_valid'), !0)
},
checkMaskingEmail = function (email) {
return '' === member_default_data.email ? !1 : email === member_default_data.email
},
checkPaymentEmail = function (only_check) {
var Email = $.trim($('#purchaser_email').val()),
shipping = $('#shipping').val(),
payment = $('#payment').val(),
re = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,}$/i;
if (1 == order_email || - 1 != $.inArray(shipping, [
'221',
'222'
]) || - 1 != $.inArray(payment, [
'401'
])) {
if (only_check) return !0;
if (!re.test(Email) && !checkMaskingEmail(Email)) return $('#purchaser_email_area').addClass('form_valid').removeClass('form_success'),
setErrorElementOffset(!1, $('#purchaser_email_area')),
!1
} else if ('' != Email) {
if (only_check) return !0;
if (!re.test(Email) && !checkMaskingEmail(Email)) return $('#purchaser_email_area').addClass('form_valid').removeClass('form_success'),
setErrorElementOffset(!1, $('#purchaser_email_area')),
!1
}
return $('#purchaser_email_area').addClass('form_success').removeClass('form_valid'),
!0
},
checkMaskingPhoneByShipping = function (phone, type) {
var re = /^09[0-9]{2}\*{6}$/;
return 'purchaser_phone' === type ? re.test(phone) && checkMaskingPhone(phone, type)  : 'receiver_phone' === type ? re.test(phone) && checkMaskingPhone(phone, type)  : void 0
},
checkPaymentPhoneByShipping = function (type) {
var phone = $.trim($('#' + type).val()),
shipping = $('#shipping').val(),
error_txt_block = $('#' + type + '_area').find('.help_block').eq(0).find('span'),
cellphone_re = /^09[0-9]{8}$/,
error_msg = Lang.get('frontend.errormsg.cart.phone.isRequired');
if ( - 1 != $.inArray(shipping, [
'221',
'222',
'241',
'242',
'243',
'244',
'245',
'246',
'247',
'131'
])) {
if (!cellphone_re.test(phone) && !checkMaskingPhoneByShipping(phone, type)) return $('#' + type + '_area').addClass('form_valid'),
error_txt_block.text(error_msg),
setErrorElementOffset(!1, $('#' + type + '_area')),
!1
} else if ('-1' != shipping.indexOf('custom') && 'undefined' != typeof shipping_options[shipping] && '1' == shipping_options[shipping].setting.in_taiwan && !cellphone_re.test(phone) && !checkMaskingPhoneByShipping(phone, type)) return $('#' + type + '_area').addClass('form_valid'),
error_txt_block.text(error_msg),
setErrorElementOffset(!1, $('#' + type + '_area')),
!1;
return !0
},
checkMaskingPhone = function (phone, type) {
return 'purchaser_phone' === type ? '' === member_default_data.phone ? !1 : phone === member_default_data.phone : 'receiver_phone' === type ? null === recipient_selected_data.get() ? !1 : phone === recipient_selected_data.get().phone : void 0
},
checkPaymentPhone = function (type) {
var phone = $.trim($('#' + type).val()),
payment = $('#payment').val(),
error_msg = [
Lang.get('frontend.errormsg.cart.phone.format1'),
Lang.get('frontend.errormsg.cart.phone.format2'),
Lang.get('frontend.errormsg.cart.phone.isRequired')
],
error_txt_block = $('#' + type + '_area').find('.help_block').eq(0).find('span');
if ('receiver_phone' == type) {
if ($('#same_info').is(':checked')) return $('#' + type + '_area').removeClass('form_valid'),
!0;
if (!checkPaymentPhoneByShipping(type)) return !1;
if ((!phone.match(/^[0-9+#]+$/) || phone.length > 20 || phone.length < 8) && !checkMaskingPhone(phone, type)) return $('#' + type + '_area').addClass('form_valid'),
error_txt_block.text(error_msg[0]),
setErrorElementOffset(!1, $('#' + type + '_area')),
!1
} else {
if ($('#same_info').is(':checked') && !checkPaymentPhoneByShipping(type)) return !1;
if ('34' == payment.substr(0, 2)) {
if ((!phone.match(/^[0-9+]+$/) || phone.length > 20 || phone.length < 8) && !checkMaskingPhone(phone, type) || phone.indexOf('*') > - 1 && '1' === member_default_data.phoneHasSharp) return $('#' + type + '_area').addClass('form_valid'),
error_txt_block.text(error_msg[1]),
setErrorElementOffset(!1, $('#' + type + '_area')),
!1
} else if ((!phone.match(/^[0-9+#]+$/) || phone.length > 20 || phone.length < 8) && !checkMaskingPhone(phone, type)) return $('#' + type + '_area').addClass('form_valid'),
error_txt_block.text(error_msg[0]),
setErrorElementOffset(!1, $('#' + type + '_area')),
!1
}
return $('#' + type + '_area').removeClass('form_valid'),
!0
},
checkMaskingName = function (name, type) {
return 'receiver_name' === type ? null === recipient_selected_data.get() ? !1 : name === recipient_selected_data.get().name : 'purchaser_name' === type ? '' === member_default_data.name ? !1 : name === member_default_data.name : void 0
},
checkPaymentName = function (type) {
var name = $.trim($('#' + type).val()),
error_msg = Lang.get('frontend.errormsg.cart.name.format'),
error_txt_block = $('#' + type + '_area').find('.help_block').eq(0).find('span');
return $('#same_info').is(':checked') && 'receiver_name' == type ? ($('#' + type + '_area').removeClass('form_valid'), !0)  : name.match(/^[\u0391-\uFFE5A-Za-z ]+$/) || checkMaskingName(name, type) ? ($('#' + type + '_area').removeClass('form_valid'), !0)  : ($('#' + type + '_area').addClass('form_valid'), error_txt_block.text(error_msg), setErrorElementOffset(!1, $('#' + type + '_area')), !1)
},
checkNumeric = function (area, field) {
var re = /^[0-9]+$/,
value = $.trim($(field).val());
return re.test(value) ? ($(area).removeClass('form_valid'), !0)  : ($(area).addClass('form_valid'), setErrorElementOffset(!1, $(field)), !1)
},
checkCheckBox = function (field, alertContent) {
return $(field).prop('checked') ? !0 : (ordersAlert(alertContent), setErrorElementOffset(!1, $(field)), !1)
},
checkCompanyUid = function () {
var company_uid = $.trim($('#company_uid').val());
return checkField('#company_uid_area', '#company_uid', '') && checkNumeric('#company_uid_area', '#company_uid') ? checkValidCompanyUid(company_uid) ? ($('#company_uid_area').removeClass('form_valid'), !0)  : ($('#company_uid_area').addClass('form_valid'), setErrorElementOffset(!1, $('#company_uid')), !1)  : !1
},
checkCartItems = function () {
return $('.js_ticket_item_list').length > 0 && $('.js_cart_item_list').length != $('.js_ticket_item_list').length ? (ordersAlert(Lang.get('frontend.errormsg.cart.ticketProduct'), 5000), !1)  : $('.js_ticket_item_list').length > 1 ? (ordersAlert(Lang.get('frontend.errormsg.cart.ticketProduct.onlyone'), 5000), !1)  : '0' == merge_pre_order && $('.js_preorder_item_list').length > 0 && $('.js_cart_item_list').length != $('.js_preorder_item_list').length && $('.js_ticket_item_list').length != $('.js_preorder_item_list').length ? (ordersAlert(Lang.get('frontend.errormsg.cart.unMergePreOrder'), 5000), !1)  : !0
};
$('#order_form').on('change', '#order_fileds input, #order_fileds select', function () {
var id = $(this).attr('id');
switch (id) {
case 'purchaser_email':
checkPaymentEmail();
break;
case 'purchaser_phone':
case 'receiver_phone':
checkPaymentPhone(id);
break;
case 'carrier_no_nature_confirm':
checkCarrierNo('#carrier_no_nature');
break;
case 'carrier_no_mobile_confirm':
checkCarrierNo('#carrier_no_mobile');
break;
case 'lovecode':
checkField('#lovecode_area', '#lovecode', ''),
checkNumeric('#lovecode_area', '#lovecode');
break;
case 'company_uid':
checkCompanyUid();
break;
case 'receiver_address':
checkReceiverAddress();
break;
case 'int_receiver_address':
case 'int_receiver_district':
case 'int_receiver_zip':
checkReceiverIntAddress(!0);
break;
case 'int_receiver_country':
checkField('#receiver_int_country_area', '#int_receiver_country', 'none');
break;
case 'receiver_country_sel':
checkField('#receiver_address_country_area', '#receiver_country_sel', '');
break;
case 'carrier_no_easycard_confirm':
checkCarrierNo('#carrier_no_easycard');
break;
case 'receiver_address_zip':
checkReceiverAddressZip();
break;
case void 0:
$(this).hasClass('js_receiver_city') && checkField('#receiver_city_area', '.js_receiver_city', '');
break;
default:
if ('shipping' == id) {
checkPaymentEmail(!0);
break
}
'payment' == id && checkPaymentEmail(!0),
'SELECT' == $(this) [0].tagName ? checkField('#' + id + '_area', '#' + id, 'none')  : checkField('#' + id + '_area', '#' + id, '')
}
}); 
var requireField = {
input: [
'receiver_name',
'receiver_phone',
'purchaser_name',
'purchaser_email',
'purchaser_phone'
],
select: [
'payment'
]
}; 
$('#order_submit').click(function(){ //console.log(locale);
	if ($(this).prop('disabled', !0), !checkCartItems()) return $(this).prop('disabled', !1),
	!1;
	var error = 0;
	error_element_offset = [
	],
	checkShipping() || (error += 1),
	checkInvoice() || (error += 1);
	for (var key in requireField) for (var i = 0; i < requireField[key].length; i++) {
		if ('input' == key) var check_val = '';
		 else var check_val = 'none';
		switch (checkField('#' + requireField[key][i] + '_area', '#' + requireField[key][i], check_val) || (error += 1), requireField[key][i]) {
		case 'purchaser_email':
		checkPaymentEmail() || (error += 1);
		break;
		case 'purchaser_phone':
		case 'receiver_phone':
		checkPaymentPhone(requireField[key][i]) || (error += 1);
		break;
		case 'receiver_name':
		case 'purchaser_name':
		checkPaymentName(requireField[key][i]) || (error += 1)
		}
	}
	if (0 == error) if (checkCheckBox('#read', Lang.get('frontend.errormsg.order.read.isRequired'))) {
		var is_holiday_send = $('.js_is_holiday_send').hasClass('hide') ? - 1 : $('#is_holiday_send').is(':checked') ? 1 : 0;
		$('#is_holiday_send_val').val(is_holiday_send),
		checkStocks(!1)
	} else $(this).prop('disabled', !1);
	 else slideToErrorField(),
	$(this).prop('disabled', !1)
}) 
}),
$(window).load(function () {
checkStocks(!0)
});
