function RecommendProduct() {
  this.index = 1,
  this.maxBlocks = 4,
  this.blocks = $('.js_recommend_area .item_block'),
  this.itemBlockCount = this.blocks.length,
  this.maxIndex = Math.ceil(this.itemBlockCount / this.maxBlocks)
}
$(document).ready(function () {
  function copyAlert(text, icon) {
    var dialog_timeout;
    $.dialog({
      animation: 'opacity',
      closeAnimation: 'none',
      closeIcon: !1,
      animationSpeed: 1000,
      title: ' ',
      content: '<span class="content_confirm_inner">' + text + '</span>',
      icon: icon,
      theme: 'waca_confirm',
      keyboardEnabled: !0,
      columnClass: 'col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2',
      onOpen: function () {
        var that = this;
        dialog_timeout = setTimeout(function () {
          that.close()
        }, 1000)
      },
      onClose: function () {
        clearTimeout(dialog_timeout)
      }
    })
  }
  /*
  var dialog_timeout,
  slideBottom = new Menu({
    type: 'push-bottom'
  });
  if ($('.js_payment_toggle').click(function (e) {
    e.preventDefault,
    hasShopsDial && shopsDial.close(),
    slideBottom.open()
  }), hasShopsDial) {
    var shopsDial = new Menu({
      type: 'shops-dial'
    });
    $('.js_btn_telcall').click(function (e) {
      e.preventDefault,
      slideBottom.close(),
      shopsDial.open()
    })
  }
  */
  var qty_element = $('.js_product_qty'),
  standard_element = $('.js_product_standard'),
  addprice_element = $('.js_add_cart_addprice'),
  buyButton = function (status) {
    'out_of_stock' == status ? ($('.js_addcart_outofsoldout').removeClass('hide'), $('.js_addcart_origin, .js_product_qty, .js_addcart_soldout').addClass('hide'))  : 'sold_out' == status ? ($('.js_addcart_soldout').removeClass('hide'), $('.js_addcart_origin, .js_product_qty, .js_addcart_outofsoldout').addClass('hide'))  : ($('.js_addcart_soldout, .js_addcart_outofsoldout').addClass('hide'), $('.js_addcart_origin, .js_product_qty').removeClass('hide'))
  },
  changeMaxProductQty = function (stock, stock_status) {
    var html = '',
    stocks = stock.split('_');
    stocks[0] = parseInt(stocks[0]),
    stocks[1] = parseInt(stocks[1]);
    for (var i = stocks[0]; i <= stocks[1]; i++) html += '<option value=' + i + '>' + i + '</option>';
    qty_element.each(function () {
      'SELECT' == this.tagName && $(this).html(html)
    }),
    qty_element.data('min', parseInt(stocks[0])),
    qty_element.data('max', parseInt(stocks[1])),
    0 == stocks[0] ? buyButton(stock_status)  : buyButton()
  },
  changeCartNum = function (data) {
    $('.js_cart_number').text(data.cart_number)
  },
  addCartItemAndPriceAction = function (is_go_cart_btn) {
    var params = {
      product_id: product_id,
      qty: qty_element.val(),
      standard_id: standard_element.val(),
      all_add_prices_id: [
      ],
      add_add_prices: {
      }
    },
    total_qty = 0,
    i = 0;
    if (addprice_element.each(function () {
      var check_val = $(this).prop('checked'),
      add_price_id = $(this).attr('data-add-price-id'),
      add_price_standard_id = $(this).attr('data-add-price-standard-id'),
      qty = $('.js_addprice_quantity[data-add-price-id=' + add_price_id + '][data-add-price-standard-id=' + add_price_standard_id + ']').val();
      params.all_add_prices_id.push(add_price_id + add_price_standard_id),
      check_val && (total_qty += parseInt(qty), params.add_add_prices[i] = {
        id: add_price_id,
        standard_id: add_price_standard_id,
        quantity: qty
      }, i++)
    }), check_add_price_qty_limit(parseInt(qty_element.val()), total_qty)) {
      if ('1' == is_fb && is_go_cart_btn) var openWindow = window.open();
      setCartItem('add_cart_item_and_add_price_item_batch', params, function (data) {
        if ('undefined' != typeof data.add_to_cart_trace) {
          $('#addtocart').remove();
          var script = document.createElement('script');
          script.id = 'addtocart',
          script.innerHTML = data.add_to_cart_trace,
          document.body.appendChild(script)
        }
        'undefined' == typeof data.status || data.status ? is_go_cart_btn ? '1' == is_fb ? openWindow.location = locale + '/cart' : location.href = locale + '/cart' : ($.dialog({
          animation: 'opacity',
          closeAnimation: 'none',
          animationSpeed: 1000,
          title: ' ',
          content: '<span class="content_dialog_inner">' + Lang.get('frontend.product.addcart.success') + '</span>',
          icon: 'icon-checkmark',
          theme: 'waca_confirm',
          keyboardEnabled: !0,
          columnClass: 'col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2',
          onOpen: function () {
            var that = this;
            dialog_timeout = setTimeout(function () {
              that.close(),
              slideBottom.close()
            }, 2000)
          },
          onClose: function () {
            clearTimeout(dialog_timeout)
          }
        }), changeCartNum(data))  : $.dialog({
          animation: 'opacity',
          closeAnimation: 'none',
          animationSpeed: 1000,
          title: ' ',
          content: '<span class="content_dialog_inner">' + data.message + '</span>',
          icon: 'icon-notice',
          theme: 'waca_confirm',
          keyboardEnabled: !0,
          columnClass: 'col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2',
          onOpen: function () {
            var that = this;
            dialog_timeout = setTimeout(function () {
              that.close(),
              slideBottom.close()
            }, 5000)
          },
          onClose: function () {
            clearTimeout(dialog_timeout)
          }
        })
      })
    }
  };
  standard_element.change(function () {
    var standard_id = this.value,
    tag_name = this.tagName;
    if ('SELECT' == tag_name) {
      var stock = $(this).find('option:selected').data('product-stock'),
      stock_status = $(this).find('option:selected').data('stock');
      $('#radio_collection_' + standard_id).prop('checked', !0)
    } else if ('INPUT' == tag_name) {
      var stock = $(this).data('product-stock'),
      stock_status = $(this).data('stock');
      $('#js_desktop_product_standard').val(standard_id),
      selectorSyncSpan($('#js_desktop_product_standard'))
    }
    changeMaxProductQty(stock, stock_status),
    qty_element.val(stock.split('_') [0]).change()
  }),
  qty_element.change(function () {
    var qty = $(this).val();
    qty_element.val(qty)
  }),
  $('.js_add_cart').click(function () {
    addCartItemAndPriceAction(!1)
  }),
  $('.js_add_and_go_cart').click(function (e) {
    addCartItemAndPriceAction(!0)
  }),
  $('.js_soldout_notify').click(function (e) {
    $.confirm({
      animation: 'opacity',
      closeAnimation: 'none',
      animationSpeed: 1000,
      title: Lang.get('frontend.product.btn.soldoutnotify'),
      content: '<div class="content_dialog_inner"><div class="form_group"><div class="help_block help_tips"><i class="icon-triangle-right"></i><span>' + Lang.get('frontend.product.soldout.notify.content') + '</span></div></div><div class="form_group"><h5>' + Lang.get('frontend.product.soldout.notify.product_name') + '：<span class="text_overflow">' + $('.js_product_name').html() + ('1' == is_multiple ? '-' + $('#js_desktop_product_standard option:selected').data('name')  : '') + '</h5></div><div class="form_group js_email_area form_lastmargin"><h5>' + Lang.get('frontend.product.soldout.notify.email') + '：<input type="text" placeholder="ex: waca@example.com" class="form_input form_control js_soldout_notify_mail"></h5><div class="help_block"><i class="icon-exclamation-circle"></i><span>' + Lang.get('frontend.errormsg.soldout.notify.email.format') + '</span></div></div></div>',
      closeIcon: !1,
      theme: 'waca_popup_store soldout_mail',
      keyboardEnabled: !0,
      confirmButtonClass: 'btn_md btn_confirm',
      cancelButtonClass: 'btn_md btn_cancel',
      confirmButton: Lang.get('frontend.contact.qa.button.submit'),
      cancelButton: Lang.get('frontend.other.btn.cancel'),
      confirm: function () {
        var email = $.trim($('.js_soldout_notify_mail').val()),
        re = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,}$/i;
        return re.test(email) ? ($('.js_email_area').removeClass('form_valid'), void $.post(soldout_notify_url, {
          product_id: product_id,
          products_standard_id: $('.js_product_standard').val(),
          email: email
        }, function (result) {
          alert_message(result.msg)
        }, 'json'))  : ($('.js_email_area').addClass('form_valid'), !1)
      },
      columnClass: 'col-lg-4 col-lg-offset-4 col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2'
    })
  }),
  $('.js_promTotal_bar').on('click', function () {
    $('.js_prom_txt').hasClass('open') ? ($('.js_prom_txt').removeClass('open'), $('.js_promTotal_cont').hide(), $('.js_promTotal_icon').removeClass('icon-chevron-small-up'))  : ($('.js_prom_txt').addClass('open'), $('.js_promTotal_cont').removeClass('hide').hide().fadeIn(), $('.js_promTotal_icon').addClass('icon-chevron-small-up'))
  }),
  browsingHistory.add(browsing_history_data) && browsingHistory.save(),
  parseInt(s_id) > 0 && $('.js_product_standard option[value="' + s_id + '"]').prop('selected', !0).change();
  var clipboard = new Clipboard('.js_redeem_reference_copy');
  clipboard.on('success', function (e) {
    copyAlert(Lang.get('frontend.member.referral.copy.success'), 'icon-checkmark')
  }),
  clipboard.on('error', function (e) {
    copyAlert(Lang.get('frontend.member.referral.copy.fail'), 'icon-notice')
  })
});
var swiper = new Swiper('.swiper-container', {
  pagination: '.product_thumbs',
  paginationClickable: !0,
  resizeReInit: !0,
  lazyLoading: !0,
  preloadImages: !1,
  paginationBulletRender: function (swiper, index, className) {
    if ($(window).width() <= 750) return '<span class="' + className + '"></span>';
    var slide = $('.' + this.wrapperClass).find('.swiper-slide img') [index],
    small_url = $(slide).data('small');
    return '<span class="' + className + '" style="width: 75px;height: 75px;background: #fff;opacity: 1;position: relative;display: block;border-radius: 0;float: left;"><img src="' + small_url + '" /></span>'
  }
}),
initPhotoSwipeFromDOM = function (gallerySelector) {
  for (var parseThumbnailElements = function (el) {
    for (var figureEl, linkEl, size, item, thumbElements = el.childNodes, numNodes = thumbElements.length, items = [
    ], i = 0; numNodes > i; i++) if (figureEl = thumbElements[i], 1 === figureEl.nodeType) {
      linkEl = figureEl.children[0],
      size = figureEl.getAttribute('data-size').split('x');
      var src = null != linkEl.getAttribute('data-src') ? linkEl.getAttribute('data-src')  : linkEl.getAttribute('src');
      item = {
        src: src,
        w: parseInt(size[0], 10),
        h: parseInt(size[1], 10)
      },
      figureEl.children.length > 1 && (item.title = figureEl.children[1].innerHTML),
      linkEl.children.length > 0 && (item.msrc = linkEl.children[0].getAttribute('src')),
      item.el = figureEl,
      items.push(item)
    }
    return items
  }, closest = function closest(el, fn) {
    return el && (fn(el) ? el : closest(el.parentNode, fn))
  }, onThumbnailsClick = function (e) {
    e = e || window.event,
    e.preventDefault ? e.preventDefault()  : e.returnValue = !1;
    var eTarget = e.target || e.srcElement,
    clickedListItem = closest(eTarget, function (el) {
      return el.tagName && 'DIV' === el.tagName.toUpperCase()
    });
    if (clickedListItem) {
      for (var index, clickedGallery = clickedListItem.parentNode, childNodes = clickedListItem.parentNode.childNodes, numChildNodes = childNodes.length, nodeIndex = 0, i = 0; numChildNodes > i; i++) if (1 === childNodes[i].nodeType) {
        if (childNodes[i] === clickedListItem) {
          index = nodeIndex;
          break
        }
        nodeIndex++
      }
      return index >= 0 && openPhotoSwipe(index, clickedGallery),
      !1
    }
  }, photoswipeParseHash = function () {
    var hash = window.location.hash.substring(1),
    params = {
    };
    if (hash.length < 5) return params;
    for (var vars = hash.split('&'), i = 0; i < vars.length; i++) if (vars[i]) {
      var pair = vars[i].split('=');
      pair.length < 2 || (params[pair[0]] = pair[1])
    }
    return params.gid && (params.gid = parseInt(params.gid, 10)),
    params
  }, openPhotoSwipe = function (index, galleryElement, disableAnimation, fromURL) {
    var gallery,
    options,
    items,
    pswpElement = document.querySelectorAll('.pswp') [0];
    if (items = parseThumbnailElements(galleryElement), options = {
      galleryUID: galleryElement.getAttribute('data-pswp-uid'),
      getThumbBoundsFn: function (index) {
        var thumbnail = items[index].el.getElementsByTagName('img') [0],
        pageYScroll = window.pageYOffset || document.documentElement.scrollTop,
        rect = thumbnail.getBoundingClientRect();
        return {
          x: rect.left,
          y: rect.top + pageYScroll,
          w: rect.width
        }
      }
    }, fromURL) if (options.galleryPIDs) {
      for (var j = 0; j < items.length; j++) if (items[j].pid == index) {
        options.index = j;
        break
      }
    } else options.index = parseInt(index, 10) - 1;
     else options.index = parseInt(index, 10);
    isNaN(options.index) || (disableAnimation && (options.showAnimationDuration = 0), gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI_Default, items, options), gallery.init())
  }, galleryElements = document.querySelectorAll(gallerySelector), i = 0, l = galleryElements.length; l > i; i++) galleryElements[i].setAttribute('data-pswp-uid', i + 1),
  galleryElements[i].onclick = onThumbnailsClick;
  var hashData = photoswipeParseHash();
  hashData.pid && hashData.gid && openPhotoSwipe(hashData.pid, galleryElements[hashData.gid - 1], !0, !0)
};
RecommendProduct.prototype.showArrow = function () {
  this.blocks.length <= 4 && $('.js_btn_reload').addClass('hide')
},
RecommendProduct.prototype.resize = function () {
  $('#recommend_prev:visible').length && this.showBlocks()
},
RecommendProduct.prototype.moveNext = function () {
  this.maxIndex > 1 && (this.maxIndex > this.index ? this.index++ : this.index = 1, this.showBlocks())
},
RecommendProduct.prototype.movePrev = function () {
  this.maxIndex > 1 && (this.index > 1 ? this.index-- : this.index = this.maxIndex, this.showBlocks())
},
RecommendProduct.prototype.showBlocks = function () {
  var index = this.index,
  maxBlocks = this.maxBlocks;
  $.each(this.blocks, function (key, ele) {
    key >= (index - 1) * maxBlocks && index * maxBlocks - 1 >= key ? $(ele).removeClass('hide')  : $(ele).addClass('hide')
  })
},
initPhotoSwipeFromDOM('.my-gallery'),
$('.js_lightbox_addprice').each(function () {
  var addprice_key = $(this).data('addpricekey');
  initPhotoSwipeFromDOM('.js_lightbox_gallery_addprice' + addprice_key)
});
