function setOwlCarousel(is_fb, is_backend, area, slideshow, spacing, number, lang) {
  if (is_backend || $('.js_banner_area').removeClass('hide'), 'undefined' != typeof lang) var banner = '.js_banner_area.' + lang + ' .js_banner_' + area;
   else var banner = '.js_banner_' + area;
  if ('undefined' != typeof swiper[area]) if ('undefined' == typeof swiper[area].length) swiper[area].destroy(!0, !0);
   else for (var l = 0; l < swiper[area].length; l++) swiper[area][l].destroy(!0, !0);
  '1' == slideshow || $(window).width() <= 991 && !is_fb && number > 1 ? (swiper[area] = new Swiper(banner, {
    pagination: '.swiper-pagination',
    paginationClickable: !0,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    autoHeight: !0,
    autoplay: !0,
    loop: !1,
    speed: 800,
    autoplayDisableOnInteraction: !1
  }), $(window).width() <= 991 ? ($(banner).find('.swiper-button-prev, .swiper-button-next').hide(), $(banner).find('.swiper-pagination').show())  : ($(banner).find('.swiper-pagination').hide(), $(banner).find('.swiper-button-prev, .swiper-button-next').show()))  : (swiper[area] = new Swiper(banner, {
    slidesPerView: number,
    spaceBetween: '1' == spacing ? 15 : 0,
    simulateTouch: !1,
    shortSwipes: !1,
    longSwipes: !1,
    followFinger: !1,
    touchMoveStopPropagation: !1
  }), $(banner).find('.swiper-button-prev, .swiper-button-next, .swiper-pagination').hide())
}
var swiper = [
];
/*
$(function () {
  var JS_VARS = JSON.parse(shops);
  '' != JS_VARS.message && 'undefined' != typeof JS_VARS.message && $.alert({
    backgroundDismiss: !1,
    content: '<span class="content_confirm_inner">' + JS_VARS.message + '</span>',
    confirmButtonClass: 'btn_confirm btn_sm',
    cancelButtonClass: 'btn_cancel btn_sm',
    confirmButton: Lang.get('frontend.other.btn.success'),
    closeIcon: !1,
    icon: 'icon-checkmark',
    title: ' ',
    theme: 'waca_confirm',
    keyboardEnabled: !0
  }),
  $(window).resize(function () {
    for (var JS_VARS = JSON.parse(shops), s = 1; 3 >= s; s++) 'undefined' != typeof JS_VARS.sliders[s] && '1' == JS_VARS.sliders[s].status && setOwlCarousel(JS_VARS.is_fb, JS_VARS.is_backend, s, JS_VARS.sliders[s].slideshow, JS_VARS.sliders[s].spacing, JS_VARS.sliders[s].number)
  }).resize(),
  $('.js_matchheight').matchHeight();
  var $productList = $('#product-list'),
  $nomoreresults = $('#js_nomoreresults'),
  $noproductdata = $('#js_noproductdata'),
  $productLoading = $('#js_loading'),
  win = $(window),
  doc = $(document),
  scroll_status = !0,
  is_loading = !1,
  scrollTimer = null,
  getProductList = function () {
    return is_loading ? !1 : (is_loading = !0, $productLoading.show(), void $.ajax({
      url: JS_VARS.urls.productlist,
      type: 'POST',
      data: {
        offset: $productList.children().size(),
        type: JS_VARS.type,
        value: JS_VARS.value,
        hash: location.hash
      },
      success: function (data) {
        $productList.append(data);
        var offset = $productList.children().size();
        $productList.off('click', 'a'),
        $productList.on('click', 'a', function () {
          window.location.hash = offset + '&' + win.scrollTop()
        }),
        offset >= JS_VARS.products_total && (scroll_status = !1, $productLoading.remove(), $nomoreresults.show()),
        is_loading = !1
      }
    }))
  };
  0 == JS_VARS.products_total ? ($noproductdata.show(), $productList.addClass('hide'))  : JS_VARS.is_backend || $(window).scroll(function () {
    is_loading || (scrollTimer && clearTimeout(scrollTimer), scrollTimer = setTimeout(function () {
      scroll_status && !is_loading && win.scrollTop() >= doc.height() - 2.5 * win.height() && getProductList(),
      'none' != $nomoreresults.css('display') && win.scrollTop() >= doc.height() - win.height() - $nomoreresults.innerHeight() && $nomoreresults.delay(400).fadeOut()
    }, 10))
  });
  var popup = {
    showEdm: function () {
      $.confirm({
        content: '<div class="form_group js_popup_edm"><p class="edm_content js_edm_content">' + JS_VARS.shops_popup.edm_content + '</p><label class="form_label" for="">' + Lang.get('frontend.edm.title.email') + '</label><div class="form_data"><input id="purchaser_name" class="form_input form_control js_popup_edm_email" maxlength="100" placeholder="' + Lang.get('frontend.edm.title.email') + '" type="text" name="orders[purchaser_name]" value=""><span class="msg_error"><i class="icon-exclamation-triangle"></i></span></div><div class="help_block js_popup_help_block"><i class="icon-exclamation-circle"></i><span>' + Lang.get('frontend.errormsg.edm.email.format') + '</span></div></div>',
        confirmButtonClass: 'btn_confirm btn_sm',
        cancelButtonClass: 'btn_cancel btn_sm',
        confirmButton: Lang.get('frontend.edm.btn.popup.subscribe'),
        closeIcon: !0,
        title: Lang.get('frontend.edm.title.info.popup', {
          shops_name: JS_VARS.shops_name
        }),
        columnClass: 'col-md-6 col-md-offset-3',
        theme: 'waca_popup_edm',
        cancelButton: !1,
        keyboardEnabled: !0,
        confirm: function () {
          var edm_email = $.trim($('.js_popup_edm_email').val()),
          popup_edm_element = $('.js_popup_edm'),
          re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
          return '' == edm_email ? (popup_edm_element.addClass('form_valid'), !1)  : re.test(edm_email) ? (popup_edm_element.removeClass('form_valid'), void $.post(locale + '/wantedm', {
            edm_email: edm_email,
            is_popup: !0
          }, function (result) {
            $.dialog({
              animation: 'opacity',
              closeAnimation: 'none',
              animationSpeed: 1000,
              title: ' ',
              content: '<span class="content_dialog_inner">' + result.message + '</span>',
              icon: 'icon-checkmark',
              theme: 'waca_confirm',
              keyboardEnabled: !0,
              onOpen: function () {
                var that = this;
                setTimeout(function () {
                  that.close()
                }, 2000)
              }
            })
          }, 'json'))  : (popup_edm_element.addClass('form_valid'), !1)
        }
      }),
      this.setCookie()
    },
    showImg: function () {
      $.dialog({
        template: '<div class="jconfirm"><div class="jconfirm-bg"></div><div class="jconfirm-scrollpane"><div class="container"><div class="row"><div class="jconfirm-box-container"><div class="jconfirm-box" role="dialog" aria-labelledby="labelled" tabindex="-1"><div class="closeIcon"><span class="icon-cross"></span></div><div class="title clearfix"></div><div class="content"></div><div class="buttons"></div><div class="jquery-clear"></div></div></div></div></div></div></div>',
        columnClass: !1,
        closeIcon: !0,
        title: !1,
        content: '<img class="js_popup_image" style="width:500px;height:' + JS_VARS.shops_popup.img_height + 'px" src="' + JS_VARS.shops_popup.img_url + '">',
        theme: 'waca_popup_img',
        keyboardEnabled: !0,
        closeIconClass: !0,
        onOpen: function () {
          $('.js_popup_image').css('height', 'auto')
        }
      }),
      this.setCookie()
    },
    showPopup: function () {
      var now = (new Date).getTime(),
      is_showed = localStorage.getItem('is_showed_popup'),
      timeDiff = Math.abs(now - is_showed),
      diffDays = Math.ceil(timeDiff / 86400000);
      return 'undefined' == typeof JS_VARS.shops_popup || 1 != JS_VARS.shops_popup.status || '' != is_showed && 1 >= diffDays ? !1 : void ('img' == JS_VARS.shops_popup.type && '' != JS_VARS.shops_popup.img_url ? this.showImg()  : 'edm' == JS_VARS.shops_popup.type && this.showEdm())
    },
    setCookie: function () {
      var expected = (new Date).getTime();
      localStorage.setItem('is_showed_popup', expected)
    }
  };
  popup.showPopup()
}),
*/
$(window).load(function () {
  if ('' != location.hash && '#' != location.hash) {
    var hashData = location.hash.substr(1).split('&');
    window.location.hash = '',
    $('html, body').animate({
      scrollTop: hashData[1]
    }, 1000)
  }
}),
$(document).on('click', '.js_listing_cart_add', function (e) {
  e.preventDefault(),
  listingCart($(this).data('pid'))
});
var listingCart = function (product_id) {
  $.dialog({
    animation: 'opacity',
    closeAnimation: 'none',
    animationSpeed: 1000,
    title: !1,
    theme: 'waca_popup_cart',
    keyboardEnabled: !0,
    columnClass: 'col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2',
    errorMessage: '<div class="error_text">' + Lang.get('frontend.product.listingcart.ajaxError') + '</div>',
    content: function () {
      var self = this;
      return $.ajax({
        url: '/product/listingCart/' + product_id,
        dataType: 'text',
        method: 'get'
      }).done(function (response) {
        return 'error' === response ? (self.setContent(self.errorMessage), !1)  : (self.setContent(response), self.standard_id = self.$content.find('#standard_id').val(), self.standards_elem = self.$content.find('.js_product_standard'), self.stock_elem = self.$content.find('.js_select_change'), self.mobile_stock_elem = self.$content.find('.js_qty_value'), self.stock_bind_elem = self.$content.find('.js_select_bind'), self.addcart_elem = self.$content.find('.js_btn_addcart'), self.buy_elem = self.$content.find('.js_add_and_go_cart'), self.stock_value_elem = self.$content.find('.js_product_qty:visible'), self.updateStock = function (stock) {
          for (var stocks = stock.split('_'), min_stock = parseInt(stocks[0]), max_stock = parseInt(stocks[1]), option_html = '', i = min_stock; max_stock >= i; i++) option_html += '<option>' + i + '</option>';
          self.stock_elem.html(option_html),
          self.stock_bind_elem.text(min_stock),
          self.mobile_stock_elem.data('max', max_stock),
          self.mobile_stock_elem.data('min', min_stock),
          self.mobile_stock_elem.val(min_stock),
          self.mobile_stock_elem.prev().prop('disabled', !0),
          1 >= max_stock ? self.mobile_stock_elem.next().prop('disabled', !0)  : self.mobile_stock_elem.next().prop('disabled', !1)
        }, self.updateStandardId = function (sid) {
          self.standard_id = sid
        }, self.addCartItem = function (is_go_cart_btn) {
          var params = {
            product_id: product_id,
            qty: self.stock_value_elem.val(),
            standard_id: self.standard_id,
            all_add_prices_id: add_price_list,
            add_add_prices: {
            }
          };
          addCartItem(params, is_go_cart_btn, self)
        }, self.standards_elem.click(function () {
          self.updateStandardId($(this).attr('id').replace('radio_collection_', '')),
          self.updateStock($(this).data('product-stock'))
        }), self.addcart_elem.click(function () {
          self.addCartItem(!1)
        }), void self.buy_elem.click(function () {
          self.addCartItem(!0)
        }))
      }).fail(function () {
        self.setContent(self.errorMessage)
      })
    }
  })
},
addCartItem = function (params, is_go_cart_btn, parent) {
  var dialog_timeout;
  if ('1' == is_fb && is_go_cart_btn) var openWindow = window.open();
  setCartItem('add_cart_item_and_add_price_item_batch', params, function (data) {
    if ('undefined' != typeof data.add_to_cart_trace) {
      $('#addtocart').remove();
      var script = document.createElement('script');
      script.id = 'addtocart',
      script.innerHTML = data.add_to_cart_trace,
      document.body.appendChild(script)
    }
    'undefined' == typeof data.status || data.status ? is_go_cart_btn ? '1' == is_fb ? openWindow.location = locale + '/cart' : location.href = locale + '/cart' : ($.dialog({
      animation: 'opacity',
      closeAnimation: 'none',
      animationSpeed: 1000,
      title: ' ',
      content: '<span class="content_dialog_inner">' + Lang.get('frontend.product.addcart.success') + '</span>',
      icon: 'icon-checkmark',
      theme: 'waca_confirm',
      keyboardEnabled: !0,
      columnClass: 'col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2',
      onOpen: function () {
        var that = this;
        dialog_timeout = setTimeout(function () {
          that.close()
        }, 2000)
      },
      onClose: function () {
        clearTimeout(dialog_timeout),
        parent.close()
      }
    }), $('.js_cart_number').text(data.cart_number))  : $.dialog({
      animation: 'opacity',
      closeAnimation: 'none',
      closeIcon: !1,
      animationSpeed: 1000,
      title: ' ',
      content: '<span class="content_dialog_inner">' + data.message + '</span>',
      icon: 'icon-notice',
      theme: 'waca_confirm',
      keyboardEnabled: !0,
      columnClass: 'col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2',
      onOpen: function () {
        var that = this;
        dialog_timeout = setTimeout(function () {
          that.close()
        }, 5000)
      },
      onClose: function () {
        clearTimeout(dialog_timeout)
      }
    })
  })
};
