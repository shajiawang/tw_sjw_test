$(function () {
  function RedeemList(type) {
    var titleE = $('.js_list_title'),
    nopeClass = 'js_nope',
    tabE = $('.js_redeem_show[data-type=' + type + ']'),
    listE = $('.js_list_area[data-type=' + type + ']');
    this.show = function () {
      tabE.addClass('active'),
      listE.removeClass('hide'),
      listE.hasClass(nopeClass) ? titleE.addClass('hide')  : titleE.removeClass('hide')
    },
    this.hide = function () {
      tabE.removeClass('active'),
      listE.addClass('hide')
    }
  }
  var $tags = $('.member_tag_area'),
  $tag_li = $('.member_tag_area li');
  $tag_li.on('click', function () {
    $tag_li.removeClass('active'),
    $(this).addClass('active')
  });
  var active = $tags.find($('.active'));
  if (active.left) {
    var target = active.offset().left;
    $('html, body').width() < $tag_li.width() * $tag_li.length + 20 && $tags.animate({
      scrollLeft: target - 10 - $tag_li.width() / 2
    }, 800)
  }
  $tags.on('scroll', function () {
    $tag_li.width() * $tag_li.length - $tags.width();
    $(this).scrollLeft() > 0 ? $(this).parent().addClass('toLeft')  : $(this).parent().removeClass('toLeft')
  }),
  $(window).on('resize', function () {
    var $tab_wrap = $('.js_member_area').find('.member_tag_area'),
    $tab = $tab_wrap.find('li');
    $tab.length * $tab.width() > $tab_wrap.width() && $('.js_member_area').addClass('toRight')
  }),
  $('.js_open_list').on('click', function () {
    var i = $(this).attr('data-list');
    $('.js_opened_list[data-list=' + i + '] ').is(':hidden') ? ($(this).find('.icon-plus').attr('class', 'icon-minus'), $('.js_opened_list[data-list=' + i + ']').removeClass('hide'), $('html, body').width() <= 991 && $('.js_opened_transport[data-list=' + i + ']').removeClass('hidden-sm hidden-xs').hide().fadeIn())  : ($(this).find('.icon-minus').attr('class', 'icon-plus'), $('.js_opened_list[data-list=' + i + ']').addClass('hide'), $('.js_opened_transport[data-list=' + i + ']').addClass('hidden-sm hidden-xs').show(), $('html, body').width() <= 991 && $('.js_opened_transport[data-list=' + i + ']').fadeOut())
  }),
  $('.js_order_detail_query').click(function (e) {
    e.stopPropagation(),
    e.preventDefault();
    var form = $('#orderquery'),
    order_no = $(this).data('no');
    form.find('input[name="order_no"]').val(order_no),
    form.submit()
  });
  ({
    $type: '',
    $lists: {
    },
    $tabClass: '.js_redeem_show',
    setType: function (type) {
      return type !== this.$type ? (this.$type = type, !0)  : !1
    },
    show: function () {
      var self = this;
      $.each(this.$lists, function (type) {
        self.$type === type ? self.$lists[type].show()  : self.$lists[type].hide()
      })
    },
    initListsAndType: function () {
      var self = this;
      $.each($(this.$tabClass), function (_, value) {
        var type = $(value).data('type');
        self.$lists[type] = new RedeemList(type),
        $(value).hasClass('active') && self.setType(type)
      })
    },
    initClick: function () {
      var self = this;
      $(this.$tabClass).click(function () {
        var type = $(this).data('type');
        self.setType(type) && self.show()
      })
    },
    init: function () {
      this.initClick(),
      this.initListsAndType(),
      this.show()
    }
  }).init()
});
