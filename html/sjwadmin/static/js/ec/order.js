$(function () {
  var JS_VARS = JSON.parse(shops);
  '' != JS_VARS.message && $.alert({
    backgroundDismiss: !1,
    confirmButtonClass: 'btn_confirm btn_sm',
    title: ' ',
    content: '<span class="content_confirm_inner">' + JS_VARS.message + '</span>',
    confirmButton: Lang.get('frontend.other.btn.success'),
    icon: 'icon-notice',
    theme: 'waca_confirm',
    keyboardEnabled: !0
  });
  var orderErrorMessage = {
    IsRequired: Lang.get('frontend.errormsg.isRequired'),
    codeIsNumber: JS_VARS.is_inquiry ? Lang.get('frontend.errormsg.order.inquiry.order_no.format')  : Lang.get('frontend.errormsg.order.order.order_no.format'),
    vcodeIsNumber: Lang.get('frontend.errormsg.order.order.vcode.format'),
    phonetIsNumber: Lang.get('frontend.errormsg.order.order.phone.format'),
    addressFormat: Lang.get('frontend.errormsg.address.format'),
    nameFormat: Lang.get('frontend.errormsg.cart.name.format'),
    phoneFormat: Lang.get('frontend.errormsg.cart.phone.format1'),
    phoneIsRequired: Lang.get('frontend.errormsg.cart.phone.isRequired'),
    nameFormat: Lang.get('frontend.errormsg.cart.name.format'),
    zipFormat: Lang.get('frontend.errormsg.address.zip.format'),
    updateReceiver: Lang.get('errormsg.order.updateReceiver.error')
  },
  showErrorMessage = function (FieldName, ErrorName) {
    FieldName.addClass('form_valid'),
    FieldName.find('.js_order_message').find('span').html(ErrorName)
  },
  hideErrorMessage = function (FieldName) {
    FieldName.removeClass('form_valid')
  },
  noCheck = function () {
    var order_no = $.trim($('.js_order_no').val());
    return '' == order_no ? (showErrorMessage($('.js_order_no_group'), orderErrorMessage.IsRequired), !1)  : order_no.match(/^[A-Za-z0-9]+$/) ? (hideErrorMessage($('.js_order_no_group')), !0)  : (showErrorMessage($('.js_order_no_group'), orderErrorMessage.codeIsNumber), !1)
  },
  vcodeCheck = function () {
    var vcode = $.trim($('.js_order_validate_code').val());
    return '' == vcode ? (showErrorMessage($('.js_order_validate_code_group'), orderErrorMessage.IsRequired), !1)  : vcode.match(/^[A-Za-z0-9]+$/) ? (hideErrorMessage($('.js_order_validate_code_group')), !0)  : (showErrorMessage($('.js_order_validate_code_group'), orderErrorMessage.vcodeIsNumber), !1)
  },
  phonetCheck = function () {
    var phonet = $.trim($('.js_order_phone').val());
    return '' == phonet ? (showErrorMessage($('.js_order_phone_group'), orderErrorMessage.IsRequired), !1)  : (hideErrorMessage($('.js_order_phone_group')), !0)
  };
  $('.js_order_no').change(function () {
    noCheck()
  }),
  $('.js_order_validate_code').change(function () {
    vcodeCheck()
  }),
  $('.js_order_phone').change(function () {
    phonetCheck()
  }),
  $('.js_order_search').click(function () {
    var noCheckStatus = ($('.js_order_no').val(), $('.js_order_validate_code').val(), noCheck()),
    vcodeCheckStatus = vcodeCheck(),
    phonetCheckStatus = phonetCheck();
    return !!(noCheckStatus && vcodeCheckStatus || phonetCheckStatus)
  }),
  $('.js_back_index').click(function () {
    location.href = JS_VARS.urls.index
  }),
  $('.js_back_member').click(function () {
    location.href = JS_VARS.urls.member
  });
  var ATMCheck = function () {
    var ATM_name = $.trim($('.js_atm_name').val()),
    ATM_bank = $.trim($('.js_atm_bank').val()),
    ATM_account = $.trim($('.js_atm_account').val()),
    error = !1;
    return '' == ATM_name ? (showErrorMessage($('.js_atm_name_group'), orderErrorMessage.IsRequired), error = !0)  : hideErrorMessage($('.js_atm_name_group')),
    '' == ATM_bank ? (showErrorMessage($('.js_atm_bank_group'), orderErrorMessage.IsRequired), error = !0)  : hideErrorMessage($('.js_atm_bank_group')),
    '' == ATM_account ? (showErrorMessage($('.js_atm_account_group'), orderErrorMessage.IsRequired), error = !0)  : hideErrorMessage($('.js_atm_account_group')),
    !error
  };
  $('.js_atm_Fields').change(function () {
    ATMCheck()
  }),
  $('.js_ispay_button').click(function () {
    var ATM_name = $('.js_atm_name').val(),
    ATM_bank = $('.js_atm_bank').val(),
    ATM_account = $('.js_atm_account').val(),
    no = $('.no').val(),
    vcode = JS_VARS.vcode;
    if (ATMCheck()) {
      var ATM = {
        name: ATM_name,
        bank: ATM_bank,
        account: ATM_account
      };
      $.ajax({
        type: 'POST',
        url: JS_VARS.urls.orderATM,
        data: {
          ATM: ATM,
          no: no,
          vcode: vcode
        },
        success: function (data) {
          1 == data.ErrorCode ? $.alert({
            backgroundDismiss: !1,
            confirmButtonClass: 'btn_confirm btn_sm',
            title: ' ',
            content: '<span class="content_confirm_inner">' + Lang.get('frontend.order.ATM.toPending.info') + '</span>',
            confirmButton: Lang.get('frontend.other.btn.success'),
            icon: 'icon-notice',
            theme: 'waca_confirm',
            keyboardEnabled: !0,
            confirm: function () {
              location.reload()
            }
          })  : $.alert({
            backgroundDismiss: !1,
            confirmButtonClass: 'btn_confirm btn_sm',
            title: ' ',
            content: '<span class="content_confirm_inner">' + Lang.get('frontend.errormsg.ATM.toPending.error') + '</span>',
            confirmButton: Lang.get('frontend.other.btn.success'),
            icon: 'icon-notice',
            theme: 'waca_confirm',
            keyboardEnabled: !0,
            confirm: function () {
              location.reload()
            }
          })
        },
        dataType: 'json'
      })
    }
  }),
  $('.js_order_close').click(function () {
    return $.confirm({
      backgroundDismiss: !1,
      content: '<span class="content_confirm_inner">' + (JS_VARS.is_inquiry ? Lang.get('frontend.order.inquiry.toCancle.info')  : Lang.get('frontend.order.order.toCancle.info')) + '</span>',
      confirmButtonClass: 'btn_confirm btn_sm',
      cancelButtonClass: 'btn_cancel btn_sm',
      confirmButton: Lang.get('frontend.other.btn.success'),
      icon: 'icon-notice',
      title: ' ',
      theme: 'waca_confirm',
      cancelButton: Lang.get('frontend.other.btn.cancel'),
      keyboardEnabled: !0,
      confirm: function () {
        return $.ajax({
          type: 'POST',
          url: JS_VARS.urls.orderCancel,
          data: {
            no: $('.no').val(),
            vcode: JS_VARS.vcode
          },
          success: function (data) {
            $.alert({
              backgroundDismiss: !1,
              confirmButtonClass: 'btn_confirm btn_sm',
              title: ' ',
              content: '<span class="content_confirm_inner">' + data.message + '</span>',
              confirmButton: Lang.get('frontend.other.btn.success'),
              icon: 'icon-notice',
              theme: 'waca_confirm',
              keyboardEnabled: !0,
              confirm: function () {
                document.cookie = 'waca_status=cancel;',
                location.reload()
              }
            })
          },
          dataType: 'json'
        }),
        !0
      }
    }),
    !1
  }),
  $('#js-select-updated-store').click(function () {
    $('#allpayForm').submit()
  }),
  $('#js-save-updated-store').click(function () {
    return $.confirm({
      backgroundDismiss: !1,
      content: '<span class="content_confirm_inner">' + Lang.get('frontend.order.updateStore.check') + '</span>',
      confirmButtonClass: 'btn_confirm btn_sm',
      cancelButtonClass: 'btn_cancel btn_sm',
      confirmButton: Lang.get('frontend.other.btn.success'),
      icon: 'icon-notice',
      title: ' ',
      theme: 'waca_confirm',
      cancelButton: Lang.get('frontend.other.btn.cancel'),
      keyboardEnabled: !0,
      confirm: function () {
        return $.ajax({
          type: 'POST',
          url: JS_VARS.urls.changeStore,
          data: {
            no: $('.no').val(),
            vcode: JS_VARS.vcode,
            update_store_data: {
              update_store_id: $('input[name=\'update_store_id\']').val(),
              update_store_name: $('input[name=\'update_store_name\']').val(),
              update_store_address: $('input[name=\'update_store_address\']').val()
            }
          },
          success: function (data) {
            $.alert({
              backgroundDismiss: !1,
              confirmButtonClass: 'btn_confirm btn_sm',
              title: ' ',
              content: '<span class="content_confirm_inner">' + data.message + '</span>',
              confirmButton: Lang.get('frontend.other.btn.success'),
              icon: 'icon-notice',
              theme: 'waca_confirm',
              keyboardEnabled: !0,
              confirm: function () {
                location.reload()
              }
            })
          },
          dataType: 'json'
        }),
        !0
      }
    }),
    !1
  }),
  parseInt(JS_VARS.custom_unread_count) > 0 && $('.js_btn_ordersQa.js_btn_ordersQa_open').click(),
  $('#qa_button_submit').click(function (event) {
    if (event.preventDefault(), 'disabled' == $(this).attr('disabled')) return !1;
    $('#qa_button_submit').attr('disabled', 'disabled');
    var msg = $('#orders_qa_msg').val(),
    img = $('#js_qa_base64Img').val(),
    path = $('#js_qa_attachImg').val();
    return '' == msg && '' == img ? ($('#qa_button_submit').removeAttr('disabled'), alert_message('請先輸入詢問內容'), !1)  : void $.post(JS_VARS.urls.saveQaMsgAjax, {
      img: img,
      path: path,
      msg: msg,
      orders_id: JS_VARS.orders_id
    }, function (result) {
      $('#qa_button_submit').removeAttr('disabled'),
      result.status ? ($('.js_qa_nodata').hide(), $('.js_qa_record_ul').show(), $('.js_qa_record_ul').append(result.view), $('.qa_area').scrollTop($('.js_qa_record').height()), initPhotoSwipeFromDOM('.js_lightbox_gallery'))  : alert_message(result.errormsg),
      $('input#js_qa_attachImg').val('').clone(!0),
      $('#js_qa_base64Img').val(''),
      $('#orders_qa_msg').val(''),
      qa_attach_statu()
    }, 'json')
  });
  var receiverInfo = {
    init: function () {
      1 == JS_VARS.is_enable_change_address ? ($('.js_receiver_input').removeClass('hide'), $('.js_receiver_text').hide(), $('.js_change_address_submit').unbind().click(function () {
        receiverInfo.validateAndSubmit()
      }).text(Lang.get('frontend.order.receiverInfo.modify')), this.initTwzipcode(), this.recordMaskValues('address'))  : 1 == JS_VARS.is_enable_change_recipient && ($('.js_recipient_input').removeClass('hide'), $('.js_recipient_text').hide(), $('.js_change_address_submit').unbind().click(function () {
        receiverInfo.validateAndSubmit()
      }).text(Lang.get('frontend.order.receiverInfo.modify')), this.initTwzipcode(), this.recordMaskValues('recipient'))
    },
    initTwzipcode: function () {
      1 == JS_VARS.is_address_in_taiwan && ($('#js_twzipcode').twzipcode({
        detect: !1,
        zipcodeIntoDistrict: !0,
        countySel: JS_VARS.receiver_address.receiver_city,
        districtSel: JS_VARS.receiver_address.receiver_district,
        zipcodeSel: JS_VARS.receiver_address.receiver_zip,
        onCountySelect: function () {
          $('.js_receiver_district').change()
        }
      }), $('.js_twzipcode_wrapper').append('<span class="form_select_bind js_select_bind"></span>'), $('.js_twzipcode_wrapper').each(function () {
        var text = $(this).children('.js_select_bind').prev().find('option:selected').text();
        $(this).children('.js_select_bind').text(text)
      }))
    },
    getSumitData: function () {
      var data = {
        no: $('.no').val(),
        vcode: JS_VARS.vcode,
        is_holiday: $('#is_holiday_send').val()
      };
      1 == JS_VARS.is_international_address ? (data.receiver_district = $('#intReceiverDistrict').val(), data.receiver_city = $('#intReceiverCity').val(), data.receiver_zip = $.trim($('#intReceiverZip').val()), 231 == JS_VARS.shipping && (data.receiver_country = $.trim($('#intReceiverCountry').val())))  : (data.receiver_zip = $.trim($('.js_receiver_zip').val()), data.receiver_city = $('.js_receiver_city').val(), data.receiver_district = $('.js_receiver_district').val());
      for (var key in this.maskFields) this.isMaskChange(key) && ('intReceiverAddress' == key ? data.receiver_address = $.trim($('#' + key).val())  : data[key] = $.trim($('#' + key).val()));
      return 1 == JS_VARS.is_time_select && (data.shipping_time = $.trim($('#shipping_time').val())),
      1 == JS_VARS.is_date_select && (data.shipping_date = $.trim($('#shipping_date').val())),
      data
    },
    changeInfo: function () {
      var data = this.getSumitData();
      $.ajax({
        url: JS_VARS.urls.changeAddressAjax,
        dataType: 'json',
        type: 'post',
        data: data,
        success: function (data) {
          alert_message(data.message),
          location.reload()
        },
        error: function () {
          alert_message(orderErrorMessage.updateReceiver)
        }
      })
    },
    maskFields: {
    },
    setMaskFields: function (key, val) {
      this.maskFields[key] = val
    },
    recordMaskValues: function (type) {
      this.setMaskFields('receiver_name', $('#receiver_name').val()),
      this.setMaskFields('receiver_phone', $('#receiver_phone').val()),
      'address' == type && (1 == JS_VARS.is_international_address ? this.setMaskFields('intReceiverAddress', $('#intReceiverAddress').val())  : this.setMaskFields('receiver_address', $('#receiver_address').val()))
    },
    validateAndSubmit: function () {
      if (1 == JS_VARS.is_enable_change_address) {
        var is_receiver_name = this.checkReceiverName(),
        is_receiver_phone = this.checkReceiverPhone();
        if (1 == JS_VARS.is_international_address) var is_receiver_city = this.checkIntReceiverCity(),
        is_receiver_district = this.checkIntReceiverDistrict(),
        is_receiver_zip = this.checkIntReceiverZip(),
        is_receiver_address = this.checkIntReceiverAddress();
         else var is_receiver_city = 1 == JS_VARS.is_address_in_taiwan ? this.checkReceiverCity()  : !0,
        is_receiver_district = 1 == JS_VARS.is_address_in_taiwan ? this.checkReceiverDistrict()  : !0,
        is_receiver_zip = 1 == JS_VARS.is_address_in_taiwan ? !0 : this.checkReceiverZip(),
        is_receiver_address = this.checkReceiverAddress();
        is_receiver_name && is_receiver_phone && is_receiver_city && is_receiver_district && is_receiver_zip && is_receiver_address && this.changeInfo()
      } else if (1 == JS_VARS.is_enable_change_recipient) {
        var is_receiver_name = this.checkReceiverName(),
        is_receiver_phone = this.checkReceiverPhone();
        is_receiver_name && is_receiver_phone && this.changeInfo()
      }
    },
    checkReceiverName: function () {
      var name = $.trim($('#receiver_name').val());
      return !name.match(/^[\u0391-\uFFE5A-Za-z ]+$/) && this.isMaskChange('receiver_name') ? (showErrorMessage($('.js_receiver_name_group'), orderErrorMessage.nameFormat), !1)  : (hideErrorMessage($('.js_receiver_name_group')), !0)
    },
    checkReceiverPhone: function () {
      var phone = $.trim($('#receiver_phone').val());
      return this.checkPhoneByShipping() ? (!phone.match(/^[0-9+#]+$/) || phone.length > 20 || phone.length < 8) && this.isMaskChange('receiver_phone') ? (showErrorMessage($('.js_receiver_phone_group'), orderErrorMessage.phoneFormat), !1)  : (hideErrorMessage($('.js_receiver_phone_group')), !0)  : !1
    },
    checkReceiverCity: function () {
      var city = $('.js_receiver_city').val();
      return '' === city ? (showErrorMessage($('.js_receiver_city_group'), orderErrorMessage.IsRequired), !1)  : (hideErrorMessage($('.js_receiver_city_group')), !0)
    },
    checkReceiverDistrict: function () {
      var district = $('.js_receiver_district').val();
      return '' === district ? (showErrorMessage($('.js_receiver_district_group'), orderErrorMessage.IsRequired), !1)  : (hideErrorMessage($('.js_receiver_district_group')), !0)
    },
    checkReceiverZip: function () {
      var zip = $.trim($('.js_receiver_zip').val());
      return zip.match(/^[0-9-A-Za-z ]+$/) ? (hideErrorMessage($('.js_receiver_zip_group')), !0)  : (showErrorMessage($('.js_receiver_zip_group'), orderErrorMessage.zipFormat), !1)
    },
    checkReceiverAddress: function () {
      var address = $.trim($('#receiver_address').val());
      return '' === address ? (showErrorMessage($('.js_receiver_address_group'), orderErrorMessage.IsRequired), !1)  : !/^0|1|2|3|4|5|6|7|8|9|０|１|２|３|４|５|６|７|８|９|一|二|三|四|五|六|七|八|九|十$/.test(address) && receiverInfo.isMaskChange('receiver_address') ? (showErrorMessage($('.js_receiver_address_group'), orderErrorMessage.addressFormat), !1)  : (hideErrorMessage($('.js_receiver_address_group')), !0)
    },
    checkIntReceiverCity: function () {
      return !0
    },
    checkIntReceiverDistrict: function () {
      var district = $('#intReceiverDistrict').val();
      return '' === district ? (showErrorMessage($('.js_int_receiver_district_group'), orderErrorMessage.IsRequired), !1)  : (hideErrorMessage($('.js_int_receiver_district_group')), !0)
    },
    checkIntReceiverZip: function () {
      var zip = $.trim($('#intReceiverZip').val());
      return zip.match(/^[0-9-A-Za-z ]+$/) ? (hideErrorMessage($('.js_int_receiver_zip_group')), !0)  : (showErrorMessage($('.js_int_receiver_zip_group'), orderErrorMessage.zipFormat), !1)
    },
    checkIntReceiverAddress: function () {
      var address = $.trim($('#intReceiverAddress').val());
      return '' === address ? (showErrorMessage($('.js_int_receiver_address_group'), orderErrorMessage.IsRequired), !1)  : !/^0|1|2|3|4|5|6|7|8|9|０|１|２|３|４|５|６|７|８|９|一|二|三|四|五|六|七|八|九|十$/.test(address) && receiverInfo.isMaskChange('intReceiverAddress') ? (showErrorMessage($('.js_int_receiver_address_group'), orderErrorMessage.addressFormat), !1)  : (hideErrorMessage($('.js_int_receiver_address_group')), !0)
    },
    isMaskChange: function (id) {
      return receiverInfo.maskFields[id] !== $('#' + id).val()
    },
    checkPhoneByShipping: function () {
      var phone = $.trim($('#receiver_phone').val()),
      cellphone_re = /^09[0-9]{8}$/;
      return ( - 1 != $.inArray(JS_VARS.shipping, [
        '221',
        '222',
        '241',
        '242',
        '243',
        '244',
        '245',
        '246',
        '247',
        '131'
      ]) || '-1' != JS_VARS.shipping.indexOf('custom') && 1 == JS_VARS.is_address_in_taiwan) && !cellphone_re.test(phone) && receiverInfo.isMaskChange('receiver_phone') ? (showErrorMessage($('.js_receiver_phone_group'), orderErrorMessage.phoneIsRequired), !1)  : !0
    }
  };
  $('.js_change_address_submit').click(function () {
    receiverInfo.init()
  });
  var locale = 'en';
  'tw' == JS_VARS.getLocale ? locale = 'zh-tw' : 'cn' == JS_VARS.getLocale && (locale = 'zh-cn'),
  Date.prototype.yyyymmdd = function () {
    var mm = this.getMonth() + 1,
    dd = this.getDate();
    return [this.getFullYear(),
    (mm > 9 ? '' : '0') + mm,
    (dd > 9 ? '' : '0') + dd].join('-')
  };
  var tomorrow = new Date((new Date).getTime() + 86400000),
  tomorrow_date = tomorrow.yyyymmdd(),
  year = new Date((new Date).getTime() + 31536000000),
  year_date = year.yyyymmdd();
  $('#shipping_date').length > 0 && $('#shipping_date').datetimepicker({
    sideBySide: !0,
    useCurrent: !1,
    format: 'YYYY-MM-DD',
    ignoreReadonly: !0,
    minDate: tomorrow_date,
    maxDate: year_date,
    locale: locale,
    daysOfWeekDisabled: [
      0,
      6
    ]
  })
});
