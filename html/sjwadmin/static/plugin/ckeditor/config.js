/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */
var current_url = location.hostname;
var ckfinder_url = 'http://'+ location.hostname + '/tlback/static/plugin/ckfinder/';
// // console.log(ckfinder_url)
/*
if ('payment.top-link.com.tw'.indexOf(current_url) > -1 ) {
    var ckfinder_url = 'http://payment.top-link.com.tw/admin/static/plugin/ckfinder/';
}
*/

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
    //
    config.language = 'zh';

    config.toolbar = [
        { name: 'document', groups: [ 'mode', 'document', 'doctools' ], items: [ 'Source', '-', 'NewPage', 'Preview', 'Print', '-', 'Templates' ] },
        { name: 'clipboard', groups: [ 'clipboard', 'undo' ], items: [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ] },
        { name: 'editing', groups: [ 'find', 'selection', 'spellchecker' ], items: [ 'Find', 'Replace', '-', 'SelectAll', '-', 'Scayt' ] },
        // { name: 'forms', items: [ 'Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button', 'ImageButton', 'HiddenField' ] },
        { name: 'links', items: [ 'Link', 'Unlink', 'Anchor' ] },
        '/',
        { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ], items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ] },
        { name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl', 'Language' ] },
        { name: 'insert', items: [ 'Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe' ] },
        '/',
        { name: 'styles', items: [ 'Styles', 'Format', 'Font', 'FontSize' ] },
        { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
        { name: 'tools', items: [ 'Maximize', 'ShowBlocks' ] },
        { name: 'others', items: [ '-' ] },
        { name: 'about', items: [ 'About' ] }
    ];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

    // Close syntax filter
    config.allowedContent=true;

    // remove start <p>
    config.enterMode = CKEDITOR.ENTER_BR;
    config.shiftEnterMode = CKEDITOR.ENTER_P;


	config.filebrowserBrowseUrl = ckfinder_url + 'ckfinder.html?langCode=zh-tw';
	config.filebrowserImageBrowseUrl = ckfinder_url + 'ckfinder.html?Type=Images&langCode=zh-tw';
	config.filebrowserFlashBrowseUrl = ckfinder_url + 'ckfinder.html?Type=Flash&langCode=zh-tw';
	// config.filebrowserUploadUrl = ckfinder_url + 'core/connector/php/connector.php?command=QuickUpload&type=Files&langCode=zh-tw';
	// config.filebrowserImageUploadUrl = ckfinder_url + 'core/connector/php/connector.php?command=QuickUpload&type=Images&langCode=zh-tw';
	// config.filebrowserFlashUploadUrl = ckfinder_url + 'core/connector/php/connector.php?command=QuickUpload&type=Flash&langCode=zh-tw';
};
