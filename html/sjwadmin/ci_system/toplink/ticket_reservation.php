<?php
class ticket_reservation
{
	public $CI;
	public $act_id;
	public $kid;
	public $current_y;
	public $income;
	public $model;
	public $user;
	public $type_array;
	public $aid_array;
	public $sum_fair;
	protected $sess_id;

	public function __construct()
	{
		$this->CI = &get_instance();

		$this->response = new stdClass();
		$this->response->error = null;
		$this->response->back = null;

		$this->model = new stdClass();
		$this->model->member = '`alpha_user`.`member`';
		$this->model->activity_sign = '`liucms`.`lc_activity_sign`';
		$this->model->activity_info = '`liucms`.`lc_activity_info`';
		$this->model->activity_web = '`liucms`.`lc_activity_web`';

		$this->sess_id = session_id();
		//$get_ip = $this->CI->input->ip_address();
	}

	//活動預約 執行起點
	public function exec()
	{
		if(empty($this->act_id) || empty($this->kid) || empty($this->user) ){
			return false;
		}

		//索票規則
		$rule = $this->ticket_rule();

		if(! empty($rule->error) ){
			return $rule->error;
		}

		//預約索票寫入DB
		$this->ticket_sign();

		//其他處理
		//$this->other_proc();

		return true;
	}


	/////////////////////////////////////////////////
	//索票規則
	protected function ticket_rule()
	{
		//判斷是否活動期間	liucms.lc_activity_info 判斷有相符之aid及kid且於活動期間
		$data = $this->check_type($this->act_id, $this->kid);
		if(empty($data) )
		{ //count($data) <= 0
			$this->response->error = array(
					'code' => 501
					, 'message' => '活動未開放 !'
			);

			return $this->response;
		}

		//檢查是否已索過票 liucms.lc_activity_sign
		$is_sign = $this->select_activity_sign();

		////////////////////////////////////
		//已索取
		if(! empty($is_sign))
		{
			/*
			$aidname = $this->check_activity_sign();
			if(!empty($aidname) && !empty($aidname->webname) )
			{
				if($this->kid==1 || $this->kid==2){
					$message = "此帳號已索取過【{$aidname->webname}】門票 !";
				}
			}
			*/

			if( ($this->kid==1 || $this->kid==2) && $this->sum_fair=='TOTAL'){
				//多展擇一
				$this->response->error = array(
					'code' => 504
					, 'message' => $is_sign->aid
				);
			} else {
				$this->response->error = array(
					'code' => 502
					, 'message' => 'Already'
				);
			}

			return $this->response;
		}


		////////////////////////////////////
		//尚未索取

		//可索票數 //'0' 為無上限
		if( ($this->kid==1 || $this->kid==2) && $this->sum_fair=='TOTAL'){
			//多展擇一
			$ticket_limit = $this->ticket_limit_amount();
		} else {
			//單展
			$ticket_limit = (int)$data->amount;
		}

		//檢查當日索票是否已超過上限
		if( ($this->kid==1 || $this->kid==2) && $this->sum_fair=='TOTAL'){
			//多展擇一
			$total = $this->total_ticket_limit();
		} else {
			//單展
			$total = $this->self_ticket_limit();
		}

		if($ticket_limit > 0 && ($ticket_limit <= $total) )
		{
			$this->response->error = array(
					'code' => 503
					, 'message' => '已超過上限 !'
			);

			return $this->response;
		}

		//已索票人數未超過上限才寫DB
		if($ticket_limit == 0 || ($ticket_limit > 0 && $ticket_limit > $total) ){
			return $this->response->back = true;
		}
	}

	//單展票數計算 $sum_fair:SELF
	protected function self_ticket_limit()
	{
		$type_str = ( is_array($this->type_array) ) ? implode(",", $this->type_array) : $this->type_array;

		$sql = "SELECT count(*) total FROM {$this->model->activity_sign} WHERE `aid` IN(%s) AND `type` IN(%s);";

		if(count($this->type_array) > 1)
		{ //多個活動
			$sql = sprintf($sql, $this->act_id, $type_str);
		}
		else
		{	//單一活動
			$sql = sprintf($sql, $this->act_id, $this->kid);
		}

		$rs = $this->get_query($sql);

		//索票數, 0為無上限
		if(! empty($rs) ){
			$_ticket_limit = (int)$rs[0]->total;
		} else {
			$_ticket_limit = 0;
		}

		return $_ticket_limit;
	}

	//多展票數合併計算 $sum_fair:TOTAL
	protected function total_ticket_limit()
	{
		$type_str = ( is_array($this->type_array) ) ? implode(",", $this->type_array) : $this->type_array;
		$aid_str = ( is_array($this->aid_array) ) ? implode(",", $this->aid_array) : $this->aid_array;

		$sql = "SELECT count(*) total FROM {$this->model->activity_sign} WHERE `aid` IN(%s) AND `type` IN(%s);";

		if(count($this->type_array) > 1)
		{ //多個活動
			$sql = sprintf($sql, $aid_str, $type_str);
		}
		else
		{	//單一活動
			$sql = sprintf($sql, $aid_str, $this->kid);
		}

		$rs = $this->get_query($sql);

		//索票數, 0為無上限
		if(! empty($rs) ){
			$_ticket_limit = (int)$rs[0]->total;
		} else {
			$_ticket_limit = 0;
		}

		return $_ticket_limit;
	}

	//索票數計算方式 $sum_fair(多展擇一與他展合併計算:TOTAL / 獨立一展計算:SELF)
	protected function ticket_limit_amount()
	{
		$type_str = ( is_array($this->type_array) ) ? implode(",", $this->type_array) : $this->type_array;
		$aid_str = ( is_array($this->aid_array) ) ? implode(",", $this->aid_array) : $this->aid_array;

		$sql ="SELECT SUM(amount) amount FROM {$this->model->activity_info} WHERE `aid` IN(%s) AND `kid` IN(%s)";
		$sql = sprintf($sql, $aid_str, $type_str);
		$rs = $this->get_query($sql);

		//索票數, 0為無上限
		if(!empty($rs)){
			$_ticket_limit = (int)$rs[0]->amount;
		} else {
			$_ticket_limit = 0;
		}

		return $_ticket_limit;
	}


	/////////////////////////////////////////////////
	// 預約活動 Function
	protected function ticket_sign()
	{
		//查詢預約
		$sql = "SELECT * FROM {$this->model->activity_sign} WHERE `email`='{$this->user['email']}' AND `aid`='{$this->act_id}' AND `type`='{$this->kid}';";
		$activity = $this->get_query($sql);

		//寫入預約
		if(! empty($activity) ){
			$sign['status'] = 0;
			$this->update_activity_sign($sign['status']);
		} else {
			$sign['insert_datetime'] = '';
			$sign['checkin_datetime'] = '';
			$sign['oeya_id'] = isset($cookie_array[0]) ? $cookie_array[0] : '';
			$sign['status'] = 0;
			$this->insert_activity_sign($sign);
		}

		return true;
	}
	protected function check_activity_sign()
	{
		$sql = "SELECT s.*, w.webname FROM {$this->model->activity_sign} as s
			LEFT JOIN {$this->model->activity_web} as w on (s.`aid`=w.`id`)
			WHERE 1 AND `aid`='%d' AND `type`='%d' AND `email`='%s';";
		$sql = sprintf($sql, $this->act_id, $this->kid, $this->user['email']);
		$data = $this->get_query($sql);
		$back = (empty($data)) ? null : $data[0];

		return $back;
	}
	protected function select_activity_sign()
	{
		$type_str = ( is_array($this->type_array) ) ? implode(",", $this->type_array) : $this->kid;
		$aid_str = ( is_array($this->aid_array) ) ? implode(",", $this->aid_array) : $this->act_id;

		$sql = "SELECT * FROM {$this->model->activity_sign} WHERE 1
					AND `aid` IN (%s)
					AND `type` IN (%s)
					AND `email`='%s';";
		$sql = sprintf($sql, $aid_str, $type_str, $this->user['email']);
		$data = $this->get_query($sql);
		$back = (empty($data)) ? null : $data[0];

		return $back;
	}
	protected function check_type($aid, $type)
	{
		$type_str = ( is_array($type) ) ? implode(",", $type) : $type;
		$aid_str = ( is_array($aid) ) ? implode(",", $aid) : $aid;

		$sql = "SELECT * FROM {$this->model->activity_info} WHERE
			unix_timestamp( NOW() ) BETWEEN `start_time` AND `end_time`
			AND `aid` IN (%s)
			AND `kid` IN (%s)
			AND `exits`=1;";
		$sql = sprintf($sql, $aid_str, $type_str);
		$data = $this->get_query($sql);
		$back = (empty($data)) ? null : $data[0];

		return $back;
	}
	protected function update_activity_sign($status=0 )
	{
		$sql = "UPDATE {$this->model->activity_sign} SET `status`='%d' ";
		if(isset($this->user['cname']) && !empty($this->user['cname']) ){ $sql .= ", `cname`='{$this->user['cname']}', "; }
		if(isset($this->user['cid']) && !empty($this->user['cid']) && $this->user['cid'] !='000000'){ $sql .= ", `cid`='{$this->user['cid']}', "; }
		if(isset($this->user['ctel']) && !empty($this->user['ctel']) && $this->user['ctel'] !='0000000000'){ $sql .= ", `ctel`='{$this->user['ctel']}' ,"; }
		$sql .= "WHERE `aid`='{$this->act_id}' AND `type`='{$this->kid}' AND `tl_member_id`='{$this->user['mid']}'; ";

		$this->set_query($sql);

		return true;
	}
	protected function insert_activity_sign($sign=array() )
	{
		if($sign['insert_datetime']==''){
			$insert_datetime = date('Y-m-d H:i:s');
		}
		if($sign['checkin_datetime']==''){
			$checkin_datetime = '0000-00-00 00:00:00';
		}
		if($sign['oeya_id']==''){
			$oeya_id = '0';
		}

		$sql = "INSERT INTO {$this->model->activity_sign} (`id`, `aid`, `type`, `cname`, `cid`, `email`, `ctel`, `insert_datetime`, `checkin_datetime`, `approach`, `oeya_id`, `status`,`tl_member_id`)
				VALUES (NULL, '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%d','%d');";
		$sql = sprintf($sql, $this->act_id, $this->kid, $this->user['cname'], $this->user['cid'], $this->user['email'], $this->user['ctel'], $insert_datetime, $checkin_datetime, $this->income, $oeya_id, $sign['status'], $this->user['mid']);
		$this->set_query($sql);

		return true;
	}

	//其他處理
	protected function other_proc()
	{
		//回寫 TEMP_預約活動
		$sql = "UPDATE `liucms`.`lc_activity_temp` SET `tl_member_id`='{$mem_id}' WHERE `sesskey`='{$this->sess_id}' AND `type`='{$even_id}' ";
		//$this->CI->member_model->set_query($sql);
	}


	/////////////////////////////////////////////////
	// 公用 DB Function
	public function get_query($sql, $as_array = false)
	{
		if($sql){
			$query = $this->CI->db->query($sql);

			if ($query->num_rows() > 0){
				if($as_array){
					return $query->result_array();
				} else {
					return $query->result();
				}
			}
		} else {
			return array();
		}
	}
	public function set_query($sql)
	{
		if($sql){
			$query = $this->CI->db->query($sql);
		}
	}

}
