<?php
class Shared_Session
{
	function __construct(){
		ini_set("session.gc_maxlifetime", 3600);
		ini_set("session.use_cookies", 1);
		ini_set("session.cookie_path", "/");
		ini_set("session.cookie_domain", ".top-link.com.tw");
		
		$this->CI = &get_instance();
	 
		//session_module_name("User");
		session_set_save_handler(
			array("Shared_Session", "open"),
			array("Shared_Session", "close"),
			array("Shared_Session", "read"),
			array("Shared_Session", "write"),
			array("Shared_Session", "destroy"),
			array("Shared_Session", "gc")
			);
	}
 
	function open($save_path, $session_name){
		return true;
	}
 
	function close(){
		return true;
	}
	
	function mem_metadata($sesskey){
		if(!isset($this->CI->cache)){
			$this->CI->load->driver('cache');
		}
		return $this->CI->cache->memcached->get_metadata($sesskey);
	}
	
	function mem_stats(){
		if(!isset($this->CI->cache)){
			$this->CI->load->driver('cache');
		}
		return $this->CI->cache->memcached->cache_info();
	}
 
	function read($sesskey){
		if(!isset($this->CI->cache)){
			$this->CI->load->driver('cache');
		}
		$get_sesskey = $this->CI->cache->memcached->get($sesskey);
		return $get_sesskey;
	}
 
	function write($sesskey, $data, $lifetime = 3600){
		if(!isset($this->CI->cache)){
			$this->CI->load->driver('cache');
		}
		$this->CI->cache->memcached->save($sesskey, $data, $lifetime);
		return true;
	}
 
	function delete($sesskey){
		if(!isset($this->CI->cache)){
			$this->CI->load->driver('cache');
		}
		$this->CI->cache->memcached->delete($sesskey);
		return true;
	}
	
	function destroy($sesskey){
		if(!isset($this->CI->cache)){
			$this->CI->load->driver('cache');
		}
		$this->CI->cache->memcached->delete($sesskey);
		$this->CI->cache->memcached->clean();
		return true;
	}
 
	function gc($maxlifetime = null){
		return true;
	}
}
