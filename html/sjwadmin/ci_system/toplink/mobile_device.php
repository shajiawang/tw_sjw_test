<?php
//Detect special conditions devices
function mobile_device(){
	$iPod = stripos($_SERVER['HTTP_USER_AGENT'],"iPod");
	$iPhone = stripos($_SERVER['HTTP_USER_AGENT'],"iPhone");
	$iPad = stripos($_SERVER['HTTP_USER_AGENT'],"iPad");
	if(stripos($_SERVER['HTTP_USER_AGENT'],"Android") && stripos($_SERVER['HTTP_USER_AGENT'],"mobile")){
			$Android = true;
	}else if(stripos($_SERVER['HTTP_USER_AGENT'],"Android")){
			$Android = false;
			$AndroidTablet = true;
	}else{
			$Android = false;
			$AndroidTablet = false;
	}
	$webOS = stripos($_SERVER['HTTP_USER_AGENT'],"webOS");
	$BlackBerry = stripos($_SERVER['HTTP_USER_AGENT'],"BlackBerry");
	$RimTablet= stripos($_SERVER['HTTP_USER_AGENT'],"RIM Tablet");


	//do something with this information
	if( $iPod || $iPhone ){
		$isMobile = true; //were an iPhone/iPod touch
	}else if($iPad){
		$isMobile = true; //were an iPad
	}else if($Android){
		$isMobile = true; //we're an Android Phone
	}else if($AndroidTablet){
		$isMobile = true; //we're an Android Phone
	}else if($webOS){
		$isMobile = true; //we're a webOS device
	}else if($BlackBerry){
		$isMobile = true; //we're a BlackBerry phone
	}else if($RimTablet){
		$isMobile = true; //we're a RIM/BlackBerry Tablet
	}else if(isset($_SERVER['HTTP_X_WAP_PROFILE'])){
		$isMobile = true; //"移动设备";
	}else if(isset($_SERVER['HTTP_VIA'])){
		//"移动设备";
		$isMobile = (stristr($_SERVER['HTTP_VIA'], "wap")) ? true : false;
	} else {
		//it`s not a mobile device.
		$isMobile = false;
	}
	return $isMobile;
}

