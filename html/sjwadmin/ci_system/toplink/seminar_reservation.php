<?php

class seminar_reservation
{
	public $CI;
	public $act_id;
	protected $sess_id;
	protected $user;

	public function __construct() 
	{
		$this->CI = &get_instance();
		$this->CI->load->model('member_model');
		
		$this->sess_id = session_id();
		$get_ip = $this->CI->input->ip_address();
	}

	//執行起點
	public function exec($type=1, $user, $act_id, $kid = array(0=>array('id'=>1)) )
	{
		$this->act_id = $act_id;
		$this->kid = $kid;
		$this->user = new stdClass();
		
		// 用戶資料
		if($type==2)
		{ //企業戶 combine_register
			$this->user->mem_id = $user->id;
			$this->user->mem_email = $user->contractor_email;
			$this->user->mem_name = isset($user->contractor) ? $user->contractor : $user->comp_name;
			$this->user->mem_cid = isset($user->account) ? $user->account : '';
			$this->user->mem_phone = $user->comp_tel;
		} 
		else 
		{ //網路會員 member
			$this->user = $user;
		}
		
		// 活動預約處理
		if(!empty($this->act_id) && !empty($this->kid) )
		{
			//活動代碼: 0:無, -2:雙週抽, 1:索票, 其他>0 都是展覽活動
			$kid_key = key($this->kid);
			
			if($kid_key==0)
			{ //免費索票
				$this->ticket_sign(); 
				
				//寄發通知
				//$this->send_email();
			} 
			elseif($kid_key==1)
			{ //一般活動
				$this->active_sign(); 
				
			} 
			elseif($kid_key==2)
			{ //預約課程
				$this->class_sign();
			}
			
			//最終處理
			$this->final_proc();
		}
	}
	
	/////////////////////////////////////////////////
	//預約免費索票
	protected function ticket_sign()
	{
		$even_id = $this->kid[0]['id'];
		$income = isset($_SESSION['income']) ? $_SESSION['income'] : 0;
		$m = $this->user;
		
		if($even_id=='1')
		{
			//其他處理
			//$this->other_proc();
			
			//查詢預約
			$sql = "SELECT * FROM `liucms`.`lc_activity_sign` WHERE `tl_member_id`='{$m->mem_id}' AND `aid`='{$this->act_id}' AND `type`='{$even_id}' ORDER BY id DESC";
			$activity = $this->CI->member_model->get_query($sql);
			
			if($this->ticket_rule() )
			{
				//寫入預約
				if(!empty($activity) ){
					$sql = "UPDATE `liucms`.`lc_activity_sign` SET ";
					$sql .= "`email`='{$m->mem_email}', `cname`='{$m->mem_name}', `cid`='{$m->mem_cid}', `ctel`='{$m->mem_phone}' ";
					$sql .= "WHERE `aid`='{$this->act_id}' AND `type`='{$even_id}' AND `tl_member_id`='{$m->mem_id}' ";
				} else {
					$sql = "INSERT INTO `liucms`.`lc_activity_sign` SET ";
					$sql .= "`tl_member_id`='{$m->mem_id}',`email`='{$m->mem_email}', `cname`='{$m->mem_name}', `cid`='{$m->mem_cid}', `ctel`='{$m->mem_phone}' ";
					$sql .= ",`aid`='{$this->act_id}',`type`='{$even_id}', `approach`='{$income}',`insert_datetime`='". date('Y-m-d H:i:d') ."' ";
				}
				$this->CI->member_model->set_query($sql);
			}
		}
	}
	
	/////////////////////////////////////////////////
	//預約課程
	protected function class_sign()
	{
		$even_id = $this->kid[2]['id'];
		$class_id = $this->kid[2]['class_id'];
		$income = isset($_SESSION['income']) ? $_SESSION['income'] : 0;
		$m = $this->user;
		
		//其他處理
		//$this->other_proc();
		
		//課程class_detail
		$sql0 = "SELECT * FROM `liucms`.`class_detail` WHERE `aid`='{$this->act_id}' AND `kid`='{$even_id}' ";
		$class_info = $this->CI->member_model->get_query($sql0);
		if($class_info)
		foreach($class_info as $v){
			$_class[$v->id]['room'] = $v->room;
			$_class[$v->id]['date'] = $v->date;
			$_class[$v->id]['class'] = $v->class;
			$_class[$v->id]['class_time'] = $v->class_time;
			$_class[$v->id]['class_name'] = $v->class_name;
		}
		
		//查詢預約
		$sql = "SELECT * FROM `liucms`.`class_booking` WHERE 
			`member_seq`='{$m->mem_id}' AND `aid`='{$this->act_id}' AND `kid`='{$even_id}' AND `class_id`='{$class_id}'
			ORDER BY seq DESC";
		$activity = $this->CI->member_model->get_query($sql);
		
		//寫入預約
		if(!empty($activity) )
		{
			$sql = "UPDATE `liucms`.`class_booking` SET ";
			$sql .= "`cname`='{$m->mem_name}', `ctell`='{$m->mem_phone}', `cemail`='{$m->mem_email}' ,`check_date`='". date('Y-m-d H:i:d') ."', ";
			$sql .= "`room`='{$_class[$class_id]['room']}', `date`='{$_class[$class_id]['date']}', `class`='{$_class[$class_id]['class']}', `class_time`='{$_class[$class_id]['class_time']}', `class_name`='{$_class[$class_id]['class_name']}' ";
			$sql .= "WHERE `member_seq`='{$m->mem_id}' AND `aid`='{$this->act_id}' AND `kid`='{$even_id}' AND `class_id`='{$class_id}' ";
			$this->CI->member_model->set_query($sql);
			
			$sql = "UPDATE `liucms`.`lc_activity_sign` SET ";
			$sql .= "`email`='{$m->mem_email}', `cname`='{$m->mem_name}', `cid`='{$m->mem_cid}', `ctel`='{$m->mem_phone}' ";
			$sql .= "WHERE `aid`='{$this->act_id}' AND `type`='{$even_id}' AND `tl_member_id`='{$m->mem_id}' AND `class_id_no`='{$class_id}'";
			$this->CI->member_model->set_query($sql);
		} 
		else 
		{
			$sql_booking = "INSERT INTO `liucms`.`class_booking` SET ";
			$sql_booking.= "`aid`='{$this->act_id}', `kid`='{$even_id}', `class_id`='{$class_id}', `cname`='{$m->mem_name}', `ctell`='{$m->mem_phone}', `cemail`='{$m->mem_email}', `check_date`='". date('Y-m-d H:i:s') ."'";
			$sql_booking.= ",`room`='{$_class[$class_id]['room']}', `date`='{$_class[$class_id]['date']}', `class`='{$_class[$class_id]['class']}', `class_time`='{$_class[$class_id]['class_time']}', `class_name`='{$_class[$class_id]['class_name']}', `member_seq`='{$m->mem_id}', `approach`='{$income}' ";
			$booking_no = $this->CI->member_model->set_query($sql_booking);
			
			$sql = "INSERT INTO `liucms`.`lc_activity_sign` SET `class_id`='{$booking_no}', `class_id_no`='{$class_id}' ";
			$sql .= ",`tl_member_id`='{$m->mem_id}',`email`='{$m->mem_email}', `cname`='{$m->mem_name}', `cid`='{$m->mem_cid}', `ctel`='{$m->mem_phone}' ";
			$sql .= ",`aid`='{$this->act_id}',`type`='{$even_id}', `approach`='{$income}',`insert_datetime`='". date('Y-m-d H:i:d') ."' ";
			$this->CI->member_model->set_query($sql);
		}
	}
	
	/////////////////////////////////////////////////
	//預約一般活動
	protected function active_sign()
	{
		
	}
	
	//索票規則
	protected function ticket_rule()
	{
		return true;
	}
	
	//其他處理
	protected function other_proc()
	{
		//回寫 TEMP_預約活動
		$sql = "UPDATE `liucms`.`lc_activity_temp` SET `tl_member_id`='{$mem_id}' WHERE `sesskey`='{$this->sess_id}' AND `type`='{$even_id}' ";
		//$this->CI->member_model->set_query($sql);
	}
	
	//最終處理
	protected function final_proc()
	{
		return true;
		/*
				$ticket_url = '';
				
				//索票完成
				$sql = "SELECT `ticket_url` FROM `liucms`.`lc_activity_web` WHERE `id`='{$data['aid']}' ";
				$get_ticket_url = $this->CI->member_model->get_query($sql);
				
				if(!empty($get_ticket_url) ){ 
					$ticket_url = $get_ticket_url[0]->ticket_url;
					
					if(in_array($data['aid'], $activity_web) ){
						if($data['aid']=='456'){
							$ticket_url .= '/online_promotion/event2_ok/'; //TTE 索票完成 http://www.top-link.com.tw/web/2016/456/ticket_ok.php
						} else {
							$ticket_url .= '/ticket_ok/';
						}
					} else {
						$ticket_url .= '/ticket_ok.php';
					}
					
					$ticket_url .= '/?mid='. $member->id;
				}
			
			*/
			
	}
	
	//寄發通知
	protected function send_email()
	{
		return true;
		
		/*
		//寄出確認信		
		$data['email'] = $this->user['email'];
		$data['decoded_pass'] = $this->password_decode($this->user['password'], $data['email']);
		
		$mail_subject = "【展覽參觀會員】註冊成功通知信 ". date("Y-m-d H:m:s");
		$mail_body = $this->load->view('users/mail_signup', $data, true);
		
		$this->load->library('email');
		$this->email->initialize($this->config->item('email_set') );
		$this->email->from($this->config->item('service_mail'), $this->config->item('service_mail_name'));
		$this->email->to($data['email']);
		$this->email->subject($mail_subject);
		$this->email->message($mail_body);
		$this->email->send(); */
	}
	
}
