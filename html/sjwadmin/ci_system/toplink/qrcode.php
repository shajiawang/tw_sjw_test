<?php
class Qrcode
{
	protected $qr_size;
	protected $qr_logo;
	
	public function __construct()
	{
		$this->CI = &get_instance();
		
		//$this->qr_data = isset($_GET['data']) ? 'http://'.$_SERVER['HTTP_HOST'].'/web/2016/453/business_card_ok.php?bid='.$_GET['data'] : SITE_URL;
		$this->qr_size = '200x200'; //isset($_GET['size']) ? $_GET['size'] : '200x200';
		$this->qr_logo = ''; //isset($_GET['logo']) ? $_GET['logo'] : 'img/tl_logo.jpg';
	}
	
	//General qrcode
	public function qrcode_general($qr_data, $save_file_path, $qr_logo=null ,$qr_size=null)
	{
		//QRcode water image
		$filename = "qr_". time() . rand(0,9999) .".png";
		
		$qr_size = (!empty($qr_size)) ? $qr_size : $this->qr_size;
		$qr_logo = (!empty($qr_logo)) ? $qr_logo : $this->qr_logo;
		
		if(empty($qr_data)){
			return false;
		}
		
		//header('Content-type: image/png');
		// Get QR Code image from Google Chart API
		// http://code.google.com/apis/chart/infographics/docs/qr_codes.html
		$QR = imagecreatefrompng('https://chart.googleapis.com/chart?cht=qr&chld=H|1&chs='. $qr_size .'&chl='.urlencode($qr_data));
		if(!empty($qr_logo))
		{
			$qr_logo = imagecreatefromstring(file_get_contents($qr_logo));
			$QR_width = imagesx($QR);
			$QR_height = imagesy($QR);
		
			$logo_width = imagesx($qr_logo);
			$logo_height = imagesy($qr_logo);
		
			// Scale logo to fit in the QR Code
			$logo_qr_width = $QR_width/3;
			$scale = $logo_width / $logo_qr_width;
			$logo_qr_height = $logo_height / $scale;
		
			imagecopyresampled($QR, $qr_logo, $QR_width/3, $QR_height/3, 0, 0, $logo_qr_width, $logo_qr_height, $logo_width, $logo_height);
			//imagesavealpha($QR, true);
		}
		
		//存檔
		$save_files = UPLOAD_PATH . $save_file_path; // "/uploadfiles/453/up_img4/water/". $filename;
		if(!file_exists($save_files)) {
			mkdir($save_files, 0755, true);
		}
		imagepng($QR, $save_files . $filename); //imagejpeg($QR,$save_files,"100");
		imagedestroy($QR);
		
		//本機測試
		//$filepath = "../../../uploadfiles/453/up_img4/water/".$filename;
		//正式機 //已上傳圖檔
		//$filepath = UPLOAD_URL . $save_file_path; //"http://www.top-link.com.tw/". $save_path . $filename; 
		return $save_file_path . $filename;
	}
}
?>