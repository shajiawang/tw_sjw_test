<?php
// income 來源點擊次數 
// AaronFu 2016-10-29
/*
-- 資料表結構 lc_income_click
CREATE TABLE IF NOT EXISTS `lc_income_click` (
`seq` int(10) unsigned NOT NULL,
  `aid` int(11) unsigned NOT NULL COMMENT '展覽代碼',
  `approach` int(11) unsigned NOT NULL COMMENT 'income代碼',
  `cdate` date NOT NULL COMMENT '建立日期',
  `chour` int(2) NOT NULL DEFAULT '0' COMMENT '建立時點',
  `clicks` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '點擊數量'
) COMMENT='income來源點擊統計';

-- 統計某期間 各income 點擊總數
SELECT `approach`, sum(clicks) click 
FROM `lc_income_click` where 
 	`aid`='207' 
	and `cdate`>='2016-11-01' and `cdate`<='2016-11-30' 
	group by `approach`

-- 資料表結構 lc_income_page
CREATE TABLE IF NOT EXISTS `lc_income_page` (
`seq` int(10) unsigned NOT NULL,
  `aid` int(11) unsigned NOT NULL COMMENT '展覽代碼',
  `approach` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'income代碼',
  `cdate` date NOT NULL COMMENT '建立日期',
  `chour` int(2) NOT NULL DEFAULT '0' COMMENT '建立時點',
  `page_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '瀏覽頁',
  `clicks` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '點擊數量'
) COMMENT='income來源瀏覽記錄';

-- 資料表結構 `lc_income_temp`
CREATE TABLE IF NOT EXISTS `lc_income_temp` (
`seq` int(10) unsigned NOT NULL,
  `aid` int(11) unsigned NOT NULL COMMENT '展覽代碼',
  `approach` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'income代碼',
  `cdate` date NOT NULL COMMENT '建立日期',
  `chour` int(2) NOT NULL DEFAULT '0' COMMENT '建立時點',
  `page_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT '瀏覽頁'
) COMMENT='income來源暫存記錄';

*/
//@session_start(); 
//date_default_timezone_set("Asia/Taipei");
require_once 'cookie_obj.php'; //載入 cookie 物件檔
$cookie_obj = new cookie_obj(); //物件實體化

//income 計數函式
function income_counter($aid=null, $ci)
{
	$expire = time()+ 60*60;
	$cookieobj = new cookie_obj(); //物件實體化
	$get_income_code = 0;
	
	//獲取來源 income
	if(isset($_GET['income']) ){
		$get_income_code = intval(trim($_GET['income']));
		$cookieobj->set_cookie('income', $get_income_code, $expire);
	} else { 
		$get_income_code = intval($cookieobj->get_cookie('income'));
	}
	
	if(empty($get_income_code) || $get_income_code !==0){
		$cookieobj->set_cookie('income', $get_income_code, 0);
	}
	
	//主機端 income 內容
	$_SESSION['income'] = $income_code = $get_income_code;
	
	//獲取路徑
	/*$page_info = $cookieobj->get_cookie('pageinfo');
	if(empty($page_info) ){ 
		$cookieobj->set_cookie('pageinfo', $_SERVER['REQUEST_URI'], $expire);
		$page_info = $_SERVER['REQUEST_URI'];
	}*/
	$page_info = $_SERVER['REQUEST_URI'];
	
	if(! empty($aid) ){
		//$db_link = @mysql_connect($db->hostname, $db->username, $db->password)or die("資料庫連接失敗!");
		//@mysql_query("SET NAMES 'utf8'");
		//@mysql_select_db("liucms") or die("資料表連接失敗!");
		$ci->load->model('site_config_model'); 
		$db = $ci->site_config_model;
		
		$info['aid'] = $aid;
		$info['sessid'] = session_id(); //訪客session_id
		$info['today'] = date("Y-m-d");
		$info['now'] = (int)date("H");
		$info['page'] = $page_info;
		$info['income'] = $income_code;
		
		
		//暫存income來源
		$set_income_temp = income_temp($info, $db);
		
		if($set_income_temp){
			//點擊統計
			income_click($info, $db);
			
			//瀏覽記錄
			income_page($info, $db);
		}
	}
}

//暫存income來源
function income_temp($info, $db)
{
	//刪除 60日以前的
	$del_sec = strtotime($info['today']) - (60 * 86400);
	$del_date = date("Y-m-d", $del_sec); 
	$sql = "DELETE FROM `liucms`.`lc_income_temp` WHERE `cdate`<'{$del_date}' ";
	$result = $db->set_query($sql);
	//echo $db->sql_debug(); 
	
	//取得現存資料
	$sql = "SELECT * FROM `liucms`.`lc_income_temp` 
		WHERE `aid`='{$info['aid']}'
		AND `approach`='{$info['income']}' 
		AND `sesskey`='{$info['sessid']}' 
		AND `cdate`='{$info['today']}' 
		AND `chour`='{$info['now']}' ";
	$result = $db->get_query($sql);
	$result_num = (!empty($result)) ? count($result) : 0;
	
	if($result_num >0){
		//已存在
		return true;
	} else {
		//不存在, 新增一筆紀錄
		$insert_str = "`aid`='{$info['aid']}', `approach`='{$info['income']}', `cdate`='{$info['today']}', `chour`='{$info['now']}'";
		
		$sql0 = "INSERT INTO `liucms`.`lc_income_temp` SET ". $insert_str .", `sesskey`='{$info['sessid']}', `page_path`='{$info['page']}' ";
		$result = $db->set_query($sql0);
		
		//來源點擊統計
		$sql_1 = "SELECT * FROM `liucms`.`lc_income_click` WHERE `aid`='{$info['aid']}' AND `approach`='{$info['income']}' AND `cdate`='{$info['today']}' AND `chour`='{$info['now']}' ";
		//$result_1 = mysql_query($sql_1);
		//$result_1_num = mysql_num_rows($result_1);
		$result_1 = $db->get_query($sql_1);
		$result_1_num = (!empty($result_1)) ? count($result_1) : 0;
		if($result_1_num >0){} else {
			$sql1 = "INSERT INTO `liucms`.`lc_income_click` SET ". $insert_str .", `clicks`='1' ";
			$result = $db->set_query($sql1);
		}
		
		//來源瀏覽記錄
		$sql_2 = "SELECT * FROM `liucms`.`lc_income_page` WHERE `aid`='{$info['aid']}' AND `approach`='{$info['income']}' AND `cdate`='{$info['today']}' AND `chour`='{$info['now']}' AND `page_path`='{$info['page']}' ";
		//$result_2 = mysql_query($sql_2);
		//$result_2_num = mysql_num_rows($result_2);
		$result_2 = $db->get_query($sql_2);
		$result_2_num = (!empty($result_2)) ? count($result_2) : 0;
		if($result_2_num >0){} else {
			$sql2 = "INSERT INTO `liucms`.`lc_income_page` SET ". $insert_str .", `clicks`='1', `page_path`='{$info['page']}' ";
			$result = $db->set_query($sql2);
		}
		
		return false;
	}
}

//income來源點擊統計
function income_click($info, $db)
{
	//取得現存資料
	$sql_0 = "SELECT count(*) count FROM `liucms`.`lc_income_temp` 
		WHERE `aid`='{$info['aid']}'
		AND `approach`='{$info['income']}' 
		AND `cdate`='{$info['today']}' 
		AND `chour`='{$info['now']}' ";
	//$result_0 = mysql_query($sql_0);
	//$row = mysql_fetch_array($result_0);
	$row = $db->get_query($sql_0);
	
	$sql = "UPDATE `liucms`.`lc_income_click` SET `clicks`='{$row[0]->count}' 
		WHERE `aid`='{$info['aid']}'
		AND `approach`='{$info['income']}' 
		AND `cdate`='{$info['today']}' 
		AND `chour`='{$info['now']}' ";
	//$result = mysql_query($sql);
	$result = $db->set_query($sql);
}

//income來源瀏覽記錄
function income_page($info, $db)
{
	//取得現存資料
	$sql_0 = "SELECT count(*) count FROM `liucms`.`lc_income_temp` 
		WHERE `aid`='{$info['aid']}'
		AND `approach`='{$info['income']}' 
		AND `cdate`='{$info['today']}' 
		AND `chour`='{$info['now']}' 
		AND `page_path`='{$info['page']}' ";
	//$result_0 = mysql_query($sql_0);
	//$row = mysql_fetch_array($result_0);
	$row = $db->get_query($sql_0); 
	
	$sql = "UPDATE `liucms`.`lc_income_page` SET `clicks`='{$row[0]->count}' 
		WHERE `aid`='{$info['aid']}'
		AND `approach`='{$info['income']}' 
		AND `cdate`='{$info['today']}' 
		AND `chour`='{$info['now']}' 
		AND `page_path`='{$info['page']}' ";
	//$result = mysql_query($sql);
	$result = $db->set_query($sql);
}
?>