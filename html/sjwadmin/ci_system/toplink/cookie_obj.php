<?php
/**
* 使用說明
require_once 'cookie_obj.php'; //載入
$cookie_obj = new cookie_obj(); //物件實體化
$cookie_obj->set_cookie('userinfo', $cookie_value, $expires); //設值
$cookie_obj->get_cookie('userinfo'); //取值
$cookie_obj->del_cookie(array('name'=>'userinfo') ); //删除
*/

class cookie_obj
{
	private $key;
	
	public function __construct() {
		$this->key = 'secret$key_top-link%1234'; //24字節
	}
	
	/**
     * 指定 cookie 取值
     */
    public function get_cookie($name)
    {
	   //主機端Cookie內容
	   $server_cookie = isset($_SESSION['cookie_'. $name]) ? $_SESSION['cookie_'. $name] : null; 
	   
	   //用户端Cookie內容
	   $client_cookie = isset($_COOKIE[$name]) ? $this->cookie_decrypt($_COOKIE[$name]) : null; 
		   
	   //Cookie 取值
	   //$this_cookie = (!empty($client_cookie) ) ? $client_cookie : $server_cookie; 
	   $this_cookie = $client_cookie; 
	   
	   return $this_cookie;
    }
	
	/**
     * 指定 cookie 設值
     */
    public function set_cookie($_name, $_value, $_expire=null, $_path='/', $_domain=null, $_secure=0)
    {
		$args['name'] = $_name;
        $args['value'] = $_value; 
        $args['expire'] = $_expire;
        $args['path'] = $_path;
        $args['domain'] = $_domain;
        $args['secure'] = $_secure;
		
		//設置主機端Cookie內容
		$_SESSION['cookie_'. $_name] = $_value; 
		
		//設置用户端Cookie內容
		$this_cookie = $this->_set_client($args);
        
		return $this_cookie;
    }

    /**
     * 設置用户端 Cookie 內容
     *
     * @param array $args
     * @return boolean
     */
    private function _set_client($args)
    {
        $name = $args['name']; 
        $value = $this->cookie_encrypt($args['value']); 
        $expire = isset($args['expire']) ? $args['expire'] : null;
        $path = isset($args['path']) ? $args['path'] : '/';
        $domain = isset($args['domain']) ? $args['domain'] : null;
        $secure = isset($args['secure']) ? $args['secure'] : 0;
		$this_cookie = setcookie($name, $value, $expire, $path, $domain, $secure);

        return $this_cookie;
    }
	
    /**
     * 删除cookie
     * 
     * @param array $args
     * @return boolean
     */
    public function del_cookie($args)
    {
        $name = $args['name'];
        $domain = isset($args['domain']) ? $args['domain'] : null;

        return isset($_COOKIE[$name]) ? setcookie($name, '', time() - 86400, '/', $domain) : true;
    }
	
	/**
     * 解密cookie
     * 
     * @param string $encryptedText
     * @return string
     */
    private function cookie_decrypt($encryptedText)
    {
        $cryptText = base64_decode($encryptedText);
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $decryptText = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $this->key, $cryptText, MCRYPT_MODE_ECB, $iv);

        return trim($decryptText);
    }

    /**
     * 加密cookie
     *
     * @param string $plainText
     * @return string
     */
    private function cookie_encrypt($plainText)
    {
        $ivSize = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
        $iv = mcrypt_create_iv($ivSize, MCRYPT_RAND);
        $encryptText = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $this->key, $plainText, MCRYPT_MODE_ECB, $iv);

        return trim(base64_encode($encryptText));
    }

}