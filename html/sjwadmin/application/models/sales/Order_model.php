<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends MY_Model
{
    public $table = 'ec_order';
    public $pk = 'id';
    public $choices_inovice_type = array(1=>'二聯式', 2=>'三聯式');
    public $choices_pay;

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);

        $this->load->model(array(
            'sales/order_item_model',
        ));

        $this->choices_pay = array(
            '1' => '上X',
            '2' => '大X',
        );
    }

    public function getFormDefault($user=array())
    {
        $data = array_merge(array(
            'order_no' => '',
            'name' => '',
            'email' => '',
            'telephone' => '',
            'total' => '',
            'order_status' => '',
            'payment_method' => '',
            'ip' => '',
            'user_agent' => '',
            'remark' => '',
            'buyer_name' => '',
            'buyer_email' => '',
            'buyer_phone' => '',
            'buyer_gender' => '',
            'buyer_birthday' => '',
        ), $user);

        return $data;
    }

    public function getVerifyConfig()
    {
        $config = array(
            'member_group_id' => array(
                'field' => 'member_group_id',
                'label' => '群組',
                'rules' => 'required',
            ),
            'name' => array(
                'field' => 'name',
                'label' => '姓名',
                'rules' => 'trim|required',
            ),
            'email' => array(
                'field' => 'email',
                'label' => 'E-Mail',
                'rules' => 'trim|required|valid_email|is_unique[member.email]',
            ),
            'telephone' => array(
                'field' => 'telephone',
                'label' => '聯絡電話',
                'rules' => 'trim|required',
            ),
            'buyer_name' => array(
                'field' => 'buyer_name',
                'label' => '付款人姓名',
                'rules' => 'trim|required',
            ),
            'buyer_email' => array(
                'field' => 'buyer_email',
                'label' => '付款人E-mail',
                'rules' => 'trim|required|valid_email',
            ),
            'buyer_phone' => array(
                'field' => 'buyer_phone',
                'label' => '付款人電話',
                'rules' => 'trim|required|numeric',
            ),
            'buyer_gender' => array(
                'field' => 'buyer_gender',
                'label' => '付款人性別',
                'rules' => 'required|in_list[1,2]',
            ),
            'buyer_birthday' => array(
                'field' => 'buyer_birthday',
                'label' => '付款人生日',
                'rules' => 'trim|required|valid_date',
            ),
        );

        return $config;
    }
	
	public function orderPayment($order_id, $pay_id)
	{
		//訂單是否存在
		if($order_id)
		{
			$payment_method = 0;
			
			if(is_numeric($pay_id)){
				$payment = $this->payment_method_model->getRowByID($pay_id);
				$payment_method = $payment[0]['id'];
			}
			
			//更新訂單付款方式
			$conditions = array( 'id' => $order_id );
			$_fields = array( 'payment_method_id' => $payment_method );
			$this->order_model->update($conditions, $_fields);
		}
	}
	
	public function orderFreight($order_id, $freight_id)
	{
		//訂單是否存在
		if($order_id)
		{
			$shipping_id = 0;
			$shipping_fee = 0;
			
			if(is_numeric($freight_id)){
				$freight = $this->shipping_method_model->getRowByID($freight_id);
				$shipping_id = $freight[0]['id'];
				$shipping_fee = $freight[0]['fee'];
			}
			
			//更新訂單運送方式
			$conditions = array( 'id' => $order_id );
			$_fields = array( 'shipping_method' => $shipping_id,
				'shipping_fee' => $shipping_fee, 
			);
			$this->order_model->update($conditions, $_fields);
		}
	}
	
	public function orderDelete($order_id, $item_id)
	{
		$now = date('Y-m-d H:i:s');
		//確認庫存 if (!$this->product_model->chkQuantity($product['id'], $qty)) { JsMsg('商品庫存不足', base_url("product/{$product_id}")); }
		
		//訂單是否存在
		if($order_id && $item_id)
		{
			//更新庫存 $this->product_model->updateQuantity($product['id'], $qty, 'minus');
			
			$conditions = array('id' => $item_id,
				'order_id' => $order_id,
			);
			$this->order_item_model->delete($conditions);
		}
	}
	
	public function orderUpdate($order_id, $item_id, $qty)
	{
		$now = date('Y-m-d H:i:s');
		//確認庫存 if (!$this->product_model->chkQuantity($product['id'], $qty)) { JsMsg('商品庫存不足', base_url("product/{$product_id}")); }
		
		//訂單是否存在
		if($order_id && $item_id)
		{
			if(empty($qty))
			{
				$this->orderDelete($order_id, $item_id);
			}
			else
			{
				//更新庫存 $this->product_model->updateQuantity($product['id'], $qty, 'minus');
				$_item = $this->order_item_model->getRowByID($order_id, $item_id);
				
				$conditions = array('id' => $item_id,
					'order_id' => $order_id,
				);
				
				//訂單商品明細
				$item_fields = array(
					'order_id' => $order_id, //訂單序號
					'quantity' => $qty,
					'subtotal' => intval($_item[0]['price']) * intval($qty),
				);
				$this->order_item_model->update($conditions, $item_fields); //echo $this->order_item_model->get_query();
			}
		}
	}
	
	public function orderInsert($product)
	{
		$now = date('Y-m-d H:i:s');
		$qty = 1; //$this->input->post('quantity'); //訂購數量
		
		//$payment_method = $this->payment_method_model->get(1); //等待付款
		//$order_status = $this->order_status_model->get(1);  //新訂單
		//確認庫存 if (!$this->product_model->chkQuantity($product['id'], $qty)) { JsMsg('商品庫存不足', base_url("goods/{$product_id}")); }
		
		//訂單是否存在
		$user_id = isset($this->flags->member) ? $this->flags->member['id'] : '';
		$user_sess_id = session_id();
		$_order = $this->getOrderInfo($user_sess_id, $user_id, $product['id']);
		
		if(isset($_order['main']['id'])){
			$saved_id = $_order['main']['id'];
		}
		else
		{
			//訂單主檔
			$order['income_id'] = 0;
			$order['order_no'] = $this->order_model->getOrderNo(1); //訂單編號
			$order['order_status_id'] = 1;
			$order['create_at'] = $now;
			$order['sess_id'] = session_id();
			$order['user_id'] = $user_id;
			$order['vendor_id'] = $_SESSION['vid'];
			$saved_id = $this->order_model->insert($order);
		
			//訂單問答記錄			
			$order_h['order_id'] = $saved_id;
			$order_h['user_id'] = $user_id;
			$order_h['create_at'] = $now;
			$order_h['ip'] = $this->input->ip_address();
			$order_h['user_agent'] = $this->input->user_agent();
			$this->order_history_model->insert($order_h);
		}
		
		if($saved_id)
		{
			//更新庫存 $this->product_model->updateQuantity($product['id'], $qty, 'minus');
			
			if(isset($_order['item']) && $_order['item']['product_id']==$product['id']){
			} else {
				//訂單商品明細
				$item_fields = array(
					'order_id' => $saved_id, //訂單序號
					'product_id' => $product['id'], //商品編號
					'name' => $product['name'],
					'image' => $product['image'],
					'quantity' => $qty,
					'price' => $product['sale_price'],
					'subtotal' => intval($product['sale_price']) * intval($qty),
					'vendor_id' => $_SESSION['vid'],
				);
				$this->order_item_model->insert($item_fields);
			}
			
			return $saved_id;
		}
		
		return 0;
	}

    public function getOrderInfo($sid=null, $uid=null, $vid=null, $item_id=null)
    {
        $order = array();
		
		if(!empty($sid) && !empty($uid) )
		{
			$this->db->where('vendor_id=', $vid);
			$this->db->where('user_id=', $uid);
			$this->db->where('sess_id=', $sid);
			$this->db->where('order_status_id=', 1);
			$query = $this->db->get($this->table);
			$order['main'] = $query->row_array();
		}
		elseif(!empty($sid))
		{
			$this->db->where('vendor_id=', $vid);
			$this->db->where('sess_id=', $sid);
			$this->db->where('order_status_id=', 1);
			$this->db->order_by('id desc');
			$query = $this->db->get($this->table);
			$order['main'] = $query->row_array();
		}
		
		if(!empty($item_id) && !empty($order))
		{
			$this->db->where('order_id=', $order['main']['id']);
			$this->db->where('product_id=', $item_id);
			$query = $this->db->get('ec_order_item');
			$order['item'] = $query->row_array();
		}
		
        return $order;
    }
	
	public function getOrderNo($kind=1)
    {
        $this->db->select_max('order_no');
        $this->db->where('create_at >=', date('Y-m-d'));
        $query = $this->db->get($this->table);
        $order = $query->row_array();
		
        if ($order['order_no']) {
            $no = $order['order_no'] + 1;
        } else {
            $no = (string)$kind . date('ymd') . '00001';
        }

        return $no;
    }

    public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
		if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }
		
		if (isset($attrs['count']) && $attrs['count'] == TRUE) {
            $params['count'] = TRUE;
            return $this->getData($params);
        }
		
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }

		$data = $this->getData($params);
		
		return $data;
    }

    public function getListCount($params=array())
    {
        $params['count'] = TRUE;
        return $this->getList($params);
    }

    public function getByMemberID($user_id)
    {
        $conditions = array(
            'user_id' => $user_id
        );
        $params = array(
            'conditions' => $conditions,
        );
        $data = $this->getList($params);
        return $data;

    }

    public function getExport($conditions)
    {
        $A = $this->table;
        $B = 'order_product';
        $this->db->from($A);
        $this->db->join($B, "{$A}.id = {$B}.order_id", 'right');
        $this->db->order_by('create_at desc');
        $where = array();
        if (isset($conditions['order_status'])) {
            $where["{$A}.order_status"] = $conditions['order_status'];
        }
        if (isset($conditions['payment_method'])) {
            $where["{$A}.payment_method"] = $conditions['payment_method'];
        }
        if (isset($conditions['shipping_method'])) {
            $where["{$A}.shipping_method"] = $conditions['shipping_method'];
        }
        if (isset($conditions['start_date'])) {
            $where["{$A}.create_at >="] = $conditions['start_date'].' 00:00:00';
        }
        if (isset($conditions['end_date'])) {
            $where["{$A}.create_at <="] = $conditions['end_date'].' 23:59:59';
        }
        $this->db->where($where);
        $query = $this->db->get();
        $data = $query->result_array();

        return $data;
    }

    public function getByOrderId($order_id)
    {
        $conditions = array(
            'id' => $order_id,
        );

        return $this->get($conditions);
    }
	
	public function getByOrderNo($order_no)
    {
        $conditions = array(
            'order_no' => $order_no,
        );

        return $this->get($conditions);
    }


    public function getListByToday($rows=10, $order_status_id=NULL)
    {
        $today = date('Y-m-d');
        $conditions = array();
        if ($order_status_id) {
            $conditions['order_status'] = $order_status_id;
        }

        $params = array(
            'select' => 'id, order_no, member_id, name, payment_method, order_status, total, create_at',
            'rows' => $rows,
            'conditions' => $conditions,
            'order_by' => 'create_at desc',
        );
        return $this->getData($params);
    }

    public function getItems($attrs=array(), $language_id=1)
    {
        $order = $this->db->dbprefix . $this->table;
        $item = $this->db->dbprefix . $this->order_item_model->table;

        $select = "a.member_id, b.commission_category_id as category_id, sum(b.quantity) as quantity";

        $sql = "select {$select} \n";
        $sql .= "from {$order} as a \n";
        $sql .= "right join {$item} as b on a.id=b.order_id \n";
        $sql .= "where 1 \n";
        if(isset($attrs['conditions'])){
            foreach ($attrs['conditions'] as $k => $v) {
                switch ($k) {
                    case 'start_date':
                        $sql .= " and a.create_at >= '{$v}' \n";
                        break;

                    case 'end_date':
                        $sql .= " and a.create_at <= '{$v}' \n";
                        break;

                    case 'order_status':
                        $sql .= " and a.order_status in (". implode(',', $v).")";
                        break;

                    default:
                        $sql .= " and a.{$k} = '{$v}' \n";
                        break;

                }
            }
        }

        $sql .= "group by category_id \n";

        $query = $this->db->query($sql);
        $data = $query->result_array();

        return $data;
    }
}


