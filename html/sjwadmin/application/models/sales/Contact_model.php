<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact_model extends MY_Model
{
    public $table = 'ec_contact';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'order_no' => '',
            'buyer_name' => '',
			'buyer_email' => '',
			'buyer_phone' => '',
			'comment' => '',
        );

        return array_merge($fields, $data);
    }

    public function getVerifyConfig()
    {
        $config = array(
            'order_no' => array(
                'field' => 'order_no',
                'label' => '訂單編號',
                'rules' => 'trim|integer',
            ),
			'buyer_name' => array(
                'field' => 'buyer_name',
                'label' => '姓名',
                'rules' => 'required',
            ),
			'buyer_email' => array(
				'field' => 'buyer_email',
				'label' => '電子信箱',
				'rules' => 'trim|valid_email',
			),
			'buyer_phone' => array(
				'field' => 'buyer_phone',
				'label' => '聯絡電話',
				'rules' => 'required|trim|integer',
			),
        );

        return $config;
    }

    public function getList($attrs=array())
    {
        $params = array(
            'select' => '',
        );
        if (isset($attrs['conditions'])) {
            $params['conditions'] = $attrs['conditions'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = array(
                'many' => TRUE,
                'data' => array(
                    array('field' => 'name', 'value'=>$attrs['q'], 'position'=>'both'),
                ),
            );
            // unset
        }

        $data = $this->getData($params);


        return $data;
    }

    public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
        );

        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
        $data = $this->getList($params);
        return count($data);
    }
    
}

