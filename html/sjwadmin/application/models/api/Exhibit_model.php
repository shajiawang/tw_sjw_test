<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exhibit_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();

        $this->load->library('api');
        $this->api->setUrl(API_URL);
    }

    public function getChoices($conditions=array())
    {
        $data = array();

        $params = array(
            'type' => 'choices',
            'online' => 0,
        );
        $response = $this->api->request('exhibit/query', $params, 'get');
        if ($response->status == TRUE){
            $data = $response->data;
        }

        return $data;
    }

}

