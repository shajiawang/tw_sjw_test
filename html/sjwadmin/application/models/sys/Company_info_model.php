<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company_info_model extends MY_Model
{
    public $table = 'company_info';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    //表單欄位預設
	public function getFormDefault($user=array())
    {
        $data = array_merge(array(
			'status' => 0,
			'exp_category' => 1,
			'comp_name' => '',
			'invoice_name' => '',
			'comp_idno' => '',
			'boss' => '',
			'boss_title' => '',
			'contractor' => '',
			'contractor_title' => '',
			'contractor_tel' => '',
			'contractor_ext' => '',
			'contractor_cell' => '',
			'contractor_fax' => '',
			'contractor_email' => '',
			'contractor_addr' => '',
			'comp_tel' => '',
			'comp_fax' => '',
			'comp_link' => '',
			'comp_mailfrom' => '',
			'comp_email' => '',
			'payment' =>  'check', /*付款方式:check,cash,credit,other,*/
			'payment_bank' => '',
			'payment_account' => '',
			'sales_opt' => 0, /*指派業務員:0無, 1有*/
			'sales_id' => '',
			'maintain_name' =>'',
			'memo' =>'',
			'qrcode' =>'',
        ), $user);

        return $data;
    }
	
	public function getTabList()
    {
        $data = array('info' => '廠商資訊'
			, 'contact' => '聯絡資料'
			, 'payment' => '付款資料'
		);

        return $data;
    }
	
	public function getPaymentWay()
    {
        $data = array('check' => '支票'
			, 'cash' => '匯款'
			, 'credit' => '信用卡'
			, 'other' => '其他'
		);

        return $data;
    }
	
	public function getOptions($vars=null)
    {
        $data['exp_category'] = $vars['exp_category'];
		$data['payment'] = isset($vars['payment']) ? $vars['payment'] : $this->getPaymentWay();

        return $data;
    }
	
	public function getFormConfig()
    {
		//Disabled欄位'status'=>2
		
		$form_field['info']['status'] = array('show'=>0, 'status'=>1, 'label'=>'使用狀態', 'type'=>null, 'remark'=>'');
		$form_field['info']['exp_category'] = array('show'=>1, 'status'=>1, 'label'=>'展覽產業', 'type'=>'select', 'remark'=>'');
		$form_field['info']['account'] = array('show'=>1, 'status'=>2, 'label'=>'使用者帳戶', 'type'=>null, 'remark'=>'');
		$form_field['info']['comp_name'] = array('show'=>1, 'status'=>1, 'label'=>'廠商名稱', 'type'=>null, 'remark'=>'');
		$form_field['info']['boss'] = array('show'=>1, 'status'=>1, 'label'=>'負責人', 'type'=>null, 'remark'=>'');
		$form_field['info']['boss_title'] = array('show'=>1, 'status'=>1, 'label'=>'頭銜', 'type'=>null, 'remark'=>'');
		$form_field['info']['sales_opt'] = array('show'=>0, 'status'=>2, 'label'=>'指派業務員', 'type'=>null, 'remark'=>'');
		$form_field['info']['sales_id'] = array('show'=>1, 'status'=>2, 'label'=>'開發人員編', 'type'=>'select', 'remark'=>'');
		$form_field['info']['maintain_name'] = array('show'=>1, 'status'=>2, 'label'=>'維護人員編', 'type'=>'select', 'remark'=>'');
		$form_field['info']['qrcode'] = array('show'=>0, 'status'=>2, 'label'=>'QR二維碼', 'type'=>null, 'remark'=>'');
		$form_field['info']['memo'] = array('show'=>1, 'status'=>2, 'label'=>'備註', 'type'=>'textarea', 'remark'=>'');
		$form_field['info']['create_at'] = array('show'=>1, 'status'=>2, 'label'=>'建檔日期', 'type'=>null, 'remark'=>'');
		$form_field['info']['update_at'] = array('show'=>1, 'status'=>2, 'label'=>'更新日期', 'type'=>null, 'remark'=>'');
		$form_field['info']['user_id'] = array('show'=>1, 'status'=>2, 'label'=>'更新人員', 'type'=>null, 'remark'=>'');
		
		$form_field['contact']['contractor'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡人', 'type'=>null, 'remark'=>'');
		$form_field['contact']['contractor_title'] = array('show'=>1, 'status'=>1, 'label'=>'職稱', 'type'=>null, 'remark'=>'');
		$form_field['contact']['contractor_tel'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡電話', 'type'=>null, 'remark'=>'');
		$form_field['contact']['contractor_ext'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡分機', 'type'=>null, 'remark'=>'');
		$form_field['contact']['contractor_cell'] = array('show'=>1, 'status'=>1, 'label'=>'行動電話', 'type'=>null, 'remark'=>'');
		$form_field['contact']['contractor_fax'] = array('show'=>1, 'status'=>1, 'label'=>'傳真號碼', 'type'=>null, 'remark'=>'');
		$form_field['contact']['contractor_email'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡電郵', 'type'=>null, 'remark'=>'');
		$form_field['contact']['contractor_addr'] = array('show'=>1, 'status'=>1, 'label'=>'營業地址', 'type'=>null, 'remark'=>'');
		$form_field['contact']['comp_tel'] = array('show'=>0, 'status'=>1, 'label'=>'電話代表號', 'type'=>null, 'remark'=>'');
		$form_field['contact']['comp_fax'] = array('show'=>0, 'status'=>1, 'label'=>'傳真代表號', 'type'=>null, 'remark'=>'');
		$form_field['contact']['comp_link'] = array('show'=>1, 'status'=>1, 'label'=>'網址', 'type'=>null, 'remark'=>'');
		$form_field['contact']['comp_mailfrom'] = array('show'=>0, 'status'=>1, 'label'=>'客服電郵名稱', 'type'=>null, 'remark'=>'');
		$form_field['contact']['comp_email'] = array('show'=>0, 'status'=>1, 'label'=>'客服電郵', 'type'=>null, 'remark'=>'');
		
		$form_field['payment']['invoice_name'] = array('show'=>1, 'status'=>1, 'label'=>'發票抬頭', 'type'=>null, 'remark'=>'');
		$form_field['payment']['comp_idno'] = array('show'=>1, 'status'=>1, 'label'=>'統一編號', 'type'=>null, 'remark'=>'');
		$form_field['payment']['payment'] = array('show'=>1, 'status'=>1, 'label'=>'付款方式', 'type'=>'select', 'remark'=>'');
		$form_field['payment']['payment_bank'] = array('show'=>1, 'status'=>1, 'label'=>'付款銀行', 'type'=>null, 'remark'=>'');
		$form_field['payment']['payment_account'] = array('show'=>1, 'status'=>1, 'label'=>'付款帳號', 'type'=>null, 'remark'=>'');

        return $form_field;
    }

    //表單欄位驗證
	public function getVerifyConfig()
    {
        $config = array(
            'exp_category' => array(
                'field' => 'exp_category',
                'label' => '展覽產業',
                'rules' => 'required',
            ),
            'comp_name' => array(
                'field' => 'comp_name',
                'label' => '廠商名稱',
                'rules' => 'trim|required',
            ),
			'contractor' => array(
                'field' => 'contractor',
                'label' => '聯絡人',
                'rules' => 'trim|required',
            ),
			'contractor_email' => array(
                'field' => 'contractor_email',
                'label' => '聯絡電郵',
                'rules' => 'trim|valid_email', /*|is_unique[user.email]*/
            ),
            'contractor_tel' => array(
                'field' => 'contractor_tel',
                'label' => '聯絡電話',
                'rules' => 'trim|required',
            ),
        );

        return $config;
    }

    public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }

        $data = $this->getData($params,'array');
		
		return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );
		
		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
		
        $data = $this->getList($params);
        return count($data);
    }

    public function getChoices($conditions=array())
    {
		$data = array();
        $users = $this->getList();
        foreach ($users as $user) {
            $data[$user['id']] = $user['name'];
        }

        return $data;
    }

    public function getRowByAccount($username)
    {
        $conditions = array(
            'account' => $username,
        );

        return $this->get($conditions);
    }
	
	public function getRowById($id)
    {
        $conditions = array(
            'id' => $id,
        );

        return $this->get($conditions);
    }

    public function getRowByEmail($email)
    {
        $conditions = array(
            'contractor_email' => $email,
        );

        return $this->get($conditions);
    }

    public function _insert($fields=array())
    {
        return $this->insert($fields, 'create_at');
    }

    public function _update($pk, $fields=array())
	{
        return parent::update($pk, $fields);
    }

}

