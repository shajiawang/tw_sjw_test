<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mb_encodevalue_model extends MY_Model
{
	public $table = 'mb_encodevalue';
	public $pk = 'id';

	public function __construct()
	{
		parent::__construct();

		$this->init($this->table, $this->pk);
	}

	//表單欄位預設
	public function getFormDefault($user=array())
	{
		$data = array_merge(array(
			'user_group_id' => 1,
			'name' => '',
			'account' => '',
			'pssword' => '',
			'passconf' => '',
			'email' => '',
			'telephone' => '',
			'enable' => 0,
		), $user);

		return $data;
	}

	//表單欄位驗證
	public function getVerifyConfig()
	{
		$config = array(
			'user_group_id' => array(
				'field' => 'user_group_id',
				'label' => '群組',
				'rules' => 'required',
			),
			'name' => array(
				'field' => 'name',
				'label' => '名稱',
				'rules' => 'trim|required',
			),
			'account' => array(
				'field' => 'account',
				'label' => '帳號',
				'rules' => 'trim|required|min_length[4]|max_length[20]|is_unique[user.account]',
			),
			'password' => array(
				'field' => 'password',
				'label' => '密碼',
				'rules' => 'required|min_length[4]|max_length[20]',
			),
			'passconf' => array(
				'field' => 'passconf',
				'label' => '確認密碼',
				'rules' => 'required|matches[password]',
			),
			'email' => array(
				'field' => 'email',
				'label' => 'E-Mail',
				'rules' => 'trim|valid_email|is_unique[user.email]',
			),
			'telephone' => array(
				'field' => 'telephone',
				'label' => 'Telephone',
				'rules' => 'trim|integer',
			),
			'enable' => array(
				'field' => 'enable',
				'label' => '啟用',
				'rules' => 'required|in_list[0,1]',
				/*
				'messagesd' => array(
					'required'=> 'XXXXX',
					'in_list' => 'sdfsdf'
				),
				*/
			),
		);

		return $config;
	}

	public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }

		$data = $this->getData($params);
		
		return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
        );
		
		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
		if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }
		
        $data = $this->getList($params);
        return count($data);
    }
	
	public function GroupName($groups=array(), $user_group_id=1 )
	{
		$user_group_array = explode(",", $user_group_id);
		
		foreach ($user_group_array as $gid){
			$_group_name[$gid] = $groups[$gid];
		}
		
		$_user_group = implode(",", $_group_name);
		
		return $_user_group;
	}
	
	public function getMemberInfo($account=0)
	{
		$data = array();
		$conditions = array(
			'account' => $account,
			'enable'=>2,
		);
		$data = $this->get($conditions); //echo $this->get_query();
		
		return $data;
	}

	public function getChoices($conditions=array())
	{
		$data = array();
		$users = $this->getList();
		foreach ($users as $user) {
			$data[$user['id']] = $user['name'];
		}

		return $data;
	}
	public function getChoicesRows($conditions=array())
	{
		$data = array();
		$params = array(
            'conditions' => $conditions,
        );
		$data = $this->getList($params);

		return $data;
	}

	public function getUser($conditions)
	{
		return $this->get($conditions);
	}
	public function getUserById($id)
	{
		$conditions = array(
			'id' => $id,
		);

		return $this->get($conditions);
	}

	public function insert($fields=array(), $now_field=NULL)
	{
		return parent::insert($fields, 'create_at');
	}

	public function update($conditions=NULL, $fields=array())
	{
		return parent::update($conditions, $fields);
	}

}

