<style type="text/css">
.full-screen {
	background-size: cover;
	background-position: center;
	background-repeat: no-repeat;
}
.item>a{
	display: block;
	height: 100%;
}
.vendor_layout {
    background-color: #fff;
    border-color: #d9d9d9;
	margin-bottom: 20px;
    padding: 30px;
}
</style>

	<!-- main_area 主內容區塊 ******************************-->
	<div class=" clearfix">
	<main class="main clearfix" role="main">
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- index_area 首頁內容區塊 ******************************-->
		<div class="index_area index_style js_index_area js_themes_layout layout_h">
		<div class="container">
			
			<!-- Banner_Slider 首頁橫幅 ******************************-->
			<div id="mycarousel" class=" carousel slide" style="margin-bottom: 30px;">
				<div class="carousel-inner" role="listbox">
					<?php if(! empty($_banner)){ ?>
					<div class="item active">
						<a href="<?=$_banner[0]['url'];?>" target="<?=$_banner[0]['url_target'];?>">
							<img src="<?=$_banner[0]['image'];?>" data-color="#eee">
							<div class="carousel-caption">
								<h3><?=$_banner[0]['slogan'];?></h3>
							</div>
						</a>
					</div>
					<?php } ?>
				</div>
			</div>
			<!--/ Banner_Slider 首頁橫幅 ******************************-->
		
			<div class="item_area vendor_layout">
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">新品上架</h2>
						<h3 class="main_sub font_avant_garde">new product</h3>
					</span>
					<hr class="main_hr">
				</div>
				
				<ul class="list-unstyled item_content clearfix" id="product-list">
				
				<?php foreach ($list as $row) 
				{ ?>
				<li class="item_block js_is_photo_style img_polaroid ">
					<a class="clearfix" href="<?=$row['link_edit'];?>" >
					<span class="item_photo js_product_image " <?php /*data-src="<?=$row['image_thumb_url'];?>"*/?> style="background-image: url('<?=$row['image_url'];?>')"></span>
						<div class="item_caption js_themes_products_bg_color" style="">
						<span class="item_vertical">
							<div class="item_info js_matchheight js_themes_products_color" style="height: 74px;">
								<div class="item_text" style="">
									<h4 class="item_name text_overflow js_products_name pt_fontsize_12px" style="color: #858585;">
									<?=$row['name'];?>
									</h4>
									<ul class="list-unstyled item_price">
										<li class="item_origin item_actual" itemprop="price"><span class="font_montserrat line_through" style="color: #858585;">$<?=$row['price'];?></span></li>
										<li class="item_sale" itemprop="price"><span class="font_montserrat">
										<?php if($row['sale_price']){ ?>
										$<?=$row['sale_price'];?>
										<?php } else { ?>
										搶購價
										<?php } ?>
										</span></li>
									</ul>
								</div>
							</div>
						</span>
						</div>
					</a>
				</li>
				<?php } ?>

				</ul>
			<div id="js_noproductdata" style="display: none;"></div>
			</div>
			
			<?php
			/*
			<div class="row">
				<div class="col-md-5">
					Showing <?=$filter['offset']+1;?>
					to <?=(($filter['offset']+$filter['rows'])>$filter['total'])?$filter['total']:$filter['offset']+$filter['rows'];?>
					of <?=$filter['total'];?> entries
				</div>
				<div class="col-md-7 text-right">
					<?=$this->pagination->create_links();?>
				</div>
			</div>
			*/?>
			
		</div>
		
		</div>
		<!--/ index_area 首頁內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

<script>
/* Banner_Slider_Carousel 首頁橫幅輪播 */
$(function(){
	var $item = $('.carousel .item'); 
	var $wHeight = 350;//$(window).height();
	//$('.carousel').css('max-width',$('.carousel img').width());
	
	$item.height($wHeight); 
	$item.addClass('full-screen');

	$('.carousel img').each(function() {
		var $src = $(this).attr('src');
		var $color = $(this).attr('data-color');
		$(this).parent().css({
			'background-image' : 'url(' + $src + ')',
			'background-color' : $color
		});
		$(this).remove();
	});

	//下方自動加入控制圓鈕
	var total = $('.carousel .carousel-inner div.item').size();
	append_li();
	function append_li(){
		var li = "";
		var get_ac = $( ".carousel .active" );
		var ac =  $( ".carousel .carousel-inner div" ).index( get_ac );

		for (var i=0; i <= total-1; i++){
			if(i == (ac)/2){
				li += "<li data-target='#mycarousel' data-slide-to='"+i+"' class='active'></li>";					
			}else{
				li += "<li data-target='#mycarousel' data-slide-to='"+i+"' class=''></li>";						
				}
			}
			$(".carousel-indicators").append(li);
	}

	//單則隱藏控制鈕
	if ($('.carousel .carousel-inner div.item').length < 2 ) { 
		$('.carousel-indicators, .carousel-control').hide();
	}

	//縮放視窗調整視窗高度
	/*$(window).on('resize', function (){
		$wHeight = $(window).height();
		$item.height($wHeight);
	});*/

	//輪播秒數與滑入停止
	$('.carousel').carousel({
		interval: 5000,
		pause: "hover"
	});
});
</script>