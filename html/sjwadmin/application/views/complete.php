<style>
    /*----------------
    massage_block
 ------------------*/
.massage_block {
    display: table;
    width: 95%;
    max-width: 992px;
    background-color: #f2f2f2;
    margin: 40px auto;
    box-shadow: 0px 1px 8px rgba(0, 0, 0, 0.32);
}
.massage_block > div {
    margin: 7px;
    border: 2px solid #b3b3b3;
    padding: 10px;
}
.massage_block h5 {
    font-size: 1.6rem;
    margin: 0;
}
.massage_block ol, .massage_block ul {
    padding-left: 40px;
    margin: 10px 0;
}

.massage_block li {
    line-height: 2;
}

.enter-btn {
     border-radius: 5px;
    padding: 10px 26px;
    font-size: 35px;
    background-color: #f00;
    color: #fff;
    margin: 20px auto;
    display: table;
    border-bottom: 6px solid #b30000;
}
.enter-btn:hover {
    background-color: #ff4e4e;
}
.imgCtrl{
    width: 100%;
    margin: 0 auto;
}
.price{
  font-size:22px;
  color:red;
  display:flex;
  justify-content:center
}
.price div{
  margin:0 20px;
}
.container {
    width: 1170px;
    margin: auto;
}
@media (max-width:1170px) {
    .container {
    	width: 992px;
    }
}
@media (max-width:992px) {
    .container {
        width: 768px;
    }
}
@media (max-width:768px) {
	.container {
    	width: calc(100% - 20px);
    	padding: 0 10px;
    }
}
</style>
<section>
    <div class="container" style="padding-top: 40px;">
        <center>
            <?php if(isset($order['items'][0]['image'])) { ?>
            <div class="imgCtrl">
                <img alt="<?=$order['items'][0]['name'];?>" src="<?=$order['items'][0]['image_src'];?>" />
            </div>
            <br>
            <?php } ?>

            <?php if ($order['order_status_id'] == 2) { ?>
                <h2 style="font-size: 2rem;">您已成功購買<span><?=$order['items'][0]['name'];?></span></h2>
                <div class="price">
                    <!--
                    <div><b>付款方式：</b><span><?=$choices['payment_method'][$order['payment_method_id']];?></span></div>
                    -->
                    <div><b>票價：</b>NT$<span><?=$order['items'][0]['price'];?><span></div>
                    <div><b>數量：</b><span><?=$order['items'][0]['quantity'];?></span>張</div>
                    <div><b>總計：</b>NT$<span><?=$order['items'][0]['subtotal'];?></span></div>
                </div>
                <!-- ==========成功 end================= -->
            <?php } else { ?>
                <!-- ====== 失敗 ====== -->
                <h2 style="font-size: 2rem;">付款失敗！</h2>
                <p style="font-size: 1.2rem;">
                    付款時發生問題，此訂單未能完成付款，請您重新購買票券！
                </p>
                <!--
                <div class="price"><div><b>付款方式：</b><span>???</span></div>[失敗]</div>
                -->
                <!-- ==========失敗 end================= -->
            <?php } ?>

            <a class="enter-btn small" href="<?= site_url("product/{$product['id']}");?>">回售票頁面</a>
        </center>
        <div class="massage_block">
            <div>
                <h5>購票說明</h5>
                <ul style="list-style-type: decimal;">
                     <li>網路購票－採線上刷卡、現場取票方式。</li>
                     <li>現場取票：當您完成購票流程後，將收到「上聯國際展覽有限公司」發送的E-MAIL購票成功通知信。
                        <br>憑E-MAIL通知信(列印或出示畫面)及購票人本人身分證正本，至各展指定地點，核對資料無誤者即可換票入場。非本人請勿代領。
                        <br /> ※ 若未收到E-MAIL通知信，憑購票人身份證正本現場資料核對無誤者，即可領取本展入場門票。</li>
                     <li>網路訂購一旦成功後，即視為售出，依法恕不接受退換票需求，敬請留意。</li>
                     <li>此交易採用電子發票，大會將會以E-MAIL的方式寄送電子發票至您的信箱，為共同響應綠色環保，將不會印製紙本發票。</li>
                </ul>
                <h5>注意事項</h5>
                <ul style="list-style: none;">
                     <li>※ 本人願意參加本次活動，並同意授權主辦單位及其相關企業，進行蒐集及電腦處理(含委任第三人處理)，以便主辦單位及其相關企業提供各類展覽訊息及各項行銷活動予本人。</li>
                     <li>※ 本展之產品、活動、價格、贈品皆以現場公佈為主，大會保留所有更改之權力。</li>
                     <li>※ 本展之所有抽獎及贈品活動，皆需年滿18歲以上方可參與。</li>
                     <li>※若有其他相關問題，請洽執行單位：上聯國際展覽有限公司，我們將有專人為您服務。</li>
                </ul>
            </div>
        </div>
    </div>
</section>

