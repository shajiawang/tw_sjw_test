<!-- Timeline CSS -->
<link href="<?=$static_css;?>timeline.css" rel="stylesheet">

<?php /*
<!-- Morris Charts CSS -->
<!-- <link href="<?=$static_plugin;?>morrisjs/morris.css" rel="stylesheet"> -->

<!--
<div class="row">
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-bell  fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">00</div>
                        <div>尚未處理通知</div>
                    </div>
                </div>
            </div>
            <a href="<?=base_url('notice');?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">00</div>
                        <div>訂單總數</div>
                    </div>
                </div>
            </div>
            <a href="<?=base_url('sales/order');?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-users fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">00</div>
                        <div>會員總數</div>
                    </div>
                </div>
            </div>
            <a href="<?=base_url('sales/member');?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-envelope  fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                    <div class="huge"><?=$subscription_total_items;?></div>
                    <div>電子報總數</div>
                    </div>
                </div>
            </div>
            <a href="<?=base_url('catalog/edm');?>">
                <div class="panel-footer">
                    <span class="pull-left">View Details</span>
                    <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                    <div class="clearfix"></div>
                </div>
            </a>
        </div>
    </div>
</div>
-->
*/?>

<div class="row">
    <?php /*<div class="col-lg-8">
        <div class="panel panel-warning" id="panel-order">
            <div class="panel-heading">
                <i class="fa fa-list fa-fw"></i> 最新訂單
                <div class="pull-right">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                            <?php
                            if ($filter['order_status_id'] == 'all') {
                                echo '全部訂單狀態';
                            } else {
                                echo $choices['order_status'][$filter['order_status_id']];
                            }
                            ?>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu pull-right" role="menu" >
                            <li class=""><a href="?order_status_id=all">全部訂單狀態</a>
                            <?php foreach ($choices['order_status'] as $k => $v) { ?>
                            <li class="<?=$filter['order_status_id']==$k?'active':'';?>"><a href="?order_status_id=<?=$k;?>"><?=$v;?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div>
                    <table class="table table-condensed">
                        <thead>
                            <tr>
                                <th>編號</th>
                                <th>購買人資料</th>
                                <th>總額</th>
                                <th>狀態</th>
                                <th width="80">建立時間</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($orders as $row) { ?>
                            <tr class="<?=($row['order_status_id']==4)?'text-danger':'';?>">
                                <td><a href="<?=base_url("sales/order/edit/{$row['id']}");?>"><?=$row['order_no'];?></a></td>
                                <td>
                                    <?=$row['buyer_name'];?>
                                </td>
                                <td><b>$<?=number_format($row['total'], 2);?></b></td>
                                <td><?=$choices['order_status'][$row['order_status_id']];?></td>
                                <td style="font-size: 90%;"><?=$row['create_at'];?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->



    </div>
    <!-- /.col-lg-8 -->*/?>

    <div class="col-lg-4">

        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-calendar fa-fw"></i> 日期
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div id="dashboard-calendar" class="datepicker"></div>
            </div>
            <!-- /.panel-body -->
        </div>

        <?php /*<!--
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> 前 24 小時註冊新會員
            </div>
            <div class="panel-body">
                <?php foreach ($members as $row) { ?>
                <p>
                    <a href="<?=base_url("sales/member/edit/{$row['id']}");?>"><?=$row['name'];?></a>
                    註冊於 <?=substr($row['create_at'], 0, 16);?>
                </p>
                <?php } ?>
            </div>
        </div>
        -->
        <!-- /.panel -->*/?>

        <?php /*<!--
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-bar-chart-o fa-fw"></i> 前 24 小時訂閱電子報人數
            </div>
            <div class="panel-body">
                <?php foreach ($subscriptions as $row) { ?>
                <p>
                    <?=mailto($row['email']);?>
                    加入於 <?=substr($row['create_at'], 0, 16);?>
                </p>
                <?php } ?>
            </div>
        </div>
        -->
        <!-- /.panel -->*/?>


    </div>
    <!-- /.col-lg-4 -->
</div>
<!-- /.row -->



<!-- Morris Charts JavaScript -->
<?php /*
<!-- <script src="<?=$static_plugin;?>raphael/raphael&#45;min.js"></script> -->
<!-- <script src="<?=$static_plugin;?>morrisjs/morris.min.js"></script> -->
<!-- <script src="<?=$static_js;?>morris&#45;data.js"></script> -->
*/?>
<script>
$(document).ready(function(){
    $('#accordion .panel-collapse:first').addClass('in');
});
function setOrderStatus(val) {
    location.href = '?order_status_id='+val;
}
</script>
