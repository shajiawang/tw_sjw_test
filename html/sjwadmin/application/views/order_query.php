	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- about_area 內容區塊 ******************************-->
		<div class="orderform_area clearboth">
		<form class="" method="post" action="<?=base_url('order_query/search');?>">
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
		
				<div class="form_layout form_style">
					<header class="title_area clearfix">
						<hgroup class="title_content clearfix">
							<h4 class="main_title">訂單查詢</h4>
							<h5 class="main_sub font_avant_garde">order search</h5>
						</hgroup>
						<hr class="main_hr">
					</header>
					<div class="row">
						<div class="col-md-6">
							<div class="form_group js_order_no_group">
								<div class="row">
									<div class="col-md-12">
										<label class="form_label" for="">訂單編號</label>
									</div>
									<div class="col-md-12">
										<div class="form_data">
											<input class="form_input form_control js_order_no" placeholder="請輸入訂單編號" name="order_no" type="text">
											<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
										</div>
										<div class="help_block js_order_message"><i class="icon-exclamation-circle"></i><span>必填欄位，不得為空白。</span></div>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form_group js_order_validate_code_group">
								<div class="row">
									<div class="col-md-12">
										<label class="form_label" for="">認證號碼</label>
									</div>
									<div class="col-md-12">
										<div class="form_data">
											<input class="form_input form_control js_order_validate_code" placeholder="請輸入認證號碼" name="order_validate_code" type="password">
											<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
										</div>
										<div class="help_block js_order_message"><i class="icon-exclamation-circle"></i><span>必填欄位，不得為空白。</span></div>
										<div class="help_block help_tips"><i class="icon-triangle-right"></i><span>請查閱信箱中的「訂購通知信」。</span></div>
									</div>
								</div>
							</div>
						</div>
					</div>
								<div class="form_group form_lastmargin">
						<div class="row">
							<div class="col-md-3 col-md-offset-9 ">
								<div class="form_group form_lastmargin">
									<button type="submit" class="btn btn_submit btn_block btn_md js_order_search"><span>確定送出</span></button>
								</div>
							</div>
						</div>
					</div>
				</div>
		</form>
		</div>

		<!--/ index_area 首頁內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->
	
	
