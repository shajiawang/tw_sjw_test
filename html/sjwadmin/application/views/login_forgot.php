	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- area 內容區塊 ******************************-->
		<div class="about_area about_style clearboth clearfix">
		<div class="main_layout main_style">
				
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">登入 - 設定簽章</h2>
						<h3 class="main_sub font_avant_garde">login</h3>
					</span>
					<hr class="main_hr">
				</div>
				
				<div class="container">
				<div class="row" style="margin:0 0 20px 0px;">
					<div class="col-md-4 col-md-offset-4">
						
						<form method="POST" role="form" id="loginform" class="form-signin" action="<?=base_url('login/forgot_check');?>">
						<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
							
							<fieldset>
								<h3 class="sign-up-title" style="color:dimgray; margin-bottom:20px;"><span style="color:blue">用戶驗證</span> >> 設定簽章</h3>
								<?php if ($error) { ?>
								<div id="alert_error" class="alert bs-callout bs-callout-danger alert-dismissible" role="alert">
									<button class="close" aria-label="Close" data-dismiss="alert" type="button" style="color:red">
										<span aria-hidden="true">× <?=$error;?></span>
									</button>
								</div>
								<?php } ?>
								
								<label class="form_label" for="phone">輸入手機</label>
								<input type="password" name="phone" id="phone" value="" class="form_input form_control text ui-widget-content ui-corner-all">
								<label class="form_label" for="private_key">輸入私鑰</label>
								<input type="password" name="private_key" id="private_key" placeholder="英數字符 6 ~ 20碼" value="" class="form_input form_control text ui-widget-content ui-corner-all" >
								
								<input class="btn btn-lg btn-success btn-block" type="submit" value="Next >>" >
								
							</fieldset>
						</form>
					</div>
			  </div>
				</div>
		</div>
		</div>
		<!--/ area 內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->


<script>
$(document).ready(function(){
    
});
</script>

