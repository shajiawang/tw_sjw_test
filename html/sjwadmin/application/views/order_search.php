<SCRIPT language="JavaScript" type="text/javascript">
function order_cancel()
{
	$.ajax({
		url: '<?php echo base_url($this->ctrl_dir);?>',
		type: "GET",
		data: {'toajax':'act_cancel','order_no':<?=$order['order_no'];?> },
		async: false,
		dataType: "text",
		success: function(response) {
			$('#order_status').html('');
			$('#order_status').html(""+ response +"")
			$('#btn_cancel').attr('disabled', true);
		}
	});
}
</SCRIPT>

	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- about_area 內容區塊 ******************************-->
		<div class="orderform_area clearboth">
		<form class="" method="post" >
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
				
				<div class="orderfinish_area orderfinish_style clearboth">
					
					<div class="form_layout form_style">
						<header class="title_area clearfix">
							<hgroup class="title_content clearfix">
								<h4 class="main_title">訂單明細</h4>
								<h5 class="main_sub font_avant_garde">order details</h5>
							</hgroup>
							<hr class="main_hr">
						</header>
						<div class="form_group">
							<h4 class="txt_ordernum">訂單編號：<span class="font_montserrat"><?=$order['order_no'];?></span></h4>
						</div>
				
				<section class="cart_detail_area cart_detail_style">
				<header class="item_header">
					<div class="th_product css_th">品名</div>
					<div class="th_quantity css_th">數量</div>
					<div class="th_price css_th">單價</div>
					<div class="th_subtotal css_th">小計</div>
				</header>
			
				<div class="item_list item_row_same clearfix">
					<?php if(!empty($items)){
					foreach($items as $row)
					{ ?>
					<div class="item_productinfo css_tr clearfix">
						<div class="td_product css_td">
							<div class="item_product clearfix">
								<div class="item_thumbs">
									<img src="<?=$row['image_thumb_url'];?>" alt="" width="50" height="50">
								</div>
								<div class="item_info">
									<a href="<?=base_url('goods');?>/<?=$row['product_id'];?>/<?=$order['vendor_id'];?>" class="item_name text_overflow" >#<?=$row['product_id'];?> <?=$row['name'];?></a>
								</div>
							</div>
						</div>
						
						<div class="td_quantity css_td " data-quantity="數量："><?=$row['quantity'];?></div>
							
						<div class="td_price css_td"><div class="item_price font_montserrat"><?=$row['price'];?></div></div>
							
						<div class="td_subtotal css_td"><div class="item_subtotal font_montserrat" data-subtotal="小計："><?=$row['subtotal'];?></div></div>
					</div>
					<?php } } ?>
				</div>

				<div class="item_discount">
					<div class="item_total item_row">
						<div class="item_desc">總金額<span class="item_currency">(TWD)</span></div>
						<div class="item_money font_montserrat"><?=$order['total'];?></div>
					</div>
				</div>
			</section>
			</div>
					
			<div class="form_layout form_style clearfix">
						
				<header class="title_area clearfix">
					<hgroup class="title_content clearfix">
						<h4 class="main_title">訂單狀態</h4>
						<h5 class="main_sub font_avant_garde">order status</h5>
					</hgroup>
					<hr class="main_hr">
				</header>

				<div class="row">
				<div class="col-md-6">
					<div class="form_group">
						<label class="form_label" for="">訂購日期</label>
						<div class="form_data">
							<h5 class="form_info"><?=$order['create_at'];?></h5>
						</div>
					</div>
					<div class="form_group">
						<label class="form_label" for="">訂單編號</label>
						<div class="form_data">
							<h5 class="form_info form_import"><?=$order['order_no'];?></h5>
						</div>
					</div>
					<div class="form_group">
						<label class="form_label" for="">認證號碼</label>
						<div class="form_data">
							<h5 class="form_info form_import"><?=$order['pin'];?></h5>
						</div>
					</div>
				</div>
				
				<div class="col-md-6">
					<div class="form_group">
						<label class="form_label" for="">處理狀態</label>
						<div class="form_data">
							<h5 class="form_info form_import" ><span id='order_status'><?=$order['status'];?></span></h5>
						</div>
					</div>
				</div>
				</div>
		
		
				<header class="title_area clearfix">
					<hgroup class="title_content clearfix">
						<h4 class="main_title">訂單資訊</h4>
						<h5 class="main_sub font_avant_garde">order info</h5>
					</hgroup>
					<hr class="main_hr">
				</header>
						
				<div class="row">
						
						<div class="col-md-6">
							<div class="form_group">
								<label class="form_label" for="">運送方式</label>
								<div class="form_data">
									<h5 class="form_info"><?=$order['shipping'];?></h5>
								</div>
							</div>
							<div class="form_group">
								<label class="form_label" for="">付款方式</label>
								<div class="form_data">
									<h5 class="form_info"><?=$order['payment'];?></h5>
								</div>
							</div>
						</div>
						
						<div class="col-md-6">
							<div class="form_payinfo">
								<h4 class="form_subtitle">購買人資訊</h4>
								<div class="form_group">
									<label class="form_label" for="">訂購人姓名</label>
									<div class="form_data">
										<h5 class="form_info"><?=$order['buyer_name'];?></h5>
									</div>
								</div>
								<div class="form_group">
									<label class="form_label" for="">購買人電子信箱</label>
									<div class="form_data">
										<h5 class="form_info"><?=$order['buyer_email'];?></h5>
									</div>
								</div>
								<div class="form_group">
									<label class="form_label" for="">購買人聯絡電話</label>
									<div class="form_data">
										<h5 class="form_info"><?=$order['buyer_phone'];?></h5>
									</div>
								</div>
								<div class="form_group">
									<label class="form_label" for="">備註事項</label>
									<div class="form_data">
										<h5 class="form_info"><?=$order['remark'];?></h5>
									</div>
								</div>
							</div>
						</div>
					</div>
						
					<div class="form_group form_lastmargin">
							<div class="after_sales_area">
								<div class="row">
									<div class="col-md-3 col-md-offset-5 "></div>
									<div class="col-md-2">
										<button type="button" id="btn_cancel" class="btn btn_cancel btn_block btn_md " onclick="order_cancel();"><span>取消訂單</span></button>
									</div>
									<div class="col-md-2">
										<button type="button" class="btn btn_back btn_block btn_md js_back"><span>回首頁</span></button>
									</div>
								</div>
							</div>
					</div>
				</div>
			</div>
				
		</form>
		</div>
		<!--/ index_area 首頁內容區塊 ******************************-->
<script>
$(function() {
    $(".js_order_cancel").on("click", function(){ 
		window.location.href = "<?=base_url('order_query');?>"
	});
	
    $(".js_back").on("click", function(){ 
		window.location.href = "<?=$home_url;?>"
	});
});
</script>
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->
	
	
