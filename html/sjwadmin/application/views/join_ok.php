	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- area 內容區塊 ******************************-->
		<div class="about_area about_style clearboth clearfix">
		<div class="main_layout main_style">
				
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">註冊</h2>
						<h3 class="main_sub font_avant_garde">join us</h3>
					</span>
					<hr class="main_hr">
				</div>
				
				<div class="container">
				<div class="row" style="margin:0 0 20px 0px;">
					<div class="col-md-4 col-md-offset-4">
						<fieldset>
							<h3 class="sign-up-title" style="color:red; margin-bottom:20px;">歡迎您! 成為 SJ Mall 新會員</h3>
							
							<label class="form_label" for="field2" style="font-size:16px;">會員帳號</label>
							<input type="text" name="field2" value="<?=$member['account'];?>" class="form_input form_control text ui-widget-content ui-corner-all" readonly >
							
							<label class="form_label" for="field1" style="font-size:16px;">手機號碼</label>
							<input type="text" name="field1" value="<?=$member['telephone'];?>" class="form_input form_control text ui-widget-content ui-corner-all" readonly >
							
							<label class="form_label" for="field2" style="font-size:16px;">會員 E-mail</label>
							<input type="text" name="field2" value="<?=$member['email'];?>" class="form_input form_control text ui-widget-content ui-corner-all" readonly >

<?php	/*							
<label class="form_label" for="field3" style="font-size:16px;">個店網址</label>
<p><a href="<?=$member['url'];?>" id="field3"><?=$member['url'];?></a></p>
<p class="line"><a class="btn link_share btn_link_line btn_block" href="http://line.me/R/msg/text/?<?=$member['url'];?>"><i class="icon-line"></i> 分享</a></p>
<p id="qrcode"><img src="http://chart.apis.google.com/chart?cht=qr&amp;chl=<?php echo urlencode($member['url']);?>&amp;chs=200x200" alt="二維碼 QRcode"></p>
<input type="button" value="QRCode" id="qrcode" />
<div id="content">
	Google Chart API 是一組能讓使用者只透過 URL 傳遞參數就能快速產生各種圖表的 API。可產生的圖片類型包括線形圖、長條圖與圓餅圖，同時也能指定的每種圖片屬性，例如大小、色彩與標籤等。
</div>
*/?>
						</fieldset>
					</div>
			  </div>
			  </div>
		</div>
		</div>
		<!--/ area 內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

<script>
$(document).ready(function(){
    function QRCode(content, width, height){
		// 預設寬高為 120x120
		width = !!width ? width : 120 ;
		height = !!height ? height : 120;
		// 編碼
		content = encodeURIComponent(content);
		return 'http://chart.apis.google.com/chart?cht=qr&chl=' + content + '&chs=' + width + 'x' + height;
	}
});
</script>
