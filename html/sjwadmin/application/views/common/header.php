<!-- topbar 上層功能區塊 ******************************-->
	<div class="container">
		<div class="topbar_area topbar_style" role="topbar">

		<ul class="nav btn_navmobile pull-left">
			<li><a id="js_nav_toggle" class="js_slide_button nav_toggle icon-bars" href="javascript:void(0)"></a></li>
		</ul>
                
		<!-- top_menu 上層選單區塊 ******************************-->
		<aside class="topnav_area nav_area clearfix" role="top_menu">
			<ul class="nav_content clearfix js_nav_content ">
				<li class="nav_category clearfix" style="margin: 10px 5px;" ><!-- 用戶編號 -->
					<?php if(!empty($user_no)){ ?>
					<img src="data:image/png;base64,<?=$user_no['src'];?>" style="width:<?=$user_no['img_width'];?>; height:<?=$user_no['img_height'];?>; border: 0;" alt=" " />
					<?php } ?>
				</li>
				<li class="nav_category clearfix" style="margin: 10px 5px;" >
					<ul class="nav js_nav_color clearfix">
						<li class="nav_home "><a href="<?=base_url('welcome');?>" >SJ Mall</a></li>
						<li class="nav_home "><a href="<?=$home_url;?>" class="<?=$home_active;?>" style="">首頁</a></li>
						<li class="nav_about "><a href="<?=base_url('about_vendor');?>" class="<?=$about_active;?>" style="">關於</a></li>
						<li class="nav_allproducts dropdown_toggle "><a href="<?=$category_url;?>" class="<?=$allproduct_active;?>" style="">商品列表</a></li>
						
						<?php if ($this->flags->web_login === TRUE){ ?>
						<li class=" "><a href="<?=base_url('member');?>" class="<?=$member_active;?>" >會員專區</a></li>
						<li class=" "><a href="<?=base_url('logout');?>" >登出</a></li>
						<?php } else { ?>
						<li class=" "><a href="<?=base_url('login');?>" >登入</a></li>
						<li class=" "><a href="<?=base_url('login/joinus');?>" >註冊</a></li>
						<?php } ?>
					</ul>
				</li>
			</ul>
			
			<ul class="topbar_list clearfix">
				<?php /*<li class="top_nav_list"><a class="topbar_contactus tooltip" data-title="聯絡我們" href="<?=base_url('contact_vendor');?>"><i class="icon-email"></i></a></li>*/?>
				<li class="top_nav_list"><a class="topbar_edm tooltip js_subscribe" data-title="訂閱電子報" href="#"><i class="icon-news"></i></a></li>
				<li class="top_nav_list"><a class="topbar_buying tooltip" data-title="服務說明" href="<?=base_url('faq_vendor');?>"><i class="icon-info"></i></a></li>
				<li class="top_nav_list"><a class="topbar_ordersearch tooltip" data-title="訂購查詢" href="<?=base_url('order_query');?>"><i class="icon-text-document"></i></a></li>
			</ul>
			<?php /*
			<ul class="top_share_list share_area clearfix">
				<li style="margin: 0 5px;"><a href="" class="i_facebook" rel="nofollow"><i class="icon-facebook"></i></a></li>
				<li style="margin: 0 5px;"><a href="" class="i_twitter" rel="nofollow"><i class="icon-twitter"></i></a></li>
			</ul>*/?>
		</aside>
		<!--/ top_menu 上層選單區塊 ******************************-->

		<!-- function_area 浮動功能區塊 ******************************-->
		<ul class="function_area list-inline">
			<li class="clearfix ">
				<a id="js_cart_toggle" class="js_slide_button cart_area" href="<?=base_url('cart');?>">
				<i class="icon-shopping-basket"></i>
				<div class="cart_number"><span class="font_montserrat js_cart_number"><?=$_cart['item'];?></span></div>
				</a>
			</li>
			<li class="clearfix">
				<form class="search_area" action="<?=base_url('category');?>" >
					<input id="js_form_search" name="search_keyword" class="search_input" placeholder="搜尋" type="text">
					<label for="js_form_search"><i class="icon-search"></i></label>
				</form>
			</li>
		</ul>
		<!--/ function_area 浮動功能區塊 ******************************-->
		
		</div>
	</div>
<!--/ topbar 上層功能區塊 ******************************-->


<!-- header_area 頁首內容區塊 ******************************-->
	<header class="header_area clearfix" role="header">
	<div class="container">
		<!-- logo_area 商標區塊 ******************************-->
		<div class="brand_area">
			<?php /*
			<div class="logo_area " style="text-align:center;">
				<a href="<?=$home_url;?>" class="clearfix">
				<div class="logo_company_thumb">
					<img src="<?=$static_img;?>472f51e0031002bd46b55693a3408ada.png" alt="">
				</div>
				<div class="logo_company_heading"><!--class="logo_company_name"-->
					<h1 >Exchange of Things</h1>
				</div>
				</a>
			</div>
			*/?>
			<div class="logo_area ">
			<h2 class="middle_content">
			<a href="<?=$home_url;?>" class="clearfix">
			<?=$_SETTING['web_site_name'];?>
			</a>
			</h1>
			</div>
		</div>
		<!--/ logo_area 商標區塊 ******************************-->
	</div>
	</header>
<!--/ header_area 頁首內容區塊 ******************************-->
