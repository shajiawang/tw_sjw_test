<aside class="contactus_area contactus_style " style="" role="contactus">
    <div class="contactus_block">
		<ul class="contactus_inner clearfix list-unstyled">
			<li>
				<a class="icon_facebook_msg icon_comments_same js_btn_contactus" data-msg="fbmsg" data-status="hide" href="#"><img src="icon_fb_messenger.svg" alt=""></a>
			</li>
		</ul>
	</div>
	
	<div class="contactus_slide_block js_contactus_area" style="bottom: -442px;">
		<div class="contactus_content js_contactus_content">
			<div class="js_msg_block hide" data-comment="fbmsg">
				<div class="contactus_topbar clearfix contactus_fb_title">
					<div class="contactus_title pull-left">
						<i class="icon-facebook-messenger"></i><span>發送訊息</span>
					</div>
					<a href="#" class="btn_contactus_close js_btn_contactus" data-status="hide" data-agent="pc"><i class="icon-minus"></i></a>
				</div>
			</div>

			<!--/ contactus 主要內容區塊 ******************************-->
			<div class="js_msg_block hide" data-comment="contactmsg">
				<div class="contactus_topbar clearfix">
					<div class="contactus_title pull-left">
						<i class="icon-email"></i><span>聯絡我們</span>
					</div>
					<a href="#" class="btn_contactus_close js_btn_contactus" data-status="hide" data-agent="pc"><i class="icon-minus"></i></a>
				</div>
				<form class="contactus_form" id="form_contact" action="post">
					<div class="form_group">
						<div class="row">
							<div class="col-md-4 col-xs-4">
								<label class="form_label" for="">姓名</label>
							</div>
							<div class="col-md-8 col-xs-8">
								<div class="form_data">
									<input id="js_contact_name" name="name" class="form_input form_control" placeholder="請輸入姓名" type="text">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
								</div>
								<div class="help_block"><i class="icon-exclamation-circle"></i><span>必填欄位，不得為空白。</span></div>
							</div>
						</div>
					</div>
					<div class="form_group">
						<div class="row">
							<div class="col-md-4 col-xs-4">
								<label class="form_label" for="">電子信箱</label>
							</div>
							<div class="col-md-8 col-xs-8">
								<div class="form_data">
									<input id="js_contact_email" name="email" class="form_input form_control" placeholder="請輸入電子信箱" type="text">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
								</div>
								<div class="help_block"><i class="icon-exclamation-circle"></i><span>必填欄位，不得為空白。</span></div>
							</div>
						</div>
					</div>
					<div class="form_group">
						<div class="row">
							<div class="col-md-4 col-xs-4">
								<label class="form_label" for="">聯絡電話</label>
							</div>
							<div class="col-md-8 col-xs-8">
								<div class="form_data">
									<input id="js_contact_phone" name="phone" class="form_input form_control" placeholder="請輸入聯絡電話" type="text">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
								</div>
								<div class="help_block"><i class="icon-exclamation-circle"></i><span>聯絡電話只能為數字。</span></div>
							</div>
						</div>
					</div>
					<div class="form_group">
						<div class="row">
							<div class="col-md-4 col-xs-4">
								<label class="form_label" for="">訂單編號</label>
							</div>
							<div class="col-md-8 col-xs-8">
								<div class="form_data">
									<input id="js_contact_name" name="order_no" class="form_input form_control" placeholder="請輸入訂單編號(選填)" type="text">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
								</div>
							</div>
						</div>
					</div>
					<div class="form_group">
						<div class="row">
							<div class="col-md-12 col-xs-4">
								<label class="form_label" for="">詢問內容</label>
							</div>
							<div class="col-md-12 col-xs-8">
								<div class="form_data">
									<textarea id="js_contact_memo" name="memo" class="form_textarea form_control" placeholder="請輸入您想對店家說的話..." cols="30" rows="3" required=""></textarea>
								</div>
								<div class="help_block"><i class="icon-exclamation-circle"></i><span>必填欄位，不得為空白。</span></div>
							</div>
						</div>
					</div>
					<div class="form_group form_lastmargin">
						<div class="row">
							<div class="col-md-5 col-md-offset-7 col-sm-12">
								<div class="form_group form_lastmargin">
									<input name="_token" value="9aDsWPIFprHIsxNh2vODsziIY0d2OAzmUOEBeN3v" type="hidden">
									<button type="button" id="js_contact_submit" class="btn btn_submit btn_block btn_md"><span>確定送出</span></button>
								</div>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</aside>