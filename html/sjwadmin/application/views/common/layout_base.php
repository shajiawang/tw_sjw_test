<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="content-language" content="zh-TW">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-touch-fullscreen" content="yes">

	<title><?=$_SETTING['web_site_title'];?></title>
	<meta name="description" content="<?=$_SETTING['web_site_description'];?>">
	<meta name="keywords" content="<?=$_SETTING['web_site_keywords'];?>">
	
	<meta property="og:title" content="<?=$_SETTING['web_og_title'];?>">
	<meta property="og:description" content="<?=$_SETTING['web_og_description'];?>">
	<meta property="og:image" content="<?=$_SETTING['web_og_image'];?>">
	<meta property="og:url" content="<?=$_SETTING['web_og_url'];?>">
	<meta property="og:site_name" content="<?=$_SETTING['web_og_sitename'];?>">
	<meta property="og:type" content="website">

	<?php /*
	<link rel="alternate" href="/" hreflang="zh-Hant">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?=$static_img;?>apple-touch-icon-144.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?=$static_img;?>apple-touch-icon-114.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?=$static_img;?>apple-touch-icon-72.png">
	<link rel="apple-touch-icon-precomposed" href="<?=$static_img;?>apple-touch-icon-57.png">
	*/?>
	<link rel="shortcut icon" href="<?=$static_img;?>favicon.png">
	<link rel="shortcut icon" href="<?=$static_img;?>favicon.ico" type="image/x-icon">

    <!-- Bootstrap Core CSS -->
    <link href="<?=$static_plugin;?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<?php /*
	<link href="<?=$static_css;?>bootstrap_v3.3.5.css" rel="stylesheet">
	*/?>
	<!-- Custom Fonts -->
    <link href="<?=$static_plugin;?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!-- Animate Core CSS -->
    <link href="<?=$static_plugin;?>animate/animate.css" rel="stylesheet">
    <!-- Bootstrap DateTime and Date Picker CSS -->
    <link href="<?=$static_plugin;?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="<?=$static_plugin;?>bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="<?=$static_plugin;?>metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <!-- msdropdown CSS -->
    <link href="<?=$static_plugin;?>msdropdown/css/msdropdown/dd.css" rel="stylesheet">

    <!-- Custom CSS -->
    <?php /*
	<link href="<?=$static_css;?>ec/sb-admin-2.css" rel="stylesheet">
	<link href="<?=$static_css;?>ec/calendar.css" rel="stylesheet">
	*/?>
	<link href="<?=$static_css;?>confirm.css" rel="stylesheet" >
	<link href="<?=$static_css;?>photoswipe.css" rel="stylesheet" >
	<link href="<?=$static_css;?>default-skin.css" rel="stylesheet" >
	<link href="<?=$static_css;?>swiper.css" rel="stylesheet" >

    <link href="<?=$static_css;?>frontend.css" rel="stylesheet" >
	<link href="<?=$static_css;?>font-face.css" rel="stylesheet" >
	<link href="<?=$static_css;?>jquery-ui-1.12.1.css" rel="stylesheet" >


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if IE]>
		<script src="<?=$static_js;?>PIE.min.js"></script>
	<![endif]-->
	<!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

	<!-- jQuery -->
	<script src="<?=$static_js;?>jquery-2.1.4.min.js"></script>
	<script src="<?=$static_js;?>jquery.menu.min.js"></script>
	<script src="<?=$static_js;?>jquery.confirm.min.js"></script>
	<script src="<?=$static_js;?>jquery-ui-1.12.1.js"></script>
	<script src="<?=$static_js;?>jquery.matchHeight.min.js" type="text/javascript" ></script>

	<!-- Bootstrap Core JavaScript -->
    <script src="<?=$static_plugin;?>bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?=$static_js;?>moment-with-locales.js"></script>
	<script src="<?=$static_js;?>bootstrap-datetimepicker.js"></script>
	<script src="<?=$static_js;?>masonry.js"></script>
	<script src="<?=$static_js;?>swiper.js" type="text/javascript" ></script>
	
	<?php /*<!-- Metis Menu Plugin JavaScript -->
    <script src="<?=$static_plugin;?>metisMenu/dist/metisMenu.min.js"></script>
	*/?>

    <!-- Noty jquery notification plugin -->
    <script src="<?=$static_plugin;?>noty/packaged/jquery.noty.packaged.min.js"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?=$static_plugin;?>jquery.mousewheel-3.0.6.pack.js"></script>

    <script type="text/javascript">
	<?php if (isset($_JSON)){ ?>
        var MY = MY || <?=json_encode($_JSON);?> || {};
    <?php } ?>
	
	var index_url = '<?=$home_url;?>';
	var payment_url = '<?=base_url('cart');?>';
	var cart_url = '<?=base_url('cart');?>';
	var contact_url = '<?=base_url('contact');?>';
	
	var payment_options = [{"id":"141","type_id":"14","vender":"Shop","payment":"none","name_tw":"\u7121\u9808\u4ed8\u6b3e","name_cn":"\u65e0\u9700\u4ed8\u6b3e","name_en":"Free Of Charge","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"110","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u7121\u9808\u4ed8\u6b3e","alias_name":null},{"id":"111","type_id":"11","vender":"Shop","payment":"face_to_face","name_tw":"\u7576\u9762\u4ed8\u6b3e","name_cn":"\u5f53\u9762\u4ed8\u6b3e","name_en":"Meet In Person","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"100","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u7576\u9762\u4ed8\u6b3e","alias_name":null},{"id":"121","type_id":"12","vender":"Shop","payment":"atm","name_tw":"\u8f49\u5e33\u532f\u6b3e","name_cn":"\u8f6c\u5e10\u6c47\u6b3e","name_en":"Bank Transfer","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"90","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u8f49\u5e33\u532f\u6b3e","alias_name":null},{"id":"131","type_id":"13","vender":"Shop","payment":"cash","name_tw":"\u5b85\u914d\u4ee3\u6536","name_cn":"\u5b85\u914d\u4ee3\u6536","name_en":"Payment On Delivery","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"80","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u5b85\u914d\u4ee3\u6536","alias_name":null},{"id":"311","type_id":"31","vender":"LinePay","payment":"mobile","name_tw":"LINE Pay (VISA & MasterCard)","name_cn":null,"name_en":null,"alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"70","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"LINE Pay (VISA & MasterCard)","alias_name":null},{"id":"321","type_id":"32","vender":"Cathay","payment":"card","name_tw":"\u4fe1\u7528\u5361\u4ed8\u6b3e","name_cn":null,"name_en":"Credit Card","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"60","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u4fe1\u7528\u5361\u4ed8\u6b3e","alias_name":null},{"id":"331","type_id":"33","vender":"EzShip","payment":"ezship","name_tw":"ezShip\u8d85\u5546\u53d6\u8ca8\u4ed8\u6b3e","name_cn":"ezShip\u8d85\u5546\u53d6\u8d27\u4ed8\u6b3e","name_en":"ezShip Convenience Store Pickup","alias_name_tw":"\u8d85\u5546\u53d6\u8ca8\u4ed8\u6b3e","alias_name_cn":"\u8d85\u5546\u53d6\u8d27\u4ed8\u6b3e","alias_name_en":"Convenience Store Pickup","TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"50","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"ezShip\u8d85\u5546\u53d6\u8ca8\u4ed8\u6b3e","alias_name":"\u8d85\u5546\u53d6\u8ca8\u4ed8\u6b3e"},{"id":"355","type_id":"35","vender":"AllPay","payment":"card","name_tw":"\u6b50\u4ed8\u5bf6allPay \u4fe1\u7528\u5361","name_cn":"\u6b27\u4ed8\u5b9dallPay \u4fe1\u7528\u5361","name_en":"allPay Credit Card","alias_name_tw":"\u4fe1\u7528\u5361","alias_name_cn":null,"alias_name_en":"Credit Card","TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"35","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u6b50\u4ed8\u5bf6allPay \u4fe1\u7528\u5361","alias_name":"\u4fe1\u7528\u5361"},{"id":"351","type_id":"35","vender":"AllPay","payment":"webatm","name_tw":"\u6b50\u4ed8\u5bf6allPay WebATM","name_cn":"\u6b27\u4ed8\u5b9dallPay WebATM","name_en":"allPay WebATM","alias_name_tw":"WebATM","alias_name_cn":null,"alias_name_en":null,"TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"33","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u6b50\u4ed8\u5bf6allPay WebATM","alias_name":"WebATM"},{"id":"354","type_id":"35","vender":"AllPay","payment":"barcode","name_tw":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u689d\u78bc","name_cn":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u689d\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","name_en":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u689d\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","alias_name_tw":"\u8d85\u5546\u689d\u78bc","alias_name_cn":"\u8d85\u5546\u689d\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","alias_name_en":"\u8d85\u5546\u689d\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"32","status":"0","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u689d\u78bc","alias_name":"\u8d85\u5546\u689d\u78bc"},{"id":"353","type_id":"35","vender":"AllPay","payment":"cvs","name_tw":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u4ee3\u78bc","name_cn":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u4ee3\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","name_en":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u4ee3\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","alias_name_tw":"\u8d85\u5546\u4ee3\u78bc","alias_name_cn":"\u8d85\u5546\u4ee3\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","alias_name_en":"\u8d85\u5546\u4ee3\u78bc\uff08\u53f0\u7063\u9650\u5b9a\uff09","TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"31","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u6b50\u4ed8\u5bf6allPay \u8d85\u5546\u4ee3\u78bc","alias_name":"\u8d85\u5546\u4ee3\u78bc"}];
	var shipping_options = {"211":{"id":"211","vender":"shop","name_tw":"\u5be6\u9ad4\u9580\u5e02\u53d6\u8ca8","name_cn":"\u5b9e\u4f53\u95e8\u5e02\u53d6\u8d27","name_en":"In-store Pickup","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"has_address":"0","in_taiwan":"1","shipping_type":"other","TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"100","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u5be6\u9ad4\u9580\u5e02\u53d6\u8ca8","alias_name":null,"setting":[{"name":"WACA\u9580\u5e02","address":"WACA\u9580\u5e02","name_lang1":"WACA\u9580\u5e02","address_lang1":"WACA\u9580\u5e02","description_lang1":"","description":""}]},"111":{"id":"111","vender":"home","name_tw":"\u4e00\u822c\u5b85\u914d","name_cn":null,"name_en":"Home Delivery","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"has_address":"1","in_taiwan":"1","shipping_type":"home_delivery","TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"90","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u4e00\u822c\u5b85\u914d","alias_name":null,"setting":{"time":"1","description_lang1":"","description":""}},"131":{"id":"131","vender":"711","name_tw":"7-11\u53d6\u8ca8\u4e0d\u4ed8\u6b3e","name_cn":"7-11\u53d6\u8d27\u4e0d\u4ed8\u6b3e","name_en":"7-11 Pickup (Pickup Only)","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"has_address":"0","in_taiwan":"1","shipping_type":"other","TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"80","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"7-11\u53d6\u8ca8\u4e0d\u4ed8\u6b3e","alias_name":null,"setting":{"description_lang1":"","description":""}},"221":{"id":"221","vender":"ezship","name_tw":"ezShip\u8d85\u5546\u53d6\u8ca8","name_cn":"ezShip\u8d85\u5546\u53d6\u8d27","name_en":"ezShip Convenience Store Pickup","alias_name_tw":"\u8d85\u5546\u53d6\u8ca8","alias_name_cn":"\u8d85\u5546\u53d6\u8d27","alias_name_en":"Convenience Store Pickup","has_address":"0","in_taiwan":"1","shipping_type":"convenience_store","TWD":"1","USD":"0","JPY":"0","MYR":"0","SGD":"0","VND":"0","HKD":"0","MOP":"0","PHP":"0","THB":"0","sort":"70","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"ezShip\u8d85\u5546\u53d6\u8ca8","alias_name":"\u8d85\u5546\u53d6\u8ca8","setting":{"description":"","condition_number":"","condition_type":"","logistics":""}},"121":{"id":"121","vender":"cash","name_tw":"\u8ca8\u5230\u4ed8\u6b3e","name_cn":"\u8d27\u5230\u4ed8\u6b3e","name_en":"Payment On Delivery","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"has_address":"1","in_taiwan":"1","shipping_type":"home_delivery","TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"60","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u8ca8\u5230\u4ed8\u6b3e","alias_name":null,"setting":{"description_lang1":"","description":""}},"231":{"id":"231","vender":"international","name_tw":"\u570b\u969b\u5feb\u905e","name_cn":"\u56fd\u9645\u5feb\u9012","name_en":"International Express","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"has_address":"1","in_taiwan":"0","shipping_type":"home_delivery","TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":"50","status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u570b\u969b\u5feb\u905e","alias_name":null,"setting":[{"country":"cn","description_lang1":"","description":""},{"country":"hk","description_lang1":"","description":""},{"country":"mo","description_lang1":"","description":""}]},"custom_duYcIG":{"id":"custom_duYcIG","vender":"custom","name_tw":"\u81ea\u8a02\u904b\u9001","name_cn":"\u81ea\u8ba2\u8fd0\u9001","name_en":"Custom Shipping","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"has_address":"1","in_taiwan":"1","shipping_type":"other","TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":29,"status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u4f4e\u6eab\u904b\u9001","alias_name":null,"setting":{"id":"custom_duYcIG","description":"\u4f4e\u6eab\u904b\u9001","has_address":"1","in_taiwan":"1","has_cash":"0","time":"0","date":"0","twland_not_send":"0","is_holiday_send":"0","logistics":null,"condition_number":null,"condition_type":null}},"custom_j703vA":{"id":"custom_j703vA","vender":"custom","name_tw":"\u81ea\u8a02\u904b\u9001","name_cn":"\u81ea\u8ba2\u8fd0\u9001","name_en":"Custom Shipping","alias_name_tw":null,"alias_name_cn":null,"alias_name_en":null,"has_address":"0","in_taiwan":"1","shipping_type":"other","TWD":"1","USD":"1","JPY":"1","MYR":"1","SGD":"1","VND":"1","HKD":"1","MOP":"1","PHP":"1","THB":"1","sort":28,"status":"1","created_at":"2016-01-26 08:05:56","updated_at":"2016-01-26 08:05:56","name":"\u6a5f\u8eca\u914d\u9001","alias_name":null,"setting":{"id":"custom_j703vA","description":"\u6a5f\u8eca\u914d\u9001","has_address":"0","in_taiwan":"1","has_cash":"0","time":"0","date":"0","twland_not_send":"0","is_holiday_send":"0","logistics":null,"condition_number":null,"condition_type":null}}};
	var cart_number = 1;
	var shops_payment_setting = {"33":{"min_price":"","max_price":""},"35":{"installments":["3","6","12","18","24"],"min_price":null,"max_price":null}};
	var shops_additional = [];
	var product_id = 0;
	var is_multiple = 0;
	var soldout_notify_url = '<?=base_url('product/soldoutnotifyajax');?>';
	var browsing_history_data = {"id":"0","name":"","image":""};
	var s_id = 0;
	var hasShopsDial = 0;
	var has_invoice = false;
	var invoice_type = '';
	var shops_name = '商店體驗';
	var receiver_default = {
		receiver_city : '',
		receiver_district : '',
		receiver_zip : ''
	}
	var allpay_collection_map = {"356":242,"357":243,"358":244,"359":245};
	var ecpay_collection_map = {"386":242,"387":243,"388":244,"389":245,"390":246,"391":247};
	var validator_error = [];
	var is_inquiry = 0;
	var merge_pre_order = 1;
	var lang = 'lang1';

	var shops_logo = '<?=$static_img;?>472f51e0031002bd46b55693a3408ada.png';
	var shops_currencies = 'TWD';
	var currency_decimals = '0';
	var currency_icon = '$';
	var check_order_count = '0';
	var order_email = '1';
	var member_default_data = [];
	var cart_redeem = '0';
	var country_list = '[{"value":"\u8acb\u9078\u64c7","country_id":""},{"value":"\u4e2d\u570b\u5927\u9678 China","country_id":"cn"},{"value":"\u9999\u6e2f Hong Kong","country_id":"hk"},{"value":"\u6fb3\u9580 Macau","country_id":"mo"},{"value":"Singapore","country_id":"sg"},{"value":"Malaysia","country_id":"my"},{"value":"Japan","country_id":"jp"},{"value":"Canada","country_id":"ca"},{"value":"Australia","country_id":"au"},{"value":"United States of America (U.S.A.)","country_id":"us"},{"value":"Afghanistan","country_id":"af"},{"value":"Albania","country_id":"al"},{"value":"Algeria","country_id":"dz"},{"value":"American samoa(u.s.a.)","country_id":"as"},{"value":"Andorra","country_id":"ad"},{"value":"Angola","country_id":"ao"},{"value":"Argentina","country_id":"ar"},{"value":"Aruba","country_id":"aw"},{"value":"Austria","country_id":"at"},{"value":"Bahamas","country_id":"bs"},{"value":"Bahrain","country_id":"bh"},{"value":"Bangladesh","country_id":"bd"},{"value":"Barbados","country_id":"bb"},{"value":"Belarus","country_id":"by"},{"value":"Belgium","country_id":"be"},{"value":"Belize","country_id":"bz"},{"value":"Benin","country_id":"bj"},{"value":"Bermuda is.","country_id":"bm"},{"value":"Bhutan","country_id":"bt"},{"value":"Bolivia","country_id":"bo"},{"value":"Bosnia-herzegovina rep.","country_id":"ba"},{"value":"Botswana","country_id":"bw"},{"value":"Brazil","country_id":"br"},{"value":"Brunei","country_id":"bn"},{"value":"Bulgaria","country_id":"bg"},{"value":"Burkina faso","country_id":"bf"},{"value":"Cambodia(kampuchea)","country_id":"kh"},{"value":"Cameroon","country_id":"cm"},{"value":"Cayman islands","country_id":"ky"},{"value":"Chile","country_id":"cl"},{"value":"Colombia","country_id":"co"},{"value":"Congo","country_id":"cg"},{"value":"Costa rica","country_id":"cr"},{"value":"Croatia","country_id":"hr"},{"value":"Cyprus","country_id":"cy"},{"value":"Czech rep.","country_id":"cz"},{"value":"Czechoslovakia","country_id":"cs"},{"value":"Denmark","country_id":"dk"},{"value":"Dominica","country_id":"dm"},{"value":"Dominican rep.","country_id":"do"},{"value":"Ecuador","country_id":"ec"},{"value":"Egypt","country_id":"eg"},{"value":"El salvador","country_id":"sv"},{"value":"Estonia","country_id":"ee"},{"value":"Ethiopia","country_id":"et"},{"value":"Fiji","country_id":"fj"},{"value":"Finland","country_id":"fi"},{"value":"France","country_id":"fr"},{"value":"Gabon","country_id":"ga"},{"value":"Gambia","country_id":"gm"},{"value":"Germany","country_id":"de"},{"value":"Ghana","country_id":"gh"},{"value":"Grenada","country_id":"gd"},{"value":"Guadeloupe","country_id":"gp"},{"value":"Guam","country_id":"gu"},{"value":"Guatemala","country_id":"gt"},{"value":"Guinea","country_id":"gn"},{"value":"Guyana","country_id":"gy"},{"value":"Haiti","country_id":"ht"},{"value":"Hellenic(greek)","country_id":"gr"},{"value":"Honduras","country_id":"hn"},{"value":"Hungary","country_id":"hu"},{"value":"Iceland","country_id":"is"},{"value":"India","country_id":"in"},{"value":"Indonesia","country_id":"id"},{"value":"Iran","country_id":"ir"},{"value":"Iraq","country_id":"iq"},{"value":"Ireland","country_id":"ie"},{"value":"Israel","country_id":"il"},{"value":"Italy","country_id":"it"},{"value":"Ivory coast","country_id":"ci"},{"value":"Jamaica","country_id":"jm"},{"value":"Jordan","country_id":"jo"},{"value":"Kazakhstan","country_id":"kz"},{"value":"Kenya","country_id":"ke"},{"value":"Kiribati rep.","country_id":"ki"},{"value":"Kosovo","country_id":"rk"},{"value":"Kuwait","country_id":"kw"},{"value":"Laos","country_id":"la"},{"value":"Latvia","country_id":"lv"},{"value":"Lebanon rep.","country_id":"lb"},{"value":"Liechtenstein","country_id":"li"},{"value":"Lithuania","country_id":"lt"},{"value":"Luxembourg","country_id":"lu"},{"value":"Macedonia rep.","country_id":"mk"},{"value":"Madagascar","country_id":"mg"},{"value":"Malawi","country_id":"mw"},{"value":"Maldives","country_id":"mv"},{"value":"Mali rep.","country_id":"ml"},{"value":"Malta","country_id":"mt"},{"value":"Marshall islands","country_id":"mh"},{"value":"Martinique island","country_id":"mq"},{"value":"Mauritius","country_id":"mu"},{"value":"Mexico","country_id":"mx"},{"value":"Micronesia","country_id":"fm"},{"value":"Moldova rep.","country_id":"md"},{"value":"Monaco","country_id":"mc"},{"value":"Mongolia","country_id":"mn"},{"value":"Morocco","country_id":"ma"},{"value":"Mozambique","country_id":"mz"},{"value":"Myanmar(burma)","country_id":"mm"},{"value":"Namibia","country_id":"na"},{"value":"Nauru rep.","country_id":"nr"},{"value":"Nepal","country_id":"np"},{"value":"Netherlands","country_id":"nl"},{"value":"New Caledonia","country_id":"nc"},{"value":"New Zealand","country_id":"nz"},{"value":"Nicaragua","country_id":"ni"},{"value":"Niger","country_id":"ne"},{"value":"Nigeria","country_id":"ng"},{"value":"North Korea","country_id":"kp"},{"value":"Norway","country_id":"no"},{"value":"Pakistan","country_id":"pk"},{"value":"Palau","country_id":"pw"},{"value":"Panama","country_id":"pa"},{"value":"Paraguay","country_id":"py"},{"value":"Peru","country_id":"pe"},{"value":"Philippines","country_id":"ph"},{"value":"Poland","country_id":"pl"},{"value":"Portugal","country_id":"pt"},{"value":"Puerto rico","country_id":"pr"},{"value":"Pupua new guinea","country_id":"pg"},{"value":"Qatar","country_id":"qa"},{"value":"R.S.A.(south africa)","country_id":"za"},{"value":"Republic of Armenia","country_id":"am"},{"value":"Reunion island","country_id":"re"},{"value":"Romania","country_id":"ro"},{"value":"Russia","country_id":"ru"},{"value":"Saipan mariana is.","country_id":"mp"},{"value":"San marino","country_id":"sm"},{"value":"Sao tome e principe","country_id":"st"},{"value":"Saudi arabia","country_id":"sa"},{"value":"Senegal","country_id":"sn"},{"value":"Seychelles","country_id":"sc"},{"value":"Slovak","country_id":"sk"},{"value":"Slovenia","country_id":"si"},{"value":"Solomon island","country_id":"sb"},{"value":"South korea","country_id":"kr"},{"value":"Spain(espana)","country_id":"es"},{"value":"Sri lanka","country_id":"lk"},{"value":"St. kitts st. nevis","country_id":"kn"},{"value":"St. lucia","country_id":"lc"},{"value":"St. vincent","country_id":"vc"},{"value":"Sudan","country_id":"sd"},{"value":"Surinam rep.","country_id":"sr"},{"value":"Swazilamd","country_id":"sz"},{"value":"Sweden","country_id":"se"},{"value":"Switzerland","country_id":"ch"},{"value":"Syria","country_id":"sy"},{"value":"Tahiti (french polynesia)","country_id":"pf"},{"value":"Tajikistan","country_id":"tj"},{"value":"Tanzania","country_id":"tz"},{"value":"Thailand","country_id":"th"},{"value":"Togo rep.","country_id":"tg"},{"value":"Tonga","country_id":"to"},{"value":"Trinidad &amp; tobago","country_id":"tt"},{"value":"Tunisia","country_id":"tn"},{"value":"Turkey","country_id":"tr"},{"value":"Turks and caicos is.","country_id":"tc"},{"value":"Tuvalu","country_id":"tv"},{"value":"U.A.E.","country_id":"ae"},{"value":"U.S.A. virgin island","country_id":"vi"},{"value":"Uganda","country_id":"ug"},{"value":"Ukraine","country_id":"ua"},{"value":"United Kingdom","country_id":"gb"},{"value":"Uruguay","country_id":"uy"},{"value":"Uzbekistan","country_id":"uz"},{"value":"Vanuatu","country_id":"vu"},{"value":"Vatican","country_id":"va"},{"value":"Venezuela","country_id":"ve"},{"value":"Vietnam","country_id":"vn"},{"value":"Western Samoa","country_id":"ws"},{"value":"Yemen Rep.","country_id":"ye"},{"value":"Yugoslavia","country_id":"yu"},{"value":"Zaire","country_id":"zr"},{"value":"Zambia","country_id":"zm"},{"value":"Zimbabwe","country_id":"zw"}]';
	var getLocale = 'tw';

	// 是否限制加購數量
	var shops_add_price_restrict_qty = 1;
	var shops = '{"urls":{"productlist":"/productlist"},"sliders":{"1":{"id":"87","area":"1","slideshow":"0","full_width":"0","spacing":"0","distance":"0","number":"1","status":"0","detail":[{"id":"345","banner_layout_id":"87","images_id_lang1":"","images_id_lang2":"0","images_id_lang3":"0","url_lang1":"","url_lang2":null,"url_lang3":null,"sort":"0","created_at":"2016-01-26 08:05:57","updated_at":"2016-01-26 08:05:57","image_lang1":"","url":"","image":"","images_id":""}]},"2":{"id":"88","area":"2","slideshow":"0","full_width":"0","spacing":"0","distance":"0","number":"1","status":"0","detail":[{"id":"349","banner_layout_id":"88","images_id_lang1":"","images_id_lang2":"0","images_id_lang3":"0","url_lang1":"","url_lang2":null,"url_lang3":null,"sort":"0","created_at":"2016-01-26 08:05:57","updated_at":"2016-01-26 08:05:57","image_lang1":"","url":"","image":"","images_id":""}]}},"message":"","type":null,"value":null,"products_total":24,"is_backend":false,"is_fb":false,"shops_name":"WACA\u5546\u5e97\u9ad4\u9a57 X \u9280 Ying \u6212 Chieh","shops_popup":{"status":0,"type":"","img_height":500,"img_url":"","edm_content":""}}';
	var locale = '<?=base_url('');?>';
	var default_locale = 'tw';
	var request_uri = '/';
	var is_fb = '0';
	var is_mobile = '0';
	var currency_lang_url = '<?=base_url('currencylang');?>';
	var shops_currency_decimals = parseInt(0);
	shops_currency_decimals = isNaN(shops_currency_decimals) ? 0 : shops_currency_decimals;
	<?php /*
	// 語言	Lang.setLocale('zh-Hant');
	// 未滿18歲提醒
	//var shops_forbidden18 = {"has_install":true,"shops_setting":"0","is_backend":false,"icon_url":"/frontend\/images\/r18.svg"};
	var shops = '{"urls":{"productlist":"productlist"},"sliders":{"1":{"id":"87","area":"1","slideshow":"0","full_width":"0","spacing":"0","distance":"0","number":"1","status":"0","detail":[{"id":"345","banner_layout_id":"87","images_id_lang1":"","images_id_lang2":"0","images_id_lang3":"0","url_lang1":"","url_lang2":null,"url_lang3":null,"sort":"0","created_at":"2016-01-26 08:05:57","updated_at":"2016-01-26 08:05:57","image_lang1":"","url":"","image":"","images_id":""}]},"2":{"id":"88","area":"2","slideshow":"0","full_width":"0","spacing":"0","distance":"0","number":"1","status":"0","detail":[{"id":"349","banner_layout_id":"88","images_id_lang1":"","images_id_lang2":"0","images_id_lang3":"0","url_lang1":"","url_lang2":null,"url_lang3":null,"sort":"0","created_at":"2016-01-26 08:05:57","updated_at":"2016-01-26 08:05:57","image_lang1":"","url":"","image":"","images_id":""}]}},"message":"","type":null,"value":null,"products_total":24,"is_backend":false,"is_fb":false,"shops_name":"WACA\u5546\u5e97\u9ad4\u9a57 X \u9280 Ying \u6212 Chieh","shops_popup":{"status":0,"type":"","img_height":500,"img_url":"","edm_content":""}}';
	*/?>
	
	</script>
	<style>
		/* Dialog-Form Style */
		fieldset { padding:0; border:0; margin-top:20px; }
		.validateTips { border: 1px solid transparent; padding: 0.3em; }
		.ui-dialog .ui-state-error { padding: .3em; }
		label, input { display:block; }
		input.text { margin-bottom:12px; width:95%; padding: .4em; }
	</style>
</head>

<body class=" themesblack_style  tw" style="background: url('<?php echo $_SETTING['web_back_image'] .'?t='. time();?>') top left repeat fixed #ffffff;">
<!-- container_wrapper 主體包裝層區塊 ******************************-->
<div id="js_nav_wrapper" class="nav_wrapper nav_shoppingcart nav_mobilecategory nav_mobilepay ">
<!-- wrapper 包裝層區塊 ******************************-->
<div class="wrapper ">	
	<div class="content js_header_layout header_a">
	
	<?php 
		// Loading header page  
		//echo $__header;
		include_once "header.php";
	?>
	
	<?php
		// Loading inner content page
		echo $__content;
	?>
	
	<div class="layout_footer">
	<?php 
		// Loading footer page 
		//echo $__footer;
		include_once "footer.php";
	?>
	</div>
	<button class="gotop_area" id="js_gotop" type="button"><i class="icon-go-top"></i></button>

	</div>
</div>
<!--/ wrapper 包裝層區塊 ******************************-->
</div>
<!--/ container_wrapper 主體包裝層區塊 ******************************-->


<!-- contactus 聯絡我們內容區塊 ******************************-->
<?php //include_once 'contactus.php'; ?>
<!--/ contactus 聯絡我們內容區塊 ******************************-->

<!-- cart 我的購物車 ******************************-->
<?php //include_once 'cart.php'; ?>
<div class="cart_content" id="cart_content"></div>
<!--/ cart 我的購物車 ******************************-->

<!-- mobile_menu 手機版選單 ******************************-->
<?php include_once 'mobile_menu.php'; ?>
<!--/ mobile_menu 手機版選單 ******************************-->

<div id="js_mask_cover" class="mask_cover"></div>
	
	<!-- Custom Theme JavaScript -->
	<script src="<?=$static_js;?>clipboard.js"></script>
	<script src="<?=$static_js;?>commons.js" type="text/javascript" ></script>
	<script src="<?=$static_js;?>script.js" type="text/javascript" ></script>
	<script src="<?=$static_js;?>pagination.js" type="text/javascript" ></script>
	<script src="<?=$static_js;?>messages.js" type="text/javascript" ></script>
	
	<script src="<?=$static_js;?>index.js" type="text/javascript" ></script>
	<script src="<?=$static_js;?>photoswipe.js"></script>
	<script src="<?=$static_js;?>photoswipe-ui-default.js"></script>
	<script src="<?=$static_js;?>my_script.js" type="text/javascript" ></script>

</body>
</html>
