<aside id="js_push-left" class="mobilenav_area mobilenav_style menu_same c-menu--push-left" role="mobile_menu">
<button type="button" class="js_menu_close menu_close">Close Menu</button>
<div class="mobilenav_content">
	<div class="company_info">
		<?php /*<div class="company_thumb"><img src="<?=$static_img;?>472f51e0031002bd46b55693a3408ada.png" alt=""></div>*/?>
		
		<div class="company_heading">
			<h4 class="company_name"><?=$_SETTING['web_site_name'];?></h4>
		</div>
		
		<?php /*
		<ul class="company_share list-unstyled clearfix">
			<li><a class="facebook_share" href="" target="_blank"><i class="icon-facebook-with-circle"></i></a></li>
			<li><a class="twitter_share" href="" target="_blank"><i class="icon-twitter-with-circle"></i></a></li>
		</ul>*/?>
	</div>
		
	<nav class="leftcategory_area clearfix"><!--nav_area nav_14px--> 
	<ul class="left_nav_content clearfix js_nav_content ">
		<li class="nav_category clearfix">
		<?php /*<div class="btn_fb_downmenu"><span>--目錄--</span><i class="icon-triangle-down"></i></div>*/?>
		<ul class="nav js_nav_color clearfix">
											
		<li class="nav_mobile_list" ><!-- 用戶編號 -->
			<?php if(!empty($user_no)){ ?>
			<img src="data:image/png;base64,<?=$user_no['src'];?>" style="width:<?=$user_no['img_width'];?>; height:<?=$user_no['img_height'];?>; border: 0;" alt=" " />
			<?php } ?>
		</li>
	
		<?php if(isset($_SESSION['vid'])){ ?>
		<li class="nav_home"><a href="<?=base_url('welcome');?>" >SJ Mall</a></li>
		<li class="nav_home"><a href="<?=$home_url;?>" class="<?=$home_active;?>" style="color: #000">首頁</a></li>
		<?php } else { ?>
		<li class="nav_home"><a href="<?=base_url('welcome');?>" class="<?=$home_active;?>" >首頁</a></li>
		<li class="nav_about"><a href="<?=base_url('about');?>" class="<?=$about_active;?>" style="color: #000">關於</a></li>
		<?php } ?>
		
		<?php if(isset($_SESSION['vid'])){ ?>
		<li class="nav_about"><a href="<?=base_url('about_vendor');?>" class="<?=$about_active;?>" style="color: #000">關於</a></li>
		<li class="nav_allproducts dropdown_toggle">
			
			<?php if(empty($category_menu)){ ?>
			<a href="<?=$category_url;?>" class="<?=$allproduct_active;?>" style="color: #000">商品列表</a>
			<?php } elseif(!empty($category_menu)){ ?>
			<a href="<?=$category_url;?>" class="<?=$allproduct_active;?>" style="color: #000">全部商品</a>
			<?php foreach($category_menu as $k=>$cat_menu){
				$class_act = 'style="color:#000"';
				$icon_triangle = '';
				if(isset($cid) && (intval($cid)==$k)){
					$icon_triangle = '<i class="icon-triangle-right"></i>';
				}
			?>
			<label class="nav_title " >
				<a href="<?=base_url($cat_menu['link']);?>" ><?=$icon_triangle;?><?=$cat_menu['title'];?></a>
			</label>
			<?php } } ?>
		</li>
		<?php } ?>

		
		<?php if ($this->flags->web_login === TRUE){ ?>
		<li class="nav_mobile_list"><a href="<?=base_url('member');?>" class="<?=$member_active;?>" >會員專區</a></li>
		<?php } ?>
		
		<?php if(isset($_SESSION['vid'])){
		if ($this->flags->web_login === TRUE){ ?>
		<li class="nav_mobile_list"><a href="<?=base_url('logout');?>" >登出</a></li>
		<?php } else { ?>
		<li class="nav_mobile_list"><a href="<?=base_url('login');?>" >登入</a></li>
		<li class="nav_mobile_list"><a href="<?=base_url('login/joinus');?>" >註冊</a></li>
		<?php } } ?>
		
		
		<li class="nav_mobile_list"><a href="<?=base_url('order_query');?>" style="color: #000"><i class="icon-text-document"></i>訂購查詢</a></li>
		
		<?php if(isset($_SESSION['vid'])){ ?>
		<?php /*<li class="nav_mobile_list"><a href="<?=base_url('contact_vendor');?>" style="color: #000"><i class="icon-typing"></i>聯絡我們</a></li>*/?>
		<li class="nav_mobile_list"><a href="<?=base_url('faq_vendor');?>" style="color: #000"><i class="icon-info"></i>服務說明</a></li>
		<?php } else { ?>
		<?php /*<li class="nav_mobile_list"><a href="<?=base_url('contact');?>" style="color: #000"><i class="icon-typing"></i>聯絡我們</a></li>*/?>
		<li class="nav_mobile_list"><a href="<?=base_url('faq');?>" style="color: #000"><i class="icon-info"></i>服務說明</a></li>
		<?php } ?>
		
		<li class="nav_mobile_list"><a href="#" style="color: #000" class="js_subscribe"><i class="icon-news"></i>訂閱電子報</a></li>
		
		</ul>
		</li>
	</ul>

	</nav>
</div>
</aside>