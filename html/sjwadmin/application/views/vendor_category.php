<div class="category_area">

	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
    
	<aside class="accordion_area nav_area clearfix" role="left_category">
    <div class="js_navfixed_area">
	
	<ul class="nav_content clearfix js_nav_content nav_14px">
        <li class="nav_category clearfix">
        
        <ul class="nav js_nav_color clearfix">
			<li class="nav_allproducts dropdown_toggle  ">
				<input id="nav_title_LqnCp9S3_all" name="accordion_LqnCp9S3" class="hide" checked="checked" type="checkbox">
				<label class="nav_title " for="nav_title_LqnCp9S3_all" style="color: #000">
					<a href="<?=$category_url;?>" style="color:#000" >全部商品</a>
				</label>
				
				<?php if(!empty($category_menu)){
				foreach($category_menu as $k=>$cat_menu)
				{
					$class_act = 'style="color:#000"';
					$icon_triangle = '';
					if(intval($cid)==$k){
						$class_act = 'class="active "';
						$icon_triangle = '<i class="icon-triangle-right"></i>';
					}
				?>
				<label class="nav_title " >
					<a href="<?=base_url($cat_menu['link']);?>" <?=$class_act;?> ><?=$icon_triangle;?><?=$cat_menu['title'];?></a>
				</label>
				<?php } } ?>
			</li>
		</ul>
    </li>
	</ul>
	</div>
	</aside>
					
	<main class="main clearfix" role="main">
        <?php /*
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		*/?>
		
		<!-- list_area 內容區塊 ******************************-->
		<div class="header_a">
		<div>
			<h1 class="category_title" content="全部商品">全部商品</h1>
		</div>
		
		<div class="index_area index_style js_index_area js_themes_layout layout_h">
		<div class="container">
		
			<!-- Banner_Slider 橫幅廣告 ******************************-->
			<div id="mycarousel" class="carousel slide" data-ride="carousel" style="margin-bottom: 30px;">
				  <!-- 廣告輪播列表 -->
				  <div class="carousel-inner" role="listbox">
					<div class="item active">
						<?php if(!empty($category_image)){ ?>
						<a href="">
							<img src="<?=$category_image;?>" data-color="#eee">
						</a>
						<?php } ?>
					</div>
				  </div>
			</div>
			<!--/ Banner_Slider 橫幅廣告 ******************************-->
			
			<div class="item_area ">
				<ul class="list-unstyled item_content clearfix" id="product-list">
				
				<?php foreach ($list as $row) 
				{ ?>
				<li class="item_block js_is_photo_style img_polaroid ">
					<a class="clearfix" href="<?=$row['link_edit'];?>" >
					<span class="item_photo js_product_image " <?php /*data-src="<?=$row['image_thumb_url'];?>"*/?> style="background-image: url('<?=$row['image_url'];?>')"></span>
						<div class="item_caption js_themes_products_bg_color" style="">
						<span class="item_vertical">
							<div class="item_info js_matchheight js_themes_products_color" style="height: 74px;">
								<div class="item_text" style="">
									<h4 class="item_name text_overflow js_products_name pt_fontsize_12px" style="color: #858585;">
									<?=$row['name'];?>
									</h4>
									<ul class="list-unstyled item_price">
										<li class="item_origin item_actual" itemprop="price"><span class="font_montserrat line_through" style="color: #858585;">$<?=$row['price'];?></span></li>
										<li class="item_sale" itemprop="price"><span class="font_montserrat">
										<?php if($row['sale_price']){ ?>
										$<?=$row['sale_price'];?>
										<?php } else { ?>
										優惠價
										<?php } ?>
										</span></li>
									</ul>
								</div>
							</div>
						</span>
						</div>
					</a>
				</li>
				<?php } ?>

				</ul>
			<div id="js_noproductdata" style="display: none;"></div>
			</div>
			
			<div class="row">
				<div class="col-md-5">
					Showing <?=$filter['offset']+1;?>
					to <?=(($filter['offset']+$filter['rows'])>$filter['total'])?$filter['total']:$filter['offset']+$filter['rows'];?>
					of <?=$filter['total'];?> entries
				</div>
				<div class="col-md-7 text-right">
					<?=$this->pagination->create_links();?>
				</div>
			</div>
			
		</div>
		</div>
		</div>
		<!--/ list_area 內容區塊 ******************************-->

	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

</div>