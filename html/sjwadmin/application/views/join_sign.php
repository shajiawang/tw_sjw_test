	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- area 內容區塊 ******************************-->
		<div class="about_area about_style clearboth clearfix">
		<div class="main_layout main_style">
				
				<div class="title_area clearfix">
					<span class="title_content clearfix">
						<h2 class="main_title">註冊</h2>
						<h3 class="main_sub font_avant_garde">join us</h3>
					</span>
					<hr class="main_hr">
				</div>
				
				<div class="container">
				<div class="row" style="margin:0 0 20px 0px;">
					<div class="col-md-4 col-md-offset-4">
						
						<form method="POST" role="form" id="loginform" class="form-signin" action="<?=base_url('login/join_sign_check');?>">
						<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
							
							<fieldset>
								<h3 class="sign-up-title" style="color:dimgray; margin-bottom:20px;">用戶 >> <span style="color:blue">數位簽章</span> >> 完成</h3>
								<?php if ($error) { ?>
								<div id="alert_error" class="alert bs-callout bs-callout-danger alert-dismissible" role="alert">
									<button class="close" aria-label="Close" data-dismiss="alert" type="button" style="color:red">
										<span aria-hidden="true">× <?=$error;?></span>
									</button>
								</div>
								<?php } ?>
								
								<label class="form_label" for="password">設定數位簽章</label>
								<input type="password" name="password" id="create_dialog" placeholder="按鈕點選 3 ~ 12次" value="" class="form_input form_control text ui-widget-content ui-corner-all" readonly >
								
								<input class="btn btn-lg btn-success btn-block" type="submit" value="Next >>" >
								
							</fieldset>
						</form>
					</div>
			  </div>
				</div>
		</div>
		</div>
		<!--/ area 內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

<!-- Dialog Form 內容區塊 ******************************-->
<div id="dialog-form" title="數位簽章">
  <form id="dialog_form">
    <fieldset>
		<div style="float:left; width:250px; margin-left:60px;" >
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_1" class="btn btn_back btn_lg" value="<?=$sign_key[6];?>"><span>.</span></button>
			</div>
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_2" class="btn btn_back btn_lg" value="<?=$sign_key[3];?>"><span>.</span></button>
			</div>
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_3" class="btn btn_back btn_lg" value="<?=$sign_key[0];?>"><span>.</span></button>
			</div>
		</div>
		<div style="float:left; width:250px; margin-left:60px;" >
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_4" class="btn btn_back btn_lg" value="<?=$sign_key[7];?>"><span>.</span></button>
			</div>
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_5" class="btn btn_back btn_lg" value="<?=$sign_key[4];?>"><span>.</span></button>
			</div>
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_6" class="btn btn_back btn_lg" value="<?=$sign_key[1];?>"><span>.</span></button>
			</div>
		</div>
		<div style="float:left; width:250px; margin-left:60px;" >
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_7" class="btn btn_back btn_lg" value="<?=$sign_key[8];?>"><span>.</span></button>
			</div>
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_8" class="btn btn_back btn_lg" value="<?=$sign_key[5];?>"><span>.</span></button>
			</div>
			<div style="float:left; padding:5px;">
				<button type="button" id="js_sign_9" class="btn btn_back btn_lg" value="<?=$sign_key[2];?>"><span>.</span></button>
			</div>
		</div>
      
		<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
 
<script>
var pwdValue = '', jq_dialog, dialog_form=$("#dialog_form"),
js_sign_1=$("#js_sign_1"),	js_sign_2=$("#js_sign_2"),	js_sign_3=$("#js_sign_3"),	js_sign_4=$("#js_sign_4"),	js_sign_5=$("#js_sign_5"),	js_sign_6=$("#js_sign_6"),	js_sign_7=$("#js_sign_7"),	js_sign_8=$("#js_sign_8"),	js_sign_9=$("#js_sign_9");
	
$(document).ready(function(){
    $('[name=password]').focus();
	
	jq_dialog = $("#dialog-form").dialog({
		autoOpen: false,
		height: 360,
		width: 360,
		modal: true,
		buttons: {
			"OK": function(){
			  $('[name=password]').val(pwdValue); 
			  alert( "輸入完成 !" );
			  jq_dialog.dialog( "close" );
			},
			Cancel: function() {
			  jq_dialog.dialog( "close" );
			}
		},
		close: function() {
			dialog_form[0].reset();
		}
    });
    
	dialog_form = jq_dialog.find( "form" ).on( "submit", function( event ) {
		event.preventDefault();
    });
	
	$( "#create_dialog" ).click(function(){ 
		jq_dialog.dialog( "open" );
	});
	js_sign_1.click(function(){ pwdValue = pwdValue + js_sign_1.val() + ","; });
	js_sign_2.click(function(){ pwdValue = pwdValue + js_sign_2.val() + ","; });
	js_sign_3.click(function(){ pwdValue = pwdValue + js_sign_3.val() + ","; });
	js_sign_4.click(function(){ pwdValue = pwdValue + js_sign_4.val() + ","; });
	js_sign_5.click(function(){ pwdValue = pwdValue + js_sign_5.val() + ","; });
	js_sign_6.click(function(){ pwdValue = pwdValue + js_sign_6.val() + ","; });
	js_sign_7.click(function(){ pwdValue = pwdValue + js_sign_7.val() + ","; });
	js_sign_8.click(function(){ pwdValue = pwdValue + js_sign_8.val() + ","; });
	js_sign_9.click(function(){ pwdValue = pwdValue + js_sign_9.val() + ","; });
});
</script>
<!--/ Dialog Form 內容區塊 ******************************-->

