<SCRIPT language="JavaScript" type="text/javascript">
function editCart(id,v)
{
	$.ajax({
		url: cart_url,
		type: "GET",
		data: {'toajax':'edit_order', 'item_id':id, 'qty':v},
		async: false,
		dataType: "text",
		success: function(response){
			if(response == 200){ 
				location.replace(cart_url);
			}
		}
	});
}
function delItem(id)
{
	$.ajax({
		url: cart_url,
		type: "GET",
		data: {'toajax':'del_order', 'item_id':id},
		async: false,
		dataType: "text",
		success: function(response){
			if(response == 200){ 
				location.replace(cart_url);
			}
		}
	});
}
function editFreight(id)
{
	$.ajax({
		url: cart_url,
		type: "GET",
		data: {'toajax':'edit_freight', 'shipping_id':id},
		async: false,
		dataType: "text",
		success: function(response){
			if(response == 200){ 
				location.replace(cart_url);
			}
		}
	});
}
function editPay(id)
{
	$.ajax({
		url: cart_url,
		type: "GET",
		data: {'toajax':'edit_payment', 'payment_id':id},
		async: false,
		dataType: "text",
		success: function(response){
			if(response == 200){ 
				location.replace(cart_url);
			}
		}
	});
}
</SCRIPT>
	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		<!-- about_area 內容區塊 ******************************-->
		<div class="payment_area clearboth payment_style">
		<form id="order_form" class="" method="post" action="<?=base_url($this->ctrl_dir .'/pay_check');?>">
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
		
				<div class="form_layout form_style clearfix">
					<header class="title_area clearfix" title="shopping_info">
						<hgroup class="title_content clearfix">
							<h4 class="main_title">我的購物車 : </h4>
							<h5 class="main_sub font_avant_garde">Shopping Cart</h5>
						</hgroup>
						<hr class="main_hr">
					</header>
			
				<section class="cart_detail_area cart_detail_style" id="js_cart_detail_area">
					<header class="item_header">
						<div class="th_product css_th">品名</div>
						<div class="th_quantity css_th">數量</div>
						<div class="th_price css_th">單價</div>
						<div class="th_subtotal css_th">小計</div>
						<div class="th_del css_th"></div>
					</header>
		
					<div class="item_list item_row_same clearfix js_cart_item_list ">
						
						<?php if(!empty($list['rows'])){
						foreach($list['rows'] as $row)
						{ ?>
						<div class="item_productinfo css_tr clearfix">
							<div class="td_product css_td">
								<div class="item_product clearfix">
									<div class="item_thumbs">
										<img src="<?=$row['image_thumb_url'];?>" alt="" width="50" height="50">
									</div>
									<div class="item_info">
										<a href="<?=base_url('goods');?>/<?=$row['product_id'];?>/<?=$_cart['vendor_id'];?>" class="item_name text_overflow" >#<?=$row['product_id'];?> <?=$row['name'];?></a>
									</div>
								</div>
							</div>
							
							<div class="td_quantity css_td form_group js_quantity_area" data-quantity="數量：">
								<div class="form_select_block">
									<select onChange="editCart('<?=$row['id'];?>', this.value);" class="form_select form_control js_quantity js_cart_list_quantity js_select_change" >
										<option value="0" >0</option>
										<?php for($i=1; $i<11; $i++){ ?>
										<option value="<?=$i;?>" <?php echo ($i == intval($row['quantity']))?"selected":'';?> ><?=$i;?></option>
										<?php } ?>
									</select>
									<span class="form_select_bind js_select_bind">1</span>
									</div>
							</div>
								
							<div class="td_price css_td">
								<div class="item_price font_montserrat"><?=$row['price'];?></div>
							</div>
								
							<div class="td_subtotal css_td">
								<div class="item_subtotal font_montserrat" data-subtotal="小計："><?=$row['subtotal'];?></div>
							</div>
								
							<div class="td_del css_td js_cart_list_del_item" >
								<div class="item_del"><a onClick="delItem('<?=$row['id'];?>');"><i class="icon-times-circle"></i></a></div>
							</div>
						</div>
						<?php } } else { ?>
							
						<?php } ?>
						
					</div>

					<div class="item_discount">
						<div class="item_total item_row">
							<div class="item_desc">
								<span class="item_total_title">實付總額</span>
								<span class="item_currency">(TWD)</span>
							</div>
							<div class="item_money">
								<span class="item_total_inner font_montserrat"><?=$list['total'];?></span>
							</div>
						</div>
					</div>
					
				</section>
				</div>
				
				<div id="order_fileds" class="">
					<div class="form_layout form_style">
						<div class="row">
						
						<!-- 購買資訊 -->
						<div class="col-md-4">
							<header class="title_area clearfix" title="purchase_info">
								<hgroup class="title_content clearfix">
									<h4 class="main_title">購買資訊</h4>
									<h5 class="main_sub font_avant_garde">Shipping &amp; Payment</h5>
								</hgroup>
								<hr class="main_hr">
							</header>
							
							<div class="form_group" id="shipping_area">
								<div class="form_data">
									<div class="form_select_block">
										<select onChange="editFreight(this.value);" class="form_select form_control js_select_change" name="form_shipping" id="purchaser_shipping">
											<option value="none" selected="selected">‐ 運送方式 ‐</option>
											<?php if(!empty($list['shipping'])){
											foreach($list['shipping'] as $k=>$v) {  
											$order_shipping_id = $order['main']['shipping_method'];
											$shipping_method = (!empty($order_shipping_id) && (intval($v['id']) == intval($order_shipping_id)) ) ? 'selected':'';
											?>
											<option value="<?=$v['id'];?>" <?=$shipping_method;?> ><?=$v['name'];?> ,運費 <?=$list['shipping_fee'][$v['id']];?></option>
											<?php } } ?>
											
										</select>
										<span class="form_select_bind js_select_bind">‐ 請選擇 運送方式 ‐</span>
										<?php /*<input id="js-custom-name" name="shipping_info[311][name]" type="hidden">*/?>
									</div>
								</div>
								<div id="shipping_error" class="help_block"><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>請選擇運送方式。</span></div>
							</div>
							
							<!-- 付款方式 -->
							<div class="form_group" id="payment_area">
								<div class="form_data">
									<div class="form_select_block">
										<select onChange="editPay(this.value);" class="form_select form_control js_select_change" name="form_payment" id="purchaser_payment">
											<option value="none" selected="selected">‐ 付款方式 ‐</option>
											<?php if(!empty($list['payment'])){
											foreach($list['payment'] as $pay) {  
											$order_payment_id = $order['main']['payment_method_id'];
											$payment_method = (!empty($order_payment_id) && (intval($pay['id']) == intval($order_payment_id)) ) ? 'selected':'';
											?>
											<option value="<?=$pay['id'];?>" <?=$payment_method;?> ><?=$pay['name'];?></option>
											<?php } } ?>
										</select>
										<span class="form_select_bind js_select_bind">‐ 請選擇 付款方式 ‐</span>
									</div>
								</div>
								<div id="payment_error" class="help_block"><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>請選擇付款方式。</span></div>
							</div>
						</div>
						<!-- 購買資訊 -->
							
						<!-- 購買人資料 -->
						<div class="col-md-8">
						<div class="row">
									
									<div class="js_information_block col-md-12">
										<div class="clearfix">
											<header class="title_area clearfix" title="purchaser">
												<hgroup class="title_content clearfix">
													<h4 class="main_title">購買人資料</h4>
													<h5 class="main_sub font_avant_garde">Billing Info</h5>
												</hgroup>
												<hr class="main_hr">
											</header>
											
											<div class="row">
												<div class="col-sm-6">
													<div class="form_group" id="purchaser_name_area">
														<div class="form_data">
															<input id="purchaser_name" class="form_input form_control" maxlength="10" placeholder="請輸入購買人姓名" name="buyer_name" type="text">
															<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
														</div>
														<div id="name_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>姓名必填，不可空白。</span></div>
														<div class="help_block help_tips"><i class="icon-triangle-right"></i><span>務必正確輸入購買人姓名。</span></div>
													</div>
												</div>
												
												<div class="col-sm-6">
													<div class="form_group" id="purchaser_phone_area">
														<div class="form_data">
															<input id="purchaser_phone" class="form_input form_control" maxlength="10" placeholder="請輸入手機號碼" name="buyer_phone" type="tel">
															<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
														</div>
														<div id="phone_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i><span>手機號必填，不可空白。</span></div>
														<div class="help_block help_tips "><i class="icon-checkmark"></i><span>請輸入10碼的手機號碼以接收簡訊提醒。</span></div>
													</div>
												</div>
											</div>
											
											<div class="form_group" id="purchaser_email_area">
												<div class="form_data">
													<input id="purchaser_email" class="form_input form_control" maxlength="100" placeholder="E-Mail, ex: example@aaa.com" name="buyer_email" type="email">
													<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
												</div>
												<div id="email_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>E-Mail必填，不可空白。</span></div>
												<div class="help_block help_tips"><i class="icon-checkmark"></i><span>訂單通知信及認證碼將寄送至此。</span></div>
											</div>
											
											<div class="form_group clearboth cleafix">
												<div class="form_data">
													<textarea class="form_textarea form_control" placeholder="請輸入備註事項(選填)" name="remark" id="customer_text" cols="30" rows="3"></textarea>
												</div>
												<div class="help_block"><i class="icon-exclamation-circle"></i><span>請輸入正確內容。</span></div>
											</div>
										</div>
									</div>
									</div>
							</div>
							<!-- 購買人資料 -->
						
						</div>
						
						<div class="bottom_total_area active clearfix">
							<div class="form_group clearfix">
									<div class="after_sales_area after_sales_block">
									<div class="form_checkbox">
										<input id="read" class="checkbox" checked="checked" type="checkbox">
										<label for="read">我已閱讀「<input class="ipt_after_sales js_after_sales" id="op" type="checkbox"><div class="txt_after_sales"><label class="" for="op">注意事項</label></div>」

											<div class="overlay">
												<section class="after_sales_content">
													<div class="container">
														<div class="row">
															<div class="col-md-8 col-md-offset-2">
																<div class="main_layout main_style">
																	<header class="title_area clearfix">
																		<hgroup class="title_content clearfix">
																			<h4 class="main_title">注意事項</h4>
																			<h5 class="main_sub font_avant_garde">Service</h5>
																		</hgroup>
																		<hr class="main_hr">
																	</header>
																	<article class="after_sales_info inner_layout clearfix"></article>
																	<div class="row">
																		<div class="col-md-3 col-md-offset-9">
																			<label class="btn btn_confirm btn_block btn_md" for="op"><span>確定</span></label>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</section>
											</div>											
										</label>
									</div>
								</div>
							</div>
							
							<div class="form_group after_total_block clearfix">
							</div>
							
							<div class="form_lastmargin clearfix">
								<div class="row">
									<div class="col-md-3 col-md-offset-9 ">
										<button type="button" id="js_order_submit" class="btn btn_next btn_block btn_md"><span>下一步</span></button>
									</div>
								</div>
							</div>
							
						</div>
					</div>
				</div>
			</form>
		</div>

		<span class="hide"></span>
		<!--/ index_area 首頁內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

<SCRIPT>
$(document).ready(function(){
	var form_stat = true;
	
	$('#js_order_submit').click(function(){
		if($('#purchaser_shipping').val()=='none'){
			$('#shipping_error').addClass('help_tips');
			form_stat = false;
		}
		if($('#purchaser_payment').val()=='none'){
			$('#payment_error').addClass('help_tips');
			form_stat = false;
		}
		
		if($('#purchaser_name').val()==''){
			$('#name_error').addClass('help_tips');
			form_stat = false;
		}
		if($('#purchaser_phone').val()==''){
			$('#phone_error').addClass('help_tips');
			form_stat = false;
		}
		if($('#purchaser_email').val()==''){
			$('#email_error').addClass('help_tips');
			form_stat = false;
		}
		
		if(form_stat == true){
			$('#order_form').submit();
		}
	});
});
</SCRIPT>