	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
	<main class="main clearfix" role="main">
                                                                
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<div class="breadcrumb_area visible-lg   list-unstyled clearfix">
			<div class="breadcrumb_home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
				<a href="<?=$home_url;?>" style="color: #000" itemprop="item">
				<span class="icon_home" itemprop="name"><i class="icon-store_mall_directory"></i></span>
				<meta itemprop="position" content="1"></a>
			</div>
			<ol class="" itemscope="" itemtype="http://schema.org/BreadcrumbList">
                <li style="color: #000" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<a class="current" href="#" style="color: #000" itemprop="item">
						<span itemprop="name">購物說明</span>
						<meta itemprop="position" content="2">
					</a>
				</li>
            </ol>
		</div>
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		
		
		<!-- about_area 內容區塊 ******************************-->
		<div class="guide_area guide_style clearboth">
			<?php if($faq){
			foreach($faq as $id=>$v){
			?>
			<article class="main_layout main_style">
				<header class="title_area clearfix">
					<hgroup class="title_content clearfix">
						<h4 class="main_title"><?=$v['title'];?></h4>
						<?php /*<h5 class="main_sub font_avant_garde">payment options</h5>*/?>
					</hgroup>
					<hr class="main_hr">
				</header>
				<section class="inner_layout clearfix">
				<?php /* echo $v['image'];*/?>
				<?=$v['content'];?>
				</section>
			</article>
			<?php } } ?>
			
		</div>
		<!--/ index_area 首頁內容區塊 ******************************-->
	
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->
	
	
