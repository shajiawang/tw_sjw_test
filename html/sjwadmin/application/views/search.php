<div class="category_area">

	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
    
	<aside class="accordion_area nav_area clearfix" role="left_category">
    <div class="js_navfixed_area">
	
	<ul class="nav_content clearfix js_nav_content nav_14px">
        <li class="nav_category clearfix">
        
        <ul class="nav js_nav_color clearfix">
			<li class="nav_allproducts dropdown_toggle  ">
				<input id="nav_title_LqnCp9S3_all" name="accordion_LqnCp9S3" class="hide" checked="checked" type="checkbox">
				<label class="nav_title " for="nav_title_LqnCp9S3_all" style="color: #000">
					<a href="" style="color:#000" >商品列表</a>
				</label>
				
				<?php /*if(!empty($category_menu)){
				foreach($category_menu as $k=>$cat_menu)
				{
					$class_act = ($cid==$k) ? 'class="active"' : '';
				?>
				<label class="nav_title " style="color: #000">
					<a href="<?=base_url($cat_menu['link']);?>" style="color:#000" <?=$class_act;?> ><?=$cat_menu['title'];?></a>
				</label>
				<?php } }*/ ?>
			</li>
		</ul>
    </li>
	</ul>
	</div>
	</aside>
					
	<main class="main clearfix" role="main">
		<?php /*
		<!-- breadcrumb_area 導覽麵包屑 ******************************-->
		<div class="breadcrumb_area visible-lg   list-unstyled clearfix">
			<div class="breadcrumb_home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
			<a href="<?=base_url('allproduct');?>" style="color: #000" itemprop="item"><span class="icon_home" itemprop="name"><i class="icon-store_mall_directory"></i></span><meta itemprop="position" content="1"></a></div>
			<ol class="" itemscope="" itemtype="http://schema.org/BreadcrumbList">
				<li style="color: #000" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
					<a class="current" href="<?=base_url('allproduct');?>" style="color: #000" itemprop="item">
						<span itemprop="name">全部商品</span>
						<meta itemprop="position" content="2">
					</a>
				</li>
			</ol>
		</div>
		<!--/ breadcrumb_area 導覽麵包屑 ******************************-->
		*/?>
		
		<!-- index_area 內容區塊 ******************************-->
		<div class="header_a">
		<div>
			<h1 class="category_title" content="全部商品">全部商品</h1>
		</div>
		
		<div class="index_area index_style js_index_area js_themes_layout layout_h">
	
	<div class="container">
		<div class="item_area ">
			<?php if($searched) { ?>
			<ul class="list-unstyled item_content clearfix" id="product-list">
			<li class="item_block js_is_photo_style img_polaroid ">
					<a class="clearfix" href="<?=base_url('goods');?>" >
					<span class="item_photo js_product_image " data-src="<?=$static_img;?>ec/34cda32495cf5ba50bbbaca2f2e15c52.png" style="background-image: url('<?=$static_img;?>ec/34cda32495cf5ba50bbbaca2f2e15c52.png')">
								</span>
												
						<div class="item_caption js_themes_products_bg_color" style="">
						<span class="item_vertical">
							<div class="item_info js_matchheight js_themes_products_color" style="height: 74px;">
								<div class="item_text" style="">
																<h4 class="item_name text_overflow js_products_name pt_fontsize_12px" style="color: #858585;">○ 鏡盒 ○ 水果鮮奶油蛋糕</h4>
																						<ul class="list-unstyled item_price">
																				<li class="item_origin item_actual" itemprop="price"><span class="font_montserrat line_through" style="color: #858585;">$150</span></li>
												<li class="item_sale" itemprop="price"><span class="font_montserrat">$120</span></li>
																		</ul>
														</div>
												</div>
						</span>
						</div>
					</a>
				</li>
			</ul>

			<?php } else { ?>
			<div id="js_noproductdata" style="">
					<h2 class="noproduct_data">
					<i class="icon-noproduct"></i>
					<span>目前無任何商品</span>
					</h2>
			</div>
			<?php } ?>
		</div>
	</div>
</div>
</div>
	<!--/ index_area 首頁內容區塊 ******************************-->

	<?php /*
	<script>
	var shops = '{"urls":{"productlist":"https:\/\/customerexperience.waca.ec\/productlist"},"sliders":[],"message":"","type":"product","value":"all","products_total":24,"is_backend":false,"is_fb":false,"shops_name":"WACA\u5546\u5e97\u9ad4\u9a57 X \u9280 Ying \u6212 Chieh","shops_popup":{"status":0,"type":"","img_height":500,"img_url":"","edm_content":""}}';
	</script>
	*/?>
	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

</div>