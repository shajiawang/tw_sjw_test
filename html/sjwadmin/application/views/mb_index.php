<div class="category_area">

	<!-- main_area 主內容區塊 ******************************-->
	<div class="container clearfix">
    
	<aside class="accordion_area nav_area clearfix" role="left_category">
    <div class="js_navfixed_area">
	
	<ul class="nav_content clearfix js_nav_content nav_14px">
        <li class="nav_category clearfix">
        
        <ul class="nav js_nav_color clearfix">
			<li class="nav_home "><a href="<?=base_url('member');?>" style="color: #ff0000">會員專區</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/paycheck');?>" style="color: #000">結帳中訂單</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/freight');?>" style="color: #000">備貨中訂單</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/finish');?>" style="color: #000">過往訂單</a></li>
			<li class="nav_about "><a href="<?=base_url('order_mem/cancel');?>" style="color: #000">作廢訂單</a></li>
		</ul>
    </li>
	</ul>
	</div>
	</aside>
					
	<main class="main clearfix" role="main">
	
		<!-- area 內容區塊 ******************************-->
		<div class="about_area about_style clearboth clearfix">
		<form id="member_form" class="" method="post" action="<?=base_url($this->ctrl_dir .'/do_edit');?>" >
		<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
		<input type="hidden" name="k" value="<?=$k;?>" />
		<input type="hidden" name="account" value="<?=$member['account'];?>" />
		
		<div class="main_layout main_style">
			<div class="container">
				<div class="row" style="margin:0 auto;">
					
					<div class="col-md-5 " >
						<fieldset >
							<p id="photo" style="text-align:center;"><img src="<?=$member['photo'];?>" alt="頭像" height='200'></p>
							<label class="form_label" for="field1" >
								會員帳號：<?=$member['account'];?>
								<?php /*<span style="color:#aaa;"> / 推薦人：<?=$member['parent_account'];?></span>*/?>
							</label>
							
							<div class="form_group" id="name_area">
							<?php if($k=='name'){ ?>
								<div class="help_block help_tips"><i class="icon-triangle-right"></i><span>輸入會員名稱</span></div>
								<div class="form_data">
									<input id="user_name" name="user_name" value="<?=$member['name'];?>" placeholder="請輸入會員名稱" maxlength="60" class="form_input form_control" type="text" style="width:80%">
									<span class="msg_error"><i class="icon-exclamation-triangle"></i></span>
									
									<a id="submit_1" href="javascript:;" title="編輯"><i class="icon-profile">Edit</i></a>
								</div>
								<div id="user_name_error" class="help_block "><span style="color:#ff0000"><i class="icon-exclamation-circle"></i>必填，會員名稱不可空白。</span></div>
								<br>
							<?php } else { ?>
								<label class="form_label" for="field1" style="font-size:18px;">名稱：<?=$member['name'];?></label>
							<?php } ?>
							</div>
							
							<div class="form_group" >
								<label class="form_label" for="field1" style="font-size:18px;">手機：<?=$member['telephone'];?></label>
							</div>
						</fieldset>
						
						<div class="row" style="margin:0 auto;">
							<hr class="main_hr">
							<div class="col-md-4 " ><span style="color:#ccc;">禮物：<?=$member['mb_gift'];?></span></div>
							<div class="col-md-8 " ><span style="color:#ccc;">私訊：<?=$member['mb_msg'];?></span></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!-- 簡介 -->
		<div class="main_layout main_style">
			<div style="margin-bottom: 20px;">
				<div class="about_heading about_company">自我簡介</div>
			</div>
			<hr class="main_hr">
			
			<?php if(!empty($member['tvno'])){ ?>
			<div class="embed-responsive embed-responsive-16by9" style="margin-bottom: 10px;">
				<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?=$member['tvno'];?>?rel=0&amp;controls=1&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
			</div>
			<?php } ?>
			
			<div class="form_group" id="info_area">
			<?php if($k=='info'){ ?>
				<div class="help_block help_tips"><i class="icon-triangle-right"></i><span>輸入簡介內容</span></div>
				<div class="form_data">
					<input id="user_info" name="info" value="<?=$member['info'];?>" placeholder="請輸入簡介" maxlength="255" class="form_input form_control" type="text" style="width:80%">
					<a id="submit_2" href="javascript:;" title="編輯"><i class="icon-profile">Edit</i></a>
				</div>
				<br>
			<?php } else { ?>
				<article class="about_article inner_layout clearfix"><?=$member['info'];?></article>
			<?php } ?>
			</div>

		</div>
		<!--/ 簡介 -->
		
		<!-- 地址 -->
		<div class="main_layout main_style">
			<div style="margin-bottom: 20px;">
				<div class="about_heading about_company">收件地址</div>
			</div>
			
			<hr class="main_hr">
			<div class="form_group" id="address_area">
			<?php if($k=='address'){ ?>
				<div class="help_block help_tips"><i class="icon-triangle-right"></i><span>輸入地址內容</span></div>
				<div class="form_data">
					<input id="user_address" name="address" value="<?=$member['address'];?>" placeholder="請輸入地址" maxlength="200" class="form_input form_control" type="text" style="width:80%">
					<a id="submit_3" href="javascript:;" title="編輯"><i class="icon-profile">Edit</i></a>
				</div>
				<br>
			<?php } else { ?>
				<article class="about_article inner_layout clearfix" style="margin-bottom: 10px;"><?=$member['address'];?></article>
			<?php } ?>
			</div>
			
			<?php if(!empty($member['address'])){ ?>
			<div class="rwd-map">
				<iframe src="https://www.google.com/maps?q=<?=$member['address'];?>&amp;hl=zh-tw&amp;z=16&amp;output=embed" frameborder="0" height="300" width="100%"></iframe>
			</div>
			<?php } ?>
		</div>
		<!--/ 地址 -->
		
		</form>
		</div>
		<!--/ area 內容區塊 ******************************-->

	</main>
	</div>
	<!--/ main_area 主內容區塊 ******************************-->

</div>


<SCRIPT language="JavaScript" type="text/javascript">
function edit_user_name()
{
	var url_to = '<?php echo base_url($this->ctrl_dir);?>';
	$.ajax({
		url: url_to,
		type: "GET",
		data: {'toajax':'del_order', 'item_id':id},
		async: false,
		dataType: "text",
		success: function(response){
			if(response == 200){ 
				location.replace(url_to); 
			}
		}
	});
}

$(document).ready(function(){
	var form_stat = true;
	
	$('#submit_1').click(function(){
		if($('#user_name').val()==''){
			$('#user_name_error').addClass('help_tips');
			form_stat = false;
		} else {
			form_stat = true;
		}

		if(form_stat == true){ 
			$('#member_form').submit(); 
		}
	});
	
	$('#submit_2').click(function(){
		$('#member_form').submit(); 
	});
	
	$('#submit_3').click(function(){
		$('#member_form').submit(); 
	});
	
});
</SCRIPT>