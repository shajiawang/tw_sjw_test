<?php
function jsMsg($msg, $url=NULL) {
	echo '<script>';
	echo '	alert("'. $msg .'");';
	if ($url) {
		echo 'location.href="'. $url .'";';
	}
	echo '</script>';
	exit;
}
function jd($var, $dead = false) {
	echo '<pre style="text-align:left;">' . "\n";
	print_r($var);
	echo "\n</pre>\n";
	if($dead)
		exit;
}

function jdd($v, $isVarExport = FALSE) {
	$_bt = debug_backtrace();
	$bt0 = & $_bt[0];
	jd("{$bt0['file']}: {$bt0['line']}");
	if ($isVarExport) {
		jd(var_export($v, 1));
	} else {
		jd($v);
	}
}

function jddlog($v, $isVarExport = FALSE) {
	$_bt = debug_backtrace();
	$bt0 = & $_bt[0];

	$out = "{$bt0['file']}: {$bt0['line']}\n";
	if ($isVarExport) {
		$out .= var_export($v, 1);
	} else {
		$out .= print_r($v, 1);
	}

	error_log($out);
}

function jt($msg = '') {
	echo microtime(true);
	echo " {$msg}<br />\n";
}

function cutContent($str, $num){
	$str = strip_tags($str);
	$rs = mb_substr($str, 0, $num, 'UTF-8');

	if (strlen($str) > strlen($rs)) {
		$rs .= '...';
	}

	return $rs;
}

function generatorRandom($lenght = 6, $word = NULL)
{
	if ( ! $word) {
		// remove o,0,1,l
		// $word = 'abcdefghijkmnpqrstuvwxyz!@#$%^&*()-=ABCDEFGHIJKLMNPQRSTUVWXYZ<>;{}[]23456789';
		$word = 'abcdefghijkmnpqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ23456789';
	}

	$len = strlen($word);

	$password = '';
	for ($i = 0; $i < $lenght; $i++) {
		$password .= $word[rand() % $len];
	}

	return $password;
}

function getClentIP() {
	if (!empty($_SERVER["HTTP_CLIENT_IP"])){
		$ip = $_SERVER["HTTP_CLIENT_IP"];
	}elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
		$ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
	}else{
		$ip = $_SERVER["REMOTE_ADDR"];
	}

	return $ip;
}

function checkDevice(){
    $ua=$_SERVER['HTTP_USER_AGENT'];
    $iphone = strstr(strtolower($ua), 'mobile'); //Search for 'mobile' in user-agent (iPhone have that)
    $android = strstr(strtolower($ua), 'android'); //Search for 'android' in user-agent
    $windowsPhone = strstr(strtolower($ua), 'phone'); //Search for 'phone' in user-agent (Windows Phone uses that)

    function androidTablet($ua){ //Find out if it is a tablet
        if(strstr(strtolower($ua), 'android') ){//Search for android in user-agent
            if(!strstr(strtolower($ua), 'mobile')){ //If there is no ''mobile' in user-agent (Android have that on their phones, but not tablets)
                return true;
            }
        }
    }
    $androidTablet = androidTablet($ua); //Do androidTablet function
    $ipad = strstr(strtolower($ua), 'ipad'); //Search for iPad in user-agent

    if($androidTablet || $ipad){ //If it's a tablet (iPad / Android)
        return 'tablet';
    }
    elseif($iphone && !$ipad || $android && !$androidTablet || $windowsPhone){ //If it's a phone and NOT a tablet
        return 'mobile';
    }
    else{ //If it's not a mobile device
        return 'desktop';
    }
}
function send_mail($recipient, $subject, $message, $from=NULL) {
	$subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";
	$headers = "MIME-Version: 1.0\r\n";
	$headers .= "Content-type: text/html; charset=utf-8\r\n";
	if ($from) {
		$headers .= "From: {$from}\r\n";
	}
    if (is_array($recipient)){
        foreach ($recipient as $to) {
            mail($to, $subject, $message, $headers);
        }
    } else {
        return mail($recipient, $subject, $message, $headers);
    }
}
function curlGet($url, $data=array())
{
    $url .= '?' . http_build_query($data);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    Curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);

    $result = json_decode(curl_exec($ch));
    curl_close($ch);

    return $result;
}

function curlPost($url, $data=array())
{
    $ch = curl_init();
    $options = array(
        CURLOPT_URL=>$toURL,
        CURLOPT_HEADER=>0,
        CURLOPT_VERBOSE=>0,
        CURLOPT_RETURNTRANSFER=>true,
        CURLOPT_USERAGENT=>"Mozilla/4.0 (compatible;)",
        CURLOPT_POST=>true,
        CURLOPT_POSTFIELDS=>http_build_query($post),
    );
    curl_setopt_array($ch, $options);
    // CURLOPT_RETURNTRANSFER=true 会传回网页回应,
    // false 时只回传成功与否
    $result = json_decode(curl_exec($ch));
    curl_close($ch);
    return $result;
}

function getAgo($date) {
    $time = time() - strtotime($date);
    $days = 0;
    $hours = 0;
    $minutes = floor($time / 60);

    if ($minutes >=60) {
        $hours = floor($minutes / 60);
        $minutes = $minutes % 60;
    }
    if ($hours >= 24) {
        $days = floor($hours / 24);
        $hours = $hours % 24;
    }

    $ago = '';
    if ($days > 0) {
        $ago .= $days.'天 ';
    }

    if ($hours > 0) {
        $ago .= $hours.'時 ';
    }

    if ($minutes > 0) {
        $ago .= $minutes.'分 ';
    }

    if ($ago != '') {
        $ago .= '前';
    }

    return $ago;
}

function std_class_object_to_array($stdclassobject)
{
    $array = array();
    if ($stdclassobject) {
        $_array = is_object($stdclassobject) ? get_object_vars($stdclassobject) : $stdclassobject;

        foreach ($_array as $key => $value) {
            $value = (is_array($value) || is_object($value)) ? std_class_object_to_array($value) : $value;
            $array[$key] = $value;
        }
    }
    return $array;
}

function remove_file($dir) {
    if (!file_exists($dir)) return true;

    if (!is_dir($dir) || is_link($dir)) {
        return unlink($dir);
    }

    foreach (scandir($dir) as $item) {
        if ($item == '.' || $item == '..') continue;

        if (!remove_file($dir . "/" . $item)) {
            chmod($dir . "/" . $item, 0777);
            if (!remove_file($dir . "/" . $item)) {
                return false;
            }
        }
    }

    return rmdir($dir);
}
