<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('catalog/category_model');
		$this->load->model('catalog/article_model');
	}

	public function index()
	{
		$this->data['faq'] = $this->getFaq();
		
		$this->layout->setLayout('common/layout_welcome');
		$this->layout->view('faq', $this->data);
	}
	
	private function getFaq()
	{
		$get_article = array();
		
		$cat_conditions = array(
				'level' => 1,
				'enable' => 1,
				'kind' => 'faq',
		);
		
		$_category = $this->category_model->getListByKind($cat_conditions);
		if($_category){
			foreach($_category as $k=>$v){
				$get_cat = $k;
			}
		
		
			$attrs = array(
				'conditions' => array("category" => $get_cat,
					'enable' => 1,
				),
				'order_by' => 'sort_order'
			);
			
			$_article = $this->article_model->getList($attrs); //echo $this->article_model->get_query();
		}
		
		$_now = time();
		$is_show = TRUE;
		
		if($_article)
		{
			foreach($_article as $k2=>$v2)
			{
				$id = $v2['id'];
				
				$start_date = ($v2['start_date'] == '0000-00-00 00:00:00') ? 0 : strtotime($v2['start_date']);
				$end_date = ($v2['end_date'] == '0000-00-00 00:00:00') ? 0 : strtotime($v2['end_date']);
				
				if($start_date > 0 && ($start_date > $_now) ){
					$is_show = FALSE;
				}
				elseif($end_date > 0 && ($end_date < $_now) ){
					$is_show = FALSE;
				}
				
				if($is_show){
					$get_article[$id]['title'] = $v2['title'];
					$get_article[$id]['image'] = $v2['image'];
					$get_article[$id]['content'] = $v2['content'];
				}
			}
		}
		
		return $get_article;
	}
	
}
