<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goods extends MY_Controller
{
	public $category_kind = 'goods';
	
	public function __construct()
	{
		parent::__construct();
		
		$this->ctrl_dir = 'goods';
		$this->view_dir = 'goods';
		$this->data['allproduct_active'] = 'active';
		
		$this->vid = $this->uri->segments[3];
		$this->_vendor = $this->vendor_model->getUser($this->vid);
		
		if(empty($this->_vendor)){
			redirect('welcome');
		}
		
	}
	
	public function index($p=null)
	{
		$_SESSION['vid'] = $this->vid;
		$this->initCompany($this->vid);

		//廠商商品分類
		$this->data['category_menu'] = $this->setVendorCategory($this->_vendor);
		
		if(empty($p)){
			redirect($this->data['home_url']);
		}
		
		$this->data['page_name'] = 'goods';
		$this->data['cart_action_save'] = base_url("goods/{$p}/{$this->vid}");
		
		//商品細節
		$this->_goods_detail($p);
		
		$this->layout->setLayout('common/layout_base');
		$this->layout->view("{$this->view_dir}", $this->data);
	}

	private function _goods_detail($id=0)
	{
		if(! $this->flags->web_login || empty($this->flags->member['id'])){
			redirect('login');
		}
		
		$today = date('Y-m-d H:i:s');
		$this->data['detail'] = array();
		
		$attrs = array(
			'conditions' => array("status !=1" => null,
				'enable' => 1,
				'id' => $id,
				'company_id' => $this->_vendor['company_id']
			),
			'order_by' => 'id'
		);
		
		$product = $this->goods_model->getList($attrs); //echo $this->goods_model->get_query();
		
		if (empty($product)) {
			$this->setAlert(4, '商品不存在!');
			redirect($this->data['home_url']);
        }
		
		$this->data['detail']['vendor_category'] = $product[0]['vendor_category'];
		$this->data['detail']['no'] = $product[0]['no'];
		$this->data['detail']['name'] = $product[0]['name'];
		$this->data['detail']['description'] = $product[0]['description'];
		$this->data['detail']['price'] = $product[0]['price'];
		$this->data['detail']['sale_price'] = $product[0]['sale_price'];
		$this->data['detail']['quantity'] = $product[0]['quantity']; //庫存量
		
		$image = $product[0]['image'];
		$this->data['detail']['image_url'] = ($image) ? $this->data['media_url'] . $image : '';
		$this->data['detail']['image_thumb_url'] = ($image) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $image) : '';
		$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $image);
		if ($extension == 'gif') {
			$this->data['detail']['image_thumb_url'] = $this->data['detail']['image_url'];
		}
		
		
		//站台參數設定
		$description = cutContent($product[0]['description'], 100);
		$description = preg_replace('/\s+/', '', $description);
			
		$this->data['_SETTING']['web_site_title'] = $product[0]['name'] .' @'. $this->data['_SETTING']['web_site_title'];
		$this->data['_SETTING']['web_site_description'] = $product[0]['name'] .' @'. $description;
		$this->data['_SETTING']['web_og_title'] = $this->data['_SETTING']['web_site_title'];
		$this->data['_SETTING']['web_og_description'] = $this->data['_SETTING']['web_site_description'];
		$this->data['_SETTING']['web_og_image'] = $this->data['detail']['image_thumb_url'];
		$this->data['_SETTING']['web_og_url'] = base_url("goods/{$id}/{$this->vid}");
		$this->data['line_share'] = $product[0]['name'] .' '. base_url("goods/{$id}/{$this->vid}");
		
		
		$this->data['detail']['start_stat'] = true;
		$this->data['detail']['end_stat'] = true;
		$this->data['detail']['start_msg'] = '';
		$this->data['detail']['end_msg'] = '';
		$sale_start_date = $product[0]['sale_start_date'];
		$sale_end_date = $product[0]['sale_end_date'];
		
		if ($sale_start_date !=='0000-00-00 00:00:00' && $sale_start_date > $today) {
            //JsMsg('「'.$product[0]['name'].'」尚未開賣\n開賣時間為 '.$sale_start_date.' ~ '.$sale_end_date, $_SERVER['HTTP_REFERER']);
			$this->data['detail']['start_stat'] = false;
			$_end_date = ($sale_end_date=='0000-00-00 00:00:00') ? '' : $sale_end_date;
			$this->data['detail']['start_msg'] = "開賣時間為<br>". $sale_start_date .' ~<br>'. $_end_date;
        }
        if ($sale_end_date !=='0000-00-00 00:00:00' && $sale_end_date < $today ) {
            //JsMsg('「'.$product[0]['name'].'」訂購巳結束\n開賣時間為 '.$sale_start_date.' ~ '.$sale_end_date, $_SERVER['HTTP_REFERER']);
			$this->data['detail']['end_stat'] = false;
			$this->data['detail']['end_msg'] = '訂購巳結束';
        }
		
        //商品結束後是否要移轉至其它商品 if ($product['end_date'] < $today && $product['transfer'] > 0) { $product = $this->product_model->get($product['transfer']); }
		$_act = $this->input->post('addcart');
		
		if (! empty($_act))
		{
			if(isset($_SESSION['vid']) && $_act=='y') {
				$order_id = $this->_order($product[0]);
				redirect('cart');
			}
		}
	}
	
	private function _order($product)
    {
		return $this->order_model->orderInsert($product);
    }
	
}
