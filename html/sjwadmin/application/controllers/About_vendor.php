<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About_vendor extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		
		$this->data['about_active'] = 'active';
	}

	public function index()
	{
		$this->layout->setLayout('common/layout_base');
		$this->layout->view('about', $this->data);
	}
	
}
