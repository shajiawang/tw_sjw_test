<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Member extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		$this->ctrl_dir = 'member';
		$this->view_dir = 'mb_index';
		$this->data['member_active'] = 'active';
		
		if(! $this->flags->web_login){
			redirect('welcome');
		}
	}
	
	public function index()
	{
		$this->data['k'] = '';
		
		//會員資訊
		$this->data['member'] = $get_mb = $this->setMember($this->flags->member);
		
		$image_def = 'member/13/13.jpg';
		$image = empty($get_mb['image']) ? $image_def : $get_mb['image'];
		$this->data['member']['image_url'] = $this->data['media_url'] . $image;
		$this->data['member']['photo'] = $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $image);
		$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $image);
		if ($extension == 'gif') {
			$this->data['member']['photo'] = $this->data['member']['image_url'];
		}
		
		$this->layout->setLayout('common/layout_welcome');
		$this->layout->view($this->view_dir, $this->data);
	}
	
	public function do_edit()
	{
		$_post = $this->input->post();
		
		if($_post['k']=='name')
		{
			$verify = $this->_isVerify($_post);
			if(! $verify['status'])
			{
				$this->setAlert(4, str_replace("\n",'<br>', $verify['error']));
				redirect(base_url($this->ctrl_dir ."/?k={$_post['k']}"));
			}
		}
		
		$get_mb = $this->setMember($this->site_id);

		//更新
		switch($_post['k'])
		{
			case 'name':
				$_fields['name'] = $_post['user_name'];
				$this->member_model->update(array('account' => $get_mb['account']), $_fields);
				break;
			
			case 'info':
				$_fields['info'] = $_post['info'];
				$this->member_info_model->update(array('member_id' => $get_mb['member_id']), $_fields);
				break;
			
			case 'address':
				$_fields['address'] = $_post['address'];
				$this->member_info_model->update(array('member_id' => $get_mb['member_id']), $_fields);
				break;
		}
		
		//redirect(base_url("{$this->_vendor['kind']}/{$this->site_id}") );
	}
	
	private function _isVerify($_post)
    {
        $result = array(
            'status' => FALSE,
            'error' => '',
        );

        $this->load->library('form_validation');
		
		switch($_post['k'])
		{
			case 'name':
				$this->form_validation->set_rules('user_name', '用戶名稱', 'trim|required');
				break;
		}
        
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
		$result['status'] = $this->form_validation->run();
        if ($result['status'] == FALSE) {
            $result['error'] = validation_errors();
        }

        return $result;
    }
	
}
