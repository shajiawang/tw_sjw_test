<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cart extends MY_Controller
{
	public $category_kind = 'goods';
	
	public function __construct()
	{
		parent::__construct();
		
		$this->ctrl_dir = 'cart';
		$this->view_dir = 'cart';
		$this->data['allproduct_active'] = 'active';
		
		if(empty($_SESSION['vid']) ){
			redirect('welcome');
		}
		
		if(! $this->flags->web_login ){
			redirect('login');
		}
		
		$this->_vendor = $this->vendor_model->getUser($_SESSION['vid']);
		if(empty($this->_vendor) ){
			redirect('welcome');
		}
		
	}
	
	public function index()
	{
		$this->data['page_name'] = 'cart';
		
		//訂單是否存在
		if(empty($this->data['_cart']) ){
			$this->setAlert(4, '無訂單資料 !');
			redirect($this->data['home_url']);
		}
		
		$get = $this->input->get();
		
		if(isset($get['toajax']) && $get['toajax']=='del_order')
		{
			if(!empty($get['item_id'])){
				echo $this->delete_order_item($get['item_id']);
			} else {
				echo 0;
			}
		}
		elseif(isset($get['toajax']) && $get['toajax']=='edit_order')
		{
			if(!empty($get['item_id'])){
				echo $this->edit_order_item($get['item_id'], $get['qty']);
			} else {
				echo 0;
			}
		}
		elseif(isset($get['toajax']) && $get['toajax']=='edit_freight')
		{
			if(!empty($get['shipping_id'])){
				echo $this->edit_order_freight($get['shipping_id']);
			} else {
				echo 0;
			}
		}
		elseif(isset($get['toajax']) && $get['toajax']=='edit_payment')
		{
			if(!empty($get['payment_id'])){
				echo $this->edit_order_payment($get['payment_id']);
			} else {
				echo 0;
			}
		}
		else
		{
			//訂單主檔
			$user_id = isset($this->flags->member) ? $this->flags->member['id'] : '';
			$_order = $this->order_model->getOrderInfo(session_id(), $user_id, $_SESSION['vid']); //echo $this->goods_model->get_query();
			$this->data['order'] = $_order;
			
			//運送方式
			$send_attrs = array( 'conditions' => array('enable'=>1) );
			$this->data['list']['shipping'] = $this->shipping_method_model->getList($send_attrs);
			foreach($this->data['list']['shipping'] as $v){
				$shipping_fee[$v['id']] = $v['fee'];
			}
			$this->data['list']['shipping_fee'] = $shipping_fee;
			
			//付款方式
			$pay_attrs = array( 'conditions' => array('enable'=>1) );
			$this->data['list']['payment'] = $this->payment_method_model->getList($pay_attrs);
			
			//訂單細節
			if(! empty($_order['main'])){
				$this->order_detail($_order['main']);
			}
			
			//訂單是否存在
			if(empty($this->data['list']['rows']) ){
			$this->setAlert(4, '無訂單品項資料 !');
				redirect($this->data['home_url']);
			}
			
			$this->layout->setLayout('common/layout_base');
			$this->layout->view("{$this->view_dir}", $this->data);
		}
	}
	
	public function pay_check()
	{
		$post = $this->input->post();
		if (!empty($post))
		{
			//驗證 運送,支付
			if($post['form_shipping'] =='none'){
				$this->setAlert(4, str_replace("\n",'<br>', '請選擇 運送方式'));
				redirect(base_url($this->ctrl_dir));
			}
			if($post['form_payment'] =='none'){
				$this->setAlert(4, str_replace("\n",'<br>', '請選擇 付款方式'));
				redirect(base_url($this->ctrl_dir));
			}
			
			$verify = $this->_isVerify();
			
			if ($verify['status'] === TRUE && ($post['form_shipping'] !='none' && $post['form_payment'] !='none') )
			{
				//更新訂單記錄
				$order_id = $this->data['_cart']['id'];
				
				$order_main = $this->order_model->getByOrderId($order_id);
				
				//小計金額
				$order_sum = $this->order_item_model->getSum('subtotal', array('order_id'=>$order_id));
				
				//訂單實付總額
				$_fields['total'] = intval($order_sum) + intval($order_main['shipping_fee']);
				$_fields['buyer_name'] = $this->input->post('buyer_name');
				$_fields['buyer_phone'] = $this->input->post('buyer_phone');
				$_fields['buyer_email'] = $this->input->post('buyer_email');
				$_fields['remark'] = $this->input->post('remark');
				$this->order_model->update(array('id' => $order_id), $_fields);
				
				redirect(base_url('paycheck'));
			} else {
				$this->setAlert(4, str_replace("\n",'<br>', $verify['error']));
				redirect(base_url($this->ctrl_dir));
            }
        }
	}
	private function _isVerify()
    {
        $result = array(
            'status' => FALSE,
            'error' => '',
        );

        $this->load->library('form_validation');
		$this->form_validation->set_rules('buyer_name', '姓名', 'trim|required');
        $this->form_validation->set_rules('buyer_email', 'E-Mail', "trim|required|valid_email");
        $this->form_validation->set_rules('buyer_phone', '手機號碼', 'trim|required|numeric');
        //$this->form_validation->set_rules('buyer_gender', '性別', 'required|in_list[1,2]');
        //$this->form_validation->set_rules('buyer_birthday', '生日', 'trim|required|valid_date');
        //$this->form_validation->set_rules('quantity', '數量', 'is_natural_no_zero');
        $this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
        
		$result['status'] = $this->form_validation->run();
        if ($result['status'] == FALSE) {
            $result['error'] = validation_errors();
        }

        return $result;
    }

	private function order_detail($_order_main = array())
	{
		//會員登入資料
		$user_id = isset($this->flags->member) ? $this->flags->member['id'] : '';
		$sess_id = session_id();
		
		$today = date('Y-m-d H:i:s');
		$this->data['detail'] = array();
		
		$attrs = array(
			'conditions' => array('order_id' => $_order_main['id']),
			'order_by' => 'id desc',
		);
		
		$total = 0;
		$list = array();
		if(!empty($user_id) || !empty($sess_id) )
		{
			$list = $this->order_item_model->getList($attrs); //echo $this->goods_model->get_query();
			
			if(!empty($list))
			foreach($list as $k=>$row)
			{
				$image = $row['image'];
				$image_url = ($image) ? $this->data['media_url'] . $image : '';
				$image_thumb_url = ($image) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $image) : '';
				$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $image);
				if ($extension == 'gif') {
					$image_thumb_url = $image_url;
				}
				$list[$k]['image_url'] = $image_url;
				$list[$k]['image_thumb_url'] = $image_thumb_url;
				
				$total += $row['subtotal'];
			}
		}
		
		//計算運費
		$order_shipping = $this->data['order']['main']['shipping_method'];
		$freight = empty($this->data['list']['shipping_fee'][$order_shipping]) ? 0 : intval($this->data['list']['shipping_fee'][$order_shipping]);
		
		$this->data['list']['rows'] = $list;
		$this->data['list']['freight'] = $freight;
		$this->data['list']['total'] = $total + $freight;
	}
	
	private function edit_order_payment($payment_id=0)
    {
		$this->order_model->orderPayment($this->data['_cart']['id'], $payment_id);
		
		return 200;
    }
	
	private function edit_order_freight($shipping_id=0)
    {
		$this->order_model->orderFreight($this->data['_cart']['id'], $shipping_id);
		
		return 200;
    }
	
	private function edit_order_item($item_id=null, $qty=0)
    {
		$this->order_model->orderUpdate($this->data['_cart']['id'], $item_id, $qty);
		
		return 200;
    }
	
	private function delete_order_item($item_id=null)
    {
		$this->order_model->orderDelete($this->data['_cart']['id'], $item_id);

		return 200;
    }
	
}
