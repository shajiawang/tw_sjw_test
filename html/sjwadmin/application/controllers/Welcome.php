<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller
{
	public $category_kind = 'goods';
	public function __construct()
	{
		parent::__construct();

		$this->ctrl_dir = 'welcome';
		$this->view_dir = 'welcome';
		$this->data['home_active'] = 'active';
		
		if (!isset($this->data['filter']['page'])) {
			$this->data['filter']['page'] = '1';
		}
		if (!isset($this->data['filter']['q'])) { 
			$this->data['filter']['q'] = ''; 
		}
		
	}
	
	public function index()
	{
		$this->data['page_name'] = 'list';
		$this->data['list'] = array();
		$page = $this->data['filter']['page'];
		$rows = 8;
		
		$this->data = $this->_get_data($page, $rows);
		
		$this->layout->setLayout('common/layout_welcome');
		$this->layout->view("{$this->view_dir}", $this->data);
	}
	
	private function _get_data($page, $rows)
	{
		$table0 = $this->goods_model->table;
		$table1 = $this->company_info_model->table;

		$conditions = array("{$table0}.status !=1" => null
			, 'enable' => 1
			, 'mall_index' => 1
		);
		$attrs = array('select' => "{$table0}.*, {$table1}.account, {$table1}.comp_name ",
			'conditions' => $conditions,
			'order_by' => "{$table0}.sort_order",
		);
		
		$on_0 = "{$table0}.company_id={$table1}.id";
		$attrs['join'][0] = array('table'=>$table1, 'on'=>$on_0, 'type'=>'left');
		
		$this->data['filter']['total'] = $total = $this->goods_model->getListCount($attrs);
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;

		$attrs['rows'] = $rows;
		$attrs['offset'] = $offset;
		
		$list = $this->goods_model->getList($attrs); //echo $this->goods_model->get_query();
		foreach ($list as $k=>$row)
		{
			$this->data['list'][$k] = $row;
			$this->data['list'][$k]['image_url'] = ($row['image']) ? $this->data['media_url'] . $row['image'] : '';
			$this->data['list'][$k]['image_thumb_url'] = ($row['image']) ? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
			$extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
			if ($extension == 'gif') {
				$this->data['list'][$k]['image_thumb_url'] = $row['image_url'];
			}
			
			$this->data['list'][$k]['link_vendor'] = base_url("vendor/{$row['account']}");
			$this->data['list'][$k]['link_edit'] = base_url("goods/{$row['id']}/{$row['account']}");
		}
		
		/*
		$this->load->library('pagination');
		$config['base_url'] = base_url("{$this->ctrl_dir}/{$_SESSION['vid']}?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $rows;
		$this->pagination->initialize($config);
		*/
		
		return $this->data;
	}
	
}
