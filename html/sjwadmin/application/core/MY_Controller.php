<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
	public $data = array();

	public function __construct()
	{
		parent::__construct();

		$this->session_id = session_id();
		$this->site = 'web';
		$this->kind = 'web';
		$this->flags = new stdClass;
		$this->_company = array();
		$this->media_path = MEDIA_PATH . basename(dirname(FCPATH)) .'/';

		$this->load->library(array(
			'layout',
			'myaes',
			//'mycrypt','mycaptcha',
		));

		$this->load->helper(array(
			'common',
		));

		/* 載入 models\sys\ 模組 */
		$this->load->model(array(
			'sys/setting_model',
			'sys/menu_model',
			'sys/company_info_model',
			'sys/user_model',
			'sys/user_group_model',
			'sys/user_group_auth_model',
			'sys/member_model',
			'sys/member_info_model',
			'sys/vendor_model',
			
		));
		$this->load->model('product/goods_model');
		$this->load->model('product/goods_image_model');
		$this->load->model('product/goods_category_model');
		$this->load->model('sales/order_model');
		$this->load->model('sales/order_item_model');
		$this->load->model('sales/order_status_model');
		$this->load->model('sales/order_history_model');
		$this->load->model('sales/payment_method_model');
		$this->load->model('sales/shipping_method_model');

		// 設定登入
		$this->initFlags();

		// 選單
		$this->initMenu();
		
		//全站商品分類 $this->initCategory();
		
		// 用戶參數
		$this->data['user_info'] = array('edit'=>FALSE
			, 'user_id'=>null
		);
		
		// META 參數設定
		$this->data['page_title'] = '前台';
		$this->data['page_description'] = '';
		$this->data['page_keywords'] = '';

		// 靜態檔案網址
		$this->data['static_css'] = STATIC_URL ."css/";
		$this->data['static_js'] = STATIC_URL ."js/";
		$this->data['static_img'] = STATIC_URL ."img/";
		$this->data['static_fonts'] = STATIC_URL ."fonts/";
		$this->data['static_plugin'] = STATIC_URL ."plugin/";
		$this->data['media_url'] = MEDIA_URL;
		
		$this->data['filter'] = array(
		);
		if ($this->input->get()) {
			$this->data['filter'] = $this->input->get();
		}
		
		
		//站台資訊參數 
		//var_dump($this->router->class); var_dump($this->router->method);
		$this->data['_SETTING'] = array();
		$this->data['vendor_id'] = null;
		$this->data['_cart']['item'] = 0;
		
		$home_class = array('welcome', 'about', 'category', 'member', 'order_mem', 'order_query', 'faq' );
		if(in_array($this->router->class, $home_class) ){
			unset($_SESSION['vid']);
		}
		
		$this->data['_SETTING'] = $this->initSetting();
		$this->data['_SETTING']['web_og_title'] = $this->data['_SETTING']['web_site_title'];
		$this->data['_SETTING']['web_og_description'] = $this->data['_SETTING']['web_site_description'];
		$this->data['_SETTING']['web_og_url'] = base_url('welcome');
		$this->data['_SETTING']['web_og_sitename'] = $this->data['_SETTING']['web_site_name'];
		$this->data['_SETTING']['web_og_image'] = $this->data['static_img'] .'logo.png';
		$this->data['_SETTING']['web_logo_image'] = $this->data['static_img'] .'logo.png';
		$this->data['_SETTING']['web_back_image'] = $this->data['static_img'] .'def_background.jpg';
			
		if(isset($_SESSION['vid']))
		{
			// 廠商網站參數
			$this->data['home_url'] = base_url("vendor/{$_SESSION['vid']}");
			$this->data['site_info'] = $this->initCompany($_SESSION['vid']);
			
			//購物車
			$this->data['_cart'] = $this->setCart($_SESSION['vid']);
		}
		else
		{
			// 預設網站參數
			$this->data['home_url'] = base_url('welcome');
			$this->data['site_info'] = $this->initCompany(1);
		}
		
		//顯示訂購按鈕
		$this->data['cart_toggle'] = TRUE;
		
		// 目前位置
		$this->data['home_active'] = '';
		$this->data['about_active'] = '';
		$this->data['allproduct_active'] = '';
		$this->data['member_active'] = '';

		// 傳送 josn 給 JavaScrip 使用
		$this->data['_JSON'] = $this->initJson();

		// CSRF Token
		$this->data['csrf'] = array(
			'name' => $this->security->get_csrf_token_name(),
			'hash' => $this->security->get_csrf_hash()
		);
		
		// 提示訊息
		$this->initAlert();
		$this->setAlert();
		$this->setJson('_ALERT', $this->data['_ALERT']);

		//資料筆數
		$this->db_rows = 30;
		$this->data['choices']['rows'] = array(
			10 => 10,
			20 => 20,
			30 => 30,
			50 => 50,
			100 => 100,
		);
		if (empty($this->data['filter']['rows'])) {
			$this->data['filter']['rows'] = $this->db_rows;
			
			if ($this->input->cookie('rows')) {
				$this->data['filter']['rows'] = $this->input->cookie('rows');
			}
		} else {
			setcookie('rows',$this->data['filter']['rows'] , time()+86400);  // exist by 7 days
		}
		
		// switch profiler;
		if (ENVIRONMENT == 'development' || ENVIRONMENT == 'testing') {
			// $this->output->enable_profiler(TRUE);
		}

		// ajax
		if ($this->input->is_ajax_request()) {
			$this->output->enable_profiler(FALSE);
		}

		// Command-line interface
		if (is_cli()) {
			$this->output->enable_profiler(FALSE);
		}

	}
	
	public function set_system_message($system_message = null, $url = null)
	{
		if(empty($system_message))
		{
			redirect();
		}
		else
		{
			$_SESSION['SYSTEM_MESSAGE']['message'] = $system_message;
			$_SESSION['SYSTEM_MESSAGE']['url'] = $url;
			
			redirect('sys_message');
		}
	}

	/********************************
	 * Flags data setting.
	 ********************************/
	private function initFlags()
	{
		$this->data['user_no'] = array();
		$this->flags->web_login = FALSE;
		$this->flags->member = NULL;
		
		if (isset($_SESSION['token']))
		{
			$_token = $this->myaes->mcrypt_de($_SESSION['token']);
			
			$conditions = array(
				'token' => $_token,
			);
			$user = $this->member_model->get($conditions);
			
			unset($user['password']);
			unset($user['private_key']);
			unset($user['token']);
			
			if ($user)
			{
				$this->flags->web_login = TRUE;
				$this->flags->member = $user;
			}
		}
		
		return $this;
	}
	public function setFlags($user)
	{
		$token = $this->login_model->setToken($user['id']);
		$_SESSION['token'] = $this->myaes->mcrypt_en($token);
		
		return $this;
	}
	
	public function setCart($vendor_id=null)
	{
		//訂單品項
		$cart = array('id' => null);
		$user_id = isset($this->flags->member) ? $this->flags->member['id'] : '';
		$user_sess_id = session_id();
		$this->order = $this->order_model->getOrderInfo($user_sess_id, $user_id, $vendor_id);

		if(!empty($this->order['main']))
		{
			$conditions = array('order_id'=>$this->order['main']['id']);
			$count = $this->order_item_model->getCount($conditions);
			
			$cart = $this->order['main'];
			$cart['item'] = $count;
		} else {
			$cart['item'] = 0;
		}
		
		return $cart;
	}
	
	//站台資訊
	public function initCompany($vid=null)
	{
		$_SESSION['company_id'] = null;
		$this->data['vendor_id'] = $vid;
		$this->data['category_url'] = base_url("vendor_nav/0/{$vid}");
		
		//company_info 廠商資訊
		$_company = $this->company_info_model->getRowByAccount($vid);
	
		if(! empty($_company))
		{
			$this->_company = $_company;
			$_SESSION['company_id'] = $_company['id'];
			
			//設定 Banner
			$this->data['_banner'] = $this->setBanner($_company['id']);
			$company_info = cutContent($_company['info'], 100);
			$company_info = preg_replace('/\s+/', '', $company_info);
			
			$this->data['_SETTING']['web_site_name'] = $_company['comp_name'];
			$this->data['_SETTING']['web_site_title'] = $_company['comp_name'];
			$this->data['_SETTING']['web_site_description'] = $company_info;
			$this->data['_SETTING']['web_site_keywords'] = $this->data['_SETTING']['web_site_description'];
			$this->data['_SETTING']['web_og_title'] = $_company['comp_name'];
			$this->data['_SETTING']['web_og_description'] = $this->data['_SETTING']['web_site_description'];
			$this->data['_SETTING']['web_og_sitename'] = $_company['comp_name'];
			$this->data['_SETTING']['web_og_url'] = $this->data['home_url'];
			
			if(!empty($_company['logo']) ){
				$this->data['_SETTING']['web_og_image'] = MEDIA_URL . $_company['logo'];
				$this->data['_SETTING']['web_logo_image'] = MEDIA_URL . $_company['logo'];
			}
			
			if(!empty($_company['back_image']) ){
				$this->data['_SETTING']['web_back_image'] = MEDIA_URL . $_company['back_image'];
			}
		}
		
		return $_company;
	}
	
	private function setBanner($company_id=null)
	{
		$_now = time();
		
		$this->load->model('catalog/banner_model');
		$get_banner = $this->banner_model->getRowByCompany($company_id);
		
		if(!empty($get_banner))
		{
			foreach($get_banner as $k=>$v)
			{
				$is_show = TRUE;
				$start_date = ($v['start_date'] == '0000-00-00 00:00:00') ? 0 : strtotime($v['start_date']);
				$end_date = ($v['end_date'] == '0000-00-00 00:00:00') ? 0 : strtotime($v['end_date']);
				
				if($start_date > 0 && ($start_date > $_now) ){
					$is_show = FALSE;
				}
				elseif($end_date > 0 && ($end_date < $_now) ){
					$is_show = FALSE;
				}
				
				if($is_show){
					$_banner[$k]['position'] = $v['position'];
					$_banner[$k]['slogan'] = $v['slogan'];
					$_banner[$k]['image'] = empty($v['image']) ? $this->data['static_img'] .'def_banner0.jpg' : MEDIA_URL . $v['image'];
					$_banner[$k]['url'] = $v['url'];
					$_banner[$k]['url_target'] = empty($v['new_window']) ? '' : '_blank';
				}
			}
			
		} else {
			$_banner[0]['position'] = 1;
			$_banner[0]['slogan'] = '';
			$_banner[0]['image'] = $this->data['static_img'] .'def_banner0.jpg';
			$_banner[0]['url'] = '';
			$_banner[0]['url_target'] = '';
		}
		
		return $_banner;
	}
	
	//廠商商品分類
	public function setVendorCategory($company=array())
	{
		$category_menu = array();
		
		$category_conditions = array(
			"status !=1" => null,
			'company_id' => $company['company_id']
		);
		$attrs = array(
			'conditions'=>$category_conditions, 
			'order_by'=>'id'
		);
		$get_category = $this->goods_category_model->getList($attrs);
		
		if(! empty($get_category))
		{
			foreach($get_category as $cat)
			{
				$k = $cat['id'];
				$category_menu[$k]['title'] = $cat['category_title'];
				$category_menu[$k]['link'] = "vendor_nav/{$k}/{$company['id']}";
			}
		}
		
		return $category_menu;
	}
	
	//全站商品分類
	public function initCategory()
	{
		$this->data['category_menu'] = array();
		
		$category_conditions = array(
			"status !=1" => null,
		);
		$attrs = array(
			'conditions'=>$category_conditions, 
			'order_by'=>'id'
		);
		$get_category = $this->goods_category_model->getList($attrs);
		
		if(! empty($get_category))
		{
			foreach($get_category as $cat)
			{
				$k = $cat['id'];
				$this->data['category_menu'][$k]['title'] = $cat['category_title'];
				$this->data['category_menu'][$k]['link'] = "category/?cid={$k}";
			}
		}
	}
	
	//會員帳戶資訊
	public function setMember($getMember)
	{
		if(!empty($getMember))
		{
			$MbInfo = $this->member_info_model->getMemberInfo($getMember['id']);
			$data = array_merge($getMember, $MbInfo);
			
			//reference 推薦人
			$reference = $this->member_model->getUserById($data['parent_uid']);
			$data['parent_account'] = $reference['account'];
		}
		
		return $data;
	}
	
	public function getToken($user)
    {
        return sha1($user['id'] . $this->session_id . time());
    }
	
	

	/********************************
	 * Alert function.
	 * param kind [int]
	 * 0 => gary,
	 * 1 => Green,
	 * 2 => Blue,
	 * 3 => Yellow,
	 * 4 => Red
	 ********************************/
	private function initAlert()
	{
		$this->data['_ALERT'] = array();

		$alert = isset($_SESSION['_ALERT'])? $_SESSION['_ALERT'] : array();
		if (isset($alert['message']) && $alert['message']) {
			$this->data['_ALERT']['kind'] = $alert['kind'];
			$this->data['_ALERT']['message'] = $alert['message'];
			$this->data['_ALERT']['sec'] = $alert['sec'];
			$this->data['_ALERT']['layout'] = $alert['layout'];
			$_SESSION['_ALERT'] = array();
		}
	}

	protected function setAlert($kind=0, $message=NULL, $sec=5 ,$layout='center')
	{
		$data = array(
			'kind' => $kind,
			'message' => $message,
			'sec' => $sec,
			'layout' => $layout,
		);
		
		$_SESSION['_ALERT'] = $data;
	}

	/********************************
	 * Menu functions.
	 ********************************/
	private function initMenu()
	{
		$this->data['_MENU'] = array();
		$this->data['menu_catalog'] = array();
		$this->data['menu_function'] = array();
		$this->data['menu_current'] = array();
		return $this;
	}

	private function setMenu()
	{
		$this->data['_MENU'] = $this->menu_model->getSidebarByPort($this->site);
		return $this;
	}

	private function setMenuLocation()
	{
		$this->data['_LOCATION'] = $this->menu_model->getLocation($this->site);
		return $this;
	}

	/********************************
	 * Setting functions.
	 ********************************/
	private function initSetting()
	{
		$data = array();
		$settings = $this->setting_model->getChoices(); //echo $this->setting_model->get_query();
		if ($settings) {
			$data = $settings;
		}
		
		return $data;
	}

	/********************************
	 * JSON functions.
	 ********************************/
	private function initJson()
	{
		return array();
	}

	protected function setJson($key, $val)
	{
		$this->data['_JSON'][$key] = $val;

		return $this;
	}

	protected function getJson($key)
	{
		if (isset($this->data['_JSON'][$key]))
		{
			return $this->data['_JSON'][$key];
		}

		return NULL;
	}

	protected function delJson($key)
	{
		unset($this->data['_JSON'][$key]);

		return $this;
	}

	/********************************
	 * Captcha functions.
	 //使用 DATA URI 將圖片以 Base64 編碼並內崁至網頁中 https://blog.gtwang.org/web-development/minimizing-http-request-using-data-uri/
	 //Data URL和图片 http://www.webhek.com/post/data-url.html
	 //php製作圖片 https://blog.allenchou.cc/php-img/
	 //http://blog.davidou.org/archives/198
	 ********************************/
	public function getCaptchaImage($config=array())
    {
        $default = array(
            'word'          => generatorRandom(6),
            'img_path'      => $this->media_path . 'captcha/',
            'img_url'       => $this->data['media_url'] . 'captcha/',
            'font_path'     => DIR_FONTS . 'MONACO.ttf',
            'img_width'     => 155,
            'img_height'    => 36,
            'expiration'    => 7200,
            // 'word_length'   => 4,
            'font_size'     => 16,
            // 'img_id'        => 'Imageid',
            // 'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                'background' => array(33, 40, 47),
                'border' => array(33, 40, 47),
                'text' => array(255,255,255),
                'grid' => array(
                    array(252,148,148),
                    array(241,252,35),
                    array(181,252,35),
                    array(35,252,252),
                    array(35,169,252),
                    array(158,35,252),
                    array(252,35,241),
                ),
            )
        );

        $config = array_merge($default, $config);

        $cap = create_captcha($config);
		$_SESSION['captcha_code'] = $cap['word'];

        return $cap['image'];
    }

	public function validCaptcha($code)
	{
		$captcha_code = $_SESSION['captcha_code'];
		return ($captcha_code == $code);
	}

	/********************************
	 * Change Query String params
	 ********************************/
	public function getQueryString($merge_params=array(), $skip_params=array())
	{
		$filter = $this->data['filter'];
		$filter = array_merge($filter, $merge_params);

		foreach ($skip_params as $param) {
			if (isset($filter[$param])) {
				unset($filter[$param]);
			}
		}

		return	http_build_query($filter);
	}
	
	/********************************
	 * Upload image
	 ********************************/
	protected function upload_image($image=NULL, $relative_dir=NULL, $thumb_width=200, $thumb_height=200)
	{
		if ($image && $relative_dir)
		{
			$file_info = pathinfo($image['name']);
			$new_name = generatorRandom(12) .'.'. strtolower($file_info['extension']);

			$path = $this->media_path . $relative_dir;
			$source_image = $path . $new_name;

			if (!is_dir($path)) {
				mkdir($path, 0777, true);
			}

			if (isset($image['tmp_name'])) {
				move_uploaded_file($image['tmp_name'], $source_image);
			}

			$this->load->library('image_lib');
			$this->image_lib->clear();
			$config = array(
				'source_image' => $source_image,
				'new_image' => $source_image,
				'wguidth' => $thumb_width,
				'height' => $thumb_height,
				'image_library' => 'gd2',
				'create_thumb' => TRUE,
				'maintain_ratio' => TRUE,
				'master_dim' => 'auto',
				'thumb_marker' => '_thumb',
			);

			$this->image_lib->initialize($config);

			$result = array(
				'status' => FALSE,
				'path' => $path,
				'relative_dir' => $relative_dir,
				'new_name' => $new_name,
				'origin_name' => $file_info['basename'],
				'extension' => $file_info['extension'],
				'filename' =>  $file_info['filename'],
			);
			if ($this->image_lib->resize()) {
				$result['status'] = TRUE;
			}

		} else {
			$result = array(
				'status' => FALSE,
				'error' => 'image and relative_dir is NULL',
			);
		}

		return $result;
	}
	
	protected function copy_image($image=NULL, $relative_dir=NULL, $thumb_width=200, $thumb_height=200)
	{
		if ($image && $relative_dir)
		{
			$origin_image = $this->media_path . $image;
			$file_info = pathinfo($image);
			$path = $this->media_path . $relative_dir;
			
			$new_name = $relative_dir . $file_info['basename'];
			$new_image = $this->media_path . $new_name;
			
			if (!is_dir($path)) {
				mkdir($path, 0777, true);
			}
			if (! empty($image)) {
				copy($origin_image, $new_image);
			}
			
			$this->load->library('image_lib');
			$this->image_lib->clear();
			$config = array(
				'source_image' => $new_image,
				'new_image' => $new_image,
				'wguidth' => $thumb_width,
				'height' => $thumb_height,
				'image_library' => 'gd2',
				'create_thumb' => TRUE,
				'maintain_ratio' => TRUE,
				'master_dim' => 'auto',
				'thumb_marker' => '_thumb',
			);

			$this->image_lib->initialize($config);

			$result = array(
				'status' => FALSE,
				'path' => $path,
				'relative_dir' => $relative_dir,
				'new_name' => $new_name,
				'origin_name' => $file_info['basename'],
				'extension' => $file_info['extension'],
				'filename' =>  $file_info['filename'],
			);
			if ($this->image_lib->resize()) {
				$result['status'] = TRUE;
			}

		} else {
			$result = array(
				'status' => FALSE,
				'error' => 'image and relative_dir is NULL',
			);
		}
		
		return $result;
	}
	
	/*
	* Internal method to do XSS clean in the derived classes
	*/
	public function xss_clean($str, $is_image = FALSE)
	{
		return $this->security->xss_clean($str, $is_image);
	}

}
