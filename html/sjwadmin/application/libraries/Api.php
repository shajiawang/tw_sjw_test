<?php

class Api
{
    // public $CI;
    public $api_url;
    public $return;
    public $timeout;

    public function __construct()
    {
		// $this->CI = & get_instance();

        $this->timeout = 5;
    }

    public function initReturn()
    {
        $data = new stdclass;
        $this->return = new stdclass;
        $this->return->status = FALSE;
        $this->return->data = NULL;
        $this->return->message = NULL;
    }

    public function setUrl($val)
    {
        $this->api_url = $val;
        return $this;
    }

    public function setTimeout($sec)
    {
        $this->timeout = $sec;
        return $this;
    }

    public function request($action, $data=array(), $method='get', $type='array')
    {
        $this->initReturn();

        $url = $this->api_url . $action;
        $ch = curl_init();
        if ($method == 'get') {
            $url .= '?' . ($data ? http_build_query($data, NULL, '&') : '');
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        } elseif ($method == 'post') {
            curl_setopt($ch, CURLOPT_URL, $url);
            //curl_setopt($ch, CURLOPT_POST,1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST,  'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        }

        $response = curl_exec($ch);
        $result = json_decode($response);
        curl_close($ch);

        if (isset($result->status) && $result->status == 200) {
            if ($type == 'array') {
                $this->return->data = $this->std_class_object_to_array($result->data);
            } else {
                $this->return->data = $result->data;
            }
            $this->return->status = TRUE;
        // } elseif (isset($result->message)) {
        } else {
            if (isset($result->message)) {
                $this->return->message = $result->message;
            }
            if (isset($result->error)) {
                $this->return->message = $result->error;
            }
            if (empty($this->message) && empty($this->error)) {
                // jdd($url);
                // jd($response);
            }
        }

        return $this->return;
    }

    private function std_class_object_to_array($stdclassobject)
    {
        $array = array();
        if ($stdclassobject) {
            $_array = is_object($stdclassobject) ? get_object_vars($stdclassobject) : $stdclassobject;

            if (is_array($_array)) {
                foreach ($_array as $key => $value) {
                    $value = (is_array($value) || is_object($value)) ? $this->std_class_object_to_array($value) : $value;
                    $array[$key] = $value;
                }
            } else {
                return $_array;
            }
        }
        return $array;
    }
}

?>


