<?php
class every8d {
	const UID = '53900711';
	const PWD = 'a5ar';
	const SEND_URL = 'http://api.every8d.com/API21/HTTP/sendSMS.ashx';
	const POINTS_INQUIRY_URL = 'http://api.every8d.com/API21/HTTP/getcredit.ashx';

	private $SB;
	private $MSG;
	private $DEST;// 手机号码多笔接收人时，请以半形逗点隔开( , )，如0912345678,0922333444。
	private $ST;// 简讯预定发送时间。- 立即发送：请传入空字串- 预约发送：请传入预计发送时间，若传送时间小于系统接单时间，将不予传送。格式为yyyyMMddHHmnss；例如: 预约2009/01/3115:30:00 发送，则传入20090131153000。若传递时间已逾现在之时间且该时间在24 小时内，将立即发送。

	public function __construct() {
		$this->CI =& get_instance();
	}

	public function setSB($SB)
	{
		$this->SB = htmlspecialchars($SB);
	}

	public function setMSG($MSG)
	{
		$this->MSG = htmlspecialchars($MSG);
	}

	public function setDEST($DEST)
	{
		$this->DEST = htmlspecialchars($DEST);
	}

	public function setST($ST)
	{
		$this->ST = htmlspecialchars($ST);
	}

	public function getSendUrl()
	{
		return every8d::SEND_URL. "?UID=". every8d::UID ."&PWD=". every8d::PWD ."&SB={$this->SB}&MSG={$this->MSG}&DEST={$this->DEST}&ST={$this->ST}";
	}

	public function send()
	{
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, $this->getSendUrl());
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$response = curl_exec($ch);
		curl_close($ch);

		// $response = explode(',', $response);
		return $response;
	}

	public function points_inquiry()
	{

		$url = every8d::POINTS_INQUIRY_URL . "?UID=". every8d::UID .'&PWD='. every8d::PWD;
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$points = curl_exec($ch);
		curl_close($ch);

		return $points;
	}

}
