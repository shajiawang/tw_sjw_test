<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Short description for payment/Spgateway.php
 *
 * @company 智付通
 * @website https://www.spgateway.com
 */

class Pay_spgateway
{
	private $CI;
	private $MerchantID;
	private $HashKey;
	private $HashIV;
	private $CheckoutURL;

	public function __construct()
	{
		$this->CI =& get_instance();
	}

	public function setConfig($params=array())
	{
        if (isset($params['MerchantID'])) {
            $this->MerchantID = $params['MerchantID'];
        } else {
            die('MerchantID 未定義');
        }
        if (isset($params['HashKey'])) {
            $this->HashKey = $params['HashKey'];
        } else {
            die('HashKey 未定義');
        }
        if (isset($params['HashIV'])) {
            $this->HashIV = $params['HashIV'];
        } else {
            die('HashIV 未定義');
        }
        if (isset($params['CheckoutURL'])) {
            $this->CheckoutURL = $params['CheckoutURL'];
        } else {
            die('CheckoutURL 未定義');
        }
		return $this;
	}

    public function Checkout($params=array())
    {
        $html = $this->getCheckoutString($params);
        print($html);
    }

    public function getCheckoutString($params=array())
    {
		$data = array();
		$data['MerchantID'] = $this->MerchantID;
        $data['RespondType'] = 'JSON';
		$data['TimeStamp'] = strtotime($params['create_at']);  // 廠商交易時間 yyyy/MM/dd HH:mm:ss
        $data['Version'] = '1.2';
        $data['LangType'] = 'zh-tw';
        $data['MerchantOrderNo'] = $params['order_no'];
		$data['Amt'] = $params['total'];
		$data['ItemDesc'] = $params['trade_desc'];
		$data['TradeLimit'] = 900;  // 超過秒數視同交易失敗
        $data['LoginType'] = 0;
        $data['OrderComment'] = '';  // 訂單備註內容
		$data['CheckValue'] = $this->getCheckValue($data['TimeStamp'], $data['MerchantOrderNo'], $data['Version'], $data['Amt']);

        if (isset($params['contact_email'])) {
            $data['Email'] = $params['contact_email'];
        }
        if (isset($params['notify_url'])) {
            $data['NotifyURL'] = $params['notify_url'];
        }
        if (isset($params['return_url'])) {
            $data['ReturnURL'] = $params['return_url'];
        }

        // 信用卡參數
        $data['CREDIT'] = 1;
        // $data['InstFlag'] = 0;  // 分期
        // // $data['CreaditRed'] = '0';  // 紅利
        // //
        // // WEBATM
        // $data['WEBATM'] = 0;

        // // ATM
        // $data['VACC'] = 0;

        // // 超商代碼
        // $data['CVS'] = '0';

        // // 超商條碼
        // $data['BARCODE'] = 0;
        // jd($data,1);


        $html = "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n";
        $html .= "<form id=\"CheckoutForm\" action=\"{$this->CheckoutURL}\" method=\"post\">\n";
        foreach ($data as $k => $v) {
            $html .= "<input type=\"hidden\" name=\"{$k}\" value=\"{$v}\">\n";
        }
        $html .= "<script type=\"text/javascript\">document.getElementById(\"CheckoutForm\").submit();</script>";
        $html .= "</form>\n";

		return $html;
	}

	public function result($response)
	{
		$result = array(
			'status' => FALSE,
            'message' => '',
            'response' => $response['JSONData'],
        );

        $data = json_decode($response['JSONData']);
        // jdd($data);
        $params = json_decode($data->Result);

        if (!$this->verifyCheckCode($params->CheckCode, $params->Amt, $params->MerchantOrderNo, $params->TradeNo)) {
            $result['message'] = 'verify check code error';
        } else {
            $result['status'] = ($data->Status == 'SUCCESS')? TRUE : FALSE;
            $result['message'] = $data->Message;
            $result['payment_trade_no'] = $params->TradeNo;
            $result['order_no'] = $params->MerchantOrderNo;
            $result['auth_code'] = $params->Auth;
            $result['payment_time'] = date('Y-m-d H:i:s', strtotime($params->PayTime));
            $result['amount'] = $params->Amt;
            $result['card6no'] = isset($params->Card6No)? $params->Card6No : '';
            $result['card4no'] = isset($params->Card4No)? $params->Card4No : '';
        }

        return $result;
	}

	private function getCheckValue($ts, $order_no, $version, $amt)
	{
        $mer_array = array(
            'MerchantID' => $this->MerchantID,
            'TimeStamp' => $ts,
            'MerchantOrderNo'=> $order_no,
            'Version' => $version,
            'Amt' => $amt,
        );
        ksort($mer_array);
        $check_merstr = http_build_query($mer_array);
        $CheckValue_str ="HashKey={$this->HashKey}&$check_merstr&HashIV={$this->HashIV}";
        $CheckValue = strtoupper(hash("sha256", $CheckValue_str));
        return $CheckValue;


		// // A-Z排序
		// ksort($data);

		// $str = "HashKey={$this->HashKey}";
		// $str .= '&Amt=' . urldecode($data['Amt']);
		// $str .= '&MerchantID=' . urldecode($data['MerchantID']);
		// $str .= '&MerchantOrderNo=' . urldecode($data['MerchantOrderNo']);
		// $str .= '&TimeStamp=' . urldecode($data['TimeStamp']);
		// $str .= '&Version=' . urldecode($data['Version']);
		// $str .= '&HashIV=' . $this->HashIV;

		// return strtoupper(hash("sha256", $str));
	}

	private function verifyCheckCode($code, $amt, $order_no, $trade_no)
	{
        $check_code = array(
            'MerchantID' => $this->MerchantID,
            "Amt" => $amt,
            "MerchantOrderNo" => $order_no,
            "TradeNo" => $trade_no,
        );
        ksort($check_code);
        $check_str = http_build_query($check_code);
        $CheckCode = "HashIV={$this->HashIV}&{$check_str}&HashKey={$this->HashKey}";
        $CheckCode = strtoupper(hash("sha256", $CheckCode));
		return ($code == $CheckCode);
	}


}

