<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	http://codeigniter.com/user_guide/general/hooks.html
|
| pre_system
| 在系统执行的初期执行。这时只有benchmark及hook类别被载入。路由或其他程序都还没执行。
| pre_controller
| 在所有的控制器(controller)呼叫之前执行。此时所有的基础类别、路由、安全检查都已经完成。
| post_controller_constructor
| 在控制器(controller)实例化之后但是任何方法都还未呼叫之前立刻执行。
| post_controller
| 在控制器(controller)执行完毕之后立刻执行。
| display_override
| 覆盖用来在系统执行完毕后向浏览器送出完成的页面的 _display() 函数。这样就允许你用你自己定义的显示方法。注意，你必须用 $this->CI =& get_instance() 取得CI参考物件然后才可透过呼叫 $this->CI->output->get_output() 来使用完成的页面资料。
| cache_override
| 让你可以呼叫自己定义的函数而非output类别中的 _display_cache() 函数。这让你可以使用自己的cache显示机制。
| post_system
| 在完成的页面送到浏览器之后呼叫。在系统执行结束时，完成的资料已送到浏览器之后执行。
*/

$hook['post_controller_constructor'][] = array(
    'class'    => 'Acl',
    'function' => 'permission',
    'filename' => 'acl.php',
    'filepath' => 'hooks',
    'params'   => array()
);


