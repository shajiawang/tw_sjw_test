<!doctype html>
<html>
<head>
    <meta charset="utf-8">
</head>
<body style="font-size: 13px;">
    <div style=" width:800px;background-color: #fff; margin: 0 auto; font-family:微軟正黑體;">

        <header style="">
            <div style="margin-top: 20px auto;text-align: center;">
                <img src="<?=$logo_url;?>" style="width: 100%;"><!--帶入網站LOGO-->
            </div>
        </header>

        <article style="padding: 10px 20px; background-color:rgba(250,250,250,1);  border-bottom:1px solid rgba(153,153,153,1);  border-top:1px solid rgba(153,153,153,1);">
            <h1 align="center">恭喜您成功購買【<?=$order['items'][0]['name'];?>】<?=$order['items'][0]['quantity'];?> 張</h1>
            <div><?=$content;?></div>
        </article>

    </div>
</body>
</html>
