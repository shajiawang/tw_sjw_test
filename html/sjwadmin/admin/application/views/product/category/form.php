<script src="<?=$static_plugin;?>ckeditor/ckeditor.js"></script>
<script>
$(document).ready(function() {
    $('[name=upload]').change(function(){
        var file = this.files[0];
        var file_size = Math.floor(file.size / 1000);
        var size_limit = parseInt('<?=$_SETTING['admin_file_size_max'];?>') * 1024;
        var ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
        var types = ['gif', 'png', 'jpg', 'jpeg', 'bmp'];
        if (types.indexOf(ext) < 0){
            my_alert(3, '只能上傳圖片格式 '+ types.join(', '), 3, 'center')
            $(this).val('');
        } else {
            if (file_size > size_limit) {
                my_alert(3, '上傳圖片的大小限制為 '+ (size_limit/1024) +' MB', 3, 'center')
                $(this).val('');
            } else {
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function(e) {
                    $('.img-show').html('<img src="'+ e.target.result +'" class="img-thumbnail"><a href="javascript: void(0);" class="img-close btn btn-link btn-xs" onclick="imgClose(this)"><i class="fa fa-times-circle"></i></a>');
                }
                $('[name=image]').val($(this).val());
            }
        }
    });
});
var imgClose = function(obj){
    $(obj).parent().parent().find('[name=upload]').val('');
    $(obj).parent().parent().find('[name=image]').val('');
    $(obj).closest('.img-show').html('');
}
</script>

<?php if (validation_errors()) { ?>
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <?=validation_errors();?>
</div>
<?php } ?>

<form id="data-form" role="form" method="post" action="<?=$link_save;?>"  enctype="multipart/form-data">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
    <input type="hidden" name="kind" value="<?=$form['kind'];?>" />

    <?php if ($level_max > 1) { ?>
    <div class="form-group <?=form_error("parent_id")?'has-error':'';?>">
        <label class="control-label">上層選單</label>
        <?php
            $parent_list = array('0'=>'-- None -- ') + $choices['parent_list'];
            echo form_dropdown('parent_id', $parent_list, $form['parent_id'], 'class="form-control"');
        ?>
        <?=form_error("parent_id"); ?>
    </div>
    <?php } else { ?>
    <input type="hidden" name="parent_id" value="0" />
    <?php } ?>
    <div class="form-group <?=form_error("name")?'has-error':'';?>">
        <label class="control-label">名稱</label>
        <input class="form-control" name="name" placeholder="" value="<?=set_value("name", $form['name']);?>">
        <?=form_error("name"); ?>
    </div>
    <div class="form-group">
        <label class="control-label">圖片</label>
        <div class="img-show">
            <?php
                if ($form['image']) {
                    echo img(array('src'=>$form['image_thumb_url'], 'class'=>'img-thumbnail'));
                    echo '<a href="javascript: void(0);" class="img-close btn btn-link  btn-xs" onclick="imgClose(this)"><i class="fa fa-times-circle"></i></a>';
                }
            ?>
        </div>
        <input type="file" name="upload">
        <input type="hidden" name="image" value="<?=$form['image'];?>">
        <p class="help-block">檔案大小限制 <?=$_SETTING['admin_file_size_max'];?>MB，建議圖片尺寸 900 * 1200px。</p>
    </div>

    <div class="form-group <?=form_error("content")?'has-error':'';?>">
        <label class="control-label">內容</label>
        <textarea class="ckeditor" name="description"><?=set_value("description", $form['description']);?></textarea>
        <?=form_error("content"); ?>
    </div>

    <div class="form-group <?=form_error('sort_order')?'has-error':'';?>">
        <label class="control-label">排序</label>
        <input class="form-control" name="sort_order" value="<?=set_value('sort_order', $form['sort_order']); ?>">
        <?=form_error('sort_order'); ?>
    </div>

    <div class="form-group required <?=form_error('enable')?'has-error':'';?>">
        <label class="control-label">是否啟用</label>
        <div>
            <div class="radio-inline">
                <label>
                    <input type="radio" value="1" name="enable" <?=set_radio('enable', '1', $form['enable']==1);?>>
                    <span style="color: green;">是　</span>
                </label>
            </div>
            <div class="radio-inline">
                <label>
                    <input type="radio" value="0" name="enable" <?=set_radio('enable', '0', $form['enable']==0);?>>
                    <span style="color: red;">否　</span>
                </label>
            </div>
            <?=form_error("enable");?>
        </div>
    </div>
    <div class="text-center">
        <button class="btn btn-primary"><i class="fa fa-save"></i> 確認送出</button>
        <a class="btn btn-default" href="<?=$link_cancel;?>"><i class="fa fa-reply"></i> 返回</a>
    </div>
</form>

