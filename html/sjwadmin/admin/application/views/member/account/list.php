<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
			<div class="panel-heading">
				<i class="fa fa-list fa-lg"></i> <?=$_LOCATION['name'];?>
			</div>
			<!-- /.panel-heading -->

			<div class="panel-body">
				<div class="row">
					<form id="filter-form" role="form" class="form-inline">
						<input type="hidden" name="sort" value="" />
						<?php /*
						<div class="col-xs-12" >
							<div class="form-group">
								<label class="control-label">群組</label>
								<?php
									$choices['group'] = array('all'=>'全部') + $choices['group'];
									echo form_dropdown('user_group_id', $choices['group'], $filter['user_group_id'], 'class="form-control"');
								?>
							</div>
						</div>*/?>
						<div class="col-xs-6" >
							<div class="form-group">
								<label class="control-label">顯示筆數</label>
								<?php
									echo form_dropdown('rows', $choices['rows'], $filter['rows'], 'class="form-control"');
								?>
							</div>
						</div>
						<div class="col-xs-6 text-right">
							<div class="form-group">
								<label class="control-label"><i class="fa fa-search"></i></label>
								<input type="text" class="form-control" name="q" placeholder="搜尋名稱,帳號,email,電話" value="<?=$filter['q'];?>">
							</div>
						</div>
					</form>
				</div>
				<form id="list-form" method="post">
					<input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
					<table class="table table-bordered table-condensed table-hover">
						<thead>
							<tr>
								<th style="width: 35px;" class="text-center"><input type="checkbox" id="chkall"></th>
								<th class="sorting<?=($filter['sort']=='name asc')?'_asc':'';?><?=($filter['sort']=='name desc')?'_desc':'';?>" data-field="name" >名稱</th>
								<?php /*<th class="sorting<?=($filter['sort']=='user_group_id asc')?'_asc':'';?><?=($filter['sort']=='user_group_id desc')?'_desc':'';?>" data-field="user_group_id" >群組</th>*/?>
								<th class="sorting<?=($filter['sort']=='account asc')?'_asc':'';?><?=($filter['sort']=='account desc')?'_desc':'';?>" data-field="account" >帳號</th>
								<th class="sorting<?=($filter['sort']=='email asc')?'_asc':'';?><?=($filter['sort']=='email desc')?'_desc':'';?>" data-field="email" >E-Mail</th>
								<th class="sorting<?=($filter['sort']=='telephone asc')?'_asc':'';?><?=($filter['sort']=='telephone desc')?'_desc':'';?>" data-field="telephone" >聯絡電話</th>
								<!-- <th>建立時間</th> -->
								<th class="sorting<?=($filter['sort']=='enable asc')?'_asc':'';?><?=($filter['sort']=='enable desc')?'_desc':'';?>" data-field="enable" >啟用</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($list as $row) { ?>
							<tr class="<?=($row['enable']==0)?'text-danger':'';?>">
								<td class="text-center"><input type="checkbox" name="rowid[]" value="<?=$row['id'];?>"></td>
								<td><?=$row['name'];?></td>
								<?php /*<td><?=implode(', ', $row['groups']);?></td>*/?>
								<td><?=$row['account'];?></td>
								<td><?=$row['email'];?></td>
								<td><?=$row['telephone'];?></td>
								<!-- <td><?=$row['create_at'];?></td> -->
								<td><?=($row['enable'] == 2)?'<span class="text-success">是</span>':'否';?></td>
								<td class="text-center" id="btn_group">
									<?php if (isset($row['link_view'])) { ?>
									<a type="button" class="btn btn-outline btn-success btn-xs btn-toggle" title="瀏覽" href="<?=$row['link_view'];?>">
										<i class="fa fa-eye fa-lg"></i>
									</a>
									<?php } ?>
									
									<?php if (isset($row['link_edit'])) { ?>
									<a type="button" class="btn btn-outline btn-warning btn-xs btn-toggle" href="<?=$row['link_edit'];?>">
										<i class="fa fa-pencil fa-lg"></i>
									</a>
									<?php } ?>
									
									<?php if (isset($row['link_info'])) { ?>
									<a type="button" class="btn btn-outline btn-success btn-xs btn-toggle" href="<?=$row['link_info'];?>">
										<i class="fa fa-edit fa-lg"></i>
									</a>
									<?php } ?>
									
									<?php if (isset($row['link_delete'])) { ?>
									<button type="button" class="btn btn-outline btn-danger btn-xs" onclick="ajaxDelete(this, '確認要刪除選單「<?=$row['name'];?>」?', '<?=$row['link_delete'];?>')">
										<i class="fa fa-trash fa-lg"></i>
									</button>
									<?php } ?>
								</td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
				</form>
				
				<div class="row">
                    <div class="col-lg-4">
                        Showing <?=$filter['offset']+1;?>
                        to <?=(($filter['offset']+$filter['rows'])>$filter['total'])?$filter['total']:$filter['offset']+$filter['rows'];?>
                        of <?=$filter['total'];?> entries
                    </div>
                    <div class="col-lg-8 text-right">
                        <?=$this->pagination->create_links();?>
                    </div>
                </div>
				
			</div>
			<!-- /.panel-body -->
		</div>
		<!-- /.panel -->
	</div>
	<!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script src="<?=$static_plugin;?>jquery.highlight-3.js"></script>
<script>
$(document).ready(function() {
	$('#filter-form select').change(function(){
		$('#filter-form').submit();
	});

	<?php if (isset($filter['q']) && $filter['q'] != ''){ ?>
	$('#list-form').highlight('<?=$filter['q'];?>');
	<?php } ?>
});
</script>
