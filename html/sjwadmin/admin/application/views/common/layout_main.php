<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset=utf-8"utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=$_SETTING['admin_title'];?></title>

    <!-- Custom Fonts -->
    <link href="<?=$static_plugin;?>font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Animate Core CSS -->
    <link href="<?=$static_plugin;?>animate/animate.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="<?=$static_plugin;?>bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap DateTime and Date Picker CSS -->
    <link href="<?=$static_plugin;?>bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet">
    <link href="<?=$static_plugin;?>bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet">

    <!-- jQuery UI CSS -->
    <!-- <link href="<?=$static_plugin;?>jqueryui/jquery&#45;ui.min.css" rel="stylesheet"> -->

    <!-- MetisMenu CSS -->
    <link href="<?=$static_plugin;?>metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?=$static_css;?>sb-admin-2.css" rel="stylesheet">

    <!-- msdropdown CSS -->
    <link href="<?=$static_plugin;?>msdropdown/css/msdropdown/dd.css" rel="stylesheet">

    <!-- Self CSS -->
    <link href="<?=$static_css;?>style.css" rel="stylesheet">
    <link href="<?=$static_css;?>calendar.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery -->
	<script src="<?=$static_plugin;?>jquery-1.11.2.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?=$static_plugin;?>bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?=$static_plugin;?>metisMenu/dist/metisMenu.min.js"></script>

	<!-- Noty jquery notification plugin -->
    <script src="<?=$static_plugin;?>noty/packaged/jquery.noty.packaged.min.js"></script>

    <!-- Add mousewheel plugin (this is optional) -->
    <script type="text/javascript" src="<?=$static_plugin;?>jquery.mousewheel-3.0.6.pack.js"></script>

    <!-- Add fancyBox main JS and CSS files -->
    <script type="text/javascript" src="<?=$static_plugin;?>fancybox/jquery.fancybox.js?v=2.1.5"></script>
    <link rel="stylesheet" type="text/css" href="<?=$static_plugin;?>fancybox/jquery.fancybox.css?v=2.1.5" media="screen" />

    <script type="text/javascript">
    <?php if (isset($_JSON)){ ?>
        var MY = MY || <?=json_encode($_JSON);?> || {};
    <?php } ?>
	
	<?php /*
	//console.log(MY._ALERT);
	$(document).ready(function(){
        // $(".fancybox-thumbs").fancybox({
            // openEffect  : 'none',
            // closeEffect : 'none'
        // });
        $("a[rel=fancybox_group]").fancybox({
            prevEffect : 'none',
            nextEffect : 'none',
            closeBtn  : true,
            // arrows    : true,
            // nextClick : true,
        });
    });
	*/?>
    </script>
</head>
<body>
    <div id="wrapper">
        <?=$__header;?>

        <!-- Page Content -->
        <div id="page-wrapper">
            <div class="page-header">
                <div class="container-fluid">
                    <div class="row">
                        
						<div class="col-lg-7">
                            <?php if ($this->uri->uri_string() == 'dashboard' || $this->uri->uri_string() == 'notice' ) { ?>
                            
							<h1><?=$_LOCATION['name'];?></h1>
							
                            <?php } elseif ($this->uri->uri_string() == 'profile') { ?>
							
                            <h1>用戶資料編輯</h1>
							
                            <?php } else { ?>
							
                            <h1><?=(isset($_LOCATION['function']))?$_LOCATION['function']['name']:$_LOCATION['name'];?></h1>
                            
							<ol class="breadcrumb">
                                <?php if (isset($_LOCATION['parent'])) { ?>
                                <li><?=$_LOCATION['parent']['name'];?></li>
                                <?php } ?>
								
                                <?php if (isset($_LOCATION['function'])) { ?>
                                
								<li><a href="<?=base_url($_LOCATION['function']['link']);?>"><?=$_LOCATION['function']['name'];?></a></li>
								<li><?=$_LOCATION['name'];?></li>
                                
								<?php }  else { ?>
                                
								<li><a href="<?=base_url($_LOCATION['link']);?>"><?=$_LOCATION['name'];?></a></li>
                                
								<?php } ?>
                            </ol>
							
                            <?php } ?>
                        </div>
						
                        <div class="col-lg-5 text-right">
                            <?php if (isset($link_add)) { ?>
                            <a class="btn btn-primary" href="<?=$link_add;?>" title="Add"><i class="fa fa-plus"></i></a>
                            <?php } ?>
							
							<?php if (isset($link_action_add)) { ?>
							<button class="btn btn-primary" onclick="actionAdd('<?=$link_action_add;?>')" title="Add"><i class="fa fa-plus"></i></button>
                            <?php } ?>

                            <?php if (isset($link_save)) { ?>
                            <a class="btn btn-primary btn-save" href="#" title="Save"><i class="fa fa-save"></i></a>
                            <?php } ?>

                            <?php if (isset($link_action_save)) { ?>
                            <button class="btn btn-danger" onclick="actionSave('<?=$link_action_save;?>')" title="Save"><i class="fa fa-save"></i></button>
                            <?php } ?>
							
							<?php if (isset($link_delete)) { ?>
                            <button class="btn btn-danger" onclick="actionDelete('<?=$link_delete;?>')" title="Delete"><i class="fa fa-trash"></i></button>
                            <?php } ?>

                            <?php if (isset($link_viewd)) { ?>
                            <button class="btn btn-success" onclick="actionSubmit('form-list')" title="Viewd"><i class="fa fa-eye"></i></button>
                            <?php } ?>

                            <?php if (isset($link_fax)) { ?>
                            <button class="btn btn-success" onclick="actionFax('<?=$link_fax;?>')" title="Fax"><i class="fa fa-fax"></i></button>
                            <?php } ?>

                            <?php if (isset($link_export)) { ?>
                            <a class="btn btn-success" href="<?=$link_export;?>" title="Export" target="_block"><i class="fa fa-file-excel-o"></i></a>
                            <?php } ?>


                            <?php if (isset($link_cancel)) { ?>
                            <a class="btn btn-default" href="<?=$link_cancel;?>" title="Cancel"><i class="fa fa-reply"></i></a>
                            <?php } ?>

                            <?php if (isset($link_refresh)) { ?>
                            <a class="btn btn-default" href="<?=$link_refresh;?>" title="Refresh"><i class="fa fa-refresh"></i></a>
                            <?php } ?>
                        </div>
						
                    </div>
                </div>
            </div>

            <div class="container-fluid">
                <?=$__content;?>
            </div>

        </div>
        <!-- /#page-wrapper -->

        <?=$__footer;?>
    </div>

    <script src="<?=$static_plugin;?>bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
	<script src="<?=$static_plugin;?>bootstrap-datepicker/locales/bootstrap-datepicker.zh-TW.min.js"></script>
	<script src="<?=$static_plugin;?>bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
	<script src="<?=$static_plugin;?>bootstrap-datetimepicker/js/locales/bootstrap-datetimepicker.zh-TW.js"></script>

    <!-- DataTables JavaScript -->
    <script src="<?=$static_plugin;?>datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?=$static_plugin;?>datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>

    <script src="<?=$static_plugin;?>msdropdown/js/jquery.dd.min.js" type="text/javascript"></script>
    <script src="<?=$static_plugin;?>moment-with-locales.js"></script>
    <script src="<?=$static_plugin;?>jStarbox/jstarbox.js"></script>

    <script src="<?=$static_js;?>my.js"></script>

</body>
</html>
