<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <i class="fa fa-eye fa-fw"></i> <?=$_LOCATION['name'];?>
            </div>
            <div class="panel-body">
                <p>
                    <a href="<?=$link_edit;?>" class="btn btn-success">
                        <i class="fa fa-pencil"></i> 前往修改頁面
                    </a>
                </p>
                <table class="table table-hover table-bordered table-condensed">
                    <tr>
                        <th>分類</th>
                        <td>
                            <?php foreach ($form['category'] as $category_id) { ?>
                            <?=$choices['category'][$category_id];?>
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <th>圖片</th>
                        <td>
                                <?php
                                if (strlen($form['image']) > 0) {
                                    echo '<a rel="fancybox_group" href="'. $form['image_src'] .'">';
                                    echo '  <img src="'. $form['image_thumb_src'] .'" class="img-rounded" style="width: 60px; height: 50px;" />';
                                    echo '</a>';
                                   } else{
                                        echo '<i class="fa fa-plus fa-2x">noimge</i>' ;
                                    }
                                    if(isset($form['subimage'])){
                                   foreach ($form['subimage'] as $num => $row) {
                                    if (strlen($row['image']) > 0) {
                                        echo '<a rel="fancybox_group" href="'. $row['image_src'] .'">';
                                        echo '  <img src="'. $row['image_thumb_src'] .'" class="img-rounded" style="width: 60px; height: 50px;" />';
                                        echo '</a>';
                                    }else{
                                        echo '<i class="fa fa-plus fa-2x">noimge</i>' ;
                                    }
                                   }
                               }
                                   //jdd($form['subimage']) ;

                                ?>
                        </td>
                    </tr>
                    <tr>
                        <th width="20%">產品名稱</th>
                        <td style="background-color: #fff;">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <li>
                                        <a href="#title_<?=$lang['code'];?>" role="tab" data-toggle="tab">
                                            <?=img(HTTP_IMG ."flags/24/{$lang['image']}");?>
                                            <?=$lang['name'];?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" style="padding-top: 8px;">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <?php
                                        $title = isset($form['name'][$lang['id']])? $form['name'][$lang['id']] : '';
                                    ?>
                                    <div class="tab-pane" id="title_<?=$lang['code'];?>"><?=$title;?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th>產品介紹</th>
                        <td style="background-color: #fff;">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <li>
                                        <a href="#content_<?=$lang['code'];?>" role="tab" data-toggle="tab">
                                            <?=img(HTTP_IMG ."flags/24/{$lang['image']}");?>
                                            <?=$lang['name'];?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" style="padding-top: 8px;">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <?php
                                        $content = isset($form['description'][$lang['id']])? $form['description'][$lang['id']] : '';
                                    ?>
                                    <div class="tab-pane" id="content_<?=$lang['code'];?>"><?=$content;?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <!-- <tr>
                        <th width="20%">Meta Title</th>
                        <td style="background-color: #fff;">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <li>
                                        <a href="#title_<?=$lang['code'];?>" role="tab" data-toggle="tab">
                                            <?=img(HTTP_IMG ."flags/24/{$lang['image']}");?>
                                            <?=$lang['name'];?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" style="padding-top: 8px;">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <?php
                                        $meta_title = isset($form['meta_title'][$lang['id']])? $form['meta_title'][$lang['id']] : '';
                                    ?>
                                    <div class="tab-pane" id="meta_title_<?=$lang['code'];?>"><?=$meta_title;?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th width="20%">Meta Description</th>
                        <td style="background-color: #fff;">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <li>
                                        <a href="#title_<?=$lang['code'];?>" role="tab" data-toggle="tab">
                                            <?=img(HTTP_IMG ."flags/24/{$lang['image']}");?>
                                            <?=$lang['name'];?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" style="padding-top: 8px;">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <?php
                                        $meta_description = isset($form['meta_description'][$lang['id']])? $form['meta_description'][$lang['id']] : '';
                                    ?>
                                    <div class="tab-pane" id="meta_description_<?=$lang['code'];?>"><?=$meta_description;?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th width="20%">Meta Keyword</th>
                        <td style="background-color: #fff;">
                            <div class="tabbable tabs-left">
                                <ul class="nav nav-tabs">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <li>
                                        <a href="#title_<?=$lang['code'];?>" role="tab" data-toggle="tab">
                                            <?=img(HTTP_IMG ."flags/24/{$lang['image']}");?>
                                            <?=$lang['name'];?>
                                        </a>
                                    </li>
                                    <?php } ?>
                                </ul>
                                <div class="tab-content" style="padding-top: 8px;">
                                    <?php foreach ($_LANGUAGE as $lang) { ?>
                                    <?php
                                        $meta_keyword = isset($form['meta_keyword'][$lang['id']])? $form['meta_keyword'][$lang['id']] : '';
                                    ?>
                                    <div class="tab-pane" id="meta_keyword_<?=$lang['code'];?>"><?=$meta_keyword;?></div>
                                    <?php } ?>
                                </div>
                            </div>
                        </td>
                    </tr> -->



                    <tr>
                        <th>排序</th>
                        <td><?php echo $form['sort_order'];?></td>
                    </tr>
                    <tr>
                        <th>啟用</th>
                        <td><?=($form['enable']=='1')?'<span style="color: green;">是</span>':'<span style="color: red;">否</span>';?></td>
                    </tr>
                </table>

            </div>
        </div>
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
<script>
$(function(){
    $('ul.nav-tabs').each(function(){
        $(this).find('li:first').addClass('active');
    });
    $('div.tab-content').each(function(){
        $(this).find('.tab-pane:first').addClass('active');
    });
});
</script>
