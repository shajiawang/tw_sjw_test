<link href="<?=$static_plugin;?>lou-multi-select/css/multi-select.css" media="screen" rel="stylesheet" type="text/css">
<style>
.ms-container {
    width: 100%;
}
.ms-container .ms-list  {
    height: 150px;
}
</style>
<script src="<?=$static_plugin;?>lou-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script type="text/javascript" src="<?=$static_plugin;?>My97DatePicker/4.8/WdatePicker.js"></script>
<script src="<?=$static_plugin;?>ckeditor/ckeditor.js"></script>
<script>
$(document).ready(function() {
    $('select[name="category_id[]"]').multiSelect({
        selectableHeader: "<div class='custom-header'>可選分類</div>",
        selectionHeader: "<div class='custom-header'>選定分類</div>",
    });
    $('#select-all').click(function(){
        $('select[name="category_id[]"]').multiSelect('select_all');
        return false;
    });
    $('#deselect-all').click(function(){
        $('select[name="category_id[]"]').multiSelect('deselect_all');
        return false;
    });
});
var imgUpload = function(obj, ) {
    var file = obj.files[0];
    var file_size = Math.floor(file.size / 1000);
    var size_limit = parseInt('<?=$_SETTING['upload_size_limit'];?>') * 1024;
    var ext = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
    var types = ['gif', 'png', 'jpg', 'jpeg', 'bmp'];
    if (types.indexOf(ext) < 0){
        my_alert(3, '只能上傳圖片格式 '+ types.join(', '), 3, 'center')
        $(obj).val('');
    } else {
        if (file_size > size_limit) {
            my_alert(3, '上傳圖片的大小限制為 '+ (size_limit/1024) +' MB', 3, 'center')
            $(this).val('');
        } else {
            var reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = function(e) {
                $(obj).parent().find('.img-show').html('<img src="'+ e.target.result +'" class="img-thumbnail"><a href="javascript: void(0);" class="img-close btn btn-link btn-xs" onclick="imgClose(this)"><i class="fa fa-times-circle"></i></a>');
            }
            $(obj).parent().find('[type=hidden]').val($(obj).val());
        }
    }
}
var imgClose = function(obj){
    $(obj).parent().parent().find('[name=upload]').val('');
    $(obj).parent().parent().find('[name=image]').val('');
    $(obj).closest('.img-show').html('');
}
function removeItem(obj) {
    $(obj).closest('tr').remove();
}

function addSubImage() {
    var num = $('#subimage table tbody tr').size();
    var html = '';
    html += '<tr rowid="0">';
    html += '   <input type="hidden" name="subimage['+ num +'][id]" value="0">';
    html += '   <td>';
    html += '       <div class="img-show"></div>';
    html += '       <input type="file" name="subimage['+ num +']" onchange="imgUpload(this);">';
    html += '       <input type="hidden" name="subimage['+ num +'][image]">';
    html += '   </td>';
    html += '   <td><input type="text" class="form-control" name="subimage['+ num +'][name]" value=""></td>';
    html += '   <td><input type="text" class="form-control" name="subimage['+ num +'][sort_order]" value="0"></td>';
    html += '   <td align="right"><button type="button" class="btn btn-danger btn-sm" onclick="removeItem(this)"><i class="fa fa-minus-circle"></i></button></td>';
    html += '</tr>';

    $('#subimage table tbody').append(html);
}
</script>
<?php if (validation_errors()) { ?>
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <?=validation_errors();?>
</div>
<?php } ?>

<?php
//廠商ID
if( $page_name=='add' && empty($company_id) ){
?>
<form id="filter-form" role="form" class="form-inline">
	<div class="row">
		<div class="form-group">
			<label class="control-label"><i class="fa fa-search"></i></label>
			<input type="text" id="form_cname" name="cname" placeholder="搜尋廠商名稱" class="form-control" value="<?=$filter['cname'];?>">
		</div>
	</div>
</form>

<?php } else { ?>

<?php if( $page_name=='add'){?>
<form id="filter-form" role="form" class="form-inline">
	<div class="row">
		<div class="form-group">
			<label class="control-label">廠商: </label>
			<?=$_SESSION['search_cname'];?>
		</div>
	</div>
</form>
<?php } ?>

<form id="data-form" role="form" method="post" action="<?=$link_save;?>"  enctype="multipart/form-data">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
	<input type="hidden" name="company_id" value="<?=$company_id;?>" />
			
	<!-- Nav tabs -->
    <?php if($page_name=='edit'){ ?>
	<ul class="nav nav-tabs" role="tablist" id="tab_kind">
        <li class="active"><a href="#general" role="tab" data-toggle="tab">一般</a></li>
		<?php /*<li><a href="#subimage" role="tab" data-toggle="tab">展示圖</a></li>*/?>
    </ul>
	<?php } ?>

    <!-- Tab panes -->
    <div class="tab-content" style="padding: 15px;">
        <div class="tab-pane active" id="general">
            <div class="form-group required <?=form_error("name")?'has-error':'';?>">
                <label class="control-label">商品名稱</label>
                <input class="form-control" name="name" placeholder="" value="<?=set_value("name", $form['name']);?>">
                <?=form_error("name");?>
            </div>

            <div class="form-group required <?=form_error('no')?'has-error':'';?>">
                <label class="control-label">產品編號</label>
                <input class="form-control" name="no" value="<?=set_value('no', $form['no']);?>">
                <?=form_error('no');?>
            </div>

            <?php /*<div class="form-group <?=form_error('category_id')?'has-error':'';?>">
                <label class="control-label">類別</label>
                <button type="button" class="btn btn-link btn-sm" id="select-all">選擇全部</button> /
                <button type="button" class="btn btn-link btn-sm" id="deselect-all">移除全部</button>
                <?php
                    echo form_dropdown('category_id[]', $choices['category'], set_value('category_id', $form['category_id']), 'style="height: 200px;" multiple');
                ?>
                <?=form_error('category_id[]');?>
            </div>*/?>

            <div class="form-group required <?=form_error('price')?'has-error':'';?>">
                <label class="control-label">價格</label>
                <input class="form-control" name="price" value="<?=set_value('price', $form['price']);?>">
                <?=form_error('price');?>
            </div>
            <div class="form-group required <?php echo form_error('sale_price')?'has-error':'';?>">
                <label class="control-label">優惠價</label>
                <input class="form-control" name="sale_price" value="<?=set_value('sale_price', $form['sale_price']);?>">
                <?php echo form_error('sale_price');?>
            </div>
			<div class="form-group">
                <label class="control-label">數量</label>
                <input class="form-control" name="quantity" value="1">
            </div>

            <?php /*
			<div class="form-group <?=form_error('sale_start_date')?'has-error':'';?>">
                <label class="control-label">優惠價自動啟用時間</label>
                <input class="form-control" type="text" name="sale_start_date" value="<?=set_value('sale_start_date', $form['sale_start_date']);?>" onfocus="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm:ss',maxDate:'#F{$dp.$D(\'datemax\')}' })" id="datemin" class="input-text Wdate">
                <?=form_error('sale_start_date');?>
            </div>
            <div class="form-group <?=form_error('sale_end_date')?'has-error':'';?>">
                <label class="control-label">優惠價自動下架時間</label>
                <input class="form-control" type="text" name="sale_end_date" value="<?=set_value('sale_end_date', $form['sale_end_date']);?>" onfocus="WdatePicker({ dateFmt:'yyyy-MM-dd HH:mm:ss',minDate:'#F{$dp.$D(\'datemin\')}' })" id="datemax" class="input-text Wdate">
                <?=form_error('sale_end_date');?>
            </div>
			*/?>

            <div class="form-group">
                <label class="control-label">圖片</label>
                <div class="img-show">
                    <?php
                        if ($form['image']) {
                            echo img(array('src'=>$form['image_thumb_url'], 'class'=>'img-thumbnail'));
                            echo '<a href="javascript: void(0);" class="img-close btn btn-link  btn-xs" onclick="imgClose(this)"><i class="fa fa-times-circle"></i></a>';
                        }
                    ?>
                </div>
                <input type="file" name="upload" onchange="imgUpload(this);">
                <input type="hidden" name="image" value="<?=$form['image'];?>">
                <p class="help-block">檔案大小限制 <?=$_SETTING['upload_size_limit'];?>MB，建議圖片尺寸 1000 * 1000px。</p>
            </div>

            <div class="form-group <?=form_error("description")?'has-error':'';?>">
                <label class="control-label">商品介紹</label>
                <textarea class="ckeditor" name="description"><?=set_value("description", $form['description']);?></textarea>
                <?=form_error("description");?>
            </div>

            <div class="form-group required <?=form_error('sort_order')?'has-error':'';?>">
                <label class="control-label">排序</label>
                <input class="form-control" name="sort_order" value="<?=set_value('sort_order', $form['sort_order']);?>">
                <?=form_error('sort_order');?>
            </div>
            <div class="form-group required <?=form_error('recommend')?'has-error':'';?>" style="display: none;">
                <label class="control-label">推薦商品</label>
                <div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" name="recommend" <?=set_radio('enable', '1', $form['recommend']==1);?>>
                            <span style="color: green;">是　</span>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" name="recommend" <?=set_radio('enable', '0', $form['recommend']==0);?>>
                            <span style="color: red;">否　</span>
                        </label>
                    </div>
                    <?=form_error("enable");?>
                </div>
            </div>
            <div class="form-group required <?=form_error('enable')?'has-error':'';?>">
                <label class="control-label">是否啟用</label>
                <div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="1" name="enable" <?=set_radio('enable', '1', $form['enable']==1);?>>
                            <span style="color: green;">是　</span>
                        </label>
                    </div>
                    <div class="radio-inline">
                        <label>
                            <input type="radio" value="0" name="enable" <?=set_radio('enable', '0', $form['enable']==0);?>>
                            <span style="color: red;">否　</span>
                        </label>
                    </div>
                    <?=form_error("enable");?>
                </div>
            </div>
        </div>
		
<?php if($page_name=='edit'){ /*
?>
        <div class="tab-pane" id="subimage">
            <table class="table table-hover" style="margin-bottom: 10px;">
                <thead>
                    <tr>
                        <th>圖片</th>
                        <th>名稱</th>
                        <th width="60">排序</th>
                        <th width="50"></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if ($form['subimage']) { ?>
                    <?php foreach ($form['subimage'] as $num => $row) { ?>
                    <tr rowid="<?=$row['id'];?>">
                        <input type="hidden" name="subimage[<?=$num;?>][id]" value="<?=$row['id'];?>">
                        <td>
                            <?php 
                            //<div class="image-block">
                            //    <div onclick="selectImage(this)">
                            //        <?php if (isset($row['image_thumb_src']) && strlen($row['image_thumb_src']) > 0) { ?><?=img(array('src'=>$row['image_thumb_src'], 'class'=>'img-rounded'));?><?php } else { ?><i class="fa fa-plus fa-2x"></i><?php } ?>
                            //    </div><input type="hidden" name="subimage[<?=$num;?>][image]" value="<?=$row['image'];?>"><input type="file" name="subimage[<?=$num;?>]" style="display: none;" accept="image/jpeg" onchange="changeImage(this);"></div>
                            ?>

                            <div class="img-show">
                                <?php
                                    if ($row['image']) {
										echo img(array('src'=>$row['image_thumb_url'], 'class'=>'img-thumbnail'));
                                        echo '<a href="javascript: void(0);" class="img-close btn btn-link  btn-xs" onclick="imgClose(this)"><i class="fa fa-times-circle"></i></a>';
                                    }
                                ?>
                            </div>
                            <input type="file" name="subimage[<?=$num;?>]" onchange="imgUpload(this);">
                            <input type="hidden" name="subimage[<?=$num;?>][image]" value="<?=$row['image'];?>">
                        </td>
                        <td><input type="text" class="form-control" name='subimage[<?=$num;?>][name]' value="<?=set_value("subimage[{$num}][name]", $form['subimage'][$num]['name']);?>"></td>
                        <td><input type="text" class="form-control" name='subimage[<?=$num;?>][sort_order]' value="<?=set_value("subimage[{$num}][sort_order]", $form['subimage'][$num]['sort_order']);?>"></td>
                        <td align="right"><button type="button" class="btn btn-danger btn-sm" onclick="removeItem(this)"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php } ?>
                    <?php } ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <p class="help-block">檔案大小限制 <?=$_SETTING['upload_size_limit'];?>MB，建議圖片尺寸 1000 * 1000px。</p>
                        </td>
                        <td align="right"><button type="button" class="btn btn-success btn-sm" onclick="addSubImage()"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                </tfoot>
            </table>
        </div>
<?php */} ?>

    </div>
    <div class="text-center">
        <button class="btn btn-primary"><i class="fa fa-save"></i> 確認送出</button>
        <a class="btn btn-default" href="<?=$link_cancel;?>"><i class="fa fa-reply"></i> 返回</a>
    </div>
</form>
<?php } ?>
