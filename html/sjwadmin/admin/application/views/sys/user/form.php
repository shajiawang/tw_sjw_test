<?php if (validation_errors()) { ?>
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <?=validation_errors();?>
</div>
<?php } ?>
<form id="data-form" role="form" method="post" action="<?=$link_save;?>">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
	<input type="hidden" name="user_group_id" value="<?=$form['user_group_id']?>" />
	
    <?php /*<div class="form-group required">
        <label class="control-label">用戶群組</label>
        <?php
			if ($page_name == 'profile') {
                echo form_dropdown('user_group_id', $choices['group'], set_value('user_group_id', $form['user_group_id']), 'class="form-control" disabled');
            } else {
                echo form_dropdown('user_group_id', $choices['group'], set_value('user_group_id', $form['user_group_id']), 'class="form-control"');
            }
        ?>
        <?=form_error('user_group_id');?>
    </div>*/?>

    <div class="form-group required <?=form_error('name')?'has-error':'';?>">
        <label class="control-label">用戶名稱</label>
        <input class="form-control" name="name" placeholder="" value="<?=set_value('name', $form['name']);?>">
        <?=form_error('name');?>
    </div>
	
    <div class="form-group required <?=form_error('username')?'has-error':'';?>">
        <label class="control-label">用戶帳號</label>
        <?php if ($page_name == 'add') { ?>
        <input class="form-control" name="username" placeholder="" value="<?=set_value('username', $form['username']);?>">
        <?php } else { ?>
        <input class="form-control" name="username" placeholder="" value="<?=$form['username'];?>" disabled>
        <?php }?>
        <?=form_error('username');?>
    </div>
	
	<div class="form-group <?=form_error('password')?'has-error':'';?>">
        <label class="control-label">密碼</label>
        <input class="form-control" type="password" name="password" placeholder="" value="<?=set_value('password');?>">
        <?=form_error('password');?>
    </div>
    
	<div class="form-group <?=form_error('passconf')?'has-error':'';?>">
        <label class="control-label">確認密碼</label>
        <input class="form-control" type="password" name="passconf" placeholder="" value="<?=set_value('passconf');?>">
        <?=form_error('passconf');?>
    </div>
	
    <div class="form-group <?=form_error('email')?'has-error':'';?>">
        <label class="control-label">E-Mail</label>
        <input class="form-control" name="email" placeholder="email@example.com" value="<?=set_value('email', $form['email']);?>">
        <?=form_error('email');?>
    </div>
	
    <div class="form-group <?=form_error('telephone')?'has-error':'';?>">
        <label class="control-label">聯絡電話</label>
        <input class="form-control" name="telephone" placeholder="0987654321" value="<?=set_value('telephone', $form['telephone']);?>">
        <?=form_error('telephone');?>
    </div>
	
	<?php /*if(!empty($sales_disabled)){ ?>
	<input type="hidden" name="sales_id" value="<?=$sales_id;?>" />
	<?php } ?>
	<?php if($page_name !== 'profile'){ ?>
	<div class="form-group ">
        <label class="control-label">開發人員編</label>
        <?php echo form_dropdown('sales_id', $sales_options, $sales_id, 'class="form-control" '. $sales_disabled);?>
    </div>
	<?php }*/ 
	
	if($this->flags->user['is_admin']=='1'){
	?>
    <div class="form-group required <?=form_error('')?'has-error':'';?>">
        <label class="control-label">是否啟用</label>
        <div>
            <?php
                $enable_1 = $form['enable']=='1'? TRUE : FALSE;
                $enable_2 = $form['enable']=='0'? TRUE : FALSE;
            ?>
            <div class="radio-inline">
                <label>
                    <input id="enable_1" type="radio" value="1" name="enable" <?=set_radio('enable', '1', $enable_1);?>>
                    <span style="color: green;">是　</span>
                </label>
            </div>
            <div class="radio-inline">
                <label>
                    <input id="enable_0" type="radio" value="0" name="enable" <?=set_radio('enable', '0', $enable_2);?>>
                    <span style="color: red;">否　</span>
                </label>
            </div>
            <?=form_error("enable");?>
        </div>
    </div>
	<?php } else { ?>
		<input type="hidden" name="enable" value="<?=$form['enable']?>" />
	<?php } ?>

</form>
