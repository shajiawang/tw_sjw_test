<?php if (validation_errors()) { ?>
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <?php echo validation_errors();?>
</div>
<?php } ?>

<form id="data-form" role="form" method="post" action="<?php echo $link_save;?>"  enctype="multipart/form-data">
    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />

    <blockquote><?php echo $form['name'];?></blockquote>
    <div class="form-group <?php echo form_error("content")?'has-error':'';?>">
        <textarea class="form-control" id="content" name="content"><?=set_value('content', $form['content']);?></textarea>
        <?php echo form_error("content"); ?>
    </div>
</form>

<script src="<?=$static_plugin;?>ckeditor/ckeditor.js"></script>
<script>
$(function() {
    $('#tab_locale a:first').tab('show');

    CKEDITOR.replace('content', {
        language: 'zh',
        uiColor: '#AADBCB',
    });
});
</script>
