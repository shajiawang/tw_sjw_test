<style>
    .item {
        padding: 3px 6px;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel&#45;heading">
                <i class="fa fa-list fa-lg"></i> <?=$_LOCATION['name'];?>
            </div>
            <!-- /.panel-heading -->

            <div class="panel-body">
                <form id="filter-form" role="form" class="form-inline">
                    <input type="hidden" name="sort" value="" />
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label">展覽</label>
                                <select id="exhibits" class="form-control")>
                                    <option value="">請先選擇展覽</option>
                                    <?php foreach ($choices['exhibits'] as $k => $v) { ?>
                                    <option value="<?=$k;?>"><?=$v;?></option>
                                    <?php } ?>
                                </select>
                                <?php
                                    // $choices['exhibits'] = array('all'=>'請先選擇展覽') + $choices['exhibits'];
                                    // echo form_dropdown('aid', $choices['exhibits'], NULL, 'class="form-control"');
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">商品</label>
                                <select id="products" class="form-control")>
                                    <option value=""> -- None -- </option>
                                </select>
                                <?php
                                    // $choices['product'] = array(''=>'') + $choices['product'];
                                    // echo form_dropdown('product_id', $choices['product'], $filter['product_id'], 'class="form-control"');
                                ?>
                            </div>
                            <button type="button" class="btn btn-primary btn-sm" id="add-product-btn"><i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-xs-12" id="product-show">
                            <?php foreach ($filter['product_id'] as $pid) { ?>
                            <div class="item">
                                <input type="hidden" name="product_id[]" value="<?=$pid;?>">
                                <?=$choices['product'][$pid];?> <button type="button" class="btn btn-xs btn-link"><i class="fa fa-trash-o text-danger"></i></button>
                            </div>
                            <?php } ?>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label">訂單狀態</label>
                                <?php
                                    $choices['order_status'] = array('all'=>'全部') + $choices['order_status'];
                                    echo form_dropdown('order_status_id', $choices['order_status'], $filter['order_status_id'], 'class="form-control"');
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">付款方式</label>
                                <?php
                                    $choices['payment_method'] = array('all'=>'全部') + $choices['payment_method'];
                                    echo form_dropdown('payment_method_id', $choices['payment_method'], $filter['payment_method_id'], 'class="form-control"');
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">領取狀態</label>
                                <?php
                                    $choices['checkin_status'] = array(
                                        'all' => '全部',
                                        '0' => '否',
                                        '1' => '是',
                                    );
                                    echo form_dropdown('checkin', $choices['checkin_status'], $filter['checkin'], 'class="form-control"');
                                ?>
                            </div>
                        </div>

                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label">日期</label>
                                <?php
                                    $choices_date_type = array(1=>'訂單成立時間', 2=>'票卷領取時間');
                                    echo form_dropdown('date_type', $choices_date_type, $filter['date_type'], 'class="form-control"');
                                ?>
                            </div>
                            <div class="form-group">
                                <label class="control-label">期間</label>
                                <div class="input-daterange input-group" id="datepicker" style="width: 240px;">
                                    <input type="text" class="form-control" name="start_date" value="<?=$filter['start_date'];?>"/>
                                    <span class="input-group-addon">to</span>
                                    <input type="text" class="form-control" name="end_date"  value="<?=$filter['end_date'];?>"/>
                                </div>
                            </div>

                            <button class="btn btn-info btn-sm" title="搜尋"><i class="fa fa-search"></i></button>
                            <a class="btn btn-default btn-sm" href="?" title="清除條件"><i class="fa fa-refresh"></i></a>
                        </div>

                        <div class="col-xs-6" >
                            <div class="form-group">
                                <label class="control-label">顯示筆數</label>
                                <?php
                                    echo form_dropdown('rows', $choices['rows'], $filter['rows'], 'class="form-control"');
                                ?>
                            </div>
                        </div>
                        <div class="col-xs-6 text-right">
                            <div class="form-group">
                                <label class="control-label"><button class="btn btn-link" style="padding: 3px;"><i class="fa fa-search"></i></button></label>
                                <input type="text" class="form-control" name="q" value="<?=$filter['q'];?>">
                            </div>
                        </div>
                    </div>
                </form>

                <form id="list-form" method="post">
                    <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                            <tr>
                                <th class="sorting<?=($filter['sort']=='order_no asc')?'_asc':'';?><?=($filter['sort']=='order_no desc')?'_desc':'';?>" data-field="order_no" >訂單編號</th>
                                <th class="sorting<?=($filter['sort']=='total asc')?'_asc':'';?><?=($filter['sort']=='total desc')?'_desc':'';?>" data-field="total" >總額</th>
                                <th class="sorting<?=($filter['sort']=='order_status asc')?'_asc':'';?><?=($filter['sort']=='order_status desc')?'_desc':'';?>" data-field="order_status" >訂單狀態</th>
                                <th class="sorting<?=($filter['sort']=='create_at asc')?'_asc':'';?><?=($filter['sort']=='create_at desc')?'_desc':'';?>" data-field="create_at" >新增時間</th>
                                <th class="sorting<?=($filter['sort']=='buyer_name asc')?'_asc':'';?><?=($filter['sort']=='buyer_name desc')?'_desc':'';?>" data-field="buyer_name" >付款人資訊</th>
                                <th class="">商品資訊</th>
                                <th class="sorting<?=($filter['sort']=='income_id asc')?'_asc':'';?><?=($filter['sort']=='income_id desc')?'_desc':'';?>" data-field="income_id" >流量碼</th>
                                <th>領取狀態</th>
                                <?php if ($this->router->class=='order' || isset($row['link_edit']) || isset($row['link_view']) || isset($row['link_delete'])) { ?>
                                <th></th>
                                <?php } ?>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($list as $row) { ?>
                            <tr class="<?=($row['order_status_id']==4)?'text-danger':'';?>">
                                <td>
                                    <?=$row['order_no'];?>
<?php if ($row['invoice_no'] != '') { ?>
    發票開立：<?=$row['invoice_no'];?>
<?php } ?>
                                </td>
                                <td>
                                    <b>$<?=number_format($row['total']);?></b><br>
                                    <span style="font-size: 80%;"><?=$choices['payment_method'][$row['payment_method_id']];?></span>
                                </td>
                                <td><?=$choices['order_status'][$row['order_status_id']];?></td>
                                <td><?=$row['create_at'];?></td>
                                <td>
                                    姓名：<?=$row['buyer_name'];?> <?=($row['buyer_gender']==1)?'男':'女';?><br>
                                    E-mail：<?=mailto($row['buyer_email']);?><br>
                                    電話：<?=$row['buyer_phone'];?><br>
                                    生日：<?=$row['buyer_birthday'];?>
                                </td>
                                <td>
                                <?php
                                    foreach ($row['items'] as $item) {
                                        echo "{$item['name']} <b>\${$item['price']}</b><br>數量：<b class=\"text-danger\">{$item['quantity']}</b>";
                                    }
                                ?>
                                </td>
                                <td><?=$row['income_id'];?></td>
                                <td>
                                <?php
                                    if ($row['order_status_id'] == 2) {
                                        if ($row['checkin'] == 1) {
                                            echo '領取於：'.$row['date_checkin'];
                                            echo '<br><button type="button" class="btn btn-danger btn-xs btn-outline btn-toggle" onclick="setCheckin(this, '.$row['id'].', 0)">取消領取</button>';
                                            // echo '<input type="checkbox" value="1" checked onchange="setCheckin('.$row['id'].', 0)">';
                                        } else {
                                            echo '<button type="button" class="btn btn-success btn-xs btn-outline btn-toggle" onclick="setCheckin(this, '.$row['id'].', 1)">立即領取</button>';
                                            // echo '<input type="checkbox" value="1"  onchange="setCheckin('.$row['id'].', 1)">';
                                        }
                                    }
                                ?>
                                </td>

                                <?php if (isset($row['link_edit']) || isset($row['link_view']) || isset($row['link_delete'])) { ?>
                                <td class="text-center" id="btn_group">
                                    <?php if (isset($row['link_view'])) { ?>
                                    <a type="button" class="btn btn-outline btn-success btn-xs btn-toggle" title="View" href="<?=$row['link_view'];?>">
                                        <i class="fa fa-eye fa-lg"></i>
                                    </a>
                                    <?php } ?>
                                    <?php if (isset($row['link_edit'])) { ?>
                                    <a type="button" class="btn btn-outline btn-warning btn-xs btn-toggle" href="<?=$row['link_edit'];?>">
                                        <i class="fa fa-pencil fa-lg"></i>
                                    </a>
                                    <?php } ?>
                                    <?php if (isset($row['link_delete'])) { ?>
                                    <button type="button" class="btn btn-outline btn-danger btn-xs" onclick="ajaxDelete(this, '確認要刪除選單「<?=$row['name'];?>」?', '<?=$row['link_delete'];?>')">
                                        <i class="fa fa-trash fa-lg"></i>
                                    </button>
                                    <?php } ?>
                                </td>
                                <?php } ?>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </form>

                <div class="row">
                    <div class="col-lg-4">
                        Showing <?=$pagination['offset']+1;?>
                        to <?=(($pagination['offset']+$pagination['rows'])>$pagination['total'])?$pagination['total']:$pagination['offset']+$pagination['rows'];?>
                        of <?=$pagination['total'];?> entries
                    </div>
                    <div class="col-lg-8 text-right">
                        <?=$this->pagination->create_links();?>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->

<script src="<?=$static_plugin;?>lou-multi-select/js/jquery.multi-select.js" type="text/javascript"></script>
<script src="<?=$static_plugin;?>jquery.highlight-3.js"></script>
<script>
$(document).ready(function() {
    $('select[name="aid[]"]').multiSelect({
        selectableHeader: "<div class='custom-header'>所有展覽</div>",
        selectionHeader: "<div class='custom-header'>選定展覽</div>",
    });
    $('#select-all').click(function(){
        $('select[name="aid[]"]').multiSelect('select_all');
        return false;
    });
    $('#deselect-all').click(function(){
        $('select[name="aid[]"]').multiSelect('deselect_all');
        return false;
    });
    // $('#filter-form select').change(function(){
        // $('#filter-form').submit();
    // });

    <?php if (isset($filter['q']) && $filter['q'] != ''){ ?>
    $('#list-form').highlight('<?=$filter['q'];?>');
    <?php } ?>


    $('#exhibits').change(function(){
        var url = '<?=base_url("sales/order/ajax/getProductChoices");?>';
        var data = {
            '<?=$csrf["name"];?>': '<?=$csrf["hash"];?>',
            aid: $(this).val(),
        }
        $.post(url, data, function(response){
            var html = '<option value=""> -- None -- </option>';
            if (response.status == true) {
                html = '<option value="">請選擇商品</option>';
                for (var i in response.data) {
                    html += '<option value="'+ i +'">'+ response.data[i] +'</option>';
                }
            }
            $('#products').html(html);
        }, 'json');
    });

    $('#add-product-btn').click(function(){
        var product_id = parseInt($('#products option:selected').val());
        var product_name = $('#products option:selected').html();
        var exist = false;
        $('#product-show .item [name="product_id[]"]').each(function(){
            if (product_id == $(this).val()) {
                my_alert(3, '商品資料「'+ product_name +'」巳存在', 4, 'center');
                exist = true;
            }
        });

        if (exist == false) {
            $('#product-show').append('<div class="item">'+
                '<input type="hidden" name="product_id[]" value="'+ product_id +'">'+
                product_name +' <button type="button" class="btn btn-xs btn-link"><i class="fa fa-trash-o text-danger"></i></button>'+
                '</div>');
        }
    });
    $('.item button').click(function(){
        $(this).parent().remove();
    });

});

var setCheckin = function(obj, id, checkin) {
    var data = {
        '<?=$csrf["name"];?>': '<?=$csrf["hash"];?>',
        'order_id': id,
        'checkin': checkin,
    }
    if (checkin == 1) {
        $.post("<?=base_url('sales/order/ajax/checkin');?>", data, function(response){
            if (response.status) {
                if (response.data.checkin == 1) {
                    $(obj).parent('td').html('領取於：'+ response.data.date_checkin +
                        '<br><button type="button" class="btn btn-danger btn-xs btn-outline btn-toggle" onclick="setCheckin(this, '+ id +', 0)">取消領取</button>');
                    my_alert(2, '領取狀態巳修改為<h4>巳領取</h4>', 4, 'center');
                } else {
                    $(obj).parent('td').html('<button type="button" class="btn btn-outline btn-success btn-xs  btn-toggle" onclick="setCheckin(this, '+ id +', 1)">立即領取</button>');
                    my_alert(3, '領取狀態巳修改為<h4>未領取</h4>', 4, 'center');
                }
            } else {
                my_alert(4, '傳送錯誤', 4, 'topCenter');
            }

        }, 'json');
    } else {
        my_alert(4, '工讀生無法取消領取狀態，如果需要取消領取請找相關人員', 4, 'center');
    }
}
</script>
