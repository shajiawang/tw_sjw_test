<style>
.zipcode {
    width: 80px !important;
}
.dd .fnone {
    width: 60px;
    height: 60px;
}
</style>

<?php if (validation_errors()) { ?>
<div class="alert alert-danger">
    <button class="close" data-dismiss="alert" type="button">×</button>
    <?=validation_errors();?>
</div>
<?php } //var_dump($form['history']); ?>

<div>
    <!-- Nav tabs -->
    <ul class="nav nav-tabs" role="tablist" id="tab_data">
        <li class="active"><a href="#general" role="tab" data-toggle="tab">一般</a></li>
        <li><a href="#history" role="tab" data-toggle="tab">訂單狀態</a></li>
        <li><a href="#product" role="tab" data-toggle="tab">產品項目</a></li>
        <li><a href="#payment" role="tab" data-toggle="tab">付款資訊</a></li>
    </ul>

    <!-- Tab panes -->
    <div class="tab-content" style="padding: 15px;">
        <div class="tab-pane active" id="general">
            <table class="table table-striped table-bordered table-condensed">
                <caption>訂單資訊</caption>
                <tbody>
                    <tr>
                        <td width="150">訂單編號</td>
                        <td><?=$form['order_no'];?></td>
                    </tr>
					<tr>
                        <td>總金額</td>
                        <td id="total_td">
                            <b class="text-danger">$<?=number_format($form['total'], 2);?></b>
                        </td>
                    </tr>
                    <tr>
                        <td>付款方式</td>
                        <td id="payment_method_td">
                            <?=$choices['payment_method'][$form['payment_method_id']];?>
                        </td>
                    </tr>
                    <tr>
                        <td>配送方式</td>
                        <td id="payment_method_td">
                            <?=$choices['shipping_method'][$form['shipping_method']];?>
                        </td>
                    </tr>
                    <?php /*
					<tr>
                        <td>訂單狀態</td>
                        <td id="order_status_td">
                        <?php
                            if ($form['order_status'] == 4) {
                                echo "<span class=\"text-danger\">{$choices['order_status'][$form['order_status_id']]}</span>";
                            } else {
                                echo $choices['order_status'][$form['order_status_id']];
                            }
                        ?>
                        </td>
                    </tr>
					<tr>
                        <td>領取狀態</td>
                        <td>
                            <?php
                                if ($form['checkin'] == 1) {
                                    echo '<input type="checkbox" checked onchange="setCheckin('.$form['id'].', 0)">';
                                } else {
                                    echo '<input type="checkbox" onchange="setCheckin('.$form['id'].', 1)">';
                                }
                            ?>
                        </td>
                    </tr>*/?>
                    <tr>
                        <td>備註</td>
                        <td id="remark_td"><?=$form['remark'];?></td>
                    </tr>
                    <tr>
                        <td>IP Address</td>
                        <td><?=$form['history'][0]['ip'];?></td>
                    </tr>
                    <tr>
                        <td>User Agent</td>
                        <td><?=$form['history'][0]['user_agent'];?></td>
                    </tr>
                    <tr>
                        <td>建立日期</td>
                        <td><?=$form['create_at'];?></td>
                    </tr>
                    <tr>
                        <td>最後修改</td>
                        <td id="dete_modified_td"><?=$form['date_modified'];?></td>
                    </tr>
                    <tr>
                        <td>付款時間</td>
                        <td><?=$form['payment_time'];?></td>
                    </tr>
                </tbody>
            </table>

            <?php /*
			<table class="table table-striped table-bordered table-condensed">
                <caption>金流資訊(智付通 <?=anchor_popup('https://www.spgateway.com/');?>)</caption>
                <tbody>
                    <tr>
                        <td width="150">金流回傳</td>
                        <td><div style="word-break: break-all; word-wrap:break-word;"><?=$form['payment_response'];?></div></td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-striped table-bordered table-condensed">
            <caption>發票資訊(智付寶 <?=anchor_popup('https://www.pay2go.com/');?>)</caption>
                <tbody>
                    <tr>
                        <td width="150">發票編號</td>
                        <td><?=$form['invoice_no'];?></td>
                    </tr>
                    <tr>
                        <td width="150">隨機碼</td>
                        <td><?=($form['invoice_random_no'])?"{$form['invoice_random_no']}(若查詢電子發票會用到)":'';?></td>
                    </tr>
                    <tr>
                        <td width="150">發票回傳</td>
                        <td><div style="word-break: break-all; word-wrap:break-word;"><?=$form['invoice_response'];?></div></td>
                    </tr>
                </tbody>
            </table>
			*/?>
        </div>

        <div class="tab-pane" id="history">
            <blockquote class="text-warning">
                若狀態修改為作廢，還需要到金流商的後台去做退刷流程。
            </blockquote>
            <form id="form-history" role="form" method="post">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <input type="hidden" name="order_id" value="<?=$form['id'];?>" />
                <div class="form-group required">
                    <label class="control-label">狀態</label>
                    <?php
                        echo form_dropdown('order_status_id', $choices['order_status'], $form['order_status_id'], 'class="form-control"');
                    ?>
                </div>
                <div class="form-group required">
                    <label class="control-label">備註</label>
                    <textarea class="form-control" name="comment"></textarea>
                </div>
                <div class="form-group text-right">
                    <button class="btn btn-primary"><i class="fa fa-plus-circle"></i> 新增記錄</button>
                </div>
                <hr>

                <table class="table table-striped table-condensed">
                    <thead>
                        <tr>
                            <th width="20%">建立時間</th>
                            <th width="15%">會員名稱</th>
                            <th width="15%">訂單狀態</th>
                            <th>狀態備註</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($form['history'] as $row) { ?>
                    <tr>
                        <td><?=$row['create_at'];?></td>
                        <td><?=$row['user_name'];?></td>
                        <td><?=$choices['order_status'][$form['order_status_id']];?></td>
                        <td><?=$row['comment'];?></td>
                    </tr>
                    <?php } ?>
                    </tbody>
                </table>
            </form>
        </div>

        <div class="tab-pane" id="product">
            <?php /*
            <blockquote class="text-warning">
                商品項目如果有做修改，請確認金額是否一樣，若不一樣還需修改訂單金額。
            </blockquote>
            */?>
            <form id="form-product" role="form" method="post">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <input type="hidden" name="order_id" value="<?=$form['id'];?>" />
                <table class="table table-striped table-condensed">
                    <thead>
                        <tr>
                            <th width="110">商品圖片</th>
                            <th>商品名稱</th>
                            <th width="60">數量</th>
                            <th width="100">價格</th>
                            <th width="100">小計</th>
                            <?php /*<th width="40"></th>*/?>
                        </tr>
                    </thead>
                    <tbody>
                    <?php
                        $sum = 0;
                        foreach ($form['items'] as $num => $row) {
                    ?>
                        <tr rowid="<?=$row['id'];?>">
                            <td><img src="<?=$row['image_thumb_src'];?>" style="height: 100px;"></td>
                            <td>
                                <a href="<?=base_url('catalog/product/edit/'.$row['product_id']);?>" target="_blank">
                                    <?=$row['name'];?>
                                </a><br>
                            </td>
                            <td><?=$row['quantity'];?></td>
                            <td><?=number_format($row['price']);?></td>
                            <td><b><?=number_format($row['subtotal']);?></b></td>
                            <?php /*
                            <td align="center">
                                <button type="button" class="btn btn-outline btn-danger btn-xs" onclick="removeProduct(this, '<?=$row['id'];?>')">
                                    <i class="fa fa-trash fa-lg"></i>
                                </button>
                            </td>
                            */?>
                        </tr>
                    <?php
                            $sum += $row['subtotal'];
                        }
                    ?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4" class="text-right">Total: </th>
                            <th id="form-product-total"><b><?=number_format($sum);?></b></th>
                        </tr>
                    </tfoot>
                </table>

                <?php /*
                <hr>
                <div class="form-group required">
                    <label class="control-label">產品名稱</label>
                    <div>
                        <select name="item_id" class="form-control">
                        <?php foreach ($products as $row) { ?>
                            <option value="<?=$row['id'];?>" data-image="<?=$row['image_thumb_src'];?>">
                            <?="{$row['name']}&emsp;<span>\${$row['price']}</span>";?>
                            </option>
                        <?php } ?>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label">產品價格</label>
                    <input class="form-control" name="price">
                </div>
                <div class="form-group required">
                    <label class="control-label">數量 </label>
                    <input class="form-control" name="quantity" value="1">
                </div>
                <div class="form-group text-right">
                    <button id="btn_product" class="btn btn-primary"><i class="fa fa-plus-circle"></i> 新增產品</button>
                </div>
                */?>
            </form>
        </div>

        <div class="tab-pane" id="payment">
            <?php /*
			<blockquote class="text-warning">
                訂單金額如果做了調整，還需要到金流商後台去修改。
            </blockquote>*/?>
            <form id="form-payment" role="form" method="post">
                <input type="hidden" name="<?=$csrf['name'];?>" value="<?=$csrf['hash'];?>" />
                <input type="hidden" name="order_id" value="<?=$form['id'];?>" />
                <div class="form-group required">
                    <label class="control-label">付款方式</label>
                    <div>
                    <?php
                        echo form_dropdown('payment_method_id', $choices['payment_method'], $form['payment_method_id'], 'class="form-control"');
                    ?>
                    </div>
                </div>
                <div class="form-group required">
                    <label class="control-label">運費</label>
                    <input class="form-control" name="shipping_fee" value="<?=$form['shipping_fee'];?>">
                </div>
				<div class="form-group required">
                    <label class="control-label">總金額</label>
                    <input class="form-control" name="total" value="<?=$form['total'];?>">
                </div>

                <div class="form-group required">
                    <label class="control-label">付款人姓名</label>
                    <input class="form-control" name="buyer_name" value="<?=$form['buyer_name'];?>">
                </div>
                <div class="form-group required">
                    <label class="control-label">付款人E-Mail</label>
                    <input class="form-control" name="buyer_email" value="<?=$form['buyer_email'];?>">
                </div>
                <div class="form-group required">
                    <label class="control-label">付款人電話</label>
                    <input class="form-control" name="buyer_phone" value="<?=$form['buyer_phone'];?>">
                </div>
                <?php /*
				<div class="form-group required <?=form_error('buyer_gender')?'has-error':'';?>">
                    <label class="control-label">性別</label>
                    <div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" value="1" name="buyer_gender" <?=set_radio('buyer_gender', '1', $form['buyer_gender']==1);?>>
                                男
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" value="2" name="buyer_gender" <?=set_radio('buyer_gender', '2', $form['buyer_gender']==2);?>>
                                女
                            </label>
                        </div>
                    </div>
                </div>
                <div id="datepicker" class="form-group required <?=form_error('buyer_birthday')?'has-error':'';?>">
                    <label class="control-label">生日</label>
                    <div class="input-group date">
                        <input type="text" class="form-control" name="buyer_birthday" placeholder="YYYY-MM-DD" value="<?=set_value('buyer_birthday', $form['buyer_birthday']); ?>">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                    </div>
                </div>
				
                <div class="form-group">
                    <label class="control-label">備註</label>
                    <textarea class="form-control" name="remark"><?=$form['remark'];?></textarea>
                </div>
                <div class="form-group text-right">
                    <button id="btn_product" class="btn btn-success"><i class="fa fa-pencil"></i> 修改付款資訊</button>
                </div>
				*/?>
            </form>
        </div>

    </div>
</div>

<script src="<?=$static_plugin;?>ckeditor/ckeditor.js"></script>
<script src="<?=$static_plugin;?>jquery-validation/jquery.validate.min.js"></script>
<script src="<?=$static_plugin;?>jquery-validation/localization/messages_zh_TW.min.js"></script>
<script src="<?=$static_plugin;?>jquery.form/jquery.form.js"></script>
<script type="text/javascript">
var choice_order_status = CI.choices.order_status;
var choice_payment_method = CI.choices.payment_method;
var choice_shipping_method = CI.choices.shipping_method;

$(function() {
    $('#tab_data a:first').tab('show');
    $('#tab_locale a:first').tab('show');

    $('#tab_data a:eq(2)').click(function(){
        var $product_id = $('#product select[name=item_id]');
        if (!$product_id.hasClass('msDropDown')) {
            setTimeout(function(){
                $product_id.addClass('msDropDown');
                $product_id.msDropDown();
            }, 200);
        }
    });

    // 訂單狀態
    var $form_history = $('#form-history');
    $form_history.validate({
        // debug: true,
        errorElement: 'span',
        errorClass: 'text-danger',
        rules: {
            order_status: {required: true},
            comment: {required: true},
        },
        highlight: function(element, errorClass, validClass){
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass){
            $(element).addClass(validClass).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            element.closest('.form-group').addClass('has-error');
            error.insertAfter(element);
        },
        submitHandler: function() {
            $.ajax({
                url: '<?=base_url("sales/order/ajax/history");?>',
                data: $form_history.serialize(),
                type: "POST",
                dataType: 'json',
                success: function(response){
                    if (response.status) {
                        $form_history.find('table tbody').prepend(
                            '<tr>'+
                            '   <td>'+ response.data.create_at +'</td>'+
                            '   <td>'+ response.data.user_name +'</td>'+
                            '   <td>'+ choice_order_status[response.data.order_status_id] +'</td>'+
                            '   <td>'+ response.data.comment +'</td>'+
                            '</tr>'
                        );

                        var order_status = choice_order_status[response.data.order_status_id];
                        var date_modified = response.data.create_at;
                        if (response.data['order_status'] == 4) {
                            order_status = '<span class="text-danger">'+ order_status +'</span>';
                        }
                        $('#order_status_td').html(order_status);
                        $('#date_modified_td').html(date_modified);

                        $form_history.find('[name=comment]').val('');
                        my_alert(1, '訂單狀態新增成功', 4, 'topCenter');
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){
                    my_alert(4, xhr.status +' '+ thrownError, 4, 'topCenter');
                }
            });

            return false;
        }
    });
	
});
<?php /*
$(function() {
	// 新增產品
    var $form_product = $('#form-product');
    $form_product.validate({
        debug: true,
        errorElement: 'span',
        errorClass: 'text-danger',
        rules: {
            price: {required: true, number: true, min: [0]},
            quantity: {required: true, digits: true, min: [1]},
        },
        highlight: function(element, errorClass, validClass){
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass){
            $(element).addClass(validClass).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function() {
            $.ajax({
                url: '<?=base_url("sales/order/ajax/product");?>',
                data: $form_product.serialize(),
                type: "POST",
                dataType: 'json',
                success: function(response){
                    if (response.status) {
                        var html = ''
                        html += '<tr rowid="'+ response.data['id'] +'">';
                        html += '   <td><img src="'+ response.data['image_thumb_src'] +'"></td>';
                        html += '   <td>';
                        html += '       <a href="'+ response.data['link'] +'">'+ response.data['name'] +'</a>';
                        html += '   </td>';
                        html += '   <td>'+ response.data['quantity'] +'</td>';
                        html += '   <td>$'+ response.data['price'] +'</td>';
                        html += '   <td><b>$'+ response.data['subtotal'] +'</b></td>';
                        html += '   <td><button type="button" class="btn btn-outline btn-danger btn-xs" onclick="removeProduct(this, \''+ response.data['id'] +'\')"><i class="fa fa-trash fa-lg"></i></button></td>';
                        html += '</tr>';

                        $form_product.find('table tbody').append(html);
                        $form_product.find('[name=price]').val('');
                        $form_product.find('[name=quantity]').val(1);

                        $form_product.find('#form-product-total').html('$'+response.data.product_total);
                        $form_product.find('#form-commission-total').html('$'+response.data.commission_total);

                        var date_modified = response.data.date_modified;
                        $('#date_modified_td').html(date_modified);

                        my_alert(1, '產品項目已新增', 4, 'topCenter');
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){
                    my_alert(4, xhr.status +' '+ thrownError, 4, 'topCenter');
                }
            });

            return false;
        }
    });
	
	// 修改付款資訊
    var $form_payment = $('#form-payment');
    $form_payment.validate({
        debug: true,
        errorElement: 'span',
        errorClass: 'text-danger',
        rules: {
            payment_method_id: {required: true},
            total: {required: true, number: true, min: [0]},
            buyer_name: {required: true},
            buyer_email: {required: true, email: true},
            buyer_phone: {required: true},
            buyer_gender: {required: true},
            buyer_birthday: {required: true, dateISO: true},
        },
        highlight: function(element, errorClass, validClass){
            $(element).addClass(errorClass).removeClass(validClass);
            $(element).closest('.form-group').addClass('has-error');
        },
        unhighlight: function(element, errorClass, validClass){
            $(element).addClass(validClass).removeClass(errorClass);
            $(element).closest('.form-group').removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element);
        },
        submitHandler: function() {
            $.ajax({
                url: '<?=base_url("sales/order/ajax/payment");?>',
                data: $form_payment.serialize(),
                type: "POST",
                dataType: 'json',
                success: function(response){
                    if (response.status) {
                        var date_modified = response.data.date_modified;
                        $('#date_modified_td').html(date_modified);

                        var payment_method = choice_payment_method[response.data.payment_method_id];
                        $('#payment_method_td').html(payment_method);

                        var total = response.data.total;
                        $('#total_td').html('<b class="text-danger">$'+ total +'</b>');

                        var remark = response.data.remark;
                        $('#remark_td').html(remark);

                        my_alert(2, '付款資訊已更新', 4,'topCenter');
                    }
                },
                error:function(xhr, ajaxOptions, thrownError){
                    my_alert(4, xhr.status +' '+ thrownError, 4, 'topCenter');
                }
            });

            return false;
        }
    });

});
*/?>

<?php 
/*
var getProductPrice = function() {
    var $form = $('#form-product');
    var $product_id = $('#product select[name=product_id]');
    var url = '<?=base_url('sales/order/ajax/?action=product_price');?>';
    var data = {
        '<?=$csrf['name'];?>': '<?=$csrf['hash'];?>',
        'pid': $product_id.val(),
    };
    $.ajax({
        url: url,
        data: data,
        type: "POST",
        dataType: 'json',
        success: function(response){
            if (response.status) {
                $form.find('[name=price]').val(response.data.price);
            }
        },
        error:function(xhr, ajaxOptions, thrownError){
            my_alert(4, xhr.status +' '+ thrownError, 4, 'topCenter');
        }
    });
}
var removeProduct = function(obj, order_item_id) {
    var $tr = $(obj).closest('tr');
    var product_image = $tr.find('td:eq(0)').html();
    var product_name = $tr.find('td:eq(1)').html();
    var msg  = '<p>確認移除</p><p>' + product_name +'<br>'+ product_image +'</p>';
    var url = '<?=base_url("sales/order/ajax/product_remove");?>';
    var data = {
        '<?=$csrf['name'];?>': '<?=$csrf['hash'];?>',
        'order_id': '<?=$form['id'];?>',
        'order_item_id': order_item_id,
    };
    var yesfunc = function() {
        $.ajax({
            url: url,
            data: data,
            type:"POST",
            dataType:'json',
            success: function(response){
                if (response.status) {
                    console.log(response);
                    $('#form-product-total').html('$'+ response.data['product_total']);
                    $tr.remove();
                    var msg  = '<p>巳移除</p><p>' + product_name +'<br>'+ product_image +'</p>';
                    my_alert(2, msg, 4, 'topCenter');
                }
            },
            error:function(xhr, ajaxOptions, thrownError){
                my_alert(4, xhr.status+' '+thrownError, 4, 'topCenter');
            }
        });

    }

    var nofunc = function() {
    }

    bk_confirm(3, msg, 'center', yesfunc, nofunc);
}
var setCheckin = function(id, checkin) {
    var data = {
        '<?=$csrf["name"];?>': '<?=$csrf["hash"];?>',
        'order_id': id,
        'checkin': checkin,
    }
    $.post("<?=base_url('sales/order/ajax/checkin');?>", data, function(response){
        my_alert(2, '領取狀態巳修改', 4, 'topCenter');

    }, 'json');
}
*/?>
</script>
