<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_item_model extends MY_Model
{
	public $table = 'ec_order_item';
	public $pk = 'id';

	public function __construct()
	{
		parent::__construct();

		$this->init($this->table, $this->pk);
	}

    public function getListByOrderID($order_id)
    {
        $item = $this->table;
        $product = $this->product_model->table;

        $default = array(
            'select' => "{$item}.*, {$product}.name, {$product}.price, {$product}.image",
            'join' => array(
                array('table' => $product, 'on' => "{$product}.id = {$item}.product_id", 'type' =>  'inner'),
            ),
            'conditions' => array(
                "{$item}.order_id" => $order_id,
            ),
            'order_by' => 'id asc',
        );

        $data = $this->getData($default);
        foreach ($data as & $row) {
            $row['image_src'] = $this->data['media_url'] . $row['image'];
            $row['image_thumb_src'] = str_replace('product/', 'product/thumb/', $row['image_src']);
        }

        return $data;
    }
	
	public function getListByOrder($order_id)
    {
        $default = array(
            'select' => "*",
            'conditions' => array(
                "order_id" => $order_id,
            ),
            'order_by' => 'id asc',
        );

        $data = $this->getData($default);
		foreach ($data as & $row) {
            $row['image_src'] = $this->data['media_url'] . $row['image'];
			$row['image_thumb_src'] = ($row['image'])? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
        }

        return $data;
    }
	
	public function getRowByID($order_id, $item_id)
    {
        $default = array(
            'select' => "*",
            'conditions' => array(
                "order_id" => $order_id,
				"id" => $item_id,
            ),
        );

        $data = $this->getData($default);

        return $data;
    }
	
	public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }

        //$data = $this->getData($params,'array',true);
		$data = $this->getData($params,'array');
		
		return $data;
    }
    public function getListCount($params=array())
    {
        $params['count'] = TRUE;
        return $this->getList($params);
    }

}


