<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_status_model extends MY_Model
{
    public $table = 'ec_order_status';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);

    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'name' => '',
            'sort_order' => 0,
			'enable' => 0,
        );


        return array_merge($fields, $data);
    }

    public function getVerifyConfig()
    {
        $config = array(
            'sort_order' => array(
                'field' => 'sort_order',
                'label' => '排序',
                'rules' => 'required|integer',
            ),
            'name' => array(
                'field' => 'name',
                'label' => '名稱',
                'rules' => 'required',
            ),
			'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|in_list[0,1]',
            ),
        );

        return $config;
    }

    public function getList($attrs=array())
    {

        $params = array(
            'select' => '',
            'order_by' => 'sort_order asc',
        );
        if (isset($attrs['conditions'])) {
            $params['conditions'] = $attrs['conditions'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = array(
                'many' => TRUE,
                'data' => array(
                    array('field' => 'name', 'value'=>$attrs['q'], 'position'=>'both'),
                ),
            );
            // unset
        }

        $data = $this->getData($params);


        return $data;
    }

    public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
        );

        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
        $data = $this->getList($params);
        return count($data);
    }

    public function getChoices()
    {
        $choices = array();
        $attrs['conditions'] = array();
        $data = $this->getList($attrs);
        foreach ($data as $row) {
            $choices[$row['id']] = $row['name'];
        }
        return $choices;
    }


}

