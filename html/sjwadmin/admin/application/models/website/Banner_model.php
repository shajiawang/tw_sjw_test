<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banner_model extends MY_Model
{
    public $table = 'banner';
    public $pk = 'id';
	public $position_choices = array(1=>'首頁版位', 2=>'分類頁版位');

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'company_id' => 1,
            'position' => 1,
            'title' => '',
			'slogan' => '',
			'image' => '',
			'url' => '',
			'start_date' => '',
			'end_date' => '',
			'new_window' => 0,
			'sort_order' => 0,
			'enable' => 0,
			'status' => 0,
        );

        return array_merge($fields, $data);
    }

    public function getVerifyConfig()
    {
        $config = array(
            'company_id' => array(
                'field' => 'company_id',
                'label' => '廠商ID',
                'rules' => 'integer',
            ),
			'position' => array(
                'field' => "position",
                'label' => "版位",
                'rules' => 'integer',
            ),
			'title' => array(
                'field' => "title",
                'label' => "廣告名稱",
                'rules' => 'required',
            ),
			/*
			'start_date' => array(
                'field' => 'start_date',
                'label' => '自動上架時間',
                'rules' => 'valid_datetime',
            ),
            'end_date' => array(
                'field' => 'end_date',
                'label' => '自動下架時間',
                'rules' => 'valid_datetime',
            ),*/
			'new_window' => array(
                'field' => 'new_window',
                'label' => '另開新頁',
                'rules' => 'required|in_list[0,1]',
            ),
            'sort_order' => array(
                'field' => 'sort_order',
                'label' => '排序',
                'rules' => 'required|integer'
            ),
			'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|in_list[0,1]',
            ),
			'status' => array(
                'field' => 'status',
                'label' => '是否刪除',
                'rules' => 'required|in_list[0,1]',
            ),
        );

        return $config;
    }

    public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
		if (isset($attrs['group_by'])) {
            $params['group_by'] = $attrs['group_by'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }
		if (isset($attrs['like'])) {
            $params['like'] = $attrs['like'];
        }

		$data = $this->getData($params);
		
		return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
        );

        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
        $data = $this->getList($params);
        return count($data);
    }

    public function getRowByID($id)
    {
        $default = array(
            'select' => "*",
            'conditions' => array(
				"id" => $id,
            ),
        );

        $data = $this->getData($default);

        return $data;
    }
	
	public function getChoices($conditions=array())
    {
        $choices = array();
        $attrs['conditions'] = $conditions;
        $data = $this->getList($attrs);
        foreach ($data as $row) {
            $choices[$row['id']] = $row['name'];
        }
        return $choices;
    }

}

