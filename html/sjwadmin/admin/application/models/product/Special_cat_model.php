<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Special_cat_model extends MY_Model
{
    public $table = 'special_cat';
    public $pk = 'id';
    
/*
滿額 full_price
加價 add_price
限時 hours
銅板 coin
*/
	public $field_name = array('full_price' => array('full_price', 'limit_qty'),
		'add_price' => array('limit_qty'),
		'hours' => array('hours', 'set_qty'),
		'coin' => array('limit_qty'),
	);

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getFormField($key=null)
    {
        $field_name = empty($key) ? array() : $this->field_name[$key];
		
		return $field_name;
    }
	
	public function getFormDefault($data=array())
    {
        $fields = array(
            'kind' => '',
            'name' => '',
            'description' => '',
            'enable' => 0,
            'sort_order' => 0,
			'exhibit_id' => 0,
			'category_id' => 5,
			'status' => 0
        );

        return array_merge($fields, $data);
    }

    public function getVerifyConfig()
    {
        $config = array(
            'kind' => array(
                'field' => 'kind',
                'label' => '功能類別',
                'rules' => 'required',
            ),
            'name' => array(
                'field' => 'name',
                'label' => '分類名稱',
                'rules' => 'required',
            ),
            'description' => array(
                'field' => 'description',
                'label' => '分類描述',
                'rules' => '',
            ),
            'sort_order' => array(
                'field' => 'sort_order',
                'label' => '排序',
                'rules' => 'required|integer',
                'errors' => array(
                    'required'=>'%s 請勿空白',
                    'integer'=>'%s 只能輸入數字',
                ),
            ),
            'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|in_list[0,1]',
            ),
			'exhibit_id' => array(
                'field' => 'exhibit_id',
                'label' => '展覽代碼',
                'rules' => 'trim|is_natural',
            ),
			'category_id' => array(
                'field' => 'category_id',
                'label' => '展覽產業ID',
                'rules' => 'trim|is_natural',
            ),
        );

        return $config;
    }

    public function getListByCid($category_id=0, $exhibit_id=null)
    {
        $data = array();
		
		$conditions = array('status' => 0,
			'category_id' => $category_id,
		);
		if($exhibit_id !== null){
			$conditions['exhibit_id'] = $exhibit_id;
		}
		
        $params = array(
            'conditions' => $conditions,
			'order_by' => 'sort_order asc',
        );
        $data = $this->getData($params);

        return $data;
    }

    public function getChoicesByCid($category_id=0)
    {
		$choices = array();
		
		$conditions = array('status' => 0,
			'category_id' => $category_id,
		);
	   
        $params = array(
            'conditions' => $conditions,
            'order_by' => 'sort_order asc',
        );
        $choices = $this->getData($params);
		
        return $choices;
    }

    public function getList($params=array(), $count=NULL)
    {
        $default = array(
            'select' => '*',
            'conditions' => array(),
            'order_by' => 'sort_order asc, create_at desc',
        );

        if (isset($params['select'])) {
            $default['select'] = $params['select'];
        }
		
        if (isset($params['conditions']) && is_array($params['conditions'])) {
            foreach ($params['conditions'] as $k => $v) {
                switch ($k) {
                    default:
                        $default['conditions']["{$this->table}.{$k}"] = $v;
                        break;
                }
            }
        }
		
        if (isset($params['q']) && $params['q'] != '') {
            $default['or_like'] = array(
                'many' => TRUE,
                'data' => array(
                    array('field'=>'title', 'value'=>$params['q'], 'position'=>'both'),
                    array('field'=>'slogan', 'value'=>$params['q'], 'position'=>'both'),
                ),
            );
        }

        if (isset($params['count']) && $params['count'] == TRUE) {
            $default['count'] = TRUE;
            return $this->getData($default);
        }

        if (isset($params['rows']) && $params['rows'] != '') $default['rows'] = $params['rows'];
        if (isset($params['offset']) && $params['offset'] != '') $default['offset'] = $params['offset'];
        if (isset($params['order_by']) && $params['order_by'] != '') $default['order_by'] = $params['order_by'];

        $data = $this->getData($default);

        return $data;
    }

    public function getListCount($params=array())
    {
        $params['count'] = TRUE;
        return $this->getList($params);
    }

}

