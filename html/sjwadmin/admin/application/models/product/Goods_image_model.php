<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Goods_image_model extends MY_Model
{
    public $table = 'goods_image';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($data=array())
    {
        $fields = array(
            'id' => 0,
            'image' => '',
            'sort_order' => 0,
        );

        return $fields;
    }

    public function getByGoodsId($goods_id)
    {
        $params = array(
            'conditions' => array('goods_id'=>$goods_id),
            'order_by' => 'sort_order asc',
        );
        $data = $this->getData($params);
        foreach ($data as & $row) {
            $row['image_url'] = ($row['image'])? $this->data['media_url'] . $row['image'] : '';
            $row['image_thumb_url'] = ($row['image'])? $this->data['media_url'] . preg_replace('/.(jpg|png|gif)$/', '_thumb.$1', $row['image']) : '';
            $extension = preg_replace('/^.*\.([^.]+)$/D', '$1', $row['image']);
            if ($extension == 'gif') {
                $row['image_thumb_url'] = $row['image_url'];
            }
        }

        return $data;
    }

}


