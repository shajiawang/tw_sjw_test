<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule_list_model extends MY_Model
{
    protected $table = 'schedule_list';
    protected $pk = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($var=array())
    {
        $data = array_merge(array(
            'auth' => array(),
            'title' => '',
        ), $var);

        return $data;
    }

    public function getVerifyConfig()
    {
        $config = array(
            'auth' => array(
                'field' => 'auth[]',
                'label' => '權限',
            ),
            'title' => array(
                'field' => 'title',
                'label' => '檔期名稱',
                'rules' => 'trim|required',
                'errors' => array('required'=>'%s 請勿空白'),
            ),
            /*
			'description' => array(
                'field' => 'description',
                'label' => 'Description',
                // 'rules' => ''
            ),
            'permission' => array(
                'field' => 'premission[]',
                'label' => 'Permission',
                // 'rules' => ''
            ),
			'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|integer',
            ),*/
        );

        return $config;
    }

    public function getList($attrs=array())
    {
        /*
		 id 序號	category_id 產業編號	years 年度	numbers 檔期編號	title 檔期名稱	venues 展覽場地	areas 展覽區域	begin_at 展覽開始	end_at 展覽截止	 in_out 進出場時間	
		 manager_inc 主辦公司 overall 統籌指導 director 專案協理	secretary 專案秘書	manager 專案經理	marketing 專案企劃	alliances 策盟部	design 視覺設計	decorate 管理部(裝潢
		*/
		$params = array( 
            'select' => 'id, numbers,years, category_id, title, venues, areas, begin_at, end_at, in_out, manager_inc, overall, director,secretary,manager,marketing,alliances,design, decorate ',
            'order_by' => 'id',
        );
        if (isset($attrs['conditions'])) {
            $params['conditions'] = $attrs['conditions'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = array(
                'many' => TRUE,
                'data' => array(
                    array('field' => 'title', 'value'=>$attrs['q'], 'position'=>'both'),
                ),
            );
            // unset
        }

        $data = $this->getData($params,'array'.true);
		
		return $data;
    }

    public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
        );

        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
        
		$data = $this->getList($params);
        
		return count($data);
    }


    public function getPermission($id)
    {
        $group = $this->get(array($this->pk=>$id));
        return explode(',', $group['permission']);
    }

    public function getChoices() {
        $choices = array();
        $groups = $this->getAll();
        foreach ($groups as $row) {
            $choices[$row['id']] = $row['name'];
        }

        return $choices;
    }
}

