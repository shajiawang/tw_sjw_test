<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Schedule_category_model extends MY_Model
{
    public $table = 'schedule_category';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();
        $this->init($this->table, $this->pk);
    }

    public function getFormDefault($user=array())
    {
        $data = array_merge(array(
            /*'auth' => array(),*/
            'title' => '',
            'enable' => 2,
        ), $user);

        return $data;
    }

    public function getVerifyConfig()
    {
        $config = array(
            /*
			'auth' => array(
                'field' => 'auth[]',
                'label' => '權限',
            ),*/
            'title' => array(
                'field' => 'title',
                'label' => '展覽產業',
                'rules' => 'trim|required',
                'errors' => array('required'=>'%s 請勿空白'),
            ),
            'enable' => array(
                'field' => 'enable',
                'label' => '啟用',
                'rules' => 'required|integer',
            ),
        );

        return $config;
    }

    public function getList($attrs=array())
    {
        $params = array(
            'select' => 'id, title, enable',
            'order_by' => 'id',
        );
        if (isset($attrs['conditions'])) {
            $params['conditions'] = $attrs['conditions'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        
		if (isset($attrs['q'])) {
            $params['or_like'] = array(
                'many' => TRUE,
                'data' => array(
                    array('field' => 'name', 'value'=>$attrs['q'], 'position'=>'both'),
                ),
            );
            // unset
        }

        $data = $this->getData($params);

        foreach ($data as & $group) {
            $conditions = array('schedule_category_id'=>$group['id']);
            $group['user_num'] = $this->user_model->getCount($conditions);
        }

        return $data;
    }

    public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
        );

        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
        $data = $this->getList($params);
        return count($data);
    }

    public function getChoices()
	{
        $choices = array();
        $groups = $this->getAll(); 
        
		foreach ($groups as $row) {
            $choices[$row['id']] = $row['title'];
        }
		ksort($choices);

        return $choices;
    }
	
	public function getUserGroup($port='is_vendor')
	{
		$params = array(
            'conditions' => array(
                'kind' => $port,
                'enable' => 1,
            ),
        );
        $groups = $this->getData($params,'array');
		
		$choices = array();
        foreach ($groups as $row) {
            $choices[$row['id']] = $row['name'];
        }

        return $choices;
    }
	
	public function getGroupbyId($id)
	{
		$params = array(
            'conditions' => array(
                'id' => $id,
                'enable' => 2,
            ),
        );
        $groups = $this->getData($params);
		
		$choices = '';
        foreach ($groups as $row) {
			$choices = $row['title'];
        }

        return $choices;
    }

}

