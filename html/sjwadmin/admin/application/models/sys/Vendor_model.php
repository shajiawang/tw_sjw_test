<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Vendor_model extends MY_Model
{
	public $table = 'user';
	public $pk = 'id';
	public $user_kind = array();

	public function __construct()
	{
		parent::__construct();

		$this->init($this->table, $this->pk);
		
		$this->user_kind = array(
			'user' => '一般用戶',
			'is_vendor' => '供應廠商',
			'back' => '後台管理'
		);

	}

	public function getList($attrs=array())
	{
		$groups = $this->user_group_model->getAll();
		$params = array(
			'select' => 'id, kind, user_group_id, name, username, email, telephone, enable, create_at',
		);
		if (isset($attrs['conditions'])) {
			$params['conditions'] = $attrs['conditions'];
		}
		if (isset($attrs['rows'])) {
			$params['rows'] = $attrs['rows'];
		}
		if (isset($attrs['offset'])) {
			$params['offset'] = $attrs['offset'];
		}
		if (isset($attrs['sort'])) {
			$params['order_by'] = $attrs['sort'];
		}
		if (isset($attrs['q'])) {
			$params['or_like'] = array(
				'many' => TRUE,
				'data' => array(
					array('field' => 'name', 'value'=>$attrs['q'], 'position'=>'both'),
					array('field' => 'username', 'value'=>$attrs['q'], 'position'=>'both'),
					array('field' => 'email', 'value'=>$attrs['q'], 'position'=>'both'),
					array('field' => 'telephone', 'value'=>$attrs['q'], 'position'=>'both'),
				),
			);
			// unset
		}

		$data = $this->getData($params);
		
		$groups_array = array();
		foreach ($groups as $v){
			$groups_array[$v['id']] = $v['name'];
		}
		
		foreach ($data as & $v_user){
			$v_user['group'] = $this->GroupName($groups_array, $v_user['user_group_id']);
		}
		
		return $data;
	}
	
	public function GroupName($groups=array(), $user_group_id=1 )
	{
		$user_group_array = explode(",", $user_group_id);
		
		foreach ($user_group_array as $gid){
			$_group_name[$gid] = $groups[$gid];
		}
		
		$_user_group = implode(",", $_group_name);
		
		return $_user_group;
	}

	public function getListCount($attrs=array())
	{
		$params = array(
			'conditions' => $attrs['conditions'],
		);

		if (isset($attrs['q'])) {
			$params['q'] = $attrs['q'];
		}
		$data = $this->getList($params);
		return count($data);
	}

	public function getChoices($conditions=array())
	{
		$data = array();
		$params = array(
			'sort' => 'id',
			'conditions' => $conditions,
		);
        
		$users = $this->getList($params);
		foreach ($users as $user) {
			$data[$user['id']] = $user['name'];
		}

		return $data;
	}
	
	public function getUserName($kind='is_vendor')
	{
		$data = array('null'=>'---');
		$conditions = array(
			'kind' => $kind,
			'enable'=>1,
			'is_admin'=>0
		);
		$params = array(
			'select' => 'username, name',
			'order_by' => 'username',
			'conditions' => $conditions,
		);
		
		$users = $this->getData($params,'array');
		foreach ($users as $user) {
			$data[$user['username']] = $user['name'];
		}

		return $data;
	}

	public function getUser($id)
	{
		$conditions = array(
			'status !=1' => null,
			'kind' => 'is_vendor',
			'enable' => 1,
			'id' => $id,
		); 
		
		return $this->get($conditions);
	}
	
	public function getUserById($id)
	{
		$conditions = array(
			'id' => $id,
		);

		return $this->get($conditions);
	}
	public function getUserByAccount($username)
	{
		$conditions = array(
			'username' => $username,
		);

		return $this->get($conditions);
	}

	public function getUserByEmail($email)
	{
		$conditions = array(
			'email' => $email,
		);

		return $this->get($conditions);
	}
	
}

