<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Setting_model extends MY_Model
{
    protected $table = 'sys_setting';
    protected $pk = 'field';
    public $set_kind = array();

    public function __construct()
    {
        parent::__construct();
        $this->init($this->table, $this->pk);

        $this->set_kind = array(
			'vendor_back' => '管理後台',
            'vendor_web' => '前台設定',
        );

    }

    public function getByField($field=NULL)
    {
        $data = $this->get(array('field'=>$field));

        return $data;
    }

    public function getChoices($conditions=array())
    {
        $data = array();
        $params = array(
            'conditions' => $conditions,
            'order_by' => 'kind asc, sort_order asc',
        );
        $settings = $this->getData($params, 'array');
		
        foreach ($settings as $row) {
            $data[$row['field']] = $row['value'];
        }

        return $data;
    }

    public function getList($_arr=array('vendor_back','vendor_web') )
    {
        $params = array(
			'conditions' => array('show'=>1),
			'where_in'=> array('field'=>'kind', 'value'=>$_arr),
			'order_by' => 'kind asc, sort_order asc',
        );
		
		$data = $setting = array();
        $setting = $this->getData($params,'array');
        
		foreach ($setting as $row ) {
            $data[$row['kind']][$row['field']] = $row;
        }

        return $data;

    }

    public function _update($fields) {
        foreach ($fields as $field => $value) {
            $this->update($field, array('value'=>$value));
        }
        return $this;
    }
}

