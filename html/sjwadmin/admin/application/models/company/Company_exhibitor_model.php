<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company_exhibitor_model extends MY_Model
{
    public $table = 'company_exhibitor';
    public $pk = 'id';

    public function __construct()
    {
        parent::__construct();

        $this->init($this->table, $this->pk);
    }

	//表單欄位預設
	public function getFormDefault($user=array())
    {
        $data = array_merge(array(
			'step2_status' => 0,
			'step2_comment' => '',
			'step3_status' => 0,
			'step3_comment' => '',
			'step4_status' => 0,
			'step4_comment' => '',
			'doc_apply1' => '',
			'doc_apply2' => '',
			'doc_apply3' => '',
			'doc_apply4' => '',
			'doc_apply5' => '',
			'doc_apply6' => '',
			'other_request' => '',
			'service_name' => '',
			
			'sign_name' => '',
			'contractor' => '',
			'contractor_title' => '',
			'contractor_tel' => '',
			'contractor_ext' => '',
			'contractor_cell' => '',
			'contractor_fax' => '',
			'contractor_email' => '',
			'join_area' => '',
			
			'comp_name_ch' => '',
			'comp_tel' => '',
			'comp_name_en' => '',
			'comp_fax' => '',
			
			'standard_place' => 0,
			'standard_place_price' => 0,
			'expand_place' => 0,
			'expand_place_price' => 0,
			'upgrade_place' => 0,
			'upgrade_place_price' => 0,
			'conner_price' => 0,
			'temporary_tel' => 0,
			'adsl' => 0,
			'total_price' => 0,
			'guarantee_price' => 0,
			'other_sel' => array(0=>0, 1=>0, 2=>0, 3=>0, 4=>0),
			'high_place_unit' => 0,	
			'high_place_price' => 0,	
			'double_place_price' => 0,
			'stage_audio' => 0,
			'pick_up' => 0,
			'balloon' => 0,
			'sponsor_item_1' => '',
			'sponsor_qty_1' => 0, 
			
        ), $user);

        return $data;
    }
	
	public function getTabList()
    {
        return array('check' => '資料審查'
			, 'info' => '廠商資訊'
			, 'stand' => '承租攤位'
		);
    }
	public function opt_step2_status()
	{
        return array(0 => '審查中'
			, 1 => '已通過'
			, 2 => '列入後補'
			, 3 => '退件'
		);
    }
	public function opt_step3_status()
	{
		return array(
			0 => '未進入繳費'
			, 1 => '已完款'
			, 2 => '有尾款或保證金'
			, 3 => '請款中'
		);
	}
	public function opt_step4_status()
	{
		return array(
			0 => '未進入複審'
			, 1 => '已通過'
			, 2 => '待補件'
			, 3 => '退件'
		);
	}
	public function opt_other_sel()
	{
		return array(
			'1' => '申請攤位內設置舞台音響'
			, '2' => '申請場內舉牌'
			, '3' => '申請懸升汽球'
			, '4' => '申請搭建超高攤位'
			, '5' => '申請搭建雙層攤位'
		);
	}
	
	public function other_sel_val($vars=null)
	{
		$data = array(1=>'',2=>'',3=>'',4=>'',5=>'');
		
		if(!empty($vars))
		{
			$other_sel = explode(",", $vars);
			foreach($other_sel as $k=>$v){
				if($v=='1'){ $data[1] = 1; }
				elseif($v=='2'){ $data[2] = 2; }
				elseif($v=='3'){ $data[3] = 3; }
				elseif($v=='4'){ $data[4] = 4; }
				elseif($v=='5'){ $data[5] = 5; }
			}
			
		}
		
		return $data;
	}

	public function getFormConfig()
    {
		//Disabled欄位'status'=>2
		
		$form_field['check']['step2_status'] = array('show'=>1, 'status'=>1, 'label'=>'初審', 'type'=>'select', 'remark'=>'');
		$form_field['check']['step2_comment'] = array('show'=>1, 'status'=>1, 'label'=>'初審審核訊息', 'type'=>'textarea', 'remark'=>'');
		$form_field['check']['step3_status'] = array('show'=>1, 'status'=>1, 'label'=>'繳款', 'type'=>'select', 'remark'=>'');
		$form_field['check']['step3_comment'] = array('show'=>1, 'status'=>1, 'label'=>'繳款審核訊息', 'type'=>'textarea', 'remark'=>'');
		$form_field['check']['step4_status'] = array('show'=>1, 'status'=>1, 'label'=>'複審', 'type'=>'select', 'remark'=>'');
		$form_field['check']['step4_comment'] = array('show'=>1, 'status'=>1, 'label'=>'複審審核訊息', 'type'=>'textarea', 'remark'=>'');
		$form_field['check']['doc_apply1'] = array('show'=>1, 'status'=>1, 'label'=>'(a)切結書', 'type'=>'link', 'remark'=>'');
		$form_field['check']['doc_apply2'] = array('show'=>1, 'status'=>1, 'label'=>'(b)執照或登記證', 'type'=>'link_img', 'remark'=>'');
		$form_field['check']['doc_apply3'] = array('show'=>1, 'status'=>1, 'label'=>'(c)銀行信託保證', 'type'=>'link_img', 'remark'=>'');
		$form_field['check']['doc_apply4'] = array('show'=>1, 'status'=>1, 'label'=>'(d)銀行履約保證', 'type'=>'link_img', 'remark'=>'');
		$form_field['check']['doc_apply5'] = array('show'=>1, 'status'=>1, 'label'=>'(e)連帶保證', 'type'=>'link_img', 'remark'=>'');
		$form_field['check']['doc_apply6'] = array('show'=>1, 'status'=>1, 'label'=>'(f)票券樣章', 'type'=>'link_img', 'remark'=>'');
		$form_field['check']['other_request'] = array('show'=>1, 'status'=>1, 'label'=>'備註', 'type'=>'textarea', 'remark'=>'');
		$form_field['check']['service_name'] = array('show'=>1, 'status'=>2, 'label'=>'業務人員', 'type'=>null, 'remark'=>'');
		$form_field['check']['create_at'] = array('show'=>1, 'status'=>2, 'label'=>'建檔日期', 'type'=>null, 'remark'=>'');
		$form_field['check']['update_at'] = array('show'=>1, 'status'=>2, 'label'=>'更新日期', 'type'=>null, 'remark'=>'');
		$form_field['check']['user_id'] = array('show'=>1, 'status'=>2, 'label'=>'異動人', 'type'=>null, 'remark'=>'');
		
		$form_field['info']['join_area'] = array('show'=>1, 'status'=>1, 'label'=>'參展性質分類', 'type'=>'select', 'remark'=>'');
		$form_field['info']['sign_name'] = array('show'=>1, 'status'=>1, 'label'=>'廠商招牌名稱', 'type'=>null, 'remark'=>'');
		$form_field['info']['contractor'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡人', 'type'=>null, 'remark'=>'');
		$form_field['info']['contractor_title'] = array('show'=>1, 'status'=>1, 'label'=>'職稱', 'type'=>null, 'remark'=>'');
		$form_field['info']['contractor_tel'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡電話', 'type'=>null, 'remark'=>'');
		$form_field['info']['contractor_ext'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡分機', 'type'=>null, 'remark'=>'');
		$form_field['info']['contractor_cell'] = array('show'=>1, 'status'=>1, 'label'=>'行動電話', 'type'=>null, 'remark'=>'');
		$form_field['info']['contractor_fax'] = array('show'=>1, 'status'=>1, 'label'=>'傳真號碼', 'type'=>null, 'remark'=>'');
		$form_field['info']['contractor_email'] = array('show'=>1, 'status'=>1, 'label'=>'聯絡電郵', 'type'=>null, 'remark'=>'');
		$form_field['info']['comp_name_ch'] = array('show'=>0, 'status'=>1, 'label'=>'手冊(中)-機關名稱', 'type'=>null, 'remark'=>'');
		$form_field['info']['comp_tel'] = array('show'=>0, 'status'=>1, 'label'=>'手冊(中)-電話', 'type'=>null, 'remark'=>'');
		$form_field['info']['comp_name_en'] = array('show'=>0, 'status'=>1, 'label'=>'手冊(英)-機關名稱', 'type'=>null, 'remark'=>'');
		$form_field['info']['comp_fax'] = array('show'=>0, 'status'=>1, 'label'=>'手冊(中)-傳真', 'type'=>null, 'remark'=>'');
		
		$form_field['stand']['standard_place'] = array('show'=>1, 'status'=>1, 'label'=>'標準攤位', 'type'=>null, 'remark'=>'');
		$form_field['stand']['standard_place_price'] = array('show'=>1, 'status'=>1, 'label'=>'攤位金額', 'type'=>null, 'remark'=>'');
		$form_field['stand']['expand_place'] = array('show'=>1, 'status'=>1, 'label'=>'不含隔間攤位', 'type'=>null, 'remark'=>'');
		$form_field['stand']['expand_place_price'] = array('show'=>1, 'status'=>1, 'label'=>'攤位金額', 'type'=>null, 'remark'=>'');
		$form_field['stand']['upgrade_place'] = array('show'=>1, 'status'=>1, 'label'=>'基裝升級方案', 'type'=>null, 'remark'=>'');
		$form_field['stand']['upgrade_place_price'] = array('show'=>1, 'status'=>1, 'label'=>'攤位金額', 'type'=>null, 'remark'=>'');
		$form_field['stand']['temporary_tel'] = array('show'=>1, 'status'=>1, 'label'=>'臨時電話', 'type'=>null, 'remark'=>'');
		$form_field['stand']['adsl'] = array('show'=>1, 'status'=>1, 'label'=>'ADSL', 'type'=>null, 'remark'=>'');
		$form_field['stand']['guarantee_price'] = array('show'=>1, 'status'=>1, 'label'=>'保證金', 'type'=>null, 'remark'=>'');
		$form_field['stand']['total_price'] = array('show'=>1, 'status'=>1, 'label'=>'參展總金額', 'type'=>null, 'remark'=>'');
		$form_field['stand']['other_sel'] = array('show'=>1, 'status'=>1, 'label'=>'其他特殊需求', 'type'=>'checkbox_group', 'remark'=>'');
		$form_field['stand']['stage_audio'] = array('show'=>1, 'status'=>1, 'label'=>'舞台音響保證金', 'type'=>null, 'remark'=>'');
		$form_field['stand']['pick_up'] = array('show'=>1, 'status'=>1, 'label'=>'舉牌保證金', 'type'=>null, 'remark'=>'');
		$form_field['stand']['balloon'] = array('show'=>1, 'status'=>1, 'label'=>'懸升氣球保證金', 'type'=>null, 'remark'=>'');
		$form_field['stand']['high_place_unit'] = array('show'=>1, 'status'=>1, 'label'=>'搭建超高攤位申請', 'type'=>null, 'remark'=>'');
		$form_field['stand']['high_place_price'] = array('show'=>1, 'status'=>1, 'label'=>'攤位金額', 'type'=>null, 'remark'=>'');
		$form_field['stand']['double_place_price'] = array('show'=>1, 'status'=>1, 'label'=>'雙層攤位', 'type'=>null, 'remark'=>'');
		$form_field['stand']['conner_price'] = array('show'=>1, 'status'=>1, 'label'=>'雙邊角落位置', 'type'=>null, 'remark'=>'');
		$form_field['stand']['sponsor_item_1'] = array('show'=>1, 'status'=>1, 'label'=>'贊助贈品品項', 'type'=>null, 'remark'=>'');
		$form_field['stand']['sponsor_qty_1'] = array('show'=>1, 'status'=>1, 'label'=>'贊助贈品數量', 'type'=>null, 'remark'=>'');

        return $form_field;
    }

    //表單欄位驗證
	public function getVerifyConfig()
    {
        $config = array(
            'sign_name' => array(
                'field' => 'sign_name',
                'label' => '廠商招牌名稱',
                'rules' => 'trim|required',
            ),
			'contractor' => array(
                'field' => 'contractor',
                'label' => '聯絡人',
                'rules' => 'trim|required',
            ),
			'contractor_email' => array(
                'field' => 'contractor_email',
                'label' => '聯絡電郵',
                'rules' => 'trim|valid_email', /*|is_unique[user.email]*/
            ),
            'contractor_tel' => array(
                'field' => 'contractor_tel',
                'label' => '聯絡電話',
                'rules' => 'trim|required',
            ),
        );

        return $config;
    }

    public function getList($attrs=array())
    {
		$params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );

		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['rows'])) {
            $params['rows'] = $attrs['rows'];
        }
        if (isset($attrs['offset'])) {
            $params['offset'] = $attrs['offset'];
        }
        if (isset($attrs['sort'])) {
            $params['order_by'] = $attrs['sort'];
        }
        if (isset($attrs['q'])) {
            $params['or_like'] = $attrs['q'];
        }

        $data = $this->getData($params,'array');
		
		return $data;
    }
	
	public function getListCount($attrs=array())
    {
        $params = array(
            'conditions' => $attrs['conditions'],
			'order_by' => $attrs['order_by'],
        );
		
		if (isset($attrs['select'])) {
            $params['select'] = $attrs['select'];
        }
		if (isset($attrs['join'])) {
            $params['join'] = $attrs['join'];
        }
        if (isset($attrs['q'])) {
            $params['q'] = $attrs['q'];
        }
		
        $data = $this->getList($params);
        return count($data);
    }

    public function getChoices($conditions=array())
    {
		$data = array();
		
		$table0 = $this->company_exhibitor_model->table;
		$table1 = $this->company_info_model->table;
		$params = array(
            'select' => $table1 .'.id,'. $table1 .'.comp_name',
			'conditions' => $conditions,
			'order_by' => 'comp_name',
        );
		
		$on = $table0 .'.company_id='. $table1 .'.id';
		$params['join'][0] = array('table'=>$table1, 'on'=>$on, 'type'=>'inner');
		
        $rows = $this->getList($params);
		if($rows)
		foreach ($rows as $v) {
            $data[$v['id']] = $v['comp_name'];
        }

        return $data;
    }
	public function getExhibitChoices()
	{
		$choices = array();
		
		$table0 = $this->company_exhibitor_model->table;
		$table1 = $this->exhibit_info_model->table;
		$today = date('Y-m-d', strtotime("now") );
		
		$conditions = array("{$table0}.status" => 0
			, "{$table0}.company_id" => $this->flags->user['company_id']
			, "{$table1}.ex_enddate >= '{$today}'" => null
		);
		
		$params = array( 
            'select' => "{$table1}.id, {$table1}.webname, {$table1}.ex_startdate",
			'conditions' => $conditions,
			'order_by' => "{$table1}.ex_startdate DESC",
        );
		$on_0 = "{$table0}.exhibit_id={$table1}.id";
		$params['join'][0] = array('table'=>$table1, 'on'=>$on_0, 'type'=>'inner');

        $rows = $this->getData($params);
		foreach ($rows as $row) {
            $choices[$row['id']] = $row['id'] .': '. $row['webname'] .' ('. $row['ex_startdate'] .')';
        }

        return $choices;
    }
	
	public function get_exhibitor($exhibit_id, $company_id=0)
    {
		$conditions = array('exhibit_id' => $exhibit_id
			, 'company_id' => $company_id
        );

        return $this->get($conditions,'array');
	}

    public function getRowByAccount($username)
    {
        $conditions = array(
            'account' => $username,
        );

        return $this->get($conditions);
    } 
	
	public function getRowById($id)
    {
        $conditions = array(
            'id' => $id,
        );

        return $this->get($conditions);
    }

    public function getRowByEmail($email)
    {
        $conditions = array(
            'contractor_email' => $email,
        );

        return $this->get($conditions);
    }

    public function _insert($fields=array())
    {
        return $this->insert($fields, 'create_at');
    }

    public function _update($pk, $fields=array())
	{
        return parent::update($pk, $fields);
    }

}

