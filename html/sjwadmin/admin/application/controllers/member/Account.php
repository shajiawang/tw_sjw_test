<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->ctrl_dir = 'member/account';
		$this->view_dir = 'member/account/';
		
		$this->load->model('sys/member_model');
		$this->load->model('sys/member_info_model');

		if (!isset($this->data['filter']['page'])) {
            $this->data['filter']['page'] = '1';
        }
        if (!isset($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = 'create_at desc';
        }
        if (!isset($this->data['filter']['q'])) {
            $this->data['filter']['q'] = '';
        }
	}

	public function index()
	{
		$this->data['page_name'] = 'list';

        $page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows']?$this->data['filter']['rows'] : $this->db_rows;
		$this->data['filter']['offset'] = $offset = ($page -1) * $rows;
		
		$attrs = array(
			'q' => $this->data['filter']['q'],
			'conditions' => array('enable !=1' => null),
			'rows' => $rows,
			'offset' => $offset,
		);
		
		if ($this->data['filter']['q'] !== '' ) {
			$attrs['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort'] !== '' ) {
			$attrs['order_by'] = $this->data['filter']['sort'];
		}
		
		$this->data['filter']['total'] = $total = $this->member_model->getListCount($attrs);
		$list = $this->member_model->getList($attrs); //echo $this->member_model->get_query();
		foreach ($list as & $row) {
			
			$row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
		}
		
		$this->data['list'] = $list;
		$this->load->library('pagination');
        $config['base_url'] = base_url("{$this->ctrl_dir}?". $this->getQueryString(array(), array('page')));
        $config['total_rows'] = $total;
        $config['per_page'] = $rows;
        $this->pagination->initialize($config);

		$this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
		//$this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");

		$this->layout->view("{$this->view_dir}list", $this->data);
	}

	public function add()
	{
		$this->data['page_name'] = 'add';
		$form = $this->member_model->getFormDefault();
		
		if ($post = $this->input->post())
		{
			//會員一個手機, 限一個帳號
			$ChoicesPhone = array();
			$conditions = array(
				'enable' => 2,
				'telephone' => $post['telephone'],
			);
			$ChoicesPhone = $this->member_model->getChoicesRows($conditions); //echo $this->member_model->get_query();
			
			if (! empty($ChoicesPhone)) {
				$this->setAlert(4, '資料錯誤, 手機已存在 !');
			} 
			elseif ($this->_isVerify('add') == TRUE)
			{
				//建立用戶帳號
				$rs = $this->chk_join($post); var_dump($rs);
				
				if ($rs['status']) {
					$this->setAlert(1, $rs['message']);
					redirect(base_url("{$this->ctrl_dir}/"));
				} else {
					$this->setAlert(4, $rs['message']);
				}
			}
		}

		$this->data['form'] =  $form;
		$this->data['link_save'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
		
		$this->layout->view("{$this->view_dir}add", $this->data);
	}
	private function chk_join( $vars=array() )
	{
		$phone = isset($vars['telephone']) ? $vars['telephone'] : NULL;
		$password = isset($vars['password']) ? $vars['password'] : NULL;
		
		/* 帳號、密碼 */
		$result = array(
			'status' => FALSE,
			'message' => '資料錯誤, 請重新輸入!',
			'user' => NULL,
		);
		
		if ($phone && $password)
		{
			//建立用戶帳號 
			$set_account = 'TW'. substr($phone, 1);
			$user_account = $set_account;
			//$_SESSION['join_account'] = $user_account;
			
			//寫入會員主檔
			$field['parent_uid'] = 1;
			$field['user_group_id'] = 14;
			$field['company_id'] = 0;
			$field['account'] = $user_account;
			$field['password'] = $this->myaes->mcrypt_en($password);
			$field['name'] = $phone;
			$field['email'] = $vars['email']; 
			$field['telephone'] = $phone;  
			$field['enable'] = 2; 
			$saved_id = $this->member_model->insert($field); //echo $this->member_model->get_query();
			
			//寫入帳戶資訊
			$mi_field['member_id'] = $saved_id;
			$mi_field['info'] = $user_account;
			$this->member_info_model->insert($mi_field);
			
			$result['status'] = TRUE;
			$result['message'] = '用戶帳號巳建立';
		}

		return $result;
	}
	
	public function edit($id=NULL)
	{
		$this->data['page_name'] = 'edit';
		
		if($this->flags->user['is_admin']){
			$this->data['chg_pwd'] = TRUE;
		}
		
		$get_data = $this->member_model->get($id);
		$form = $this->member_model->getFormDefault($get_data);
		if (!$form) {
			$this->setAlert(4, '錯誤連結請重新操作');
			redirect($_SERVER['HTTP_REFERER']);
		}
		
		$post = $this->input->post();
		//var_dump($post);exit;array(5) { ["user_group_id"]=> string(2) "14" ["name"]=> string(16) "0900999003名稱" ["password"]=> string(6) "123123" ["email"]=> string(11) "123@aaa.com" ["enable"]=> string(1) "0" } 
		if ($post) 
		{
			if ($this->_isVerify('edit', $form) == TRUE) {
				
				//寫入會員主檔
				$field['parent_uid'] = 1;
				$field['user_group_id'] = 14;
				$field['company_id'] = 0;
				$field['account'] = $user_account;
				$field['password'] = $this->myaes->mcrypt_en($password);
				$field['name'] = $phone;
				$field['email'] = $vars['email']; 
				$field['telephone'] = $phone;  
				$field['enable'] = 2; 
				$saved_id = $this->member_model->insert($field); //echo $this->member_model->get_query();
			
			/*
				$rs = $this->member_model->update($id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				redirect(base_url("{$this->ctrl_dir}?". $this->getQueryString(array('kind',$post['kind']))));
				*/
			}
		}

		$this->data['form'] = $form;

		$this->data['link_save'] = base_url("{$this->ctrl_dir}/edit/{$id}/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
		$this->layout->view("{$this->view_dir}edit", $this->data);
	}
	
	public function info($id=NULL)
	{
		$this->data['page_name'] = 'info';
		
		$get_user = $this->member_model->get($id);
		
		$old_data = $this->data['company_info'] = $this->company_info_model->getRowByAccount($get_user['username']);
		if (empty($this->data['company_info']))
		{
			$this->data['company_info']['account'] = $post2['account'] = $get_user['username'];//使用者帳戶user_id	
			$this->data['company_info']['comp_idno'] = $post2['comp_idno'] = $get_user['username'];//統一編號
			$this->data['company_info']['comp_name'] = $post2['comp_name'] = $get_user['name'];//廠商名稱	
			$this->data['company_info']['invoice_name'] = $post2['invoice_name'] = $get_user['name'];//發票抬頭		
			$this->data['company_info']['contractor_tel'] = $post2['contractor_tel'] = $get_user['telephone'];//聯絡電話	
			$this->data['company_info']['contractor_email'] = $post2['contractor_email'] = $get_user['email'];//聯絡電郵
			
			//新增`company_info`
			$company_info = $this->company_info_model->insert($post2);
			$this->data['company_info']['id'] = $company_info;
			$old_data = $this->data['company_info'];
			
			//更新`user`
			$user_post['company_id'] = $company_info;
			$rs = $this->member_model->update($id, $user_post);
		}

		//表單欄位選項
		$option_vars['exp_category'] = $this->schedule_category_model->getChoices(); 
		$this->data['form_options'] = $this->company_info_model->getOptions($option_vars);
		$this->data['form_options']['sales_id'] = $this->data['choices']['user_id'];
		$this->data['form_options']['maintain_name'] = $this->data['choices']['user_id'];
		
		//表單頁籤 create_at 
		$this->data['tablist'] = $this->company_info_model->getTabList();
		
		//表單欄位陣列
		$form_field = $this->company_info_model->getFormConfig();
		
		$this->data['form_field'] = $this->info_form_rule($this->data['tablist'], $form_field);

		if ($post = $this->input->post())
		{
			if ($this->_isVerify('info', $old_data) == TRUE)
			{
				$company_id = $this->data['company_info']['id'];
				$post['update_at'] = date('Y-m-d H:i:s', time() );
				$post['user_id'] = $this->flags->user['username'];
				
				$rs = $this->company_info_model->_update($company_id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				
				redirect(base_url("{$this->ctrl_dir}/info/{$id}/?{$_SERVER['QUERY_STRING']}"));
			}
		}
		
		$this->data['form'] = $this->member_model->getFormDefault($this->member_model->get($id));

		$this->data['link_save'] = base_url("{$this->ctrl_dir}/info/{$id}/?{$_SERVER['QUERY_STRING']}");
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?{$_SERVER['QUERY_STRING']}");
		$this->layout->view("{$this->view_dir}info", $this->data);
	}

	public function delete()
	{
		if ($post = $this->input->post()) {
			$del_num = 0;
			foreach ($post['rowid'] as $id) {
				$num = $this->ticket_sign_model->getCount(array('member_id'=>$id));
				if ($num == 0) {
					$rs = $this->member_model->delete($id);
					if ($rs) {
						$del_num ++;
					}
				}
			}
			$error_num = count($post['rowid']) - $del_num;
			if ($error_num == 0) {
				$this->setAlert(2, "共刪除 {$del_num} 筆資料");
			} else {
				$this->setAlert(3, "共刪除 {$del_num} 筆資料, {$error_num} 筆未刪除，請確認刪除的會員是否還有報名資料未刪除！");
			}
		}

		redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
	}

	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->member_model->getVerifyConfig();
		if ($action == 'edit') {
			$config['username']['rules'] = '';
			//$config['password']['rules'] = 'min_length[4]|max_length[20]';
			//$config['passconf']['rules'] = 'matches[password]';
			if ($old_data['email'] == $this->input->post('email')) {
				$config['email']['rules'] = 'valid_email';
			}
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}
}

