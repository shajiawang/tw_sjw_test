<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->ctrl_dir = 'member/service';
		$this->view_dir = 'member/service/';
		
		$this->load->model('company/company_info_model');

		$this->data['choices']['group'] = $this->user_group_model->getChoices(array());
		$this->data['choices']['user_id'] = $this->user_model->getUserName();
		$this->data['sales_options'] = $this->data['choices']['user_id'];

		$this->data['filter']['page'] = $this->input->get('page')? $this->input->get('page') : 1;
		$this->data['filter']['sort'] = $this->input->get('sort')? $this->input->get('sort') : NULL;
		$this->data['filter']['q'] = $this->input->get('q')? $this->input->get('q'): '';
		
		if (!isset($this->data['filter']['user_group_id'])) {
			$get_gid = isset($_SESSION['search_gid']) ? $_SESSION['search_gid'] : 'all';
			$this->data['filter']['user_group_id'] = $get_gid;
        } else {
			$_SESSION['search_gid'] = $this->data['filter']['user_group_id'];
		}
	}

	public function index()
	{
		//var_dump($this->flags->user['is_admin']);
		//var_dump($this->flags->user['kind']);
		//var_dump($this->data['choices']['group']);
		
		$page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows']?$this->data['filter']['rows'] : $this->db_rows;
		$offset = ($page -1) * $rows;

		$params = array(
			'q' => $this->data['filter']['q'],
			'conditions' => array( 'kind' => $this->kind ),
			'rows' => $rows,
			'offset' => $offset,
		);
	
		if ($this->data['filter']['user_group_id'] != 'all') {
			$params['conditions']['user_group_id'] = $this->data['filter']['user_group_id'];
		}
		
		if ($this->data['filter']['q'] !== '' ) {
			$params['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort'] !== '' ) {
			$params['order_by'] = $this->data['filter']['sort'];
		}
		$total = $this->user_model->getDataCount($params);
		$list = $this->user_model->getData($params); //echo $this->user_model->get_query();
		foreach ($list as & $row)
		{
			$row['groups'] = array();
			$group_arr = explode(',', $row['user_group_id']);
			foreach ($group_arr as $g) {
				$row['groups'][$g] = $this->data['choices']['group'][$g];
			}

			$row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
			
			if($row['kind']=='is_vendor'){
				$row['link_info'] = base_url("{$this->ctrl_dir}/info/{$row['id']}/?". $this->getQueryString());
			}
		}
		
		$this->load->library('pagination');
		$config['base_url'] = site_url("{$this->ctrl_dir}?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $params['rows'];
		$this->pagination->initialize($config);

		$this->data['list'] = $list;
		$this->data['pagination'] = array(
			'page' => $page,
			'total' => $total,
			'rows' => $rows,
			'offset' => $offset,
		);

		$this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
		$this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");
		//	$this->data['link_toggle_enable'] = base_url("{$this->ctrl_dir}/ajax_toggle/enable/');

		$this->layout->view("{$this->view_dir}list", $this->data);
	}

	public function add()
	{
		$this->data['page_name'] = 'add';
		$this->data['form'] = $this->user_model->getFormDefault();
		
		/*
		if($this->flags->user['is_admin']==1){
			$this->data['sales_id'] = null;
			$this->data['sales_disabled'] = '';
		} else {
			$this->data['sales_id'] = $this->flags->user['username'];
			$this->data['sales_disabled'] = 'disabled';
		}*/
		// default form data
		//if (isset($this->data['filter']['user_group_id'])) { $this->data['form']['user_group_id'] = $this->data['filter']['user_group_id']; }

		if ($post = $this->input->post())
		{
			if ($this->_isVerify('add') == TRUE)
			{
				$saved_id = $this->user_model->insert($post);
				if ($saved_id) {
					$this->setAlert(1, '資料新增成功');
				}

				redirect(base_url("{$this->ctrl_dir}/"));
			}
		}

		$this->data['link_save'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
		
		$this->layout->view("{$this->view_dir}add", $this->data);
	}

	public function edit($id=NULL)
	{
		$this->data['page_name'] = 'edit';

		$form = $this->user_model->get($id);
		if ($post = $this->input->post())
		{
			if ($this->_isVerify('edit', $form) == TRUE)
			{
				$post['user_group_id'] = isset($post['auth']) ? implode(",", $post['auth']) : $post['user_group_id'];
				
				$GroupKind = $this->user_group_model->getGroupKind($post['auth']);
				unset($post['auth']);
				$post['kind'] = $GroupKind;
				
				$rs = $this->user_model->update($id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				
				redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
			}
		}
		
		$this->data['form'] = $this->user_model->getFormDefault($this->user_model->get($id));
		$this->data['form']['auth'] = explode(",", $this->data['form']['user_group_id']);

		$this->data['link_save'] = base_url("{$this->ctrl_dir}/edit/{$id}/?". $this->getQueryString());
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
		
		$this->layout->view("{$this->view_dir}edit", $this->data);
	}
	
	public function info_form_rule($tablist, $form_field)
	{
		foreach ($tablist as $k => $v) 
		{
			foreach ($form_field[$k] as $_field=>$row)
			{
				$_rows[$k][$_field] = $row;
				
				//依權限調整欄位
				if($_field=='sales_id' || $_field=='maintain_name'){
					$_rows[$k][$_field]['status'] = ($this->flags->user['is_admin']==1) ? 1 : 2;
				}
				
				if($_field=='memo'){
					$_rows[$k][$_field]['status'] = ($this->flags->user['kind']=='tlback') ? 1 : 2;
					$_rows[$k][$_field]['show'] = ($this->flags->user['kind']=='tlback') ? 1 : 0;
				}
			}
			
			//只顯示 ['show']=1 欄位
			foreach ($_rows[$k] as $_k=>$_row)
			{
				if($_row['show']==1){
					$rows[$k][$_k] = $_row;
					$rows[$k][$_k]['disabled'] = ($_row['status']==2) ? 'disabled' : '';
				}
			}
		}
		
		return $rows;
	}
	
	public function info($id=NULL)
	{
		$this->data['page_name'] = 'info';
		
		$get_user = $this->user_model->get($id);
		
		$old_data = $this->data['company_info'] = $this->company_info_model->getRowByAccount($get_user['username']);
		if (empty($this->data['company_info']))
		{
			$this->data['company_info']['account'] = $post2['account'] = $get_user['username'];//使用者帳戶user_id	
			$this->data['company_info']['comp_idno'] = $post2['comp_idno'] = $get_user['username'];//統一編號
			$this->data['company_info']['comp_name'] = $post2['comp_name'] = $get_user['name'];//廠商名稱	
			$this->data['company_info']['invoice_name'] = $post2['invoice_name'] = $get_user['name'];//發票抬頭		
			$this->data['company_info']['contractor_tel'] = $post2['contractor_tel'] = $get_user['telephone'];//聯絡電話	
			$this->data['company_info']['contractor_email'] = $post2['contractor_email'] = $get_user['email'];//聯絡電郵
			
			//新增`company_info`
			$company_info = $this->company_info_model->insert($post2);
			$this->data['company_info']['id'] = $company_info;
			$old_data = $this->data['company_info'];
			
			//更新`user`
			$user_post['company_id'] = $company_info;
			$rs = $this->user_model->update($id, $user_post);
		}

		//表單欄位選項
		$option_vars['exp_category'] = $this->schedule_category_model->getChoices(); 
		$this->data['form_options'] = $this->company_info_model->getOptions($option_vars);
		$this->data['form_options']['sales_id'] = $this->data['choices']['user_id'];
		$this->data['form_options']['maintain_name'] = $this->data['choices']['user_id'];
		
		//表單頁籤 create_at 
		$this->data['tablist'] = $this->company_info_model->getTabList();
		
		//表單欄位陣列
		$form_field = $this->company_info_model->getFormConfig();
		
		$this->data['form_field'] = $this->info_form_rule($this->data['tablist'], $form_field);

		if ($post = $this->input->post())
		{
			if ($this->_isVerify('info', $old_data) == TRUE)
			{
				$company_id = $this->data['company_info']['id'];
				$post['update_at'] = date('Y-m-d H:i:s', time() );
				$post['user_id'] = $this->flags->user['username'];
				
				$rs = $this->company_info_model->_update($company_id, $post);
				if ($rs) {
					$this->setAlert(2, '資料編輯成功');
				}
				
				redirect(base_url("{$this->ctrl_dir}/info/{$id}/?{$_SERVER['QUERY_STRING']}"));
			}
		}
		
		$this->data['form'] = $this->user_model->getFormDefault($this->user_model->get($id));

		$this->data['link_save'] = base_url("{$this->ctrl_dir}/info/{$id}/?{$_SERVER['QUERY_STRING']}");
		$this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?{$_SERVER['QUERY_STRING']}");
		$this->layout->view("{$this->view_dir}info", $this->data);
	}

	public function delete()
	{
		if ($post = $this->input->post()) {
			foreach ($post['rowid'] as $id) {
				$rs = $this->user_model->delete($id);
			}
			$this->setAlert(2, '資料刪除成功');
		}

		redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
	}


	private function _isVerify($action='add', $old_data=array())
	{
		$config = $this->user_model->getVerifyConfig();
		
		if ($action == 'edit')
		{
			$config['username']['rules'] = '';
			$config['password']['rules'] = 'min_length[4]|max_length[20]';
			$config['passconf']['rules'] = 'matches[password]';
			if ($old_data['email'] == $this->input->post('email')) {
				$config['email']['rules'] = 'valid_email';
			}
		}
		elseif ($action == 'info')
		{
			$config = $this->company_info_model->getVerifyConfig();
			
			if ($old_data['contractor_email'] == $this->input->post('contractor_email')) {
				$config['contractor_email']['rules'] = 'valid_email';
			}
		}

		$this->form_validation->set_rules($config);
		$this->form_validation->set_error_delimiters('<p class="text-danger">', '</p>');
		// $this->form_validation->set_message('required', '請勿空白');

		return ($this->form_validation->run() == FALSE)? FALSE : TRUE;
	}


	public function ajax_toggle($field)
	{
		$result = array(
			'status' => FALSE,
			'message' => '',
		);

		if ($post = $this->input->post()) {
			$rs = $this->user_model->update($post['pk'], array($field=>$post['value']));
			if ($rs) {
				$result['status'] = TRUE;
			}
		}

		echo json_encode($result);
	}

}
