<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
            redirect(base_url('welcome'));
        }

        $this->load->model(array(
            'sales/order_model',
            'sales/order_history_model',
            'sales/order_item_model',
            'sales/order_status_model',
            'sales/payment_method_model',
        ));
		
		$this->data['subscription_total_items'] = '';
		$this->data['members'] = array();
		$this->data['subscriptions'] = array();

        $this->data['choices']['order_status'] = $this->order_status_model->getChoices();
        $this->data['choices']['payment_method'] = $this->payment_method_model->getChoices();
    }

    public function index()
    {
		if (!isset($this->data['filter']['order_status_id'])) {
            $this->data['filter']['order_status_id'] = 'all';
        }

        // $start_date = date('Y-m-d H:i:s', time() - 3600*24);
        // $end_date = date('Y-m-d H:i:s');

        $params = array(
            'conditions' => array(
                // 'start_date' => $start_date,
                // 'end_date' => $end_date,
            ),
            'order_by' => 'create_at desc',
            'rows' => 20,
        );
        if ($this->data['filter']['order_status_id'] != 'all') {
            $params['conditions']['order_status_id'] = $this->data['filter']['order_status_id'];
        }

        //$this->data['orders'] = $this->order_model->getList($params);

        $this->layout->view('dashboard', $this->data);
    }

}

