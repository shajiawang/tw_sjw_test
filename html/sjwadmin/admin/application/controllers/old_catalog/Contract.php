<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contract extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();

		if ($this->flags->is_login === FALSE) {
			redirect(base_url('welcome'));
		}

		$this->ctrl_dir = 'catalog/contract';
		$this->view_dir = 'catalog/contract/';

		$this->load->model(array(
            'sales/order_model',
        ));

        $this->data['choices']['order_status'] = $this->order_status_model->getChoices();
        //$this->data['choices']['payment_method'] = array();//$this->payment_model->getChoices();
        //$this->data['choices']['product'] = $this->product_model->getChoices();

        // default filter params
        if (empty($this->data['filter']['page'])) {
            $this->data['filter']['page'] = 1;
        }
        if (empty($this->data['filter']['q'])) {
            $this->data['filter']['q'] = '';
        }
        if (empty($this->data['filter']['sort'])) {
            $this->data['filter']['sort'] = 'create_at desc';
        }
        //if (empty($this->data['filter']['order_status_id'])) {
        //    $this->data['filter']['order_status_id'] = 1;
        //}
        //if (empty($this->data['filter']['payment_method_id'])) { $this->data['filter']['payment_method_id'] = 1; }
        //if (!isset($this->data['filter']['checkin'])) {  $this->data['filter']['checkin'] = 'all'; }
        if (!isset($this->data['filter']['date_type'])) {
            $this->data['filter']['date_type'] = 1;  // 訂單成立時間
        }
        if (empty($this->data['filter']['start_date'])) {
            // $this->data['filter']['start_date'] = date('Y-m-d', time() - (86400 * 7));
            $this->data['filter']['start_date'] = '';
        }
        if (empty($this->data['filter']['end_date'])) {
            // $this->data['filter']['end_date'] = date('Y-m-d', time() + (86400 * 1));
            $this->data['filter']['end_date'] = '';
        }
        if (empty($this->data['filter']['product_id'])) {
            $this->data['filter']['product_id'] = array();
        }
	}

	/*
	public function index()
	{
		$page = $this->data['filter']['page'];
		$rows = $this->data['filter']['rows']?$this->data['filter']['rows'] : $this->db_rows;
		$offset = ($page -1) * $rows;

		$params = array(
			'q' => $this->data['filter']['q'],
			'conditions' => array(),
			'rows' => $rows,
			'offset' => $offset,
		);
		if ($this->data['filter']['user_group_id'] != 'all' ) {
			$params['conditions']['user_group_id'] = $this->data['filter']['user_group_id'];
		}
		if ($this->data['filter']['q'] !== '' ) {
			$params['q'] = $this->data['filter']['q'];
		}
		if ($this->data['filter']['sort'] !== '' ) {
			$params['order_by'] = $this->data['filter']['sort'];
		}
		$total = $this->user_model->getDataCount($params);
		$list = $this->user_model->getData($params);
		foreach ($list as & $row) {
			$row['groups'] = array();
			$group_arr = explode(',', $row['user_group_id']);
			foreach ($group_arr as $g) {
				$row['groups'][$g] = $this->data['choices']['group'][$g];
			}

			$row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
		}
		
		$this->load->library('pagination');
		$config['base_url'] = site_url("{$this->ctrl_dir}?". $this->getQueryString(array(), array('page')));
		$config['total_rows'] = $total;
		$config['per_page'] = $params['rows'];
		$this->pagination->initialize($config);

		$this->data['list'] = $list;
		$this->data['pagination'] = array(
			'page' => $page,
			'total' => $total,
			'rows' => $rows,
			'offset' => $offset,
		);

		$this->data['link_add'] = base_url("{$this->ctrl_dir}/add/?". $this->getQueryString());
		$this->data['link_delete'] = base_url("{$this->ctrl_dir}/delete/?". $this->getQueryString());
		$this->data['link_refresh'] = base_url("{$this->ctrl_dir}/");
		//	$this->data['link_toggle_enable'] = base_url("{$this->ctrl_dir}/ajax_toggle/enable/');

		$this->layout->view("{$this->view_dir}list", $this->data);
	}*/
	public function index()
    {
        $this->data['page_name'] = 'contract_list';

        $page = $this->data['filter']['page'] = $this->input->get('page')? $this->input->get('page'): 1;
        $rows = $this->db_rows;
        $offset = ($page -1) * $rows;

        $params = array(
            'conditions' => array(),
            'rows' => $rows,
            'offset' => $offset,
        );

        //if ($this->data['filter']['checkin'] == '1' ) { $params['sort'] = 'date_checkin desc';  }
        //if ($this->data['filter']['payment_method_id'] != 'all') { $params['conditions']['payment_method_id'] = $this->data['filter']['payment_method_id']; }
        //if ($this->data['filter']['checkin'] != 'all') { $params['conditions']['checkin'] = $this->data['filter']['checkin']; }
		
		if ($this->data['filter']['q'] !== '' ) {
            $params['q'] = $this->data['filter']['q'];
        }
        if ($this->data['filter']['sort'] !== '' ) {
            $params['sort'] = $this->data['filter']['sort'];
        }
        //if ($this->data['filter']['order_status_id'] != 'all') {
        //    $params['conditions']['order_status_id'] = $this->data['filter']['order_status_id'];
        //}
		if (count($this->data['filter']['product_id']) > 0) {
            $params['conditions']['product_id'] = $this->data['filter']['product_id'];
        }
        if ($this->data['filter']['date_type'] == 1) {
            if ($this->data['filter']['start_date'] != '') {
                $params['conditions']['create_at >='] = "{$this->data['filter']['start_date']} 00:00:00";
            }
            if ($this->data['filter']['end_date'] != '') {
                $params['conditions']['create_at <='] = "{$this->data['filter']['end_date']} 23:59:59";
            }
        } elseif ($this->data['filter']['date_type'] == 2) {
            if ($this->data['filter']['start_date'] != '') {
                $params['conditions']['date_checkin >='] = "{$this->data['filter']['start_date']} 00:00:00";
            }
            if ($this->data['filter']['end_date'] != '') {
                $params['conditions']['date_checkin <='] = "{$this->data['filter']['end_date']} 23:59:59";
            }
            if ($this->data['filter']['start_date'] != '' || $this->data['filter']['end_date'] != '') {
                $params['conditions']['checkin'] = 1;
            }
        }

        $total = $this->order_model->getListCount($params);
        $this->data['list'] = $this->order_model->getList($params); //echo $this->user_model->get_query();
        
		foreach ($this->data['list'] as & $row)
		{
            //$row['date_modified'] = strtotime($row['date_modified'])>0?$row['date_modified']:'';
			$order_status_id = $this->order_history_model->getStatusByID($row['id'], $row['order_no']);
			$row['order_status'] = $this->data['choices']['order_status'][$order_status_id];
            $row['items'] = $this->order_item_model->getListByOrderID($row['id'],$row['order_no']);
			$row['link_edit'] = base_url("{$this->ctrl_dir}/edit/{$row['id']}/?". $this->getQueryString());
        }

        //$this->load->model('api/exhibit_model');
        //$this->data['choices']['exhibits'] = $this->exhibit_model->getChoices();


        $this->load->library('pagination');
        $config['base_url'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString(array(), array('page')));
        $config['total_rows'] = $total;
        $config['per_page'] = $rows;
        $this->pagination->initialize($config);

        $this->data['pagination'] = array(
            'page' => $page,
            'page_total' => ceil($total/$rows),
            'total' => $total,
            'rows' => $rows,
            'offset' => $offset,
        );

        //$this->data['link_truck'] = base_url("{$this->ctrl_dir}/truck/?". $this->getQueryString());
        //$this->data['link_export'] = base_url("{$this->ctrl_dir}/export/?". $this->getQueryString());

        $this->layout->view("{$this->view_dir}list", $this->data);
    }

    public function edit($id=NULL)
    {
        $this->data['page_name'] = 'sales_order_edit';
        $this->data['form'] = $this->order_model->getFormDefault($this->order_model->get($id));
        $this->data['form']['history'] = $this->order_history_model->getListByOrderID($id);
        $this->data['form']['items'] = $this->order_item_model->getListByOrderID($id);

        if ($post = $this->input->post()) {
            $old_data = $this->order_model->get($id);
            if ($this->_isVerify('edit', $old_data) == TRUE) {
                $rs = $this->order_model->update($id, $post);
                if ($rs) {
                    $this->setAlert(2, '資料編輯成功');
                }
                redirect(base_url("{$this->ctrl_dir}/?". $this->getQueryString()));
            }
        }

        $this->data['products'] = $this->product_model->getList();
        $this->setJson('products', $this->data['products']);
        $this->setJson('choices', $this->data['choices']);

        $this->data['link_cancel'] = base_url("{$this->ctrl_dir}/?". $this->getQueryString());
        $this->layout->view("{$this->view_dir}edit", $this->data);
    }

    public function export()
    {
        $params = array(
            'conditions' => array(
            ),
        );

        if ($this->data['filter']['q'] !== '' ) {
            $params['q'] = $this->data['filter']['q'];
        }
        if ($this->data['filter']['sort'] !== '' ) {
            $params['sort'] = $this->data['filter']['sort'];
        }
        if ($this->data['filter']['checkin'] == '1' ) {
            $params['sort'] = 'date_checkin desc';
        }

        if ($this->data['filter']['order_status_id'] != 'all') {
            $params['conditions']['order_status_id'] = $this->data['filter']['order_status_id'];
        }
        if ($this->data['filter']['payment_method_id'] != 'all') {
            $params['conditions']['payment_method_id'] = $this->data['filter']['payment_method_id'];
        }
        if (count($this->data['filter']['product_id']) > 0) {
            $params['conditions']['product_id'] = $this->data['filter']['product_id'];
        }
        if ($this->data['filter']['checkin'] != 'all') {
            $params['conditions']['checkin'] = $this->data['filter']['checkin'];
        }

        if ($this->data['filter']['date_type'] == 1) {
            if ($this->data['filter']['start_date'] != '') {
                $params['conditions']['create_at >='] = "{$this->data['filter']['start_date']} 00:00:00";
            }
            if ($this->data['filter']['end_date'] != '') {
                $params['conditions']['create_at <='] = "{$this->data['filter']['end_date']} 23:59:59";
            }
        } elseif ($this->data['filter']['date_type'] == 2) {
            if ($this->data['filter']['start_date'] != '') {
                $params['conditions']['date_checkin >='] = "{$this->data['filter']['start_date']} 00:00:00";
            }
            if ($this->data['filter']['end_date'] != '') {
                $params['conditions']['date_checkin <='] = "{$this->data['filter']['end_date']} 23:59:59";
            }
            if ($this->data['filter']['start_date'] != '' || $this->data['filter']['end_date'] != '') {
                $params['conditions']['checkin'] = 1;
            }
        }


        $orders = $this->order_model->getList($params);

        // 新增Excel物件
        $this->load->library('excel');
        $objPHPExcel = new PHPExcel();

        // 設定屬性
        $objPHPExcel->getProperties()->setCreator("PHP")
                    ->setLastModifiedBy("PHP")
                    ->setTitle("Orders")
                    ->setSubject("Subject")
                    ->setDescription("Description")
                    ->setKeywords("Keywords")
                    ->setCategory("Category");

        // 設定操作中的工作表
        $objPHPExcel->setActiveSheetIndex(0);
        $sheet = $objPHPExcel->getActiveSheet();

        // 將工作表命名
        $sheet->setTitle('Order List');

        $row = 1;
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, '訂單編號');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $row, '發票編號');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $row, '品名');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $row, '數量');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $row, '單價');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $row, '小計');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $row, '姓名');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(7, $row, '性別');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(8, $row, 'E-mail');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(9, $row, '支付方式');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(10, $row, '訂單狀態');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(11, $row, '廣告碼');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(12, $row, '巳領取');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(13, $row, '領取時間');
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(14, $row, '建立時間');
        $row += 1;

        foreach ($orders as $v)
		{
            $items = $this->order_item_model->getListByOrderID($v['id']);
            $order_status = $this->data['choices']['order_status'][$v['order_status_id']];
            $payment_method = $this->data['choices']['payment_method'][$v['payment_method_id']];
            $checkin_status = ($v['checkin']==1)? '是' : '否';
            $create_at = strtotime($v['create_at'])>0? $v['create_at'] : '';
            $date_checkin = strtotime($v['date_checkin'])>0? $v['date_checkin'] : '';

            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(0, $row)->setValueExplicit($v['order_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(1, $row)->setValueExplicit($v['invoice_no'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(2, $row)->setValueExplicit($items[0]['name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(3, $row)->setValueExplicit($items[0]['quantity'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(4, $row)->setValueExplicit($items[0]['price'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(5, $row)->setValueExplicit($items[0]['subtotal'], PHPExcel_Cell_DataType::TYPE_NUMERIC);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(6, $row)->setValueExplicit($v['buyer_name'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(7, $row)->setValueExplicit(($v['buyer_gender']==1)?'男':'女', PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(8, $row)->setValueExplicit($v['buyer_email'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(9, $row)->setValueExplicit($payment_method, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(10, $row)->setValueExplicit($order_status, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(11, $row)->setValueExplicit($v['income_id'], PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(12, $row)->setValueExplicit($checkin_status, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(13, $row)->setValueExplicit($date_checkin, PHPExcel_Cell_DataType::TYPE_STRING);
            $objPHPExcel->getActiveSheet()->getCellByColumnAndRow(14, $row)->setValueExplicit($create_at, PHPExcel_Cell_DataType::TYPE_STRING);
            
            $row++;
        }
        
        $filename = generatorRandom(10) . '.xlsx';  // 亂數檔名
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'. $filename .'"');
        header('Cache-Control: max-age=0');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
        $objWriter->save('php://output');
        exit;
    }

    /**
     * @param string history
     *
    **/
    public function ajax($action=NULL)
    {
        $result = array(
            'status' => FALSE,
            'data' => array(),
        );

        $post = $this->input->post();
        if ($action && $post) {
            $saved_id = NULL;
            switch ($action) {
                case 'payment':
                    $result['status'] = TRUE;

                    $order_fields = array(
                        'payment_method_id' => $post['payment_method_id'],
                        'total' => $post['total'],
                        'buyer_name' => $post['buyer_name'],
                        'buyer_email' => $post['buyer_email'],
                        'buyer_phone' => $post['buyer_phone'],
                        'buyer_gender' => $post['buyer_gender'],
                        'buyer_birthday' => $post['buyer_birthday'],
                        'remark' => $post['remark'],
                    );
                    $rs = $this->order_model->update($post['order_id'], $order_fields);
                    if ($rs) {
                        $result['status'] = TRUE;
                        $result['data'] = $order_fields;
                    }

                    break;

                case 'history':
                    $now = date('Y-m-d H:i:s');
                    $fields = array(
                        'order_id' => $post['order_id'],
                        'user_id' => $this->flags->user['id'],
                        'order_status_id' => $post['order_status_id'],
                        'comment' => $post['comment'],
                        'create_at' => $now,
                    );
                    $saved_id = $this->order_history_model->insert($fields);
                    if ($saved_id) {
                        // 更新最後修改時間
                        $order_fields = array(
                            'order_status_id' => $post['order_status_id'],
                            'date_modified' => $now,
                        );
                        $this->order_model->update($post['order_id'], $order_fields);

                        $result['status'] = TRUE;
                        $fields['user_name'] = $this->flags->user['name'];
                        $result['data'] = $fields;
                    }
                    break;

                case 'product_remove':
                    $rs = $this->order_item_model->delete($post['order_item_id']);
                    if ($rs) {
                        $result['status'] = TRUE;
                        $result['data']['product_total'] = $this->order_item_model->getSum('subtotal', array('order_id'=>$post['order_id']));

                        $order_fields = array(
                            'date_modified' => date('Y-m-d H:i:s'),
                        );
                        $this->order_model->update($post['order_id'], $order_fields);
                    }
                    break;

                case 'product':
                    $order = $this->order_model->get($post['order_id']);
                    $product = $this->goods_model->getInfo($post['item_id']);

                    $subtotal = $post['price'] * $post['quantity'];

                    $fields = array(
                        'order_id' => $post['order_id'],
                        'item_id' => $product['id'],
                        'name' => $product['name'],
                        'appraisal_no' => $product['appraisal_no'],
                        'image' => $product['image_thumb_src'],
                        'quantity' => $post['quantity'],
                        'price' => $post['price'],
                        'subtotal' => $subtotal,
                    );
                    $saved_id = $this->order_item_model->insert($fields);
                    if ($saved_id) {
                        $result['status'] = TRUE;
                        $result['data'] = $fields;
                        $result['data']['image_thumb_src'] = $product['image_thumb_src'];
                        $result['data']['product_total'] = $this->order_item_model->getSum('subtotal', array('order_id'=>$post['order_id']));
                        $result['data']['link'] = base_url("catalog/goods/edit/{$product['id']}");

                        $order_fields = array(
                            'date_modified' => date('Y-m-d H:i:s'),
                        );
                        $this->order_model->update($post['order_id'], $order_fields);
                    }

                    break;

                case 'checkin':
                    $fields = array(
                        'id' => $post['order_id'],
                        'checkin' => $post['checkin'],
                        'date_checkin' => date('Y-m-d H:i:s'),
                    );
                    $rs = $this->order_model->update($post['order_id'], $fields);
                    if ($rs) {
                        $result['status'] = TRUE;
                        $result['data'] = $fields;
                    }
                    break;

                case 'getProductChoices':
                    $aid = $this->input->post('aid');
                    if ($aid) {
                        $result['data'] = $this->product_model->getChoices(array('aid'=>$aid));
                        $result['status'] = TRUE;
                    }
                    break;
            }
        }

        echo json_encode($result);
    }

	public function ajax_toggle($field)
	{
		$result = array(
			'status' => FALSE,
			'message' => '',
		);

		if ($post = $this->input->post()) {
			$rs = $this->user_model->update($post['pk'], array($field=>$post['value']));
			if ($rs) {
				$result['status'] = TRUE;
			}
		}

		echo json_encode($result);
	}

}
