<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Allpay
{
	private $CI;
	private $MerchantID;
	private $HashKey;
	private $HashIV;

	public function __construct()
	{

		$this->CI =& get_instance();
	}

	public function setMerchantID($value)
	{
		$this->MerchantID = $value;
		return $this;
	}

	public function setHashKey($value)
	{
		$this->HashKey = $value;
		return $this;
	}

	public function setHashIV($value)
	{
		$this->HashIV = $value;
		return $this;
	}

	public function getPaymentInfo($attrs)
	{
		$data = array();
		$data['MerchantID'] = $this->MerchantID;
		$data['MerchantTradeNo'] = $attrs['MerchantTradeNo'];
		$data['MerchantTradeDate'] = date('Y/m/d H:i:s', strtotime($attrs['MerchantTradeDate']));  // 厂商交易时间 yyyy/MM/dd HH:mm:ss
		$data['PaymentType'] = 'aio';
		$data['TotalAmount'] = $attrs['TotalAmount'];
		$data['TradeDesc'] = $attrs['TradeDesc'];
		$data['ItemName'] = $attrs['ItemName'];
		$data['ChoosePayment'] = $attrs['ChoosePayment'];  // Create, ATM, CVS
		$data['ChooseSubPayment'] = $attrs['ChooseSubPayment'];  // Create, ATM, CVS
		$data['ItemURL'] = '';
		$data['Remark'] = '';
		$data['NeedExtraPaidInfo'] = 'Y';
		$data['DeviceSource'] = 'P';  // 版面选择 P=桌机,  M=手机

		$data['ClientBackURL'] = '';  // 付款完成页面
		$data['ReturnURL'] = $attrs['ReturnURL'];  // 销帐网址
		$data['OrderResultURL'] = $attrs['OrderResultURL'];  // 付款结果页

		$data['CheckMacValue'] = $this->getCheckMacValue($data);

		return $data;
	}

	public function writeOff($data)
	{
		$result = array(
			'status' => FALSE,
			'response' => json_encode($data)
			);


		$CheckMacValue = $data['CheckMacValue'];
		unset($data['CheckMacValue']);

		// if ($this->getCheckMacValue($data) == $CheckMacValue) {
			$result['status'] = ($data['RtnCode'] == 1)? TRUE : FALSE;
			$result['order_no'] = $data['MerchantTradeNo'];
			$result['error_message'] = $data['RtnMsg'];
			$result['payment_time'] = date('Y-m-d H:i:s', strtotime($data['PaymentDate']));
			$result['cardno'] = "{$data['card6no']}******{$data['card4no']}";
			$result['amount'] = $data['amount'];
		// }

		return $result;
	}

	private function getCheckMacValue($data)
	{
		// A-Z排序
		ksort($data);

		$hash_code = "HashKey={$this->HashKey}";
		$hash_code .= '&' . urldecode(http_build_query($data));
		$hash_code .= '&' . $this->HashIV;

		// $hash_code = "HashKey={$this->HashKey}";
		// foreach ($data as $key=>$value) {
		// 	$hash_code .= "&{$key}={$value}";
		// }
		// $hash_code .= "&HashIV={$this->HashIV}";


		// jdd($hash_code);
		// jdd(urlencode($hash_code));
		// jdd(strtolower(urlencode($hash_code)));
		// jdd(md5(strtolower(urlencode($hash_code))));

		return md5(strtolower(urlencode($hash_code)));
	}

}

/* End of file order.php */
/* Location: ./application/libraries/allpay.php */

