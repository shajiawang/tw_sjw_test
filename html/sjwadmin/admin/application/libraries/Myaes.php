<?php
/*
//加密
$_encrypt = $this->myaes->mcrypt_en('許衛珮123'); var_dump($_encrypt);
//解密
$_decrypt = $this->myaes->mcrypt_de($_encrypt); var_dump($_decrypt);

$private_key = $_encrypt; 
//動態加密
$dynamic_encrypt = $this->myaes->dynamic_crypt($private_key); var_dump($dynamic_encrypt);
//驗證動態加密
$dy_decrypt = $this->myaes->dynamic_crypt($private_key, 'de', $dynamic_encrypt); var_dump($dy_decrypt);
*/

class MyAes
{
    private $hex_iv = '00000000000000000000000000000000'; # converted JAVA byte code in to HEX and placed it here
    /**
     * var string $secret_key 加解密的密鑰
     */
    protected $secret_key;
	private $key = '497f2eh61709104f6w68356ehsb68k90'; #Same as in JAVA
 
	public function __construct()
	{
		$this->_Static_key = 'tY&2/8XrwlC~kLfAEvzRgJ52jnUYdb+Sm';
        $this->secret_key = hash('sha256', $this->_Static_key, true);
    }
 
    /*
	uniqid()是基於時間的，根據php.net的“返回值與microtime（）有點不同”。 
	PHP7 隨機亂數——PHP7 為 CSPRNG 引入了二種新函數：random_bytes 與 random_int。
	https://segmentfault.com/a/1190000004182573
	*/

	//// 設定 Token hash ////////////////////////
	
	public function getToken($length=40)
	{
		$token = '';
		$codeAlphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$codeAlphabet.= 'abcdefghijklmnopqrstuvwxyz';
		$codeAlphabet.= '0123456789';
		$max = strlen($codeAlphabet); // edited

		for($i=0; $i < $length; $i++) {
			//$token .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
			$token .= $codeAlphabet[random_int(0, $max-1)];
		}

		return $token;
	}
	public function token_hash()
	{
		//$rand = uniqid(rand(), TRUE);
		//$_hash = hash('sha1', md5($rand) . microtime(TRUE));
		$_hash = $this->getToken();
		
		return $_hash;
	}
	private function crypto_rand_secure($min, $max)
	{
		$range = $max - $min;
		if ($range < 1) return $min; // not so random...
		$log = ceil(log($range, 2));
		$bytes = (int) ($log / 8) + 1; // length in bytes
		$bits = (int) $log + 1; // length in bits
		$filter = (int) (1 << $bits) - 1; // set all lower bits to 1
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter; // discard irrelevant bits
		} while ($rnd > $range);

		return $min + $rnd;
	}

	
	//// 靜態 加解密 ////////////////////////
	
	// 靜態加密
	public function mcrypt_en($input)
    {
	   $data = openssl_encrypt($input, 'AES-256-CBC', $this->secret_key, OPENSSL_RAW_DATA, $this->hexToStr($this->hex_iv));
        $data = base64_encode($data);
		
		return $data;
    }
    // 靜態解密
	public function mcrypt_de($input)
    {
        $decrypted = openssl_decrypt(base64_decode($input), 'AES-256-CBC', $this->secret_key, OPENSSL_RAW_DATA, $this->hexToStr($this->hex_iv));
        
		return $decrypted;
    }
	
	/* For PKCS7 padding */
    private function addpadding($string, $blocksize = 16)
	{
        $len = strlen($string);
        $pad = $blocksize - ($len % $blocksize);
        $string .= str_repeat(chr($pad), $pad);
 
        return $string;
    }
    private function strippadding($string)
	{
        $slast = ord(substr($string, -1));
        $slastc = chr($slast);
        $pcheck = substr($string, -$slast);
 
        if (preg_match("/$slastc{" . $slast . "}/", $string)) {
            $string = substr($string, 0, strlen($string) - $slast);
 
            return $string;
 
        } else {
            return false;
        }
    }
	private function hexToStr($hex)
    {
        $string='';
 
        for ($i=0; $i < strlen($hex)-1; $i+=2)
        {
            $string .= chr(hexdec($hex[$i].$hex[$i+1]));
        }
 
        return $string;
    }
	
	/*
	PHP7 password_hash 雜湊加密採用隨機 SALT
	https://adon988.logdown.com/posts/2513623-modern-php-password-hash-hash-encryption-using-random-salt-uses
	*/
	
	//// 動態 加解密 ////////////////////////
	
	// 動態加密
	private function dynamic_EnCrypt($private_key)
    {
        $hash = password_hash($private_key, PASSWORD_DEFAULT);
		return $hash;
    }
	// 驗證動態加密
	private function dynamic_DeCrypt($private_key, $hash)
    {
        $valid = FALSE;
		
		if (password_verify($private_key, $hash)) {
			$valid = TRUE;
		}
		
		return $valid;
    }
	public function dynamic_crypt($private_key, $type='en', $hash=null)
	{
		if($type=='de')
		{
			$data = empty($hash) ? null : base64_decode($hash);
			$decrypted = $this->dynamic_DeCrypt($private_key, $data);
			
			return $decrypted;
		}
		else
		{
			$hash = $this->dynamic_EnCrypt($private_key);
			$encrypted = base64_encode($hash);
			
			return $encrypted;
		}
	}
	
	
	//// 動態 數位簽章 ////////////////////////
	
	//數位簽章
	private function sign_config($id=0)
	{
		$sign_array[0] = array(
			'+'=>'1', '~'=>'2', '!'=>'3', '@'=>'4', '#'=>'5', '%'=>'6', '^'=>'7', '&'=>'8', '*'=>'9'
		);
		$sign_array[1] = array(
			'!'=>'1', '@'=>'2', '#'=>'3', '%'=>'4', '^'=>'5', '~'=>'6', '&'=>'7', '*'=>'8', '+'=>'9'
		);
		$sign_array[2] = array(
			'+'=>'1', '~'=>'2', '@'=>'3', '#'=>'4', '!'=>'5', '%'=>'6', '&'=>'7', '*'=>'8', '^'=>'9'
		);
		$sign_array[3] = array(
			'@'=>'1', '+'=>'2', '~'=>'3', '!'=>'4', '^'=>'5', '#'=>'6', '&'=>'7', '*'=>'8', '%'=>'9'
		);
		$sign_array[4] = array(
			'#'=>'1', '+'=>'2', '~'=>'3', '!'=>'4', '%'=>'5', '&'=>'6', '^'=>'7', '*'=>'8', '@'=>'9'
		);
		$sign_array[5] = array(
			'*'=>'1', '~'=>'2', '!'=>'3', '@'=>'4', '#'=>'5', '%'=>'6', '+'=>'7', '^'=>'8', '&'=>'9' 
		);
		$sign_array[6] = array(
			'&'=>'1', '~'=>'2', '!'=>'3', '+'=>'4', '@'=>'5', '%'=>'6', '*'=>'7', '^'=>'8', '#'=>'9' 
		);
		$sign_array[7] = array(
			'%'=>'1', '+'=>'2', '!'=>'3', '@'=>'4', '*'=>'5', '#'=>'6', '^'=>'7', '&'=>'8', '~'=>'9'
		);
		$sign_array[8] = array(
			'~'=>'1', '#'=>'2', '!'=>'3', '+'=>'4', '@'=>'5', '^'=>'6', '&'=>'7', '%'=>'8', '*'=>'9'
		);
		$sign_array[9] = array(
			'^'=>'1', '+'=>'2', '~'=>'3', '@'=>'4', '#'=>'5', '%'=>'6', '&'=>'7', '*'=>'8', '!'=>'9'
		);
		
		return $sign_array[$id];
	}
	private function sign_value($key=null)
	{
		$sign_value = array(
			'1'=>'右1', '2'=>'右2', '3'=>'右3', '4'=>'M上', '5'=>'M中', '6'=>'M下', '7'=>'左U', '8'=>'左C', '9'=>'左D'
		);
		
		return $sign_value[$key];
	}
	// 設定 Array hash
	private function array_hash($length=6, $set_arr=array() )
	{
		$my_array = array('a','A','b','B','c','C','d','D','e','E','f','F','g','G','h','H','i','I','j','J','k','K','l','L','m','M','n','N','o','O','p','P','q','Q','r','R','s','S','t','T','u','U','v','V','w','W','x','X','y','Y','z','Z','0','1','2','3','4','5','6','7','8','9');
		
		if(!empty($set_arr)){
			$my_array = array_merge($my_array, $set_arr);
		}
		
		shuffle($my_array);
		$hash = array_slice($my_array, 0, $length);
		return implode('', $hash);
	}
	
	/*
	//製作數位簽章
	*/
	public function sign_key($pin=null)
	{
		$random_id = empty($pin) ? random_int(0, 9) : $pin;
		
		$_config = $this->sign_config($random_id);
		$set_arr = array('^', '+', '~', '@', '#', '%', '&', '*', '!');
		
		foreach($_config as $k=>$v){
			$_key[] = $this->array_hash(3, $set_arr) . $k . $this->array_hash(2, $set_arr) . $random_id;
		}
		
		return $_key;
	}
	
	/*
	//簽章編碼
	*/
	public function sign_encode($sign_key = array() )
	{
		foreach($sign_key as $v){
			$_key[] = $this->sign_value($v);
		}
		
		$private_key = implode('', $_key);
		
		//加密編碼 
		$enAES = $this->mcrypt_en($private_key); 
		
		return $enAES;
	}
	
	/*
	//簽章解碼
	*/
	public function sign_parser($key = null, $pin = 0)
	{
		$get_arr = explode(",", $key);
		$_config = $this->sign_config($pin);
		
		foreach($get_arr as $v){
			$key_v = substr($v, 3);
			$key_v = substr($key_v, 0, 1);
			$_key[] = $_config[$key_v];
		}
		
		return $this->sign_encode($_key);
	}
	public function sign_decode($sign_key = null)
	{
		//解密編碼
		$deAES = $this->mcrypt_de($sign_key);
		
		return $deAES;
	}
 
}

?>
