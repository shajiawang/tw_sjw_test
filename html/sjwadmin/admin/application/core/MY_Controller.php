<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public $data = array();
	public $media_path;

    public function __construct()
    {
        parent::__construct();

        $this->session_id = session_id();
        $this->site = 'back'; //'vendor_back';
		$this->kind = 'back'; //'is_vendor';
		
		$this->_company = array();
		$this->media_path = dirname(FCPATH) ."/uploadfiles/";

		$this->load->library(array(
			'layout',
			'myaes',
			//'mycrypt','mycaptcha',
		));

        $this->load->helper(array(
            'captcha',
            'common',
        ));

        /* 載入 models\sys\ 模組 */
		$this->load->model(array(
            'sys/setting_model',
            'sys/menu_model',
            'sys/user_model',
            'sys/user_group_model',
            'sys/user_group_auth_model',
        ));
		
        // 設定登入
        $this->initFlags();

        // 選單
        $this->data['_LOCATION'] = array();
		$this->initMenu();
		
		// 用戶參數
		$this->data['user_info'] = array('edit'=>FALSE
			, 'user_id'=>null
		);
		
		//允許改密碼
		$this->data['chg_pwd'] = FALSE;

        // 目前位置
		if ($this->flags->is_login === TRUE)
		{
			$this->data['user_info']['user_id'] = $this->flags->user['id'];
			
			if($this->flags->user['kind']=='is_vendor' && $this->flags->user['is_admin']==0){
				$this->data['user_info']['edit'] = TRUE;
			}
			
			$this->setMenu();
            $this->setMenuLocation();
        }

        // 網站參數
        $this->data['_SETTING'] = $this->initSetting();

        // 傳送 josn 給 JavaScrip 使用
        $this->data['_JSON'] = $this->initJson();
		
        // 提示訊息
        $this->initAlert();
        $this->setAlert();
        $this->setJson('_ALERT', $this->data['_ALERT']);

        // CSRF Token
        $this->data['csrf'] = array(
            'name' => $this->security->get_csrf_token_name(),
            'hash' => $this->security->get_csrf_hash()
        );

        $this->initJson();
        $this->setJson('_ALERT', $this->data['_ALERT']);

        // META 參數設定
        $this->data['page_title'] = '後台管理專區';
        $this->data['page_description'] = '';
        $this->data['page_keywords'] = '';

        // 靜態檔案網址
        $this->data['static_css'] = STATIC_URL ."css/";
		$this->data['static_js'] = STATIC_URL ."js/";
		$this->data['static_img'] = STATIC_URL ."img/";
		$this->data['static_fonts'] = STATIC_URL ."fonts/";
		$this->data['static_plugin'] = STATIC_URL ."plugin/";
		$this->data['media_url'] = MEDIA_URL;

        $this->data['filter'] = array(
        );
        if ($this->input->get()) {
            $this->data['filter'] = $this->input->get();
        }

        // 資料筆數 10 => 10, 20 => 20,
        $this->db_rows = 30;
		$this->data['choices']['rows'] = array(
			10 => 10,
			20 => 20,
            30 => 30,
            50 => 50,
            100 => 100,
        );
        if (empty($this->data['filter']['rows'])) {
            $this->data['filter']['rows'] = 30;
            
			if ($this->input->cookie('rows')) {
                $this->data['filter']['rows'] = $this->input->cookie('rows');
            }
        } else {
            setcookie('rows',$this->data['filter']['rows'] , time()+86400);  // exist by 7 days
        }
    }
	
	public function debug()
	{
		// $this->load->library(array('debug_cookie'));
		// $this->debug_cookie->params_to_cookie();
	
		// switch profiler;
		$this->output->enable_profiler(TRUE);

		// ajax
		if ($this->input->is_ajax_request()) {
			$this->output->enable_profiler(FALSE);
		}

		// Command-line interface
		if (is_cli()) {
			$this->output->enable_profiler(FALSE);
		}
	}


    /********************************
     * Flags data setting.
     ********************************/
    private function initFlags()
    {
		$this->flags = new stdClass;
		$this->flags->is_login = FALSE;
		$this->flags->user = NULL;
		
		if (isset($_SESSION['token'])) {
			$this->initToken();
		} 
		
		return $this;
    }
	
	private function initToken()
	{
		$de = $this->myaes->mcrypt_de($_SESSION['token']);
		$conditions = array(
			'token' => $de,
		);
		$user = $this->user_model->get($conditions);
		
		unset($user['password']);
		unset($user['token']);
		
		if ($user)
		{
			$this->flags->is_login = TRUE;
			$this->flags->user = $user;
			

			//選單權限錯誤!!
			$group_arr = explode(",", $user['user_group_id']);
			$this->flags->permission = $this->user_group_auth_model->getByGroupStr($group_arr);
		}
	}

    public function setFlags($user)
    {
		$token = $this->login_model->setAdminToken($user['id']);
		$_SESSION['token'] = $this->myaes->mcrypt_en($token);
		
		return $this;
    }

    public function getToken($user)
    {
        return sha1($user['id'] . $this->session_id . time());
    }

    /********************************
     * Alert function.
     * param kind [int]
     * 0 => gary,
     * 1 => Green,
     * 2 => Blue,
     * 3 => Yellow,
     * 4 => Red
     ********************************/
    private function initAlert()
    {
        $this->data['_ALERT'] = array();
		
		$alert = isset($_SESSION['_ALERT'])? $_SESSION['_ALERT'] : array();
		if (isset($alert['message']) && $alert['message']) {
			$this->data['_ALERT']['kind'] = $alert['kind'];
			$this->data['_ALERT']['message'] = $alert['message'];
			$this->data['_ALERT']['sec'] = $alert['sec'];
			$this->data['_ALERT']['layout'] = $alert['layout'];
			$_SESSION['_ALERT'] = array();
		}
    }

    protected function setAlert($kind=0, $message=NULL, $sec=5 ,$layout='center')
    {
		$data = array(
			'kind' => $kind,
			'message' => $message,
			'sec' => $sec,
			'layout' => $layout,
		);
		$_SESSION['_ALERT'] = $data;
    }

    /********************************
     * Menu functions.
     ********************************/
    private function initMenu()
    {
        $this->data['_MENU'] = array();
        $this->data['menu_catalog'] = array();
        $this->data['menu_function'] = array();
        $this->data['menu_current'] = array();
        return $this;
    }

    private function setMenu()
    {
		$get_MENU = $this->menu_model->getSidebarByPort($this->kind);
		
		$this->data['_MENU'] = $get_MENU;
    }
	
	private function setMenuLocation()
    {
        $this->data['_LOCATION'] = $this->menu_model->getLocation($this->kind);

		return $this;
    }
	
	private function set_special_menu($get_MENU, $exhibit_id=0)
    {
		$this->load->model('product/category_model');
		
		$condition = array('enable'=>1, 
			'category_id' => 5,
			'exhibit_id' => $exhibit_id
		);
		
		$category = $this->category_model->getRowByCategory($condition);
		
		foreach($get_MENU as $id=>$m)
		{
			$_MENU[$id] = $m;
			
			if($m['link'] == 'comexpo' )
			{
				foreach($m['sub'] as $subid=>$subv)
				{
					$_MENU[$id]['sub'][$subid] = $subv; 
					
					if($subv['link'] == 'comexpo/com_goods' )
					{
						$sub_id = $subid;
						foreach($category as $cat)
						{
							$_MENU[$id]['sub'][$sub_id]['id']= $subv['id'];
							$_MENU[$id]['sub'][$sub_id]['icon']= $subv['icon'];
							$_MENU[$id]['sub'][$sub_id]['name']= $cat['name'];
							$_MENU[$id]['sub'][$sub_id]['link']= $subv['link'] .'/?scat='. $cat['special_cat'] .'&cat='. $cat['id'] .'&aid='. $exhibit_id;
							$_MENU[$id]['sub'][$sub_id]['auth']= $subv['auth'];
							$sub_id = $sub_id + 1;
						}
					}
				}
			}
		}
		
		return $_MENU;
	}
    

    /********************************
     * Setting functions.
     ********************************/
    private function initSetting()
    {
        $data = array();
		$conditions = array( 'kind' => $this->site );
        $settings = $this->setting_model->getChoices(); //$conditions
        if ($settings) {
            $data = $settings;
        }
        return $data;
    }
	

    /********************************
     * JSON functions.
     ********************************/
    private function initJson()
    {
        return array();
    }

    protected function setJson($key, $val)
    {
        $this->data['_JSON'][$key] = $val;

        return $this;
    }

    protected function getJson($key)
    {
        if (isset($this->data['_JSON'][$key]))
        {
            return $this->data['_JSON'][$key];
        }

        return NULL;
    }

    protected function delJson($key)
    {
        unset($this->data['_JSON'][$key]);

        return $this;
    }

    /********************************
     * Captcha functions.
     ********************************/
    public function getCaptchaImage($config=array())
    {
        $default = array(
            'word'          => generatorRandom(6),
            'img_path'      => $this->media_path . 'captcha/',
            'img_url'       => $this->data['media_url'] . 'captcha/',
            'font_path'     => DIR_FONTS . 'MONACO.ttf',
            'img_width'     => 155,
            'img_height'    => 36,
            'expiration'    => 7200,
            // 'word_length'   => 4,
            'font_size'     => 16,
            // 'img_id'        => 'Imageid',
            // 'pool'          => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',

            // White background and border, black text and red grid
            'colors'        => array(
                'background' => array(33, 40, 47),
                'border' => array(33, 40, 47),
                'text' => array(255,255,255),
                'grid' => array(
                    array(252,148,148),
                    array(241,252,35),
                    array(181,252,35),
                    array(35,252,252),
                    array(35,169,252),
                    array(158,35,252),
                    array(252,35,241),
                ),
            )
        );

        $config = array_merge($default, $config);

        $cap = create_captcha($config);
		$_SESSION['captcha_code'] = $cap['word'];

        return $cap['image'];
    }

    public function validCaptcha($code)
    {
		$captcha_code = $_SESSION['captcha_code'];
		return ($captcha_code == $code);
    }

    /********************************
     * Change Query String params
     ********************************/
    public function getQueryString($merge_params=array(), $skip_params=array())
    {
        $filter = $this->data['filter'];
        $filter = array_merge($filter, $merge_params);

        foreach ($skip_params as $param) {
            if (isset($filter[$param])) {
                unset($filter[$param]);
            }
        }

        return http_build_query($filter);
    }
	
	/********************************
	 * Upload image
	 ********************************/
	protected function upload_image($image=NULL, $relative_dir=NULL, $thumb_width=200, $thumb_height=200)
	{
		if ($image && $relative_dir)
		{
			$file_info = pathinfo($image['name']);
			$new_name = generatorRandom(12) .'.'. strtolower($file_info['extension']);

			$path = $this->media_path . $relative_dir;
			$source_image = $path . $new_name;

			if (!is_dir($path)) {
				mkdir($path, 0777, true);
			}

			if (isset($image['tmp_name'])) {
				move_uploaded_file($image['tmp_name'], $source_image);
			}

			$this->load->library('image_lib');
			$this->image_lib->clear();
			$config = array(
				'source_image' => $source_image,
				'new_image' => $source_image,
				'wguidth' => $thumb_width,
				'height' => $thumb_height,
				'image_library' => 'gd2',
				'create_thumb' => TRUE,
				'maintain_ratio' => TRUE,
				'master_dim' => 'auto',
				'thumb_marker' => '_thumb',
			);

			$this->image_lib->initialize($config);

			$result = array(
				'status' => FALSE,
				'path' => $path,
				'relative_dir' => $relative_dir,
				'new_name' => $new_name,
				'origin_name' => $file_info['basename'],
				'extension' => $file_info['extension'],
				'filename' =>  $file_info['filename'],
			);
			if ($this->image_lib->resize()) {
				$result['status'] = TRUE;
			}

		} else {
			$result = array(
				'status' => FALSE,
				'error' => 'image and relative_dir is NULL',
			);
		}

		return $result;
	}
	
	protected function copy_image($image=NULL, $relative_dir=NULL, $thumb_width=200, $thumb_height=200)
	{
		if ($image && $relative_dir)
		{
			$origin_image = $this->media_path . $image;
			$file_info = pathinfo($image);
			$path = $this->media_path . $relative_dir;
			
			$new_name = $relative_dir . $file_info['basename'];
			$new_image = $this->media_path . $new_name;

			if (!is_dir($path)) {
				mkdir($path, 0777, true);
			}
			if (! empty($image)) {
				copy($origin_image, $new_image);
			}
			
			$this->load->library('image_lib');
			$this->image_lib->clear();
			$config = array(
				'source_image' => $new_image,
				'new_image' => $new_image,
				'wguidth' => $thumb_width,
				'height' => $thumb_height,
				'image_library' => 'gd2',
				'create_thumb' => TRUE,
				'maintain_ratio' => TRUE,
				'master_dim' => 'auto',
				'thumb_marker' => '_thumb',
			);

			$this->image_lib->initialize($config);

			$result = array(
				'status' => FALSE,
				'path' => $path,
				'relative_dir' => $relative_dir,
				'new_name' => $new_name,
				'origin_name' => $file_info['basename'],
				'extension' => $file_info['extension'],
				'filename' =>  $file_info['filename'],
			);
			if ($this->image_lib->resize()) {
				$result['status'] = TRUE;
			}

		} else {
			$result = array(
				'status' => FALSE,
				'error' => 'image and relative_dir is NULL',
			);
		}
		
		return $result;
	}

}
