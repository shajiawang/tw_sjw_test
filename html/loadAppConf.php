<?php
   $LANG=$_REQUEST['lang'];
   $arr=array();
   if(empty($LANG) || $LANG=='zh_CN') {
      // {channelid, Cname, Ename, Longitude, Latitude}
      $area=array(
	   array("1","上海","Shanghai","121.26","31.12")
	  ,array("2","北京","Beijing","116.28","39.54")
	  ,array("52","广州","Guangzhou","113.18","23.10")
	  ,array("37","重庆","Chungking","106.33","29.33")
	  ,array("48","深圳","Shenzhen","114.05","22.32")
	  ,array("57","成都","Chengdu","104.04","30.39")
	  );
   } else if($LANG=='zh_TW') {
      $area=array(
       array("1","上海","Shanghai","121.26","31.12")
	  ,array("2","北京","Beijing","116.28","39.54")
	  ,array("52","廣州","Guangzhou","113.18","23.10")
	  ,array("37","重慶","Chungking","106.33","29.33")
	  ,array("48","深圳","Shenzhen","114.05","22.32")
	  ,array("57","成都","Chengdu","104.04","30.39")
	  );
   }
/*   
   手机数码
家用电器
服饰内衣、鞋靴童装
家居家纺、锅具餐具
运动健康
箱包、珠宝饰品、手表
美容护理
名牌精品
家具家装
票券积分
游戏电玩
休闲旅游
房屋土地
餐饮美食
交通
其他
台湾馆
*/
   $arr['areaList']=$area;
   $arr['showNeighborIcon']='N';
   
   echo json_encode($arr);
?>