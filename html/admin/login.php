<?php

    if($_SERVER['SERVER_NAME']!='mgr.saja.com.tw' && $_SERVER['SERVER_NAME']!='test.shajiawang.com' && $_SERVER['SERVER_NAME']!='test.saja.com.tw') {
	   header("Location: https://mgr.saja.com.tw/admin/login.php");
       exit;	   
	}
    require_once '/var/www/html/admin/libs/helpers.php';
    require_once '/var/www/html/admin/phpmodule/include/config.ini.php';

    $arrAllowIP=['61.228.133.217','210.61.148.35','60.251.120.84','60.251.120.81','211.75.238.37','211.75.238.38','211.75.238.39','211.75.238.40','211.75.238.41','211.75.238.42','150.116.207.59','1.162.45.159'];
    $ip=GetIP();
    if(!in_array($ip,$arrAllowIP)) {
           die("Your IP : ".$ip." is not authorized to login to admin system ,<br>Please connect via. VPN ~");
           exit();
    }
	
    session_destroy();
	
    session_start();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <!--script type="text/javascript" src="http://code.jquery.com/jquery-1.9.1.min.js"></script -->
		<script type="text/javascript" src="/admin/js/jquery.min.js"></script>
	    <!-- script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script -->
		<script type="text/javascript" src="/admin/js/jquery-ui.js"></script>
	    <!-- script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.13.0/jquery.validate.min.js"></script -->
		<script type="text/javascript" src="/admin/js/jquery.validate.min.js"></script>
	    <!-- script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery.Validate/1.6/localization/messages_tw.js"></script -->
	    <script type="text/javascript" src="/admin/js/messages_tw.js"></script>
		<!--<link href="css/misa.css" rel="stylesheet" type="text/css" />-->
	    <!-- link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/le-frog/jquery-ui.css" type="text/css" -->
	    
	    
	    
		<link rel="stylesheet" href="/admin/css/jquery-ui.css" type="text/css" >
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="/admin/css/esStyle/es-default.css">
	</head>
	<body>
		<style>
            body {
                background-image: url(images/admin/admin-bg.jpg);
                background-repeat: no-repeat;
                background-attachment: fixed;
                background-position: center;
                background-size: cover;
            }
            .fixed_box {
                position: fixed;
/*
                top: 0;
                bottom: 0;
*/
                right: 0;
                left: 0;
            }
            .login_box {
                width: 20.834rem;
                background: #FFF;
                margin: 2rem;
                margin-top: 8rem;
                overflow: hidden;
            }
            .login_header {
                border: 1px solid #009688;
                background: #009688;
                color: #ffffff;
                font-weight: bold;
                min-height: .3rem;
/*                height: 3.334rem;*/   /*header 有標題時使用*/
            }
            .login_box .input_box {
                margin: 2rem;
            }
            .login_box .input_box label {
                font-weight: 600;
            }
            .input-group>.input-group-text:last-child{
                border-top-left-radius: 0;
                border-bottom-left-radius: 0;
            }
            .gcolor {
                color: #009688;
            }
            .submit-btn {
                background: #009688;
                color: #FFF;
                margin-top: 2rem;
            }
            .submit-btn:hover {
                background: #007d71;
                color: #FFF;
            }
            .submit-btn:active {
                opacity: .8;
            }
			label.error {
				color: red;
			}
		</style>
		
		<script language="javascript" type="text/javascript">
		//<!--
//		$(document).ready(function() {
//		   $("button, input:button, input:submit").button();
//		   $("#LoginForm").validate({
//		      submitHandler: function (form)
//		      {
//		         $("button, input:button, input:submit").button({ disabled: true });
//		         form.submit();
//		      }
//		   });
//			$("#LoginConfirm").click(function(){
//				$("#LoginForm").submit();
//			});
//		   $("#Reset").click(function(){
//		      $("#LoginForm").each(function(){
//		         this.reset();
//		      });
//		      $("#LoginForm").validate().resetForm();
//		      $("input,select").removeClass("ui-state-error");
//		   });
//			});
		//-->
            $(function(){
                $("#LoginConfirm").on('click',function(){
                    var $name = $('#name').val();
                    var $password = $('#passwd').val();
                    if($name == '' || $password == ''){
                        alert('欄位不得為空')
                        location.reload();
                    }else{
                        console.log($name,$password);
                        $('#LoginForm').submit();
                    }
                });
                $('.password-btn').on('click', function(){
                    var $that = $(this);
                    var $icon = $that.find('.fas');
                    var $passinput = $('#passwd');
                    if($icon.hasClass('fa-eye-slash')){
                        $passinput.attr('type','text');
                        $icon.addClass('fa-eye');
                        $icon.removeClass('fa-eye-slash');
                    }else{
                        $passinput.attr('type','password');
                        $icon.removeClass('fa-eye');
                        $icon.addClass('fa-eye-slash');
                    }
                })
            })
		</script>
		<form action="/admin/user_login.php" method="post" name="LoginForm" id="LoginForm">
           <div class="fixed_box d-flex justify-content-center">
                <div class="login_box shadow rounded">
                    <div class="login_header d-flex align-items-center justify-content-center"></div>
                    <div class="input_box">
                        <div class="form-group">
                            <label for="name">管理者帳號：</label>
                            <input type="text" name="name" id="name" class="required form-control" minlength="4" maxlength="20">
                        </div>
                        <div class="form-group">
                            <label for="passwd">管理者密碼：</label>
                            <div class="input-group">
                                <input type="password" name="passwd" id="passwd" class="required form-control" minlength="4" maxlength="16">
                                <span class="input-group-text password-btn d-flex align-items-center justify-content-center">
                                    <i class="fas fa-eye-slash gcolor"></i>
                                </span>
                            </div>
                        </div>
                        <button type="button" id="LoginConfirm" name="LoginConfirm" class="submit-btn btn btn-lg btn-block">登入</button>
                    </div>
                </div>
           </div>
           
           
<!--
            <table class="ui-widget" align="center">
                <tbody>
                    <tr>
                        <td valign="top" width="250">
                        <table border="0" cellspacing="0" cellpadding="0" align="right">
                            <tbody>
                                <tr>
                                    <td height="274">
                                    <table border="0" cellspacing="0" cellpadding="0" width="234">
                                        <tbody>
                                            <tr>
                                                <td height="40" class="ui-widget-header"><div align="center"></div></td>
                                            </tr>
                                            <tr>
                                                <td height="180" style="border: 1px solid #72b42d;">
                                                <table border="0" cellspacing="0" cellpadding="5" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td valign="bottom"><label for="name">管理者帳號：</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:215px;" class="ui-state-error-text"><input type="text" name="name" id="name" class="required" style="width:210px;" minlength="4" maxlength="20" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td valign="bottom"><label for="passwd">管理者密碼：</label></td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width:215px;" class="ui-state-error-text"><input type="password" name="passwd" id="passwd" class="required" style="width:210px;" minlength="4" maxlength="16" /></td>
                                                        </tr>
                                                        <tr>
                                                            <td align="center">
                                                                <input type="button" id="LoginConfirm" name="LoginConfirm" alt="登入" title="登入" value="登入" />
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        </td>
                        <td valign="top" width="30"></td>
                        <td valign="top" width="723">
                        <table border="0" cellspacing="0" cellpadding="0" width="440">
                            <tbody>
                                <tr><td height="15"></td></tr>
                                <tr><td><img width="640" height="320" alt="形象" title="形象" src="/admin/images/admin/banner-1.jpg" /></td></tr>
                            </tbody>
                        </table>
                        </td>
                    </tr>
                </tbody>
            </table>
-->
        </form>
	</body>
</html>


