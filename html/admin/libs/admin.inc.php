﻿<?php
	class intputOutput {
		public $input = array();
		function __construct() {
			if(isset($_POST)) {
				$this->input["post"] = (ini_get('magic_quotes_gpc')) ? $_POST : $this->stripslashesArr($_POST);
			}
			if(isset($_GET)) {
				$this->input["get"]	= (ini_get('magic_quotes_gpc')) ? $_GET : $this->stripslashesArr($_GET);
			}
			if(isset($_COOKIE)) {
				$this->input["cookie"] = (ini_get('magic_quotes_gpc')) ? $_COOKIE : $this->stripslashesArr($_COOKIE);
			}
			if(isset($_SESSION)) {
				$this->input["session"]	= (ini_get('magic_quotes_gpc')) ? $_SESSION: $this->stripslashesArr($_SESSION);
			}
			if(isset($_SERVER)) {
				$this->input["server"] = (ini_get('magic_quotes_gpc')) ? $_SERVER  :  $this->stripslashesArr($_SERVER);
			}
			if(isset($_FILES)){
				$this->input["files"] = (ini_get('magic_quotes_gpc')) ? $_FILES : $this->stripslashesArr($_FILES);
			}
		}
		
		function stripslashesArr($var){
			$output = array();
			if(is_array($var)) {
	  			foreach ($var as $key => $value) {
					if(is_array($value)) {
						foreach($value as $key2 => $value2){
							if(is_array($value2)) {
								foreach($value2 as $key3 => $value3){
	    							$output[$key][$key2][$key3] = addslashes($value3);
								}
							}
							else{
	    						$output[$key][$key2] = addslashes($value2);
							}
						}
					}
					else {
	    				$output[$key] = addslashes($value);
					}
				}
			}
			return $output;
		}
	}
	
	function jsAlertMsg($msg){
		echo "
			<html>
			<head>
			<script type=\"text/javascript\">
			<!--
				alert('".$msg."');
				history.back(-1);
			-->	
			</script>
			</head>
			</html>
		";
		exit ; 

	}
?>