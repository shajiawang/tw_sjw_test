<?php
  define("REDIS_SERVER",'127.0.0.1');
  function getUserIP()
  {
      // Get real visitor IP behind CloudFlare network
      if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
                $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
                $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
      }
      $client  = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote  = $_SERVER['REMOTE_ADDR'];

      if(filter_var($client, FILTER_VALIDATE_IP))
      {
          $ip = $client;
      }
      elseif(filter_var($forward, FILTER_VALIDATE_IP))
      {
          $ip = $forward;
      }
      else
      {
          $ip = $remote;
      }
      if (inet_pton($ip)){
        return $ip;
      }else{
        error_log('[/libs/helpers] getUserIP fail: {$ip}');
        return -1;
      }
  }

  function getRedisList($listtype){
 //Connecting to Redis server on localhost
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    return $redis->keys("{$listtype}_*");
  }

  function setRedisList($listtype,$ip,$acttype=''){
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    //store data in redis list
    return $redis->set("{$listtype}_{$ip}", "{$acttype}");
  }
  function removeRedisList($listtype,$ip,$acttype=''){
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    //store data in redis list
    return $redis->del("{$listtype}_{$ip}");
  }
  function inlistRedistList($listtype,$ip){
    $redis = new Redis();
    $redis->connect(REDIS_SERVER, 6379);
    //store data in redis list
    return $redis->exists("{$listtype}_{$ip}");
  }

  //對接到 與內政部對接的主機走內部ip不驗簽之後加鎖ip
  function id_verify($personId,$applyYyymmdd,$applyCode,$issueSiteId){
        $fields = array(
          'personId'  => $personId,
          'applyYyymmdd'    => $applyYyymmdd,
          'applyCode'        => $applyCode,
          'issueSiteId'        => $issueSiteId
        );
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://api.saja.com.tw/ris/' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, ( $fields ) );
        $result = curl_exec( $ch );
        curl_close( $ch );
        return $result;
  }
    // 6碼數字亂數
	function get_shuffle($num='', $eword='')
	{
		// if ($num > $eword){
			// $v1 = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
			// $v2 = array('0','1','2','3','4','5','6','7','8','9');
			// shuffle($v);
			// /*
			// $rand = time() * rand(1,9);
			// $rand1 = substr($rand, -6, 2);
			// $rand2 = substr($rand, -4, 2);
			// $rand3 = substr($rand, -2);

			// $code = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $rand3;
			// */
			// $code = $v[2].$v[7].$v[3].$v[8].$v[0].$v[9];
		// }else{
			// $v = array('0','1','2','3','4','5','6','7','8','9');
			// shuffle($v);
			// $code = $v[2].$v[7].$v[3].$v[8].$v[0].$v[9];
		// }
		$v1 = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
		$v2 = array('0','1','2','3','4','5','6','7','8','9');
		shuffle($v1);
		shuffle($v2);

		if (($num > $eword) && ($num > 0) && ($eword > 0)){
			//取得英文字
			for($i=0; $i<($eword); ++$i) {
				$c1[$i]= strtolower($v1[$i]);
			}
			//取得數字
			for($k=0; $k<($num-$eword); ++$k) {
				$c1[$k+$eword]= $v2[$k];
			}

			shuffle($c1);
			//組合字串
			for($c=0; $c<count($c1);$c++){
				$code .= $c1[$c];
			}

		} else {

			shuffle($v2);
			$code = $v1[2].$v2[7].$v2[3].$v2[6].$v2[0].$v2[9];

		}

		return $code;
	}

	function GetIP(){
        if(!empty($_SERVER["HTTP_CLIENT_IP"])){
            $cip = $_SERVER["HTTP_CLIENT_IP"];
        }
        elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
            $temp_ip = explode(",", $_SERVER['HTTP_X_FORWARDED_FOR']);
            $cip = $temp_ip[0];
        }
        elseif(!empty($_SERVER["REMOTE_ADDR"])){
            $cip = $_SERVER["REMOTE_ADDR"];
        }
        else{
            $cip = "unknown";
        }
        error_log("[libs/helpers]IP : ".$cip);
        return $cip;
    }

    // 呼叫系統 s3cmd 套件 將檔案sync到hicloud S3
    // 須先安裝 s3cmd (見 http://s3help.cloudbox.hinet.net/index.php/2015-02-12-07-08-03)
    // 並修改 .s3cfg 的signature_v2=True
    function syncToS3($FileRealPath, $BucketName='', $BucketDir='') {
        if(empty($FileRealPath)) {
           error_log("[admin/libs/helpers] empty FileRealPath !!");
           return false;
        }
        if(!file_exists($FileRealPath)) {
           error_log("[admin/libs/helpers] File not exists !!");
           return false;
        }
        // Bucket 預設是hicloud上的S3
        if(empty($BucketName)) {
           $BucketName='s3://img.saja.com.tw';
        }
        // Bucket目錄須以"/"結尾  否則會被當成檔案
        // 預設是商品目錄
        if(empty($BucketDir)) {
           $BucketDir='/site/images/site/product/';
        }
        $ret = shell_exec("s3cmd put --acl-public ${FileRealPath} ${BucketName}${BucketDir} ");
        error_log("[admin/libs/helpers] s3cmd put --acl-public ${FileRealPath} ${BucketName}${BucketDir} ...".$ret);
        return true;
    }

	function getUrlProtocol() {
	        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https" : "http";
    	    return $protocol;
    }

    function postReq($url='', $arrPost='') {
           $ret = array();
           if(empty($url)) {
              $ret['retCode']=-1;
              $ret['retMsg']="NO_URL";
              return $ret;
           }
            if(empty($arrPost)) {
              $ret['retCode']=-2;
              $ret['retMsg']="NO_DATA";
              return $ret;
           }
           error_log("[admin/libs/helpers/postReq] postdata : ".json_encode($arrPost));
           $stream_options = array(
                'http' => array (
                        'method' => "POST",
                        'content' => json_encode($arrPost),
                        'header' => "Content-Type:application/json"
                )
            );
            $context  = stream_context_create($stream_options);
            // 送出json內容並取回結果
            $response = file_get_contents($url, false, $context);
            error_log("[admin/libs/helpers] response : ".$response);

            // 讀取json內容
            $arr = json_decode($response,true);
            return $arr;
    }

	// refer : https://codertw.com/%E7%A8%8B%E5%BC%8F%E8%AA%9E%E8%A8%80/214361/
	// 要安裝一個php模組mcrypt
	//加密
	function md5_crypt($str, $salt="Sjw#333") {
		$td = mcrypt_module_open(MCRYPT_DES,'','ecb','');
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		$ks = mcrypt_enc_get_key_size($td);
		$key = substr(md5($salt), 0, $ks);
		mcrypt_generic_init($td, $key, $iv);
		$secret = mcrypt_generic($td, $str);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return $secret;
	}

	//解密
	function md5_decrypt($sec, $salt="Sjw#333") {
		$td = mcrypt_module_open(MCRYPT_DES,'','ecb','');
		$iv = mcrypt_create_iv(mcrypt_enc_get_iv_size($td), MCRYPT_RAND);
		$ks = mcrypt_enc_get_key_size($td);
		$key = substr(md5($salt), 0, $ks);
		mcrypt_generic_init($td, $key, $iv);
		$string = mdecrypt_generic($td, $sec);
		mcrypt_generic_deinit($td);
		mcrypt_module_close($td);
		return trim($string);
	}

  define('ES_SALT','sJw#333');
  function encrypt($code)
  {
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5(ES_SALT), $code, MCRYPT_MODE_CBC, md5(md5(ES_SALT))));
  }
  /**
  * 解密
  * @param [type] $code [description]
  * @return [type]  [description]
  */
  function decrypt($code)
  {
    return rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5(ES_SALT), base64_decode($code), MCRYPT_MODE_CBC, md5(md5(ES_SALT))), "12");
  }

    // 取得用戶設備token (for 推播)
	function get_token($userid=''){
		global $db, $config;

		$db = new mysql($config["db"]);
		$db->connect();

		$where = "";
		if ($userid != "") {
			$where = "AND userid in ({$userid})";
		}
		$query = "SELECT udk.token
			FROM saja_user.saja_user_device_token udk
			WHERE switch = 'Y' ".$where;

		$table = $db->getQueryRecord($query);

		if(!empty($table['table']['record'])){
			$token = array();
			foreach ($table['table']['record'] as $key => $value) {
				$token[]=$value['token'];
			}
			$token = array_chunk($token, 1000);
			return $token;
		}else{
			return false;
		}
	}


    // 推播訊息給用戶
    function push_notify($userid='', $title='', $body='', $action="9", $productid="0", $action="0", $url_title="", $url="") {
             if(empty($userid) || empty($title) || empty($title)) {
				 error_log("[admin/libs/helpers/push_notify] : cannot find token of userid : ".$userid);
				 return false;
			 }

			 $ids=get_token($userid);
			 if(empty($ids)) {
				error_log("[admin/libs/helpers/push_notify] : cannot find token of userid : ".$userid);
				return false;
			 }
			 $msg = array(
				'title'		=> $title,
				'body' 		=> $body,
				'vibrate'	=> 1,
				'sound'		=> 1
			 );

			 $action_list = array(
					'type'		=> $action,
					'url_title'	=> $url_title,
					'url'		=> $url,
					'imgurl'	=> '',
					'page'		=> $page,
					'productid'	=> $productid
			 );
			 $action_list = json_encode($action_list);

			 $data = array(
				'action'	=> $action,
				'productid'	=> $productid,
				'url_title'	=> $url_title,
				'url'		=> $url,
				'action_list'=> "{$action_list}"
			 );
			 fcm_broadcast_send($ids, $msg, $data);
    }

    /* firebase 推播傳送模組
			$title : 標題
			$body : 內文
			$ids: 接收者registrationIds[array]
			$vibrate : 振動(0:無/1:有)
			$sound : 題示音(0:無/1:有)
	*/
	function fcm_broadcast_send($ids='', $msg=array(), $data=array()) {

			global $db, $config;

			//設定 Action 相關參數
			set_status($this->controller);

			//參數未傳送返回false
			if(empty($ids)||empty($msg)||empty($data)){
				return FALSE;
			}

			// API access key from Google API's Console
			define( 'API_ACCESS_KEY', 'AAAAbCNeUVQ:APA91bGcY4z5iUZyWFxirakTqx6h5XR1-f9K-XMNuiUsUuqx9S2Js4Sk4KSywwlaUD4YjcAqIlLbkQQ289G_vI996dZDOU32Bu9XPGBxI7vWrHtOqTUg_rqP1iN0Nu4RxF2AswUfg93U' );

			$headers = array(
				'Authorization: key=' . API_ACCESS_KEY,
				'Content-Type: application/json'
			);


			foreach ($ids as $key => $value) {
				//接收者token $ids
				$fields = array(
					'registration_ids' 	=> $value,
					'notification'		=> $msg,
					'data'				=> $data
				);

				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				$result = curl_exec( $ch );
				curl_close( $ch );
			}
	}
/*

  $inp=array();
  $inp['title']="testtitle".time();
  $inp['body']="testbody";
  $inp['productid']="";
  $inp['action']="";
  $inp['url_title']="";
  $inp['url']="";
  $inp['sender']="5014";
  $inp['sendto']="5014";
  $inp['page']=0;
  $inp['target']='saja'
  $rtn=sendFCM($inp);

  title 推播標題 body 推播正文 productid 商品編號 action 動作 url_title 網頁標頭 url 連結網址 sender 發送者
  page 跳轉頁面 sendto 接收者（多人以,分隔）target token的來源預設為saja王
  以上 title body sender sendto  為必填之欄位
*/
function sendFCM($inp){
 global $db, $config;
 include_once "saja/mysql.ini.php";

  $db = new mysql($config['db'][0]);
  $db->connect();

  //過濾商品名稱內 單引號 雙引號 右斜線
  $push_title = $inp['title'];//$this->io->input["post"]["title"];
  $push_title = str_replace('\\', '', $push_title);
  $push_title = str_replace('\'', '', $push_title);
  $push_title = str_replace('"', '', $push_title);
  $target=(empty($inp["target"]))?'saja': $inp["target"];
  if (empty($inp["body"])) {
    $rtn['retCode']=-2;
    $rtn['retMsg']='推播內容不得留白';
  }else{
    $body = (empty($inp['body']))? "":$inp['body'];
    $productid = (empty($inp['productid']))? "":$inp['productid'];
    if ($productid=='') $productid=0;
    $action = (empty($inp['action']))? "":$inp['action'];
    $url_title= (empty($inp['url_title']))? "":$inp['url_title'];
    $url = (empty($inp['url']))? "":$inp['url'];
    $sendto = (empty($inp['sendto']))? "":$inp['sendto'];
    $page = (empty($inp['page']))? "":$inp['page'];
    $groupset= "";
    $rtn=array();
    if (empty($sendto)) {
      $rtn['retCode']=0;
      $rtn['retMsg']='未指定接收ID';
    }else{
      $sendtos=explode(",",$sendto);
      if ($inp['target']=='saja'){

        if (sizeof($sendtos)>1){
        $query = "SELECT token
          FROM saja_user.saja_user_device_token
          WHERE switch = 'Y'
          AND userid in ({$sendto})";
        }else{
        $query = "SELECT token
          FROM saja_user.saja_user_device_token
          WHERE switch = 'Y'
          AND userid = '{$sendto}' ";
        }
      }else{
        $query = "SELECT token
          FROM saja_user.saja_enterprise_device_token
          WHERE switch = 'Y'
          AND enterpriseid = '{$sendto}' ";
      }

      $table = $db->getQueryRecord($query);
      if (!empty($table["table"]["record"])) {
        $ids = array();
        foreach ($table["table"]["record"] as $key => $value) {
          $ids[]=$value['token'];
        }
        // 每批最多一千筆推播
        $ids = array_chunk($ids, 1000);

        // API access key from Google API's Console
        if ($inp['target']=='saja'){
          define( 'API_ACCESS_KEY', 'AAAAbCNeUVQ:APA91bGcY4z5iUZyWFxirakTqx6h5XR1-f9K-XMNuiUsUuqx9S2Js4Sk4KSywwlaUD4YjcAqIlLbkQQ289G_vI996dZDOU32Bu9XPGBxI7vWrHtOqTUg_rqP1iN0Nu4RxF2AswUfg93U' );
          $log_target='';
        }else{
          $log_target='enterprise_';
          //
          //for test api key
          define( 'API_ACCESS_KEY', 'AAAASBjySok:APA91bFvEjhLF3x3QiZl4SD5Z-37VzaK0ejorZe86FaHdxJa_gGsUAVlISWySFPvZ_GlnZUJiueuUcQbLK346yCd4xuIOVlkaDyaQk10-UuWumhPIER6mGn3oU67vJo707n5SrHkFl2B' );
        }
        //var_dump($log_target);
        //var_dump($ids);
        $action_list = array(
            'type'    => $action,
            'url_title' => $url_title,
            'url'   => $url,
            'imgurl'  => '',
            'page'    => $page,
            'productid' => $productid
          );
        $action_list = json_encode($action_list);

        $msg = array(
          'title'   => $push_title,
          'body'    => $body,
          'vibrate' => $vibrate,
          'sound'   => $sound
        );

        // 新舊規格一起傳
        if ($action == "4") {
          switch ($page) {
            case '1':
              $action = "saja";
              break;
            case '4':
              $action = "index";
              break;
            case '5':
              $action = "saja_history";
            break;
          }
        }

        $data = array(
          'action'  => $action,
          'productid' => $productid,
          'url_title' => $url_title,
          'url'   => $url,
          'action_list'=> "{$action_list}"
        );

        $headers = array(
          'Authorization: key=' . API_ACCESS_KEY,
          'Content-Type: application/json'
        );

        foreach ($ids as $key => $value) {
          //接收者token $ids
          $fields = array(
            'registration_ids'  => $value,
            'notification'    => $msg,
            'data'        => $data
          );

          $ch = curl_init();
          curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
          curl_setopt( $ch,CURLOPT_POST, true );
          curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
          curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
          curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
          curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
          $result = curl_exec( $ch );
          curl_close( $ch );
        }
        for ($i=0;$i<sizeof($sendtos);$i++){
          $query ="
          INSERT INTO `{$config['db'][2]['dbname']}`.`{$config['default_prefix']}push_{$log_target}msg_history`
          SET
            `title`     = '{$push_title}',
            `groupset`    = '{$groupset}',
            `body`      = '{$body}',
            `action`    = '{$inp["action"]}',
            `productid`   = {$productid},
            `url_title`   = '{$inp["url_title"]}',
            `url`     = '{$inp["url"]}',
            `page`      = '{$inp["page"]}',
            `sender`    = '{$inp["sender"]}',
            `sendto`    = '{$sendtos[$i]}',
            `sendtime`    = now(),
            `pushed`    = 'Y',
            `is_hand`       = 'N',
            `push_type`     = 'S',
            `insertt`   = now()
          " ;
          $db->query($query);
        }
        $rtn['retCode']=1;
        $rtn['retMsg']='finish';
      }else{
        $rtn['retCode']=-1;
        $rtn['retMsg']='指定ID不具有效token';
      }
    }
  }
  return $rtn;
}

/*
    $actid 為必要參數其餘視需調整
          $ary_bidder = array('actid'   => 'BID_WINNER',
                    'name'    => $winner_name,
                    'productid' => "{$product['productid']}",
                    'status'  => $status,
                    'thumbnail_file' => $thumbnail_file ,
                    'thumbnail_url' => $r['thumbnail_url']
                );
*/
function sendSocket($pt){
  //error_reporting(E_ALL); ini_set('display_errors', '1');
  require("/var/www/html/site/lib/vendor/autoload.php"); // 載入wss client 套件(composer)
  $ret=array();
  $ret['retCode']=0;
  try{
    $ary_bidder=$pt;
    $ary_bidder['ts']=time();
    $ary_bidder['sign']=MD5($ary_bidder['ts']."|sjW333-_@");
    $ts = time();
    $wss_url=(in_array(array('mgr.saja.com.tw','www.saja.com.tw','saja.com.tw'),$_SERVER['HTTP_HOST']))?'wss://ws.saja.com.tw:3334':'wss://test.saja.com.tw:3334';
    $client = new WebSocket\Client($wss_url);
    $client->send(json_encode($ary_bidder));
  }catch(Exception $e){
    $ret['retCode']=$e->getCode();
    $ret['retCode']=$e->getMessage();
  }finally{
    return $ret;
  }
}

function strEncode($data,$key,$encode_type = "crypt"){
	if($encode_type == "crypt"){
		$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		return urlencode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256,$key,$data,MCRYPT_MODE_ECB,$iv));
}
	elseif($encode_type == "md5"){
			return urlencode($data);
	}
	else{
			echo "Please write \$encode_type type to config.ini.php!!"; exit ;
	}
} 

function strDecode($data,$key,$encode_type = 'crypt'){
	if($encode_type == "crypt"){
			$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256 , MCRYPT_MODE_ECB);
			$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
			return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256,$key,urldecode($data),MCRYPT_MODE_ECB,$iv));
	}
	elseif($encode_type == "md5"){
			return urldecode($data);
	}
	else{
			echo "Please write \$encode_type type to config.ini.php file!!"; exit ;
	}

}
?>
