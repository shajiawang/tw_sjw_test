<?PHP

$debug_mod   = "Y";
//$host_name   = "localhost";
$host_name   = "sajadb01";
$host2_name   = "sajadb02";
$protocol    = "http";
$domain_name = 'test.shajiawang.com';

############################################################################
# defient
############################################################################
$config["project_id"]             = "saja"; 
$config["default_main"]           = "/admin"; 
$config["default_charset"]        = "UTF-8";
$config["sql_charset"]            = "utf8";
$config["default_prefix"]         = "saja_" ;  			// prefix
$config["default_prefix_id"]      = "saja" ;		// prefixid
$config["cookie_path"]            = "/";  
$config["cookie_domain"]          = "";  
$config["tpl_type"]               = "php"; 
$config["module_type"]            = "php";	  		// or xml
$config["fix_time_zone"]          = -8 ;                          // modify time zone 
$config["default_time_zone"]      = 8 ;                           // default time zone 
$config["max_page"]               = 20 ;
$config["max_range"]              = 10 ;
$config["encode_type"]            = "crypt" ; 			// crypt or md5
$config["encode_key"]             = "%^$#@%S_d_+!" ; 			// crypt encode key
$config["session_time"]           = 15	; 			// Session time out
$config["default_lang"]           = "zh_TW"	; 			// en , tc
$config["default_template"]       = "default"	; 		// 
$config["default_topn"]           = 10	; 			// default top n
$config["debug_mod"]              = $debug_mod ; 			// enable all the debug console , N : disable , Y : enable 
$config["max_upload_file_size"]   = 800000000 ; 			//unit is k
$config["expire"]                 = "60" ; 			// memcace expire time . sec
$config["domain_name"]            = $domain_name;
$config["protocol"]               = $protocol;
$config["admin_email"]            = array('pc028771@gmail.com') ; 
$config["currency_email"]         = array('pc028771@gmail.com') ; 
$config["transaction_time_limit"] = 5; //The minimized time between two transaction
############################################################################
# FaceBook
############################################################################
$config['fb']['appid' ]  = "";
$config['fb']['secret']  = "";


############################################################################
# path
############################################################################
$config["path_admin"]           = "/var/www/html/admin/phpmodule";
$config["path_class"]           = $config["path_admin"]."/class" ; 
$config["path_function"]        = $config["path_admin"]."/function" ; 
$config["path_include"]         = $config["path_admin"]."/include" ; 
$config["path_bin"]             = $config["path_admin"]."/bin" ; 
$config["path_data"]            = $config["path_admin"]."/data/" ; 
$config["path_cache"]           = $config["path_admin"]."/data/cache" ; 
$config["path_sources"]         = $config["path_admin"]."/sources/".$config["module_type"] ; 
$config["path_style"]           = $config["path_admin"]."/template/".$config["tpl_type"] ; 
$config["path_language"]        = $config["path_admin"]."/language" ; 
$config["path_images"]          = dirname($config["path_admin"])."/images" ; 
//$config["path_site_products_images"] = dirname(dirname($config["path_admin"]))."/images/products" ; 
$config["path_products_images"] = "/var/www/html/site/images/site/product" ; 
$config["path_ad_images"]       = "/var/www/html/site/images/site/ad" ; 
$config["path_lb_images"]     	= "/var/www/html/site/images/site/broadcast" ; 
$config["path_upload_images"]   = "/var/www/html/site/images/site/upload" ; 
$config["path_invoice_images"]   = "/var/www/html/site/images/site/invoice" ; 
$config["path_deposit_images"]   = "/var/www/html/site/images/site/deposit" ; 

$config["path_admins"]          = $config["path_admin"]."/admins/" ; 
$config["path_javascript"]      = $config["path_admin"]."/javascript/" ; 
$config["path_pdf_template"]    = $config["path_class"]."/tcpdf/template/" ; 
$config["path_eamil_api"]       = "/usr/bin/" ;  // email api path 

############################################################################
# sql Shaun 
############################################################################
$config["db"][0]["charset"]  = "utf8" ;
$config["db"][0]["host"]     = $host_name ;
$config["db"][0]["type"]     = "mysql";
#$config["db"][0]["username"] = "saja"; 
$config["db"][0]["username"] =  get_cfg_var('mysql.default_user'); 

#$config["db"][0]["password"] = "saja#333" ;
$config["db"][0]["password"] = get_cfg_var('mysql.default_password');

#$config["db_port"][0]       = "/var/lib/mysql/mysql.sock";

$config["db"][0]["dbname"]   = "saja_user" ;
$config["db"][1]["dbname"]   = "saja_cash_flow" ;
$config["db"][2]["dbname"]   = "saja_channel" ;
$config["db"][3]["dbname"]   = "saja_exchange" ;
$config["db"][4]["dbname"]   = "saja_shop" ;

############################################################################
# sql Shaun 2
############################################################################
$config["db2"][0]["charset"]  = "utf8" ;
$config["db2"][0]["host"]     = $host2_name ;
$config["db2"][0]["type"]     = "mysql";
$config["db2"][0]["username"] = "saja"; 
$config["db2"][0]["password"] = "saja#333" ;

$config["db2"][0]["dbname"]   = "saja_user" ;
$config["db2"][1]["dbname"]   = "saja_cash_flow" ;
$config["db2"][2]["dbname"]   = "saja_channel" ;
$config["db2"][3]["dbname"]   = "saja_exchange" ;
$config["db2"][4]["dbname"]   = "saja_shop" ;

############################################################################
# memcache 
############################################################################
// $config['memcache']['server_list']['session']['ip']	= 'localhost';
// $config['memcache']['server_list']['session']['port']	= 11211;
// $config['memcache']['MEM_PERSISTENT']		= true;
// $config['memcache']['MEM_TIMEOUT']		= 1;
// $config['memcache']['MEM_RETRY_INTERVAL']	= 1;
// $config['memcache']['MEM_STATUS']		= 1;
// $config['memcache']['MEM_WEIGHT']		= 1000;

############################################################################
# Payment 
############################################################################
$config['alipay']['merchantnumber'] = '456025';
$config['alipay']['code']           = 'abcd1234';
$config['alipay']['paymenttype']    = 'ALIPAY_WAP'; //'ALIPAY'; //
$config['alipay']['url_payment']    = 'http://testmaple2.neweb.com.tw/CashSystemFrontEnd/Payment';
$config['alipay']['url_query']      = 'http://testmaple2.neweb.com.tw/CashSystemFrontEnd/Query';
$config['alipay']['nexturl']		= BASE_URL . APP_DIR .'/deposit/alipay_complete/';
//$config['alipay']['htmlurl']		= BASE_URL . APP_DIR .'/deposit/alipay_html/';
$config['alipay']['htmlurl']		= BASE_URL . APP_DIR .'/lib/alipay/alipayapi.php';
$config['alipay']['sellemail']		= "alpay@shajiawang.com";


$config['bankcomm']['htmlurl']		= APP_DIR .'/lib/bankcomm/merchant.php';
$config['bankcomm']['interfaceVersion'] = '1.0.0.0';			//銀聯支付版本
$config['bankcomm']['merchID']		= '301310053119880';		//商戶號
$config['bankcomm']['tranCode']		= 'cb2200_sign';			//
$config['bankcomm']['tranType']		= '0';						//0:B2C 
$config['bankcomm']['curType']		= 'CNY';					//交易幣種
$config['bankcomm']['socket_ip']	= "127.0.0.1";				//socket服?ip
$config['bankcomm']['socket_port']	= "8080";					//socket服?端口
$config['bankcomm']['paymenttype']  = 'BANKCOMM_WAP'; 			//'BANKCOMM'; //
$config['bankcomm']['goodsURL']		= BASE_URL . APP_DIR .'/deposit/bankcomm_html/';
$config['bankcomm']['merURL']		= BASE_URL . APP_DIR .'/deposit/bankcomm_active_notify/';


$config['weixinpay']['htmlurl']		= BASE_URL . APP_DIR .'/lib/weixinpay/jsapi_call.php';
$config['weixinpay']['paymenttype'] = 'WEIXIN_WAP'; 			//'WEIXIN'; //

$config['ibonpay']['htmlurl'] = APP_DIR."/deposit/ibonpay/";
$config['ibonpay']['merchantnumber'] = "456025";       //商家編號
$config['ibonpay']['paymenttype']   = "MMK";
$config['ibonpay']['code'] = "gcxa3him";              //藍新認證碼
$config['ibonpay']['code2'] = "sajo498";              //殺價認證
$config['ibonpay']['url_payment'] = "https://aquarius.neweb.com.tw:80/CashSystemFrontEnd/Payment";
$config['ibonpay']['nexturl'] = BASE_URL . APP_DIR ."/deposit/ibonpay_success/";   // callback URL (似乎沒用??)

$config['hinet']['htmlurl'] = APP_DIR."/deposit/hinetpts_exc/";
$config['hinet']['paymenttype']="HINET_PTS";

$config['sjb_rate'] = 1; 		//當地貨幣與殺價幣兌換比率
$config['country'] = 2; 		// default country id (中國)
$config['region'] = 3;          // default region id (華東)
$config['province'] = 2;		// default province id (上海)
$config['channel'] = 1;         // default channel id (浦東)
$config['currency'] = "RMB";	//幣別

$config['test_id']=Array(
       '1'=>'Y',
       '28'=>'Y',
       '116' => 'Y',
       '586'=>'Y'	   
);

define("PASSWORD_SALT", $config["encode_key"]);
