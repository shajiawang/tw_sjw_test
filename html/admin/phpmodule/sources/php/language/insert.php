<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}language` SET
	`name`='{$this->io->input["post"]["name"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='{$this->io->input["post"]["seq"]}', 
	`switch`='Y',
	`insertt`=now()
" ;
$this->model->query($query);

// Insert End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$language_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'language_insert', 
	`active` = '語系新增寫入', 
	`memo` = '{$language_data}', 
	`root` = 'language/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));