<?php
// Check Variable Start
if (empty($this->io->input["get"]["langid"])) {
	$this->jsAlertMsg('語系ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// database start

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}language` SET 
switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `langid` = '{$this->io->input["get"]["langid"]}'
" ;
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$language_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'language_delete', 
	`active` = '語系刪除', 
	`memo` = '{$language_data}', 
	`root` = 'language/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End

