<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(userid|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	
	$sub_sort_query = " ORDER BY " . implode(',', $orders);
}
// Sort End


// Search Start
$status["status"]["search"] = "";
$sub_search_query = " AND ua.`userid`>0 
	AND ua.`phone` IS NOT null
	AND ah.`activity_type`=6
";

$join_search_query = "LEFT JOIN `saja_cash_flow`.`saja_activity_history` ah 
	ON ah.phone=ua.phone ";

if($this->io->input["get"]["search_act"] == 'N'){
	$status["status"]["search"]["search_act"] = $this->io->input["get"]["search_act"];
	$status["status"]["search_path"] .= "&search_act=". $this->io->input["get"]["search_act"];
	$sub_search_query .=  " AND ua.`verified`='N' ";
} elseif($this->io->input["get"]["search_act"] == 'A'){
	$status["status"]["search"]["search_act"] = $this->io->input["get"]["search_act"];
	$status["status"]["search_path"] .= "&search_act=". $this->io->input["get"]["search_act"];
} else {
	$status["status"]["search"]["search_act"] = $this->io->input["get"]["search_act"];
	$status["status"]["search_path"] .= "&search_act=". $this->io->input["get"]["search_act"];
	$sub_search_query .=  " AND ua.`verified`='Y' ";
}

if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = trim($this->io->input["get"]["search_userid"]);
	$status["status"]["search_path"] .= "&search_userid=". trim($this->io->input["get"]["search_userid"]);
	$sub_search_query .=  " AND ua.`userid` ='". trim($this->io->input["get"]["search_userid"]) ."' ";
}

if($this->io->input["get"]["search_tel"] != ''){
	$status["status"]["search"]["search_tel"] = trim($this->io->input["get"]["search_tel"]);
	$status["status"]["search_path"] .= "&search_tel=". trim($this->io->input["get"]["search_tel"]);
	$sub_search_query .=  " AND ua.`phone` ='". trim($this->io->input["get"]["search_tel"]) ."' ";
}

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start

// Table Count Start 
$query="SELECT count(*) as num 
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user_sms_auth` ua
{$join_search_query}
WHERE ua.`prefixid`='saja'
";
$query .= $sub_search_query; 
$num = $this->model->getQueryRecord($query); 

$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和

$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start

$query ="SELECT ua.authid, ua.userid, ua.phone, ua.verified, ua.modifyt, ua.switch, ah.userid uactid, ah.activity_type ";
$query .=" FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sms_auth` ua
{$join_search_query}
WHERE ua.`prefixid`='saja'
" ;

$query .= $sub_search_query;
$query .= $sub_sort_query; 
$query .= $query_limit;

$table = $this->model->getQueryRecord($query);

if(!empty($table['table']['record']) )
{
	foreach($table['table']['record'] as $rk => $rv)
	{
		//會員暱稱
		$query ="SELECT up.`userid`,up.nickname
			FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
			WHERE up.prefixid='saja'
			AND up.`userid`={$rv['userid']}
			";
		$user = $this->model->getQueryRecord($query);
		$table['table']['record'][$rk]["nickname"] = $user['table']['record'][0]["nickname"];
		
		//領取人暱稱
		$query2 ="SELECT up.`userid`,up.nickname
			FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
			WHERE up.prefixid='saja'
			AND up.`userid`={$rv['uactid']}
			";
		$user2 = $this->model->getQueryRecord($query2);
		$table['table']['record'][$rk]["uactname"] = $user2['table']['record'][0]["nickname"];
	}
}


// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user', 
	`active` = '手機驗證送查詢', 
	`memo` = '{$user_data}', 
	`root` = 'user/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
//$this->model->query($query);
// Log End 
##############################################################################################################################################
$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();
