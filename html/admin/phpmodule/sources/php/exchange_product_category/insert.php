<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################

if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	$thumbnail = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	if (file_exists($this->config->path_products_images."/category/$thumbnail")) {
		$this->jsAlertMsg('主圖名稱重覆!!');
	} else {
        if (move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/category/$thumbnail")) {
		   syncToS3($this->config->path_products_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/product/category/');
        } else {
           $this->jsAlertMsg('主圖上傳錯誤!!');
	    }
    }
}

$query ="
INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` SET
	`node`='{$this->io->input["post"]["node"]}',
	`layer`='{$this->io->input["post"]["layer"]}',
	`channelid`='{$this->io->input["post"]["channelid"]}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`thumbnail` = '{$thumbnail}', 	
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='{$this->io->input["post"]["seq"]}', 
	`display`='{$this->io->input["post"]["display"]}',
	`switch`='{$this->io->input["post"]["switch"]}',
	`insertt`=now()
" ;
$this->model->query($query);

##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$exchange_product_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_product_category_insert', 
	`active` = '兌換分類商品新增寫入', 
	`memo` = '{$exchange_product_data}', 
	`root` = 'exchange_product_category/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));