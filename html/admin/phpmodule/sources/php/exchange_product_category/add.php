<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT c.* 
FROM `{$db_channel}`.`{$this->config->default_prefix}channel` c 
WHERE 
c.prefixid = '{$this->config->default_prefix_id}' 
AND c.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_rt"] = $recArr['table']['record'];

// Relation exchange_product_category => layer=1
// $query ="
// SELECT p.*
// FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` p 
// WHERE 
// p.prefixid = '{$this->config->default_prefix_id}'
// AND p.layer = 1
// AND p.switch = 'Y'
// ";
// $recArr = $this->model->getQueryRecord($query);
// $table["table"]["rt"]["product_category"] = $recArr['table']['record'];

$query ="
SELECT `layer`
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND switch = 'Y'
GROUP BY `layer` 
ORDER BY `layer` DESC
LIMIT 1
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category_level_all"] = $recArr['table']['record'][0]['layer'];

for ($i=1; $i<$recArr['table']['record'][0]['layer']+1; $i++){ 
	//Relation exchange_product_category =>
	$query ="
	SELECT p.*
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` p 
	WHERE 
	p.prefixid = '{$this->config->default_prefix_id}'
	AND p.layer = {$i}
	AND p.switch = 'Y'
	";
	$recArr[$i] = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["product_category_level".$i] = $recArr[$i]['table']['record'];
}
 
//Relation exchange_product_category => layer=3
// $query ="
// SELECT p.*
// FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` p 
// WHERE 
// p.prefixid = '{$this->config->default_prefix_id}'
// AND p.layer = 3
// AND p.switch = 'Y'
// ";
// $recArr = $this->model->getQueryRecord($query);
// $table["table"]["rt"]["product_category_level3"] = $recArr['table']['record'];
 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_product_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_product_category_add', 
	`active` = '兌換商品分類新增', 
	`memo` = '{$exchange_product_data}', 
	`root` = 'exchange_product_category/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();