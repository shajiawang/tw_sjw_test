<?php
// Check Variable Start
if(empty($this->io->input["get"]["epcid"])) {
	$this->jsAlertMsg('分類ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// database start
$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `epcid` = '{$this->io->input["get"]["epcid"]}'
";
$this->model->query($query);


$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `epcid` = '{$this->io->input["get"]["epcid"]}'
";
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_product_category_delete', 
	`active` = '兌換商品分類刪除', 
	`memo` = '{$exchange_prodcut_data}', 
	`root` = 'exchange_product_category/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End

