<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('直播標題錯誤!!');
}
if (empty($this->io->input["post"]["lbuserid"])) {
	$this->jsAlertMsg('直播主編號錯誤!!');
}
// if (empty($this->io->input["post"]["pcid"])) {
	// $this->jsAlertMsg('分類編號錯誤!!');
// }
if (strtotime($this->io->input["post"]["ontime"]) === false) {
	$this->jsAlertMsg('上架時間錯誤!!');
}
if (strtotime($this->io->input["post"]["offtime"]) === false) {
	$this->jsAlertMsg('下架時間錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	if (file_exists($this->config->path_lb_images."/$thumbnail")) {
		$this->jsAlertMsg('直播間主圖名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_lb_images."/$thumbnail")) {
		$this->jsAlertMsg('直播間主圖上傳錯誤!!');
	}
	syncToS3($this->config->path_products_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/broadcast/');

}

$name = htmlspecialchars($this->io->input["post"]["name"]);
$description = htmlspecialchars($this->io->input["post"]["description"]);

$query = "
SELECT lbname
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user`
WHERE
	prefixid = '{$this->config->default_prefix_id}'
	AND switch = 'Y'
	AND `lbuserid` = '{$this->io->input["post"]["lbuserid"]}'
";
$recArr = $this->model->getQueryRecord($query);
$lbname = $recArr['table']['record'][0]['lbname'];

$query ="
INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}live_broadcast` SET
	`prefixid` = '{$this->config->default_prefix_id}',
	`lbuserid` = '{$this->io->input["post"]["lbuserid"]}',
	`lbname` = '{$lbname}',
	`name` = '{$name}',
	`description` = '{$description}',
	`thumbnail` = '{$thumbnail}',
	`pcid` = '{$this->io->input["post"]["pcid"]}',
	`lb_url` = '{$this->io->input["post"]["lb_url"]}',
	`share_link` = '{$this->io->input["post"]["share_link"]}',
	`promote_link` = '{$this->io->input["post"]["promote_link"]}',
	`adpage_link` = '{$this->io->input["post"]["adpage_link"]}',
	`keyword` = '{$this->io->input["post"]["keyword"]}',
	`ontime` = '{$this->io->input["post"]["ontime"]}',
	`offtime` = '{$this->io->input["post"]["offtime"]}',
	`uptime` = '{$this->io->input["post"]["uptime"]}',
	`seq` = '{$this->io->input["post"]["seq"]}',
	`insertt` = now()
" ;
$this->model->query($query);
$this->model->_con->insert_id;

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$broadcast_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'live_broadcast_insert', 
	`active` = '直播間新增寫入', 
	`memo` = '{$broadcast_data}', 
	`root` = 'live_broadcast/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
