<?php
session_start();

if(empty($_SESSION['user']['userid'])) { //'请先登入会员账号'
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
} 
else {
	
	include_once "saja/mysql.ini.php";
	$db = new mysql($this->config->db[0]);
	$config = $this->config;
	
	$p = new PreviewUpdateAuth;
	
	if($_POST['type'] == 'update') {
		$p->home($db, $config);
	}
}

class PreviewUpdateAuth 
{
	public function home($db, $config) 
	{
		$ret['status'] = 0;
		
		if(empty($_POST['userid'])) {	//會員id
			$ret['status'] = 2;
		}
		if(empty($_POST['evrid'])) {	//訂單id
			$ret['status'] = 3;
		}
		error_log("home start");
		$chk1 = $this->dataupdate($db, $config);
		
		if($chk1['err']) {
			$ret['status'] = $chk1['err'];
		}
		
		if(empty($ret['status']) ) {
			//回傳: 
			$ret['status'] = 200;
		}
		error_log("home end");
		echo json_encode($ret);
	}
	
	public function dataupdate($db, $config) {
		//資料庫連結介面
		$db->connect();
		error_log("dataupdate start");
		if ($_SESSION['user']['userid'] == '20') {
			$enterpriseid = '2';
		}
		else {
			$this->jsAlertMsg('請勿進入!!');
		}
		
		$userid = $_POST["userid"];
		$evrid = $_POST["evrid"];
		
		//檢查訂單
		$query ="SELECT * FROM `{$config->db[3]['dbname']}`.`{$config->default_prefix}exchange_vendor_record` 
		WHERE 
			`prefixid` = '{$config->default_prefix_id}' 
			AND `evrid` = '{$evrid}'
			and `tx_currency` = 'bonus'
			and `total_bonus` > 0
			AND `switch` = 'Y'
		LIMIT 1	
		";
		$recArr = $db->getQueryRecord($query);
		$total_bonus = isset($recArr['table']['record'][0]["total_bonus"]) ? (float)$recArr['table']['record'][0]["total_bonus"] : 0;
		$vendor_prodid = $recArr['table']['record'][0]["vendor_prodid"];
		
		//檢查使用者的紅利點數
		$query = "SELECT SUM(amount) bonus FROM `{$config->db[1]['dbname']}`.`{$config->default_prefix}bonus`  
		WHERE 
			`prefixid` = '{$config->default_prefix_id}' 
			AND `userid` = '{$userid}' 
			AND `switch` = 'Y'
		";
		$recArr = $db->getQueryRecord($query);
		$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;
		
		if($user_bonus < $total_bonus) {
			$r['err'] = 4;		//會員紅利點數不足!!
		}
		else {
			//查詢會員資訊
			$query = "SELECT * FROM `{$config->db[0]['dbname']}`.`{$config->default_prefix}user_profile`  
		    WHERE 
		    	`prefixid` = '{$config->default_prefix_id}' 
		    	AND `userid` = '{$userid}' 
		    	AND `switch` = 'Y'
		    ";
		    $table = $db->getQueryRecord($query);
		    
		    //扣除會員紅利點數
		    $query = "INSERT INTO `{$config->db[1]['dbname']}`.`{$config->default_prefix}bonus` SET 
			    `prefixid`='{$config->default_prefix_id}',
				`userid` = '{$userid}',
				`countryid` = '{$table['table']['record'][0]['countryid']}',
				`behav` = 'user_exchange',
				`amount` = '-{$total_bonus}',
				`seq` = '0',
				`switch` = 'Y', 
				`insertt` = now()
			";
			$db->query($query);
			$bonusid = $db->_con->insert_id;
			
			//查詢商家資訊
			$query = "SELECT e.esid, ep.* FROM `{$config->db[0]['dbname']}`.`{$config->default_prefix}enterprise` e 
			left join `{$config->db[0]['dbname']}`.`{$config->default_prefix}enterprise_profile` ep on
				ep.`prefixid` = e.`prefixid`
				and ep.`enterpriseid` = e.`enterpriseid`
				and ep.`switch` = 'Y'
		    WHERE 
		    	e.`prefixid` = '{$config->default_prefix_id}' 
		    	AND e.`enterpriseid` = '{$enterpriseid}' 
		    	AND e.`switch` = 'Y'
		    ";
		    $enterpriseArr = $db->getQueryRecord($query);
		    
		    //增加商家紅利點數
		    $query = "INSERT INTO `{$config->db[1]['dbname']}`.`{$config->default_prefix}bonus_store` SET 
			    `prefixid` = '{$config->default_prefix_id}',
			    `bonusid` = '{$bonusid}',
			    `esid` = '{$enterpriseArr['table']['record'][0]['esid']}', 
			    `enterpriseid`= '{$enterpriseArr['table']['record'][0]['enterpriseid']}', 
				`countryid` = '{$enterpriseArr['table']['record'][0]['countryid']}',
				`behav` = 'user_exchange',
				`amount` = '{$total_bonus}',
				`seq` = '0',
				`switch` = 'Y', 
				`insertt` = now()
			";
			$db->query($query);
		    
			//編輯訂單 Exchange_vendor_record
			$query = "UPDATE `{$config->db[3]['dbname']}`.`{$config->default_prefix}exchange_vendor_record` SET 
				`bonusid` = '{$bonusid}',
				`tx_status` = '3', 
				`commit_time` = NOW(), 
				`modifyt` = NOW()
			where
				`prefixid` = '{$config->default_prefix_id}'
				and `evrid` = '{$evrid}'
				and `vendorid` = '{$_SESSION['user']['userid']}'
				and `userid` = '{$userid}'
				and `tx_status` = '1'
				and `switch` = 'Y'
			";
			$db->query($query);
			error_log("dataupdate success end");
			return false;
		}
		error_log("dataupdate error end");
		return $r;
	}	
}