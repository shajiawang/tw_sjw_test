<?php
// Check Variable Start
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

##############################################################################################################################################
// Table  Start 

// Table Content Start
$query = "SELECT * FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_vendor_record` 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `evrid` = '{$this->io->input["get"]["evrid"]}'
	and `tx_type` = 'admin_gen_tx' 
	and `tx_status` = '1'
	AND `switch` = 'Y' 
" ;
$recArr = $this->model->getQueryRecord($query);
if (empty($recArr['table']['record'][0])) {
	$this->jsPrintMsg('訂單已完成!!', base64_encode(urlencode($this->config->default_main."/".$this->io->input["get"]["fun"])));
}
$table['table']['exchange_vendor_record'] = $recArr['table']['record'][0];

$query = "select * from `{$db_user}`.`{$this->config->default_prefix}user_profile` 
where 
	`prefixid` = '{$this->config->default_prefix_id}'
	and `userid` = '{$table['table']['exchange_vendor_record']['userid']}'
	and `switch`= 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table['table']['user'] = $recArr['table']['record'][0];

//兌換商品
$query = "select * from `{$db_exchange}`.`{$this->config->default_prefix}exchange_product`  
where 
	`prefixid` = '{$this->config->default_prefix_id}'
	and `epid` = '{$table['table']['exchange_vendor_record']['vendor_prodid']}'
	and `switch`= 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table['table']['exchange_product'] = $recArr['table']['record'][0];
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End 
##############################################################################################################################################

//$status["status"]["base_href"] = $status["status"]["path"] ."orderid={$this->io->input["get"]["orderid"]}";
//$status["status"]["base_href"] = $this->io->input["get"]["location_url"];
if (!isset($_SESSION['SMSCount'])) {
	$_SESSION['SMSCount'] = 3;
}

$this->tplVar('table', $table['table']) ;
$this->tplVar('status', $status["status"]);
$this->display();