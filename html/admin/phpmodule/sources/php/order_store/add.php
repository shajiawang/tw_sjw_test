<?php
// Check Variable Start
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
unset($_SESSION['SMSCount']);

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

##############################################################################################################################################
// Table  Start 

// Table Content Start
$query = "select up.* from `{$db_user}`.`{$this->config->default_prefix}user_profile` up 
where 
	`prefixid` = '{$this->config->default_prefix_id}'
	and `phone` = '{$this->io->input['get']['search_name']}'
	and `switch`= 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table['table']['user'] = $recArr['table']['record'][0];

//會員紅利點數
if (!empty($table['table']['user']['userid'])) {
	$query = "select sum(amount) as total_bonus from `{$db_cash_flow}`.`{$this->config->default_prefix}bonus`  
	where 
		`prefixid` = '{$this->config->default_prefix_id}'
		and `userid` = '{$table['table']['user']['userid']}'
		and `switch`= 'Y'
	";
	$recArr = $this->model->getQueryRecord($query);
	$table['table']['bonus'] = $recArr['table']['record'][0];
}

if ($this->io->input['session']['user']["userid"] == '20') {
	$enterpriseid = '2';
	$epid = '490, 491, 492';
}
else {
	$this->jsAlertMsg('請勿進入!!');
}
//兌換商品
$query = "select * from `{$db_exchange}`.`{$this->config->default_prefix}exchange_product`  
where 
	`prefixid` = '{$this->config->default_prefix_id}'
	and `epid` in ({$epid})
	and `switch`= 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table['table']['exchange_product'] = $recArr['table']['record'];

$query = "select * from `{$db_user}`.`{$this->config->default_prefix}enterprise`  
where 
	`prefixid` = '{$this->config->default_prefix_id}'
	and `enterpriseid` = '{$enterpriseid}'
	and `switch`= 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table['table']['enterprise'] = $recArr['table']['record'][0];
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"] ."search_name={$this->io->input["get"]["search_name"]}";

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();