<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 

if($_SESSION['user']['department'] != 'S') { 
	$this->jsAlertMsg('您無權限登入！');
	die();
}

$userid = $this->io->input['session']['user']["userid"];

if (empty($this->io->input["get"]["userid"])) {
	$this->jsAlertMsg('userid錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]["dbname"];

$query ="
SELECT u.*
FROM `{$db_user}`.`{$this->config->default_prefix}admin_user` u
WHERE 
u.prefixid = '".$this->config->default_prefix_id."' 
AND u.userid = '".$this->io->input["get"]["userid"]."'
" ;
$table = $this->model->getQueryRecord($query); 

##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$admin_user_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'admin_user_edit', 
	`active` = '管理者帳號修改', 
	`memo` = '{$admin_user_data}', 
	`root` = 'admin_user/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();