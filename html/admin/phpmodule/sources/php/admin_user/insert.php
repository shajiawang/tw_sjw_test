<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
$this->str = new convertString();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]["dbname"];

if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('帳號不可空白!!');
}
if (empty($this->io->input["post"]["passwd"])) {
	$this->jsAlertMsg('密碼不可空白!!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('Email不可空白!!');
}
if (!filter_var($this->io->input['post']['email'], FILTER_VALIDATE_EMAIL)) {
	$this->jsAlertMsg('Email格式錯誤!!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('Email不可空白!!');
}

##############################################################################################################################################
// database start

$passwd = $this->io->input['post']['passwd']; //$this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key);
$query ="
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_user` SET 
	`name`   = '{$this->io->input["post"]["name"]}',
	`passwd` = '{$passwd}',
	`email`  = '{$this->io->input["post"]["email"]}',
	`department`  = '{$this->io->input["post"]["department"]}',
	`prefixid`='{$this->config->default_prefix_id}', 
	`seq`='0',
	`insertt`=now() 
" ;
$this->model->query($query); 

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$admin_user_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'admin_user_insert', 
	`active` = '管理者帳號新增寫入', 
	`memo` = '{$admin_user_data}', 
	`root` = 'admin_user/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// success message Start
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
// success message End