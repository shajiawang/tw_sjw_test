<?php
// Check Variable Start
if(empty($this->io->input["get"]["userid"])){
	$this->jsAlertMsg('參數錯誤,請聯絡技術人員!');
}
// Check Variable End

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 

if($_SESSION['user']['department'] != 'S') { 
	$this->jsAlertMsg('您無權限登入！');
	die();
}

$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]["dbname"];

##############################################################################################################################################
// database start

$query ="
UPDATE `{$db_user}`.`{$this->config->default_prefix}admin_user` SET 
switch = 'N'
WHERE 
prefixid = '{$this->config->default_prefix_id}' 
AND userid = '{$this->io->input["get"]["userid"]}'
" ;
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$admin_user_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'admin_user_delete', 
	`active` = '管理者帳號刪除', 
	`memo` = '{$admin_user_data}', 
	`root` = 'admin_user/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!',$this->io->input["get"]["location_url"]);
// success message End