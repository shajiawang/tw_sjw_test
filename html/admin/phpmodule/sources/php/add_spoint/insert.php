<?php
// Check Variable Start
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員編號不可為空白 !!');
}
if (empty($this->io->input["post"]["behav"])) {
	$this->jsAlertMsg('殺價幣行為不可為空白!!');
}
if (!is_numeric($this->io->input["post"]["spoint"]) || $this->io->input["post"]["spoint"] <= 0) {
	$this->jsAlertMsg('殺價幣金額錯誤!!');
}

// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
$snum = $this->io->input["post"]["snum"];

$query ="
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}spoint` SET 
	`prefixid`='{$this->config->default_prefix_id}', 
	`userid` = '{$this->io->input["post"]["userid"]}', 
	`behav` = '{$this->io->input["post"]["behav"]}', 
	`amount` = '{$this->io->input["post"]["spoint"]}', 
	`remark` = '{$this->io->input["post"]["remark"]}', 
	`memo` = '{$this->io->input["post"]["memo"]}', 
	`seq`='0', 
	`switch`='Y', 
	`insertt`=now()
" ;
$this->model->query($query);
$spointid = $this->model->_con->insert_id;

if ($this->io->input["post"]["remark"] != 0){
	$query = "INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}spoint_free_history` SET
		`userid`='{$this->io->input["post"]["userid"]}',
		`behav`='{$this->io->input["post"]["behav"]}',
		`activity_type`='{$this->io->input["post"]["remark"]}',
		`amount_type`='1',
		`free_amount`='{$this->io->input["post"]["spoint"]}',
		`total_amount`='{$this->io->input["post"]["spoint"]}',
		`spointid`='{$spointid}',
		`insertt`=NOW()
	";
	$this->model->query($query); 
}

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_spoint_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_spoint_insert', 
	`active` = '手動加殺價幣新增寫入', 
	`memo` = '{$add_spoint_data}', 
	`root` = 'add_spoint/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
// $this->jsPrintMsg('殺價券(userid:{$t['table']['record'][0]['userid']},productid:${productid}, total:${snum} ) : 新增 ${total} 組完成!!',$this->io->input["get"]["location_url"]);
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));