<?php
// Check Variable Start
if (empty($this->io->input["post"]["spointid"])) {
	$this->jsAlertMsg('殺價幣編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
//Update Start

	$query = "
	UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}spoint` SET 
		`switch` = '{$this->io->input["post"]['switch']}', 
		`modifyt` = NOW()
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `spointid` = '{$this->io->input["post"]['spointid']}'
	" ;
	$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_spoint_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_spoint_update', 
	`active` = '手動加殺價幣修改寫入', 
	`memo` = '{$add_spoint_data}', 
	`root` = 'add_spoint/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
		   
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));