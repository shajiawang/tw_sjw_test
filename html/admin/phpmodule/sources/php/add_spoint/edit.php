<?php
// Check Variable Start
if (empty($this->io->input["get"]["spointid"])) {
	$this->jsAlertMsg('殺價幣編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}spoint`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `spointid` = '{$this->io->input["get"]["spointid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}action`
WHERE NOW() between ontime and offtime 
" ;
$recArr = $this->model->getQueryRecord($query);

$table["table"]["rt"]["action"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_spoint_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_spoint_edit', 
	`active` = '手動加殺價幣修改', 
	`memo` = '{$add_spoint_data}', 
	`root` = 'add_spoint/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();