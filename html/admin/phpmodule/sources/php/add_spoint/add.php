<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start
 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start

$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}action`
WHERE NOW() between ontime and offtime 
" ;
$table = $this->model->getQueryRecord($query);
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_spoint_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_spoint_add', 
	`active` = '手動加殺價幣新增', 
	`memo` = '{$add_spoint_data}', 
	`root` = 'add_spoint/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->display();