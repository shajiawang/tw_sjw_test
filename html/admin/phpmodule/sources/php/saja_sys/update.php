<?php
// Check Variable Start
if (empty($this->io->input["post"]["id"])) {
	$this->jsAlertMsg('編號錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('項目名稱錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$filename = md5(date("YmdHis")."_".$this->io->input['post']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_products_images."/$filename") && ($filename != $this->io->input['post']['oldthumbnail'])) {
		$this->jsAlertMsg('圖片名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/$filename")) {
		$this->jsAlertMsg('圖片上傳錯誤!!');
	}
	
	//刪除舊圖
	if (!empty($this->io->input['post']['oldthumbnail']) && ($filename != $this->io->input['post']['oldthumbnail'])) {
		unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail']);
	}
	
	$update_thumbnail = "`thumbnail` = '{$thumbnail}', ";
	// syncToS3($this->config->path_products_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/product/');
}

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}sys` SET 
{$update_thumbnail}
`name` = '{$this->io->input["post"]["name"]}',
`description`='{$this->io->input["post"]["description"]}',
`used`='{$this->io->input["post"]["used"]}',
`modifyt`='".date('Y-m-d H:i:s')."'
WHERE 
`id` = '{$this->io->input["post"]["id"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$sys_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'sys_update', 
	`active` = '系統訊息修改寫入', 
	`memo` = '{$sys_data}', 
	`root` = 'sys/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));