<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('公告標題錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('公告內容錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
$description = htmlspecialchars($this->io->input["post"]["description"]);
$query ="
INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}news` SET
	`ontime`='{$this->io->input["post"]["ontime"]}',
	`offtime`='{$this->io->input["post"]["offtime"]}',
	`public`='{$this->io->input["post"]["public"]}',
	`name`='{$this->io->input["post"]["name"]}',
	`osversion` = '{$this->io->input["post"]["osversion"]}', 
	`description`='{$description}', 
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='{$this->io->input["post"]["seq"]}', 
	`insertt`=now()
" ;
$this->model->query($query);
$newsid = $this->model->_con->insert_id;

//Relation channel_news_rt
if(empty($this->io->input["post"]["channelid"]) ) {
	$channelid[0] = 1;
}
else{
	$channelid = $this->io->input["post"]["channelid"];
}

if(is_array($channelid) )
{
	foreach($channelid as $rk => $rv) {
		$query = "INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}channel_news_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`channelid`='{$rv}',
		`newsid`='{$newsid}',
		`seq`='".(($rk+1)*10)."', 
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$news_data = json_encode($this->io->input["get"]));

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'news_insert', 
	`active` = '會員公告新增寫入', 
	`memo` = '{$news_data}', 
	`root` = 'news/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));