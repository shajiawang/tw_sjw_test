<?php
// Check Variable Start
if (empty($this->io->input["post"]["newsid"])) {
	$this->jsAlertMsg('公告ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('公告標題錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('公告內容錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
$description = htmlspecialchars($this->io->input["post"]["description"]);
$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}news` SET 
	`ontime`='{$this->io->input["post"]["ontime"]}',
	`offtime`='{$this->io->input["post"]["offtime"]}',
	`public` = '".$this->io->input["post"]["public"]."', 
	`name` = '{$this->io->input["post"]["name"]}', 
	`osversion` = '{$this->io->input["post"]["osversion"]}', 
	`description` = '{$description}', 
	`seq` = '{$this->io->input["post"]["seq"]}'
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `newsid` = '{$this->io->input["post"]["newsid"]}'
" ;
$this->model->query($query);

//Relation channel_news_rt
$query = "UPDATE `{$db_channel}`.`{$this->config->default_prefix}channel_news_rt` 
SET `switch` = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `newsid` = '{$this->io->input["post"]["newsid"]}'
";
$this->model->query($query);

if(empty($this->io->input["post"]["channelid"]) ) {
	$channelid[0] = 1;
}else{
	$channelid = $this->io->input["post"]["channelid"];
}

if(is_array($channelid) )
{
	foreach($channelid as $rk => $rv) {
		$query = "INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}channel_news_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`channelid`='{$rv}',
		`newsid` = '{$this->io->input["post"]["newsid"]}',
		`seq`='".(($rk+1)*10)."', 
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query);
	}
}
//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$news_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'news_update', 
	`active` = '會員公告修改寫入', 
	`memo` = '{$news_data}', 
	`root` = 'news/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));