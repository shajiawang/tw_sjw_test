<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(enterpriseid|insertt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = " dh.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if($this->io->input["get"]["search_companyname"] != ''){
	$status["status"]["search"]["search_companyname"] = $this->io->input["get"]["search_companyname"] ;
	$status["status"]["search_path"] .= "&search_companyname=".$this->io->input["get"]["search_companyname"] ;
	$sub_search_query .=  "
		AND epr.`companyname` like '%".$this->io->input["get"]["search_companyname"]."%'";
}
if($this->io->input["get"]["startdatetime"] != ''){
	$status["status"]["search"]["startdatetime"] = $this->io->input["get"]["startdatetime"] ;
	$status["status"]["search_path"] .= "&startdatetime=".$this->io->input["get"]["startdatetime"] ;
	$sub_search_query .=  "
		AND bs.`insertt` >= '".$this->io->input["get"]["startdatetime"]."'";
}
if($this->io->input["get"]["enddatetime"] != ''){
	$status["status"]["search"]["enddatetime"] = $this->io->input["get"]["enddatetime"] ;
	$status["status"]["search_path"] .= "&enddatetime=".$this->io->input["get"]["enddatetime"] ;
	$sub_search_query .=  "
		AND bs.`insertt` <= '".$this->io->input["get"]["enddatetime"]."'";
}
if($this->io->input["get"]["search_settle"] != ''){
	$status["status"]["search"]["search_settle"] = $this->io->input["get"]["search_settle"] ;
	$status["status"]["search_path"] .= "&search_settle=".$this->io->input["get"]["search_settle"] ;
	$sub_search_query .=  "
		AND bs.`is_settle` = '".$this->io->input["get"]["search_settle"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start

// Table Count Start
$query = "SELECT count(*) as num
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus_store` bs
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_store_history` ebsh
on
	bs.prefixid = ebsh.prefixid
	and bs.bsid = ebsh.bsid
	and ebsh.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}order` o
on
	ebsh.prefixid = o.prefixid
	and ebsh.orderid = o.orderid
	and o.type = 'exchange'
	and o.epid != 0
	and o.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` ep
on
	o.prefixid = ep.prefixid
	and o.epid = ep.epid
	and ep.switch = 'Y'
left outer join `{$db_user}`.`{$this->config->default_prefix}enterprise_profile` epr
on
	bs.prefixid = epr.prefixid
	and bs.enterpriseid = epr.enterpriseid
	and epr.switch = 'Y'
WHERE
	bs.`prefixid` = '{$this->config->default_prefix_id}'
	and bs.bsid IS NOT NULL

";
$query .= $sub_search_query ;
$query .= $sub_sort_query ;
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"] - 1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end

// Table Record Start
$query = "select bs.bsid, bs.bonusid,bs.switch,ep.name, bs.behav, bs.amount, epr.companyname,
(select up.nickname from `{$db_user}`.`{$this->config->default_prefix}user_profile` up
where o.prefixid = up.prefixid and o.userid = up.userid and up.switch = 'Y' ) nickname,
bs.is_settle, bs.insertt
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus_store` bs
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_store_history` ebsh
on
	bs.prefixid = ebsh.prefixid
	and bs.bsid = ebsh.bsid
	and ebsh.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}order` o
on
	ebsh.prefixid = o.prefixid
	and ebsh.orderid = o.orderid
	and o.type = 'exchange'
	and o.epid != 0
	and o.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` ep
on
	o.prefixid = ep.prefixid
	and o.epid = ep.epid
	and ep.switch = 'Y'
left outer join `{$db_user}`.`{$this->config->default_prefix}enterprise_profile` epr
on
	bs.prefixid = epr.prefixid
	and bs.enterpriseid = epr.enterpriseid
	and epr.switch = 'Y'
WHERE
	bs.`prefixid` = '{$this->config->default_prefix_id}'
	and bs.bsid IS NOT NULL

";
$query .= $sub_search_query ;
$query .= $sub_sort_query ;
$query .= $query_limit ;
$table = $this->model->getQueryRecord($query);

// Table Record End


//Amount Total Start
//紅利總數
$query = "SELECT sum(bs.amount) as TotalAmount
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus_store` bs
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_store_history` ebsh
on
	bs.prefixid = ebsh.prefixid
	and bs.bsid = ebsh.bsid
	and ebsh.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}order` o
on
	ebsh.prefixid = o.prefixid
	and ebsh.orderid = o.orderid
	and o.type = 'exchange'
	and o.epid != 0
	and o.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` ep
on
	o.prefixid = ep.prefixid
	and o.epid = ep.epid
	and ep.switch = 'Y'
left outer join `{$db_user}`.`{$this->config->default_prefix}enterprise_profile` epr
on
	bs.prefixid = epr.prefixid
	and bs.enterpriseid = epr.enterpriseid
	and epr.switch = 'Y'
WHERE
	bs.`prefixid` = '{$this->config->default_prefix_id}'
	and bs.bsid IS NOT NULL
	and bs.switch = 'Y'
" ;
$query .= $sub_search_query;
$TotalAmount = $this->model->getQueryRecord($query);
$Total['TotalAmount'] = $TotalAmount['table']['record'][0]['TotalAmount'];

//已結清紅利總數
$query = "SELECT sum(bs.amount) as SettledAmount 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus_store` bs 
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_store_history` ebsh 
on
	bs.prefixid = ebsh.prefixid 
	and bs.bsid = ebsh.bsid 
	and ebsh.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}order` o 
on
	ebsh.prefixid = o.prefixid 
	and ebsh.orderid = o.orderid 
	and o.type = 'exchange'
	and o.epid != 0
	and o.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` ep 
on
	o.prefixid = ep.prefixid 
	and o.epid = ep.epid 
	and ep.switch = 'Y'
left outer join `{$db_user}`.`{$this->config->default_prefix}enterprise_profile` epr 
on
	bs.prefixid = epr.prefixid 
	and bs.enterpriseid = epr.enterpriseid 
	and epr.switch = 'Y'
WHERE 
	bs.`prefixid` = '{$this->config->default_prefix_id}' 
	and bs.bsid IS NOT NULL
	and bs.is_settle = 'Y'
	and bs.switch = 'Y'
" ;
$query .= $sub_search_query;
$SettledAmount = $this->model->getQueryRecord($query);
$Total['SettledAmount'] = $SettledAmount['table']['record'][0]['SettledAmount'];

//未結清紅利總數
$query = "SELECT sum(bs.amount) as UnsettledAmount 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus_store` bs 
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_store_history` ebsh 
on
	bs.prefixid = ebsh.prefixid 
	and bs.bsid = ebsh.bsid 
	and ebsh.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}order` o 
on
	ebsh.prefixid = o.prefixid 
	and ebsh.orderid = o.orderid 
	and o.type = 'exchange'
	and o.epid != 0
	and o.switch = 'Y'
left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` ep 
on
	o.prefixid = ep.prefixid 
	and o.epid = ep.epid 
	and ep.switch = 'Y'
left outer join `{$db_user}`.`{$this->config->default_prefix}enterprise_profile` epr 
on
	bs.prefixid = epr.prefixid 
	and bs.enterpriseid = epr.enterpriseid 
	and epr.switch = 'Y'
WHERE 
	bs.`prefixid` = '{$this->config->default_prefix_id}' 
	and bs.bsid IS NOT NULL
	and bs.is_settle = 'N'
	and bs.switch = 'Y'
" ;
$query .= $sub_search_query;
$UnsettledAmount = $this->model->getQueryRecord($query);
$Total['UnsettledAmount'] = $UnsettledAmount['table']['record'][0]['UnsettledAmount'];
//Amount Total End

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$enterprise_bonus_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'enterprise_bonus', 
	`active` = '商家紅利清單查詢', 
	`memo` = '{$enterprise_bonus_data}', 
	`root` = 'enterprise_bonus/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################
$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table', $table['table']) ;
$this->tplVar('status', $status["status"]);
$this->tplVar('Total', $Total);
$this->display();