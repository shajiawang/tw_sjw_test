<?php
// Check Variable Start
/* 鎖uid */
if (($this->io->input['session']['user']['userid'] != '1') && ($this->io->input['session']['user']['userid'] != '2') && ($this->io->input['session']['user']['userid'] != '3')) {
	$this->jsAlertMsg('您無權限登入！');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

if (!empty($this->io->input["post"]["type"])) {
	if ($this->io->input["post"]["type"] == 'settle_single') 
    {
    	$bsid = $this->io->input["post"]["bsid"];
    	
    	$query = "UPDATE `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus_store`
    	SET 
    		is_settle = 'Y', 
    		modifyt = NOW()
    	WHERE 
    		prefixid = '{$this->config->default_prefix_id}'
    		AND bsid = '{$bsid}'
    		and switch = 'Y'
    	" ;
    	$this->model->query($query);
    }
    else if ($this->io->input["post"]["type"] == 'settle') {
    	$fieldname = "";
    	
    	if (!empty($this->io->input["post"]["companyname"])) {
    		$fieldname .= " AND epr.`companyname` like '%{$this->io->input["post"]["companyname"]}%'";
    	}
    	if (!empty($this->io->input["post"]["startdatetime"])) {
    		$fieldname .= " AND bs.`insertt` >= '{$this->io->input["post"]["startdatetime"]}'";
    	}
    	if (!empty($this->io->input["post"]["enddatetime"])) {
    		$fieldname .= " AND bs.`insertt` <= '{$this->io->input["post"]["enddatetime"]}'";
    	}
    	if (!empty($this->io->input["post"]["settle"])) {
    		$fieldname .= " AND bs.`is_settle` = '{$this->io->input["post"]["settle"]}'";
    	}
    	
    	$companyname = $this->io->input["post"]["companyname"];
    	$startdatetime = $this->io->input["post"]["startdatetime"];
    	$enddatetime = $this->io->input["post"]["enddatetime"];
    	$settle = $this->io->input["post"]["settle"];
    	
    	$query = "UPDATE `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus_store` bs 
    	left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_store_history` ebsh 
        on
        	bs.prefixid = ebsh.prefixid 
        	and bs.bsid = ebsh.bsid 
        	and ebsh.switch = 'Y'
        left outer join `{$db_exchange}`.`{$this->config->default_prefix}order` o 
        on
        	ebsh.prefixid = o.prefixid 
        	and ebsh.orderid = o.orderid 
        	and o.type = 'exchange'
        	and o.epid != 0
        	and o.switch = 'Y'
        left outer join `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` ep 
        on
        	o.prefixid = ep.prefixid 
        	and o.epid = ep.epid 
        	and ep.switch = 'Y'
        left outer join `{$db_user}`.`{$this->config->default_prefix}enterprise_profile` epr 
        on
        	bs.prefixid = epr.prefixid 
        	and bs.enterpriseid = epr.enterpriseid 
        	and epr.switch = 'Y'
        SET 
    		bs.`is_settle` = 'Y', 
    		bs.`modifyt` = NOW()
    	WHERE 
    		bs.`prefixid` = '{$this->config->default_prefix_id}'
    		{$fieldname}
    		and bs.`switch` = 'Y'
    	" ;
    	$this->model->query($query);
    }
    $ret['msg'] = '紅利已結清';
    echo json_encode($ret);
	


##############################################################################################################################################
// Log Start 
$enterprise_bonus_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'enterprise_bonus_update', 
	`active` = '商家紅利結清寫入', 
	`memo` = '{$enterprise_bonus_data}', 
	`root` = 'enterprise_bonus/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
	
}
else {
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
}