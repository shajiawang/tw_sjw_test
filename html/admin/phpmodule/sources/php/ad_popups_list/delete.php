<?php
// Check Variable Start
if(empty($this->io->input["get"]["adid"])) {
	$this->jsAlertMsg('廣告編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// database start
$query ="UPDATE `{$db_channel}`.`{$this->config->default_prefix}ad` 
SET switch = 'N' 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `adid` = '{$this->io->input["get"]["adid"]}'
	AND `acid` = '7'
";
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$ad_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ad_popups_delete', 
	`active` = '首頁彈窗刪除', 
	`memo` = '{$ad_data}', 
	`root` = 'ad_popups/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End

