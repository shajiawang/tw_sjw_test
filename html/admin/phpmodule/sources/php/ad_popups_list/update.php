<?php
// Check Variable Start
if (empty($this->io->input["post"]["adid"])) {
	$this->jsAlertMsg('廣告編號錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('主題錯誤!!');
}
if (empty($this->io->input["post"]["acid"])) {
	$this->jsAlertMsg('選擇主題錯誤!!');
}
if (strtotime($this->io->input["post"]["ontime"]) === false) {
	$this->jsAlertMsg('上架時間錯誤!!');
}
if (strtotime($this->io->input["post"]["offtime"]) === false) {
	$this->jsAlertMsg('下架時間錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_ad_images."/$thumbnail") && ($thumbnail != $this->io->input['post']['oldthumbnail'])) {
		$this->jsAlertMsg('首頁彈窗主圖名稱重覆!!');
	}
	else {
        if (move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_ad_images."/$thumbnail")) {
            syncToS3($this->config->path_ad_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/ad/');
        } else {
		    $this->jsAlertMsg('首頁彈窗主圖上傳錯誤!!');
        }
	}
	
	//刪除舊圖
	if (!empty($this->io->input['post']['oldthumbnail']) && ($thumbnail != $this->io->input['post']['oldthumbnail'])) {
		unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail']);
	}
	$update_thumbnail = "`thumbnail` = '{$thumbnail}', ";
	syncToS3($this->config->path_products_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/ad/');

}

if (!empty($this->io->input['files']['thumbnail2']) && exif_imagetype($this->io->input['files']['thumbnail2']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail2 = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])."_2").".".pathinfo($this->io->input['files']['thumbnail2']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_ad_images."/$thumbnail2") && ($thumbnail != $this->io->input['post']['oldthumbnail2'])) {
		$this->jsAlertMsg('首頁彈窗按鈕圖名稱重覆!!');
	}
	else {
        if (move_uploaded_file($this->io->input['files']['thumbnail2']['tmp_name'], $this->config->path_ad_images."/$thumbnail2")) {
             syncToS3($this->config->path_ad_images."/".$thumbnail2,'s3://img.saja.com.tw','/site/images/site/ad/');
        } else {    
		    $this->jsAlertMsg('首頁彈窗按鈕圖上傳錯誤!!');
        }
	}
	
	//刪除舊圖
	if (!empty($this->io->input['post']['oldthumbnail2']) && ($thumbnail != $this->io->input['post']['oldthumbnail2'])) {
		unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail2']);
	}
	
	$update_thumbnail2 = "`thumbnail2` = '{$thumbnail2}', ";
	syncToS3($this->config->path_products_images."/".$thumbnail2,'s3://img.saja.com.tw','/site/images/site/ad/');
}


$name = htmlspecialchars($this->io->input["post"]["name"]);
$description = htmlspecialchars($this->io->input["post"]["description"]);
$action_memo = array(
		"type"			=> $this->io->input["post"]["action_memo_type"],
		"url_title"		=> urlencode($this->io->input["post"]["action_memo_url_title"]),
		"url"			=> $this->io->input["post"]["action_memo_url"],
		"imgurl"		=> $this->io->input["post"]["action_memo_imgurl"],
		"page"			=> $this->io->input["post"]["action_memo_page"],
		"productid"		=> $this->io->input["post"]["action_memo_productid"],
		"epcid"			=> $this->io->input["post"]["action_memo_epcid"],
		"layer"			=> $this->io->input["post"]["action_memo_layer"],
		"epid"			=> $this->io->input["post"]["action_memo_epid"],
		"type2"			=> $this->io->input["post"]["action_memo_type2"],
		"url_title2"	=> urlencode($this->io->input["post"]["action_memo_url_title2"]),
		"url2"			=> $this->io->input["post"]["action_memo_url2"],
		"imgurl2"		=> $this->io->input["post"]["action_memo_imgurl2"],
		"page2"			=> $this->io->input["post"]["action_memo_page2"],
		"productid2"	=> $this->io->input["post"]["action_memo_productid2"],
		"epcid2"		=> $this->io->input["post"]["action_memo_epcid2"],
		"layer2"		=> $this->io->input["post"]["action_memo_layer2"],
		"epid2"			=> $this->io->input["post"]["action_memo_epid2"]
	);
$action_memo = json_encode($action_memo);

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}ad` SET 
	`acid`='7',
	`name` = '{$name}', 
	`description` = '{$description}',
	{$update_thumbnail}
	{$update_thumbnail2}
	`qrcode_url` = '{$this->io->input["post"]["url"]}',
	`action_a` = '{$this->io->input["post"]["url_a"]}',
	`action_i` = '{$this->io->input["post"]["url_i"]}',
	`action_memo` = '{$action_memo}',
	`ontime` = '{$this->io->input["post"]["ontime"]}', 
	`offtime` = '{$this->io->input["post"]["offtime"]}',
	`seq` = '{$this->io->input["post"]["seq"]}', 
	`used` = '{$this->io->input["post"]["used"]}', 
	`modifyt` = NOW()
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `adid` = '{$this->io->input["post"]["adid"]}'
" ;
$this->model->query($query);


$query = "
SELECT place 
FROM `{$db_channel}`.`{$this->config->default_prefix}ad_category` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND acid = '7'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$place = $recArr['table']['record'][0]['place'];

$query = "
SELECT place 
FROM `{$db_channel}`.`{$this->config->default_prefix}ad_category` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND acid = '7'
	AND switch = 'Y'
";
$recArr2 = $this->model->getQueryRecord($query);
$place2 = $recArr2['table']['record'][0]['place'];

$query = " 
	select ad.adid, ad.name, ad.seq, ad.qrcode_url as action, ad.action_a, ad.action_i, ad.thumbnail_url, ad.thumbnail_url2, ad.thumbnail, ad.thumbnail2 , ad.thumbnail_popup_a  , ad.thumbnail_popup_i, ad.offtime
	from `{$db_channel}`.`{$this->config->default_prefix}ad` ad
	LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}ad_category` ac ON ad.acid = ac.acid
	where 
	  ad.switch = 'Y' 
	  and ad.used = 'Y' 
	  and ac.switch = 'Y' 
	  and ac.used = 'Y' 
	  and NOW() between ad.ontime and ad.offtime 
	  and ac.place = '5'
    order by ad.seq asc ";

//取得資料
$table = $this->model->getQueryRecord($query);

$query2 = " 
	select ad.adid, ad.name, ad.seq, ad.qrcode_url as action, ad.action_a, ad.action_i, ad.thumbnail_url, ad.thumbnail_url2, ad.thumbnail, ad.thumbnail2 , ad.thumbnail_popup_a , ad.thumbnail_popup_i, ad.offtime
	from `{$db_channel}`.`{$this->config->default_prefix}ad` ad
	LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}ad_category` ac ON ad.acid = ac.acid
	where 
	  ad.switch = 'Y' 
	  and ad.used = 'Y' 
	  and ac.switch = 'Y' 
	  and ac.used = 'Y' 
	  and NOW() between ad.ontime and ad.offtime 
	  and ac.place = '5'
    order by ad.seq asc ";

//取得資料
$table2 = $this->model->getQueryRecord($query2);

//Update End
##############################################################################################################################################
$redis = new Redis();
$redis->connect('sajacache01',6379);
$redis->del("SiteHomeAdPopupsList");

##############################################################################################################################################
// Log Start 
$ad_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ad_popups_update', 
	`active` = '首頁彈窗修改寫入', 
	`memo` = '{$ad_data}', 
	`root` = 'ad_popups/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
		   
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));