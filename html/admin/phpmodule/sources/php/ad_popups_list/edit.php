<?php
// Check Variable Start
if (empty($this->io->input["get"]["adid"])) {
	$this->jsAlertMsg('廣告編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}ad`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `adid` = '{$this->io->input["get"]["adid"]}'
	AND `acid` = '7'	
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query = "
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}ad_category` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND `acid` = '7'	
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["ad_category"] = $recArr['table']['record'];
 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$ad_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ad_popups_edit', 
	`active` = '首頁彈窗修改', 
	`memo` = '{$ad_data}', 
	`root` = 'ad_popups/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();