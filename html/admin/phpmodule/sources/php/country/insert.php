<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('敘述錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}country` SET
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='0', 
	`switch`='Y',
	`insertt`=now()
" ;
$this->model->query($query);
$countryid = $this->model->_con->insert_id;

//Relation country_language_rt
if($this->io->input["post"]["langid"])
{
	foreach($this->io->input["post"]["langid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}country_language_rt` SET
		`langid`='{$rv}',
		`countryid` = '{$countryid}',
		`prefixid`='{$this->config->default_prefix_id}',
		`seq`='".(($rk+1)*10)."', 
		`switch`='Y',
		`insertt`=NOW() ";
		$this->model->query($query);
	}
}

// Insert End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$country_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'country_insert', 
	`active` = '國別新增寫入', 
	`memo` = '{$country_data}', 
	`root` = 'country/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));