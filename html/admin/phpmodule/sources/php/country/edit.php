<?php
// Check Variable Start
if (empty($this->io->input["get"]["countryid"])) {
	$this->jsAlertMsg('國別ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}country`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `countryid` = '{$this->io->input["get"]["countryid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
 
$query ="
SELECT l.*, lr.langid languageid
FROM `{$db_channel}`.`{$this->config->default_prefix}language` l 
LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}country_language_rt` lr ON 
	l.prefixid = lr.prefixid
	AND l.langid = lr.langid
	AND lr.countryid = '{$this->io->input["get"]["countryid"]}'
	AND lr.switch = 'Y'
WHERE 
l.prefixid = '{$this->config->default_prefix_id}' 
AND l.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["language"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$country_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'country_edit', 
	`active` = '國別修改', 
	`memo` = '{$country_data}', 
	`root` = 'country/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();