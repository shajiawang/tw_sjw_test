<?php
// Check Variable Start
if (empty($this->io->input["post"]["countryid"])) {
	$this->jsAlertMsg('國別ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}country` SET 
`name` = '{$this->io->input["post"]["name"]}',
`description` = '{$this->io->input["post"]["description"]}'
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `countryid` = '{$this->io->input["post"]["countryid"]}'
" ;
$this->model->query($query);


//Relation country_language_rt

$query = "UPDATE `{$db_channel}`.`{$this->config->default_prefix}country_language_rt` 
SET `switch` = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `countryid` = '{$this->io->input["post"]["countryid"]}'
";
$this->model->query($query);

if($this->io->input["post"]["langid"])
{
	foreach($this->io->input["post"]["langid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}country_language_rt` SET
		`langid`='{$rv}',
		`countryid` = '{$this->io->input["post"]["countryid"]}',
		`prefixid`='{$this->config->default_prefix_id}',
		`seq`='".(($rk+1)*10)."', 
		`switch`='Y',
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query);
	}
}

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$country_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'country_update', 
	`active` = '國別修改寫入', 
	`memo` = '{$country_data}', 
	`root` = 'country/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));