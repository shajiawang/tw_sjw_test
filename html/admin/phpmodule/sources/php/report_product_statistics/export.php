<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

//搜尋條件
$sub_search_query = "";
$sub_having_query = "";
$sub_group_query = " group by p.productid";
$sub_sort_query =  " ORDER BY p.productid";
if(isset($data["productid"]) && is_numeric($data["productid"])){				//商品代碼
	$sub_search_query .=  "
		AND p.`productid` = '".$data["productid"]."'";
}
if(!empty($data["pdtname"])){													//商品名稱
	$sub_search_query .=  "
		AND p.`name` like '%".$data["pdtname"]."%'";
}
if(!empty($data["ontimefrom"])){												//商品上架開始日期
	$sub_search_query .=  "
		AND `ontime` >= '".$data["ontimefrom"]."'";
}
if(!empty($data["ontimeto"])){													//商品上架結束日期
	$sub_search_query .=  "
		AND `ontime` < '".$data["ontimeto"]."'";
}
if(!empty($data["offtimefrom"])){												//商品流/結標起始日期
	$sub_search_query .=  "
		AND `offtime` >= '".$data["offtimefrom"]."'";
}
if(!empty($data["offtimeto"])){													//商品流/結標結束日期
	$sub_search_query .=  "
		AND `offtime` < '".$data["offtimeto"]."'";
}
if(!empty($data["closed"])){													//流/結標
	if($data["closed"] != 'all'){
		$sub_search_query .=  "
			AND p.`closed` = '".$data["closed"]."'";
	}else{
		if ($this->io->input['session']['user']['userid'] > 3) {	
			$sub_search_query .= " AND p.`closed` != 'N'";
		}	
	}	
} else {
	if ($this->io->input['session']['user']['userid'] > 3) {	
		$sub_search_query .= " AND p.`closed` != 'N'";
	}	
}
if(isset($data["price"]) && is_numeric($data["price"])){						//得標金額
	$sub_search_query .=  "
		AND `price` = '".$data["price"]."'";
}
if(isset($data["retail_price"]) && is_numeric($data["retail_price"])){			//商品市價
	$sub_search_query .=  "
		AND `retail_price` = '".$data["retail_price"]."'";
}
if(!empty($data["ptype"])){														//商品類型
	$sub_search_query .=  "
		AND p.`ptype` = '".$data["ptype"]."'";
}
if (isset($data["history_count"]) && is_numeric($data["history_count"])) {		//下標次數
	$sub_having_query .=  "
		having `history_count` = '".$data["history_count"]."'";
}
if (!empty($data["userid"])) {													//會員編號
	$sub_search_query .=  "
		AND `userid` = '".$data["userid"]."'";
}



$query = "
select p.productid, p.name pdtname, str_to_date(ifnull(p.ontime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') ontime, str_to_date(ifnull(p.offtime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') offtime, 
p.closed, ifnull(pgp.price, 0) price, ifnull(p.saja_fee, 0) saja_fee, ifnull(p.retail_price, 0) retail_price, p.ptype, p.process_fee, p.totalfee_type, 
(select count(*) from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
where 
	h.prefixid = p.prefixid 
	and h.productid = p.productid 
	and h.type = 'bid'
	and h.switch = 'Y'
) history_count, u.name username, u.userid, pgp.complete, 
(select (ifnull(count(h.shid) * p.saja_fee, 0) + pgp.price + p.retail_price * p.process_fee * 0.01) 
from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h on 
	pgp.prefixid = h.prefixid 
	and pgp.productid = h.productid 
 	and pgp.userid = h.userid 
 	and h.spointid != 0 
 	and h.type = 'bid' 
	and h.switch = 'Y'
where 
 	pgp.prefixid = p.prefixid 
	and pgp.productid = p.productid 
	and pgp.switch = 'Y'
) bidcost 
from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp on 
	p.prefixid = pgp.prefixid
	and p.productid = pgp.productid
	and pgp.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u on 
	u.prefixid = pgp.prefixid
	and u.userid = pgp.userid
	and u.switch = 'Y'
where 
	p.prefixid = '".$this->config->default_prefix_id."'
	and p.switch = 'Y'
";
$query .= $sub_search_query;
$query .= $sub_group_query;
$query .= $sub_having_query;
$query .= $sub_sort_query;
$table = $this->model->getQueryRecord($query);

foreach ($table['table']['record'] as $key => $value) {
	
	//SQL指令
	$query_record = " SELECT SUM(h.price) as totalprice, SUM(p.saja_fee) as totalfee 
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
						JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
						ON h.prefixid = p.prefixid AND h.productid = p.productid AND p.switch = 'Y'
						WHERE 
							h.prefixid = '".$this->config->default_prefix_id."'
							AND h.userid = '{$value['userid']}'
							AND h.productid = '{$value['productid']}'
							AND h.switch = 'Y'
							AND p.productid IS NOT NULL
							AND h.type='bid'
							AND h.spointid > 0 
						GROUP BY h.productid
					";
	//取得資料
	$table2 = $this->model->getQueryRecord($query_record);
	$total_data = $table2['table']['record'][0];
	
	switch($value['totalfee_type']) {
		case 'A':
			$saja_fee_amount = $total_data['totalprice']+$total_data['totalfee'];
			break;
		case 'B':
			$saja_fee_amount = $total_data['totalprice'];
			break;			  
		case 'O':
			$saja_fee_amount = 0;
			break;
		case 'F':
			$saja_fee_amount = $total_data['totalfee'];
			break;
		default:
			$saja_fee_amount = $total_data['totalfee'];
			break;			  
	}

	$table['table']['record'][$key]['alltotal'] = ceil($saja_fee_amount);
	
	
	//SQL指令
	$query_record2 = " SELECT SUM(h.price) as totalprice, SUM(p.saja_fee) as totalfee 
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
						JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
						ON h.prefixid = p.prefixid AND h.productid = p.productid AND p.switch = 'Y'
						WHERE 
							h.prefixid = '".$this->config->default_prefix_id."'
							AND h.productid = '{$value['productid']}'
							AND h.switch = 'Y'
							AND p.productid IS NOT NULL
							AND h.type='bid'
							AND h.spointid > 0 
						GROUP BY h.productid
					";
	//取得資料
	$table3 = $this->model->getQueryRecord($query_record2);
	$every_total_data = $table3['table']['record'][0];
	
	switch($value['totalfee_type']) {
		case 'A':
			$saja_fee_amount2 = $every_total_data['totalprice']+$every_total_data['totalfee'];
			break;
		case 'B':
			$saja_fee_amount2 = $every_total_data['totalprice'];
			break;			  
		case 'O':
			$saja_fee_amount2 = 0;
			break;
		case 'F':
			$saja_fee_amount2 = $every_total_data['totalfee'];
			break;
		default:
			$saja_fee_amount2 = $every_total_data['totalfee'];
			break;			  
	}

	$table['table']['record'][$key]['everyalltotal'] = ceil($saja_fee_amount2);	
	
	
	//SQL指令
	$query_record3 = " SELECT COUNT(h.shid) as paycount
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
						JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
						ON h.prefixid = p.prefixid AND h.productid = p.productid AND p.switch = 'Y'
						WHERE 
							h.prefixid = '".$this->config->default_prefix_id."'
							AND h.productid = '{$value['productid']}'
							AND h.switch = 'Y'
							AND p.productid IS NOT NULL
							AND h.type='bid'
							AND h.spointid > 0 						
					";
	//取得資料
	$table4 = $this->model->getQueryRecord($query_record3);
	
	$table['table']['record'][$key]['paycount'] = $table4['table']['record'][0]['paycount'];		
	
}


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_product_statistics', 
	`active` = '下標商品統計清單匯出', 
	`memo` = '{$report_data}', 
	`root` = 'report_product_statistics/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//下標商品資料表
$Report_Array[0]['title'] = array("A1" => "商品代碼", "B1" => "商品名稱", "C1" => "商品上架日期", "D1" => "商品結標日期", "E1" => "流/結標", "F1" => "得標金額", "G1" => "商品市價", "H1" => "商品類型", "I1" => "商品總下標次數", "J1" => "會員帳號", "K1" => "得標者成本", "L1" => "得標處理費", "M1" => "得標者下標手續費","N1" => "單次下標手續費","O1" => "總下標手續費","P1" => "付費下標數","Q1" => "結帳狀態");
//流/結標
$closedArr = array('Y' => '結標', 'NB' => '流標', 'N' => '未結標');
//未結/結帳
$completeArr = array('Y' => '已結帳', 'N' => '未結帳');

$ptypeArr = array('0' => '殺戮商品', '1' => '圓夢商品');

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
		$bid_count = round($value['retail_price'] * $value['process_fee'] * 0.01);
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['productid']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['pdtname']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['ontime']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['offtime']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $closedArr[$value['closed']]);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['price']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['retail_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $ptypeArr[$value['ptype']]);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['history_count']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$row, (string)$value['username'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, round($value['bidcost']));
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $bid_count);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $value['alltotal']);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $value['saja_fee']);
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $value['everyalltotal']);
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$row, $value['paycount']);
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$row, $completeArr[$value['complete']]);
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('下標商品統計');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="下標商品統計'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
		
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>