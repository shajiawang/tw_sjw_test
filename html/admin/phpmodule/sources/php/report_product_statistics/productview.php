<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
if (empty($this->io->input['get']['productid'])) {
	$this->jsAlertMsg('商品ID錯誤!!');
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 							
									


// Table Record Start
$query = "
select p.productid, p.name, group_concat(pc.name) pcname, p.description, p.ontime, p.offtime, p.retail_price, 
p.saja_fee, p.process_fee, p.closed, p.insertt 
from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt` pcr on 
	p.prefixid = pcr.prefixid 
	and p.productid = pcr.productid
	and pcr.switch = 'Y'
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` pc on 
	pcr.prefixid = pc.prefixid 
	and pcr.pcid = pc.pcid
	and pc.switch = 'Y'
where 
	p.prefixid = '".$this->config->default_prefix_id."' 
	and p.productid = '".$this->io->input['get']['productid']."'
	and p.switch = 'Y'
";
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_product_statistics', 
	`active` = '下標商品統計商品資料查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_product_statistics/productview', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status['status']['closed'] = array('Y' => '結標', 'NB' => '流標', 'N' => '未結標');			//結/流標

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();