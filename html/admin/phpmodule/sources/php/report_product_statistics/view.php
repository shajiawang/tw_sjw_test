<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Group Start
$sub_group_query = " group by p.productid";
// Group End


// Sort Start
$sub_sort_query =  " ORDER BY p.offtime DESC, p.productid DESC";
// Sort End


// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
$sub_having_query = "";
if(isset($this->io->input["get"]["productid"]) && is_numeric($this->io->input["get"]["productid"])){				//商品代碼
	$status["status"]["search"]["productid"] = $this->io->input["get"]["productid"] ;
	$status["status"]["search_path"] .= "&productid=".$this->io->input["get"]["productid"] ;
	$sub_search_query .=  "
		AND p.`productid` = '".$this->io->input["get"]["productid"]."'";
}
if(!empty($this->io->input["get"]["pdtname"])){																		//商品名稱
	$status["status"]["search"]["pdtname"] = $this->io->input["get"]["pdtname"] ;
	$status["status"]["search_path"] .= "&pdtname=".$this->io->input["get"]["pdtname"] ;
	$sub_search_query .=  "
		AND p.`name` like '%".$this->io->input["get"]["pdtname"]."%'";
}
if(!empty($this->io->input["get"]["ontimefrom"])){																	//商品上架開始日期
	$status["status"]["search"]["ontimefrom"] = $this->io->input["get"]["ontimefrom"] ;
	$status["status"]["search_path"] .= "&ontimefrom=".$this->io->input["get"]["ontimefrom"] ;
	$sub_search_query .=  "
		AND `ontime` >= '".$this->io->input["get"]["ontimefrom"]."'";
}
if(!empty($this->io->input["get"]["ontimeto"])){																	//商品上架結束日期
	$status["status"]["search"]["ontimeto"] = $this->io->input["get"]["ontimeto"] ;
	$status["status"]["search_path"] .= "&ontimeto=".$this->io->input["get"]["ontimeto"] ;
	$sub_search_query .=  "
		AND `ontime` < '".$this->io->input["get"]["ontimeto"]."'";
}
if(!empty($this->io->input["get"]["offtimefrom"])){																	//商品流/結標起始日期
	$status["status"]["search"]["offtimefrom"] = $this->io->input["get"]["offtimefrom"] ;
	$status["status"]["search_path"] .= "&offtimefrom=".$this->io->input["get"]["offtimefrom"] ;
	$sub_search_query .=  "
		AND `offtime` >= '".$this->io->input["get"]["offtimefrom"]."'";
}
if(!empty($this->io->input["get"]["offtimeto"])){																	//商品流/結標結束日期
	$status["status"]["search"]["offtimeto"] = $this->io->input["get"]["offtimeto"] ;
	$status["status"]["search_path"] .= "&offtimeto=".$this->io->input["get"]["offtimeto"] ;
	$sub_search_query .=  "
		AND `offtime` < '".$this->io->input["get"]["offtimeto"]."'";
}
if(!empty($this->io->input["get"]["closed"])){																		//流/結標
	$status["status"]["search"]["closed"] = $this->io->input["get"]["closed"] ;
	$status["status"]["search_path"] .= "&closed=".$this->io->input["get"]["closed"] ;
	if($this->io->input["get"]["closed"] != 'all'){
		$sub_search_query .=  "
			AND p.`closed` = '".$this->io->input["get"]["closed"]."'";
	}else{
		if ($this->io->input['session']['user']['userid'] > 3) {	
			$sub_search_query .= " AND p.`closed` != 'N'";
		}	
	}	
} else {
	if ($this->io->input['session']['user']['userid'] > 3) {	
		$sub_search_query .= " AND p.`closed` != 'N'";
	}	
}

if(isset($this->io->input["get"]["price"]) && is_numeric($this->io->input["get"]["price"])){						//得標金額
	$status["status"]["search"]["price"] = $this->io->input["get"]["price"] ;
	$status["status"]["search_path"] .= "&price=".$this->io->input["get"]["price"] ;
	$sub_search_query .=  "
		AND `price` = '".$this->io->input["get"]["price"]."'";
}
if(isset($this->io->input["get"]["retail_price"]) && is_numeric($this->io->input["get"]["retail_price"])){			//商品市價
	$status["status"]["search"]["retail_price"] = $this->io->input["get"]["retail_price"] ;
	$status["status"]["search_path"] .= "&retail_price=".$this->io->input["get"]["retail_price"] ;
	$sub_search_query .=  "
		AND `retail_price` = '".$this->io->input["get"]["retail_price"]."'";
}
if(!empty($this->io->input["get"]["ptype"])){																		//商品類型
	$status["status"]["search"]["ptype"] = $this->io->input["get"]["ptype"] ;
	$status["status"]["search_path"] .= "&ptype=".$this->io->input["get"]["ptype"] ;
	if($this->io->input["get"]["ptype"] != 'all'){
		$sub_search_query .=  "
			AND p.`ptype` = '".$this->io->input["get"]["ptype"]."'";
	}	
}
if (isset($this->io->input["get"]["history_count"]) && is_numeric($this->io->input["get"]["history_count"])) {		//下標次數
	$status["status"]["search"]["history_count"] = $this->io->input["get"]["history_count"] ;
	$status["status"]["search_path"] .= "&history_count=".$this->io->input["get"]["history_count"] ;
	$sub_having_query .=  "
		having `history_count` >= '".$this->io->input["get"]["history_count"]."'";
		$sub_sort_query =  " ORDER BY history_count DESC, p.offtime DESC, p.productid DESC ";
}
if (!empty($this->io->input["get"]["userid"])) {																	//會員編號
	$status["status"]["search"]["userid"] = $this->io->input["get"]["userid"] ;
	$status["status"]["search_path"] .= "&userid=".$this->io->input["get"]["userid"] ;
	$sub_search_query .=  "
		AND up.`userid` = '".$this->io->input["get"]["userid"]."'";
}
if(isset($this->io->input["get"]["complete"]) && is_numeric($this->io->input["get"]["complete"])){			//訂單結帳
	$status["status"]["search"]["complete"] = $this->io->input["get"]["complete"] ;
	$status["status"]["search_path"] .= "&complete=".$this->io->input["get"]["complete"] ;
	$sub_search_query .=  "
		AND pgp.complete = '".$this->io->input["get"]["complete"]."'";
}
// Search End


// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

if (isset($this->io->input["get"]['ptype'])){
// Table Count Start 
$query ="
select count(ps.num) num from 
(select count(*) num, 
(select count(*) from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
where 
	h.prefixid = p.prefixid 
	and h.productid = p.productid 
	and h.type = 'bid'
	and h.switch = 'Y'
) history_count 
from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp on 
	p.prefixid = pgp.prefixid
	and p.productid = pgp.productid
	and pgp.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on 
	pgp.userid = up.userid
	and up.switch = 'Y'	
where 
	p.prefixid = '".$this->config->default_prefix_id."'
	and p.switch = 'Y'

" ;
$query .= $sub_search_query ; 
$query .= $sub_group_query;
$query .= $sub_having_query;
$query .= ") ps";
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 


// Table Record Start
$query = "
select p.productid, p.name pdtname, str_to_date(ifnull(p.ontime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') ontime, str_to_date(ifnull(p.offtime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') offtime, 
p.closed, ifnull(pgp.price, 0) price, ifnull(p.retail_price, 0) retail_price, p.hot_prod, p.process_fee, p.totalfee_type, p.ptype, pgp.userid, up.nickname, pgp.complete, 
(select count(*) from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
where 
	h.prefixid = p.prefixid 
	and h.productid = p.productid 
	and h.type = 'bid'
	and h.switch = 'Y'
) history_count, 
(select (ifnull(count(h.shid) * p.saja_fee, 0) + pgp.price + p.retail_price * p.process_fee * 0.01) 
from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h on 
	pgp.prefixid = h.prefixid 
	and pgp.productid = h.productid 
 	and pgp.userid = h.userid 
 	and h.spointid != 0 
 	and h.type = 'bid' 
	and h.switch = 'Y'
where 
 	pgp.prefixid = p.prefixid 
	and pgp.productid = p.productid 
	and pgp.switch = 'Y'
) bidcost, pgp.orderid, pt.filename as thumbnail 
from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
left join (
	select pgp.*, o.orderid 
	from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
	left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o on 
		pgp.pgpid = o.pgpid 
		AND pgp.userid = o.userid 
) pgp on 
	p.prefixid = pgp.prefixid
	and p.productid = pgp.productid
	and pgp.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on 
	pgp.userid = up.userid
	and up.switch = 'Y'	
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
where 
	p.prefixid = '".$this->config->default_prefix_id."'
	and p.switch = 'Y'
";
$query .= $sub_search_query ;
$query .= $sub_group_query;
$query .= $sub_having_query;
$query .= $sub_sort_query;
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
var_dump($query);
foreach ($table['table']['record'] as $key => $value) {
	
	//SQL指令
	$query_record = " SELECT SUM(h.price) as totalprice, SUM(p.saja_fee) as totalfee 
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
						JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
						ON h.prefixid = p.prefixid AND h.productid = p.productid AND p.switch = 'Y'
						WHERE 
							h.prefixid = '".$this->config->default_prefix_id."'
							AND h.userid = '{$value['userid']}'
							AND h.productid = '{$value['productid']}'
							AND h.switch = 'Y'
							AND p.productid IS NOT NULL
							AND h.type='bid'
							AND h.spointid > 0 
						GROUP BY h.productid
					";
	//取得資料
	$table2 = $this->model->getQueryRecord($query_record);
	$total_data = $table2['table']['record'][0];
	
	switch($value['totalfee_type']) {
		case 'A':
			$saja_fee_amount = $total_data['totalprice']+$total_data['totalfee'];
			break;
		case 'B':
			$saja_fee_amount = $total_data['totalprice'];
			break;			  
		case 'O':
			$saja_fee_amount = 0;
			break;
		case 'F':
			$saja_fee_amount = $total_data['totalfee'];
			break;
		default:
			$saja_fee_amount = $total_data['totalfee'];
			break;			  
	}

	$table['table']['record'][$key]['alltotal'] = ceil($saja_fee_amount);

}
}else{
	$table['table']=array();
	$table['page']=0;
}
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_product_statistics', 
	`active` = '下標商品統計清單查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_product_statistics/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

if ($this->io->input['session']['user']['userid'] > 3) {	
	$status['status']['closed'] = array('Y' => '結標', 'NB' => '流標');			//結/流標
} else {
	$status['status']['closed'] = array('Y' => '結標', 'NB' => '流標', 'N' => '未結標');			//結/流標
}

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status', $status["status"]);
$this->tplVar('pcArr', $pcArr);
$this->display();