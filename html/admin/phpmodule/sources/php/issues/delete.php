<?php
if (empty($this->io->input["get"]["uiid"])) {
	$this->jsAlertMsg('自動回覆ID錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$query = "UPDATE ".$this->config->db[0]["dbname"].".".$this->config->default_prefix."user_issues SET 
	`switch`='N',`modifyt`=NOW()
WHERE `uiid` = '".$this->io->input["get"]["uiid"]."' 
";
$this->model->query($query);

##############################################################################################################################################
// Log Start 
$issues_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_delete', 
	`active` = '客服留言管理刪除', 
	`memo` = '{$issues_data}', 
	`root` = 'issues/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// INSERT success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input["get"]["location_url"]);
// INSERT success message End