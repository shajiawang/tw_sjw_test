<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (!is_array($this->io->input["post"]["keyword"])) {
	$this->jsAlertMsg('自動回覆關鍵字錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('自動回覆名稱錯誤!!');
}
if (!is_array($this->io->input["post"]["rcid"]) || empty($this->io->input["post"]["rcid"])) {
	$this->jsAlertMsg('自動回覆分類錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('自動回覆敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('自動回覆排序錯誤!!');
}

$keyword = implode(",", array_filter($this->io->input["post"]["keyword"]));
$description = htmlspecialchars($this->io->input["post"]["description"]);

$query ="
INSERT INTO `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}response` 
SET
	`prefixid`		='{$this->config->default_prefix_id}',
	`name`			='{$this->io->input["post"]["name"]}',
	`description`	='{$description}',
	`keyword`       ='{$keyword}', 
	`msgtype`       ='{$this->io->input["post"]["msgtype"]}',  
	`thumbnail_url`	='{$this->io->input["post"]["thumbnail_url"]}',
	`url`	        ='{$this->io->input["post"]["url"]}',
	`seq`			='{$this->io->input["post"]["seq"]}',
	`insertt`		=now()
" ;
// echo '<pre>';echo $query;exit;
$this->model->query($query);
$responseid = $this->model->_con->insert_id;

//寫入自動回覆分類rt
$values = array();
foreach($this->io->input["post"]["rcid"] as $rk => $rv) {
	$values[] = "
		('".$this->config->default_prefix_id."', '".$responseid."', '".$rv."', '".(($rk+1)*10)."', NOW())";
}
$query = "
INSERT INTO `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt`(
	`prefixid`,
	`responseid`,
	`rcid`,
	`seq`, 
	`insertt`
)
VALUES
".implode(',', $values); 
$this->model->query($query);


##############################################################################################################################################
// Log Start 
$issues_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_insert', 
	`active` = '客服留言管理新增寫入', 
	`memo` = '{$issues_data}', 
	`root` = 'issues/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));