<?php
if (empty($this->io->input["get"]["uiid"])) {
	$this->jsAlertMsg('回覆ID錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT ui.*, uir.uirid, uir.content as reply_content 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues` ui 
left outer join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_reply` uir on ui.uiid = uir.uiid 
and ui.switch = 'Y'  
WHERE 
ui.uiid = '".$this->io->input["get"]["uiid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT uic.* 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_category` uic 
WHERE uic.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["user_issues_category"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$issues_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_edit', 
	`active` = '客服留言管理修改', 
	`memo` = '{$issues_data}', 
	`root` = 'issues/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']);
$this->tplVar('msgtype_array' , $msgtype_array);
//$this->tplVar('status',$status["status"]);
$this->display();