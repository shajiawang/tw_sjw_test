<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["uiid"])) {
	$this->jsAlertMsg('ID錯誤!!');
}

if (empty($this->io->input["post"]["replycontent"])) {
	$this->jsAlertMsg('回覆內容 不可空白!!');
}else{
	$description = htmlspecialchars($this->io->input["post"]["replycontent"]);
}
$adminid = $this->io->input['session']['admin_user']["userid"];

//判斷是否有回覆過
if (!empty($this->io->input["post"]["uirid"])){
	$uirid = $this->io->input["post"]["uirid"];
	
	$query ="UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_issues_reply` SET
		`content`	= '{$description}',
		`modifyt`   = NOW()
	WHERE
		`uirid` = '{$uirid}'
	" ;
	$this->model->query($query);

}else{
	
	$query ="INSERT INTO `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_issues_reply` 
	SET
		`uiid`			='{$this->io->input["post"]["uiid"]}',
		`content`		='{$description}',
		`userid`       	='0',
		`adminid`		='{$adminid}',
		`status`        ='2',
		`readtime`		= null, 
		`insertt`		= now()
	" ;
	$this->model->query($query);
	$uirid = $this->model->_con->insert_id;
}

if (!empty($uirid)){
	$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_issues` SET";
	$query .= "`status`	= '2',
		closetime= null,
		`modifyt`   = NOW()
	WHERE
		`uiid` = '{$this->io->input["post"]["uiid"]}'
	" ;
	$this->model->query($query);
}

if (!empty($this->io->input['files']['rpfid']) && exif_imagetype($this->io->input['files']['rpfid']['tmp_name'])) {
	
	$files = $this->io->input['files']['rpfid'];
	$file_error = $files["error"];//上傳後系統返回的值 
	
	if(is_uploaded_file($files['tmp_name'])){
	
		$file_name = ".". pathinfo($files['name'], PATHINFO_EXTENSION);
		$filename = md5(date("YmdHis") ."_". $this->io->input['post']['uiid']) .$file_name;
		$path = '/var/www/html/site/images/site/issues';
		
		if (file_exists($path.'/'.$filename) && ($filename != $this->io->input['post']['oldrpfid'])) {
			$this->jsAlertMsg('附圖圖名稱重覆!!');
		}
		elseif($file_error!='0'){
			$this->jsAlertMsg('附圖圖上傳錯誤!!');
		}
			
			move_uploaded_file($files['tmp_name'], $path.'/'.$filename);
			
			//刪除舊圖
			if (!empty($this->io->input['post']['oldrpfid']) && ($filename != $this->io->input['post']['oldrpfid'])) {
				unlink($path.'/'.$this->io->input['post']['oldrpfid']);
			}

			$query = "UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_issues_reply` SET
					`rpfid`='{$filename}'
				WHERE
					uirid = '{$uirid}'
			";
			$this->model->query($query);

			//if($is_test == '0'){
			//	syncToS3($path.'/'.$filename, 's3://img.saja.com.tw','/site/images/site/issues/');
			//}
	}
}

##############################################################################################################################################
// Log Start 
$issues_reply_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_reply_update', 
	`active` = '客服留言管理回覆修改寫入', 
	`memo` = '{$issues_reply_data}', 
	`root` = 'issues_reply/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
$location_url = $this->config->default_main .'/'. $this->io->input["get"]["fun"] .'/reply/uiid='. $this->io->input["post"]["uiid"];

//header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
header("location:". $location_url);