<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];


if (empty($this->io->input["post"]["uiid"])) {
	$this->jsAlertMsg('ID錯誤!!');
}

$is_close = $this->io->input["post"]["isclose"];
$isclose = empty($is_close) ? 'N' : 'Y';

	$query ="UPDATE `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user_issues` SET";
	
	if($isclose=='Y'){ 
		$query .= "`status`	= '3',closetime= NOW(),";
	}else{
		$query .= "`status`	= '2',closetime= null,";
	}
	
	$query .= " `uicid`	= '{$this->io->input["post"]["category"]}',
		`switch` = '{$this->io->input["post"]["switch"]}',
		`modifyt` = NOW()
		WHERE uiid = '{$this->io->input["post"]["uiid"]}'
	";
	$this->model->query($query);

##############################################################################################################################################
// Log Start 
$issues_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_update', 
	`active` = '客服留言管理修改寫入', 
	`memo` = '{$issues_data}', 
	`root` = 'issues/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));