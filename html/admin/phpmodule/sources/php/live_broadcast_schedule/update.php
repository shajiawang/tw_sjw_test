<?php
// Check Variable Start
if (empty($this->io->input["post"]["lbsid"])) {
	$this->jsAlertMsg('直播時間表ID錯誤!!');
}
if (empty($this->io->input["post"]["lbuserid"])) {
	$this->jsAlertMsg('直播主ID錯誤!!');
}
if (empty($this->io->input["post"]["lbid"])) {
	$this->jsAlertMsg('直播間ID錯誤!!');
}
if (empty($this->io->input["post"]["ontime"])) {
	$this->jsAlertMsg('直播間開始時間錯誤!!');
}
if (empty($this->io->input["post"]["offtime"])) {
	$this->jsAlertMsg('直播間結束時間錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_schedule` SET 
	`lbuserid` = '{$this->io->input["post"]["lbuserid"]}',
	`lbid` = '{$this->io->input["post"]["lbid"]}', 
	`ontime` = '{$this->io->input["post"]["ontime"]}', 
	`offtime` = '{$this->io->input["post"]["offtime"]}', 	
	`seq` = '{$this->io->input["post"]["seq"]}'
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `lbsid` = '{$this->io->input["post"]["lbsid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$broadcast_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'live_broadcast_schedule_update', 
	`active` = '直播時間表修改寫入', 
	`memo` = '{$broadcast_data}', 
	`root` = 'live_broadcast_schedule/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));