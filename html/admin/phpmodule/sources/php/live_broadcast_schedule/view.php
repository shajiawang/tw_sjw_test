<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(lbsid|name|seq|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}


if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";

if($this->io->input["get"]["search_lbuserid"] != ''){
	$status["status"]["search"]["search_lbuserid"] = $this->io->input["get"]["search_lbuserid"] ;
	$status["status"]["search_path"] .= "&search_lbuserid=".$this->io->input["get"]["search_lbuserid"] ;
	$sub_search_query .=  "AND lbs.`lbuserid` ='{$this->io->input["get"]["search_lbuserid"]}' ";
}
if($this->io->input["get"]["search_lbid"] != ''){
	$status["status"]["search"]["search_lbid"] = $this->io->input["get"]["search_lbid"] ;
	$status["status"]["search_path"] .= "&search_lbid=".$this->io->input["get"]["search_lbid"] ;
	$sub_search_query .=  "AND lbs.`lbid` ='{$this->io->input["get"]["search_lbid"]}' ";
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	$sub_search_query .=  "
		AND lbs.`ontime` >= '".$this->io->input["get"]["search_btime"]."' AND lbs.`offtime` <= '".$this->io->input["get"]["search_etime"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="
SELECT count(*) as num 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_schedule` lbs
LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user` lbu ON
	lbs.prefixid = lbu.prefixid
	AND lbs.lbuserid = lbu.lbuserid
LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}live_broadcast` lb ON
	lbs.prefixid = lb.prefixid 
	AND lbs.lbid = lb.lbid 		
WHERE 
	lbs.`prefixid` = '{$this->config->default_prefix_id}' 
	AND lbs.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT lbs.*, lbu.lbname, lb.name  
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_schedule` lbs 
LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user` lbu ON
	lbs.prefixid = lbu.prefixid 
	AND lbs.lbuserid = lbu.lbuserid 
LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}live_broadcast` lb ON
	lbs.prefixid = lb.prefixid 
	AND lbs.lbid = lb.lbid 	
WHERE 
	lbs.`prefixid` = '{$this->config->default_prefix_id}' 
	AND lbs.`switch`='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query);  
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

$query ="
SELECT lbuserid, lbname 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["live_broadcast_user"] = $recArr['table']['record'];


$query ="
SELECT lbid, name 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["live_broadcast"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$broadcast_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'live_broadcast_schedule', 
	`active` = '直播時間表清單查詢', 
	`memo` = '{$broadcast_data}', 
	`root` = 'live_broadcast_schedule/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();