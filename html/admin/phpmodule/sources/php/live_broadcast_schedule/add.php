<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

//直播主
$query ="
SELECT lbuserid, lbname 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["live_broadcast_user"] = $recArr['table']['record'];

//直播間
$query ="
SELECT lbid, name, lbuserid 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["live_broadcast"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$broadcast_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'live_broadcast_schedule_add', 
	`active` = '直播時間表新增', 
	`memo` = '{$broadcast_data}', 
	`root` = 'live_broadcast_schedule/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();