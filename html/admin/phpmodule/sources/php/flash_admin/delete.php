<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

// Check Variable Start
if(empty($this->io->input["get"]["faid"])){
	$this->jsAlertMsg('參數錯誤,請聯絡技術人員!');
}
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

// DELETE database start
$query ="UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."flash_admin`
SET 
	switch = 'N'
WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND faid = '".$this->io->input["get"]["faid"]."'
" ;
$this->model->query($query);
// DELETE database end


##############################################################################################################################################
// Log Start 
$flash_admin_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'flash_admin_delete', 
	`active` = '閃殺管理帳號刪除', 
	`memo` = '{$flash_admin_data}', 
	`root` = 'flash_admin/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// DELETE success message Start
$this->jsPrintMsg('刪除成功!!',$this->io->input["get"]["location_url"]);
// DELETE success message End