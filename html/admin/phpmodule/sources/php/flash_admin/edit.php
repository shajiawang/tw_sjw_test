<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

if (empty($this->io->input["get"]["faid"])) {
	$this->jsAlertMsg('faid錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$query ="
SELECT * FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."flash_admin` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND faid = '".$this->io->input["get"]["faid"]."'
" ;
$table = $this->model->getQueryRecord($query);


##############################################################################################################################################
// Relation Start 
$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."country` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country"] = $recArr['table']['record'];

$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."region` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["region"] = $recArr['table']['record'];

$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."province` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["province"] = $recArr['table']['record'];

$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$flash_admin_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'flash_admin_edit', 
	`active` = '閃殺管理帳號修改', 
	`memo` = '{$flash_admin_data}', 
	`root` = 'flash_admin/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();