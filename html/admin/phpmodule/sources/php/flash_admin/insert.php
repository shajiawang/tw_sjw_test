<?php
/*if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
*/
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];


if (empty($this->io->input["post"]["loginname"])) {
	$this->jsAlertMsg('使用者帳號不可空白!!');
}
if (empty($this->io->input["post"]["passwd"])) {
	$this->jsAlertMsg('密碼不可空白!!');
}
if (empty($this->io->input["post"]["passwd_confirm"])) {
	$this->jsAlertMsg('密碼確認不可空白!!');
}
if ($this->io->input["post"]["passwd"] != $this->io->input["post"]["passwd_confirm"]) {
	$this->jsAlertMsg('密碼與密碼確認不同!!');
}
if (empty($this->io->input["post"]["fullname"])) {
	$this->jsAlertMsg('姓名不可空白!!');
}
if (empty($this->io->input["post"]["phone"])) {
	$this->jsAlertMsg('電話不可空白!!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('電子郵件不可空白!!');
}
if (!filter_var($this->io->input['post']['email'], FILTER_VALIDATE_EMAIL)){
	$this->jsAlertMsg('電子郵件格式錯誤!!');
}
if (empty($this->io->input["post"]["countryid"])) {
	$this->jsAlertMsg('國家不可空白!!');
}
if (empty($this->io->input["post"]["regionid"])) {
	$this->jsAlertMsg('區域不可空白!!');
}
if (empty($this->io->input["post"]["provinceid"])) {
	$this->jsAlertMsg('省(直轄市)不可空白!!');
}
if (empty($this->io->input["post"]["channelid"])) {
	$this->jsAlertMsg('經銷商(分區)不可空白!!');
}

$query ="
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."flash_admin`(
	`prefixid`,
	`loginname`,
	`passwd`, 
	`countryid`, 
	`regionid`, 
	`provinceid`,
	`channelid`,
	`email`, 
	`fullname`, 
	`phone`, 
	`seq`, 
	`insertt`
) 
VALUES(
	'".$this->config->default_prefix_id."',
	'".$this->io->input["post"]["loginname"]."',
	'".$this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key)."',
	'".$this->io->input['post']['countryid']."',
	'".$this->io->input["post"]["regionid"]."',
	'".$this->io->input['post']['provinceid']."',
	'".$this->io->input["post"]["channelid"]."',
	'".$this->io->input['post']['email']."',
	'".$this->io->input["post"]["fullname"]."',
	'".$this->io->input["post"]["phone"]."',
	'".$this->io->input["post"]["seq"]."',
	now()
)
" ;
//echo $query;exit;
$this->model->query($query);
//$userid = $this->model->_con->insert_id;


##############################################################################################################################################
// Log Start 
$flash_admin_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'flash_admin_insert', 
	`active` = '閃殺管理帳號新增寫入', 
	`memo` = '{$flash_admin_data}', 
	`root` = 'flash_admin/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


echo "success!!";

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));