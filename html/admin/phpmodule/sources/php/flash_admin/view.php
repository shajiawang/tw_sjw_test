<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(faid|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$join_search_query = "";

if($this->io->input["get"]["search_loginname"] != ''){
	$status["status"]["search"]["search_loginname"] = $this->io->input["get"]["search_loginname"] ;
	$status["status"]["search_path"] .= "&search_loginname=".$this->io->input["get"]["search_loginname"] ;
	
	$sub_search_query .=  "AND fa.`loginname` like '%".$this->io->input["get"]["search_loginname"]."%'
	";
}
if($this->io->input["get"]["search_fullname"] != ''){
	$status["status"]["search"]["search_fullname"] = $this->io->input["get"]["search_fullname"] ;
	$status["status"]["search_path"] .= "&search_fullname=".$this->io->input["get"]["search_fullname"] ;
	
	$sub_search_query .=  "AND fa.`fullname` like '%".$this->io->input["get"]["search_fullname"]."%'
	";
}
if($this->io->input["get"]["search_email"] != ''){
	$status["status"]["search"]["search_email"] = $this->io->input["get"]["search_email"] ;
	$status["status"]["search_path"] .= "&search_email=".$this->io->input["get"]["search_email"] ;
	
	$sub_search_query .=  "AND fa.`email` like '%".$this->io->input["get"]["search_email"]."%'
	";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."flash_admin` fa 
{$join_search_query}
WHERE 
fa.prefixid = '".$this->config->default_prefix_id."'
AND fa.switch='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="SELECT fa.faid, fa.loginname, fa.insertt, fa.modifyt, fa.loginname, fa.email, fa.fullname, 
(select name from `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` c where fa.prefixid = c.prefixid and fa.channelid = c.channelid and c.switch = 'Y') channel_name 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."flash_admin` fa
{$join_search_query}
WHERE 
fa.prefixid = '".$this->config->default_prefix_id."' 
AND fa.switch='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$flash_admin_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'flash_admin', 
	`active` = '閃殺管理帳號清單查詢', 
	`memo` = '{$flash_admin_data}', 
	`root` = 'flash_admin/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################
$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();