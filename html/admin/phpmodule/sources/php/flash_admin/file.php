<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start
$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';

if($files['file_name']["error"] != 0 || empty($files['file_name']["size"]) ) {
	$this->jsAlertMsg('上傳錯誤!!');
}
else
{
	$flash_admin_File = $this->config->path_sources."/flash_admin/".$files['file_name']['name']; 
	
    if(is_uploaded_file($files['file_name']['tmp_name']) ) {
		move_uploaded_file($files['file_name']['tmp_name'], "{$flash_admin_File}");
	}

	$handle = fopen($flash_admin_File, 'r+');
	if (is_resource($handle))
    {
        $insertt = date('Y-m-d H:i:s');
		$newData = array();
		
		$filesize = filesize($flash_admin_File);
		$data = fread($handle, $filesize);
		$data = mb_convert_encoding($data, "UTF-8", "big5");
		$newData = explode("\n", $data);
		//array_shift($newData);
		$newData = array_values(array_filter($newData));
		
		if (!empty($newData)) {
			foreach ($newData as $key => $value) {
				$dataArr[$key] = explode(",", $value);
			}
			//print_r($dataArr);exit;
			
			if (!empty($dataArr)) {
				//判斷是否空白
				foreach ($dataArr as $key => $value) {
					if ($key > 0) {
						foreach ($value as $key1 => $value1) {
							if ($value1 === "") {
								unlink($flash_admin_File);
								$this->jsAlertMsg($dataArr[0][$key1].' 請勿空白!!');
							}
						}
					}
				}
				
				//將第一列資料清除
				array_shift($dataArr);
				$dataArr = array_values(array_filter($dataArr));
				
				foreach ($dataArr as $key => $value) {
			        //查詢地推人員帳號
			        $query = "SELECT faid 
			        	FROM `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}flash_admin` 
			        	WHERE 
			        		prefixid = '{$this->config->default_prefix_id}' 
			        		AND loginname = '{$value[0]}'
			        		and switch = 'Y'
			        ";
			        $table = $this->model->getQueryRecord($query);
			        if (!empty($table['table']['record'][0]["faid"])) {
			        	unlink($flash_admin_File);
			        	$this->jsAlertMsg('使用者帳號重覆，請重新填寫!!');
			        }
			        
			        //新增地推人員資料
			        $query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}flash_admin` 
			        set 
			        	prefixid = '{$this->config->default_prefix_id}', 
			        	loginname = '{$value[0]}', 
			        	passwd = '{$this->str->strEncode($value[1], $this->config->encode_key)}', 
			        	countryid = '{$value[5]}',
			        	regionid = '{$value[6]}',
			        	provinceid = '{$value[7]}', 
			        	channelid = '{$value[8]}',
			        	fullname = '{$value[2]}',
			        	phone = '{$value[3]}',
			        	email = '{$value[4]}', 
			        	verified = 'Y', 
			        	seq = 0, 
			        	switch = 'Y',
			        	insertt = '{$insertt}'
			        ";
			        $this->model->query($query);
					//$faid = $this->model->_con->insert_id;
			    }
			}
		}
		//exit;
        fclose($handle);
    }
    unlink($flash_admin_File);
	
	$query = "SELECT count(*) count
	FROM `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}flash_admin`
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}' 
		AND insertt = '{$insertt}'
		AND switch = 'Y'
	";
	$rs = $this->model->getQueryRecord($query);
	$this->jsPrintMsg('已成功上傳 '. $rs['table']['record'][0]['count'] .' 筆!', $this->io->input['get']['location_url']);
}

// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$flash_admin_data = json_encode($this->io->input["files"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'flash_admin_file', 
	`active` = '閃殺管理帳號匯入', 
	`memo` = '{$flash_admin_data}', 
	`root` = 'flash_admin/file', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

//header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));