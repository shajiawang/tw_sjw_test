<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT p.*
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` p 
WHERE 
p.prefixid = '{$this->config->default_prefix_id}'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["deposit_rule"] = $recArr['table']['record'];

//Relation country
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}country` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country_rt"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_rule_item_add', 
	`active` = '儲值規則項目新增', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_rule_item/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();