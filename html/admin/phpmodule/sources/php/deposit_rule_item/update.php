<?php
// Check Variable Start
if (empty($this->io->input["post"]["driid"])) {
	$this->jsAlertMsg('活動規則項目ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('活動規則項目名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('活動規則項目敘述錯誤!!');
}
if (empty($this->io->input["post"]["countryid"])) {
	$this->jsAlertMsg('國別ID錯誤!!');
}
if (!is_numeric($this->io->input["post"]["amount"])) {
	$this->jsAlertMsg('儲值項目價格錯誤!!');
}
if (!is_numeric($this->io->input["post"]["spoint"])) {
	$this->jsAlertMsg('儲值殺幣點數錯誤!!');
}
// Check Variable Start

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];


$codeformat1 = (empty($this->io->input["post"]["codeformat1"]))? "" : "`codeformat1` = '{$this->io->input["post"]["codeformat1"]}',";
$codeformat2 = (empty($this->io->input["post"]["codeformat2"]))? "" : "`codeformat2` = '{$this->io->input["post"]["codeformat2"]}',";

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` SET 
`drid` = '{$this->io->input["post"]["drid"]}',
`countryid` = '{$this->io->input["post"]["countryid"]}',
`amount` = '{$this->io->input["post"]["amount"]}',
`spoint` = '{$this->io->input["post"]["spoint"]}',
`name` = '{$this->io->input["post"]["name"]}',
`code1` = '{$this->io->input["post"]["code1"]}',
{$codeformat1}
`code2` = '{$this->io->input["post"]["code2"]}',
{$codeformat2}
`code3` = '{$this->io->input["post"]["code3"]}',
`code4` = '{$this->io->input["post"]["code4"]}',
`description` = '{$this->io->input["post"]["description"]}' ";
//`seq` = '{$this->io->input["post"]["seq"]}',
//`switch` = '{$this->io->input["post"]["switch"]}' 
$query .="
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `driid` = '{$this->io->input["post"]["driid"]}'
";
$this->model->query($query);

// Update End
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_rule_item_update', 
	`active` = '儲值規則項目修改寫入', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_rule_item/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));