<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1];
	}
}

//搜尋條件
$query_search = '';
if ($data['tester'] == 1) {
	$query_search .= ' AND u.`userid` not in (1, 3, 9, 28, 116, 507, 585, 586, 588)';
}
if(!empty($data["joindatefrom"])){			//加入起始日期
	$sub_search_query .=  "AND r.`insertt` >= '".$data["joindatefrom"]."'";
}
if(!empty($data["joindateto"])){			//加入結束日期
	$sub_search_query .=  "AND r.`insertt` < '".$data["joindateto"]."'";
}
if(!empty($data["registerfrom"])){			//從哪加入
	$sub_search_query .=  "
		AND r.`sso_name` = '".$data["registerfrom"]."'";
}
if(!empty($data["gender"])){					//性別
	$sub_search_query .=  "
		AND r.`gender` = '".$data["gender"]."'";
}
if (!empty($data["deposit"])) {				//是否儲值
	if($data["deposit"] == 1){
		$sub_search_query .=  "
			and r.deposit_total > 0";
	}
	else {
		$sub_search_query .=  "
			and r.deposit_total = 0";
	}
}
if (!empty($data["bid"])) {					//是否下標
	if($data["bid"] == 1) {
		$sub_search_query .=  "
			and r.history_total > 0";
	}
	else {
		$sub_search_query .=  "
			and r.history_total = 0";
	}
}
if (!empty($data["exchange"])) {				//是否兌換商品
	if($data["exchange"] == 1){
		$sub_search_query .=  "
			and r.exchange_total > 0";
	}
	else {
		$sub_search_query .=  "
			and r.exchange_total = 0";
	}
}
if(!empty($data["search_nickname"])){			//暱稱查詢
	$sub_search_query .= " AND r.`nickname` like '%".$data["search_nickname"]."%'
	";
}
if (!(empty($data["search_userid"])))$sub_search_query .= " and r.userid='".$data["search_userid"]."'";
$query = "
select r.name, r.userid, r.nickname, r.insertt, r.gender, str_to_date(ifnull(r.deposit_date, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') deposit_date,
str_to_date(ifnull(r.history_date, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') history_date, r.history_total, r.deposit_total, r.sso_name, r.exchange_total
from
(SELECT
u.name, u.userid, up.nickname, u.insertt, up.gender,
(select str_to_date(ifnull(d.insertt, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
where
	d.prefixid = u.prefixid
	and d.userid = u.userid
	and d.switch = 'Y'
order by d.insertt desc
limit 0, 1) deposit_date,
(select str_to_date(ifnull(h.insertt, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
where
	h.prefixid = u.prefixid
	and h.userid = u.userid
	and h.type = 'bid'
	and h.switch = 'Y'
order by h.insertt desc
limit 0, 1) history_date,
(select ifnull(count(*),0) from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
where
	h.prefixid = u.prefixid
	and h.userid = u.userid
	and h.type = 'bid'
	and h.switch = 'Y'
) history_total,
(select ifnull(count(*), 0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
where
	d.prefixid = u.prefixid
	and d.userid = u.userid
	and d.switch = 'Y'
) deposit_total, ifnull(sso.name, 'shajiawang') sso_name,
(select ifnull(count(*),0) from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o
where
	o.prefixid = u.prefixid
	and o.userid = u.userid
 	and o.type = 'saja'
 	and o.pgpid != 0
	and o.switch = 'Y'
) exchange_total
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
on
	u.prefixid = up.prefixid
	and u.userid = up.userid
	and up.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt` usr on
 	usr.prefixid = up.prefixid
 	and up.userid = usr.userid
 	and usr.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso` sso on
 	sso.prefixid = usr.prefixid
 	and sso.ssoid = usr.ssoid
 	and sso.switch = 'Y'
WHERE
	u.prefixid = '".$this->config->default_prefix_id."'
	AND u.switch = 'Y'
group by u.userid
order by u.userid
) r
where
	1 = 1
";
$query .= $sub_search_query;
$table = $this->model->getQueryRecord($query);

##############################################################################################################################################
// Log Start
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_member_statistics_export',
	`active` = '會員統計清單匯出',
	`memo` = '{$report_data}',
	`root` = 'report_member_statistics/export',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//會員資料表
$Report_Array[0]['title'] = array("A1" => "帳號", "B1" => "暱稱", "C1" => "加入日期", "D1" => "最近儲值日", "E1" => "最近下標日", "F1" => "下標次數", "G1" => "儲值次數", "H1" => "性別");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value);
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$row, (string)$value['name'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$row, (string)$value['nickname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['insertt']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['deposit_date']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['history_date']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['history_total']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['deposit_total']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['gender']);
	}
}
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('會員統計');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="會員統計'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');

// Save Excel 2007 file
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>