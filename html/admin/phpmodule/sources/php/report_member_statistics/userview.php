<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
if (empty($this->io->input['get']['userid'])) {
	$this->jsAlertMsg('會員ID錯誤!!');
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 							
									


// Table Record Start
$query = "
select u.userid, u.name, up.nickname, up.phone, up.gender, up.area, up.address, up.addressee, 
GROUP_CONCAT(ifnull(sso.name, 'shajiawang')) sso_name, sms.verified sms_verified 
from `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u 
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	u.prefixid = up.prefixid
	and u.userid = up.userid
	and up.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt` usr on
 	usr.prefixid = up.prefixid
 	and up.userid = usr.userid
 	and usr.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso` sso on
 	sso.prefixid = usr.prefixid
 	and sso.ssoid = usr.ssoid
 	and sso.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sms_auth` sms on
 	u.prefixid = sms.prefixid
 	and u.userid = sms.userid
WHERE 
	u.prefixid = '".$this->config->default_prefix_id."' 
	and u.userid = '".$this->io->input['get']['userid']."'
	AND u.switch = 'Y'
";
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_member_statistics', 
	`active` = '會員統計會員資料查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_member_statistics/userview', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();