<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
//$sub_sort_query =  " ORDER BY u.userid";
// Sort End



// Search Start
$hasWhereCondition=false;
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	// $sub_search_query .=  " AND r.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
	$sub_search_query .=  " AND r.`userid` IN (".$this->io->input["get"]["search_userid"].") ";
	$hasWhereCondition=true;
}
if(!empty($this->io->input["get"]["registerfrom"])){			//從哪加入
	$status["status"]["search"]["registerfrom"] = $this->io->input["get"]["registerfrom"] ;
	$status["status"]["search_path"] .= "&registerfrom=".$this->io->input["get"]["registerfrom"] ;
	$sub_search_query .=  "
		AND r.`sso_name` = '".$this->io->input["get"]["registerfrom"]."'";
	$hasWhereCondition=true;
}
if(!empty($this->io->input["get"]["gender"])){					//性別
	$status["status"]["search"]["gender"] = $this->io->input["get"]["gender"] ;
	$status["status"]["search_path"] .= "&gender=".$this->io->input["get"]["gender"] ;
	$sub_search_query .=  "
		AND r.`gender` = '".$this->io->input["get"]["gender"]."'";
	$hasWhereCondition=true;
}
if (!empty($this->io->input["get"]["deposit"])) {				//是否儲值
	$status["status"]["search"]["deposit"] = $this->io->input["get"]["deposit"] ;
	$status["status"]["search_path"] .= "&deposit=".$this->io->input["get"]["deposit"] ;
	if($this->io->input["get"]["deposit"] == 1){
		$sub_search_query .=  "
			and r.deposit_total > 0";
	}
	else {
		$sub_search_query .=  "
			and r.deposit_total = 0";
	}
	$hasWhereCondition=true;
}
if (!empty($this->io->input["get"]["bid"])) {					//是否下標
	$status["status"]["search"]["bid"] = $this->io->input["get"]["bid"] ;
	$status["status"]["search_path"] .= "&bid=".$this->io->input["get"]["bid"] ;
	if($this->io->input["get"]["bid"] == 1) {
		$sub_search_query .=  "
			and r.history_total > 0";
	}
	else {
		$sub_search_query .=  "
			and r.history_total = 0";
	}
	$hasWhereCondition=true;
}
if (!empty($this->io->input["get"]["exchange"])) {				//是否兌換商品
	$status["status"]["search"]["exchange"] = $this->io->input["get"]["exchange"] ;
	$status["status"]["search_path"] .= "&exchange=".$this->io->input["get"]["exchange"] ;
	if($this->io->input["get"]["exchange"] == 1){
		$sub_search_query .=  "
			and r.exchange_total > 0";
	}
	else {
		$sub_search_query .=  "
			and r.exchange_total = 0";
	}
	$hasWhereCondition=true;
}
if($this->io->input["get"]["search_nickname"] != ''){			//暱稱查詢
	$status["status"]["search"]["search_nickname"] = $this->io->input["get"]["search_nickname"] ;
	$status["status"]["search_path"] .= "&search_nickname=".$this->io->input["get"]["search_nickname"] ;
	
	$sub_search_query .= "AND r.`nickname` like '%".$this->io->input["get"]["search_nickname"]."%'
	";
	$hasWhereCondition=true;
}
if(!empty($this->io->input["get"]["joindatefrom"])){			//加入起始日期
	$status["status"]["search"]["joindatefrom"] = $this->io->input["get"]["joindatefrom"] ;
	$status["status"]["search_path"] .= "&joindatefrom=".$this->io->input["get"]["joindatefrom"] ;
	$sub_search_query .=  " AND r.`insertt` >= '".$this->io->input["get"]["joindatefrom"]."'";
	$hasWhereCondition=true;
} else {
    if(!$hasWhereCondition)
	    $sub_search_query .=  " AND r.`insertt` >='".date('Y-m-d')." 00:00:00' ";	
}
if(!empty($this->io->input["get"]["joindateto"])){				//加入結束日期
	$status["status"]["search"]["joindateto"] = $this->io->input["get"]["joindateto"] ;
	$status["status"]["search_path"] .= "&joindateto=".$this->io->input["get"]["joindateto"] ;
	$sub_search_query .=  " AND r.`insertt` < '".$this->io->input["get"]["joindateto"]."' ";
} else {
	if(!$hasWhereCondition)
	    $sub_search_query .=  " AND r.`insertt`<='".date('Y-m-d')." 23:59:59' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM 
(SELECT 
u.name, u.userid, up.nickname, u.insertt, up.gender, 
(select str_to_date(ifnull(d.insertt, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
where 
	d.prefixid = u.prefixid 
	and d.userid = u.userid 
	and d.switch = 'Y' 
order by d.insertt desc 
limit 0, 1) deposit_date,
(select str_to_date(ifnull(h.insertt, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
where 
	h.prefixid = u.prefixid 
	and h.userid = u.userid 
	and h.type = 'bid'
	and h.switch = 'Y' 
order by h.insertt desc 
limit 0, 1) history_date,
(select ifnull(count(*),0) from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
where 
	h.prefixid = u.prefixid 
	and h.userid = u.userid 
	and h.type = 'bid'
	and h.switch = 'Y' 
) history_total, 
(select ifnull(count(*), 0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
where 
	d.prefixid = u.prefixid 
	and d.userid = u.userid 
	and d.switch = 'Y' 
) deposit_total, 
(select ifnull(count(*),0) from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
where 
	o.prefixid = u.prefixid
	and o.userid = u.userid
 	and o.type = 'saja'
 	and o.pgpid != 0
	and o.switch = 'Y'
) exchange_total 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u 
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up 
on
	u.prefixid = up.prefixid
	and u.userid = up.userid
WHERE 
	u.prefixid = '".$this->config->default_prefix_id."' 
group by u.userid
order by u.userid DESC 
) r
where 
	1 = 1
" ;
$query .= $sub_search_query ; 

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 
									
									


// Table Record Start
$query = "
select r.userid, r.name, r.nickname, r.insertt, r.gender, str_to_date(ifnull(r.deposit_date, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') deposit_date, 
str_to_date(ifnull(r.history_date, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') history_date, r.history_total, r.deposit_total, r.exchange_total 
from
(SELECT 
u.userid, u.name, up.nickname, u.insertt, up.gender, 
(select str_to_date(ifnull(d.insertt, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
where 
	d.prefixid = u.prefixid 
	and d.userid = u.userid 
	and d.switch = 'Y' 
order by d.insertt desc 
limit 0, 1) deposit_date,
(select str_to_date(ifnull(h.insertt, '0000-00-00 00:00:00'), '%Y-%m-%d %H:%i:%s') from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
where 
	h.prefixid = u.prefixid 
	and h.userid = u.userid 
	and h.type = 'bid'
	and h.switch = 'Y' 
order by h.insertt desc 
limit 0, 1) history_date,
(select ifnull(count(*),0) from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
where 
	h.prefixid = u.prefixid 
	and h.userid = u.userid 
	and h.type = 'bid'
	and h.switch = 'Y' 
) history_total, 
(select ifnull(count(*), 0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
where 
	d.prefixid = u.prefixid 
	and d.userid = u.userid 
	and d.switch = 'Y' 
) deposit_total, 
(select ifnull(count(*),0) from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
where 
	o.prefixid = u.prefixid
	and o.userid = u.userid
 	and o.type != 'saja'
 	and o.pgpid = 0
	and o.switch = 'Y'
) exchange_total
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u 
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up 
on
	u.prefixid = up.prefixid
	and u.userid = up.userid 
WHERE 
	u.prefixid = '".$this->config->default_prefix_id."' 
group by u.userid
order by u.userid DESC 
) r 
where 
	1 = 1
";
$query .= $sub_search_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_member_statistics', 
	`active` = '會員統計清單查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_member_statistics/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status['status']['registerfrom'] = array('shajiawang' => '殺價王', 'weixin' => '微信', 'qq' => 'QQ');
$status['status']['gender'] = array('male' => '男', 'female' => '女');
$status['status']['deposit'] = array('1' => '是', '2' => '否');		//是否儲值
$status['status']['bid'] = array('1' => '是', '2' => '否');			//是否下標
$status['status']['exchange'] = array('1' => '是', '2' => '否');	//是否兌換

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();