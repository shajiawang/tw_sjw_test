<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
if (empty($this->io->input['get']['userid'])) {
	$this->jsAlertMsg('會員ID錯誤!!');
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Search Start
$status["status"]["search_path"] = "&userid=".$this->io->input['get']['userid'];
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p on 
	p.prefixid = h.prefixid
	and p.productid = h.productid
WHERE 
	h.prefixid = '".$this->config->default_prefix_id."' 
	and h.productid is not null
	and h.userid = '".$this->io->input['get']['userid']."'
	and h.type = 'bid'
	AND h.switch = 'Y'
order by h.productid
" ;

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 
									
									


// Table Record Start
$query = "
select h.*, p.name pdtname 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h 
left join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p on 
	p.prefixid = h.prefixid
	and p.productid = h.productid
WHERE 
	h.prefixid = '".$this->config->default_prefix_id."' 
	and h.productid is not null
	and h.userid = '".$this->io->input['get']['userid']."'
	and h.type = 'bid'
	AND h.switch = 'Y'
order by h.productid
";
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_member_statistics', 
	`active` = '會員統計下標次數資料查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_member_statistics/historylist', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();