<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
if (empty($this->io->input['get']['userid'])) {
	$this->jsAlertMsg('會員ID錯誤!!');
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]["dbname"];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Search Start
$status["status"]["search_path"] = "&userid=".$this->io->input['get']['userid'];
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `{$db_exchange}`.`{$this->config->default_prefix}order` 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y'
	AND `type`!='saja' 
";
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end


// Table Record Start
$query ="SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}order`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `userid` = '".$this->io->input['get']['userid']."' 
	AND `type`!='saja' 
	AND `switch`='Y'
ORDER BY 
	orderid DESC 
" ;
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query);  

if($table["table"]['record']){
	foreach($table["table"]['record'] as $rk => $rv)
	{
		$table["table"]['record'][$rk] = $rv;
		
		if($rv['status']=='1'){ $order_status = '配送中'; }
		elseif($rv['status']=='2'){ $order_status = '退費中'; }
		elseif($rv['status']=='3'){ $order_status = '已發送'; }
		elseif($rv['status']=='4'){ $order_status = '已到貨'; }
		elseif($rv['status']=='5'){ $order_status = '已退費'; }
		else{ $order_status = '處理中'; } 
		$table["table"]['record'][$rk]['status'] = $order_status;

		$query ="SELECT name 
		FROM `{$db_user}`.`{$this->config->default_prefix}user` 
		WHERE 
			`prefixid` = '{$this->config->default_prefix_id}' 
			AND switch = 'Y' 
			AND `userid` = '{$rv['userid']}'
		";
		$userArr = $this->model->getQueryRecord($query); 
		$table["table"]['record'][$rk]['username'] = $userArr['table']['record'][0]['name'];
		
		/*
		 orderid 遞增 訂單id	userid 使用者id	status 訂單狀態id	type 訂單類別	epid 兌換商品id	pgpid 得標記錄ID	num 訂單數量	esid 店家ID	point_price 兌換單價	process_fee 處理費	total_fee 總價	profit 分潤	memo 訂單備註	confirm 確認訂單	seq 	switch 	insertt 	modifyt 
		*/
		if($rv['type']=='exchange')
		{
			//兌換商品
			$query ="SELECT name  
			FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` 
			WHERE 
				`prefixid` = '{$this->config->default_prefix_id}' 
				AND switch = 'Y' 
				AND `epid` = '{$rv['epid']}'
			";
			
			$productArr = $this->model->getQueryRecord($query); 
			$table["table"]['record'][$rk]['epname'] = $productArr['table']['record'][0]['name'];
			$table["table"]['record'][$rk]['type'] = '兌換';
			
		}
		else
		{
			//兌換商品
			$query ="SELECT name  
			FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` 
			WHERE 
				`prefixid` = '{$this->config->default_prefix_id}' 
				AND switch = 'Y' 
				AND `epid` = '{$rv['epid']}'
			";
		
			$productArr = $this->model->getQueryRecord($query); 
			$table["table"]['record'][$rk]['epname'] = $productArr['table']['record'][0]['name'];
			$table["table"]['record'][$rk]['type'] = '生活繳費';
		}
	}
}
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_member_statistics', 
	`active` = '會員統計兌換次數資料查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_member_statistics/exchangelist', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();