<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='e'){
	$page_name='實體發送類別';
}elseif($this->io->input["get"]["t"]=='d'){
	$page_name='一般發送類別';
}elseif($this->io->input["get"]["t"]=='c'){
	$page_name='充值滿額類別';
}else{
	$page_name='分享連結類別';
}

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="SELECT *
FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote`
WHERE 
prefixid = '{$this->config->default_prefix_id}'
AND switch = 'Y'
AND behav = '{$this->io->input["get"]["t"]}'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["promote"] = $recArr['table']['record'];


//取得充值活動項目內容
$query2 ="
SELECT dri.*, dr.name as dr_name  
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri
LEFT OUTER JOIN `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr ON 
	dri.prefixid = dr.prefixid
	AND dri.drid = dr.drid
	AND dr.switch = 'Y'
WHERE 
	dri.prefixid = '{$this->config->default_prefix_id}'
	AND dr.switch = 'Y'	
	AND dri.switch = 'Y'
";
$recArr2 = $this->model->getQueryRecord($query2);
$table["table"]["rt"]["deposit_rule_item"] = $recArr2['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_rt_{$this->io->input["get"]["t"]}_add', 
	`active` = '{$page_name}組數新增', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_rt_{$this->io->input["get"]["t"]}/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]['get']['t'] = $this->io->input["get"]["t"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();