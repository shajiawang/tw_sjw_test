<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('類別名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["num"])) {
	$this->jsAlertMsg('贈送組數錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];


if($this->io->input["post"]["behav"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["post"]["behav"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["post"]["behav"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["post"]["behav"]=='lb'){
	$page_name='直播領取';
}elseif($this->io->input["post"]["behav"]=='ad'){
	$page_name='廣告領取';
}else{
	$page_name='分享連結';
}
##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt` 
SET
	`prefixid`='{$this->config->default_prefix_id}',
	`spid` = '{$this->io->input["post"]["spid"]}',
	`behav`='{$this->io->input["post"]["behav"]}',
	`name`='{$this->io->input["post"]["name"]}',
	`amount`='{$this->io->input["post"]["amount"]}',
	`num`='{$this->io->input["post"]["num"]}', 
	`seq`='{$this->io->input["post"]["seq"]}',
	`driid` = '{$this->io->input["post"]["driid"]}',
	`insertt`=now()
" ;
$this->model->query($query);

// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_rt_{$this->io->input["post"]["behav"]}_insert', 
	`active` = '{$page_name}組數新增寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_rt_{$this->io->input["post"]["behav"]}/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));