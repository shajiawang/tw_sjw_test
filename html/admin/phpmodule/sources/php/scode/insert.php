<?php
// Check Variable Start
if(!empty($this->io->input["post"]["type"]) && $this->io->input["post"]["type"]=='new_scode_item') {
	if(empty($this->io->input["post"]["qty"])) {
		$this->jsAlertMsg('序號建立數量錯誤!!');
	}
}
else
{
	if(empty($this->io->input["post"]["name"])) {
		$this->jsAlertMsg('活動名稱錯誤!!');
	}
	if(!is_numeric($this->io->input["post"]["seq"])) {
		$this->jsAlertMsg('排序錯誤!!');
	}
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

$search = array(" ","　","\n","\r","\t");
$replace = array("","","","","");
$name = str_replace($search, $replace, $this->io->input["post"]["name"]);

if($this->io->input["post"]["behav"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["post"]["behav"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["post"]["behav"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["post"]["behav"]=='lb'){
	$page_name='直播領取';
}elseif($this->io->input["post"]["behav"]=='ad'){
	$page_name='廣告領取';
}else{
	$page_name='分享連結';
}
##############################################################################################################################################
// Insert Start
if($this->io->input["post"]["type"]=='new_scode_item')
{
	//取得 S碼活動
	$query = "SELECT s.*, sp.name spname, sp.num num
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` s
		LEFT OUTER JOIN `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt` sp ON 
			sp.prefixid = s.prefixid
			AND sp.spid = s.spid
			AND sp.switch = 'Y'
		WHERE 
			s.prefixid = '{$this->config->default_prefix_id}' 
			AND s.spid = '{$this->io->input["post"]["spid"]}'
			AND unix_timestamp( s.offtime ) >0 
			AND unix_timestamp() >= unix_timestamp( s.ontime ) 
			AND unix_timestamp() <= unix_timestamp( s.offtime ) 
			AND s.switch = 'Y'
			AND sp.spid IS NOT NULL
	";
	$rs = $this->model->getQueryRecord($query);
	
	if(empty($rs['table']['record'][0]) ) {
		$this->jsAlertMsg('S碼活動錯誤!!');
	} 
	else
	{
		$batch = time();
		for($i=0; $i<(int)$this->io->input["post"]["qty"]; $i++)
		{
			$v = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
			shuffle($v);
			
			$rand = $batch * rand(1,9);
			$rand1 = substr($rand, -6, 2);	
			$rand2 = substr($rand, -3, 2);
			$_serial = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $v[3];
			$serial = $_serial . sprintf("%04d", $i);
			
			shuffle($v);
			$pwd = $v[0] . $v[1] . $rand1 . $v[2] . $v[3] . $rand2 . $v[4] . $v[5];
			
			$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_item` 
			SET
				`prefixid`='{$this->config->default_prefix_id}',
				`spid`='{$this->io->input["post"]["spid"]}',
				`amount`='{$rs['table']['record'][0]['num']}',
				`serial`='{$serial}',
				`pwd`='{$pwd}',
				`verified`='N',
				`batch`='{$batch}',
				`insertt`=now()
			";// var_dump($query); echo "<br>";
			$this->model->query($query);
		}
	}
}
else
{
	
	if ($this->io->input["post"]["behav"] == 'lb'){
		$broadcast_use = 'Y';
	}else{
		$broadcast_use = 'N';
	}

	$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` 
	SET
		`prefixid`='{$this->config->default_prefix_id}',
		`behav`='{$this->io->input["post"]["behav"]}',
		`name`='{$name}',
		`description`= '{$this->io->input["post"]["description"]}',
		`ontime`= '{$this->io->input["post"]["ontime"]}',
		`offtime`= '{$this->io->input["post"]["offtime"]}',
		`productid`= '{$this->io->input["post"]["productid"]}',
		`onum`= '{$this->io->input["post"]["onum"]}',
		`seq`='{$this->io->input["post"]["seq"]}',
		`broadcast_use`='{$broadcast_use}',
		`insertt`=now()
	" ;
	$this->model->query($query);
	$spid = $this->model->_con->insert_id;

}


if ($this->io->input["post"]["behav"] == 'lb'){
	//寫入直播主關聯rt
	$values = array();
	foreach($this->io->input["post"]["lbu"] as $rk => $rv) {
		$values[] = "
			('".$this->config->default_prefix_id."', '".$spid."', '".$rv."', '".(($rk+1)*10)."', 'lb', '".$this->io->input["post"]["onum"]."', '".$name."', NOW())";
	}
	$query = "
	INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_lb_rt`(
		`prefixid`,
		`spid`,
		`lbuserid`,
		`seq`,
		`behav`,
		`num`,
		`name`,	
		`insertt`
	)
	VALUES
	".implode(',', $values); 
	$this->model->query($query);	
}


if ($this->io->input["post"]["behav"] == 'b' ){
	//寫入分享關聯rt
	$values = array();
	
	$values[] = "('".$this->config->default_prefix_id."', '".$spid."', '".(($rk+1)*10)."', 'b', '".$this->io->input["post"]["onum"]."', '".$name."', NOW())";
	
	$query = "
	INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_rt`(
		`prefixid`,
		`spid`,
		`seq`,
		`behav`,
		`num`,
		`name`,	
		`insertt`
	)
	VALUES
	".implode(',', $values); 
	$this->model->query($query);	
}


if ($this->io->input["post"]["behav"] == 'e' ){
	//寫入分享關聯rt
	$values = array();
	
	$values[] = "('".$this->config->default_prefix_id."', '".$spid."', '".(($rk+1)*10)."', 'e', '".$this->io->input["post"]["onum"]."', '".$name."', NOW())";
	
	$query = "
	INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_rt`(
		`prefixid`,
		`spid`,
		`seq`,
		`behav`,
		`num`,
		`name`,	
		`insertt`
	)
	VALUES
	".implode(',', $values); 
	$this->model->query($query);	
}

if ( $this->io->input["post"]["behav"] == 'c' ){
	//寫入充值關聯rt
	$values = array();
	
	$values[] = "('".$this->config->default_prefix_id."', '".$spid."', '".(($rk+1)*10)."', 'c', '".$this->io->input["post"]["onum"]."', '".$name."', '".$this->io->input["post"]["driid"]."', NOW())";
	
	$query = "
	INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_rt`(
		`prefixid`,
		`spid`,
		`seq`,
		`behav`,
		`num`,
		`name`,
		`driid`,		
		`insertt`
	)
	VALUES
	".implode(',', $values); 
	$this->model->query($query);	
}
// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["post"]["behav"]}_insert', 
	`active` = '{$page_name}新增寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["post"]["behav"]}/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));