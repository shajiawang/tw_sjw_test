<?php
// Check Variable Start
if(empty($this->io->input["post"]["spid"]) || empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員id或spid不可為空!!');
}
/* 鎖uid */
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 

if($_SESSION['user']['department'] != 'S') { 
	$this->jsAlertMsg('您無權限登入！');
	die();
}

// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["post"]["behav"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["post"]["behav"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["post"]["behav"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["post"]["behav"]=='lb'){
	$page_name='直播領取';
}elseif($this->io->input["post"]["behav"]=='ad'){
	$page_name='廣告領取';
}else{
	$page_name='分享連結';
}
$page_name .='記錄詳情';

##############################################################################################################################################
// Insert Start
if($this->io->input["post"]["behav"]=='c')
{
	//取得 S碼活動
	$query = "SELECT s.*, sp.name spname, sp.num num, sp.amount price
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` s
		LEFT OUTER JOIN `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt` sp ON 
			sp.prefixid = s.prefixid
			AND sp.spid = s.spid
			AND sp.switch = 'Y'
		WHERE 
			s.prefixid = '{$this->config->default_prefix_id}' 
			AND s.spid = '{$this->io->input["post"]["spid"]}'
			AND unix_timestamp( s.offtime ) >0 
			AND unix_timestamp() >= unix_timestamp( s.ontime ) 
			AND unix_timestamp() <= unix_timestamp( s.offtime ) 
			AND s.switch = 'Y'
			AND sp.spid IS NOT NULL
	";
	//print_r($query);exit;
	$rs = $this->model->getQueryRecord($query);
	
	if(empty($rs['table']['record'][0]) ) {
		$this->jsAlertMsg('S碼活動錯誤!!');
	} 
	else
	{
		/*$batch = time();
		for($i=0; $i<(int)$this->io->input["post"]["qty"]; $i++)
		{
			$v = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
			shuffle($v);
			
			$rand = $batch * rand(1,9);
			$rand1 = substr($rand, -6, 2);	
			$rand2 = substr($rand, -3, 2);
			$_serial = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $v[3];
			$serial = $_serial . sprintf("%04d", $i);
			
			shuffle($v);
			$pwd = $v[0] . $v[1] . $rand1 . $v[2] . $v[3] . $rand2 . $v[4] . $v[5];
			
			$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_item` 
			SET
				`prefixid`='{$this->config->default_prefix_id}',
				`spid`='{$this->io->input["post"]["spid"]}',
				`amount`='{$rs['table']['record'][0]['num']}',
				`serial`='{$serial}',
				`pwd`='{$pwd}',
				`verified`='N',
				`batch`='{$batch}',
				`insertt`=now()
			";// var_dump($query); echo "<br>";
			$this->model->query($query);
		}*/
		
		$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode` 
		SET
			`prefixid`='{$this->config->default_prefix_id}',
			`userid`='{$this->io->input["post"]["userid"]}', 
			`behav`='{$this->io->input["post"]["behav"]}',
			`spid`='{$this->io->input["post"]["spid"]}',
			`productid`= '0',
			`amount`= '{$rs['table']['record'][0]['num']}',
			`remainder`= '{$rs['table']['record'][0]['num']}',
			`closed`= 'N',
			`seq`='0',
			`switch`='Y',
			`insertt`=now()
		" ;
		$this->model->query($query);
		$scodeid = $this->model->_con->insert_id;
		
		
		$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_history` 
		SET
			`prefixid`='{$this->config->default_prefix_id}',
			`userid`='{$this->io->input["post"]["userid"]}', 
			`scodeid`='{$scodeid}', 
			`spid`='{$this->io->input["post"]["spid"]}',
			`promote_amount`='{$rs['table']['record'][0]['price']}',
			`num`= '{$rs['table']['record'][0]['num']}',
			`memo`='{$rs['table']['record'][0]['name']}',
			`seq`='0',
			`switch`='Y',
			`insertt`=now()
		" ;
		$this->model->query($query);
		
		
	}
}

// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["get"]["t"]}_history_insert', 
	`active` = '{$page_name}新增寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["get"]["t"]}_history/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));