<?php
// Check Variable Start
if (empty($this->io->input["get"]["t"]) ) {
	$this->jsAlertMsg('活動型態錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start

//if($this->io->input["get"]["t"]=='d'){ $page_name='一般發送'; }
$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';

if($files['file_name']["error"] !=0 || empty($files['file_name']["size"]) ) {
	$this->jsAlertMsg('上傳錯誤!!');
}
else
{
	$productFile = $files['file_name']['name']; 
	
    if(is_uploaded_file($files['file_name']['tmp_name']) ) {
		move_uploaded_file($files['file_name']['tmp_name'], "./{$productFile}");
	}

	$handle = fopen($productFile, 'r');
	if (is_resource($handle))
    {
        $batch = time();
		$newData = array();
        while (!feof($handle))
        {
            $buffer = fgets($handle); 
			if(!empty($buffer))
            {
                $data = explode(",", $buffer); 
				$uname = trim($data[0]); 
				$uname = str_replace("'", '', $uname);
				$uname = trim(str_replace('"', '', $uname));
				
				$spid = trim($data[1]);
				$spid = str_replace("'", '', $spid);
				$spid = trim(str_replace('"', '', $spid));
				
				$inum = $data[2];
				
                //if(!empty($uname) && !empty($spid) ){ continue; }
				if(!empty($uname) && !empty($spid) )
				{
					//取得 S碼活動
					$query = "SELECT s.*, sp.name spname, sp.num num
						FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` s
						LEFT OUTER JOIN `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt` sp ON 
							sp.prefixid = s.prefixid
							AND sp.spid = s.spid
							AND sp.switch = 'Y'
						WHERE 
							s.prefixid = '{$this->config->default_prefix_id}' 
							AND s.spid = '{$spid}'
							AND s.behav = 'd'
							AND unix_timestamp( s.offtime ) >0 
							AND unix_timestamp() >= unix_timestamp( s.ontime ) 
							AND unix_timestamp() <= unix_timestamp( s.offtime ) 
							AND s.switch = 'Y'
							AND sp.spid IS NOT NULL
					";
					$table = $this->model->getQueryRecord($query);
					
					//取得$uid
					$query = "SELECT userid
						FROM `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}user`
						WHERE 
							`prefixid` = '{$this->config->default_prefix_id}' 
							AND name = '{$uname}'
							AND switch = 'Y'
					";
					$rs = $this->model->getQueryRecord($query);
					
					if(!empty($table['table']['record'][0]) && !empty($rs['table']['record'][0]) ) 
					{
						$info = $table['table']['record'][0]; 
						$uid = $rs['table']['record'][0]['userid'];
						
						$query = "INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode`
						SET
						   `prefixid`='{$this->config->default_prefix_id}',
						   `userid`='{$uid}',
						   `spid`='{$spid}',
						   `behav`='{$info['behav']}',
						   `amount`='{$info['num']}',
						   `insertt`=NOW()
						";
						$this->model->query($query);
						$scodeid = $this->model->_con->insert_id;
						
						$query = "INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_history`
						SET
						   `prefixid`='{$this->config->default_prefix_id}',
						   `userid`='{$uid}',
						   `scodeid`='{$scodeid}',
						   `spid`='{$spid}',
						   `promote_amount`='1',
						   `num`='{$info['num']}',
						   `memo`='{$info['name']}',
						   `batch`='{$batch}',
						   `insertt`=NOW()
						";
						$this->model->query($query);
						
						$query = "UPDATE `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote`
						SET
						   `scode_sum`=`scode_sum` + '{$info['num']}',
						   `amount`=`amount` + 1 
						WHERE 
							`prefixid` = '{$this->config->default_prefix_id}' 
							AND spid = '{$spid}' 
							AND switch = 'Y'
						";
						$this->model->query($query);
					}
				}
            }
        }
        fclose($handle);
    }
    unlink($productFile);
	
	$query = "SELECT count(*) count
	FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_history`
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}' 
		AND batch = '{$batch}'
		AND switch = 'Y'
	";
	$rs = $this->model->getQueryRecord($query);
	$this->jsPrintMsg('已成功上傳 '. $rs['table']['record'][0]['count'] .' 筆!', $this->io->input['get']['location_url']);
}

// Insert Start
##############################################################################################################################################

##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["files"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_file', 
	`active` = '殺價序號上傳', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote/file', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################
	

//header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));