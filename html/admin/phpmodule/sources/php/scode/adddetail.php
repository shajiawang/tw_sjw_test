<?php
// Check Variable Start
/* 鎖uid */
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 

if($_SESSION['user']['department'] != 'S') { 
	$this->jsAlertMsg('您無權限登入！');
	die();
}

// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["get"]["t"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["get"]["t"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["get"]["t"]=='lb'){
	$page_name='直播領取';		
}else{
	$page_name='分享連結';
}
$page_name .='記錄詳情';

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["get"]["t"]}_history_add', 
	`active` = '{$page_name}新增', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["get"]["t"]}_history/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]['get']['t'] = $this->io->input["get"]["t"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();