<?php
// Check Variable Start
if (empty($this->io->input["post"]["spid"])) {
	$this->jsAlertMsg('活動ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('活動名稱錯誤!!');
}

if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable Start
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

$search = array(" ","　","\n","\r","\t");
$replace = array("","","","","");
$name = str_replace($search, $replace, $this->io->input["post"]["name"]);

if($this->io->input["post"]["behav"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["post"]["behav"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["post"]["behav"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["post"]["behav"]=='lb'){
	$page_name='直播領取';
}elseif($this->io->input["post"]["behav"]=='ad'){
	$page_name='廣告領取';
}else{
	$page_name='分享連結';
}
##############################################################################################################################################
// Update Start

$query ="UPDATE `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote`  
SET 
	`name` = '{$name}',
	`description`= '{$this->io->input["post"]["description"]}',
	`ontime`= '{$this->io->input["post"]["ontime"]}',
	`offtime`= '{$this->io->input["post"]["offtime"]}',
	`productid`= '{$this->io->input["post"]["productid"]}',
	`onum`= '{$this->io->input["post"]["onum"]}',
	`seq` = '{$this->io->input["post"]["seq"]}' 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `spid` = '{$this->io->input["post"]["spid"]}'
" ;
$this->model->query($query);


if ($this->io->input["post"]["behav"] == 'lb'){

//更新直播主關聯rt
$query = "
DELETE FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_lb_rt`
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND spid = '".$this->io->input["post"]["spid"]."'
";
$this->model->query($query);

$values = array();
foreach($this->io->input["post"]["lbu"] as $rk => $rv) {
	$values[] = "
		('".$this->config->default_prefix_id."', '".$this->io->input["post"]["spid"]."', '".$rv."', '".(($rk+1)*10)."', 'lb', '".$this->io->input["post"]["onum"]."', '".$name."', NOW())";
}
$query = "
INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_lb_rt`(
	`prefixid`,
	`spid`,
	`lbuserid`,
	`seq`,
	`behav`,
	`num`,
	`name`,	
	`insertt`
)
VALUES
".implode(',', $values)."
ON DUPLICATE KEY UPDATE switch = 'Y'
"; 
$this->model->query($query);
}

if ($this->io->input["post"]["behav"] == 'c'){
	
$query ="SELECT * 
FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `sprid` = '{$this->io->input["get"]["sprid"]}'
" ;
$table = $this->model->getQueryRecord($query);
	
	
$query ="UPDATE `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt`  
SET 
	`driid` = '{$this->io->input["post"]["driid"]}',
	`num` = '{$this->io->input["post"]["onum"]}'	
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `spid` = '{$this->io->input["post"]["spid"]}' 
" ;
$this->model->query($query);

}

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["post"]["behav"]}_update', 
	`active` = '{$page_name}修改寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["post"]["behav"]}/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));