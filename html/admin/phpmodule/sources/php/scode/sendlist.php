<?php
$page_name='分享連結查詢';

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if($this->io->input["get"]["t"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["get"]["t"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["get"]["t"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["get"]["t"]=='lb'){
	$page_name='直播領取';	
}elseif($this->io->input["get"]["t"]=='ad'){
	$page_name='廣告領取';		
}else{
	$page_name='分享連結';
}
$page_name .='記錄詳情';

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(spid|name|ontime|offtime|seq|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}


if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End


// Search Start
$status["status"]["search"] = "";
$sub_search_query = "AND sp.spid IS NOT NULL ";

$join_search_query = "LEFT OUTER JOIN `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}scode_promote` sp ON
	sp.prefixid = o.prefixid
	AND sp.spid = o.spid
	AND sp.`behav`='{$this->io->input["get"]["t"]}'
	AND sp.`switch`='Y'
LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` u1 ON
	u1.prefixid = o.prefixid
	AND u1.userid = o.userid
	AND u1.`switch`='Y'
";

if($this->io->input["get"]["t"] != '')
{
	$status["status"]["search_path"] .= "&t=".$this->io->input["get"]["t"];
}

if($this->io->input["get"]["spid"] != ''){
	$status["status"]["search"]["spid"] = $this->io->input["get"]["spid"] ;
	$status["status"]["search_path"] .= "&spid=".$this->io->input["get"]["spid"] ;
	$sub_search_query .=  "AND o.`spid` ='{$this->io->input["get"]["spid"]}'
	";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}oscode` o
{$join_search_query}
WHERE 
o.`prefixid` = '{$this->config->default_prefix_id}' 
AND o.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query = "SELECT o.*, sp.name as spname, u1.name as u1name ";

$query .= "FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}oscode` o
{$join_search_query}
WHERE 
o.`prefixid` = '{$this->config->default_prefix_id}' 
AND o.`switch`='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->tplVar('page_name', $page_name);
$this->display();