<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["get"]["t"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["get"]["t"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["get"]["t"]=='lb'){
	$page_name='直播領取';
}elseif($this->io->input["get"]["t"]=='ad'){
	$page_name='廣告領取';		
}else{
	$page_name='分享連結';
}

##############################################################################################################################################
// Table  Start 

// Table Content Start 

// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["live_broadcast_user"] = $recArr['table']['record'];

//取得充值活動項目內容
$query2 ="
SELECT dri.*, dr.name as dr_name  
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri
LEFT OUTER JOIN `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr ON 
	dri.prefixid = dr.prefixid
	AND dri.drid = dr.drid
	AND dr.switch = 'Y'
WHERE 
	dri.prefixid = '{$this->config->default_prefix_id}'
	AND dr.switch = 'Y'	
	AND dri.switch = 'Y'
";
$recArr2 = $this->model->getQueryRecord($query2);

//可供選擇的產品編號
$table["table"]["rt"]["deposit_rule_item"] = $recArr2['table']['record'];
$query3 = "SELECT productid,name From  `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product`  WHERE
					prefixid = '{$this->config->default_prefix_id}' 
					AND unix_timestamp( offtime ) >0 
					AND unix_timestamp() >= unix_timestamp( ontime ) 
					AND unix_timestamp() <= unix_timestamp( offtime ) 
					AND switch = 'Y'
			";
$recArr3 = $this->model->getQueryRecord($query3);
$table["table"]["rt"]["valid_product"]=$recArr3['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["get"]["t"]}_add', 
	`active` = '{$page_name}新增', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["get"]["t"]}/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]['get']['t'] = $this->io->input["get"]["t"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();