<?php
// Check Variable Start
if (empty($this->io->input["get"]["spid"])) {
	$this->jsAlertMsg('活動ID錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["get"]["t"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["get"]["t"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["get"]["t"]=='lb'){
	$page_name='直播領取';
}elseif($this->io->input["get"]["t"]=='ad'){
	$page_name='廣告領取';
}else{
	$page_name='分享連結';
}


##############################################################################################################################################
// Record 

$query ="SELECT sp.*, spr.driid  
FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` sp
LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_rt` spr ON 
	sp.prefixid = spr.prefixid
	AND sp.spid = spr.spid
	AND spr.switch = 'Y'
WHERE 
sp.`prefixid` = '{$this->config->default_prefix_id}' 
AND sp.`switch`='Y' 
AND sp.`spid` = '{$this->io->input["get"]["spid"]}'
" ;
$table = $this->model->getQueryRecord($query);

if (empty($table['table']['record'])) {
	$this->jsAlertMsg('活動錯誤!!');
}

$info_product = $old_product['table']['record'][0];

##############################################################################################################################################


##############################################################################################################################################
// Table Start
if($this->io->input["get"]["type"]=='new_scode_item')
{
	//取得 S碼活動
/* 	$query = "SELECT s.*, sp.name spname, sp.num num
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` s
		LEFT OUTER JOIN `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt` sp ON 
			sp.prefixid = s.prefixid
			AND sp.spid = s.spid
			AND sp.switch = 'Y'
		WHERE 
			s.prefixid = '{$this->config->default_prefix_id}' 
			AND s.spid = '{$this->io->input["post"]["spid"]}'
			AND unix_timestamp( s.offtime ) >0 
			AND unix_timestamp() >= unix_timestamp( s.ontime ) 
			AND unix_timestamp() <= unix_timestamp( s.offtime ) 
			AND s.switch = 'Y'
			AND sp.spid IS NOT NULL
	";
	$rs = $this->model->getQueryRecord($query);
	
	if(empty($rs['table']['record'][0]) ) {
		$this->jsAlertMsg('S碼活動錯誤!!');
	} 
	else
	{
		$batch = time();
		for($i=0; $i<(int)$this->io->input["post"]["qty"]; $i++)
		{
			$v = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
			shuffle($v);
			
			$rand = $batch * rand(1,9);
			$rand1 = substr($rand, -6, 2);	
			$rand2 = substr($rand, -3, 2);
			$_serial = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $v[3];
			$serial = $_serial . sprintf("%04d", $i);
			
			shuffle($v);
			$pwd = $v[0] . $v[1] . $rand1 . $v[2] . $v[3] . $rand2 . $v[4] . $v[5];
			
			$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_item` 
			SET
				`prefixid`='{$this->config->default_prefix_id}',
				`spid`='{$this->io->input["post"]["spid"]}',
				`amount`='{$rs['table']['record'][0]['num']}',
				`serial`='{$serial}',
				`pwd`='{$pwd}',
				`verified`='N',
				`batch`='{$batch}',
				`insertt`=now()
			";// var_dump($query); echo "<br>";
			$this->model->query($query);
		}
	} */
}
else
{
	
	$query = "SELECT s.* 
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` s
		WHERE 
			s.prefixid = '{$this->config->default_prefix_id}' 
			AND s.spid = '{$this->io->input["get"]["spid"]}'

	";
	$rs = $this->model->getQueryRecord($query);	
	$ontime = date('Y-m-d H:i:s', strtotime('+10 day'));
	
	$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` 
	SET
		`prefixid`='{$rs['table']['record'][0]['prefixid']}',
		`behav`='{$rs['table']['record'][0]['behav']}',
		`name`='{$rs['table']['record'][0]['name']}',
		`description`= '{$rs['table']['record'][0]['description']}',
		`ontime`= '{$ontime}',
		`offtime`= '{$rs['table']['record'][0]['offtime']}',
		`productid`= '{$rs['table']['record'][0]['productid']}',
		`onum`= '{$rs['table']['record'][0]['onum']}',
		`seq`='{$rs['table']['record'][0]['seq']}',
		`broadcast_use`='{$rs['table']['record'][0]['broadcast_use']}',
		`insertt`=now()
	" ;
	$this->model->query($query);
	$spid = $this->model->_con->insert_id;
	
}


if ($this->io->input["get"]["behav"] == 'lb'){
	
	$query = "SELECT s.* 
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_lb_rt` s
		WHERE 
			s.prefixid = '{$this->config->default_prefix_id}' 
			AND s.spid = '{$this->io->input["get"]["spid"]}'

	";
	$sp = $this->model->getQueryRecord($query);		
	
	//寫入直播主關聯rt
	foreach($sp['table']['record'] as $rk => $rv) {
		
		$query = "
		INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_lb_rt` 
		SET
			`prefixid`='{$rv['prefixid']}',
			`spid`='{$spid}',
			`lbuserid`='{$rv['lbuserid']}',
			`seq`='{$rv['seq']}',
			`behav`='{$rv['behav']}',
			`num`='{$rv['num']}',
			`name`='{$rv['name']}',	
			`insertt`=now()
		";
		$this->model->query($query);
	}	
} else {
	
	$query = "SELECT s.* 
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt` s
		WHERE 
			s.prefixid = '{$this->config->default_prefix_id}' 
			AND s.spid = '{$this->io->input["get"]["spid"]}'

	";
	$sp = $this->model->getQueryRecord($query);		
	
	//寫入直播主關聯rt
	foreach($sp['table']['record'] as $rk => $rv) {
		
		$query = "
		INSERT INTO `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode_promote_rt` 
		SET
			`prefixid`='{$rv['prefixid']}',
			`spid`='{$spid}',
			`driid`='{$rv['driid']}',
			`seq`='{$rv['seq']}',
			`behav`='{$rv['behav']}',
			`num`='{$rv['num']}',
			`name`='{$rv['name']}',	
			`insertt`=now()
		";
		$this->model->query($query);
	}	
}

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["get"]["t"]}_copy', 
	`active` = '{$page_name}複製寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["get"]["t"]}/copy', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));