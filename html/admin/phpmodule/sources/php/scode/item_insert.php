<?php
// Check Variable Start

if(!empty($this->io->input["post"]["type"]) && $this->io->input["post"]["type"]=='new_scode_item') {
	if(empty($this->io->input["post"]["qty"])) {
		$this->jsAlertMsg('序號建立數量錯誤!!');
	}
}
else
{
	if(empty($this->io->input["post"]["name"])) {
		$this->jsAlertMsg('活動名稱錯誤!!');
	}
	if(!is_numeric($this->io->input["post"]["seq"])) {
		$this->jsAlertMsg('排序錯誤!!');
	}
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start
if($this->io->input["post"]["type"]=='new_scode_item')
{
	//取得 S碼活動
	$query = "SELECT s.*, sp.name spname, sp.num num
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` s
		LEFT OUTER JOIN `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote_rt` sp ON 
			sp.prefixid = s.prefixid
			AND sp.spid = s.spid
			AND sp.switch = 'Y'
		WHERE 
			s.prefixid = '{$this->config->default_prefix_id}' 
			AND s.spid = '{$this->io->input["post"]["spid"]}'
			AND unix_timestamp( s.offtime ) >0 
			AND unix_timestamp() >= unix_timestamp( s.ontime ) 
			AND unix_timestamp() <= unix_timestamp( s.offtime ) 
			AND s.switch = 'Y'			
	";
	$rs = $this->model->getQueryRecord($query);
	
	if(empty($rs['table']['record'][0]) ) {
		$this->jsAlertMsg('S碼活動錯誤!!');
	} 
	else
	{
		$batch = date('Ymd');
		for($i=0; $i<(int)$this->io->input["post"]["qty"]; $i++)
		{
			// $v = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
			// shuffle($v);
			
			// $rand = $batch * rand(1,9);
			// $rand1 = substr($rand, -6, 2);	
			// $rand2 = substr($rand, -3, 2);
			// $_serial = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $v[3];
			// $serial = $_serial . sprintf("%04d", $i);
			
			// shuffle($v);
			// $pwd = $v[0] . $v[1] . $rand1 . $v[2] . $v[3] . $rand2 . $v[4] . $v[5];
			
			$word = 'abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
			$len = strlen($word);
			$batch = date('Ymd');
			for ($m = 0; $m < 12; $m++) {
				$data[$i]['serial'] .= $word[rand() % $len];
				$data[$i]['passwd'] .= $word[rand() % $len];
			}	
			
			$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_item` 
			SET
				`prefixid`='{$this->config->default_prefix_id}',
				`spid`='{$this->io->input["post"]["spid"]}',
				`amount`='{$this->io->input["post"]["amount"]}',
				`offtime`='{$this->io->input["post"]["offtime"]}',
				`serial`='{$data[$i]['serial']}',
				`pwd`='{$data[$i]['passwd']}',
				`verified`='N',
				`batch`='{$batch}',
				`insertt`=now()
			";// var_dump($query); echo "<br>";
			$this->model->query($query);
		}
	}
}

// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_item_insert', 
	`active` = '{$page_name}新增寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_item/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));