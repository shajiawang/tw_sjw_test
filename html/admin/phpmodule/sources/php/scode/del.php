<?php
// Check Variable Start
if(empty($this->io->input["get"]["spid"])) {
	$this->jsAlertMsg('活動ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='e'){
	$page_name='實體發送';
}elseif($this->io->input["get"]["t"]=='d'){
	$page_name='一般發送';
}elseif($this->io->input["get"]["t"]=='c'){
	$page_name='充值滿額';
}elseif($this->io->input["get"]["t"]=='lb'){
	$page_name='直播領取';
}elseif($this->io->input["get"]["t"]=='ad'){
	$page_name='廣告領取';		
}else{
	$page_name='分享連結';
}

##############################################################################################################################################
// database start
$query ="UPDATE `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `spid` = '{$this->io->input["get"]["spid"]}'
" ;
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["get"]["t"]}_delete', 
	`active` = '{$page_name}刪除', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["get"]["t"]}/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################



// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End
