<?php
// Check Variable Start
if(empty($this->io->input["get"]["drid"])) {
	$this->jsAlertMsg('儲值規則ID錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];


##############################################################################################################################################
// database start
$query ="UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `drid` = '{$this->io->input["get"]["drid"]}'
" ;
$this->model->query($query);
// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_rule_delete', 
	`active` = '儲值規則刪除寫入', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_rule/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End
