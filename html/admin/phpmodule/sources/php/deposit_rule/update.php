<?php
// Check Variable Start
if (empty($this->io->input["post"]["drid"])) {
	$this->jsAlertMsg('儲值規則ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('儲值規則名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('儲值規則敘述錯誤!!');
}
if (empty($this->io->input["post"]["mark"])) {
	$this->jsAlertMsg('幣別符號錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable Start

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];


##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` SET 
`name` = '{$this->io->input["post"]["name"]}',
`description` = '{$this->io->input["post"]["description"]}',
`remark` = '{$this->io->input["post"]["remark"]}',
`mark` = '{$this->io->input["post"]["mark"]}',
`type` = '{$this->io->input["post"]["type"]}',
`seq` = '{$this->io->input["post"]["seq"]}',
`switch` = '{$this->io->input["post"]["switch"]}' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `drid` = '{$this->io->input["post"]["drid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_rule_update', 
	`active` = '儲值規則修改寫入', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_rule/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));