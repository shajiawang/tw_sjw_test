<?php
// Check Variable Start
if (empty($this->io->input["get"]["drid"])) {
	$this->jsAlertMsg('儲值規則ID錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];


##############################################################################################################################################
// Table  Start 

// Table Content Start 
/*
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `drid` = '{$this->io->input["get"]["drid"]}'
" ;
*/
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `drid` = '{$this->io->input["get"]["drid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
/*
$query ="
SELECT c.* 
FROM `{$db_channel}`.`{$this->config->default_prefix}channel` c 
WHERE 
c.prefixid = '{$this->config->default_prefix_id}' 
AND c.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_rt"] = $recArr['table']['record'];
 */
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_rule_edit', 
	`active` = '儲值規則修改', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_rule/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();