<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('儲值規則名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('儲值規則敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];


##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` SET
	`prefixid`='{$this->config->default_prefix_id}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`mark` = '{$this->io->input["post"]["mark"]}',
	`seq`='{$this->io->input["post"]["seq"]}', 
	`type`='{$this->io->input["post"]["type"]}', 
	`switch`='{$this->io->input["post"]["switch"]}',
	`insertt`=now()
" ;
$this->model->query($query);

// Insert Start
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_rule_insert', 
	`active` = '儲值規則新增寫入', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_rule/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));