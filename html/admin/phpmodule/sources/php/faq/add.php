<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];


##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT c.* 
FROM `{$db_user}`.`{$this->config->default_prefix}faq_category` c 
WHERE 
	c.prefixid = '{$this->config->default_prefix_id}' 
	AND c.switch = 'Y'
	AND c.`mod`='a'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["faq_category"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$faq_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'faq_add', 
	`active` = '新手教學問答新增', 
	`memo` = '{$faq_data}', 
	`root` = 'faq/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();