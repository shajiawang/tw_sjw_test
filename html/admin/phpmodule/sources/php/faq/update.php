<?php
// Check Variable Start
if (empty($this->io->input["post"]["faqid"])) {
	$this->jsAlertMsg('問答ID錯誤!!');
}
if (empty($this->io->input["post"]["menu"])) {
	$this->jsAlertMsg('顯示目錄名稱錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('問答標題錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('問答內容錯誤!!');
}
if (empty($this->io->input["post"]["fcid"])) {
	$this->jsAlertMsg('分類錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];


##############################################################################################################################################
//Update Start
$description = htmlspecialchars($this->io->input["post"]["description"]);
$query ="
UPDATE `{$db_user}`.`{$this->config->default_prefix}faq` SET 
	`fcid`='{$this->io->input["post"]["fcid"]}',
	`menu`='{$this->io->input["post"]["menu"]}',
	`name` = '{$this->io->input["post"]["name"]}', 
	`description` = '{$description}',
	`desc_only`='{$this->io->input["post"]["desc_only"]}',
	`seq` = '{$this->io->input["post"]["seq"]}'
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `faqid` = '{$this->io->input["post"]["faqid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$faq_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'faq_update', 
	`active` = '新手教學問答修改寫入', 
	`memo` = '{$faq_data}', 
	`root` = 'faq/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));