<?php
// Check Variable Start
if (empty($this->io->input["post"]["spid"])) {
	$this->jsAlertMsg('活動ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('活動名稱錯誤!!');
}
if(!empty($this->io->input["post"]["newproductid"]) && !is_numeric($this->io->input["post"]["newproductid"]) ) {
	$this->jsAlertMsg('競標商品ID錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable Start
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["post"]["behav"]=='h'){
	$page_name='一般發送管理';
}else{
	$page_name='序號發送管理';
}
##############################################################################################################################################
// Update Start

$update_productid = '';

if(!empty($this->io->input["post"]["newproductid"]) )
{
	$update_productid = "`productid` = '{$this->io->input["post"]["newproductid"]}', ";
	
	$query ="UPDATE `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}oscode`  
	SET `productid` = '{$this->io->input["post"]["newproductid"]}'
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `spid` = '{$this->io->input["post"]["spid"]}'
	";
	$this->model->query($query);
	
	$behav = $this->io->input["post"]["behav"];
	//限定S碼發送組數
	if($behav=='h'){ 
		$setunum = "onum='{$this->io->input["post"]["onum"]}',"; 
	}else{
		$setunum = '';
	}
}

$query ="UPDATE `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote`  
SET 
	`name` = '{$this->io->input["post"]["name"]}',
	`description`= '{$this->io->input["post"]["description"]}',
	{$update_productid}
	{$setunum}
	`seq` = '{$this->io->input["post"]["seq"]}' 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `spid` = '{$this->io->input["post"]["spid"]}'
" ;
/*
	`ontime`= '{$this->io->input["post"]["ontime"]}',
	`offtime`= '{$this->io->input["post"]["offtime"]}',
	*/
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["post"]["behav"]}_update', 
	`active` = '{$page_name}修改寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["post"]["behav"]}/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));