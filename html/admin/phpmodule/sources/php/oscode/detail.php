<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='h'){
	$page_name='一般發送記錄詳情';
}else{
	$page_name='序號發送記錄詳情';
}

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(spid|name|ontime|offtime|seq|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}


if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End


// Search Start
$status["status"]["search"] = "";
$sub_search_query = "AND sp.spid IS NOT NULL ";

$status["status"]["search"]["spid"] = $this->io->input["get"]["spid"] ;
$status["status"]["search_path"] .= "&spid=".$this->io->input["get"]["spid"] ;
$sub_search_query .=  "AND s.`spid` ='{$this->io->input["get"]["spid"]}' ";

$join_search_query = "LEFT OUTER JOIN `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}scode_promote` sp ON 
	sp.prefixid = s.prefixid
	AND sp.spid = s.spid
	AND sp.`switch`='Y'
LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` u1 ON 
	u1.prefixid = s.prefixid
	AND u1.userid = s.userid
	AND u1.`switch`='Y'
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p ON 
	p.prefixid = s.prefixid
	AND p.productid = s.productid
	AND p.`switch`='Y'
";

/*
if(!empty($this->io->input["get"]["search_ontime"])){
	$status["status"]["search"]["search_ontime"] = $this->io->input["get"]["search_ontime"] ;
	$status["status"]["search_path"] .= "&search_ontime=".$this->io->input["get"]["search_ontime"] ;
	$sub_search_query .=  "
		AND s.`ontime` >= '".$this->io->input["get"]["search_ontime"]."'";
}
*/

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}oscode` s
{$join_search_query}
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query = "SELECT s.*, sp.name spname, u1.name u1name, p.name pname 
FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}oscode` s
{$join_search_query}
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`switch`='Y'
	
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
/*
$query ="
SELECT c.* 
FROM `{$db_channel}`.`{$this->config->default_prefix}channel` c 
WHERE 
c.prefixid = '{$this->config->default_prefix_id}' 
AND c.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_rt"] = $recArr['table']['record'];
*/
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'oscode', 
	`active` = '殺價券{$page_name}清單查詢', 
	`memo` = '{$scode_data}', 
	`root` = 'oscode/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();