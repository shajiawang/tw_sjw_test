<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='h'){
	$page_name='一般發送管理';
}else{
	$page_name='序號發送管理';
}

##############################################################################################################################################
// Table  Start 
//可供選擇的產品編號
$query3 = "SELECT productid,name From  `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product`  WHERE
					prefixid = '{$this->config->default_prefix_id}' 
					AND unix_timestamp( offtime ) >0 
					AND unix_timestamp() >= unix_timestamp( ontime ) 
					AND unix_timestamp() <= unix_timestamp( offtime ) 
					AND switch = 'Y'
			";
$recArr3 = $this->model->getQueryRecord($query3);
$table["table"]["rt"]["valid_product"]=$recArr3['table']['record'];
// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["get"]["t"]}_add', 
	`active` = '殺價券{$page_name}新增', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["get"]["t"]}/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]['get']['t'] = $this->io->input["get"]["t"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();