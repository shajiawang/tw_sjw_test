<?php
// Check Variable Start

if(!empty($this->io->input["post"]["type"]) && $this->io->input["post"]["type"]=='new_scode_item') {
	if(empty($this->io->input["post"]["qty"])) {
		$this->jsAlertMsg('序號建立數量錯誤!!');
	}
}
else
{
	if(empty($this->io->input["post"]["name"])) {
		$this->jsAlertMsg('活動名稱錯誤!!');
	}
	if(empty($this->io->input["post"]["productid"]) || !is_numeric($this->io->input["post"]["productid"]) ) {
		$this->jsAlertMsg('競標商品ID錯誤!!');
	}
	if(!is_numeric($this->io->input["post"]["seq"])) {
		$this->jsAlertMsg('排序錯誤!!');
	}
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["post"]["behav"]=='h'){
	$page_name='一般發送管理';
}else{
	$page_name='序號發送管理';
}

##############################################################################################################################################
// Insert Start
if(!empty($this->io->input["post"]["spid"]) && $this->io->input["post"]["type"]=='new_scode_item')
{
	//取得 S碼活動
	$query = "SELECT s.*
	FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` s
	WHERE 
		s.prefixid = '{$this->config->default_prefix_id}' 
		AND s.spid = '{$this->io->input["post"]["spid"]}'
		AND s.switch = 'Y'
	";
	/*
	AND unix_timestamp( s.offtime ) >0 
	AND unix_timestamp() >= unix_timestamp( s.ontime ) 
	AND unix_timestamp() <= unix_timestamp( s.offtime ) 
	*/
	$rs = $this->model->getQueryRecord($query);
	
	if(empty($rs['table']['record'][0]) ) {
		$this->jsAlertMsg('S碼活動錯誤!!');
	} 
	else
	{
		$batch = time();
		for($i=0; $i<(int)$this->io->input["post"]["qty"]; $i++)
		{
			$v = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
			shuffle($v);
			
			$rand = $batch * rand(1,9);
			$rand1 = substr($rand, -6, 2);	
			$rand2 = substr($rand, -3, 2);
			$_serial = $v[0]. $rand1 . $v[1] . $rand2 . $v[2] . $v[3];
			$serial = $_serial . sprintf("%04d", $i);
			
			shuffle($v);
			$pwd = $v[0] . $v[1] . $rand1 . $v[2] . $v[3] . $rand2 . $v[4] . $v[5];
			
			$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}oscode` 
			SET
				`prefixid`='{$this->config->default_prefix_id}',
				`spid`='{$this->io->input["post"]["spid"]}',
				`amount`='1',
				`serial`='{$serial}',
				`pwd`='{$pwd}',
				`verified`='N',
				`productid`='{$rs['table']['record'][0]['productid']}',
				`insertt`=now()
			";
			$this->model->query($query);
		}
	}
}
else
{
	$behav = $this->io->input["post"]["behav"];
	//限定S碼發送組數
	if($behav=='h'){ 
		$setunum = "onum='{$this->io->input["post"]["onum"]}',"; 
	}else{
		$setunum = '';
	}
	
	$query ="INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote` 
	SET
		{$setunum}
		`prefixid`='{$this->config->default_prefix_id}',
		`behav`='{$this->io->input["post"]["behav"]}',
		`name`='{$this->io->input["post"]["name"]}',
		`description`= '{$this->io->input["post"]["description"]}',
		`productid`='{$this->io->input["post"]["productid"]}',
		`seq`='{$this->io->input["post"]["seq"]}',
		`insertt`=now()
	" ;
	/*
		`ontime`= '{$this->io->input["post"]["ontime"]}',
		`offtime`= '{$this->io->input["post"]["offtime"]}',
		*/
	$this->model->query($query);
}

// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["post"]["behav"]}_insert', 
	`active` = '殺價券{$page_name}新增寫入', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["post"]["behav"]}/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));