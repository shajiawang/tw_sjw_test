<?php
// Check Variable Start
if (empty($this->io->input["get"]["spid"])) {
	$this->jsAlertMsg('活動ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if($this->io->input["get"]["t"]=='h'){
	$page_name='一般發送管理';
}else{
	$page_name='序號發送管理';
}

##############################################################################################################################################
// Table  Start 

// Table Content Start
$query ="SELECT * 
FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}scode_promote`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `spid` = '{$this->io->input["get"]["spid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

$query ="SELECT count(*) num 
FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}oscode`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND ( used='Y' OR verified='Y' ) 
	AND `spid` = '{$this->io->input["get"]["spid"]}'
" ;
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["oscode_used"] = $recArr['table']['record'][0];

$query3 = "SELECT productid,name From  `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product`  WHERE
					prefixid = '{$this->config->default_prefix_id}' 
					AND unix_timestamp( offtime ) >0 
					AND unix_timestamp() >= unix_timestamp( ontime ) 
					AND unix_timestamp() <= unix_timestamp( offtime ) 
					AND switch = 'Y' OR (productid='".$table["table"]["record"][0]['productid']."')
			";

$recArr3 = $this->model->getQueryRecord($query3);
$table["table"]["rt"]["valid_product"]=$recArr3['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_{$this->io->input["get"]["t"]}_edit', 
	`active` = '殺價券{$page_name}修改', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_{$this->io->input["get"]["t"]}/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]['get']['t'] = $this->io->input["get"]["t"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();