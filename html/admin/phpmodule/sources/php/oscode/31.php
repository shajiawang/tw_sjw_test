<?php
$page_name='一般發送管理';

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(spid|name|ontime|offtime|seq|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}


if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "s.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
} else {
	$sub_sort_query =  " ORDER BY s.spid DESC ";
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "AND s.`behav`='h' ";
if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND s.`name` like '%".$this->io->input["get"]["search_name"]."%'";
}
/*
if(!empty($this->io->input["get"]["search_ontime"])){
	$status["status"]["search"]["search_ontime"] = $this->io->input["get"]["search_ontime"] ;
	$status["status"]["search_path"] .= "&search_ontime=".$this->io->input["get"]["search_ontime"] ;
	$sub_search_query .=  "
		AND s.`ontime` >= '".$this->io->input["get"]["search_ontime"]."'";
}
if(!empty($this->io->input["get"]["search_offtime"])){
	$status["status"]["search"]["search_offtime"] = $this->io->input["get"]["search_offtime"] ;
	$status["status"]["search_path"] .= "&search_offtime=".$this->io->input["get"]["search_offtime"] ;
	$sub_search_query .=  "
		AND s.`offtime` <= '".$this->io->input["get"]["search_offtime"]."'";
}
*/
/*
if(is_array($this->io->input["get"]["search_channelid"]) && !empty($this->io->input["get"]["search_channelid"])){
	$status["status"]["search"]["search_channelid"] = $this->io->input["get"]["search_channelid"] ;
	$status["status"]["search_path"] .= "&search_channelid=".$this->io->input["get"]["search_channelid"] ;
	$sub_search_query .=  "
		AND `channelid` = '". $this->io->input["get"]["search_channelid"] ."' ";
}*/
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote` s 
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p ON
	p.prefixid = s.prefixid
	AND p.productid = s.productid
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="SELECT s.*, p.name pname
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote` s 
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p ON
	p.prefixid = s.prefixid
	AND p.productid = s.productid
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`switch`='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 
// Table Record End 

// Table End 
##############################################################################################################################################

##############################################################################################################################################
// Relation Start 
/*
$query ="
SELECT c.* 
FROM `{$db_channel}`.`{$this->config->default_prefix}channel` c 
WHERE 
c.prefixid = '{$this->config->default_prefix_id}' 
AND c.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_rt"] = $recArr['table']['record'];
*/
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'scode_promote_h', 
	`active` = '殺價券活動清單查詢', 
	`memo` = '{$scode_data}', 
	`root` = 'scode_promote_h/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();