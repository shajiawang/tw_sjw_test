<?php
$page_name='實體發送序號明細';

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(spid|name|ontime|offtime|seq|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
} 
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "AND sp.spid IS NOT NULL ";
$sub_search_query .= "AND si.`spid` ='{$this->io->input["get"]["spid"]}' ";
$status["status"]["search"]["spid"] = $this->io->input["get"]["spid"];
$status["status"]["search_path"] = "&spid=".$this->io->input["get"]["spid"];

$join_search_query = "LEFT OUTER JOIN `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}scode_promote` sp ON
	sp.prefixid = si.prefixid
	AND sp.spid = si.spid
	AND sp.`switch`='Y'
LEFT OUTER JOIN `{$this->config->db[0]["dbname"]}`.`{$this->config->default_prefix}user` u1 ON
	u1.prefixid = si.prefixid
	AND u1.userid = si.userid
	AND u1.`switch`='Y'
";

if($this->io->input["get"]["search_serial"] != ''){
	$status["status"]["search"]["search_serial"] = $this->io->input["get"]["search_serial"] ;
	$status["status"]["search_path"] .= "&search_serial=".$this->io->input["get"]["search_serial"] ;
	$sub_search_query .= "AND si.`serial` ='".$this->io->input["get"]["search_serial"]."' ";
}
if($this->io->input["get"]["search_verified"] != ''){
	$status["status"]["search"]["search_verified"] = $this->io->input["get"]["search_verified"] ;
	$status["status"]["search_path"] .= "&search_verified=".$this->io->input["get"]["search_verified"] ;
	$sub_search_query .= "AND si.`verified` ='".$this->io->input["get"]["search_verified"]."' ";
}
if($this->io->input["get"]["search_productid"] != ''){
	$status["status"]["search"]["search_productid"] = $this->io->input["get"]["search_productid"] ;
	$status["status"]["search_path"] .= "&search_productid=".$this->io->input["get"]["search_productid"] ;
	$sub_search_query .= "AND si.`productid` ='".$this->io->input["get"]["search_productid"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}oscode` si
{$join_search_query}
WHERE 
	si.`prefixid` = '{$this->config->default_prefix_id}' 
	AND si.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query = "SELECT si.*, sp.name spname, u1.name u1name ";
$query .= "FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}oscode` si
{$join_search_query}
WHERE 
	si.`prefixid` = '{$this->config->default_prefix_id}' 
	AND si.`switch`='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'oscode', 
	`active` = '殺價券序號明細清單查詢', 
	`memo` = '{$scode_data}', 
	`root` = 'oscode/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->tplVar('page_name', $page_name);
$this->display();