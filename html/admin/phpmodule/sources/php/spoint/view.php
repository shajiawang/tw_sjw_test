<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(switch|spointid|userid|esid|fromid|behav|amount/remark|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY i.spointid DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = " i.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if(!empty($this->io->input["get"]["search_spointid"])){
	$status["status"]["search"]["search_spointid"] = $this->io->input["get"]["search_spointid"] ;
	$status["status"]["search_path"] .= "&search_spointid=".$this->io->input["get"]["search_spointid"] ;
	$sub_search_query .=  "	AND i.`spointid` = '".strtoupper($this->io->input["get"]["search_spointid"])."' ";
}
if(!empty($this->io->input["get"]["search_userid"])){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "	AND i.`userid` = '".$this->io->input["get"]["search_userid"]."' ";
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	$sub_search_query .=  " AND i.`insertt` BETWEEN '".$this->io->input["get"]["search_btime"]."' AND '".$this->io->input["get"]["search_etime"]."' ";
}
if(!empty($this->io->input["get"]["search_behav"])){
	$status["status"]["search"]["search_behav"] = $this->io->input["get"]["search_behav"] ;
	$status["status"]["search_path"] .= "&search_behav=".$this->io->input["get"]["search_behav"] ;
	$sub_search_query .=  "	AND i.`behav` = '".$this->io->input["get"]["search_behav"]."' ";
}
if(!empty($this->io->input["get"]["search_remark"])){
	$status["status"]["search"]["search_remark"] = $this->io->input["get"]["search_remark"] ;
	$status["status"]["search_path"] .= "&search_remark=".$this->io->input["get"]["search_remark"] ;
	$sub_search_query .=  "	AND i.`remark` = '".$this->io->input["get"]["search_remark"]."' ";
}
if(!empty($this->io->input["get"]["search_switch"])){
	$status["status"]["search"]["search_switch"] = $this->io->input["get"]["search_switch"] ;
	$status["status"]["search_path"] .= "&search_switch=".$this->io->input["get"]["search_switch"] ;
	$sub_search_query .=  "	AND i.`switch` = '".$this->io->input["get"]["search_switch"]."' ";
}


// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="SELECT count(*) as num
	       FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` i 
	    {$category_search_query}
	     WHERE i.prefixid = '".$this->config->default_prefix_id."' 
	       AND i.spointid IS NOT NULL " ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);  //打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="SELECT i.spointid, up.nickname, i.userid, i.esid, i.fromid, i.countryid, i.behav, i.amount, i.remark, i.memo, i.switch, i.insertt, i.modifyt 
            FROM `saja_cash_flow`.`saja_spoint` i JOIN `saja_user`.`saja_user_profile` up
			  ON i.userid=up.userid
	       WHERE i.prefixid = '".$this->config->default_prefix_id."' 
  		     AND i.spointid IS NOT NULL ";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];



$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();