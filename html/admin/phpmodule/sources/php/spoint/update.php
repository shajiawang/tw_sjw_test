<?php
include_once "saja/mysql.ini.php";


error_log("[spoint/update] location_url : ".urldecode(base64_decode($this->io->input['post']['location_url'])));
$spointid=$this->io->input["post"]["spointid"];
if(empty($spointid)) {
   $spointid=$this->io->input["get"]["spointid"];	
}
if (empty($spointid)) {
	$this->jsAlertMsg('殺幣資料編號錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}

$userid=$this->io->input["post"]["userid"];
if(empty($userid)) {
   $userid=$this->io->input["get"]["userid"];	
}
if (empty($userid)) {
    $this->jsAlertMsg('用戶編號錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}

$switch=$this->io->input["post"]["switch"];
if(empty($switch)) {
   $switch=$this->io->input["get"]["switch"];	
}
if (empty($switch)) {
    $this->jsAlertMsg('資料狀態錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}

$behav=$this->io->input["post"]["behav"];
if(empty($behav)) {
   $behav=$this->io->input["get"]["behav"];	
}
if (empty($behav)) {
    $this->jsAlertMsg('來源/用途資料項錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}

$amount=$this->io->input["post"]["amount"];
if(empty($amount)) {
   $amount=$this->io->input["get"]["amount"];	
}
if (empty($amount)) {
    $this->jsAlertMsg('殺幣點數錯誤!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}
if(!is_numeric($amount)) {
	$this->jsAlertMsg('殺幣點數需為數字 !');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}

$remark=$this->io->input["post"]["remark"];
if(empty($remark)) {
   $remark=$this->io->input["get"]["remark"];	
}
if (empty($remark)) {
    $remark="0";
}

$memo=$this->io->input["post"]["memo"];
if(empty($memo)) {
   $memo=$this->io->input["get"]["memo"];	
}

$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$query =" UPDATE `saja_cash_flow`.`saja_spoint` SET 
			 `switch` = '{$switch}'
			,`amount` = '{$amount}'
			,`userid` = '{$userid}'
			,`behav` = '{$behav}'
			,`remark` = '{$remark}'
			,`memo`='{$memo}'
           WHERE prefixid = '{$this->config->default_prefix_id}'
			 AND spointid = '{$spointid}' " ;
			 
$ret = $this->model->query($query);

error_log("[spoint/update] query : ".$query." ->".$ret);

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));