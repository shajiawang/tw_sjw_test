<?php
// Check Variable Start
if (empty($this->io->input["get"]["dtid"])) {
	$this->jsAlertMsg('活動ID錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT dt.* 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_times` dt 
WHERE 
	dt.`prefixid` = '{$this->config->default_prefix_id}' 
	AND dt.`dtid` = '{$this->io->input["get"]["dtid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_times_edit', 
	`active` = '首次儲值倍數送修改', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_times/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();