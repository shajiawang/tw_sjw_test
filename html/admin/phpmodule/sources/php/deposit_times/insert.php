<?php
// Check Variable Start
if (empty($this->io->input["post"]["times"])) {
	$this->jsAlertMsg('倍數錯誤!!');
}
if (strtotime($this->io->input["post"]["btime"]) === false) {
	$this->jsAlertMsg('開始時間錯誤!!');
}
if (strtotime($this->io->input["post"]["etime"]) === false) {
	$this->jsAlertMsg('結束時間錯誤!!');
}

// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Update Start

if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	$thumbnail = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["times"])).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_deposit_images."/$thumbnail") && ($thumbnail != $this->io->input['post']['oldthumbnail'])) {
		$this->jsAlertMsg('主圖名稱重覆!!');
	}
	else {
        if (move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_deposit_images."/$thumbnail")) {
           syncToS3($this->config->path_deposit_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/deposit/');
        } else {
		    $this->jsAlertMsg('主圖上傳錯誤!!');
        }
	}
	
	$insert_thumbnail = "`thumbnail` = '{$thumbnail}', ";
	syncToS3($this->config->path_deposit_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/deposit/');
}

$query = "
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_times` SET 
	`times` 	= '{$this->io->input['post']['times']}',
	{$insert_thumbnail}	
	`btime`   	= '{$this->io->input["post"]["btime"]}',
	`etime` 	= '{$this->io->input["post"]["etime"]}',
	`insertt`	= now()
";
$this->model->query($query);
	
// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_times_insert', 
	`active` = '首次儲值倍數送新增寫入', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit_times/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));