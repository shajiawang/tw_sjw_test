<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
 
$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

//搜尋條件
$sub_search_query = "";
$sub_sort_query =  " ORDER BY prdate_no DESC, u_date DESC, ibonexcid DESC ";


if($data["search_no"] != ''){
	$sub_search_query .=  "AND prdate_no ='{$data["search_no"]}' ";
}
if($data["search_code"] != ''){
	$sub_search_query .=  "AND code ='{$data["search_code"]}' ";
}
if($data["search_pname"] != ''){

	$sub_search_query .=  "AND `prname` like '%".$data["search_pname"]."%' ";
}
if($data["search_btime"] != '' && $data["search_etime"] != ''){
	$sub_search_query .=  "AND `u_date` BETWEEN '".$data["search_btime"]."' AND '".$data["search_etime"]."' ";
}
if($data["search_uid"] != ''){
	$sub_search_query .=  "AND `u_id` = '{$data["search_uid"]}' ";
}

$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_ibon_record` WHERE 1=1 " ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 


##############################################################################################################################################
// Log Start 
$ibon_data = json_encode($data);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ibon_record', 
	`active` = 'ibon交易紀錄清單匯出', 
	`memo` = '{$ibon_data}', 
	`root` = 'ibon_record/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//資料表
$Report_Array[0]['title'] = array("A1" => "交易編號", "B1" => "商品編號", "C1" => "活動代號", "D1" => "商品名稱", "E1" => "會員編號", "F1" => "總點數", "G1" => "兌換檔次", "H1" => "兌換日期", "I1" => "備註");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['ibonexcid']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['id']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['code']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['prname']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['u_id']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['tr_ibons']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['prdate_no']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['u_date']);
		
		$Arr = array("9","28","116","1777","2788","3682","3691","3807","5424","6486","29109","40617","46656","62427","133665","136254","155256","179646","185541","187218","195102","197670","456393","456780","456787","457888","465141");
		
		if (in_array($value['u_id'], $Arr)) {
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$row, '內部員工');
		}else{
			$objPHPExcel->getActiveSheet()->setCellValue('I'.$row, '一般會員');
		}
		
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('ibon交易紀錄');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="ibon交易紀錄'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
		
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";
 
// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>