<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start

//check enterprise data 
if (empty($this->io->input["post"]["exchange_store"])) {
	$this->jsAlertMsg('請指定綁定店家 !!');
}
if (empty($this->io->input["post"]["loginname"])) {
	$this->jsAlertMsg('請填寫登入帳號 !!');
}
if (empty($this->io->input["post"]["passwd"])) {
	$this->jsAlertMsg('請填寫登入密碼 !!');
}
if (empty($this->io->input["post"]["passwd_confirm"]) || $this->io->input["post"]["passwd"] != $this->io->input["post"]["passwd_confirm"]) {
	$this->jsAlertMsg('密碼確認錯誤 !!');
}
if (empty($this->io->input["post"]["contact_mobile"])) {
	$this->jsAlertMsg('聯絡手機錯誤!!');
}
if (empty($this->io->input["post"]["profit_ratio"])) {
	$this->jsAlertMsg('請設定分潤比!!');
}
/*
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('請指定綁定用戶 !!');
}

if (empty($this->io->input["post"]["companyname"])) {
	$this->jsAlertMsg('請填寫企業名稱 !!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('請填寫負責人姓名 !!');
}
if (empty($this->io->input["post"]["phone"])) {
	$this->jsAlertMsg('請填寫聯絡電話 !!');
}
if (empty($this->io->input["post"]["execute_contactor"])) {
	$this->jsAlertMsg('請填寫實際營運人姓名 !!');
}
if (empty($this->io->input["post"]["execute_phone"])) {
	$this->jsAlertMsg('請填寫實際營運人聯絡電話 !!');
}
if (empty($this->io->input["post"]["exchangepasswd"])) {
	$this->jsAlertMsg('請填寫提現密碼 !!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('電子信箱錯誤!!');
}
if (!filter_var($this->io->input['post']['email'], FILTER_VALIDATE_EMAIL)) {
	$this->jsAlertMsg('電子信箱格式錯誤!!');
}

if (empty($this->io->input["post"]["exchange_store"])) {
	$this->jsAlertMsg('兌換中心錯誤!!');
}

//check enterprise_profile data
if (empty($this->io->input["post"]["companyname"])) {
	$this->jsAlertMsg('公司名稱錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('負責人姓名!!');
}
if (empty($this->io->input["post"]["uniform"])) {
	$this->jsAlertMsg('統一編號錯誤!!');
}
if (empty($this->io->input["post"]["country"])) {
	$this->jsAlertMsg('國家錯誤!!');
}
if (empty($this->io->input["post"]["channel"])) {
	$this->jsAlertMsg('經銷商!!');
}
if (empty($this->io->input["post"]["area"]) || empty($this->io->input["post"]["address"])) {
	$this->jsAlertMsg('公司地址錯誤!!');
}

//check enterprise_account data
if (empty($this->io->input["post"]["account_contactor"])) {
	$this->jsAlertMsg('帳務負責人錯誤!!');
}
if (empty($this->io->input["post"]["account_title"])) {
	$this->jsAlertMsg('帳務負責人職務錯誤!!');
}
if (empty($this->io->input["post"]["account_phone"])) {
	$this->jsAlertMsg('帳務負責人手機號碼錯誤!!');
}
if (empty($this->io->input["post"]["execute_contactor"])) {
	$this->jsAlertMsg('帳務實際營運人錯誤!!');
}
if (empty($this->io->input["post"]["execute_title"])) {
	$this->jsAlertMsg('帳務實際營運人職務錯誤!!');
}
if (empty($this->io->input["post"]["execute_phone"])) {
	$this->jsAlertMsg('帳務實際營運人手機號碼錯誤!!');
}
if (empty($this->io->input["post"]["accountname"])) {
	$this->jsAlertMsg('撥款帳戶!!');
}
if (empty($this->io->input["post"]["accountnumber"])) {
	$this->jsAlertMsg('撥款帳號!!');
}
if (empty($this->io->input["post"]["enterprisename"])) {
	$this->jsAlertMsg('銀行名稱!!');
}
if (empty($this->io->input["post"]["branchname"])) {
	$this->jsAlertMsg('分行名稱!!');
}
*/
/*
//check enterprise_shop data
if (empty($this->io->input["post"]["marketingname"])) {
	$this->jsAlertMsg('店家名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["url"])) {
	$this->jsAlertMsg('網址錯誤!!');
}
if (strtotime($this->io->input["post"]["businessontime"]) === false) {
	$this->jsAlertMsg('營業開始時間錯誤!!');
}
if (strtotime($this->io->input["post"]["businessofftime"]) === false) {
	$this->jsAlertMsg('營業結束時間錯誤!!');
}
if (empty($this->io->input["post"]["turnover"])) {
	$this->jsAlertMsg('月平均營業額錯誤!!');
}
if (empty($this->io->input["post"]["transaction"])) {
	$this->jsAlertMsg('單筆平均交易額錯誤!!');
}
if (empty($this->io->input["post"]["trade"])) {
	$this->jsAlertMsg('行業型態錯誤!!');
}
if (empty($this->io->input["post"]["marketingmodel"])) {
	$this->jsAlertMsg('行銷模式錯誤!!');
}
if(!empty($this->io->input["post"]["business_area"]) && !is_numeric($this->io->input["post"]["business_area"]) ) {
	$this->jsAlertMsg('營業面積錯誤!!');
}
if(!empty($this->io->input["post"]["employees"]) && !is_numeric($this->io->input["post"]["employees"]) ) {
	$this->jsAlertMsg('員工人數錯誤!!');
}
if(!empty($this->io->input["post"]["franchise_store"]) && !is_numeric($this->io->input["post"]["franchise_store"]) ) {
	$this->jsAlertMsg('分店/加盟店錯誤!!');
}
if (empty($this->io->input["post"]["seniority"])) {
	$this->jsAlertMsg('營業年資錯誤!!');
}
if (empty($this->io->input["post"]["source"])) {
	$this->jsAlertMsg('商品來源錯誤!!');
}
if (empty($this->io->input["post"]["card_demand"])) {
	$this->jsAlertMsg('國外信用卡需求錯誤!!');
}
if (empty($this->io->input["post"]["operation_overview"])) {
	$this->jsAlertMsg('營運概況錯誤!!');
}
if (empty($this->io->input["post"]["contact_email"])) {
	$this->jsAlertMsg('聯絡信箱錯誤!!');
}
if (empty($this->io->input["post"]["contact_phone"])) {
	$this->jsAlertMsg('聯絡電話錯誤!!');
}
if (!filter_var($this->io->input['post']['contact_email'], FILTER_VALIDATE_EMAIL)) {
	$this->jsAlertMsg('聯絡信箱格式錯誤!!');
}
if (empty($this->io->input["post"]["service_email"])) {
	$this->jsAlertMsg('客服信箱錯誤!!');
}
if (!filter_var($this->io->input['post']['service_email'], FILTER_VALIDATE_EMAIL)) {
	$this->jsAlertMsg('客服信箱格式錯誤!!');
}
*/
//查詢商家使用者帳號是否重複
$query = "SELECT count(enterpriseid) as num FROM `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise` 
	WHERE prefixid = '{$this->config->default_prefix_id}' 
	  AND loginname = '{$this->io->input["post"]["loginname"]}'
	  AND switch = 'Y' ";
$table = $this->model->getQueryRecord($query);
	
if ($table['table']['record'][0]["num"]>0) {
	$this->jsAlertMsg('使用者帳號重覆，請重新填寫!!');
} else {
	//新增企業登入資料
	$query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise` 
	set prefixid = '{$this->config->default_prefix_id}', 
		esid = '{$this->io->input["post"]["exchange_store"]}', 
		loginname = '{$this->io->input["post"]["loginname"]}', 
		passwd = '{$this->str->strEncode($this->io->input["post"]["passwd"], $this->config->encode_key)}', 
		exchangepasswd = '{$this->str->strEncode($this->io->input["post"]["exchangepasswd"], $this->config->encode_key)}', 
		email = '{$this->io->input["post"]["email"]}', 
		verified = 'Y', 
		seq = 0, 
		switch = 'Y',
		insertt = NOW()
	";
	$this->model->query($query);
	$enterpriseid = $this->model->_con->insert_id;
	error_log("[admin/enterprise/insert] Create enterpriseid : ".$enterpriseid);
	
	if (!empty($enterpriseid)) {
		
		//新增企業-用戶關聯資料
		$query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}user_enterprise_rt`
		             set enterpriseid='{$this->io->input["post"]["exchange_store"]}',
					     userid='{$this->io->input["post"]["userid"]}',
						 insertt=NOW(),
						 switch='Y' ";
		$this->model->query($query);
		$ueid=$this->model->_con->insert_id;
		error_log("[admin/enterprise/insert] Create user_enterprise_rt id : ".$ueid);
	}	
		
	if (!empty($this->io->input['files']['thumbnail_file']) && exif_imagetype($this->io->input['files']['thumbnail_file']['tmp_name'])) {
		$filename = md5(date("YmdHis")."_".$this->io->input['post']['name']).".".pathinfo($this->io->input['files']['thumbnail_file']['name'], PATHINFO_EXTENSION);
		
		if (file_exists($this->config->path_headimgs_images."/".$filename)) {
			$this->jsAlertMsg('企業LOGO名稱重覆!!');
		} else if (!move_uploaded_file($this->io->input['files']['thumbnail_file']['tmp_name'], $this->config->path_headimgs_images."/$filename")) {
			$this->jsAlertMsg('企業LOGO上傳錯誤!!');
		}
	}
		
	//新增企業基本資料
	$query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_profile` 
	set 
		prefixid = '{$this->config->default_prefix_id}', 
		enterpriseid = '{$enterpriseid}', 
		companyname = '{$this->io->input["post"]["companyname"]}',
		marketingname = '{$this->io->input["post"]["companyname"]}',
		thumbnail_file = '{$filename}', 
		thumbnail_url = '{$this->io->input["post"]["thumbnail_url"]}', 
		name = '{$this->io->input["post"]["name"]}', 
		uniform = '{$this->io->input["post"]["uniform"]}', 
		phone = '{$this->io->input["post"]["phone"]}', 
		fax = '{$this->io->input["post"]["fax"]}', 
		countryid = '{$this->io->input["post"]["country"]}', 
		channelid = '{$this->io->input["post"]["channel"]}', 
		area = '{$this->io->input["post"]["area"]}', 
		address = '{$this->io->input["post"]["address"]}', 
		seq = 0, 
		switch = 'Y',
		insertt = NOW()
	";
	$this->model->query($query);
	
	//新增企業帳務資料
	$query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_account` 
	set 
		prefixid = '{$this->config->default_prefix_id}', 
		enterpriseid = '{$enterpriseid}', 
		execute_contactor = '{$this->io->input["post"]["execute_contactor"]}', 
		execute_title = '{$this->io->input["post"]["execute_title"]}', 
		execute_phone = '{$this->io->input["post"]["execute_phone"]}', 
		account_contactor = '{$this->io->input["post"]["account_contactor"]}', 
		account_title = '{$this->io->input["post"]["account_title"]}', 
		account_phone = '{$this->io->input["post"]["account_phone"]}', 			
		accountname = '{$this->io->input["post"]["accountname"]}', 
		accountnumber = '{$this->io->input["post"]["accountnumber"]}', 
		enterprisename = '{$this->io->input["post"]["enterprisename"]}', 
		branchname = '{$this->io->input["post"]["branchname"]}', 
		seq = 0, 
		switch = 'Y',
		insertt = NOW()
	";
	$this->model->query($query);
	
	//新增企業商店資料
	$query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_shop` 
	set 
		prefixid = '{$this->config->default_prefix_id}', 
		enterpriseid = '{$enterpriseid}',
		contact_mobile = '{$this->io->input["post"]["contact_mobile"]}',
		profit_ratio = {$this->io->input["post"]["profit_ratio"]},			
		contact_phone = '{$this->io->input["post"]["contact_phone"]}', 			
		contact_email = '{$this->io->input["post"]["contact_email"]}', 
		url = '{$this->io->input["post"]["url"]}', 
		businessontime = '{$this->io->input["post"]["businessontime"]}', 
		businessofftime = '{$this->io->input["post"]["businessofftime"]}', 
		service_email = '{$this->io->input["post"]["service_email"]}', 
		turnover = '{$this->io->input["post"]["turnover"]}', 
		transaction = '{$this->io->input["post"]["transaction"]}', 
		trade = '{$this->io->input["post"]["trade"]}', 
		marketingmodel = '{$this->io->input["post"]["marketingmodel"]}', 
		business_area = '{$this->io->input["post"]["business_area"]}', 
		employees = '{$this->io->input["post"]["employees"]}', 
		franchise_store = '{$this->io->input["post"]["franchise_store"]}', 
		seniority = '{$this->io->input["post"]["seniority"]}', 
		source = '{$this->io->input["post"]["source"]}', 
		card_demand = '{$this->io->input["post"]["card_demand"]}', 
		operation_overview = '{$this->io->input["post"]["operation_overview"]}', 
		seq = 0, 
		switch = 'Y',
		insertt = NOW()
	";
	$this->model->query($query);
	
	/*
	} else {
		$this->jsAlertMsg('使用者帳號建立失敗，請重新填寫!!');
	}
    */
}

// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$enterprise_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'enterprise_insert', 
	`active` = '商家帳號新增寫入', 
	`memo` = '{$enterprise_data}', 
	`root` = 'enterprise/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));