<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

if (empty($this->io->input["get"]["enterpriseid"])) {
	$this->jsAlertMsg('企業id錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$query = " SELECT * FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise` 
			WHERE prefixid = '".$this->config->default_prefix_id."' 
			  AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
			  AND switch = 'Y' " ;
$table = $this->model->getQueryRecord($query);

##############################################################################################################################################
// Relation Start 
$query = "SELECT * FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_profile` 
WHERE prefixid = '".$this->config->default_prefix_id."'
	AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["enterprise_profile"] = $recArr['table']['record'];

$query = "
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_account` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["enterprise_account"] = $recArr['table']['record'];

$query = " SELECT * 
			 FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_shop` 
		    WHERE prefixid = '".$this->config->default_prefix_id."'
	          AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
	          AND switch = 'Y' ";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["enterprise_shop"] = $recArr['table']['record'];

// 綁定用戶
$query = "SELECT * FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_enterprise_rt` rt 
           JOIN  `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
		     ON  rt.userid = up.userid
		   WHERE rt.enterpriseid = '{$this->io->input["get"]["enterpriseid"]}'
	         AND rt.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["user_profile"] = $recArr['table']['record'];

//綁定兌換中心
$query = " SELECT * FROM `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_store` 
			WHERE prefixid = '".$this->config->default_prefix_id."' 
			  AND switch = 'Y' ";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_store"] = $recArr['table']['record'];

//國家
$query = "
select * 
from `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."country` 
where 
	prefixid = '".$this->config->default_prefix_id."'
	and switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country"] = $recArr['table']['record'];

//經銷商
$query = "
select * 
from `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` 
where 
	prefixid = '".$this->config->default_prefix_id."'
	and switch = 'Y'
	and name <> ''
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel"] = $recArr['table']['record'];
// Relation End 


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status', $status['status']);
//echo '<pre>';print_r($status);die();
$this->display();