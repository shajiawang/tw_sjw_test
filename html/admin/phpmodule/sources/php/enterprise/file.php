<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start
$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';

if($files['file_name']["error"] != 0 || empty($files['file_name']["size"]) ) {
	$this->jsAlertMsg('上傳錯誤!!');
}
else
{
	$enterpriseFile = $this->config->path_sources."/enterprise/".$files['file_name']['name']; 
	
    if(is_uploaded_file($files['file_name']['tmp_name']) ) {
		move_uploaded_file($files['file_name']['tmp_name'], "{$enterpriseFile}");
	}

	$handle = fopen($enterpriseFile, 'r+');
	if (is_resource($handle))
    {
        $insertt = date('Y-m-d H:i:s');
		$newData = array();
		
		$filesize = filesize($enterpriseFile);
		$data = fread($handle, $filesize);
		$data = mb_convert_encoding($data, "UTF-8", "big5");
		$newData = explode("\n", $data);
		//array_shift($newData);
		$newData = array_values(array_filter($newData));
		
		if (!empty($newData)) {
			foreach ($newData as $key => $value) {
				$dataArr[$key] = explode(",", $value);
			}
			//print_r($dataArr);exit;
			
			if (!empty($dataArr)) {
				//判斷是否空白
				foreach ($dataArr as $key => $value) {
					if ($key > 0) {
						foreach ($value as $key1 => $value1) {
							if ($value1 === "") {
								unlink($enterpriseFile);
								$this->jsAlertMsg($dataArr[0][$key1].' 請勿空白!!');
							}
						}
					}
				}
				
				//將第一列資料清除
				array_shift($dataArr);
				$dataArr = array_values(array_filter($dataArr));
				
				foreach ($dataArr as $key => $value) {
			        //查詢商家使用者帳號
			        $query = "SELECT enterpriseid 
			        	FROM `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise` 
			        	WHERE 
			        		prefixid = '{$this->config->default_prefix_id}' 
			        		AND loginname = '{$value[0]}'
			        		and switch = 'Y'
			        ";
			        $table = $this->model->getQueryRecord($query);
			        if (!empty($table['table']['record'][0]["enterpriseid"])) {
			        	unlink($enterpriseFile);
			        	$this->jsAlertMsg('使用者帳號重覆，請重新填寫!!');
			        }
			        
			        //查詢mall是否重覆
			        $query = "SELECT emid 
			        	FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_mall` 
			        	WHERE 
			        		prefixid = '{$this->config->default_prefix_id}' 
			        		AND name = '{$value[22]}'
			        		and switch = 'Y'
			        ";
			        $mall = $this->model->getQueryRecord($query);
			        if (!$mall['table']['record'][0]['emid']) {
			        	//新增Exchange_Mall資料
			        	$query = "insert into `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_mall` 
			        	set 
			        		prefixid = '{$this->config->default_prefix_id}', 
			        		name = '{$value[22]}',
			        		description = '{$value[22]}',
			        		seq = 0, 
			        		switch = 'Y', 
			        		insertt = '{$insertt}'
			        	";
			        	$this->model->query($query);
						$emid = $this->model->_con->insert_id;
			        }
			        else {
			        	$emid = $mall['table']['record'][0]['emid'];
			        }
			        
			        //查詢store是否重覆
			        $query = "SELECT esid 
			        	FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_store` 
			        	WHERE 
			        		prefixid = '{$this->config->default_prefix_id}' 
			        		AND name = '{$value[24]}'
			        		and switch = 'Y'
			        ";
			        $store = $this->model->getQueryRecord($query);
			        if (!empty($store['table']['record'][0]["esid"])) {
			        	unlink($enterpriseFile);
			        	$this->jsAlertMsg('分店名稱重覆，請重新填寫!!');
			        }
			        else {
			        	//新增Exchange_Store資料
			        	$query = "insert into `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_store` 
			        	set 
			        		prefixid = '{$this->config->default_prefix_id}', 
			        		emid = '{$emid}',
			        		name = '{$value[24]}',
			        		description = '{$value[24]}',
			        		seq = 0, 
			        		switch = 'Y', 
			        		insertt = '{$insertt}'
			        	";
			        	$this->model->query($query);
						$esid = $this->model->_con->insert_id;
			        }
			        
			        //新增企業資料
			        $query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise` 
			        set 
			        	prefixid = '{$this->config->default_prefix_id}', 
			        	esid = '{$esid}', 
			        	loginname = '{$value[0]}', 
			        	passwd = '{$this->str->strEncode($value[1], $this->config->encode_key)}', 
			        	exchangepasswd = '{$this->str->strEncode($value[2], $this->config->encode_key)}', 
			        	email = '{$value[0]}', 
			        	verified = 'Y', 
			        	seq = 0, 
			        	switch = 'Y',
			        	insertt = '{$insertt}'
			        ";
			        $this->model->query($query);
					$enterpriseid = $this->model->_con->insert_id;
					
					//新增企業基本資料
			        $query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_profile` 
			        set 
			        	prefixid = '{$this->config->default_prefix_id}', 
			        	enterpriseid = '{$enterpriseid}', 
			        	companyname = '{$value[3]}', 
			        	name = '{$value[4]}', 
			        	uniform = '{$value[5]}', 
			        	phone = '{$value[6]}', 
			        	fax = '{$value[7]}', 
			        	countryid = '{$value[8]}', 
			        	channelid = '{$value[9]}', 
			        	area = '{$value[10]}', 
			        	address = '{$value[11]}', 
			        	seq = 0, 
			        	switch = 'Y',
			        	insertt = '{$insertt}'
			        ";
			        $this->model->query($query);
			        
			        //新增企業帳務資料
			        $query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_account` 
			        set 
			        	prefixid = '{$this->config->default_prefix_id}', 
			        	enterpriseid = '{$enterpriseid}', 
			        	account_contactor = '{$value[12]}', 
			        	account_title = '{$value[13]}', 
			        	account_phone = '{$value[14]}', 
			        	execute_contactor = '{$value[15]}', 
			        	execute_title = '{$value[16]}', 
			        	execute_phone = '{$value[17]}', 
			        	accountname = '{$value[18]}', 
			        	accountnumber = '{$value[19]}', 
			        	bankname = '{$value[20]}', 
			        	branchname = '{$value[21]}', 
			        	seq = 0, 
			        	switch = 'Y',
			        	insertt = '{$insertt}'
			        ";
			        $this->model->query($query);
			        
			        //新增企業商店資料
			        $query = "insert into `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_shop` 
			        set 
			        	prefixid = '{$this->config->default_prefix_id}', 
			        	enterpriseid = '{$enterpriseid}', 
			        	marketingname = '{$value[22]}', 
			        	url = '{$value[25]}', 
			        	businessontime = '{$value[26]}', 
			        	businessofftime = '{$value[27]}', 
			        	contact_email = '{$value[39]}', 
			        	service_email = '{$value[40]}', 
			        	turnover = '{$value[28]}', 
			        	transaction = '{$value[29]}', 
			        	trade = '{$value[30]}', 
			        	marketingmodel = '{$value[31]}', 
			        	business_area = '{$value[32]}', 
			        	employees = '{$value[33]}', 
			        	franchise_store = '{$value[34]}', 
			        	seniority = '{$value[35]}', 
			        	source = '{$value[36]}', 
			        	card_demand = '{$value[37]}', 
			        	operation_overview = '{$value[38]}', 
			        	seq = 0, 
			        	switch = 'Y',
			        	insertt = '{$insertt}'
			        ";
			        $this->model->query($query);
			    }
			}
		}
		//exit;
        fclose($handle);
    }
    unlink($enterpriseFile);
	
	$query = "SELECT count(*) count
	FROM `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise`
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}' 
		AND insertt = '{$insertt}'
		AND switch = 'Y'
	";
	$rs = $this->model->getQueryRecord($query);
	$this->jsPrintMsg('已成功上傳 '. $rs['table']['record'][0]['count'] .' 筆!', $this->io->input['get']['location_url']);
	


##############################################################################################################################################
// Log Start 
$enterprise_data = json_encode($this->io->input["files"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'enterprise_file', 
	`active` = '商家帳號匯入', 
	`memo` = '{$enterprise_data}', 
	`root` = 'enterprise/file', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################	
}

// Insert Start
##############################################################################################################################################
//header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));