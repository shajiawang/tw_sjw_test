<?php
if (empty($this->io->input["get"]["enterpriseid"])) {
	$this->jsAlertMsg('企業ID不存在!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$query = "UPDATE `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise` 
SET 
switch = 'N'
WHERE
prefixid = '".$this->config->default_prefix_id."' 
AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."' 
";
$this->model->query($query);

$query = "UPDATE `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_profile` 
SET 
switch = 'N'
WHERE
prefixid = '".$this->config->default_prefix_id."' 
AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."' 
";
$this->model->query($query);

$query = "UPDATE `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_account` 
SET 
switch = 'N'
WHERE
prefixid = '".$this->config->default_prefix_id."' 
AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."' 
";
$this->model->query($query);

$query = "UPDATE `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}enterprise_shop` 
SET 
switch = 'N'
WHERE
prefixid = '".$this->config->default_prefix_id."' 
AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."' 
";
$this->model->query($query);


##############################################################################################################################################
// Log Start 
$enterprise_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'enterprise_delete', 
	`active` = '商家帳號刪除', 
	`memo` = '{$enterprise_data}', 
	`root` = 'enterprise/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// INSERT success message Start
$this->jsPrintMsg('刪除成功!!',$this->io->input["get"]["location_url"]);
// INSERT success message End