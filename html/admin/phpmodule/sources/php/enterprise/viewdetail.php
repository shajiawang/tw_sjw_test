<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

if (empty($this->io->input["get"]["enterpriseid"])) {
	$this->jsAlertMsg('企業id錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$query = "
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
	and switch = 'Y'
" ;
$table = $this->model->getQueryRecord($query);


##############################################################################################################################################
// Relation Start 
$query = "
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_profile` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
	and switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["enterprise_profile"] = $recArr['table']['record'];

$query = "
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_account` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
	and switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["enterprise_account"] = $recArr['table']['record'];

$query = "
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_shop` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND enterpriseid = '".$this->io->input["get"]["enterpriseid"]."'
	and switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["enterprise_shop"] = $recArr['table']['record'];

//兌換中心
$query = "
select * 
from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_store` 
where 
	prefixid = '".$this->config->default_prefix_id."'
	AND esid = '".$table["table"]['record'][0]["esid"]."'
	and switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_store"] = $recArr['table']['record'];

//國家
$query = "
select * 
from `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."country` 
where 
	prefixid = '".$this->config->default_prefix_id."'
	AND countryid = '".$table["table"]["rt"]["enterprise_profile"][0]["countryid"]."'
	and switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country"] = $recArr['table']['record'];

//經銷商
$query = "
select * 
from `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` 
where 
	prefixid = '".$this->config->default_prefix_id."'
	AND channelid = '".$table["table"]["rt"]["enterprise_profile"][0]["channelid"]."'
	and switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################
//月平均營業額
$status['status']["turnoverArr"] = array("1" => "10萬以下", "2" => "20萬以下", "3" => "50萬以下", "4" => "100萬以下", "5" => "100萬以上");
//單筆平均交易額
$status['status']["transactionArr"] = array("1" => "1千以下", "2" => "5千以下", "3" => "1萬以下", "4" => "2萬以下", "5" => "2萬以上");
//行業型態
$status['status']["tradeArr"] = array("1" => "會員制", "2" => "批發/經銷制", "3" => "連鎖加盟制");
//行銷模式
$status['status']["marketingmodelArr"] = array("1" => "個人工作室", "2" => "傳銷制", "3" => "零售業", "4" => "店面", "5" => "網路行銷", "6" => "電話行銷", "7" => "其他");
//商品來源
$status['status']["sourceArr"] = array("1" => "國內", "2" => "國外", "3" => "國內/外");
//國外信用卡需求
$status['status']["card_demandArr"] = array("1" => "有", "2" => "無");


##############################################################################################################################################
// Log Start 
$enterprise_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'enterprise_viewdetail', 
	`active` = '商家帳號詳細內容', 
	`memo` = '{$enterprise_data}', 
	`root` = 'enterprise/viewdetail', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status', $status['status']);
$this->display();