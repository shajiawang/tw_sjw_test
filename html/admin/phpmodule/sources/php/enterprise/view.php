<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(enterpriseid|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
/*
$join_search_query = "
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_profile` ep ON 
	e.prefixid = ep.prefixid
	AND e.enterpriseid = ep.enterpriseid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_account` ea ON 
	e.prefixid = ea.prefixid
	AND e.enterpriseid = ea.enterpriseid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_shop` es ON 
	es.prefixid = e.prefixid
	AND es.enterpriseid = e.enterpriseid
";
*/
$join_search_query = "
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_enterprise_rt` rt ON 
     e.enterpriseid = rt.enterpriseid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
     rt.userid = up.userid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_profile` ep ON 
	 e.enterpriseid = ep.enterpriseid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise_account` ea ON 
	 e.enterpriseid = ea.enterpriseid
LEFT OUTER JOIN `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_store` est ON 
     e.esid = est.esid
";
/*
if($this->io->input["get"]["search_loginname"] != ''){
	$status["status"]["search"]["search_loginname"] = $this->io->input["get"]["search_loginname"] ;
	$status["status"]["search_path"] .= "&search_loginname=".$this->io->input["get"]["search_loginname"] ;
	
	$sub_search_query .=  "AND e.`loginname` like '%".$this->io->input["get"]["search_loginname"]."%'
	";
}
*/
// 兌換商家名稱
if($this->io->input["get"]["search_storename"] != ''){
	$status["status"]["search"]["search_storename"] = $this->io->input["get"]["search_storename"] ;
	$status["status"]["search_path"] .= "&search_storename=".$this->io->input["get"]["search_storename"] ;
	$sub_search_query .=  " AND est.`name` like '%".$this->io->input["get"]["search_storename"]."%' ";
}

// 企業公司名稱
if($this->io->input["get"]["search_companyname"] != ''){
	$status["status"]["search"]["search_companyname"] = $this->io->input["get"]["search_companyname"] ;
	$status["status"]["search_path"] .= "&search_companyname=".$this->io->input["get"]["search_companyname"] ;
	$sub_search_query .=  " AND ep.`companyname` like '%".$this->io->input["get"]["search_companyname"]."%'
	";
}

// 負責人名稱
if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  " AND ep.`name` like '%".$this->io->input["get"]["search_name"]."%'
	";
}
// 統編
if($this->io->input["get"]["search_uniform"] != ''){
	$status["status"]["search"]["search_uniform"] = $this->io->input["get"]["search_uniform"] ;
	$status["status"]["search_path"] .= "&search_uniform=".$this->io->input["get"]["search_uniform"] ;
	$sub_search_query .=  " AND ep.`uniform` like '%".$this->io->input["get"]["search_uniform"]."%'
	";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."enterprise` e 
{$join_search_query}
WHERE e.prefixid = '".$this->config->default_prefix_id."'
 AND e.switch='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="SELECT e.*, ep.*, est.name as storename, up.nickname as username, up.userid
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."enterprise` e 
  {$join_search_query}
WHERE e.prefixid = '".$this->config->default_prefix_id."' 
  AND e.switch='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$enterprise_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'enterprise', 
	`active` = '商家帳號清單查詢', 
	`memo` = '{$enterprise_data}', 
	`root` = 'enterprise/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################
$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();