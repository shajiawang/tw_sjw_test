<?php
$rtn=array();
$rtn['retCode']=-1;
if (empty($this->io->input['session']['user'])) {
	$rtn['retMsg']='請先登入';
}else{
	if (empty($_REQUEST['Rtype'])){
		$rtn['retMsg']='缺少清單型別';
	}else{
		require_once '/var/www/html/admin/libs/helpers.php';
		$ipaddr=htmlspecialchars($_REQUEST['ipaddr']);
		if (empty($ipaddr)){
			$rtn['retMsg']='缺少ipaddress';
		}else{
			//檢查是否為正常的ip
			if (inet_pton($ipaddr)){
				$Rtype=htmlspecialchars($_REQUEST['Rtype']);
				//如果已在清單中視為要從清單中移除
				if (inlistRedistList($Rtype,$ipaddr)){
					if (removeRedisList($Rtype,$ipaddr)){
						$rtn['retCode']=1;
						$rtn['retStatus']='';
						$rtn['retMsg']="{$Rtype}新增ip {$ipaddr}成功";
					}else{
						$rtn['retMsg']="{$Rtype}新增ip {$ipaddr}失敗";
					}
				}else{
					//強制從另一方移除
					if ($Rtype=='white_list'){
						removeRedisList('black_list',$ipaddr);
					}else{
						removeRedisList('white_list',$ipaddr);
					}
					if (setRedisList($Rtype,$ipaddr)){
						$rtn['retCode']=1;
						$rtn['retStatus']='show';
						$rtn['retMsg']="{$Rtype}新增ip {$ipaddr}成功";
					}else{
						$rtn['retMsg']="{$Rtype}新增ip {$ipaddr}失敗";
					}
				}
			}else{
				$rtn['retMsg']='無效的ipaddress格式';
			}
		}
	}
}
echo json_encode(($rtn));
die();