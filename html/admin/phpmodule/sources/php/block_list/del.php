<?php
$rtn=array();
$rtn['retCode']=-1;
if (empty($this->io->input['session']['user'])) {
	$rtn['retMsg']='請先登入';
}else{
	if (empty($_REQUEST['Rtype'])){
		$rtn['retMsg']='缺少清單型別';
	}else{
		require_once '/var/www/html/admin/libs/helpers.php';
		$ipaddr=htmlspecialchars($_REQUEST['ipaddr']);
		if (empty($ipaddr)){
			$rtn['retMsg']='缺少ipaddress';
		}else{
			if (inet_pton($ipaddr)){
				$Rtype=htmlspecialchars($_REQUEST['Rtype']);
				if (removeRedisList($Rtype,$ipaddr)){
					$rtn['retCode']=1;
					$rtn['retMsg']='ip{$ipaddr}自{$Rtype}移除成功';
				}else{
					$rtn['retMsg']='ip{$ipaddr}自{$Rtype}移除失敗';
				}
			}else{
				$rtn['retMsg']='無效的ipaddress格式';
			}
		}
	}
}
echo json_encode(($rtn));
die();