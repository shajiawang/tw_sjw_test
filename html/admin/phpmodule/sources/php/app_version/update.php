<?php
// Check Variable Start
if (empty($this->io->input["post"]["appid"])) {
	$this->jsAlertMsg('appID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('app名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["osversion"])) {
	$this->jsAlertMsg('os版本號錯誤!!');
}
if (!is_numeric($this->io->input["post"]["appversion"])) {
	$this->jsAlertMsg('app版本號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start
$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}app_os` SET 
`name` = '{$this->io->input["post"]["name"]}',
`osversion`='{$this->io->input["post"]["osversion"]}',
`appversion`='{$this->io->input["post"]["appversion"]}',
`modifyt`='".date('Y-m-d H:i:s')."'
WHERE 
`appid` = '{$this->io->input["post"]["appid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$app_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'app_update', 
	`active` = 'app版本修改寫入', 
	`memo` = '{$app_data}', 
	`root` = 'app/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));