<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_affiliate_add', 
	`active` = '會員推薦新增', 
	`memo` = '{$user_data}', 
	`root` = 'user/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
//$this->model->query($query);
// Log End 
##############################################################################################################################################
$status["status"]['get']['location_url'] = '/admin/user_referral';

$this->tplVar('status',$status);
$this->display();