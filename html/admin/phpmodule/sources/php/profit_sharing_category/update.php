<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["pscid"])) {
	$this->jsAlertMsg('拆分帳目ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('拆分帳目名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('拆分帳目敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('拆分帳目排序錯誤!!');
}

$query ="
UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."profit_sharing_category`
SET
`name` = '".$this->io->input["post"]["name"]."',
`description` = '".$this->io->input["post"]["description"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND pscid = '".$this->io->input["post"]["pscid"]."'
" ;
$this->model->query($query);




##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'profit_sharing_category_update', 
	`active` = '拆分帳目分類修改寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'profit_sharing_category/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));