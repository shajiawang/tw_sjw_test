<?php
if (empty($this->io->input["get"]["pscid"])) {
	$this->jsAlertMsg('拆分帳目ID錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT 
*
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."profit_sharing_category`
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND pscid = '".$this->io->input["get"]["pscid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################




##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'profit_sharing_category_edit', 
	`active` = '拆分帳目分類修改', 
	`memo` = '{$prodcut_data}', 
	`root` = 'profit_sharing_category/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();