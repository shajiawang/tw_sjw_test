<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

// Check Variable Start
if(empty($this->io->input["get"]["epid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
if(empty($this->io->input["get"]["epftid2"]) && empty($this->io->input["get"]["epftid3"]) && empty($this->io->input["get"]["epftid4"])) {
	$this->jsAlertMsg('商品主圖ID錯誤!!');
}

// Check Variable End

##############################################################################################################################################
// Update Start

// exchange_product_thumbnail

if(!empty($this->io->input["get"]["epftid2"]) ) {

	$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET 
		switch = 'N' 
	WHERE
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `epftid` = '{$this->io->input["get"]["epftid2"]}'
	" ;
	$this->model->query($query);  

	// exchange_product
	$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET 
		`epftid2` = '0' 
	WHERE
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `epid` = '{$this->io->input["get"]["epid"]}'
	" ;
	$this->model->query($query);  

}

if(!empty($this->io->input["get"]["epftid3"]) ) {

	$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET 
		switch = 'N' 
	WHERE
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `epftid` = '{$this->io->input["get"]["epftid3"]}'
	" ;
	$this->model->query($query);  

	// exchange_product
	$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET 
		`epftid3` = '0' 
	WHERE
		`prefixid3` = '{$this->config->default_prefix_id}'
		AND `epid` = '{$this->io->input["get"]["epid"]}'
	" ;
	$this->model->query($query);  
}

if(!empty($this->io->input["get"]["epftid4"]) ) {

	$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET 
		switch = 'N' 
	WHERE
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `epftid` = '{$this->io->input["get"]["epftid4"]}'
	" ;
	$this->model->query($query);  

	// exchange_product
	$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET 
		`epftid4` = '0' 
	WHERE
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `epid` = '{$this->io->input["get"]["epid"]}'
	" ;
	$this->model->query($query);  
}

// Update End
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url']);
// success message End
