<?php
// Check Variable Start
if (empty($this->io->input["get"]["epid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table Start

// 原商品 Record
$query ="
SELECT p.* ,pt.original, pt.filename, pt.name imgname
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p
LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` pt ON
	p.prefixid = pt.prefixid
	AND p.eptid = pt.eptid
WHERE
p.`prefixid` = '{$this->config->default_prefix_id}'
AND p.`switch`='Y'
AND p.`epid` = '{$this->io->input["get"]["epid"]}'
";
$old_product = $this->model->getQueryRecord($query);

if (empty($old_product['table']['record'])) {
	$this->jsAlertMsg('商品錯誤!!');
}

$info_product = $old_product['table']['record'][0];

//Relation exchange_product_thumbnail
if (!empty($info_product["filename"]) ) {
	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` SET
	`name`='{$info_product["imgname"]}',
	`original`='{$info_product["original"]}',
	`filename`='{$info_product["filename"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$eptid = $this->model->_con->insert_id;
}
//給預設值
$eptid=(empty($eptid))?0:$eptid;
$prid=(empty($info_product["prid"]))?$prid=1:$info_product["prid"];
$ontime=date('Y-m-d H:i:s');
$offtime=date('Y-m-d H:i:s');
//產生新商品 exchange_product
$description = str_replace("'", '', htmlspecialchars($info_product["description"]) );

$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET
	`name`='[Copy] {$info_product["name"]}',
	`description`='{$description}',
	`eptid`='{$eptid}',
	`thumbnail_url`='{$info_product["thumbnail_url"]}',
	`point_price`='{$info_product["point_price"]}',
	`retail_price`='{$info_product["retail_price"]}',
	`process_fee`='{$info_product["process_fee"]}',
	`cost_price`='{$info_product["cost_price"]}',
	`ontime`='{$ontime}',
	`offtime`='{$offtime}',
	`esid`='{$info_product["esid"]}',
	`prid`='{$prid}',
	`mob_type`='{$info_product["mob_type"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='{$info_product["seq"]}',
	`insertt`=now()
";
$this->model->query($query);

//新商品ID
$productid = $this->model->_con->insert_id;


// 商品分類促銷
$query ="SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}product_promote_rt`
WHERE
`prefixid` = '{$this->config->default_prefix_id}'
AND `switch`='Y'
AND `epid` = '{$this->io->input["get"]["epid"]}'
";
$old_product_promote = $this->model->getQueryRecord($query);

//Relation product_promote_rt
if($old_product_promote['table']['record'])
{
	foreach($old_product_promote['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}product_promote_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`ppid`='{$rv['ppid']}',
		`epid`='{$productid}',
		`seq`='{$rv['seq']}',
		`switch` = 'Y',
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

// 兌換商品分類
$query ="SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt`
WHERE
`prefixid` = '{$this->config->default_prefix_id}'
AND `switch`='Y'
AND `epid` = '{$this->io->input["get"]["epid"]}'
";
$old_product_cat = $this->model->getQueryRecord($query);

//Relation exchange_product_category
if($old_product_cat['table']['record'])
{
	foreach($old_product_cat['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`epcid`='{$rv['epcid']}',
		`epid`='{$productid}',
		`seq`='{$rv['seq']}',
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

// 兌換商品與活動規則
$query ="SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item_product_rt`
WHERE
`prefixid` = '{$this->config->default_prefix_id}'
AND `switch`='Y'
AND `epid` = '{$this->io->input["get"]["epid"]}'
";
$old_product_promote = $this->model->getQueryRecord($query);

//Relation promote_rule_item
if($old_product_promote['table']['record'])
{
	foreach($old_product_promote['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item_product_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`priid`='{$rv['priid']}',
		`epid`='{$productid}',
		`seq`='{$rv['seq']}',
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

// Table End
##############################################################################################################################################



##############################################################################################################################################
// Log Start
$exchange_prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_product_copy',
	`active` = '商品複製寫入',
	`memo` = '{$exchange_prodcut_data}',
	`root` = 'exchange_product/copy',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


$to = $this->config->default_main .'/exchange_product/edit/epid='. $productid .'&location_url='. $this->io->input['get']['location_url'];
header("location:". $to); //header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));