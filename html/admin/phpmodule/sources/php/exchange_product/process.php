<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

//正式機才上傳圖片至S3
//0.正式機  1.測試機
$is_test = 1;

// Check Variable Start

if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('商品名稱錯誤!!');
}

if($this->io->input["get"]['mod']!='add')
{

	if (empty($this->io->input["post"]["description"])) {
		$this->jsAlertMsg('商品說明錯誤!!');
	}
	if (empty($this->io->input["post"]["thumbnail"])) {
		//$this->jsAlertMsg('商品主圖錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["point_price"]) || $this->io->input["post"]["point_price"] =='') {
		$this->jsAlertMsg('兌換點數錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["retail_price"]) || $this->io->input["post"]["retail_price"] =='') {
		$this->jsAlertMsg('商品市價錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["cost_price"]) || $this->io->input["post"]["cost_price"] =='') {
		$this->jsAlertMsg('進貨成本價錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["process_fee"]) || $this->io->input["post"]["process_fee"] =='') {
		$this->jsAlertMsg('處理費錯誤!!');
	}
	// Add By Thomas 2019/11/04
	if($this->io->input["post"]["point_price"]<=0) {
	    $this->jsAlertMsg('兌換點數不可為 0 or 負值!!');	
	}
	if($this->io->input["post"]["retail_price"]<=0) {
		$this->jsAlertMsg('商品市價不可為 0 or 負值!!');
	}
	if($this->io->input["post"]["cost_price"]<=0) {
		$this->jsAlertMsg('進貨成本不可為負值!!');
	}
	if($this->io->input["post"]["process_fee"]<0) {
		$this->jsAlertMsg('處理費不可為負值!!');
	}
	// Add End
	
	if (strtotime($this->io->input["post"]["ontime"]) === false) {
		$this->jsAlertMsg('前台顯示時間錯誤!!');
	}
	if (strtotime($this->io->input["post"]["offtime"]) === false) {
		$this->jsAlertMsg('下架時間錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["seq"])) {
		$this->jsAlertMsg('排序錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["esid"])) {
		$this->jsAlertMsg('供應商錯誤!!'); //店家ID
	}
	if (!$this->io->input["post"]["epcid"]) {
		$this->jsAlertMsg('商品分類錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["eptype"])) {
		$this->jsAlertMsg('商品類型錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["reserved_stock"])) {
		$this->jsAlertMsg('預留庫存錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["rebate"]) || $this->io->input["post"]["rebate"] =='') {
		$this->jsAlertMsg('兌換可折抵購物金比例錯誤!!');
	}
	if ($this->io->input["post"]["rebate"] > ($this->io->input["post"]["retail_price"]/2)) {
		$this->jsAlertMsg('兌換可折抵購物金比例大於巿價的50%!!');
	}
}

// Check Variable End

##############################################################################################################################################
// Update Start

//exchange_product 新增商品
if($this->io->input["get"]['mod']=='add')
{
	

##############################################################################################################################################
// Log Start
$exchange_product_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_product_insert',
	`active` = '兌換商品新增寫入',
	`memo` = '{$exchange_product_data}',
	`root` = 'exchange_product/insert',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input["post"]["name"]}',
		`esid`='1',
		`insertt`= now()
	";
	$this->model->query($query);
	$productid = $this->model->_con->insert_id;
	
	//新增時預設庫存數 20
	$query ="
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}stock` SET
		`epid` = '{$productid}',
		`num` = '20',
		`behav` = 'admin_add',
		`prefixid`='{$this->config->default_prefix_id}',
		`seq`='0', 
		`switch`='Y',
		`insertt`=now()
	" ;
	$this->model->query($query); 

	//回傳狀態
	$ret['id'] = $productid;
	$ret['status'] = 1;
	echo json_encode($ret);
	exit;
}

//exchange_product 異動商品

//Relation exchange_product_thumbnail
if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	$filename = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);

	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` SET
	`name`='{$this->io->input["post"]["name"]}',
	`original`='{$this->io->input['files']['thumbnail']['name']}',
	`filename`='{$filename}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$eptid = $this->model->_con->insert_id;

	if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/{$filename}")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}

	$update_thumbnail = "`eptid` = '{$eptid}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}

if (!empty($this->io->input['files']['thumbnail_file']) && exif_imagetype($this->io->input['files']['thumbnail_file']['tmp_name'])) {
	$filename_file = md5($this->io->input['files']['thumbnail_file']['name']).".".pathinfo($this->io->input['files']['thumbnail_file']['name'], PATHINFO_EXTENSION);

	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET
	`name`='{$this->io->input["post"]["name"]}',
	`original`='{$this->io->input['files']['thumbnail']['name']}',
	`filename`='{$filename_file}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$epftid = $this->model->_con->insert_id;

	if (!move_uploaded_file($this->io->input['files']['thumbnail_file']['tmp_name'], $this->config->path_products_images."/{$filename_file}")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}

	$update_thumbnail_file = "`epftid` = '{$epftid}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename_file,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}

if (!empty($this->io->input['files']['thumbnail_file2']) && exif_imagetype($this->io->input['files']['thumbnail_file2']['tmp_name'])) {
	$filename_file2 = md5($this->io->input['files']['thumbnail_file2']['name']).".".pathinfo($this->io->input['files']['thumbnail_file2']['name'], PATHINFO_EXTENSION);

	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET
	`name`='{$this->io->input["post"]["name"]}',
	`original`='{$this->io->input['files']['thumbnail']['name']}',
	`filename`='{$filename_file2}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$epftid2 = $this->model->_con->insert_id;

	if (!move_uploaded_file($this->io->input['files']['thumbnail_file2']['tmp_name'], $this->config->path_products_images."/{$filename_file2}")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}

	$update_thumbnail_file2 = "`epftid2` = '{$epftid2}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename_file2,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}

if (!empty($this->io->input['files']['thumbnail_file3']) && exif_imagetype($this->io->input['files']['thumbnail_file3']['tmp_name'])) {
	$filename_file3 = md5($this->io->input['files']['thumbnail_file3']['name']).".".pathinfo($this->io->input['files']['thumbnail_file3']['name'], PATHINFO_EXTENSION);

	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET
	`name`='{$this->io->input["post"]["name"]}',
	`original`='{$this->io->input['files']['thumbnail']['name']}',
	`filename`='{$filename_file3}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$epftid3 = $this->model->_con->insert_id;

	if (!move_uploaded_file($this->io->input['files']['thumbnail_file3']['tmp_name'], $this->config->path_products_images."/{$filename_file3}")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}

	$update_thumbnail_file3 = "`epftid3` = '{$epftid3}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename_file3,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}

if (!empty($this->io->input['files']['thumbnail_file4']) && exif_imagetype($this->io->input['files']['thumbnail_file4']['tmp_name'])) {
	$filename_file4 = md5($this->io->input['files']['thumbnail_file4']['name']).".".pathinfo($this->io->input['files']['thumbnail_file4']['name'], PATHINFO_EXTENSION);

	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET
	`name`='{$this->io->input["post"]["name"]}',
	`original`='{$this->io->input['files']['thumbnail']['name']}',
	`filename`='{$filename_file4}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$epftid4 = $this->model->_con->insert_id;

	if (!move_uploaded_file($this->io->input['files']['thumbnail_file4']['tmp_name'], $this->config->path_products_images."/{$filename_file4}")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}

	$update_thumbnail_file4 = "`epftid4` = '{$epftid4}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename_file4,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}


//exchange_product
$description = (empty($this->io->input["post"]["description"])) ? '' : htmlspecialchars($this->io->input["post"]["description"]);

$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET
	`name`='{$this->io->input["post"]["name"]}',
	`eptype`='{$this->io->input["post"]["eptype"]}',
	`description`='{$description}',
	`tx_url`='{$this->io->input["post"]["tx_url"]}',
	`thumbnail_url`= '{$this->io->input["post"]["thumbnail_url"]}',
	`point_price`='{$this->io->input["post"]["point_price"]}',
	`retail_price`='{$this->io->input["post"]["retail_price"]}',
	`process_fee`='{$this->io->input["post"]["process_fee"]}',
	`cost_price`='{$this->io->input["post"]["cost_price"]}',
	`ontime`='{$this->io->input["post"]["ontime"]}',
	`offtime`='{$this->io->input["post"]["offtime"]}',
	`esid`='{$this->io->input["post"]["esid"]}',
	`prid`='{$this->io->input["post"]["prid"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`mob_type`='{$this->io->input["post"]["mob_type"]}',
	{$update_thumbnail}
	{$update_thumbnail_file}
	{$update_thumbnail_file2}
	{$update_thumbnail_file3}
	{$update_thumbnail_file4}	
	`seq`='{$this->io->input["post"]["seq"]}',
	`use_type`='{$this->io->input["post"]["use_type"]}',
	`feedback_price`='{$this->io->input["post"]["feedback_price"]}',
	`limit_qty`='{$this->io->input["post"]["limit_qty"]}',
	`set_qty`='{$this->io->input["post"]["set_qty"]}',
	`reserved_stock`='{$this->io->input["post"]["reserved_stock"]}',
	`display`='{$this->io->input["post"]["display"]}',
	`rebate`='{$this->io->input["post"]["rebate"]}',
	`memo`='{$this->io->input["post"]["memo"]}'
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `epid` = '{$this->io->input["post"]["epid"]}'
" ;
$this->model->query($query);
$epid = $this->model->_con->insert_id;

//Relation product_promote_rt 商品分類促銷
$query = "DELETE FROM `{$db_exchange}`.`{$this->config->default_prefix}product_promote_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}'
AND `epid` = '{$this->io->input["post"]["epid"]}' ";
$this->model->query($query);

if($this->io->input["post"]["ppid"])
{
	$ppseq = $this->io->input["post"]["ppseq"];
	foreach($this->io->input["post"]["ppid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}product_promote_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`ppid`='{$rv}',
		`epid`='{$this->io->input["post"]["epid"]}',
		`seq`='{$ppseq[$rk]}',
		`switch` = 'Y',
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query);
	}
}

//Relation exchange_product_category_rt
$query = "UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt`
SET `switch` = 'N'
WHERE
`prefixid` = '{$this->config->default_prefix_id}'
AND `epid` = '{$this->io->input["post"]["epid"]}'
";
$this->model->query($query);

// print_r($this->io->input["post"]);
// exit;
if($this->io->input["post"]["epcid"])
{
	foreach($this->io->input["post"]["epcid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`epcid`='{$rv}',
		`epid`='{$this->io->input["post"]["epid"]}',
		`seq`='".(($rk+1)*10)."',
		`switch` = 'Y',
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query);
	}
}

//Relation promote_rule_item
$query = "UPDATE `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item_product_rt`
SET `switch` = 'N'
WHERE
`prefixid` = '{$this->config->default_prefix_id}'
AND `epid` = '{$this->io->input["post"]["epid"]}'
";
$this->model->query($query);

if($this->io->input["post"]["priid"])
{
	foreach($this->io->input["post"]["priid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item_product_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`priid`='{$rv}',
		`epid`='{$this->io->input["post"]["epid"]}',
		`seq`='".(($rk+1)*10)."',
		`switch` = 'Y',
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query);
	}
}

// Update End
##############################################################################################################################################

##############################################################################################################################################
// Log Start
$exchange_product_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_product_update',
	`active` = '兌換商品修改寫入',
	`memo` = '{$exchange_product_data}',
	`root` = 'exchange_product/update',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


//header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
header("location:". $this->config->default_main .'/exchange_product/' );