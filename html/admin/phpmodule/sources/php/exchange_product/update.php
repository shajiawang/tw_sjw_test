<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["todayid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('商品名稱錯誤!!');
}
if (empty($this->io->input["post"]["link"]) ) {
	$this->jsAlertMsg('連結網址錯誤!!');
}
if (strtotime($this->io->input["post"]["ontime"]) === false) {
	$this->jsAlertMsg('上架時間錯誤!!');
}
if (strtotime($this->io->input["post"]["offtime"]) === false) {
	$this->jsAlertMsg('結標時間錯誤!!');
}

if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$filename = md5(date("YmdHis")."_".$this->io->input['post']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_products_images."/$filename") && ($filename != $this->io->input['post']['oldthumbnail'])) {
		$this->jsAlertMsg('商品Banner名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/$filename")) {
		$this->jsAlertMsg('商品Banner上傳錯誤!!');
	}
	
	if (!empty($this->io->input['post']['oldptid']) && $this->io->input['post']['oldptid'] > 0)
	{
		//刪除舊圖
		if (!empty($this->io->input['post']['oldthumbnail']) && ($filename != $this->io->input['post']['oldthumbnail'])) {
			unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail']);
		}
		
		$query = "delete from `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` 
			where ptid = '{$this->io->input['post']['oldptid']}'";
		$this->model->query($query);
	}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input['post']['name']}',
		`original`='{$this->io->input['files']['thumbnail']['name']}',
		`filename`='{$filename}',
		`insertt`=now()
	";
	$this->model->query($query);
	$ptid = $this->model->_con->insert_id;

	$update_thumbnail = "
	`ptid` = '{$ptid}',";
}else{
	$this->jsAlertMsg('商品主圖錯誤!!');
}


$description = htmlspecialchars($this->io->input["post"]["description"]);

$query ="
UPDATE `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_today` 
SET
	`name`         	= '{$this->io->input["post"]["name"]}',
	`link`   		= '{$this->io->input["post"]["link"]}',
	`description`	= '{$description}',
	{$update_thumbnail}
	`thumbnail_url`	= '{$this->io->input["post"]["thumbnail_url"]}',
	`ontime`       	= '{$this->io->input["post"]["ontime"]}',
	`offtime`      	= '{$this->io->input["post"]["offtime"]}',
	`seq`          	= '{$this->io->input["post"]["seq"]}'
WHERE
	prefixid      = '{$this->config->default_prefix_id}'
	AND todayid = '{$this->io->input["post"]["todayid"]}'
	AND switch = 'Y'
" ;
$this->model->query($query);


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));