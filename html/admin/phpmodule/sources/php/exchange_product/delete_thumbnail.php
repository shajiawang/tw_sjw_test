<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

// Check Variable Start
if(empty($this->io->input["get"]["epid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
if(empty($this->io->input["get"]["eptid"])) {
	$this->jsAlertMsg('商品主圖ID錯誤!!');
}
// Check Variable End

##############################################################################################################################################
// Update Start

// exchange_product_thumbnail
$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` SET 
	switch = 'N' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `eptid` = '{$this->io->input["get"]["eptid"]}'
" ;
$this->model->query($query);  

// exchange_product
$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET 
	`eptid` = '0' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `epid` = '{$this->io->input["get"]["epid"]}'
" ;
$this->model->query($query);  

// Update End
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url']);
// success message End
