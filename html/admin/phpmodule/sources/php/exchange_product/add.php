<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

//Relation exchange_product_category
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];


//Relation exchange_store
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_store`  
WHERE  
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_store"] = $recArr['table']['record'];

//Relation promote_rule
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}promote_rule`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["promote_rule"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();