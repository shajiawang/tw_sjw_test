<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["year"])) {
	$this->jsAlertMsg('年份錯誤!!');
}
if (empty($this->io->input["post"]["start_month"])) {
	$this->jsAlertMsg('起始月份錯誤!!');
}
if (empty($this->io->input["post"]["end_month"])) {
	$this->jsAlertMsg('迄止月份錯誤!!');
}
if (empty($this->io->input["post"]["rail_prefix"])) {
	$this->jsAlertMsg('字軌名稱錯誤!!');
}
if (empty($this->io->input["post"]["seq_start"])) {
	$this->jsAlertMsg('起始編號錯誤!!');
}
if (empty($this->io->input["post"]["seq_end"])) {
	$this->jsAlertMsg('迄止編號錯誤!!');
}
if (empty($this->io->input["post"]["switch"])) {
	$this->jsAlertMsg('狀態錯誤!!');
}
if (!is_numeric($this->io->input["post"]["total"]) || (int)$this->io->input["post"]["total"] < 1) {
	$this->jsAlertMsg('可用數量錯誤!!');
}
if (((int)$this->io->input["post"]["end_month"] - (int)$this->io->input["post"]["start_month"]) != 1) {
	$this->jsAlertMsg('起迄月份間距錯誤!!');
}
if (((int)$this->io->input["post"]["seq_end"] - (int)$this->io->input["post"]["seq_start"]) < 0) {
	$this->jsAlertMsg('起迄編號間距錯誤!!');
}
if (((int)$this->io->input["post"]["seq_end"] - (int)$this->io->input["post"]["seq_start"]) > (int)$this->io->input["post"]["total"] ) {
	$this->jsAlertMsg('起迄編號間距大於可用數量!!');
}
$rp = strtoupper($this->io->input["post"]["rail_prefix"]);

$query ="
INSERT INTO `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}invoice_rail` 
SET
	`prefixid`		= '{$this->config->default_prefix_id}',
	`year`			= '{$this->io->input["post"]["year"]}',
	`start_month`	= '{$this->io->input["post"]["start_month"]}',
	`end_month`		= '{$this->io->input["post"]["end_month"]}',
	`rail_prefix`	= '{$rp}',
	`seq_start`		= '{$this->io->input["post"]["seq_start"]}',
	`seq_end`		= '{$this->io->input["post"]["seq_end"]}',
	`total`			= '{$this->io->input["post"]["total"]}',
	`used`			= '0',
	`switch`		= '{$this->io->input["post"]["switch"]}',
	`insertt`		= now()
" ;

$this->model->query($query);
$railid = $this->model->_con->insert_id;


##############################################################################################################################################
// Log Start 
$invoice_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'invoiceno_insert', 
	`active` = '字軌新增寫入', 
	`memo` = '{$invoice_data}', 
	`root` = 'invoiceno/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));