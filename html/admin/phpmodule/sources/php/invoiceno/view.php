<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(switch|railid|name|seq_start||year|start_month|end_month|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY ir.railid DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = " ir.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if(!empty($this->io->input["get"]["search_name"])){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND ir.`rail_prefix` = '".strtoupper($this->io->input["get"]["search_name"])."'";
}
if(!empty($this->io->input["get"]["search_year"])){
	$status["status"]["search"]["search_year"] = $this->io->input["get"]["search_year"] ;
	$status["status"]["search_path"] .= "&search_year=".$this->io->input["get"]["search_year"] ;
	$sub_search_query .=  "
		AND ir.`year` = '".$this->io->input["get"]["search_year"]."'";
}
if(!empty($this->io->input["get"]["search_start_month"])){
	$status["status"]["search"]["search_start_month"] = $this->io->input["get"]["search_start_month"] ;
	$status["status"]["search_path"] .= "&search_start_month=".$this->io->input["get"]["search_start_month"] ;
	$sub_search_query .=  "
		AND ir.`start_month` = '".$this->io->input["get"]["search_start_month"]."'";
}
if(!empty($this->io->input["get"]["search_end_month"])){
	$status["status"]["search"]["search_end_month"] = $this->io->input["get"]["search_end_month"] ;
	$status["status"]["search_path"] .= "&search_end_month=".$this->io->input["get"]["search_end_month"] ;
	$sub_search_query .=  "
		AND ir.`end_month` = '".$this->io->input["get"]["search_end_month"]."'";
}
if(!empty($this->io->input["get"]["search_switch"])){
	$status["status"]["search"]["search_switch"] = $this->io->input["get"]["search_switch"] ;
	$status["status"]["search_path"] .= "&search_switch=".$this->io->input["get"]["search_switch"] ;
	$sub_search_query .=  "
		AND ir.`switch` = '".$this->io->input["get"]["search_switch"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice_rail` ir 
{$category_search_query}
WHERE 
ir.prefixid = '".$this->config->default_prefix_id."' 
AND ir.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT ir.*
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice_rail` ir 
{$category_search_query}
WHERE 
ir.prefixid = '".$this->config->default_prefix_id."' 
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$invoice_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'invoiceno', 
	`active` = '字軌清單查詢', 
	`memo` = '{$invoice_data}', 
	`root` = 'invoiceno/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();