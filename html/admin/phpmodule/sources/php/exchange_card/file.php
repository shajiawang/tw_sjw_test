<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start
$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';

if($files['file_name']["error"] != 0 || empty($files['file_name']["size"]) ) {
	$this->jsAlertMsg('上傳錯誤!!');
}
else
{
	$exchange_card_File = $files['file_name']['name']; 
	
    if(is_uploaded_file($files['file_name']['tmp_name']) ) {
		move_uploaded_file($files['file_name']['tmp_name'], "./{$exchange_card_File}");
	}

	$handle = fopen($exchange_card_File, 'r+');
	if (is_resource($handle))
    {
        $insertt = date('Y-m-d H:i:s');
		$newData = array();
		
		$filesize = filesize($exchange_card_File);
		$data = fread($handle, $filesize);
		$data = mb_convert_encoding($data, "UTF-8", "big5");
		$newData = explode("\n", $data);
		//array_shift($newData);
		$newData = array_values(array_filter($newData));
		
		if (!empty($newData)) {
			foreach ($newData as $key => $value) {
				$dataArr[$key] = explode(",", $value);
			}
			//print_r($dataArr);exit;

			if (!empty($dataArr)) {
				//判斷是否空白
				foreach ($dataArr as $key => $value) {
					if ($key > 0) {
						foreach ($value as $key1 => $value1) {
							if ($key1 == 1 && $value1 === "") {
								unlink($exchange_card_File);
								$this->jsAlertMsg($dataArr[0][$key1].' 請勿空白!!');
							}
						}
					}
				}
				//將第一列資料清除
				array_shift($dataArr);
				$dataArr = array_values(array_filter($dataArr));
				foreach ($dataArr as $key => $value) {
					
					$codeformat2 = $value[6] ? $value[6] : '0';
					$codeformat3 = $value[7] ? $value[7] : '0';
					$codeformat4 = $value[8] ? $value[8] : '0';
					$purchaseid = $value[9] ? $value[9] : '';
			        //新增商品卡資料
			        $query = "insert into `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_card` 
			        set 
						epid = '{$value[0]}', 
						code1 = '{$value[1]}',
						codeformat1 = '{$value[5]}',
						code2 = '{$value[2]}',
						codeformat2 = '{$codeformat2}',
						code3 = '{$value[3]}', 
						codeformat3 = '{$codeformat3}',
						code4 = '{$value[4]}',
						codeformat4 = '{$codeformat4}',
						purchaseid = {$purchaseid},
			        	used = 'N',
			        	switch = 'Y',
			        	insertt = '{$insertt}'
			        ";
					$this->model->query($query);

					unset($codeformat2);
					unset($codeformat3);
					unset($codeformat4);
				}
			}
		}
		//exit;
        fclose($handle);
    }
    unlink($exchange_card_File);
	


##############################################################################################################################################
// Log Start 
$exchange_card_data = json_encode($this->io->input["files"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_card_file', 
	`active` = '兌換商品卡上傳', 
	`memo` = '{$exchange_card_data}', 
	`root` = 'exchange_card/file', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################
	
	
	
	$query = "SELECT count(*) count
	FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_card`
	WHERE 
		insertt = '{$insertt}'
		AND switch = 'Y'
	";
	$rs = $this->model->getQueryRecord($query);
	$this->jsPrintMsg('已成功上傳 '. $rs['table']['record'][0]['count'] .' 筆!', $this->io->input['get']['location_url']);
}

// Insert Start
##############################################################################################################################################
//header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));