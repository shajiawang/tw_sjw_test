<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(cid|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY ec." . implode(',', $orders);
}else{
	$sub_sort_query =  " ORDER BY ec.modifyt DESC";
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$join_search_query = "";

if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	
	$sub_search_query .=  "
		AND ec.`userid` = '".$this->io->input["get"]["search_userid"]."'";
}
if($this->io->input["get"]["search_category"] != ''){
	$status["status"]["search"]["search_category"] = $this->io->input["get"]["search_category"] ;
	$status["status"]["search_path"] .= "&search_category=".$this->io->input["get"]["search_category"] ;
	
	$sub_search_query .=  "
		AND ec.`category` = '".$this->io->input["get"]["search_category"]."'";
}
if($this->io->input["get"]["search_epid"] != ''){
	$status["status"]["search"]["search_epid"] = $this->io->input["get"]["search_epid"] ;
	$status["status"]["search_path"] .= "&search_epid=".$this->io->input["get"]["search_epid"] ;
	
	$sub_search_query .=  "
		AND ec.`epid` = '".$this->io->input["get"]["search_epid"]."'";
}
if($this->io->input["get"]["search_code1"] != ''){
	$status["status"]["search"]["search_code1"] = $this->io->input["get"]["search_code1"] ;
	$status["status"]["search_path"] .= "&search_code1=".$this->io->input["get"]["search_code1"] ;
	
	$sub_search_query .=  "
		AND ec.`code1` = '".$this->io->input["get"]["search_code1"]."'";
}
if($this->io->input["get"]["search_code2"] != ''){
	$status["status"]["search"]["search_code2"] = $this->io->input["get"]["search_code2"] ;
	$status["status"]["search_path"] .= "&search_code2=".$this->io->input["get"]["search_code2"] ;
	
	$sub_search_query .=  "
		AND ec.`code2` = '".$this->io->input["get"]["search_code2"]."'";
}
if($this->io->input["get"]["search_used"] != ''){
	$status["status"]["search"]["search_used"] = $this->io->input["get"]["search_used"] ;
	$status["status"]["search_path"] .= "&search_used=".$this->io->input["get"]["search_used"] ;
	
	$sub_search_query .=  "
		AND ec.`used` = '".$this->io->input["get"]["search_used"]."'";
}
if($this->io->input["get"]["search_starttime"] != ''){
	$status["status"]["search"]["search_starttime"] = $this->io->input["get"]["search_starttime"] ;
	$status["status"]["search_path"] .= "&search_starttime=".$this->io->input["get"]["search_starttime"] ;
	
	$sub_search_query .=  "
		AND o.`insertt` >= '".$this->io->input["get"]["search_starttime"]."'";
}
if($this->io->input["get"]["search_endtime"] != ''){
	$status["status"]["search"]["search_endtime"] = $this->io->input["get"]["search_endtime"] ;
	$status["status"]["search_path"] .= "&search_endtime=".$this->io->input["get"]["search_endtime"] ;
	
	$sub_search_query .=  "
		AND o.`insertt` <= '".$this->io->input["get"]["search_endtime"]."'";
}
if($this->io->input["get"]["search_orderid"] != ''){
	$status["status"]["search"]["search_orderid"] = $this->io->input["get"]["search_orderid"] ;
	$status["status"]["search_path"] .= "&search_orderid=".$this->io->input["get"]["search_orderid"] ;
	
	$sub_search_query .=  "
		AND ec.`orderid` = '".$this->io->input["get"]["search_orderid"]."'";
}

if($this->io->input["get"]["search_startinsertt"] != ''){
	$status["status"]["search"]["search_startinsertt"] = $this->io->input["get"]["search_startinsertt"] ;
	$status["status"]["search_path"] .= "&search_startinsertt=".$this->io->input["get"]["search_startinsertt"] ;
	
	$sub_search_query .=  "
		AND ec.`insertt` >= '".$this->io->input["get"]["search_startinsertt"]."'";
}
if($this->io->input["get"]["search_endinsertt"] != ''){
	$status["status"]["search"]["search_endinsertt"] = $this->io->input["get"]["search_endinsertt"] ;
	$status["status"]["search_path"] .= "&search_endinsertt=".$this->io->input["get"]["search_endinsertt"] ;
	
	$sub_search_query .=  "
		AND ec.`insertt` <= '".$this->io->input["get"]["search_endinsertt"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `".$this->config->db[3]['dbname']."`.`".$this->config->default_prefix."exchange_card` ec 
LEFT JOIN `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o
ON ec.orderid = o.orderid
{$join_search_query}
WHERE ec.switch='Y'";

$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="SELECT ec.cid, ec.category, ec.epid, ec.code1, ec.codeformat1, ec.code2, ec.codeformat2, ec.code3, ec.codeformat3, ec.code4, ec.codeformat4, ec.user_card_id, ec.userid, ec.orderid, ec.used, ec.insertt, ec.modifyt, o.insertt AS order_time
FROM `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_card` ec
LEFT JOIN `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o
ON ec.orderid = o.orderid
{$join_search_query}
WHERE ec.switch='Y'";

$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_card_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_card', 
	`active` = '兌換商品卡清單查詢', 
	`memo` = '{$exchange_card_data}', 
	`root` = 'exchange_card/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();