<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["epid"])) {
	$this->jsAlertMsg('商品編號不可空白!!');
}
if (empty($this->io->input["post"]["code1"])) {
	$this->jsAlertMsg('序號1不可空白!!');
}
if (empty($this->io->input["post"]["codeformat1"])) {
	$this->jsAlertMsg('序號格式1不可空白!!');
}
/*
if (empty($this->io->input["post"]["code2"])) {
	$this->jsAlertMsg('序號2不可空白!!');
}
if (empty($this->io->input["post"]["codeformat2"])) {
	$this->jsAlertMsg('序號格式2不可空白!!');
}
*/

$query ="
INSERT INTO `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_card`(
	`epid`,
	`code1`,
	`codeformat1`,
	`code2`,
	`codeformat2`,
	`code3`,
	`codeformat3`,
	`code4`,
	`codeformat4`,
	`used`,
	`purchaseid`,
	`insertt`
) 
VALUES(
	'".$this->io->input['post']['epid']."',
	'".$this->io->input['post']['code1']."',
	'".$this->io->input['post']['codeformat1']."',
	'".$this->io->input["post"]["code2"]."',
	'".$this->io->input["post"]["codeformat2"]."',
	'".$this->io->input['post']['code3']."',
	'".$this->io->input["post"]["codeformat3"]."',
	'".$this->io->input["post"]["code4"]."',
	'".$this->io->input["post"]["codeformat4"]."',
	'".$this->io->input["post"]["purchaseid"]."',
	'N',
	now()
)
" ;

$this->model->query($query);

##############################################################################################################################################
// Log Start 
$exchange_card_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_card_insert', 
	`active` = '兌換商品卡新增寫入', 
	`memo` = '{$exchange_card_data}', 
	`root` = 'exchange_card/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################

echo "success!!";

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));