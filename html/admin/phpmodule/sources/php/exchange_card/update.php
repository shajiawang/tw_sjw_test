<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["cid"])) {
	$this->jsAlertMsg('卡片ID錯誤!!');
}
if (empty($this->io->input["post"]["epid"])) {
	$this->jsAlertMsg('商品編號不可空白!!');
}
if (empty($this->io->input["post"]["code1"])) {
	$this->jsAlertMsg('序號1不可空白!!');
}
if (empty($this->io->input["post"]["codeformat1"])) {
	$this->jsAlertMsg('序號格式1不可空白!!');
}
/*
if (empty($this->io->input["post"]["code2"])) {
	$this->jsAlertMsg('序號2不可空白!!');
}
if (empty($this->io->input["post"]["codeformat2"])) {
	$this->jsAlertMsg('序號格式2不可空白!!');
}
*/

$query ="
UPDATE `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_card` 
set
	`epid` = '".$this->io->input['post']['epid']."',
	`code1` = '".$this->io->input['post']['code1']."',
	`codeformat1` = '".$this->io->input['post']['codeformat1']."',
	`code2` = '".$this->io->input["post"]["code2"]."',
	`codeformat2` = '".$this->io->input['post']['codeformat2']."',
	`code3` = '".$this->io->input['post']['code3']."',
	`codeformat3` = '".$this->io->input['post']['codeformat3']."',
	`code4` = '".$this->io->input["post"]["code4"]."',
	`codeformat4` = '".$this->io->input['post']['codeformat4']."',
	`userid` = '".$this->io->input['post']['userid']."',
	`orderid` = '".$this->io->input['post']['orderid']."',
	`purchaseid` = '".$this->io->input['post']['purchaseid']."',
	`used` = '".$this->io->input['post']['used']."',
	`modifyt` = now()
where 
	`cid` = '".$this->io->input["post"]["cid"]."'
	and `switch` = 'Y'
" ;
//echo $query;exit;
$this->model->query($query);

if ((!empty($this->io->input['post']['userid'])) && (!empty($this->io->input['post']['orderid']))){

	$query ="SELECT o.*
		FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o
		WHERE
			o.`prefixid` = '{$this->config->default_prefix_id}'
			AND o.`switch` = 'Y'
			AND o.`orderid` = '{$this->io->input["post"]["orderid"]}'
	" ;
	$table = $this->model->getQueryRecord($query);
	
	if (!empty($table["table"]['record'][0]) && ($table["table"]['record'][0]['status'] < 2)){
		
		if ($table["table"]['record'][0]['status'] == 0){
			$query ="INSERT INTO `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` SET
				`prefixid` = '{$this->config->default_prefix_id}',
				`orderid`='{$this->io->input["post"]["orderid"]}',
				`outtime`=now(),
				`outtype`='廠商自送',
				`outcode`='',
				`outmemo`='',
				`closetime`=now(),
				`closememo`='',				
				`insertt`=now()
			";
			$this->model->query($query);
		
		} elseif ($table["table"]['record'][0]['status'] == 1){
			$query ="UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` SET
				`closetime`=now(),
				`closememo`='',
				`modifyt`=now()
			WHERE
				`prefixid` = '{$this->config->default_prefix_id}'
				AND `orderid`='{$this->io->input["post"]["orderid"]}'
			" ;
			$this->model->query($query);			
		}
		
		$query ="UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` SET
			`status` ='4',
			`modifyt`=now()
		WHERE
			`orderid`='{$this->io->input["post"]["orderid"]}'
		" ;
		$this->model->query($query);
	}
}

##############################################################################################################################################
// Log Start 
$exchange_card_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_card_update', 
	`active` = '兌換商品卡修改寫入', 
	`memo` = '{$exchange_card_data}', 
	`root` = 'exchange_card/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));