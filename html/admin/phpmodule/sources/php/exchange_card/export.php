<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);
if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}
$sub_sort_query =  " ORDER BY ec.modifyt DESC";

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$join_search_query = "";

if($data["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $data["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$data["search_userid"] ;
	
	$sub_search_query .=  "
		AND ec.`userid` = '".$data["search_userid"]."'";
}
if($data["search_category"] != ''){
	$status["status"]["search"]["search_category"] = $data["search_category"] ;
	$status["status"]["search_path"] .= "&search_category=".$data["search_category"] ;
	
	$sub_search_query .=  "
		AND ec.`category` = '".$data["search_category"]."'";
}
if($data["search_epid"] != ''){
	$status["status"]["search"]["search_epid"] = $data["search_epid"] ;
	$status["status"]["search_path"] .= "&search_epid=".$data["search_epid"] ;
	
	$sub_search_query .=  "
		AND ec.`epid` = '".$data["search_epid"]."'";
}
if($data["search_code1"] != ''){
	$status["status"]["search"]["search_code1"] = $data["search_code1"] ;
	$status["status"]["search_path"] .= "&search_code1=".$data["search_code1"] ;
	
	$sub_search_query .=  "
		AND ec.`code1` = '".$data["search_code1"]."'";
}
if($data["search_code2"] != ''){
	$status["status"]["search"]["search_code2"] = $data["search_code2"] ;
	$status["status"]["search_path"] .= "&search_code2=".$data["search_code2"] ;
	
	$sub_search_query .=  "
		AND ec.`code2` = '".$data["search_code2"]."'";
}
if($data["search_used"] != ''){
	$status["status"]["search"]["search_used"] = $data["search_used"] ;
	$status["status"]["search_path"] .= "&search_used=".$data["search_used"] ;
	
	$sub_search_query .=  "
		AND ec.`used` = '".$data["search_used"]."'";
}
if($data["search_starttime"] != ''){
	$status["status"]["search"]["search_starttime"] = $data["search_starttime"] ;
	$status["status"]["search_path"] .= "&search_starttime=".$data["search_starttime"] ;
	
	$sub_search_query .=  "
		AND o.`insertt` >= '".$data["search_starttime"]."'";
}
if($data["search_endtime"] != ''){
	$status["status"]["search"]["search_endtime"] = $data["search_endtime"] ;
	$status["status"]["search_path"] .= "&search_endtime=".$data["search_endtime"] ;
	
	$sub_search_query .=  "
		AND o.`insertt` <= '".$data["search_endtime"]."'";
}

if($data["search_startinsertt"] != ''){
	$status["status"]["search"]["search_startinsertt"] = $data["search_startinsertt"] ;
	$status["status"]["search_path"] .= "&search_startinsertt=".$data["search_startinsertt"] ;
	
	$sub_search_query .=  "
		AND ec.`insertt` >= '".$data["search_startinsertt"]."'";
}
if($data["search_endinsertt"] != ''){
	$status["status"]["search"]["search_endinsertt"] = $data["search_endinsertt"] ;
	$status["status"]["search_path"] .= "&search_endinsertt=".$data["search_endinsertt"] ;
	
	$sub_search_query .=  "
		AND ec.`insertt` <= '".$data["search_endinsertt"]."'";
}

if($data["sort_cid"] != ''){
	$sub_sort_query =  " ORDER BY ec.cid ".$data["sort_cid"];
}

if($data["sort_insertt"] != ''){
	$sub_sort_query =  " ORDER BY o.orderid ".$data["sort_insertt"];
}
// Search End

// Table Record Start
$query ="SELECT ep.name, ec.epid, ec.code1, ec.code2, ec.code3, ec.code4, ec.userid, ec.orderid, ec.used, ec.insertt as ecinsertt, o.insertt AS order_time 
FROM `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_card` ec
LEFT JOIN `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o
ON ec.orderid = o.orderid
LEFT JOIN `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` ep 
ON ec.epid = ep.epid
{$join_search_query}
WHERE ec.switch='Y'";

$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 

// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 


##############################################################################################################################################
// Log Start 
$exchange_card_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_card_export', 
	`active` = '兌換商品卡匯出', 
	`memo` = '{$exchange_card_data}', 
	`root` = 'exchange_card/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Add some data
//print_r($_POST['report']);exit;
//會員資料表
$Report_Array[0]['title'] = array("A1" => "商品名稱", "B1" => "商品編號", "C1" => "序號1", "D1" => "序號2", "E1" => "序號3", "F1" => "序號4",
							"G1" => "會員編號", "H1" => "訂單編號", "I1" => "訂單日期","J1" => "使用狀態", "K1" => "建置日期");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		if(empty($value['order_time'])){
			$order_time = '0000-00-00 00:00:00';
		}else{
			$order_time = $value['order_time'];
		}
		
		if($value['used'] == 'Y'){
			$used = '已使用';
		}else{
			$used = '未使用';
		}
		
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$row, (string)$value['name'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['epid']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['code1'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row, (string)$value['code2'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['code3'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$row, (string)$value['code4'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['userid']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['orderid']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$row, (string)$order_time,PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$row, (string)$used,PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$row, (string)$value['ecinsertt'],PHPExcel_Cell_DataType::TYPE_STRING);
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('商品卡管理');

/*
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setCellValue("A3","test11"); 
$objPHPExcel->getActiveSheet()->setCellValue("B3","test22"); 
$objPHPExcel->getActiveSheet()->setCellValue("C3","test33"); 
$objPHPExcel->getActiveSheet()->setCellValue("D3","測試");

// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('Simple2');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);
*/
header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="商品卡管理'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
	
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>