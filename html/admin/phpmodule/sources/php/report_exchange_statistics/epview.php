<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
if (empty($this->io->input['get']['epid'])) {
	$this->jsAlertMsg('兌換商品ID錯誤!!');
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 							
									


// Table Record Start
$query = "
select ep.epid, ep.name, group_concat(epc.name) epcname, ep.description, ep.ontime, ep.offtime, ep.process_fee, 
ep.point_price, ep.cost_price, ep.insertt 
from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` ep 
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category_rt` epcr on 
	ep.prefixid = epcr.prefixid 
	and ep.epid = epcr.epid
	and epcr.switch = 'Y'
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category` epc on 
	epcr.prefixid = epc.prefixid 
	and epcr.epcid = epc.epcid
	and epc.switch = 'Y'
where 
	ep.prefixid = '".$this->config->default_prefix_id."' 
	and ep.epid = '".$this->io->input['get']['epid']."'
	and ep.switch = 'Y'
";
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_exchange_statistics', 
	`active` = '兌換商品統計商品資料查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_exchange_statistics/productview', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();