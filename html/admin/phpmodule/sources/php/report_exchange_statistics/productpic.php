<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
if($_SESSION['user']['department'] != 'S') { 
	$this->jsAlertMsg('您無權限登入！');
	die();
}

$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
// Sort End

// Search Start
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

##############################################################################################################################################
// Table  Start 

// Table Record Start
for($i = 0 ; $i < 5; $i++) {
	$query = "
	select count(p.epid) as total 
	from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
	left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` p on 
	o.prefixid = p.prefixid
	and o.epid = p.epid
	and o.switch = 'Y'
	where 
		p.prefixid = '".$this->config->default_prefix_id."' 
		and p.switch = 'Y' 
		and YEAR(o.modifyt) = YEAR(NOW()) 
		and p.eptype = '".$i."'
	";
	$order = $this->model->getQueryRecord($query);
	$table["table"]['record'][$i]['eptype'] = $i;
	$table["table"]['record'][$i]['eptype_total'] = $order["table"]['record'][0]['total']; 
}

// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

foreach($table["table"]['record'] as $rk => $rv)
{
	$rv['total'] = ''; 
	for($i = 1 ; $i < 13; $i++) {
		$query = "
			select count(p.epid) as sun_total 
			from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
			left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` p on 
				o.prefixid = p.prefixid
				and o.epid = p.epid
				and o.switch = 'Y'
			where 
				p.prefixid = '".$this->config->default_prefix_id."' 
				and p.switch = 'Y' 
				and YEAR(p.modifyt) = YEAR(NOW()) 
				and MONTH(p.modifyt) = '".($i)."'
				and p.eptype = '".$rv['eptype']."'
		";
		$total = $this->model->getQueryRecord($query);	
		$rv['total'] .= empty($total["table"]['record'][0]['sun_total']) ? '0,' : round($total["table"]['record'][0]['sun_total']).',';
	}
	$table["table"]['record'][$rk]['total'] = $rv['total']; 

}	

$query = "
	select count(o.orderid) as total_amount 
	from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
	left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` p on 
	o.prefixid = p.prefixid
	and o.epid = p.epid
	and o.switch = 'Y'
	where 
		p.prefixid = '".$this->config->default_prefix_id."' 
		and p.switch = 'Y' 
		and YEAR(o.modifyt) = YEAR(NOW())		
";

$alltotal = $this->model->getQueryRecord($query);
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_exchange_statistics', 
	`active` = '兌換商品統計報表查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_exchange_statistics/productpic', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status['status']['gender'] = array('Y' => '結標', 'NB' => '流標', 'N' => '競標中');
$status['status']['alltotal'] = round($alltotal["table"]['record'][0]['total_amount']);

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();