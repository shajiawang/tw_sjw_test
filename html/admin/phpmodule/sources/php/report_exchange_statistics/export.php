<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

//搜尋條件
$sub_search_query = "";
$sub_having_query = "";
$sub_group_query = " group by ep.epid";
$sub_sort_query =  " ORDER BY ep.epid";
if(isset($data["epid"]) && is_numeric($data["epid"])){							//兌換商品代碼
	$sub_search_query .=  "
		AND ep.`epid` = '".$data["epid"]."'";
}
if(!empty($data["epname"])){													//兌換商品名稱
	$sub_search_query .=  "
		AND ep.`name` like '%".$data["epname"]."%'";
}
if(!empty($data["ontimefrom"])){												//兌換商品上架開始日期
	$sub_search_query .=  "
		AND `ontime` >= '".$data["ontimefrom"]."'";
}
if(!empty($data["ontimeto"])){													//兌換商品上架結束日期
	$sub_search_query .=  "
		AND `ontime` < '".$data["ontimeto"]."'";
}
if(!empty($data["offtimefrom"])){												//兌換商品下架起始日期
	$sub_search_query .=  "
		AND `offtime` >= '".$data["offtimefrom"]."'";
}
if(!empty($data["offtimeto"])){													//兌換商品下架結束日期
	$sub_search_query .=  "
		AND `offtime` < '".$data["offtimeto"]."'";
}
if(isset($data["total_price"]) && is_numeric($data["total_price"])){			//兌換點數
	$sub_search_query .=  "
		AND (ifnull(ep.process_fee, 0) + ifnull(ep.point_price, 0)) = '".$data["total_price"]."'";
}
if(isset($data["cost_price"]) && is_numeric($data["cost_price"])){				//商品成本
	$sub_search_query .=  "
		AND ep.`cost_price` = '".$data["cost_price"]."'";
}
if(!empty($data["epcid"])){														//商品分類
	$sub_search_query .=  "
		AND epc.`epcid` = '".$data["epcid"]."'";
}
if (isset($data["exchange_count"]) && is_numeric($data["exchange_count"])) {	//下標次數
	$sub_having_query .=  "
		having `exchange_count` = '".$data["exchange_count"]."'";
}



$query = "
select ep.epid, ep.name epname, str_to_date(ifnull(ep.ontime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') ontime, str_to_date(ifnull(ep.offtime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') offtime, 
(ifnull(ep.process_fee, 0) + ifnull(ep.point_price, 0)) total_price, ifnull(ep.cost_price, 0) cost_price, group_concat(epc.name) epcname, 
(select ifnull(sum(num),0) from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
where 
	o.prefixid = ep.prefixid 
	and o.epid = ep.epid 
	and o.status = 1
	and o.type = 'exchange'
	and o.epid is not null
	and o.switch = 'Y'
) exchange_count 
from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` ep 
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category_rt` epcr on 
	ep.prefixid = epcr.prefixid
	and ep.epid = epcr.epid
	and epcr.switch = 'Y'
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category` epc on 
	epcr.prefixid = epc.prefixid
	and epc.epcid = epcr.epcid
	and epc.switch = 'Y'
where 
	ep.prefixid = '".$this->config->default_prefix_id."'
	and ep.switch = 'Y'
";
$query .= $sub_search_query;
$query .= $sub_group_query;
$query .= $sub_having_query;
$query .= $sub_sort_query;
$table = $this->model->getQueryRecord($query);


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_exchange_statistics', 
	`active` = '兌換商品統計清單匯出', 
	`memo` = '{$report_data}', 
	`root` = 'report_exchange_statistics/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//下標商品資料表
$Report_Array[0]['title'] = array("A1" => "兌換商品代碼", "B1" => "兌換商品名稱", "C1" => "兌換商品上架日期", "D1" => "兌換商品下架日期", "E1" => "兌換點數", "F1" => "商品成本", "G1" => "兌換商品分類", "H1" => "兌換次數");
//流/結標
$closedArr = array('Y' => '結標', 'NB' => '流標', 'N' => '未結標');

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['epid']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['epname']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['ontime']);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['offtime']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['total_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['cost_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['epcname']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['exchange_count']);
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('兌換商品統計');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="兌換商品統計'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
		
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>