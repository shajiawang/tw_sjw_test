<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Group Start
$sub_group_query = " group by ep.epid";
// Group End


// Sort Start
$sub_sort_query =  " ORDER BY ep.epid DESC";
// Sort End


// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
$sub_having_query = "";
if(!empty($this->io->input["get"]["epid"])){																		//兌換商品代碼
	$status["status"]["search"]["epid"] = $this->io->input["get"]["epid"] ;
	$status["status"]["search_path"] .= "&epid=".$this->io->input["get"]["epid"] ;
	$sub_search_query .=  "
		AND ep.`epid` = '".$this->io->input["get"]["epid"]."'";
}
if(!empty($this->io->input["get"]["epname"])){																		//兌換商品名稱
	$status["status"]["search"]["epname"] = $this->io->input["get"]["epname"] ;
	$status["status"]["search_path"] .= "&epname=".$this->io->input["get"]["epname"] ;
	$sub_search_query .=  "
		AND ep.`name` like '%".$this->io->input["get"]["epname"]."%'";
}
if(!empty($this->io->input["get"]["ontimefrom"])){																	//兌換商品上架開始日期
	$status["status"]["search"]["ontimefrom"] = $this->io->input["get"]["ontimefrom"] ;
	$status["status"]["search_path"] .= "&ontimefrom=".$this->io->input["get"]["ontimefrom"] ;
	$sub_search_query .=  "
		AND `ontime` >= '".$this->io->input["get"]["ontimefrom"]."'";
	$output_control = 'true';	
}
if(!empty($this->io->input["get"]["ontimeto"])){																	//兌換商品上架結束日期
	$status["status"]["search"]["ontimeto"] = $this->io->input["get"]["ontimeto"] ;
	$status["status"]["search_path"] .= "&ontimeto=".$this->io->input["get"]["ontimeto"] ;
	$sub_search_query .=  "
		AND `ontime` < '".$this->io->input["get"]["ontimeto"]."'";
}
if(!empty($this->io->input["get"]["offtimefrom"])){																	//兌換商品下架開始日期
	$status["status"]["search"]["offtimefrom"] = $this->io->input["get"]["offtimefrom"] ;
	$status["status"]["search_path"] .= "&offtimefrom=".$this->io->input["get"]["offtimefrom"] ;
	$sub_search_query .=  "
		AND `offtime` >= '".$this->io->input["get"]["offtimefrom"]."'";
}
if(!empty($this->io->input["get"]["offtimeto"])){																	//兌換商品下架結束日期
	$status["status"]["search"]["offtimeto"] = $this->io->input["get"]["offtimeto"] ;
	$status["status"]["search_path"] .= "&offtimeto=".$this->io->input["get"]["offtimeto"] ;
	$sub_search_query .=  "
		AND `offtime` < '".$this->io->input["get"]["offtimeto"]."'";
}
if(isset($this->io->input["get"]["total_price"]) && is_numeric($this->io->input["get"]["total_price"])){			//兌換點數
	$status["status"]["search"]["total_price"] = $this->io->input["get"]["total_price"] ;
	$status["status"]["search_path"] .= "&total_price=".$this->io->input["get"]["total_price"] ;
	$sub_search_query .=  "
		AND (ifnull(ep.process_fee, 0) + ifnull(ep.point_price, 0)) = '".$this->io->input["get"]["total_price"]."'";
}
if(isset($this->io->input["get"]["cost_price"]) && is_numeric($this->io->input["get"]["cost_price"])){				//兌換成本
	$status["status"]["search"]["cost_price"] = $this->io->input["get"]["cost_price"] ;
	$status["status"]["search_path"] .= "&cost_price=".$this->io->input["get"]["cost_price"] ;
	$sub_search_query .=  "
		AND `cost_price` = '".$this->io->input["get"]["cost_price"]."'";
}
if(!empty($this->io->input["get"]["epcid"])){																		//兌換商品分類
	$status["status"]["search"]["epcid"] = $this->io->input["get"]["epcid"] ;
	$status["status"]["search_path"] .= "&epcid=".$this->io->input["get"]["epcid"] ;
	$sub_search_query .=  "
		AND epc.`epcid` = '".$this->io->input["get"]["epcid"]."'";
}
if (isset($this->io->input["get"]["exchange_count"]) && is_numeric($this->io->input["get"]["exchange_count"])) {		//兌換次數
	$status["status"]["search"]["exchange_count"] = $this->io->input["get"]["exchange_count"] ;
	$status["status"]["search_path"] .= "&exchange_count=".$this->io->input["get"]["exchange_count"] ;
	$sub_having_query .=  "
		having `exchange_count` >= '".$this->io->input["get"]["exchange_count"]."'";
		$sub_sort_query =  " ORDER BY exchange_count DESC, ep.epid DESC";

}
// Search End


// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


if (empty($sub_search_query)){
	$sub_search_query = " AND `ontime` < NOW() AND `offtime` >= NOW() ";
}

##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
select count(eps.num) num from 
(select count(*) num, ep.name epname, (ifnull(ep.process_fee, 0) + ifnull(ep.point_price, 0)) total_price, ifnull(ep.cost_price, 0) cost_price, 
(select count(*) from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
where 
	o.prefixid = ep.prefixid 
	and o.epid = ep.epid 
	and o.type = 'exchange'
	and o.epid is not null
	and o.switch = 'Y'
) exchange_count 
from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` ep 
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category_rt` epcr on 
	ep.prefixid = epcr.prefixid
	and ep.epid = epcr.epid
	and epcr.switch = 'Y'
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category` epc on 
	epcr.prefixid = epc.prefixid
	and epc.epcid = epcr.epcid
	and epc.switch = 'Y'
where 
	ep.prefixid = '".$this->config->default_prefix_id."'
	and ep.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_group_query;
$query .= $sub_having_query;
$query .= ") eps";
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;

// Table Count end 


// Table Record Start
$query = "
select ep.epid, ep.name epname, str_to_date(ifnull(ep.ontime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') ontime, str_to_date(ifnull(ep.offtime, '0000-00-00 00:00'), '%Y-%m-%d %H:%i') offtime, 
(ifnull(ep.process_fee, 0) + ifnull(ep.point_price, 0)) total_price, ifnull(ep.cost_price, 0) cost_price, group_concat(epc.name) epcname, 
(select ifnull(sum(num),0) from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."order` o 
where 
	o.prefixid = ep.prefixid 
	and o.epid = ep.epid 
	and o.type = 'exchange'
	and o.epid is not null
	and o.switch = 'Y'
) exchange_count 
from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product` ep 
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category_rt` epcr on 
	ep.prefixid = epcr.prefixid
	and ep.epid = epcr.epid
	and epcr.switch = 'Y'
left join `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category` epc on 
	epcr.prefixid = epc.prefixid
	and epc.epcid = epcr.epcid
	and epc.switch = 'Y'
where 
	ep.prefixid = '".$this->config->default_prefix_id."'
	and ep.switch = 'Y'
";
$query .= $sub_search_query ;
$query .= $sub_group_query;
$query .= $sub_having_query;
$query .= $sub_sort_query;
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
$query = "select epcid, name, node, layer 
from `".$this->config->db[3]["dbname"]."`.`".$this->config->default_prefix."exchange_product_category` 
where switch = 'Y'
order by epcid
";
$recArr = $this->model->getQueryRecord($query);

if (is_array($recArr['table']['record'])) {
	$i = 0;
	//$j.$value['layer'] = 0;
	foreach ($recArr['table']['record'] as $key => $value)
	{
		if ($value['layer'] == 1) {
			$epcArr[$value['layer']][$i]['epcid'] = $value['epcid'];
			$epcArr[$value['layer']][$i]['name'] = $value['name'];
			$i++;
		}
		else if ($value['layer'] > 1) {
			$epcArr[$value['layer']][$value['node']][$j[$value['node']]]['epcid'] = $value['epcid'];
			$epcArr[$value['layer']][$value['node']][$j[$value['node']]]['name'] = $value['name'];
			$j[$value['node']]++;
		}
	}
}
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_exchange_statistics', 
	`active` = '兌換商品統計清單查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_exchange_statistics/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$this->tplVar('table' , $table['table']) ;
$this->tplVar('status', $status["status"]);
$this->tplVar('epcArr', $epcArr);
$this->display();