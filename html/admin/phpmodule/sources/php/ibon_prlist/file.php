<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start
$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';

if($files['file_name']["error"] != 0 || empty($files['file_name']["size"]) ) {
	$this->jsAlertMsg('上傳錯誤!!');
}
else
{
	$exchange_ibon_File = $files['file_name']['name']; 
	
    if(is_uploaded_file($files['file_name']['tmp_name']) ) {
		move_uploaded_file($files['file_name']['tmp_name'], "./{$exchange_ibon_File}");
	}

	$handle = fopen($exchange_ibon_File, 'r+');
	if (is_resource($handle))
	{
		$newData = array();
		
		$filesize = filesize($exchange_ibon_File);
		$data = fread($handle, $filesize);
		$data = mb_convert_encoding($data, "UTF-8", "big5");
		$newData = explode("\n", $data);
		//array_shift($newData);
		$newData = array_values(array_filter($newData));
		
		if (!empty($newData)) {
			foreach ($newData as $key => $value) {
				$dataArr[$key] = explode(",", $value);
			}
			//print_r($dataArr);exit;

			if (!empty($dataArr)) {
				//判斷是否空白
				foreach ($dataArr as $key => $value) {
					if ($key > 0) {
						foreach ($value as $key1 => $value1) {
							if ($key1 == 1 && $value1 === "") {
								unlink($exchange_ibon_File);
								$this->jsAlertMsg($dataArr[0][$key1].' 請勿空白!!');
							}
						}
					}
				}
				//將第一列資料清除
				array_shift($dataArr);
				$dataArr = array_values(array_filter($dataArr));
				foreach ($dataArr as $key => $value) {
					$prdate_no = trim(str_replace(array('\r\n', '\r', '\n'), '', $value[5]));
			        //新增商品資料
			        $query = "insert into `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_ibon_prlist` 
			        set 
						ac_code = '{$value[0]}', 
						id = '{$value[1]}',
						pname = '{$value[2]}',
						price = '{$value[3]}',
						bonus = '{$value[4]}',
						used = '0',
						prdate_no = '{$prdate_no}'
			        ";
					$this->model->query($query);
					$no = $prdate_no;
				}
			}
		}
		//exit;
        fclose($handle);
    }
    unlink($exchange_ibon_File);

##############################################################################################################################################
// Log Start 
$ibon_data = json_encode($this->io->input["files"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ibon_prlist_file', 
	`active` = 'ibon商品上傳', 
	`memo` = '{$ibon_data}', 
	`root` = 'ibon_prlist/file', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################
	
	
	
	$query = "SELECT count(*) count
	FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_ibon_prlist`
	WHERE 
		prdate_no = '{$no}'
	";
	$rs = $this->model->getQueryRecord($query);
	
	$this->jsPrintMsg('已成功上傳 '. $rs['table']['record'][0]['count'] .' 筆!', $this->io->input['get']['location_url']);
}

// Insert Start
##############################################################################################################################################
//header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));