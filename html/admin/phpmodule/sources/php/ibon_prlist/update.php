<?php
// Check Variable Start
if (empty($this->io->input["post"]["num"])) {
	$this->jsAlertMsg('商品編號錯誤!!');
}
if (empty($this->io->input["post"]["ac_code"])) {
	$this->jsAlertMsg('活動代號錯誤!!');
}
if (empty($this->io->input["post"]["id"])) {
	$this->jsAlertMsg('商品品號錯誤!!');
}
if (empty($this->io->input["post"]["pname"])) {
	$this->jsAlertMsg('商品品名錯誤!!');
}
if (!is_numeric($this->io->input["post"]["price"]) || $this->io->input["post"]["price"] <= 0) {
	$this->jsAlertMsg('商品零售價錯誤!!');
}
if (!is_numeric($this->io->input["post"]["bonus"]) || $this->io->input["post"]["bonus"] <= 0) {
	$this->jsAlertMsg('兌換點數價錯誤!!');
}
if (!is_numeric($this->io->input["post"]["prdate_no"])) {
	$this->jsAlertMsg('兌換檔次錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
$query ="
UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_ibon_prlist` SET 
	`ac_code`='{$this->io->input["post"]["ac_code"]}', 
	`id`='{$this->io->input["post"]["id"]}', 
	`pname`='{$this->io->input["post"]["pname"]}', 
	`price`='{$this->io->input["post"]["price"]}', 
	`bonus`='{$this->io->input["post"]["bonus"]}', 
	`prdate_no`='{$this->io->input["post"]["prdate_no"]}' 
WHERE 
	`num` = '{$this->io->input["post"]["num"]}'
" ;
$this->model->query($query);
//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$ibon_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ibon_prlist_update', 
	`active` = 'ibon商品修改寫入', 
	`memo` = '{$ibon_data}', 
	`root` = 'ibon_prlist/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));