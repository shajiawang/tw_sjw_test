<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

if (($_SESSION['user']['department'] != 'S') &&
	($_SESSION['user']['department'] != 'B') &&
	($_SESSION['user']['department'] != 'D')){ 
	$this->jsAlertMsg('您無權限使用！');
	die();
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$rebate_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'rebate_add', 
	`active` = '購物金新增', 
	`memo` = '{$rebate_data}', 
	`root` = 'rebate/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["rebate_status"] = array("order_refund" => "訂單退費", "bid_close" => "結標轉入", "user_exchange" => "兌換折抵", "user_deposit" => "使用者儲值", "system" => "系統轉入", "system_test" => "系統測試", "others" => "其它", "gift" => "轉贈別人");
$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();