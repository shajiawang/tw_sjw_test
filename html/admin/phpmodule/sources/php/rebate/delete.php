<?php
// Check Variable Start
if(empty($this->io->input["get"]["reid"])) {
	$this->jsAlertMsg('購物金項目編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

if (($_SESSION['user']['department'] != 'S') &&
	($_SESSION['user']['department'] != 'B') &&
	($_SESSION['user']['department'] != 'D')) { 
	$this->jsAlertMsg('您無權限使用！');
	die();
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// database start
$query ="UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}rebate` 
SET switch = 'N' 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `reid` = '{$this->io->input["get"]["reid"]}'
";
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$rebate_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'rebate_delete', 
	`active` = '購物金刪除', 
	`memo` = '{$rebate_data}', 
	`root` = 'rebate/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End

