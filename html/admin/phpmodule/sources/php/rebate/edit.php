<?php
// Check Variable Start
if (empty($this->io->input["get"]["reid"])) {
	$this->jsAlertMsg('購物金編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

if (($_SESSION['user']['department'] != 'S') &&
	($_SESSION['user']['department'] != 'B') &&
	($_SESSION['user']['department'] != 'D')){ 
	$this->jsAlertMsg('您無權限使用！');
	die();
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}rebate`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `reid` = '{$this->io->input["get"]["reid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$rebate_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'rebate_edit', 
	`active` = '購物金修改', 
	`memo` = '{$rebate_data}', 
	`root` = 'rebate/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["rebate_status"] = array("order_refund" => "訂單退費", "bid_close" => "結標轉入", "user_exchange" => "兌換折抵", "user_deposit" => "使用者儲值", "system" => "系統轉入", "system_test" => "系統測試", "others" => "其它", "gift" => "轉贈別人");
$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();