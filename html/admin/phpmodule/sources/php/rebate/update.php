<?php
// Check Variable Start
if (empty($this->io->input["post"]["reid"])) {
	$this->jsAlertMsg('購物金編號錯誤!!');
}
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員編號不可為空白 !!');
}
if (empty($this->io->input["post"]["behav"])) {
	$this->jsAlertMsg('購物金行為不可為空白!!');
}
if (empty($this->io->input["post"]["amount"])) {
	$this->jsAlertMsg('購物金金額錯誤!!');
}
if (!is_numeric($this->io->input["post"]["orderid"]) || $this->io->input["post"]["orderid"] < 0) {
	$this->jsAlertMsg('訂單編號錯誤!!');
}
if (!is_numeric($this->io->input["post"]["productid"]) || $this->io->input["post"]["productid"] < 0) {
	$this->jsAlertMsg('商品編號錯誤!!');
}
if (!is_numeric($this->io->input["post"]["remark"]) || $this->io->input["post"]["remark"] < 0) {
	$this->jsAlertMsg('活動備註錯誤!!');
}
if (!is_numeric($this->io->input["post"]["touid"]) || $this->io->input["post"]["touid"] < 0) {
	$this->jsAlertMsg('轉贈會員編號錯誤!!');
}

// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
//Update Start

	$query = "
	UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}rebate` SET 
		`userid` = '{$this->io->input["post"]["userid"]}',
		`amount` = '{$this->io->input["post"]["amount"]}', 
		`behav` = '{$this->io->input["post"]["behav"]}',
		`orderid` = '{$this->io->input["post"]["orderid"]}',	
		`productid` = '{$this->io->input["post"]["productid"]}', 
		`remark` = '{$this->io->input["post"]["remark"]}',
		`touid`='{$this->io->input["post"]["touid"]}',
		`memo`='{$this->io->input["post"]["memo"]}', 
		`modifyt` = NOW()
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `reid` = '{$this->io->input["post"]['reid']}'
	" ;
	$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$rebate_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'rebate_update', 
	`active` = '購物金修改寫入', 
	`memo` = '{$rebate_data}', 
	`root` = 'rebate/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
		   
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));