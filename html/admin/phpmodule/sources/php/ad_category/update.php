<?php
// Check Variable Start
if (empty($this->io->input["post"]["acid"])) {
	$this->jsAlertMsg('主題編號錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('主題標題錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('主題敘述錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}ad_category` SET 
	`place`='{$this->io->input["post"]["place"]}',
	`name` = '{$this->io->input["post"]["name"]}', 
	`description` = '{$this->io->input["post"]["description"]}', 
	`used` = '{$this->io->input["post"]["used"]}'
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	 AND `acid` = '{$this->io->input["post"]["acid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################
$redis = new Redis();
$redis->connect('sajacache01',6379);
$redis->del("SiteHomeAdBannerList");
$redis->del("SiteAdBannerList".$this->io->input["post"]["place"]);



##############################################################################################################################################
// Log Start 
$ad_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ad_category_update', 
	`active` = '廣告版位修改寫入', 
	`memo` = '{$ad_data}', 
	`root` = 'ad_category/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));