<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

// Check Variable Start
if(empty($this->io->input["get"]["userid"])){
	$this->jsAlertMsg('參數錯誤,請聯絡技術人員!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

// INSERT database start
$query ="UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`
SET 
switch = 'N'
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND userid = '".$this->io->input["get"]["userid"]."'
" ;
$this->model->query($query);

$query ="UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sms_auth`
SET 
switch = 'N'
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND userid = '".$this->io->input["get"]["userid"]."'
" ;
$this->model->query($query);

$query ="UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
SET 
switch = 'N'
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND userid = '".$this->io->input["get"]["userid"]."'
" ;
$this->model->query($query);

$query = "SELECT *
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt`
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND userid = '".$this->io->input["get"]["userid"]."'
AND switch = 'Y'
";
$table = $this->model->getQueryRecord($query);

if (!empty($table['table']['record'])) {
	$ssoid = $table['table']['record'][0]['ssoid'];

	$query = "UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt`
	SET 
	switch = 'N'
	WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND userid = '".$this->io->input["get"]["userid"]."'
	AND ssoid = '".$ssoid."'
	" ;
	$this->model->query($query);

	$query = "UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso`
	SET 
	switch = 'N'
	WHERE 
	prefixid = '".$this->config->default_prefix_id."' 
	AND ssoid = '".$ssoid."'
	" ;
	$this->model->query($query);
}
// INSERT database end


##############################################################################################################################################
// Log Start 
$user_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_delete', 
	`active` = '會員帳號刪除', 
	`memo` = '{$user_data}', 
	`root` = 'user/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// INSERT success message Start
$this->jsPrintMsg('刪除成功!!',$this->io->input["get"]["location_url"]);
// INSERT success message End