<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

if (empty($this->io->input["get"]["userid"])) {
	$this->jsAlertMsg('userid錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$query ="
SELECT * 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND userid = '".$this->io->input["get"]["userid"]."'
" ;
$table = $this->model->getQueryRecord($query);

##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_edit', 
	`active` = '會員帳號修改', 
	`memo` = '{$user_data}', 
	`root` = 'user/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();