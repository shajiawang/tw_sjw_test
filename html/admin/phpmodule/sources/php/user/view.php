<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start

// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(userid|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
        if($orders=="\'userid\'") $orders="\'u.userid\'";
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
} else {
	$sub_sort_query =  " ORDER BY u.userid DESC";
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$join_search_query = "
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON
	u.prefixid = up.prefixid
	AND u.userid = up.userid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sso_rt` us ON
	u.prefixid = us.prefixid
	AND u.userid = us.userid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."sso` s ON
	us.prefixid = s.prefixid
	AND us.ssoid = s.ssoid
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sms_auth` ua ON
	u.prefixid = ua.prefixid
	AND u.userid = ua.userid
";


if($this->io->input["get"]["search_tel"] != ''){
	$status["status"]["search"]["search_tel"] = $this->io->input["get"]["search_tel"] ;
	$status["status"]["search_path"] .= "&search_tel=".$this->io->input["get"]["search_tel"] ;

	$sub_search_query .=  "AND ua.`phone`='".$this->io->input["get"]["search_tel"]."'
	";
}

if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;

	$sub_search_query .=  "AND u.`name` like '%".$this->io->input["get"]["search_name"]."%'
	";
}
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;

	$sub_search_query .=  " AND u.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
}
if($this->io->input["get"]["search_verified"] != ''){
	$status["status"]["search"]["search_verified"] = $this->io->input["get"]["search_verified"] ;
	$status["status"]["search_path"] .= "&search_verified=".$this->io->input["get"]["search_verified"] ;

	$sub_search_query .=  "AND ua.`verified`='".$this->io->input["get"]["search_verified"]."'
	";
}
if($this->io->input["get"]["search_nickname"] != ''){
	$status["status"]["search"]["search_nickname"] = $this->io->input["get"]["search_nickname"] ;
	$status["status"]["search_path"] .= "&search_nickname=".$this->io->input["get"]["search_nickname"] ;

	$sub_search_query .= "AND up.`nickname` like '%".$this->io->input["get"]["search_nickname"]."%'
	";
}
if($this->io->input["get"]["search_email"] != ''){
	$status["status"]["search"]["search_email"] = $this->io->input["get"]["search_email"] ;
	$status["status"]["search_path"] .= "&search_email=".$this->io->input["get"]["search_email"] ;

	$sub_search_query .=  "
	AND u.email like '%".$this->io->input["get"]["search_email"]."%'
	AND u.userid IS NOT NULL
	";
}
if($this->io->input["get"]["search_provider_name"] != ''){
	$status["status"]["search"]["search_provider_name"] = $this->io->input["get"]["search_provider_name"] ;
	$status["status"]["search_path"] .= "&search_provider_name=".$this->io->input["get"]["search_provider_name"] ;

	$sub_search_query .=  "AND s.name = '".$this->io->input["get"]["search_provider_name"]."'
	AND s.ssoid IS NOT NULL
	";
}
if($this->io->input["get"]["search_switch"] != ''){
	$status["status"]["search"]["search_switch"] = $this->io->input["get"]["search_switch"] ;
	$status["status"]["search_path"] .= "&search_switch=".$this->io->input["get"]["search_switch"] ;

	$sub_search_query .=  "AND u.switch ='".$this->io->input["get"]["search_switch"]."'
	";
}
if($this->io->input["get"]["search_bid"] != ''){
	$status["status"]["search"]["search_bid"] = $this->io->input["get"]["search_bid"] ;
	$status["status"]["search_path"] .= "&search_bid=".$this->io->input["get"]["search_bid"] ;

	$sub_search_query .=  "AND u.`bid_enable`='".$this->io->input["get"]["search_bid"]."'
	";
}
if($this->io->input["get"]["search_idnum"] != ''){
	$status["status"]["search"]["search_idnum"] = $this->io->input["get"]["search_idnum"] ;
	$status["status"]["search_path"] .= "&search_idnum=".$this->io->input["get"]["search_idnum"] ;

	$sub_search_query .=  "AND (up.`idnum`='".$this->io->input["get"]["search_idnum"]."' or up.`idnum`='".encrypt($this->io->input["get"]["search_idnum"])."')
	";
}

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start

// Table Count Start
$query="SELECT count(*) as num
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user` u
{$join_search_query}
WHERE
u.prefixid = '".$this->config->default_prefix_id."' ";
$query .= $sub_search_query ;
$query .= $sub_sort_query ;
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end

// Table Record Start
$query ="SELECT u.*, up.nickname,up.idnum, up.memo, up.addressee,up.area,up.address,up.rphone, ua.verified, s.name as provider_name, ua.phone, ua.code
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
{$join_search_query}
WHERE
u.prefixid = '".$this->config->default_prefix_id."' " ;
$query .= $sub_search_query ;
$query .= $sub_sort_query ;
$query .= $query_limit ;
$table = $this->model->getQueryRecord($query);
// Table Record End
for ($i=0;$i<sizeof($table['table']['record']);$i++){
	if (strlen($table['table']['record'][$i]['idnum'])>20) $table['table']['record'][$i]['idnum']=trim(decrypt($table['table']['record'][$i]['idnum']));
}
// Table End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$user_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user',
	`active` = '會員帳號清單查詢',
	`memo` = '{$user_data}',
	`root` = 'user/view',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################
$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();
