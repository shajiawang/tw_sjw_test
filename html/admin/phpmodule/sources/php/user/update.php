<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

/*
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('userid錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('使用者帳號不可空白!!');
}
if (empty($this->io->input["post"]["passwd"])) {
	$this->jsAlertMsg('使用者密碼不可空白!!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('Email不可空白!!');
}
if (!filter_var($this->io->input['post']['email'], FILTER_VALIDATE_EMAIL)) {
	$this->jsAlertMsg('Email格式錯誤!!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('不可空白!!');
}
if (empty($this->io->input["post"]["nickname"])) {
	$this->jsAlertMsg('暱稱不可空白!!');
}
if (empty($this->io->input["post"]["gender"])) {
	$this->jsAlertMsg('性別不可空白!!');
}
if (empty($this->io->input["post"]["birthday"])) {
	$this->jsAlertMsg('生日不可空白!!');
}
if (empty($this->io->input["post"]["area"])) {
	$this->jsAlertMsg('地址不可空白!!');
}
if (empty($this->io->input["post"]["address"])) {
	$this->jsAlertMsg('地址不可空白!!');
}
if (empty($this->io->input["post"]["addressee"])) {
	$this->jsAlertMsg('收件人不可空白!!');
}
if (empty($this->io->input["post"]["phone"])) {
	$this->jsAlertMsg('聯絡電話不可空白!!');
}
*/

if (!empty($this->io->input["post"]["type"]) )
{
	$uid = $this->io->input["post"]["uid"];

	if ($this->io->input["post"]["type"]=='get_pwd')
	{
		$ret['msg'] = get_shuffle();
	}
	elseif ($this->io->input["post"]["type"]=='bid_enable')
	{
		//兌換/設定下標功能
		$var['key'] = 'bid_enable';
		$var['mod'] = $this->io->input["post"]["mod"];
		$_ret = set_bid_enable($this->config, $this->model, $uid, $var);

		if($_ret==1){
			$ret['msg'] = '已完成設定';
		}else {
			$ret['msg'] = '設定失敗';
		}
	}
	elseif ($this->io->input["post"]["type"]=='dream_enable')
	{
		//設定圓夢下標功能
		$var['key'] = 'dream_enable';
		$var['mod'] = $this->io->input["post"]["mod"];
		$_ret = set_bid_enable($this->config, $this->model, $uid, $var);

		if($_ret==1){
			$ret['msg'] = '已完成設定';
		}else {
			$ret['msg'] = '設定失敗';
		}
	}
	elseif ($this->io->input["post"]["type"]=='exchange_enable')
	{
		//設定兌換功能
		$var['key'] = 'exchange_enable';
		$var['mod'] = $this->io->input["post"]["mod"];
		$_ret = set_bid_enable($this->config, $this->model, $uid, $var);

		if($_ret==1){
			$ret['msg'] = '已完成設定';
		}else {
			$ret['msg'] = '設定失敗';
		}
	}
	elseif ($this->io->input["post"]["type"]=='deposit_enable')
	{
		//設定充值功能
		$var['key'] = 'deposit_enable';
		$var['mod'] = $this->io->input["post"]["mod"];
		$_ret = set_bid_enable($this->config, $this->model, $uid, $var);

		if($_ret==1){
			$ret['msg'] = '已完成設定';
		}else {
			$ret['msg'] = '設定失敗';
		}
	}
	elseif ($this->io->input["post"]["type"]=='change_pwd')
	{
		//修改收件資料
		$var['passwd'] = $this->io->input["post"]["passwd"];
		if(!empty($var['passwd']) ){
			//修改
			$_ret = change_pwd($this->config, $this->model, $uid, $var);
		} else {
			$_ret = 0;
		}

		if($_ret==1){
			$ret['msg'] = $var['passwd'] .', 已存檔';
		} else {
			$ret['msg'] = '設定失敗';
		}
	}
	elseif ($this->io->input["post"]["type"]=='modify_address')
	{
		//修改收件資料
		$var['addressee'] = $this->io->input["post"]["addressee"];
		$var['rphone'] = $this->io->input["post"]["rphone"];
		$var['area'] = $this->io->input["post"]["area"];
		$var['address'] = $this->io->input["post"]["address"];

		//修改
		$_ret = set_addressee($this->config, $this->model, $uid, $var);

		if($_ret==1){
			$ret['msg'] = '收件資料已修改';
		}else {
			$ret['msg'] = '編輯失敗';
		}
	}
	elseif ($this->io->input["post"]["type"]=='modify_nickname')
	{
		//修改會員資料
		$var['nickname'] = $this->io->input["post"]["nickname"];
		$var['memo'] = $this->io->input["post"]["memo"];

		//修改會員名稱
		$_ret = set_nickname($this->config, $this->model, $uid, $var);

		if($_ret==1){
			$ret['msg'] = '會員資料已修改';
		}else {
			$ret['msg'] = '編輯失敗';
		}
	}
	elseif ($this->io->input["post"]["type"]=='verified')
	{
		//手機驗證
		$var['phone'] = $this->io->input["post"]["phone"];
		$var['smscode'] = $this->io->input["post"]["smscode"];

		$_ret = set_sms_auth($this->config, $this->model, $uid, $var);
		if($_ret==1){
			$ret['msg'] = '手機已驗證';
		}else {
			$ret['msg'] = '手機驗證錯誤';
		}
	}
	elseif ($this->io->input["post"]["type"]=='del_verified')
	{
		$_ret = del_sms_auth($this->config, $this->model, $uid);
		if($_ret==1){
			$ret['msg'] = '手機已取消驗證';
		}else {
			$ret['msg'] = '手機取消驗證錯誤';
		}
	}
	elseif ($this->io->input["post"]["type"]=='modify_idnum')
	{
		//修改會員資料
		$var['idnum'] = $this->io->input["post"]["idnum"];
		$var['applyYyymmdd'] = $this->io->input["post"]["applyYyymmdd"];
		$var['applyCode'] = $this->io->input["post"]["applyCode"];
		$var['issueSiteId'] = $this->io->input["post"]["issueSiteId"];
		$checkid = id_card($var['idnum']);
		if ($checkid == '驗證通過'){
			//修改會員名稱
			$result=json_decode(id_verify($var['idnum'],$var['applyYyymmdd'],$var['applyCode'],$var['issueSiteId']),true);
			if (empty($result['responseData']['checkIdCardApply'])){
				$_ret=-1;
				$ret['msg']='與驗証主機出現問題'.((empty($result['httpCode']))?$result['message']:$result['httpMessag']);
				//$ret['msg']=json_encode($result);
			}else{
				$_ret=$result['responseData']['checkIdCardApply'];
				if (check_id_verified($this->config, $this->model,$var['idnum'])==0){
					if ($_ret=='1') $_ret= set_idnum($this->config, $this->model, $uid, $var);
				}else{
					$_ret=9;
					$ret['msg']='身份證字號'.$var['idnum'].'已被其他帳號使用';
				}
			}

		}
		switch($_ret){
			case '0':
				$ret['msg'] = '身份證字號修改失敗 '.$checkid ;
				break;
			case '1':
				$ret['msg'] = '身份證字號已修改';
				break;
			case '2':
				$ret['msg'] = '身分證字號 '.$var['idnum'].' 目前驗證資料錯誤次數已達 1 次，今日錯誤累積達 3 次後，此身分證字號將無法查詢。 ' ;
				break;
			case '3':
				$ret['msg'] = '身分證字號 '.$var['idnum'].' 目前驗證資料錯誤次數已達 2 次，今日錯誤累積達 3 次後，此身分證字號將無法查詢。 ' ;
				break;
			case '4':
				$ret['msg'] = '身分證字號 '.$var['idnum'].' 目前驗證資料錯誤次數已達 3 次，今日錯誤累積達 3 次後，此身分證字號將無法查詢。 ' ;
				break;
			case '5':
				$ret['msg'] = '身分證字號 '.$var['idnum'].' 驗證資料錯誤次數已達 3 次。今 日無法查詢，請明日再查!!';
				break;
			case '6':
				$ret['msg'] = '您所查詢的國民身分證字號 '.$var['idnum'].' 已停止使用。 ' ;
				break;
			case '7':
				$ret['msg'] = '您所查詢的國民身分證 '.$var['idnum'].'，業依當事人申請登錄掛失。' ;
				break;
			case '8':
				$ret['msg'] = '單一使用者出現異常使用情形，暫停使用者權限。 ' ;
				break;
		}
	}
	elseif ($this->io->input["post"]["type"]=='level')
	{
		//修改會員資料
		$var['level'] = $this->io->input["post"]["value"];

		//修改會員名稱
		$_ret = set_level($this->config, $this->model, $uid, $var);

		if($_ret==1){
			$ret['msg'] = '會員等級已修改';
		}else {
			$ret['msg'] = '編輯失敗';
		}
	}

	echo json_encode($ret);
}
else
{
	$query ="
	UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`
	SET
		`name`   = '".$this->io->input["post"]["name"]."',
		`passwd` = '".$this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key)."',
		`email`  = '".$this->io->input["post"]["email"]."',
		`seq`    = '".$this->io->input["post"]["seq"]."'
	WHERE
	prefixid   = '".$this->config->default_prefix_id."'
	AND userid = '".$this->io->input["post"]["userid"]."'
	" ;
	//echo $query;exit;
	$this->model->query($query);

	$query ="
	UPDATE `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`
	SET
		`nickname`  = '".$this->io->input["post"]["nickname"]."',
		`gender`    = '".$this->io->input["post"]["gender"]."',
		`birthday`  = '".$this->io->input["post"]["birthday"]."',
		`area`      = '".$this->io->input["post"]["area"]."',
		`address`   = '".$this->io->input["post"]["address"]."',
		`addressee` = '".$this->io->input["post"]["addressee"]."',
		`phone`     = '".$this->io->input["post"]["phone"]."',
		`seq`       = '".$this->io->input["post"]["seq"]."'
	WHERE
	prefixid   = '".$this->config->default_prefix_id."'
	AND userid = '".$this->io->input["post"]["userid"]."'
	" ;
	$this->model->query($query);

	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
}


//鎖定/解鎖下標兌換
function set_bid_enable($config, $model, $uid, $var=array())
{
	if($var['mod']=="disable"){
	//鎖定
		$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` SET ";
		$query .="`{$var['key']}`='N',";
		$query .="modifyt = now()
			WHERE `prefixid` = '{$config->default_prefix_id}'
				AND `userid` = '{$uid}'
			";
		$model->query($query);

	} else {
	//解鎖

		$u_query = '';

		//查驗圓夢是否得標
		if($var['key']=='dream_enable'){
			$query ="SELECT pp.`pgpid`,pp.`productid`,pp.`userid`,pp.`complete`
				FROM `saja_shop`.`saja_pay_get_product` pp
				LEFT JOIN `saja_shop`.`saja_product` p ON  p.`productid`=pp.`productid`
				WHERE pp.`prefixid`='saja'
					AND pp.`switch`='Y'
					AND pp.`complete`='Y'
					AND p.`ptype`=1
					AND p.`switch`='Y'
					AND pp.`userid`='{$uid}'
				";
			$recArr = $model->getQueryRecord($query);
			if(empty($recArr['table']['record']) ) {
				$u_query = "`dream_enable`='Y',";
			} else {
				$u_query = "`dream_enable`='N',";
			}
		} else {
			$u_query = "`{$var['key']}`='Y',";
		}

		$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` SET ";
		$query .= $u_query;
		$query .="modifyt = now()
			WHERE `prefixid` = '{$config->default_prefix_id}'
				AND `userid` = '{$uid}'
			";
		$model->query($query);
	}

	return 1;
}

//設定臨時密碼
function change_pwd($config, $model, $uid, $var=array())
{
	$query ="SELECT *
		FROM `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` u
		WHERE u.`prefixid` = '{$config->default_prefix_id}'
			AND u.`userid` = '{$uid}'
			AND u.`switch` = 'Y'
		";
	$recArr = $model->getQueryRecord($query);
	if(empty($recArr['table']['record']) ) {
		return 0;
	}

	$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` SET
		`passwd_tmp`='{$var['passwd']}',
		modifyt = now()
		WHERE
			`prefixid` = '{$config->default_prefix_id}'
			AND `userid` = '{$uid}'
		";
	$model->query($query);

	return 1;
}

//修改收件資料
function set_addressee($config, $model, $uid, $var=array())
{
	$query ="SELECT *
		FROM `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` u
		INNER JOIN `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile` up
			ON u.`userid`=up.`userid`
		WHERE u.`prefixid` = '{$config->default_prefix_id}'
			AND u.`userid` = '{$uid}'
			AND u.`switch` = 'Y'
		";
	$recArr = $model->getQueryRecord($query);
	if(empty($recArr['table']['record']) ) {
		return 0;
	}

	$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile` SET
		`addressee`='{$var['addressee']}',
		`rphone`='{$var['rphone']}',
		`area`='{$var['area']}',
		`address`='{$var['address']}'
		WHERE
			`prefixid` = '{$config->default_prefix_id}'
			AND `userid` = '{$uid}'
		" ;
	$model->query($query);

	return 1;
}

//修改會員名稱
function set_nickname($config, $model, $uid, $var=array())
{
	$query ="SELECT *
		FROM `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` u
		INNER JOIN `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile` up
			ON u.`userid`=up.`userid`
		WHERE u.`prefixid` = '{$config->default_prefix_id}'
			AND u.`userid` = '{$uid}'
			AND u.`switch` = 'Y'
		";
	$recArr = $model->getQueryRecord($query);
	if(empty($recArr['table']['record']) ) {
		return 0;
	}

	$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile` SET
		`nickname`='{$var['nickname']}',
		`memo`='{$var['memo']}'
		WHERE
			`prefixid` = '{$config->default_prefix_id}'
			AND `userid` = '{$uid}'
		" ;
	$model->query($query);

	return 1;
}

//手機驗證
function set_sms_auth($config, $model, $uid, $var=array())
{
	$query ="SELECT *
		FROM `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_sms_auth`
		WHERE
			prefixid = '{$config->default_prefix_id}'
			AND userid = '{$uid}'
		";
	$recArr = $model->getQueryRecord($query);

	if(empty($recArr['table']['record']) ) {
			$query = "INSERT INTO `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_sms_auth`
			SET
				`prefixid` = '{$config->default_prefix_id}',
				`userid`='{$uid}',
				`code`='',
				`verified`='N',
				`insertt`=NOW()
			";
			$model->query($query);
	}

	$checkcode = set_checkcode();
	$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_sms_auth`
		SET verified='Y', code='{$checkcode}'
		WHERE
			prefixid = '{$config->default_prefix_id}'
			AND userid = '{$uid}'
		" ;
	$model->query($query);

	return 1;
}

function del_sms_auth($config, $model, $uid)
{
	$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_sms_auth`
		SET verified='N'
		WHERE
			prefixid = '{$config->default_prefix_id}'
			AND userid = '{$uid}'
		" ;
	$model->query($query);

	return 1;
}

function set_checkcode()
{
	$v = array('A','B','C','D','E','F','G','H','J','K','L','M','N','P','Q','R','S','T','U','V','W','X','Y','Z');
	shuffle($v);
	$rand = time() * rand(1,9);
	$rand1 = substr($rand, -6, 2);
	$rand2 = substr($rand, -4, 2);
	$checkcode = $v[0]. $rand1 . $v[1] . $rand2;

	return $checkcode;
}

/*
 * check_id_verified
 */
 function check_id_verified($config, $model,$idnum) {
    $crypt_id=encrypt($idnum);
	$query = "SELECT userid
	FROM `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile`
	WHERE
		(idnum='{$idnum}' or idnum='{$crypt_id}')
		AND switch = 'Y'
	";
	$recArr = $model->getQueryRecord($query);
	if(empty($recArr['table']['record']) ) {
		return 0;
	}else{
		return 1;
	}
}

//修改會員身份證字號
function set_idnum($config, $model, $uid, $var=array())
{
	$query ="SELECT *
		FROM `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` u
		INNER JOIN `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile` up
			ON u.`userid`=up.`userid`
		WHERE u.`prefixid` = '{$config->default_prefix_id}'
			AND u.`userid` = '{$uid}'
		";
	$recArr = $model->getQueryRecord($query);
	if(empty($recArr['table']['record']) ) {
		return 0;
	}

	$idnum=$var['idnum'];
	$term=($idnum[1]=='1')?'male':'female';
	$idnum=encrypt($idnum);
	$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile` SET
		`idnum`='{$idnum}' ,gender='{$term}'
		WHERE
			`prefixid` = '{$config->default_prefix_id}'
			AND `userid` = '{$uid}'
		" ;
	$model->query($query);

	return 1;
}

//修改會員名稱
function set_level($config, $model, $uid, $var=array())
{
	$query ="SELECT *
		FROM `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` u
		INNER JOIN `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user_profile` up
			ON u.`userid`=up.`userid`
		WHERE u.`prefixid` = '{$config->default_prefix_id}'
			AND u.`userid` = '{$uid}'
			AND u.`switch` = 'Y'
		";
	$recArr = $model->getQueryRecord($query);
	if(empty($recArr['table']['record']) ) {
		return 0;
	}

	$query ="UPDATE `".$config->db[0]["dbname"]."`.`".$config->default_prefix."user` SET
		`level`='{$var['level']}'
		WHERE
			`prefixid` = '{$config->default_prefix_id}'
			AND `userid` = '{$uid}'
		" ;
	$model->query($query);

	return 1;
}

//身份證字號格式檢查
function id_card($cardid){
$err ='';
//先將字母數字存成陣列
$alphabet =['A'=>'10','B'=>'11','C'=>'12','D'=>'13','E'=>'14','F'=>'15','G'=>'16','H'=>'17','I'=>'34',
			'J'=>'18','K'=>'19','L'=>'20','M'=>'21','N'=>'22','O'=>'35','P'=>'23','Q'=>'24','R'=>'25',
			'S'=>'26','T'=>'27','U'=>'28','V'=>'29','W'=>'32','X'=>'30','Y'=>'31','Z'=>'33'];
//檢查字元長度
if(strlen($cardid) !=10){$err = '1';}//長度不對

//驗證英文字母正確性
$alpha = substr($cardid,0,1);//英文字母
$alpha = strtoupper($alpha);//若輸入英文字母為小寫則轉大寫
if(!preg_match("/[A-Za-z]/",$alpha)){
	$err = '2';}else{
		//計算字母總和
		$nx = $alphabet[$alpha];
		$ns = $nx[0]+$nx[1]*9;//十位數+個位數x9
	}

//驗證男女性別
$gender = substr($cardid,1,1);//取性別位置
if($gender !='1' && $gender !='2'){$err = '3';}//驗證性別

//N2x8+N3x7+N4x6+N5x5+N6x4+N7x3+N8x2+N9+N10
if($err ==''){
	$i = 8;
	$j = 1;
	$ms =0;
	//先算 N2x8 + N3x7 + N4x6 + N5x5 + N6x4 + N7x3 + N8x2
	while($i >= 2){
		$mx = substr($cardid,$j,1);//由第j筆每次取一個數字
		$my = $mx * $i;//N*$i
		$ms = $ms + $my;//ms為加總
		$j+=1;
		$i--;
	}
	//最後再加上 N9 及 N10
	$ms = $ms + substr($cardid,8,1) + substr($cardid,9,1);
	//最後驗證除10
	$total = $ns + $ms;//上方的英文數字總和 + N2~N10總和
	if( ($total%10) !=0){$err = '4';}
}
	//錯誤訊息返回
	switch($err){
		case '1':$msg = '字元數錯誤';break;
		case '2':$msg = '英文字母錯誤';break;
		case '3':$msg = '性別錯誤';break;
		case '4':$msg = '驗證失敗';break;
		default:$msg = '驗證通過';break;
	}
	return $msg;
}


##############################################################################################################################################
// Log Start
$user_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_update',
	`active` = '會員帳號修改寫入',
	`memo` = '{$user_data}',
	`root` = 'user/update',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################
