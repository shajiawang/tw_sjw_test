<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];



if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('使用者帳號不可空白!!');
}
if (empty($this->io->input["post"]["passwd"])) {
	$this->jsAlertMsg('使用者密碼不可空白!!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('Email不可空白!!');
}
if (!filter_var($this->io->input['post']['email'], FILTER_VALIDATE_EMAIL)) {
	$this->jsAlertMsg('Email格式錯誤!!');
}
if (empty($this->io->input["post"]["email"])) {
	$this->jsAlertMsg('不可空白!!');
}
if (empty($this->io->input["post"]["nickname"])) {
	$this->jsAlertMsg('暱稱不可空白!!');
}
if (empty($this->io->input["post"]["gender"])) {
	$this->jsAlertMsg('性別不可空白!!');
}
if (empty($this->io->input["post"]["birthday"])) {
	$this->jsAlertMsg('生日不可空白!!');
}
if (empty($this->io->input["post"]["area"])) {
	$this->jsAlertMsg('地址不可空白!!');
}
if (empty($this->io->input["post"]["address"])) {
	$this->jsAlertMsg('地址不可空白!!');
}
if (empty($this->io->input["post"]["addressee"])) {
	$this->jsAlertMsg('收件人不可空白!!');
}
if (empty($this->io->input["post"]["phone"])) {
	$this->jsAlertMsg('聯絡電話不可空白!!');
}

$query ="
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user`(
	`prefixid`,
	`name`,
	`passwd`, 
	`email`, 
	`seq`, 
	`insertt`
) 
VALUES(
	'".$this->config->default_prefix_id."',
	'".$this->io->input["post"]["name"]."',
	'".$this->str->strEncode($this->io->input['post']['passwd'], $this->config->encode_key)."',
	'".$this->io->input["post"]["email"]."',
	'".$this->io->input["post"]["seq"]."',
	now()
)
" ;
//echo $query;exit;
$this->model->query($query);
$userid = $this->model->_con->insert_id;

$query ="
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile`(
	`prefixid`,
	`nickname`,
	`gender`, 
	`birthday`, 
	`area`, 
	`address`, 
	`addressee`, 
	`phone`, 
	`seq`, 
	`insertt`
) 
VALUES(
	'".$this->config->default_prefix_id."',
	'".$this->io->input["post"]["nickname"]."',
	'".$this->io->input["post"]["gender"]."',
	'".$this->io->input["post"]["birthday"]."',
	'".$this->io->input["post"]["area"]."',
	'".$this->io->input["post"]["address"]."',
	'".$this->io->input["post"]["addressee"]."',
	'".$this->io->input["post"]["phone"]."',
	'".$this->io->input["post"]["seq"]."',
	now()
)
" ;
$this->model->query($query);


##############################################################################################################################################
// Log Start 
$user_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_insert', 
	`active` = '會員帳號新增寫入', 
	`memo` = '{$user_data}', 
	`root` = 'user/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));