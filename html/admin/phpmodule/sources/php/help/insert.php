<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('問答標題錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('問答內容錯誤!!');
}
if (empty($this->io->input["post"]["hcid"])) {
	$this->jsAlertMsg('分類錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
$description = htmlspecialchars($this->io->input["post"]["description"]);
$query ="
INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}help` SET
	`hcid`='{$this->io->input["post"]["hcid"]}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$description}',
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='{$this->io->input["post"]["seq"]}', 
	`insertt`=now()
" ;
$this->model->query($query);
$faqid = $this->model->_con->insert_id;

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$help_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'help_insert', 
	`active` = '幫助中心新增寫入', 
	`memo` = '{$help_data}', 
	`root` = 'help/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));