<?php
// Check Variable Start
if (empty($this->io->input["post"]["hid"])) {
	$this->jsAlertMsg('問答ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('問答標題錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('問答內容錯誤!!');
}
if (empty($this->io->input["post"]["hcid"])) {
	$this->jsAlertMsg('分類錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
//Update Start
$description = htmlspecialchars($this->io->input["post"]["description"]);
$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}help` SET 
	`hcid`='{$this->io->input["post"]["hcid"]}',
	`name` = '{$this->io->input["post"]["name"]}', 
	`description` = '{$description}',
	`seq` = '{$this->io->input["post"]["seq"]}'
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `hid` = '{$this->io->input["post"]["hid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$help_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'help_update', 
	`active` = '幫助中心修改寫入', 
	`memo` = '{$help_data}', 
	`root` = 'help/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));