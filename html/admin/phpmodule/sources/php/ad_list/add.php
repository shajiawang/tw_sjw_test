<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}ad_category` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND switch = 'Y'
	AND acid != '7'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["ad_category"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$ad_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ad_add', 
	`active` = '廣告新增', 
	`memo` = '{$ad_data}', 
	`root` = 'ad/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["ad_status"] = array("1" => "首頁上方輪播", "2" => "首頁下方輪播", "3" => "殺戮戰場", "4" => "鯊魚商城");
$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();