<?php
// Check Variable Start
if (empty($this->io->input["post"]["adid"])) {
	$this->jsAlertMsg('廣告編號錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('主題錯誤!!');
}
if (empty($this->io->input["post"]["acid"])) {
	$this->jsAlertMsg('選擇主題錯誤!!');
}
if (strtotime($this->io->input["post"]["ontime"]) === false) {
	$this->jsAlertMsg('上架時間錯誤!!');
}
if (strtotime($this->io->input["post"]["offtime"]) === false) {
	$this->jsAlertMsg('下架時間錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_ad_images."/$thumbnail") && ($thumbnail != $this->io->input['post']['oldthumbnail'])) {
		$this->jsAlertMsg('廣告主圖名稱重覆!!');
	}
	else {
        if (move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_ad_images."/$thumbnail")) {
            syncToS3($this->config->path_ad_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/ad/');
        } else {
		    $this->jsAlertMsg('廣告主圖上傳錯誤!!');
        }
	}
	
	//刪除舊圖
	if (!empty($this->io->input['post']['oldthumbnail']) && ($thumbnail != $this->io->input['post']['oldthumbnail'])) {
		unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail']);
	}
	$update_thumbnail = "`thumbnail` = '{$thumbnail}', ";
	syncToS3($this->config->path_products_images."/".$thumbnail,'s3://img.saja.com.tw','/site/images/site/ad/');

}

if (!empty($this->io->input['files']['thumbnail2']) && exif_imagetype($this->io->input['files']['thumbnail2']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail2 = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])."_2").".".pathinfo($this->io->input['files']['thumbnail2']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_ad_images."/$thumbnail2") && ($thumbnail != $this->io->input['post']['oldthumbnail2'])) {
		$this->jsAlertMsg('APP特殊專用圖名稱重覆!!');
	}
	else {
        if (move_uploaded_file($this->io->input['files']['thumbnail2']['tmp_name'], $this->config->path_ad_images."/$thumbnail2")) {
             syncToS3($this->config->path_ad_images."/".$thumbnail2,'s3://img.saja.com.tw','/site/images/site/ad/');
        } else {    
		    $this->jsAlertMsg('APP特殊專用圖上傳錯誤!!');
        }
	}
	
	//刪除舊圖
	if (!empty($this->io->input['post']['oldthumbnail2']) && ($thumbnail != $this->io->input['post']['oldthumbnail2'])) {
		unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail2']);
	}
	
	$update_thumbnail2 = "`thumbnail2` = '{$thumbnail2}', ";
	syncToS3($this->config->path_products_images."/".$thumbnail2,'s3://img.saja.com.tw','/site/images/site/ad/');
}

if (!empty($this->io->input['files']['thumbnail_popup_a']) && exif_imagetype($this->io->input['files']['thumbnail_popup_a']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail_popup_a = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])."_popup_a").".".pathinfo($this->io->input['files']['thumbnail_popup_a']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_ad_images."/$thumbnail_popup_a") && ($thumbnail != $this->io->input['post']['oldthumbnail_popup_a'])) {
		$this->jsAlertMsg('APP特殊專用圖名稱重覆!!');
	}
	else {
        if (move_uploaded_file($this->io->input['files']['thumbnail_popup_a']['tmp_name'], $this->config->path_ad_images."/$thumbnail_popup_a")) {
             syncToS3($this->config->path_ad_images."/".$thumbnail_popup_a,'s3://img.saja.com.tw','/site/images/site/ad/');
        } else {    
		    $this->jsAlertMsg('彈出圖上傳錯誤!!');
        }
	}
	
	//刪除舊圖
	if (!empty($this->io->input['post']['oldthumbnail_popup_a']) && ($thumbnail != $this->io->input['post']['oldthumbnail_popup_a'])) {
		unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail_popup_a']);
	}
	
	$update_thumbnail_popup_a = "`thumbnail_popup_a` = '{$thumbnail_popup_a}', ";
	syncToS3($this->config->path_products_images."/".$thumbnail_popup_a,'s3://img.saja.com.tw','/site/images/site/ad/');
}

if (!empty($this->io->input['files']['thumbnail_popup_i']) && exif_imagetype($this->io->input['files']['thumbnail_popup_i']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail_popup_i = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])."_popup_i").".".pathinfo($this->io->input['files']['thumbnail_popup_i']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_ad_images."/$thumbnail_popup_i") && ($thumbnail != $this->io->input['post']['oldthumbnail_popup_i'])) {
		$this->jsAlertMsg('APP特殊專用圖名稱重覆!!');
	}
	else {
        if (move_uploaded_file($this->io->input['files']['thumbnail_popup_i']['tmp_name'], $this->config->path_ad_images."/$thumbnail_popup_i")) {
             syncToS3($this->config->path_ad_images."/".$thumbnail_popup_i,'s3://img.saja.com.tw','/site/images/site/ad/');
        } else {    
		    $this->jsAlertMsg('彈出圖上傳錯誤!!');
        }
	}
	
	//刪除舊圖
	if (!empty($this->io->input['post']['oldthumbnail_popup_i']) && ($thumbnail != $this->io->input['post']['oldthumbnail_popup_i'])) {
		unlink($this->config->path_ad_images."/".$this->io->input['post']['oldthumbnail_popup_i']);
	}
	
	$update_thumbnail_popup_i = "`thumbnail_popup_i` = '{$thumbnail_popup_i}', ";
	syncToS3($this->config->path_products_images."/".$thumbnail_popup_i,'s3://img.saja.com.tw','/site/images/site/ad/');
}

$name = htmlspecialchars($this->io->input["post"]["name"]);
$description = htmlspecialchars($this->io->input["post"]["description"]);
// $this->io->input["post"]["action_memo_epcid"] 是字串, 但資料庫欄位是INT  如是空白得改為0
if(empty($this->io->input["post"]["action_memo_epcid"])) {
	$this->io->input["post"]["action_memo_epcid"]=0;
}
if(empty($this->io->input["post"]["action_memo_productid"])) {
	$this->io->input["post"]["action_memo_productid"]=0;
}
$action_memo = array(
		"type"		=> $this->io->input["post"]["action_memo_type"],
		"url_title"	=> urlencode($this->io->input["post"]["action_memo_url_title"]),
		"url"		=> $this->io->input["post"]["action_memo_url"],
		"imgurl"	=> $this->io->input["post"]["action_memo_imgurl"],
		"page"		=> $this->io->input["post"]["action_memo_page"],
		"productid"	=> $this->io->input["post"]["action_memo_productid"],
		"epcid"		=> $this->io->input["post"]["action_memo_epcid"],
		"layer"		=> $this->io->input["post"]["action_memo_layer"],
		"epid"		=> $this->io->input["post"]["action_memo_epid"],
		"action_type" => array("act_type" => $this->io->input["post"]["action_memo_acttype"]),
	);
$action_memo = json_encode($action_memo);

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}ad` SET 
	`acid`='{$this->io->input["post"]["acid"]}',
	`name` = '{$name}', 
	`description` = '{$description}',
	{$update_thumbnail}
	{$update_thumbnail2}
	{$update_thumbnail_popup_a}
	{$update_thumbnail_popup_i}
	`thumbnail_url` = '{$this->io->input["post"]["thumbnail_url"]}', 
	`thumbnail_url2` = '{$this->io->input["post"]["thumbnail_url2"]}', 	
	`qrcode_url` = '{$this->io->input["post"]["url"]}',
	`action_a` = '{$this->io->input["post"]["url_a"]}',
	`action_i` = '{$this->io->input["post"]["url_i"]}',		
	`action_memo` = '{$action_memo}',		
	`ontime` = '{$this->io->input["post"]["ontime"]}', 
	`offtime` = '{$this->io->input["post"]["offtime"]}',
	`seq` = '{$this->io->input["post"]["seq"]}', 
	`used` = '{$this->io->input["post"]["used"]}', 
	`epcid`={$this->io->input["post"]["action_memo_epcid"]},
	`productid`={$this->io->input["post"]["action_memo_productid"]},
	`modifyt` = NOW()
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `adid` = '{$this->io->input["post"]["adid"]}'
" ;
$this->model->query($query);


$query = "
SELECT place 
FROM `{$db_channel}`.`{$this->config->default_prefix}ad_category` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND acid = '{$this->io->input["post"]["acid"]}'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$place = $recArr['table']['record'][0]['place'];

$query = "
SELECT place 
FROM `{$db_channel}`.`{$this->config->default_prefix}ad_category` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND acid = '{$this->io->input["post"]["oacid"]}'
	AND switch = 'Y'
";
$recArr2 = $this->model->getQueryRecord($query);
$place2 = $recArr2['table']['record'][0]['place'];

$query = " 
	select ad.adid, ad.name, ad.seq, ad.qrcode_url as action, ad.action_a, ad.action_i, ad.thumbnail_url, ad.thumbnail_url2, ad.thumbnail, ad.thumbnail2 , ad.thumbnail_popup_a  , ad.thumbnail_popup_i, ad.offtime
	from `{$db_channel}`.`{$this->config->default_prefix}ad` ad
	LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}ad_category` ac ON ad.acid = ac.acid
	where 
	  ad.switch = 'Y' 
	  and ad.used = 'Y' 
	  and ac.switch = 'Y' 
	  and ac.used = 'Y' 
	  and NOW() between ad.ontime and ad.offtime 
	  and ac.place = '{$place}'
    order by ad.seq asc ";

//取得資料
$table = $this->model->getQueryRecord($query);

$query2 = " 
	select ad.adid, ad.name, ad.seq, ad.qrcode_url as action, ad.action_a, ad.action_i, ad.thumbnail_url, ad.thumbnail_url2, ad.thumbnail, ad.thumbnail2 , ad.thumbnail_popup_a , ad.thumbnail_popup_i, ad.offtime
	from `{$db_channel}`.`{$this->config->default_prefix}ad` ad
	LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}ad_category` ac ON ad.acid = ac.acid
	where 
	  ad.switch = 'Y' 
	  and ad.used = 'Y' 
	  and ac.switch = 'Y' 
	  and ac.used = 'Y' 
	  and NOW() between ad.ontime and ad.offtime 
	  and ac.place = '{$place2}'
    order by ad.seq asc ";

//取得資料
$table2 = $this->model->getQueryRecord($query2);

//Update End
##############################################################################################################################################
$redis = new Redis();
$redis->connect('sajacache01',6379);
$redis->del("SiteHomeAdBannerList");
$redis->del("SiteAdBannerList".$place);
$redis->del("SiteAdBannerList".$place2);

/*
$rdata='';
$rkey = "SiteAdBannerList".$place;
$rdata = $redis->get($rkey);
$ad_list = $table['table']['record'];
if(!empty($ad_list) && is_array($ad_list)) {
	$redis->set($rkey, json_encode($ad_list));
}  

$rdata='';
$rkey = "SiteAdBannerList".$place2;
$rdata = $redis->get($rkey);
$ad_list = $table2['table']['record'];
if(!empty($ad_list) && is_array($ad_list)) {
	$redis->set($rkey, json_encode($ad_list));
} 	
*/


##############################################################################################################################################
// Log Start 
$ad_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ad_update', 
	`active` = '廣告修改寫入', 
	`memo` = '{$ad_data}', 
	`root` = 'ad/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
		   
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));