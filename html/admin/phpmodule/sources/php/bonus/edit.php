<?php
// Check Variable Start
if (empty($this->io->input["get"]["bonusid"])) {
	$this->jsAlertMsg('紅利帳目ID錯誤!!');
}
/* 鎖uid */
if (($this->io->input['session']['user']['userid'] != '1') && ($this->io->input['session']['user']['userid'] != '2') && ($this->io->input['session']['user']['userid'] != '3') && ($this->io->input['session']['user']['userid'] != '4') &&($this->io->input['session']['user']['userid'] != '26') && ($this->io->input['session']['user']['userid'] != '27')) {
	$this->jsAlertMsg('您無權限登入！');
}
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `bonusid` = '{$this->io->input["get"]["bonusid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

//Relation country
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}country` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country_rt"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();