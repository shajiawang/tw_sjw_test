<?php
// Check Variable Start
if(empty($this->io->input["get"]["bonusid"])) {
	$this->jsAlertMsg('紅利帳目ID錯誤!!');
}
/* 鎖uid */
if ($this->io->input['session']['user']['userid'] != '1' && ($this->io->input['session']['user']['userid'] != '2') && ($this->io->input['session']['user']['userid'] != '3')) {
	$this->jsAlertMsg('您無權限登入！');
}
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// database start

$query ="UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}bonus` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `bonusid` = '{$this->io->input["get"]["bonusid"]}'
" ;
$this->model->query($query);

// database end
##############################################################################################################################################


// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End

