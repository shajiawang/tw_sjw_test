<?php
// Check Variable Start
if (empty($this->io->input["post"]["priid"])) {
	$this->jsAlertMsg('項目ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('項目名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('項目敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	//$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable Start

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_exchange}`.`{$this->config->default_prefix}product_promote` SET 
`name` = '{$this->io->input["post"]["name"]}',
`description` = '{$this->io->input["post"]["description"]}',
`seq` = '{$this->io->input["post"]["seq"]}'
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `ppid` = '{$this->io->input["post"]["ppid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));