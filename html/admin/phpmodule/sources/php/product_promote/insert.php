<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('項目名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('項目敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	//$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}product_promote` SET
	`ppid` = '{$this->io->input["post"]["ppid"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`insertt`=now()
" ;
$this->model->query($query);

// Insert Start
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));