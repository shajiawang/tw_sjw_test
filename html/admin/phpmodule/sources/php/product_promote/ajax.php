<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(ppid|name|seq|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query = " ORDER BY " . implode(',', $orders);
} else {
	//$sub_sort_query = " GROUP BY p.ppid, p.epcid, p.channelid"; 
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$category_search_query = "";
$sub_search_query = "";

if($this->io->input["get"]["search_epid"] != ''){
	$status["status"]["search"]["search_epid"] = $this->io->input["get"]["search_epid"] ;
	$status["status"]["search_path"] .= "&search_epid=".$this->io->input["get"]["search_epid"] ;
	
	$category_search_query .=  "LEFT OUTER JOIN `saja_exchange`.`saja_product_promote_rt` pr 
	ON p.prefixid = pr.prefixid AND p.ppid = pr.ppid AND pr.switch = 'Y' 
	AND pr.`epid` = '{$this->io->input["get"]["search_epid"]}' ";
}

if(!empty($this->io->input["get"]["search_epcid"]) ){
	$status["status"]["search"]["search_epcid"] = $this->io->input["get"]["search_epcid"] ;
	$status["status"]["search_path"] .= "&search_epcid=".$this->io->input["get"]["search_epcid"] ;
	
	$search_epcid = explode(",", $this->io->input["get"]["search_epcid"]);
	if(!empty($search_epcid) ) {
		$sub_search_query .= "AND p.`epcid` IN ('". implode("','", $search_epcid) ."') ";
	} 
	//$sub_search_query .=  " AND p.`epcid` = '".$this->io->input["get"]["search_epcid"]."' ";
}

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="
SELECT count(*) as num 
FROM `{$db_exchange}`.`{$this->config->default_prefix}product_promote` p
{$category_search_query}
WHERE 
p.`prefixid` = '{$this->config->default_prefix_id}'
AND p.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;

// Table Record Start
$query ="
SELECT p.ppid, p.name, pr.epid pp_epid, pr.seq pp_seq
FROM `{$db_exchange}`.`{$this->config->default_prefix}product_promote` p
{$category_search_query}
WHERE 
p.`prefixid` = '{$this->config->default_prefix_id}' 
AND p.`switch`='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 

// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();