<?php
// Check Variable Start
if(empty($this->io->input["get"]["ppid"])) {
	$this->jsAlertMsg('項目ID錯誤!!');
}
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
// database start
$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}product_promote` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `ppid` = '{$this->io->input["get"]["priid"]}'
" ;
$this->model->query($query);

// database end
##############################################################################################################################################


// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End
