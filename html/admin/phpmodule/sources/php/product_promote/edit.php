<?php
// Check Variable Start
if (empty($this->io->input["get"]["ppid"])) {
	$this->jsAlertMsg('項目ID錯誤!!');
}
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}product_promote`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `ppid` = '{$this->io->input["get"]["ppid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

//Relation promote_rule

// Relation End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();