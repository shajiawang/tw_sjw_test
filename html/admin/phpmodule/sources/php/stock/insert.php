<?php
// Check Variable Start
if (empty($this->io->input["post"]["epid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
if (!is_numeric($this->io->input["post"]["num"])) {
	$this->jsAlertMsg('數量錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}stock` SET
	`epid` = '{$this->io->input["post"]["epid"]}',
	`num` = '{$this->io->input["post"]["num"]}',
	`behav` = 'admin_add',
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='0', 
	`switch`='Y',
	`insertt`=now()
" ;
$this->model->query($query); 

// Insert End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$stock_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'stock_insert', 
	`active` = '庫存新增寫入', 
	`memo` = '{$stock_data}', 
	`root` = 'stock/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



if(isset($this->io->input["post"]["back_url"]) ) {
	header("location:". $this->io->input["post"]["back_url"] );
} else {
	header("location:". urldecode(base64_decode($this->io->input['post']['location_url'])) );
}