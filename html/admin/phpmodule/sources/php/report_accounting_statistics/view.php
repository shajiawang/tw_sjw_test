<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start

// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End

// Sort Start
$sub_sort_query =  " ORDER BY o.modifyt DESC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if(!empty($this->io->input["get"]["joindatefrom"])){			//加入起始日期
	$status["status"]["search"]["joindatefrom"] = $this->io->input["get"]["joindatefrom"] ;
	$status["status"]["search_path"] .= "&joindatefrom=".$this->io->input["get"]["joindatefrom"] ;
	$sub_search_query .=  "
		AND o.`insertt` >= '".$this->io->input["get"]["joindatefrom"]."'";
}
if(!empty($this->io->input["get"]["joindateto"])){				//加入結束日期
	$status["status"]["search"]["joindateto"] = $this->io->input["get"]["joindateto"] ;
	$status["status"]["search_path"] .= "&joindateto=".$this->io->input["get"]["joindateto"] ;
	$sub_search_query .=  "
		AND o.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
}
if(!empty($this->io->input["get"]["userid"])){				//會員編號
	$status["status"]["search"]["userid"] = $this->io->input["get"]["userid"] ;
	$status["status"]["search_path"] .= "&userid=".$this->io->input["get"]["userid"] ;
	$sub_search_query .=  "	AND o.`userid` IN (".$this->io->input["get"]["userid"].") ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

##############################################################################################################################################
// Table  Start

// Table Count Start
// Table Count end

// Table Record Start
$query = "
SELECT SUM(amount) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."oscode` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
";

$query .= $sub_search_query ;
$query .= $sub_sort_query;
$query .= $query_limit ;
$table = $this->model->getQueryRecord($query);

$query2 = "
SELECT SUM(amount) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."oscode` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.used = 'Y'
";

$query2 .= $sub_search_query ;
$query2 .= $sub_sort_query;
$query2 .= $query_limit ;
$table2 = $this->model->getQueryRecord($query2);

$query3 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.behav != 'a'
";

$query3 .= $sub_search_query ;
$query3 .= $sub_sort_query;
$query3 .= $query_limit ;
$table3 = $this->model->getQueryRecord($query3);

$query4 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."scode` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.behav = 'a'
";

$query4 .= $sub_search_query ;
$query4 .= $sub_sort_query;
$query4 .= $query_limit ;
$table4 = $this->model->getQueryRecord($query4);


$query5 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount > 0
";

$query5 .= $sub_search_query ;
$query5 .= $sub_sort_query;
$query5 .= $query_limit ;
$table5 = $this->model->getQueryRecord($query5);

$query6 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount < 0
";

$query6 .= $sub_search_query ;
$query6 .= $sub_sort_query;
$query6 .= $query_limit ;
$table6 = $this->model->getQueryRecord($query6);

$query7 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount > 0
	and o.behav in ('gift', 'system', 'other')
";

$query7 .= $sub_search_query ;
$query7 .= $sub_sort_query;
$query7 .= $query_limit ;
$table7 = $this->model->getQueryRecord($query7);


$query8 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount > 0
";

$query8 .= $sub_search_query ;
$query8 .= $sub_sort_query;
$query8 .= $query_limit ;
$table8 = $this->model->getQueryRecord($query8);

$query9 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount < 0
";

$query9 .= $sub_search_query ;
$query9 .= $sub_sort_query;
$query9 .= $query_limit ;
$table9 = $this->model->getQueryRecord($query9);

$query10 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount > 0
	and o.behav in ('gift', 'system', 'other')
";

$query10 .= $sub_search_query ;
$query10 .= $sub_sort_query;
$query10 .= $query_limit ;
$table10 = $this->model->getQueryRecord($query10);


$query11 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."rebate` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount > 0
";

$query11 .= $sub_search_query ;
$query11 .= $sub_sort_query;
$query11 .= $query_limit ;
$table11 = $this->model->getQueryRecord($query11);

$query12 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."rebate` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount < 0
";

$query12 .= $sub_search_query ;
$query12 .= $sub_sort_query;
$query12 .= $query_limit ;
$table12 = $this->model->getQueryRecord($query12);

$query13 = "
SELECT SUM(ABS(amount)) as total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."rebate` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
	and o.amount > 0
	and o.behav in ('gift', 'system', 'other')
";

$query13 .= $sub_search_query ;
$query13 .= $sub_sort_query;
$query13 .= $query_limit ;
$table13 = $this->model->getQueryRecord($query13);
// Table Record End

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_accounting_statistics',
	`active` = '財務統計清單查詢',
	`memo` = '{$report_data}',
	`root` = 'report_accounting_statistics/view',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status['status']['depositfrom'] = $rulelist["table"]['record'];
$status['status']['gender'] = array('male' => '男', 'female' => '女', '' => '男');
$status['status']['alltotal'] = round($alltotal["table"]['record'][0]['total_amount']);

$this->tplVar('table' , $table['table']) ;
$this->tplVar('table2' , $table2['table']) ;
$this->tplVar('table3' , $table3['table']) ;
$this->tplVar('table4' , $table4['table']) ;
$this->tplVar('table5' , $table5['table']) ;
$this->tplVar('table6' , $table6['table']) ;
$this->tplVar('table7' , $table7['table']) ;
$this->tplVar('table8' , $table8['table']) ;
$this->tplVar('table9' , $table9['table']) ;
$this->tplVar('table10' , $table10['table']) ;
$this->tplVar('table11' , $table11['table']) ;
$this->tplVar('table12' , $table12['table']) ;
$this->tplVar('table13' , $table13['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();