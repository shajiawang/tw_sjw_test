<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start

// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End

// Sort Start
$sub_sort_query =  " GROUP BY o.userid ORDER BY o.userid DESC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if(!empty($this->io->input["get"]["joindatefrom"])){			//加入起始日期
	$status["status"]["search"]["joindatefrom"] = $this->io->input["get"]["joindatefrom"] ;
	$status["status"]["search_path"] .= "&joindatefrom=".$this->io->input["get"]["joindatefrom"] ;
	$sub_search_query .=  "
		AND o.`insertt` >= '".$this->io->input["get"]["joindatefrom"]."'";
	$oa_search_query .=  "
		AND oa.`insertt` >= '".$this->io->input["get"]["joindateto"]."'";
	$ou_search_query .=  "
		AND ou.`insertt` >= '".$this->io->input["get"]["joindateto"]."'";
	$of_search_query .=  "
		AND of.`insertt` >= '".$this->io->input["get"]["joindateto"]."'";
	$od_search_query .=  "
		AND od.`insertt` >= '".$this->io->input["get"]["joindateto"]."'";
}
if(!empty($this->io->input["get"]["joindateto"])){				//加入結束日期
	$status["status"]["search"]["joindateto"] = $this->io->input["get"]["joindateto"] ;
	$status["status"]["search_path"] .= "&joindateto=".$this->io->input["get"]["joindateto"] ;
	$sub_search_query .=  "
		AND o.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
	$oa_search_query .=  "
		AND oa.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
	$ou_search_query .=  "
		AND ou.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
	$of_search_query .=  "
		AND of.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
	$od_search_query .=  "
		AND od.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
}
if(!empty($this->io->input["get"]["userid"])){				//會員編號
	$status["status"]["search"]["userid"] = $this->io->input["get"]["userid"] ;
	$status["status"]["search_path"] .= "&userid=".$this->io->input["get"]["userid"] ;
	$sub_search_query .=  "	AND o.`userid` IN (".$this->io->input["get"]["userid"].") ";
	$oa_search_query .=  "	AND oa.`userid` IN (".$this->io->input["get"]["userid"].") ";
	$ou_search_query .=  "	AND ou.`userid` IN (".$this->io->input["get"]["userid"].") ";
	$of_search_query .=  "	AND of.`userid` IN (".$this->io->input["get"]["userid"].") ";
	$od_search_query .=  "	AND od.`userid` IN (".$this->io->input["get"]["userid"].") ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

##############################################################################################################################################
// Table  Start

// Table Count Start
$query="
select count(ps.num) num from (
SELECT count(userid) as num 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}spoint` o 
WHERE 
	o.`prefixid` = '{$this->config->default_prefix_id}' 
	AND o.`switch`='Y'
";
$query .= $sub_search_query ;
$query .= $sub_sort_query;
$query .= ") ps";
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;

// Table Count end
// print_r($query);  
// Table Record Start
$query = "
SELECT o.userid, 
	(select round(ifnull(sum(oa.amount),0)) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` oa where oa.prefixid = o.prefixid and oa.userid = o.userid and oa.switch = 'Y' AND oa.amount > 0 {$oa_search_query}) all_total,
	(select round(ifnull(sum(ou.amount),0)) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` ou where ou.prefixid = o.prefixid and ou.userid = o.userid and ou.switch = 'Y' AND ou.amount < 0 {$ou_search_query}) use_total,
	(select round(ifnull(sum(of.amount),0)) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` of where of.prefixid = o.prefixid and of.userid = o.userid and of.switch = 'Y' AND of.amount > 0 AND of.behav IN ('gift', 'system', 'other') {$of_search_query}) free_total,
	(select round(ifnull(sum(od.amount),0)) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` od where od.prefixid = o.prefixid and od.userid = o.userid and od.switch = 'Y' AND od.amount > 0 AND od.behav IN ('user_deposit') {$od_search_query}) deposit_total 
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` o
where
	o.prefixid = '".$this->config->default_prefix_id."'
	and o.switch = 'Y'
";

$query .= $sub_search_query ;
$query .= $sub_sort_query;
$query .= $query_limit ;
$table = $this->model->getQueryRecord($query);

// Table Record End

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_accounting_statistics',
	`active` = '財務統計殺價幣清單查詢',
	`memo` = '{$report_data}',
	`root` = 'report_accounting_statistics/view',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();