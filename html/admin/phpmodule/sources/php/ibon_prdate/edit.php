<?php
// Check Variable Start
if (empty($this->io->input["get"]["no"])) {
	$this->jsAlertMsg('檔期編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_ibon_prdate`
WHERE `no` = '{$this->io->input["get"]["no"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$ibon_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ibon_prdate_edit', 
	`active` = 'ibon檔期修改', 
	`memo` = '{$ibon_data}', 
	`root` = 'ibon_prdate/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["ad_status"] = array("1" => "首頁上方輪播", "2" => "首頁下方輪播", "3" => "殺戮戰場", "4" => "鯊魚商城");
$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();