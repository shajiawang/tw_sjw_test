<?php
// Check Variable Start
if (!is_numeric($this->io->input["post"]["date_start"])) {
	$this->jsAlertMsg('開始日期格式錯誤!!');
}
if (!is_numeric($this->io->input["post"]["date_end"])) {
	$this->jsAlertMsg('結束日期格式錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################

$query ="
INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_ibon_prdate` SET
	`date_start`='{$this->io->input["post"]["date_start"]}', 
	`date_end`='{$this->io->input["post"]["date_end"]}' 
" ;
$this->model->query($query);
$this->model->_con->insert_id;

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$ibon_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'ibon_prdate_insert', 
	`active` = 'ibon檔期新增寫入', 
	`memo` = '{$ibon_data}', 
	`root` = 'ibon_prdate/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));