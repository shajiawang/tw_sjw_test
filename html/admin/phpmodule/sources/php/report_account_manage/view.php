<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
//$db_user = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start

// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End



// Sort Start
$sub_sort_query =  " ORDER BY u.userid DESC";
// Sort End



// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if(!empty($this->io->input["get"]["tester"])){			//是否加入測試人員
	$status["status"]["search"]["tester"] = $this->io->input["get"]["tester"];
	$status["status"]["search_path"] .= "&tester=".$this->io->input["get"]["tester"];
	$sub_search_query .=  "
		AND u.`userid` not in (1, 3, 9, 28, 116, 507, 585, 586, 588)";
}

if($this->io->input["get"]["search_switch"] != ''){
	$status["status"]["search"]["search_switch"] = $this->io->input["get"]["search_switch"] ;
	$status["status"]["search_path"] .= "&search_switch=".$this->io->input["get"]["search_switch"] ;
	$sub_search_query .=  " AND u.`switch` ='".$this->io->input["get"]["search_switch"]."' ";
} else {
	$sub_search_query .=  " AND u.`switch` ='Y' ";
}

if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  " AND u.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
}
if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  " AND u.`name` like '%".$this->io->input["get"]["search_name"]."%' ";
}
if($this->io->input["get"]["search_gift_spoint"] != ''){
	$status["status"]["search"]["search_gift_spoint"] = $this->io->input["get"]["search_gift_spoint"] ;
	$status["status"]["search_path"] .= "&search_gift_spoint=".$this->io->input["get"]["search_gift_spoint"] ;
	if($this->io->input["get"]["search_gift_spoint"]=='Y') {
	   $sub_search_query .=  " AND (select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='gift' ) > 0 ";
	} else if($this->io->input["get"]["search_gift_spoint"]=='N') {
	   $sub_search_query .=  " AND (select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='gift' ) = 0 ";
	}
	// $sub_search_query .=  " AND u.`name` ='".$this->io->input["get"]["search_name"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start

// Table Count Start
$query="
SELECT count(*) as num
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
WHERE
	u.`prefixid` = '{$this->config->default_prefix_id}'
	AND u.`switch`='Y'
";
$query .= $sub_search_query ;
$query .= $sub_sort_query ;
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end

// Table Record Start
/*
$query = "
SELECT
u.userid, u.name, up.nickname, up.bonus_noexpw,
(select ifnull(sum(d.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d where u.prefixid = d.prefixid and u.userid = d.userid and d.switch = 'Y') deposit_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='gift' ) gift_input_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='system' ) system_input_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount > 0 and s.behav!='bid_refund') input_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount > 0 and s.behav='bid_refund') bid_refund,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount < 0) output_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='user_deposit' ) spoints_by_deposit,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' ) spoint_total,
(SELECT ifnull(sum(t.total_count),0) FROM (SELECT h.userid, count(*) * p.saja_fee AS total_count FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
	LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON
		h.prefixid = p.prefixid
		AND h.productid = p.productid
		AND p.closed = 'N'
		AND p.switch = 'Y'
	WHERE
		h.prefixid = '".$this->config->default_prefix_id."'
		AND h.switch = 'Y'
		AND p.productid IS NOT NULL
		AND h.spointid != '0'
		AND h.scodeid = '0'
		AND p.closed = 'N'
		GROUP BY h.productid, h.userid, p.saja_fee
	)t
	where t.userid = u.userid
) freeze,
(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y') bonus_total,
(select ifnull(-sum(b1.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b1 where u.prefixid = b1.prefixid and u.userid = b1.userid
          and b1.behav in ('user_exchange', 'ibon_exchange','order_refund') and b1.switch = 'Y') bonus_overage,
(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
	WHERE s.prefixid = '".$this->config->default_prefix_id."'
		AND s.switch = 'Y'
		AND s.behav = 'process_fee'
	) t2
	where t2.userid = u.userid
) total_process_amount,
(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
	WHERE s.prefixid = '".$this->config->default_prefix_id."'
		AND s.switch = 'Y'
		AND s.behav = 'user_saja'
	) t2
	where t2.userid = u.userid
) total_bid_process_amount,
(select ifnull(sum(t1.total_count),0) from (SELECT h.userid, count(*) * p.saja_fee AS total_count FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp
	LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h ON
		h.prefixid = pgp.prefixid
		AND h.userid = pgp.userid
		AND h.productid = pgp.productid
		AND h.switch = 'Y'
	LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON
		h.prefixid = p.prefixid
		AND h.productid = p.productid
		AND p.closed = 'Y'
		AND p.switch = 'Y'
	WHERE pgp.prefixid = '".$this->config->default_prefix_id."'
		AND h.switch = 'Y'
		AND p.productid IS NOT NULL
		AND h.spointid != '0'
		AND p.closed = 'Y'
		AND h.type = 'bid'
	GROUP BY h.productid
	) t1
	where t1.userid = u.userid
) total_saja_fee,
(select ifnull(sum(pgp.bid_price_total),0) from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp where u.prefixid = pgp.prefixid and u.userid = pgp.userid and pgp.complete = 'Y' and pgp.switch = 'Y') bid_price_total_all,
(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y' and b.amount > 0 and b.behav in ('product_close')) bonus_input_total
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	up.prefixid = u.prefixid
	and up.userid = u.userid
	and up.switch = 'Y'
WHERE
	u.prefixid = '".$this->config->default_prefix_id."'
	AND u.switch = 'Y'
";
*/
/*
$query = "
SELECT
u.userid, u.name, up.nickname, up.bonus_noexpw, u.switch,
(select ifnull(sum(d.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d where u.prefixid = d.prefixid and u.userid = d.userid and d.switch = 'Y') deposit_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='gift' ) gift_input_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='system' ) system_input_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount > 0 and s.behav!='bid_refund') input_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount > 0 and s.behav='bid_refund') bid_refund,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount < 0) output_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='user_deposit' ) spoints_by_deposit,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' ) spoint_total,
(SELECT ifnull(sum(t.total_count),0) FROM (SELECT h.userid, count(*) * p.saja_fee AS total_count FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
	LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON
		h.prefixid = p.prefixid
		AND h.productid = p.productid
		AND p.closed = 'N'
		AND p.switch = 'Y'
	WHERE
		h.prefixid = '".$this->config->default_prefix_id."'
		AND h.switch = 'Y'
		AND p.productid IS NOT NULL
		AND h.spointid != '0'
		AND h.scodeid = '0'
		AND p.closed = 'N'
		GROUP BY h.productid, h.userid, p.saja_fee
	)t
	where t.userid = u.userid
) freeze,
(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y') bonus_total,
(select ifnull(-sum(b1.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b1 where u.prefixid = b1.prefixid and u.userid = b1.userid
          and b1.behav in ('user_exchange', 'ibon_exchange','order_refund') and b1.switch = 'Y') bonus_overage,
(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
	WHERE s.prefixid = '".$this->config->default_prefix_id."'
		AND s.switch = 'Y'
		AND s.behav = 'process_fee'
	) t2
	where t2.userid = u.userid
) total_process_amount,
(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
	WHERE s.prefixid = '".$this->config->default_prefix_id."'
		AND s.switch = 'Y'
		AND s.behav = 'user_saja'
	) t2
	where t2.userid = u.userid
) total_bid_process_amount,
(select ifnull(sum(t1.total_count),0) from (SELECT h.userid, count(*) * p.saja_fee AS total_count FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp
	LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h ON
		h.prefixid = pgp.prefixid
		AND h.userid = pgp.userid
		AND h.productid = pgp.productid
		AND h.switch = 'Y'
	LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON
		h.prefixid = p.prefixid
		AND h.productid = p.productid
		AND p.closed = 'Y'
		AND p.switch = 'Y'
	WHERE pgp.prefixid = '".$this->config->default_prefix_id."'
		AND h.switch = 'Y'
		AND p.productid IS NOT NULL
		AND h.spointid != '0'
		AND p.closed = 'Y'
		AND h.type = 'bid'
	GROUP BY h.productid
	) t1
	where t1.userid = u.userid
) total_saja_fee,
(select ifnull(sum(o.total_fee),0) from `saja_exchange`.`saja_order` o where u.prefixid = o.prefixid and o.type='saja' and o.userid = u.userid and o.status in (0,1,2,3,4) and o.switch = 'Y') bid_price_total_all,
(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y' and b.amount > 0 and b.behav in ('product_close')) bonus_input_total,
(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y' and b.amount > 0 and b.behav in ('order_close')) bonus_input_total1,
(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y' and b.amount > 0 and b.behav in ('system')) bonus_input_total2
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	up.prefixid = u.prefixid
	and up.userid = u.userid
	and up.switch = 'Y'
WHERE
	u.prefixid = '".$this->config->default_prefix_id."'
";
*/
		$query = "
		SELECT
		u.userid, u.name, up.nickname, up.bonus_noexpw, u.switch,
		(select ifnull(sum(d.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d where u.prefixid = d.prefixid and u.userid = d.userid and d.switch = 'Y') deposit_total,
		(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='gift' ) gift_input_total,
		(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='system' ) system_input_total,
		(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount > 0 and s.behav!='bid_refund') input_total,
		(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount > 0 and s.behav='bid_refund') bid_refund,
		(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount < 0) output_total,
		(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='user_deposit' ) spoints_by_deposit,
		(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' ) spoint_total,
		(select ifnull(sum(sh.bid_total),0) 
		        from `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
				join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p
				  on sh.prefixid=p.prefixid
				 and sh.productid=p.productid 
				 and p.closed='N'
				 and sh.switch='Y'
				 and sh.type='bid'
				 and sh.spointid>0
			   where sh.userid=u.userid	) as freeze,
		(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y') bonus_total,
		(select ifnull(-sum(b1.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b1 where u.prefixid = b1.prefixid and u.userid = b1.userid
				  and b1.behav in ('user_exchange', 'ibon_exchange','order_refund') and b1.switch = 'Y') bonus_overage,
		(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
			WHERE s.prefixid = '".$this->config->default_prefix_id."'
				AND s.switch = 'Y'
				AND s.behav = 'process_fee'
			) t2
			where t2.userid = u.userid
		) total_process_amount,
		(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
			WHERE s.prefixid = '".$this->config->default_prefix_id."'
				AND s.switch = 'Y'
				AND s.behav = 'user_saja'
			) t2
			where t2.userid = u.userid
		) total_bid_process_amount,
		(select ifnull(sum(t1.total_count),0) from (
		  SELECT h.userid, count(*) * p.saja_fee AS total_count 
		    FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp
			LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h ON
				h.prefixid = pgp.prefixid
				AND h.userid = pgp.userid
				AND h.productid = pgp.productid
				AND h.switch = 'Y'
			LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON
				h.prefixid = p.prefixid
				AND h.productid = p.productid
				AND p.closed = 'Y'
				AND p.switch = 'Y'
			WHERE pgp.prefixid = '".$this->config->default_prefix_id."'
				AND h.switch = 'Y'
				AND p.productid IS NOT NULL
				AND h.spointid != '0'
				AND p.closed = 'Y'
				AND h.type = 'bid'
				AND pgp.userid IN (".$this->io->input["get"]["search_userid"].") 
			GROUP BY h.productid
			) t1
			where t1.userid = u.userid
		) total_saja_fee,
		(select ifnull(sum(o.total_fee),0) from `saja_exchange`.`saja_order` o where u.prefixid = o.prefixid and o.type='saja' and o.userid = u.userid and o.status in (0,1,2,3,4) and o.switch = 'Y') bid_price_total_all,
		(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y' and b.amount > 0 and b.behav in ('product_close')) bonus_input_total,
		(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y' and b.amount > 0 and b.behav in ('order_close')) bonus_input_total1,
		(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y' and b.amount > 0 and b.behav in ('system')) bonus_input_total2		
		FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
		   join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
			up.prefixid = u.prefixid
			and up.userid = u.userid
		WHERE
			u.prefixid = '".$this->config->default_prefix_id."'	";
// 先移除 switch=Y的限制
//	AND u.switch = 'Y'


$query .= $sub_search_query ;
$query .= "group by u.userid";
$query .= $sub_sort_query ;
$query .= $query_limit ;
$table = $this->model->getQueryRecord($query);
// Table Record End

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_account_manage',
	`active` = '會員帳戶管理清單查詢',
	`memo` = '{$report_data}',
	`root` = 'report_account_manage/view',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status["status"]["tester"] = array("1" => "否", "0" => "是");

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();