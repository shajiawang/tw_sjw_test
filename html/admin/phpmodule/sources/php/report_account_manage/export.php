<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];


/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1];
	}
}

$query_search = '';
if ($data['tester'] == 1) {
	$query_search .= ' AND u.`userid` not in (1, 3, 9, 28, 116, 507, 585, 586, 588)';
}
if (!(empty($data["search_userid"])))$query_search .= " and u.userid='".$data["search_userid"]."'";
if (!(empty($data["search_name"])))$query_search .= " and u.name like '%".$data["search_name"]."%'";
$query = "
SELECT
u.userid, u.name, up.nickname,
(select ifnull(sum(d.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d where u.prefixid = d.prefixid and u.userid = d.userid and d.switch = 'Y') deposit_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount > 0) input_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.amount < 0) output_total,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y' and s.behav='user_deposit' ) spoints_by_deposit,
(select ifnull(sum(s.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s where u.prefixid = s.prefixid and u.userid = s.userid and s.switch = 'Y') spoint_total,
(SELECT ifnull(sum(t.total_count),0) FROM (SELECT h.userid, count(*) * p.saja_fee AS total_count FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h
	LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON
		h.prefixid = p.prefixid
		AND h.productid = p.productid
		AND p.closed = 'N'
		AND p.switch = 'Y'
	WHERE
		h.prefixid = '".$this->config->default_prefix_id."'
		AND h.switch = 'Y'
		AND p.productid IS NOT NULL
		AND h.spointid != '0'
		AND h.scodeid = '0'
		AND p.closed = 'N'
		GROUP BY h.productid, h.userid, p.saja_fee
	)t
	where t.userid = u.userid
) freeze,
(select ifnull(sum(b.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b where u.prefixid = b.prefixid and u.userid = b.userid and b.switch = 'Y') bonus_total,
(select ifnull(-sum(b1.amount),0) from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b1 where u.prefixid = b1.prefixid and u.userid = b1.userid and b1.behav in ('user_exchange', 'order_refund') and b1.switch = 'Y') bonus_overage,
(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
	WHERE s.prefixid = '".$this->config->default_prefix_id."'
		AND s.switch = 'Y'
		AND s.behav = 'process_fee'
	) t2
	where t2.userid = u.userid
) total_process_amount,
(select ifnull(sum(t2.total_count),0) from (SELECT s.userid, s.amount AS total_count FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
	WHERE s.prefixid = '".$this->config->default_prefix_id."'
		AND s.switch = 'Y'
		AND s.behav = 'user_saja'
	) t2
	where t2.userid = u.userid
) total_bid_process_amount,
(select ifnull(sum(t1.total_count),0) from (SELECT h.userid, count(*) * p.saja_fee AS total_count FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp
	LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` h ON
		h.prefixid = pgp.prefixid
		AND h.userid = pgp.userid
		AND h.productid = pgp.productid
		AND h.switch = 'Y'
	LEFT JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p ON
		h.prefixid = p.prefixid
		AND h.productid = p.productid
		AND p.closed = 'Y'
		AND p.switch = 'Y'
	WHERE pgp.prefixid = '".$this->config->default_prefix_id."'
		AND h.switch = 'Y'
		AND p.productid IS NOT NULL
		AND h.spointid != '0'
		AND p.closed = 'Y'
		AND h.type = 'bid'
	GROUP BY h.productid
	) t1
	where t1.userid = u.userid
) total_saja_fee
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	up.prefixid = u.prefixid
	and up.userid = u.userid
	and up.switch = 'Y'
WHERE
	u.prefixid = '".$this->config->default_prefix_id."'
	AND u.switch = 'Y'
	{$query_search}
group by u.userid
ORDER BY u.userid";
$table = $this->model->getQueryRecord($query);

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Add some data
//print_r($_POST['report']);exit;
//會員資料表
$Report_Array[0]['title'] = array("A1" => "會員編號", "B1" => "會員帳號", "C1" => "會員暱稱", "D1" => "獲得殺幣總額", "E1" => "殺幣餘額", "F1" => "下標費用總額", "G1" => "結帳總額", "H1" => "鯊魚點數餘額", "I1" => "已使用鯊魚點數", "J1" => "凍結殺幣");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value);
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['userid']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$row, (string)$value['name'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['nickname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, rtrim(abs((int)$value['input_total']),'.'));
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, rtrim(abs((int)$value['spoint_total']),'.'));
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, rtrim(abs((int)$value['total_bid_process_amount']),'.'));
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, rtrim(abs((int)$value['total_process_amount']),'.'));
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, rtrim(abs((int)$value['bonus_total']),'.'));
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, rtrim(abs((int)$value['bonus_overage']),'.'));
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, rtrim(abs((int)$value['freeze']),'.'));
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('會員帳戶管理');

/*
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setCellValue("A3","test11");
$objPHPExcel->getActiveSheet()->setCellValue("B3","test22");
$objPHPExcel->getActiveSheet()->setCellValue("C3","test33");
$objPHPExcel->getActiveSheet()->setCellValue("D3","測試");

// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('Simple2');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);
*/
header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="會員帳戶管理'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');

// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>