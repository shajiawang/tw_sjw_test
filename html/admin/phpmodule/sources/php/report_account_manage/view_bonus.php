<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
//$db_user = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$sub_sort_query =  " ORDER BY b.insertt DESC";
// Sort End

// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if($this->io->input["get"]["userid"] != ''){
	$status["status"]["search"]["userid"] = $this->io->input["get"]["userid"] ;
	$status["status"]["search_path"] .= "&userid=".$this->io->input["get"]["userid"] ;
	$sub_search_query .= " AND b.`userid` ='".$this->io->input["get"]["userid"]."' ";
}
if($this->io->input["get"]["behav"] != ''){
	$status["status"]["search"]["behav"] = $this->io->input["get"]["behav"] ;
	$status["status"]["search_path"] .= "&behav=".$this->io->input["get"]["behav"] ;
	$sub_search_query .= " AND b.`behav` ='".$this->io->input["get"]["behav"]."' ";
}
if(!empty($this->io->input["get"]["joindatefrom"])){			//加入起始日期
	$status["status"]["search"]["joindatefrom"] = $this->io->input["get"]["joindatefrom"] ;
	$status["status"]["search_path"] .= "&joindatefrom=".$this->io->input["get"]["joindatefrom"] ;
	$sub_search_query .= "	AND b.`insertt` >= '".$this->io->input["get"]["joindatefrom"]."'";
}
if(!empty($this->io->input["get"]["joindateto"])){				//加入結束日期
	$status["status"]["search"]["joindateto"] = $this->io->input["get"]["joindateto"] ;
	$status["status"]["search_path"] .= "&joindateto=".$this->io->input["get"]["joindateto"] ;
	$sub_search_query .= "	AND b.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="
SELECT count(*) as num 
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b 
WHERE 
	b.`prefixid` = '{$this->config->default_prefix_id}' 
	AND b.`userid` = '{$this->io->input["get"]["userid"]}' 	
	AND b.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query = "
SELECT *
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."bonus` b 
WHERE 
	b.`prefixid` = '{$this->config->default_prefix_id}' 
	AND b.`userid` = '{$this->io->input["get"]["userid"]}' 	
	AND b.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################

/*
ibon_exchange :ibon 兌換, order_refund:缺貨退費, user_exchange:兌換商品, product_close:結標轉紅利, 
user_qrcode_tx:Qrcode 兌換,other_system_exchange:第三方兌換使用,system:系統/手動加入
*/
##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status["status"]["behav"] = array(
	"ibon_exchange" => "ibon 兌換", 
	"order_refund" => "缺貨退費", 
	"user_exchange" => "兌換商品", 
	"product_close" => "結標轉紅利",
	"user_qrcode_tx" => "Qrcode 兌換",
	"other_system_exchange" => "第三方兌換使用",
	"system" => "系統/手動加入",
	"system_test" => "測試用",
	"order_close" => "得標獲鯊魚點"
);

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();