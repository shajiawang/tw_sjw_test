<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
//$db_user = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$sub_sort_query =  " ORDER BY s.insertt DESC";
// Sort End

// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if($this->io->input["get"]["userid"] != ''){
	$status["status"]["search"]["userid"] = $this->io->input["get"]["userid"] ;
	$status["status"]["search_path"] .= "&userid=".$this->io->input["get"]["userid"] ;
	$sub_search_query .= " AND s.`userid` ='".$this->io->input["get"]["userid"]."' ";
}
if($this->io->input["get"]["behav"] != ''){
	$status["status"]["search"]["behav"] = $this->io->input["get"]["behav"] ;
	$status["status"]["search_path"] .= "&behav=".$this->io->input["get"]["behav"] ;
	$sub_search_query .= " AND s.`behav` ='".$this->io->input["get"]["behav"]."' ";
}
if(!empty($this->io->input["get"]["joindatefrom"])){			//加入起始日期
	$status["status"]["search"]["joindatefrom"] = $this->io->input["get"]["joindatefrom"] ;
	$status["status"]["search_path"] .= "&joindatefrom=".$this->io->input["get"]["joindatefrom"] ;
	$sub_search_query .= "	AND s.`insertt` >= '".$this->io->input["get"]["joindatefrom"]."'";
}
if(!empty($this->io->input["get"]["joindateto"])){				//加入結束日期
	$status["status"]["search"]["joindateto"] = $this->io->input["get"]["joindateto"] ;
	$status["status"]["search_path"] .= "&joindateto=".$this->io->input["get"]["joindateto"] ;
	$sub_search_query .= "	AND s.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="
SELECT count(*) as num 
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s 
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`userid` = '{$this->io->input["get"]["userid"]}' 	
	AND s.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query = "
SELECT *
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s 
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`userid` = '{$this->io->input["get"]["userid"]}' 	
	AND s.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################

/*
feedback:(推薦)回饋,prod_buy:商品購買,prod_sell:中標商品折算,
sajabonus:鯊魚點兌換,process_fee:中標處理費,user_saja:下標手續費,
bid_refund:流標退款,user_deposit:使用者儲值,bid_by_saja:中標送殺價幣,
gift :(儲值)贈送, system:系統轉入, system_test:系統測試,
other_system_use : 第三方抵扣, others : 其他
*/
##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_account_manage', 
	`active` = '會員帳戶殺價幣清單查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_account_manage/view_spoint', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status["status"]["behav"] = array(
	"feedback" => "回饋", 
	"prod_buy" => "商品購買", 
	"prod_sell" => "中標商品折算", 
	"sajabonus" => "鯊魚點兌換",
	"process_fee" => "中標處理費",
	"user_saja" => "下標手續費",
	"bid_refund" => "流標退款",
	"user_deposit" => "使用者儲值",
	"bid_by_saja" => "中標送殺價幣",
	"gift" => "(儲值)贈送",
	"system" => "系統轉入",
	"system_test" => "系統測試",
	"other_system_use" => "第三方抵扣",
	"others" => "其他"
);

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();