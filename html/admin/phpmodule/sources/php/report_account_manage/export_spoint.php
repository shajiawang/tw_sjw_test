<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];


$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1];
	}
}
// Sort Start
$sub_sort_query =  " ORDER BY s.insertt DESC";
// Sort End

// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if($data["userid"] != ''){
	$sub_search_query .= " AND s.`userid` ='".$data["userid"]."' ";
}
if($data["behav"] != ''){
	$sub_search_query .= " AND s.`behav` ='".$data["behav"]."' ";
}
if(!empty($data["joindatefrom"])){			//加入起始日期
	$sub_search_query .= "	AND s.`insertt` >= '".$data["joindatefrom"]."'";
}
if(!empty($data["joindateto"])){				//加入結束日期
	$sub_search_query .= "	AND s.`insertt` < '".$data["joindateto"]."'";
}
// Search End

$query = "
SELECT *
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s 
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$table = $this->model->getQueryRecord($query);


##############################################################################################################################################
// Log Start
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_account_manage',
	`active` = '會員帳戶管理清單殺價幣明細匯出',
	`memo` = '{$report_data}',
	`root` = 'report_account_manage/export_spoint',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Add some data
//print_r($_POST['report']);exit;
//會員資料表
$Report_Array[0]['title'] = array("A1" => "殺幣來源/用途", "B1" => "金額", "C1" => "新增時間", "D1" => "活動註記", "E1" => "備註說明");

    $arrDesc['behav']['feedback']='回饋';
	$arrDesc['behav']['prod_buy']='商品購買';
	$arrDesc['behav']['prod_sell']='中標商品折算';
	$arrDesc['behav']['sajabonus']='鯊魚點兌換';
	$arrDesc['behav']['process_fee']='中標處理費';
	$arrDesc['behav']['user_saja']='下標手續費';
	$arrDesc['behav']['bid_refund']='流標退款';
	$arrDesc['behav']['user_deposit']='使用者儲值';
	$arrDesc['behav']['bid_by_saja']='中標送殺價幣';
	$arrDesc['behav']['gift']='(儲值)贈送';
	$arrDesc['behav']['system']='系統轉入';
	$arrDesc['behav']['system_testv']='系統測試';
	$arrDesc['behav']['other_system_use']='第三方抵扣';
	$arrDesc['behav']['others']='其他';
	
	$arrDesc['remark']['0']='';
	$arrDesc['remark']['1']='老殺友回娘家';
	$arrDesc['remark']['2']='記者會活動';
	$arrDesc['remark']['3']='重機交車派對';
	$arrDesc['remark']['6']='手機驗證送';
	$arrDesc['remark']['8']='推薦送';
	
//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value);
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$row, (string)$arrDesc['behav'][$value['behav']],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, rtrim(abs((int)$value['amount']),'.'));
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['insertt'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row, (string)$arrDesc['remark'][$value['remark']],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['memo'],PHPExcel_Cell_DataType::TYPE_STRING);
	}
}
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('會員帳戶管理-殺價幣明細');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="會員帳戶管理-殺價幣明細-'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');

// Save Excel 2007 file
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>