<?php
// Check Variable Start
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員ID錯誤!!');
}
if (empty($this->io->input["post"]["countryid"])) {
	$this->jsAlertMsg('國別ID錯誤!!');
}
if (!is_numeric($this->io->input["post"]["amount"])) {
	$this->jsAlertMsg('禮券數量錯誤!!');
}
 
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}gift` SET 
	`userid` = '{$this->io->input["post"]["userid"]}', 
	`countryid` = '{$this->io->input["post"]["countryid"]}', 
	`amount` = '{$this->io->input["post"]["amount"]}', 
	`behav` = '{$this->io->input["post"]["behav"]}', 
	`prefixid`='{$this->config->default_prefix_id}', 
	`seq`='0', 
	`switch`='Y', 
	`insertt`=now()
" ; 
$this->model->query($query);

// Insert End
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));