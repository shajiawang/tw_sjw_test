<?php
if (empty($this->io->input["get"]["pushid"])) {
	$this->jsAlertMsg('推播編號錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 


##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT 
pm.*
FROM `{$db_channel}`.`{$this->config->default_prefix}push_msg` pm 
WHERE pm.pushid = '".$this->io->input["get"]["pushid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$push_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'push_msg_edit', 
	`active` = '一般推播修改', 
	`memo` = '{$push_data}', 
	`root` = 'push_msg/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"] ."pushid={$this->io->input["get"]["productid"]}";

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();