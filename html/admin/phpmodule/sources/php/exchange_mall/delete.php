<?php
// Check Variable Start
if(empty($this->io->input["get"]["emid"])) {
	$this->jsAlertMsg('兌換中心ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// database start

$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_mall` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `emid` = '{$this->io->input["get"]["emid"]}'
" ;
$this->model->query($query);

$query ="UPDATE `{$db_channel}`.`{$this->config->default_prefix}channel_exchange_mall_rt` 
SET switch = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `emid` = '{$this->io->input["get"]["emid"]}'
" ;
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_mall_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_mall_delete', 
	`active` = '兌換中心刪除', 
	`memo` = '{$exchange_mall_data}', 
	`root` = 'exchange_mall/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End

