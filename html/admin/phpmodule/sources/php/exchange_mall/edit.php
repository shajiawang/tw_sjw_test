<?php
// Check Variable Start
if (empty($this->io->input["get"]["emid"])) {
	$this->jsAlertMsg('兌換中心ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_mall`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `emid` = '{$this->io->input["get"]["emid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT c.*, cr.emid
FROM `{$db_channel}`.`{$this->config->default_prefix}channel` c 
LEFT OUTER JOIN `{$db_channel}`.`{$this->config->default_prefix}channel_exchange_mall_rt` cr ON 
	c.prefixid = cr.prefixid
	AND c.channelid = cr.channelid
	AND cr.emid = '{$this->io->input["get"]["emid"]}'
	AND cr.switch = 'Y'
WHERE 
c.prefixid = '{$this->config->default_prefix_id}' 
AND c.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_rt"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_mall_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_mall_edit', 
	`active` = '兌換中心修改', 
	`memo` = '{$exchange_mall_data}', 
	`root` = 'exchange_mall/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();