<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('兌換中心名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('兌換中心敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// INSERT Start

$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_mall` SET 
	`prefixid`='{$this->config->default_prefix_id}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`seq`='{$this->io->input["post"]["seq"]}', 
	`insertt`=now()
";
$this->model->query($query);
$emid = $this->model->_con->insert_id;

//Relation channel_exchange_mall_rt
if($this->io->input["post"]["channelid"])
{
	foreach($this->io->input["post"]["channelid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}channel_exchange_mall_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`channelid`='{$rv}',	
		`emid`='{$emid}',
		`seq`='".(($rk+1)*10)."', 
		`insertt`=NOW()
		"; 
		$this->model->query($query);
	}
}

// INSERT End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_mall_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_mall_insert', 
	`active` = '兌換中心新增寫入', 
	`memo` = '{$exchange_mall_data}', 
	`root` = 'exchange_mall/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));