<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('兌換中心名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('兌換中心敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
if (!$this->io->input["post"]["channelid"]) {
	$this->jsAlertMsg('頻道ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_mall` SET
	`prefixid`='{$this->config->default_prefix_id}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`seq`='{$this->io->input["post"]["seq"]}' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `emid` = '{$this->io->input["post"]["emid"]}'
" ;
$this->model->query($query);


//Relation channel_exchange_mall_rt

$query = "UPDATE `{$db_channel}`.`{$this->config->default_prefix}channel_exchange_mall_rt` 
SET `switch` = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `emid` = '{$this->io->input["post"]["emid"]}'
";
$this->model->query($query); 

if($this->io->input["post"]["channelid"])
{
	foreach($this->io->input["post"]["channelid"] as $rk => $rv) { 
		$query = "INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}channel_exchange_mall_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`channelid`='{$rv}',
		`emid`='{$this->io->input["post"]["emid"]}',
		`seq`='".(($rk+1)*10)."', 
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query); 
	}
}

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_mall_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_mall_update', 
	`active` = '兌換中心修改寫入', 
	`memo` = '{$exchange_mall_data}', 
	`root` = 'exchange_mall/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));