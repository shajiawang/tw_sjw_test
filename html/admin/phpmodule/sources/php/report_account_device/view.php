<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
//$db_user = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$sub_sort_query =  " ORDER BY u.userid DESC";
// Sort End



// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  " AND u.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query = " 
SELECT count(*) as num 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_device` u 
WHERE 
	u.switch = 'Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 



// Table Record Start
$query = " 
SELECT COUNT(`userid`) AS user_count, sum(IF(`android` IS NOT NULL, 1, 0)) AS android_count, sum(IF(`ios` IS NOT NULL, 1, 0)) AS ios_count, sum(IF(`web` IS NOT NULL, 1, 0)) AS web_count 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_device` u 
WHERE u.switch = 'Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status["status"]["tester"] = array("1" => "否", "0" => "是");

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();