<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('商品分類名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('商品分類敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('商品分類排序錯誤!!');
}

$query ="
INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category`(
	`prefixid`,
	`ptype`,
	`node`,
	`layer`,
	`name`,
	`description`, 
	`seq`, 
	`switch`,
	`insertt`
) 
VALUES(
	'".$this->config->default_prefix_id."',
	'".$this->io->input["post"]["ptype"]."',
	'".$this->io->input["post"]["node"]."',
	'".$this->io->input["post"]["layer"]."',
	'".$this->io->input["post"]["name"]."',
	'".$this->io->input["post"]["description"]."',
	'".$this->io->input["post"]["seq"]."',
	'".$this->io->input["post"]["switch"]."',
	now()
)
" ;
//echo $query;exit;
$this->model->query($query);

$redis = new Redis();
$redis->connect('sajacache01',6379);
$redis->del("product_category_list");
$redis->del("dream_product_category_list");


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));