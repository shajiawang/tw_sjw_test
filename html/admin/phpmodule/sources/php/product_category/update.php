<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["pcid"])) {
	$this->jsAlertMsg('商品分類ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('商品分類名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('商品分類敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('商品分類排序錯誤!!');
}

$query ="
UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category`
SET
`ptype` = '".$this->io->input["post"]["ptype"]."',
`node` = '".$this->io->input["post"]["node"]."',
`layer` = '".$this->io->input["post"]["layer"]."',
`name` = '".$this->io->input["post"]["name"]."',
`description` = '".$this->io->input["post"]["description"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND pcid = '".$this->io->input["post"]["pcid"]."'
" ;
$this->model->query($query);

$redis = new Redis();
$redis->connect('sajacache01',6379);
$redis->del("product_category_list");
$redis->del("dream_product_category_list");


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));