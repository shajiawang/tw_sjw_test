<?php
if (empty($this->io->input["get"]["storeid"])) {
	$this->jsAlertMsg('商店ID錯誤!!');
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT 
*
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store`
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND storeid = '".$this->io->input["get"]["storeid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
c.*, cs.storeid
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` c 
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	c.prefixid = cs.prefixid
	AND c.channelid = cs.channelid
	AND cs.storeid = '".$this->io->input["get"]["storeid"]."'
	AND cs.switch = 'Y'
WHERE 
c.prefixid = '".$this->config->default_prefix_id."'
AND c.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_store_rt"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################



$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();