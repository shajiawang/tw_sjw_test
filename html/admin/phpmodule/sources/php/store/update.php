<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["storeid"])) {
	$this->jsAlertMsg('商店ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('商店名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('商店敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('商店排序錯誤!!');
}
if (!is_array($this->io->input["post"]["channelid"]) || empty($this->io->input["post"]["channelid"])) {
	$this->jsAlertMsg('經銷商錯誤!!');
}

$query ="
UPDATE `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store`
SET
`name` = '".$this->io->input["post"]["name"]."',
`description` = '".$this->io->input["post"]["description"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND storeid = '".$this->io->input["post"]["storeid"]."'
" ;
$this->model->query($query);

$query = "
DELETE 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt`
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND storeid = '".$this->io->input["post"]["storeid"]."'
";
$this->model->query($query);

$values = array();
foreach($this->io->input["post"]["channelid"] as $rk => $rv) {
	$values[] = "
		('".$this->config->default_prefix_id."', '".$this->io->input["post"]["storeid"]."', '".$rv."', '".(($rk+1)*10)."', NOW())";
}
$query = "
INSERT INTO `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt`(
	`prefixid`,
	`storeid`,
	`channelid`,
	`seq`, 
	`insertt`
)
VALUES
".implode(',', $values); 
//echo $query;exit;
$this->model->query($query);

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));