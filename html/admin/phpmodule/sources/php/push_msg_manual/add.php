<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user` 
WHERE 
	prefixid = '{$this->config->default_prefix_id}' 
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["live_broadcast_user"] = $recArr['table']['record'];

$query3 = "SELECT productid,name From  `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product`  WHERE
					prefixid = '{$this->config->default_prefix_id}' 
					AND unix_timestamp( offtime ) >0 
					AND unix_timestamp() >= unix_timestamp( ontime ) 
					AND unix_timestamp() <= unix_timestamp( offtime ) 
					AND switch = 'Y'
			";
$recArr3 = $this->model->getQueryRecord($query3);
$table["table"]["rt"]["valid_product"]=$recArr3['table']['record'];

$query3 = "SELECT groupset From  `{$db_channel}`.`{$this->config->default_prefix}push_msg` where groupset<>'' group by groupset";
$recArr3 = $this->model->getQueryRecord($query3);
$table["table"]["rt"]["groupsets"]=$recArr3['table']['record'];
##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$push_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'push_msg_manual_add', 
	`active` = '推播排程新增', 
	`memo` = '{$push_data}', 
	`root` = 'push_msg_manual/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
// $this->tplVar('table' , $table['table']) ;
// $this->tplVar('status',$status["status"]);
$this->display();