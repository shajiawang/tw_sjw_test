<?php
// Check Variable Start
if (empty($this->io->input["get"]["priid"])) {
	$this->jsAlertMsg('活動規則項目ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `priid` = '{$this->io->input["get"]["priid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

//Relation promote_rule
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}promote_rule`
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["promote_rule"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$promote_rule_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'promote_rule_item_edit', 
	`active` = '活動規則項目修改', 
	`memo` = '{$promote_rule_data}', 
	`root` = 'promote_rule_item/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();