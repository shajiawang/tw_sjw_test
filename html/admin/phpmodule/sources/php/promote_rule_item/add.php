<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT p.*
FROM `{$db_exchange}`.`{$this->config->default_prefix}promote_rule` p 
WHERE 
p.prefixid = '{$this->config->default_prefix_id}'
AND p.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["promote_rule"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$promote_rule_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'promote_rule_item_add', 
	`active` = '活動規則項目新增', 
	`memo` = '{$promote_rule_data}', 
	`root` = 'promote_rule_item/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();