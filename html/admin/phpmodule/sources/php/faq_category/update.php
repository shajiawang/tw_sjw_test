<?php
// Check Variable Start
if (empty($this->io->input["post"]["fcid"])) {
	$this->jsAlertMsg('分類ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('分類標題錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('分類敘述錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
$query ="
UPDATE `{$db_user}`.`{$this->config->default_prefix}faq_category` SET 
	`name` = '{$this->io->input["post"]["name"]}', 
	`description` = '{$this->io->input["post"]["description"]}', 
	`seq` = '{$this->io->input["post"]["seq"]}'
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `fcid` = '{$this->io->input["post"]["fcid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$faq_category_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'faq_category_update', 
	`active` = '新手教學分類修改寫入', 
	`memo` = '{$faq_category_data}', 
	`root` = 'faq_category/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));