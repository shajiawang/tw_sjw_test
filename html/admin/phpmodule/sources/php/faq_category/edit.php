<?php
// Check Variable Start
if (empty($this->io->input["get"]["fcid"])) {
	$this->jsAlertMsg('分類ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_user}`.`{$this->config->default_prefix}faq_category`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `fcid` = '{$this->io->input["get"]["fcid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$faq_category_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'faq_category_edit', 
	`active` = '新手教學分類修改', 
	`memo` = '{$faq_category_data}', 
	`root` = 'faq_category/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();