<?php
$salid=$this->io->input["get"]["salid"];
if (empty($salid)) {
	$salid=$this->io->input["post"]["salid"];
}
if (empty($salid)) {
	$this->jsAlertMsg('折讓單序號錯誤 !!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

##############################################################################################################################################
// Table Start 
$query = " SELECT salid, allowance_no, userid, allowance_date, invoice_buyer_id, invoice_buyer_name, allowance_amt, allowance_tax_amt, allowance_type, upload, 
                  allowance_for, objid, memo, switch, insertt, modifyt                
             FROM `saja_cash_flow`.`saja_allowance`
            WHERE 1=1
              AND salid='{$salid}' ";

$table = $this->model->getQueryRecord($query);
// Table End

// Table Content Start
$query = " SELECT saliid, salid, seq_no, ori_invoice_date, ori_invoiceno, unitprice, quantity, amount, tax, tax_type, switch, insertt, modifyt 
             FROM `saja_cash_flow`.`saja_allowance_items` 
            WHERE 1=1
              AND salid='{$salid}' ";
             
$details = $this->model->getQueryRecord($query);
// Table Content end 
if($table['table']['record'][0]['salid']>0 && count($details['table']['record'])>0) {
   foreach( $details['table']['record'] as $item) {
       $table['table']['record'][0]['items'][]=$item;
   }      
}
// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$allowance_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'allowance_edit', 
	`active` = '折讓修改', 
	`memo` = '{$allowance_data}', 
	`root` = 'allowance/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

error_log(json_encode($table['table']['record'][0]));
$this->tplVar('table',$table["table"]);
$this->display();
