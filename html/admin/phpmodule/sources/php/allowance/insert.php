<?php
$userid=$this->io->input["post"]["userid"];
if (empty($userid)) {
	$userid=$this->io->input["get"]["userid"];
}
if(empty($userid)) {
   $this->jsAlertMsg('缺用戶編號 !!');
    return;    
}

$allowance_amt=$this->io->input["post"]["allowance_amt"];
if (empty($allowance_amt)) {
	$allowance_amt=$this->io->input["get"]["allowance_amt"];
}
if(empty($allowance_amt)) {
   $this->jsAlertMsg('缺折讓金額 !!');
   return;    
}
if($allowance_amt<=0){
   $this->jsAlertMsg('折讓金額錯誤 !!');
   return;
}

if(round($allowance_amt) != $allowance_amt){
   $this->jsAlertMsg('折讓金額不可為小數 !!');
   return;
}

// 開立折讓
// $ret = mkSalesAllowance($userid, $amount);

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 
include_once "saja/mysql.ini.php";
include_once "/var/www/html/admin/libs/helpers.php";
$ip = GetIP();

$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$config = $this->config;
// error_log("[admin]config : ".json_encode($config));
$db = new mysql($config->db[0]);
$db->connect();

// 取得用戶有折讓中的發票 (依發票時間 asc & 可折讓金額 asc 排列)
$invoice_list_in_allowance = getInvoiceListInAllowance($db, $userid, 'S');
if($invoice_list_in_allowance){
   // error_log("invoice_list_in_allowance : ".json_encode($invoice_list_in_allowance));
   foreach($invoice_list_in_allowance as $item){
      $invoice_list[]=$item;
   }
}

// 取得用戶未折讓的發票, (依發票時間 asc & 可折讓金額 asc 排列)
$invoice_list_no_allowance  = getInvoiceListNotInAllowance($db, $userid, 'S');
if($invoice_list_no_allowance){
   // error_log("invoice_list_no_allowance : ".json_encode($invoice_list_no_allowance));
   foreach($invoice_list_no_allowance as $item){
     $invoice_list[]=$item;
   }
}

// 保留次序  將兩批資料合併(已折讓過的發票在前面)
if(!$invoice_list || count($invoice_list)<1){
   $this->jsAlertMsg('該用戶無發票可折讓 !!');
   return;
}

$amount = $allowance_amt;
$arrAllowanceItem=array();
foreach($invoice_list as $item){
  if($amount>0){
    $allowanceItem=array();
    $allowanceItem['tax_type']='1';
    $allowanceItem['ori_invoice_date']=$item['invoice_datetime'];
    $allowanceItem['ori_invoiceno']=$item['invoiceno'];
    // 折讓明細目前只有一項  先寫死
    $allowanceItem['quantity']=1;
    $allowanceItem['ori_description']=$item['description'];
    // 發票剩餘可折讓金額
    $allowable_allowance_amt = $item['total_amt']-$item['allowance_amt'];
    if($allowable_allowance_amt<=0)
       continue;
    if($amount>=$allowable_allowance_amt){
      // 當折讓金額>=發票可折讓金額時 發票全部折讓掉
      $item['allowance_amt']=$allowable_allowance_amt;
      $allowanceItem['amount']=$allowable_allowance_amt;
      $allowanceItem['tax']=round($allowable_allowance_amt*$item['tax_rate']);
      $allowanceItem['unitprice']=$allowable_allowance_amt;
      $amount-=$allowable_allowance_amt;
    }else if($amount<$allowable_allowance_amt){
      // 當 剩餘折讓金額<發票可折讓金額時 發票僅部分折讓
      $item['allowance_amt']=$amount;
      $allowanceItem['amount']=$amount;
      $allowanceItem['unitprice']=$amount;
      $allowanceItem['tax']=round($amount*$item['tax_rate']);
      $allowanceItem['unitprice']=$amount;
      $amount=0;
    }
    array_push($arrAllowanceItem,$allowanceItem);
  }
}

if($amount>0){
   $this->jsAlertMsg('可折讓金額不足 !!');
   return;
}  

// 統計折讓稅額
$allowance_tax_amt = 0;
foreach($arrAllowanceItem as $item){
   $allowance_tax_amt+=$item['tax'];
} 
error_log("arrAllowanceItem : ".json_encode($arrAllowanceItem));

// 新增allowance主檔
$arrAllowance = array();
$arrAllowance['allowance_no']=time();
$arrAllowance['userid']=$userid;
$arrAllowance['allowance_date']=date('Ymd');
$arrAllowance['invoice_buyer_id']='00000000';
$arrAllowance['invoice_buyer_name']=$userid;
$arrAllowance['allowance_amt']=$allowance_amt;
$arrAllowance['allowance_tax_amt']=$allowance_tax_amt;

// 1:買方開立折讓證明單
// 2:賣方開立折讓證明通知單
$arrAllowance['allowance_type']='2';
$salid = createAllowance($db, $arrAllowance);
if($salid){
  // 新增allowance明細
  $arrAllowance['salid']= $salid;
  $seq_no=1;
  foreach($arrAllowanceItem as $item){
    $item['salid']=$salid;
    $item['seq_no']=str_pad($seq_no,3,"0",STR_PAD_LEFT);
    $saliid = createAllowanceItem($db, $item);
    if($saliid>0){
      ++$seq_no;
      // 修改發票資料的allowance_amt
      $invoiceData = getInvoiceByNo($db, $item['ori_invoiceno']);
      if($invoiceData){
        $addAmt = $invoiceData['allowance_amt']+$item['amount'];
        updateInvoice($db, array("allowance_amt"=>$addAmt), array("invoiceno"=>$item['ori_invoiceno']));
      }
    }
  }
  $ret['retCode']=1;
  $ret['retMsg']='OK';
  $arrAllowance['items']=$arrAllowanceItem;
  $ret['retObj']=$arrAllowance;
}



##############################################################################################################################################
// Log Start 
$allowance_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'allowance_insert', 
	`active` = '折讓新增寫入', 
	`memo` = '{$allowance_data}', 
	`root` = 'allowance/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// error_log("[admin/allowance] allowance : ".json_encode($arrAllowance));
$this->tplVar('result' , $ret) ;
$this->tplVar('status',$status["status"]);
$this->display();

// functions begin ============================================================================== 
/*
function postReq($url='', $arrPost='') {
       $ret = array();                     
       if(empty($url)) {
          $ret['retCode']=-1;
          $ret['retMsg']="NO_URL";
          return $ret;    
       }
        if(empty($postdata)) {
          $ret['retCode']=-2;
          $ret['retMsg']="NO_DATA";
          return $ret;    
       }
       error_log("[admin/allowance/postReq] postdata : ".json_encode($arrPost));
       $stream_options = array(
            'http' => array (
                    'method' => "POST",
                    'content' => json_encode($arrPost),
                    'header' => "Content-Type:application/json" 
            ) 
        );
        $context  = stream_context_create($stream_options);
        // 送出json內容並取回結果
        $response = file_get_contents($url, false, $context);
        error_log("[admin/libs/helpers] response : ".$response);
    
        // 讀取json內容
        $arr = json_decode($response,true);
        return $arr;            
}
  
// 開立折讓
public function mkSalesAllowance($userid, $amount) {
    global $db, $config;

    $arrPost=array();
    $arrPost['userid']=$userid;
    $arrPost['amount']=$amount;
    $arrPost['json']='Y';
    $ret = postReq("https://www.saja.com.tw/site/invoice/createAllowance/",$arrPost);
    error_log("[admin/allowance/mkSalesAllowance] ret : ".json_encode($ret));
    return $ret;
}
*/
/*
// 取得用戶折讓中發票
function getInvoiceListInAllowance($db, $userid, $utype='S') {
    if(empty($userid))
       return false;
    // error_log("[getInvoiceListInAllowance] config : ".json_encode($config));
    // 在allowance_items資料中編號有出現的發票, 並以可折讓金額小到大, 日期舊到新排列
    $query = "SELECT * FROM `saja_cash_flow`.`saja_invoice` 
              WHERE prefixid ='saja' 
               AND userid = '{$userid}' 
               AND invoiceno IN (
                      SELECT ai.ori_invoiceno  
                        FROM `saja_cash_flow`.`saja_allowance` a
                        JOIN `saja_cash_flow`.`saja_allowance_items` ai
                          ON a.salid = ai.salid
                       WHERE 1=1
                         AND a.switch='Y' 
                         AND ai.switch='Y'
                         AND a.userid='{$userid}'
               )
            AND allowance_amt>0 ";
    if(!empty($utype)) {
       $query.= " AND utype='{$utype}' ";
    }	
    $query.=" ORDER BY (allowance_amt-sales_amt) asc, invoice_datetime asc,  siid asc ";

    // error_log("[getInvoiceListInAllowance] query : ".$query);
    // 取得資料
    $table = $db->getQueryRecord($query);

    if(!empty($table['table']['record'])) {
       return $table['table']['record'];
    } else {
       return false;
    }
}
    
// 取得用戶未折讓過的發票
function getInvoiceListNotInAllowance($db, $userid, $utype='S') {
    if(empty($userid)) {
       return false;
    }   
     // 在allowance_items資料中編號沒有出現的發票, 並以日期舊到新, 及可折讓金額小到大排列
    $query = " SELECT * FROM `saja_cash_flow`.`saja_invoice` 
                WHERE prefixid ='saja' 
                  AND userid = '{$userid}' 
                  AND invoiceno NOT IN 
                 (
                     SELECT ai.ori_invoiceno  
                        FROM `saja_cash_flow`.`saja_allowance` a
                        JOIN `saja_cash_flow`.`saja_allowance_items` ai
                          ON a.salid = ai.salid
                       WHERE 1=1
                         AND a.switch='Y' 
                         AND ai.switch='Y'
                         AND ai.ori_invoiceno!=''
                         AND a.userid='{$userid}'
                  ) ";

    if(!empty($utype)) {
       $query.= " AND utype='${utype}' ";
    }
    $query.=" ORDER BY invoice_datetime ASC, (allowance_amt-sales_amt) asc, siid asc ";         
    //取得資料
    // error_log("[getInvoiceListInAllowance] query : ".$query);
    $table = $db->getQueryRecord($query);
    if(!empty($table['table']['record'])) {
       return $table['table']['record'];
    } else {
       return false;
    }
}


// 新增折讓單主檔資料
function createAllowance($db, $arrNew='') {
    if(empty($arrNew) || !is_array($arrNew)){
        return false;
    }
    $allowance_no=time();
    
    $query = "INSERT INTO `saja_cash_flow`.`saja_allowance`
        SET 
        userid='{$arrNew['userid']}',
        allowance_no='${allowance_no}',
        allowance_date='{$arrNew['allowance_date']}',
        invoice_buyer_id='{$arrNew['invoice_buyer_id']}',
        invoice_buyer_name='{$arrNew['invoice_buyer_name']}',
        allowance_amt=${arrNew['allowance_amt']},
        allowance_tax_amt=${arrNew['allowance_tax_amt']},
        allowance_type='${arrNew['allowance_type']}',
        insertt=NOW()
    ";
    $db->query($query);
    $salid = $db->_con->insert_id;
    error_log("[admin/insert/createAllowance] salid : ".$salid);
    return $salid;
}

// 新增折讓單明細資料
function createAllowanceItem($db, $arrNew='') {
    if(empty($arrNew) || !is_array($arrNew))
        return false;
    $query = "INSERT INTO `saja_cash_flow`.`saja_allowance_items`
        SET 
        salid='{$arrNew['salid']}',
        seq_no='{$arrNew['seq_no']}',
        ori_invoice_date='{$arrNew['ori_invoice_date']}',
        ori_invoiceno='{$arrNew['ori_invoiceno']}',
        unitprice={$arrNew['unitprice']},
        quantity=${arrNew['quantity']},
        amount=${arrNew['amount']},
        tax=${arrNew['tax']},
        tax_type='${arrNew['tax_type']}',
        insertt=NOW()
    ";
    $db->query($query);
    $saliid = $db->_con->insert_id;
    error_log("[admin/insert/createAllowanceItem] saliid : ".$saliid);
    return $saliid;
}

function getInvoiceByNo($db, $invoiceno, $utype='') {
        
		$query = " SELECT * 
		             FROM `saja_cash_flow`.`saja_invoice`
		            WHERE prefixid = 'saja' 
			          AND switch = 'Y' ";
        if(!empty($invoiceno)) {
           $query.= " AND invoiceno = '{$invoiceno}' ";  
        } 
        if(!empty($type)) {
           $query.= " AND utype='{$utype}' ";  
        }    
            
		//取得資料
		$table = $db->getQueryRecord($query);
		
		// error_log("[admin/allowance/insert/getInvoiceByNo] query :".$query);
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }

function updateInvoice($db, $arrUpd, $arrCond) {
		
        if(empty($arrCond) || count($arrCond)<1) {
           return false;   
        }
        if(empty($arrUpd) || count($arrUpd)<1) {
           return false;   
        }
        $query = "UPDATE `saja_cash_flow`.`saja_invoice` SET ";
        foreach($arrUpd as $k=>$v) {
            $query.= " ".$k."='".$v."',";      
        }
        $query = substr($query,0,-1);
        $query.=" WHERE prefixid='saja' ";
        
        foreach($arrCond as $k=>$v) {
            $query.= " AND ".$k."='".$v."' ";      
        }
        $ret =  $db->query($query);
        error_log("[admin/allowance/insert/updateInvoice] : ".$query."-->".$ret);
        return $ret;
    }
// functions end ==========================================================================================
*/
// $status["status"]["base_href"] = $status["status"]["path"] ."productid={$this->io->input["get"]["productid"]}";



  
  