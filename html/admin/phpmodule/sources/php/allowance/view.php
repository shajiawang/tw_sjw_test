<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	// 調整排序欄位請修改下列Modify here to assign sort columns 
	if (preg_match("/^sort_(allowance_no|sailid|invoice_buyer_name|allowance_amt|insertt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY a.insertt DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = " a.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
// $category_search_query = "";
if(!empty($this->io->input["get"]["search_salid"])){
	$status["status"]["search"]["search_salid"] = $this->io->input["get"]["search_salid"] ;
	$status["status"]["search_path"] .= "&search_salid=".$this->io->input["get"]["search_salid"] ;
	$sub_search_query .=  "	AND a.`salid` = '".strtoupper($this->io->input["get"]["search_salid"])."' ";
}
if(!empty($this->io->input["get"]["search_allowance_no"])){
	$status["status"]["search"]["search_allowance_no"] = $this->io->input["get"]["search_allowance_no"] ;
	$status["status"]["search_path"] .= "&search_allowance_no=".$this->io->input["get"]["search_allowance_no"] ;
	$sub_search_query .=  "	AND a.`allowance_no` = '".$this->io->input["get"]["search_allowance_no"]."' ";
}
if(!empty($this->io->input["get"]["search_userid"])){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "	AND a.`userid` = '".$this->io->input["get"]["search_userid"]."' ";
}
if(!empty($this->io->input["get"]["search_objid"])){
	$status["status"]["search"]["search_objid"] = $this->io->input["get"]["search_objid"] ;
	$status["status"]["search_path"] .= "&search_objid=".$this->io->input["get"]["search_objid"] ;
	$sub_search_query .=  "	AND a.`objid` = '".$this->io->input["get"]["search_objid"]."' ";
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	$sub_search_query .=  " AND a.`allowance_date` BETWEEN '".$this->io->input["get"]["search_btime"]."' AND '".$this->io->input["get"]["search_etime"]."' ";
}
if(!empty($this->io->input["get"]["search_upload"])){
	$status["status"]["search"]["search_upload"] = $this->io->input["get"]["search_upload"] ;
	$status["status"]["search_path"] .= "&search_upload=".$this->io->input["get"]["search_upload"] ;
	$sub_search_query .=  "	AND a.`upload` = '".$this->io->input["get"]["search_upload"]."' ";
}
if(!empty($this->io->input["get"]["search_switch"])){
	$status["status"]["search"]["search_switch"] = $this->io->input["get"]["search_switch"] ;
	$status["status"]["search_path"] .= "&search_switch=".$this->io->input["get"]["search_switch"] ;
	$sub_search_query .=  "	AND a.`switch` = '".$this->io->input["get"]["search_switch"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query =" SELECT 
count(*) as num
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."allowance` a 
WHERE 1=1
AND a.allowance_no IS NOT NULL " ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query =" SELECT a.*, (SELECT nickname FROM saja_user.saja_user_profile WHERE userid=a.userid) as user_name
			FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."allowance` a
		   WHERE 1=1 " ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 

error_log("[admin/allowance/view] : ".$query);
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$allowance_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'allowance', 
	`active` = '折讓清單查詢', 
	`memo` = '{$allowance_data}', 
	`root` = 'allowance/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();