<?php
   
    $arrAllowanceFor=array("O"=>"訂單");

	function getValue($io, $key, $replace_v='') {
		 $v=$io->input["post"][$key];
		 if(empty($v)) {
			$v= $io->input["get"][$key];
		 }
		 if(empty($v)) {
			return $replace_v; 
		 } 
		 error_log("[getValue] ".$key."=>".htmlspecialchars($v));
		 return htmlspecialchars(trim($v));
    }

	// 折讓相關功能抽出
    // 取得用戶折讓中發票
    function getInvoiceListInAllowance($db, $userid, $utype='S') {
        if(empty($userid))
           return false;
        
        // 在allowance_items資料中編號有出現的發票, 並以可折讓金額小到大, 日期舊到新排列
        $query = "SELECT * FROM `saja_cash_flow`.`saja_invoice` 
                  WHERE prefixid ='saja' 
                   AND userid = '{$userid}' 
				   AND is_void = 'N'
				   AND utype = 'S'
				   AND upload!='N' 	
                   AND invoiceno IN (
                          SELECT ai.ori_invoiceno  
                            FROM `saja_cash_flow`.`saja_allowance` a
                            JOIN `saja_cash_flow`.`saja_allowance_items` ai
                              ON a.salid = ai.salid
                           WHERE 1=1
                             AND a.switch='Y' 
                             AND ai.switch='Y'
                             AND a.userid='{$userid}'
                   )
                AND allowance_amt>0 ";
        if(!empty($utype)) {
           $query.= " AND utype='{$utype}' ";
        }	
        $query.=" ORDER BY (allowance_amt-sales_amt) asc, invoice_datetime asc,  siid asc ";

        error_log("[getInvoiceListInAllowance] query : ".$query);
        // 取得資料
        $table = $db->getQueryRecord($query);

        if(!empty($table['table']['record'])) {
           return $table['table']['record'];
        } else {
           return false;
        }
    }
    
    // 取得用戶未折讓過的發票
    function getInvoiceListNotInAllowance($db, $userid, $utype='S') {
        if(empty($userid)) {
           return false;
        } 
        error_log("[getInvoiceListNotInAllowance] ${userid}, ${utype} ");        
         // 在allowance_items資料中編號沒有出現的發票, 並以日期舊到新, 及可折讓金額小到大排列
        $query = " SELECT * FROM `saja_cash_flow`.`saja_invoice` 
                    WHERE prefixid ='saja' 
                      AND userid = '{$userid}'
                      AND is_void = 'N'	
                      AND utype = 'S'
                      AND upload!='N' 					  
                      AND invoiceno NOT IN 
                     (
                         SELECT ai.ori_invoiceno  
                            FROM `saja_cash_flow`.`saja_allowance` a
                            JOIN `saja_cash_flow`.`saja_allowance_items` ai
                              ON a.salid = ai.salid
                           WHERE 1=1
                             AND a.switch='Y' 
                             AND ai.switch='Y'
                             AND ai.ori_invoiceno!=''
                             AND a.userid='{$userid}'
                      ) ";

        if(!empty($utype)) {
           $query.= " AND utype='${utype}' ";
        }
        $query.=" ORDER BY invoice_datetime ASC, (allowance_amt-sales_amt) asc, siid asc ";         
        //取得資料
        error_log("[getInvoiceListInAllowance] query : ".$query);
        $table = $db->getQueryRecord($query);
        if(!empty($table['table']['record'])) {
           return $table['table']['record'];
        } else {
           return false;
        }
    }

    // 新增折讓單主檔資料
    function createAllowance($db, $arrNew='') {
        if(empty($arrNew) || !is_array($arrNew)){
            return false;
        }
        $allowance_no=time();
        
        $query = "INSERT INTO `saja_cash_flow`.`saja_allowance`
            SET 
            userid='{$arrNew['userid']}',
            allowance_no='${allowance_no}',
            allowance_date='{$arrNew['allowance_date']}',
            invoice_buyer_id='{$arrNew['invoice_buyer_id']}',
            invoice_buyer_name='{$arrNew['invoice_buyer_name']}',
            allowance_amt=${arrNew['allowance_amt']},
            allowance_tax_amt=${arrNew['allowance_tax_amt']},
            allowance_type='${arrNew['allowance_type']}',
			allowance_for='${arrNew['allowance_for']}',
			objid='${arrNew['objid']}',
			memo='${arrNew['memo']}',
            insertt=NOW()
        ";
        $db->query($query);
        $salid = $db->_con->insert_id;
        error_log("[allowance/funcitons/createAllowance] salid : ".$salid);
        return $salid;
    }

    // 新增折讓單明細資料
    function createAllowanceItem($db, $arrNew='') {
        if(empty($arrNew) || !is_array($arrNew))
            return false;
        $query = "INSERT INTO `saja_cash_flow`.`saja_allowance_items`
            SET 
            salid='{$arrNew['salid']}',
            seq_no='{$arrNew['seq_no']}',
            ori_invoice_date='{$arrNew['ori_invoice_date']}',
            ori_invoiceno='{$arrNew['ori_invoiceno']}',
            unitprice={$arrNew['unitprice']},
            quantity=${arrNew['quantity']},
            amount=${arrNew['amount']},
            tax=${arrNew['tax']},
            tax_type='${arrNew['tax_type']}',
            insertt=NOW()
        ";
        $db->query($query);
        $saliid = $db->_con->insert_id;
        error_log("[allowance/funcitons/createAllowanceItem] saliid : ".$saliid);
        return $saliid;
    }

    function getInvoiceByNo($db, $invoiceno, $utype='') {
        
		$query = " SELECT * 
		             FROM `saja_cash_flow`.`saja_invoice`
		            WHERE prefixid = 'saja' 
			          AND switch = 'Y' ";
        if(!empty($invoiceno)) {
           $query.= " AND invoiceno = '{$invoiceno}' ";  
        } 
        if(!empty($type)) {
           $query.= " AND utype='{$utype}' ";  
        }    
            
		//取得資料
		$table = $db->getQueryRecord($query);
		
		// error_log("[allowance/funcitons/getInvoiceByNo] query :".$query);
		if(!empty($table['table']['record'][0])) {
			return $table['table']['record'][0];
		}
		return false;
    }

    function updateInvoice($db, $arrUpd, $arrCond) {
		
        if(empty($arrCond) || count($arrCond)<1) {
           return false;   
        }
        if(empty($arrUpd) || count($arrUpd)<1) {
           return false;   
        }
        $query = "UPDATE `saja_cash_flow`.`saja_invoice` SET ";
        foreach($arrUpd as $k=>$v) {
            $query.= " ".$k."='".$v."',";      
        }
        $query = substr($query,0,-1);
        $query.=" WHERE prefixid='saja' ";
        
        foreach($arrCond as $k=>$v) {
            $query.= " AND ".$k."='".$v."' ";      
        }
        $ret =  $db->query($query);
        error_log("[allowance/funcitons/updateInvoice] : ".$query."-->".$ret);
        return $ret;
    }
	
	/* 新增折讓關聯資料
	function createAllowanceExtraInfo($db, $arrNew) {
		if(empty($arrNew) || count($arrNew)<1) {
           return false;   
        }
		error_log("[arrExtra] : ".json_encode($arrNew));
		$query = " insert into `saja_cash_flow`.`saja_allowance_extrainfo` SET insertt=NOW() ";
		if(!empty($arrNew['salid'])) {
		    $query.=", salid='{$arrNew['salid']}'";	
	    }
		if(!empty($arrNew['allowance_for'])) {
		    $query.=", allowance_for='{$arrNew['allowance_for']}'";	
	    }
		if(!empty($arrNew['objid'])) {
		    $query.=", objid='{$arrNew['objid']}'";	
	    }
		if(!empty($arrNew['memo'])) {
		    $query.=", memo='{$arrNew['memo']}'";	
	    }
		if(!empty($arrNew['switch'])) {
		    $query.=", switch='{$arrNew['switch']}'";	
	    } 
		$db->query($query);
		$saexid = $db->_con->insert_id; 
		error_log("[allowance/funcitons/createAllowanceExtraInfo] : ".$query."-->".$saexid);
        return $saexid;
	}
	*/
	// 取得折讓單主檔資料
    function getAllowanceList($db, $arrCond) {
		if(!$db) {
		   return false;	
		}
		if(empty($arrCond) || count($arrCond)<1) {
		   return false;
		}
		$query = "SELECT * FROM `saja_cash_flow`.`saja_allowance` WHERE 1=1 "; 
		if(!empty($arrCond['salid'])) {
			$query.=" AND salid='".$arrCond['salid']."' ";
		}	
		if(!empty($arrCond['allowance_no'])) {
			$query.=" AND allowance_no='".$arrCond['allowance_no']."' ";
		}		
		if(!empty($arrCond['userid'])) {
			$query.=" AND userid='".$arrCond['userid']."' ";
		}
		if(!empty($arrCond['allowance_no'])) {
			$query.=" AND allowance_no='".$arrCond['allowance_no']."' ";
		}
		if(!empty($arrCond['allowance_date'])) {
			$query.=" AND allowance_date='".$arrCond['allowance_date']."' ";
		}
		if(!empty($arrCond['allowance_type'])) {
			$query.=" AND allowance_type='".$arrCond['allowance_type']."' ";
		}
		if(!empty($arrCond['allowance_for'])) {
			$query.=" AND allowance_for='".$arrCond['allowance_for']."' ";
		}
		if(!empty($arrCond['upload'])) {
			$query.=" AND upload='".$arrCond['upload']."' ";
		}
		if(!empty($arrCond['switch'])) {
			$query.=" AND switch='".$arrCond['switch']."' ";
		}
		if(!empty($arrCond['_ORDERBY'])) {
			$query.=" ORDER BY ".$arrCond['_ORDERBY'];
		}
		if(!empty($arrCond['_LIMIT'])) { 
			$query.=" LIMIT ".$arrCond['_LIMIT'];
		}
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
    }
	
	// 取得折讓單明細檔資料
    function getAllowanceItemList($db, $arrCond) {
		if(!$db) {
		   return false;	
		}
		if(empty($arrCond) || count($arrCond)<1) {
		   return false;
		} 
		$query = "SELECT * FROM `saja_cash_flow`.`saja_allowance_items` WHERE 1=1 "; 
		
		if(!empty($arrCond['salid'])) {
			$query.=" AND salid='".$arrCond['salid']."' ";
		}
		if(!empty($arrCond['saliid'])) {
			$query.=" AND saliid='".$arrCond['saliid']."' ";
		}
		if(!empty($arrCond['seq_no'])) {
			$query.=" AND seq_no='".$arrCond['seq_no']."' "; 
		}
		if(!empty($arrCond['ori_invoiceno'])) {
			$query.=" AND ori_invoiceno='".$arrCond['ori_invoiceno']."' ";
		}
		if(!empty($arrCond['ori_invoice_date'])) {
			$query.=" AND ori_invoice_date='".$arrCond['ori_invoice_date']."' ";
		}
		if(!empty($arrCond['tax_type'])) {
			$query.=" AND tax_type='".$arrCond['tax_type']."' ";
		}
		if(!empty($arrCond['switch'])) {
			$query.=" AND switch='".$arrCond['switch']."' ";
		}
		if(!empty($arrCond['_ORDERBY'])) {
			$query.=" ORDER BY ".$arrCond['_ORDERBY'];
		}	
		if(!empty($arrCond['_LIMIT'])) {
			$query.=" LIMIT ".$arrCond['_LIMIT'];
		}
		$table = $db->getQueryRecord($query);
		
		if(!empty($table['table']['record'])) {
			return $table['table']['record'];
		} else {
			return false;
		}
    }
	
	// 由折讓資料計算發票折讓金額
	function updAllowanceAmtOfInvoice($db, $invoiceno) {
		     if(!$db) {
		        error_log("[updAllowanceAmtOfInvoice] error db connect !!");
				return false;	
		     }
			 if(empty($invoiceno)) {
		        error_log("[updAllowanceAmtOfInvoice] empty invoiceno !!");
				return false;	
		     }
			 $query = "SELECT allowance_amt FROM  `saja_cash_flow`.`saja_invoice` WHERE invoiceno='${invoiceno}' ";
			 $table = $db->getQueryRecord($query);
			 if(!empty($table['table']['record'])) {
			    error_log("[allowance/funcitons/updAllowanceAmtOfInvoice] ori allowance_amt : ".$table['table']['record'][0]['allowance_amt']);
		     } else {
			    error_log("[allowance/funcitons/updAllowanceAmtOfInvoice] ori allowance_amt : NO_DATA ! ");
				return ;
	 	     }
			 
		     $query = "UPDATE `saja_cash_flow`.`saja_invoice` SET allowance_amt=(
			             SELECT IFNULL(SUM(amount),0)  AS allowance_amt
                           FROM `saja_cash_flow`.`saja_allowance_items` 
						  WHERE switch='Y' 
						    AND ori_invoiceno='${invoiceno}' ) 
					   WHERE invoiceno='${invoiceno}' ";
		     $ret =  $db->query($query);
             error_log("[allowance/funcitons/updAllowanceAmtOfInvoice] : ".$query."-->".$ret);
			 return $ret;
	}
?>