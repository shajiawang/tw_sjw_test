<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "/var/www/lib/saja/mysql.ini.php";
include_once "functions.php";

$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$userid=getValue($this->io, "userid");
if(empty($userid)) {
   $this->jsAlertMsg('缺用戶編號 !!');
    return;    
}

$allowance_amt=getValue($this->io,"allowance_amt");
if(empty($allowance_amt)) {
   $this->jsAlertMsg('缺折讓金額 !!');
   return;    
}
if($allowance_amt<=0){
   $this->jsAlertMsg('折讓金額錯誤 !!');
   return;
}

if(round($allowance_amt) != $allowance_amt){
   $this->jsAlertMsg('折讓金額不可為小數 !!');
   return;
}

$total_idx=getValue($this->io,"total_idx");
if(empty($total_idx)) {
   $this->jsAlertMsg('無明細筆數資料 !!');
   return; 	
}

$user_name=getValue($this->io, "user_name");
$objid=getValue($this->io,"objid");
$allowance_for=getValue($this->io,"allowance_for","N");
$memo=getValue($this->io,"memo","N/A");

// allowance主檔
$arrAllowance = array();
$arrAllowance['allowance_no']=time();
$arrAllowance['userid']=$userid;
$arrAllowance['user_name']=$user_name;
$arrAllowance['allowance_date']=date('Ymd');
$arrAllowance['invoice_buyer_id']='00000000';
$arrAllowance['invoice_buyer_name']=$userid;
// 關聯資訊
$arrAllowance['memo']=$memo;
$arrAllowance['allowance_for']=$allowance_for;
$arrAllowance['objid']=$objid;
	  
// 折讓金額
$arrAllowance['allowance_amt']=$allowance_amt;

// 由總金額推算的稅額
$arrAllowance['allowance_tax_amt']=$allowance_amt-round($allowance_amt/1.05);

// allowance_type
// 1:買方開立折讓證明單
// 2:賣方開立折讓證明通知單
$arrAllowance['allowance_type']='2';

// 發票明細檔
$arrAllowanceItem=array();
$allowance_amt_total=0;

for($idx=0;$idx<$total_idx;++$idx) {
	
    $arrAllowanceItem[$idx]=array(); 
    $arrAllowanceItem[$idx]["ori_invoiceno"]=getValue($this->io,"ori_invoiceno_".$idx);
    $arrAllowanceItem[$idx]["ori_invoice_date"]=getValue($this->io,"ori_invoice_date_".$idx);	
   	$arrAllowanceItem[$idx]["ori_allowance_amt"]=getValue($this->io,"ori_allowance_amt_".$idx);
	$arrAllowanceItem[$idx]["amount"]=getValue($this->io, "allowance_amt_".$idx,0);
	$arrAllowanceItem[$idx]["tax_type"]="1";
	$arrAllowanceItem[$idx]["tax"]=getValue($this->io,"allowance_tax_".$idx,0);
	$arrAllowanceItem[$idx]['quantity']=1;
	$arrAllowanceItem[$idx]['unitprice']=$arrAllowanceItem[$idx]["amount"]-$arrAllowanceItem[$idx]["tax"];
	
	// 由明細累加的折讓金額
	$allowance_amt_total+=$arrAllowanceItem[$idx]["amount"];
	
}

// 檢查 : 折讓總金額 = 明細發票中累加的金額 ?
if($allowance_amt_total!=$arrAllowance['allowance_amt']) {
   $this->jsAlertMsg("折讓金額與發票明細加總不符 !!");
   return;
}


$config = $this->config;
// error_log("[admin]config : ".json_encode($config));
$db = new mysql($config->db[0]);
$db->connect();

$salid = createAllowance($db, $arrAllowance);
if($salid){
	  $arrAllowance['salid']= $salid;
	  // 寫入折讓明細
	  $seq_no=1;
	  for($idx=0;$idx<count($arrAllowanceItem);++$idx) {
			$arrAllowanceItem[$idx]['salid']=$salid;
			$arrAllowanceItem[$idx]['seq_no']=str_pad($seq_no,3,"0",STR_PAD_LEFT);
			
			// 新增allowance明細
			$saliid = createAllowanceItem($db, $arrAllowanceItem[$idx]);
			if($saliid>0){
				  ++$seq_no;
				  // 修改發票資料的allowance_amt, 將本次折讓金額加到發票的已折讓金額
				  $invoiceData = getInvoiceByNo($db, $arrAllowanceItem[$idx]['ori_invoiceno']);
				  if($invoiceData){
					 $addAmt = $invoiceData['allowance_amt']+$arrAllowanceItem[$idx]["amount"];
					 updateInvoice($db, array("allowance_amt"=>$addAmt), array("invoiceno"=>$arrAllowanceItem[$idx]['ori_invoiceno']));
				  }
			}
	  }
      $ret['retCode']=1;
	  $ret['retMsg']='OK';
	  $arrAllowance['items']=$arrAllowanceItem;
	  $ret['retObj']=$arrAllowance;
}




// Path Start 

$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}

if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}


##############################################################################################################################################
// Log Start 
$allowance_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'allowance_insert2', 
	`active` = '折讓預覽新增寫入', 
	`memo` = '{$allowance_data}', 
	`root` = 'allowance/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// Path End 

//error_log("[admin/allowance] allowance : ".json_encode($arrAllowance));
$this->tplVar('result' , $ret) ;
$this->tplVar('status',$status["status"]);
$this->display();




  
  