<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "/var/www/lib/saja/mysql.ini.php";
include_once "functions.php";

$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$arrUpd = array();

$arrUpd['allowance_no']=getValue($this->io,"allowance_no");
if(empty($arrUpd['allowance_no'])) {
   $this->jsAlertMsg('缺折讓單號 !!');
   return;    
}

$arrUpd['salid']=getValue($this->io,"salid");
if(empty($arrUpd['salid'])) {
   $this->jsAlertMsg('缺折讓單序號 !!');
   return;    
}

$arrUpd['userid']=getValue($this->io,"userid");
if(empty($arrUpd['userid'])) {
   $this->jsAlertMsg('缺用戶編號 !!');
   return;    
}

$arrUpd['invoice_buyer_name']=getValue($this->io,"invoice_buyer_name");
if(empty($arrUpd['invoice_buyer_name'])) {
   $this->jsAlertMsg('缺用戶名稱 !!');
   return;    
}

$arrUpd['allowance_type']=getValue($this->io,"allowance_type");
if(empty($arrUpd['allowance_type'])) {
   $this->jsAlertMsg('缺折讓類型 !!');
   return;    
}

$arrUpd['allowance_date']=getValue($this->io,"allowance_date");
if(empty($arrUpd['allowance_date'])) {
   $this->jsAlertMsg('缺折讓時間 !!');
   return;    
}

$arrUpd['upload']=getValue($this->io,"upload");
if(empty($arrUpd['upload'])) {
   $this->jsAlertMsg('缺處理狀態(Y/N) !!');
   return;    
}

$arrUpd['switch']=getValue($this->io,"switch");
if(empty($arrUpd['switch'])) {
   $this->jsAlertMsg('缺資料狀態(Y/N) !!');
   return;    
}

$arrUpd['allowance_for']=getValue($this->io,"allowance_for");
$arrUpd['objid']=getValue($this->io,"objid");
$arrUpd['memo']=getValue($this->io,"memo");

$query = " UPDATE `saja_cash_flow`.`saja_allowance` SET 
             userid='${arrUpd['userid']}',
			 invoice_buyer_name='${arrUpd['invoice_buyer_name']}',
			 allowance_type='${arrUpd['allowance_type']}',
			 allowance_date='${arrUpd['allowance_date']}',
			 upload='${arrUpd['upload']}',
			 allowance_for='${arrUpd['allowance_for']}',
			 objid='${arrUpd['objid']}',
			 memo='${arrUpd['memo']}',
			 switch ='${arrUpd['switch']}'          			 
			WHERE salid='${arrUpd['salid']}' ";

$config = $this->config;
$db = new mysql($config->db[0]);
$db->connect();

$result = $db->query($query);
error_log("[admin/allowance/update] : ".$query."==>".$result);

// 明細資料的資料有效性須和主檔一樣
if($result==1 && $arrUpd['salid']>0) {
   $query1 =  " UPDATE `saja_cash_flow`.`saja_allowance_items` SET  switch ='${arrUpd['switch']}' WHERE salid='${arrUpd['salid']}' ";
   $result_item = $db->query($query1);
   error_log("[admin/allowance/update] : ".$query1."==>".$result_item);
   if($result_item>0) {
	  // 重新計算發票折讓金額 
	   $query2=" SELECT ori_invoiceno FROM `saja_cash_flow`.`saja_allowance_items` WHERE salid='${arrUpd['salid']}' ";
       $table = $db->getQueryRecord($query2);
       error_log("[admin/allowance/update] : ".$query2."==>".json_encode($table)."==>".count($table['table']['record']));
	   if(count($table['table']['record'])>0) {
          foreach ($table['table']['record'] as $v) {
			  if(!empty($v['ori_invoiceno'])) {
				 updAllowanceAmtOfInvoice($db, $v['ori_invoiceno']);
              }				  
		  }
       }		   
   }
}

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}

if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
// Path End 
// $this->tplVar('result' , $ret) ;



##############################################################################################################################################
// Log Start 
$allowance_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'allowance_update', 
	`active` = '折讓修改寫入', 
	`memo` = '{$allowance_data}', 
	`root` = 'allowance/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('status',$status["status"]);
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
// $this->display();




  
  