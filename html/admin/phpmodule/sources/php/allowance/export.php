<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

$sub_sort_query =  " ORDER BY a.insertt DESC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";

if(!empty($data["search_salid"])){
	$sub_search_query .= "	AND a.`salid` = '".strtoupper($data["search_salid"])."' ";
}
if(!empty($data["search_allowance_no"])){
	$sub_search_query .= "	AND a.`allowance_no` = '".$data["search_allowance_no"]."' ";
}
if(!empty($data["search_userid"])){
	$sub_search_query .= "	AND a.`userid` = '".$data["search_userid"]."' ";
}
if(!empty($data["search_objid"])){
	$sub_search_query .= "	AND a.`objid` = '".$data["search_objid"]."' ";
}
if($data["search_btime"] != '' && $data["search_etime"] != ''){
	$sub_search_query .= " AND a.`allowance_date` BETWEEN '".$data["search_btime"]."' AND '".$data["search_etime"]."' ";
}
if(!empty($data["search_upload"])){
	$sub_search_query .= "	AND a.`upload` = '".$data["search_upload"]."' ";
}
if(!empty($data["search_switch"])){
	$sub_search_query .= "	AND a.`switch` = '".$data["search_switch"]."' ";
}
// Search End

$query_search = '';
if ($data['tester'] == 1) {
	$query_search .= ' AND a.`userid` not in (1, 3, 9, 28, 116, 507, 585, 586, 588) ';
}

$query ="SELECT a.*, (SELECT nickname FROM saja_user.saja_user_profile WHERE userid=a.userid) as user_name, ai.ori_invoiceno, ai.ori_description, ai.unitprice, i.invoice_datetime, i.buyer_id, i.buyer_name, i.seller_id, i.seller_name, ai.tax 
			FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."allowance` a 
			LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."allowance_items` ai 
			ON ai.salid = a.salid 
			LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice` i 
			ON i.invoiceno = ai.ori_invoiceno 
		   WHERE 1=1 
" ;
$query .= $query_search ; 
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
// error_log("[source/allowance] export : ".$query);
$table = $this->model->getQueryRecord($query);  

##############################################################################################################################################
// Log Start 
$allowance_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'allowance_export', 
	`active` = '折讓匯出', 
	`memo` = '{$allowance_data}', 
	`root` = 'allowance/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//會員資料表
$Report_Array[0]['title'] = array(
	"A1" => "序號",
	"B1" => "折讓單號",
    "C1" => "折讓日期",
	"D1" => "用戶編號",
    "E1" => "會員姓名",
    // "F1" => "折讓金額(含稅)",
    // "G1" => "折讓稅額",
    "F1" => "申報狀態",
	"G1" => "資料狀態",
	"H1" => "新增日期",
	"I1" => "發票號碼",
	"J1" => "發票日期",
	"K1" => "買方統編",
	"L1" => "買方名稱",
	"M1" => "賣方統編",
	"N1" => "賣方名稱",
	"O1" => "品項名稱",
	"P1" => "品項折讓金額(不含稅)",
	"Q1" => "品項折讓稅額"
	);

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		
		if ($value['upload'] == 'N') :
			$up = '尚未申報';
		elseif ($value['upload'] == 'Y') :
			$up = '已申報完成';
		elseif ($value['upload'] == 'P') :
			$up = '申報中';
		else :
			$up = '尚未申報';
		endif;
		
		if ($value['switch'] == 'N') :
			$sw = '無效';
		else :
			$sw = '有效';
		endif;
		
		$row = $key + 2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['salid']);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$row, (string)$value['allowance_no'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['allowance_date'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row, (string)$value['userid'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['user_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		// $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['allowance_amt']);
		// $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['allowance_tax_amt']);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$row, (string)$up,PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$row, (string)$sw,PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$row, (string)$value['insertt'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('I'.$row, (string)$value['ori_invoiceno'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('J'.$row, (string)$value['invoice_datetime'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$row, (string)$value['buyer_id'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$row, (string)$value['buyer_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$row, (string)$value['seller_id'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$row, (string)$value['seller_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$row, (string)$value['ori_description'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$row, (string)$value['unitprice'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$row, (string)$value['tax'],PHPExcel_Cell_DataType::TYPE_STRING);
	}
}

$objPHPExcel->getActiveSheet()->setTitle('折讓管理');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="折讓管理'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>