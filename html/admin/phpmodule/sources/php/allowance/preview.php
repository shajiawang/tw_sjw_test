<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
include_once "functions.php";

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

$allowance_for=getValue($this->io, "allowance_for","");
$objid=htmlspecialchars(getValue($this->io, "objid",""));
if(empty($allowance_for) && !empty($objid)) {
   $this->jsAlertMsg('請確認關聯單據類型 !!');
   return;
}

if(!empty($allowance_for) && empty($objid)) {
   $this->jsAlertMsg('請填寫關聯單據編號 !!');
   return;
}

$memo=htmlspecialchars(getValue($this->io, "memo",""));

$db_user = $this->config->db[0]['dbname'];
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if($allowance_for=='I')  {
	// 折讓單筆發票
	if(empty($objid)) {
	   $this->jsAlertMsg('請填發票編號 !!');
       return;	
	}
	// 撈取對應發票
	$item = getInvoiceByNo($this->model,$objid,'');
	if(!$item) {
	   $this->jsAlertMsg('找不到發票 !!');
       return;	
	}
	
	$arrUserProfile="";
	if($item['userid']) {
	   $query = " SELECT * FROM `saja_user`.`saja_user_profile` WHERE prefixid ='saja' AND userid = '".$item['userid']."' ";
	   $table = $this->model->getQueryRecord($query); 
	   if(empty($table['table']['record'])) {
		  $this->jsAlertMsg('該發票無用戶資料 !!');
		  return; 
	   } else {
		  $arrUserProfile=$table['table']['record'][0];
	   }
	} else {
	   $this->jsAlertMsg('發票無用戶編號 !!');
	   return; 
	}
	
	// 新增allowance主檔
	$arrAllowance = array();
	$arrAllowance['allowance_for']=$allowance_for;
	$arrAllowance['objid']=$objid;
	$arrAllowance['memo']=$memo;
	$arrAllowance['allowance_no']=time();
	$arrAllowance['userid']=$item['userid'];
	$arrAllowance['allowance_date']=date('Ymd');
	$arrAllowance['invoice_buyer_id']='00000000';
	$arrAllowance['invoice_buyer_name']=$item['userid'];
	
	$allowance_amt0=$item['total_amt']-$item['allowance_amt'];;
	$allowance_tax_amt0=$allowance_amt0-round($allowance_amt0/1.05);
	
	$arrAllowance['allowance_amt']=$allowance_amt0;
	$arrAllowance['allowance_tax_amt']=$allowance_tax_amt0;
	// 1:買方開立折讓證明單
	// 2:賣方開立折讓證明通知單
	$arrAllowance['allowance_type']='2';
	
	// 折讓明細
	$allowanceItem=array();
	$allowanceItem['tax_type']='1';
	$allowanceItem['ori_invoice_date']=$item['invoice_datetime'];
	$allowanceItem['ori_invoiceno']=$item['invoiceno'];
	$allowanceItem['ori_allowance_amt']=$item['allowance_amt'];
	$allowanceItem['ori_sales_amt']=$item['sales_amt'];
	$allowanceItem['ori_tax_amt']=$item['tax_amt'];
	$allowanceItem['ori_total_amt']=$item['total_amt'];
    $allowanceItem['quantity']=1;
	$allowanceItem['ori_description']=$item['description'];	
	
	$allowanceItem['amount']=$allowance_amt0;
	$allowanceItem['tax']=$allowance_tax_amt0;
	
	$ret['retCode']=1;
	$ret['retMsg']='OK';
	$arrAllowance['items']=array();
	array_push($arrAllowance['items'],$allowanceItem);
	$arrAllowance['user_data'] = $arrUserProfile;
	$ret['retObj']=$arrAllowance;
	error_log("Invoice arrAllowance : ".json_encode($arrAllowance));
	
} else {
	$userid=$this->io->input["post"]["userid"];
	if (empty($userid)) {
		$userid=$this->io->input["get"]["userid"];
	}
	if(empty($userid)) {
	   $this->jsAlertMsg('缺用戶編號 !!');
		return;    
	}

	$allowance_amt=$this->io->input["post"]["allowance_amt"];
	if (empty($allowance_amt)) {
		$allowance_amt=$this->io->input["get"]["allowance_amt"];
	}
	if(empty($allowance_amt)) {
	   $this->jsAlertMsg('缺折讓金額 !!');
	   return;    
	}
	if($allowance_amt<=0){
	   $this->jsAlertMsg('折讓金額錯誤 !!');
	   return;
	}

	if(round($allowance_amt) != $allowance_amt){
	   $this->jsAlertMsg('折讓金額不可為小數 !!');
	   return;
	}

	// 取得用戶資訊
	$arrUserProfile="";
	$query = " SELECT * FROM `saja_user`.`saja_user_profile` WHERE prefixid ='saja' AND userid = '{$userid}' ";
	$table = $this->model->getQueryRecord($query); 
	if(empty($table['table']['record'])) {
		$this->jsAlertMsg('無該用戶資料 !!');
		return; 
	} else {
		$arrUserProfile=$table['table']['record'][0];
	}
	
	// 取得用戶有折讓中的發票 (依發票時間 asc & 可折讓金額 asc 排列)
	$invoice_list_in_allowance = getInvoiceListInAllowance($this->model, $userid, 'S');
	if($invoice_list_in_allowance){
	   // error_log("invoice_list_in_allowance : ".json_encode($invoice_list_in_allowance));
	   foreach($invoice_list_in_allowance as $item){
		  $invoice_list[]=$item;
	   }
	}

	// 取得用戶未折讓的發票, (依發票時間 asc & 可折讓金額 asc 排列)
	$invoice_list_no_allowance  = getInvoiceListNotInAllowance($this->model, $userid, 'S');
	if($invoice_list_no_allowance){
	   // error_log("invoice_list_no_allowance : ".json_encode($invoice_list_no_allowance));
	   foreach($invoice_list_no_allowance as $item){
		 $invoice_list[]=$item;
	   }
	}

	// 保留次序  將兩批資料合併(已折讓過的發票在前面)
	if(!$invoice_list || count($invoice_list)<1){
	   $this->jsAlertMsg('該用戶無發票可折讓 !!');
	   return;
	}

	$amount = $allowance_amt;
	$arrAllowanceItem=array();
	foreach($invoice_list as $item){
	  if($amount>0){
		$allowanceItem=array();
		$allowanceItem['tax_type']='1';
		$allowanceItem['ori_invoice_date']=$item['invoice_datetime'];
		$allowanceItem['ori_invoiceno']=$item['invoiceno'];
		$allowanceItem['ori_allowance_amt']=$item['allowance_amt'];
		$allowanceItem['ori_sales_amt']=$item['sales_amt'];
		$allowanceItem['ori_tax_amt']=$item['tax_amt'];
		$allowanceItem['ori_total_amt']=$item['total_amt'];	
		// 折讓明細目前只有一項  先寫死
		$allowanceItem['quantity']=1;
		$allowanceItem['ori_description']=$item['description'];
		// 發票剩餘可折讓金額
		$allowable_allowance_amt = $item['total_amt']-$item['allowance_amt'];
		if($allowable_allowance_amt<=0)
		   continue;
		if($amount>=$allowable_allowance_amt){
		  // 當折讓金額>=發票可折讓金額時 發票全部折讓掉
		  $item['allowance_amt']=$allowable_allowance_amt;
		  $allowanceItem['amount']=$allowable_allowance_amt;
		  $allowance_without_tax=round($allowanceItem['amount']/(1.0+$item['tax_rate']));
		  $allowanceItem['tax']=$allowanceItem['amount']-$allowance_without_tax;
		  // $allowanceItem['tax']=round($allowable_allowance_amt*$item['tax_rate']);
		  $allowanceItem['unitprice']=$allowable_allowance_amt;
		  $amount-=$allowable_allowance_amt;
		}else if($amount<$allowable_allowance_amt){
		  // 當 剩餘折讓金額<發票可折讓金額時 發票僅部分折讓
		  $item['allowance_amt']=$amount;
		  $allowanceItem['amount']=$amount;
		  $allowanceItem['unitprice']=$amount;
		  $allowance_without_tax=round($amount/(1.0+$item['tax_rate']));
		  $allowanceItem['tax']=$allowanceItem['amount']-$allowance_without_tax;
		  // $allowanceItem['tax']=round($amount*$item['tax_rate']);
		  $amount=0;
		}
		array_push($arrAllowanceItem,$allowanceItem);
	  }
	}

	if($amount>0){
	   $this->jsAlertMsg('可折讓金額不足 !!');
	   return;
	}  

	/* 統計折讓稅額
	$allowance_tax_amt = 0;
	foreach($arrAllowanceItem as $item){
	   $allowance_tax_amt+=$item['tax'];
	} 
	*/
	$allowance_tax_amt = $allowance_amt-round($allowance_amt/1.05);
	
	// 新增allowance主檔
	$arrAllowance = array();
	$arrAllowance['allowance_for']=$allowance_for;
	$arrAllowance['objid']=$objid;
	$arrAllowance['memo']=$memo;
	$arrAllowance['allowance_no']=time();
	$arrAllowance['userid']=$userid;
	$arrAllowance['allowance_date']=date('Ymd');
	$arrAllowance['invoice_buyer_id']='00000000';
	$arrAllowance['invoice_buyer_name']=$userid;
	$arrAllowance['allowance_amt']=$allowance_amt;
	$arrAllowance['allowance_tax_amt']=$allowance_tax_amt;

	// 1:買方開立折讓證明單
	// 2:賣方開立折讓證明通知單
	$arrAllowance['allowance_type']='2';
	$ret['retCode']=1;
	$ret['retMsg']='OK';
	$arrAllowance['items']=$arrAllowanceItem;
	$arrAllowance['user_data'] = $arrUserProfile;
	$ret['retObj']=$arrAllowance;
    error_log("Order arrAllowanceItem : ".json_encode($arrAllowanceItem));
	
}

##############################################################################################################################################
// Log Start 
$allowance_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'allowance_preview', 
	`active` = '折讓預覽', 
	`memo` = '{$allowance_data}', 
	`root` = 'allowance/preview', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

error_log("[admin/allowance/preview] allowance : ".json_encode($arrAllowance));
$this->tplVar('result' , $ret) ;
$this->tplVar('status',$status["status"]);
$this->display();


// $status["status"]["base_href"] = $status["status"]["path"] ."productid={$this->io->input["get"]["productid"]}";



  
  