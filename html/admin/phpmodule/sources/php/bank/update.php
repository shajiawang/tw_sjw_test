<?php
// Check Variable Start
if (empty($this->io->input["post"]["bkid"])) {
	$this->jsAlertMsg('銀行系ID錯誤!!');
}
if (empty($this->io->input["post"]["bankname"])) {
	$this->jsAlertMsg('銀行名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["bankid"])) {
	$this->jsAlertMsg('銀行代號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}bank` SET 
	`bankname` = '{$this->io->input["post"]["bankname"]}',
	`bankid`='{$this->io->input["post"]["bankid"]}',
	`content1`='{$this->io->input["post"]["content1"]}',
	`bank_switch1`='{$this->io->input["post"]["bank_switch1"]}',
	`content2`='{$this->io->input["post"]["content2"]}',
	`bank_switch2`='{$this->io->input["post"]["bank_switch2"]}',	
	`content3`='{$this->io->input["post"]["content3"]}',
	`bank_switch3`='{$this->io->input["post"]["bank_switch3"]}',
	`content4`='{$this->io->input["post"]["content4"]}',
	`bank_switch4`='{$this->io->input["post"]["bank_switch4"]}',
	`content5`='{$this->io->input["post"]["content5"]}',
	`bank_switch5`='{$this->io->input["post"]["bank_switch5"]}',	
	`switch`='{$this->io->input["post"]["switch"]}'   
WHERE 
	`bkid` = '{$this->io->input["post"]["bkid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$bank_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'bank_update', 
	`active` = '銀行修改寫入', 
	`memo` = '{$bank_data}', 
	`root` = 'bank/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));