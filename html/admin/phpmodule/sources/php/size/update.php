<?php
// Check Variable Start
if (empty($this->io->input["post"]["sid"])) {
	$this->jsAlertMsg('尺寸編號錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('尺寸名稱錯誤!!');
}
if (empty($this->io->input["post"]["sizecode"])) {
	$this->jsAlertMsg('尺寸色碼錯誤!!');
}
if (empty($this->io->input["post"]["kind"])) {
	$this->jsAlertMsg('尺寸使用類型錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

// Check Variable End
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}size` SET 
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}',
	`seq`='{$this->io->input["post"]["seq"]}',
	`sizecode`='{$this->io->input["post"]["sizecode"]}', 
	`kind`='{$this->io->input["post"]["kind"]}',
	`used`='{$this->io->input["post"]["used"]}',
	`stype`='{$this->io->input["post"]["stype"]}'
WHERE 
	`sid` = '{$this->io->input["post"]["sid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$size_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'size_update', 
	`active` = '尺寸修改寫入', 
	`memo` = '{$size_data}', 
	`root` = 'size/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));