<?php
// Check Variable Start
if (empty($this->io->input["get"]["ueid"])) {
	$this->jsAlertMsg('會員其他資訊ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_user}`.`{$this->config->default_prefix}user_extrainfo`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `ueid` = '{$this->io->input["get"]["ueid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT c.* 
FROM `{$db_user}`.`{$this->config->default_prefix}user_extrainfo_category` c 
WHERE 
	c.prefixid = '{$this->config->default_prefix_id}' 
	and c.uecid = '{$table['table']['record'][0]['uecid']}' 
	AND c.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["ue_category"] = $recArr['table']['record'][0];
 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_extrainfo_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_extrainfo_edit', 
	`active` = '會員卡包修改', 
	`memo` = '{$user_extrainfo_data}', 
	`root` = 'user_extrainfo/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();