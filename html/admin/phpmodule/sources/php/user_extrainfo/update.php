<?php
// Check Variable Start
if (empty($this->io->input["post"]["ueid"])) {
	$this->jsAlertMsg('會員其他資訊錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Update Start
//$description = htmlspecialchars($this->io->input["post"]["description"]);
$query = "select * from `{$db_user}`.`{$this->config->default_prefix}user_extrainfo_category` 
where
	`prefixid` = '{$this->config->default_prefix_id}'
	and `uecid` = '{$this->io->input["post"]["uecid"]}'
	and `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);

if ($recArr['table']['record'][0]) {
	$flagArr = str_split($recArr['table']['record'][0]['flag']);
	foreach ($flagArr as $fk => $fv) {
		$fieldname = 'field'.($fk + 1).'name';
		if ($fv) {
			if (empty($this->io->input["post"][$fieldname])) {
				$this->jsAlertMsg($recArr['table']['record'][0][$fieldname].'錯誤!!');
			}
			$field .= "`".$fieldname."` = '{$this->io->input["post"][$fieldname]}', ";
		}
	}
}

$query ="
UPDATE `{$db_user}`.`{$this->config->default_prefix}user_extrainfo` SET 
	{$field}
	`seq` = '{$this->io->input["post"]["seq"]}',
	`modifyt` = NOW()
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `ueid` = '{$this->io->input["post"]["ueid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_extrainfo_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_extrainfo_update', 
	`active` = '會員卡包修改寫入', 
	`memo` = '{$user_extrainfo_data}', 
	`root` = 'user_extrainfo/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));