<?php
// Check Variable Start
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員id錯誤!!');
}
if (empty($this->io->input["post"]["uecid"])) {
	$this->jsAlertMsg('會員其他資訊分類錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
//$description = htmlspecialchars($this->io->input["post"]["description"]);
$query = "select * from `{$db_user}`.`{$this->config->default_prefix}user_extrainfo_category` 
where
	`prefixid` = '{$this->config->default_prefix_id}'
	and `uecid` = '{$this->io->input["post"]["uecid"]}'
	and `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);

if ($recArr['table']['record'][0]) {
	$flagArr = str_split($recArr['table']['record'][0]['flag']);
	foreach ($flagArr as $fk => $fv) {
		$fieldname = 'field'.($fk + 1).'name';
		if ($fv) {
			if (empty($this->io->input["post"][$fieldname])) {
				$this->jsAlertMsg($recArr['table']['record'][0][$fieldname].'錯誤!!');
			}
			$field .= "`".$fieldname."` = '{$this->io->input["post"][$fieldname]}', ";
			$orderData .= $recArr['table']['record'][0][$fieldname].":".$this->io->input["post"][$fieldname]."\n";
		}
	}
}

if ($this->io->input["post"]["uecid"] == 1) {
	//改變訂單狀態
	$query = "
	select orderid from `{$db_exchange}`.`{$this->config->default_prefix}order` 
	where
		`prefixid` = '{$this->config->default_prefix_id}'
		and `userid` = '{$this->io->input["post"]["userid"]}'
		and `status` = '0'
		and `type` = 'exchange'
		and `epid` = '689'
		and `switch` = 'Y'
	" ;
	$table = $this->model->getQueryRecord($query);
	
	if ($table['table']['record'][0]['orderid']) {
		//改變訂單狀態
		$query ="
		UPDATE `{$db_exchange}`.`{$this->config->default_prefix}order` SET
			`status` = '1',
			`memo` = '{$orderData}',
			`modifyt` = now()
		where
			`prefixid` = '{$this->config->default_prefix_id}'
			and `userid` = '{$this->io->input["post"]["userid"]}'
			and `status` = '0'
			and `type` = 'exchange'
			and `epid` = '493'
			and `switch` = 'Y'
		" ;
		$this->model->query($query);
		
		//新增訂單出貨單
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}order_shipping` SET 
			`prefixid` = '{$this->config->default_prefix_id}', 
			`orderid` = '{$table['table']['record'][0]['orderid']}',
			`outtime` = NOW(), 
			`outtype` = '中油卡',
			`outcode` = '',
			`outmemo` = '{$orderData}',
			`seq` = '0',
			`switch` = 'Y', 
			`insertt` = now() 
		" ;
		$this->model->query($query);
	}
	else {
		// $this->jsAlertMsg('此會員尚未購買中油卡!!');
		// break;
		
		$retcode=1; 
		
	}
}

//新增會員其他資訊
$query ="
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}user_extrainfo` SET
	`prefixid` = '{$this->config->default_prefix_id}',
	`userid` = '{$this->io->input["post"]["userid"]}',
	`uecid` = '{$this->io->input["post"]["uecid"]}',
	{$field}
	`seq` = '{$this->io->input["post"]["seq"]}', 
	`insertt` = now()
" ;
$this->model->query($query);

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_extrainfo_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_extrainfo_insert', 
	`active` = '會員卡包新增寫入', 
	`memo` = '{$user_extrainfo_data}', 
	`root` = 'user_extrainfo/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



if ($retcode==1){
	$this->jsPrintMsg('更新完成，此會員非兌換中油卡商品，需手動更新訂單狀態!!',$this->io->input["post"]["location_url"]);
}
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));