<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT *
FROM `{$db_product}`.`{$this->config->default_prefix}product` p 
WHERE 
p.prefixid = '".$this->config->default_prefix_id."'
and p.closed='N' 
and p.offtime>NOW() 
and p.switch='Y' 
and p.ontime>='2018-12-01 00:00:00' 
ORDER BY p.seq
";
$table = $this->model->getQueryRecord($query);

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_oscode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_oscode_depositadd', 
	`active` = '手動儲值會員送券新增', 
	`memo` = '{$add_oscode_data}', 
	`root` = 'add_oscode/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->display();