<?php
// Check Variable Start
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員編號不可為空白 !!');
}
if (empty($this->io->input["post"]["snum"])) {
	$this->jsAlertMsg('組數不可為空白!!');
}
if (empty($this->io->input["post"]["productid"])) {
	$this->jsAlertMsg('商品不可為空白!!');
}

// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
$snum = $this->io->input["post"]["snum"];

$query ="
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}oscode` SET
	`prefixid`='{$this->config->default_prefix_id}', 
	`userid` = '{$this->io->input["post"]["userid"]}',
	`productid` = '{$this->io->input["post"]["productid"]}',
	`spid` = 0,
	`behav` = 'd',
	`used` = 'N',
	`used_time` = '0000-00-00 00:00:00',
	`amount` = 1,
	`serial` = '',
	`verified` = 'N',
	`switch`= 'Y',
	`seq` = 0,
	`insertt`=now()
" ;

for($i=0; $i<$snum;++$i) {
	$r = $this->model->query($query);
	$this->model->_con->insert_id;
	$total+=$r;
}
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_oscode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_oscode_insert', 
	`active` = '手動送券新增寫入', 
	`memo` = '{$add_oscode_data}', 
	`root` = 'add_oscode/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
// $this->jsPrintMsg('殺價券(userid:{$t['table']['record'][0]['userid']},productid:${productid}, total:${snum} ) : 新增 ${total} 組完成!!',$this->io->input["get"]["location_url"]);
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));