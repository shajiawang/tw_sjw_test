<?php
if (empty($this->io->input["get"]["channelid"])) {
	$this->jsAlertMsg('經銷商ID錯誤!!');
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$query = "
UPDATE ".$this->config->db[2]["dbname"].".".$this->config->default_prefix."channel c
LEFT OUTER JOIN ".$this->config->db[2]["dbname"].".".$this->config->default_prefix."channel_store_rt cs ON 
	c.prefixid = cs.prefixid
	AND c.channelid = cs.channelid
	AND cs.switch = 'Y'
LEFT OUTER JOIN ".$this->config->db[2]["dbname"].".".$this->config->default_prefix."store s ON 
	cs.prefixid = s.prefixid
	AND cs.storeid = s.storeid
	AND s.switch = 'Y'
LEFT OUTER JOIN ".$this->config->db[2]["dbname"].".".$this->config->default_prefix."store_product_rt sp ON 
	cs.prefixid = sp.prefixid
	AND cs.storeid = sp.storeid
	AND sp.switch = 'Y'
LEFT OUTER JOIN ".$this->config->db[4]["dbname"].".".$this->config->default_prefix."product p ON 
	sp.prefixid = p.prefixid
	AND sp.productid = p.productid
	AND p.switch = 'Y'
SET 
c.switch = 'N',
cs.switch = 'N',
s.switch = 'N',
sp.switch = 'N',
p.switch = 'N'
WHERE
c.prefixid = '".$this->config->default_prefix_id."' 
AND c.channelid = '".$this->io->input["get"]["channelid"]."' 
";
$this->model->query($query);

// INSERT success message Start
$this->jsPrintMsg('刪除成功!!',$this->io->input["get"]["location_url"]);
// INSERT success message End