<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["channelid"])) {
	$this->jsAlertMsg('經銷商ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('經銷商名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('經銷商敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('經銷商排序錯誤!!');
}

$query ="
UPDATE `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."product_category`
SET
`name` = '".$this->io->input["post"]["name"]."',
`description` = '".$this->io->input["post"]["description"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND channelid = '".$this->io->input["post"]["channelid"]."'
" ;
$this->model->query($query);

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));