<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('商品名稱錯誤!!');
}
// if (empty($this->io->input["post"]["link"]) ) {
	// $this->jsAlertMsg('連結網址錯誤!!');
// }
if (strtotime($this->io->input["post"]["ontime"]) === false) {
	$this->jsAlertMsg('上架時間錯誤!!');
}
if (strtotime($this->io->input["post"]["offtime"]) === false) {
	$this->jsAlertMsg('結標時間錯誤!!');
}

if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$filename = md5(date("YmdHis")."_".$this->io->input['post']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_products_images."/$filename")) {
		$this->jsAlertMsg('商品Banner名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/$filename")) {
		$this->jsAlertMsg('商品Banner上傳錯誤!!');
	}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input['post']['name']}',
		`original`='{$this->io->input['files']['thumbnail']['name']}',
		`filename`='{$filename}',
		`insertt`=now()
	"; 
	$this->model->query($query); 
	$ptid = $this->model->_con->insert_id;
	
	$update_thumbnail = " `ptid` = '{$ptid}',";
}
	$update_thumbnail = " `ptid` = '0',";
if (!empty($this->io->input['files']['thumbnail2']) && exif_imagetype($this->io->input['files']['thumbnail2']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail2']['name']).".".pathinfo($this->io->input['files']['thumbnail2']['name'], PATHINFO_EXTENSION);
	$filename2 = md5(date("YmdHis")."_".$this->io->input['post']['name']."_2").".".pathinfo($this->io->input['files']['thumbnail2']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_products_images."/$filename2")) {
		$this->jsAlertMsg('商品主圖二名稱重覆!!'.$filename2);
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail2']['tmp_name'], $this->config->path_products_images."/$filename2")) {
		$this->jsAlertMsg('商品主圖二上傳錯誤!!');
	}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input['post']['name']}',
		`original`='{$this->io->input['files']['thumbnail2']['name']}',
		`filename`='{$filename2}',
		`insertt`=now()
	"; 
	$this->model->query($query); 
	$ptid2 = $this->model->_con->insert_id;
	$update_thumbnail2 = " `pftid` = '{$ptid2}',";
}
	$update_thumbnail2 = " `pftid` = '0',";
	
$description = htmlspecialchars($this->io->input["post"]["description"]);

$query ="
INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_today` 
SET
	`prefixid`='{$this->config->default_prefix_id}',
	`name`='{$this->io->input["post"]["name"]}',
	`link`='{$this->io->input["post"]["link"]}',
	`description`='{$description}',
	{$update_thumbnail}
	{$update_thumbnail2}	
	`thumbnail_url`='{$this->io->input["post"]["thumbnail_url"]}',
	`ontime`='{$this->io->input["post"]["ontime"]}',
	`offtime`='{$this->io->input["post"]["offtime"]}',
	`seq`='{$this->io->input["post"]["seq"]}',
	`productid`='{$this->io->input["post"]["productid"]}',
	`display`='{$this->io->input["post"]["display"]}',	
	`insertt`=now()
" ;
// echo '<pre>';echo $query;exit;
$this->model->query($query);
$todayid = $this->model->_con->insert_id;



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_today_insert', 
	`active` = '主題商品新增寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_today/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));