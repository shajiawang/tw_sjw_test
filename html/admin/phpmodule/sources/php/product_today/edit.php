<?php
if (empty($this->io->input["get"]["todayid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT 
p.*, pt.filename as thumbnail , pt2.filename as thumbnail2
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_today` p 
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt2 ON 
	p.prefixid = pt2.prefixid
	AND p.pftid = pt2.ptid
	AND pt2.switch = 'Y'	
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND p.todayid = '".$this->io->input["get"]["todayid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_today_edit', 
	`active` = '主題商品修改', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_today/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();