<?php
// Check Variable Start
if (empty($this->io->input["get"]["productid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// 原商品 Record 

$query ="SELECT p.* ,pt.original, pt.filename, pt.name imgname
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p 
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
WHERE p.prefixid = '{$this->config->default_prefix_id}'
AND p.`switch`='Y' 
AND p.productid = '{$this->io->input["get"]["productid"]}'
" ;
$old_product = $this->model->getQueryRecord($query);

if (empty($old_product['table']['record'])) {
	$this->jsAlertMsg('商品錯誤!!');
}

$info_product = $old_product['table']['record'][0];

//Relation exchange_product_thumbnail
if (!empty($info_product["filename"]) ) {
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname"]}',
	`original`='{$info_product["original"]}',
	`filename`='{$info_product["filename"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid = $this->model->_con->insert_id;
}

##############################################################################################################################################


##############################################################################################################################################
// Table Start

// 產生新商品
$description = str_replace("'", '', htmlspecialchars($info_product["description"]) );
$query ="INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` SET
	`prefixid`='{$this->config->default_prefix_id}',
	`name`='[Copy] {$info_product["name"]}',
	`description`='{$description}',
	`retail_price`='{$info_product["retail_price"]}',
	`ptid`='{$ptid}',
	`thumbnail_url`='{$info_product["thumbnail_url"]}',
	`ontime`='',
	`offtime`='',
	`saja_limit`='{$info_product["saja_limit"]}',
	`user_limit`='{$info_product["user_limit"]}',
	`usereach_limit`='{$info_product["usereach_limit"]}',
	`saja_fee`='{$info_product["saja_fee"]}',
	`process_fee`='{$info_product["process_fee"]}',
	`price_limit`='{$info_product["price_limit"]}',
	`bonus_type`='{$info_product["bonus_type"]}',
	`bonus`='{$info_product["bonus"]}',
	`gift_type`='{$info_product["gift_type"]}',
	`gift`='{$info_product["gift"]}',
	`vendorid`='{$info_product["vendorid"]}',
	`split_rate`='{$info_product["split_rate"]}',
	`base_value`='{$info_product["base_value"]}',
	`bid_type`='{$info_product["bid_type"]}',
	`mob_type`='{$info_product["mob_type"]}',
	`loc_type`='{$info_product["loc_type"]}',
	`is_flash`='{$info_product["is_flash"]}',
	`seq`='{$info_product["seq"]}',
	`insertt`=now()
" ;
$this->model->query($query);

//新商品ID
$productid = $this->model->_con->insert_id;


//寫入商品分類rt
$query ="SELECT *
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_category_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_cat = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_cat['table']['record'])
{
	foreach($old_product_cat['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_category_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`pcid`='{$rv['pcid']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}', 
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

//寫入商店rt
$query ="SELECT *
FROM `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_store = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_store['table']['record'])
{
	foreach($old_product_store['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`storeid`='{$rv['storeid']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}', 
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

//寫入下標條件rt
$query ="SELECT *
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_rule = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_rule['table']['record'])
{
	foreach($old_product_rule['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`srid`='{$rv['srid']}',
		`value`='{$rv['value']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}',
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

// Table End
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_today_copy', 
	`active` = '主題商品複製寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_today/copy', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



$to = $this->config->default_main .'/product/edit/productid='. $productid .'&location_url='. $this->io->input['get']['location_url'];
header("location:". $to); //header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));