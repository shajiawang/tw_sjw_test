<?php
if (empty($this->io->input["get"]["todayid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$query = "
UPDATE `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_today`
SET 
switch = 'N'
WHERE
prefixid = '".$this->config->default_prefix_id."' 
AND todayid = '".$this->io->input["get"]["todayid"]."' 
";
$this->model->query($query);



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_today_delete', 
	`active` = '主題商品刪除', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_today/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



// INSERT success message Start
$this->jsPrintMsg('刪除成功!!',$this->io->input["get"]["location_url"]);
// INSERT success message End