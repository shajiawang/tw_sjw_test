<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(name|description|ontime|offtime|seq)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY p.insertt DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End


// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if(!empty($this->io->input["get"]["search_name"])){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND p.`name` like '%".$this->io->input["get"]["search_name"]."%'";
}
if(!empty($this->io->input["get"]["search_description"])){
	$status["status"]["search"]["search_description"] = $this->io->input["get"]["search_description"] ;
	$status["status"]["search_path"] .= "&search_description=".$this->io->input["get"]["search_description"] ;
	$sub_search_query .=  "
		AND p.`description` like '%".$this->io->input["get"]["search_description"]."%'";
}
if(!empty($this->io->input["get"]["search_ontime"])){
	$status["status"]["search"]["search_ontime"] = $this->io->input["get"]["search_ontime"] ;
	$status["status"]["search_path"] .= "&search_ontime=".$this->io->input["get"]["search_ontime"] ;
	$sub_search_query .=  "
		AND p.`ontime` >= '".$this->io->input["get"]["search_ontime"]."'";
}
if(!empty($this->io->input["get"]["search_offtime"])){
	$status["status"]["search"]["search_offtime"] = $this->io->input["get"]["search_offtime"] ;
	$status["status"]["search_path"] .= "&search_offtime=".$this->io->input["get"]["search_offtime"] ;
	$sub_search_query .=  "
		AND p.`offtime` <= '".$this->io->input["get"]["search_offtime"]."'";
}

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="SELECT count(*) as num
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_today` p 
{$category_search_query}
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND p.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="SELECT p.*
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_today` p 
{$category_search_query}
WHERE 
p.prefixid = '".$this->config->default_prefix_id."' 
AND p.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################



##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_today', 
	`active` = '主題商品清單查詢', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_today/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];



$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();