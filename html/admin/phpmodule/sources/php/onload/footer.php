<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Footer Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css">

	</head>
<body>
	<div class="iframefooter">Copyright &copy; 2012 殺價王股份有限公司 All Rights Reserved.</div>
</body>
</html>


