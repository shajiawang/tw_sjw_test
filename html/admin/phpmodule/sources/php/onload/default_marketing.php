<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    
	    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	    
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/main.css" type="text/css">
	    
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-main.css" type="text/css">

		<!-- script src="http://code.jquery.com/jquery-1.10.1.min.js"></script -->
<!--		<script src="<?php echo $this->config->default_main; ?>/js/jquery.min.js"></script>-->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script type="text/javascript">
		(function($){
			$.fn.bindFormAction = function() {
				return this.each(function() {
					var $this = $(this);

					if ($this.is('form')) {
						$this.find('.button.cancel').click(function(){
							$this.slideUp();
						});
					}
				});
			};
		})(jQuery);

		jQuery(document).ready(function($) {
			$('#left-control').click(function() {
				$(this).toggleClass('expanded collapsed');
				window.parent.toggleLeft();
			});
			$('.sortable').click(function() {
				$(this).children('a').toggleClass($(this).has('.init').length?'init asc':'asc desc');
			});
			$('.icon-edit').click(function(){
				var form = $('#form-edit').slideDown();

				$(this).parents('tr').children('.column').each(function() {
					form.find('input[name='+$(this).attr('name')+']').val($(this).text());
				});
			});
			$('#form-edit,#form-add').bindFormAction();
		});
		</script>
	</head>
	<body>
		<div class="es-menu-iconboxs d-flex flex-wrap">
            <div class="es-menu-item text-center" data-href="first1">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-users-cog fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">使用者系統</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first2">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-ticket-alt fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">殺價券管理</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first3">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-balance-scale fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">競標系統</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first7">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-american-sign-language-interpreting fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">兌換中心系統</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first8">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-video fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">直播管理</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first9">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-ad fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">廣告管理</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first11">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-chart-bar fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">報表</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first13">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-cog fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">系統管理</div>
		    </div>
		</div>
		<div class="es-first-boxs">
<!-- 生成 first tab content -->
		</div>
		<div class="es-second-boxs">
<!-- 生成 second tab content -->
		</div>
		
<!-- 對應層級 -->
		<script>
            var $islogin = "<?php echo $this->io->input['session']['user'] ?>" == '';
            var $loginUrl,$loginTitle;
            if($islogin){
                $loginUrl = "<?php echo $this->config->default_main; ?>/admin_user/login";
                $loginTitle = "登入系統";
            }else{
                $loginUrl = "<?php echo $this->config->default_main; ?>/admin_user/logout";
                $loginTitle = "登出系統";
            };
            
            var $menuItem = {
                first1:[
                    {title:$loginTitle,href:$loginUrl,item:''},
                    {title:'會員中心管理',href:'',
                        item:[
                            {title:'會員公告',href:'<?php echo $this->config->default_main; ?>/news'},
                            {title:'新手教學分類',href:'<?php echo $this->config->default_main; ?>/faq_category'},
                            {title:'新手教學問答',href:'<?php echo $this->config->default_main; ?>/faq'},
                            {title:'殺價密技分類',href:'<?php echo $this->config->default_main; ?>/tech_category'},
                            {title:'殺價密技問答',href:'<?php echo $this->config->default_main; ?>/tech'}
                        ]
                    }
                ],
                first2:[
                    {title:'分享連結/閃殺活動',href:'',
                        item:[
                            {title:'分享連結/閃殺查詢',href:'<?php echo $this->config->default_main; ?>/scode/11'},
                            {title:'組數設定',href:'<?php echo $this->config->default_main; ?>/scode_spr/12'}
                        ]
                    },
                    {title:'充值滿額活動',href:'',
                        item:[
                            {title:'充值滿額查詢',href:'<?php echo $this->config->default_main; ?>/scode/21'},
                            {title:'組數設定',href:'<?php echo $this->config->default_main; ?>/scode_spr/22'}
                        ]
                    },
                    {title:'殺價券活動',href:'',
                        item:[
                            {title:'殺價券活動查詢',href:'<?php echo $this->config->default_main; ?>/scode/31'},
                            {title:'組數設定',href:'<?php echo $this->config->default_main; ?>/scode_spr/32'}
                        ]
                    },
                    {title:'殺價券序號發送',href:'',
                        item:[
                            {title:'殺價券序號發送查詢',href:'<?php echo $this->config->default_main; ?>/scode/41'},
                            {title:'組數設定',href:'<?php echo $this->config->default_main; ?>/scode_spr/42'}
                        ]
                    },
                    {title:'直播活動',href:'',
                        item:[
                            {title:'直播活動查詢',href:'<?php echo $this->config->default_main; ?>/scode/51'}
                        ]
                    },
                    {title:'廣告活動',href:'',
                        item:[
                            {title:'廣告活動查詢',href:'<?php echo $this->config->default_main; ?>/scode/61'}
                        ]
                    },
                    {title:'競標/下標送殺價券',href:'',
                        item:[
                            {title:'殺價券活動查詢',href:'<?php echo $this->config->default_main; ?>/oscode/31'},
                            {title:'殺價券序號發送',href:'<?php echo $this->config->default_main; ?>/oscode/41'}
                        ]
                    }
                ],
                first3:[
                    {title:'競標商品管理',href:'',
                        item:[
                            {title:'商品分類管理',href:'<?php echo $this->config->default_main; ?>/product_category'},
                            {title:'商品資料管理',href:'<?php echo $this->config->default_main; ?>/product'},
                            {title:'拆分帳目分類管理',href:'<?php echo $this->config->default_main; ?>/profit_sharing_category'},
                            {title:'下標條件管理',href:'<?php echo $this->config->default_main; ?>/saja_rule'},
                            {title:'今日必殺商品',href:'<?php echo $this->config->default_main; ?>/product_today'}
                        ]
                    }
                ],
                first7:[
                    {title:'兌換中心商品管理',href:'',
                        item:[
                            {title:'兌換中心',href:'<?php echo $this->config->default_main; ?>/exchange_mall'},
                            {title:'兌換店家',href:'<?php echo $this->config->default_main; ?>/exchange_store'},
                            {title:'兌換商品分類',href:'<?php echo $this->config->default_main; ?>/exchange_product_category'},
                            {title:'兌換商品',href:'<?php echo $this->config->default_main; ?>/exchange_product'}
                        ]
                    },
                    {title:'兌換商品活動',href:'',
                        item:[
                            {title:'活動規則',href:'<?php echo $this->config->default_main; ?>/promote_rule'},
                            {title:'活動規則項目',href:'<?php echo $this->config->default_main; ?>/promote_rule_item'}
                        ]
                    },
                    {title:'兌換商品序號',href:'',
                        item:[
                            {title:'電子卡序號',href:'<?php echo $this->config->default_main; ?>/exchange_ecoupon'}
                        ]
                    },
                    {title:'兌換相關記錄',href:'',
                        item:[
                            {title:'兌換紅利記錄',href:'<?php echo $this->config->default_main; ?>/exchange_bonus_history'},
                            {title:'兌換禮券記錄',href:'<?php echo $this->config->default_main; ?>/exchange_gift_history'},
                            {title:'退貨記錄',href:'<?php echo $this->config->default_main; ?>/return_history'}
                        ]
                    }
                ],
                first8:[
                    {title:'直播相關管理',href:'',
                        item:[
                            {title:'直播間',href:'<?php echo $this->config->default_main; ?>/live_broadcast'},
                            {title:'直播主',href:'<?php echo $this->config->default_main; ?>/live_broadcast_user'},
                            {title:'直播時間表',href:'<?php echo $this->config->default_main; ?>/live_broadcast_schedule'}
                        ]
                    }
                ],
                first9:[
                    {title:'各類廣告管理',href:'',
                        item:[
                            {title:'廣告查詢',href:'<?php echo $this->config->default_main; ?>/ad_list'},
                            {title:'廣告分類',href:'<?php echo $this->config->default_main; ?>/ad_category'}
                        ]
                    }
                ],
                first10:[
                    {title:'客服功能',href:'',
                        item:[
                            {title:'客戶留言管理',href:'<?php echo $this->config->default_main; ?>/issues'},
                            {title:'客服分類管理',href:'<?php echo $this->config->default_main; ?>/issues_category'}
                        ]
                    }
                ],
                first11:[
                    {title:'資料統計',href:'',
                        item:[
                            {title:'下標商品統計',href:'<?php echo $this->config->default_main; ?>/report_product_statistics'},
                            {title:'兌換商品統計',href:'<?php echo $this->config->default_main; ?>/report_exchange_statistics'},
                            {title:'排行榜',href:'<?php echo $this->config->default_main; ?>/report_ranking'},
							{title:'會員使用裝置統計',href:'<?php echo $this->config->default_main; ?>/report_account_device'},
							{title:'活動來源統計',href:'<?php echo $this->config->default_main; ?>/report_activity_statistics'}
                        ]
                    }
                ],
                first13:[
                    {title:'系統功能',href:'',
                        item:[
                            {title:'銀行管理',href:'<?php echo $this->config->default_main; ?>/bank'},
                            {title:'貸款管理',href:'<?php echo $this->config->default_main; ?>/loan'},							
                            {title:'電信業者管理',href:'<?php echo $this->config->default_main; ?>/telecompany'},
							{title:'電信業務管理',href:'<?php echo $this->config->default_main; ?>/telpaytype'},
							{title:'色彩使用管理',href:'<?php echo $this->config->default_main; ?>/color'}
                        ]
                    }
                ]			
            }
            
            $(function(){
                //生成tab_content <div>
                $('.es-menu-iconboxs').find('.es-menu-item').each(function(){
                    var $that = $(this);
                    var $firstID = $that.data('href');
                    var $firstBox = $('.es-first-boxs');
                    var $secondBox = $('.es-second-boxs');
                    $firstBox.append(
                        $('<div class="es-first-item-box"/>')
                        .attr('id',$firstID)
                    )
                    $.map($menuItem,function(item, index){
                        if(index == $firstID){
                            var i = 0;
                            $.map(item,function(item, index){
                                if(item['href']==''){
                                    i++;
                                    var $second = 'second' + $firstID.replace('first','') +'-'+i;
                                    $('#'+$firstID).append(
                                        $('<div class="first_btn"/>')
                                        .attr('data-href',$second)
                                        .append(
                                            $('<a href="javascript:void(0);"/>')
                                            .text(item['title'])
                                        )
                                    );
                                    $secondBox.append(
                                        $('<div class="es-second-item-box"/>')
                                        .attr('id',$second)
                                    )
                                    $.map(item['item'],function(item, index){
                                        $('#'+$second).append(
                                            $('<div class="second_btn"/>')
                                            .append(
                                                $('<a href="'+item['href']+'"/>')
                                                .text(item['title'])
                                            )
                                        )
                                    })
                                }else{
                                    i++;
                                    $('#'+$firstID).append(
                                        $('<div class="first_btn"/>')
                                        .append(
                                            $('<a/>')
                                            .attr('href',item['href'])
                                            .text(item['title'])
                                        )
                                    );
                                }
                            })
                        }
                    })
                });
                
                //icon menu 切換按鈕
                $('.es-menu-iconboxs').on('click','.es-menu-item',function(){
                    var $that = $(this);
                    var $firstID = $('#'+$that.data('href'));
                    var $firstItems = $('.es-first-boxs .es-first-item-box');
                    var $group = $('.es-menu-iconboxs').find('.es-menu-item');
                    var $secondItems = $('.es-second-boxs .es-second-item-box');
                    $group.removeClass('active');
                    $that.addClass('active');
                    $firstItems.removeClass('active').removeAttr('style');
                    $secondItems.removeClass('active').removeAttr('style');
                    $firstID.addClass('active').animate({'opacity':'100'});
                });
                
                //first menu
                $('.es-first-item-box').on('click','.first_btn', function(){
                    var $that = $(this);
                    var $secondID = $('#'+$that.data('href'));
                    var $secondItems = $('.es-second-boxs .es-second-item-box');
                    var $group = $('.es-first-item-box').find('.first_btn');
                    $group.removeClass('active');
                    $that.addClass('active');
                    $secondItems.removeClass('active').removeAttr('style');
                    $secondID.addClass('active').animate({'opacity':'100'});
                })
                
                //預設開啟第一項
                $('.es-menu-iconboxs .es-menu-item:first-child').click();
            });
        </script>
		
<!--
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a>後台人員管理</a>>><a>群組</a>
		</div>
		<div class="searchbar header-style">
			<ul>
				<li class="search-field">
					<span class="label">代碼：</span>
					<input type="text" id="code" size="20"/>
				</li>
				<li class="search-field">
					<span class="label">名稱：</span>
					<input type="text" id="name" size="20"/>
				</li>
				<li class="button">
					<a>搜尋</a>
				</li>
			</ul>
		</div>
		<form class="form" id="form-edit">
			<div class="form-label">編輯資料</div>
			<div class="field">
				<label for="code">代碼：</label>
				<input name="code" type="text"/>
			</div>
			<div class="field">
				<label for="name">名稱：</label>
				<input name="name" type="text"/>
			</div>
			<div class="functions">
				<div class="button submit"><a>送出</a></div>
				<div class="button cancel"><a>取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<form class="form" id="form-add">
			<div class="form-label">新增資料</div>
			<div class="field">
				<label for="code">代碼：</label>
				<input name="code" type="text"/>
			</div>
			<div class="field">
				<label for="name">名稱：</label>
				<input name="name" type="text"/>
			</div>
			<div class="functions">
				<div class="button submit"><a>送出</a></div>
				<div class="button cancel"><a>取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"></li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th class="sortable">代碼<a class="sort init"></a></th>
								<th class="sortable">名稱<a class="sort asc"></a></th>
								<th class="sortable">新增日期<a class="sort desc"></a></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="icon"><a class="icon-edit icons"></a></td>
								<td class="icon"><a class="icon-delete icons"></a></td>
								<td class="column" name="code">abcdefr</td>
								<td class="column" name="name">暗黑破壞神三</td>
								<td class="column" name="insertt">2012-05-15</td>
							</tr>
						</tbody>
					</table>
				</li>
				<li class="footer">
					<ul>
						<li><a>最前頁</a></li>
						<li class="splitter"></li>
						<li><a>上一頁</a></li>
						<li class="splitter"></li>
						<li>&nbsp;[&nbsp;</li>
						<li>
							<a>1</a>
							<a>2</a>
							<a>3</a>
							<a>4</a>
							<a>5</a>
						</li>
						<li>&nbsp;]&nbsp;</li>
						<li class="splitter"></li>
						<li><a>下一頁</a></li>
						<li class="splitter"></li>
						<li><a>最後頁</a></li>
					</ul>
				</li>
			</ul>
		</div>
-->
	</body>
</html>