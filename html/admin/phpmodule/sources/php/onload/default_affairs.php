<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    
	    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	    
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/images.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/main.css" type="text/css">
	    
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-main.css" type="text/css">

		<!-- script src="http://code.jquery.com/jquery-1.10.1.min.js"></script -->
<!--		<script src="<?php echo $this->config->default_main; ?>/js/jquery.min.js"></script>-->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<script type="text/javascript">
		(function($){
			$.fn.bindFormAction = function() {
				return this.each(function() {
					var $this = $(this);

					if ($this.is('form')) {
						$this.find('.button.cancel').click(function(){
							$this.slideUp();
						});
					}
				});
			};
		})(jQuery);

		jQuery(document).ready(function($) {
			$('#left-control').click(function() {
				$(this).toggleClass('expanded collapsed');
				window.parent.toggleLeft();
			});
			$('.sortable').click(function() {
				$(this).children('a').toggleClass($(this).has('.init').length?'init asc':'asc desc');
			});
			$('.icon-edit').click(function(){
				var form = $('#form-edit').slideDown();

				$(this).parents('tr').children('.column').each(function() {
					form.find('input[name='+$(this).attr('name')+']').val($(this).text());
				});
			});
			$('#form-edit,#form-add').bindFormAction();
		});
		</script>
	</head>
	<body>
		<div class="es-menu-iconboxs d-flex flex-wrap">
            <div class="es-menu-item text-center" data-href="first1">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-users-cog fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">使用者系統</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first4">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-clipboard-check fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">訂單系統</div>
		    </div>
		    <div class="es-menu-item text-center" data-href="first7">
                <div class="fa-stack fa-2x">
                    <i class="fas fa-square fa-stack-2x"></i>
		            <i class="fas fa-american-sign-language-interpreting fa-stack-1x fa-inverse"></i>
                </div>
		        <div class="title">兌換中心系統</div>
		    </div>

		</div>
		<div class="es-first-boxs">
<!-- 生成 first tab content -->
		</div>
		<div class="es-second-boxs">
<!-- 生成 second tab content -->
		</div>
		
<!-- 對應層級 -->
		<script>
            var $islogin = "<?php echo $this->io->input['session']['user'] ?>" == '';
            var $loginUrl,$loginTitle;
            if($islogin){
                $loginUrl = "<?php echo $this->config->default_main; ?>/admin_user/login";
                $loginTitle = "登入系統";
            }else{
                $loginUrl = "<?php echo $this->config->default_main; ?>/admin_user/logout";
                $loginTitle = "登出系統";
            };
            
            var $menuItem = {
                first4:[
                    {title:'訂單相關管理',href:'',
                        item:[
                            {title:'訂單管理',href:'<?php echo $this->config->default_main; ?>/order'},
                        ]
                    }
                ],
                first7:[
                    {title:'商品卡相關管理',href:'',
                        item:[
                            {title:'商品卡管理',href:'<?php echo $this->config->default_main; ?>/exchange_card'}
                        ]
                    }
                ],

            }
            
            $(function(){
                //生成tab_content <div>
                $('.es-menu-iconboxs').find('.es-menu-item').each(function(){
                    var $that = $(this);
                    var $firstID = $that.data('href');
                    var $firstBox = $('.es-first-boxs');
                    var $secondBox = $('.es-second-boxs');
                    $firstBox.append(
                        $('<div class="es-first-item-box"/>')
                        .attr('id',$firstID)
                    )
                    $.map($menuItem,function(item, index){
                        if(index == $firstID){
                            var i = 0;
                            $.map(item,function(item, index){
                                if(item['href']==''){
                                    i++;
                                    var $second = 'second' + $firstID.replace('first','') +'-'+i;
                                    $('#'+$firstID).append(
                                        $('<div class="first_btn"/>')
                                        .attr('data-href',$second)
                                        .append(
                                            $('<a href="javascript:void(0);"/>')
                                            .text(item['title'])
                                        )
                                    );
                                    $secondBox.append(
                                        $('<div class="es-second-item-box"/>')
                                        .attr('id',$second)
                                    )
                                    $.map(item['item'],function(item, index){
                                        $('#'+$second).append(
                                            $('<div class="second_btn"/>')
                                            .append(
                                                $('<a href="'+item['href']+'"/>')
                                                .text(item['title'])
                                            )
                                        )
                                    })
                                }else{
                                    i++;
                                    $('#'+$firstID).append(
                                        $('<div class="first_btn"/>')
                                        .append(
                                            $('<a/>')
                                            .attr('href',item['href'])
                                            .text(item['title'])
                                        )
                                    );
                                }
                            })
                        }
                    })
                });
                
                //icon menu 切換按鈕
                $('.es-menu-iconboxs').on('click','.es-menu-item',function(){
                    var $that = $(this);
                    var $firstID = $('#'+$that.data('href'));
                    var $firstItems = $('.es-first-boxs .es-first-item-box');
                    var $group = $('.es-menu-iconboxs').find('.es-menu-item');
                    var $secondItems = $('.es-second-boxs .es-second-item-box');
                    $group.removeClass('active');
                    $that.addClass('active');
                    $firstItems.removeClass('active').removeAttr('style');
                    $secondItems.removeClass('active').removeAttr('style');
                    $firstID.addClass('active').animate({'opacity':'100'});
                });
                
                //first menu
                $('.es-first-item-box').on('click','.first_btn', function(){
                    var $that = $(this);
                    var $secondID = $('#'+$that.data('href'));
                    var $secondItems = $('.es-second-boxs .es-second-item-box');
                    var $group = $('.es-first-item-box').find('.first_btn');
                    $group.removeClass('active');
                    $that.addClass('active');
                    $secondItems.removeClass('active').removeAttr('style');
                    $secondID.addClass('active').animate({'opacity':'100'});
                })
                
                //預設開啟第一項
                $('.es-menu-iconboxs .es-menu-item:first-child').click();
            });
        </script>
		

	</body>
</html>