<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 


// Sort Start 
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(eghid|orderid|insertt)/";
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if($this->io->input["get"]["search_orderid"] != ''){
	$status["status"]["search"]["search_orderid"] = $this->io->input["get"]["search_orderid"] ;
	$status["status"]["search_path"] .= "&search_orderid=".$this->io->input["get"]["search_orderid"] ;
	$sub_search_query .=  "
		AND `orderid` ='".$this->io->input["get"]["search_orderid"]."'";
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	$sub_search_query .=  "
		AND `insertt` BETWEEN '".$this->io->input["get"]["search_btime"]."' AND '".$this->io->input["get"]["search_etime"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT count(*) as num
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_gift_history` p 
{$category_search_query}
WHERE 
p.prefixid = '{$this->config->default_prefix_id}' 
AND p.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT p.*
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_gift_history` p 
{$category_search_query}
WHERE 
p.prefixid = '{$this->config->default_prefix_id}' 
AND p.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 


// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$help_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_gift_history`', 
	`active` = '兌換禮券紀錄清單查詢', 
	`memo` = '{$help_data}', 
	`root` = 'exchange_gift_history/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################



$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();