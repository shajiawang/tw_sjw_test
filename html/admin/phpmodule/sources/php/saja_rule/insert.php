<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["srid"])) {
	$this->jsAlertMsg('下標資格ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('下標資格名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('下標資格排序錯誤!!');
}

$query ="
INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."rule`(
	`prefixid`,
	`srid`, 
	`name`,
	`seq`, 
	`switch`,
	`insertt`
) 
VALUES(
	'".$this->config->default_prefix_id."',
	'".$this->io->input["post"]["srid"]."',
	'".$this->io->input["post"]["name"]."',
	'".$this->io->input["post"]["seq"]."',
	'".$this->io->input["post"]["switch"]."',
	now()
)
" ;
//echo $query;exit;
$this->model->query($query);



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'saja_rule_insert', 
	`active` = '下標條件新增寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'saja_rule/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));