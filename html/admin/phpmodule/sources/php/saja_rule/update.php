<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["new_srid"])) {
	$this->jsAlertMsg('下標條件ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('下標條件名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('下標條件排序錯誤!!');
}

$query ="
UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."rule`
SET
`srid` = '".$this->io->input["post"]["new_srid"]."',
`name` = '".$this->io->input["post"]["name"]."',
`description` = '".$this->io->input["post"]["description"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND srid = '".$this->io->input["post"]["srid"]."'
" ;
$this->model->query($query);


##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'saja_rule_update', 
	`active` = '下標條件修改寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'saja_rule/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));