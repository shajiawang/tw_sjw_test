<?php
// Check Variable Start
if (empty($this->io->input["get"]["faqid"])) {
	$this->jsAlertMsg('問答ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];


##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_user}`.`{$this->config->default_prefix}faq`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `faqid` = '{$this->io->input["get"]["faqid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT c.* 
FROM `{$db_user}`.`{$this->config->default_prefix}faq_category` c 
WHERE 
	c.prefixid = '{$this->config->default_prefix_id}' 
	AND c.switch = 'Y'
	AND c.mod = 'b'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["faq_category"] = $recArr['table']['record'];
 // Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$tech_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'tech_edit', 
	`active` = '殺價密技問答修改', 
	`memo` = '{$tech_data}', 
	`root` = 'tech/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();