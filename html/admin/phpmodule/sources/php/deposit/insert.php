<?php
/* 鎖uid */
// if ($this->io->input['session']['user']['userid'] != '1' && ($this->io->input['session']['user']['userid'] != '2') && ($this->io->input['session']['user']['userid'] != '3') && ($this->io->input['session']['user']['userid'] != '26') && ($this->io->input['session']['user']['userid'] != '29')) {
// 	$this->jsAlertMsg('您無權限登入！');
// }
// Check Variable Start
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員編號錯誤!!');
}
$userid=htmlspecialchars(trim($this->io->input["post"]["userid"]));
// if (empty($this->io->input["post"]["countryid"])) {
// 	$this->jsAlertMsg('國別ID錯誤!!');
// }
if (empty($this->io->input["post"]["currency"])) {
	$this->jsAlertMsg('幣別錯誤!!');
}

/*if (empty($this->io->input["post"]["account"])) {
	$this->jsAlertMsg('儲值帳號錯誤!!');
}*/

// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

error_log("[deposit/insert] drid:".$this->io->input["post"]["drid"]);
error_log("[deposit/insert] driid:".$this->io->input["post"]["driid"]);
error_log("[deposit/insert] HTTP_ORIGIN :".$_SERVER['HTTP_ORIGIN']);



##############################################################################################################################################
// Insert Start

$query = "select * from `{$db_user}`.`{$this->config->default_prefix}admin_user` 
where 
	`prefixid`='{$this->config->default_prefix_id}' 
	and `userid` = '{$userid}'
	and `switch` = 'Y'
";
$recAdminUser = $this->model->getQueryRecord($query);

$insert_str = "";
$drid = $this->io->input["post"]["drid"];

if ($this->io->input["post"]["driid"] != '999'){
    // 儲值規定的金額
	$query = "select dri.*, dr.act from `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri 
	left join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr on 
		dri.prefixid = dr.prefixid
		and dri.drid = dr.drid
		and dr.switch = 'Y'
	where 
		dri.`prefixid`='{$this->config->default_prefix_id}' 
		and dri.`driid` = '{$this->io->input["post"]["driid"]}' 
	";
	$table = $this->model->getQueryRecord($query);

	if ($drid=="17" || $drid=="20" || $drid=="23") {
		$insert_str = " ,`ori_currency` = '{$this->io->input["post"]["ori_currency"]}'
						,`ori_amount` = '{$table["table"]["record"][0]["amount"]}'";
		$table["table"]["record"][0]["amount"] = $table["table"]["record"][0]["spoint"];
	}

} else {
	// 儲值非規定的金額
    $query = "select dr.*
	from `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr 
	where 
		dr.`prefixid`='{$this->config->default_prefix_id}' 
		and dr.`drid` = '{$this->io->input["post"]["drid"]}' 
	";
	$table = $this->model->getQueryRecord($query);
	
	
	$table["table"]["record"][0]["driid"] = 999;

	if ($drid=="17" || $drid=="20" || $drid=="23") {
		$table["table"]["record"][0]["spoint"] = $this->io->input["post"]["num"];
		$table["table"]["record"][0]["amount"] = $this->io->input["post"]["num"];
		$insert_str = " ,`ori_currency` = '{$this->io->input["post"]["ori_currency"]}'
						,`ori_amount` = '{$this->io->input["post"]["ori_amount"]}'";
	}else{
		$table["table"]["record"][0]["amount"] = $this->io->input["post"]["other"];
		$table["table"]["record"][0]["spoint"] = $this->io->input["post"]["total"];
	}
	
}
$query = "
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` SET 
	`prefixid` = '{$this->config->default_prefix_id}', 
	`userid` = '{$userid}', 
	`behav` = '{$this->io->input["post"]["behav"]}', 
	`currency` = '{$this->io->input["post"]["currency"]}', 
	`amount` = '{$table["table"]["record"][0]["amount"]}', 
	`seq`='0', 
	`switch`='Y', 
	`insertt`=now()
".$insert_str ; 

$this->model->query($query);
$depositid = $this->model->_con->insert_id;

$query = "
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}spoint` SET 
	`prefixid`='{$this->config->default_prefix_id}', 
	`userid` = '{$userid}', 
	`behav` = '{$this->io->input["post"]["behav"]}', 
	`amount` = '{$table["table"]["record"][0]["spoint"]}', 
	`seq`='0', 
	`switch`='Y', 
	`insertt`=now()
" ; 
$this->model->query($query);
$spointid = $this->model->_con->insert_id;

$query = "
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` SET 
	`prefixid`='{$this->config->default_prefix_id}', 
	`userid` = '{$userid}', 
	`driid` = '{$table["table"]["record"][0]["driid"]}', 
	`depositid` = '{$depositid}', 
	`spointid` = '{$spointid}', 
	`status` = 'order', 
	`seq`='0', 
	`switch`='Y', 
	`insertid` = '{$recAdminUser['table']['record'][0]['userid']}', 
	`inserttype` = 'Admin_User', 
	`insertname` = '{$recAdminUser['table']['record'][0]['name']}', 
	`insertt` = now(),
	`modifierid` = '{$recAdminUser['table']['record'][0]['userid']}', 
	`modifiertype` = 'Admin_User', 
	`modifiername` = '{$recAdminUser['table']['record'][0]['name']}'
" ; 
$this->model->query($query);
$dhid = $this->model->_con->insert_id;

if ($this->io->input["post"]["is_other"] == 'N'){
	$sign=MD5(time()."|".$this->io->input["post"]["userid"]."|".$depositid."|sjW333-_@");
    
    // 送出json內容並取回結果
	error_log("[admin/deposit] call createInvoiceViaSign : ".$_SERVER['HTTP_ORIGIN'].'/site/deposit/createInvoiceViaSign/?userid='.$this->io->input["post"]["userid"].'&depositid='.$depositid."&sign=".$sign);
    $response = file_get_contents($_SERVER['HTTP_ORIGIN'].'/site/deposit/createInvoiceViaSign/?userid='.$userid.'&depositid='.$depositid."&sign=".$sign);
	$arr = json_decode($response,true);
}

/*
if (empty($this->io->input["post"]["account"]) && $table['table']['record'][0]['act'] != 'weixinpay') {
	$this->jsAlertMsg('帳戶錯誤!!');
}
*/

if ($table['table']['record'][0]['act'] == 'alipay') {
	$data = "`data` = '{\"out_trade_no\":\"".$dhid."\", \"trade_no\":\"".$this->io->input['post']['account']."\", \"result\":\"".$this->io->input['post']['account']."\", \"timepaid\":\"".date("YmdHis")."\", \"paymenttype\":\"ALIPAY_WAP\"}',";
}
else if ($table['table']['record'][0]['act'] == 'bankcomm') {
	$data = "`data` = '{\"out_trade_no\":\"".$dhid."\", \"trade_no\":\"".$this->io->input['post']['account']."\", \"result\":\"1\", \"timepaid\":\"".date("YmdHis")."\", \"OrderMono\":\"OdrID:".$dhid."/OdrTime:".date("YmdHis")."/Pay:".$table['table']['record'][0]['amount']."/Spts:".$table['table']['record'][0]['spoint']."\", \"paymenttype\":\"BANKCOMM_WAP\"}',";
}
else if ($table['table']['record'][0]['act'] == 'weixinpay') {
	$data = "`data` = '{\"out_trade_no\":\"".$dhid."\", \"trade_no\":\"\", \"result\":\"get_brand_wcpay_request:ok\", \"timepaid\":\"".date("YmdHis")."\", \"paymenttype\":\"WEIXIN_WAP\"}',";
}
else if ($table['table']['record'][0]['act'] == 'hinet') {
	$data = "`data` = '{\"out_trade_no\":\"".$dhid."\", \"spoints\":\"".$table['table']['record'][0]['spoint']."\", \"userid\":\"".$userid."\", \"serialno\":\"".$this->io->input['post']['account']."\", \"paymenttype\":\"HINET_PTS\", \"timepaid\":\"".date("YmdHis")."\"}',";
}
else if ($table['table']['record'][0]['act'] == 'ibonpay') {
	$checksum =strtolower(md5($this->config->ibonpay['merchantnumber'].$dhid.$this->config->ibonpay['code'].$table['table']['record'][0]['amount']));
	$data = "`data` = '{\"rc\":\"0\",\"amount\":\"".$table['table']['record'][0]['amount']."\",\"merchantnumber\":\"".$this->config->ibonpay['merchantnumber']."\",\"ordernumber\":\"".$dhid."\",\"paycode\":\"".$this->io->input['post']['account']."\",\"checksum\":\"".$checksum."\"}',";
}
else {
	if ($drid=="17" || $drid=="20" || $drid=="23") {
		$ratio = 0;
		$tax = 0;
	}else{
		$ratio = $this->io->input['post']['ratio'];
		$tax = $this->io->input['post']['tax'];
		
	}
	$data = "`data` = '{\"name\":\"".$table['table']['record'][0]['act']."\",\"amount\":\"".$table['table']['record'][0]['amount']."\",\"drid\":\"".$this->io->input['post']['drid']."\",\"tax\":\"".$tax."\",\"ratio\":\"".$ratio."\",\"is_other\":\"".$this->io->input['post']['is_other']."\"}',";
}


$query = "
update `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` set 
	`status` = 'deposit', 
	{$data}
	`modifierid` = '{$recAdminUser['table']['record'][0]['userid']}', 
	`modifiertype` = 'Admin_User', 
	`modifiername` = '{$recAdminUser['table']['record'][0]['name']}', 
	`modifyt`= NOW()
where 
	`prefixid` = '{$this->config->default_prefix_id}' 
	and `dhid` = '{$dhid}'
" ; 
$this->model->query($query);

//送殺價券/超級殺價券
if ($this->io->input["post"]["status"] == 'deposit' &&  $this->io->input["post"]["driid"] != "999") {
	
    $query = "SELECT rt.*, sp.name spname, sp.productid 
	FROM `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote_rt` rt 
	left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote` sp on
		rt.prefixid = sp.prefixid 
		and rt.spid = sp.spid 
		and sp.ontime <= NOW()
		and sp.offtime > NOW()
		and sp.switch = 'Y'
	WHERE
		rt.`prefixid`='{$this->config->default_prefix_id}' 
		and rt.driid = '{$table['table']['record'][0]['driid']}'
		and rt.behav = 'c'
		and rt.switch = 'Y'
	";
	$recArr = $this->model->getQueryRecord($query);
	
	if (is_array($recArr['table']['record'])) {
		foreach ($recArr['table']['record'] as $key => $value) {
			if ($value['productid'] == 0) {
				$query = "
				INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode` SET 
					`prefixid`='{$this->config->default_prefix_id}', 
					`userid` = '{$userid}', 
					`spid` = '{$value["spid"]}', 
					`behav` = 'c', 
					`productid` = '0',
					`amount` = '{$value["num"]}', 
					`remainder` = '{$value["num"]}', 
					`closed` = 'N', 
					`seq`='0', 
					`switch`='Y', 
					`insertt`=now()
				"; 
				$this->model->query($query);
				$scodeid = $this->model->_con->insert_id;
				
				$query = "
				INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode_history` SET 
					`prefixid`='{$this->config->default_prefix_id}', 
					`userid` = '{$userid}', 
					`scodeid` = '{$scodeid}', 
					`spid` = '{$value["spid"]}', 
					`promote_amount` = '{$value["amount"]}', 
					`num` = '{$value["num"]}', 
					`memo` = '{$value["spname"]}', 
					`batch` = '0', 
					`seq`='0', 
					`switch`='Y', 
					`insertt`=now()
				"; 
				$this->model->query($query);
			} else {
				for($i = 0; $i < $value['num']; $i++) {
					$query = "INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}oscode` 
					SET 
						prefixid = '{$this->config->default_prefix_id}', 
						userid = '{$userid}', 
						productid = '{$value['productid']}', 
						spid = '{$value['spid']}', 
						behav = 'user_deposit', 
						used = 'N',
						amount = '1', 
						verified = 'N',
						seq = '0', 
						switch = 'Y',
						insertt=NOW()
					";
					$this->model->query($query);
				}
			}
		}
	
		//取得首充資料
		$query2 = " SELECT count(d.depositid) as num
		FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` d
		LEFT JOIN `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh ON 
			d.prefixid = dh.prefixid AND d.depositid = dh.depositid AND d.switch = 'Y' 
		left join `{$db_cash_flow}`.`{$this->config->default_prefix}spoint` sp ON 
			dh.prefixid = sp.prefixid and dh.spointid = sp.spointid and dh.switch = 'Y' 
		WHERE
			d.prefixid = '{$this->config->default_prefix_id}'
			AND d.userid = '{$userid}'
			AND d.switch = 'Y'
			AND d.depositid IS NOT NULL
		";				
		$recArr2 = $this->model->getQueryRecord($query2);
		
		//判斷是否為首充
		if ($recArr2['table']['record'][0]['num'] == 1){
			foreach ($recArr['table']['record'] as $key => $value) {
				if ($value['productid'] == 0) {
					$query = "
					INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode` SET 
						`prefixid`='{$this->config->default_prefix_id}', 
						`userid` = '{$userid}', 
						`spid` = '{$value["spid"]}', 
						`behav` = 'c', 
						`productid` = '0',
						`amount` = '{$value["num"]}', 
						`remainder` = '{$value["num"]}', 
						`closed` = 'N', 
						`seq`='0', 
						`switch`='Y', 
						`insertt`=now()
					"; 
					$this->model->query($query);
					$scodeid = $this->model->_con->insert_id;
					
					$query = "
					INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode_history` SET 
						`prefixid`='{$this->config->default_prefix_id}', 
						`userid` = '{$userid}', 
						`scodeid` = '{$scodeid}', 
						`spid` = '{$value["spid"]}', 
						`promote_amount` = '{$value["amount"]}', 
						`num` = '{$value["num"]}', 
						`memo` = '{$value["spname"]}', 
						`batch` = '0', 
						`seq`='0', 
						`switch`='Y', 
						`insertt`=now()
					"; 
					$this->model->query($query);
				} else {
					for($i = 0; $i < $value['num']; $i++) {
						$query = "INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}oscode` 
						SET 
							prefixid = '{$this->config->default_prefix_id}', 
							userid = '{$userid}', 
							productid = '{$value['productid']}', 
							spid = '{$value['spid']}', 
							behav = 'user_deposit', 
							used = 'N',
							amount = '1', 
							verified = 'N',
							seq = '0', 
							switch = 'Y',
							insertt=NOW()
						";
						$this->model->query($query);
					}
				
				}
			}
		}	
	}
}

// Insert End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$userid}',
	`kind` = 'deposit_insert', 
	`active` = '儲值新增寫入', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));