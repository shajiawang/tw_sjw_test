<?php
// Check Variable Start
/* 鎖uid */

if (($this->io->input['session']['user']['userid'] != '1') && ($this->io->input['session']['user']['userid'] != '2') && ($this->io->input['session']['user']['userid'] != '3') && ($this->io->input['session']['user']['userid'] != '8') && ($this->io->input['session']['user']['userid'] != '26') && ($this->io->input['session']['user']['userid'] != '27')) {
	$this->jsAlertMsg('您無權限登入！');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

//Relation country
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}country` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country_rt"] = $recArr['table']['record'];

//儲值規則
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["deposit_rule"] = $recArr['table']['record'];

//儲值金額
if (is_array($table["table"]["deposit_rule"]))
{
	foreach ($table["table"]["deposit_rule"] as $key => $value)
	{
		$query ="
		SELECT * 
		FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` 
		WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		and `drid` = '{$value['drid']}' 
		AND `switch` = 'Y'
		";
		$recArr = $this->model->getQueryRecord($query);
		
		$table["table"]["deposit_rule_item"][$value['drid']] = $recArr['table']['record'];
	}
}

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_add', 
	`active` = '儲值新增', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('deposit_rule_item' , json_encode($table['table']["deposit_rule_item"])) ;
//$this->tplVar('status',$status["status"]);
$this->display();