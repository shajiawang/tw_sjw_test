<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1];
	}
}

//搜尋條件
$query_search = '';
$sub_group_query =  " group by d.depositid";
$sub_sort_query =  " ORDER BY u.userid, d.modifyt";
if(!empty($data["joindatefrom"])){			//加入起始日期
	$sub_search_query .=  " AND u.`insertt` >= '".$data["joindatefrom"]."'";
}
if(!empty($data["joindateto"])){			//加入結束日期
	$sub_search_query .=  " AND u.`insertt` < '".$data["joindateto"]."'";
}
if(!empty($data["depositdatefrom"])){		//儲值起始日期
	$sub_search_query .=  " AND d.`modifyt` >= '".$data["depositdatefrom"]."'";
}
if(!empty($data["depositdateto"])){			//儲值結束日期
	$sub_search_query .=  " AND d.`modifyt` < '".$data["depositdateto"]."'";
}
if(!empty($data["userid"])){					//會員編號
	$sub_search_query .=  " AND u.`userid` = '".$data["userid"]."'";
}
if(!empty($data["name"])){					//會員帳號
	$sub_search_query .=  " AND u.`name` like '%".$data["name"]."%'";
}
if(!empty($data["depositfrom"])){			//儲值方式
	$sub_search_query .=  " AND dr.`act` < '".$data["depositfrom"]."'";
}
if(!empty($data["total_amount"])){			//儲值金額
	$sub_search_query .=  " AND total_amount < '".$data["total_amount"]."'";
}
if(!empty($data["gender"])){				//性別
	$sub_search_query .=  " AND up.`gender` < '".$data["gender"]."'";
}
if(!empty($data["usergroup"])){				//可以勾選已會員帳號為群組(沒有勾選則為流水帳)
	if($data["usergroup"] == 1){
		$sub_group_query =  " group by u.userid";
	}
	else {
		$sub_group_query =  " group by d.depositid";
	}
}
if(!empty($data["nickname"])){					//會員帳號
	$sub_search_query .=  " AND up.`nickname` like '%".$data["nickname"]."%'";
}
if(!empty($data["dhid"])){				//儲值記錄編號
	$sub_search_query .=  " AND dh.`dhid` = '".$data["dhid"]."'";
}

$query = "SELECT dh.*, d.countryid, (dr.name) as behav, d.currency, d.amount, d.ori_currency, d.ori_amount,
                 up.nickname, c.name as cname , u.name login_name, u.insertt login_insertt
from `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` d 
on
	dh.prefixid = d.prefixid 
	and dh.depositid = d.depositid 
left outer join `{$db_user}`.`{$this->config->default_prefix}user_profile` up 
on
	up.prefixid = d.prefixid 
	and up.userid = d.userid 
left outer join `{$db_user}`.`{$this->config->default_prefix}user` u 
on
	u.prefixid = d.prefixid 
	and u.userid = d.userid 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri
on
    dh.driid=dri.driid
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
on
    dr.drid=dri.drid
left outer join `{$db_channel}`.`{$this->config->default_prefix}country` c
on
    c.countryid=d.countryid	
where
	dh.`prefixid` = '{$this->config->default_prefix_id}' 
	and dh.depositid IS NOT NULL
	and dh.dhid is not null
	and up.userid IS NOT NULL
	and dh.switch = 'Y'
";
$query .= $sub_search_query;
$query .= $sub_group_query;
$query .= $sub_sort_query;
$table = $this->model->getQueryRecord($query);

foreach($table["table"]['record'] as $rk => $rv)
{
	if ($rv['behav'] == ''){
		$data = json_decode($rv['data'],true);
		// print_r($data);
		$query = "SELECT name 
		FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr 
		WHERE 
			dr.`prefixid` = '{$this->config->default_prefix_id}' 
			AND dr.drid = '{$data['drid']}' 
			AND dr.`switch`='Y' 
		" ;
		$rulename = $this->model->getQueryRecord($query);		
		
		$table["table"]['record'][$rk]['behav'] = $rulename["table"]['record'][0]['name'];
		$table["table"]['record'][$rk]['drid'] = $data['drid'];
	}
	
	$query = "select ifnull(s.amount,0) as spoints_by_deposit 
	from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s 
	where 
		s.prefixid = '{$this->config->default_prefix_id}' 
		and s.userid = '{$rv['userid']}'
		and s.spointid = '{$rv['spointid']}'
		and s.switch = 'Y' 
		and s.behav='user_deposit'
	";
	$spoints_by_deposit = $this->model->getQueryRecord($query);	
	$table["table"]['record'][$rk]['spoints_by_deposit'] = $spoints_by_deposit["table"]['record'][0]['spoints_by_deposit'];
	
}

##############################################################################################################################################
// Log Start
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_export',
	`active` = '儲值帳戶清單匯出',
	`memo` = '{$deposit_data}',
	`root` = 'deposit/export',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//會員資料表
$Report_Array[0]['title'] = array(
								"A1" => "會員編號", 
								"B1" => "帳號", 
								"C1" => "暱稱", 
								"D1" => "加入日期", 
								"E1" => "儲值單號", 
								"F1" => "儲值記錄單號", 
								"G1" => "儲值日期", 
								"H1" => "儲值方式", 
								"I1" => "儲值狀態", 
								"J1" => "台幣儲值金額", 
								"K1" => "外幣幣別", 
								"L1" => "外幣儲值金額", 
								"M1" => "稅額", 
								"N1" => "獲得殺價幣", 
								"O1" => "修改者", 
								"P1" => "來源位置",
								"Q1" => "儲值明細"
							);
//儲值方式
$depositfromArr = array('alipay' => '支付寶', 'bankcomm' => '銀聯支付', 'weixinpay' => '微信支付');
//儲值狀態
$arrDef = array('order'=>'待付款', 'bid'=>'得標發送', 'deposit'=>'已儲值', 'cancel'=>'取消儲值', 'writeoff' =>'已銷帳', 'error' =>'儲值錯誤');
//性別
$genderArr = array('male' => '男', 'female' => '女');

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value);
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
		
		if ( $value['drid']==12 || $value['drid']==13) {
			$spoints_by_deposit = round(intval($value['spoints_by_deposit'] * 5) / 100);
		}else if($value['drid']==8){
			$spoints_by_deposit = round(intval($value['spoints_by_deposit'] * 8) / 100);
		}else{
			$spoints_by_deposit = 0;
		}
		
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['userid']);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$row, (string)$value['login_name'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['nickname'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['login_insertt']);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['depositid']);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['dhid']);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['insertt']);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['behav']);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $arrDef[$value['status']]);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$row, intval($value['amount'] * 100) / 100);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['ori_currency']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$row, intval($value['ori_amount'] * 100) / 100);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $spoints_by_deposit);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$row, round(intval($value['spoints_by_deposit'] * 100) / 100));
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $value['modifiername']);
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$row, $value['src_ip']);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$row, (string)$value['data'],PHPExcel_Cell_DataType::TYPE_STRING);

		// $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $genderArr[$value['gender']]);
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('儲值帳戶');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="儲值帳戶'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');

// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>