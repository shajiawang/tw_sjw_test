<?php
// Check Variable Start
if (empty($this->io->input["get"]["dhid"])) {
	$this->jsAlertMsg('儲值帳目ID錯誤!!');
}
/* 鎖uid */
if (($this->io->input['session']['user']['userid'] != '1') && ($this->io->input['session']['user']['userid'] != '2') && ($this->io->input['session']['user']['userid'] != '3') && ($this->io->input['session']['user']['userid'] != '8')) {
	$this->jsAlertMsg('您無權限登入！');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT dh.*, d.amount, d.behav, d.countryid, d.currency 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` d 
on
	d.prefixid = dh.prefixid 
	and d.depositid = dh.depositid 
	and d.behav = 'user_deposit'
WHERE 
	dh.`prefixid` = '{$this->config->default_prefix_id}' 
	AND dh.`switch`='Y' 
	AND dh.`dhid` = '{$this->io->input["get"]["dhid"]}'
" ;
$table = $this->model->getQueryRecord($query);

if ($table["table"]["record"][0]['status'] == 'cancel') {
	$this->jsAlertMsg('此儲值帳戶已取消，不可再變動!!');
}
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

//Relation country
//儲值規則
$query ="
SELECT dr.* 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri 
on 
	dri.prefixid = dr.prefixid 
	and dri.drid = dr.drid 
	and dri.switch = 'Y' 
WHERE 
	dr.`prefixid` = '{$this->config->default_prefix_id}'
	AND dr.`switch` = 'Y'
	and dri.`driid` = '{$table['table']['record'][0]['driid']}'
";
$recdrArr = $this->model->getQueryRecord($query);
$table["table"]["deposit_rule"] = $recdrArr['table']['record'];

$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}country` 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `switch` = 'Y'
";
$recCountry = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country_rt"] = $recCountry['table']['record'];

// Relation End 
##############################################################################################################################################

##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_edit', 
	`active` = '儲值修改', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();