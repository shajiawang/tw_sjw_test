<?php
// Check Variable Start
if (empty($this->io->input["post"]["dhid"])) {
	$this->jsAlertMsg('儲值帳目ID錯誤!!');
}
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員ID錯誤!!');
}
if (empty($this->io->input["post"]["countryid"])) {
	$this->jsAlertMsg('國別ID錯誤!!');
}
if (empty($this->io->input["post"]["currency"])) {
	$this->jsAlertMsg('幣別錯誤!!');
}
/* 鎖uid */
// if ($this->io->input['session']['user']['userid'] != '1' && ($this->io->input['session']['user']['userid'] != '2') && ($this->io->input['session']['user']['userid'] != '3') && ($this->io->input['session']['user']['userid'] != '26') && ($this->io->input['session']['user']['userid'] != '29')) {
// 	$this->jsAlertMsg('您無權限登入！');
// }
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
// Update Start
$query = "select dh.*, dr.act, dri.amount, dri.spoint from `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh 
left join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri on 
	dh.prefixid = dri.prefixid 
	and dh.driid = dri.driid
	and dri.switch = 'Y'
left join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr on 
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
	and dr.switch = 'Y'
where 
	dh.`prefixid` = '{$this->config->default_prefix_id}' 
	AND dh.`dhid` = '{$this->io->input["post"]["dhid"]}'
";
$table = $this->model->getQueryRecord($query);

if ($this->io->input["post"]["status"] != $this->io->input["post"]["oldstatus"])
{
	if ($this->io->input["post"]["status"] == 'cancel') {
		$switch = 'N';
		$data = '';
	} 
	else if ($this->io->input["post"]["status"] == 'deposit') {
		if (empty($this->io->input["post"]["account"]) && $table['table']['record'][0]['act'] != 'weixinpay') {
			$this->jsAlertMsg('帳戶錯誤!!');
		}
		
		$switch = 'Y';
		if ($table['table']['record'][0]['act'] == 'alipay') {
			$data = "`data` = '{\"out_trade_no\":\"".$this->io->input['post']['dhid']."\", \"trade_no\":\"".$this->io->input['post']['account']."\", \"result\":\"".$this->io->input['post']['account']."\", \"timepaid\":\"".date("YmdHis")."\", \"paymenttype\":\"ALIPAY_WAP\"}',";
		}
		else if ($table['table']['record'][0]['act'] == 'bankcomm') {
			$data = "`data` = '{\"out_trade_no\":\"".$this->io->input['post']['dhid']."\", \"trade_no\":\"".$this->io->input['post']['account']."\", \"result\":\"1\", \"timepaid\":\"".date("YmdHis")."\", \"OrderMono\":\"OdrID:".$this->io->input['post']['dhid']."/OdrTime:".$table['table']['record'][0]['insertt']."/Pay:".$table['table']['record'][0]['amount']."/Spts:".$table['table']['record'][0]['spoint']."\", \"paymenttype\":\"BANKCOMM_WAP\"}',";
		}
		else if ($table['table']['record'][0]['act'] == 'weixinpay') {
			$data = "`data` = '{\"out_trade_no\":\"".$this->io->input['post']['dhid']."\", \"trade_no\":\"\", \"result\":\"get_brand_wcpay_request:ok\", \"timepaid\":\"".date("YmdHis")."\", \"paymenttype\":\"WEIXIN_WAP\"}',";
		}
		else if ($table['table']['record'][0]['act'] == 'hinet') {
			$data = "`data` = '{\"out_trade_no\":\"".$this->io->input['post']['dhid']."\", \"spoints\":\"".$table['table']['record'][0]['spoint']."\", \"userid\":\"".$table['table']['record'][0]['userid']."\", \"serialno\":\"".$this->io->input['post']['account']."\", \"paymenttype\":\"HINET_PTS\", \"timepaid\":\"".date("YmdHis")."\"}',";
		}
		else if ($table['table']['record'][0]['act'] == 'ibonpay') {
			$checksum =strtolower(md5($this->config->ibonpay['merchantnumber'].$this->io->input['post']['dhid'].$this->config->ibonpay['code'].$table['table']['record'][0]['amount']));
			$data = "`data` = '{\"rc\":\"0\",\"amount\":\"".$table['table']['record'][0]['amount']."\",\"merchantnumber\":\"".$this->config->ibonpay['merchantnumber']."\",\"ordernumber\":\"".$this->io->input['post']['dhid']."\",\"paycode\":\"".$this->io->input['post']['account']."\",\"checksum\":\"".$checksum."\"}',";
		}
	}
	
	$query = "
	update `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` SET 
		`status` = '{$this->io->input['post']['status']}', 
		{$data}
		`modifierid` = '{$this->io->input['session']['user']['userid']}',
		`modifiertype` = 'Admin_User',
		`modifiername` = '{$this->io->input['session']['user']['name']}',
		`modifyt` = NOW()
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `dhid` = '{$table['table']['record'][0]['dhid']}'
	";
	$this->model->query($query);
	
	$query = "
	UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` SET 
		`countryid` = '{$this->io->input["post"]["countryid"]}', 
		`currency` = '{$this->io->input["post"]["currency"]}', 
		`switch` = '{$switch}', 
		`modifyt` = NOW()
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `depositid` = '{$table['table']['record'][0]['depositid']}'
	" ;
	$this->model->query($query);
	
	$query = "
	UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}spoint` SET 
		`countryid` = '{$this->io->input["post"]["countryid"]}', 
		`switch` = '{$switch}', 
		`modifyt` = NOW()
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `spointid` = '{$table['table']['record'][0]['spointid']}'
	" ;
	$this->model->query($query);
	
	if ($this->io->input["post"]["status"] == 'deposit') {
		$query = "SELECT rt.*, sp.name spname, sp.productid 
		FROM `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote_rt` rt 
		left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote` sp on
			rt.prefixid = sp.prefixid 
			and rt.spid = sp.spid 
			and sp.ontime <= NOW()
			and sp.offtime > NOW()
			and sp.switch = 'Y'
		WHERE
			rt.`prefixid`='{$this->config->default_prefix_id}' 
			and rt.driid = '{$table['table']['record'][0]['driid']}'
			and rt.behav = 'c'
			and rt.switch = 'Y'
		";
		$recArr = $this->model->getQueryRecord($query);
		
		if (is_array($recArr['table']['record'])) {
			foreach ($recArr['table']['record'] as $key => $value) {
				if ($value['productid'] == 0) {
					$query = "
					INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode` SET 
						`prefixid`='{$this->config->default_prefix_id}', 
						`userid` = '{$this->io->input["post"]["userid"]}', 
						`spid` = '{$value["spid"]}', 
						`behav` = 'c', 
						`productid` = '0',
						`amount` = '{$value["num"]}', 
						`remainder` = '{$value["num"]}', 
						`closed` = 'N', 
						`seq`='0', 
						`switch`='Y', 
						`insertt`=now()
					" ; 
					$this->model->query($query);
					$scodeid = $this->model->_con->insert_id;
					
					$query = "
					INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode_history` SET 
						`prefixid`='{$this->config->default_prefix_id}', 
						`userid` = '{$this->io->input["post"]["userid"]}', 
						`scodeid` = '{$scodeid}', 
						`spid` = '{$value["spid"]}', 
						`promote_amount` = '{$value["amount"]}', 
						`num` = '{$value["num"]}', 
						`memo` = '{$value["spname"]}', 
						`batch` = '0', 
						`seq`='0', 
						`switch`='Y', 
						`insertt`=now()
					" ; 
					$this->model->query($query);
				}
				else {
					for($i = 0; $i < $value['num']; $i++)
					{
					
						$query = "INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}oscode` 
						SET 
							prefixid = '{$this->config->default_prefix_id}', 
							userid = '{$this->io->input['post']['userid']}', 
							productid = '{$value['productid']}', 
							spid = '{$value['spid']}', 
							behav = 'user_deposit', 
							used = 'N',
							amount = '1', 
							verified = 'Y',
							seq = '0', 
							switch = 'Y',
							insertt=NOW()
						";
						$this->model->query($query);
					}
				}
			}
		}
	}
}

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit_update', 
	`active` = '儲值修改寫入', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



if ($this->io->input["post"]["status"] == 'cancel') {
	$this->jsPrintMsg('狀態改為取消儲值，請手動將S碼移除。', $this->io->input['post']['location_url']);
} else {
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
}