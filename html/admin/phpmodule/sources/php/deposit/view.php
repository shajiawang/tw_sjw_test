<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(dhid|userid|modifyt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = " dh.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
} else {
	$sub_sort_query =  " ORDER BY d.depositid DESC "; 
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
$output_control = 'false';//輸出資料控制旗標(true/false)預設false
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "
		AND d.`userid` = '".$this->io->input["get"]["search_userid"]."'";
	$output_control = 'true';
}
if($this->io->input["get"]["search_countryid"] != ''){
	$status["status"]["search"]["search_countryid"] = $this->io->input["get"]["search_countryid"] ;
	$status["status"]["search_path"] .= "&search_countryid=".$this->io->input["get"]["search_countryid"] ;
	$sub_search_query .=  "
		AND d.`countryid` = '".$this->io->input["get"]["search_countryid"]."'";
	$output_control = 'true';
}
if($this->io->input["get"]["startdatetime"] != ''){
	$status["status"]["search"]["startdatetime"] = $this->io->input["get"]["startdatetime"] ;
	$status["status"]["search_path"] .= "&startdatetime=".$this->io->input["get"]["startdatetime"] ;
	$sub_search_query .=  "
		AND d.`modifyt` >= '".$this->io->input["get"]["startdatetime"]."'";
	$output_control = 'true';
}
if($this->io->input["get"]["enddatetime"] != ''){
	$status["status"]["search"]["enddatetime"] = $this->io->input["get"]["enddatetime"] ;
	$status["status"]["search_path"] .= "&enddatetime=".$this->io->input["get"]["enddatetime"] ;
	$sub_search_query .=  "
		AND d.`modifyt` <= '".$this->io->input["get"]["enddatetime"]."'";
	$output_control = 'true';
}
if($this->io->input["get"]["deposit_rule_status"] != ''){
	$status["status"]["search"]["deposit_rule_status"] = $this->io->input["get"]["deposit_rule_status"] ;
	$status["status"]["search_path"] .= "&deposit_rule_status=".$this->io->input["get"]["deposit_rule_status"] ;
	IF($this->io->input["get"]["deposit_rule_status"] != 'ALL'){
		$sub_search_query .=  "
			AND dri.`drid` = '".$this->io->input["get"]["deposit_rule_status"]."'";
		$category_search_query .= "
			LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri ON
			dri.driid = dh.driid
			AND dri.switch = 'Y'
		";
	}
	$output_control = 'true';
}
if($this->io->input["get"]["deposit_history_status"] != ''){
	$status["status"]["search"]["deposit_history_status"]=$this->io->input["get"]["deposit_history_status"];
	$status["status"]["search_path"] .= "&deposit_history_status=".$this->io->input["get"]["deposit_history_status"] ;
	IF($this->io->input["get"]["deposit_history_status"] != 'ALL'){
		$sub_search_query .=  "
		AND dh.`status` = '".$this->io->input["get"]["deposit_history_status"]."'";
	}
	$output_control = 'true';
} //else {
//     // 預設撈已儲值的
// 	$this->io->input["get"]["deposit_history_status"]='deposit';
// 	$status["status"]["search"]["deposit_history_status"]=$this->io->input["get"]["deposit_history_status"];
// 	$status["status"]["search_path"] .= "&deposit_history_status=".$this->io->input["get"]["deposit_history_status"] ;
//     $sub_search_query .=  "
// 	     AND dh.`status` = '".$this->io->input["get"]["deposit_history_status"]."'";
// 	$output_control = 'true';
// }

if($this->io->input["get"]["tester"] != ''){			//是否加入測試人員
	$status["status"]["search"]["tester"] = $this->io->input["get"]["tester"];
	$status["status"]["search_path"] .= "&tester=".$this->io->input["get"]["tester"];
	IF(!empty($this->io->input["get"]["tester"])){
		$sub_search_query .=  "
			AND d.`userid` not in (28, 116)";
		/*$sub_search_query .=  "
			AND d.`userid` not in (1, 3, 9, 28, 116, 507, 585, 586, 588)";*/
	}
	$output_control = 'true';
}

if(!empty($this->io->input["get"]["dhid"])){				//儲值記錄編號
	$status["status"]["search"]["dhid"] = $this->io->input["get"]["dhid"] ;
	$status["status"]["search_path"] .= "&dhid=".$this->io->input["get"]["dhid"] ;
	$sub_search_query .=  "
		AND dh.`dhid` = '".$this->io->input["get"]["dhid"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query = "SELECT count(*) as num 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` d 
on
	dh.prefixid = d.prefixid 
	and dh.depositid = d.depositid 
left outer join `{$db_user}`.`{$this->config->default_prefix}user_profile` up 
on
	up.prefixid = d.prefixid 
	and up.userid = d.userid 
left outer join `{$db_user}`.`{$this->config->default_prefix}user` u 
on
	u.prefixid = d.prefixid 
	and u.userid = d.userid 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri
on
    dh.driid=dri.driid
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
on
    dr.drid=dri.drid
WHERE 
	dh.`prefixid` = '{$this->config->default_prefix_id}' 
	and dh.depositid IS NOT NULL
	and dh.dhid is not null
	and up.userid IS NOT NULL
	and dh.switch = 'Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"] - 1).",".($this->config->max_page);

if($output_control == 'true'){
	$this->tplVar('page' , $page) ;
}else{
	$this->tplVar('page' , '') ;
}

// Table Count end

// Table Record Start

$query = "SELECT dh.*, d.countryid, (dr.name) as behav, 
                 d.currency, d.amount, d.ori_currency, d.ori_amount,
                 up.nickname, c.name as cname , u.name login_name, u.insertt login_insertt
from `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` d 
on
	dh.prefixid = d.prefixid 
	and dh.depositid = d.depositid 
left outer join `{$db_user}`.`{$this->config->default_prefix}user_profile` up 
on
	up.prefixid = d.prefixid 
	and up.userid = d.userid 
left outer join `{$db_user}`.`{$this->config->default_prefix}user` u 
on
	u.prefixid = d.prefixid 
	and u.userid = d.userid 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri
on
    dh.driid=dri.driid
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
on
    dr.drid=dri.drid
left outer join `{$db_channel}`.`{$this->config->default_prefix}country` c
on
    c.countryid=d.countryid	
where
	dh.`prefixid` = '{$this->config->default_prefix_id}' 
	and dh.depositid IS NOT NULL
	and dh.dhid is not null
	and up.userid IS NOT NULL
	and dh.switch = 'Y'
";


$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 

// Table Record End 



//Amount Total Start
/*  comment by Thomas 2019/07/17
$query = "SELECT sum(d.amount) as TotalAmount, count(d.amount) as TotalCount
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` d
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh
on
	dh.prefixid = d.prefixid
	and dh.depositid = d.depositid
	and dh.status = 'deposit'
	and dh.data != ''
	and dh.switch = 'Y'
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri
on
    dh.driid=dri.driid
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
on
    dr.drid=dri.drid
WHERE 
	d.`prefixid` = '{$this->config->default_prefix_id}' 
	and d.depositid IS NOT NULL
	and dh.dhid is not null
	AND d.`switch`='Y'
" ;
*/
$query = "SELECT sum(d.amount) as TotalAmount, count(d.amount) as TotalCount 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit` d 
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_history` dh 
on
	dh.prefixid = d.prefixid 
	and dh.depositid = d.depositid 
	and dh.status = 'deposit'
	and dh.data != ''
	and dh.switch = 'Y'
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule_item` dri
on
    dh.driid=dri.driid
left outer join `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
on
    dr.drid=dri.drid
WHERE 
	d.`prefixid` = '{$this->config->default_prefix_id}' 
	and d.depositid IS NOT NULL
	and dh.dhid is not null
	AND d.`switch`='Y'
" ;
$query .= $sub_search_query;
$Total = $this->model->getQueryRecord($query);
//$TotalAmount['table']['record'][0]['TotalAmount']

//Amount Total End

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
foreach($table["table"]['record'] as $rk => $rv)
{
	if ($rv['behav'] == ''){
		$data = json_decode($rv['data'],true);
		// print_r($data);
		$query = "SELECT name 
		FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr 
		WHERE 
			dr.`prefixid` = '{$this->config->default_prefix_id}' 
			AND dr.drid = '{$data['drid']}' 
			AND dr.`switch`='Y' 
		" ;
		$rulename = $this->model->getQueryRecord($query);		
		
		$table["table"]['record'][$rk]['behav'] = $rulename["table"]['record'][0]['name'];
		$table["table"]['record'][$rk]['drid'] = $data['drid'];
	}
	
	$query = "select ifnull(s.amount,0) as spoints_by_deposit 
	from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s 
	where 
		s.prefixid = '{$this->config->default_prefix_id}' 
		and s.userid = '{$rv['userid']}'
		and s.spointid = '{$rv['spointid']}'
		and s.switch = 'Y' 
		and s.behav='user_deposit'
	";
	$spoints_by_deposit = $this->model->getQueryRecord($query);	
	$table["table"]['record'][$rk]['spoints_by_deposit'] = $spoints_by_deposit["table"]['record'][0]['spoints_by_deposit'];
	
}	
// Relation End 
##############################################################################################################################################
// 儲值方式選單
$query = "
	SELECT drid,name
	FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
	where switch = 'Y'
	order by seq , drid
"; 
$drid_option = $this->model->getQueryRecord($query);	
$drid_option = $drid_option['table']['record'];

##############################################################################################################################################
// Log Start 
$deposit_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'deposit', 
	`active` = '儲值清單查詢', 
	`memo` = '{$deposit_data}', 
	`root` = 'deposit/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status["status"]["deposit_history_status"] = array("order" => "儲值訂單", "bid" => "中標發送", "writeoff" => "已銷帳", "deposit" => "已儲值", "cancel" => "取消儲值", "error" => "儲值錯誤");

if($output_control == 'true'){
	$this->tplVar('table', $table['table']) ;
	$this->tplVar('Total', $Total['table']['record'][0]);
}else{
	$this->tplVar('table', '') ;
	$this->tplVar('Total', '');
}
$this->tplVar('status', $status["status"]);
$this->tplVar('drid_option', $drid_option);
//echo '<pre>';print_r($status);die();
$this->display();
