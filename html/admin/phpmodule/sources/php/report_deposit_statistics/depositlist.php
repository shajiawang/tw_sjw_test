<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
if (empty($this->io->input['get']['userid'])) {
	$this->jsAlertMsg('會員ID錯誤!!');
}
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Search Start
$status["status"]["search_path"] = "&userid=".$this->io->input['get']['userid'];
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d 
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on 
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.status = 'deposit'
	and dh.data != ''
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on 
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
	and dri.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on 
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
	and dr.switch = 'Y'
WHERE 
	d.prefixid = '".$this->config->default_prefix_id."' 
	and d.depositid is not null
	and d.userid = '".$this->io->input['get']['userid']."'
	AND d.switch = 'Y'
order by d.depositid
" ;

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 
									
									


// Table Record Start
$query = "
select dr.name drname, d.amount, d.countryid, d.currency, dh.modifyt 
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d 
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on 
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.status = 'deposit'
	and dh.data != ''
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on 
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
	and dri.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on 
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
	and dr.switch = 'Y'
WHERE 
	d.prefixid = '".$this->config->default_prefix_id."' 
	and d.depositid is not null
	and d.userid = '".$this->io->input['get']['userid']."'
	AND d.switch = 'Y'
order by d.depositid
";
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];


$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();