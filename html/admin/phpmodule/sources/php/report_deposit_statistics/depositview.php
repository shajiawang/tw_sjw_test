<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
if (empty($this->io->input['get']['depositid'])) {
	$this->jsAlertMsg('儲值ID錯誤!!');
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 							
									


// Table Record Start
$query = "
select d.depositid, dr.name drname, d.amount, d.countryid, d.currency, dh.dhid, dh.modifyt 
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d 
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on 
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.status = 'deposit'
	and dh.data != ''
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on 
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
	and dri.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on 
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
	and dr.switch = 'Y'
WHERE 
	d.prefixid = '".$this->config->default_prefix_id."' 
	and d.depositid = '".$this->io->input['get']['depositid']."'
	AND d.switch = 'Y'
";
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_deposit_statistics', 
	`active` = '儲值統計訂單明細查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_deposit_statistics/depositview', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();