<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
if($_SESSION['user']['department'] != 'S') { 
	$this->jsAlertMsg('您無權限登入！');
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
// Sort End

// Search Start
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

##############################################################################################################################################
// Table  Start 

// Table Record Start
$query = "
SELECT dr.name, dr.drid, sum(d.amount) total_amount
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d 
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on 
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on 
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on 
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
where 
	d.prefixid = '".$this->config->default_prefix_id."' 
	and d.switch = 'Y'
	and d.behav = 'user_deposit'
	and dr.switch = 'Y'
	and dh.status = 'deposit'
	and YEAR(d.modifyt) = YEAR(NOW())
group by dr.drid
";
$table = $this->model->getQueryRecord($query);



// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

foreach($table["table"]['record'] as $rk => $rv)
{
	for($i = 1 ; $i < 13; $i++) {
		$query = "
			SELECT dr.name, year(d.modifyt) as year, month(d.modifyt) as month, sum(d.amount) as sun_total 
			from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d 
			left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on 
				d.prefixid = dh.prefixid
				and d.depositid = dh.depositid
				and dh.switch = 'Y'
			left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on 
				dh.prefixid = dri.prefixid
				and dh.driid = dri.driid
			left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on 
				dri.prefixid = dr.prefixid
				and dri.drid = dr.drid
			where 
				d.prefixid = '".$this->config->default_prefix_id."' 
				and d.switch = 'Y'
				and d.behav = 'user_deposit'
				and dh.status = 'deposit'
				and YEAR(d.modifyt) = YEAR(NOW())
				and MONTH(d.modifyt) = '".($i)."'
				and dr.drid = '".$rv['drid']."'
		";
		$total = $this->model->getQueryRecord($query);	
		$table["table"]['record'][$rk]['total'] .= empty($total["table"]['record'][0]['sun_total']) ? '0,' : round($total["table"]['record'][0]['sun_total']).',';
	}
}	
$query = "SELECT name, act ,drid 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr 
WHERE 
	dr.`prefixid` = '{$this->config->default_prefix_id}' 
	and dr.switch = 'Y' 
" ;
$rulelist = $this->model->getQueryRecord($query);		

$query = "
SELECT sum(d.amount) total_amount
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d 
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on 
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on 
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on 
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
where 
	d.prefixid = '".$this->config->default_prefix_id."' 
	and d.switch = 'Y'
	and d.behav = 'user_deposit'
	and dr.switch = 'Y'
	and dh.status = 'deposit'
	and YEAR(d.modifyt) = YEAR(NOW())
" ;
$alltotal = $this->model->getQueryRecord($query);

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_deposit_statistics', 
	`active` = '儲值統計圖表查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_deposit_statistics/depositpic', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status['status']['depositfrom'] = $rulelist["table"]['record'];
$status['status']['gender'] = array('male' => '男', 'female' => '女', '' => '男');
$status['status']['usergroup'] = array('1' => '是', '0' => '否');			//是否以會員為群組
$status['status']['alltotal'] = round($alltotal["table"]['record'][0]['total_amount']);

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();