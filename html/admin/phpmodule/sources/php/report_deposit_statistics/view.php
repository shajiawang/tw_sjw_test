<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_channel = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start

// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End

// Sort Start
$sub_sort_query =  " ORDER BY d.modifyt DESC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
$sub_group_query .=  " group by d.depositid";
if(!empty($this->io->input["get"]["joindatefrom"])){			//加入起始日期
	$status["status"]["search"]["joindatefrom"] = $this->io->input["get"]["joindatefrom"] ;
	$status["status"]["search_path"] .= "&joindatefrom=".$this->io->input["get"]["joindatefrom"] ;
	$sub_search_query .=  "
		AND u.`insertt` >= '".$this->io->input["get"]["joindatefrom"]."'";
}
if(!empty($this->io->input["get"]["joindateto"])){				//加入結束日期
	$status["status"]["search"]["joindateto"] = $this->io->input["get"]["joindateto"] ;
	$status["status"]["search_path"] .= "&joindateto=".$this->io->input["get"]["joindateto"] ;
	$sub_search_query .=  "
		AND u.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
}
if(!empty($this->io->input["get"]["depositdatefrom"])){			//儲值起始日期
	$status["status"]["search"]["depositdatefrom"] = $this->io->input["get"]["depositdatefrom"] ;
	$status["status"]["search_path"] .= "&depositdatefrom=".$this->io->input["get"]["depositdatefrom"] ;
	$sub_search_query .=  "
		AND d.`modifyt` >= '".$this->io->input["get"]["depositdatefrom"]."'";
}
if(!empty($this->io->input["get"]["depositdateto"])){			//儲值結束日期
	$status["status"]["search"]["depositdateto"] = $this->io->input["get"]["depositdateto"] ;
	$status["status"]["search_path"] .= "&depositdateto=".$this->io->input["get"]["depositdateto"] ;
	$sub_search_query .=  "
		AND d.`modifyt` < '".$this->io->input["get"]["depositdateto"]."'";
}
if(!empty($this->io->input["get"]["userid"])){				//會員編號
	$status["status"]["search"]["userid"] = $this->io->input["get"]["userid"] ;
	$status["status"]["search_path"] .= "&userid=".$this->io->input["get"]["userid"] ;
	// $sub_search_query .=  "	AND u.`userid` = '".$this->io->input["get"]["userid"]."'";
	$sub_search_query .=  "	AND u.`userid` IN (".$this->io->input["get"]["userid"].") ";
}
if(!empty($this->io->input["get"]["name"])){				//會員帳號
	$status["status"]["search"]["name"] = $this->io->input["get"]["name"] ;
	$status["status"]["search_path"] .= "&name=".$this->io->input["get"]["name"] ;
	$sub_search_query .=  "
		AND u.`name` like '%".$this->io->input["get"]["name"]."%'";
}
if(!empty($this->io->input["get"]["depositfrom"])){				//儲值方式
	$status["status"]["search"]["depositfrom"] = $this->io->input["get"]["depositfrom"] ;
	$status["status"]["search_path"] .= "&depositfrom=".$this->io->input["get"]["depositfrom"] ;
	$sub_search_query .=  "
		AND dr.`act` = '".$this->io->input["get"]["depositfrom"]."'";
}
if(!empty($this->io->input["get"]["total_amount"])){			//儲值金額
	$status["status"]["search"]["total_amount"] = $this->io->input["get"]["total_amount"] ;
	$status["status"]["search_path"] .= "&total_amount=".$this->io->input["get"]["total_amount"] ;
	$sub_search_query .=  "
		AND `total_amount` = '".$this->io->input["get"]["total_amount"]."'";
}
if(!empty($this->io->input["get"]["gender"])){					//性別
	$status["status"]["search"]["gender"] = $this->io->input["get"]["gender"] ;
	$status["status"]["search_path"] .= "&gender=".$this->io->input["get"]["gender"] ;
	$sub_search_query .=  "
		AND up.`gender` = '".$this->io->input["get"]["gender"]."'";
}
if (!empty($this->io->input["get"]["usergroup"])) {				//可以勾選已會員帳號為群組(沒有勾選則為流水帳)
	$status["status"]["search"]["usergroup"] = $this->io->input["get"]["usergroup"] ;
	$status["status"]["search_path"] .= "&usergroup=".$this->io->input["get"]["usergroup"] ;
	if($this->io->input["get"]["usergroup"] == 1){
		$sub_group_query =  "
			group by u.userid";
	}
	else {
		$sub_group_query =  "
			group by d.depositid";
	}
}
if($this->io->input["get"]["nickname"] != ''){			//暱稱查詢
	$status["status"]["search"]["nickname"] = $this->io->input["get"]["nickname"] ;
	$status["status"]["search_path"] .= "&nickname=".$this->io->input["get"]["nickname"] ;
	$sub_search_query .= " AND up.`nickname` like '%".$this->io->input["get"]["nickname"]."%'	";
}

if(!empty($this->io->input["get"]["src_ip"])){					//ip
	$status["status"]["search"]["src_ip"] = $this->io->input["get"]["src_ip"] ;
	$status["status"]["search_path"] .= "&src_ip=".$this->io->input["get"]["src_ip"] ;
	$sub_search_query .=  "
		AND dh.`src_ip` = '".$this->io->input["get"]["src_ip"]."'";
}
if(!empty($this->io->input["get"]["dhid"])){				//儲值記錄編號
	$status["status"]["search"]["dhid"] = $this->io->input["get"]["dhid"] ;
	$status["status"]["search_path"] .= "&dhid=".$this->io->input["get"]["dhid"] ;
	$sub_search_query .=  "
		AND dh.`dhid` = '".$this->io->input["get"]["dhid"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End

##############################################################################################################################################
// Table  Start

// Table Count Start
$query ="
select count(ds.num) num from
(SELECT
count(*) as num
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u on
	u.prefixid = d.prefixid
	and u.userid = d.userid
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	up.prefixid = u.prefixid
	and up.userid = u.userid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
where
	d.prefixid = '".$this->config->default_prefix_id."'
	and d.switch = 'Y'
" ;
$query .= $sub_search_query ;
$query .= $sub_group_query;
$query .= ") ds";
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end

// Table Record Start
$query = "
SELECT d.userid, u.name, up.nickname, u.insertt joindate, d.modifyt deposit_date, dri.driid, dr.drid, dr.act, sum(d.amount) total_amount, up.gender,
		d.depositid, u.userid, dh.dhid, dh.modifiername, dh.spointid, dh.data , dh.src_ip, (dr.name) as behav, dr.drid, d.ori_amount, d.ori_currency
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u on
	u.prefixid = d.prefixid
	and u.userid = d.userid
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	up.prefixid = u.prefixid
	and up.userid = u.userid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
where
	d.prefixid = '".$this->config->default_prefix_id."'
	and d.switch = 'Y'
";

/*  不含 ori_currency & ori_amount 的sql
$query = "
SELECT d.userid, u.name, up.nickname, u.insertt joindate, d.modifyt deposit_date, dri.driid, dr.drid, dr.act, sum(d.amount) total_amount, up.gender,
		d.depositid, u.userid, dh.modifiername, dh.spointid, dh.data ,(dr.name) as behav, dr.drid
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u on
	u.prefixid = d.prefixid
	and u.userid = d.userid
	and u.switch = 'Y'
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	up.prefixid = u.prefixid
	and up.userid = u.userid
	and up.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
where
	d.prefixid = '".$this->config->default_prefix_id."'
	and d.switch = 'Y'
";
*/
$query .= $sub_search_query ;
$query .= $sub_group_query;
$query .= $sub_sort_query;
$query .= $query_limit ;
$table = $this->model->getQueryRecord($query);
// Table Record End

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$black_list=getRedisList('black_list');
if (sizeof($black_list)>0){
}else{
	$black_list=array();
}

$white_list=getRedisList('white_list');
if (sizeof($white_list)>0){
}else{
	$white_list=array();
}
foreach($table["table"]['record'] as $rk => $rv)
{
	if ($rv['act'] == ''){
		$data = json_decode($rv['data'],true);
		// print_r($data);
		$query = "SELECT name
		FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
		WHERE
			dr.`prefixid` = '{$this->config->default_prefix_id}'
			AND dr.drid = '{$data['drid']}'
			AND dr.`switch`='Y'
		" ;
		$rulename = $this->model->getQueryRecord($query);

		$table["table"]['record'][$rk]['behav'] = $rulename["table"]['record'][0]['name'];
		$table["table"]['record'][$rk]['drid'] = $data['drid'];
	}
	$table["table"]['record'][$rk]['inblack']=in_array('black_list_'.$table["table"]['record'][$rk]['src_ip'],$black_list);
	$table["table"]['record'][$rk]['inwhite']=in_array('white_list_'.$table["table"]['record'][$rk]['src_ip'],$white_list);
	$query = "select ifnull(s.amount,0) as spoints_by_deposit
	from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."spoint` s
	where
		s.prefixid = '{$this->config->default_prefix_id}'
		and s.userid = '{$rv['userid']}'
		and s.spointid = '{$rv['spointid']}'
		and s.switch = 'Y'
		and s.behav='user_deposit'
	";
	$spoints_by_deposit = $this->model->getQueryRecord($query);
	$table["table"]['record'][$rk]['spoints_by_deposit'] = $spoints_by_deposit["table"]['record'][0]['spoints_by_deposit'];
}

// print_r($table["table"]['record']);

$query = "SELECT name, act ,drid
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}deposit_rule` dr
WHERE
	dr.`prefixid` = '{$this->config->default_prefix_id}'
" ;
$rulelist = $this->model->getQueryRecord($query);

$query = "
SELECT sum(d.amount) total_amount
from `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit` d
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u on
	u.prefixid = d.prefixid
	and u.userid = d.userid
left join `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up on
	up.prefixid = u.prefixid
	and up.userid = u.userid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh on
	d.prefixid = dh.prefixid
	and d.depositid = dh.depositid
	and dh.switch = 'Y'
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri on
	dh.prefixid = dri.prefixid
	and dh.driid = dri.driid
left join `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr on
	dri.prefixid = dr.prefixid
	and dri.drid = dr.drid
where
	d.prefixid = '".$this->config->default_prefix_id."'
	and d.switch = 'Y'
" ;
$query .= $sub_search_query ;
$query .= $query_limit ;
$alltotal = $this->model->getQueryRecord($query);

// Relation End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_deposit_statistics',
	`active` = '儲值統計清單查詢',
	`memo` = '{$report_data}',
	`root` = 'report_deposit_statistics/view',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
// $status['status']['depositfrom'] = array('alipay' => '支付寶', 'bankcomm' => '銀聯支付', 'weixinpay' => '微信支付', 'cashpay' => '現金支付', 'remittance' => '轉帳匯款', 'creditcard' => '臺灣信用卡支付', 'gama' => '橘子支付', '' => '其它支付' );
$status['status']['depositfrom'] = $rulelist["table"]['record'];
$status['status']['gender'] = array('male' => '男', 'female' => '女', '' => '男');
$status['status']['usergroup'] = array('1' => '是', '0' => '否');			//是否以會員為群組
$status['status']['alltotal'] = round($alltotal["table"]['record'][0]['total_amount']);

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();