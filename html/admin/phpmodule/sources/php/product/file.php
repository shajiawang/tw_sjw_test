<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
include_once "KLogger.php";
$log = new KLogger ( "/var/log/product_operation" , KLogger::DEBUG );
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];
if ($_SERVER['SERVER_NAME']=='mgr.saja.com.tw'){
	$imgprefix="https://s3.hicloud.net.tw/img.saja.com.tw/site/images/site/product/";
}else{
	$imgprefix="http://".$_SERVER['SERVER_NAME']."/site/images/site/product/";
}

function html_table($data = array())
{
    $rows = array();
    foreach ($data as $row) {
        $cells = array();
        foreach ($row as $cell) {
            $cells[] = "<td>{$cell}</td>";
        }
        $rows[] = "<tr>" . implode('', $cells) . "</tr>";
    }
    return "<table class='TB_COLLAPSE'>" . implode('', $rows) . "</table>";
}
##############################################################################################################################################
// Insert Start
$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';

if($files['file_name']["error"] != 0 || empty($files['file_name']["size"]) ) {
	$this->jsAlertMsg('上傳錯誤!!');
}
else
{
	$import_time=date("YmdHis");
	$import_name=explode(".",$files['file_name']['name']);
	$product_File = $import_name[0].$import_time.".".$import_name[1];
    if(is_uploaded_file($files['file_name']['tmp_name']) ) {
		move_uploaded_file($files['file_name']['tmp_name'], "/var/www/html/admin/imp/{$product_File}");
	}
	$handle = fopen("/var/www/html/admin/imp/{$product_File}", 'r+');
	if (is_resource($handle))
    {
	  	$log->LogDebug("import product original filename: ".$files['file_name']['name']." time: ".$import_time);
		$myfile = fopen("/var/www/html/admin/imp/".$import_time.".html", "w");
        $insertt = date('Y-m-d H:i:s');
		$newData = array();

		$filesize = filesize("/var/www/html/admin/imp/{$product_File}");
		$data = fread($handle, $filesize);

		$data = mb_convert_encoding($data, "UTF-8", 'big5');
		$newData = explode("\n", $data);
		$newData = array_values(array_filter($newData));

		if (!empty($newData)) {
			foreach ($newData as $key => $value) {
				$dataArr[$key] = explode(",", $value);
			}

			if (!empty($dataArr)) {
				$inpar=array();
				//將第一列資料清除
				array_push($inpar,'樣本商品碼');
				array_push($inpar,'商品名稱');
				array_push($inpar,'市價');
				array_push($inpar,'下標處理費');
				array_push($inpar,'得標處理費');
				array_push($inpar,'下架時間');
				array_push($inpar,'圖一及路徑');
				array_push($inpar,'圖二及路徑');
				array_push($inpar,'圖三及路徑');
				array_push($inpar,'圖四及路徑');
				array_shift($dataArr);
				$infodata=array();
				array_push($infodata,$inpar);
				$dataArr = array_values(array_filter($dataArr));
				$count = 0;
				foreach ($dataArr as $key => $value) {
					$query ="SELECT p.* ,pt.original, pt.filename, pt.name imgname,
					                     pt2.original as original2, pt2.filename as filename2, pt2.name as name2,
										 pt3.original as original3, pt3.filename as filename3, pt3.name as name3,
										 pt4.original as original4, pt4.filename as filename4, pt4.name as name4
					FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p
					LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt ON
						p.prefixid = pt.prefixid
						AND p.ptid = pt.ptid
						AND pt.switch = 'Y'
					LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt2 ON
						p.prefixid = pt2.prefixid
						AND p.ptid2 = pt2.ptid
						AND pt2.switch = 'Y'
					LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt3 ON
						p.prefixid = pt3.prefixid
						AND p.ptid3 = pt3.ptid
						AND pt3.switch = 'Y'
					LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt4 ON
						p.prefixid = pt4.prefixid
						AND p.ptid4 = pt4.ptid
						AND pt4.switch = 'Y'
					WHERE p.prefixid = '{$this->config->default_prefix_id}'
					AND p.`switch`='Y'
					AND p.productid = '{$value[0]}'
					" ;
					$old_product = $this->model->getQueryRecord($query);
					if (!empty($old_product['table']['record'])) {

						$info_product = $old_product['table']['record'][0];
						$info_product["saja_limit"]=0;
						// $info_product["user_limit"]=20;
						$info_product["usereach_limit"]=20;

						if (!empty($info_product["filename"]) ) {
                            $update_thumbnail = " `ptid` = '${info_product["ptid"]}',";
						}
						if (!empty($info_product["filename2"]) ) {
                            $update_thumbnail2 = " `ptid2` = '${info_product["ptid2"]}',";
						}
						if (!empty($info_product["filename3"]) ) {
						    $update_thumbnail3 = " `ptid3` = '${info_product["ptid3"]}',";
						}
						if (!empty($info_product["filename4"]) ) {
                            $update_thumbnail4 = " `ptid4` = '${info_product["ptid4"]}',";
						}
						//過濾商品名稱內 單引號 雙引號 右斜線
						$product_name = $value[1];
						$product_name = str_replace('\\', '', $product_name);
						$product_name = str_replace('\'', '', $product_name);
						$product_name = str_replace('"', '', $product_name);
						$inpar=array();
						//將第一列資料清除
						array_push($inpar,$value[0]);
						array_push($inpar,$value[1]);
						array_push($inpar,$value[2]);
						array_push($inpar,$value[3]);
						array_push($inpar,$value[4]);
						array_push($inpar,$value[7]);
						$ptid=empty($info_product["ptid"])?'':$info_product['filename'].'<br><img src="'.$imgprefix.$info_product['filename'].'" style="width:100px;height:auto"/>';
						$ptid2=empty($info_product["ptid2"])?'':$info_product['filename2'].'<br><img src="'.$imgprefix.$info_product['filename2'].'"  style="width:100px;height:auto"/>';
						$ptid3=empty($info_product["ptid3"])?'':$info_product['filename3'].'<br><img src="'.$imgprefix.$info_product['filename3'].'"  style="width:100px;height:auto"/>';
						$ptid4=empty($info_product["ptid4"])?'':$info_product['filename4'].'<br><img src="'.$imgprefix.$info_product['filename4'].'"  style="width:100px;height:auto"/>';
						array_push($inpar,$ptid);
						array_push($inpar,$ptid2);
						array_push($inpar,$ptid3);
						array_push($inpar,$ptid4);
						array_push($infodata,$inpar);
						if ($value[5] == '7'){
							$description = '<p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">◎溫馨提醒</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">若您並非為&ldquo;新手殺友&rdquo;（已有得標紀錄），在您得標&ldquo;新手限定&rdquo;商品後，一律視同違反下標規則，將不予出貨。</span></p>'.$info_product['description'];
						} else if ($value[5] == '21'){
							$description = '<p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">◎溫馨提醒</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">若您的得標紀錄已超過5筆，在得標本檔&ldquo;新手限定（得標五次內）&rdquo;的商品後，一律視同違反下標規則，將不予出貨。</span></p>'.$info_product['description'];
						} else if ($value[5] == '22'){
							$description = '<p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">◎溫馨提醒</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">若您的得標紀錄已超過3筆，在得標本檔&ldquo;新手限定（得標三次內）&rdquo;的商品後，一律視同違反下標規則，將不予出貨。</span></p>'.$info_product['description'];
						} else {
							$description = $info_product['description'];
						}
/* 						if (date("Y-m-d H:i:s", strtotime($value[6])) >= '2020-02-07 00:01:00' && date("Y-m-d H:i:s", strtotime($value[7])) <= '2020-02-14 23:59:00'){
							$pcid = explode("|", $value[11]);
							//寫入商品分類rt
							$res = array();
							foreach($pcid as $rk => $rv) {
								if ($rv == 90){
									$description = '<p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#0000ff;">◎好康報報:</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#0000ff;">殺價王情人節下標抽獎活動來囉~</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#0000ff;">活動期間：2020/02/07 00:01~02/14 23:59</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#0000ff;">活動辦法：只要在活動期間</span><span style="color:#ff0000;"><span style="background-color:#ffff00;">付費下標次數達10次以上(含)</span></span><span style="color:#0000ff;">不限商品、金額(不含免費下標)</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#0000ff;">就可以參加抽獎!!將於02/15抽出50位幸運兒(</span><span style="color:#ff0000;">名單將公佈於臉書粉絲團，趕快設定搶先看!!</span><span style="color:#0000ff;">)</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;"><span style="background-color:#ffff00;">※一個自然人帳號僅能參與一次</span></span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#0000ff;">快去手刀下標參加抽獎吧~</span></p>'.$description;
								}
							}
						} */
						
						$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` SET
							`prefixid`		= '{$this->config->default_prefix_id}',
							`ptype`			= '{$value[10]}',
							`name`			= '{$product_name}',
							`description`	= '{$description}',
							`retail_price`	= '{$value[2]}',
							{$update_thumbnail}
							{$update_thumbnail2}
							{$update_thumbnail3}
							{$update_thumbnail4}
							`thumbnail_url`	= '{$info_product["thumbnail_url"]}',
							`ontime`		= '{$value[6]}',
							`offtime`		= '{$value[7]}',
							`saja_limit`	= '{$info_product["saja_limit"]}',
							`user_limit`	= '{$info_product["user_limit"]}',
							`usereach_limit`= '{$info_product["usereach_limit"]}',
							`saja_fee`		= '{$value[3]}',
							`process_fee`	= '{$value[4]}',
							`price_limit`	= '{$info_product["price_limit"]}',
							`bonus_type`	= '{$info_product["bonus_type"]}',
							`bonus`			= '{$info_product["bonus"]}',
							`gift_type`		= '{$info_product["gift_type"]}',
							`gift`			= '{$info_product["gift"]}',
							`vendorid`		= '{$info_product["vendorid"]}',
							`split_rate`	= '{$info_product["split_rate"]}',
							`base_value`	= '{$info_product["base_value"]}',
							`display`		= '{$value[8]}',
							`display_all`	= '{$value[9]}',
							`bid_type`		= '{$info_product["bid_type"]}',
							`mob_type`		= '{$info_product["mob_type"]}',
							`loc_type`		= '{$info_product["loc_type"]}',
							`is_flash`		= '{$info_product["is_flash"]}',
							`flash_loc`     = '{$info_product["flash_loc"]}',
							`flash_loc_map` = '{$info_product["flash_loc_map"]}',
							`bid_spoint`	= '{$info_product["bid_spoint"]}',
							`bid_scode`		= '{$info_product["bid_scode"]}',
							`bid_oscode`	= '{$info_product["bid_oscode"]}',
							`pay_type`		= '{$info_product["pay_type"]}',
							`seq`           = '{$info_product["seq"]}',
							`totalfee_type`	= '{$info_product["totalfee_type"]}',
							`is_big`		= '{$info_product["is_big"]}',
							`checkout_type`	= '{$info_product["checkout_type"]}',
							`checkout_money`= '{$info_product["checkout_money"]}',
							`is_discount`	= '{$info_product["is_discount"]}',
							`hot_used`		= '{$info_product["hot_used"]}',
							`is_test`		= '{$info_product["is_test"]}',
							`limitid`		= '{$value[5]}',
							`lb_used`		= '{$info_product["lb_used"]}',
							`is_exchange`  	= '{$info_product["is_exchange"]}',
							`epid`  		= '{$info_product["epid"]}',
							`ordertype`  	= '{$info_product["ordertype"]}',
							`orderbonus`  	= '{$info_product["orderbonus"]}',
							`everydaybid`  	= '{$info_product["everydaybid"]}',
							`freepaybid`  	= '{$info_product["freepaybid"]}',
							`insertt`       = now()
						" ;
						$this->model->query($query);

						//新商品ID
						$productid = $this->model->_con->insert_id;


						$pcid = explode("|", $value[11]);
						//寫入商品分類rt
						$res = array();
						foreach($pcid as $rk => $rv) {
							$res[] = "
								('".$this->config->default_prefix_id."', '".$productid."', '".$rv."', '".(($rk+1)*10)."', NOW())";
						}
						$query = "
						INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt`(
							`prefixid`,
							`productid`,
							`pcid`,
							`seq`,
							`insertt`
						)
						VALUES
						".implode(',', $res);
						$this->model->query($query);

						//寫入商店rt
						$query ="SELECT *
						FROM `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt`
						WHERE `prefixid` = '{$this->config->default_prefix_id}'
						AND `switch`='Y'
						AND `productid` = '{$value[0]}'
						";
						$old_product_store = $this->model->getQueryRecord($query);

						//Relation product_category
						if($old_product_store['table']['record'])
						{
							foreach($old_product_store['table']['record'] as $rk => $rv) {
								$query = "INSERT INTO `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt` SET
								`prefixid`='{$this->config->default_prefix_id}',
								`storeid`='{$rv['storeid']}',
								`productid`='{$productid}',
								`seq`='{$rv['seq']}',
								`insertt`=NOW()
								";
								$this->model->query($query);
							}
						}

						//寫入下標條件rt
						$query ="SELECT *
						FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt`
						WHERE `prefixid` = '{$this->config->default_prefix_id}'
						AND `switch`='Y'
						AND `productid` = '{$value[0]}'
						";
						$old_product_rule = $this->model->getQueryRecord($query);

						//Relation product_category
						if($old_product_rule['table']['record'])
						{
							foreach($old_product_rule['table']['record'] as $rk => $rv) {
								$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt` SET
								`prefixid`='{$this->config->default_prefix_id}',
								`srid`='{$rv['srid']}',
								`value`='{$rv['value']}',
								`productid`='{$productid}',
								`seq`='{$rv['seq']}',
								`insertt`=NOW()
								";
								$this->model->query($query);
							}
						}

						unset($product_name);
						unset($ptid);
						unset($ptid2);
						unset($ptid3);
						unset($ptid4);
						unset($pcid);
						$count ++;
					} else {
						$errorid[$i] = $value[0];
						$i ++;
					}
				}
				$errorid = implode(",",$errorid);
			}
			$content="<style>
table.TB_COLLAPSE {
  width:100%;
  border-collapse:collapse;
}
table.TB_COLLAPSE caption {
  padding:10px;
  font-size:24px;
  background-color:#f3f6f9;
}
table.TB_COLLAPSE thead th {
  padding:5px 0px;
  color:#fff;
  background-color:#915957;
}
table.TB_COLLAPSE tbody td {
  padding:5px 0px;
  color:#555;
  text-align:center;
  background-color:#fff;
  border-bottom:1px solid #915957;
}
table.TB_COLLAPSE tfoot td {
  padding:5px 0px;
  text-align:center;
  background-color:#d6d6a5;
}
    </style>";

			fwrite($myfile, $content);

		}
        fclose($handle);
    }
    //unlink($product_File);

	$redis = new Redis();
	$redis->connect('sajacache01',6379);
	$redis->del("inx_product_list");
	$redis->del("product_category_list");
	$redis->del("hot_product_list");
	$redis->del("lb_product_list");

	$errormsg = '';
	if (!empty($errorid)){
		$errormsg = '，商品匯入 '.$errorid.' 資料不符';
	}
	fwrite($myfile, '已成功上傳 '. $count .' 筆!'.$errormsg);
	$content.=html_table($infodata);
	fwrite($myfile,$content);

			fclose($myfile);

##############################################################################################################################################
// Log Start
$prodcut_data = json_encode($this->io->input["files"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_file',
	`active` = '商品匯入寫入',
	`memo` = '{$prodcut_data}',
	`root` = 'product/file',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

	header("location:/admin/imp/".$import_time.".html");
	//$this->jsPrintMsg('已成功上傳 '. $count .' 筆!'.$errormsg, $this->io->input['get']['location_url']);
}

// Insert Start
##############################################################################################################################################
//header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));