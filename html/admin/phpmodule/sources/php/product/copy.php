<?php
// Check Variable Start
if (empty($this->io->input["get"]["productid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// 原商品 Record 

$query ="SELECT p.* ,pt.original, pt.filename, pt.name imgname,pt2.original as original2, pt2.filename as filename2, pt2.name as name2, pt3.original as original3, pt3.filename as filename3, pt3.name as name3, pt4.original as original4, pt4.filename as filename4, pt4.name as name4 
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p 
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt2 ON 
	p.prefixid = pt2.prefixid
	AND p.ptid2 = pt2.ptid
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt3 ON 
	p.prefixid = pt3.prefixid
	AND p.ptid3 = pt3.ptid	
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt4 ON 
	p.prefixid = pt4.prefixid
	AND p.ptid4 = pt4.ptid		
WHERE p.prefixid = '{$this->config->default_prefix_id}'
AND p.`switch`='Y' 
AND p.productid = '{$this->io->input["get"]["productid"]}'
" ;
$old_product = $this->model->getQueryRecord($query);

if (empty($old_product['table']['record'])) {
	$this->jsAlertMsg('商品錯誤!!');
}

$info_product = $old_product['table']['record'][0];

//Relation exchange_product_thumbnail
if (!empty($info_product["filename"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename"])) {
		$filename = md5(date("YmdHis")."_".$info_product["name"]).".".pathinfo($info_product["filename"], PATHINFO_EXTENSION);
		copy($this->config->path_products_images."/".$info_product["filename"], $this->config->path_products_images."/".$filename);
		syncToS3($this->config->path_products_images."/".$filename,'s3://img.saja.com.tw','/site/images/site/product/');
	}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname"]}',
	`original`='{$info_product["original"]}',
	`filename`='{$info_product["filename"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid = $this->model->_con->insert_id;
	
	$update_thumbnail = "
	`ptid` = '{$ptid}',";
}

if (!empty($info_product["filename2"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename2"])) {
		$filename2 = md5(date("YmdHis")."_".$info_product["name2"]).".".pathinfo($info_product["filename2"], PATHINFO_EXTENSION);
		copy($this->config->path_products_images."/".$info_product["filename2"], $this->config->path_products_images."/".$filename2);
		syncToS3($this->config->path_products_images."/".$filename2,'s3://img.saja.com.tw','/site/images/site/product/');
	}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname2"]}',
	`original`='{$info_product["original2"]}',
	`filename`='{$info_product["filename2"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid2 = $this->model->_con->insert_id;
	
	$update_thumbnail2 = "
	`ptid2` = '{$ptid2}',";
}

if (!empty($info_product["filename3"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename3"])) {
		$filename3 = md5(date("YmdHis")."_".$info_product["name3"]).".".pathinfo($info_product["filename3"], PATHINFO_EXTENSION);
		copy($this->config->path_products_images."/".$info_product["filename3"], $this->config->path_products_images."/".$filename3);
		syncToS3($this->config->path_products_images."/".$filename3,'s3://img.saja.com.tw','/site/images/site/product/');
	}

	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname3"]}',
	`original`='{$info_product["original3"]}',
	`filename`='{$info_product["filename3"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid3 = $this->model->_con->insert_id;
	
	$update_thumbnail3 = "
	`ptid3` = '{$ptid3}',";
}

if (!empty($info_product["filename4"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename4"])) {
		$filename4 = md5(date("YmdHis")."_".$info_product["name4"]).".".pathinfo($info_product["filename4"], PATHINFO_EXTENSION);
		copy($this->config->path_products_images."/".$info_product["filename4"], $this->config->path_products_images."/".$filename4);
		syncToS3($this->config->path_products_images."/".$filename4,'s3://img.saja.com.tw','/site/images/site/product/');
	}

	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname4"]}',
	`original`='{$info_product["original4"]}',
	`filename`='{$info_product["filename4"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid4 = $this->model->_con->insert_id;
	
	$update_thumbnail4 = "
	`ptid4` = '{$ptid4}',";
}

##############################################################################################################################################


##############################################################################################################################################
// Table Start

// 產生新商品
$description = str_replace("'", '', htmlspecialchars($info_product["description"]) );
$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` SET
	`prefixid`		= '{$this->config->default_prefix_id}',
	`name`			= '[Copy] {$info_product["name"]}',
	`description`	= '{$description}',
	`retail_price`	= '{$info_product["retail_price"]}',
	{$update_thumbnail}
	{$update_thumbnail2}
	{$update_thumbnail3}
	{$update_thumbnail4}	
	`thumbnail_url`	= '{$info_product["thumbnail_url"]}',
	`ontime`		= '{$info_product["ontime"]}',
	`offtime`		= '{$info_product["offtime"]}',
	`saja_limit`	= '{$info_product["saja_limit"]}',
	`user_limit`	= '{$info_product["user_limit"]}',
	`usereach_limit`= '{$info_product["usereach_limit"]}',
	`saja_fee`		= '{$info_product["saja_fee"]}',
	`process_fee`	= '{$info_product["process_fee"]}',
	`price_limit`	= '{$info_product["price_limit"]}',
	`bonus_type`	= '{$info_product["bonus_type"]}',
	`bonus`			= '{$info_product["bonus"]}',
	`gift_type`		= '{$info_product["gift_type"]}',
	`gift`			= '{$info_product["gift"]}',
	`vendorid`		= '{$info_product["vendorid"]}',
	`split_rate`	= '{$info_product["split_rate"]}',
	`base_value`	= '{$info_product["base_value"]}',
	`bid_type`		= '{$info_product["bid_type"]}',
	`mob_type`		= '{$info_product["mob_type"]}',
	`loc_type`		= '{$info_product["loc_type"]}',
	`is_flash`		= '{$info_product["is_flash"]}',
	`flash_loc`     = '{$info_product["flash_loc"]}',
	`flash_loc_map` = '{$info_product["flash_loc_map"]}',
	`bid_spoint`	= '{$info_product["bid_spoint"]}',
	`bid_scode`		= '{$info_product["bid_scode"]}',
	`pay_type`		= '{$info_product["pay_type"]}',
	`seq`           = '{$info_product["seq"]}',
	`totalfee_type`	= '{$info_product["totalfee_type"]}',	
	`is_discount`	= '{$info_product["is_discount"]}',
	`is_exchange`	= '{$info_product["is_exchange"]}',
	`epid`			= '{$info_product["epid"]}',
	`hot_used`		= '{$info_product["hot_used"]}',
	`ptype`			= '{$info_product["ptype"]}',
	`ordertype`		= '{$info_product["ordertype"]}',
	`orderbonus`	= '{$info_product["orderbonus"]}',
	`everydaybid`	= '{$info_product["everydaybid"]}',
	`freepaybid`	= '{$info_product["freepaybid"]}',
	`codepid`		= '{$info_product["codepid"]}',
	`codenum`		= '{$info_product["codenum"]}',	
	`insertt`       = now()
" ;
$this->model->query($query);

//新商品ID
$productid = $this->model->_con->insert_id;


//寫入商品分類rt
$query ="SELECT *
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_category_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_cat = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_cat['table']['record'])
{
	foreach($old_product_cat['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_category_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`pcid`='{$rv['pcid']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}', 
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

//寫入商店rt
$query ="SELECT *
FROM `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_store = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_store['table']['record'])
{
	foreach($old_product_store['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`storeid`='{$rv['storeid']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}', 
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

//寫入下標條件rt
$query ="SELECT *
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_rule = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_rule['table']['record'])
{
	foreach($old_product_rule['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`srid`='{$rv['srid']}',
		`value`='{$rv['value']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}',
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

// Table End
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_copy', 
	`active` = '商品複製寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product/copy', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$to = $this->config->default_main .'/product/edit/productid='. $productid .'&location_url='. $this->io->input['get']['location_url'];
header("location:". $to); //header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));