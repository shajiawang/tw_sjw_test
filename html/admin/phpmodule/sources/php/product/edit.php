<?php
if (empty($this->io->input["get"]["productid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End


##############################################################################################################################################
// Table  Start

// Table Content Start
$query ="
SELECT
p.*, pt.filename as thumbnail, pt2.filename as thumbnail2, pt3.filename as thumbnail3 , pt4.filename as thumbnail4
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt ON
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
	AND pt.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt2 ON
	p.prefixid = pt2.prefixid
	AND p.ptid2 = pt2.ptid
	AND pt2.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt3 ON
	p.prefixid = pt3.prefixid
	AND p.ptid3 = pt3.ptid
	AND pt3.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_thumbnail` pt4 ON
	p.prefixid = pt4.prefixid
	AND p.ptid4 = pt4.ptid
	AND pt4.switch = 'Y'
WHERE
p.prefixid = '".$this->config->default_prefix_id."'
AND p.productid = '".$this->io->input["get"]["productid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT
pc.*, pcr.productid
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` pc
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt` pcr ON
	pc.prefixid = pcr.prefixid
	AND pc.pcid = pcr.pcid
	AND pcr.productid = '".$this->io->input["get"]["productid"]."'
	AND pcr.switch = 'Y'
WHERE pc.prefixid = '".$this->config->default_prefix_id."'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category_rt"] = $recArr['table']['record'];

$query ="
SELECT
s.*, sp.productid, cs.channelid
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store` s
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt` sp ON
	s.prefixid = sp.prefixid
	AND s.storeid = sp.storeid
	AND sp.productid = '".$this->io->input["get"]["productid"]."'
	AND sp.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON
	s.prefixid = cs.prefixid
	AND s.storeid = cs.storeid
	AND cs.switch = 'Y'
WHERE
    s.prefixid = '".$this->config->default_prefix_id."'
    AND s.switch = 'Y'
GROUP BY s.storeid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["store_product_rt"] = $recArr['table']['record'];

if (is_array($table["table"]["rt"]["store_product_rt"])) {
	foreach ($table["table"]["rt"]["store_product_rt"] as $key => $value) {
		if (!is_null($value['productid'])) $storeArr[] = $value['storeid'];
	}
	$store_query = implode(',', $storeArr);
}

if (empty($store_query)){
	$store_query  = 0;
}
$query ="
SELECT
c.*, cs.storeid
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` c
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON
	c.prefixid = cs.prefixid
	AND c.channelid = cs.channelid
	AND cs.storeid in ({$store_query})
	AND cs.switch = 'Y'
WHERE
    c.prefixid = '".$this->config->default_prefix_id."'
    AND c.switch = 'Y'
GROUP BY c.channelid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_store_rt"] = $recArr['table']['record'];


$query ="
SELECT
r.*, pr.productid, pr.value
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."rule` r
LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_rule_rt` pr ON
	r.prefixid = pr.prefixid
	AND r.srid = pr.srid
	AND pr.productid = '".$this->io->input["get"]["productid"]."'
	AND pr.switch = 'Y'
WHERE
r.prefixid = '".$this->config->default_prefix_id."'
AND r.switch = 'Y'
ORDER BY r.seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["saja_rule"] = $recArr['table']['record'];

//經銷商channel
$query ="
SELECT *
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel`
WHERE
	`prefixid` = '".$this->config->default_prefix_id."'
	AND `switch` = 'Y'
ORDER BY `channelid`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["channel"] = $recArr['table']['record'];

//分館store
$query ="
SELECT s.*, cs.channelid
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store` s
left join `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs on
	s.`prefixid` = cs.`prefixid`
	and cs.`storeid` = s.`storeid`
	and cs.`switch` = 'Y'
WHERE
	s.`prefixid` = '".$this->config->default_prefix_id."'
	AND s.`switch` = 'Y'
ORDER BY `storeid`
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["store"] = $recArr['table']['record'];

//今日必殺
$query ="
SELECT count(todayid) as total
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_today` p
WHERE
p.prefixid = '".$this->config->default_prefix_id."'
AND p.productid = '".$this->io->input["get"]["productid"]."'
AND p.`switch` = 'Y'
" ;
$recArr = $this->model->getQueryRecord($query);
$table["table"]["today"] = $recArr['table']['record'][0]['total'];

//取得下標送卷活動項目內容
$query ="SELECT s.*, p.name pname
FROM `".$this->config->db[1]["dbname"]."`.`{$this->config->default_prefix}scode_promote` s
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p ON
	p.prefixid = s.prefixid
	AND p.productid = s.productid
WHERE
	s.`prefixid` = '{$this->config->default_prefix_id}'
	AND s.`switch` = 'Y'
	AND s.`behav` = 'h'
ORDER BY spid DESC
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rule_oscode"] = $recArr['table']['record'];

$query ="
SELECT *
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_limited`
WHERE
	used = 'Y'
	AND switch = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_limited"] = $recArr['table']['record'];

$query ="
SELECT COUNT(shid) AS cnt
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history`
WHERE
	prefixid = '".$this->config->default_prefix_id."'
	AND productid = '".$this->io->input["get"]["productid"]."'
	AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
if($recArr['table']['record'][0]['cnt'] > 0){
	$table["table"]["already_bid"] = 'true';
}else{
	$table["table"]["already_bid"] = 'false';
}
// Relation End
##############################################################################################################################################



##############################################################################################################################################
// Log Start
$prodcut_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_edit',
	`active` = '商品修改',
	`memo` = '{$prodcut_data}',
	`root` = 'product/edit',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################



$status["status"]["base_href"] = $status["status"]["path"] ."productid={$this->io->input["get"]["productid"]}";

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();