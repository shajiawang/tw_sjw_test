<?php

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start

// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(flash|productid|name|description|ontime|offtime|seq|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY p.insertt DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = " p.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if(!empty($this->io->input["get"]["search_productid"])){
	$status["status"]["search"]["search_productid"] = $this->io->input["get"]["search_productid"] ;
	$status["status"]["search_path"] .= "&search_productid=".$this->io->input["get"]["search_productid"] ;
	$sub_search_query .=  "
		AND p.`productid` like '%".$this->io->input["get"]["search_productid"]."%'";
}
if(!empty($this->io->input["get"]["search_name"])){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND p.`name` like '%".$this->io->input["get"]["search_name"]."%'";
}
if(!empty($this->io->input["get"]["search_description"])){
	$status["status"]["search"]["search_description"] = $this->io->input["get"]["search_description"] ;
	$status["status"]["search_path"] .= "&search_description=".$this->io->input["get"]["search_description"] ;
	$sub_search_query .=  "
		AND p.`description` like '%".$this->io->input["get"]["search_description"]."%'";
}
if(!empty($this->io->input["get"]["search_flash"])){
	$status["status"]["search"]["search_flash"] = $this->io->input["get"]["search_flash"] ;
	$status["status"]["search_path"] .= "&search_flash=".$this->io->input["get"]["search_flash"] ;
	$sub_search_query .=  "
		AND p.`is_flash` = '".$this->io->input["get"]["search_flash"]."'";
}
if(!empty($this->io->input["get"]["search_ontime"])){
	$status["status"]["search"]["search_ontime"] = $this->io->input["get"]["search_ontime"] ;
	$status["status"]["search_path"] .= "&search_ontime=".$this->io->input["get"]["search_ontime"] ;
	$sub_search_query .=  "
		AND p.`ontime` >= '".$this->io->input["get"]["search_ontime"]."'";
}
if(!empty($this->io->input["get"]["search_offtime"])){
	$status["status"]["search"]["search_offtime"] = $this->io->input["get"]["search_offtime"] ;
	$status["status"]["search_path"] .= "&search_offtime=".$this->io->input["get"]["search_offtime"] ;
	$sub_search_query .=  "
		AND p.`offtime` <= '".$this->io->input["get"]["search_offtime"]."'";
}
if(!empty($this->io->input["get"]["search_pcid"])){
	$status["status"]["search"]["search_pcid"] = $this->io->input["get"]["search_pcid"] ;
	$status["status"]["search_path"] .= "&search_pcid=".$this->io->input["get"]["search_pcid"] ;
	$category_search_query .=  "
	LEFT OUTER JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt` pcr ON
	p.prefixid = pcr.prefixid
	AND p.productid = pcr.productid
	AND pcr.`pcid` IN (".$status["status"]["search"]["search_pcid"].")";
	$sub_search_query .=  "
	AND pcr.pcid IS NOT NULL";
}
if(!empty($this->io->input["get"]["search_status"])){
	$status["status"]["search"]["search_status"] = $this->io->input["get"]["search_status"] ;
	$status["status"]["search_path"] .= "&search_status=".$this->io->input["get"]["search_status"] ;

	if ($this->io->input["get"]["search_status"] == 'no') {
		$sub_search_query .=  "
		and p.ontime > NOW()";
	}
	else if ($this->io->input["get"]["search_status"] == 'ontime') {
		$sub_search_query .=  "
		and p.offtime > NOW() and p.ontime <= NOW() ";
	}
	else if ($this->io->input["get"]["search_status"] == 'offtime') {
		$sub_search_query .=  "
		and p.offtime < NOW()";
	}
}
if(!empty($this->io->input["get"]["search_lb"])){
	$status["status"]["search"]["search_lb"] = $this->io->input["get"]["search_lb"] ;
	$status["status"]["search_path"] .= "&search_lb=".$this->io->input["get"]["search_lb"] ;
	$sub_search_query .=  "
		AND p.`lb_used` = '".$this->io->input["get"]["search_lb"]."'";
}
if(!empty($this->io->input["get"]["search_closed"])){
	$status["status"]["search"]["search_closed"] = $this->io->input["get"]["search_closed"] ;
	$status["status"]["search_path"] .= "&search_closed=".$this->io->input["get"]["search_closed"] ;
	$sub_search_query .=  "
		AND p.`closed` = '".$this->io->input["get"]["search_closed"]."'";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start

// Table Count Start
$query ="
SELECT
count(*) as num
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_rookie` p
{$category_search_query}
WHERE
p.prefixid = '".$this->config->default_prefix_id."'
AND p.switch = 'Y'
" ;
$query .= $sub_search_query ;
$query .= $sub_sort_query ;

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end

// Table Record Start
$query ="
SELECT
p.*
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_rookie` p
{$category_search_query}
WHERE
p.prefixid = '".$this->config->default_prefix_id."'
AND p.switch = 'Y'
" ;
$query .= $sub_search_query ;
$query .= $sub_sort_query ;
$query .= $query_limit ;
// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End

// Table End
##############################################################################################################################################




##############################################################################################################################################
// Relation Start
$query ="
SELECT
*
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category`
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

$query ="
SELECT
*
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store`
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["store"] = $recArr['table']['record'];

$query ="
SELECT
*
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."gift_rule`
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["gift_rule"] = $recArr['table']['record'];

if (is_array($table['table']['record']))
{
	foreach ($table['table']['record'] as $key => $value)
	{
		$query ="
		SELECT
		count(productid) as count
		FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history`
		WHERE
			prefixid = '".$this->config->default_prefix_id."'
			and productid = '{$value['productid']}'
			and type = 'bid'
			AND switch = 'Y'
		group by productid
		";
		$recArr = $this->model->getQueryRecord($query);
		$table["table"]["historycount"][$value['productid']] = $recArr['table']['record'];
	}
}
for ($i=0;$i<sizeof($table['table']['record']);$i++){
	$query ="SELECT p.* ,pt.original, pt.filename, pt.name imgname,
					                     pt2.original as original2, pt2.filename as filename2, pt2.name as name2,
										 pt3.original as original3, pt3.filename as filename3, pt3.name as name3,
										 pt4.original as original4, pt4.filename as filename4, pt4.name as name4
				FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rookie` p
				LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt ON
					p.prefixid = pt.prefixid
					AND p.ptid = pt.ptid
					AND pt.switch = 'Y'
				LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt2 ON
					p.prefixid = pt2.prefixid
					AND p.ptid2 = pt2.ptid
					AND pt2.switch = 'Y'
				LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt3 ON
					p.prefixid = pt3.prefixid
					AND p.ptid3 = pt3.ptid
					AND pt3.switch = 'Y'
				LEFT  JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt4 ON
					p.prefixid = pt4.prefixid
					AND p.ptid4 = pt4.ptid
					AND pt4.switch = 'Y'
				WHERE p.prefixid = '{$this->config->default_prefix_id}'
				AND p.`switch`='Y'
				AND p.productid = '{$table["table"]["record"][$i]["productid"]}'
				" ;
		$recArr = $this->model->getQueryRecord($query);
		$table["table"]["record"][$i]["img1"]=$recArr['table']['record'][0]['filename'];
		$table["table"]["record"][$i]["img2"]=$recArr['table']['record'][0]['filename2'];
		$table["table"]["record"][$i]["img3"]=$recArr['table']['record'][0]['filename3'];
		$table["table"]["record"][$i]["img4"]=$recArr['table']['record'][0]['filename4'];
		if ($_SERVER['SERVER_NAME']=='mgr.saja.com.tw'){
			$imgprefix="https://s3.hicloud.net.tw/img.saja.com.tw/site/images/site/product/";
		}else{
			$imgprefix="http://".$_SERVER['SERVER_NAME']."/site/images/site/product/";
		}

		$table["table"]["record"][$i]["img1"]=(empty($recArr['table']['record'][0]['filename']))?"":'<img src="'.$imgprefix.$recArr['table']['record'][0]['filename'].'" style="border: 1px solid;float:left;width:auto;height:50px"/>&nbsp;';
		$table["table"]["record"][$i]["img2"]=(empty($recArr['table']['record'][0]['filename2']))?"":'<img src="'.$imgprefix.$recArr['table']['record'][0]['filename2'].'" style="border: 1px solid;float:left;width:auto;height:50px"/>&nbsp;';
		$table["table"]["record"][$i]["img3"]=(empty($recArr['table']['record'][0]['filename3']))?"":'<img src="'.$imgprefix.$recArr['table']['record'][0]['filename3'].'" style="border: 1px solid;float:left;width:auto;height:50px"/>&nbsp;';
		$table["table"]["record"][$i]["img4"]=(empty($recArr['table']['record'][0]['filename4']))?"":'<img src="'.$imgprefix.$recArr['table']['record'][0]['filename4'].'" style="border: 1px solid;float:left;width:auto;height:50px"/>&nbsp;';
		$rtn = $this->model->getRecord("SELECT if (count(*)>1,'Y','N')as used FROM `saja_shop`.`saja_product` WHERE rookieid='".$table["table"]["record"][$i]["productid"]."'");

		$table["table"]["record"][$i]["used"]=$rtn[0]['used'];
}
$rtn=$this->model->getRecord("SELECT qty,handling_fee from saja_shop.saja_oscode_set_rule where switch='Y'");
$table["table"]['set_rule']=$rtn;
// Relation End
##############################################################################################################################################

##############################################################################################################################################
// Log Start
$prodcut_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product',
	`active` = '新手商品清單查詢',
	`memo` = '{$prodcut_data}',
	`root` = 'product/rookieview',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();