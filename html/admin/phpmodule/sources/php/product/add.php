<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
pc.*
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category` pc 
WHERE pc.prefixid = '".$this->config->default_prefix_id."'
AND switch='Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

/*
$query ="
SELECT 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store`
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["store"] = $recArr['table']['record'];
*/

$query ="
SELECT 
* 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."rule` 
WHERE prefixid = '".$this->config->default_prefix_id."'
AND switch = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["saja_rule"] = $recArr['table']['record'];

$query = "
SELECT 
s.*, sp.productid, cs.channelid
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store` s 
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt` sp ON 
	s.prefixid = sp.prefixid
	AND s.storeid = sp.storeid
	AND sp.productid = '".$this->io->input["get"]["productid"]."'
	AND sp.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	s.prefixid = cs.prefixid
	AND s.storeid = cs.storeid
	AND cs.switch = 'Y'
WHERE 
    s.prefixid = '".$this->config->default_prefix_id."'
    AND s.switch = 'Y'
GROUP BY s.storeid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["store_product_rt"] = $recArr['table']['record'];


$query = "
SELECT 
c.*, cs.storeid
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` c 
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel_store_rt` cs ON 
	c.prefixid = cs.prefixid
	AND c.channelid = cs.channelid
	AND cs.switch = 'Y'
WHERE 
    c.prefixid = '".$this->config->default_prefix_id."'
    AND c.switch = 'Y'
GROUP BY c.channelid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel_store_rt"] = $recArr['table']['record'];


//取得下標送卷活動項目內容
$query ="SELECT s.*, p.name pname
FROM `".$this->config->db[1]["dbname"]."`.`{$this->config->default_prefix}scode_promote` s 
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p ON
	p.prefixid = s.prefixid
	AND p.productid = s.productid
WHERE 
	s.`prefixid` = '{$this->config->default_prefix_id}' 
	AND s.`switch` = 'Y'
	AND s.`behav` = 'h'
ORDER BY spid DESC 
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["rule_oscode"] = $recArr['table']['record'];

$query ="
SELECT * 
FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_limited` 
WHERE 
	used = 'Y'
	AND switch = 'Y'
ORDER BY seq
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_limited"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_add', 
	`active` = '商品新增', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table',$table["table"]);
//$this->tplVar('status',$status["status"]);
$this->display();