<?php
// ini_set('display_errors', '1'); ini_set('display_startup_errors', '1'); error_reporting(E_ALL);
// Check Variable Start
if (empty($this->io->input["get"]["productid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
// Check Variable End

require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";

function createTicket($qrcodeImagePath,$productpath){
	$img_url='http://test.shajiawang.com/site/images/site/product/t.png';
	$img = imagecreatefrompng($img_url);
	//(16);

	header('Content-Type:image/png');
	error_log($qrcodeImagePath);

	$qCodeImg = imagecreatefromstring((file_get_contents($qrcodeImagePath)));
	error_log(23);
	list($qCodeWidth, $qCodeHight, $qCodeType) = getimagesize($qrcodeImagePath);
	$thumb = imagecreatetruecolor(100, 100);
	imagecopyresized($thumb, $qCodeImg, 0, 0, 0, 0, 100, 100, $qCodeWidth, $qCodeHight);
	error_log(26);
	imagecopymerge($img, $thumb, 25, 105, 0, 0, 100, 100, 100);
	$newfilename=md5(date("YmdHis")."_".rand(0,999)).'.png';
	error_log(29);
	error_log($productpath."/".$newfilename);
	imagejpeg($img,$productpath."/".$newfilename);
	error_log(33);
	die();
	imagedestroy($img);
	imagedestroy($thumb);
	imagedestroy($qCodeImg);
	return $newfilename;
}

$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];


##############################################################################################################################################
// 原商品 Record 
/*

*/
$query ="SELECT p.* ,pt.original, pt.filename, pt.name imgname,pt2.original as original2, pt2.filename as filename2, pt2.name as name2, pt3.original as original3, pt3.filename as filename3, pt3.name as name3, pt4.original as original4, pt4.filename as filename4, pt4.name as name4 
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p 
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt2 ON 
	p.prefixid = pt2.prefixid
	AND p.ptid2 = pt2.ptid
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt3 ON 
	p.prefixid = pt3.prefixid
	AND p.ptid3 = pt3.ptid	
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt4 ON 
	p.prefixid = pt4.prefixid
	AND p.ptid4 = pt4.ptid		
WHERE p.prefixid = '{$this->config->default_prefix_id}'
AND p.`switch`='Y' 
AND p.productid = '{$this->io->input["get"]["productid"]}'
" ;
$old_product = $this->model->getQueryRecord($query);

if (empty($old_product['table']['record'])) {
	$this->jsAlertMsg('商品錯誤!!');
}

$info_product = $old_product['table']['record'][0];
/*
票卷樣版148168 tmpl_product
 */

$query ="SELECT p.* ,pt.original, pt.filename, pt.name imgname,pt2.original as original2, pt2.filename as filename2, pt2.name as name2, pt3.original as original3, pt3.filename as filename3, pt3.name as name3, pt4.original as original4, pt4.filename as filename4, pt4.name as name4 
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` p 
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt ON 
	p.prefixid = pt.prefixid
	AND p.ptid = pt.ptid
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt2 ON 
	p.prefixid = pt2.prefixid
	AND p.ptid2 = pt2.ptid
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt3 ON 
	p.prefixid = pt3.prefixid
	AND p.ptid3 = pt3.ptid	
LEFT OUTER JOIN `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` pt4 ON 
	p.prefixid = pt4.prefixid
	AND p.ptid4 = pt4.ptid		
WHERE p.prefixid = '{$this->config->default_prefix_id}'
AND p.`switch`='Y' 
AND p.productid = '148168'
" ;
$ticket_product = $this->model->getRecord($query);
$ticket_product=$ticket_product[0];
//Relation exchange_product_thumbnail
if (!empty($info_product["filename"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename"])) {
		$filename = md5(date("YmdHis")."_".$info_product["name"]).".".pathinfo($info_product["filename"], PATHINFO_EXTENSION);
		
		copy($this->config->path_products_images."/".$info_product["filename"], $this->config->path_products_images."/".$filename);
		if ($_SERVER['SERVER_NAME']!='test.shajiawang.com') syncToS3($this->config->path_products_images."/".$filename,'s3://img.saja.com.tw','/site/images/site/product/');
	}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname"]}',
	`original`='{$info_product["original"]}',
	`filename`='{$info_product["filename"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid = $this->model->_con->insert_id;

	$update_thumbnail = "
	`ptid` = '{$ptid}',";

	/*if
	(file_exists($this->config->path_products_images."/".$info_product["filename2"]))
	{*/
		//var_dump("118:".$this->config->path_products_images."/".$info_product["filename"]);
		$filename2=createTicket($this->config->path_products_images."/".$info_product["filename"],$this->config->path_products_images);
		//var_dump("120".$this->config->path_products_images."/".$filename2);
		error_log(126);
		//$filename2 = md5(date("YmdHis")."_".$info_product["name2"]).".".pathinfo($info_product["filename2"], PATHINFO_EXTENSION);
		//copy($this->config->path_products_images."/".$info_product["filename"], $this->config->path_products_images."/".$filename2);
		if ($_SERVER['SERVER_NAME']!='test.shajiawang.com') syncToS3($this->config->path_products_images."/".$filename2,'s3://img.saja.com.tw','/site/images/site/product/');
	//}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname"]}',
	`original`='{$info_product["original"]}',
	`filename`='{$filename2}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid2 = $this->model->_con->insert_id;
	error_log(142);
	$update_thumbnail2 = "
	`ptid2` = '{$ptid2}',";	
}
/*
if (!empty($info_product["filename2"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename2"])) {
		$filename2 = md5(date("YmdHis")."_".$info_product["name2"]).".".pathinfo($info_product["filename2"], PATHINFO_EXTENSION);
		copy($this->config->path_products_images."/".$info_product["filename2"], $this->config->path_products_images."/".$filename2);
		syncToS3($this->config->path_products_images."/".$filename2,'s3://img.saja.com.tw','/site/images/site/product/');
	}
	
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname2"]}',
	`original`='{$info_product["original2"]}',
	`filename`='{$info_product["filename2"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid2 = $this->model->_con->insert_id;
	
	$update_thumbnail2 = "
	`ptid2` = '{$ptid2}',";
}
*/

if (!empty($info_product["filename3"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename3"])) {
		$filename3 = md5(date("YmdHis")."_".$info_product["name3"]).".".pathinfo($info_product["filename3"], PATHINFO_EXTENSION);
		copy($this->config->path_products_images."/".$info_product["filename3"], $this->config->path_products_images."/".$filename3);
		if ($_SERVER['SERVER_NAME']!='test.shajiawang.com') syncToS3($this->config->path_products_images."/".$filename3,'s3://img.saja.com.tw','/site/images/site/product/');
	}

	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname3"]}',
	`original`='{$info_product["original3"]}',
	`filename`='{$info_product["filename3"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid3 = $this->model->_con->insert_id;
	
	$update_thumbnail3 = "
	`ptid3` = '{$ptid3}',";
}

if (!empty($info_product["filename4"]) ) {
	if (file_exists($this->config->path_products_images."/".$info_product["filename4"])) {
		$filename4 = md5(date("YmdHis")."_".$info_product["name4"]).".".pathinfo($info_product["filename4"], PATHINFO_EXTENSION);
		copy($this->config->path_products_images."/".$info_product["filename4"], $this->config->path_products_images."/".$filename4);
		if ($_SERVER['SERVER_NAME']!='test.shajiawang.com') syncToS3($this->config->path_products_images."/".$filename4,'s3://img.saja.com.tw','/site/images/site/product/');
	}

	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET 
	`name`='{$info_product["imgname4"]}',
	`original`='{$info_product["original4"]}',
	`filename`='{$info_product["filename4"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$ptid4 = $this->model->_con->insert_id;
	
	$update_thumbnail4 = "
	`ptid4` = '{$ptid4}',";
}

##############################################################################################################################################


##############################################################################################################################################
// Table Start
$pname=$info_product['name']."-殺價券";
// 產生新商品
$description = str_replace("'", '', htmlspecialchars($ticket_product["description"]) );
$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product` SET
	`prefixid`		= '{$this->config->default_prefix_id}',
	`name`			= '{$pname}',
	`description`	= '{$description}',
	`retail_price`	= '25',
	`cost_price`	= '25',
	{$update_thumbnail}
	{$update_thumbnail2}
	{$update_thumbnail3}
	{$update_thumbnail4}	
	`thumbnail_url`	= '{$info_product["thumbnail_url"]}',
	`ontime`		= '{$ticket_product["ontime"]}',
	`offtime`		= '{$ticket_product["offtime"]}',
	`saja_limit`	= '{$ticket_product["saja_limit"]}',
	`user_limit`	= '{$ticket_product["user_limit"]}',
	`usereach_limit`= '{$ticket_product["usereach_limit"]}',
	`saja_fee`		= '{$ticket_product["saja_fee"]}',
	`process_fee`	= '{$ticket_product["process_fee"]}',
	`price_limit`	= '{$ticket_product["price_limit"]}',
	`bonus_type`	= '{$ticket_product["bonus_type"]}',
	`bonus`			= '{$ticket_product["bonus"]}',
	`gift_type`		= '{$ticket_product["gift_type"]}',
	`gift`			= '{$ticket_product["gift"]}',
	`vendorid`		= '{$ticket_product["vendorid"]}',
	`split_rate`	= '{$ticket_product["split_rate"]}',
	`base_value`	= '{$ticket_product["base_value"]}',
	`bid_type`		= '{$ticket_product["bid_type"]}',
	`mob_type`		= '{$ticket_product["mob_type"]}',
	`loc_type`		= '{$ticket_product["loc_type"]}',
	`is_flash`		= '{$ticket_product["is_flash"]}',
	`flash_loc`     = '{$ticket_product["flash_loc"]}',
	`flash_loc_map` = '{$ticket_product["flash_loc_map"]}',
	`bid_spoint`	= '{$ticket_product["bid_spoint"]}',
	`bid_scode`		= '{$ticket_product["bid_scode"]}',
	`display`		= '{$ticket_product["display"]}',
	`display_all`	= '{$ticket_product["display_all"]}',	
	`pay_type`		= '{$ticket_product["pay_type"]}',
	`seq`           = '{$ticket_product["seq"]}',
	`totalfee_type`	= '{$ticket_product["totalfee_type"]}',	
	`is_discount`	= '{$ticket_product["is_discount"]}',
	`is_exchange`	= '{$ticket_product["is_exchange"]}',
	`epid`			= '{$ticket_product["epid"]}',
	`hot_used`		= '{$ticket_product["hot_used"]}',
	`ptype`			= '{$ticket_product["ptype"]}',
	`ordertype`		= '{$ticket_product["ordertype"]}',
	`orderbonus`	= '{$ticket_product["orderbonus"]}',
	`everydaybid`	= '{$ticket_product["everydaybid"]}',
	`freepaybid`	= '{$ticket_product["freepaybid"]}',
	`codepid`		= '{$this->io->input["get"]["productid"]}',
	`codenum`		= '1',
	`insertt`       = now()
" ;
error_log(273);
error_log($query);
$this->model->query($query);
//新商品ID
$productid = $this->model->_con->insert_id;


//寫入商品分類rt
$query ="SELECT *
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_category_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_cat = $this->model->getQueryRecord($query);

//Relation product_category
//if($old_product_cat['table']['record'])
//{
//	foreach($old_product_cat['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_category_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`pcid`='166',
		`productid`='{$productid}',
		`seq`='10', 
		`insertt`=NOW()
		";
		$this->model->query($query);
//	}
//}

//寫入商店rt
$query ="SELECT *
FROM `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_store = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_store['table']['record'])
{
	foreach($old_product_store['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}store_product_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`storeid`='{$rv['storeid']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}', 
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

//寫入下標條件rt
$query ="SELECT *
FROM `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y' 
AND `productid` = '{$this->io->input["get"]["productid"]}'
";
$old_product_rule = $this->model->getQueryRecord($query);

//Relation product_category
if($old_product_rule['table']['record'])
{
	foreach($old_product_rule['table']['record'] as $rk => $rv) {
		$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`srid`='{$rv['srid']}',
		`value`='{$rv['value']}',
		`productid`='{$productid}',
		`seq`='{$rv['seq']}',
		`insertt`=NOW()
		";
		$this->model->query($query);
	}
}

// Table End
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_copy', 
	`active` = '商品複製寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product/copy', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$to = $this->config->default_main .'/product/edit/productid='. $productid .'&location_url='. $this->io->input['get']['location_url'];
error_log("379 ".$to);
header("location:". $to); //header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));