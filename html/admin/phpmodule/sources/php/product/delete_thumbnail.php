<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_product = $this->config->db[4]['dbname'];

// Check Variable Start
if(empty($this->io->input["get"]["productid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
if(empty($this->io->input["get"]["ptid2"]) && empty($this->io->input["get"]["ptid3"]) && empty($this->io->input["get"]["ptid4"])) {
	$this->jsAlertMsg('商品主圖ID錯誤!!');
}
// Check Variable End

##############################################################################################################################################
// Update Start

// exchange_product_thumbnail

if(!empty($this->io->input["get"]["ptid2"]) ) {

$query ="UPDATE `{$db_product}`.`{$this->config->default_prefix}product_thumbnail` SET 
	switch = 'N' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `ptid` = '{$this->io->input["get"]["ptid2"]}'
" ;
$this->model->query($query);  

// exchange_product
$query ="UPDATE `{$db_product}`.`{$this->config->default_prefix}product` SET 
	`ptid2` = '0' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `productid` = '{$this->io->input["get"]["productid"]}'
" ;
$this->model->query($query);  

}

if(!empty($this->io->input["get"]["ptid3"]) ) {

$query ="UPDATE `{$db_product}`.`{$this->config->default_prefix}product_thumbnail` SET 
	switch = 'N' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `ptid` = '{$this->io->input["get"]["ptid3"]}'
" ;
$this->model->query($query);  

// exchange_product
$query ="UPDATE `{$db_product}`.`{$this->config->default_prefix}product` SET 
	`ptid3` = '0' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `productid` = '{$this->io->input["get"]["productid"]}'
" ;
$this->model->query($query);  
}

if(!empty($this->io->input["get"]["ptid4"]) ) {

$query ="UPDATE `{$db_product}`.`{$this->config->default_prefix}product_thumbnail` SET 
	switch = 'N' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `ptid` = '{$this->io->input["get"]["ptid4"]}'
" ;
$this->model->query($query);  

// exchange_product
$query ="UPDATE `{$db_product}`.`{$this->config->default_prefix}product` SET 
	`ptid4` = '0' 
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `productid` = '{$this->io->input["get"]["productid"]}'
" ;
$this->model->query($query); 
}
// Update End
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url']);
// success message End
