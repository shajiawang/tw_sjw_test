<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

//正式機才上傳圖片至S3
//0.正式機  1.測試機
$is_test = 0;

//過濾商品名稱內 單引號 雙引號 右斜線
$product_name = $this->io->input["post"]["name"];
$product_name = str_replace('\\', '', $product_name);
$product_name = str_replace('\'', '', $product_name);
$product_name = str_replace('"', '', $product_name);

if (empty($product_name)) {
	$this->jsAlertMsg('商品名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('商品敘述錯誤!!');
}
// if (empty($this->io->input["post"]["rule"])) {
	// $this->jsAlertMsg('商品下標規則錯誤!!');
// }
if (!is_numeric($this->io->input["post"]["retail_price"]) || $this->io->input["post"]["retail_price"] <= 0) {
	$this->jsAlertMsg('商品市價錯誤!!');
}
if (!is_numeric($this->io->input["post"]["cost_price"]) || $this->io->input["post"]["cost_price"] <= 0) {
	$this->jsAlertMsg('商品成本價錯誤!!');
}
if (empty($this->io->input['files']['thumbnail'])) {
	// $this->jsAlertMsg('商品主圖錯誤!!');
}
if (strtotime($this->io->input["post"]["ontime"]) === false) {
	$this->jsAlertMsg('上架時間錯誤!!');
}
if (strtotime($this->io->input["post"]["offtime"]) === false) {
	$this->jsAlertMsg('結標時間錯誤!!');
}

// 檢查上下架時間的早晚
if(strtotime($this->io->input["post"]["ontime"])>=strtotime($this->io->input["post"]["offtime"])) {
    $this->jsAlertMsg('上架時間不可大於結標時間!!'); 	
}

if (!is_numeric($this->io->input["post"]["saja_limit"]) || (int)$this->io->input["post"]["saja_limit"] < 0) {
	$this->jsAlertMsg('最低標數限制錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('商品排序錯誤!!');
}
if (empty($this->io->input["post"]["vendorid"])) {
	$this->jsAlertMsg('供應商代碼錯誤!!');
}
if (!is_array($this->io->input["post"]["pcid"]) || empty($this->io->input["post"]["pcid"])) {
	$this->jsAlertMsg('商品分類錯誤!!');
}
if (!is_numeric($this->io->input["post"]["usereach_limit"]) || (int)$this->io->input["post"]["usereach_limit"] > 100) {
	$this->jsAlertMsg('會員每次連續下標次數設定異常!!');
}


if(!empty($this->io->input["post"]["bid_spoint"]) && !is_numeric($this->io->input["post"]["bid_spoint"]) ) {
	$this->jsAlertMsg('中標得殺幣點數錯誤!!');
}
if(!empty($this->io->input["post"]["bid_scode"]) && !is_numeric($this->io->input["post"]["bid_scode"]) ) {
	$this->jsAlertMsg('中標S碼活動編號錯誤!!');
}
if(!empty($this->io->input["post"]["bid_oscode"]) && !is_numeric($this->io->input["post"]["bid_oscode"]) ) {
	$this->jsAlertMsg('中標限定S碼活動編號錯誤!!');
}
if (!is_array($this->io->input["post"]["channelid"]) || empty($this->io->input["post"]["channelid"])) {
	$this->jsAlertMsg('經銷商選擇錯誤!!');
}
if (!is_array($this->io->input["post"]["storeid"]) || empty($this->io->input["post"]["storeid"])) {
	$this->jsAlertMsg('分館選擇錯誤!!');
}
if($this->io->input["post"]["is_exchange"] == 1){
	if (empty($this->io->input["post"]["epid"]) || $this->io->input["post"]["epid"] == 0) {
		$this->jsAlertMsg('兌換商品ID錯誤!!');
	}
}
if(!empty($this->io->input["post"]["ordertype"]) && !is_numeric($this->io->input["post"]["ordertype"]) ) {
	$this->jsAlertMsg('出貨方式錯誤!!');
}
if(!is_numeric($this->io->input["post"]["orderbonus"]) ) {
	$this->jsAlertMsg('出貨鯊魚點數量錯誤!!');
}
if($this->io->input["post"]["ordertype"] == 2 && $this->io->input["post"]["orderbonus"] == 0) {
	$this->jsAlertMsg('出貨鯊魚點數量不可為零!!');
}

if($this->io->input["post"]["ordertype"] == 3 && ($this->io->input["post"]["codepid"] == 0 || !is_numeric($this->io->input["post"]["codepid"]))) {
	$this->jsAlertMsg('殺價券關聯商品編號錯誤!!');
}else{
	$ctime = date('Y-m-d H:i:s');
	$atime = date("Y-m-d H:i:s",strtotime($ctime."+20 min"));
	$query ="
		SELECT p.* 
		FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p
		WHERE
			p.prefixid = '".$this->config->default_prefix_id."'
			AND p.productid = '".$this->io->input["post"]["codepid"]."'
			AND p.switch = 'Y'
			AND p.closed = 'N'
			AND p.`offtime` >= '{$atime}'
			" ;
	$table = $this->model->getQueryRecord($query);	
	
	if (empty($table['table']['record'][0]['productid'])){
		$this->jsAlertMsg('殺價券關聯商品異常!!');
	}
	
}
if($this->io->input["post"]["ordertype"] == 3 && ($this->io->input["post"]["codenum"] == 0 || !is_numeric($this->io->input["post"]["codenum"]))) {
	$this->jsAlertMsg('殺價券出貨數量錯誤!!');
}

if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$filename = md5(date("YmdHis")."_".$this->io->input['post']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);

	if (file_exists($this->config->path_products_images."/$filename")) {
		$this->jsAlertMsg('商品主圖名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/$filename")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}
	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input['post']['name']}',
		`original`='{$this->io->input['files']['thumbnail']['name']}',
		`filename`='{$filename}',
		`insertt`=now()
	";
	$this->model->query($query);
	$ptid = $this->model->_con->insert_id;
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}else{
	$this->jsAlertMsg('商品主圖錯誤!!');
}

if (!empty($this->io->input['files']['thumbnail2']) && exif_imagetype($this->io->input['files']['thumbnail2']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail2']['name']).".".pathinfo($this->io->input['files']['thumbnail2']['name'], PATHINFO_EXTENSION);
	$filename2 = md5(date("YmdHis")."_".$this->io->input['post']['name']."_2").".".pathinfo($this->io->input['files']['thumbnail2']['name'], PATHINFO_EXTENSION);

	if (file_exists($this->config->path_products_images."/$filename2")) {
		$this->jsAlertMsg('商品主圖二名稱重覆!!'.$filename2);
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail2']['tmp_name'], $this->config->path_products_images."/$filename2")) {
		$this->jsAlertMsg('商品主圖二上傳錯誤!!');
	}

	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input['post']['name']}',
		`original`='{$this->io->input['files']['thumbnail2']['name']}',
		`filename`='{$filename2}',
		`insertt`=now()
	";
	$this->model->query($query);
	$ptid2 = $this->model->_con->insert_id;
	$insert_thumbnail2 = " `ptid2` = '{$ptid2}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename2,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}

if (!empty($this->io->input['files']['thumbnail3']) && exif_imagetype($this->io->input['files']['thumbnail3']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail3']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$filename3 = md5(date("YmdHis")."_".$this->io->input['post']['name']."_3").".".pathinfo($this->io->input['files']['thumbnail3']['name'], PATHINFO_EXTENSION);

	if (file_exists($this->config->path_products_images."/$filename3")) {
		$this->jsAlertMsg('商品主圖三名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail3']['tmp_name'], $this->config->path_products_images."/$filename3")) {
		$this->jsAlertMsg('商品主圖三上傳錯誤!!');
	}

	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input['post']['name']}',
		`original`='{$this->io->input['files']['thumbnail3']['name']}',
		`filename`='{$filename3}',
		`insertt`=now()
	";
	$this->model->query($query);
	$ptid3 = $this->model->_con->insert_id;
	$insert_thumbnail3 = " `ptid3` = '{$ptid3}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename3,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}

if (!empty($this->io->input['files']['thumbnail4']) && exif_imagetype($this->io->input['files']['thumbnail4']['tmp_name'])) {
	//$filename = md5($this->io->input['files']['thumbnail4']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$filename4 = md5(date("YmdHis")."_".$this->io->input['post']['name']."_4").".".pathinfo($this->io->input['files']['thumbnail4']['name'], PATHINFO_EXTENSION);

	if (file_exists($this->config->path_products_images."/$filename4")) {
		$this->jsAlertMsg('商品主圖四名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail4']['tmp_name'], $this->config->path_products_images."/$filename4")) {
		$this->jsAlertMsg('商品主圖四上傳錯誤!!');
	}

	$query = "
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input['post']['name']}',
		`original`='{$this->io->input['files']['thumbnail4']['name']}',
		`filename`='{$filename4}',
		`insertt`=now()
	";
	$this->model->query($query);
	$ptid4 = $this->model->_con->insert_id;
	$insert_thumbnail4 = " `ptid4` = '{$ptid4}',";
	if($is_test == '0'){
		syncToS3($this->config->path_products_images."/".$filename4,'s3://img.saja.com.tw','/site/images/site/product/');
	}
}

// $description = htmlspecialchars($this->io->input["post"]["description"]);
// $description = ($this->io->input["post"]["description"]);
$description = str_replace('src='.'"'.'/site/images/site/upload/','src='.'"'.$this->config->protocol."://".$this->config->domain_name."/site/images/site/upload/", stripslashes($this->io->input["post"]["description"]));
if(!empty($this->io->input["post"]["price_limit"]) && floatval($this->io->input["post"]["price_limit"])>=0.01 ) {
	$price_limit = $this->io->input["post"]["price_limit"];
} else {
	$price_limit = '0.01';
}

if (($this->io->input["post"]["is_flash"] == "Y") && empty($this->io->input["post"]["flash_loc"]) ) {
	$flash_loc = "第一場";
} else {
	$flash_loc = $this->io->input["post"]["flash_loc"];
}

// 2020/03/12
if (!is_numeric($this->io->input["post"]["everydaybid"])) {
	$this->jsAlertMsg('每天付費可下標數錯誤!!');
}
if (!is_numeric($this->io->input["post"]["freepaybid"])) {
	$this->jsAlertMsg('每天免費可下標數錯誤!!');
}

$query ="
INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product`
SET
	`prefixid`		= '{$this->config->default_prefix_id}',
	`ptype`			= '{$this->io->input["post"]["ptype"]}',
	`name`			= '{$product_name}',
	`description`	= '{$description}',
	`rule`			= '{$this->io->input["post"]["rule"]}',
	`retail_price`	= '{$this->io->input["post"]["retail_price"]}',
	`cost_price` 	= '{$this->io->input["post"]["cost_price"]}',	
	`ptid`			= '{$ptid}',
	{$insert_thumbnail2}
	{$insert_thumbnail3}
	{$insert_thumbnail4}
	`thumbnail_url`	= '{$this->io->input["post"]["thumbnail_url"]}',
	`ontime`		= '{$this->io->input["post"]["ontime"]}',
	`offtime`		= '{$this->io->input["post"]["offtime"]}',
	`saja_limit`	= '{$this->io->input["post"]["saja_limit"]}',
	`user_limit`	= '{$this->io->input["post"]["user_limit"]}',
	`usereach_limit`= '{$this->io->input["post"]["usereach_limit"]}',
	`saja_fee`		= '{$this->io->input["post"]["saja_fee"]}',
	`process_fee`	= '{$this->io->input["post"]["process_fee"]}',
	`price_limit`	= '{$price_limit}',
	`bonus_type`	= '{$this->io->input["post"]["bonus_type"]}',
	`bonus`			= '{$this->io->input["post"]["bonus"]}',
	`gift_type`		= '{$this->io->input["post"]["gift_type"]}',
	`gift`			= '{$this->io->input["post"]["gift"]}',
	`vendorid`		= '{$this->io->input["post"]["vendorid"]}',
	`split_rate`	= '{$this->io->input["post"]["split_rate"]}',
	`base_value`	= '{$this->io->input["post"]["base_value"]}',
	`display`		= '{$this->io->input["post"]["display"]}',
	`display_all`	= '{$this->io->input["post"]["display_all"]}',
	`bid_type`		= '{$this->io->input["post"]["bid_type"]}',
	`mob_type`		= '{$this->io->input["post"]["mob_type"]}',
	`loc_type`		= '{$this->io->input["post"]["loc_type"]}',
	`is_flash`		= '{$this->io->input["post"]["is_flash"]}',
	`flash_loc`     = '{$flash_loc}',
	`flash_loc_map` = '{$this->io->input["post"]["flash_loc_map"]}',
	`bid_spoint`	= '{$this->io->input["post"]["bid_spoint"]}',
	`bid_scode`		= '{$this->io->input["post"]["bid_scode"]}',
	`bid_oscode`	= '{$this->io->input["post"]["bid_oscode"]}',
	`pay_type`		= '{$this->io->input["post"]["pay_type"]}',
	`seq`			= '{$this->io->input["post"]["seq"]}',
	`totalfee_type`	= '{$this->io->input["post"]["totalfee_type"]}',
	`is_big`		= '{$this->io->input["post"]["is_big"]}',
	`checkout_type`	= '{$this->io->input["post"]["checkout_type"]}',
	`checkout_money`= '{$this->io->input["post"]["checkout_money"]}',
	`insertt`		= now(),
	`is_test`		= '{$this->io->input["post"]["is_test"]}',
	`limitid`		= '{$this->io->input["post"]["limitid"]}',
	`hot_used`		= '{$this->io->input["post"]["hot_used"]}',
	`lb_used`		= '{$this->io->input["post"]["lb_used"]}',
	`is_discount`	= '{$this->io->input["post"]["is_discount"]}',
	`is_exchange`   = '{$this->io->input["post"]["is_exchange"]}',
	`epid`  		= '{$this->io->input["post"]["epid"]}',
	`ordertype`  	= '{$this->io->input["post"]["ordertype"]}',
	`orderbonus`  	= '{$this->io->input["post"]["orderbonus"]}',
	`everydaybid`  	= '{$this->io->input["post"]["everydaybid"]}',
	`freepaybid`  	= '{$this->io->input["post"]["freepaybid"]}',
	`codepid`  		= '{$this->io->input["post"]["codepid"]}',
	`codenum`  		= '{$this->io->input["post"]["codenum"]}'
	
" ;
// echo '<pre>';echo $query;exit;
$this->model->query($query);
$productid = $this->model->_con->insert_id;

//寫入商品分類rt
$values = array();
foreach($this->io->input["post"]["pcid"] as $rk => $rv) {
	$values[] = "
		('".$this->config->default_prefix_id."', '".$productid."', '".$rv."', '".(($rk+1)*10)."', NOW())";
}
$query = "
INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt`(
	`prefixid`,
	`productid`,
	`pcid`,
	`seq`,
	`insertt`
)
VALUES
".implode(',', $values);
$this->model->query($query);

//寫入商店rt
$store = $this->io->input["post"]["storeid"];
//$store = array(1);
$values = array();
foreach($store as $rk => $rv) {
	$values[] = "
		('".$this->config->default_prefix_id."', '".$productid."', '".$rv."', '".(($rk+1)*10)."', NOW())";
}
$query = "
INSERT INTO `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt`(
	`prefixid`,
	`productid`,
	`storeid`,
	`seq`,
	`insertt`
)
VALUES
".implode(',', $values);
$this->model->query($query);

//寫入下標條件rt
$srid = (!empty($this->io->input["post"]["srid"]) ) ? $this->io->input["post"]["srid"] : 'any_saja';
$srid_value = empty($this->io->input["post"][$srid."_value"]) ? '0' : (int)$this->io->input["post"][$srid."_value"];

$query = "INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_rule_rt` SET
	`prefixid`='{$this->config->default_prefix_id}',
	`productid`='{$productid}',
	`srid`='{$srid}',
	`value`='{$srid_value}',
	`seq`='".(($rk+1)*10)."',
	`insertt`=NOW()
";
$this->model->query($query);

//今日必殺同步新增判斷
if (!empty($this->io->input["post"]["is_today"]) && ($this->io->input["post"]["is_today"] == 'Y') ){
	$query ="
	INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_today`
	SET
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$product_name}',
		`link`='https://www.saja.com.tw/site/product/saja/?productid={$productid}',
		`description`='{$description}',
		`ptid`='{$ptid}',
		`thumbnail_url`='{$this->io->input["post"]["thumbnail_url"]}',
		`ontime`='{$this->io->input["post"]["ontime"]}',
		`offtime`='{$this->io->input["post"]["offtime"]}',
		`seq`='{$this->io->input["post"]["seq"]}',
		`productid`='{$productid}',
		`insertt`=now()
	" ;
	$this->model->query($query);
	$todayid = $this->model->_con->insert_id;
}

$redis = new Redis();
$redis->connect('sajacache01',6379);
$redis->del("inx_product_list");
$redis->del("product_category_list");
$redis->del("hot_product_list");
$redis->del("lb_product_list");



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_insert', 
	`active` = '商品新增寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
