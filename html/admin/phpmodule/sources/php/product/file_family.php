<?php
// 全家商品卡的整批匯入功能
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start
$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';

if($files['file_name']["error"] != 0 || empty($files['file_name']["size"]) ) {
	$this->jsAlertMsg('上傳錯誤!!');
}
else
{
	$product_File = $files['file_name']['name']; 
	
    if(is_uploaded_file($files['file_name']['tmp_name']) ) {
		move_uploaded_file($files['file_name']['tmp_name'], "./{$product_File}");
	}

	$handle = fopen($product_File, 'r+');
	if (is_resource($handle))
    {
        $insertt = date('Y-m-d H:i:s');
		$newData = array();
		
		$filesize = filesize($product_File);
		$data = fread($handle, $filesize);
		$data = mb_convert_encoding($data, "UTF-8", "big5");
		$newData = explode("\n", $data);
		//array_shift($newData);
		$newData = array_values(array_filter($newData));
		
		if (!empty($newData)) {
			foreach ($newData as $key => $value) {
				$dataArr[$key] = explode(",", $value);
			}
			// print_r($dataArr);exit;

			if (!empty($dataArr)) {

				//將第一列資料清除
				array_shift($dataArr);
				$dataArr = array_values(array_filter($dataArr));
				$count = 0;
				foreach ($dataArr as $key => $value) {

					//過濾商品名稱內 單引號 雙引號 右斜線
					$product_name = $value[1];
					$product_name = str_replace('\\', '', $product_name);
					$product_name = str_replace('\'', '', $product_name);
					$product_name = str_replace('"', '', $product_name);
					$ptype = $value[14];
					
					switch(round($value[2])) {
						case 100:
							$picm = 'family_100.jpg';
							break;						
						case 200:
							$picm = 'family_200.jpg';
							break;
						case 500:
							$picm = 'family_500.jpg';
							break;
						case 1000:
							$picm = 'family_1000.jpg';
							break;
					}
					$pics = 'family_detail_1.png';
					
					//商品主圖1
					// if($value[3] == ''){
						// $this->jsAlertMsg('商品主圖請勿空白!!');
					// }else{
						$query = "
						INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
							`prefixid`='saja',
							`name`='{$product_name}',
							`original`='{$picm}',
							`filename`='{$picm}',
							`insertt`=now()
						";
						$this->model->query($query);
						$ptid = $this->model->_con->insert_id;
					// }

					//商品主圖2
					// if($value[4] == ''){
						// $ptid2 = 0;
					// }else{
						$query = "
						INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
							`prefixid`='saja',
							`name`='{$product_name}',
							`original`='{$pics}',
							`filename`='{$pics}',
							`insertt`=now()
						";
						$this->model->query($query);
						$ptid2 = $this->model->_con->insert_id;
					// }

					//商品主圖3
					// if($value[5] == ''){
						// $ptid3 = 0;
					// }else{
						$query = "
						INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
							`prefixid`='saja',
							`name`='{$product_name}',
							`original`='{$pics}',
							`filename`='{$pics}',
							`insertt`=now()
						";
						$this->model->query($query);
						$ptid3 = $this->model->_con->insert_id;
					// }

					//商品主圖4
					// if($value[6] == ''){
						// $ptid4 = 0;
					// }else{
						$query = "
						INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product_thumbnail` SET
							`prefixid`='saja',
							`name`='{$product_name}',
							`original`='{$pics}',
							`filename`='{$pics}',
							`insertt`=now()
						";
						$this->model->query($query);
						$ptid4 = $this->model->_con->insert_id;
					// }
					
					$query =" 
					SELECT p.description, p.is_exchange, p.epid, p.ordertype, p.orderbonus  
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` p 
					WHERE 
						p.prefixid = '".$this->config->default_prefix_id."' 
						AND productid = 11715
					" ;
					$table = $this->model->getQueryRecord($query);
					
					if ($value[13] == '7'){
						$description = '<p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">◎溫馨提醒</span></p><p style="color:#696969;font-size:14px;line-height:24px;"><span style="color:#ff0000;">若您並非為&ldquo;新手殺友&rdquo;（已有得標紀錄），在您得標&ldquo;新手限定&rdquo;商品後，一律視同違反下標規則，將不予出貨。</span></p>'.$table['table']['record'][0]['description'];
					}else{
						$description = $table['table']['record'][0]['description'];
					} 
					
					$query ="
					INSERT INTO `{$this->config->db[4]["dbname"]}`.`{$this->config->default_prefix}product`
					SET
						`prefixid`		= 'saja',
						`ptype`			= '{$ptype}',
						`name`			= '{$product_name}',
						`retail_price`	= '{$value[2]}',
						`ptid`			= '{$ptid}',
						`ptid2`			= '{$ptid2}',
						`ptid3`			= '0',
						`ptid4`			= '0',
						`ontime`		= '{$value[10]}',
						`offtime`		= '{$value[11]}',
						`saja_limit`	= 0,
						`user_limit`	= 20,
						`usereach_limit`= 20,
						`saja_fee`		= '{$value[7]}',
						`process_fee`	= '{$value[8]}',
						`price_limit`	= '1',
						`bonus_type`	= 'ratio',
						`bonus`			= '100',
						`gift_type`		= 'none',
						`gift`			= '0',
						`vendorid`		= 'saja',
						`split_rate`	= '0',
						`base_value`	= '0',
						`display`		= 'Y',
						`display_all`	= 'Y',
						`bid_type`		= 'T',
						`mob_type`		= 'all',
						`loc_type`		= 'G',
						`is_flash`		= 'N',
						`bid_spoint`	= '0',
						`bid_scode`		= '0',
						`bid_oscode`	= '0',
						`pay_type`		= 'b',
						`seq`			= '1',
						`totalfee_type`	= 'F',
						`is_big`		= 'N',
						`checkout_type`	= 'all',
						`checkout_money`= '{$value[9]}',
						`insertt`		= now(),
						`is_test`		= 'N',
						`limitid`		= '{$value[13]}',
						`hot_used`		= 'Y',
						`lb_used`		= 'N',
						`is_discount`	= 'Y',
						`is_exchange`  	= '{$table['table']['record'][0]['is_exchange']}',
						`epid`  		= '{$table['table']['record'][0]['epid']}',
						`ordertype`  	= '{$table['table']['record'][0]['ordertype']}',
						`orderbonus`  	= '{$table['table']['record'][0]['orderbonus']}',							
						`description`   = '{$description}' 
					" ;
					// echo '<pre>';echo $query;exit;
					$this->model->query($query);
					$productid = $this->model->_con->insert_id;

					$pcid = explode("|", $value[12]);
					//寫入商品分類rt
					$res = array();
					foreach($pcid as $rk => $rv) {
						$res[] = "
							('".$this->config->default_prefix_id."', '".$productid."', '".$rv."', '".(($rk+1)*10)."', NOW())";
					}
					$query = "
					INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_category_rt`(
						`prefixid`,
						`productid`,
						`pcid`,
						`seq`,
						`insertt`
					)
					VALUES
					".implode(',', $res);
					$this->model->query($query);

					$store = array();
					$store[0] = 0;
					$values = array();
					foreach($store as $rk => $rv) {
						$values[] = "
							('".$this->config->default_prefix_id."', '".$productid."', '".$rv."', '".(($rk+1)*10)."', NOW())";
					}
					$query = "
					INSERT INTO `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."store_product_rt`(
						`prefixid`,
						`productid`,
						`storeid`,
						`seq`,
						`insertt`
					)
					VALUES
					".implode(',', $values);
					$this->model->query($query);

					unset($product_name);
					unset($ptid);
					unset($ptid2);
					unset($ptid3);
					unset($ptid4);
					unset($pcid);
					$count ++;
				}
			}
		}
		//exit;
        fclose($handle);
    }
    unlink($product_File);

	$redis = new Redis();
	$redis->connect('sajacache01',6379);
	$redis->del("inx_product_list");
	$redis->del("product_category_list");
	$redis->del("hot_product_list");
	$redis->del("lb_product_list");	
	
	$this->jsPrintMsg('已成功上傳 '. $count .' 筆!', $this->io->input['get']['location_url']);
}

// Insert Start
##############################################################################################################################################
//header("location:".urldecode(base64_decode($this->io->input['get']['location_url'])));