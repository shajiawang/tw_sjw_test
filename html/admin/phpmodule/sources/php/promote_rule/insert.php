<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('活動規則名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('活動規則敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Insert Start

$query ="
INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}promote_rule` SET
	`channelid`='{$this->io->input["post"]["channelid"]}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`prefixid`='{$this->config->default_prefix_id}',
	`seq`='{$this->io->input["post"]["seq"]}', 
	`insertt`=now()
" ;
$this->model->query($query);

// Insert Start
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$promote_rule_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'promote_rule_insert', 
	`active` = '活動規則新增寫入', 
	`memo` = '{$promote_rule_data}', 
	`root` = 'promote_rule/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));