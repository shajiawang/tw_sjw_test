<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

//過濾商品名稱內 單引號 雙引號 右斜線
$push_title = $this->io->input["post"]["title"];
$push_title = str_replace('\\', '', $push_title);
$push_title = str_replace('\'', '', $push_title);
$push_title = str_replace('"', '', $push_title);
if (empty($this->io->input["post"]["body"])) {
	$this->jsAlertMsg('推播內容錯誤!!');
}

$body = (empty($this->io->input["post"]["body"]))? "":$this->io->input["post"]["body"];
$productid = (empty($this->io->input["post"]["productid"]))? "":$this->io->input["post"]["productid"];
if ($productid=='') $productid=0;
$action = (empty($this->io->input["post"]["action"]))? "":$this->io->input["post"]["action"];
$url_title= (empty($this->io->input["post"]["url_title"]))? "":$this->io->input["post"]["url_title"];
$url = (empty($this->io->input["post"]["url"]))? "":$this->io->input["post"]["url"];
$sendto = (empty($this->io->input["post"]["sendto"]))? "":$this->io->input["post"]["sendto"];
$page = (empty($this->io->input["post"]["page"]))? "":$this->io->input["post"]["page"];
$groupset= empty($this->io->input["post"]["groupset"])?'':$this->io->input["post"]["groupset"];
$force_show= empty($this->io->input["post"]["force_show"])?'N':$this->io->input["post"]["force_show"];
if ($sendto!='') $force_show='N';
if (empty($sendto)) {
	$query = "SELECT token
		FROM saja_user.saja_user_device_token
		WHERE switch = 'Y'
		AND userid is not null
		AND userid <> ''";
	$table = $this->model->getQueryRecord($query);
}else{
	$sendtos=explode(",",$sendto);
	if (sizeof($sendtos)>1){
		$query = "SELECT token
			FROM saja_user.saja_user_device_token
			WHERE switch = 'Y'
			AND userid in ({$sendto})";
	}else{
		$query = "SELECT token
			FROM saja_user.saja_user_device_token
			WHERE switch = 'Y'
			AND userid = '{$sendto}' ";

	}
	$table = $this->model->getQueryRecord($query);
}

if (!empty($table["table"]["record"])) {
	$ids = array();
	foreach ($table["table"]["record"] as $key => $value) {
		$ids[]=$value['token'];
	}
	// 每批最多一千筆推播
	$ids = array_chunk($ids, 1000);
}else{
	echo "<script>alert('token為空!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
	exit();
}
error_log("send line 63".implode(",", $ids));
// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AAAAbCNeUVQ:APA91bGcY4z5iUZyWFxirakTqx6h5XR1-f9K-XMNuiUsUuqx9S2Js4Sk4KSywwlaUD4YjcAqIlLbkQQ289G_vI996dZDOU32Bu9XPGBxI7vWrHtOqTUg_rqP1iN0Nu4RxF2AswUfg93U' );

$action_list = array(
		'type'		=> $action,
		'url_title'	=> $url_title,
		'url'		=> $url,
		'imgurl'	=> '',
		'page'		=> $page,
		'productid'	=> $productid
	);
$action_list = json_encode($action_list);

$msg = array(
	'title'		=> $push_title,
	'body' 		=> $body,
	'vibrate'	=> $vibrate,
	'sound'		=> $sound
);

// $data = array(
// 	'action'	=> $action,
// 	'productid'	=> $productid,
// 	'url_title'	=> $url_title,
// 	'url'		=> $url,
// 	'action_list'=> "{$action_list}"
// );

$data = array(
	'action_list'=> "{$action_list}"
);

$headers = array(
	'Authorization: key=' . API_ACCESS_KEY,
	'Content-Type: application/json'
);

foreach ($ids as $key => $value) {
	//接收者token $ids
	$fields = array(
		'registration_ids' 	=> $value,
		'notification'		=> $msg,
		'data'				=> $data
	);

	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec( $ch );
	error_log("send line 117".$result);
	curl_close( $ch );
}

    /*
	$query ="
	INSERT INTO `$db_channel`.`{$this->config->default_prefix}push_msg`
	SET
		`title`			= '{$push_title}',
		`groupset`		= '{$groupset}',
		`body`			= '{$body}',
		`action`		= '{$this->io->input["post"]["action"]}',
		`productid`		= '{$productid}',
		`url_title`		= '{$this->io->input["post"]["url_title"]}',
		`url`			= '{$this->io->input["post"]["url"]}',
		`page`			= '{$this->io->input["post"]["page"]}',
		`sender`		= '{$this->io->input["session"]["user"]["userid"]}',
		`sendto`		= '{$sendto}',
		`sendtime`		= now(),
		`pushed`		= 'Y',
		`is_hand`       = 'Y',
		`push_type`     = 'H',
		`force_show`	='{$force_show}',
		`insertt`		= now()
	" ;
	*/
	error_log("99987 line 168 ".$sendto);
	//寫記錄的部份不需按人分開20191227
	if (empty($sendto))   {
		$query ="
		INSERT INTO `$db_channel`.`{$this->config->default_prefix}push_msg`
		SET
			`title`			= '{$push_title}',
			`groupset`		= '{$groupset}',
			`body`			= '{$body}',
			`action`		= '{$this->io->input["post"]["action"]}',
			`productid`		= '{$productid}',
			`url_title`		= '{$this->io->input["post"]["url_title"]}',
			`url`			= '{$this->io->input["post"]["url"]}',
			`page`			= '{$this->io->input["post"]["page"]}',
			`sender`		= '{$this->io->input["session"]["user"]["userid"]}',
			`sendtime`		= now(),
			`sendto`		= '',
			`pushed`		= 'Y',
			`is_hand`       = 'Y',
			`push_type`     = 'H',
			`force_show`	='{$force_show}',
			`insertt`		= now()
		" ;
	}else{
		$query ="
		INSERT INTO `$db_channel`.`{$this->config->default_prefix}push_msg`
		SET
			`title`			= '{$push_title}',
			`groupset`		= '{$groupset}',
			`body`			= '{$body}',
			`action`		= '{$this->io->input["post"]["action"]}',
			`productid`		= '{$productid}',
			`url_title`		= '{$this->io->input["post"]["url_title"]}',
			`url`			= '{$this->io->input["post"]["url"]}',
			`page`			= '{$this->io->input["post"]["page"]}',
			`sender`		= '{$this->io->input["session"]["user"]["userid"]}',
			`sendto`		= '{$sendto}',
			`sendtime`		= now(),
			`pushed`		= 'Y',
			`is_hand`       = 'Y',
			`push_type`     = 'H',
			`force_show`	='{$force_show}',
			`insertt`		= now()
		" ;
	}
	$this->model->query($query);


##############################################################################################################################################
// Log Start
$push_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'push_msg_hand_send',
	`active` = '即時推播修改寫入',
	`memo` = '{$push_data}',
	`root` = 'push_msg_hand/send',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

echo "<script>alert('發送成功!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";