<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

//過濾商品名稱內 單引號 雙引號 右斜線
$push_title = $this->io->input["post"]["title"];
$push_title = str_replace('\\', '', $push_title);
$push_title = str_replace('\'', '', $push_title);
$push_title = str_replace('"', '', $push_title);
if (empty($push_title)) {
	$this->jsAlertMsg('推播標題錯誤!!');
}
if (empty($this->io->input["post"]["body"])) {
	$this->jsAlertMsg('推播內容錯誤!!');
}
if (strtotime($this->io->input["post"]["sendtime"]) === false) {
	$this->jsAlertMsg('發送時間錯誤!!');
}

$body = implode("",$this->io->input["post"]["body"]);

$productid = $this->io->input["post"]["productid"];
// $productid = ($productid=='')? 'null' : $productid;
$productid = ($productid=='')? 0 : $productid;

$sendto = $this->io->input["post"]["sendto"];
$groupset= empty($this->io->input["post"]["groupset"])?'':$this->io->input["post"]["groupset"];
$force_show= empty($this->io->input["post"]["force_show"])?'N':$this->io->input["post"]["force_show"];

$sendto = ($sendto=='')? '' : $sendto;
if ($sendto!='') $force_show='N';

$query ="INSERT INTO `$db_channel`.`{$this->config->default_prefix}push_msg`
SET
	`title`			= '{$push_title}',
	`groupset`		= '{$groupset}',
	`body`			= '{$body}',
	`action`		= '{$this->io->input["post"]["action"]}',
	`productid`		= {$productid},
	`url_title`		= '{$this->io->input["post"]["url_title"]}',
	`url`			= '{$this->io->input["post"]["url"]}',
	`page`			= '{$this->io->input["post"]["page"]}',
	`sender`		= '{$this->io->input["session"]["user"]["userid"]}',
	`sendto`        = '{$sendto}',
	`sendtime`		= '{$this->io->input["post"]["sendtime"]}',
	`force_show`	= '{$force_show}',
	`insertt`		= now()
" ;
$this->model->query($query);


##############################################################################################################################################
// Log Start
$push_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'push_scode_insert',
	`active` = '推播排程新增寫入',
	`memo` = '{$push_data}',
	`root` = 'push_scode/insert',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
