<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["pushid"])) {
	$this->jsAlertMsg('推播編號錯誤!!');
}

//過濾商品名稱內 單引號 雙引號 右斜線
$push_title = $this->io->input["post"]["title"];
$push_title = str_replace('\\', '', $push_title);
$push_title = str_replace('\'', '', $push_title);
$push_title = str_replace('"', '', $push_title);

if (empty($push_title)) {
	$this->jsAlertMsg('推播標題錯誤!!');
}
if (empty($this->io->input["post"]["body"])) {
	$this->jsAlertMsg('推播內容錯誤!!');
}
if (strtotime($this->io->input["post"]["sendtime"]) === false) {
	$this->jsAlertMsg('發送時間錯誤!!');
}

$body = implode("",$this->io->input["post"]["body"]);

$productid = $this->io->input["post"]["productid"];
$productid = ($productid=='')? '0' : $productid;

$sendto = $this->io->input["post"]["sendto"];
$sendto = ($sendto=='')? 'null' : $sendto;
$groupset= empty($this->io->input["post"]["groupset"])?'':$this->io->input["post"]["groupset"];
$force_show= empty($this->io->input["post"]["force_show"])?'N':$this->io->input["post"]["force_show"];
if ($sendto!='') $force_show='N';
$query ="UPDATE `$db_channel`.`{$this->config->default_prefix}push_msg`
SET
	`title`			= '{$push_title}',
	`groupset`		= '{$groupset}',
	`body`			= '{$body}',
	`action`		= '{$this->io->input["post"]["action"]}',
	`productid`		= {$productid},
	`url_title`		= '{$this->io->input["post"]["url_title"]}',
	`url`			= '{$this->io->input["post"]["url"]}',
	`page`			= '{$this->io->input["post"]["page"]}',
	`sender`		= '{$this->io->input["session"]["user"]["userid"]}',
	`sendto`		= {$sendto},
	`force_show`	='{$force_show}',
	`sendtime`		= '{$this->io->input["post"]["sendtime"]}'
WHERE
	pushid = '{$this->io->input["post"]["pushid"]}'
	AND switch = 'Y'
" ;

$this->model->query($query);


##############################################################################################################################################
// Log Start
$push_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'push_scode_update',
	`active` = '推播排程修改寫入',
	`memo` = '{$push_data}',
	`root` = 'push_scode/update',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
