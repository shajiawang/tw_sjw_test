<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

// Check Variable Start

if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('商品名稱錯誤!!');
}

if($this->io->input["get"]['mod']!='add')
{
	if (empty($this->io->input["post"]["description"])) {
		$this->jsAlertMsg('商品說明錯誤!!');
	}
	if (empty($this->io->input["post"]["thumbnail"])) {
		//$this->jsAlertMsg('商品主圖錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["point_price"]) || $this->io->input["post"]["point_price"] =='') {
		$this->jsAlertMsg('兌換點數錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["process_fee"]) || $this->io->input["post"]["process_fee"] =='') {
		$this->jsAlertMsg('處理費錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["retail_price"]) || $this->io->input["post"]["retail_price"] =='') {
		$this->jsAlertMsg('商品市價錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["cost_price"]) || $this->io->input["post"]["cost_price"] =='') {
		$this->jsAlertMsg('進貨價錯誤!!');
	}
	if (strtotime($this->io->input["post"]["ontime"]) === false) {
		$this->jsAlertMsg('前台顯示時間錯誤!!');
	}
	if (strtotime($this->io->input["post"]["offtime"]) === false) {
		$this->jsAlertMsg('下架時間錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["seq"])) {
		$this->jsAlertMsg('排序錯誤!!');
	}
	if (!is_numeric($this->io->input["post"]["esid"])) {
		$this->jsAlertMsg('供應商錯誤!!'); //店家ID
	}
	if (!$this->io->input["post"]["epcid"]) {
		$this->jsAlertMsg('商品分類錯誤!!');
	}
}

// Check Variable End

##############################################################################################################################################
// Update Start

//exchange_product 新增商品
if($this->io->input["get"]['mod']=='add')
{
	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET 
		`prefixid`='{$this->config->default_prefix_id}',
		`name`='{$this->io->input["post"]["name"]}',
		`esid`='1',
		`insertt`= now()
	";
	$this->model->query($query);
	$productid = $this->model->_con->insert_id;


##############################################################################################################################################
// Log Start 
$exchange_lifepay_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_lifepay_insert', 
	`active` = '生活繳費新增寫入', 
	`memo` = '{$exchange_lifepay_data}', 
	`root` = 'exchange_lifepay/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

	
	//回傳狀態
	$ret['id'] = $productid;
	$ret['status'] = 1;
	echo json_encode($ret); 
	exit;
}


//exchange_product 異動商品

//Relation exchange_product_thumbnail
if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	$filename = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` SET 
	`name`='{$this->io->input["post"]["name"]}',
	`original`='{$this->io->input['files']['thumbnail']['name']}',
	`filename`='{$filename}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$eptid = $this->model->_con->insert_id;

	if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/{$filename}")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}
	
	$update_thumbnail = "`eptid` = '{$eptid}',";
	// syncToS3($this->config->path_products_images."/".$filename,'s3://img.saja.com.tw','/site/images/site/product/');

}


if (!empty($this->io->input['files']['thumbnail_file']) && exif_imagetype($this->io->input['files']['thumbnail_file']['tmp_name'])) {
	$filename_file = md5($this->io->input['files']['thumbnail_file']['name']).".".pathinfo($this->io->input['files']['thumbnail_file']['name'], PATHINFO_EXTENSION);
	
	$query = "
	INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` SET 
	`name`='{$this->io->input["post"]["name"]}',
	`original`='{$this->io->input['files']['thumbnail']['name']}',
	`filename`='{$filename_file}',
	`prefixid`='{$this->config->default_prefix_id}',
	`insertt`=now()
	";
	$this->model->query($query);
	$epftid = $this->model->_con->insert_id;

	if (!move_uploaded_file($this->io->input['files']['thumbnail_file']['tmp_name'], $this->config->path_products_images."/{$filename_file}")) {
		$this->jsAlertMsg('商品主圖上傳錯誤!!');
	}

	$update_thumbnail_file = "`epftid` = '{$epftid}',";
	// syncToS3($this->config->path_products_images."/".$filename_file,'s3://img.saja.com.tw','/site/images/site/product/');
}

	


//exchange_product
$description = (empty($this->io->input["post"]["description"])) ? '' : htmlspecialchars($this->io->input["post"]["description"]);

$query ="UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` SET 
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$description}',
	`eptype`='{$this->io->input["post"]["eptype"]}',	
	`tx_url`='{$this->io->input["post"]["tx_url"]}',	
	`thumbnail_url`= '{$this->io->input["post"]["thumbnail_url"]}',
	`point_price`='{$this->io->input["post"]["point_price"]}',
	`retail_price`='{$this->io->input["post"]["retail_price"]}',
	`process_fee`='{$this->io->input["post"]["process_fee"]}',
	`cost_price`='{$this->io->input["post"]["cost_price"]}',
	`ontime`='{$this->io->input["post"]["ontime"]}',
	`offtime`='{$this->io->input["post"]["offtime"]}',
	`esid`='{$this->io->input["post"]["esid"]}',
	`prid`='{$this->io->input["post"]["prid"]}',
	`prefixid`='{$this->config->default_prefix_id}',
	`mob_type`='{$this->io->input["post"]["mob_type"]}',
	{$update_thumbnail}
	{$update_thumbnail_file}
	`seq`='{$this->io->input["post"]["seq"]}',
	`use_type`='{$this->io->input["post"]["use_type"]}',
	`feedback_price`='{$this->io->input["post"]["feedback_price"]}'
	
WHERE
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `epid` = '{$this->io->input["post"]["epid"]}'
" ;
$this->model->query($query);
$epid = $this->model->_con->insert_id;


//Relation product_promote_rt 商品分類促銷
$query = "DELETE FROM `{$db_exchange}`.`{$this->config->default_prefix}product_promote_rt` 
WHERE `prefixid` = '{$this->config->default_prefix_id}'
AND `epid` = '{$this->io->input["post"]["epid"]}' ";
$this->model->query($query);

if($this->io->input["post"]["ppid"])
{
	$ppseq = $this->io->input["post"]["ppseq"];
	foreach($this->io->input["post"]["ppid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}product_promote_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`ppid`='{$rv}',
		`epid`='{$this->io->input["post"]["epid"]}',
		`seq`='{$ppseq[$rk]}',
		`switch` = 'Y', 
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query); 
	}
}

//Relation exchange_product_category_rt
$query = "UPDATE `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` 
SET `switch` = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `epid` = '{$this->io->input["post"]["epid"]}'
";
$this->model->query($query);

// print_r($this->io->input["post"]);
// exit;
if($this->io->input["post"]["epcid"])
{
	foreach($this->io->input["post"]["epcid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`epcid`='{$rv}',
		`epid`='{$this->io->input["post"]["epid"]}',
		`seq`='".(($rk+1)*10)."',
		`switch` = 'Y', 
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query);
	}
}

//Relation promote_rule_item
$query = "UPDATE `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item_product_rt` 
SET `switch` = 'N' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND `epid` = '{$this->io->input["post"]["epid"]}'
";
$this->model->query($query);

if($this->io->input["post"]["priid"])
{
	foreach($this->io->input["post"]["priid"] as $rk => $rv) {
		$query = "INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item_product_rt` SET
		`prefixid`='{$this->config->default_prefix_id}',
		`priid`='{$rv}',
		`epid`='{$this->io->input["post"]["epid"]}',
		`seq`='".(($rk+1)*10)."',
		`switch` = 'Y',		
		`insertt`=NOW() ";
		$query .= "ON DUPLICATE KEY UPDATE `switch` = 'Y' ";
		$this->model->query($query);
	}
}

// Update End
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$exchange_lifepay_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_lifepay_update', 
	`active` = '生活繳費修改寫入', 
	`memo` = '{$exchange_lifepay_data}', 
	`root` = 'exchange_lifepay/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


//header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
header("location:". $this->config->default_main .'/exchange_lifepay/' );