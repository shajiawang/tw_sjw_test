<?php
// Check Variable Start
if ($this->io->input["get"]['mod']!='add' && empty($this->io->input["get"]["epid"])) {
	$this->jsAlertMsg('商品ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

##############################################################################################################################################
// Table  Start 

// Table Content Start
$table['table']['record'][0] = array(
	'epid'=>'',
	'name'=>'',
	'description'=>'',
	'point_price'=>'',
	'process_fee'=>'',
	'retail_price'=>'',
	'cost_price'=>'',
	'seq'=>'0',
	'ontime'=>'',
	'offtime'=>'',
	'mob_type'=>'all',
	'thumbnail_url'=>'',
	'esid'=>'',
	'prid'=>'',
);

if($this->io->input["get"]['mod']!='add') 
{
	$query ="
	SELECT p.*, pt.filename as thumbnail, ptf.filename as thumbnail_file
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` pt ON  
		p.prefixid = pt.prefixid
		AND p.eptid = pt.eptid
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` ptf ON  
		p.prefixid = ptf.prefixid
		AND p.epftid = ptf.epftid		
	WHERE 
	p.`prefixid` = '{$this->config->default_prefix_id}' 
	AND p.`switch`='Y' 
	AND p.`epid` = '{$this->io->input["get"]["epid"]}'
	" ;
	$table = $this->model->getQueryRecord($query);
}

// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
if($this->io->input["get"]['mod']!='add') 
{
	$query ="
	SELECT ep.*, pcr.epid product_cid
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` ep 
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` pcr ON 
		ep.prefixid = pcr.prefixid
		AND ep.epcid = pcr.epcid
		AND pcr.epid = '{$this->io->input["get"]["epid"]}'
		AND pcr.switch = 'Y'
	WHERE 
	ep.prefixid = '{$this->config->default_prefix_id}' 
	AND ep.switch = 'Y'
	";
	$recArr = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

	//Relation stock
	$query ="
	SELECT SUM(num) num
	FROM `{$db_exchange}`.`{$this->config->default_prefix}stock`  
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch` = 'Y' 
	AND `epid` = '{$this->io->input["get"]["epid"]}'
	";
	$recArr = $this->model->getQueryRecord($query); 
	$table["table"]["rt"]["stock"] = ($recArr['table']['record'][0]['num']) ? $recArr['table']['record'][0]['num'] : 1;

	//Relation promote_rule_item_product_rt
	$query ="
	SELECT pr.* 
	FROM `{$db_exchange}`.`{$this->config->default_prefix}promote_rule_item_product_rt` pr 
	WHERE 
		pr.prefixid = '{$this->config->default_prefix_id}' 
		AND pr.switch = 'Y' 
		AND pr.epid = '{$this->io->input["get"]["epid"]}'
	";
	$recArr = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["promote_rule_item_product_rt"] = $recArr['table']['record'];

	//Relation exchange_product_thumbnail
	$query ="
	SELECT pt.* 
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail` pt ON 
		p.prefixid = pt.prefixid
		AND p.eptid = pt.eptid
		AND pt.switch = 'Y'
	WHERE 
		p.prefixid = '{$this->config->default_prefix_id}'
		AND p.epid = '{$this->io->input["get"]["epid"]}'
		AND p.switch = 'Y' 
	";
	$recArr = $this->model->getQueryRecord($query); 
	$table["table"]["rt"]["product_thumbnail"] = $recArr['table']['record'];

	$query ="
	SELECT ptf.* 
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_thumbnail_file` ptf ON 
		p.prefixid = ptf.prefixid
		AND p.epftid = ptf.epftid
		AND ptf.switch = 'Y'
	WHERE 
		p.prefixid = '{$this->config->default_prefix_id}'
		AND p.epid = '{$this->io->input["get"]["epid"]}'
		AND p.switch = 'Y' 
	";
	$recArr = $this->model->getQueryRecord($query); 
	$table["table"]["rt"]["product_thumbnail_file"] = $recArr['table']['record'];	
	
	//exchange_product_category_rt "兌換商品分類關聯"
	$product_category = $table["table"]["rt"]["product_category"];
	foreach($product_category as $ck => $cv){
		if ($cv['layer'] == 1 && !empty($cv['product_cid'])) {
			$product_category_rt[] = $cv['epcid'];						
		}
	}
	$table["table"]["rt"]["product_category_rt"] = implode(',', $product_category_rt);

	//Relation product_promote "商品促銷項目關聯"
	$query ="
	SELECT pp.* 
	FROM `{$db_exchange}`.`{$this->config->default_prefix}product_promote` pp
	WHERE 
		pp.prefixid = '{$this->config->default_prefix_id}' 
		AND pp.switch = 'Y' 
	";
	$recArr = $this->model->getQueryRecord($query); 
	$table["table"]["rt"]["product_promote"] = $recArr['table']['record'];
}
else
{
	//Relation exchange_product_category
	$query ="
	SELECT *
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
	WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y'
	";
	$recArr = $this->model->getQueryRecord($query);
	$table["table"]["rt"]["product_category"] = $recArr['table']['record'];
}

//Relation promote_rule
$query ="SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}promote_rule`
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["promote_rule"] = $recArr['table']['record'];
	
//Relation exchange_store
$query ="SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_store`  
WHERE `prefixid` = '{$this->config->default_prefix_id}' 
AND `switch`='Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_store"] = $recArr['table']['record'];
	
// Relation End 
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$exchange_lifepay_data = json_encode($this->io->input["get"]);

if ($this->io->input["get"]['mod']=='add'){
	$query = "
	INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
		`name` = '{$this->io->input['session']['user']["name"]}', 
		`userid` = '{$this->io->input['session']['user']["userid"]}',
		`kind` = 'exchange_lifepay_add', 
		`active` = '生活繳費新增', 
		`memo` = '{$exchange_lifepay_data}', 
		`root` = 'exchange_lifepay/add', 
		`ipaddress`='{$ip}', 
		`atime`=now(), 
		`insertt`=now()
	";
} else {
	$query = "
	INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
		`name` = '{$this->io->input['session']['user']["name"]}', 
		`userid` = '{$this->io->input['session']['user']["userid"]}',
		`kind` = 'exchange_lifepay_edit', 
		`active` = '生活繳費修改', 
		`memo` = '{$exchange_lifepay_data}', 
		`root` = 'exchange_lifepay/edit', 
		`ipaddress`='{$ip}', 
		`atime`=now(), 
		`insertt`=now()
	";
}

$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"] ."epid={$this->io->input["get"]["epid"]}";

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();