<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(epid|name|seq|modifyt)/";
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY p.insertt DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End


// Search Start
$status["status"]["search"] = "";
$category_search_query = "";
$sub_search_query = "";

if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND p.`name` like '%".$this->io->input["get"]["search_name"]."%'";
}
if($this->io->input["get"]["search_description"] != ''){
	$status["status"]["search"]["search_description"] = $this->io->input["get"]["search_description"] ;
	$status["status"]["search_path"] .= "&search_description=".$this->io->input["get"]["search_description"] ;
	$sub_search_query .=  "
		AND p.`description` like '%".$this->io->input["get"]["search_description"]."%'";
}
if($this->io->input["get"]["search_ontime"] != ''){
	$status["status"]["search"]["search_ontime"] = $this->io->input["get"]["search_ontime"] ;
	$status["status"]["search_path"] .= "&search_ontime=".$this->io->input["get"]["search_ontime"] ;
	$sub_search_query .=  "
		AND p.`ontime` >= '".$this->io->input["get"]["search_ontime"]."'";
}
if($this->io->input["get"]["search_offtime"] != ''){
	$status["status"]["search"]["search_offtime"] = $this->io->input["get"]["search_offtime"] ;
	$status["status"]["search_path"] .= "&search_offtime=".$this->io->input["get"]["search_offtime"] ;
	$sub_search_query .=  "
		AND p.`offtime` <= '".$this->io->input["get"]["search_offtime"]."'";
}
if($this->io->input["get"]["search_esid"] != ''){
	$status["status"]["search"]["search_esid"] = $this->io->input["get"]["search_esid"] ;
	$status["status"]["search_path"] .= "&search_esid=".$this->io->input["get"]["search_esid"] ;
	$sub_search_query .=  "
		AND p.`esid` = '".$this->io->input["get"]["search_esid"]."'";
}
if($this->io->input["get"]["search_epcid"] != ''){
	$status["status"]["search"]["search_epcid"] = $this->io->input["get"]["search_epcid"] ;
	$status["status"]["search_path"] .= "&search_epcid=".$this->io->input["get"]["search_epcid"] ;
	$category_search_query .= "
	LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` pcr ON 
		p.prefixid = pcr.prefixid 
		AND p.epid = pcr.epid 
		AND pcr.`epcid` = '{$this->io->input["get"]["search_epcid"]}' 
		AND pcr.`switch`='Y'
	";
	$sub_search_query .=  "
		AND pcr.epid IS NOT NULL";
}

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT count(*) as num
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
{$category_search_query}
WHERE 
p.prefixid = '{$this->config->default_prefix_id}' 
AND p.`switch`='Y'
AND p.epid < 300  
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT p.*
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
{$category_search_query}
WHERE 
p.prefixid = '{$this->config->default_prefix_id}' 
AND p.`switch`='Y'
AND p.epid < 300 
" ;
$query .= $sub_search_query ;
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query);  


if($table["table"]['record']){ 
foreach($table["table"]['record'] as $rk => $rv)
{
	$table["table"]['record'][$rk] = $rv;
	$table["table"]['record'][$rk]['ontime'] = ($rv['ontime'] && $rv['ontime']!='0000-00-00 00:00:00') ? date("Y-m-d", strtotime($rv['ontime'])).'<br>'.date("H:i:s", strtotime($rv['ontime'])) : '';
	$table["table"]['record'][$rk]['offtime'] = ($rv['offtime'] && $rv['offtime']!='0000-00-00 00:00:00') ? date("Y-m-d", strtotime($rv['offtime'])).'<br>'.date("H:i:s", strtotime($rv['offtime'])) : '';
	$table["table"]['record'][$rk]['profit'] = $rv['retail_price'] - $rv['cost_price'];
	$table["table"]['record'][$rk]['status'] = (time() <= strtotime($rv['offtime'])) ? '兌換中' : '未顯示';
	$table["table"]['record'][$rk]['modifyt'] = ($rv['modifyt'] && $rv['modifyt']!='0000-00-00 00:00:00') ? date("Y-m-d", strtotime($rv['modifyt'])).'<br>'.date("H:i:s", strtotime($rv['modifyt'])) : '';
	
	$query ="
	SELECT pc.name 
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category_rt` pcr
	INNER JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` pc ON 
	pc.prefixid = pcr.prefixid 
	AND pc.epcid = pcr.epcid
	AND pc.layer = '1' 
	AND pc.`switch`='Y'
	WHERE 
	pcr.prefixid = '{$this->config->default_prefix_id}' 
	AND pcr.epid = '{$rv['epid']}'
	AND pcr.`switch`='Y'
	";
	$recArr = $this->model->getQueryRecord($query); 
	
	if ($recArr['table']['record'] && is_array($recArr['table']['record']) ) {
		foreach($recArr['table']['record'] as $k2 => $v2){ $epc_info[$rk][$k2] = $v2["name"]; }
		$table["table"]['record'][$rk]['epcinfo'] = implode(',', $epc_info[$rk]);
		continue;
	} else {
		$table["table"]['record'][$rk]['epcinfo'] = '';
	}
}
}

// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 

$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query); 
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];


$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_store` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["store"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$exchange_lifepay_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_lifepay', 
	`active` = '生活繳費清單查詢', 
	`memo` = '{$exchange_lifepay_data}', 
	`root` = 'exchange_lifepay/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();