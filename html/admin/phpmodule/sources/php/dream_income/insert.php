<?php
// Check Variable Start
if (empty($this->io->input["post"]["dinum"])) {
	$this->jsAlertMsg('來源編號錯誤!!');
}
if (empty($this->io->input["post"]["diname"])) {
	$this->jsAlertMsg('來源名稱錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
$query ="
INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}dream_income` SET
	`prefixid`='{$this->config->default_prefix_id}',
	`dinum`='{$this->io->input["post"]["dinum"]}',
	`diname`='{$this->io->input["post"]["diname"]}',
	`switch` = '{$this->io->input["post"]["switch"]}',
	`insertt`=now()
" ;
$this->model->query($query);
$this->model->_con->insert_id;

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$dream_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'dream_income_insert', 
	`active` = '圓夢活動來源新增寫入', 
	`memo` = '{$dream_data}', 
	`root` = 'dream_income/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));