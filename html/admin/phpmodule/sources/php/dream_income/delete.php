<?php
// Check Variable Start
if(empty($this->io->input["get"]["diid"])) {
	$this->jsAlertMsg('來源編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// database start
$query ="UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}dream_income` 
SET switch = 'N' 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}'
	AND `diid` = '{$this->io->input["get"]["diid"]}'
";
$this->model->query($query);

// database end
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$dream_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'dream_income_delete', 
	`active` = '圓夢活動來源刪除', 
	`memo` = '{$dream_data}', 
	`root` = 'dream_income/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End

