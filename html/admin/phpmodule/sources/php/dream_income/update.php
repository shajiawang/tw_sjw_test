<?php
// Check Variable Start
if (empty($this->io->input["post"]["diid"])) {
	$this->jsAlertMsg('來源編號錯誤!!');
}
if (empty($this->io->input["post"]["diname"])) {
	$this->jsAlertMsg('來源標題錯誤!!');
}

// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
//Update Start
$query ="
UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}dream_income` SET 
	`dinum` = '{$this->io->input["post"]["dinum"]}',
	`diname` = '{$this->io->input["post"]["diname"]}' 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	 AND `diid` = '{$this->io->input["post"]["diid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$dream_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'dream_income_update', 
	`active` = '圓夢活動來源修改寫入', 
	`memo` = '{$dream_data}', 
	`root` = 'dream_income/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));