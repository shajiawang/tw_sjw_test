<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["gid"])) {
	$this->jsAlertMsg('禮券項目ID錯誤!!');
}
if (empty($this->io->input["post"]["grid"])) {
	$this->jsAlertMsg('禮券回饋規則ID錯誤!!');
}
if (empty($this->io->input["post"]["serial"])) {
	$this->jsAlertMsg('禮券序號錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('禮券項目排序錯誤!!');
}

$query ="
UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."gift_item`
SET
`grid` = '".$this->io->input["post"]["grid"]."',
`serial` = '".$this->io->input["post"]["serial"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND giid = '".$this->io->input["post"]["giid"]."'
" ;
$this->model->query($query);

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));