<?php
// Check Variable Start
if (empty($this->io->input["post"]["lid"])) {
	$this->jsAlertMsg('貸款類別ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('貸款類別名稱錯誤!!');
}
if (!is_numeric($this->io->input["post"]["loanid"])) {
	$this->jsAlertMsg('貸款類別代號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}loan` SET 
`name` = '{$this->io->input["post"]["name"]}',
`loanid`='{$this->io->input["post"]["loanid"]}'  
WHERE 
`lid` = '{$this->io->input["post"]["lid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$loan_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'loan_update', 
	`active` = '貸款類別修改寫入', 
	`memo` = '{$loan_data}', 
	`root` = 'loan/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));