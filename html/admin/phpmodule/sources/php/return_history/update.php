<?php
// Check Variable Start
if (empty($this->io->input["post"]["rhid"])) {
	$this->jsAlertMsg('退貨ID錯誤!!');
}
if (!is_numeric($this->io->input["post"]["num"])) {
	$this->jsAlertMsg('數量錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_exchange}`.`{$this->config->default_prefix}return_history` SET 
`num` = '{$this->io->input["post"]["num"]}', 
`returntype` = '{$this->io->input["post"]["returntype"]}' 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `rhid` = '{$this->io->input["post"]["rhid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$return_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'return_history_update', 
	`active` = '退貨紀錄修改寫入', 
	`memo` = '{$return_data}', 
	`root` = 'return_history/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));