<?php
// Check Variable Start
if(empty($this->io->input["get"]["esid"])) {
	$this->jsAlertMsg('店家ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_store`
WHERE 
`prefixid` = '".$this->config->default_prefix_id."' 
AND `switch`='Y' 
AND `esid` = '".$this->io->input["get"]["esid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT m.*
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_mall` m 
WHERE 
m.prefixid = '{$this->config->default_prefix_id}' 
AND m.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["exchange_mall"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_store_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_store_edit', 
	`active` = '兌換店家修改', 
	`memo` = '{$exchange_store_data}', 
	`root` = 'exchange_store/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();