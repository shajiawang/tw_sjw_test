<?php
// Check Variable Start
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('店家名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('店家敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('排序錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
//Insert Start

$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_store` SET 
	`prefixid`='{$this->config->default_prefix_id}',
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}', 
	`seq`='{$this->io->input["post"]["seq"]}', 
	`switch`='{$this->io->input["post"]["switch"]}',
	`emid`='{$this->io->input["post"]["emid"]}',
	`insertt`=now()
"; 
$this->model->query($query);

//Insert End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$exchange_store_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'exchange_store_insert', 
	`active` = '兌換店家新增寫入', 
	`memo` = '{$exchange_store_data}', 
	`root` = 'exchange_store/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));