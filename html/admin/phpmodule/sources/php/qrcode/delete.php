<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

// Check Variable Start
if(empty($this->io->input["get"]["qrid"])){
	$this->jsAlertMsg('參數錯誤,請聯絡技術人員!');
}
// Check Variable End

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]["dbname"];

// DELETE database start
$query ="UPDATE `".$db_cash_flow."`.`".$this->config->default_prefix."qrcode`
SET 
	switch = 'N'
WHERE 
	qrid = '".$this->io->input["get"]["qrid"]."'
" ;
$this->model->query($query);
// DELETE database end



##############################################################################################################################################
// Log Start 
$qrcode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'qrcode_delete', 
	`active` = '二維碼刪除', 
	`memo` = '{$qrcode_data}', 
	`root` = 'qrcode/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



// DELETE success message Start
$this->jsPrintMsg('刪除成功!!',$this->io->input["get"]["location_url"]);
// DELETE success message End