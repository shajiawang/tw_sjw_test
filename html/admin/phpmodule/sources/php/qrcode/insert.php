<?php
// Check Variable Start
if (!is_numeric($this->io->input["post"]["epcid"]) || $this->io->input["post"]["epcid"] =='') {
	$this->jsAlertMsg('商品分類編號不可為空白 !!');
}
if (!is_numeric($this->io->input["post"]["epcid"]) || $this->io->input["post"]["epcid"] =='') {
	$this->jsAlertMsg('商品編號不可為空白 !!');
}
if (strtotime($this->io->input["post"]["ontime"]) === false) {
	$this->jsAlertMsg('開始時間錯誤!!');
}
if (strtotime($this->io->input["post"]["offtime"]) === false) {
	$this->jsAlertMsg('到期時間錯誤!!');
}
if (!is_numeric($this->io->input["post"]["amount"]) || $this->io->input["post"]["amount"] =='') {
	$this->jsAlertMsg('價值錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];
 
##############################################################################################################################################
$snum = $this->io->input["post"]["qty"];

for($i=0; $i<(int)$snum;++$i) {
	
	$word = 'ABCDEFGHIJKLMNPQRSTUVWXYZ123456789';
	$len = strlen($word);
	$batch = date('Ymd');
	for ($m = 0; $m < 12; $m++) {
		$data[$i]['serial'] .= $word[rand() % $len];
	}

	$query ="
	INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}qrcode` SET
		`prefixid`='{$this->config->default_prefix_id}', 
		`name` = '{$data[$i]['serial']}',
		`userid` = '{$this->io->input["post"]["userid"]}',
		`url_app` = '{$this->io->input["post"]["url_app"]}',
		`url_web` = '{$this->io->input["post"]["url_web"]}',
		`epid` = '{$this->io->input["post"]["epid"]}',
		`epcid` = '{$this->io->input["post"]["epcid"]}',
		`spid` = 78,
		`used` = 'N',
		`ontime`='{$this->io->input["post"]["ontime"]}',
		`offtime`='{$this->io->input["post"]["offtime"]}',
		`used_time` = '0000-00-00 00:00:00',
		`amount` = {$this->io->input["post"]["amount"]},
		`orderid` = 0,
		`remark` = {$batch},
		`switch`= 'Y',
		`seq` = 0,
		`insertt`=now()
	" ;	
	$r = $this->model->query($query);
	// $this->model->_con->insert_id;
	// $total+=$r;
}
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$qrcode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'qrcode_insert', 
	`active` = '二維碼新增寫入', 
	`memo` = '{$qrcode_data}', 
	`root` = 'qrcode/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
// $this->jsPrintMsg('殺價券(userid:{$t['table']['record'][0]['userid']},epid:${epid}, total:${snum} ) : 新增 ${total} 組完成!!',$this->io->input["get"]["location_url"]);
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));