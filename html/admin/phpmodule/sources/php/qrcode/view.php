<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]["dbname"];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(qrid|userid|seq|insertt|modifyt|name)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "os.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}else{
	$sub_sort_query =  " ORDER BY os.qrid DESC ";
}
// Sort End


// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "AND os.userid ='{$this->io->input["get"]["search_userid"]}' ";
}
if($this->io->input["get"]["search_epid"] != ''){
	$status["status"]["search"]["search_epid"] = $this->io->input["get"]["search_epid"] ;
	$status["status"]["search_path"] .= "&search_epid=".$this->io->input["get"]["search_epid"] ;
	$sub_search_query .=  "AND os.epid ='{$this->io->input["get"]["search_epid"]}' ";
}
if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "AND os.`name` like '%".$this->io->input["get"]["search_name"]."%' ";
}
if($this->io->input["get"]["search_pname"] != ''){
	$status["status"]["search"]["search_pname"] = $this->io->input["get"]["search_pname"] ;
	$status["status"]["search_path"] .= "&search_pname=".$this->io->input["get"]["search_pname"] ;
	$sub_search_query .=  "AND p.`name` like '%".$this->io->input["get"]["search_pname"]."%' ";
}
if($this->io->input["get"]["search_spid"] != ''){
	$status["status"]["search"]["search_spid"] = $this->io->input["get"]["search_spid"] ;
	$status["status"]["search_path"] .= "&search_spid=".$this->io->input["get"]["search_spid"] ;
	$sub_search_query .=  "AND os.spid ='{$this->io->input["get"]["search_spid"]}' ";
}
if($this->io->input["get"]["search_orderid"] != ''){
	$status["status"]["search"]["search_orderid"] = $this->io->input["get"]["search_orderid"] ;
	$status["status"]["search_path"] .= "&search_orderid=".$this->io->input["get"]["search_orderid"] ;
	$sub_search_query .=  "AND os.orderid ='{$this->io->input["get"]["search_orderid"]}' ";
}
if(!empty($this->io->input["get"]["search_ontime"])){
	$status["status"]["search"]["search_ontime"] = $this->io->input["get"]["search_ontime"] ;
	$status["status"]["search_path"] .= "&search_ontime=".$this->io->input["get"]["search_ontime"] ;
	$sub_search_query .=  "
		AND os.`ontime` >= '".$this->io->input["get"]["search_ontime"]."'";
}
if(!empty($this->io->input["get"]["search_offtime"])){
	$status["status"]["search"]["search_offtime"] = $this->io->input["get"]["search_offtime"] ;
	$status["status"]["search_path"] .= "&search_offtime=".$this->io->input["get"]["search_offtime"] ;
	$sub_search_query .=  "
		AND os.`offtime` <= '".$this->io->input["get"]["search_offtime"]."'";
}
if($this->io->input["get"]["search_used"] != ''){
	$status["status"]["search"]["search_used"] = $this->io->input["get"]["search_used"] ;
	$status["status"]["search_path"] .= "&search_used=".$this->io->input["get"]["search_used"] ;
	$sub_search_query .=  "AND os.`used` = '{$this->io->input["get"]["search_used"]}' ";
}
if($this->io->input["get"]["search_remark"] != ''){
	$status["status"]["search"]["search_remark"] = $this->io->input["get"]["search_remark"] ;
	$status["status"]["search_path"] .= "&search_remark=".$this->io->input["get"]["search_remark"] ;
	$sub_search_query .=  "AND os.`remark` = '{$this->io->input["get"]["search_remark"]}' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="
SELECT count(*) as num 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}qrcode` os  
WHERE 
os.`prefixid` = '{$this->config->default_prefix_id}' 
AND os.`switch`='Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT os.*, p.name as panme 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}qrcode` os
LEFT JOIN `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p on 
	p.prefixid = os.prefixid 
	and p.epid = os.epid 
	and p.switch = 'Y' 
WHERE 
os.prefixid = '{$this->config->default_prefix_id}' 
AND os.switch = 'Y' 
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query); 
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$qrcode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'qrcode', 
	`active` = '二維碼清單查詢', 
	`memo` = '{$qrcode_data}', 
	`root` = 'qrcode/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();