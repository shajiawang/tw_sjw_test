<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` p 
WHERE 
p.prefixid = '".$this->config->default_prefix_id."'
and p.switch='Y' 
AND p.epid > 300 
ORDER BY p.seq
";
$table = $this->model->getQueryRecord($query);

//Relation exchange_product_category
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND switch = 'Y' 
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category"] = $recArr['table']['record'];

//Relation exchange_product_category2
$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND switch = 'Y' 
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category_level2"] = $recArr['table']['record'];
  
$query ="
SELECT `layer`
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product_category` 
WHERE 
`prefixid` = '{$this->config->default_prefix_id}'
AND switch = 'Y'
GROUP BY `layer` 
ORDER BY `layer` DESC
LIMIT 1
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["product_category_level"] = $recArr['table']['record'][0]['layer'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_qrcode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'qrcode_add', 
	`active` = '二維碼新增', 
	`memo` = '{$add_qrcode_data}', 
	`root` = 'qrcode/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->display();