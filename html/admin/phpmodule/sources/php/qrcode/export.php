<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]["dbname"];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);
if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}
$sub_sort_query =  " ORDER BY os.qrid DESC";

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$join_search_query = "";
if($data["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $data["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$data["search_userid"] ;
	$sub_search_query .=  "AND os.userid ='{$data["search_userid"]}' ";
}
if($data["search_epid"] != ''){
	$status["status"]["search"]["search_epid"] = $data["search_epid"] ;
	$status["status"]["search_path"] .= "&search_epid=".$data["search_epid"] ;
	$sub_search_query .=  "AND os.epid ='{$data["search_epid"]}' ";
}
if($data["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $data["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$data["search_name"] ;
	$sub_search_query .=  "AND os.`name` like '%".$data["search_name"]."%' ";
}
if($data["search_pname"] != ''){
	$status["status"]["search"]["search_pname"] = $data["search_pname"] ;
	$status["status"]["search_path"] .= "&search_pname=".$data["search_pname"] ;
	$sub_search_query .=  "AND p.`name` like '%".$data["search_pname"]."%' ";
}
if($data["search_spid"] != ''){
	$status["status"]["search"]["search_spid"] = $data["search_spid"] ;
	$status["status"]["search_path"] .= "&search_spid=".$data["search_spid"] ;
	$sub_search_query .=  "AND os.spid ='{$data["search_spid"]}' ";
}
if($data["search_orderid"] != ''){
	$status["status"]["search"]["search_orderid"] = $data["search_orderid"] ;
	$status["status"]["search_path"] .= "&search_orderid=".$data["search_orderid"] ;
	$sub_search_query .=  "AND os.orderid ='{$data["search_orderid"]}' ";
}
if(!empty($data["search_ontime"])){
	$status["status"]["search"]["search_ontime"] = $data["search_ontime"] ;
	$status["status"]["search_path"] .= "&search_ontime=".$data["search_ontime"] ;
	$sub_search_query .=  "
		AND os.`ontime` >= '".$data["search_ontime"]."'";
}
if(!empty($data["search_offtime"])){
	$status["status"]["search"]["search_offtime"] = $data["search_offtime"] ;
	$status["status"]["search_path"] .= "&search_offtime=".$data["search_offtime"] ;
	$sub_search_query .=  "
		AND os.`offtime` <= '".$data["search_offtime"]."'";
}
if($data["search_used"] != ''){
	$status["status"]["search"]["search_used"] = $data["search_used"] ;
	$status["status"]["search_path"] .= "&search_used=".$data["search_used"] ;
	$sub_search_query .=  "AND os.`used` = '{$data["search_used"]}' ";
}
if($data["search_remark"] != ''){
	$status["status"]["search"]["search_remark"] = $data["search_remark"] ;
	$status["status"]["search_path"] .= "&search_remark=".$data["search_remark"] ;
	$sub_search_query .=  "AND os.`remark` = '{$data["search_remark"]}' ";
}
// Search End

// Table Record Start
$query ="SELECT * 
FROM `".$db_cash_flow."`.`".$this->config->default_prefix."qrcode` os
{$join_search_query}
WHERE os.switch='Y'";

$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 

// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 


##############################################################################################################################################
// Log Start 
$qrcode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'qrcode_export', 
	`active` = '兌換商品二維碼(廠商)匯出', 
	`memo` = '{$qrcode_data}', 
	`root` = 'exchange_card/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";

$this->model->query($query);
// Log End 
##############################################################################################################################################


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Add some data

//會員資料表
$Report_Array[0]['title'] = array("A1" => "流水編號", "B1" => "二維碼內容", "C1" => "開始時間", "D1" => "到期時間", "E1" => "批號", "F1" => "建置日期");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{

		$content = '{"mtype":"app","act_type":78,"code":"'.$value['name'].'"}';

		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('A'.$row, $value['qrid']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, (string)$content,PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['ontime'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row, (string)$value['offtime'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['remark'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$row, (string)$value['insertt'],PHPExcel_Cell_DataType::TYPE_STRING);
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('二維碼管理');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="二維碼管理'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
	
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>