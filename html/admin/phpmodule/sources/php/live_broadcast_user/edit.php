<?php
// Check Variable Start
if (empty($this->io->input["get"]["lbuserid"])) {
	$this->jsAlertMsg('直播主ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `lbuserid` = '{$this->io->input["get"]["lbuserid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################

##############################################################################################################################################
// Relation Start 
//國家
$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."country` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
order by countryid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["country"] = $recArr['table']['record'];
//區域
$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."region` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
order by regionid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["region"] = $recArr['table']['record'];
//省
$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."province` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
order by provinceid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["province"] = $recArr['table']['record'];
//分區
$query ="
SELECT * FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."channel` 
WHERE 
	prefixid = '".$this->config->default_prefix_id."'
	AND switch = 'Y'
order by channelid
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["channel"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$broadcast_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'live_broadcast_user_edit', 
	`active` = '直播主修改', 
	`memo` = '{$broadcast_data}', 
	`root` = 'live_broadcast_user/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();