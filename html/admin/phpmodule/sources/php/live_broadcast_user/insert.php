<?php
// Check Variable Start
if (empty($this->io->input["post"]["lbname"])) {
	$this->jsAlertMsg('直播主名稱錯誤!!');
}
if (empty($this->io->input["post"]["phone"])) {
	$this->jsAlertMsg('直播主手機號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	$thumbnail = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["lbname"])).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	if (file_exists($this->config->path_lb_images."/$thumbnail")) {
		$this->jsAlertMsg('直播主主圖名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_lb_images."/$thumbnail")) {
		$this->jsAlertMsg('直播主主圖上傳錯誤!!');
	}
}

$query ="
INSERT INTO `{$db_channel}`.`{$this->config->default_prefix}live_broadcast_user` SET
	`prefixid`='{$this->config->default_prefix_id}',
	`lbname`='{$this->io->input["post"]["lbname"]}',
	`lbpic` = '{$thumbnail}',	
	`countryid`='{$this->io->input["post"]["countryid"]}', 	
	`regionid`='{$this->io->input["post"]["regionid"]}',
	`provinceid`='{$this->io->input["post"]["provinceid"]}', 
	`channel`='{$this->io->input["post"]["channel"]}',	
	`phone`='{$this->io->input["post"]["phone"]}',
	`gender`='{$this->io->input["post"]["gender"]}', 
	`ontime`='{$this->io->input["post"]["ontime"]}', 
	`offtime`='{$this->io->input["post"]["offtime"]}', 
	`userid`='{$this->io->input["post"]["userid"]}',	
	`seq`='{$this->io->input["post"]["seq"]}', 
	`insertt`=now()
" ;
$this->model->query($query);
$this->model->_con->insert_id;

##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$broadcast_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'live_broadcast_user_insert', 
	`active` = '直播主新增寫入', 
	`memo` = '{$broadcast_data}', 
	`root` = 'live_broadcast_user/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));