<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "/var/www/html/admin/libs/helpers.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$ret['msg'] = '';

//檢查訂單
$query ="SELECT o.*
FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o
LEFT OUTER JOIN `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` os ON
	os.prefixid = o.prefixid
	AND os.orderid = o.orderid
	AND os.switch = 'Y'
WHERE
	o.`prefixid` = '{$this->config->default_prefix_id}'
	AND o.`switch` = 'Y'
	AND o.`orderid` = '{$this->io->input["post"]["orderid"]}'
	AND os.orderid IS NOT NULL
LIMIT 1
";

$recArr = $this->model->getQueryRecord($query);

if ($this->io->input["post"]["type"] == "invoice"){

	if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
		//$thumbnail = md5($this->io->input['files']['thumbnail']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
		$thumbnail = md5(date("YmdHis")."_".htmlspecialchars($this->io->input["post"]["name"])).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);

		if (file_exists($this->config->path_invoice_images."/$thumbnail") && ($thumbnail != $this->io->input['post']['oldthumbnail'])) {
			$this->jsAlertMsg('發票圖名稱重覆!!');
		}
		else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_invoice_images."/$thumbnail")) {
			$this->jsAlertMsg('發票圖上傳錯誤!!');
		}

		//刪除舊圖
		if (!empty($this->io->input['post']['oldthumbnail']) && ($thumbnail != $this->io->input['post']['oldthumbnail'])) {
			unlink($this->config->path_invoice_images."/".$this->io->input['post']['oldthumbnail']);
		}

		$update_thumbnail = "`thumbnail` = '{$thumbnail}', ";
	}

	$query ="UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` SET
		{$update_thumbnail}
		`modifyt`=now()
	WHERE
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `orderid`='{$this->io->input["post"]["orderid"]}'
	" ;
	$this->model->query($query);

	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));

}
else if($this->io->input["post"]["type"] == "shipping_card"){

	if(empty($this->io->input["post"]["num"])){
		$this->jsAlertMsg('數量錯誤!!');
	}

	$this->model->query('start transaction');

	for($i = 0; $i < $this->io->input["post"]["num"]; $i ++){
		if(empty($this->io->input["post"]["code1"][$i]) || empty($this->io->input["post"]["codeformat1"][$i])){

			$this->model->query('ROLLBACK');
			$this->jsAlertMsg('第'.($i + 1).'張序號資訊錯誤!!');
			exit();

		}

		//新增商品卡序號
		$query ="INSERT INTO `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_card` SET
		`epid` = '{$this->io->input["post"]["epid"]}',
		`code1` = '{$this->io->input["post"]["code1"][$i]}',
		`codeformat1` = '{$this->io->input["post"]["codeformat1"][$i]}',
		`code2` = '{$this->io->input["post"]["code2"][$i]}',
		`codeformat2` = '{$this->io->input["post"]["codeformat2"][$i]}',
		`code3` = '{$this->io->input["post"]["code3"][$i]}',
		`codeformat3` = '{$this->io->input["post"]["codeformat3"][$i]}',
		`code4` = '{$this->io->input["post"]["code4"][$i]}',
		`codeformat4` = '{$this->io->input["post"]["codeformat4"][$i]}',
		`userid` = '{$this->io->input["post"]["userid"]}',
		`orderid` = '{$this->io->input["post"]["orderid"]}',
		`used` = 'Y',
		`insertt`=now()
		";
		$this->model->query($query);

	}
	$this->model->query('COMMIT');


	// //訂單狀態: 4 已到貨
	set_status($this->config, $this->model, '4', $this->io->input["post"]["orderid"]);
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
}
else
{

	if(empty($recArr['table']['record']) )
	{
		//新增訂單出貨
		$query ="INSERT INTO `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` SET
			`prefixid` = '{$this->config->default_prefix_id}',
			`orderid`='{$this->io->input["post"]["orderid"]}',
			`insertt`=now()
		";
		$this->model->query($query);
		$osid = $this->model->_con->insert_id;
	}

	if($this->io->input["post"]["type"] == 'back')
	{
		//退還 紅利點數
		//參考 saja_exchange.saja_exchange_bonus_history, saja_cash_flow.saja_bonus
		$backdata = back_bonus($this->config, $this->model, $this->io->input["post"]["orderid"], $recArr['table']['record']);
		if ($backdata == 'Y'){
			$this->jsAlertMsg('缺貨退費失敗!!');
		}else{
			//訂單狀態: 2 缺貨退費
			set_status($this->config, $this->model, '5', $this->io->input["post"]["orderid"]);
		}
		//目前無退貨部分
		$sql = "`returntime`='{$this->io->input["post"]["returntime"]}',
		`returnmemo`='{$this->io->input["post"]["returnmemo"]}',
		";
	}
	elseif ($this->io->input["post"]["type"] == 'backin')
	{
		//訂單狀態: 2 缺貨退費
		set_status($this->config, $this->model, '2', $this->io->input["post"]["orderid"]);
		$sql = "`backtime`='{$this->io->input["post"]["backtime"]}',
		`backmemo`='{$this->io->input["post"]["backmemo"]}',
		";
	}
	elseif ($this->io->input["post"]["type"] == 'close')
	{
		//訂單狀態: 1 已出貨(已扣除 紅利點數)
		//參考 saja_exchange.saja_exchange_bonus_history, saja_cash_flow.saja_bonus
		set_status($this->config, $this->model, '4', $this->io->input["post"]["orderid"]);

		$sql = "`closetime`='{$this->io->input["post"]["closetime"]}',
		`closememo`='{$this->io->input["post"]["closememo"]}',
		";
	}
	else
	{
		if ($this->io->input["post"]["type"] == 'shipping') {
			//訂單狀態: 1 已出貨(已扣除 紅利點數)
			//參考 saja_exchange.saja_exchange_bonus_history, saja_cash_flow.saja_bonus
			set_status($this->config, $this->model, '1', $this->io->input["post"]["orderid"]);
            // Add By Thomas 20190617
            /* 生活繳費 => 要做折讓
            if($recArr['table']['record'][0]['type']=='lifepay' &&
               $recArr['table']['record'][0]['total_fee']>0) {
               allowance($this->model, $recArr['table']['record'][0]['userid'], $recArr['table']['record'][0]['total_fee']);
            }
            */
		}

		$sql = "`outtime`='{$this->io->input["post"]["outtime"]}',
		`outtype`='{$this->io->input["post"]["outtype"]}',
		`outcode`='{$this->io->input["post"]["outcode"]}',
		`outmemo`='{$this->io->input["post"]["outmemo"]}',
		";
	}
	

	//修改訂單狀態
	if ($this->io->input["post"]["type"] == 'stat_modify')
	{
		$array = explode(',', $this->io->input["post"]["orderid"]);
		if($array)
		foreach ($array as $key => $orderid) {
		
			//$oid_status = $this->io->input["post"]["oid_status"];
			//$oid_memo = $this->io->input["post"]["oid_memo"]; 
			//參考 saja_exchange.saja_exchange_bonus_history, saja_cash_flow.saja_bonus
			$order_status = $this->io->input["post"]["order_status"];
			set_status($this->config, $this->model, $order_status, $orderid);
			
			//設定訂單異動記錄 saja_order_log
			$var['stat_memo'] = htmlspecialchars($this->io->input["post"]["status_memo"]);
			set_order_log($this->config, $this->model, $orderid, $var);
		}
	} 
	else 
	{
		$query ="UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` SET
			{$sql}
			`modifyt`=now()
		WHERE
			`prefixid` = '{$this->config->default_prefix_id}'
			AND `orderid`='{$this->io->input["post"]["orderid"]}'
		" ;
		$this->model->query($query);

		$query ="UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` SET
			`memo`='{$this->io->input["post"]["outmemo"]}',
			`modifyt`=now()
		WHERE
			`prefixid` = '{$this->config->default_prefix_id}'
			AND `orderid`='{$this->io->input["post"]["orderid"]}'
		" ;
		$this->model->query($query);
	}
	
	//如果type為close發送推播
	if ($this->io->input["post"]["type"] == 'close')
	{
		$push_info=array();
		if (!(empty($recArr['table']['record'][0]))){
			if ($recArr['table']['record'][0]['type']=='saja'){
				$query="SELECT p.productid,p.name FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product` p ,`{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o, `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp WHERE o.orderid='{$recArr["table"]["record"][0]["orderid"]}' and o.pgpid=pgp.pgpid and pgp.productid=p.productid";
			}else{
				$query="SELECT o.epid as productid,p.name FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_product` p ,`{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o, `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp WHERE o.orderid='{$recArr["table"]["record"][0]["orderid"]}' and o.epid=p.epid";
			}
			$productinfo = $this->model->getQueryRecord($query);
			$push_info['title']="訂單出貨通知";
			$push_info['body']="訂單編號:{$recArr['table']['record'][0]['orderid']} 商品{$productinfo['table']['record'][0]['name']}己完成出貨";
			$push_info['productid']="{$productinfo['table']['record'][0]['productid']}";
			$push_info['action']="0";
			$push_info['url_title']="";
			$push_info['url']="";
			$push_info['sendto']="{$recArr['table']['record'][0]['userid']}";
			$push_info['page']="9";
			$push_info['groupset']="";
			error_log("[push after_saja ]: ".json_encode($push_info));
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_URL, $_SERVER['HTTP_ORIGIN']."/site/push/goods_arrival");
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($push_info));
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($ch);
			error_log("[push after_saja ]:57 ".json_encode($result));
			$r=array();
			$r['retCode']=1;
			$r['msg']=$result;
			// echo json_encode($r);
			curl_close($ch);
		}
	}else if ($this->io->input["post"]["type"] == 'back'){
		$inp=array();
		$inp['title']="殺價王退費通知 - 訂單編號:{$recArr['table']['record'][0]['orderid']}";
		$inp['body']=$this->io->input["post"]["returnmemo"];
		$inp['productid']="";
		$inp['action']="";
		$inp['url_title']="";
		$inp['url']="";
		$inp['sender']=99;		
		$inp['sendto']="{$recArr['table']['record'][0]['userid']}";
		$inp['page']=0;
		$inp['target']='saja';
		$rtn=sendFCM($inp);

	}

}

function back_bonus($config, $model, $orderid, $orderdata)
{
	//取得扣除紅利點數記錄
	$query ="SELECT b.*
	FROM `{$config->db[3]['dbname']}`.`{$config->default_prefix}exchange_bonus_history` bh
	LEFT OUTER JOIN `{$config->db[1]['dbname']}`.`{$config->default_prefix}bonus` b ON
		bh.prefixid = b.prefixid
		AND bh.bonusid = b.bonusid
	WHERE
		bh.`prefixid` = '{$config->default_prefix_id}'
		AND bh.`switch` = 'Y'
		AND bh.`orderid` = '{$orderid}'
		AND b.bonusid IS NOT NULL
	LIMIT 1
	";
	$rs = $model->getQueryRecord($query);

	if(!empty($rs['table']['record']) )
	{
		$userid = $rs['table']['record'][0]['userid'];
		$countryid = $rs['table']['record'][0]["countryid"];
		
		if ($orderdata[0]['type'] !='lifepay' ){
			$amount = $rs['table']['record'][0]["amount"];
		} else {
			$amount = $orderdata[0]['point_price'];
		}
		
		$amount_b = abs($amount);
		//退還 紅利點數
		$query ="INSERT INTO `{$config->db[1]['dbname']}`.`{$config->default_prefix}bonus` SET
			`prefixid` 	= '{$config->default_prefix_id}',
			`userid`	= '{$userid}',
			`countryid`	= '{$countryid}',
			`behav`		= 'order_refund',
			`amount`	= '{$amount_b}',
			`insertt`	= now()
		";
		$model->query($query);
		$bonusid = $model->_con->insert_id;

		//寫入紅利點數記錄
		$query ="INSERT INTO `{$config->db[3]['dbname']}`.`{$config->default_prefix}exchange_bonus_history` SET
			`prefixid` 	= '{$config->default_prefix_id}',
			`userid`	= '{$userid}',
			`orderid`	= '{$orderid}',
			`bonusid`	= '{$bonusid}',
			`amount`	= '{$amount_b}',
			`insertt`	= now()
		";
		$model->query($query);

		// 計算免費鯊魚點餘額並更新
		$query = "SELECT sum(free_amount) total_free_amount
			FROM `{$config->db[1]['dbname']}`.`{$config->default_prefix}spoint_free_history`
			WHERE `userid`='{$userid}'
			AND   `switch` = 'Y'
			AND   `amount_type` = 2
			AND   `orderid` = '{$orderid}'
			";
		$table = $model->getQueryRecord($query);

		if (abs($table['table']['record'][0]['total_free_amount'])> 0) {

			$free_amount = ( abs($table['table']['record'][0]['total_free_amount']) >= abs($amount) ) ? abs($amount) : abs($table['table']['record'][0]['total_free_amount']);
			$query = "INSERT INTO `{$config->db[1]['dbname']}`.`{$config->default_prefix}spoint_free_history`
			SET
				`userid`='{$userid}',
				`behav` = 'order_refund',
				`amount_type` = 2,
				`free_amount` = '{$free_amount}',
				`total_amount` = '{$amount}',
				`bonusid` = '{$bonusid}',
				`orderid` = '{$orderid}',
				`switch` = 'Y',
				`insertt` = now()
			";
			$model->query($query);
		}
	}else{
		$error = 'Y';
		return $error;
		exit;
	}

	//取得商家紅利點數記錄
	$query = "SELECT bs.*
	FROM `{$config->db[3]['dbname']}`.`{$config->default_prefix}exchange_bonus_store_history` bsh
	LEFT OUTER JOIN `{$config->db[1]['dbname']}`.`{$config->default_prefix}bonus_store` bs ON
		bsh.prefixid = bs.prefixid
		AND bsh.bsid = bs.bsid
	WHERE
		bsh.`prefixid` = '{$config->default_prefix_id}'
		AND bsh.`orderid` = '{$orderid}'
		AND bsh.`switch` = 'Y'
		AND bs.bsid IS NOT NULL
	LIMIT 1
	";
	$rsArr = $model->getQueryRecord($query);

	if(!empty($rsArr['table']['record']) && !empty($bonusid))
	{
		$enterpriseid = $rsArr['table']['record'][0]['enterpriseid'];
		$esid = $rsArr['table']['record'][0]['esid'];
		$countryid = $rsArr['table']['record'][0]["countryid"];
		$amount = -$rsArr['table']['record'][0]["amount"];

		//扣除商家得到紅利點數
		$query = "insert into `{$config->db[1]['dbname']}`.`{$config->default_prefix}bonus_store` set
			`prefixid`      = '{$config->default_prefix_id}',
			`bonusid`       = '{$bonusid}',
			`esid`          = '{$esid}',
			`enterpriseid`  = '{$enterpriseid}',
			`countryid`     = '{$countryid}',
			`behav`         = 'order_refund',
			`amount`        = '{$amount}',
			`seq`           = '0',
			`switch`        = 'Y',
			`insertt`       = now()
		";
		$model->query($query);
		$bsid = $model->_con->insert_id;

		//寫入商家紅利點數記錄
		$query = "insert into `{$config->db[3]['dbname']}`.`{$config->default_prefix}exchange_bonus_store_history` set
			`prefixid`      = '{$config->default_prefix_id}',
			`esid`          = '{$esid}',
			`orderid`       = '{$orderid}',
			`bsid`          = '{$bsid}',
			`enterpriseid`  = '{$enterpriseid}',
			`amount`        = '{$amount}',
			`seq`           = '0',
			`switch`        = 'Y',
			`insertt`       = now()
		";
		$model->query($query);
	}
}

function set_order_log($config, $model, $orderid, $var)
{
	//取得記錄
	$query = "SELECT *
		FROM `{$config->db[3]['dbname']}`.`{$config->default_prefix}order_log`
		WHERE `switch` = 'Y'
			AND `orderid` = '{$orderid}'";
	$rsArr = $model->getQueryRecord($query);
	
	if(empty($rsArr['table']['record']) )
	{
		//新增
		$query = "INSERT INTO `{$config->db[3]['dbname']}`.`{$config->default_prefix}order_log` SET
			`prefixid`      = '{$config->default_prefix_id}',
			`orderid`       = '{$orderid}',
			`stat_memo`     = '{$var['stat_memo']}',
			`switch`        = 'Y',
			`insertt`       = now()
		";
		$model->query($query);
	
	} else {
		//修改
		$query ="UPDATE `{$config->db[3]['dbname']}`.`{$config->default_prefix}order_log` SET
			`stat_memo` = '{$var['stat_memo']}',
			`modifyt` = now()
		WHERE `orderid`='{$orderid}'
			AND `olid`='{$rsArr['table']['record'][0]['olid']}'
		" ;
		$model->query($query);
	}
}

function set_status($config, $model, $status, $orderid)
{
	$query ="UPDATE `{$config->db[3]['dbname']}`.`{$config->default_prefix}order` SET
		`status` ='{$status}',
		`modifyt`=now()
	WHERE
		`prefixid` = '{$config->default_prefix_id}'
		AND `orderid`='{$orderid}'
	" ;
	$model->query($query);
}

// add By Thomas 20190614
// functions for allowance -- begin
function allowance($db, $userid, $allowance_amt) {

        error_log("[allowance] ${userid} => ${allowance_amt}");
        $ret=array();
        if(empty($userid)) {
           $ret['retCode']=-1;
           $ret['retMsg']='缺用戶編號 !!';
           return $ret;
        }
        if(empty($allowance_amt)) {
           $ret['retCode']=-2;
           $ret['retMsg']='缺折讓金額 !!';
           return $ret;
        }
        // 取得用戶有折讓中的發票 (依發票時間 asc & 可折讓金額 asc 排列)
        $invoice_list_in_allowance = getInvoiceListInAllowance($db, $userid, 'S');
        if($invoice_list_in_allowance){
            // error_log("invoice_list_in_allowance : ".json_encode($invoice_list_in_allowance));
            foreach($invoice_list_in_allowance as $item){
              $invoice_list[]=$item;
            }
        }

        // 取得用戶未折讓的發票, (依發票時間 asc & 可折讓金額 asc 排列)
        $invoice_list_no_allowance  = getInvoiceListNotInAllowance($db, $userid, 'S');
        if($invoice_list_no_allowance){
            // error_log("invoice_list_no_allowance : ".json_encode($invoice_list_no_allowance));
            foreach($invoice_list_no_allowance as $item){
                $invoice_list[]=$item;
            }
        }

        // 保留次序  將兩批資料合併(已折讓過的發票在前面)
        if(!$invoice_list || count($invoice_list)<1){
            $ret['retCode']=-3;
            $ret['retMsg']='該用戶無發票可折讓 !!';
            return $ret;
        }

        $amount = $allowance_amt;
        $arrAllowanceItem=array();
        foreach($invoice_list as $item){
            if($amount>0){
                $allowanceItem=array();
                $allowanceItem['tax_type']='1';
                $allowanceItem['ori_invoice_date']=$item['invoice_datetime'];
                $allowanceItem['ori_invoiceno']=$item['invoiceno'];
                // 折讓明細目前只有一項  先寫死
                $allowanceItem['quantity']=1;
                $allowanceItem['ori_description']=$item['description'];
                // 發票剩餘可折讓金額
                $allowable_allowance_amt = $item['total_amt']-$item['allowance_amt'];
                if($allowable_allowance_amt<=0)
                   continue;
                if($amount>=$allowable_allowance_amt){
                   // 當折讓金額>=發票可折讓金額時 發票全部折讓掉
                   $item['allowance_amt']=$allowable_allowance_amt;
                   $allowanceItem['amount']=$allowable_allowance_amt;
                   $allowance_no_tax = round($allowanceItem['amount']/(1.0+$item['tax_rate']));
				   $allowanceItem['tax'] = $allowanceItem['amount']-$allowance_no_tax;
				   // $allowanceItem['tax']=round($allowable_allowance_amt*$item['tax_rate']);
                   $allowanceItem['unitprice']=$allowable_allowance_amt;
                   $amount-=$allowable_allowance_amt;
                }else if($amount<$allowable_allowance_amt){
                   // 當 剩餘折讓金額<發票可折讓金額時 發票僅部分折讓
                   $item['allowance_amt']=$amount;
                   $allowanceItem['amount']=$amount;
				   $allowance_no_tax = round($allowanceItem['amount']/(1.0+$item['tax_rate']));
				   $allowanceItem['tax'] = $allowanceItem['amount']-$allowance_no_tax;
                   // $allowanceItem['tax']=round($amount*$item['tax_rate']);
                   $allowanceItem['unitprice']=$amount;
                   $amount=0;
                }
                array_push($arrAllowanceItem,$allowanceItem);
            }
        }

        if($amount>0){
            $ret['retCode']=-3;
            $ret['retMsg']='可折讓金額不足 !!';
            return $ret;
        }

        // 統計折讓稅額
        $allowance_tax_amt = 0;
        foreach($arrAllowanceItem as $item){
            $allowance_tax_amt+=$item['tax'];
        }
        error_log("arrAllowanceItem : ".json_encode($arrAllowanceItem));

        // 新增allowance主檔
        $arrAllowance = array();
        $arrAllowance['allowance_no']=time();
        $arrAllowance['userid']=$userid;
        $arrAllowance['allowance_date']=date('Ymd');
        $arrAllowance['invoice_buyer_id']='0000000000';
        $arrAllowance['invoice_buyer_name']=$userid;
        $arrAllowance['allowance_amt']=$allowance_amt;
        $arrAllowance['allowance_tax_amt']=$allowance_tax_amt;

        // 1:買方開立折讓證明單
        // 2:賣方開立折讓證明通知單
        $arrAllowance['allowance_type']='2';
        $salid = createAllowance($db, $arrAllowance);
        if($salid){
            // 新增allowance明細
            $arrAllowance['salid']= $salid;
            $seq_no=1;
            foreach($arrAllowanceItem as $item){
                $item['salid']=$salid;
                $item['seq_no']=str_pad($seq_no,3,"0",STR_PAD_LEFT);
                $saliid = createAllowanceItem($db, $item);
                if($saliid>0){
                      ++$seq_no;
                      // 修改發票資料的allowance_amt
                      $invoiceData = getInvoiceByNo($db, $item['ori_invoiceno']);
                      if($invoiceData){
                        $addAmt = $invoiceData['allowance_amt']+$item['amount'];
                        updateInvoice($db, array("allowance_amt"=>$addAmt), array("invoiceno"=>$item['ori_invoiceno']));
                      }
                }
            }
            $ret['retCode']=1;
            $ret['retMsg']='OK';
            $arrAllowance['items']=$arrAllowanceItem;
            $ret['retObj']=$arrAllowance;
        }
        return $ret;
}

// functions for allowance --end
// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$order_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'order_update',
	`active` = '訂單修改寫入',
	`memo` = '{$order_data}',
	`root` = 'order/update',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

$ret['msg'] = '資料已送出';

echo json_encode($ret);