<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

// $sub_sort_query =  " ORDER BY orderid DESC ";
$sub_sort_query =  " group by orderid ORDER BY orderid ASC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_orderid"] != ''){
	$sub_search_query .=  "AND t.`orderid` in (".$this->io->input["get"]["search_orderid"].") ";
}
if($this->io->input["get"]["search_userid"] != ''){
	$sub_search_query .=  "AND t.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
}
if($this->io->input["get"]["search_confirm"] != '' && $this->io->input["get"]["search_confirm"] != 'ALL'){
	$sub_search_query .=  "AND t.`confirm` ='".$this->io->input["get"]["search_confirm"]."' ";
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$sub_search_query .=  "AND t.`insertt` BETWEEN '".$this->io->input["get"]["search_btime"]."' 
	AND '".$this->io->input["get"]["search_etime"]."' ";
}
if($this->io->input["get"]["search_status"] != '' && $this->io->input["get"]["search_status"] != 'ALL'){
	$sub_search_query .=  "AND t.`status` ='".$this->io->input["get"]["search_status"]."' ";
}
if($this->io->input["get"]["search_type"] != '' && $this->io->input["get"]["search_type"] != 'ALL'){
	$sub_search_query .=  "AND t.`type` ='".$this->io->input["get"]["search_type"]."' ";
}
if($this->io->input["get"]["search_epname"] != ''){
	$sub_search_query .=  "AND t.`epname` like '%".$this->io->input["get"]["search_epname"]."%' ";
}
// Search End

$query_search = '';
if ($data['tester'] == 1) {
	$query_search .= ' AND u.`userid` not in (1, 3, 9, 28, 116, 507, 585, 586, 588) ';
}

$query ="SELECT
				t.*, u.`name` username,
				es.`name` esname, 
				IFNULL(IFNULL(oc.phone, up.rphone), up.phone) as getphone, 
				IFNULL(oc.address, up.address) as getaddress, 
				IFNULL(oc.name, up.addressee) as getname,
				(SELECT usa.phone as smsphone 
					FROM `{$db_user}`.`{$this->config->default_prefix}user_sms_auth` usa 
					WHERE 
						u.prefixid = 'saja' 
						AND u.switch = 'Y' 
						AND u.userid = usa.userid
				) as smsphone 
			FROM
				(
					SELECT
						o.orderid,
						o.insertt,
						o.`status`,
						(
							CASE o.`status`
							WHEN '1' THEN
								'配送中'
							WHEN '2' THEN
								'退費中'
							WHEN '3' THEN
								'已發送'
							WHEN '4' THEN
								'已到貨'
							WHEN '5' THEN
								'已退費'
							ELSE
								'處理中'
							END
						) AS `statusname`,
						o.confirm,
						o.epid,
						o.esid,
						o.userid,
						o.type,
						'得標' typename,
						o.num,
						o.point_price,
						o.total_fee,
						o.discount_fee,
						o.process_fee,
						o.profit,
						o.memo,
						o.tx_data,
						o.pgpid,
						p. NAME AS epname, 
						p.retail_price
					FROM
						saja_exchange.saja_order o
					LEFT JOIN saja_shop.saja_pay_get_product pgp ON o.pgpid = pgp.pgpid
						AND pgp.prefixid = 'saja'
						AND pgp.switch = 'Y'
					LEFT JOIN saja_shop.saja_product p ON p.productid = pgp.productid
						AND p.prefixid = 'saja'
						AND p.switch = 'Y'
					WHERE
						o.`prefixid` = 'saja'
					AND o.`switch` = 'Y'
					AND o.type = 'saja'
					UNION ALL
						SELECT
							o.orderid,
							o.insertt,
							o.`status`,
							(
								CASE o.`status`
								WHEN '1' THEN
									'配送中'
								WHEN '2' THEN
									'退費中'
								WHEN '3' THEN
									'已發送'
								WHEN '4' THEN
									'已到貨'
								WHEN '5' THEN
									'已退費'
								ELSE
									'處理中'
								END
							) AS `statusname`,
							o.confirm,
							o.epid,
							o.esid,
							o.userid,
							o.type,
							(
								CASE o.`type`
								WHEN 'exchange' THEN
									'兌換'
								ELSE
									'生活繳費'
								END
							) AS `typename`,
							o.num,
							o.point_price,
							o.total_fee,
							o.discount_fee,
							o.process_fee,
							o.profit, 
							o.memo, 
							o.tx_data, 
							o.pgpid, 
							ep.NAME AS epname,
							ep.retail_price
						FROM
							saja_exchange.saja_order o
						LEFT JOIN saja_exchange.saja_exchange_product ep ON o.epid = ep.epid
						AND ep.prefixid = 'saja'
						AND ep.switch = 'Y'
						WHERE
							o.`prefixid` = 'saja'
						AND o.`switch` = 'Y'
						AND o.type <> 'saja'
				) t
			LEFT JOIN saja_exchange.saja_order_consignee oc ON t.orderid = oc.orderid
				AND oc.prefixid = 'saja'
				AND oc.switch = 'Y'
			LEFT JOIN saja_user.saja_user u ON t.userid = u.userid
				AND u.switch = 'Y'
				AND u.prefixid = 'saja'
			LEFT JOIN saja_user.saja_user_profile up ON t.userid = up.userid
				AND up.switch = 'Y'
				AND up.prefixid = 'saja'
			LEFT JOIN saja_exchange.saja_exchange_store es ON t.esid = es.esid
				AND es.switch = 'Y'
				AND es.prefixid = 'saja' 
			WHERE 1
" ;
$query .= $query_search ; 
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
error_log("[source/order] export : ".$query);
$table = $this->model->getQueryRecord($query);  

if($table["table"]['record']){
	foreach($table["table"]['record'] as $rk => $rv)
	{
		$table["table"]['record'][$rk] = $rv;
		
		$query ="SELECT name  
		FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_store` 
		WHERE 
			`prefixid` = '{$this->config->default_prefix_id}' 
			AND switch = 'Y' 
			AND `esid` = '{$rv['esid']}'
		";
		$productArr = $this->model->getQueryRecord($query); 
		$table["table"]['record'][$rk]['esname'] = $productArr['table']['record'][0]['name'];

		$query ="SELECT SUM(free_amount) as free_amount
		FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}spoint_free_history` 
		WHERE 
				 `switch` = 'Y'
			AND `orderid` = '{$rv['orderid']}'
		";
		$recArr = $this->model->getQueryRecord($query);
		$table["table"]['record'][$rk]["free_history"] = number_format(abs($recArr['table']['record'][0]['free_amount']));

		/*
		 orderid 遞增 訂單id	userid 使用者id	status 訂單狀態id	type 訂單類別	epid 兌換商品id	pgpid 得標記錄ID	num 訂單數量	esid 店家ID	point_price 兌換單價	process_fee 處理費	total_fee 總價	profit 分潤	memo 訂單備註	confirm 確認訂單	seq 	switch 	insertt 	modifyt 
		*/
		if($rv['type']=='lifepay')
		{
			$paydata = json_decode($rv['tx_data'], true);
			
			if (!empty($paydata['calltype']) && ($paydata['calltype'] == "addorder")){
				
				switch($paydata['type']) {
					case 1:		// 信用卡繳費
						$info = "信用卡繳費";
						break;
					case 2:		// 房地產貸款支付
						$info = "房地產貸款支付";
						break;
					case 3:		// 汽機車貸款支付
						$info = "汽機車貸款支付";
						break;
					case 4:		// 信用貸款支付
						$info = "信用貸款支付";
						break;
					case 5:		// 就學貸款支付
						$info = "就學貸款支付";
						break;
					case 6:		// 電信費支付
						$info = "電信費支付";
						if (!empty($paydata['toid'])) {
							$query ="SELECT tmo.name  
							FROM `{$this->config->db[2]['dbname']}`.`{$this->config->default_prefix}telecom_op` tmo 
							WHERE 
								tmo.switch = 'Y' 
								AND tmo.toid = '{$paydata['toid']}'
							";
							$getto = $this->model->getQueryRecord($query);
							$paydata['toname'] = $getto['table']['record'][0]['name'];						
						}
						if (!empty($paydata['tpid'])) {
							$query ="SELECT tp.name 
							FROM `{$this->config->db[2]['dbname']}`.`{$this->config->default_prefix}telpaytype` tp 
							WHERE 
								tp.switch = 'Y' 
								AND tp.toid = '{$paydata['toid']}' 
								AND tp.tpid = '{$paydata['tpid']}'
							";
							$gettp = $this->model->getQueryRecord($query);
							$paydata['tpname'] = $gettp['table']['record'][0]['name'];						
						}

						break;					
					case 7:		// 宅公益
						$info = "宅公益";
						$number = $paydata['number'];
						break;							
					case 8:		// 停車費支付
						$info = "停車費支付";
						$number = $paydata['number'];
						break;
					case 9:		// 水費支付
						$info = "水費支付";
						$number = $paydata['number'];
						break;
					case 10:	// 電費支付
						$info = "電費支付";
						$number = $paydata['number'];
						break;	
				}			
				
				if ($paydata['type'] < 6 && !empty($paydata['bankid'])){
					$query ="SELECT bk.bankname  
					FROM `{$this->config->db[2]['dbname']}`.`{$this->config->default_prefix}bank` bk 
					WHERE 
						bk.switch = 'Y' 
						AND bk.bankid = '{$paydata['bankid']}' 
					";		
					$getbk = $this->model->getQueryRecord($query);
					$paydata['bkname'] = $getbk['table']['record'][0]['bankname'];						
				}
				
				$table["table"]['record'][$rk]['payname'] = $info." - 繳費到期日 : ".$paydata['endtime'];
				$table["table"]['record'][$rk]['paydata'] = $paydata;

			} else {
				$table["table"]['record'][$rk]['payname'] = "";
				$table["table"]['record'][$rk]['paydata'] = "";
			}			
			
		}
		
	}
}

##############################################################################################################################################
// Log Start 
$order_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'order_export', 
	`active` = '訂單匯出', 
	`memo` = '{$order_data}', 
	`root` = 'order/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//會員資料表
$Report_Array[0]['title'] = array(
	"A1" => "訂單編號", 
	"B1" => "訂單日期", 
	"C1" => "訂單狀態",
    "D1" => "會員", 
	"E1" => "手機驗證", 
    "F1" => "類別", 
    "G1" => "商品名稱",
    "H1" => "兌換數量", 
    "I1" => "商品單價",
	"J1" => "商品巿價",
	"K1" => "手續費",
	"L1" => "折抵金額",
    "M1" => "包含免費贈幣", 
	"N1" => "訂單總價",
	"O1" => "銷帳資料",
    "P1" => "用戶證號", 
    "Q1" => "收件人名稱", 
	"R1" => "收件人電話",
	"S1" => "單位名稱",
    "T1" => "訂單備註", 
	"U1" => "收件人地址"
    // "C1" => "訂單狀態", 
    // "D1" => "訂單確認",
    // "K1" => "訂單分潤", 	
    // "P1" => "銀行編號", 
    // "Q1" => "銀行名稱", 
    // "R1" => "電信資料", 
	// "R1" => "區碼",

	);

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['orderid']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$row, (string)$value['insertt'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['statusname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row, ($value['username']."(".$value['userid'].")"),PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['smsphone'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$row, (string)$value['typename'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$row, (string)$value['epname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['num']);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['point_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $value['retail_price']);
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['process_fee']);
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $value['discount_fee']);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $value['free_history']);
        $objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $value['total_fee']);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('O'.$row, (string)$value['paydata']['number'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$row, (string)$value['paydata']['idnumber'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('Q'.$row, (string)$value['getname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$row, (string)$value['getphone'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$row, (string)$value['paydata']['bkname'].$value['paydata']['toname'].(string)$value['paydata']['tpname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('T'.$row, (string)$value['payname'],PHPExcel_Cell_DataType::TYPE_STRING);
		$objPHPExcel->getActiveSheet()->setCellValueExplicit('U'.$row, (string)$value['getaddress'],PHPExcel_Cell_DataType::TYPE_STRING);
        // $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$row, (string)$value['paydata']['pcode'],PHPExcel_Cell_DataType::TYPE_STRING);
        // $objPHPExcel->getActiveSheet()->setCellValueExplicit('S'.$row, (string)$value['payname'],PHPExcel_Cell_DataType::TYPE_STRING);
		// $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['status'],PHPExcel_Cell_DataType::TYPE_STRING);
        // $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['confirm']);
        // $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['username'],PHPExcel_Cell_DataType::TYPE_STRING);
        // $objPHPExcel->getActiveSheet()->setCellValueExplicit('P'.$row, $value['paydata']['bankid']);		
        // $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['profit']);
        // $objPHPExcel->getActiveSheet()->setCellValueExplicit('R'.$row, (string)$value['paydata']['toname'].(string)$value['paydata']['tpname'],PHPExcel_Cell_DataType::TYPE_STRING);
	}
}

$objPHPExcel->getActiveSheet()->setTitle('訂單管理');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="訂單管理'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>