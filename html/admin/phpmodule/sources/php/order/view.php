<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(orderid|insertt)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}


if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',"t.".$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}else{
	$sub_sort_query =  " ORDER BY t.orderid DESC ";
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_orderid"] != ''){
	$status["status"]["search"]["search_orderid"] = $this->io->input["get"]["search_orderid"] ;
	$status["status"]["search_path"] .= "&search_orderid=".$this->io->input["get"]["search_orderid"] ;
	$sub_search_query .=  "AND t.`orderid` in ('".$this->io->input["get"]["search_orderid"]."') ";
}
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "AND t.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
}
if($this->io->input["get"]["search_confirm"] != ''){
	$status["status"]["search"]["search_confirm"] = $this->io->input["get"]["search_confirm"] ;
	$status["status"]["search_path"] .= "&search_confirm=".$this->io->input["get"]["search_confirm"] ;
	IF($this->io->input["get"]["search_confirm"] != 'ALL'){
		$sub_search_query .=  "AND t.`confirm` ='".$this->io->input["get"]["search_confirm"]."' ";
	}
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	$sub_search_query .=  "AND t.`insertt` BETWEEN '".$this->io->input["get"]["search_btime"]."'
	AND '".$this->io->input["get"]["search_etime"]."' ";
}
if($this->io->input["get"]["search_status"] != ''){
	$status["status"]["search"]["search_status"] = $this->io->input["get"]["search_status"] ;
	$status["status"]["search_path"] .= "&search_status=".$this->io->input["get"]["search_status"] ;
	IF($this->io->input["get"]["search_status"] != 'ALL'){
		$sub_search_query .=  "AND t.`status` ='".$this->io->input["get"]["search_status"]."' ";
	}
}
if($this->io->input["get"]["search_type"] != ''){
	$status["status"]["search"]["search_type"] = $this->io->input["get"]["search_type"] ;
	$status["status"]["search_path"] .= "&search_type=".$this->io->input["get"]["search_type"] ;
	IF($this->io->input["get"]["search_type"] != 'ALL'){
		$sub_search_query .=  "AND t.`type` ='".$this->io->input["get"]["search_type"]."' ";
	}
}
if($this->io->input["get"]["search_epname"] != ''){
	$status["status"]["search"]["search_epname"] = $this->io->input["get"]["search_epname"] ;
	$status["status"]["search_path"] .= "&search_epname=".$this->io->input["get"]["search_epname"] ;
	// $sub_search_query .=  "AND `epanme` ='".$this->io->input["get"]["search_epname"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT
				count(*) num
			FROM
				(
					SELECT
						o.orderid,
						o.insertt,
						o.`status`,
						(
							CASE o.`status`
							WHEN '1' THEN
								'配送中'
							WHEN '2' THEN
								'退費中'
							WHEN '3' THEN
								'已發送'
							WHEN '4' THEN
								'已到貨'
							WHEN '5' THEN
								'已退費'
							WHEN '6' THEN
								'不出貨'
							ELSE
								'處理中'
							END
						) AS `statusname`,
						o.confirm,
						o.esid,
						o.userid,
						o.type,
						'得標' typename,
						o.num,
						o.point_price,
						o.total_fee,
						o.discount_fee,
						o.profit,
						p. NAME AS epname
					FROM
						saja_exchange.saja_order o
					LEFT JOIN saja_shop.saja_pay_get_product pgp ON o.pgpid = pgp.pgpid
					AND pgp.prefixid = 'saja'
					AND pgp.switch = 'Y'
					LEFT JOIN saja_shop.saja_product p ON p.productid = pgp.productid
					AND p.prefixid = 'saja'
					AND p.switch = 'Y'
					WHERE
						o.`prefixid` = 'saja'
					AND o.`switch` = 'Y'
					AND o.type = 'saja'
					UNION ALL
						SELECT
							o.orderid,
							o.insertt,
							o.`status`,
							(
								CASE o.`status`
								WHEN '1' THEN
									'配送中'
								WHEN '2' THEN
									'退費中'
								WHEN '3' THEN
									'已發送'
								WHEN '4' THEN
									'已到貨'
								WHEN '5' THEN
									'已退費'
								WHEN '6' THEN
									'不出貨'
								ELSE
									'處理中'
								END
							) AS `statusname`,
							o.confirm,
							o.esid,
							o.userid,
							o.type,
							(
								CASE o.`type` 
								WHEN 'exchange' THEN
									'兌換'
								WHEN 'user_qrcode_tx' THEN
									'鯊魚點支付'
								ELSE
									'生活繳費'
								END
							) AS `typename`,
							o.num,
							o.point_price,
							o.total_fee,
							o.discount_fee,
							o.profit,
							ep. NAME AS epname
						FROM
							saja_exchange.saja_order o
						LEFT JOIN saja_exchange.saja_exchange_product ep ON o.epid = ep.epid
						AND ep.prefixid = 'saja'
						AND ep.switch = 'Y'
						WHERE
							o.`prefixid` = 'saja'
						AND o.`switch` = 'Y'
						AND o.type <> 'saja'
				) t
			LEFT JOIN saja_user.saja_user u ON t.userid = u.userid
			AND u.switch = 'Y'
			AND u.prefixid = 'saja'
			LEFT JOIN saja_exchange.saja_exchange_store es ON t.esid = es.esid
			AND es.switch = 'Y'
			AND es.prefixid = 'saja'
			WHERE 1 
			";
if (!empty($this->io->input["get"]["search_epname"])){
	$query .= " AND t.`epname` like '%".$this->io->input["get"]["search_epname"]."%' ";
}
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;

// Table Count end

// Table Record Start
$query = "  SELECT
				t.*, u.`name` username,
				es.`name` esname, oc.name as oname, oc.phone as ophone, oc.address as oaddress 
			FROM
				(
					SELECT
						o.orderid,
						o.insertt,
						o.`status`,
						(
							CASE o.`status`
							WHEN '1' THEN
								'配送中'
							WHEN '2' THEN
								'退費中'
							WHEN '3' THEN
								'已發送'
							WHEN '4' THEN
								'已到貨'
							WHEN '5' THEN
								'已退費'
							WHEN '6' THEN
								'不出貨'
							ELSE
								'處理中'
							END
						) AS `statusname`,
						o.confirm,
						o.esid,
						o.userid,
						o.type,
						'得標' typename,
						o.num,
						o.point_price,
						o.total_fee,
						o.discount_fee,
						o.cash_pay,
						o.profit,
						p.NAME AS epname,
						p.productid AS epid 
					FROM
						saja_exchange.saja_order o
					LEFT JOIN saja_shop.saja_pay_get_product pgp ON o.pgpid = pgp.pgpid
						AND pgp.prefixid = 'saja'
						AND pgp.switch = 'Y'
					LEFT JOIN saja_shop.saja_product p ON p.productid = pgp.productid
						AND p.prefixid = 'saja'
						AND p.switch = 'Y'
					WHERE
						o.`prefixid` = 'saja'
					AND o.`switch` = 'Y'
					AND o.type = 'saja'
					UNION ALL
						SELECT
							o.orderid,
							o.insertt,
							o.`status`,
							(
								CASE o.`status`
								WHEN '1' THEN
									'配送中'
								WHEN '2' THEN
									'退費中'
								WHEN '3' THEN
									'已發送'
								WHEN '4' THEN
									'已到貨'
								WHEN '5' THEN
									'已退費'
								WHEN '6' THEN
									'不出貨'
								ELSE
									'處理中'
								END
							) AS `statusname`,
							o.confirm,
							o.esid,
							o.userid,
							o.type,
							(
								CASE o.`type`
								WHEN 'exchange' THEN
									'兌換'
								WHEN 'user_qrcode_tx' THEN
									'鯊魚點支付'
								ELSE
									'生活繳費'
								END
							) AS `typename`,
							o.num,
							o.point_price,
							o.total_fee,
							o.discount_fee,
							o.cash_pay,
							o.profit,
							ep.NAME AS epname,
							o.epid 
						FROM
							saja_exchange.saja_order o
						LEFT JOIN saja_exchange.saja_exchange_product ep ON o.epid = ep.epid
						AND ep.prefixid = 'saja'
						AND ep.switch = 'Y'
						WHERE
							o.`prefixid` = 'saja'

						AND o.type <> 'saja'
				) t
			LEFT JOIN saja_exchange.saja_order_consignee oc ON t.orderid = oc.orderid
				AND oc.prefixid = 'saja'
				AND oc.switch = 'Y'
			LEFT JOIN saja_user.saja_user u ON t.userid = u.userid
				AND u.switch = 'Y'
				AND u.prefixid = 'saja'
			LEFT JOIN saja_exchange.saja_exchange_store es ON t.esid = es.esid
				AND es.switch = 'Y'
				AND es.prefixid = 'saja'
			WHERE 1
			";
if (!empty($this->io->input["get"]["search_epname"])){
	$query .= " AND t.`epname` like '%".$this->io->input["get"]["search_epname"]."%' ";
}
$query .= $sub_search_query ;
$query .= $sub_sort_query ;
$query .= $query_limit ;
$table = $this->model->getQueryRecord($query);

$table["table"]["rt"] = array();
if(!empty($table['table']['record']) ){
	$data_arr = $table['table']['record'];
	foreach($data_arr as $rk => $rv){
		$query ="SELECT stat_memo, mall_status 
		FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_log` 
		WHERE `switch` = 'Y'
			AND `orderid` = '{$rv['orderid']}'
		";
		$recArr = $this->model->getQueryRecord($query);
		$stat_memo = $recArr['table']['record'][0]['stat_memo'];
		
		$mall_status = $recArr['table']['record'][0]['mall_status'];
		switch ($mall_status) {
			case 'order':
				$table["table"]["rt"][$rv['orderid']]["mall_status"] = '支付中';
				break;
			case 'cancel':
				$table["table"]["rt"][$rv['orderid']]["mall_status"] = '取消支付';
				break;
			case 'error':
				$table["table"]["rt"][$rv['orderid']]["mall_status"] = '支付錯誤';
				break;
			case 'sajabonus':
				$table["table"]["rt"][$rv['orderid']]["mall_status"] = '鯊魚點支付';
				break;
			case 'gift':
				$table["table"]["rt"][$rv['orderid']]["mall_status"] = '贈送';
				break;
			case 'writeoff':
				$table["table"]["rt"][$rv['orderid']]["mall_status"] = '已銷帳';
				break;
			case 'payment':
				$table["table"]["rt"][$rv['orderid']]["mall_status"] = '已付款';
				break;
			default:
				if(!empty($stat_memo) && $rv['cash_pay'] != 0){
					$table["table"]["rt"][$rv['orderid']]["mall_status"] = '待支付';
				}else{
					$table["table"]["rt"][$rv['orderid']]["mall_status"] = '無需支付';
				}
				break; 
		}
		
		if(!empty($stat_memo)){
			$table["table"]["rt"][$rv['orderid']]["stat_memo"] = $stat_memo;
		} else {
			$table["table"]["rt"][$rv['orderid']]["stat_memo"] = '';
		}
	}
} 
// Table Record End

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
/*
$query ="
SELECT *
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."product_category`
WHERE
prefixid = '".$this->config->default_prefix_id."'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["user"] = $recArr['table']['record'];
*/

// Relation End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$order_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'order',
	`active` = '訂單清單查詢',
	`memo` = '{$order_data}',
	`root` = 'order/view',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$this->tplVar('table' , $table['table']) ;


$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();
