<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "/var/www/html/admin/libs/helpers.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

$ret['msg'] = '';
$array = explode(',', $this->io->input["post"]["orderid"]);
##############################################################################################################################################
// Update Start
if ($this->io->input["post"]["type"] == 'close') {
	$otype = 1;
} else {
	$otype = 0;
}

$errormsg = '';
$cardmsg = '';
if(!empty($this->io->input["post"]["orderid"])){
	$count = 0;
	$i=0;
	$errorid = array();
	$orderstatus = '';
	foreach ($array as $key => $value) {
		$query ="SELECT o.*
		FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o
		WHERE 
			o.`prefixid` = '{$this->config->default_prefix_id}' 
			AND o.`switch` = 'Y'
			AND o.`status` = '{$otype}'
			AND o.`orderid` = '{$value}'
		LIMIT 1	
		";
		$orderstatus = $this->model->getQueryRecord($query);
	
		if(!empty($orderstatus['table']['record'])) {
			//檢查訂單
			$query ="SELECT o.*
			FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o
			LEFT OUTER JOIN `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` os ON 
				os.prefixid = o.prefixid
				AND os.orderid = o.orderid
				AND os.switch = 'Y'
			WHERE 
				o.`prefixid` = '{$this->config->default_prefix_id}' 
				AND o.`switch` = 'Y'
				AND o.`orderid` = '{$value}'
				AND os.orderid IS NOT NULL
			LIMIT 1	
			";
			$recArr = $this->model->getQueryRecord($query);

			if(empty($recArr['table']['record'])) {

				//新增訂單出貨
				$query ="INSERT INTO `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` SET 
					`prefixid` = '{$this->config->default_prefix_id}', 
					`orderid`='{$value}',
					`insertt`=now()
				";
				$this->model->query($query);
				$osid = $this->model->_con->insert_id;
			}
			
			if ($this->io->input["post"]["type"] == 'close') {
				
				if ($recArr['table']['record'][0]['epid'] != 0){
					
					$query ="SELECT p.*
					FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp
					LEFT OUTER JOIN `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product` p ON
						pgp.prefixid = p.prefixid
						AND pgp.productid = p.productid
						AND p.switch = 'Y'
					WHERE
						pgp.`prefixid` = '{$this->config->default_prefix_id}'
						AND pgp.pgpid  = '{$recArr['table']['record'][0]['pgpid']}'
						AND pgp.switch = 'Y'
					" ;
					$recArrpd = $this->model->getQueryRecord($query);
					
					
					if($recArrpd['table']['record'][0]['is_exchange'] == 1){
						
						//取兌換商品資訊
						$query ="SELECT *
						FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_product`
						WHERE
							epid = '{$recArrpd['table']['record'][0]['epid']}'
							AND switch = 'Y'
						";
						$ep_info = $this->model->getQueryRecord($query);
						
						//序號商品卡
						if($ep_info['table']['record'][0]['eptype'] != 4){
							//取商品卡資訊
							$query ="SELECT cid
							FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_card`
							WHERE
								epid = '{$recArrpd['table']['record'][0]['epid']}'
								AND userid = '0'
								AND orderid = '0'
								AND used = 'N'
								AND switch = 'Y'
								ORDER BY cid ASC,insertt ASC
								LIMIT {$ep_info['table']['record'][0]['set_qty']}
							";
							$ec_info = $this->model->getQueryRecord($query);
							
							//商品卡庫存
							$card_num = count($ec_info['table']['record']);
							
							if($card_num >= $ep_info['table']['record'][0]['set_qty']){		//庫存足夠
								$cid_value = '';
								foreach($ec_info['table']['record'] as $row){
									$cid_value .= $row['cid'].',';
								}
								//將cid組合成字串
								$cid_value = rtrim($cid_value,','); 

								$this->model->query('start transaction');
								$affected_num = 0;
								$query = "UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_card`
								set
									`userid` = '{$recArr['table']['record'][0]['userid']}',
									`orderid` = '{$value}',
									`used` = 'Y'
								WHERE `cid` IN ({$cid_value}) AND `userid` = 0 AND `orderid` = 0 AND `used` = 'N'";

								$res = $this->model->query($query);
								//update的資料筆數
								$affected_num = $this->model->_con->affected_rows;

								if($affected_num == $ep_info['table']['record'][0]['set_qty']){		//更新比數與兌換數量符合
									$this->model->query('COMMIT');
									//調整預留庫存
									if($ep_info['table']['record'][0]['reserved_stock'] > 0){
										$reserved_stock = $ep_info['table']['record'][0]['reserved_stock'] - $ep_info['table']['record'][0]['set_qty'];
										if($reserved_stock <= 0){
											$reserved_stock = 0;
										}
										$query = "UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}exchange_product` set
										`reserved_stock` = '{$reserved_stock}'
										WHERE `epid` = '{$product['epid']}'";
										$res = $this->model->query($query);
									}
									
									//訂單狀態: 1 已到貨(已扣除 紅利點數)
									set_status($this->config, $this->model, '4', $value);
									$sql = "`closetime`='{$this->io->input["post"]["closetime"]}', 
									`closememo`='{$this->io->input["post"]["closememo"]}', ";
									
								}else{											//更新比數與兌換數量不符合
									$this->model->query('ROLLBACK');
									$errorid[$i] = $value;
								}
							}else{     //庫存不足
								$errorid[$i] = $value;
								$cardmsg = '庫存不足';
							}
						}else{
							$cardmsg = '不是有序號商品';
						}
					} else {
						//訂單狀態: 1 已到貨(已扣除 紅利點數)
						set_status($this->config, $this->model, '4', $value);
						$sql = "`closetime`='{$this->io->input["post"]["closetime"]}', 
						`closememo`='{$this->io->input["post"]["closememo"]}', ";
					}
				}else{
					//訂單狀態: 1 已到貨(已扣除 紅利點數)
					set_status($this->config, $this->model, '4', $value);
					$sql = "`closetime`='{$this->io->input["post"]["closetime"]}', 
					`closememo`='{$this->io->input["post"]["closememo"]}', ";
				}
			} else {
				if ($this->io->input["post"]["type"] == 'shipping') {
					//訂單狀態: 1 已出貨(已扣除 紅利點數)
					set_status($this->config, $this->model, '1', $value);
				}
				
				$sql = "`outtime`='{$this->io->input["post"]["outtime"]}', 
				`outtype`='{$this->io->input["post"]["outtype"]}',
				`outcode`='{$this->io->input["post"]["outcode"]}',
				`outmemo`='{$this->io->input["post"]["outmemo"]}', 
				";
			}

			$query ="UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` SET 
				{$sql} 
				`modifyt`=now() 
			WHERE
				`prefixid` = '{$this->config->default_prefix_id}'
				AND `orderid` = {$value}
			" ;
			$this->model->query($query);
			$count ++;
			
		} else {
			$errorid[$i] = $value;
			$i ++;		
		}
	}
	$errorid = implode(",",$errorid);
	
}


if (!empty($errorid)){
	$errormsg = '，訂單 '.$errorid.' 狀態不符'.$cardmsg ;
}

function set_status($config, $model, $status, $orderid)
{
	$query ="UPDATE `{$config->db[3]['dbname']}`.`{$config->default_prefix}order` SET 
		`status` ='{$status}',
		`modifyt`=now() 
	WHERE
		`prefixid` = '{$config->default_prefix_id}'
		AND `orderid` = '{$orderid}'
	" ;
	$model->query($query); 
}
// functions for allowance --end
// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$order_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'order_arrayupdate', 
	`active` = '訂單批次修改寫入', 
	`memo` = '{$order_data}', 
	`root` = 'order/arrayupdate', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
if ($otype == 1) {
	$ret['msg'] = '訂單批次到貨已執行 '.$count.' 筆'.$errormsg;
} else {
	$ret['msg'] = '訂單批次出貨已執行 '.$count.' 筆'.$errormsg;
}

echo json_encode($ret);