<?php
// Check Variable Start
if (empty($this->io->input["get"]["orderid"])) {
	$this->jsAlertMsg('訂單編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

##############################################################################################################################################
// Table  Start 

// Table Content Start
/*
$query ="SELECT o.*, oc.name consignee, oc.phone, oc.zip, oc.address, oc.gender ,u.name uname
FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o 
LEFT OUTER JOIN `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_consignee` oc ON  
	o.prefixid = oc.prefixid
	AND o.orderid = oc.orderid
LEFT OUTER JOIN `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}user` u ON  
	o.prefixid = u.prefixid
	AND o.userid = u.userid
WHERE 
	o.`prefixid` = '{$this->config->default_prefix_id}' 
	AND o.`switch`='Y' 
	AND o.`orderid` = '{$this->io->input["get"]["orderid"]}'
" ;
*/
// Add By Thomas 20141113
// Update 20191015
// 收件人資訊撈取順序 :
// 收件人,收件地址 : 該訂單收件資訊>profile收件人資訊
// 電話 : 該訂單收件人電話-> user profile的手機號->驗證的手機號
$query ="SELECT o.*, IFNULL(oc.name, up.addressee) consignee, 
                     IFNULL(IFNULL(IFNULL(oc.phone, up.rphone), up.phone), usa.phone) as phone, 
                     IFNULL(oc.address, up.address) as address, 
					 IFNULL(oc.zip, up.area) as zip, 
					 oc.gender ,u.name uname ,up.qqno ,usa.phone as smsphone, up.nickname 
FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o 
LEFT OUTER JOIN `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_consignee` oc ON  
	o.prefixid = oc.prefixid
	AND o.orderid = oc.orderid
LEFT OUTER JOIN `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}user` u ON  
	o.prefixid = u.prefixid
	AND o.userid = u.userid
LEFT OUTER JOIN `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}user_profile` up ON  
	o.prefixid = up.prefixid
	AND o.userid = up.userid
LEFT OUTER JOIN `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}user_sms_auth` usa ON  
	o.prefixid = usa.prefixid
	AND o.userid = usa.userid	
WHERE 
	o.`prefixid` = '{$this->config->default_prefix_id}' 
	AND o.`switch`='Y' 
	AND o.`orderid` = '{$this->io->input["get"]["orderid"]}'
" ;

$table = $this->model->getQueryRecord($query);

if($table["table"]['record']){
	foreach($table["table"]['record'] as $rk => $rv)
	{
		if($rv['type']=='saja' && $rv['epid'] == 0)
		{
			//????
			$query ="SELECT p.name, p.ptype, p.productid, pgp.price
			FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp 
			LEFT OUTER JOIN `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product` p ON 
				pgp.prefixid = p.prefixid
				AND pgp.productid = p.productid
				AND p.switch = 'Y'
			WHERE 
				pgp.prefixid = '{$this->config->default_prefix_id}' 
				AND pgp.switch = 'Y' 
				AND pgp.pgpid = '{$rv['pgpid']}'
			";
			$productArr = $this->model->getQueryRecord($query); 
			$table["table"]['record'][$rk]['epname'] = $productArr['table']['record'][0]['name'];
			$table["table"]['record'][$rk]['ptype'] = $productArr['table']['record'][0]['ptype'];
			$table["table"]['record'][$rk]['productid'] = $productArr['table']['record'][0]['productid'];
			$table["table"]['record'][$rk]['price'] = $productArr['table']['record'][0]['price'];

			// $table["table"]['record'][$rk]['type'] = $rv['type'];
		} else	{
			//????
			$query ="
			SELECT name, eptype  
			FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` 
			WHERE 
				`prefixid` = '{$this->config->default_prefix_id}' 
				AND switch = 'Y' 
				AND `epid` = '{$rv['epid']}'
			";
			$productArr = $this->model->getQueryRecord($query); 
			$table["table"]['record'][$rk]['epname'] = $productArr['table']['record'][0]['name'];
			$table["table"]['record'][$rk]['eptype'] = $productArr['table']['record'][0]['eptype'];
			// $table["table"]['record'][$rk]['type'] = 'exchange';
			// Add by Thomas 2020/02/03 如果是中油捷利卡(Jeili) -> 撈卡號
			if($rv['epid'] == '689') {
			    $query ="SELECT *  
						   FROM `{$this->config->db[0]['dbname']}`.`{$this->config->default_prefix}user_extrainfo` 
						  WHERE `prefixid` = '{$this->config->default_prefix_id}' 
							AND `switch` = 'Y' 
							AND `uecid` = '1'
							AND `userid`='{$rv['userid']}' ";
                $arrUserExtra = $this->model->getQueryRecord($query);
				$table["table"]['record'][$rk]['jieli_card_owner']=	$arrUserExtra['table']['record'][0]['field1name'];
                $table["table"]['record'][$rk]['jieli_card_no']=	$arrUserExtra['table']['record'][0]['field2name'];		
			}
		}
		
		$paydata = json_decode($rv['tx_data'], true);
		
		if (!empty($paydata['calltype']) && ($paydata['calltype'] == "addorder")){
			
			switch($paydata['type']) {
				case 1:		// 信用卡繳費
					$info = "信用卡繳費";
					break;
				case 2:		// 房地產貸款支付
					$info = "房地產貸款支付";
					break;
				case 3:		// 汽機車貸款支付
					$info = "汽機車貸款支付";
					break;
				case 4:		// 信用貸款支付
					$info = "信用貸款支付";
					break;
				case 5:		// 就學貸款支付
					$info = "就學貸款支付";
					break;
				case 6:		// 電信費支付
					$info = "電信費支付";
					if (!empty($paydata['toid'])) {
						$query ="SELECT tmo.name  
						FROM `{$this->config->db[2]['dbname']}`.`{$this->config->default_prefix}telecom_op` tmo 
						WHERE tmo.toid = '{$paydata['toid']}'
						";
						$getto = $this->model->getQueryRecord($query);
						$paydata['toname'] = $getto['table']['record'][0]['name'];						
					}
					if (!empty($paydata['tpid'])) {
						$query ="SELECT tp.name 
						FROM `{$this->config->db[2]['dbname']}`.`{$this->config->default_prefix}telpaytype` tp 
						WHERE 
							tp.toid = '{$paydata['toid']}' 
							AND tp.tpid = '{$paydata['tpid']}'
						";
						$gettp = $this->model->getQueryRecord($query);
						$paydata['tpname'] = $gettp['table']['record'][0]['name'];						
					}

					break;					
				case 7:		// 宅公益
					$info = "宅公益";
					$number = $paydata['number'];
					break;							
				case 8:		// 停車費支付
					$info = "停車費支付";
					$number = $paydata['number'];
					break;
				case 9:		// 水費支付
					$info = "水費支付";
					$number = $paydata['number'];
					break;
				case 10:	// 電費支付
					$info = "電費支付";
					$number = $paydata['number'];
					break;	
			}			
			
			if ($paydata['type'] < 6 && !empty($paydata['bankid'])){
				$query ="SELECT bk.bankname  
				FROM `{$this->config->db[2]['dbname']}`.`{$this->config->default_prefix}bank` bk 
				WHERE 
					bk.switch = 'Y' 
					AND bk.bankid = '{$paydata['bankid']}' 
				";
				$getbk = $this->model->getQueryRecord($query);
				$paydata['bkname'] = $getbk['table']['record'][0]['bankname'];
			}
			$table["table"]['record'][$rk]['payname'] = $info." - 繳費到期日 : ".$paydata['endtime'];
			$table["table"]['record'][$rk]['paydata'] = $paydata;
		} else {
			$table["table"]['record'][$rk]['payname'] = $table["table"]['record'][$rk]['memo'];
			$table["table"]['record'][$rk]['paydata'] = "";
		}
		if($table["table"]['record'][$rk]['ptype'] == 1){
			$query ="SELECT h.shid,shd.*
			FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}history` h 
			LEFT OUTER JOIN `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}history_detail` shd ON 
				h.shdid = shd.shdid
				AND h.productid = shd.productid
				AND shd.switch = 'Y'
			WHERE 
				h.productid = '{$table["table"]['record'][$rk]['productid']}'
				AND h.userid = '{$table["table"]['record'][$rk]['userid']}' 
				AND h.switch = 'Y' 
				AND h.price = '{$table["table"]['record'][$rk]['price']}'
			";
			$history_detail = $this->model->getQueryRecord($query); 
			$table["table"]['record'][$rk]['title'] = $history_detail['table']['record'][0]['title'];
			$table["table"]['record'][$rk]['content'] = $history_detail['table']['record'][0]['content'];		
			$table["table"]['record'][$rk]['purchase_url'] = $history_detail['table']['record'][0]['purchase_url'];
			$table["table"]['record'][$rk]['thumbnail_filename'] = '/site/images/dream/products/'.$history_detail['table']['record'][0]['thumbnail_filename'];
		}
	}
}	
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="SELECT *
FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_shipping` 
WHERE 
	`prefixid` = '".$this->config->default_prefix_id."'
	AND `switch` = 'Y'
	AND `orderid` = '{$this->io->input["get"]["orderid"]}'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["shipping"] = $recArr['table']['record'][0];
// Relation End 
##############################################################################################################################################

##############################################################################################################################################
// Relation Start
$query ="SELECT SUM(free_amount) as free_amount
FROM `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}spoint_free_history` 
WHERE 
		 `switch` = 'Y'
	AND `orderid` = '{$this->io->input["get"]["orderid"]}'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["free_history"] = $recArr['table']['record'][0];

// Relation End 
##############################################################################################################################################

##############################################################################################################################################
// Relation Start
$query ="SELECT stat_memo, mall_status 
FROM `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order_log` 
WHERE `switch` = 'Y'
	AND `orderid` = '{$this->io->input["get"]["orderid"]}'
";
$recArr = $this->model->getQueryRecord($query);
$stat_memo = $recArr['table']['record'][0]['stat_memo'];
if(!empty($stat_memo)){
	$table["table"]["rt"]["stat_memo"] = $stat_memo;
} else {
	$table["table"]["rt"]["stat_memo"] = '';
}

$mall_status = $recArr['table']['record'][0]['mall_status'];
switch ($mall_status) {
	case 'order':
		$table["table"]["rt"]["mall_status"] = '待支付';
		break;
	case 'cancel':
		$table["table"]["rt"]["mall_status"] = '取消支付';
		break;
	case 'error':
		$table["table"]["rt"]["mall_status"] = '支付錯誤';
		break;
	case 'sajabonus':
		$table["table"]["rt"]["mall_status"] = '鯊魚點支付';
		break;
	case 'gift':
		$table["table"]["rt"]["mall_status"] = '贈送';
		break;
	case 'writeoff':
		$table["table"]["rt"]["mall_status"] = '已銷帳';
		break;
	case 'payment':
		$table["table"]["rt"]["mall_status"] = '已付款';
		break;
	default:
		if(!empty($stat_memo) && $table["table"]['record'][0]['cash_pay'] != 0){
		 	$table["table"]["rt"]["mall_status"] = '待支付';
		}else{
			$table["table"]["rt"]["mall_status"] = '無需支付';
		}
		break; 
}

// Relation End 
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$order_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'order_edit', 
	`active` = '訂單修改', 
	`memo` = '{$order_data}', 
	`root` = 'order/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"] ."orderid={$this->io->input["get"]["orderid"]}";

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();