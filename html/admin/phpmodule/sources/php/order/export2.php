<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];


/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

$sub_sort_query =  " ORDER BY orderid DESC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_orderid"] != ''){
	$sub_search_query .=  "AND o.`orderid` ='".$this->io->input["get"]["search_orderid"]."' ";
}
if($this->io->input["get"]["search_userid"] != ''){
	$sub_search_query .=  "AND o.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
}
if($this->io->input["get"]["search_confirm"] != ''){
	$sub_search_query .=  "AND o.`confirm` ='".$this->io->input["get"]["search_confirm"]."' ";
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$sub_search_query .=  "AND o.`insertt` BETWEEN '".$this->io->input["get"]["search_btime"]."' 
	AND '".$this->io->input["get"]["search_etime"]."' ";
}
if($this->io->input["get"]["search_status"] != ''){
	$sub_search_query .=  "AND o.`status` ='".$this->io->input["get"]["search_status"]."' ";
}
if($this->io->input["get"]["search_type"] != ''){
	$sub_search_query .=  "AND o.`type` ='".$this->io->input["get"]["search_type"]."' ";
}
// Search End

$query_search = '';
if ($data['tester'] == 1) {
	$query_search .= ' AND u.`userid` not in (1, 3, 9, 28, 116, 507, 585, 586, 588) ';
}

$query ="SELECT o.*, oc.name as getname, oc.address as getaddress, oc.phone as getphone 
FROM `{$db_exchange}`.`{$this->config->default_prefix}order` o
LEFT OUTER JOIN `{$db_exchange}`.`{$this->config->default_prefix}order_consignee` oc ON 
	o.prefixid = oc.prefixid
	AND o.orderid = oc.orderid
	AND o.switch = 'Y'
WHERE 
	o.`prefixid` = '{$this->config->default_prefix_id}' 
	AND o.`switch`='Y'
" ;
$query .= $query_search ; 
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query);  

if($table["table"]['record']){
	foreach($table["table"]['record'] as $rk => $rv)
	{
		$table["table"]['record'][$rk] = $rv;
		
		if($rv['status']=='1'){ $order_status = '配送中'; }
		elseif($rv['status']=='2'){ $order_status = '缺貨退費'; }
		elseif($rv['status']=='3'){ $order_status = '已發送'; }
		elseif($rv['status']=='4'){ $order_status = '已到貨'; }
		else{ $order_status = '處理中'; } 
		$table["table"]['record'][$rk]['status'] = $order_status;
		
		$query ="SELECT name 
		FROM `{$db_user}`.`{$this->config->default_prefix}user` 
		WHERE 
			`prefixid` = '{$this->config->default_prefix_id}' 
			AND switch = 'Y' 
			AND `userid` = '{$rv['userid']}'
		";
		$userArr = $this->model->getQueryRecord($query); 
		$table["table"]['record'][$rk]['username'] = $userArr['table']['record'][0]['name'];
		
		$query ="SELECT name  
		FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_store` 
		WHERE 
			`prefixid` = '{$this->config->default_prefix_id}' 
			AND switch = 'Y' 
			AND `esid` = '{$rv['esid']}'
		";
		$productArr = $this->model->getQueryRecord($query); 
		$table["table"]['record'][$rk]['esname'] = $productArr['table']['record'][0]['name'];
		
		/*
		 orderid 遞增 訂單id	userid 使用者id	status 訂單狀態id	type 訂單類別	epid 兌換商品id	pgpid 得標記錄ID	num 訂單數量	esid 店家ID	point_price 兌換單價	process_fee 處理費	total_fee 總價	profit 分潤	memo 訂單備註	confirm 確認訂單	seq 	switch 	insertt 	modifyt 
		*/
		if($rv['type']=='saja')
		{
			//得標商品
			$query ="SELECT p.name  
			FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp 
			LEFT OUTER JOIN `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product` p ON 
				pgp.prefixid = p.prefixid
				AND pgp.productid = p.productid
				AND p.switch = 'Y'
			WHERE 
				pgp.prefixid = '{$this->config->default_prefix_id}' 
				AND pgp.switch = 'Y' 
				AND pgp.pgpid = '{$rv['pgpid']}'
			";
			$productArr = $this->model->getQueryRecord($query); 
			$table["table"]['record'][$rk]['epname'] = $productArr['table']['record'][0]['name'];
			
			$table["table"]['record'][$rk]['type'] = '得標';
		}
		else
		{
			//兌換商品
			$query ="SELECT name  
			FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_product` 
			WHERE 
				`prefixid` = '{$this->config->default_prefix_id}' 
				AND switch = 'Y' 
				AND `epid` = '{$rv['epid']}'
			";
			$productArr = $this->model->getQueryRecord($query); 
			$table["table"]['record'][$rk]['epname'] = $productArr['table']['record'][0]['name'];
			
			$table["table"]['record'][$rk]['type'] = '兌換';
		}
		
	}
}

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//會員資料表
$Report_Array[0]['title'] = array("A1" => "訂單編號", "B1" => "訂單日期", "C1" => "訂單狀態", "D1" => "訂單確認", "E1" => "會員", "F1" => "類別", "G1" => "商品名稱", "H1" => "兌換數量", "I1" => "商品單價", "J1" => "訂單總價", "K1" => "訂單分潤", "L1" => "收件人名稱", "M1" => "收件人電話", "N1" => "收件人地址");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['userid']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$row, (string)$value['insertt'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['status'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['confirm']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['username'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$row, (string)$value['type'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$row, (string)$value['epname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['num']);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['point_price']);
        $objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $value['total_fee']);
        $objPHPExcel->getActiveSheet()->setCellValue('K'.$row, $value['profit']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('L'.$row, (string)$value['getname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('M'.$row, (string)$value['getphone'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('N'.$row, (string)$value['getaddress'],PHPExcel_Cell_DataType::TYPE_STRING);
		
	}
}

$objPHPExcel->getActiveSheet()->setTitle('訂單管理');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="訂單管理'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
	

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>