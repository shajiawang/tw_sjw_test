<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
	$files = (!empty($this->io->input["files"]) ) ? $this->io->input["files"] : '';
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];

$osn = (empty($this->io->input["post"]["osn"]))? "":$this->io->input["post"]["osn"];
$productid = (empty($this->io->input["post"]["productid"]))? "":$this->io->input["post"]["productid"];
$sendto = (empty($this->io->input["post"]["sendto"]))? "":$this->io->input["post"]["sendto"];
$body=(empty($this->io->input["post"]["body"]))? "":$this->io->input["post"]["body"];

$r=array();

if ($osn==''){
	$r['retCode']=-1;
	$r['retOsn']=$osn;
	$r['retMsg']='訂單編號錯誤';
}
if ($productid==''){
	$r['retCode']=-2;
	$r['retOsn']=$osn;
	$r['retMsg']='產品編號錯誤';
}
if ($sendto==''){
	$r['retCode']=-3;
	$r['retOsn']=$osn;
	$r['retMsg']='會員編號錯誤';
}
if ($body==''){
	$r['retCode']=-4;
	$r['retOsn']=$osn;
	$r['retMsg']='發送訊息內容錯誤';
}
$query="SELECT o.orderid,o.userid,p.productid,p.name,p.retail_price,o.status FROM `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}product` p ,`{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` o, `{$this->config->db[4]['dbname']}`.`{$this->config->default_prefix}pay_get_product` pgp WHERE o.orderid in ({$osn}) and o.status<2 and o.pgpid=pgp.pgpid and pgp.productid=p.productid";

$table = $this->model->getQueryRecord($query);
if (empty($table['table']['record'][0])){
	$r['retCode']=-5;
	$r['retOsn']=$osn;
	$r['retMsg']='查無此單';
}else{
	if (($table['table']['record'][0]['userid']!=$sendto)||($table['table']['record'][0]['productid']!=$productid)){
		$r['retCode']=-6;
		$r['retOsn']=$osn;
		$r['retMsg']='查詢結果與參數不合';
	}else{
		$insQ="INSERT `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}bonus`
		       SET
			       `userid`='{$sendto}',
			       `behav`='order_close',
			       `amount`='{$table["table"]["record"][0]["retail_price"]}',
				   `insertt`=now()
		       ";
		$inschk=$this->model->query($insQ);
		if ($inschk){
			$query ="UPDATE `{$this->config->db[3]['dbname']}`.`{$this->config->default_prefix}order` SET status='4' where type='saja' and orderid='{$osn}'" ;
			$this->model->query($query);
			$push_info['title']="711貨品訂單出貨通知(測試)";
			$push_info['body']=$body;
			$push_info['productid']=$productid;
			$push_info['action']="0";
			$push_info['url_title']="";
			$push_info['url']="";
			$push_info['sendto']=$sendto;
			$push_info['page']="9";
			$push_info['groupset']="";
			error_log("[push after_order_close ]: ".json_encode($push_info));
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_URL, $_SERVER['HTTP_ORIGIN']."/site/push/goods_arrival");
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($push_info));
			curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

			$result = curl_exec($ch);
			error_log("[push after_order_close ]:78 ".json_encode($result));
			curl_close($ch);
			$r['retCode']=1;
			$r['retOsn']=$osn;
			$r['retMsg']=(empty(trim($result)))?'出貨完成':$result;

		}else{
			$r['retCode']=-7;
			$r['retOsn']=$osn;
			$r['retMsg']='鯊魚點寫入失敗';
		}
	}
}
echo json_encode($r);

// Table Record End

// Table End
##############################################################################################################################################


##############################################################################################################################################
// Log Start
$order_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET
	`name` = '{$this->io->input['session']['user']["name"]}',
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'order',
	`active` = '711訂單轉鯊魚點出貨',
	`memo` = '{$order_data}',
	`root` = 'order/batch_process_711',
	`ipaddress`='{$ip}',
	`atime`=now(),
	`insertt`=now()
";
$this->model->query($query);
// Log End
##############################################################################################################################################

