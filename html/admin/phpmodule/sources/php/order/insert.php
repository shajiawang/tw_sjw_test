<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];


##############################################################################################################################################
// Insert Start

$order_status = isset($this->io->input["post"]["status"]) ? $this->io->input["post"]["status"] : '0';
$userid = isset($this->io->input["post"]["userid"]) ? $this->io->input["post"]["userid"] : '';
$num = isset($this->io->input["post"]["num"]) ? (int)$this->io->input["post"]["num"] : 1;
$point_price = isset($this->io->input["post"]["point_price"]) ? (float)$this->io->input["post"]["point_price"] : 0;
$process_fee = isset($this->io->input["post"]["process_fee"]) ? (float)$this->io->input["post"]["process_fee"] : 0;
$retail_price = isset($this->io->input["post"]["retail_price"]) ? (float)$this->io->input["post"]["retail_price"] : 0;
$cost_price = isset($this->io->input["post"]["cost_price"]) ? (float)$this->io->input["post"]["cost_price"] : 0;

//分潤=(市價-進貨價)*數量
$profit = ($retail_price - $cost_price) * $num;
//總費用
$total_fee = ($point_price + $process_fee) * $num;

//檢查使用者的紅利點數
$query ="
SELECT SUM(amount) bonus 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}bonus`  
WHERE 
`prefixid` = '{$this->config->default_prefix_id}' 
AND `switch` = 'Y'
AND `userid` = '{$userid}' 
";
$recArr = $this->model->getQueryRecord($query);
$user_bonus = ($recArr['table']['record'][0]['bonus']) ? (float)$recArr['table']['record'][0]['bonus'] : 0;

if($user_bonus < $total_fee) {
	$this->jsAlertMsg('使用者的紅利點數錯誤!!');	
} else {

	//新增訂單 Relation order
	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}order` SET 
		`userid`='{$userid}',
		`status`='{$order_status}', 
		`epid`='{$this->io->input["post"]["epid"]}',
		`num`='{$num}',
		`esid`='{$this->io->input["post"]["esid"]}',
		`point_price`='{$point_price}',
		`process_fee`='{$process_fee}',
		`total_fee`='{$total_fee}',
		`profit`='{$profit}',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	";
	$this->model->query($query);
	$orderid = $this->model->_con->insert_id;

	//扣除 庫存  Relation stock
	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}stock` SET 
		`epid`='{$this->io->input["post"]["epid"]}',
		`num`='-{$num}',
		`orderid`='{$orderid}',
		`behav`='user_exchange',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	";
	$this->model->query($query);
	
	//扣除 紅利點數 Relation bonus
	$query ="INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}bonus` SET 
		`userid`='{$this->io->input["post"]["userid"]}',
		`amount`='-{$total_fee}',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	";
	$this->model->query($query);
	$bonusid = $this->model->_con->insert_id;
	
	//Relation exchange_bonus_history
	$query ="INSERT INTO `{$db_exchange}`.`{$this->config->default_prefix}exchange_bonus_history` SET 
		`userid`='{$this->io->input["post"]["userid"]}',
		`orderid`='{$orderid}',
		`bonusid`='{$bonusid}',
		`amount`='-{$total_fee}',
		`prefixid`='{$this->config->default_prefix_id}',
		`insertt`=now()
	";
	$this->model->query($query);
}

// Insert End
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$order_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'order_insert', 
	`active` = '訂單新增寫入', 
	`memo` = '{$order_data}', 
	`root` = 'order/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
