<?php
// Check Variable Start
if (empty($this->io->input["get"]["plid"])) {
	$this->jsAlertMsg('限定編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_shop = $this->config->db[4]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT pl.*, c.name as cname, c.colorcode 
FROM `{$db_shop}`.`{$this->config->default_prefix}product_limited` pl 
LEFT JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."color` c ON
pl.colorid = c.cid
WHERE 
pl.`switch`='Y' 
AND pl.`plid` = '{$this->io->input["get"]["plid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
* 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."color` 
WHERE 
kind = 'product' 
AND switch = 'Y' 
AND used = 'Y' 
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["color"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################




##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_limited_edit', 
	`active` = '限定商品修改', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_limited/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################



$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();