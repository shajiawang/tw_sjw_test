<?php
// Check Variable Start
if (empty($this->io->input["get"]["plid"])) {
	$this->jsAlertMsg('限定編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_shop = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// database start

$query ="
UPDATE `{$db_shop}`.`{$this->config->default_prefix}product_limited` SET 
switch = 'N' 
WHERE 
 `plid` = '{$this->io->input["get"]["plid"]}'
" ;
$this->model->query($query);

// database end
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_limited_delete', 
	`active` = '限定商品刪除', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_limited/delete', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// success message Start
$this->jsPrintMsg('刪除成功!!', $this->io->input['get']['location_url'] );
// success message End
