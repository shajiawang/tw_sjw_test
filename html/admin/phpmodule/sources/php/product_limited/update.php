<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

// Check Variable Start
if (empty($this->io->input["post"]["plid"])) {
	$this->jsAlertMsg('限定編號錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('限定名稱錯誤!!');
}
if (empty($this->io->input["post"]["colorid"])) {
	$this->jsAlertMsg('限定色碼錯誤!!');
}
if (empty($this->io->input["post"]["kind"])) {
	$this->jsAlertMsg('限定類型錯誤!!');
}

// Check Variable End
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[4]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

if (!empty($this->io->input['files']['thumbnail']) && exif_imagetype($this->io->input['files']['thumbnail']['tmp_name'])) {
	$filename = md5(date("YmdHis")."_".$this->io->input['post']['name']).".".pathinfo($this->io->input['files']['thumbnail']['name'], PATHINFO_EXTENSION);
	
	if (file_exists($this->config->path_products_images."/limit/$filename") && ($filename != $this->io->input['post']['oldthumbnail'])) {
		$this->jsAlertMsg('類別圖名稱重覆!!');
	}
	else if (!move_uploaded_file($this->io->input['files']['thumbnail']['tmp_name'], $this->config->path_products_images."/limit/$filename")) {
		$this->jsAlertMsg('類別圖上傳錯誤!!');
	}
	
	if (!empty($this->io->input['post']['oldthumbnail']))
	{
		//刪除舊圖
		if (!empty($this->io->input['post']['oldthumbnail']) && ($filename != $this->io->input['post']['oldthumbnail'])) {
			unlink($this->config->path_products_images."/limit/".$this->io->input['post']['oldthumbnail']);
		}
	}
	$pic = "`thumbnail`='{$filename}',";
	syncToS3($this->config->path_products_images."/".$filename,'s3://img.saja.com.tw','/site/images/site/product/limit/');
}

if (empty($this->io->input["post"]["mvalue"])){
	$mvalue = 0;
} else {
	$mvalue = $this->io->input["post"]["mvalue"];
}

if (empty($this->io->input["post"]["svalue"])){
	$svalue = 0;
} else {
	$svalue = $this->io->input["post"]["svalue"];
}

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}product_limited` SET 
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}',
	`seq`='{$this->io->input["post"]["seq"]}',
	`colorid`='{$this->io->input["post"]["colorid"]}', 
	`mvalue`='{$mvalue}', 
	`svalue`='{$svalue}',
	`kind`='{$this->io->input["post"]["kind"]}',	
	`limit_group`='{$this->io->input["post"]["limit_group"]}',	
	{$pic} 
	`used`='{$this->io->input["post"]["used"]}'  
WHERE 
	`plid` = '{$this->io->input["post"]["plid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################



##############################################################################################################################################
// Log Start 
$prodcut_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'product_limited_update', 
	`active` = '限定商品修改寫入', 
	`memo` = '{$prodcut_data}', 
	`root` = 'product_limited/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################



header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));