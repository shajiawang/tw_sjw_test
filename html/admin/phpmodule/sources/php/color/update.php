<?php
// Check Variable Start
if (empty($this->io->input["post"]["cid"])) {
	$this->jsAlertMsg('色彩編號錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('色彩名稱錯誤!!');
}
if (empty($this->io->input["post"]["colorcode"])) {
	$this->jsAlertMsg('色彩色碼錯誤!!');
}
if (empty($this->io->input["post"]["kind"])) {
	$this->jsAlertMsg('色彩類型錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

// Check Variable End
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Update Start

$query ="
UPDATE `{$db_channel}`.`{$this->config->default_prefix}color` SET 
	`name`='{$this->io->input["post"]["name"]}',
	`description`='{$this->io->input["post"]["description"]}',
	`seq`='{$this->io->input["post"]["seq"]}',
	`colorcode`='{$this->io->input["post"]["colorcode"]}', 
	`kind`='{$this->io->input["post"]["kind"]}', 
	`used`='{$this->io->input["post"]["used"]}'    
WHERE 
	`cid` = '{$this->io->input["post"]["cid"]}'
" ;
$this->model->query($query);

// Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$color_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'color_update', 
	`active` = '色彩修改寫入', 
	`memo` = '{$color_data}', 
	`root` = 'color/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));