<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
$nickname = $this->io->input['session']['user']["nickname"];
$uname = $this->io->input['session']['user']["name"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user=$this->config->db[0]['dbname'];
$db_cash_flow=$this->config->db[1]['dbname'];
$prefix_=$this->config->default_prefix;
$prefix=$this->config->default_prefix_id;


##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
error_log("status['path']:".$status["status"]["path"]);
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sub_sort_query="";
$sort_pattern = "/^sort_(userid|name|nickname|amount)/";
$status["status"]["sort"] = ""; 
error_log("input['get']:".json_encode($this->io->input["get"]));
// 抓出$_GET裡面名稱開頭為sort_XXX的參數及值
foreach($this->io->input["get"] as $gk => $gv) {
    if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
	    // error_log("matches :".json_encode($matches));
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"]= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;	 		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
/*
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "
		AND u.userid = '".$this->io->input["get"]["search_userid"]."'";
}
*/
if($this->io->input["get"]["search_name"] != ''){
    $searchName=str_replace('*','%',$this->io->input["get"]["search_name"]);
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"]= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "	AND u.name like '".$searchName."'";
}
if($this->io->input["get"]["search_nickname"] != ''){
    $searchNickname=str_replace('*','%',$this->io->input["get"]["search_nickname"]) ;
	$status["status"]["search"]["search_nickname"] =$this->io->input["get"]["search_nickname"]; 
	$status["status"]["search_path"]= "&search_nickname=".$this->io->input["get"]["search_nickname"]; 
	$sub_search_query .=  "	AND up.nickname like '".$searchNickname."'";
}
if($this->io->input["get"]["search_userid"] != ''){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "AND u.`userid` ='".$this->io->input["get"]["search_userid"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 
// Count Total
$query = "SELECT count(u.userid)
FROM  `{$db_user}`.`{$prefix_}user` u
JOIN  `{$db_user}`.`{$prefix_}user_profile` up 
  ON u.userid = up.userid AND u.switch='Y' and u.prefixid =  '{$prefix}'
LEFT OUTER JOIN  `{$db_cash_flow}`.`{$prefix_}bonus` ub 
  ON u.userid = ub.userid AND ub.switch =  'Y' WHERE 1=1 ";

$query.= $sub_search_query ;
$query.=" GROUP BY u.userid, u.name, up.nickname ";
error_log("Count SQL : ".$query);
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage(count($num['table']['record']), $this);
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);

// Get Data Rows
$query = "SELECT u.userid, u.name, up.nickname, SUM(IFNULL(ub.amount,0)) as amount
FROM  `{$db_user}`.`{$prefix_}user` u
JOIN  `{$db_user}`.`{$prefix_}user_profile` up 
  ON u.userid = up.userid AND u.switch='Y' and u.prefixid =  '{$prefix}'
LEFT OUTER JOIN  `{$db_cash_flow}`.`{$prefix_}bonus` ub 
  ON u.userid = ub.userid AND ub.switch =  'Y' WHERE 1=1 ";
  
$query.= $sub_search_query ;
$query.=" GROUP BY u.userid, u.name, up.nickname ";
$query.= $sub_sort_query ; 
$query.=$query_limit;

error_log("Detail SQL : ".$query);
// error_log("[member_bonus] : ".$query);

$table = $this->model->getQueryRecord($query); 

// Table End 
##############################################################################################################################################

##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$member_bonus_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'member_bonus', 
	`active` = '會員紅利點數清單查詢', 
	`memo` = '{$member_bonus_data}', 
	`root` = 'member_bonus/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

//$status["status"]["base_href"]=$status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status["status"]["base_href"]=$status["status"]["path"].$status["status"]["search_path"].$status["status"]["p"];

$this->tplVar('page' , $page) ;	
$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);

$this->display();