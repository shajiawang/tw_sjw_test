<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End

##############################################################################################################################################
// 查詢變數設定 開始

$sdate = $this->io->input["get"]["sdate"]; //查詢起始日期
$edate = $this->io->input["get"]["edate"]; //查詢結束日期

if (!empty($sdate) && !empty($edate)){
		$sdate = $sdate;
		$edate = $edate;
} else {
		$now_date = date("Y-m-d");
		$sdate = $now_date." 00:00:00";
		$edate = $now_date." 23:59:59";
}

$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$status["status"]["search"]["sdate"] = $sdate ;
$status["status"]["search_path"] .= "&sdate=".$sdate;
$status["status"]["search"]["edate"] = $edate ;
$status["status"]["search_path"] .= "&edate=".$edate;

$this->tplVar('sdate' , $sdate);
$this->tplVar('edate' , $edate);
$this->tplVar('status',$status["status"]);

// 查詢變數設定 結束
##############################################################################################################################################
// 競標中商品 查詢 開始

$query = "SELECT sh.*, sp.`name` as prod_name, IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(sh.`userid`) AS subtotal, sp.`name` AS product_name, sp.`offtime` AS offtime,
					sp.`saja_fee` AS saja_fee,COUNT(sh.`userid`)*sp.`saja_fee` AS saja_sum,sp.hot_prod, sp.retail_price, 
					(SELECT `name` FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_limited` pl 
					WHERE pl.plid = sp.limitid AND pl.switch ='Y') as limitidname 
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
						ON sh.`productid` = sp.`productid`
					WHERE sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."'
							AND closed IN ('N')
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `pay`,`productid`,`prod_name`
					ORDER BY offtime,productid,pay ASC";
error_log("[admin/report_interval_analysis_statistics] 0 : ".$query);
$table = $this->model->getQueryRecord($query);

$this->tplVar('table0' , $table['table']) ;


##############################################################################################################################################
// 結標商品清單 查詢 開始
/*
$query = "SELECT sh.*, sp.`name` as prod_name, IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(sh.`userid`) AS subtotal, sp.`name` AS product_name, sp.`offtime` AS offtime,
					sp.`saja_fee` AS saja_fee,COUNT(sh.`userid`)*sp.`saja_fee` AS saja_sum,sp.hot_prod, sp.retail_price, 
					(SELECT `name` FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product_limited` pl 
					WHERE pl.plid = sp.limitid AND pl.switch ='Y') as limitidname 
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
						ON sh.`productid` = sp.`productid`
					WHERE sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."' AND offtime <='".$edate."'
							AND closed IN ('Y','NB')
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `pay`,`productid`,`prod_name`
					ORDER BY offtime,productid,pay ASC";
*/
$query = "SELECT sh.*, sp.`name` as prod_name, IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(sh.`userid`) AS subtotal, sp.`name` AS product_name, sp.`offtime` AS offtime,
					sp.`saja_fee` AS saja_fee,COUNT(sh.`userid`)*sp.`saja_fee` AS saja_sum,sp.hot_prod, sp.retail_price, 
					pgp.price as final_price,
                    pgp.userid as winner_userid,
                    pgp.nickname as winner_name					
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					 JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
						ON sh.`productid` = sp.`productid`
					 JOIN `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."pay_get_product` pgp 
					    ON sp.productid=pgp.productid
					WHERE pgp.switch='Y'
					  AND sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."' AND offtime <='".$edate."'
							AND closed IN ('Y')
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `pay`,`productid`,`prod_name`
					ORDER BY offtime,productid,pay ASC";
error_log("[admin/report_interval_analysis_statistics] 1 : ".$query);
$table = $this->model->getQueryRecord($query);

$this->tplVar('table1' , $table['table']) ;


// 結標商品清單 查詢 結束

##############################################################################################################################################
// 付費/免費人數統計 查詢 開始
/*
$query = "SELECT *
					FROM (SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(userid) AS subtotal
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
						ON sh.`productid` = sp.`productid`
					WHERE sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."' AND offtime <='".$edate."'
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `userid`,`pay`
					ORDER BY `subtotal` desc) AS tb1";
*/
$query = "SELECT *
					FROM (SELECT sh.*, sp.name as prod_name, IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(sh.userid) AS subtotal
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
						ON sh.`productid` = sp.`productid`
					WHERE sh.insertt >='".$sdate."' AND sh.insertt <='".$edate."' 
					  AND sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE switch ='Y'
						    AND offtime >='".$sdate."' 
							AND is_test = 'N'
							AND display = 'Y' 
						)
					GROUP BY `userid`,`pay`,`prod_name`
					ORDER BY sp.`productid` desc ,`subtotal` desc) AS tb1";



error_log("[admin/report_interval_analysis_statistics] 2 : ".$query);
$table = $this->model->getQueryRecord($query);

$this->tplVar('table2' , $table['table']) ;


// 付費/免費人數統計 查詢 結束

##############################################################################################################################################
// 總下標人數 查詢 開始
/*
$query ="SELECT count(*) as people_count
					FROM (SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(userid) AS subtotal
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
					 ON sh.`productid` = sp.`productid`
					WHERE sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."' AND offtime <='".$edate."'
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `userid`
					ORDER BY userid asc) AS tb1";
*/			
$query ="SELECT count(*) as people_count
					FROM (SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(userid) AS subtotal
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
					  ON sh.`productid` = sp.`productid`
					WHERE sh.insertt >='".$sdate."' AND sh.insertt <='".$edate."' 
					  AND sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE closed='N'
						    AND offtime >='".$sdate."' 
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `userid`
					ORDER BY userid asc) AS tb1";

error_log("[admin/report_interval_analysis_statistics] 3 : ".$query);
$table = $this->model->getQueryRecord($query);

$this->tplVar('people_count' , $table['table']['record'][0]['people_count']) ;


// 總下標人數 查詢 結束

##############################################################################################################################################
// 新增會員統計 查詢 開始
/*
$query ="SELECT `up`.`userid`,`up`.`nickname`,`u`.`insertt`
					FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
					LEFT JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
					 ON `u`.`userid` = `up`.`userid`
					WHERE `u`.`insertt` >='".$sdate."'AND `u`.`insertt` <='".$edate."'";
*/
$query ="SELECT `up`.`userid`,`up`.`nickname`,`u`.`insertt`
					FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
					 JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
					 ON `u`.`userid` = `up`.`userid`
					WHERE `u`.`insertt` >='".$sdate."'AND `u`.`insertt` <='".$edate."'
					ORDER BY `u`.`userid` desc ";

$table = $this->model->getQueryRecord($query);

$this->tplVar('table3' , $table['table']) ;


// 新增會員統計 查詢 結束

##############################################################################################################################################
// 手機驗證統計查詢  開始

$query ="SELECT count(userid) as cnt FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sms_auth` auth
		  WHERE switch='Y' 
		    AND verified='Y' 
			AND `modifyt` >='".$sdate."' 
			AND `modifyt` <='".$edate."'";

$table = $this->model->getQueryRecord($query);

$this->tplVar('table4' , $table['table']) ;


// Add By Thomas 2020/02/05 
// 儲值金額統計 開始
$query = " SELECT dr.name, SUM(IFNULL(d.amount,0)) AS total 
             FROM saja_cash_flow.saja_deposit_history dh 
             JOIN saja_cash_flow.saja_deposit d 
			   ON dh.depositid=d.depositid 
			 JOIN saja_cash_flow.saja_deposit_rule_item dri
			   ON dh.driid = dri.driid
			 JOIN saja_cash_flow.saja_deposit_rule dr
			   ON dri.drid = dr.drid
			WHERE dh.status='deposit' 
			  AND d.switch='Y'
  			  AND dh.switch='Y' 
			  AND d.behav='user_deposit'
			  AND dh.userid!='1705'
			  AND dr.mark='NT$' ";
$query.= " AND d.modifyt>='".$sdate."' AND d.modifyt<='".$edate."' ";	
$query.= " GROUP BY dr.name ";
					  
$table = $this->model->getQueryRecord($query);
$this->tplVar('table5' , $table['table']) ;
// 儲值金額統計 結束


// 手機驗證統計查詢 結束
##############################################################################################################################################

##############################################################################################################################################
// Log Start 
$report_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'report_interval_analysis_statistics', 
	`active` = '區間分析統計清單查詢', 
	`memo` = '{$report_data}', 
	`root` = 'report_interval_analysis_statistics/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################




$this->display();
