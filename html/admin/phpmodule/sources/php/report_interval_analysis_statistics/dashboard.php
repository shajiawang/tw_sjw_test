<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
}
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

##############################################################################################################################################
// Path Start
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End

##############################################################################################################################################
// 查詢變數設定 開始

$sdate = $this->io->input["get"]["sdate"]; //查詢起始日期
$edate = $this->io->input["get"]["edate"]; //查詢結束日期

if (!empty($sdate) && !empty($edate)){
		$sdate = $sdate;
		$edate = $edate;
} else {
		$now_date = date("Y-m-d");
		$sdate = $now_date." 00:00:00";
		$edate = $now_date." 23:59:59";
}

$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$status["status"]["search"]["sdate"] = $sdate ;
$status["status"]["search_path"] .= "&sdate=".$sdate;
$status["status"]["search"]["edate"] = $edate ;
$status["status"]["search_path"] .= "&edate=".$edate;

$this->tplVar('sdate' , $sdate);
$this->tplVar('edate' , $edate);
$this->tplVar('status',$status["status"]);

// 查詢變數設定 結束

// 儲值紀錄查詢
$query = "SELECT HOUR(insertt) as hour, SUM(amount) as amt FROM  saja_cash_flow.saja_deposit 
           WHERE 1=1
		     AND  insertt >='".$sdate."' AND insertt <='".$edate."'".
		 " GROUP BY hour ";


$query = "SELECT HOUR(insertt) as hour, IFNULL(SUM(amount),0) as amt FROM  saja_cash_flow.saja_deposit 
           WHERE 1=1
		     AND  insertt >='2019-07-20 00:00:00' AND insertt <='2019-07-31 23:59:59'
		 GROUP BY hour ";
$table = $this->model->getQueryRecord($query);
$this->tplVar('table' , $table) ;
// 

##############################################################################################################################################
// 結標商品清單 查詢 開始
/*
$query = "SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(sh.`userid`) AS subtotal, sp.`name` AS product_name, sp.`offtime` AS offtime,
									sp.`saja_fee` AS saja_fee,COUNT(sh.`userid`)*sp.`saja_fee` AS saja_sum
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
						ON sh.`productid` = sp.`productid`
					WHERE sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."' AND offtime <='".$edate."'
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `pay`,`productid`
					ORDER BY productid,pay ASC";

$table = $this->model->getQueryRecord($query);

$this->tplVar('table1' , $table['table']) ;
*/

// 結標商品清單 查詢 結束

##############################################################################################################################################
// 付費/免費人數統計 查詢 開始
/*
$query = "SELECT *
					FROM (SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(userid) AS subtotal
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
						ON sh.`productid` = sp.`productid`
					WHERE sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."' AND offtime <='".$edate."'
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `userid`,`pay`
					ORDER BY `subtotal` desc) AS tb1";

$table = $this->model->getQueryRecord($query);

$this->tplVar('table2' , $table['table']) ;
*/

// 付費/免費人數統計 查詢 結束

##############################################################################################################################################
// 總下標人數 查詢 開始
/*
$query ="SELECT count(*) as people_count
					FROM (SELECT sh.*,IF(sh.`spointid` = 0 OR sp.`totalfee_type` = 'O', 'free', 'paid') as pay, COUNT(userid) AS subtotal
					FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."history` sh
					LEFT join `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product` sp
					 ON sh.`productid` = sp.`productid`
					WHERE sh.`productid` IN (
						SELECT `productid`
						FROM `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."product`
						WHERE offtime >='".$sdate."' AND offtime <='".$edate."'
							AND is_test = 'N'
							AND display = 'Y'
							AND switch ='Y')
					GROUP BY `userid`
					ORDER BY userid asc) AS tb1";

$table = $this->model->getQueryRecord($query);

$this->tplVar('people_count' , $table['table']['record'][0]['people_count']) ;
*/

// 總下標人數 查詢 結束

##############################################################################################################################################
// 新增會員統計 查詢 開始
/*
$query ="SELECT `up`.`userid`,`up`.`nickname`,`u`.`insertt`
					FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
					LEFT JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
					 ON `u`.`userid` = `up`.`userid`
					WHERE `u`.`insertt` >='".$sdate."'AND `u`.`insertt` <='".$edate."'";

$table = $this->model->getQueryRecord($query);

$this->tplVar('table3' , $table['table']) ;

*/
// 新增會員統計 查詢 結束

##############################################################################################################################################

$this->display();
