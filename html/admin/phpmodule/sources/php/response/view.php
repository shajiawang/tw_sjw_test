<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(flash|name|description|ontime|offtime|seq)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY r.insertt DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if(!empty($this->io->input["get"]["search_category"])){
	$status["status"]["search"]["search_category"] = $this->io->input["get"]["search_category"] ;
	$status["status"]["search_category"] .= "&search_category=".$this->io->input["get"]["search_category"] ;
	$sub_search_query .=  "
		AND rt.`rcid` = '".$this->io->input["get"]["search_category"]."'";
	$category_search_query .= "
		left outer join `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt` rt on 
		rt.responseid = r.responseid
		and rt.rcid = '".$status["status"]["search"]["search_category"]."' 
		and rt.switch = 'Y'";
}
if(!empty($this->io->input["get"]["search_keyword"])){
	$status["status"]["search"]["search_keyword"] = $this->io->input["get"]["search_keyword"] ;
	$status["status"]["search_path"] .= "&search_keyword=".$this->io->input["get"]["search_keyword"] ;
	$sub_search_query .=  "
		AND r.`keyword` like '%".$this->io->input["get"]["search_keyword"]."%'";
}
if(!empty($this->io->input["get"]["search_name"])){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND r.`name` like '%".$this->io->input["get"]["search_name"]."%'";
}
/*$category_search_query = "
	left outer join `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt` rt on 
	rt.responseid = r.responseid 
	and rt.switch = 'Y'";*/
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response` r 
{$category_search_query}
WHERE 
r.prefixid = '".$this->config->default_prefix_id."' 
AND r.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="
SELECT 
r.* 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response` r 
{$category_search_query}
WHERE 
r.prefixid = '".$this->config->default_prefix_id."' 
AND r.switch = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
* 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["response_category"] = $recArr['table']['record'];

$query ="
SELECT 
* 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt` 
WHERE 
prefixid = '".$this->config->default_prefix_id."'
AND switch = 'Y'
";
$rtrecArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["response_category_rt"] = $rtrecArr['table']['record'];
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$msgtype_array = array("news" => "发送图文消息", "text" => "发送文本消息", "image" => "发送图片消息", "voice" => "发送语音消息", "video" => "发送视频消息", "music" => "发送音乐消息");


$this->tplVar('table' , $table['table']);
$this->tplVar('status',$status["status"]);
$this->tplVar('msgtype_array',$msgtype_array);
//echo '<pre>';print_r($status);die();
$this->display();