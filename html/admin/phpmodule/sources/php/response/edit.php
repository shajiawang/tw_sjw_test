<?php
if (empty($this->io->input["get"]["responseid"])) {
	$this->jsAlertMsg('自動回覆ID錯誤!!');
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT r.*, rcr.rcid 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response` r 
left outer join `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt` rcr on
r.responseid = rcr.responseid 
and rcr.prefixid = '".$this->config->default_prefix_id."' 
and rcr.switch = 'Y' 
WHERE 
r.prefixid = '".$this->config->default_prefix_id."' 
AND r.responseid = '".$this->io->input["get"]["responseid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
rc.*, rcr.responseid
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category` rc 
LEFT OUTER JOIN `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt` rcr ON 
	rc.prefixid = rcr.prefixid
	AND rc.rcid = rcr.rcid
	AND rcr.responseid = '".$this->io->input["get"]["responseid"]."'
	AND rcr.switch = 'Y'
WHERE 
rc.prefixid = '".$this->config->default_prefix_id."'
AND rc.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["response_category_rt"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################
$msgtype_array = array("news" => "发送图文消息", "text" => "发送文本消息", "image" => "发送图片消息", "voice" => "发送语音消息", "video" => "发送视频消息", "music" => "发送音乐消息");


$this->tplVar('table' , $table['table']);
$this->tplVar('msgtype_array' , $msgtype_array);
//$this->tplVar('status',$status["status"]);
$this->display();