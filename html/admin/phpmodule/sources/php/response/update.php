<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["responseid"])) {
	$this->jsAlertMsg('自動回覆ID錯誤!!');
}
if (!is_array($this->io->input["post"]["rcid"]) || empty($this->io->input["post"]["rcid"])) {
	$this->jsAlertMsg('自動回覆分類錯誤!!');
}
if (!is_array($this->io->input["post"]["keyword"])) {
	$this->jsAlertMsg('自動回覆關鍵字錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('自動回覆名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('自動回覆敘述錯誤!!');
}

$keyword = implode(",", array_filter($this->io->input["post"]["keyword"]));
$description = htmlspecialchars($this->io->input["post"]["description"]);

$query ="
UPDATE `{$this->config->db[2]["dbname"]}`.`{$this->config->default_prefix}response` 
SET
	`name`         	= '{$this->io->input["post"]["name"]}',
	`description`	= '{$description}',
	`keyword`	    = '{$keyword}',
	`thumbnail_url`	='{$this->io->input["post"]["thumbnail_url"]}',
	`msgtype`       ='{$this->io->input["post"]["msgtype"]}', 
	`url`	        ='{$this->io->input["post"]["url"]}',
	`seq`          	= '{$this->io->input["post"]["seq"]}', 
	`modifyt`       = NOW()
WHERE
	prefixid      = '{$this->config->default_prefix_id}'
	AND responseid = '{$this->io->input["post"]["responseid"]}'
	AND switch = 'Y'
" ;
$this->model->query($query);

//更新自動回覆分類rt
$query = "
DELETE FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt`
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND responseid = '".$this->io->input["post"]["responseid"]."'
";
$this->model->query($query);

$values = array();
foreach($this->io->input["post"]["rcid"] as $rk => $rv) {
	$values[] = "
		('".$this->config->default_prefix_id."', '".$this->io->input["post"]["responseid"]."', '".$rv."', '".(($rk+1)*10)."', NOW())";
}

$query = "
INSERT INTO `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category_rt`(
	`prefixid`,
	`responseid`,
	`rcid`,
	`seq`, 
	`insertt`
)
VALUES
".implode(',', $values)."
ON DUPLICATE KEY UPDATE switch = 'Y'
"; 
$this->model->query($query);


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));