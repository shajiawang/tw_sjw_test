<?php
// Check Variable Start
if (empty($this->io->input["post"]["deid"])) {
	$this->jsAlertMsg('活動編號錯誤!!');
}
if (empty($this->io->input["post"]["denum"])) {
	$this->jsAlertMsg('檔案編碼錯誤!!');
}
if (empty($this->io->input["post"]["dename"])) {
	$this->jsAlertMsg('活動標題錯誤!!');
}
if (empty($this->io->input["post"]["year"])) {
	$this->jsAlertMsg('年度錯誤!!');
}
if (empty($this->io->input["post"]["ontime"])) {
	$this->jsAlertMsg('開始時間錯誤!!');
}
if (empty($this->io->input["post"]["offtime"])) {
	$this->jsAlertMsg('結束時間錯誤!!');
}
if (empty($this->io->input["post"]["isdefault"])) {
	$this->jsAlertMsg('預設狀態錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

$denum = str_pad($this->io->input["post"]["denum"],4,'0',STR_PAD_LEFT);
##############################################################################################################################################
//Update Start
$query ="
UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}dream_event` SET 
	`denum`='{$denum}',
	`dename`='{$this->io->input["post"]["dename"]}',
	`year`='{$this->io->input["post"]["year"]}',
	`ontime`='{$this->io->input["post"]["ontime"]}',
	`offtime`='{$this->io->input["post"]["offtime"]}',	
	`url`='{$this->io->input["post"]["url"]}',	
	`tplfile`='{$this->io->input["post"]["tplfile"]}',	
	`isdefault`='{$this->io->input["post"]["isdefault"]}' 	
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	 AND `deid` = '{$this->io->input["post"]["deid"]}'
" ;
$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$dream_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'dream_event_update', 
	`active` = '圓夢活動管理修改寫入', 
	`memo` = '{$dream_data}', 
	`root` = 'dream_event/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));