<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_exchange = $this->config->db[3]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/"; 
// Path End 


// Sort Start
/** 調整排序欄位請修改下列Modify here to assign sort columns **/
$sort_pattern = "/^sort_(code|bonusid|prdate_no|id)/";
$status["status"]["sort"] = ""; 
foreach($this->io->input["get"] as $gk => $gv) {
	if (preg_match($sort_pattern, $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
$sub_sort_query =  " ORDER BY u.userid DESC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_uid"] != ''){
	$status["status"]["search"]["search_uid"] = $this->io->input["get"]["search_uid"] ;
	$status["status"]["search_path"] .= "&search_uid=".$this->io->input["get"]["search_uid"] ;
	$sub_search_query .=  "AND u.`userid` = '{$this->io->input["get"]["search_uid"]}' ";
}
if($this->io->input["get"]["search_nickname"] != ''){
	$status["status"]["search"]["search_nickname"] = $this->io->input["get"]["search_nickname"] ;
	$status["status"]["search_path"] .= "&search_nickname=".$this->io->input["get"]["search_nickname"] ;
	$sub_search_query .= "AND up.`nickname` like '%".$this->io->input["get"]["search_nickname"]."%' ";
}
if($this->io->input["get"]["search_switch"] != ''){
	$status["status"]["search"]["search_switch"] = $this->io->input["get"]["search_switch"] ;
	$status["status"]["search_path"] .= "&search_switch=".$this->io->input["get"]["search_switch"] ;
	
	$sub_search_query .=  "AND u.switch ='".$this->io->input["get"]["search_switch"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user` u 
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	u.prefixid = up.prefixid
	AND u.userid = up.userid 
WHERE 
u.prefixid = '".$this->config->default_prefix_id."' ";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query ="SELECT u.userid, u.name, u.switch, up.nickname, up.idnum 
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	u.prefixid = up.prefixid
	AND u.userid = up.userid 
WHERE 
u.prefixid = '".$this->config->default_prefix_id."' " ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
foreach($table["table"]['record'] as $rk => $rv)
{
	$query ="
	SELECT sum(tr_ibons) as TotalAmount, count(tr_ibons) as TotalCount 
	FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_ibon_record` 
	WHERE u_id = '{$rv['userid']}'
	" ;
	$ibon = $this->model->getQueryRecord($query); 
	$table["table"]['record'][$rk]['TotalAmount'] = $ibon["table"]['record'][0]['TotalAmount'];
	$table["table"]['record'][$rk]['TotalCount'] = $ibon["table"]['record'][0]['TotalCount'];
}

// Table Record End 

// Table End 
##############################################################################################################################################




##############################################################################################################################################
// Relation Start 

$query ="
SELECT *
FROM `{$db_exchange}`.`{$this->config->default_prefix}exchange_ibon_prdate` WHERE 1=1 " ;
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["prdate"] = $recArr['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$ibon_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_ibon', 
	`active` = '會員ibon兌換資料清單查詢', 
	`memo` = '{$ibon_data}', 
	`root` = 'user_ibon/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();