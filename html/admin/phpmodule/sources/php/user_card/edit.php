<?php
// Check Variable Start
if (empty($this->io->input["get"]["ucid"])) {
	$this->jsAlertMsg('信用卡ID錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_user}`.`{$this->config->default_prefix}user_creditcard`
WHERE 
	`switch`='Y' 
	AND `ucid` = '{$this->io->input["get"]["ucid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_card_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_card_edit', 
	`active` = '會員信用卡詳細內容查詢', 
	`memo` = '{$user_card_data}', 
	`root` = 'user_card/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();