<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];
$db_exchange = $this->config->db[3]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

$sub_sort_query =  " ORDER BY dc.dcid ASC ";
// Sort End

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_dinum"] != ''){
	$sub_search_query .=  "AND dc.`dinum` like '%".$this->io->input["get"]["search_dinum"]."%' ";
}
if($this->io->input["get"]["search_denum"] != ''){
	$sub_search_query .=  "AND dc.`denum` like '%".$this->io->input["get"]["search_denum"]."%' ";
}
// Search End

$query ="SELECT dc.dcid,dc.denum,dc.dinum,dc.count,de.dename,de.year,de.ontime,de.offtime,di.diname 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}dream_code` dc
LEFT OUTER JOIN `{$db_cash_flow}`.`{$this->config->default_prefix}dream_event` de ON 
	dc.prefixid = de.prefixid
	AND dc.denum = de.denum
	AND de.switch = 'Y'
LEFT OUTER JOIN `{$db_cash_flow}`.`{$this->config->default_prefix}dream_income` di ON 
	dc.prefixid = di.prefixid
	AND dc.dinum = di.dinum
	AND di.switch = 'Y'	
WHERE 
	dc.`prefixid` = '{$this->config->default_prefix_id}' 
	AND dc.`switch`='Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ;  
$table = $this->model->getQueryRecord($query);  

##############################################################################################################################################
// Log Start 
$code_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'dream_code_export', 
	`active` = '圓夢編碼匯出', 
	`memo` = '{$code_data}', 
	`root` = 'dream_code/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//會員資料表
$Report_Array[0]['title'] = array(
	"A1" => "圓夢編號", 
	"B1" => "來源編碼", 
    "C1" => "流量來源名稱", 
    "D1" => "檔期編碼", 
    "E1" => "活動名稱", 
    "F1" => "年度", 
    "G1" => "開始時間",
    "H1" => "結束時間", 
    "I1" => "觸發計數");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['dcid']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('B'.$row, $value['dinum']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['diname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row, $value['denum']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('E'.$row, (string)$value['dename'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('F'.$row, (string)$value['year'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('G'.$row, (string)$value['ontime'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('H'.$row, (string)$value['offtime'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['count']);
	
	}
}

$objPHPExcel->getActiveSheet()->setTitle('圓夢編號管理');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="圓夢編號管理'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
	

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>