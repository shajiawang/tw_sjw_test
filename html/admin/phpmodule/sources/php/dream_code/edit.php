<?php
// Check Variable Start
if (empty($this->io->input["get"]["dcid"])) {
	$this->jsAlertMsg('圓夢編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}dream_code`
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y' 
	AND `dcid` = '{$this->io->input["get"]["dcid"]}'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
 $query ="
SELECT *
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}dream_event` 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y'
" ;
$recArr = $this->model->getQueryRecord($query);  

$table["table"]["rt"]["dream_event"] = $recArr['table']['record'];

$query ="
SELECT *
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}dream_income` 
WHERE 
	`prefixid` = '{$this->config->default_prefix_id}' 
	AND `switch`='Y'
" ;
$recArr2 = $this->model->getQueryRecord($query); 
 
$table["table"]["rt"]["dream_income"] = $recArr2['table']['record'];

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$dream_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'dream_code_edit', 
	`active` = '圓夢編號修改', 
	`memo` = '{$dream_data}', 
	`root` = 'dream_code/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();