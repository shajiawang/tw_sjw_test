<?php
// Check Variable Start
if (empty($this->io->input["post"]["userid"])) {
	$this->jsAlertMsg('會員編號不可為空白 !!');
}
if (empty($this->io->input["post"]["behav"])) {
	$this->jsAlertMsg('超級殺價券行為不可為空白!!');
}
if (!is_numeric($this->io->input["post"]["scode"]) || $this->io->input["post"]["scode"] <= 0) {
	$this->jsAlertMsg('超級殺價券數量錯誤!!');
}

// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

if (($_SESSION['user']['department'] != 'S') &&
	($_SESSION['user']['department'] != 'B') &&
	($_SESSION['user']['department'] != 'D')) { 
	$this->jsAlertMsg('您無權限使用！');
	die();
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
$snum = $this->io->input["post"]["snum"];
$in_ar = explode(",", $this->io->input["post"]["userid"]);
$productid = (empty($this->io->input["post"]["productid"]))? 0:$this->io->input["post"]["productid"];
$batch = 'SYS_'. date('YmdHi');
$memo = $batch .' : '. $this->io->input["post"]["memo"];

//贈送超殺券
for ($i=0;$i<sizeof($in_ar);$i++){
	$query ="INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode` SET 
		`prefixid`='{$this->config->default_prefix_id}', 
		`userid` = '".$in_ar[$i]."', 
		`behav` = '{$this->io->input["post"]["behav"]}',
		`productid` = '{$productid}', 	
		`amount` = '{$this->io->input["post"]["scode"]}', 
		`remainder` = '{$this->io->input["post"]["scode"]}', 
		`offtime` = '{$this->io->input["post"]["offtime"]}',
		`spid` = '{$this->io->input["post"]["spid"]}',
		`memo` = '{$memo}',
		`closed` = 'N', 
		`seq`='0', 
		`switch`='Y', 
		`insertt`=now()
	" ;
	$this->model->query($query);
	$scodeid = $this->model->_con->insert_id;

	//if ($this->io->input["post"]["spid"] != 0){
		$query = "INSERT INTO `{$db_cash_flow}`.`{$this->config->default_prefix}scode_history`
		SET `prefixid`='{$this->config->default_prefix_id}',
		   `userid`='".$in_ar[$i]."', 
		   `scodeid`='{$scodeid}',
		   `spid`='{$this->io->input["post"]["spid"]}',
		   `promote_amount`='1',
		   `num`='{$this->io->input["post"]["scode"]}',
		   `batch`='{$batch}',
		   `insertt`=NOW()
		";
		$this->model->query($query);
	//}
}

//推播內容參數
$sendto = (empty($this->io->input["post"]["userid"]))? "":$this->io->input["post"]["userid"];

//過濾商品名稱內 單引號 雙引號 右斜線
$push_title = $this->io->input["post"]["push_title"];
$push_title = str_replace('\\', '', $push_title);
$push_title = str_replace('\'', '', $push_title);
$push_title = str_replace('"', '', $push_title);
if (empty($this->io->input["post"]["push_body"])) {
	$body = '';
	$this->jsAlertMsg('推播內容空白!!');
}
$body = $this->io->input["post"]["push_body"];

$inp=array();
$inp['title'] = $push_title; // .'_'. date('m-d');
$inp['body'] = $body;
$inp['productid'] = $productid;
$inp['action'] = 4;
$inp['url_title'] = '';
$inp['url'] = '';
$inp['sender'] = $this->io->input["session"]["user"]["userid"];
$inp['page'] = 22;
$inp['target'] = 'saja';

//推播內容寫入記錄
$push_query ="INSERT INTO `$db_channel`.`{$this->config->default_prefix}push_msg`
		SET
			`title`			= '{$push_title}',
			`groupset`		= '0',
			`body`			= '{$inp['body']}',
			`action`		= '{$inp['action']}',
			`productid`		= '{$inp['productid']}',
			`url_title`		= '{$inp['url_title']}',
			`url`			= '{$inp['url']}',
			`page`			= '{$inp['page']}',
			`sender`		= '{$inp['sender']}',
			`sendto`		= '{$sendto}',
			`sendtime`		= now(),
			`pushed`		= 'Y',
			`is_hand`       = 'Y',
			`push_type`     = 'H',
			`force_show`	='Y',
			`insertt`		= now()
		";
if (!empty($sendto)){
	error_log("[scode_push_msg]: ". $push_query);
	$this->model->query($push_query);
	
	//發推播
	$inp['sendto'] = $sendto. ",38802";
	$rtn=sendFCM($inp);
	error_log("[sendFCM]inp: ". json_encode($inp) ." , rtn:". json_encode($rtn) );
}
	
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_scode_insert', 
	`active` = '手動加超級殺價券新增寫入', 
	`memo` = '{$add_scode_data}', 
	`root` = 'add_scode/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
// $this->jsPrintMsg('殺價券(userid:{$t['table']['record'][0]['userid']},productid:${productid}, total:${snum} ) : 新增 ${total} 組完成!!',$this->io->input["get"]["location_url"]);
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));