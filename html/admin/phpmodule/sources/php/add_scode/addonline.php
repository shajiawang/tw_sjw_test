<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

if (($_SESSION['user']['department'] != 'S') &&
	($_SESSION['user']['department'] != 'B') &&
	($_SESSION['user']['department'] != 'D')){ 
	$this->jsAlertMsg('您無權限使用！');
	die();
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start
//$query_user ="SELECT `uid`,`insertt` FROM `{$db_user}`.`{$this->config->default_prefix}user_detail_log` 
//WHERE `uid`>0 AND DATE_FORMAT(`insertt`, '%Y-%m-%d') = CURDATE() AND HOUR(`insertt`) = DATE_FORMAT(CURTIME(), '%H') AND MINUTE(`insertt`) <= 10
//GROUP BY `uid` ORDER BY `insertt` DESC";
$query_user ="SELECT `userid` FROM `{$db_user}`.`{$this->config->default_prefix}user_detail_online` 
WHERE `userid`>0 AND DATE_FORMAT(`atday`, '%Y-%m-%d') = CURDATE() 
GROUP BY `userid`
ORDER BY `attime` DESC
";
$table_user = $this->model->getQueryRecord($query_user);

//在線會員
$online_user = '';
if(!empty($table_user['table']['record'])){
	foreach($table_user['table']['record'] as $key => $value){
		$users[] = $value['userid'];
	}
	$online_user = implode(",", $users);
}
$this->tplVar('online_user' , $online_user) ; 

// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote`
WHERE NOW() between ontime and offtime 
" ;
$table = $this->model->getQueryRecord($query);
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_scode_addonline', 
	`active` = '在線會員手動加送超級殺價券', 
	`memo` = '{$add_scode_data}', 
	`root` = 'add_scode/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->display();