<?php
// Check Variable Start
if (empty($this->io->input["post"]["scodeid"])) {
	$this->jsAlertMsg('超級殺價券編號錯誤!!');
}
// Check Variable End
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

if (($_SESSION['user']['department'] != 'S') &&
	($_SESSION['user']['department'] != 'B') &&
	($_SESSION['user']['department'] != 'D')) { 
	$this->jsAlertMsg('您無權限使用！');
	die();
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
//Update Start

	$query = "
	UPDATE `{$db_cash_flow}`.`{$this->config->default_prefix}scode` SET 
		`switch` = '{$this->io->input["post"]['switch']}', 
		`memo` = '{$this->io->input["post"]['memo']}', 
		`offtime` = '{$this->io->input["post"]['offtime']}', 
		`modifyt` = NOW()
	WHERE 
		`prefixid` = '{$this->config->default_prefix_id}'
		AND `scodeid` = '{$this->io->input["post"]['scodeid']}'
	" ;
	$this->model->query($query);

//Update End
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_scode_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_scode_update', 
	`active` = '手動加超級殺價券修改寫入', 
	`memo` = '{$add_scode_data}', 
	`root` = 'add_scode/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################
		   
header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));