<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

if (($_SESSION['user']['department'] != 'S') &&
	($_SESSION['user']['department'] != 'B') &&
	($_SESSION['user']['department'] != 'D')){ 
	$this->jsAlertMsg('您無權限使用！');
	die();
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_channel = $this->config->db[2]['dbname'];
$db_user = $this->config->db[0]['dbname'];
$db_product = $this->config->db[4]["dbname"];
$db_cash_flow = $this->config->db[1]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start
 
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start
$query ="
SELECT * 
FROM `{$db_cash_flow}`.`{$this->config->default_prefix}scode_promote`
WHERE NOW() between ontime and offtime 
" ;
$table = $this->model->getQueryRecord($query);
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$add_scode_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'add_scode_add', 
	`active` = '手動加超級殺價券新增', 
	`memo` = '{$add_scode_data}', 
	`root` = 'add_scode/add', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
$this->display();