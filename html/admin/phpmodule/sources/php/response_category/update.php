<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["rcid"])) {
	$this->jsAlertMsg('自動回覆分類ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('自動回覆分類名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('自動回覆分類敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('自動回覆分類排序錯誤!!');
}

$query ="
UPDATE `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category`
SET
`name` = '".$this->io->input["post"]["name"]."',
`description` = '".$this->io->input["post"]["description"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND rcid = '".$this->io->input["post"]["rcid"]."'
" ;
$this->model->query($query);

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));