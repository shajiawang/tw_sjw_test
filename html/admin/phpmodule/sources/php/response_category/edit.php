<?php
if (empty($this->io->input["get"]["rcid"])) {
	$this->jsAlertMsg('自動回覆分類ID錯誤!!');
}

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT 
*
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category`
WHERE 
prefixid = '".$this->config->default_prefix_id."' 
AND rcid = '".$this->io->input["get"]["rcid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT 
rc.*
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."response_category` rc 
WHERE 
rc.prefixid = '".$this->config->default_prefix_id."'
AND rc.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["response_category"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################



$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();