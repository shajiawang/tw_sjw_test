<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["grid"])) {
	$this->jsAlertMsg('禮券回饋規則ID錯誤!!');
}
if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('禮券回饋規則名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('禮券回饋規則敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('禮券回饋規則排序錯誤!!');
}

$query ="
UPDATE `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."gift_rule`
SET
`name` = '".$this->io->input["post"]["name"]."',
`description` = '".$this->io->input["post"]["description"]."',
`seq` = '".$this->io->input["post"]["seq"]."',
`switch` = '".$this->io->input["post"]["switch"]."'
WHERE
prefixid = '".$this->config->default_prefix_id."'
AND grid = '".$this->io->input["post"]["grid"]."'
" ;
$this->model->query($query);

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));