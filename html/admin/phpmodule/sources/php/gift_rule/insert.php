<?php
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('禮券回饋規則名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('禮券回饋規則敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('禮券回饋規則排序錯誤!!');
}

$query ="
INSERT INTO `".$this->config->db[4]["dbname"]."`.`".$this->config->default_prefix."gift_rule`(
	`prefixid`,
	`name`,
	`description`, 
	`seq`, 
	`switch`,
	`insertt`
) 
VALUES(
	'".$this->config->default_prefix_id."',
	'".$this->io->input["post"]["name"]."',
	'".$this->io->input["post"]["description"]."',
	'".$this->io->input["post"]["seq"]."',
	'".$this->io->input["post"]["switch"]."',
	now()
)
" ;
//echo $query;exit;
$this->model->query($query);

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));