<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$insert_by = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/convertString.ini.php";
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$this->str = new convertString();
$db_user = $this->config->db[0]['dbname'];

$intro_by = trim($this->io->input['post']["intro_by"]);
$userid = trim($this->io->input['post']["userid"]);

if(isset($this->io->input['post']["type"]) && $this->io->input['post']["type"]=="to_spoint"){
	$type = 1;
} else {
	$type = 0;
}

if (empty($intro_by) || ! is_numeric($intro_by) ) {
	$ret['retCode'] = -1;
	$ret['retMsg']='推薦人ID, 不能空白,且必須是數字 !!';
}
elseif (empty($userid) || ! is_numeric($userid) ) {
	$ret['retCode'] = -2;
	$ret['retMsg']='新會員ID, 不能空白,且必須是數字 !!';
}
elseif ($intro_by==$userid) {
	$ret['retCode'] = -3;
	$ret['retMsg']='推薦人'. $intro_by .'與新會員'. $userid .' 必須不同 !!';
}

if( empty($type) ){
	$this->jsAlertMsg($ret['retMsg']);
}

$today = date("Y-m-d H:i:s");
$productid = '';//商品代碼
$oscode_cnt = 0;//券數量

//新人送幣數量
$spoint_cnt_user = 0;
if ( $today >= '2019-11-22 10:00:00' ) {
	$spoint_cnt_user = 50; 
}

//推薦人送幣數量
$spoint_cnt_intro = 50;
$chk_referral = false;
$insert_spoint = false;


// 1)推薦人的 intro_by 不能是 新人$userid
$query = "SELECT  `suaid`, `userid`, `intro_by`, `promoteid` 
	FROM `{$db_user}`.`{$this->config->default_prefix}user_affiliate` 
	WHERE `userid` = '{$intro_by}' 
		AND `intro_by` = '{$userid}'
		AND `act` = 'REG'
		AND switch = 'Y'
		AND `insertt` > '2019-09-24 12:00:00'
	";
$introArr = $this->model->getQueryRecord($query);

if(empty($introArr['table']['record']) ){
	$chk_referral = true;
} else {
	$chk_referral = false;
	
	$ret['retCode'] = -4;
	$ret['retMsg']='推薦人'. $intro_by .'資格錯誤 !!';
	if( empty($type) ){
		$this->jsAlertMsg($ret['retMsg']);
	}
}

if($chk_referral)
{
	// 2)被推薦新人有無資料
	$query = "SELECT  `suaid`, `userid`,  `intro_by`, `promoteid` 
	FROM `{$db_user}`.`{$this->config->default_prefix}user_affiliate` 
	WHERE 
		`userid` = '{$userid}' 
		AND `act` = 'REG'
		AND switch = 'Y'
		AND `insertt` > '2019-09-24 12:00:00'
	";
	$recArr = $this->model->getQueryRecord($query);

	// 3)設定推薦人
	if(empty($recArr['table']['record']) ){
		$query = "INSERT INTO `{$db_user}`.`{$this->config->default_prefix}user_affiliate` 
				SET `come_from`='{$ip}',
					`userid`='{$userid}',
					`intro_by`='{$intro_by}',
					`act`='REG',
					`promoteid`='8',
					`insert_by`='{$insert_by}',
					`insertt`=NOW()";
		$this->model->query($query);
		
		$insert_spoint = true;

	} else {
		
		if(empty($recArr['table']['record'][0]["intro_by"]) ){
			$query = "UPDATE `{$db_user}`.`{$this->config->default_prefix}user_affiliate`  
					SET `intro_by`='{$intro_by}', 
					`insert_by`='{$insert_by}'
					WHERE `userid`='{$userid}'";
			$this->model->query($query);
			
			$insert_spoint = true;
			
		} else {
			
			if( $recArr['table']['record'][0]["intro_by"] == $intro_by){
				$insert_spoint = true;
			} else {
				$insert_spoint = false;
				
				$ret['retCode'] = -5;
				$ret['retMsg']='新會員ID '.$userid.', 資料已存在 !!';
				if( empty($type) ){
					$this->jsAlertMsg($ret['retMsg']);
				}
			}
		}
	}

	// 4)推薦送幣
	if($insert_spoint)
	{
		//新人 - 送幣紀錄
		$query = "INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}spoint` SET
				`userid`='{$userid}',
				`behav`='gift',
				`countryid`='1',
				`amount`='{$spoint_cnt_user}',
				`remark`='18',
				`insertt`=NOW()";
		$this->model->query($query);
		$spointid = $this->model->_con->insert_id;

		//新人 - 活動送幣使用紀錄
		$query = "INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}spoint_free_history` SET
				`userid`='{$userid}',
				`behav`='gift',
				`activity_type`='18',
				`amount_type`='1',
				`free_amount`='{$spoint_cnt_user}',
				`total_amount`='{$spoint_cnt_user}',
				`spointid`='{$spointid}',
				`insertt`=NOW()";
		$this->model->query($query); 

		//新人 - 寫入活動紀錄
		$query = "INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}activity_history` SET
				`userid`='{$userid}',
				`activity_type`=18,
				`insertt`=NOW()";
		$this->model->query($query); 
			
		
		//推薦人 - intro_by 送幣紀錄
		$query_intro = "INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}spoint` SET
			`userid`='{$intro_by}',
			`memo`='{$userid}',
			`behav`='gift',
			`countryid`='1',
			`amount`='{$spoint_cnt_intro}',
			`remark`='8',
			`insertt`=NOW()
		";
		$this->model->query($query_intro);
		$spointid_intro = $this->model->_con->insert_id;

		//推薦人 - 活動送幣使用紀錄
		$query_intro = "INSERT INTO `{$this->config->db[1]['dbname']}`.`{$this->config->default_prefix}spoint_free_history` SET
			`userid`='{$intro_by}',
			`behav`='gift',
			`activity_type`='8',
			`amount_type`='1',
			`free_amount`='{$spoint_cnt_intro}',
			`total_amount`='{$spoint_cnt_intro}',
			`spointid`='{$spointid_intro}',
			`insertt`=NOW()
		";
		$this->model->query($query_intro); 
		
		$ret['retCode'] = 1;
		$ret['retMsg']='OK';
	}

}

##############################################################################################################################################
// Log Start 
$user_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);
$db['db_insert']="user_affiliate|activity_history|spoint|spoint_free_history";
$user_data .= json_encode($db);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user_affiliate_insert', 
	`active` = '會員推薦新增寫入', 
	`memo` = '{$user_data}', 
	`root` = 'spoint/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

if( empty($type) ){
	header("location:". $this->io->input['post']['location_url'] );
} else {
	echo json_encode($ret);
}
