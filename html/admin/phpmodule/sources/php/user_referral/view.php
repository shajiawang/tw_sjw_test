<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(intro_by|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	
	//if($orders=="\'userid\'") $orders="\'ua.userid\'";
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}else{
	$sub_sort_query =  " ORDER BY ua.`intro_by`";
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "AND ua.`intro_by` >0
		AND ua.`act` = 'REG'
		AND ua.`insertt` > '2019-09-24 12:00:00'
		AND ua.`promoteid`=8";
$join_search_query = "
LEFT JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up ON 
	up.`userid`=ua.`intro_by`
LEFT JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user` u ON 
	u.`userid`=ua.`intro_by`
";

//(SELECT switch FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user` where `userid`=ua.`intro_by`) as switch


if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	$sub_search_query .=  "
		AND ua.`insertt` BETWEEN '".$this->io->input["get"]["search_btime"]."' AND '".$this->io->input["get"]["search_etime"]."' ";
}
		
if($this->io->input["get"]["search_introby"] != ''){
	$status["status"]["search"]["search_introby"] = trim($this->io->input["get"]["search_introby"]);
	$status["status"]["search_path"] .= "&search_introby=". trim($this->io->input["get"]["search_introby"]);
	$sub_search_query .=  " AND ua.`intro_by` ='". trim($this->io->input["get"]["search_introby"]) ."' ";
}

if($this->io->input["get"]["search_act"] == 'Y'){
	$status["status"]["search"]["search_act"] = $this->io->input["get"]["search_act"];
	$status["status"]["search_path"] .= "&search_act=". $this->io->input["get"]["search_act"];
	$sub_search_query .=  " AND u.`switch`='N' ";
} elseif($this->io->input["get"]["search_act"] == 'A'){
	$status["status"]["search"]["search_act"] = $this->io->input["get"]["search_act"];
	$status["status"]["search_path"] .= "&search_act=". $this->io->input["get"]["search_act"];
} else {
	$status["status"]["search"]["search_act"] = $this->io->input["get"]["search_act"];
	$status["status"]["search_path"] .= "&search_act=". $this->io->input["get"]["search_act"];
	$sub_search_query .=  " AND u.`switch`='Y' ";
}

if($this->io->input["get"]["search_detail"] == 'Y'){
	$status["status"]["search"]["search_detail"] = 'Y';
	$status["status"]["search_path"] .= "&search_detail=Y";

	if($this->io->input["get"]["search_userid"] != ''){
		$status["status"]["search"]["search_userid"] = trim($this->io->input["get"]["search_userid"]);
		$status["status"]["search_path"] .= "&search_userid=". trim($this->io->input["get"]["search_userid"]);
		$sub_search_query .=  " AND ua.`userid` ='". trim($this->io->input["get"]["search_userid"]) ."' ";
	}
	
	$join_search_query .= "
LEFT JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_sms_auth` sa ON 
	sa.`userid`=ua.`userid` 
	AND sa.verified='Y'
";
	
} else {
	$sub_search_query .=" GROUP BY ua.`intro_by`"; 
}

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start

// Table Count Start 
$query="SELECT count(*) as num 
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user_affiliate` ua
{$join_search_query}
WHERE ua.switch='Y'
";
$query .= $sub_search_query ; 
$num = $this->model->getQueryRecord($query);

if($this->io->input["get"]["search_detail"] == 'Y'){
	$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
}else{
	$num_cnt = count($num['table']['record']);
	$page = $this->model->recordPage($num_cnt, $this);
}

$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
if($this->io->input["get"]["search_detail"] == 'Y'){
	$query ="SELECT ua.`intro_by`,up.nickname, up.phone, ua.`userid`, sa.`verified`, sa.`phone` phone2, ua.`insertt`, ua.`insert_by`, 
			(SELECT name FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user` where `userid`=ua.`userid`) as phone3,
			u.switch
			";
} else {
	$query ="SELECT ua.`intro_by`,up.nickname, up.phone, count(ua.`intro_by`) cnt, u.switch
			";
}

$query .=" FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_affiliate` ua
{$join_search_query}
WHERE ua.switch='Y'
" ;

$query .= $sub_search_query ;
if($this->io->input["get"]["search_detail"] == 'Y'){
	$query .= $sub_sort_query ; 
} else {
	$query .=  " ORDER BY cnt DESC";
}

$query .= $query_limit ;

$table = $this->model->getQueryRecord($query);

if(!empty($table['table']['record']) && ($this->io->input["get"]["search_detail"] == 'Y') ){
	foreach($table['table']['record'] as $rk => $rv){
		//被推薦新人的暱稱
		$query ="SELECT up.`userid`,up.nickname
			FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` up
			WHERE up.prefixid='saja'
			AND up.`userid`={$rv['userid']}
			";
		$user = $this->model->getQueryRecord($query);
		$table['table']['record'][$rk]["username"] = $user['table']['record'][0]["nickname"];
		
		//推薦人的殺幣
		$query ="SELECT `userid`,`amount`,`remark`,`memo`,`insertt` FROM `saja_cash_flow`.`saja_spoint` 
			WHERE `remark`=8 AND `userid`={$rv['intro_by']} AND `memo`={$rv['userid']} ";
		$r_spoint = $this->model->getQueryRecord($query);
		$table['table']['record'][$rk]["intro_spoint"] = $r_spoint['table']['record'][0]["amount"];
		$table['table']['record'][$rk]["intro_insertt"] = $r_spoint['table']['record'][0]["insertt"];
		
		//被推薦新人的殺幣
		$query ="SELECT `userid`,`amount`,`remark`,`memo`,`insertt` FROM `saja_cash_flow`.`saja_spoint` 
			WHERE `remark`=18 AND `userid`={$rv['userid']} ";
		$u_spoint = $this->model->getQueryRecord($query);
		$table['table']['record'][$rk]["user_spoint"] = $u_spoint['table']['record'][0]["amount"];
		$table['table']['record'][$rk]["user_insertt"] = $u_spoint['table']['record'][0]["insertt"];
	}
}

// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$user_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'user', 
	`active` = '會員推薦查詢', 
	`memo` = '{$user_data}', 
	`root` = 'user/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
//$this->model->query($query);
// Log End 
##############################################################################################################################################
$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();
