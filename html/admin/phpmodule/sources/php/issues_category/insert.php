<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

if (empty($this->io->input["post"]["name"])) {
	$this->jsAlertMsg('分類名稱錯誤!!');
}
if (empty($this->io->input["post"]["description"])) {
	$this->jsAlertMsg('分類敘述錯誤!!');
}
if (!is_numeric($this->io->input["post"]["seq"])) {
	$this->jsAlertMsg('分類排序錯誤!!');
}

$query ="
INSERT INTO `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_category`(
	`name`,
	`description`, 
	`seq`, 
	`switch`,
	`insertt`
) 
VALUES(
	'".$this->io->input["post"]["name"]."',
	'".$this->io->input["post"]["description"]."',
	'".$this->io->input["post"]["seq"]."',
	'".$this->io->input["post"]["switch"]."',
	now()
)
" ;
//echo $query;exit;
$this->model->query($query);


##############################################################################################################################################
// Log Start 
$issues_category_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_category_insert', 
	`active` = '客服分類管理新增寫入', 
	`memo` = '{$issues_category_data}', 
	`root` = 'issues_category/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));