<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(pcid|name|seq)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;		
	}
}

if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = "`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
if($this->io->input["get"]["search_name"] != ''){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "
		AND `name` like '%".$this->io->input["get"]["search_name"]."%'";
}

//統計起迄
$stat_search_query = '';
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	
	$search_btime = $this->io->input["get"]["search_btime"];
	$search_etime = $this->io->input["get"]["search_etime"];
	$stat_search_query .=  " AND ir.`insertt` BETWEEN '".$this->io->input["get"]["search_btime"]."'
	AND '".$this->io->input["get"]["search_etime"]."' ";
} else {
	$search_btime = date('Y-m-d').' 00:00:00';
	$search_etime = date('Y-m-d').' 23:59:59';
	$this->io->input["get"]["search_btime"] = $search_btime;
	$this->io->input["get"]["search_etime"] = $search_etime;
	
	$stat_search_query .=  " AND ir.`insertt` BETWEEN '".$search_btime."' 
	AND '".$search_etime."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query="SELECT count(*) as num 
FROM `".$this->config->db[0]['dbname']."`.`".$this->config->default_prefix."user_issues_category` 
WHERE `switch` = 'Y'
";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// echo '<pre>';print_r($page);exit;
// Table Count end 

// Table Record Start
$query ="SELECT *
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_category`
WHERE `switch` = 'Y'
" ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="SELECT *
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_category`  
WHERE`switch` = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["issues_category"] = $recArr['table']['record'];
if (! empty($recArr['table']['record'])){
	foreach($recArr['table']['record'] as $rk => $rv){
		$issues_category[$rv['uicid']] = $rv['name'];
	}
}

//統計起迄
$query ="SELECT ui.`uicid`, count(*) cnt
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_reply` ir
LEFT JOIN `saja_user`.`saja_user_issues` ui
ON ir.`uiid`=ui.`uiid`
WHERE ui.`switch` = 'Y'
AND ui.`status`=2
";
$query .= $stat_search_query;
$query .= "GROUP BY `uicid` ORDER BY `uicid` "; 
$recArr_stat = $this->model->getQueryRecord($query);

if (! empty($recArr_stat['table']['record'])){
	foreach($recArr_stat['table']['record'] as $rk => $rv){
		$table["table"]["rt"]["issues_stat"][$rk] = $rv;
		$table["table"]["rt"]["issues_stat"][$rk]['name'] = $issues_category[$rv['uicid']];
	}
}

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$issues_category_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_category', 
	`active` = '客服分類管理清單查詢', 
	`memo` = '{$issues_category_data}', 
	`root` = 'issues_category/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################
$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];


$this->tplVar('stat_btime' , $search_btime) ;
$this->tplVar('stat_etime' , $search_etime) ;

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();