<?php
if (empty($this->io->input["get"]["uicid"])) {
	$this->jsAlertMsg('分類ID錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
// Table  Start 

// Table Content Start 
$query ="
SELECT *
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_category`
WHERE `uicid` = '".$this->io->input["get"]["uicid"]."'
" ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
$query ="
SELECT uic.*
FROM `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_issues_category` uic 
WHERE uic.switch = 'Y'
";
$recArr = $this->model->getQueryRecord($query);
$table["table"]["rt"]["response_category"] = $recArr['table']['record'];
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$issues_category_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'issues_category_edit', 
	`active` = '客服分類管理修改', 
	`memo` = '{$issues_category_data}', 
	`root` = 'issues_category/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################

$this->tplVar('table' , $table['table']) ;
//$this->tplVar('status',$status["status"]);
$this->display();