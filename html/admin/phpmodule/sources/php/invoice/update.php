<?php
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

error_log("[invoice/update] location_url : ".urldecode(base64_decode($this->io->input['post']['location_url'])));
$siid=$this->io->input["post"]["siid"];
if(empty($siid)) {
   $siid=$this->io->input["get"]["siid"];	
}
if (empty($siid)) {
	$this->jsAlertMsg('發票主鍵ID錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}

$total_amt=$this->io->input["post"]["total_amt"];
if(empty($total_amt)) {
   $total_amt=$this->io->input["get"]["total_amt"];	
}
if($total_amt<=0) {
  $this->jsAlertMsg('發票總金額錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}
if(!is_numeric($total_amt)) {
	$this->jsAlertMsg('發票總金額請填數字!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}


$allowance_amt=$this->io->input["post"]["allowance_amt"];
if(empty($allowance_amt)) {
   $allowance_amt=$this->io->input["get"]["allowance_amt"];	
}

if(!is_numeric($allowance_amt)) {
	$this->jsAlertMsg('已折讓金額請填數字!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}
if($allowance_amt<=0) {
  $this->jsAlertMsg('已折讓金額錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}

if($allowance_amt>$total_amt) {
   $this->jsAlertMsg('已折讓金額不可大於發票總金額!');
   header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
   return;	
}


$upload=$this->io->input["post"]["upload"];
if(empty($upload)) {
   $upload=$this->io->input["get"]["upload"];	
}
/*
if (empty($upload)) {
    $this->jsAlertMsg('申報狀態錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}
*/
$is_void=$this->io->input["post"]["is_void"];
if(empty($is_void)) {
   $is_void=$this->io->input["get"]["is_void"];	
}

/*
if (empty($is_void)) {
    $this->jsAlertMsg('作廢狀態錯誤!!');
	header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));
	return;
}
*/
$is_winprize=$this->io->input["post"]["is_winprize"];
if(empty($is_winprize)) {
   $is_winprize=$this->io->input["get"]["is_winprize"];	
}
/*
if (empty($is_is_winprize)) {
    $is_winprize='N';
}
*/
$switch=$this->io->input["post"]["switch"];
if(empty($switch)) {
   $switch=$this->io->input["get"]["switch"];	
}

$query =" UPDATE `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}invoice` SET modifyt=NOW() ";

           
if(!empty($allowance_amt)) {
   $query.=" ,`allowance_amt` = {$allowance_amt} "; 
}
if(!empty($upload)) {
   $query.=" ,`upload`='{$upload}' "; 
}	
if(!empty($is_void)) {
   $query.=" ,`is_void`='{$is_void}' "; 
}
if(!empty($is_winprize)) {
   $query.=" ,`is_winprize`='{$is_winprize}' "; 
}
if(!empty($switch)) {
   $query.=" ,`switch`='{$switch}' "; 
}

$query.=" WHERE prefixid = '{$this->config->default_prefix_id}'
			AND siid = '{$siid}' " ;
			 
			 
error_log("[invoice/update] query : ".$query);
$this->model->query($query);


##############################################################################################################################################
// Log Start 
$invoice_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'invoice_update', 
	`active` = '發票修改寫入', 
	`memo` = '{$invoice_data}', 
	`root` = 'invoice/update', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));