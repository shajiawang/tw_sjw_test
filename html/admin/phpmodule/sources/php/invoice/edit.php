<?php
if (empty($this->io->input["get"]["siid"])) {
	$this->jsAlertMsg('發票ID錯誤!!');
}
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

##############################################################################################################################################
// Table  Start 
// 取得發票資料

// Table Content Start 
$query ="
SELECT i.*, ip.`seq_no`, ip.`description`, ip.`amount`, ir.`year`, ir.`start_month`, ir.`end_month`, p.nickname, p.addressee, p.rphone, p.address, p.area 
  FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice` i 
LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice_proditems` ip 
     ON i.siid = ip.siid	AND ip.switch = 'Y'
LEFT OUTER JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice_rail` ir 
     ON i.railid = ir.railid 
LEFT OUTER JOIN `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` p 
     ON p.userid=i.userid
WHERE i.prefixid = '".$this->config->default_prefix_id."' 
       AND i.siid = '".$this->io->input["get"]["siid"]."'
     " ;
$table = $this->model->getQueryRecord($query);
// Table Content end 

if ($table["table"]['record']){

	// 一維條碼資料組合
	$tx_code_bar = ($table["table"]['record'][0]['year']-1911).$table["table"]['record'][0]['end_month'].$table["table"]['record'][0]['invoiceno'].$table["table"]['record'][0]['random_number'];

	// 左側二維碼資料組成
	$hex_sales_amt = base_convert(round($table["table"]['record'][0]['sales_amt']),10,16);
	$amount = str_pad($hex_sales_amt,8,'0',STR_PAD_LEFT);						// 銷售額
	
	$hex_total_amt = base_convert(round($table["table"]['record'][0]['total_amt']),10,16);
	$total = str_pad($hex_total_amt,8,'0',STR_PAD_LEFT);						  // 總計額
	
	$InvoiceDateTime = Date("Y-m-d", strtotime($table["table"]['record'][0]['invoice_datetime']));
	$InvoiceYear = substr($InvoiceDateTime,0 ,4); 										                  // 年份
	$InvoiceMonth = substr($InvoiceDateTime,5 ,2);                                     	// 月
	$InvoiceDay = substr($InvoiceDateTime,8 ,2);                                     	 // 日
	// $SellerId = '25089889'; 												               // 賣方統一編號
	// $BuyerId = '00000000';
	$SellerId = $table["table"]['record'][0]['seller_id'];	
	// 買方統一編號
    $BuyerId = $table["table"]['record'][0]['buyer_id'];

	$date = ($InvoiceYear-1911).$InvoiceMonth.$InvoiceDay;

	$key = '70D2ABE2F6EE45B0C474C88969FB08E2';
	$aeskey = getEncrypt(($table["table"]['record'][0]['invoiceno'].$table["table"]['record'][0]['random_number']),$key);

	$tx_code_qrcl0 = $table["table"]['record'][0]['invoiceno']."|".$date."|".$table["table"]['record'][0]['random_number']."|".$amount."|".$total."|".$BuyerId."|".$SellerId."|".$aeskey."|".':**********:1:1:0';

	$tx_code_qrcl = $table["table"]['record'][0]['invoiceno'].$date.$table["table"]['record'][0]['random_number'].$amount.$total.$BuyerId.$SellerId.$aeskey.':**********:1:1:0';

	error_log("[invoice/edit] tx_code_qrcl0 : ".$tx_code_qrcl0);
	error_log("[invoice/edit] tx_code_qrcl : ".$tx_code_qrcl);
	// 右側二維碼資料組成
	$tx_code_qrcr = '**系統使用費:1:'.round($table["table"]['record'][0]['sales_amt']);

	$table["table"]['record'][0]['SellerName'] = '殺價王股份有限公司';
	$table["table"]['record'][0]['year'] = $table["table"]['record'][0]['year']-1911;
	$table["table"]['record'][0]['start_month'] = $table["table"]['record'][0]['start_month'];
	$table["table"]['record'][0]['end_month'] = $table["table"]['record'][0]['end_month'];
	$table["table"]['record'][0]['invoiceno'] = $table["table"]['record'][0]['invoiceno'];
	$table["table"]['record'][0]['date'] = Date("Y-m-d", strtotime($table["table"]['record'][0]['invoice_datetime']));
	$table["table"]['record'][0]['time'] = Date("H:i:s", strtotime($table["table"]['record'][0]['invoice_datetime']));
	$table["table"]['record'][0]['random_code'] = $table["table"]['record'][0]['random_number'];
	$table["table"]['record'][0]['amount'] = round($table["table"]['record'][0]['total_amt']);
	$table["table"]['record'][0]['SellerId'] = $SellerId;

	$table["table"]['record'][0]['tx_code_bar'] = $tx_code_bar;
	$table["table"]['record'][0]['tx_code_qrcl'] = $tx_code_qrcl;
	$table["table"]['record'][0]['tx_code_qrcr'] = $tx_code_qrcr;
	
}

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 
/*
function getEncrypt($sStr, $sKey) {

$iv_size = mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_ECB);
	$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
	return base64_encode(
		mcrypt_encrypt(
			MCRYPT_RIJNDAEL_128,
			$sKey,
			$sStr,
			MCRYPT_MODE_ECB,
			$iv
		)
	);

}
*/

/*
https://www.facebook.com/notes/%E6%A5%8A%E5%B0%8F%E9%9B%84/%E8%B2%A1%E6%94%BF%E9%83%A8%E9%9B%BB%E5%AD%90%E7%99%BC%E7%A5%A8qrcode-aes%E5%8A%A0%E5%AF%86%E6%96%B9%E5%BC%8F-php%E7%89%88/10153229860846149/
*/
function getEncrypt($_str, $_key) {
	     $sKey = hex2bin($_key); 
		 $iv = base64_decode("Dt8lyToo17X/XkXaQvihuA==");
		 $aes_data = $_str; //發票號碼10碼+隨機碼4碼
		 $encrypted = mcrypt_encrypt(MCRYPT_RIJNDAEL_128, $sKey, pkcs5_pad($aes_data, 16), MCRYPT_MODE_CBC, $iv);
		 $aes_data_str = base64_encode($encrypted);
		 return $aes_data_str;
}
function pkcs5_pad($text, $blocksize) {
    $pad = $blocksize - (strlen($text) % $blocksize);
    return $text . str_repeat(chr($pad), $pad);
}
// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$invoice_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'invoice_edit', 
	`active` = '發票修改', 
	`memo` = '{$invoice_data}', 
	`root` = 'invoice/edit', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


$status["status"]["base_href"] = $status["status"]["path"] ."productid={$this->io->input["get"]["productid"]}";

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();