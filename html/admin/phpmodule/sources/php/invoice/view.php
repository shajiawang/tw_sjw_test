<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_cash_flow = $this->config->db[1]['dbname'];
$db_user = $this->config->db[0]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 



// Sort Start
$status["status"]["sort"] = "";
foreach($this->io->input["get"] as $gk => $gv) {
	/** 調整排序欄位請修改下列Modify here to assign sort columns **/
	if (preg_match("/^sort_(switch|railid|name|seq_start||year|start_month|end_month|insertt|modifyt)/", $gk, $matches) && !empty($gv)) {
		$status["status"]["sort"]["sort_".$matches[1]] = $this->io->input["get"]["sort_".$matches[1]] ;
		$status["status"]["sort_path"] .= "&sort_".$matches[1]."=".$this->io->input["get"]["sort_".$matches[1]] ;
	}
}

$sub_sort_query =  " ORDER BY i.insertt DESC";
if(is_array($status["status"]["sort"])){
	$orders = array();
	foreach($status["status"]["sort"] as $sk => $sv){
		if($sv != 'asc' && $sv != 'desc'){
			echo  "Sort Params is wrong!!!"; exit  ;
		}
		$orders[] = " i.`".str_replace('sort_','',$sk)."` ".$sv;
	}
	$sub_sort_query =  " ORDER BY " . implode(',', $orders);
}
// Sort End



// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if(!empty($this->io->input["get"]["search_name"])){
	$status["status"]["search"]["search_name"] = $this->io->input["get"]["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$this->io->input["get"]["search_name"] ;
	$sub_search_query .=  "	AND i.`invoiceno` = '".strtoupper($this->io->input["get"]["search_name"])."' ";
}
if(!empty($this->io->input["get"]["search_userid"])){
	$status["status"]["search"]["search_userid"] = $this->io->input["get"]["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$this->io->input["get"]["search_userid"] ;
	$sub_search_query .=  "	AND i.`userid` = '".$this->io->input["get"]["search_userid"]."' ";
}
if($this->io->input["get"]["search_btime"] != '' && $this->io->input["get"]["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $this->io->input["get"]["search_btime"];
	$status["status"]["search"]["search_etime"] = $this->io->input["get"]["search_etime"];
	$status["status"]["search_path"] .= "&search_btime=".$this->io->input["get"]["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$this->io->input["get"]["search_etime"] ;
	$sub_search_query .=  " AND i.`invoice_datetime` BETWEEN '".$this->io->input["get"]["search_btime"]."' AND '".$this->io->input["get"]["search_etime"]."' ";
}
if(!empty($this->io->input["get"]["search_upload"])){
	$status["status"]["search"]["search_upload"] = $this->io->input["get"]["search_upload"] ;
	$status["status"]["search_path"] .= "&search_upload=".$this->io->input["get"]["search_upload"] ;
	$sub_search_query .=  "	AND i.`upload` = '".$this->io->input["get"]["search_upload"]."' ";
}

if(!empty($this->io->input["get"]["search_is_void"])){
	$status["status"]["search"]["search_is_void"] = $this->io->input["get"]["search_is_void"] ;
	$status["status"]["search_path"] .= "&search_is_void=".$this->io->input["get"]["search_is_void"] ;
	$sub_search_query .=  "	AND i.`is_void` = '".$this->io->input["get"]["search_is_void"]."' ";
}

if(!empty($this->io->input["get"]["search_is_winprize"])){
	$status["status"]["search"]["search_is_winprize"] = $this->io->input["get"]["search_is_winprize"] ;
	$status["status"]["search_path"] .= "&search_is_winprize=".$this->io->input["get"]["search_is_winprize"] ;
	$sub_search_query .=  "	AND i.`is_winprize` = '".$this->io->input["get"]["search_is_winprize"]."' ";
}
// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 
/*
// Table Count Start 
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice` i 
{$category_search_query}
WHERE 
i.prefixid = '".$this->config->default_prefix_id."' 
AND i.switch = 'Y'
AND i.invoiceno IS NOT NULL
" ;
*/
$query ="
SELECT 
count(*) as num
FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice` i 
{$category_search_query}
WHERE 
i.prefixid = '".$this->config->default_prefix_id."' 
AND i.invoiceno IS NOT NULL
" ;

$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 

$num = $this->model->getQueryRecord($query);
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 
/*
// Table Record Start
$query ="
SELECT i.*,dr.name as dr_name,
(select nickname from `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` where userid=i.userid) as nickname
 FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice` i 
 LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh ON 
	i.prefixid = dh.prefixid
	AND i.invoiceno = dh.invoiceno
	AND i.userid = dh.userid
	AND dh.switch = 'Y'
LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri ON 
	dh.prefixid = dri.prefixid 
	AND dh.driid = dri.driid 
	AND dri.switch = 'Y'
LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr ON 
	dri.prefixid = dr.prefixid 
	AND dri.drid = dr.drid 
	AND dr.switch = 'Y'
{$category_search_query}
 WHERE i.prefixid = '".$this->config->default_prefix_id."'
   AND i.switch = 'Y' 
   AND i.invoiceno IS NOT NULL " ;
*/   
$query ="
SELECT i.*,dr.name as dr_name,
(select nickname from `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` where userid=i.userid) as nickname
 FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice` i 
 LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh ON 
	i.prefixid = dh.prefixid
	AND i.invoiceno = dh.invoiceno
	AND i.userid = dh.userid
	AND dh.switch = 'Y'
LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri ON 
	dh.prefixid = dri.prefixid 
	AND dh.driid = dri.driid 
	AND dri.switch = 'Y'
LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr ON 
	dri.prefixid = dr.prefixid 
	AND dri.drid = dr.drid 
	AND dr.switch = 'Y'
{$category_search_query}
 WHERE i.prefixid = '".$this->config->default_prefix_id."'
   AND i.invoiceno IS NOT NULL " ;
   
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$query .= $query_limit ; 
// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################


##############################################################################################################################################
// Relation Start 

// Relation End 
##############################################################################################################################################


##############################################################################################################################################
// Log Start 
$invoice_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'invoice', 
	`active` = '發票清單查詢', 
	`memo` = '{$invoice_data}', 
	`root` = 'invoice/view', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();