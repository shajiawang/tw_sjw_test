<?php
// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 
include_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];

include_once "/var/www/html/admin/libs/helpers.php";
$config = $this->config;
$ip = GetIP();

$userid=$this->io->input["post"]["userid"];
if (empty($userid)) {
	$userid=$this->io->input["get"]["userid"];
}
if(empty($userid)) {
   $this->jsAlertMsg('缺用戶編號 !!');
   return;    
}

$utype=$this->io->input["post"]["utype"];
if (empty($utype)) {
	$utype=$this->io->input["get"]["utype"];
}
if(empty($utype)) {
   $this->jsAlertMsg('請指定發票後續是否可折讓 !!');
   return;    
}

$sales_amt=$this->io->input["post"]["sales_amt"];
if (empty($sales_amt)) {
	$sales_amt=$this->io->input["get"]["sales_amt"];
}
if(empty($sales_amt)) {
   $this->jsAlertMsg('缺銷售金額 !!');
   return;    
}
if($sales_amt<=0){
   $this->jsAlertMsg('銷售金額錯誤 !!');
   return;
}
if(round($sales_amt) != $sales_amt){
   $this->jsAlertMsg('銷售金額不可為小數 !!');
   return;
}


$total_amt=$this->io->input["post"]["total_amt"];
if (empty($total_amt)) {
	$total_amt=$this->io->input["get"]["total_amt"];
}
if(empty($total_amt)) {
   $this->jsAlertMsg('缺發票總金額 !!');
   return;    
}
if($total_amt<=0){
   $this->jsAlertMsg('發票總金額錯誤 !!');
   return;
}

if(round($total_amt) != $total_amt){
   $this->jsAlertMsg('發票總金額不可為小數 !!');
   return;
}

$tax_rate=$this->io->input["post"]["tax_rate"];
if (empty($tax_rate)) {
	$tax_rate=$this->io->input["get"]["tax_rate"];
}
if(empty($tax_rate)) {
   $this->jsAlertMsg('請填寫稅率 !!');
   return;    
}
if($tax_rate<0){
   $this->jsAlertMsg('稅率錯誤 !!');
   return;
}
$tax_rate = round($tax_rate/100.0,2);

$tax_amt=trim($this->io->input["post"]["tax_amt"]);
if (empty($tax_amt)) {
	$tax_amt=trim($this->io->input["get"]["tax_amt"]);
}
if(empty($tax_amt)) {
   $tax_amt=0; 
}
if($tax_amt<0){
   $this->jsAlertMsg('稅額錯誤 !!');
   return;
}
if(round($tax_amt) != $tax_amt){
   $this->jsAlertMsg('稅額不可為小數 !!');
   return;
}

$description1=$this->io->input["post"]["description1"];
if (empty($description1)) {
	$description1=$this->io->input["get"]["description1"];
}
if(empty($description1)) {
   $this->jsAlertMsg('請填寫品項 1 !!');
   return;    
}

$unitprice1=$this->io->input["post"]["unitprice1"];
if (empty($unitprice1)) {
	$unitprice1=$this->io->input["get"]["unitprice1"];
}
if(empty($unitprice1)) {
   $unitprice1 = $sales_amt;
   // $this->jsAlertMsg('請填寫品項 1 的單價 !!');
   return;    
}

$quantity1=$this->io->input["post"]["quantity1"];
if (empty($quantity1)) {
	$quantity1=$this->io->input["get"]["quantity1"];
}
if(empty($quantity1)) {
   $quantity1=1;  
}


$db = new mysql($config->db[0]);
$db->connect();

// 開立發票
$arrPost=array();
$ret = array("retCode"=>0,"retMsg"=>"");

$arrPost['json']='Y';
$arrPost['userid']=$userid;
$arrPost['utype']=$utype;
$arrPost['is_winprize']="N";

// 總金額(含稅)
$arrPost['total_amt']=$total_amt;
// 銷售金額(不含稅)
$arrPost['sales_amt']=$sales_amt;
// 稅率
$arrPost['tax_rate']=$tax_rate;

$arrPost['items']=array();
$arrPost['items'][0]=array();
$arrPost['items'][0]["seq_no"]=1;
$arrPost['items'][0]["quantity"]=$quantity1;
$arrPost['items'][0]["description"]=$description1;
$arrPost['items'][0]["unitprice"]=$unitprice1;   
$arrPost['items'][0]["amount"]=round($unitprice1 * $quantity1);

$ret = postReq(getUrlProtocol()."://".$_SERVER['SERVER_NAME']."/site/invoice/createSalesInvoice/",$arrPost);
error_log("[admin/invoice/insert] ret : ".json_encode($ret));



##############################################################################################################################################
// Log Start 
$invoice_data = json_encode($this->io->input["get"]).json_encode($this->io->input["post"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'invoice_insert', 
	`active` = '發票新增寫入', 
	`memo` = '{$invoice_data}', 
	`root` = 'invoice/insert', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
"; 
$this->model->query($query);
// Log End 
##############################################################################################################################################


header("location:".urldecode(base64_decode($this->io->input['post']['location_url'])));



  
  