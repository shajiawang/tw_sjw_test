<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];
require_once '/var/www/html/admin/libs/helpers.php';
$ip = GetIP();

/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
$db_user = $this->config->db[0]['dbname'];


$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);
if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}
$sub_sort_query =  " ORDER BY i.siid ASC";

// Search Start
$status["status"]["search"] = "";
$sub_search_query = "";
$category_search_query = "";
if(!empty($data["search_name"])){
	$status["status"]["search"]["search_name"] = $data["search_name"] ;
	$status["status"]["search_path"] .= "&search_name=".$data["search_name"] ;
	$sub_search_query .=  "	AND i.`invoiceno` = '".strtoupper($data["search_name"])."' ";
}
if(!empty($data["search_userid"])){
	$status["status"]["search"]["search_userid"] = $data["search_userid"] ;
	$status["status"]["search_path"] .= "&search_userid=".$data["search_userid"] ;
	$sub_search_query .=  "	AND i.`userid` = '".$data["search_userid"]."' ";
}
if($data["search_btime"] != '' && $data["search_etime"] != ''){
	$status["status"]["search"]["search_btime"] = $data["search_btime"];
	$status["status"]["search"]["search_etime"] = $data["search_etime"];
	$status["status"]["search_path"] .= "&search_btime=".$data["search_btime"] ;
	$status["status"]["search_path"] .= "&search_etime=".$data["search_etime"] ;
	$sub_search_query .=  " AND i.`invoice_datetime` BETWEEN '".$data["search_btime"]."' AND '".$data["search_etime"]."' ";
}
if(!empty($data["search_upload"])){
	$status["status"]["search"]["search_upload"] = $data["search_upload"] ;
	$status["status"]["search_path"] .= "&search_upload=".$data["search_upload"] ;
	$sub_search_query .=  "	AND i.`upload` = '".$data["search_upload"]."' ";
}

// Search End

// Table Record Start
$query ="
SELECT i.*,dr.name as dr_name, dh.data,
(select nickname from `".$this->config->db[0]["dbname"]."`.`".$this->config->default_prefix."user_profile` where userid=i.userid) as nickname
 FROM `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."invoice` i 
 LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_history` dh ON 
	i.prefixid = dh.prefixid
	AND i.invoiceno = dh.invoiceno
	AND i.userid = dh.userid
	AND dh.switch = 'Y'
LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule_item` dri ON 
	dh.prefixid = dri.prefixid 
	AND dh.driid = dri.driid 
LEFT JOIN `".$this->config->db[1]["dbname"]."`.`".$this->config->default_prefix."deposit_rule` dr ON 
	dri.prefixid = dr.prefixid 
	AND dri.drid = dr.drid 
{$category_search_query}
 WHERE i.prefixid = '".$this->config->default_prefix_id."'
   AND i.switch = 'Y' 
   AND i.invoiceno IS NOT NULL " ;
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
// echo '<pre>'.$query;exit;
$table = $this->model->getQueryRecord($query);
// Table Record End 



##############################################################################################################################################
// Log Start 
$invoice_data = json_encode($this->io->input["get"]);

$query = "
INSERT INTO `{$db_user}`.`{$this->config->default_prefix}admin_log` SET 
	`name` = '{$this->io->input['session']['user']["name"]}', 
	`userid` = '{$this->io->input['session']['user']["userid"]}',
	`kind` = 'invoice', 
	`active` = '發票清單匯出', 
	`memo` = '{$invoice_data}', 
	`root` = 'invoice/export', 
	`ipaddress`='{$ip}', 
	`atime`=now(), 
	`insertt`=now()
";
$this->model->query($query);
// Log End 
##############################################################################################################################################


// Create new PHPExcel object
$objPHPExcel = new PHPExcel();
// Add some data
//print_r($_POST['report']);exit;
//會員資料表
$Report_Array[0]['title'] = array("A1" => "發票號碼", "B1" => "發票時間", "C1" => "會員編號", "D1" => "會員暱稱", "E1" => "銷售金額", "F1" => "稅率", 
							"G1" => "營業稅", "H1" => "總金額", "I1" => "已折讓金額", "J1" => "發票防偽隨機碼", "K1" => "申報狀態",
							"L1" => "儲值方式", "M1" => "新增日期", "N1" => "修改日期","O1"=>"是否作廢");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		if ($value['dr_name'] == ''){
			$data = json_decode($value['data'],true);
			$query = "SELECT name
			FROM `{$this->config->db[1]["dbname"]}`.`{$this->config->default_prefix}deposit_rule` dr 
			WHERE 
				dr.`prefixid` = '{$this->config->default_prefix_id}' 
				AND dr.drid = '{$data['drid']}' 
				AND dr.`switch`='Y' 
			" ;
			$rulename = $this->model->getQueryRecord($query);		
			$value['dr_name'] = $rulename["table"]['record'][0]['name'];
		}
	
		if($value['upload'] == 'N'){
			$upload_type = '尚未申報';
		}else if($value['upload'] == 'Y'){
			$upload_type = '已申報完成';
		}else if($value['upload'] == 'P'){
			$upload_type = '申報中';
		}else{
			$upload_type = '尚未申報';
		}
		$row = $key + 2;
		if (!empty($value['dr_name'])){
			$dr_name = $value['dr_name'];
		}else{
			$dr_name = '其它';
		}
        $objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['invoiceno']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['invoice_datetime']);
        $objPHPExcel->getActiveSheet()->setCellValue('C'.$row, $value['userid']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('D'.$row, (string)$value['nickname'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['sales_amt']);
        $objPHPExcel->getActiveSheet()->setCellValue('F'.$row, $value['tax_rate']);
        $objPHPExcel->getActiveSheet()->setCellValue('G'.$row, $value['tax_amt']);
        $objPHPExcel->getActiveSheet()->setCellValue('H'.$row, $value['total_amt']);
        $objPHPExcel->getActiveSheet()->setCellValue('I'.$row, $value['allowance_amt']);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$row, $value['random_number']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('K'.$row, (string)$upload_type,PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('L'.$row, $dr_name);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$row, $value['insertt']);
        $objPHPExcel->getActiveSheet()->setCellValue('N'.$row, $value['modifyt']);
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$row, $value['is_void']);
	}
}
// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('發票資料管理');

/*
$objPHPExcel->createSheet();
$objPHPExcel->setActiveSheetIndex(1);
$objPHPExcel->getActiveSheet()->setCellValue("A3","test11"); 
$objPHPExcel->getActiveSheet()->setCellValue("B3","test22"); 
$objPHPExcel->getActiveSheet()->setCellValue("C3","test33"); 
$objPHPExcel->getActiveSheet()->setCellValue("D3","測試");

// Rename sheet
//echo date('H:i:s') . " Rename sheet\n";
$objPHPExcel->getActiveSheet()->setTitle('Simple2');
// Set active sheet index to the first sheet, so Excel opens this as the first sheet
//$objPHPExcel->setActiveSheetIndex(0);
*/
header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="發票資料_'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
	
// Save Excel 2007 file
//echo date('H:i:s') . " Write to Excel2007 format\n";
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//$objWriter->save(str_replace('.php', '.xlsx', __FILE__));
$objWriter->save('php://output');
// Echo memory peak usage
//echo date('H:i:s') . " Peak memory usage: " . (memory_get_peak_usage(true) / 1024 / 1024) . " MB\r\n";

// Echo done
//echo date('H:i:s') . " Done writing file.\r\n";
?>