<?php
if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();
//$db_user = $this->config->db[2]['dbname'];

##############################################################################################################################################
#// Status Start 

// Path Start 
$status["status"]["path"] = $this->config->default_main;
if($this->io->input["get"]["fun"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["fun"] ;
}
if($this->io->input["get"]["act"] != ''){
	$status["status"]["path"] .= "/".$this->io->input["get"]["act"] ;
}
$status["status"]["path"] .= "/";
// Path End 

// Sort Start
$sub_sort_query =  " ORDER BY a.asid DESC";
// Sort End

// Search Start
$status["status"]["search"] = "";
$status["status"]["search_path"] = "";
$sub_search_query = "";
if(!empty($this->io->input["get"]["search_phone"])){
	$status["status"]["search"]["search_phone"] = $this->io->input["get"]["search_phone"] ;
	$status["status"]["search_path"] .= "&search_phone=".$this->io->input["get"]["search_phone"] ;
	$sub_search_query .=  "	AND a.`phone` like '%".$this->io->input["get"]["search_phone"]."%' ";
}
if($this->io->input["get"]["search_source"] != ''){
	$status["status"]["search"]["search_source"] = $this->io->input["get"]["search_source"] ;
	$status["status"]["search_path"] .= "&search_source=".$this->io->input["get"]["search_source"] ;
	$sub_search_query .=  " AND a.`source` ='".$this->io->input["get"]["search_source"]."' ";
}
if(!empty($this->io->input["get"]["joindatefrom"])){			//加入起始日期
	$status["status"]["search"]["joindatefrom"] = $this->io->input["get"]["joindatefrom"] ;
	$status["status"]["search_path"] .= "&joindatefrom=".$this->io->input["get"]["joindatefrom"] ;
	$sub_search_query .=  "AND a.`insertt` >= '".$this->io->input["get"]["joindatefrom"]."'";
}
if(!empty($this->io->input["get"]["joindateto"])){				//加入結束日期
	$status["status"]["search"]["joindateto"] = $this->io->input["get"]["joindateto"] ;
	$status["status"]["search_path"] .= "&joindateto=".$this->io->input["get"]["joindateto"] ;
	$sub_search_query .=  "AND a.`insertt` < '".$this->io->input["get"]["joindateto"]."'";
}

// Search End

// Page Start
if($this->io->input["get"]["p"] != ''){
	$status["status"]["search"]["p"] = $this->io->input["get"]["p"] ;
	//$status["status"]["path"] .= "&p=".$this->io->input["get"]["p"] ;
}
// Page End


##############################################################################################################################################
// Table  Start 

// Table Count Start 
$query = " 
SELECT count(*) as num 
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."activity_statistics` a 
WHERE a.switch = 'Y' ";
$query .= $sub_search_query ; 
$query .= $sub_sort_query ; 
$num = $this->model->getQueryRecord($query); 
$page = $this->model->recordPage($num['table']['record'][0]['num'], $this);//打入了max_page和
$query_limit = " limit ".($page["rec_start"]-1).",".($this->config->max_page);
$this->tplVar('page' , $page) ;
// Table Count end 

// Table Record Start
$query = " 
SELECT *  
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."activity_statistics` a 
WHERE a.switch = 'Y' ";
$query .= $sub_search_query; 
$query .= $sub_sort_query; 
$query .= $query_limit; 
$table = $this->model->getQueryRecord($query);
// Table Record End 

// Table End 
##############################################################################################################################################



##############################################################################################################################################
// Relation Start
 
// Relation End 
##############################################################################################################################################

$status["status"]["base_href"] = $status["status"]["path"].$status["status"]["search_path"].$status["status"]["sort_path"].$status["status"]["p"];
$status["status"]["switch"] = array("N" => "否", "Y" => "是");
$status["status"]["source"] = array("1" => "banner", "2" => "sms", "3" => "line", "4" => "fb");

$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
$this->display();