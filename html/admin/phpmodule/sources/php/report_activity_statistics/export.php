<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2009 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2009 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.7.0, 2009-08-10
 */

if (empty($this->io->input['session']['user'])) {
	$this->jsPrintMsg('請先登入管理者帳號!!', $this->config->default_main."/admin_user/login");
	die();
} 
$userid = $this->io->input['session']['user']["userid"];


/** Error reporting */
error_reporting(E_ALL);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';

/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();

$location_url = urldecode(base64_decode($this->io->input['get']['location_url']));
$locationArr = explode("&", $location_url);
unset($locationArr[0]);
$locationArr = array_values($locationArr);

if (is_array($locationArr)) {
	foreach ($locationArr as $lk => $lv) {
		$dataArr = explode("=", $locationArr[$lk]);
		$data[$dataArr[0]] = $dataArr[1]; 
	}
}

//搜尋條件
$query_search = '';
if(!empty($data["joindatefrom"])){			//加入起始日期
	$sub_search_query .=  "AND a.`insertt` >= '".$data["joindatefrom"]."'";
}
if(!empty($data["joindateto"])){			//加入結束日期
	$sub_search_query .=  "AND a.`insertt` < '".$data["joindateto"]."'";
}
if(!empty($data["search_phone"])){			//手機號
	$sub_search_query .=  "
		AND a.`phone` like '%".$data["search_phone"]."%'";
}
if(!empty($data["search_source"])){			//從哪加入		
	$sub_search_query .=  "
		AND a.`source` = '".$data["search_source"]."'";
}


$query = "
select *
FROM `".$this->config->db[2]["dbname"]."`.`".$this->config->default_prefix."activity_statistics` a 
where 1 = 1
";
$query .= $sub_search_query;
$table = $this->model->getQueryRecord($query);

/** PHPExcel */
require_once '/var/www/html/admin/libs/PHPExcel.php';
/** PHPExcel_IOFactory */
require_once '/var/www/html/admin/libs/PHPExcel/IOFactory.php';
// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

//會員資料表
$Report_Array[0]['title'] = array("A1" => "流水編號", "B1" => "來源方式", "C1" => "手機號", "D1" => "使用狀態", "E1" => "新增時間");

//Title
if (is_array($Report_Array[0]['title'])) {
	$objPHPExcel->createSheet();
	$objPHPExcel->setActiveSheetIndex(0);
	foreach ($Report_Array[0]['title'] as $key => $value) {
		$objPHPExcel->getActiveSheet()->setCellValue($key,$value); 
	}
}

if (is_array($table['table']['record'])) {
	foreach ($table['table']['record'] as $key => $value)
	{
		$row = $key + 2;
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$row, $value['asid']);
        $objPHPExcel->getActiveSheet()->setCellValue('B'.$row, $value['source']);
        $objPHPExcel->getActiveSheet()->setCellValueExplicit('C'.$row, (string)$value['phone'],PHPExcel_Cell_DataType::TYPE_STRING);
        $objPHPExcel->getActiveSheet()->setCellValue('D'.$row, $value['switch']);
        $objPHPExcel->getActiveSheet()->setCellValue('E'.$row, $value['insertt']);
	}
}
// Rename sheet
$objPHPExcel->getActiveSheet()->setTitle('活動來源統計');

header('Content-Type: application/vnc.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="活動來源統計'.date("Ymd").'.xlsx"');
header('Cache-Control:max-age=0');
		
// Save Excel 2007 file
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>