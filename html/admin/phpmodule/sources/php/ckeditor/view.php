<?php
//ckeditor 圖檔管理

// Text
$heading_title        = '圖檔管理';
$text_uploaded        = '成功：您的檔案已上傳!';
$text_file_delete     = '檔案已被刪除!';
$text_create          = '成功：新建目錄!';
$text_delete          = '成功：檔案或目錄已被刪除!';
$text_move            = '成功：檔案或目錄已被移動!';
$text_copy            = '成功：檔案或目錄已被複製!';
$text_rename          = '成功：檔案或目錄已被重命名!';
$entry_folder         = '新建檔案夾:';
$entry_delete         = '確認 刪除 檔案或目錄 !';
$entry_move           = '移動檔案:';
$entry_copy           = '複製名稱:';
$entry_rename         = '重命名名稱:';
$error_select         = '警告：請選擇一個目錄或檔案!';
$error_file           = '警告：請選擇一個檔案!';
$error_directory      = '警告：請選擇一個目錄!';
$error_default        = '警告：不能更改預設目錄!';
$error_delete         = '警告：您不能刪除此目錄!';
$error_filename       = '警告：檔案名必須在3至255個字元之間!';
$error_missing        = '警告：檔案或目錄不存在!';
$error_exists         = '警告：檔案名或目錄名已存在!';
$error_name           = '警告：請輸入一個新名稱!';
$error_move           = '警告：移動至目錄不存在!';
$error_copy           = '警告：無法複製這個檔案或目錄!';
$error_rename         = '警告：不能重命名此目錄!';
$error_file_type      = '警告：不正確的檔案類型!';
$error_file_size      = '警告：圖片檔案太大，檔案尺寸不得大於 2000KB，且檔案像素不得大於2000px X 2000px!';
$error_uploaded       = '警告：原因不明，檔案不能上傳!';
$error_permission     = '警告：權限被拒絕!';
$button_folder        = '新建檔案夾';
$button_delete        = '刪除';
$button_move          = '移動';
$button_copy          = '複製';
$button_rename        = '重命名';
$button_upload        = '上傳';
$button_refresh       = '刷新';

$error = array();

require_once "saja/mysql.ini.php";
$this->model = new mysql($this->config->db[0]);
$this->model->connect();	


$token = ''; //$_SESSION['token'];
$directory = HTTP_IMAGE . 'data/';

if (isset($this->io->input["get"]['field'])) {
	$field = $this->io->input["get"]['field'];
} else {
	$field = '';
}

if (isset($this->io->input["get"]['CKEditorFuncNum'])) {
	$fckeditor = TRUE;
	$CKEditorFuncNum = $this->io->input["get"]['CKEditorFuncNum'];
} else {
	$fckeditor = FALSE;
}

$action = isset($this->io->input["get"]['action']) ? $this->io->input["get"]['action'] : '';
	
$this->tplVar('table' , $table['table']) ;
$this->tplVar('status',$status["status"]);
//echo '<pre>';print_r($status);die();
$this->display();

	
function image() {
	$this->model_tool_image = new ToolImage();
	
	$resize = $this->model_tool_image->resize($this->io->input["post"]['image'], 100, 100)
	
	if (isset($this->io->input["post"]['image'])) {
		return $resize;
	}
}
	
function directory() 
{	
	$json = array();
	
	if (isset($this->io->input["post"]['directory'])) 
	{
		$directories = glob(rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['directory']), '/') . '/*', GLOB_ONLYDIR); 
		
		if ($directories) 
		{
			$i = 0;
		
			foreach ($directories as $directory) {
				$json[$i]['data'] = basename($directory);
				$json[$i]['attributes']['directory'] = substr($directory, strlen(DIR_IMAGE . 'data/'));
				
				$children = glob(rtrim($directory, '/') . '/*', GLOB_ONLYDIR);
				
				if ($children)  {
					$json[$i]['children'] = ' ';
				}
				
				$i++;
			}
		}		
	}

	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));		
}
	
function files() 
{
	$json = array();
	$this->model_tool_image = new ToolImage();
	
	if (isset($this->io->input["post"]['directory']) && $this->io->input["post"]['directory']) {
		$directory = DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['directory']);
	} else {
		$directory = DIR_IMAGE . 'data/';
	}
	
	$allowed = array(
		'.jpg', '.jpeg', '.png', '.gif'
	);
	
	$files = array();
	$files = glob(rtrim($directory, '/') . '/*');
	
	if ($files && is_array($files) )
	foreach ($files as $file) 
	{
		if (is_file($file)) {
			$ext = strrchr($file, '.');
		} else {
			$ext = '';
		}	
		
		if (in_array(strtolower($ext), $allowed)) 
		{
			$size = filesize($file);
			$i = 0;

			$suffix = array(
				'B', 'KB', 'MB', 'GB',
			);

			while (($size / 1024) > 1) {
				$size = $size / 1024;
				$i++;
			}
				
			$json[] = array(
				'file'     => substr($file, strlen(DIR_IMAGE . 'data/')),
				'filename' => basename($file),
				'size'     => round(substr($size, 0, strpos($size, '.') + 4), 2) . $suffix[$i],
				'thumb'    => $this->model_tool_image->resize(substr($file, strlen(DIR_IMAGE)), 100, 100)
			);
		}
	}
	
	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));	
}	
	
function create() 
{
	$json = array();
	
	if (isset($this->io->input["post"]['directory'])) 
	{
		if (isset($this->io->input["post"]['name']) || $this->io->input["post"]['name']) {
			$directory = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['directory']), '/');							   
			
			if (!is_dir($directory)) {
				$json['error'] = '警告：請選擇一個目錄!';
			}
			
			if (file_exists($directory . '/' . str_replace('../', '', $this->io->input["post"]['name']))) {
				$json['error'] = '警告：檔案名或目錄名已存在!';
			}
		} else {
			$json['error'] = '警告：請輸入一個新名稱!';
		}
	} else {
		$json['error'] = '警告：請選擇一個目錄!';
	}
	
	if (!isset($json['error'])) {	
		mkdir($directory . '/' . str_replace('../', '', $this->io->input["post"]['name']), 0777);
		$json['success'] = '成功：新建目錄!';
	}	
	
	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));
}
	
function delete() 
{
	$json = array();
	
	if (isset($this->io->input["post"]['path'])) 
	{
		$path = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['path']), '/');
		 
		if (!file_exists($path)) {
			$json['error'] = '警告：請選擇一個目錄或檔案!';
		}
		
		if ($path == rtrim(DIR_IMAGE . 'data/', '/')) {
			$json['error'] = '警告：您不能刪除此目錄!';
		}
	} else {
		$json['error'] = '警告：請選擇一個目錄或檔案!';
	}
	
	if (!isset($json['error'])) 
	{
		if (is_file($path)) {
			unlink($path);
		} elseif (is_dir($path)) {
			$recursiveDelete($path);
		}
		
		$json['success'] = '成功：檔案或目錄已被刪除!';
	}				
	
	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));
}

function recursiveDelete($directory) 
{
	if (is_dir($directory)) {
		$handle = opendir($directory);
	}
	
	if (!$handle) {
		return FALSE;
	}
	
	while (false !== ($file = readdir($handle))) 
	{
		if ($file != '.' && $file != '..') {
			if (!is_dir($directory . '/' . $file)) {
				unlink($directory . '/' . $file);
			} else {
				recursiveDelete($directory . '/' . $file);
			}
		}
	}
	
	closedir($handle);
	rmdir($directory);
	return TRUE;
}

function move() 
{
	$json = array();
	
	if (isset($this->io->input["post"]['from']) && isset($this->io->input["post"]['to'])) 
	{
		$from = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['from']), '/');
		
		if (!file_exists($from)) {
			$json['error'] = '警告：檔案或目錄不存在!';
		}
		
		if ($from == DIR_IMAGE . 'data') {
			$json['error'] = '警告：不能更改預設目錄!';
		}
		
		$to = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['to']), '/');

		if (!file_exists($to)) {
			$json['error'] = '警告：移動至目錄不存在!';
		}	
		
		if (file_exists($to . '/' . basename($from))) {
			$json['error'] = '警告：檔案名或目錄名已存在!';
		}
	} else {
		$json['error'] = '警告：請選擇一個目錄!';
	}
	
	if (!isset($json['error'])) {
		rename($from, $to . '/' . basename($from));
		$json['success'] = '成功：檔案或目錄已被移動!';
	}
	
	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));
}	
	
function copy() 
{
	$json = array();
	
	if (isset($this->io->input["post"]['path']) && isset($this->io->input["post"]['name'])) 
	{
		if ((strlen(utf8_decode($this->io->input["post"]['name'])) < 3) || (strlen(utf8_decode($this->io->input["post"]['name'])) > 255)) {
			$json['error'] = '警告：檔案名必須在3至255個字元之間!';
		}
			
		$old_name = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['path']), '/');
		
		if (!file_exists($old_name) || $old_name == DIR_IMAGE . 'data') {
			$json['error'] = '警告：無法複製這個檔案或目錄!';
		}
		
		if (is_file($old_name)) {
			$ext = strrchr($old_name, '.');
		} else {
			$ext = '';
		}		
		
		$new_name = dirname($old_name) . '/' . str_replace('../', '', $this->io->input["post"]['name'] . $ext);
																		   
		if (file_exists($new_name)) {
			$json['error'] = '警告：檔案名或目錄名已存在!';
		}			
	} else {
		$json['error'] = '警告：請選擇一個目錄或檔案!';
	}
	
	if (!isset($json['error'])) 
	{
		if (is_file($old_name)) {
			copy($old_name, $new_name);
		} else {
			$recursiveCopy($old_name, $new_name);
		}
		
		$json['success'] = '成功：檔案或目錄已被複製!';
	}
	
	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));	
}

function recursiveCopy($source, $destination) 
{ 
	$directory = opendir($source); 
	@mkdir($destination); 
	
	while (false !== ($file = readdir($handle))) 
	{
		if (($file != '.') && ($file != '..')) { 
			if (is_dir($source . '/' . $file)) { 
				recursiveCopy($source . '/' . $file, $destination . '/' . $file); 
			} else { 
				copy($source . '/' . $file, $destination . '/' . $file); 
			} 
		} 
	} 
	
	closedir($directory); 
} 

function folders() {
	return recursiveFolders(DIR_IMAGE . 'data/');	
}
	
function recursiveFolders($directory) 
{
	$output = '';
	$output .= '<option value="' . substr($directory, strlen(DIR_IMAGE . 'data/')) . '">' . substr($directory, strlen(DIR_IMAGE . 'data/')) . '</option>';
	
	$directories = glob(rtrim(str_replace('../', '', $directory), '/') . '/*', GLOB_ONLYDIR);
	
	if ($directories && is_array($directories) )
	foreach ($directories  as $directory) {
		$output .= recursiveFolders($directory);
	}
	
	return $output;
}
	
function rename() 
{
	$json = array();
	
	if (isset($this->io->input["post"]['path']) && isset($this->io->input["post"]['name'])) 
	{
		if ((strlen(utf8_decode($this->io->input["post"]['name'])) < 3) || (strlen(utf8_decode($this->io->input["post"]['name'])) > 255)) {
			$json['error'] = '警告：檔案名必須在3至255個字元之間!';
		}
			
		$old_name = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['path']), '/');
		
		if (!file_exists($old_name) || $old_name == DIR_IMAGE . 'data') {
			$json['error'] = $error_rename;
		}
		
		if (is_file($old_name)) {
			$ext = strrchr($old_name, '.');
		} else {
			$ext = '';
		}		
		
		$new_name = dirname($old_name) . '/img' . str_replace('../', '', $this->io->input["post"]['name'] . $ext);
																		   
		if (file_exists($new_name)) {
			$json['error'] = '警告：檔案名或目錄名已存在!';
		}			
	}
	
	if (!isset($json['error'])) {
		rename($old_name, $new_name);
		$json['success'] = '成功：檔案或目錄已被重命名!';
	}
	
	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));
}
	
function upload() 
{
	$json = array();
	
	if (isset($this->io->input["post"]['directory'])) 
	{
		if (isset($this->io->input["files"]['image']) && $this->io->input["files"]['image']['tmp_name']) {
			if ((strlen(utf8_decode($this->io->input["files"]['image']['name'])) < 3) || (strlen(utf8_decode($this->io->input["files"]['image']['name'])) > 255)) {
				$json['error'] = '警告：檔案名必須在3至255個字元之間!';
			}
				
			$directory = rtrim(DIR_IMAGE . 'data/' . str_replace('../', '', $this->io->input["post"]['directory']), '/');
			
			if (!is_dir($directory)) {
				$json['error'] = '警告：請選擇一個目錄!';
			}
			
			if ($this->io->input["files"]['image']['size'] > 2048000) { //300000
				$json['error'] = '警告：圖片檔案太大，檔案尺寸不得大於 2000KB，且檔案像素不得大於2000px X 2000px!';
			}
			
			$allowed = array(
				'image/jpg',
				'image/jpeg',
				'image/pjpeg',
				'image/png',
				'image/x-png',
				'image/gif',
				'application/x-shockwave-flash'
			);
					
			if (!in_array($this->io->input["files"]['image']['type'], $allowed)) {
				$json['error'] = '警告：不正確的檔案類型!';
			}
			
			$allowed = array(
				'.jpg', '.jpeg', '.gif', '.png', '.flv', '.swf'
			);
					
			if (!in_array(strtolower(strrchr($this->io->input["files"]['image']['name'], '.')), $allowed)) {
				$json['error'] = '警告：不正確的檔案類型!';
			}
			
			if ($this->io->input["files"]['image']['error'] != UPLOAD_ERR_OK) {
				$json['error'] = 'error_upload_' . $this->io->input["files"]['image']['error'];
			}			
		} else {
			$json['error'] = '警告：請選擇一個檔案!';
		}
	} else {
		$json['error'] = '警告：請選擇一個目錄!';
	}
	
	if (!isset($json['error'])) {	
		if (@move_uploaded_file($this->io->input["files"]['image']['tmp_name'], $directory . '/img' . basename($this->io->input["files"]['image']['name']) ) ) {		
			$json['success'] = '成功：您的檔案已上傳!';
			$json['myfilename'] = $this->io->input["files"]['image']['name']; //===wommon 2011/06/28
		} else {
			$json['error'] = '警告：原因不明，檔案不能上傳!';
		}
	}
	
	//$this->load->library('json');
	//$this->response->setOutput(Json::encode($json));
}

class ToolImage 
{
	public function resize($filename, $width, $height) 
	{
		if (!file_exists(DIR_IMAGE . $filename) || !is_file(DIR_IMAGE . $filename)) {
			return;
		} 
		
		$info = pathinfo($filename);
		$extension = $info['extension'];
		
		$old_image = $filename;
		$new_image = 'cache/' . substr($filename, 0, strrpos($filename, '.')) . '-' . $width . 'x' . $height . '.' . $extension;
		
		if (!file_exists(DIR_IMAGE . $new_image) || (filemtime(DIR_IMAGE . $old_image) > filemtime(DIR_IMAGE . $new_image))) 
		{
			$path = '';
			
			$directories = explode('/', dirname(str_replace('../', '', $new_image)));
			
			foreach ($directories as $directory) {
				$path = $path . '/' . $directory;
				
				if (!file_exists(DIR_IMAGE . $path)) {
					@mkdir(DIR_IMAGE . $path, 0777);
				}		
			}
			
			$image = new Image(DIR_IMAGE . $old_image);
			$image->resize($width, $height);
			$image->save(DIR_IMAGE . $new_image);
		}
	
		return HTTP_IMAGE . $new_image;
	}
}

class Image 
{
    private $file;
    private $image;
    private $info;
	public $bgcolor_RGB = array('r'=>'255','g'=>'255','b'=>'255');
		
	public function __construct($file) 
	{
		if (file_exists($file)) {
			$this->file = $file;

			$info = getimagesize($file);

			$this->info = array(
            	'width'  => $info[0],
            	'height' => $info[1],
            	'bits'   => $info['bits'],
            	'mime'   => $info['mime']
        	);
        	
        	$this->image = $this->create($file);
    	} else {
      		exit('Error: Could not load image ' . $file . '!');
    	}
	}
		
	private function create($image) 
	{
		$mime = $this->info['mime'];
		
		if ($mime == 'image/gif') {
			return imagecreatefromgif($image);
		} elseif ($mime == 'image/png') {
			return imagecreatefrompng($image);
		} elseif ($mime == 'image/jpeg') {
			return imagecreatefromjpeg($image);
		}
    }	
	
	public function save($file, $quality = 100) 
	{
		$info = pathinfo($file);
		$extension = $info['extension'];
		
		if (strcmp($extension,'jpeg') || strcmp($extension,'jpg')) {
			imagejpeg($this->image, $file, $quality);
		} elseif(strcmp($extension,'png')) {
			imagepng($this->image, $file, 0);
		} elseif(strcmp($extension,'gif')) {
			imagegif($this->image, $file);
		}
		imagedestroy($this->image);
	}
	
    public function resize($width = 0, $height = 0) 
	{
    	if (!$this->info['width'] || !$this->info['height']) {
			return;
		}

		$xpos = 0;
		$ypos = 0;

		$scale = min($width / $this->info['width'], $height / $this->info['height']);
		
		if ($scale == 1) {
			return;
		}
		
		$new_width = (int)($this->info['width'] * $scale);
		$new_height = (int)($this->info['height'] * $scale);			
    	$xpos = (int)(($width - $new_width) / 2);
   		$ypos = (int)(($height - $new_height) / 2);
        		        
       	$image_old = $this->image;
        $this->image = imagecreatetruecolor($width, $height);
		
		if (isset($this->info['mime']) && $this->info['mime'] == 'image/png') 
		{		
			imagealphablending($this->image, false);
			imagesavealpha($this->image, true);
			$background = imagecolorallocatealpha($this->image, $this->bgcolor_RGB['r'], $this->bgcolor_RGB['g'], $this->bgcolor_RGB['b'], 127);
			
			imagecolortransparent($this->image, $background);
		} else {
			$background = ImageColorAllocate($this->image, $this->bgcolor_RGB['r'], $this->bgcolor_RGB['g'], $this->bgcolor_RGB['b']); //imagecolorallocate($this->image, 255, 255, 255);
		}
		
		imagefilledrectangle($this->image, 0, 0, $width, $height, $background);
	
        imagecopyresampled($this->image, $image_old, $xpos, $ypos, 0, 0, $new_width, $new_height, $this->info['width'], $this->info['height']);
        imagedestroy($image_old);
           
        $this->info['width']  = $width;
        $this->info['height'] = $height;
    }
    
    public function watermark($file, $position = 'bottomright') 
	{
        $watermark = $this->create($file);
        
        $watermark_width = imagesx($watermark);
        $watermark_height = imagesy($watermark);
        
        switch($position) {
            case 'topleft':
                $watermark_pos_x = 0;
                $watermark_pos_y = 0;
                break;
            case 'topright':
                $watermark_pos_x = $this->info['width'] - $watermark_width;
                $watermark_pos_y = 0;
                break;
            case 'bottomleft':
                $watermark_pos_x = 0;
                $watermark_pos_y = $this->info['height'] - $watermark_height;
                break;
            case 'bottomright':
                $watermark_pos_x = $this->info['width'] - $watermark_width;
                $watermark_pos_y = $this->info['height'] - $watermark_height;
                break;
        }
        
        imagecopy($this->image, $watermark, $watermark_pos_x, $watermark_pos_y, 0, 0, 120, 40);
        
        imagedestroy($watermark);
    }
    
    public function crop($top_x, $top_y, $bottom_x, $bottom_y) 
	{
        $image_old = $this->image;
        $this->image = imagecreatetruecolor($bottom_x - $top_x, $bottom_y - $top_y);
        
        imagecopy($this->image, $image_old, 0, 0, $top_x, $top_y, $this->info['width'], $this->info['height']);
        imagedestroy($image_old);
        
        $this->info['width'] = $bottom_x - $top_x;
        $this->info['height'] = $bottom_y - $top_y;
    }
    
    public function rotate($degree, $color = 'FFFFFF') 
	{
		$rgb = $this->html2rgb($color);
		$background = ImageColorAllocate($this->image, $rgb[0], $rgb[1], $rgb[2]);
		
        $this->image = imagerotate($this->image, $degree, $background);
        
		$this->info['width'] = imagesx($this->image);
		$this->info['height'] = imagesy($this->image);
    }
	    
    private function filter($filter) 
	{
        imagefilter($this->image, $filter);
    }
            
    private function text($text, $x = 0, $y = 0, $size = 5, $color = '000000') 
	{
		$rgb = $this->html2rgb($color);
		$background = ImageColorAllocate($this->image, $rgb[0], $rgb[1], $rgb[2]);
        
		imagestring($this->image, $size, $x, $y, $text, $background);
    }
    
    private function merge($file, $x = 0, $y = 0, $opacity = 100) 
	{
        $merge = $this->create($file);

        $merge_width = imagesx($image);
        $merge_height = imagesy($image);
		        
        imagecopymerge($this->image, $merge, $x, $y, 0, 0, $merge_width, $merge_height, $opacity);
    }
			
	private function html2rgb($color) 
	{
		if ($color[0] == '#') {
			$color = substr($color, 1);
		}
		
		if (strlen($color) == 6) {
			list($r, $g, $b) = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);   
		} elseif (strlen($color) == 3) {
			list($r, $g, $b) = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);    
		} else {
			return FALSE;
		}
		
		$r = hexdec($r); 
		$g = hexdec($g); 
		$b = hexdec($b);    
		
		return array($r, $g, $b);
	}	
}
?>