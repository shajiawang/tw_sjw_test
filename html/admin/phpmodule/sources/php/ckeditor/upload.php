<?php
if (!empty($this->io->input['files']['upload']) 
	&& exif_imagetype($this->io->input['files']['upload']['tmp_name'])
) {
	$filename = $this->config->path_upload_images."/".$this->io->input['files']['upload']['name'];
	if (move_uploaded_file($this->io->input['files']['upload']['tmp_name'], $filename)) {
		$url = '/site/images/site/upload/'.$this->io->input['files']['upload']['name'];

		$message = "";
		$funcNum = $this->io->input['get']['CKEditorFuncNum'];
		echo "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction($funcNum, '$url', '$message');</script>";
	}
}