<div id="dialog-product-category" title="商品分類列表" class="dialog">
	<table>
		<tbody>
			<tr>
				<th>搜尋：</th>
				<td><input type="text" id="product-category-name" placeholder="商品分類名稱"></td>
			</tr>
			<tr>
				<th>商品分類</th>
				<td>
					<ul class="rt-list">
					<?php foreach($this->tplVar['table']["record"] as $rk => $rv) : ?>
  						<li>
  							<input type="checkbox" id="pcid_<?php echo $rk; ?>" class="relation" name="pcid[]" value="<?php echo $rv['pcid']; ?>">
  							<label for="pcid_<?php echo $rk; ?>" title="<?php echo $rv['description']; ?>"><?php echo $rv['name']; ?></label>
  						</li>
					<?php endforeach; ?>
  					</ul>
				</td>
			</tr>
			<tr>
				<td class="button-block" colspan="2">
					<a href="javascript:void(0)" name="dlg-add" id="dlg-add">加入</a>
				</td>
			</tr>
		</tbody>
	</table>
</div>