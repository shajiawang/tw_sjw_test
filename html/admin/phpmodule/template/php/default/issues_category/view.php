<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">管理</a>>>
			<a>分類</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">分類名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_btime">統計開始：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">統計截止：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<th>分類名稱</th>
								<th style="max-width:200px;">分類敘述</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_seq"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_seq=desc">排序<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_seq"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=">排序<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=asc">排序<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>啟用</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<?php /*<td class="list-checkbox"><input type="checkbox" class="list-item"/></td>*/?>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/uicid=<?php echo $rv["uicid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/uicid=<?php echo $rv["uicid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="description" style="max-width:200px;text-align:left"><?php echo $rv['description']; ?></td>
								<td class="column" name="seq"><?php echo $rv['seq']; ?></td>
								<td class="column" name="switch"><?php echo $rv['switch']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php require_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="body" >統計區間: <?php echo $this->tplVar['stat_btime'];?> ~ <?php echo $this->tplVar['stat_etime'];?></li>
				<li class="body" >
					<table style="width:500px; float:left;">
						<thead>
							<tr>
								<th>分類名稱</th>
								<th>結案數量</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]["rt"]["issues_stat"])) { ?>
							<?php foreach($this->tplVar["table"]["rt"]["issues_stat"] as $rk => $rv) { ?>
							<tr>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="cnt"><?php echo $rv['cnt']; ?></td>
							</tr>
							<?php 
							$sum += intval($rv['cnt']); 
							}//endforeach; 
							?>
							<tr>
								<td class="column" > </td>
								<td class="column" >總計: <?php echo $sum; ?></td>
							</tr>
							<?php }//endif; ?>
						</tbody>
					</table>
				</li>
			</ul>
		</div>
		
	</body>
</html>