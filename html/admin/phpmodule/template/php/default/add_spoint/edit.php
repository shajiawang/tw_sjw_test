<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
			
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>			
				<tr>
					<td><label for="spoint">殺價幣數量：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['amount']; ?></td>
				</tr>
				<tr>
					<td><label for="spoint">關聯編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['fromid']; ?></td>
				</tr>				
				<tr>
					<td><label for="behav">殺價幣行為：</label></td>
					<td>
						<select id="behav" name="behav" disabled="disabled">
							<option value="prod_buy" <?php echo $this->tplVar['table']['record'][0]['behav']=="prod_buy"?" selected":""; ?>>商品購買</option>
							<option value="prod_sell" <?php echo $this->tplVar['table']['record'][0]['behav']=="prod_sell"?" selected":""; ?>>中標商品折算</option>
							<option value="process_fee" <?php echo $this->tplVar['table']['record'][0]['behav']=="process_fee"?" selected":""; ?>>中標處理費</option>
							<option value="user_saja" <?php echo $this->tplVar['table']['record'][0]['behav']=="user_saja"?" selected":""; ?>>下標手續費</option>
							<option value="other_system_use" <?php echo $this->tplVar['table']['record'][0]['behav']=="other_system_use"?" selected":""; ?>>第三方抵扣</option>
							<option value="feedback" <?php echo $this->tplVar['table']['record'][0]['behav']=="feedback"?" selected":""; ?>>(推薦)回饋</option>
							<option value="sajabonus" <?php echo $this->tplVar['table']['record'][0]['behav']=="sajabonus"?" selected":""; ?>>鯊魚點兌換</option>
							<option value="bid_by_saja" <?php echo $this->tplVar['table']['record'][0]['behav']=="bid_by_saja"?" selected":""; ?>>中標送殺價幣</option>
							<option value="bid_refund" <?php echo $this->tplVar['table']['record'][0]['behav']=="bid_refund"?" selected":""; ?>>流標退款</option>
							<option value="gift" <?php echo $this->tplVar['table']['record'][0]['behav']=="gift"?" selected":""; ?>>(儲值)贈送</option>
							<option value="system" <?php echo $this->tplVar['table']['record'][0]['behav']=="system"?" selected":""; ?>>系統轉入</option>
							<option value="others" <?php echo $this->tplVar['table']['record'][0]['behav']=="others"?" selected":""; ?>>其他</option>
						</select>					
					</td>
				</tr>
				<tr>
					<td><label for="remark">活動編號：</label></td>
					<td>
						<select id="remark" name="remark" disabled="disabled">
							<option value="0">無相關活動</option>
							<?php foreach($this->tplVar['table']["rt"]["action"] as $ck => $cv) : ?>
							<option value="<?php echo $cv['activity_type']; ?>" <?php if($this->tplVar['table']['record'][0]['remark']==$cv['action_type']){ echo 'selected'; } ?> ><?php echo $cv['action_text']; ?></option>
							<?php endforeach; ?>
						</select>					
					</td>
				</tr>				
				<tr>
					<td><label for="memo">備註：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['memo']; ?></td>
				</tr>	
				<tr>
					<td><label for="switch">顯示：</label></td>
					<td>
						<select name="switch" readonly="readonly">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>
				</tr>	
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="spointid" value="<?php echo $this->tplVar["status"]["get"]["spointid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">回上頁</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>