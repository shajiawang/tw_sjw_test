<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><input name="userid" id="userid" type="text" maxlength="100" /></td>
				</tr>			
				<tr>
					<td><label for="spoint">殺價幣數量：</label></td>
					<td><input name="spoint" id="spoint" type="number" value="0" maxlength="6" /></td>
				</tr>
				<tr>
					<td><label for="behav">殺價幣行為：</label></td>
					<td>
						<select id="behav" name="behav">
							<option value="feedback">(推薦)回饋</option>
							<option value="sajabonus">鯊魚點兌換</option>
							<option value="bid_by_saja">中標送殺價幣</option>
							<option value="bid_refund">流標退款</option>
							<option value="gift">(儲值)贈送</option>
							<option value="system">系統轉入</option>
							<option value="others">其他</option>
						</select>					
					</td>
				</tr>
				<tr>
					<td><label for="remark">活動編號：</label></td>
					<td>
						<select id="remark" name="remark">
							<option value="0">無相關活動</option>
							<?php foreach($this->tplVar['table']['record'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['activity_type']; ?>"><?php echo $cv['action_text']; ?></option>
							<?php endforeach; ?>
						</select>					
					</td>
				</tr>	
				<tr>
                   <td><label for="memo">備註：</label></td>
				   <td colspan="3"> 				    
					<textarea name="memo" cols="64" rows="8" value=""></textarea>
				   </td>
				</tr>				
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>