<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_btime">開始時間：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">結束時間：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>	
				<li class="search-field">
					<span class="label" for="search_behav">使用狀態：</span>
					<select id="search_behav" name="search_behav" >
						<option value="prod_buy" <?php echo $this->io->input['get']['search_behav']=="prod_buy"?" selected":""; ?>>商品購買</option>
						<option value="prod_sell" <?php echo $this->io->input['get']['search_behav']=="prod_sell"?" selected":""; ?>>中標商品折算</option>
						<option value="process_fee" <?php echo $this->io->input['get']['search_behav']=="process_fee"?" selected":""; ?>>中標處理費</option>
						<option value="user_saja" <?php echo $this->io->input['get']['search_behav']=="user_saja"?" selected":""; ?>>下標手續費</option>
						<option value="other_system_use" <?php echo $this->io->input['get']['search_behav']=="other_system_use"?" selected":""; ?>>第三方抵扣</option>
						<option value="feedback" <?php echo $this->io->input['get']['search_behav']=="feedback"?" selected":""; ?>>(推薦)回饋</option>
						<option value="sajabonus" <?php echo $this->io->input['get']['search_behav']=="sajabonus"?" selected":""; ?>>鯊魚點兌換</option>
						<option value="bid_by_saja" <?php echo $this->io->input['get']['search_behav']=="bid_by_saja"?" selected":""; ?>>中標送殺價幣</option>
						<option value="bid_refund" <?php echo $this->io->input['get']['search_behav']=="bid_refund"?" selected":""; ?>>流標退款</option>
						<option value="gift" <?php echo $this->io->input['get']['search_behav']=="gift"?" selected":""; ?>>(儲值)贈送</option>
						<option value="system" <?php echo $this->io->input['get']['search_behav']=="system"?" selected":""; ?>>系統轉入</option>
						<option value="others" <?php echo $this->io->input['get']['search_behav']=="others"?" selected":""; ?>>其他</option>
					</select>
				</li>					
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_spointid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_spointid=desc">殺價幣編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_spointid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_spointid=">殺價幣編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_spointid=asc">殺價幣編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>會員編號</th>
								<th>點數</th>
								<th>使用狀態</th>
								<th>活動編號</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/spointid=<?php echo $rv["spointid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="spointid"><?php echo $rv['spointid']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
								<td class="column" name="behav"><?php echo $this->tplVar['status']['behav'][$rv['behav']];  ?></td>
								<td class="column" name="remark"><?php echo $rv['remark']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>