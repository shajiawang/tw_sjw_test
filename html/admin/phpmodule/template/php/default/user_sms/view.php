<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<style type="text/css">
		.message {
			color: #29736c; width:300px; background-color: #ecf8f7; border: 1px solid #005d54; border-radius: 4px; display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px; background-color: #8dc8c3; border-radius: 4px; position: relative;
		}
		.message header h2 {
			font-weight: bold; font-size: 16px; line-height: 20px; margin: 0 12px;			
		}
		.message header .button-close {
			text-decoration: none; color: #ecf8f7; float: right; margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px; padding: 10px 0;
		}
		.message.center {
			position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		.msg{
			color: #29736c;
		}
		</style>
	</head>
	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a>手機驗證送查詢</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				
				<li class="search-field">
					<span class="label" for="search_act">是否驗證：</span>
					<select name="search_act">
						<option value="Y" <?php echo ($this->io->input['get']['search_act']=='Y')?'selected':''; ?> >--</option>
						<option value="A" <?php echo ($this->io->input['get']['search_act']=='A')?'selected':''; ?> >不限</option>
						<option value="N" <?php echo ($this->io->input['get']['search_act']=='N')?'selected':''; ?> >No</option>
					</select>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_tel">手機號碼：</span>
					<input type="text" name="search_tel" size="20" value="<?php echo $this->io->input['get']['search_tel']; ?>"/>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_userid">會員ID：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<?php /*<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add">新增</a>*/?>
						</div>					
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								
								<?php /*
								$base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; 
								ua.authid, ua.userid, ua.phone, ua.verified, ua.switch, ah.userid uactid, ua.modifyt, ah.activity_type
								*/?>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_userid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_userid=desc">會員<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_userid"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_userid=">會員<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_userid=asc">會員<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								
								<th>手機號碼</th>
								<th>驗證狀態</th>
								<th>Switch</th>
								<th>領取人</th>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_modifyt=desc">異動日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_modifyt=">異動日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=asc">異動日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								
							</tr>
						</thead>
						
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="userid">
									<?php echo $rv['userid'];?> 
									<br><?php echo $rv['nickname']; ?>
								</td>
								
								<td class="column" name="phone"><?php echo $rv['phone']; ?></td>
								<td class="column" name="verified">
								<?php if(!empty($rv['verified']) && $rv['verified']=='Y'){ ?>已驗證
								<?php }else{ ?>No<?php } ?> 
								</td>
								
								<td class="column" name="switch">
								<?php if(!empty($rv['switch']) && $rv['switch']=='Y'){ ?>Yes
								<?php }else{ ?>No<?php } ?> 
								</td>
								
								<td class="column" name="uactid">
									<?php echo $rv['uactid']; ?>
									<br><?php echo $rv['uactname']; ?>
								</td>
								
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		
		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg" id="msg-title">訊息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="" >
				<p id="msg">Message content</p>
			</div>
		</article>
		
	</body>
</html>

<script type="text/javascript">

$(function(){
	//訊息窗
	$('#msg-close').on('click', function(){
		$('#msg-dialog').hide();
	});
});		

</script>