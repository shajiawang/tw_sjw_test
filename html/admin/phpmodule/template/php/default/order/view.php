<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<style type="text/css">
		.message {
			color: #29736c; width:300px; background-color: #ecf8f7; border: 1px solid #005d54; border-radius: 4px; display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px; background-color: #8dc8c3; border-radius: 4px; position: relative;
		}
		.message header h2 {
			font-weight: bold; font-size: 16px; line-height: 20px; margin: 0 12px;			
		}
		.message header .button-close {
			text-decoration: none; color: #ecf8f7; float: right; margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px; padding: 10px 0;
		}
		.message.center {
			position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		.msg{
			color: #29736c;
		}
		</style>		
	</head>
	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form name="searchform" id="searchform" method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_orderid">訂單編號：</span>
					<input type="text" name="search_orderid" size="20" value="<?php echo $this->io->input['get']['search_orderid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_confirm">訂單確認：</span>
					<select name="search_confirm">
						<option value="ALL" <?php echo ($this->io->input['get']['search_confirm']=='ALL') ? 'selected' : ''; ?>>ALL</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_confirm']=='Y') ? 'selected' : ''; ?>>是</option>
						<option value="N" <?php echo ($this->io->input['get']['search_confirm']=='N') ? 'selected' : ''; ?>>否</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_status">訂單狀態：</span>
					<select name="search_status">
						<option value="ALL" <?php echo ($this->io->input['get']['search_status']=='ALL') ? 'selected' : ''; ?>>ALL</option>
						<option value="0" <?php echo ($this->io->input['get']['search_status']=='0') ? 'selected' : ''; ?>>處理中</option>
						<option value="1" <?php echo ($this->io->input['get']['search_status']=='1') ? 'selected' : ''; ?>>配送中</option>
						<option value="2" <?php echo ($this->io->input['get']['search_status']=='2') ? 'selected' : ''; ?>>退費中</option>
						<option value="3" <?php echo ($this->io->input['get']['search_status']=='3') ? 'selected' : ''; ?>>已發送</option>
						<option value="4" <?php echo ($this->io->input['get']['search_status']=='4') ? 'selected' : ''; ?>>已到貨</option>
						<option value="5" <?php echo ($this->io->input['get']['search_status']=='5') ? 'selected' : ''; ?>>已退費</option>
						<option value="6" <?php echo ($this->io->input['get']['search_status']=='6') ? 'selected' : ''; ?>>不出貨</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_type">類別：</span>
					<select name="search_type">
						<option value="ALL" <?php echo ($this->io->input['get']['search_type'] == 'ALL') ? 'selected' : ''; ?>>ALL</option>
						<option value="saja" <?php echo ($this->io->input['get']['search_type'] == 'saja') ? 'selected' : ''; ?>>得標</option>
						<option value="exchange" <?php echo ($this->io->input['get']['search_type'] == 'exchange') ? 'selected' : ''; ?>>兌換商品</option>
						<option value="lifepay" <?php echo ($this->io->input['get']['search_type'] == 'lifepay') ? 'selected' : ''; ?>>生活繳費</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_btime">訂單開始：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">訂單截止：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<?php /*
				<li class="search-field">
					<span class="label" for="search_userid">會員：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_esid">商品店家：</span>
					<input type="text" name="search_esid" size="20" value="<?php echo $this->io->input['get']['search_esid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_epcid">商品分類：</span>
					<input type="text" name="search_epcid" size="20" value="<?php echo $this->io->input['get']['search_epcid']; ?>"/>
				</li>*/?>
				<li class="search-field">
					<span class="label" for="search_epname">商品名稱：</span>
					<input type="text" name="search_epname" size="20" value="<?php echo $this->io->input['get']['search_epname']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<form id=form-edit>
		<input type="hidden" name="type" size="20">
		</form>
		<form name="exportform" id="exportform" >
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<?php /*<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>*/?>
							<a href="#" onClick="ocheck('exportform');">匯出</a>
						</div>	
						<div class="button">
							<a id="shipping" href="#" onClick="oupcheck('exportform');">批次出貨</a>
						</div>							
						<div class="button">
							<a id="close" href="#" onClick="ogetcheck('exportform');">批次到貨</a>
						</div>
						<div class="button">
							<a id="close" href="#" onClick="orderstatus('exportform');">批次訂單狀態</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<?php /*<th></th>*/?>
								<th></th>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_orderid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_orderid=desc">訂單編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_orderid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_orderid=">訂單編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_orderid=asc">訂單編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">訂單日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">訂單日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">訂單日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>訂單狀態</th>
								<?php if($_SESSION['user']['department'] != 'A') { ?>
								<th>訂單確認</th>
								<th>會員</th>
								<?php }else{  ?>
								<th>會員編號</th>
								<?php } ?>
								<?php /*<th>商品主圖</th>*/?>
								<th>類別</th>
								<th style="max-width:100px;">商品名稱</th>
								<?php if($_SESSION['user']['department'] != 'A') { ?>
								<th>兌換數量</th>
								<?php } ?>
								<?php /*<th>供應店家</th>*/?>
								<th>商品單價</th>
								<th>訂單總價</th>
								<th>折抵金額</th>
								<th>訂單分潤</th>
								<?php if($_SESSION['user']['department'] == 'A') { ?>
								<th>手機號</th>
								<th>收件人</th>
								<th>地址</th>								
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<?php if (empty($this->io->input['get']['search_epname'])){	
							$order_status = $rv['status'];
							$stat_memo = $this->tplVar['table']['rt'][$rv['orderid']]['stat_memo'];
							?>
							<tr>
								<td class="icon" name="orderid"><input type="checkbox" name="orderidchk" value="<?php echo $rv['orderid']; ?>"/></td>
								<?php /*<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/orderid=<?php echo $rv["orderid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>*/?>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/orderid=<?php echo $rv["orderid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="orderid" id="<?php echo $rv['orderid']; ?>" data-memo="<?php echo $stat_memo;?>" data-stat="<?php echo $order_status;?>"><?php echo $rv['orderid']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="status"><?php echo $rv['statusname']; ?>(<font style="color:#FF0000;"><?php echo $this->tplVar['table']['rt'][$rv['orderid']]['mall_status'];?></font>)</td>
								<?php if($_SESSION['user']['department'] != 'A') { ?>
								<td class="column" name="confirm"><?php echo $rv['confirm']; ?></td>								
								<td class="column" name="username"><?php echo $rv['username']; ?>( <?php echo $rv['userid']; ?> )</td>
								<?php }else{  ?>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<?php } ?>
								<td class="column" name="type"><?php echo $rv['typename']; ?></td>
								<?php if ($rv['typename'] == '得標'){ ?>
								<td class="column" name="epname" style="max-width:100px;text-align:left"><a href="<?php echo $this->config->default_main; ?>/product/edit/?productid=<?php echo $rv['epid']; ?>" target="_blank"><?php echo $rv['epname']; ?></a></td>
								<?php }else{ ?>
								<td class="column" name="epname" style="max-width:100px;text-align:left"><a href="<?php echo $this->config->default_main; ?>/exchange_product/form/?mod=edit&epid=<?php echo $rv['epid']; ?>" target="_blank"><?php echo $rv['epname']; ?></a></td>
								<?php } ?>
								<?php if($_SESSION['user']['department'] != 'A') { ?>
								<td class="column" name="num"><?php echo $rv['num']; ?></td>
								<?php } ?>
								<td class="column" name="point_price"><?php echo $rv['point_price']; ?></td>
								<td class="column" name="total_fee"><?php echo $rv['total_fee']; ?></td>
								<td class="column" name="discount_fee"><?php echo $rv['discount_fee']; ?></td>
								<td class="column" name="profit"><?php echo $rv['profit']; ?></td>
								<?php if($_SESSION['user']['department'] == 'A') { ?>
								<td class="column" name="phone"><?php echo $rv['ophone']; ?></td>
								<td class="column" name="name"><?php echo $rv['oname']; ?></td>	
								<td class="column" name="address"><?php echo $rv['oaddress']; ?></td>		
								<?php } ?>								
							</tr>
							<?php } else { ?>
							
							<?php if (stristr($rv['epname'], $this->io->input['get']['search_epname'])==true){ ?>
							<tr>
								<td class="icon" name="orderid"><input type="checkbox" name="orderidchk" value="<?php echo $rv['orderid']; ?>"/></td>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/orderid=<?php echo $rv["orderid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="orderid"><?php echo $rv['orderid']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="status"><?php echo $rv['statusname']; ?></td>
								<?php if($_SESSION['user']['department'] != 'A') { ?>
								<td class="column" name="confirm"><?php echo $rv['confirm']; ?></td>
								<td class="column" name="username"><?php echo $rv['username']; ?></td>
								<?php }else{  ?>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<?php } ?>
								<td class="column" name="type"><?php echo $rv['typename']; ?></td>
								<td class="column" name="epname" style="max-width:100px;text-align:left"><?php echo $rv['epname']; ?></td>
								<?php if($_SESSION['user']['department'] != 'A') { ?>
								<td class="column" name="num"><?php echo $rv['num']; ?></td>
								<?php } ?>
								<td class="column" name="point_price"><?php echo $rv['point_price']; ?></td>
								<td class="column" name="total_fee"><?php echo $rv['total_fee']; ?></td>
								<td class="column" name="discount_fee"><?php echo $rv['discount_fee']; ?></td>
								<td class="column" name="profit"><?php echo $rv['profit']; ?></td>
								<?php if($_SESSION['user']['department'] == 'A') { ?>
								<td class="column" name="phone"><?php echo $rv['ophone']; ?></td>
								<td class="column" name="name"><?php echo $rv['oname']; ?></td>	
								<td class="column" name="address"><?php echo $rv['oaddress']; ?></td>									
								<?php } ?>								
							</tr>
							<?php } ?>
							
							<?php } ?>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		</form>
		
		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg"  id="msg-title">訊息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="">
				<p id="msg">Message content</p>
			</div>
		</article>	
		
	</body>
</html>
	<script language="Javascript">
		function ocheck(formObj) {
			var form = document.getElementById(formObj);
			var obj = form.orderidchk;
			var selected=[];
			
			if (typeof obj !== 'undefined') {
				var selected=[];
				for (var i=0; i<obj.length; i++) {
					// console.log(obj[i].checked);
					if (obj[i].checked) {
						selected.push(obj[i].value);
					}
				}
				// console.log(selected.join());
				if (selected.join() != ''){
					var oid = selected.join();
					// console.log(String(oid));
					location.href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>&search_orderid="+String(oid)+"&search_userid=<?php echo $this->io->input['get']['search_userid']; ?>&search_confirm=<?php echo $this->io->input['get']['search_confirm']; ?>&search_status=<?php echo $this->io->input['get']['search_status']; ?>&search_type=<?php echo $this->io->input['get']['search_type']; ?>&search_btime=<?php echo $this->io->input['get']['search_btime']; ?>&search_etime=<?php echo $this->io->input['get']['search_etime']; ?>&search_epname=<?php echo $this->io->input['get']['search_epname']; ?>";
				}else{
					location.href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>&search_orderid=<?php echo $this->io->input['get']['search_orderid']; ?>&search_userid=<?php echo $this->io->input['get']['search_userid']; ?>&search_confirm=<?php echo $this->io->input['get']['search_confirm']; ?>&search_status=<?php echo $this->io->input['get']['search_status']; ?>&search_type=<?php echo $this->io->input['get']['search_type']; ?>&search_btime=<?php echo $this->io->input['get']['search_btime']; ?>&search_etime=<?php echo $this->io->input['get']['search_etime']; ?>&search_epname=<?php echo $this->io->input['get']['search_epname']; ?>";
				}
			}else{
				location.href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>&search_orderid=<?php echo $this->io->input['get']['search_orderid']; ?>&search_userid=<?php echo $this->io->input['get']['search_userid']; ?>&search_confirm=<?php echo $this->io->input['get']['search_confirm']; ?>&search_status=<?php echo $this->io->input['get']['search_status']; ?>&search_type=<?php echo $this->io->input['get']['search_type']; ?>&search_btime=<?php echo $this->io->input['get']['search_btime']; ?>&search_etime=<?php echo $this->io->input['get']['search_etime']; ?>&search_epname=<?php echo $this->io->input['get']['search_epname']; ?>";
			}
		}

		//修改訂單狀態
		function orderstatus(formObj) {
			var form = document.getElementById(formObj);
			var obj = form.orderidchk;
			//var selected=[];
			var selected=[];
			for (var i=0; i<obj.length; i++) {
				if (obj[i].checked) {
					selected.push(obj[i].value);
				}
			}
			if (selected.join() != ''){
				var oid = "'"+selected.join()+"'";
				//var stat = $("#"+ oid).data("stat");
				//var memo = $("#"+ oid).data("memo");
				var data = '<table><tr><td>修改訂單狀態：</td></tr><tr><td>訂單狀態：<select name="stat_id" id="stat_id"><option value="0">處理中</option><option value="1">配送中</option><option value="2">退費中</option><option value="3">已發送</option><option value="4">已到貨</option><option value="5">已退費</option><option value="6">不出貨</option></select></td></tr><tr><td>備　　註：<textarea name="stat_memo" type="text" id="stat_memo" ></textarea></td></tr><tr><td><input type="button" value="確認修改" onclick="to_stat_modify('+oid+')"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();
			}
		}
		function to_stat_modify(oid){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
				{
					type: "stat_modify",
					orderid: oid,
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					oid_status: '',
					oid_memo: '',
					order_status: $('#stat_id').val(),
					status_memo: $('#stat_memo').val(),
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};
		
		//出貨
		function oupcheck(formObj) {
			var form = document.getElementById(formObj);
			var obj = form.orderidchk;
			var selected=[];
			var selected=[];
			for (var i=0; i<obj.length; i++) {
				if (obj[i].checked) {
					selected.push(obj[i].value);
				}
			}
			if (selected.join() != ''){
				var oid = "'"+selected.join()+"'";
				var eptype = '4';
				if(eptype == 4){
					$("#type").val('shipping_card');
					$("#form-edit").attr("action", "<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/edit2");
					$("#form-edit").submit();
				}else{
					var data = '<table><tr><td>通知出貨：</td></tr><tr><td>出貨時間：<input name="s_time" type="text" id="s_time" value="<?php echo date('Y-m-d H:i',time());?>" ></td></tr><tr><td>配送方式：<input name="s_type" type="text" id="s_type" value="廠商自送" ></td></tr><tr><td>物流單號：<input name="s_code" type="text" id="s_code" value="" ></td></tr><tr><td>備　　註：<textarea name="s_memo" type="text" id="s_memo" ></textarea></td></tr><tr><td><input type="button" value="確認出貨" onclick="to_shipping('+oid+')"></td></tr></table>';
					$('#msg').html(data);
					$('#msg-dialog').show();
				}
			
			}
		}
		
		//到貨
		function ogetcheck(formObj) {
			var form = document.getElementById(formObj);
			var obj = form.orderidchk;
			var selected=[];
			var selected=[];
			for (var i=0; i<obj.length; i++) {
				if (obj[i].checked) {
					selected.push(obj[i].value);
				}
			}
			if (selected.join() != ''){
				var oid = "'"+selected.join()+"'";
				var data = '<table><tr><td>通知到貨：</td></tr><tr><td>到貨時間：<input name="c_time" type="text" id="c_time" value="<?php echo date('Y-m-d H:i',time());?>" ></td></tr><tr><td>備　　註：<textarea name="c_memo" type="text" id="c_memo" ></textarea></td></tr><tr><td><input type="button" value="確認出貨" onclick="to_close('+oid+')"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();			
			}
		}

		//通知出貨
		function to_shipping(oid){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/arrayupdate',
				{
					type: "shipping",
					orderid: oid,
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					outtime: $('#s_time').val(),
					outtype: $('#s_type').val(),
					outcode: $('#s_code').val(),
					outmemo: $('#s_memo').val()
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};
		
		//通知到貨
		function to_close(oid){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/arrayupdate',
				{
					type: "close",
					orderid: oid,
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					closetime: $('#c_time').val(),
					closememo: $('#c_memo').val(),
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};	

		$(function(){
			//訊息窗
			$('#msg-close').on('click', function(){
				$('#msg-dialog').hide();
			});
		});		
	</script>