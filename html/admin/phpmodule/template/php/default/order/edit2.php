<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->tplVar["status"]["get"]["fun"]; ?>/update">
			<div class="form-label">新增資料</div>
			<?php for($i = 1;$i <= $this->tplVar["post"]['num']; $i ++){
				?>
				<table class="form-table">
					<tr>
						<td><label for="epid[]">商品卡第<?php echo $i;?>張：</label></td>
					</tr>
					<tr>
						<td><label for="code1[]">序號1：</label></td>
						<td><input name="code1[]" type="text" size="60" value=""/></td>
					</tr>
					<tr>
						<td><label for="codeformat1[]">序號格式1：</label></td>
						<td><input name="codeformat1[]" type="text" size="60" value="0"/>
							<br>
							條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
						</td>
					</tr>
					<tr>
						<td><label for="code2[]">序號2：</label></td>
						<td><input name="code2[]" type="text" size="60" value=""/></td>
					</tr>
					<tr>
						<td><label for="codeformat2[]">序號格式2：</label></td>
						<td><input name="codeformat2[]" type="text" size="60" value="0"/>
							<br>
							條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
						</td>
					</tr>
					<tr>
						<td><label for="code3[]">序號3：</label></td>
						<td><input name="code3[]" type="text" size="60" value=""/></td>
					</tr>
					<tr>
						<td><label for="codeformat3[]">序號格式3：</label></td>
						<td><input name="codeformat3[]" type="text" size="60" value="0"/>
							<br>
							條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
						</td>
					</tr>
					<tr>
						<td><label for="code4[]">序號4：</label></td>
						<td><input name="code4[]" type="text" size="60" value=""/></td>
					</tr>
					<tr>
						<td><label for="codeformat4[]">序號格式4：</label></td>
						<td><input name="codeformat4[]" type="text" size="60" value="0"/>
							<br>
							條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
						</td>
					</tr>
				</table>
				<?php
				}
				?>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar['post']['location_url']; ?>">
				<input type="hidden" name="type" value="<?php echo $this->tplVar['post']['type']; ?>">
				<input type="hidden" name="orderid" value="<?php echo $this->tplVar['post']['orderid']; ?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['post']['userid']; ?>">
				<input type="hidden" name="epid" value="<?php echo $this->tplVar['post']['epid']; ?>">
				<input type="hidden" name="num" value="<?php echo $this->tplVar['post']['num']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/orderid=<?php echo $this->tplVar["post"]['orderid'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['post']['location_url'])) ;?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>