<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
			    <link rel="stylesheet" href="/admin/css/blueprint/screen.css" type="text/css" media="screen, projection">
	    <link rel="stylesheet" href="/admin/css/blueprint/print.css" type="text/css" media="print">
	    <!--[if lt IE 8]><link rel="stylesheet" href="/admin/css/blueprint/ie.css" type="text/css" media="screen, projection"><![endif]-->
	    <link rel="stylesheet" href="/admin/css/images.css" type="text/css">
<!--	    <link rel="stylesheet" href="/admin/css/main.css" type="text/css">-->
	    <link rel="stylesheet" href="/admin/css/widget.css" type="text/css">
	    <!-- link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/le-frog/jquery-ui.css" type="text/css" -->
		<link rel="stylesheet" href="/admin/css/jquery-ui.css" type="text/css">
	    <!-- script src="http://code.jquery.com/jquery-1.9.1.js"></script -->

	    <link rel="stylesheet" href="/admin/css/esStyle/es-default.css" type="text/css">
	    <link rel="stylesheet" href="/admin/css/esStyle/es-main.css" type="text/css">

		<!-- script src="http://code.jquery.com/jquery-1.10.1.min.js"></script -->
<!--		<script src="/admin/js/jquery.min.js"></script>-->
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>

		<!--<script src="/admin/js/jquery.min.js"></script>
	     script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script -->
		 <script src="/admin/js/jquery-ui.js"></script>
	    <script type="text/javascript" src="/admin/js/jquery-ui-timepicker-addon.js"></script>
		<script type="text/javascript" src="/admin/js/ckeditor/ckeditor.js"></script>
	    <script type="text/javascript">
	    jQuery(function($){
	    	//清除搜尋表單
	    	$('button.clear').on('click', function(e){
	    		$(this)
	    		.parents('form')
	    		.find('input,select,textarea')
	    		.val('')
	    	});
	    	//在列表點選刪除按鈕時，自動刪除被勾選的項目
			$('.table .functions .delete-all').click(function(){
				if (!confirm('是否確認刪除資料？')) {
					return false;
				}

				var now = 0,
					total = $('.table .body tr:has(.list-item:checked)').each(function(){
						$.get($('.icon-delete.icons', this).prop('href'), function(){
							now++;
							if (now == total) {
								window.location.reload();
							}
						});
					}).length;
			});
			$('.icon-delete.icons').click(function(){
				return confirm('是否確認刪除資料？');
			});
			//列表上方全選按鈕
			$('.table .checkbox-all').click(function(){
				$('.table .list-item:checkbox').prop('checked', $(this).is(':checked') );
			});
			//點擊rt列表名稱時，自動勾選
			$('.form .relation-list .relation-item-name').click(function(){
				$(this).siblings('input:checkbox').prop('checked', !$(this).siblings('input:checkbox').prop('checked'));
			});
			//日曆widget
			$('.datetime').datetimepicker({dateFormat:"yy-mm-dd"});

			$.fn.showPicker = function(options) {
				var defaults = {
						modal : true,
						height : 420,
						width : 320,
						autoOpen : true
		    		},
		    		settings = $.extend( {}, defaults, options );

	    		return this.each(function(){
	    			if (settings.fun) {
	    				var funid = settings.fun.replace('_','-'),
	    					$this = $(this);

						if (!$('#dialog-' + funid).length) {
							$.get('/admin/' + settings.fun + '/view/tpl=widget', function(html){
								if (html) {
									$(html).appendTo($this);
									$('#dialog-' + funid)
									.dialog(settings)
									.find('.rt-list .relation, .button-block a')
									.button();
								}
							});
						}
						else {
							$('#dialog-' + funid).dialog('open');
						}
	    			}
	    		});
			};

			if ($('#ckeditor_description').length) {
				CKEDITOR.replace('ckeditor_description', {
					filebrowserBrowseUrl: '/admin/ckeditor/view/',
					filebrowserImageBrowseUrl: '/admin/ckeditor/view/',
					filebrowserFlashBrowseUrl: '/admin/ckeditor/view/',
					filebrowserUploadUrl: '/admin/ckeditor/upload',
					filebrowserImageUploadUrl: '/admin/ckeditor/upload',
					filebrowserFlashUploadUrl: '/admin/ckeditor/upload'
				});
			}
			if ($('#ckeditor_rule').length) {
				CKEDITOR.replace('ckeditor_rule', {
					filebrowserBrowseUrl: '/admin/ckeditor/view/',
					filebrowserImageBrowseUrl: '/admin/ckeditor/view/',
					filebrowserFlashBrowseUrl: '/admin/ckeditor/view/',
					filebrowserUploadUrl: '/admin/ckeditor/upload',
					filebrowserImageUploadUrl: '/admin/ckeditor/upload',
					filebrowserFlashUploadUrl: '/admin/ckeditor/upload'
				});
			}
	    });
	    </script>
		<style type="text/css">
		.message {
			color: #29736c; width:300px; background-color: #ecf8f7; border: 1px solid #005d54; border-radius: 4px; display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px; background-color: #8dc8c3; border-radius: 4px; position: relative;
		}
		.message header h2 {
			font-weight: bold; font-size: 16px; line-height: 20px; margin: 0 12px;
		}
		.message header .button-close {
			text-decoration: none; color: #ecf8f7; float: right; margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px; padding: 10px 0;
		}
		.message.center {
			position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		.msg{
			color: #29736c;
		}
		</style>
	</head>

	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<div class="searchbar header-style">
			<form name="searchform" id="searchform" enctype="multipart/form-data" method="post" >
			<ul>
				<li class="search-field">
					<span class="label" for="search_btime">訂單開始：</span>
					<input type="file" name="file_name" id="file" />
				</li>

				<li class="button">
					<button>匯入CSV</button>
				</li>
			</ul>
			</form>
		</div>
		<form name="QCheckoutForm" id="QCheckoutForm" enctype="multipart/form-data">
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<?php /*<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>*/?>
							<a href="#" onClick="Qcheckout();">快速出貨</a>
						</div>

					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<?php /*<th></th>*/?>
								<th><input type="checkbox" name="allcheck" id='allcheck'/></th>
								<th>訂單編號</th>
								<th>使用者編號</th>
								<th>商品編號</th>
								<th>商品名稱</th>
								<th>兌換鯊魚點(市價)</th>
								<th>訂單狀態</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon" name="orderid"><input type="checkbox" name="orderidchk[]" value="<?php echo $rv['orderid'];  ?>" id="orderidchk_<?php echo $rv['orderid'];  ?>"/></td>
								<td class="column" name="orderid" ><?php echo $rv['orderid']; ?></td>
								<td class="column" name="userid" id="userid_<?php echo $rv['orderid']; ?>"><?php echo $rv['userid']; ?></td>
								<td class="column" name="productid"id="productid_<?php echo $rv['orderid']; ?>"><?php echo $rv['productid']; ?></td>
								<td class="column" name="name"id="name_<?php echo $rv['orderid']; ?>"><?php echo $rv['name']; ?></td>
								<td class="column" name="retail_price"id="retail_price_<?php echo $rv['orderid']; ?>"><?php echo $rv['retail_price']; ?></td>
								<td class="column" name="status"id="status_<?php echo $rv['orderid']; ?>"><?php echo $rv['status']; ?></td>
							</tr>

							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>

				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		</form>

		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg"  id="msg-title">訊息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="">
				<p id="msg">Message content</p>
			</div>
		</article>

	</body>
</html>
	<script language="Javascript">

		$("#allcheck").click(function() {
		   if($("#allcheck").prop("checked")) {
		     $("input[name='orderidchk[]']").each(function() {
		         $(this).prop("checked", true);
		     });
		   } else {
		     $("input[name='orderidchk[]']").each(function() {
		         $(this).prop("checked", false);
		     });
		   }
		});

		function Qcheckout(){
			var osn;
			$("input[name='orderidchk[]']:checked").each(function() {

				osn=$(this).val();
				var formData = new FormData();
				formData.append('body',"訂單編號:"+osn+" 商品"+$('#name_'+osn).html()+"己完成出貨");
				formData.append('productid',$('#productid_'+osn).html());
				formData.append('sendto',$('#userid_'+osn).html());
				formData.append('osn',osn);
				$.ajax({
			        url : 'batch_process_711',
			        type : "POST",
			        data : formData,
			        contentType: false,
			        cache: false,
			        processData: false,
			        success : function(rtn)
			        {
			        	data=JSON.parse(rtn);
			        	if (data.retCode=='1'){
			        		$('#orderidchk_'+data.retOsn).prop("checked", false);
			        	}
			        	$('#status_'+data.retOsn).html(data.retMsg);
			            console.log('#status_'+data.retOsn+"--"+data.retMsg);
			        },error: function(data)
			        {
			            console.log('無法送出');
			        }
			    })
     		});
		}



		$(function(){
			//訊息窗
			$('#msg-close').on('click', function(){
				$('#msg-dialog').hide();
			});
		});
	</script>