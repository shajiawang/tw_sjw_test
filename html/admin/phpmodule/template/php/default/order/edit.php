<!DOCTYPE html>
<html>
<?php $order_status = $this->tplVar['table']['record'][0]['status'];?>	
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<style type="text/css">
		.message {
			width: 300px;			background-color: #FFFFFF;			border: 1px solid #AAAAAA;			border-radius: 4px;			display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px;			background-color: #CCCCCC;						border-radius: 4px;			position: relative;
		}
		.message header h2 {
			font-weight: bold;			font-size: 16px;			line-height: 20px; margin: 0 12px;			
		}
		.message header .button-close {
			text-decoration: none;			color: black;			float: right;			margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px;			padding: 10px 0;
		}
		.message.center {
		  position: fixed;		  left: 50%;		  top: 50%;		  transform: translate(-50%, -50%);		  -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		</style>
		
		<script type="text/javascript">
		//通知出貨
		function to_shipping(){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
				{
					type: "shipping",
					orderid: '<?php echo $this->tplVar["status"]["get"]["orderid"] ;?>',
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					outtime: $('#s_time').val(),
					outtype: $('#s_type').val(),
					outcode: $('#s_code').val(),
					outmemo: $('#s_memo').val()
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};

		//缺貨退費
		function to_backin(){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
				{
					type: "backin",
					orderid: '<?php echo $this->tplVar["status"]["get"]["orderid"] ;?>',
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					backtime: $('#b_time').val(),
					backmemo: $('#b_memo').val(),
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};

		//缺貨已退費
		function to_back(){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
				{
					type: "back",
					orderid: '<?php echo $this->tplVar["status"]["get"]["orderid"] ;?>',
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					returntime: $('#b_time').val(),
					returnmemo: $('#b_memo').val(),
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};
		
		//通知到貨
		function to_close(){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
				{
					type: "close",
					orderid: '<?php echo $this->tplVar["status"]["get"]["orderid"] ;?>',
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					closetime: $('#c_time').val(),
					closememo: $('#c_memo').val(),
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};		
		
		//出貨訊息修改
		function shipping_modify(){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
			{
				type: "modify",
				orderid: '<?php echo $this->tplVar["status"]["get"]["orderid"] ;?>',
				location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
				outtime: $('#s_time').val(),
				outtype: $('#s_type').val(),
				outcode: $('#s_code').val(),
				outmemo: $('#s_memo').val()
			},
			function(str_json){
				if (str_json) {
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					//$('#msg').text(json.msg);
					alert(json.msg);
					location.reload();
				}
				//$('#msg-dialog').show();
			});
		}
		
		//修改訂單狀態
		function stat_modify(){
			$('#msg-dialog').hide();
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
				{
					type: "stat_modify",
					orderid: '<?php echo $this->tplVar["status"]["get"]["orderid"] ;?>',
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>',
					oid_status: '<?php echo $order_status; ?>',
					oid_memo: '<?php echo $this->tplVar['table']['rt']['stat_memo'];?>',
					order_status: $('#stat_id').val(),
					status_memo: $('#stat_memo').val(),
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						//$('#msg').text(json.msg);
						alert(json.msg);
						location.reload();
					}
					//$('#msg-dialog').show();
				}
			);
		};	
		
		$(function(){
			//訊息窗
			$('#msg-close').on('click', function(){
				$('#msg-dialog').hide();
			});
			
			//待退費
			$('#backformin').on('click', function(){
				var data = '<table><tr><td>待退費資料：</td></tr><tr><td>預計退費時間：<input name="b_time" type="text" id="b_time" value="<?php echo date('Y-m-d H:i',time());?>" ></td></tr><tr><td>辦理退費原因：<input name="b_memo" type="text" id="b_memo" value="" ></td></tr><tr><td><input type="button" value="辦理退費" onclick="to_backin()"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();
			});
			
			//缺貨退費
			$('#backform').on('click', function(){
				var data = '<table><tr><td>缺貨退費資料：</td></tr><tr><td>退費時間：<input name="b_time" type="text" id="b_time" value="<?php echo date('Y-m-d H:i',time());?>" ></td></tr><tr><td>退費原因：<input name="b_memo" type="text" id="b_memo" value="" ></td></tr><tr><td><input type="button" value="確認退費" onclick="to_back()"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();
			});
			
			//通知出貨
			$('#shipping').on('click', function(){
				var eptype = '<?php echo $this->tplVar['table']['record'][0]['eptype'];?>';
				if(eptype == 4){
					//var data = '<table><tr><td>通知出貨：</td></tr><tr><td>出貨時間：<input name="s_time" type="text" id="s_time" value="<?php echo date('Y-m-d H:i',time());?>" ></td></tr><tr><td>序號1：<input name="code1" type="text" id="code1" value="" ></td></tr><tr><td>序號1格式：<input name="codeformat1" type="text" id="codeformat1" value="" ></td></tr><tr><td>序號2：<input name="code2" type="text" id="code2" value="" ></td></tr><tr><td>序號2格式：<input name="codeformat2" type="text" id="codeformat2" value="" ></td></tr><tr><td><input type="button" value="確認出貨" onclick="to_shipping()"></td></tr></table>';
					$("#type").val('shipping_card');
					$("#form-edit").attr("action", "<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/edit2");
					$("#form-edit").submit();
				}else{
					var data = '<table><tr><td>通知出貨：</td></tr><tr><td>出貨時間：<input name="s_time" type="text" id="s_time" value="<?php echo date('Y-m-d H:i',time());?>" ></td></tr><tr><td>配送方式：<input name="s_type" type="text" id="s_type" value="廠商自送" ></td></tr><tr><td>物流單號：<input name="s_code" type="text" id="s_code" value="" ></td></tr><tr><td>備　　註：<textarea name="s_memo" type="text" id="s_memo" ></textarea></td></tr><tr><td><input type="button" value="確認出貨" onclick="to_shipping()"></td></tr></table>';
					$('#msg').html(data);
					$('#msg-dialog').show();
				}
			});
			
			//通知出貨訊息修改
			$('#shippingmessage').on('click', function(){
				var data = '<table><tr><td>通知出貨：</td></tr><tr><td>預計時間：<input name="s_time" type="text" id="s_time" value="<?php echo $this->tplVar['table']['rt']['shipping']['outtime'];?>" ></td></tr><tr><td>配送方式：<input name="s_type" type="text" id="s_type" value="<?php echo $this->tplVar['table']['rt']['shipping']['outtype']; ?>" ></td></tr><tr><td>物流單號：<input name="s_code" type="text" id="s_code" value="<?php echo $this->tplVar['table']['rt']['shipping']['outcode']; ?>" ></td></tr><tr><td>備　　註：<textarea name="s_memo" type="text" id="s_memo" ><?php echo str_replace("<br />", "\\n", preg_replace("/\r?\n/s", "<br />", $this->tplVar['table']['rt']['shipping']['outmemo'])); ?></textarea></td></tr><tr><td><input type="button" value="確認修改" onclick="shipping_modify()"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();
			});
			
			//通知到貨
			$('#close').on('click', function(){
				var data = '<table><tr><td>通知到貨：</td></tr><tr><td>到貨時間：<input name="c_time" type="text" id="c_time" value="<?php echo date('Y-m-d H:i',time());?>" ></td></tr><tr><td>備　　註：<textarea name="c_memo" type="text" id="c_memo" ></textarea></td></tr><tr><td><input type="button" value="確認出貨" onclick="to_close()"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();
			});	
			
			//修改訂單狀態
			$('#orderstatus').on('click', function(){
				var data = '<table><tr><td>修改訂單狀態：</td></tr><tr><td>訂單狀態：<select name="stat_id" id="stat_id"><option value="0" <?php echo ($order_status=='0') ? 'selected' : ''; ?>>處理中</option><option value="1" <?php echo ($order_status=='1') ? 'selected' : ''; ?>>配送中</option><option value="2" <?php echo ($order_status=='2') ? 'selected' : ''; ?>>退費中</option><option value="3" <?php echo ($order_status=='3') ? 'selected' : ''; ?>>已發送</option><option value="4" <?php echo ($order_status=='4') ? 'selected' : ''; ?>>已到貨</option><option value="5" <?php echo ($order_status=='5') ? 'selected' : ''; ?>>已退費</option><option value="6" <?php echo ($order_status=='6') ? 'selected' : ''; ?>>不出貨</option></select></td></tr><tr><td>備　　註：<textarea name="stat_memo" type="text" id="stat_memo" ><?php echo $this->tplVar['table']['rt']['stat_memo'];?></textarea></td></tr><tr><td><input type="button" value="確認修改" onclick="stat_modify()"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();
			});		
		});
		</script>
		
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="type">訂單類別</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['type']; ?></td>
				</tr>
				<tr>
					<td><label for="orderid">訂單編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['orderid']; ?></td>
				</tr>
				<tr>
					<td><label for="status">訂單狀態：</label></td>
					<td>
						<?php 
						if($order_status=='1'){ echo '配送中'; }
						elseif($order_status=='2'){ echo '退費中'; }
						elseif($order_status=='3'){ echo '已發送'; }
						elseif($order_status=='4'){ echo '己到貨'; }
						elseif($order_status=='5'){ echo '缺貨己退費'; }
						elseif($order_status=='6'){ echo '不出貨'; }
						else{ echo '處理中'; }
						?>(<font style="color:#FF0000;"><?php echo $this->tplVar['table']['rt']['mall_status'];?></font>)
					</td>
				</tr>
				<tr>
					<td><label for="insertt">訂單日期：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['insertt']; ?></td>
				</tr>
				<tr>
					<td><label for="point_price">商品名稱：</label></td>
					<td>
					   <?php echo $this->tplVar['table']['record'][0]['epname']; ?>
					   <?php 
					      if($this->tplVar['table']['record'][0]['epid']=='689') {
						      echo "(".$this->tplVar['table']['record'][0]['jieli_card_no'].")";
						  }
					   ?> 
					</td>
				</tr>
				<tr>
					<td><label for="point_price">商品單價 (繳費應付金額)：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['point_price']; ?></td>
				</tr>
				<tr>
					<td><label for="point_price">得標處理費：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['process_fee']; ?></td>
				</tr>	
				<tr>
					<td><label for="discount_fee">下標處理費：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['discount_fee']; ?></td>
				</tr>
				<tr>
					<td><label for="free_amount">包含免費金額：</label></td>
					<td><?php echo number_format(abs($this->tplVar['table']['rt']['free_history']['free_amount']), 4); ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">應補差額：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['total_fee']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">鯊魚點支付：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['bonus_pay']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">現金(刷卡)支付：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['cash_pay']; ?></td>
				</tr>
				<tr>
					<td><label for="thumbnail">發票：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/invoice/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata'])){ ?>
				
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata']['bankid'])){ ?>
				<tr>
					<td><label for="retail_price">銀行資枓：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['bankid']; ?> - <?php echo $this->tplVar['table']['record'][0]['paydata']['bkname']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">銀行分行：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['branch']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">戶名：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['account_name']; ?></td>
				</tr>				
				<?php } ?>
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata']['toid'])){ ?>
				<tr>
					<td><label for="retail_price">電信資料：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['toname']; ?> <?php echo $this->tplVar['table']['record'][0]['paydata']['tpname']; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td><label for="retail_price">銷帳資料：</label></td>
					<td><?php if ($this->tplVar['table']['record'][0]['paydata']['pcode']) { echo $this->tplVar['table']['record'][0]['paydata']['pcode'].' - '; } ?><?php echo $this->tplVar['table']['record'][0]['paydata']['number']; ?></td>
				</tr>
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata']['idnumber'])){ ?>
				<tr>
					<td><label for="retail_price">用戶身份證字號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['idnumber']; ?></td>
				</tr>
				<?php } ?>
				
				<?php } ?>
				
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="retail_price">會員：</label></td>
					<td>
                        <?php echo $this->tplVar['table']['record'][0]['uname']; ?>
                        ( <?php echo $this->tplVar['table']['record'][0]['userid']; ?> )
                    </td>
				</tr>
				<tr>
					<td><label for="retail_price">會員暱稱：</label></td>
					<td>
                        <?php echo $this->tplVar['table']['record'][0]['nickname']; ?>
                    </td>
				</tr>
				<tr>
					<td><label for="retail_price">驗證手機：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['smsphone']; ?></td>
				</tr>				
				<tr>
					<td><label for="retail_price">收件人(電話)：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['consignee'] .' ('. $this->tplVar['table']['record'][0]['phone'] .')'; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">收件人郵編,地址：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['zip'] .' '. $this->tplVar['table']['record'][0]['address']; ?></td>
				</tr>
				<!-- tr>
					<td><label>qq號碼：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['qqno']; ?></td>
				</tr -->
				<tr>
					<td><label>備&nbsp;&nbsp;註:</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['payname']; ?></td>
				</tr>
				<tr>
					<td><label>管理員備註:</label></td>
					<td><?php echo $this->tplVar['table']['rt']['stat_memo'];?></td>
				</tr>
				<tr>
					<td><label>用戶留言:</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['note']; ?></td>
				</tr>
				<?php
				if($this->tplVar['table']['record'][0]['ptype'] == 1){
					?>
					<tr>
						<td><label>圓夢商品標題:</label></td>
						<td><?php echo $this->tplVar['table']['record'][0]['title']; ?></td>
					</tr>
					<tr>
						<td><label>圓夢商品內文:</label></td>
						<td><?php echo $this->tplVar['table']['record'][0]['content']; ?></td>
					</tr>
					<tr>
						<td><label>圓夢商品購買連結:</label></td>
						<td><?php echo $this->tplVar['table']['record'][0]['purchase_url']; ?></td>
					</tr>
					<tr>
						<td><label>圓夢商品圖:</label></td>
						<td><a href="<?php echo $this->tplVar['table']['record'][0]['thumbnail_filename'];?>" target="_blank">查看</a></td>
					</tr>
					<?php
				}
				?>
			</table>
			
			<?php 
			if(!empty($this->tplVar['table']['rt']['shipping']) ){
			$shipping = $this->tplVar['table']['rt']['shipping'];
			?>
			<table class="form-table">
				<tr>
					<td><label for="retail_price">出貨訊息：</label></td>
					<td>
						出貨時間：<?php echo $shipping['outtime'];?><br>
						配送方式：<?php echo $shipping['outtype'];?><br>
						物流單號：<?php echo $shipping['outcode'];?><br>
						備    註：<?php echo $shipping['outmemo'];?><br>
					</td>
				</tr>
				<?php if ($order_status == 2){ ?>
				<tr>
					<td><label for="retail_price">退費中訊息：</label></td>
					<td>
						預計退費時間：<?php echo $shipping['backtime'];?><br>
						退費原因：<?php echo $shipping['backmemo'];?>
					</td>
				</tr>
				<?php } else { ?>
					<tr>
					<td><label for="retail_price">退費中訊息：</label></td>
					<td>
						預計退費時間：<?php echo $shipping['backtime'];?><br>
						退費原因：<?php echo $shipping['backmemo'];?>
					</td>
				</tr>
				<tr>
					<td><label for="retail_price">退費完成訊息：</label></td>
					<td>
						退費時間：<?php echo $shipping['returntime'];?><br>
						退費原因：<?php echo $shipping['returnmemo'];?>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td><label for="retail_price">到貨訊息：</label></td>
					<td>
						到貨時間：<?php echo $shipping['closetime'];?><br>
						備    註：<?php echo $shipping['closememo'];?>
					</td>
				</tr>
			</table>
			<?php } ?>
			
			<div class="functions">
				<input type="hidden" name="oldthumbnail" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail'];?>">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="orderid" value="<?php echo $this->tplVar["status"]["get"]["orderid"] ;?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['record'][0]["userid"] ;?>">
				<input type="hidden" name="epid" value="<?php echo $this->tplVar['table']['record'][0]["epid"] ;?>">
				<input type="hidden" name="num" value="<?php echo $this->tplVar['table']['record'][0]["num"] ;?>">
				<input type="hidden" name="type" id="type" value="invoice">
				<div class="button submit"><input type="submit" value="關閉" class="submit"></div>
				
				<!--div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div-->
				<?php if($order_status == '0'){ ?>
				<div class="button submit"><input type="button" value="通知出貨" id="shipping" class="submit"></div>
				<?php } ?>
				<?php if(($order_status != '2') && ($order_status != '4') && ($order_status != '5') && ($this->tplVar['table']['record'][0]['type'] == "exchange" || $this->tplVar['table']['record'][0]['type'] == "lifepay")){ ?>
				<div class="button submit"><input type="button" value="通知退費" id="backformin" class="submit"></div>
				<?php } ?>
				<?php if(($order_status == '2') && ($order_status != '4') && ($order_status != '5') && ($this->tplVar['table']['record'][0]['type'] == "exchange" || $this->tplVar['table']['record'][0]['type'] == "lifepay")){ ?>
				<div class="button submit"><input type="button" value="缺貨退費(退鯊魚點)" id="backform" class="submit"></div>
				<?php } ?>
				<?php if($order_status == '1'){ ?>
				<div class="button submit"><input type="button" value="出貨訊息修改" id="shippingmessage" class="submit"></div>
				<?php } ?>
				<?php if($order_status == '1'){ ?>
				<div class="button submit"><input type="button" value="通知到貨" id="close" class="submit"></div>
				<?php } ?>
				
				<?php /*$order_status = $this->tplVar['table']['record'][0]['status']; */ ?>
				<div class="button submit"><input type="button" value="修改訂單狀態" id="orderstatus" class="submit"></div>
				
				<div class="button submit"><input type="button" value="列印" id="print" class="submit" onclick="printScreen(printdata);"></div>
				<div class="clear"></div>
			</div>
		</form>
			
		<article class="message center" id="msg-dialog">
				<header>
					<h2>
						<span class="msg" id="msg-title">訊息窗</span>
						<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
					</h2>
				</header>
				<div class="entry">
					<p id="msg">Message content</p>
				</div>
			</article>
			
		<div id="printdata" style="display:none;">
			<table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all" cellpadding='5';>
				<tr>
					<td><label for="type">訂單類別</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['type']; ?></td>
				</tr>
				<tr>
					<td><label for="orderid">訂單編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['orderid']; ?></td>
				</tr>
				<tr>
					<td><label for="status">訂單狀態：</label></td>
					<td>
						<?php 
						$order_status = $this->tplVar['table']['record'][0]['status'];
						if($order_status=='1'){ echo '配送中'; }
						elseif($order_status=='2'){ echo '退費中'; }
						elseif($order_status=='3'){ echo '已發送'; }
						elseif($order_status=='4'){ echo '己到貨'; }
						elseif($order_status=='5'){ echo '缺貨己退費'; }
						elseif($order_status=='6'){ echo '不出貨'; }
						else{ echo '處理中'; }
						?>
					</td>
				</tr>
				<tr>
					<td><label for="insertt">訂單日期：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['insertt']; ?></td>
				</tr>
				<tr>
					<td><label for="point_price">商品名稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['epname']; ?></td>
				</tr>
				<tr>
					<td><label for="point_price">商品單價 (繳費應付金額)：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['point_price']; ?></td>
				</tr>
				<tr>
					<td><label for="point_price">得標處理費：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['process_fee']; ?></td>
				</tr>	
				<tr>
					<td><label for="discount_fee">下標處理費：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['discount_fee']; ?></td>
				</tr>
				<tr>
					<td><label for="free_amount">包含免費金額：</label></td>
					<td><?php echo number_format(abs($this->tplVar['table']['rt']['free_history']['free_amount']), 4); ?></td>
				</tr>				
				<tr>
					<td><label for="retail_price">應補差額：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['total_fee']; ?></td>
				</tr>
				<tr>
					<td><label for="thumbnail">發票：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/invoice/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata'])){ ?>
				
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata']['bankid'])){ ?>
				<tr>
					<td><label for="retail_price">銀行資枓：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['bankid']; ?> - <?php echo $this->tplVar['table']['record'][0]['paydata']['bkname']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">銀行分行：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['branch']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">戶名：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['account_name']; ?></td>
				</tr>
				<?php } ?>
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata']['toid'])){ ?>
				<tr>
					<td><label for="retail_price">電信資料：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['toname']; ?> <?php echo $this->tplVar['table']['record'][0]['paydata']['tpname']; ?></td>
				</tr>
				<?php } ?>
				<tr>
					<td><label for="retail_price">銷帳資料：</label></td>
					<td><?php if ($this->tplVar['table']['record'][0]['paydata']['pcode']) { echo $this->tplVar['table']['record'][0]['paydata']['pcode'].' - '; } ?><?php echo $this->tplVar['table']['record'][0]['paydata']['number']; ?></td>
				</tr>
				<?php if (!empty($this->tplVar['table']['record'][0]['paydata']['idnumber'])){ ?>
				<tr>
					<td><label for="retail_price">用戶身份證字號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['paydata']['idnumber']; ?></td>
				</tr>
				<?php } ?>
				
				<?php } ?>
				
			</table>
			
			<table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all" cellpadding='5';>
				<tr>
					<td><label for="retail_price">會員：</label></td>
					<td>
                        <?php echo $this->tplVar['table']['record'][0]['uname']; ?>
                        ( <?php echo $this->tplVar['table']['record'][0]['userid']; ?> )
                    </td>
				</tr>
				<tr>
					<td><label for="retail_price">會員名稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['nickname']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">手機驗證：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['smsphone']; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">收件人(電話)：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['consignee'] .' ('. $this->tplVar['table']['record'][0]['phone'] .')'; ?></td>
				</tr>
				<tr>
					<td><label for="retail_price">收件人郵編,地址：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['zip'] .' '. $this->tplVar['table']['record'][0]['address']; ?></td>
				</tr>
				<tr>
					<td><label>qq號碼：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['qqno']; ?></td>
				</tr>
				<tr>
					<td><label>訂單備註:</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['payname']; ?></td>
				</tr>
				<tr>
					<td><label>管理員備註:</label></td>
					<td><?php echo $this->tplVar['table']['rt']['stat_memo'];?></td>
				</tr>
			</table>
			
			<?php 
			if(!empty($this->tplVar['table']['rt']['shipping']) ){
			$shipping = $this->tplVar['table']['rt']['shipping'];
			?>
			<table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all" cellpadding='5';>
				<tr>
					<td><label for="retail_price">出貨訊息：</label></td>
					<td>
						出貨時間：<?php echo $shipping['outtime'];?><br>
						配送方式：<?php echo $shipping['outtype'];?><br>
						物流單號：<?php echo $shipping['outcode'];?><br>
						備    註：<?php echo $shipping['outmemo'];?><br>
						
					</td>
				</tr>
				<?php if ($order_status == 2){ ?>
				<tr>
					<td><label for="retail_price">退費中訊息：</label></td>
					<td>
						預計退費時間：<?php echo $shipping['backtime'];?><br>
						退費原因：<?php echo $shipping['backmemo'];?>
					</td>
				</tr>
				<?php } else { ?>
				<tr>
					<td><label for="retail_price">退費中訊息：</label></td>
					<td>
						預計退費時間：<?php echo $shipping['backtime'];?><br>
						退費原因：<?php echo $shipping['backmemo'];?>
					</td>
				</tr>
				<tr>
					<td><label for="retail_price">退費訊完成訊息：</label></td>
					<td>
						退費時間：<?php echo $shipping['returntime'];?><br>
						退費原因：<?php echo $shipping['returnmemo'];?>
					</td>
				</tr>
				<?php } ?>
				<tr>
					<td><label for="retail_price">到貨訊息：</label></td>
					<td>
						到貨時間：<?php echo $shipping['closetime'];?><br>
						備    註：<?php echo $shipping['closememo'];?>
					</td>
				</tr>
			</table>
			<?php } ?>
			
		</div>	
		
	</body>
</html>

<script type="text/javascript">
function printScreen(printlist)
  {
     var value = printlist.innerHTML;
     var printPage = window.open("", "Printing...", "");
     printPage.document.open();
     printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
     printPage.document.write("<PRE>");
     printPage.document.write(value);
     printPage.document.write("</PRE>");
     printPage.document.close("</BODY></HTML>");
  }
</script>