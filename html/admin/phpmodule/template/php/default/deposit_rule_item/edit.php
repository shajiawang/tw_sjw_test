<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="driid">規則項目ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['driid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="amount">儲值項目價格：</label></td>
					<td><input name="amount" type="text" value="<?php echo $this->tplVar['table']['record'][0]['amount']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="spoint">儲值殺幣點數：</label></td>
					<td><input name="spoint" type="text" value="<?php echo $this->tplVar['table']['record'][0]['spoint']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="drid">儲值規則ID：</label></td>
					<td><select name="drid">
						<?php foreach ($this->tplVar['table']['rt']['deposit_rule'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['drid'];?>" <?php if($this->tplVar['table']['record'][0]['drid']==$pv['drid']){ echo "selected"; }?> ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="countryid">國別ID：</label></td>
					<td><select name="countryid">
						<?php foreach ($this->tplVar['table']['rt']['country_rt'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['countryid'];?>" <?php if($this->tplVar['table']['record'][0]['countryid']==$pv['countryid']){ echo "selected"; }?> ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="code1">序號1：</label></td>
					<td><input name="code1" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code1']; ?>" size="60"/></td>
				</tr>
				<tr>
					<td><label for="codeformat1">序號格式1：</label></td>
					<td><input name="codeformat1" type="text" value="<?php echo $this->tplVar['table']['record'][0]['codeformat1']; ?>"/>
						條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
					</td>
				</tr>
				<tr>
					<td><label for="code2">序號2：</label></td>
					<td><input name="code2" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code2']; ?>" size="60"/></td>
				</tr>
				<tr>
					<td><label for="codeformat2">序號格式2：</label></td>
					<td><input name="codeformat2" type="text" value="<?php echo $this->tplVar['table']['record'][0]['codeformat2']; ?>"/>
						條碼格式(1.code128 2.QRCode 3.code39 4.code93 5.EAN-13 6.EAN-8 7.UPC-A 8.UPC-E 9.Data-Matrix 10.PDF-417 11.ITF 12.Codabar 13.序號)
					</td>
				</tr>
				<tr>
					<td><label for="code3">序號3：</label></td>
					<td><input name="code3" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code3']; ?>" size="60"/></td>
				</tr>
				<tr>
					<td><label for="code4">序號4：</label></td>
					<td><input name="code4" type="text" value="<?php echo $this->tplVar['table']['record'][0]['code4']; ?>" size="60"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="driid" value="<?php echo $this->tplVar["status"]["get"]["driid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>