<?php
$body = $this->tplVar['table']['record'][0]['body'];

?>

<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="title">推播標題：</label></td>
					<td><input name="title" type="text" data-validate="required" value="<?php echo $this->tplVar['table']['record'][0]['title']; ?>"/> 商品名稱 請勿輸入 單引號' 雙引號" 右斜線\ (系統儲存時將會自動過濾移除)</td>
				</tr>
				<tr>
					<td><label for="body">推播內容：</label></td>
					<td>
						<select name="body[]">
							<option value="">無</option>
							<option value="{nickname}"    >使用者名稱</option>
							<option value="{winner}"      >得標者名稱</option>
							<option value="{productname}" >商品名稱</option>
						</select>

						<input type="text" name="body[]" value="" style="width: 250px;">

						<select name="body[]">
							<option value="">無</option>
							<option value="{nickname}"    >使用者名稱</option>
							<option value="{winner}"      >得標者名稱</option>
							<option value="{productname}" >商品名稱</option>
						</select>
					
						<input type="text" name="body[]" value="" style="width: 250px;">

						<select name="body[]">
							<option value="">無</option>
							<option value="{nickname}"    >使用者名稱</option>
							<option value="{winner}"      >得標者名稱</option>
							<option value="{productname}" >商品名稱</option>
						</select>
						<br>
						推播內容 請勿輸入 分號 " ； "
					</td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="text" value="<?php echo($body)?>" readonly="readonly" style="width: 855px;">
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="pushid" value="<?php echo $this->tplVar["status"]["get"]["pushid"] ;?>">
				<div class="button submit"><input name="edit_item" id="edit_item" type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
			
		</form>
		<div class="dialogs"></div>
	</body>
</html>
