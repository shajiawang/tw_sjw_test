<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	    <script type="text/javascript">
	    	//區域
			function country(id) {
				$('select[name="regionid"] option').hide();
				$('select[name="regionid"]').val("");
				$('select[name="regionid"] option[rel=""]').show();
				$('select[name="regionid"] option[rel='+id+']').show();
				
				region(id);
				province(id);
			}
	    	
	    	//省(直轄市)
			function region(id) {
				$('select[name="provinceid"] option').hide();
				$('select[name="provinceid"]').val("");
				$('select[name="provinceid"] option[rel=""]').show();
				$('select[name="provinceid"] option[rel='+id+']').show();
				
				province($('select[name="regionid"] option').val());
			}
			
			//(分區)
			function province(id) {
				$('select[name="channel"] option').hide();
				$('select[name="channel"]').val("");
				$('select[name="channel"] option[rel=""]').show();
				$('select[name="channel"] option[rel='+id+']').show();
			}
	    	
			$(function(){
				$('select[name="countryid"] option[value="<?php echo $this->config->country; ?>"]').prop('selected', true);
				$('select[name="regionid"] option').hide();
				$('select[name="regionid"] option[rel=""]').show();
				$('select[name="regionid"] option[rel="1"]').show();
				// $('select[name="regionid"] option[value="<?php echo $this->config->region; ?>"]').prop('selected', true);
				$('select[name="provinceid"] option').hide();
				$('select[name="provinceid"] option[rel=""]').show();
				$('select[name="provinceid"] option[rel="8"]').show();
				// $('select[name="provinceid"] option[value="<?php echo $this->config->province; ?>"]').prop('selected', true);
				$('select[name="channel"] option').hide();
				$('select[name="channel"] option[rel=""]').show();
				$('select[name="channel"] option[rel="8"]').show();
				// $('select[name="channel"] option[value="<?php echo $this->config->channel; ?>"]').prop('selected', true);
			});
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">直播主</a>>>
			<a>新增</a>	
		</div>		
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				
				<tr>
					<td><label for="name">直播主名稱：</label></td>
					<td><input name="lbname" type="text"/></td>
				</tr>
				<tr>
					<td><label for="phone">直播主手機號：</label></td>
					<td><input name="phone" type="text"/></td>
				</tr>
				<tr>
					<td><label for="gender">性別：</label></td>
					<td><select name="gender">
							<option value="male" selected>男</option>
							<option value="female" >女</option>
						</select></td>
				</tr>
				<tr>
					<td><label for="userid">直播主會員編號：</label></td>
					<td><input name="userid" type="number" value="0" /></td>
				</tr>
				<tr>
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo date("Y-m-d H:i", strtotime("+1 day")); ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo date("Y-m-d H:i", strtotime("+1 year")); ?>" data-org="<?php echo date("Y-m-d H:i", strtotime("+1 year")); ?>"/></td>
				</tr>				
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="0"/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="thumbnail">直播主美照：</label></td>
					<td><input name="thumbnail" type="file"/></td>
				</tr>			
				<tr>
					<td><label for="countryid">國家：</label></td>
					<td>
						<select name="countryid" onchange="country(this.value);">
							<option value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['country'])) {
									foreach ($this->tplVar['table']['rt']['country'] as $key => $value) {
										echo '<option value="'.$value['countryid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="regionid">區域：</label></td>
					<td>
						<!--select name="regionid" onchange="region(this.value);"-->
						<select name="regionid">
							<option rel="" value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['region'])) {
									foreach ($this->tplVar['table']['rt']['region'] as $key => $value) {
										echo '<option rel="'.$value['countryid'].'" value="'.$value['regionid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="provinceid">省(直轄市)：</label></td>
					<td>
						<select name="provinceid" onchange="province(this.value);">
							<option rel="" value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['province'])) {
									foreach ($this->tplVar['table']['rt']['province'] as $key => $value) {
										echo '<option rel="'.$value['countryid'].'" value="'.$value['provinceid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="channel">地區：</label></td>
					<td>
						<select name="channel">
							<option rel="" value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['channel'])) {
									foreach ($this->tplVar['table']['rt']['channel'] as $key => $value) {
										echo '<option rel="'.$value['provinceid'].'" value="'.$value['channelid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>