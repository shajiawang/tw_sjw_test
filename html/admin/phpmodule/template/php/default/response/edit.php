<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">自動回覆關鍵字：</label></td>
					<td>
						<div valign="top" id="keyword">
						<?php
							if (!empty($this->tplVar['table']['record'][0]['keyword'])) {
								$keyword_array = explode(",", $this->tplVar['table']['record'][0]['keyword']);
							}
							if (is_array($keyword_array)) {
								foreach ($keyword_array as $key => $value)
								{
									//echo $key;
									if ($key == 0) {
										echo '<b>關鍵字：<input name="keyword['.($key + 1).']" type="text" class="required" value="'.$value.'"/></b>';
									}
									else if ($key % 5 == 4) {
										echo '<b>關鍵字：<input name="keyword['.($key + 1).']" type="text" value="'.$value.'"/></b><br />';
									}
									else {
										echo '<b>關鍵字：<input name="keyword['.($key + 1).']" type="text" value="'.$value.'"/></b>';
									}
								}
							}
						?>
						</div>
						<b><input name="morekeyword" id="morekeyword" type="button" value="新增"/></b>
					</td>
				</tr>
				<script language="javascript" type="text/javascript">
					var keywordid = <?php echo count($keyword_array); ?>;
					$("#morekeyword").click(function() {
						keywordid++;
						if (parseInt(keywordid) % 5 == 0) {
							$("#keyword").append('<b>關鍵字：<input name="keyword[' + keywordid + ']" type="text"/></b><br />').faceIn('10000');
						}
						else {
							$("#keyword").append('<b>關鍵字：<input name="keyword[' + keywordid + ']" type="text"/></b>').faceIn('10000');
						}
						
					});
					
				</script>
				<tr>
					<td><label for="description">自動回覆敘述：</label></td>
					<td><textarea name="description" class="description" cols="40"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="fcid">回覆方式：</label></td>
					<td>
						<select name="msgtype">
						<?php
							foreach($this->tplVar['msgtype_array'] as $key => $value) {
								if ($key == $this->tplVar["table"]['record'][0]['msgtype']) {
									echo '<option value="'.$key.'" selected>'.$value.'</option>';
								}
								else {
									echo '<option value="'.$key.'">'.$value.'</option>';
								}
							}
						?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="name">自動回覆名稱：</label></td>
					<td><input name="name" type="text" data-validate="required" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">自動回覆敘述：</label></td>
					<td><textarea name="description" class="description" cols="40"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail_url">自動回覆主圖(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="url">連結網址：</label></td>
					<td><input name="url" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['url']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">自動回覆排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">自動回覆顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['display']=="Y"?" selected":""; ?>>顯示</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['display']=="N"?" selected":""; ?>>不顯示</option>
						</select>
					</td>
				</tr>
			</table>
			<table class="form-table">
				<tr>
					<td><label for="description">自動回覆分類：</label></td>
					<td>
						<ul class="relation-list">
						<?php foreach($this->tplVar['table']['rt']['response_category_rt'] as $rck => $rcv) : ?>
							<li>
								<?php if (empty($rcv['responseid'])) : ?>
								<input class="relation-item" type="checkbox" name="rcid[]" id="rcid_<?php echo $rck; ?>" value="<?php echo $rcv['rcid']; ?>"/>
								<?php else : ?>
								<input class="relation-item" type="checkbox" name="rcid[]" id="rcid_<?php echo $rck; ?>" value="<?php echo $rcv['rcid']; ?>" checked/>
								<?php endif; ?>
								<span class="relation-item-name" for="rcid_<?php echo $rck; ?>"><?php echo $rcv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="responseid" value="<?php echo $this->tplVar["status"]["get"]["responseid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>