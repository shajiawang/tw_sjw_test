<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">自動回覆關鍵字：</label></td>
					<td>
						<div valign="top" id="keyword">
							<b>關鍵字：<input name="keyword[1]" type="text" class="required"/></b>
						</div>
						<b><input name="morekeyword" id="morekeyword" type="button" value="新增"/></b>
					</td>
				</tr>
				<script language="javascript" type="text/javascript">
					var keywordid = 1;
					$("#morekeyword").click(function() {
						keywordid++;
						if (parseInt(keywordid) % 5 == 0) {
							$("#keyword").append('<b>關鍵字：<input name="keyword[' + keywordid + ']" type="text"/></b><br />').faceIn('10000');
						}
						else {
							$("#keyword").append('<b>關鍵字：<input name="keyword[' + keywordid + ']" type="text"/></b>').faceIn('10000');
						}
					});
				</script>
				<tr>
					<td><label for="rcid">回覆方式：</label></td>
					<td>
						<select name="msgtype">
						<?php foreach($this->tplVar['msgtype_array'] as $key => $value) : ?>
							<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="name">自動回覆主題：</label></td>
					<td><input name="name" type="text" class="required"/></td>
				</tr>
				<tr>
					<td><label for="description">自動回覆敘述：</label></td>
					<td><textarea name="description" class="description" cols="40"></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail_url">自動回覆主圖(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="url">連結網址：</label></td>
					<td><input name="url" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="seq">自動回覆排序：</label></td>
					<td><input name="seq" type="text" value="0"/></td>
				</tr>
				<tr>
					<td><label for="seq">自動回覆顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y">顯示</option>
							<option value="N" checked>不顯示</option>
						</select>
					</td>
				</tr>
			</table>
			<table class="form-table">
				<tr>
					<td><label for="description">商品種類：</label></td>
					<td>
						<ul class="relation-list">
						<?php foreach($this->tplVar['table']['rt']['response_category'] as $rck => $rcv) : ?>
							<li>
								<input class="relation-item" type="checkbox" name="rcid[]" id="rcid_<?php echo $rck; ?>" value="<?php echo $rcv['rcid']; ?>"/>
								<span class="relation-item-name" for="rcid_<?php echo $rck; ?>"><?php echo $rcv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>