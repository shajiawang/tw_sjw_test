<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">自動回覆管理</a>>>
			<a>自動回覆</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_category">分類：</span>
					<select name="search_category">
						<option value="">All</option>
						<?php
							if (is_array($this->tplVar["table"]["rt"]["response_category"])) {
								foreach ($this->tplVar["table"]["rt"]["response_category"] as $key => $value) {
									if ($this->io->input['get']['search_category'] == $value['rcid']) {
										echo '<option value="'.$value['rcid'].'" selected>'.$value['name'].'</option>';
									}
									else {
										echo '<option value="'.$value['rcid'].'">'.$value['name'].'</option>';
									}
								}
							}
						?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_keyword">關鍵字：</span>
					<input type="text" name="search_keyword" size="20" value="<?php echo $this->io->input['get']['search_keyword']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">主題：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<th>分類</th>
								<th>關鍵字</th>
								<th>回覆方式</th>
								<th>主題</th>
								<th>內容</th>
								<!--<th>圖片連結</th>
								<th>連結</th>-->
								<th>使用次數</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_seq"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_seq=desc">排序<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_seq"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=">排序<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=asc">排序<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php
								if (is_array($this->tplVar["table"]['record'])) {
									foreach($this->tplVar["table"]['record'] as $rk => $rv) {
										$TypeName = ""; 
										if (is_array($this->tplVar["table"]["rt"]["response_category_rt"])) {
											foreach ($this->tplVar["table"]["rt"]["response_category_rt"] as $rtkey => $rtvalue)
											{
												if (is_array($this->tplVar["table"]["rt"]["response_category"])) {
													foreach ($this->tplVar["table"]["rt"]["response_category"] as $key => $value) {
														if (($rv['responseid'] == $rtvalue['responseid']) && ($rtvalue['rcid'] == $value['rcid'])) {
															$TypeName .= $value['name']." ";
														}
													}
												}
											}
										}
							?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/responseid=<?php echo $rv["responseid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/responseid=<?php echo $rv["responseid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td>
								<td class="column" name="TypeName"><?php echo $TypeName; ?></td>
								<td class="column" name="keyword"><?php echo $rv['keyword']; ?></td>
								<td class="column" name="msgtype"><?php echo $this->tplVar["msgtype_array"][$rv['msgtype']]; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="description"><?php echo mb_substr($rv['description'], 0, 20, 'utf-8')."..."; ?></td>
								<!--<td class="column" name="thumbnail_url"><?php echo $rv['thumbnail_url']; ?></td>
								<td class="column" name="url"><?php echo $rv['url']; ?></td>-->
								<td class="column" name="seq"><?php echo rand(1000, 3500); ?></td>
								<td class="column" name="seq"><?php echo $rv['seq']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>