<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
			<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_productid">商品編號：</span>
				    <input type="text" name="productid" value="<?php echo $this->io->input['get']['productid']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_pdtname">商品名稱：</span>
				    <input type="text" name="pdtname" value="<?php echo $this->io->input['get']['pdtname']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_ontime">商品上架日期：</span>
					<input name="ontimefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['ontimefrom']; ?>" /> ~ 
					<input name="ontimeto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['ontimeto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_offtime">商品結標日期：</span>
					<input name="offtimefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['offtimefrom']; ?>" /> ~ 
					<input name="offtimeto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['offtimeto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_closed">流結標：</span>
					<select name="closed" id="closed">
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['closed'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['closed'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_price">得標金額：</span>
					<input type="text" name="price" size="20" value="<?php echo $this->io->input['get']['price']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_retail_price">商品市價：</span>
					<input type="text" name="retail_price" size="20" value="<?php echo $this->io->input['get']['retail_price']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_depositfrom">商品類型：</span>
					<select name="ptype" id="ptype">
						<option value="">全部</option>
						<option value="0" <?php echo ($this->io->input['get']['ptype'] == 0) ? "selected" : "";?> >殺戮商品</option>
						<option value="1" <?php echo ($this->io->input['get']['ptype'] == 1) ? "selected" : "";?> >圓夢商品</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_depositfrom">結帳狀態：</span>
					<select name="complete" id="complete">
						<option value="">全部</option>
						<option value="Y" <?php echo ($this->io->input['get']['complete'] == 'Y') ? "selected" : "";?> >已結帳</option>
						<option value="N" <?php echo ($this->io->input['get']['complete'] == 'N') ? "selected" : "";?> >未結帳</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_history_count">下標次數：</span>
					<input type="text" name="history_count" size="20" value="<?php echo $this->io->input['get']['history_count']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="number" name="userid" size="20" value="<?php echo $this->io->input['get']['userid']; ?>" />
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
                </ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>	
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/productpic/" >當年度報表</a>						
						</div>							
					</div>
				</li>
				<li class="body">
					<?php if (isset($_GET['ptype'])){?>
					<table>
						<thead>
							<tr>
								<th>商品編號</th>
								<th>商品名稱</th>
								<th>商品上架日期</th>
								<th>商品結標日期</th>
								<th>流結標</th>
								<th>得標金額</th>
								<th>商品市價</th>
								<th>得標處理費</th>
								<th>點擊數</th>
								<th>商品類型</th>
								<?php if ($this->io->input['session']['user']['department'] == 'S' || $this->tplVar['status']['closed'][$rv['closed']] != '未結標') { ?>
								<th>下標次數</th>
								<?php } ?>
								<th>會員編號</th>
								<th>會員暱稱</th>
								<?php //if ($this->io->input['session']['user']['department'] == 'S'  || $this->tplVar['status']['closed'][$rv['closed']] != '未結標') { ?>
								<!--th>得標者成本</th-->
								<?php //} ?>
								<?php if ($this->io->input['session']['user']['department'] == 'S'  || $this->tplVar['status']['closed'][$rv['closed']] != '未結標') { ?>
								<th>下標費用</th>
								<?php } ?>
								<th>得標者結帳</th>
								<th>訂單編號</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="productid"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/productview/?productid=<?php echo $rv['productid']; ?>" ><?php echo $rv['productid']; ?></a></td>
								<td class="column" name="pdtname"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/historylist/?productid=<?php echo $rv['productid']; ?>" ><?php echo $rv['pdtname']; ?></a></td>
								<td class="column" name="ontime"><?php echo $rv['ontime']; ?></td>
								<td class="column" name="offtime"><?php echo $rv['offtime']; ?></td>
								<td class="column" name="closed"><?php echo $this->tplVar['status']['closed'][$rv['closed']]; ?></td>
								<td class="column" name="price"><?php echo round($rv['price']); ?></td>
								<td class="column" name="retail_price"><?php echo round($rv['retail_price']); ?></td>
								<td class="column" name="getbidp"><?php echo round($rv['retail_price'] * $rv['process_fee'] * 0.01); ?></td>
								<td class="column" name="hot_prod"><?php echo $rv['hot_prod']; ?></td>
								<td class="column" name="ptype"><?php if ($rv['ptype'] == 0){ echo "殺戮商品"; }else{ echo "圓夢商品"; } ?></td>
								<?php if ($this->io->input['session']['user']['department'] == 'S' || $this->tplVar['status']['closed'][$rv['closed']] != '未結標') { ?>
								<td class="column" name="history_count"><?php echo $rv['history_count']; ?></td>
								<?php }else{ ?>
								<td class="column" name="history_count">0</td-->
								<?php } ?>
								<td class="column" name="userid"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/userview/?userid=<?php echo $rv['userid']; ?>" ><?php echo $rv['userid']; ?></a></td>
								<td class="column" name="nickname"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/userview/?userid=<?php echo $rv['userid']; ?>" ><?php echo urldecode($rv['nickname']); ?></a></td>								
								<?php //if ($this->io->input['session']['user']['department'] == 'S' || $this->tplVar['status']['closed'][$rv['closed']] != '未結標') { ?>
								<!--td class="column" name="bidcost"><?php echo ceil($rv['bidcost']); ?></td>
								<?php //}else{ ?>
								<td class="column" name="bidcost">0</td-->
								<?php //} ?>
								<?php if ($this->io->input['session']['user']['department'] == 'S' || $this->tplVar['status']['closed'][$rv['closed']] != '未結標') { ?>
								<td class="column" name="alltotal"><?php echo ceil($rv['alltotal']); ?></td>
								<?php }else{ ?>
								<td class="column" name="alltotal">0</td>
								<?php } ?>								
								<td class="column" name="complete"><?php if ($rv['complete'] == 'Y'){ echo "已結帳"; }else{ echo "未結帳"; } ?></td>
								<td class="column" name="orderid"><a href="<?php echo $this->config->default_main.'/order'; ?>/edit/orderid=<?php echo $rv["orderid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"><?php echo $rv['orderid']; ?></a></td>								
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					<?php
					}else{
						echo "<table><tr><td align='center'>請先輸入條件後進行查詢</td></tr></table>";	
					}
					?>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>