<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>下標商品統計</a>
		</div>
		<form class="form">
			<div class="form-label">商品資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">商品名稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="name">商品分類：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['pcname']; ?></td>
				</tr>
				<!--<tr>
					<td><label for="nickname">商品敘述：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['description']; ?></td>
				</tr>-->
				<tr>
					<td><label for="description">商品市價：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['retail_price']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">上架時間：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['ontime']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">下架時間：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['offtime']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">下標手續費：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['saja_fee']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">得標處理費：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['retail_price'] * $this->tplVar['table']['record'][0]['process_fee'] * 0.01; ?></td>
				</tr>
				<tr>
					<td><label for="phone">結流標：</label></td>
					<td><?php echo $this->tplVar['status']['closed'][$this->tplVar['table']['record'][0]['closed']]; ?></td>
				</tr>
				<tr>
					<td><label for="phone">建立日期：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['insertt']; ?></td>
				</tr>
			</table>
			
			<div class="functions">
				<div class="button submit"><input type="button" value="回上一頁" class="button" onclick="location.href='<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>';"></div>
				<!--<div class="button submit"><input type="button" value="回上一頁" class="button" onclick="history.back();"></div>-->
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>