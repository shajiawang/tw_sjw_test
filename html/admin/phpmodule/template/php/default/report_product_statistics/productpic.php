<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
       <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/echarts/echarts.min.js"></script>
       <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/echarts-gl/echarts-gl.min.js"></script>
       <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/echarts-stat/ecStat.min.js"></script>
       <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/echarts/extension/dataTool.min.js"></script>
       <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/echarts/map/js/china.js"></script>
       <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/echarts/map/js/world.js"></script>
	   <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/echarts/extension/bmap.min.js"></script>
       <script type="text/javascript" src="https://echarts.baidu.com/gallery/vendors/simplex.js"></script>	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>下標商品</a> >> 2019年度
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					   <div id="container" style="height: 800px"></div>
					   <script type="text/javascript">
							var dom = document.getElementById("container");
							var myChart = echarts.init(dom);
							var app = {};
							option = null;
							app.title = '折柱混合';

							option = {
								tooltip: {
									trigger: 'axis',
									axisPointer: {
										type: 'cross',
										crossStyle: {
											color: '#999'
										}
									}
								},
								toolbox: {
									feature: {
										dataView: {show: true, readOnly: false},
										magicType: {show: true, type: ['line', 'bar']},
										restore: {show: true},
										saveAsImage: {show: true}
									}
								},
								legend: {
									data:['結標','流標','競標中']
								},
								xAxis: [
									{
										type: 'category',
										data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
										axisPointer: {
											type: 'shadow'
										}
									}
								],
								yAxis: [
									{
										type: 'value',
										name: '數量',
										min: 0,
										max: <?php echo str_pad(1,strlen($this->tplVar['status']['alltotal']),'0',STR_PAD_RIGHT);?>,
										interval: <?php echo round((str_pad(1,strlen($this->tplVar['status']['alltotal']),'0',STR_PAD_RIGHT))/10);?>,
										axisLabel: {
											formatter: '{value}'
										}
									}
								],
								series: [
									{
										name:'結標',
										type:'bar',
										<?php 
											foreach ($this->tplVar["table"]['record'] as $key2 => $value2) : 
											if ($value2['closed']=='Y'){
										?>										
										data:[<?php echo substr($value2['total'], 0, -1);?>]
										<?php
											}
											endforeach;
										?>	
									},
									{
										name:'流標',
										type:'bar',
										<?php 
											foreach ($this->tplVar["table"]['record'] as $key2 => $value2) : 
											if ($value2['closed']=='NB'){
										?>										
										data:[<?php echo substr($value2['total'], 0, -1);?>]
										<?php
											}
											endforeach;
										?>	
									},
									{
										name:'競標中',
										type:'bar',
										<?php 
											foreach ($this->tplVar["table"]['record'] as $key2 => $value2) : 
											if ($value2['closed']=='N'){
										?>										
										data:[<?php echo substr($value2['total'], 0, -1);?>]
										<?php
											}
											endforeach;
										?>	
									}
									
								]
							};
							;
							if (option && typeof option === "object") {
								myChart.setOption(option, true);
							}
					   </script>

				</li>

				
			</ul>
		</div>
	</body>
</html>