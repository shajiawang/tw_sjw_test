<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>下標商品統計</a>
		</div>
		<div class="searchbar header-style">
			<ul>&nbsp;</ul>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
　
					</div>
				</li>
				<li class="body">  
					<table>
						<thead>
							<tr>
								<th>總下標總筆數</th>
								<th>總殺價幣下標筆數</th>  
								<th>總超級殺價卷下標筆數</th>
								<th>總殺價卷下標筆數</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="column" name="pdtname"><?php echo $this->tplVar["table"]['rt']['all_bid']['all_bid'];?></td>
								<td class="column" name="type"><?php echo $this->tplVar["table"]['rt']['all_bid']['all_bid_spoint'];?></td>
								<td class="column" name="type"><?php echo $this->tplVar["table"]['rt']['all_bid']['all_bid_scode'];?></td>
								<td class="column" name="type"><?php echo $this->tplVar["table"]['rt']['all_bid']['all_bid_oscode'];?></td>
							</tr>
						</tbody>
						<thead>
							<tr>
								<th>得標者總下標筆數</th> 
								<th>得標者總殺價幣下標筆數</th> 
								<th>得標者總超級殺價卷下標筆數</th> 
								<th>得標者總殺價卷下標筆數</th> 
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="column" name="type"><?php echo $this->tplVar["table"]['rt']['get_bid']['get_bid'];?></td>
								<td class="column" name="type"><?php echo $this->tplVar["table"]['rt']['get_bid']['get_bid_spoint'];?></td>
								<td class="column" name="type"><?php echo $this->tplVar["table"]['rt']['get_bid']['get_bid_scode'];?></td>
								<td class="column" name="type"><?php echo $this->tplVar["table"]['rt']['get_bid']['get_bid_oscode'];?></td>  
							</tr>
						</tbody>
					</table>
				</li>
			</ul>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/">回上一頁</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>競標商品</th>
								<th>類別</th>
								<th>競標方式</th>
								<th>會員編號</th>
								<th>會員暱稱</th>
								<th>競標價位</th>
								<th>下標總費用</th>
								<th>建立時間</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="pdtname"><?php echo $rv['pdtname']; ?></td>
								<td class="column" name="type"><?php echo $rv['type']; ?></td>
								<td class="column" name="historytype"><?php if ($rv['spointid'] != 0) { echo '殺幣'; } elseif ($rv['scodeid'] != 0) { echo '超級殺價卷'; } elseif ($rv['oscodeid'] != 0) { echo '殺價卷'; } else { echo '禮卷'; } ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="price"><?php echo $rv['price']; ?></td>
								<td class="column" name="bid_total"><?php echo $rv['bid_total']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>