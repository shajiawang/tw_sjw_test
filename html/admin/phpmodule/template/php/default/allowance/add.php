<?php
    $arrAllowanceFor=array("O"=>"訂單","I"=>"發票");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">折讓資料管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/preview">
			<div class="form-label">新增折讓單</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">用戶編號：</label></td>
					<td><input name="userid" id="userid" type="text" /></td>
					<td><label for="allowance_amt">折讓總金額(含稅)：</label></td>
					<td><input name="allowance_amt" id="allowance_amt" type="number" /></td>
				</tr>
				<tr>
					<td><label for="allowance_for">(選填) 關聯單據：</label></td>
					<td colspan="3" >
					    <select name="allowance_for" id="allowance_for">
						  <option value="">無</option>
						  <option value="O">訂單</option>
						  <option value="I">發票</option>
						</select>
						&nbsp;單據編號 : 
						<input name="objid" id="objid" type="text" value="" />
					</td>
				</tr>
                <tr>
					<td><label for="memo">(選填) 備註：</label></td>
					<td colspan="3" ><textarea name="memo" id="memo" rows="8" cols="64" value="" ></textarea></td>
				</tr>
			</table>


			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value=" 預覽 " class="submit"></div>
				<!-- div class="button submit"><input type="submit" value="送出" class="submit"></div -->
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>