<?php
    $arrAllowanceFor=array("O"=>"訂單");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">折讓單</a>>><a>詳細資料</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">折讓單資料</div>
			<table class="form-table">
				<tr>
				   <td><label for="salid">序號：</label></td>
					<td><input name="salid" type="hidden"  value="<?php echo $this->tplVar['table']['record'][0]['salid']; ?>"/>
					    <?php echo $this->tplVar['table']['record'][0]['salid']; ?>
					</td>
				
					<td><label for="allowance_no">折讓單號：</label></td>
					<td><input name="allowance_no" type="hidden" data-validate="required" value="<?php echo $this->tplVar['table']['record'][0]['allowance_no']; ?>"/>
					    <?php echo $this->tplVar['table']['record'][0]['allowance_no']; ?>
					</td>
				</tr>
				<tr>
					<td><label for="allowance_date">折讓日期：</label></td>
					<td><input name="allowance_date" type="text" class="datetime" value="<?php echo $this->tplVar['table']['record'][0]['allowance_date']; ?>" />
					    <?php // echo $this->tplVar['table']['record'][0]['allowance_date']; ?>
					</td>
					<td><label for="switch">資料狀態：</label></td>
					<td>
						<select name="switch">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>有效(Y)</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>無效(N)</option>
						</select>
					</td>
				 </tr>
				 <tr>
					<td><label for="allowance_amt">折讓金額(含稅)：</label></td>
					<td>
					    <!-- input name="allowance_amt" type="number" value="<?php echo $this->tplVar['table']['record'][0]['allowance_amt']; ?>"/ -->
						<?php echo $this->tplVar['table']['record'][0]['allowance_amt']; ?>
					</td>
               				
				   <td><label for="allowance_tax_amt">折讓稅額：</label></td>
					<td>
					   <!-- input name="allowance_tax_amt" type="number" value="<?php echo $this->tplVar['table']['record'][0]['allowance_tax_amt']; ?>"/ -->
					   <?php echo $this->tplVar['table']['record'][0]['allowance_tax_amt']; ?>
					</td>
				</tr>
                <tr>
					<td><label for="userid">用戶編號：</label></td>
					<td>
					   <input name="userid" type="text" data-validate="" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>"/>
					</td>
					<td><label for="invoice_buyer_name">用戶名稱：</label></td>
					<td>
					    <input name="invoice_buyer_name" type="text" data-validate="" value="<?php echo $this->tplVar['table']['record'][0]['invoice_buyer_name']; ?>"/>
					</td>
				</tr>
                <tr>
				    <td><label for="allowance_type">折讓類型：</label></td>
					<td>
						<select name="allowance_type" id="allowance_type">
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['allowance_type']=="1"?" selected":""; ?>>買方開立</option>
							<option value="2" <?php echo $this->tplVar['table']['record'][0]['allowance_type']=="2"?" selected":""; ?>>(預設)賣方折讓</option>
						</select>
					</td>
					<td><label for="allowance_for">關聯單據：</label></td>
					<td>
					  <select name="allowance_for" id="allowance_for" >
					     <option value="N">無</option>
						 <option value="O" <?php echo ($this->tplVar['table']['record'][0]['allowance_for']=='O')?"selected":""; ?> ><?php echo $arrAllowanceFor["O"]; ?></option>
					  </select>
					</td>
				</tr>
				<tr>
 				  <td><label for="objid">關聯單號：</label></td>
					<td>
					  <input name="objid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['objid']; ?>"/>
					</td>
				    <td><label for="upload">處理狀態：</label></td>
					<td>
						<select name="upload">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['upload']=="Y"?" selected":""; ?> >處理完成(Y)</option>
							<option value="P" <?php echo $this->tplVar['table']['record'][0]['upload']=="P"?" selected":""; ?> >已上傳,處理中(P)</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['upload']=="N"?" selected":""; ?> >未上傳(N)</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="memo">備註：</label></td>
					<td colspan="3">
					  <textarea name="memo" id="memo"  rows="8" cols="64" value="<?php echo $this->tplVar['table']['record'][0]['memo']; ?>" ><?php echo $this->tplVar['table']['record'][0]['memo']; ?>
					  </textarea>
					</td>
				</tr>				
			</table>
            <table>
                <thead>
                    <tr>
                        <th>發票編號</th>
                        <th>發票日期</th>
                        <th>折讓金額(未稅)</th>
						<th>折讓稅額</th>
						<th>折讓總金額(含稅)</th>
                        <th>新增日期</th>
						<th>資料狀態</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  
                         foreach($this->tplVar['table']['record'][0]['items'] as $item) {  
                    ?>
                    <tr>
                        <td class="column" name="ori_invoiceno"><a href="#" onClick="javascript:window.open('/admin/invoice/allowance_detail/?invoiceno=<?php echo $item['ori_invoiceno']; ?>','_blank',config='width=600,height=250,scrollbars=yes');"><?php echo $item['ori_invoiceno']; ?></a></td>
                        <td class="column" name="ori_invoice_date"><?php echo $item['ori_invoice_date']; ?></td>
                        <td class="column" name="unitprice"><?php echo $item['unitprice']; ?></td>
                        <td class="column" name="tax"><?php echo $item['tax']; ?></td>
						<td class="column" name="amount"><?php echo $item['amount']; ?></td>
                        <td class="column" name="insertt"><?php echo $item['insertt']; ?></td>
						<td class="column" name="switch">
						  <?php
						       if($item['switch']=='Y') echo "有效";  
							   else if ($item['switch']=='N') echo "<b><font color='red'>無效</font></b>";
							   else  echo "未知";
						  ?>
						</td>
                    </tr>
                    <?php   }    ?>
                </tbody>
			</table>            
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="button submit"><input name="edit_item" id="edit_item" type="submit" value="提交修改" class="submit"></div>
				<div class="button "><a href="#" onclick="printScreen(printdata);">列印</a></div>
				<div class="clear"></div>
			</div>
			
		</form>
		<div class="dialogs"></div>
		<div id="printdata" style="display:none;" >
			<table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all">
				<tr>
				   <td>序號：</td>
					<td><?php echo $this->tplVar['table']['record'][0]['salid']; ?></td>
				
					<td>折讓單號：</td>
					<td><?php echo $this->tplVar['table']['record'][0]['allowance_no']; ?></td>
				</tr>
				<tr>
					<td>折讓日期：</td>
					<td><?php echo $this->tplVar['table']['record'][0]['allowance_date']; ?></td>
					<td>資料狀態：</td>
					<td><?php if ($this->tplVar['table']['record'][0]['switch']=="Y"){ ?>>有效(Y)<?php }else{ ?>無效(N)<?php }?></td>
				 </tr>
				 <tr>
					<td>折讓金額(含稅)：</td>
					<td><?php echo $this->tplVar['table']['record'][0]['allowance_amt']; ?></td>
               				
				   <td>折讓稅額：</td>
					<td><?php echo $this->tplVar['table']['record'][0]['allowance_tax_amt']; ?></td>
				</tr>
                <tr>
					<td>用戶編號：</td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
					<td>用戶名稱：</td>
					<td><?php echo $this->tplVar['table']['record'][0]['invoice_buyer_name']; ?></td>
				</tr>
                <tr>
				    <td>折讓類型：</td>
					<td><?php if ($this->tplVar['table']['record'][0]['allowance_type']=="1"){ ?>買方開立<?php }else{ ?>(預設)賣方折讓<?php } ?></td>
					<td>關聯單據：</td>
					<td><?php if ($this->tplVar['table']['record'][0]['allowance_for']=='O') { ?><?php echo $arrAllowanceFor["O"]; ?><?php }else{ ?>無<?php } ?></td>
				</tr>
				<tr>
 				  <td>關聯單號：</td>
					<td>
					  <?php echo $this->tplVar['table']['record'][0]['objid']; ?>
					</td>
				    <td>處理狀態：</td>
					<td>
						<?php if ($this->tplVar['table']['record'][0]['upload']=="Y") { ?>處理完成(Y)<?php } ?>
						<?php if ($this->tplVar['table']['record'][0]['upload']=="P") { ?>已上傳,處理中(P)<?php } ?>
						<?php if ($this->tplVar['table']['record'][0]['upload']=="N") { ?>未上傳(N)<?php } ?>
					</td>
				</tr>
				<tr>
					<td>備註：</td>
					<td colspan="3">
					  <?php echo $this->tplVar['table']['record'][0]['memo']; ?>
					</td>
				</tr>
			</table>
            <table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all">
                <thead>
                    <tr>
                        <th>發票編號</th>
                        <th>發票日期</th>
                        <th>折讓金額(未稅)</th>
						<th>折讓稅額</th>
						<th>折讓總金額(含稅)</th>
                        <th>新增日期</th>
						<th>資料狀態</th>
                    </tr>
                </thead>
                <tbody>
                    <?php  
                         foreach($this->tplVar['table']['record'][0]['items'] as $item) {  
                    ?>
                    <tr>
                        <td class="column" name="ori_invoiceno"><?php echo $item['ori_invoiceno']; ?></td>
                        <td class="column" name="ori_invoice_date"><?php echo $item['ori_invoice_date']; ?></td>
                        <td class="column" name="unitprice"><?php echo $item['unitprice']; ?></td>
                        <td class="column" name="tax"><?php echo $item['tax']; ?></td>
						<td class="column" name="amount"><?php echo $item['amount']; ?></td>
                        <td class="column" name="insertt"><?php echo $item['insertt']; ?></td>
						<td class="column" name="switch">
						  <?php
						       if($item['switch']=='Y') echo "有效";  
							   else if ($item['switch']=='N') echo "<b><font color='red'>無效</font></b>";
							   else  echo "未知";
						  ?>
						</td>
                    </tr>
                    <?php   }    ?>
                </tbody>
			</table>            
		</div>			
	</body>
</html>
<script type="text/javascript">
function printScreen(printlist)
  {
     var value = printlist.innerHTML;
     var printPage = window.open("", "Printing...", "");
     printPage.document.open();
     printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
     printPage.document.write("<PRE>");
     printPage.document.write(value);
     printPage.document.write("</PRE>");
     printPage.document.close("</BODY></HTML>");
  }
</script>