<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">發票管理</a>>>
			<a>折讓資料</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				
				<li class="search-field">
					<span class="label" for="search_userid">用戶編號：</span>
					<input type="text" name="search_userid" size="16" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
                <li class="search-field">
					<span class="label" for="search_allowance_no">折讓單號：</span>
					<input type="text" name="search_allowance_no" size="20" value="<?php echo $this->io->input['get']['search_allowance_no']; ?>"/>
				</li>
                <li class="search-field">
					<span class="label" for="search_objid">訂單編號：</span>
					<input type="text" name="search_objid" size="8" value="<?php echo $this->io->input['get']['search_objid']; ?>"/>
				</li>					
				<li class="search-field">
					<span class="label" for="search_allowance_date">折讓日期：</span>
					<input type="text" name="search_btime" size="16" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				    至
                    <input type="text" name="search_etime" size="16" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-start"/>
                </li>
				<li class="search-field">
					<span class="label" for="search_upload">申報狀態：</span>
					<select name="search_upload">
						<option value="">ALL</option>
						<option value="N" <?php echo ($this->io->input['get']['search_upload']=='N')?'selected':''; ?> >尚未申報</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_upload']=='Y')?'selected':''; ?> >已申報完成</option>
						<option value="P" <?php echo ($this->io->input['get']['search_upload']=='P')?'selected':''; ?> >申報中</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_switch">資料狀態：</span>
					<select name="search_switch">
						<option value="">ALL</option>
						<option value="N" <?php echo ($this->io->input['get']['search_switch']=='N')?'selected':''; ?> >無效</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_switch']=='Y')?'selected':''; ?> >有效</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
                    <div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>動作</th>
								<th>序號</th>
								<th>折讓單號</th>
								<th>折讓日期</th>
								<th>用戶編號</th>
                                <th>會員姓名</th>
								<th>折讓金額(含稅)</th>
								<th>折讓稅額</th>
								<th>申報狀態</th>
								<th>資料狀態</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) { ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?salid=<?php echo $rv["salid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<!--td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/?siid=<?php echo $rv["salid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td-->							
								<td class="column" name="salid"><?php echo $rv['salid']; ?></td>
                                <td class="column" name="allowance_no"><?php echo $rv['allowance_no']; ?></td>
								<td class="column" name="allowance_date"><?php echo $rv['allowance_date']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
                                <td class="column" name="user_name"><?php echo $rv['user_name']; ?></td>
								<td class="column" name="allowance_amt"><?php echo $rv['allowance_amt']; ?></td>
								<td class="column" name="allowance_tax_amt"><?php echo $rv['allowance_tax_amt']; ?></td>
								<td class="column" name="invoice_upload">
									<?php if ($rv['upload'] == 'N') : ?>
										尚未申報
									<?php elseif ($rv['upload'] == 'Y') : ?>
										已申報完成
									<?php elseif ($rv['upload'] == 'P') : ?>
										申報中
									<?php else : ?>
										尚未申報
									<?php endif; ?>
								</td>
								<td class="column" name="switch"><?php echo ($rv['switch']=='N'?"<font color='red'><b>無效</b></font>":"有效"); ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>