<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">字軌資料管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增折讓單</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">用戶編號：</label></td>
					<td><input name="userid" id="userid" type="text" /></td>
				</tr>
				<tr>
					<td><label for="allowance_amt">總折讓金額：</label></td>
					<td><input name="allowance_amt" id="allowance_amt" type="number" /></td>
				</tr>
                <!-- tr>
					<td><label for="allowance_tax">折讓稅額：</label></td>
					<td><input name="allowance_tax" type="number" /></td>
				</tr>
                <tr>
					<td><label for="allowance_amt">折讓金額：</label></td>
					<td><input name="seq_start" type="number" /></td>
				</tr -->
			</table>


			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs">
        
        </div>
        <?php 
            if (is_array($this->tplVar['result']['retObj'])) { 
                 $rv = $this->tplVar['result']['retObj'];
        ?>
        <div class="table-wrapper">
			<ul class="table">
				<!-- li class="header">
                    <div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li -->
                
				<li class="body">
					<p>折讓資料</p>
                    <table>
						<thead>
							<tr>
								<th>序號</th>
								<th>折讓單號</th>
								<th>折讓日期</th>
								<th>用戶編號</th>
                                <th>會員姓名</th>
								<th>折讓金額</th>
								<th>折讓稅額</th>
								<th>申報狀態</th>
                                <th>新增日期</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<!-- td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td -->
								<!--td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td-->							
								<td class="column" name="salid"><?php echo $rv['salid']; ?></td>
                                <td class="column" name="allowance_no"><?php echo $rv['allowance_no']; ?></td>
								<td class="column" name="allowance_date"><?php echo $rv['allowance_date']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
                                <td class="column" name="user_name"><?php echo $rv['user_name']; ?></td>
								<td class="column" name="allowance_amt"><?php echo $rv['allowance_amt']; ?></td>
								<td class="column" name="allowance_tax_amt"><?php echo $rv['allowance_tax_amt']; ?></td>
								<td class="column" name="invoice_upload">
									<?php if ($rv['upload'] == 'N') : ?>
										尚未申報
									<?php elseif ($rv['upload'] == 'Y') : ?>
										已申報完成
									<?php elseif ($rv['upload'] == 'P') : ?>
										申報中
									<?php else : ?>
										尚未申報										
									<?php endif; ?>
								</td>
								
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
						</tbody>
					</table>
                    <p>折讓發票明細</p>
                    <?php  
                        if($rv['items']) {                            
                    ?>
                    <table>
						<thead>
							<tr>
								<th>發票編號</th>
								<th>發票日期</th>
								<th>折讓金額</th>
								<th>折讓稅額</th>
                                <th>新增日期</th>
							</tr>
						</thead>
						<tbody>
							<?php  
                                 foreach($rv['items'] as $item) {  
                            ?>
                            <tr>
								<td class="column" name="ori_invoiceno"><?php echo $item['ori_invoiceno']; ?></td>
								<td class="column" name="ori_invoice_date"><?php echo $item['ori_invoice_date']; ?></td>
                                <td class="column" name="amount"><?php echo $item['amount']; ?></td>
								<td class="column" name="tax"><?php echo $item['tax']; ?></td>
								<td class="column" name="insertt"><?php echo $item['insertt']; ?></td>
							</tr>
                            <?php   }    ?>
						</tbody>
					</table>
					<?php   }   ?>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
        <?php  }   ?>
	</body>
</html>