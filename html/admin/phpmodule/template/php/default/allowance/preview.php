<?php
    $arrAllowanceFor=array("O"=>"訂單 (生活繳費)", "P"=>"商品 (得標結帳)","I"=>"發票");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">折讓資料管理</a>>><a>新增</a>
		</div>
		<div class="dialogs">
        
        </div>
        <?php 
            if (is_array($this->tplVar['result']['retObj'])) { 
                 $rv = $this->tplVar['result']['retObj'];
        ?>
        <div class="table-wrapper">
			<ul class="table">
				<!-- li class="header">
                    <div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li -->
                
				<li class="body">
					<br>折讓單資料預覽</br>
					<form action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert2" method="post" name="theForm" >
                    <table>
						<thead>
							<tr>
								<!-- th>折讓單號</th -->
								<th>折讓日期</th>
								<th>用戶編號</th>
                                <th>會員姓名</th>
								<th>本次折讓金額</th>
								<th>本次折讓稅額</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<!-- td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td -->
								<!--td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td-->							
                                <td class="column" ><input type="hidden" name="allowance_date" id="allowance_date" value="<?php echo $rv['allowance_date']; ?>"><?php echo $rv['allowance_date']; ?></td>
								<td class="column" ><input type="hidden" name="userid" id="userid" value="<?php echo $rv['userid']; ?>"><?php echo $rv['userid']; ?></td>
                                <td class="column" ><input type="hidden" name="user_name" id="user_name" value="<?php echo $rv['user_data']['nickname']; ?>"><?php echo $rv['user_data']['nickname']; ?></td>
								<td class="column" ><input type="hidden" name="allowance_amt" id="allowance_total" value="<?php echo $rv['allowance_amt']; ?>"><?php echo $rv['allowance_amt']; ?></td>
								<td class="column" ><input type="hidden" name="allowance_tax_amt" id="allowance_tax_amt" value="<?php echo $rv['allowance_tax_amt']; ?>"><?php echo $rv['allowance_tax_amt']; ?></td>
							</tr>
							
						</tbody>
					</table>
					<br>關聯資料</br>
					<table>
						<thead>
							<tr>
								<th>關聯類型</th>
								<th>關聯訂單/商品編號</th>
								<th>備註</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="column" >
								    <select name="allowance_for" id="allowance_for">
									  <option value="">無</option>
									  <option value="O" <?php echo $rv['allowance_for']=="O"?"selected":""; ?> >訂單(生活繳費)編號</option>
									  <option value="I" <?php echo $rv['allowance_for']=="I"?"selected":""; ?> >發票編號</option>
									</select>
								</td>
							    <td class="column" >
								   <input type="text" name="objid" id="objid" value="<?php echo $rv['objid']; ?>">
								</td>
							    <td colspan="3">
								   <textarea name="memo" id="memo" cols="64" rows="3" value="<?php echo $rv['memo'];?>"><?php echo $rv['memo'];?></textarea> 
								</td>
							</tr>
						</tbody>
					</table>
					
				</li>
				<li class="body">
                    <br>折讓發票明細</br>
                    <?php  
                        if($rv['items']) {                            
                    ?>
                    <table>
						<thead>
							<tr>
								<th>發票編號</th>
								<th>發票日期</th>
								<th>發票銷售金額</th>
								<th>發票銷售稅額</th>
								<th>發票總金額</th>
								<th>發票已折讓金額</th>
								<th>折讓金額小計</th>
								<th>折讓稅額小計</th>
							</tr>
						</thead>
						<tbody>
							<?php 
                                 $idx=0;							
                                 foreach($rv['items'] as $item) {  
                            ?>
                            <tr>
								<td class="column"><input type="hidden" name="ori_invoiceno_<?php echo $idx; ?>" value="<?php echo $item['ori_invoiceno']; ?>" ><center><?php echo $item['ori_invoiceno']; ?></center></td>
								<td class="column"><input type="hidden" name="ori_invoice_date_<?php echo $idx; ?>" value="<?php echo $item['ori_invoice_date']; ?>" ><center><?php echo $item['ori_invoice_date']; ?></center</td>
								<td class="column"><input type="hidden" name="ori_sales_amt_<?php echo $idx; ?>" value="<?php echo $item['ori_sales_amt']; ?>" ><?php echo $item['ori_sales_amt']; ?></td>
								<td class="column"><input type="hidden" name="ori_tax_amt_<?php echo $idx; ?>" value="<?php echo $item['ori_tax_amt']; ?>" ><?php echo $item['ori_tax_amt']; ?></td>
								<td class="column"><input type="hidden" name="ori_total_amt_<?php echo $idx; ?>" value="<?php echo $item['ori_total_amt']; ?>" ><?php echo $item['ori_total_amt']; ?></td>
                                <td class="column"><input type="hidden" name="ori_allowance_amt_<?php echo $idx; ?>" value="<?php echo $item['ori_allowance_amt']; ?>" ><?php echo $item['ori_allowance_amt']; ?></td>
								<td class="column"><input type="number" name="allowance_amt_<?php echo $idx; ?>" value="<?php echo $item['amount']; ?>" ></td>
								<td class="column"><input type="number" name="allowance_tax_<?php echo $idx; ?>" value="<?php echo $item['tax']; ?>" ></td>
							</tr>
                            <?php   
									++$idx;
								}    
							?>
						</tbody>
					</table>
					<?php   }   ?>
					<input type="hidden" name="total_idx" id="total_idx" value="<?php echo $idx; ?>" />
				   
				</li>
				<div class="functions">
					<div class="button cancel"><a href="/admin/allowance/add">重新計算</a></div>
					<div class="button submit"><input type="submit" value="確定送出" class="submit"></div>
				</div> 
				</form>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
        <?php  }   ?>
	</body>
</html>