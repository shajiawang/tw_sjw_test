<?php
    $arrAllowanceFor=array("O"=>"訂單 (生活繳費)", "P"=>"商品 (得標結帳)","I"=>"發票");
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">折讓資料管理</a>>><a>新增</a>
		</div>
		<div class="dialogs">
        
        </div>
        <?php 
            if (is_array($this->tplVar['result']['retObj'])) { 
                 $rv = $this->tplVar['result']['retObj'];
        ?>
        <div class="table-wrapper">
			<ul class="table">
				<!-- li class="header">
                    <div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li -->
                
				<li class="body">
						<p>折讓資料</p>
						<table>
							<thead>
								<tr>
									<th>序號</th>
									<th>折讓單號</th>
									<th>折讓日期</th>
									<th>用戶編號</th>
									<th>會員姓名</th>
									<th>折讓金額</th>
									<th>未稅金額</th>
									<th>折讓稅額</th>
									<th>申報狀態</th>
									<!--th>新增日期</th -->
								</tr>
							</thead>
							<tbody>
								<tr>
									<!-- td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td -->
									<!--td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td-->							
									<td class="column" name="salid"><?php echo $rv['salid']; ?></td>
									<td class="column" name="allowance_no"><?php echo $rv['allowance_no']; ?></td>
									<td class="column" name="allowance_date"><?php echo $rv['allowance_date']; ?></td>
									<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
									<td class="column" name="user_name"><?php echo $rv['user_name']; ?></td>
									<td class="column" name="allowance_amt"><?php echo $rv['allowance_amt']; ?></td>
									<td class="column" name="tax_amt"><?php echo round($rv['allowance_amt']-$rv['allowance_tax_amt']); ?></td>
									<td class="column" name="allowance_tax_amt"><?php echo $rv['allowance_tax_amt']; ?></td>
									<td class="column" name="invoice_upload">
										<?php if ($rv['upload'] == 'N') : ?>
											尚未申報
										<?php elseif ($rv['upload'] == 'Y') : ?>
											已申報完成
										<?php elseif ($rv['upload'] == 'P') : ?>
											申報中
										<?php else : ?>
											尚未申報										
										<?php endif; ?>
									</td>
									
									<!-- td class="column" name="insertt"><?php echo $rv['insertt']; ?></td -->
								</tr>
							</tbody>
						</table>

						<p>折讓發票明細</p>
						<?php  
							if($rv['items']) {                            
						?>
						<table>
							<thead>
								<tr>
									<th>發票編號</th>
									<th>發票日期</th>
									<th>折讓金額</th>
									<th>折讓稅額</th>
									<!-- th>新增日期</th -->
								</tr>
							</thead>
							<tbody>
								<?php  
									 foreach($rv['items'] as $item) {  
								?>
								<tr>
									<td class="column" name="ori_invoiceno"><?php echo $item['ori_invoiceno']; ?></td>
									<td class="column" name="ori_invoice_date"><?php echo $item['ori_invoice_date']; ?></td>
									<td class="column" name="amount"><?php echo $item['amount']; ?></td>
									<td class="column" name="tax"><?php echo $item['tax']; ?></td>
									<!-- td class="column" name="insertt"><?php echo $item['insertt']; ?></td -->
								</tr>
								<?php   }    ?>
							</tbody>
						</table>
						<?php   }   ?>
						
						<p>關聯資料</p>
						<table>
							<thead>
								<tr>
									<th>關聯類型</th>
									<th>關聯訂單/商品編號</th>
									<th>備註</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="column" ><?php echo $arrAllowanceFor[$rv['allowance_for']]; ?></td>
									<td class="column" ><?php echo $rv['objid']; ?></td>
									<td class="column" ><?php echo $rv['memo'];?></td>
								</tr>
							</tbody>
						</table>
				</li>
				
				<li class="body">
					<div id="button" class="functions">
						<div class="button cancel"><a href="/admin/allowance/add">繼續新增</a></div>
						<div class="button "><a href="#" onclick="printScreen(printdata);">列印</a></div>
					</div> 
				</li>
			</ul>
		</div>
        <?php  }   ?>
		<div id="printdata" style="display:none;" >
			<?php 
				if (is_array($this->tplVar['result']['retObj'])) { 
					 $rv = $this->tplVar['result']['retObj'];
			?>

							<p>折讓資料</p>
							<table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all" cellpadding='5';>
									<tr>
										<th>序號</th>
										<th>折讓單號</th>
										<th>折讓日期</th>
										<th>用戶編號</th>
										<th>會員姓名</th>
										<th>折讓金額</th>
										<th>未稅金額</th>
										<th>折讓稅額</th>
									</tr>
									<tr>
										<td  name="salid"><?php echo $rv['salid']; ?></td>
										<td  name="allowance_no"><?php echo $rv['allowance_no']; ?></td>
										<td  name="allowance_date"><?php echo $rv['allowance_date']; ?></td>
										<td  name="userid"><?php echo $rv['userid']; ?></td>
										<td  name="user_name"><?php echo $rv['user_name']; ?></td>
										<td  name="allowance_amt"><?php echo $rv['allowance_amt']; ?></td>
										<td  name="tax_amt"><?php echo round($rv['allowance_amt']-$rv['allowance_tax_amt']); ?></td>
										<td  name="allowance_tax_amt"><?php echo $rv['allowance_tax_amt']; ?></td>
									</tr>
							</table>
							<p>折讓發票明細</p>
							<?php  
								if($rv['items']) {                            
							?>
							<table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all" cellpadding='5';>
									<tr>
										<th>發票編號</th>
										<th>發票日期</th>
										<th>折讓金額</th>
										<th>折讓稅額</th>
									</tr>
									<?php  
										 foreach($rv['items'] as $item) {  
									?>
									<tr>
										<td name="ori_invoiceno"><?php echo $item['ori_invoiceno']; ?></td>
										<td name="ori_invoice_date"><?php echo $item['ori_invoice_date']; ?></td>
										<td name="amount"><?php echo $item['amount']; ?></td>
										<td name="tax"><?php echo $item['tax']; ?></td>
									</tr>
									<?php   }    ?>
							</table>
							<?php   }   ?>
							
							<p>關聯資料</p>
							<table width="100%" style="border:3px #000000 solid;padding:5px;" rules="all" cellpadding='5';>
									<tr>
										<th>關聯類型</th>
										<th>關聯訂單/商品編號</th>
										<th>備註</th>
									</tr>

									<tr>
										<td><?php echo $arrAllowanceFor[$rv['allowance_for']]; ?></td>
										<td><?php echo $rv['objid']; ?></td>
										<td><?php echo $rv['memo'];?></td>
									</tr>
							</table>
							<p>　</p> 
							<table style="width:100%; border:0px;">
									<tr>
										<td>核准:</td>
										<td>工程:</td>
										<td>出納:</td>
										<td>主管:</td>
										<td>主管(大於3萬):</td>
										<td>申請人:</td>
									</tr>
							</table>							
					</li>
				</ul>
			</div>
			<?php  }   ?>
		</div>		
	</body>
</html>
<script type="text/javascript">
function printScreen(printlist)
  {
     var value = printlist.innerHTML;
     var printPage = window.open("", "Printing...", "");
     printPage.document.open();
     printPage.document.write("<HTML><head></head><BODY onload='window.print();window.close()'>");
     printPage.document.write("<PRE>");
     printPage.document.write(value);
     printPage.document.write("</PRE>");
     printPage.document.close("</BODY></HTML>");
  }
</script>