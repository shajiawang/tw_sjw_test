<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>殺價王管理後台</title>
	    <!--script src="http://code.jquery.com/jquery-1.9.1.min.js"></script -->
		<script src="<?php echo $this->config->default_main; ?>/js/jquery-min.js"></script>
		<script type="text/javascript">
		window.toggleLeft = function() {
			if (parseInt($('#left').css('width'))) {
				$('#frame').attr('cols','0,*');
			} else {
				$('#frame').attr('cols','200,*');
			}
		}
		</script>
	</head>
	<frameset rows="80,*,20" framespacing="0" frameborder="O" border="0">
		<frame src="<?php echo $this->config->default_main; ?>/onload/head" scrolling="no" name="head" frameborder="0" marginwidth="0"/>
		<frameset id="frame" cols="200,*" rows="*" framespacing="0" frameborder="O">
		
		<?php if($_SESSION['user']['department'] == 'S') { ?>
    			<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default"   scrolling="auto" name="default" frameborder="0" noresize/>
		<?php } else if($_SESSION['user']['department'] == 'A') { ?>	
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_management" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_management"   scrolling="auto" name="default" frameborder="0" noresize/>	
		<?php } else if($_SESSION['user']['department'] == 'B') { ?>	
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_engineering" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_engineering"   scrolling="auto" name="default" frameborder="0" noresize/>	
		<?php } else if($_SESSION['user']['department'] == 'C') { ?>	
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_marketing" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_marketing"   scrolling="auto" name="default" frameborder="0" noresize/>					
		<?php } else if($_SESSION['user']['department'] == 'D') { ?>
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_accounting" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_accounting"   scrolling="auto" name="default" frameborder="0" noresize/>	
		<?php } else if($_SESSION['user']['department'] == 'E') { ?>
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_service" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_service"   scrolling="auto" name="default" frameborder="0" noresize/>					
		<?php } else if($_SESSION['user']['department'] == 'F') { ?>
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_affairs" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_affairs"   scrolling="auto" name="default" frameborder="0" noresize/>		
		<?php } else if($_SESSION['user']['department'] == 'G') { ?>
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_sales" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_sales"   scrolling="auto" name="default" frameborder="0" noresize/>	
		<?php } else if($_SESSION['user']['department'] == 'H') { ?>
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_personnel" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_personnel"   scrolling="auto" name="default" frameborder="0" noresize/>					
		<?php } else { ?>
				<frame id="left" src="<?php echo $this->config->default_main; ?>/onload/left_onlock" scrolling="no" name="left" frameborder="0"/>
				<frame src="<?php echo $this->config->default_main; ?>/onload/default_onlock"   scrolling="auto" name="default" frameborder="0" noresize/>
		<?php  }  ?>
		
		</frameset>
		<frame src="<?php echo $this->config->default_main; ?>/onload/footer"  scrolling="auto" name="footer"/>
	</frameset>
</html>
