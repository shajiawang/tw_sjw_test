<?php
$body = $this->tplVar['table']['record'][0]['body'];

?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		<script>
			function changeshow(){
				if ($('#sendto').val()==''){
					$('#select_force_show').attr('disabled',false);
				}else{
					$('#select_force_show').attr('disabled',true);
				}
			}
			function tosend(){
				alert('請再次確認, 是否發送推播!!');
				$("#form-add").submit();
			}
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">手動推播(<span style="color:red;font-size: 0.8em;">編輯功能僅能影響通知中心的顯示文字</span>)</div>
			<table class="form-table">
				<tr>
					<td><label for="groupset">推播分組：</label></td>
					<td><input type="text" name="groupset" id="groupset" value="<?php echo $this->tplVar['table']['record'][0]['groupset'];?>">
					</td>
				</tr>
				<tr>
					<td><label for="title">推播標題：</label></td>
					<td><input name="title" type="text" data-validate="required" value="<?php echo $this->tplVar['table']['record'][0]['title'];?>"/> 商品名稱 請勿輸入 單引號' 雙引號" 右斜線\ (系統儲存時將會自動過濾移除)</td>
				</tr>
				<tr>
					<td><label for="body">推播內容：</label></td>
					<td>
						<input type="text" name="body" style="width: 700px;" value="<?php echo $this->tplVar['table']['record'][0]['body'];?>">
					</td>
				</tr>
				<tr id="tr_action">
					<td><label for="action">推送路徑(APP)：</label></td>
					<td>
						<select name="action" id="select_action">
							<option value="0" <?php if( $this->tplVar['table']['record'][0]['action'] == "0") echo "selected='selected'";?>>不做事</option>
							<option value="1" <?php if( $this->tplVar['table']['record'][0]['action'] == "1") echo "selected='selected'";?> >開啟web(內開)</option>
							<option value="2" <?php if( $this->tplVar['table']['record'][0]['action'] == "2") echo "selected='selected'";?> >開啟web(外連)</option>
							<option value="3" <?php if( $this->tplVar['table']['record'][0]['action'] == "3") echo "selected='selected'";?> >開大圖</option>
							<option value="4" <?php if( $this->tplVar['table']['record'][0]['action'] == "4") echo "selected='selected'";?> >跳頁面</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="page">跳轉頁面</label></td>
					<td><select name="page" id="page">
							<option value="1" <?php if( $this->tplVar['table']['record'][0]['page'] == "1") echo "selected='selected'";?> >商品頁</option>
							<option value="2" <?php if( $this->tplVar['table']['record'][0]['page'] == "2") echo "selected='selected'";?> >購買殺價幣</option>
							<option value="3" <?php if( $this->tplVar['table']['record'][0]['page'] == "3") echo "selected='selected'";?> >直播頁</option>
							<option value="4" <?php if( $this->tplVar['table']['record'][0]['page'] == "4") echo "selected='selected'";?> >首頁</option>
							<option value="5" <?php if( $this->tplVar['table']['record'][0]['page'] == "5") echo "selected='selected'";?> >殺價紀錄</option>
							<option value="6" <?php if( $this->tplVar['table']['record'][0]['page'] == "6") echo "selected='selected'";?> >商城兌換清單</option>
							<option value="7" <?php if( $this->tplVar['table']['record'][0]['page'] == "7") echo "selected='selected'";?> >邀請好友</option>
							<option value="8" <?php if( $this->tplVar['table']['record'][0]['page'] == "8") echo "selected='selected'";?> >圓夢商品頁</option>
							<option value="9" <?php if( $this->tplVar['table']['record'][0]['page'] == "9") echo "selected='selected'";?> >殺友專區</option>
							<option value="10" <?php if( $this->tplVar['table']['record'][0]['page'] == "10") echo "selected='selected'";?> >我的帳號</option>
							<option value="11" <?php if( $this->tplVar['table']['record'][0]['page'] == "11") echo "selected='selected'";?> >殺戮戰場首頁</option>
							<option value="12" <?php if( $this->tplVar['table']['record'][0]['page'] == "12") echo "selected='selected'";?> >圓夢商品首頁</option>
							<option value="13" <?php if( $this->tplVar['table']['record'][0]['page'] == "13") echo "selected='selected'";?> >鯊魚商城首頁</option>
							<option value="14" <?php if( $this->tplVar['table']['record'][0]['page'] == "14") echo "selected='selected'";?> >殺價幣</option>
							<option value="15" <?php if( $this->tplVar['table']['record'][0]['page'] == "15") echo "selected='selected'";?> >殺價劵</option>
							<option value="16" <?php if( $this->tplVar['table']['record'][0]['page'] == "16") echo "selected='selected'";?> >票劵/卡片</option>
							<option value="17" <?php if( $this->tplVar['table']['record'][0]['page'] == "17") echo "selected='selected'";?> >新手教學</option>
							<option value="18" <?php if( $this->tplVar['table']['record'][0]['page'] == "18") echo "selected='selected'";?> >最新得標</option>
							<option value="19" <?php if( $this->tplVar['table']['record'][0]['page'] == "19") echo "selected='selected'";?> >王者秘笈</option>
							<option value="20" <?php if( $this->tplVar['table']['record'][0]['page'] == "20") echo "selected='selected'";?> >掃瞄支付頁(使用者掃店家)</option>
							<option value="21" <?php if( $this->tplVar['table']['record'][0]['page'] == "21") echo "selected='selected'";?> >鯊魚點使用明細</option>
							<option value="22" <?php if( $this->tplVar['table']['record'][0]['page'] == "22") echo "selected='selected'";?> >超級殺價劵</option>
							<option value="23" <?php if( $this->tplVar['table']['record'][0]['page'] == "23") echo "selected='selected'";?> >關於殺價王</option>
						</select>
					</td>
				</tr>
				<tr id="tr_productid">
					<td><label for="productid">產品編號：</label></td>
					<td>
						<input type="text" name="productid" id="productid" value="<?php echo $this->tplVar['table']['record'][0]['productid'];?>">
					</td>
				</tr>
				<tr id="tr_url_title">
					<td><label for="url_title">webview標題：</label></td>
					<td>
						<input type="text" name="url_title"  value="<?php echo $this->tplVar['table']['record'][0]['url_title'];?>">
					</td>
				</tr>
				<tr id="tr_url">
					<td><label for="url">webview連結：</label></td>
					<td>
						<input type="text" name="url"  value="<?php echo $this->tplVar['table']['record'][0]['url'];?>">
					</td>
				</tr>
				<tr id="tr_sendto">
					<td><label for="sendto">發送對象(userid)：</label></td>
					<td>
						<input type="text" name="sendto" id ='sendto' onkeyup="changeshow();"  value="<?php echo $this->tplVar['table']['record'][0]['sendto'];?>">
					</td>
				</tr>
				<tr id="tr_force_show">
					<td><label for="force_show">通知中心顯示：(此欄群發時才有效)</label></td>
					<td>
						<select name="force_show" id="select_force_show"  <?php if (!(empty($this->tplVar['table']['record'][0]['sendto']))) echo "disabled='disabled'"; ?> >
							<option value="Y" <?php if( $this->tplVar['table']['record'][0]['force_show'] == "Y") echo "selected='selected'";?>>強制顯示</option>
							<option value="N" <?php if( $this->tplVar['table']['record'][0]['force_show'] == "N") echo "selected='selected'";?>>強制不顯示</option>
						</select>
					</td>
				</tr>
			</table>

			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="pushid" value="<?php echo $this->tplVar["status"]["get"]["pushid"] ;?>">
				<div class="button submit"><input name="edit_item" id="edit_item" type="button" value="送出" onclick="tosend();" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>

		</form>
		<div class="dialogs"></div>
	</body>
</html>