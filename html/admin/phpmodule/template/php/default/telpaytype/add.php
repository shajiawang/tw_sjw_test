<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">類型名稱：</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="toid">電信公司：</label></td>
					<td><select name="toid">
						<option value="0">請選擇</option>
					<?php foreach ($this->tplVar['table']["rt"]["telecom"] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['toid']; ?>" ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
					</td>
				</tr>				
			</table>			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>