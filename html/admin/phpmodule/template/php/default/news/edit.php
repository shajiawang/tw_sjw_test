<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="newsid">公告ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['newsid']; ?></td>
				</tr>
				<tr>
					<td><label for="public">公開公告：</label></td>
					<td>
						<select name="public">
							<option value="Y" <?php if($this->tplVar['table']['record'][0]['public']=='Y'){ echo 'selected'; } ?> >首頁公告</option>
							<option value="N" <?php if($this->tplVar['table']['record'][0]['public']=='N'){ echo 'selected'; } ?> >站內公告</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="name">公告標題：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="description">公告內容：</label></td>
					<td><textarea name="description" id="ckeditor_description"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="ontime">前台顯示時間：</label></td>
					<td><input name="ontime" type="text" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>" class="datetime time-start" /></td>
				</tr>
				
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" class="datetime time-stop" /></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="osversion">版本號</label></td>
					<td><input name="osversion" type="text" value="<?php echo $this->tplVar['table']['record'][0]['osversion']; ?>"/></td>
				</tr>
			</table>
			
			<input type="hidden" name="channelid[]" id="channelid_0" value="0" />
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="newsid" value="<?php echo $this->tplVar["status"]["get"]["newsid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>