<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button"> 
							<!--a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a -->
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<!--th></th-->
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_langid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_langid=desc">貸款類別編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_bankid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_langid=">貸款類別編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_langid=asc">貸款類別編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_name"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_name=desc">貸款類別名稱<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_bankname"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=">貸款類別名稱<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=asc">貸款類別名稱<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>啟用</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/lid=<?php echo $rv["lid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<!--td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/lid=<?php echo $rv["lid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td-->
								<td class="column" name="loanid"><?php echo $rv['loanid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="switch"><?php echo $rv['switch']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>