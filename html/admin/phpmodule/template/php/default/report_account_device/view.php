<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">

			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>會員數量</th>
								<th>Web</th>
								<th>IOS</th>
								<th>Android</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="column" name="userid"><?php echo $this->tplVar["table"]['record'][0]['user_count']; ?></td>
								<td class="column" name="Web"><?php echo $this->tplVar["table"]['record'][0]['web_count']; ?></td>
								<td class="column" name="IOS"><?php echo $this->tplVar["table"]['record'][0]['ios_count']; ?></td>
								<td class="column" name="Android"><?php echo $this->tplVar["table"]['record'][0]['android_count']; ?></td>
                            </tr>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php // include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>