<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(document).ready(function() {
			$('button[name="settle"]').click(function() {
				$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
			    {
			    	type: "settle",
			    	companyname: "<?php echo $this->io->input['get']['search_companyname']; ?>",
			    	startdatetime: "<?php echo $this->io->input['get']['startdatetime']; ?>",
			    	enddatetime: "<?php echo $this->io->input['get']['enddatetime']; ?>",
			    	settle: "<?php echo $this->io->input['get']['search_settle']; ?>",
			    	location_url: '<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>'
			    },
			    function(str_json){
			    	if (str_json) {
			    		var json = $.parseJSON(str_json);
			    		console && console.log($.parseJSON(str_json));
			    		alert(json.msg);
			    		location.reload();
			    	}
			    });
			});
		});
		
		//結清鯊魚點
		function to_settled(id){
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
			{
				type: "settle_single",
				bsid: id,
				location_url: '<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>'
			},
			function(str_json){
				if (str_json) {
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					alert(json.msg);
					location.reload();
				}
			});
		};
		</script>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_companyname">公司名稱：</span>
					<input type="text" name="search_companyname" size="20" value="<?php echo $this->io->input['get']['search_companyname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_datetime">交易時間：</span>
					<input name="startdatetime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['startdatetime']; ?>" /> ~ 
					<input name="enddatetime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['enddatetime']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_settle">是否已結清鯊魚點：</span>
					<select name="search_settle">
					    <option value="">全部</option>
					    <option value="Y" <?php echo ( $this->io->input['get']["search_settle"]=='Y'?"selected":""); ?> >是</option>
					    <option value="N" <?php echo ( $this->io->input['get']["search_settle"]=='N'?"selected":""); ?> >否</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<!--<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>-->
							<!--<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update/?type=settle&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">鯊魚點結清</a>-->
							<button type="button" name="settle">鯊魚點結清</button>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>交易商品</th>
								<th>交易狀態</th>
								<th>交易鯊魚點</th>
								<th>公司名稱</th>
								<th>會員暱稱</th>
								<th>是否已結清鯊魚點</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">建立日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">建立日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">建立日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr <?php if (strtoupper($rv['switch'])=='N') echo 'style="text-decoration: line-through"';?>>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="behav"><?php if ($rv['behav'] == 'user_exchange') { echo '鯊魚點兌換'; } else if ($rv['behav'] == 'order_refund') { echo '退貨'; } else { echo $rv['behav']; } ?></td>
								<td class="column" name="amount"><?php echo intval($rv['amount'] * 100) / 100; ?></td>
								<td class="column" name="companyname"><?php echo $rv['companyname']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="is_settle"><?php
									if (strtoupper($rv['switch'])=='Y'){
										if ($rv['is_settle'] == 'Y') { echo '是'; } else { echo '否<input class="table_td_button" type="button" value="結清" onclick="to_settled('.$rv['bsid'].');"/>';
										}
									}

									?>
								</td>
								<td class="column" name="modifyt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>

				</li>
				<li class="footer">
					<div style="float:left;color: #4b6d23;">鯊魚點總共 <?php echo intval($this->tplVar["Total"]["TotalAmount"] * 100) / 100; ?> 點 / 已結清鯊魚點總共 <?php echo intval($this->tplVar["Total"]["SettledAmount"] * 100) / 100; ?> 點 / 未結清鯊魚點總共 <?php echo intval($this->tplVar["Total"]["UnsettledAmount"] * 100) / 100; ?> 點</div>
				<!-- Page Start -->
				 <?php echo ("<!-- ".$this->tplVar["block"]["page"]." -->"); ?>
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>