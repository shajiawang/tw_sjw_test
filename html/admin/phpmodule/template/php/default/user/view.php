<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<style type="text/css">
		.message {
			color: #29736c; width:300px; background-color: #ecf8f7; border: 1px solid #005d54; border-radius: 4px; display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px; background-color: #8dc8c3; border-radius: 4px; position: relative;
		}
		.message header h2 {
			font-weight: bold; font-size: 16px; line-height: 20px; margin: 0 12px;
		}
		.message header .button-close {
			text-decoration: none; color: #ecf8f7; float: right; margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px; padding: 10px 0;
		}
		.message.center {
			position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		.msg{
			color: #29736c;
		}
		</style>
	</head>

	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a>會員帳號管理</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label">登入帳號：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<?php /*<li class="search-field">
					<span class="label">下標功能：</span>
					<select name="search_bid"><option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_bid']=='N') ? 'selected' : ''; ?> >No</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_bid']=='Y') ? 'selected' : ''; ?> >Yes</option>
					</select>
				</li>*/?>
				<li class="search-field">
					<span class="label" for="search_tel">手機號碼：</span>
					<input type="text" name="search_tel" size="20" value="<?php echo $this->io->input['get']['search_tel']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">手機驗證：</span>
					<select name="search_verified"><option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_verified']=='N') ? 'selected' : ''; ?> >No</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_verified']=='Y') ? 'selected' : ''; ?> >Yes</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label">暱稱：</span>
					<input type="text" name="search_nickname" size="20" value="<?php echo $this->io->input['get']['search_nickname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">資料狀態：</span>
					<select name="search_switch">
						<option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_switch']=='N') ? 'selected' : ''; ?> >停權中</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_switch']=='Y') ? 'selected' : ''; ?> >使用中</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_idnum">身份證字號：</span>
					<input type="text" name="search_idnum" size="20" value="<?php echo $this->io->input['get']['search_idnum']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<?php /*<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add">新增</a>*/?>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								
								<th>刪除</th>
								<?php $base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; ?>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_userid"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_u.userid=desc">會員編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_userid"] == 'desc') : ?>
									<a href="<?php echo $base_href ;?>&sort_u.userid=">會員編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_u.userid=asc">會員編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>登入帳號</th>
								<th>會員等級</th>
								<th>下標兌換充值</th>
								<th>手機驗證</th>
								<th>暱稱</th>
								<th>收件人</th>
								<!--th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_email"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_email=desc">Email<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_email"] == 'desc') : ?>
									<a href="<?php echo $base_href ;?>&sort_email=">Email<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_email=asc">Email<span class="sort init"></span></a>
								<?php endif; ?>
								</th-->
								<!--th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_provider_name"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_provider_name=desc">SSO名稱<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_provider_name"] == 'desc') : ?>
									<a href="<?php echo $base_href ;?>&sort_provider_name=">SSO名稱<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_provider_name=asc">SSO名稱<span class="sort init"></span></a>
								<?php endif; ?>
								</th-->
								<th>身份證字號</th>
								<th>載具歸戶</th>
								<th>資料狀態</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="userid">
									<input class="table_td_button" type="button" value="設密碼" onclick="edit_pass('<?php echo $rv['userid']; ?>')"> <?php echo $rv['userid']; ?>
								</td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="level"><input name="level" type="number" value="<?php echo $rv['level']; ?>" onkeyup="to_edit(<?php echo $rv['userid']; ?>,this.value,'level')" style="width:50px" /></td>
								<td class="column" name="bid">下標<?php echo $rv['bid_enable']; ?>
								<?php if($rv['bid_enable']=='N'){ ?>
								<input class="table_td_button" type="button" value="解鎖" onclick="bidEnable('<?php echo $rv['userid']; ?>','bid_enable','enable')">
								<?php } else { ?>
								<input class="table_td_button" type="button" value="下標鎖定" onclick="bidEnable('<?php echo $rv['userid']; ?>','bid_enable','disable')">
								<?php } ?><br>

								圓夢<?php echo $rv['dream_enable']; ?>
								<?php if($rv['dream_enable']=='N'){ ?>
								<input class="table_td_button" type="button" value="解鎖" onclick="bidEnable('<?php echo $rv['userid']; ?>','dream_enable','enable')">
								<?php } else { ?>
								<input class="table_td_button" type="button" value="圓夢鎖定" onclick="bidEnable('<?php echo $rv['userid']; ?>','dream_enable','disable')">
								<?php } ?><br>

								兌換<?php echo $rv['exchange_enable']; ?>
								<?php if($rv['exchange_enable']=='N'){ ?>
								<input class="table_td_button" type="button" value="解鎖" onclick="bidEnable('<?php echo $rv['userid']; ?>','exchange_enable','enable')">
								<?php } else { ?>
								<input class="table_td_button" type="button" value="兌換鎖定" onclick="bidEnable('<?php echo $rv['userid']; ?>','exchange_enable','disable')">
								<?php } ?>

								充值<?php echo $rv['deposit_enable']; ?>
								<?php if($rv['deposit_enable']=='N'){ ?>
								<input class="table_td_button" type="button" value="解鎖" onclick="bidEnable('<?php echo $rv['userid']; ?>','deposit_enable','enable')">
								<?php } else { ?>
								<input class="table_td_button" type="button" value="充值鎖定" onclick="bidEnable('<?php echo $rv['userid']; ?>','deposit_enable','disable')">
								<?php } ?>
								</td>

								<td class="column" name="verified"><?php echo $rv['phone']; ?>
								<?php if(!empty($rv['verified']) && $rv['verified']=='Y'){ ?>
									已驗證 <input class="table_td_button" type="button" value="取消驗證" onclick="del_verified('<?php echo $rv['userid']; ?>')">
								<?php }else{ ?>
									未驗證 <input class="table_td_button" type="button" value="驗證" onclick="ogetcheck('<?php echo $rv['userid']; ?>','<?php echo $rv['nickname']; ?>','<?php echo $rv['code']; ?>')">
								<?php } ?>
								</td>

								<td class="column" name="nickname" id="<?php echo $rv['userid']; ?>" data-memo="<?php echo $rv['memo'];?>">
									<input class="table_td_button" type="button" value="編輯" onclick="edit_name('<?php echo $rv['userid']; ?>','<?php echo $rv['nickname']; ?>')"> <?php echo $rv['nickname'];?>
								</td>

								<td class="column" name="addressee" >
									<input class="table_td_button" type="button" value="編輯" onclick="edit_address('<?php echo $rv['userid']; ?>')">
									<span id="addu<?php echo $rv['userid']; ?>" data-addu="<?php echo $rv['addressee'];?>"><?php echo $rv['addressee'];?></span><br>
									<span id="zip<?php echo $rv['userid']; ?>" data-zip="<?php echo $rv['area'];?>"><?php echo $rv['area'];?></span> <br>
									<span id="addr<?php echo $rv['userid']; ?>" data-addr="<?php echo $rv['address'];?>"><?php echo $rv['address'];?></span><br>
									<span id="tel<?php echo $rv['userid']; ?>" data-tel="<?php echo $rv['rphone'];?>"><?php echo $rv['rphone'];?></span>
								</td>

								<!--td class="column" name="email"><?php echo $rv['email']; ?></td-->
								<!--td class="column" name="provider_name"><?php echo (!empty($rv['provider_name']) ? $rv['provider_name'] : '一般'); ?></td-->
								<td class="column" name="idnum">
									<input class="table_td_button" type="button" value="編輯" onclick="edit_idnum('<?php echo $rv['userid']; ?>','<?php echo $rv['idnum']; ?>')"> <?php echo $rv['idnum']; ?>
								</td>
								<td class="column"><input class="table_td_button" type="button" value="載具資料" onclick="javascript:location.href='<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/userid=<?php echo $rv["userid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>'"></td>

								<td class="column" name="switch"><?php if ($rv['switch'] == 'Y') { echo '使用中'; }else{ echo '停權中'; } ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>

				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>

		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg" id="msg-title">訊息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="" >
				<p id="msg">Message content</p>
			</div>
		</article>

	</body>
</html>

<script type="text/javascript">

//鎖定/解鎖下標兌換
function bidEnable(id,typestr,modstr){
	$('#msg-dialog').hide();
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type:typestr,
			uid:id,
			mod:modstr,
			location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				alert(json.msg);
				location.reload();
			}
		}
	);
};

//收件資料：
function edit_address(userid) {
	var addu = $("#addu"+ userid).data("addu");
	var tel = $("#tel"+ userid).data("tel");
	var zip = $("#zip"+ userid).data("zip");
	var addr = $("#addr"+ userid).data("addr");
	var data = '<table><tr><td>設定收件資料</td></tr><tr><td>收件人：<input name="addressee" type="text" id="addressee" value="'+addu+'" ></td></tr><tr><td>收件電話：<input name="rphone" type="text" id="rphone" value="'+tel+'" ></td></tr><tr><td>郵遞區號：<input name="area" type="text" id="area" value="'+zip+'" ></td></tr><tr><td>收件地址：<textarea name="address" type="text" id="address" >'+addr+'</textarea></td></tr><tr><td><input type="button" value="確定送出" onclick="to_address('+userid+')"></td></tr></table>';
	$('#msg').html(data);
	$('#msg-dialog').show();
};
//修改收件資料
function to_address(id){
	$('#msg-dialog').hide();
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type: "modify_address",
			uid:id,
			addressee: $('#addressee').val(),
			rphone: $('#rphone').val(),
			area: $('#area').val(),
			address: $('#address').val(),
			location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				//$('#msg').text(json.msg);
				alert(json.msg);
				location.reload();
			}
			//$('#msg-dialog').show();
		}
	);
};
//會員名稱：
function edit_name(userid,username) {
	var memo = $("#"+ userid).data("memo");
	var data = '<table><tr><td>設定會員資料</td></tr><tr><td>會員編號：'+userid+'</td></tr><tr><td>會員名稱：<input name="username" type="text" id="username" value="'+username+'" ></td></tr><tr><td>備　　註：<textarea name="memo" type="text" id="f_memo" >'+memo+'</textarea></td></tr><tr><td><input type="button" value="確定送出" onclick="to_username('+userid+')"></td></tr></table>';
	$('#msg').html(data);
	$('#msg-dialog').show();
};

//修改名稱
function to_username(id){
	$('#msg-dialog').hide();
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type: "modify_nickname",
			uid:id,
			nickname: $('#username').val(),
			memo: $('#f_memo').val(),
			location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				//$('#msg').text(json.msg);
				alert(json.msg);
				location.reload();
			}
			//$('#msg-dialog').show();
		}
	);
};


//會員身份證字號：
function edit_idnum(userid,idnum) {
	var data = '<table><tr><td>設定會員資料</td></tr><tr><td>會員編號：'+userid+'</td></tr><tr><td><label for="idnum">身份證字號：</label></td><td><input name="idnum" type="text" id="idnum" value="'+idnum+'" ></td></tr><tr><td><label for="applyYyymmdd">發證時間(0970101)：</label></td><td><input name="applyYyymmdd" id="applyYyymmdd" type="text" ></td></tr><tr><td><label for="applyCode">補換證別：</label></td><td><select name="applyCode" id="applyCode"><option value="0">未領</option><option value="1">領證</option><option value="2">補證</option><option value="3">換證</option></select></td></tr><tr><td><label for="issueSiteId">發證地點行政區域：</label></td><td><select name="issueSiteId" id="issueSiteId"><option value="10001">北縣</option><option value="10002">宜縣</option><option value="10003">桃縣</option><option value="10004">竹縣</option><option value="10005">苗縣</option><option value="10006">中縣</option><option value="10007">彰縣</option><option value="10008">投縣</option><option value="10009">雲縣</option><option value="10010">嘉縣</option><option value="10011">南縣</option><option value="10012">高縣</option><option value="10013">屏縣</option><option value="10014">東縣</option><option value="10015">花縣</option><option value="10016">澎縣</option><option value="10017">基市</option><option value="10018">竹市</option><option value="10020">嘉市</option><option value="09007">連江</option><option value="09020">金門</option><option value="63000">北市</option><option value="64000">高市</option><option value="65000">新北市</option><option value="66000">中市</option><option value="67000">南市</option><option value="68000">桃市</option></select></td></tr><tr><td><input type="button" value="確定送出" onclick="to_useridnum('+userid+')"></td></tr></table>';
	$('#msg').html(data);
	$('#msg-dialog').show();

};

//修改身份證字號
function to_useridnum(id){
	$('#msg-dialog').hide();
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type: "modify_idnum",
			uid:id,
			idnum: $('#idnum').val(),
			applyYyymmdd: $('#applyYyymmdd').val(),
			applyCode:$('#applyCode').val(),
			issueSiteId:$('#issueSiteId').val(),
			location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				//$('#msg').text(json.msg);
				alert(json.msg);
				location.reload();
			}
			//$('#msg-dialog').show();
		}
	);
};

//設定臨時密碼
function edit_pass(userid) {
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{ type:"get_pwd",
		uid:userid },
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				//alert(json.msg);
				var passwd = json.msg;
				var data = '<table><tr><td>設定臨時密碼</td></tr><tr><td>會員編號：'+userid+'</td></tr><tr><td>臨時密碼：<input name="passwd" type="text" id="passwd" value="'+passwd+'" readonly ></td></tr><tr><td><input type="button" value="確定提交" onclick="to_pwd('+userid+')"></td></tr></table>';
				$('#msg').html(data);
				$('#msg-dialog').show();
			}
		}
	);
};

//發送臨時密碼
function to_pwd(id){
	$('#msg-dialog').hide();
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type:"change_pwd",
			uid:id,
			passwd:$('#passwd').val(),
			location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				alert(json.msg);
				//location.reload();
			}
		}
	);
};
//設定兌換密碼
function to_exchangepwd(id){
	$('#msg-dialog').hide();
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type:"exchange_pwd",
			uid:id,
			location_url: '<?php echo $this->tplVar["status"]['get']['location_url']; ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				alert(json.retMsg);
				location.reload();
			}
		}
	);
};

//驗證手機號
function ogetcheck(userid,username,smscode) {
	var data = '<table><tr><td>設定會員資料</td></tr><tr><td>會員編號：'+userid+'</td></tr><tr><td>會員名稱：'+username+'</td></tr><tr><td>驗 證 碼：'+smscode+'</td></tr><tr><td>手機號碼：<input name="phone" type="text" id="phone" value="" ><input name="smscode" type="hidden" id="smscode" value="'+smscode+'" ></td></tr><tr><td><input type="button" value="發送驗證碼" onclick="to_sms('+userid+')">　<input type="button" value="驗證送出" onclick="to_verified('+userid+')"></td></tr></table>';
	$('#msg').html(data);
	$('#msg-dialog').show();
};

//發送驗證碼
function to_sms(id){
	$.post('https://www.saja.com.tw/site/ajax/auth.php',
		{
			type:'verify_phone',
			phone:$('#phone').val(),
			mid:'',
			auth_id:id,
			location_url: '<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				if(json.retCode==1){
					alert(json.retMsg);
				}else {
					alert(json.retMsg);
				}
				location.reload();
			}
		}
	);
};


//手機驗證
function to_verified(id){
	$.post('https://www.saja.com.tw/site/ajax/auth.php',
		{
			type:'check_sms',
			phone:$('#phone').val(),
			smscode:$('#smscode').val(),
			mid:'',
			auth_id:id,
			location_url: '<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				if(json.retCode==1){
					alert(json.retMsg);
				}else{
					alert(json.retMsg);
				}
				location.reload();
			}
		}
	);
};

$(function(){
	//訊息窗
	$('#msg-close').on('click', function(){
		$('#msg-dialog').hide();
	});
});

//取消手機驗證
function del_verified(id){
	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type: "del_verified",
			uid: id,
			location_url: '<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				alert(json.msg);
				location.reload();
			}
		}
	);
};
function to_edit(userid,value,kind){

	if (kind != 'level' && (isNaN(value)==true || value == '') ){
		alert("參數有誤");
		return false;
	}

	$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update',
		{
			type:'level',
			value:value,
			uid:userid,
			location_url: '<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>'
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				console && console.log($.parseJSON(str_json));
				alert(json.msg);
				location.reload();
			}
		}
	);
};
</script>