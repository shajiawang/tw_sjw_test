<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">載具資料</a>>><a>內容</a>
		</div>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">內容資料</div>
			<table class="form-table">
				<tr>
					<td><label for="">載具</label></td>
					<td>殺價王會員載具</td>
				</tr>
				<tr>
					<td><label for="carrier_type">載具類別編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['carrier_type']; ?></td>
				</tr>
				<tr>
					<td><label for="carrier_aggregate">載具是否歸戶</label></td>
					<td><?php if ($this->tplVar['table']['record'][0]['carrier_aggregate'] == 'Y'){echo "是";}else{echo "否";}?></td>
				</tr>
				<tr>
					<td><label for="carrier_id1">載具明碼</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['carrier_id1']; ?></td>
				</tr>
				<tr>
					<td><label for="carrier_id2">載具隱碼</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['carrier_id2']; ?></td>
				</tr>
				<tr>
					<td><label for="phonecode">手機條碼：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['phonecode']; ?></td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="ucid" value="<?php echo $this->tplVar["status"]["get"]["ucid"] ;?>">
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">回列表頁</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>