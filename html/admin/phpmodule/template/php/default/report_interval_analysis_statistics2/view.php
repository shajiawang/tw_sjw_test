<!DOCTYPE html>
<html>

		<head>
				<title>Main Frame</title>
				<?php require_once $this->tplVar["block"]["head"]; ?>
		</head>

		<body>
				<div id="left-control" class="left-control fold-horizontal expanded"></div>
				<div class="breadcrumb header-style">
						<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
						<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
						<a>區間分析統計報表</a>
				</div>
				<div class="searchbar header-style">
						<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
								<ul>
										<li class="search-field">
												<span class="label" for="search_joindate">查詢區間：</span>
												<input name="sdate" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['sdate']; ?>" /> ~
												<input name="edate" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['edate']; ?>"/>
										</li>
										<li class="button">
												<button>搜尋</button>
										</li>
								</ul>
						</form>
				</div>


				<style>
						h2{
								font-weight:bold;
								color:green;
						}
						h3{
								font-weight:bold;
								color:red;
						}
						h4{
								font-weight:bold;
								color:blue;
						}
				</style>


				<!--統計區間區塊-->
				<div class="table-wrapper">
						<ul>
								<?php
										//取得開始日期結束日期並輸出
										$sdate = $this->tplVar["sdate"];
										$edate = $this->tplVar["edate"];
										echo "<h2>統計區間: ".$sdate." ~ ".$edate."</h2>";
								?>
						</ul>
				</div>
				<div class="table-wrapper">				
					<ul>
								<h3>新增會員統計</h3>
						</ul>
						<ul>
								<?php
										$table3['table'] = $this->tplVar["table3"];
										$new_member_count = 0;
										foreach($table3['table']['record'] as $tk => $tv)
										{
											$new_member_count = $new_member_count + 1;
										}
										echo "<h4>  新增會員人數: ".$new_member_count." 人</h4>";
								 ?>
								<?php
										$table4['table'] = $this->tplVar["table4"];
										echo "<h4>  通過手機驗證人數: ".$table4['table']['record'][0]['cnt']." 人</h4>";
										echo "<hr/>";
								 ?>
						</ul>
						<ul>
							<h3>儲值金額統計</h3>
						</ul>
						<ul>
								<?php
										$table5['table'] = $this->tplVar["table5"];
										$income=0;
										foreach($table5['table']['record'] as $tk => $tv)
										{
											echo "<h4>".$tv['name']." : ".round($tv['total'])." 元</h4>";
											$income+=$tv['total'];
										}

								 ?>
								<?php
										echo "<h4><font style='color:#B87800'>儲值總金額 : ".round($income,2)." 元</font></h4>";
										echo "<hr/>";
								 ?>
						</ul>
						<ul>
							<h3>商品下標狀況區塊</h3>
						</ul>
						<ul>
								<?php

										//取得統計資料
										$table['table'] = $this->tplVar["table1"];

										$paid_total_price = 0;
										$free_total_price = 0;

										foreach ($table['table']['record'] as $tk => $tv) {
											if ($table['table']['record'][$tk]['pay'] == 'paid') {
													$paid_total_price = $paid_total_price + round($table['table']['record'][$tk]['saja_sum']);
											} else if ($table['table']['record'][$tk]['pay'] == 'free') {
													$free_total_price = $free_total_price + round($table['table']['record'][$tk]['saja_sum']);
											}
										}
										echo "</br>";
										echo "<h4>付費 總下標手續費 合計: ".$paid_total_price." 元</h4>";
										echo "<h4>免費 總下標手續費 合計: ".$free_total_price." 元</h4>";
										echo "<hr/>";
								?>
						</ul>
						<ul>
							<h3>付費人數統計區塊</h3>
						</ul>
						<ul>
								<?php

									//取得統計資料
									$table['table'] = $this->tplVar["table2"];

									$paid_count = 0;
									$paid_total = 0;

									foreach ($table['table']['record'] as $tk => $tv) {
											if ($table['table']['record'][$tk]['pay'] == 'paid') {
													$paid_count = $paid_count + 1;
													$paid_total = $paid_total + $table['table']['record'][$tk]['subtotal'];
											}
									}

									echo "</br>";
									echo "<h4>付費人數 合計: ".$paid_count." 人</h4>";
									echo "<h4>殺價幣 合計: ".$paid_total." 筆</h4>";
									echo "<hr/>";
								?>
						</ul>
						<ul>
							<h3>免費人數統計區塊</h3>
						</ul>
						<ul>
								<?php
										//取得統計資料
										$table['table'] = $this->tplVar["table2"];

										$free_count = 0;
										$free_total = 0;
										foreach ($table['table']['record'] as $tk => $tv) {
												if ($table['table']['record'][$tk]['pay'] == 'free') {
														$free_count = $free_count + 1;
														$free_total = $free_total + $table['table']['record'][$tk]['subtotal'];
												}
										}
										echo "</br>";
										echo "<h4>免費人數 合計: ".$free_count." 人</h4>";
										echo "<h4>殺價券+免下標手續費 合計: ".$free_total." 筆</h4>";
										echo "<hr/>";
								?>
						</ul>
						<ul>
							<h3>下標結果統計</h3>
						</ul>
						<ul>
								<?php

										//取得總下標人數資料
										$people_count = $this->tplVar["people_count"];

										echo "<h4>總下標人數: ".$people_count." 人</h4>";

										$total_count = ($paid_total+$free_total);
										echo "<h4>總下標筆數: ".$total_count." 筆</h4>";

										if($people_count > 0){
											$pay_ratio = round((($paid_count/$people_count)*100),2) ." %";
										}else{
											$pay_ratio = "0 %";
										}
										echo "<h4>下標付費人數比率: ".$pay_ratio."</h4>";
										echo "<hr/>";

								?>
						</ul>	
				</div>
				
				<!--商品下標狀況區塊-->
				<div class="table-wrapper">
						<ul>
								<h3>結標商品清單</h3>
						</ul>
						<ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>商品編號</th>
																<th>商品名稱</th>
																<th>結標時間</th>
																<th>得標者</th>
																<th>得標價</th>
																<th>下標方式</th>
																<th>筆數</th>
																<th>下標會員數</th>
																<th>下標手續費</th>
																<th>總下標手續費</th>
																<th>點擊數</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table1"];

														$paid_total_price = 0;
														$free_total_price = 0;
                                                        $prod_bid_member_cnt = $this->tplVar["prod_bid_member_cnt"];
														
														foreach ($table['table']['record'] as $tk => $tv) {
																$productid=$table['table']['record'][$tk]['productid'];
																echo "<tr>";
																echo "<td class=\"column\">".$productid."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['product_name']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['offtime']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['nickname']."(".$table['table']['record'][$tk]['userid'].")"."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['price'])."</td>";
																
																if ($table['table']['record'][$tk]['pay'] == 'paid') {
																		echo "<td class=\"column\">"."付費"."</td>";
																} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																		echo "<td class=\"column\">"."免費"."</td>";
																}
																echo "<td class=\"column\">".$table['table']['record'][$tk]['subtotal']."</td>";
                                                                echo "<td class=\"column\">".$prod_bid_member_cnt[$productid]."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['saja_fee'])."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['saja_sum'])."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['hot_prod'])."</td>";
																echo "</tr>";

																if ($table['table']['record'][$tk]['pay'] == 'paid') {
																		$paid_total_price = $paid_total_price + round($table['table']['record'][$tk]['saja_sum']);
																} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																		$free_total_price = $free_total_price + round($table['table']['record'][$tk]['saja_sum']);
																}
														}

												?>
												</tbody>
										</table>
										<?php
												echo "</br>";
												echo "<h4>付費 總下標手續費 合計: ".$paid_total_price." 元</h4>";
												echo "<h4>免費 總下標手續費 合計: ".$free_total_price." 元</h4>";
												echo "<hr/>";
										?>
								</li>
						</ul>
				</div>


				<!--付費人數統計區塊-->
				<div class="table-wrapper">
						<ul>
								<h3>殺價幣(依會員帳號統計 排序由下標筆數多到少)</h3>
						</ul>
						<ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>會員編號</th>
																<th>會員名稱</th>
																<th>使用時間</th>
																<th>下標方式</th>
																<th>筆數</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table2"];

														$paid_count = 0;
												    $paid_total = 0;

														foreach ($table['table']['record'] as $tk => $tv) {
																if ($table['table']['record'][$tk]['pay'] == 'paid') {
																		echo "<tr>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['userid']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['nickname']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['insertt']."</td>";
																		if ($table['table']['record'][$tk]['pay'] == 'paid') {
																				echo "<td class=\"column\">"."付費"."</td>";
																		} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																				echo "<td class=\"column\">"."免費"."</td>";
																		}
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['subtotal']."</td>";
																		echo "</tr>";
																		$paid_count = $paid_count + 1;
																		$paid_total = $paid_total + $table['table']['record'][$tk]['subtotal'];
																}
														}

												?>
												</tbody>
										</table>
										<?php
												echo "</br>";
												echo "<h4>付費人數 合計: ".$paid_count." 人</h4>";
												echo "<h4>殺價幣 合計: ".$paid_total." 筆</h4>";
												echo "<hr/>";
										?>
								</li>
						</ul>
				</div>


				<!--免費人數統計區塊-->
				<div class="table-wrapper">
						<ul>
								<h3>殺價券+免下標手續費(依會員帳號統計 排序由下標筆數多到少)</h3>
						</ul>
						<ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>會員編號</th>
																<th>會員名稱</th>
																<th>使用時間</th>
																<th>下標方式</th>
																<th>筆數</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table2"];

														$free_count = 0;
														$free_total = 0;
														foreach ($table['table']['record'] as $tk => $tv) {
																if ($table['table']['record'][$tk]['pay'] == 'free') {
																		echo "<tr>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['userid']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['nickname']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['insertt']."</td>";
																		if ($table['table']['record'][$tk]['pay'] == 'paid') {
																				echo "<td class=\"column\">"."付費"."</td>";
																		} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																				echo "<td class=\"column\">"."免費"."</td>";
																		}
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['subtotal']."</td>";
																		echo "</tr>";
																		$free_count = $free_count + 1;
																		$free_total = $free_total + $table['table']['record'][$tk]['subtotal'];
																}
														}

												?>
												</tbody>
										</table>
										<?php
												echo "</br>";
												echo "<h4>免費人數 合計: ".$free_count." 人</h4>";
												echo "<h4>殺價券+免下標手續費 合計: ".$free_total." 筆</h4>";
												echo "<hr/>";
										?>
								</li>
						</ul>
				</div>


				<!--下標結果統計區塊-->
				<div class="table-wrapper">
						<ul>
								<h3>下標結果統計</h3>
						</ul>
						<ul>
								<?php

										//取得總下標人數資料
										$people_count = $this->tplVar["people_count"];

										echo "<h4>總下標人數: ".$people_count." 人</h4>";

										$total_count = ($paid_total+$free_total);
										echo "<h4>總下標筆數: ".$total_count." 筆</h4>";

										if($people_count > 0){
											$pay_ratio = round((($paid_count/$people_count)*100),2) ." %";
										}else{
											$pay_ratio = "0 %";
										}
										echo "<h4>下標付費人數比率: ".$pay_ratio."</h4>";
										echo "<hr/>";

								?>
						</ul>
				</div>


				<!--新增會員統計區塊-->
				<div class="table-wrapper">
						<ul>
							<h3>新增會員統計</h3>
						</ul>
						<ul>
							<?php
									/*
									$table3['table'] = $this->tplVar["table3"];
									$new_member_count = 0;
									foreach($table3['table']['record'] as $tk => $tv)
									{
										$new_member_count = $new_member_count + 1;
									}
									*/
									echo "<h4>  新增會員人數: ".$new_member_count." 人</h4>";
							 ?>
						</ul>
						<ul>
						<h3>新增會員清單:</h3>
						</ul>
						<ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>序號</th>
																<th>會員編號</th>
																<th>會員名稱</th>
																<th>新增時間</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														
														foreach ($table3['table']['record'] as $tk => $tv) {
																echo "<tr>";
																echo "<td class=\"column\">".($tk+1)."</td>";
																echo "<td class=\"column\">".$table3['table']['record'][$tk]['userid']."</td>";
																echo "<td class=\"column\">".$table3['table']['record'][$tk]['nickname']."</td>";
																echo "<td class=\"column\">".$table3['table']['record'][$tk]['insertt']."</td>";
																echo "</tr>";
														}

												?>
												</tbody>
										</table>
										<?php
												echo "<hr/>";
										?>
								</li>
						</ul>
				</div>

				<!--下標商品結標排行計算-->
				<div class="table-wrapper">
						<ul>
								<h3>下標商品結標排行計算</h3>
						</ul>
						<ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>筆數</th>
																<th>商品編號</th>
																<th>商品名稱</th>
																<th>結標時間</th>
																<th>商品巿價</th>
																<th>下標手續費</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table6"];



														foreach ($table['table']['record'] as $tk => $tv) {
																echo "<tr>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['num']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['productid']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['name']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['offtime']."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['retail_price'])."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['saja_fee'])."</td>";
																echo "</tr>";
														}

												?>
												</tbody>
										</table>

								</li>
						</ul>
				</div>

	</body>
</html>
