<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">字軌資料管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="year">年份：</label></td>
					<td>
					<select name="year">
						<option value="">請選擇</option>
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
						<option value="2021">2021</option>
						<option value="2022">2022</option>
					</select>
					</td>
				</tr>				
				<tr>
					<td><label for="start_month">起始月份：</label></td>
					<td>
						<select name="start_month">
							<option value="">請選擇</option>
							<option value="01" >01</option>
							<option value="03" >03</option>
							<option value="05" >05</option>
							<option value="07" >07</option>
							<option value="09" >09</option>
							<option value="11" >11</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="end_month">迄止月份：</label></td>
					<td>
						<select name="end_month">
							<option value="">請選擇</option>
							<option value="02" >02</option>
							<option value="04" >04</option>
							<option value="06" >06</option>
							<option value="08" >08</option>
							<option value="10" >10</option>
							<option value="12" >12</option>
						</select>
					</td>
				</tr>
			
				<tr>
					<td><label for="rail_prefix">字軌名稱：</label></td>
					<td><input name="rail_prefix" type="text" /></td>
				</tr>
				<tr>
					<td><label for="seq_start">起始編號：</label></td>
					<td><input name="seq_start" type="number" /></td>
				</tr>
				<tr>
					<td><label for="seq_end">迄止編號：</label></td>
					<td><input name="seq_end" type="number" /></td>
				</tr>
				
				<tr>
					<td><label for="switch">狀態：</label></td>
					<td>
						<select name="switch">
							<option value="">請選擇</option>
							<option value="N" >非使用中</option>
							<option value="Y" >使用中</option>
							<option value="E" >已用完</option>
						</select>
					</td>
				</tr>							
				<tr>
					<td><label for="total">可用數量：</label></td>
					<td><input name="total" type="number" value="50" /></td>
				</tr>
			</table>


			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>