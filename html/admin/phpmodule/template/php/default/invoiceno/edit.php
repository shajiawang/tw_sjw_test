<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">字軌資料管理</a>>><a>編輯</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="year">年份：</label></td>
					<td>
					<select name="year">
						<option value="">請選擇</option>
						<option value="2018" <?php echo $this->tplVar['table']['record'][0]['year']=="2018"?" selected":""; ?>>2018</option>
						<option value="2019" <?php echo $this->tplVar['table']['record'][0]['year']=="2019"?" selected":""; ?>>2019</option>
						<option value="2020" <?php echo $this->tplVar['table']['record'][0]['year']=="2020"?" selected":""; ?>>2020</option>
						<option value="2021" <?php echo $this->tplVar['table']['record'][0]['year']=="2021"?" selected":""; ?>>2021</option>
						<option value="2022" <?php echo $this->tplVar['table']['record'][0]['year']=="2022"?" selected":""; ?>>2022</option>
					</select>
					</td>
				</tr>				
				<tr>
					<td><label for="start_month">起始月份：</label></td>
					<td>
						<select name="start_month">
							<option value="">請選擇</option>
							<option value="01" <?php echo $this->tplVar['table']['record'][0]['start_month']=="01"?" selected":""; ?>>01</option>
							<option value="03" <?php echo $this->tplVar['table']['record'][0]['start_month']=="03"?" selected":""; ?>>03</option>
							<option value="05" <?php echo $this->tplVar['table']['record'][0]['start_month']=="05"?" selected":""; ?>>05</option>
							<option value="07" <?php echo $this->tplVar['table']['record'][0]['start_month']=="07"?" selected":""; ?>>07</option>
							<option value="09" <?php echo $this->tplVar['table']['record'][0]['start_month']=="09"?" selected":""; ?>>09</option>
							<option value="11" <?php echo $this->tplVar['table']['record'][0]['start_month']=="11"?" selected":""; ?>>11</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="end_month">迄止月份：</label></td>
					<td>
						<select name="end_month">
							<option value="">請選擇</option>
							<option value="02" <?php echo $this->tplVar['table']['record'][0]['end_month']=="02"?" selected":""; ?>>02</option>
							<option value="04" <?php echo $this->tplVar['table']['record'][0]['end_month']=="04"?" selected":""; ?>>04</option>
							<option value="06" <?php echo $this->tplVar['table']['record'][0]['end_month']=="06"?" selected":""; ?>>06</option>
							<option value="08" <?php echo $this->tplVar['table']['record'][0]['end_month']=="08"?" selected":""; ?>>08</option>
							<option value="10" <?php echo $this->tplVar['table']['record'][0]['end_month']=="10"?" selected":""; ?>>10</option>
							<option value="12" <?php echo $this->tplVar['table']['record'][0]['end_month']=="12"?" selected":""; ?>>12</option>
						</select>
					</td>
				</tr>
			
				<tr>
					<td><label for="rail_prefix">字軌名稱：</label></td>
					<td><input name="rail_prefix" type="text" value="<?php echo $this->tplVar['table']['record'][0]['rail_prefix']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="seq_start">起始編號：</label></td>
					<td><input name="seq_start" type="number" value="<?php echo $this->tplVar['table']['record'][0]['seq_start']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="seq_end">迄止編號：</label></td>
					<td><input name="seq_end" type="number" value="<?php echo $this->tplVar['table']['record'][0]['seq_end']; ?>" /></td>
				</tr>
				
				<tr>
					<td><label for="switch">狀態：</label></td>
					<td>
						<select name="switch">
							<option value="">ALL</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>非使用中</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>使用中</option>
							<option value="E" <?php echo $this->tplVar['table']['record'][0]['switch']=="E"?" selected":""; ?>>已用完</option>
						</select>
					</td>
				</tr>							
				<tr>
					<td><label for="total">可用數量：</label></td>
					<td><input name="total" type="number" value="<?php echo $this->tplVar['table']['record'][0]['total']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="used">已使用數量：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['used']; ?></td>
				</tr>				
			</table>	


			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="railid" value="<?php echo $this->tplVar["status"]["get"]["railid"] ;?>">

				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>