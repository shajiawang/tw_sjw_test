<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">字軌管理</a>>>
			<a>字軌資料管理</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">字軌名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_year">年份：</span>
					<select name="search_year">
						<option value="">請選擇</option>
						<option value="2018" <?php echo ($this->io->input['get']['search_year']=='2018')?'selected':''; ?> >2018</option>
						<option value="2019" <?php echo ($this->io->input['get']['search_year']=='2019')?'selected':''; ?> >2019</option>
						<option value="2020" <?php echo ($this->io->input['get']['search_year']=='2020')?'selected':''; ?> >2020</option>
						<option value="2021" <?php echo ($this->io->input['get']['search_year']=='2021')?'selected':''; ?> >2021</option>
						<option value="2022" <?php echo ($this->io->input['get']['search_year']=='2022')?'selected':''; ?> >2022</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_start_month">起始月份：</span>
					<select name="search_start_month">
						<option value="">請選擇</option>
						<option value="01" <?php echo ($this->io->input['get']['search_start_month']=='01')?'selected':''; ?> >01</option>
						<option value="03" <?php echo ($this->io->input['get']['search_start_month']=='03')?'selected':''; ?> >03</option>
						<option value="05" <?php echo ($this->io->input['get']['search_start_month']=='05')?'selected':''; ?> >05</option>
						<option value="07" <?php echo ($this->io->input['get']['search_start_month']=='07')?'selected':''; ?> >07</option>
						<option value="09" <?php echo ($this->io->input['get']['search_start_month']=='09')?'selected':''; ?> >09</option>
						<option value="11" <?php echo ($this->io->input['get']['search_start_month']=='11')?'selected':''; ?> >11</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_end_month">迄止月份：</span>
					<select name="search_end_month">
						<option value="">請選擇</option>
						<option value="02" <?php echo ($this->io->input['get']['search_end_month']=='02')?'selected':''; ?> >02</option>
						<option value="04" <?php echo ($this->io->input['get']['search_end_month']=='04')?'selected':''; ?> >04</option>
						<option value="06" <?php echo ($this->io->input['get']['search_end_month']=='06')?'selected':''; ?> >06</option>
						<option value="08" <?php echo ($this->io->input['get']['search_end_month']=='08')?'selected':''; ?> >08</option>
						<option value="10" <?php echo ($this->io->input['get']['search_end_month']=='10')?'selected':''; ?> >10</option>
						<option value="12" <?php echo ($this->io->input['get']['search_end_month']=='12')?'selected':''; ?> >12</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_switch">狀態：</span>
					<select name="search_switch">
						<option value="">ALL</option>
						<option value="N" <?php echo ($this->io->input['get']['search_switch']=='N')?'selected':''; ?> >非使用中</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_switch']=='Y')?'selected':''; ?> >使用中</option>
						<option value="E" <?php echo ($this->io->input['get']['search_switch']=='S')?'selected':''; ?> >已用完</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th>流水編號</th>
								<th>年份</th>
								<th>起始月份</th>
								<th>迄止月份</th>
								<th>字軌名稱</th>
								<th>起始編號</th>
								<th>迄止編號</th>
								<th>可用數量</th>
								<th>使用數量</th>
								<th>狀態</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) { ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?railid=<?php echo $rv["railid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/?railid=<?php echo $rv["railid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td>
								<td class="column" name="railid"><?php echo $rv['railid']; ?></td>
								<td class="column" name="year"><?php echo $rv['year']; ?></td>
								<td class="column" name="start_month"><?php echo $rv['start_month']; ?></td>
								<td class="column" name="end_month"><?php echo $rv['end_month']; ?></td>
								<td class="column" name="rail_prefix"><?php echo $rv['rail_prefix']; ?></td>
								<td class="column" name="seq_start"><?php echo $rv['seq_start']; ?></td>
								<td class="column" name="seq_end"><?php echo $rv['seq_end']; ?></td>
								<td class="column" name="total"><?php echo $rv['total']; ?></td>
								<td class="column" name="used"><?php echo $rv['used']; ?></td>
								<td class="column" name="switch">
								<?php if ($rv['switch'] == 'N') : ?>
									非使用中
								<?php elseif ($rv['switch'] == 'Y') : ?>
									使用中
								<?php else : ?>
									已用完
								<?php endif; ?>
								</td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>