<!DOCTYPE html>
<html>
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Left Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/image.css?t=20190222172156" type="text/css">
<!--	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/left.css" type="text/css">-->
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css">
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-left.css">
	</head>
	<body>
		<div class="canvas">
			<div class="left-header" id="left-header">
				<ul class="button left-collapse">
					<li class="text">全部縮小</li>
					<li class="icon fold-all"></li>
				</ul>
				<div class="splitter"></div>
				<ul class="button left-expand">
					<li class="text">全部展開</li>
					<li class="icon fold-all"></li>
				</ul>
			</div>
			<div class="left-body" id="left-body">
				<ul class="block">
					<li class="title">使用者系統</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">
						<a href="/admin/admin_user/logout" target="default">登出系統</a>
					</div>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">使用者管理</div>
					</li>
					<li class="items">
						<ul>

							<li class="item">
								<a href="/admin/user" target="default">會員帳號</a>
							</li>
							<li class="item">
								<a href="/admin/member_asset" target="default">會員資產</a>
							</li>								
							
						</ul>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">會員中心管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/news" target="default">會員公告</a>
							</li>
							<li class="item">
								<a href="/admin/faq_category" target="default">新手教學分類</a>
							</li>
							<li class="item">
								<a href="/admin/faq" target="default">新手教學問答</a>
							</li>
							<li class="item">
								<a href="/admin/tech_category" target="default">殺價密技分類</a>
							</li>
							<li class="item">
								<a href="/admin/tech" target="default">殺價密技問答</a>
							</li>
							<li class="item">
								<a href="/admin/help_category" target="default">幫助中心分類</a>
							</li>
							<li class="item">
								<a href="/admin/help" target="default">幫助中心</a>
							</li>								
						</ul>
					</li>
				</ul>
                <ul class="block">
					<li class="title">競標系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">競標商品管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/product" target="default">商品資料管理</a>
							</li>
								
						</ul>
					</li>
				</ul>
				</ul>
				<ul class="block">
					<li class="title">兌換中心系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">兌換中心商品管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/exchange_product" target="default">兌換商品</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_card" target="default">商品卡管理</a>
							</li>
							<li class="item">
								<a href="/admin/qrcode" target="default">商品二維碼管理</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">客服管理</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">客服功能</div>
					</li>
					<li class="items">
						<ul>
							<?php /*
							<li class="item">
								<a href="/admin/issues" target="default">客戶留言管理</a>
							</li>
							<li class="item">
								<a href="/admin/issues_category" target="default">客服分類管理</a>
							</li>*/?>
							
							<li class="item">
								<a href="/admin/user_sms" target="default">手機驗證送查詢</a>
							</li>
							<li class="item">
								<a href="/admin/user_scode" target="default">推薦送S碼查詢</a>
							</li>
							<li class="item">
								<a href="/admin/user_referral" target="default">推薦送幣查詢</a>
							</li>
							<li class="item">
								<a href="/admin/user_bid" target="default">出價得標查詢</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">金流系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">帳戶管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/add_oscode" target="default">手動加殺價卷</a>
							</li>						
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/add_scode" target="default">手動加超級殺價卷</a>
							</li>								
						</ul>
					</li>
				</ul>
                <ul class="block">
					<li class="title">報表</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">資料統計</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/report_account_manage" target="default">會員帳戶管理</a>
							</li>
							<li class="item">
								<a href="/admin/report_member_statistics" target="default">會員統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_deposit_statistics" target="default">儲值統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_interval_analysis_statistics2" target="default">(行銷用)結標資料統計</a>
							</li>
						</ul>
					</li>
				</ul>				
				<ul class="block">
					<li class="title">微信</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">自動回覆管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/response" target="default">自動回覆</a>
							</li>
							<li class="item">
								<a href="/admin/response_category" target="default">自動回覆分類</a>
							</li>
						</ul>
					</li>
				</ul>

				<ul class="block">
					<li class="title">推播</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">推播功能管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/push_msg" target="default">一般推播管理</a>
							</li>
							<li class="item">
								<a href="/admin/push_msg_manual" target="default">推播排程管理</a>
							</li>
							<li class="item">
								<a href="/admin/push_msg_hand" target="default">即時推播管理</a>
							</li>
						</ul>
					</li>
				</ul>				
			</div>
		</div>
		<script type="text/javascript">
			jQuery(function($){
                //展開項目
				$('.block .folder', $('#left-body')).click(function() {
					$(this)
					.children('.icon').toggleClass('collapsed').end()
					.next('.items').find('ul').slideToggle();
				});
                //全部縮小
				$('.button.left-collapse', $('#left-header')).click(function() {
					$('.folder:has(.icon:not(.collapsed))').click()
				});
                //全部展開
				$('.button.left-expand', $('#left-header')).click(function() {
					$('.folder:has(.icon.collapsed)').click()
				});
			});
		</script>
	</body>
</html>
