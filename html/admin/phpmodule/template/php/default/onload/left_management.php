<!DOCTYPE html>
<html>
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Left Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/image.css?t=20190222172156" type="text/css">
<!--	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/left.css" type="text/css">-->
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css">
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-left.css">
	</head>
	<body>
		<div class="canvas">
			<div class="left-header" id="left-header">
				<ul class="button left-collapse">
					<li class="text">全部縮小</li>
					<li class="icon fold-all"></li>
				</ul>
				<div class="splitter"></div>
				<ul class="button left-expand">
					<li class="text">全部展開</li>
					<li class="icon fold-all"></li>
				</ul>
			</div>
			<div class="left-body" id="left-body">
				<ul class="block">
					<li class="title">使用者系統</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">
						<a href="/admin/admin_user/logout" target="default">登出系統</a>
					</div>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">使用者管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/user" target="default">會員帳號</a>
							</li>
							<li class="item">
								<a href="/admin/member_asset" target="default">會員資產</a>
							</li>								
							<li class="item">
								<a href="/admin/member_bonus" target="default">會員鯊魚點點數</a>
							</li>
							<li class="item">
								<a href="/admin/enterprise" target="default">商家帳號</a>
							</li>
							<li class="item">
								<a href="/admin/enterprise_bonus" target="default">商家鯊魚點點數</a>
							</li>
							<li class="item">
								<a href="/admin/flash_admin" target="default">閃殺管理帳號</a>
							</li>
							<li class="item">
								<a href="/admin/user_extrainfo" target="default">會員卡包資訊</a>
							</li>
							<li class="item">
								<a href="/admin/user_card" target="default">會員信用卡資訊</a>
							</li>
							<li class="item">
								<a href="/admin/user_ibon" target="default">會員ibon兌換資訊</a>
							</li>							
						</ul>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">會員中心管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/help_category" target="default">幫助中心分類</a>
							</li>
							<li class="item">
								<a href="/admin/help" target="default">幫助中心</a>
							</li>								
						</ul>
					</li>					
				</ul>
				
				<ul class="block">
					<li class="title">殺價卷管理</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">分享連結/閃殺活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/scode/11" target="default">分享連結/閃殺查詢</a>
							</li>
							<li class="item">
								<a href="/admin/scode_spr/12" target="default">組數設定</a>
							</li>
						</ul>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">充值滿額活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/scode/21" target="default">充值滿額查詢</a>
							</li>
							<li class="item">
								<a href="/admin/scode_spr/22" target="default">組數設定</a>
							</li>
						</ul>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">殺價卷活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/scode/31" target="default">殺價券活動查詢</a>
							</li>
							<li class="item">
								<a href="/admin/scode_spr/32" target="default">組數設定</a>
							</li>
						</ul>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">殺價卷序號發送</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/scode/41" target="default">殺價券序號發送查詢</a>
							</li>
							<li class="item">
								<a href="/admin/scode_spr/42" target="default">組數設定</a>
							</li>
						</ul>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">直播活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/scode/51" target="default">直播活動查詢</a>
							</li>
							<!--li class="item">
								<a href="/admin/scode_spr/52" target="default">組數設定</a>
							</li-->
						</ul>
					</li>					
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">廣告活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/scode/61" target="default">廣告活動查詢</a>
							</li>
							<!--li class="item">
								<a href="/admin/scode_spr/52" target="default">組數設定</a>
							</li-->
						</ul>
					</li>	
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">競標/下標送殺價券</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/oscode/31" target="default">殺價券活動查詢</a>
							</li>
							<li class="item">
								<a href="/admin/oscode/41" target="default">殺價券序號發送</a>
							</li>
						</ul>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">首充加倍送活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/deposit_times" target="default">首充加倍送查詢</a>
							</li>
						</ul>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">圓夢活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/dream_income" target="default">活動來源管理</a>
							</li>
							<li class="item">
								<a href="/admin/dream_event" target="default">圓夢活動管理</a>
							</li>
							<li class="item">
								<a href="/admin/dream_code" target="default"> 圓夢編號管理</a>
							</li>
						</ul>
					</li>
				</ul>
				
                <ul class="block">
					<li class="title">客服管理</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">客服功能</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/user_bid" target="default">出價得標查詢</a>
							</li>
							<li class="item">
								<a href="/admin/user_sms" target="default">手機驗證送查詢</a>
							</li>
							<li class="item">
								<a href="/admin/user_scode" target="default">推薦送S碼查詢</a>
							</li>
							<li class="item">
								<a href="/admin/user_referral" target="default">推薦送幣查詢</a>
							</li>
							<li class="item">
								<a href="/admin/issues" target="default">客戶留言管理</a>
							</li>
							<li class="item">
								<a href="/admin/issues_category" target="default">客服分類管理</a>
							</li>
						</ul>
					</li>
				</ul>		
				<ul class="block">
					<li class="title">競標系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">競標商品管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/product_category" target="default">商品分類管理</a>
							</li>
							<li class="item">
								<a href="/admin/product" target="default">商品資料管理</a>
							</li>
							<li class="item">
								<a href="/admin/profit_sharing_category" target="default">拆分帳目分類管理</a>
							</li>
							<li class="item">
								<a href="/admin/saja_rule" target="default">下標條件管理</a>
							</li>
							<li class="item">
								<a href="/admin/product_today" target="default">主題商品</a>
							</li>
							<li class="item">
								<a href="/admin/product_limited" target="default">限定商品類別管理</a>
							</li>							
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">訂單系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">訂單相關管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/order" target="default">訂單管理</a>
							</li>
							<li class="item">
								<a href="/admin/order_store_list" target="default">商家訂單查詢</a>
							</li>
							<li class="item">
								<a href="/admin/stock" target="default">庫存管理</a>
							</li>
						</ul>
					</li>
				</ul>				
				
				<ul class="block">
					<li class="title">金流系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">帳戶管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/deposit" target="default">儲值帳戶</a>
							</li>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/deposit_rule" target="default">儲值規則</a>
							</li>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/deposit_rule_item" target="default">儲值規則項目</a>
							</li>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/bonus" target="default">鯊魚點使用明細</a>
							</li>							
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/gift" target="default">禮券帳戶</a>
							</li>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/rebate" target="default">購物金管理</a>
							</li>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/add_oscode" target="default">手動加殺價卷</a>
							</li>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/add_spoint" target="default">手動加殺價幣</a>
							</li>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/add_scode" target="default">手動加超級殺價卷</a>
							</li>								
						</ul>
					</li>
				</ul>
				
				<ul class="block">
					<li class="title">發票系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">發票管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/invoiceno" target="default">字軌管理</a>
							</li>
						</ul>
						<ul>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/invoice" target="default">發票管理</a>
							</li>
						</ul>						
					</li>
                    <li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">折讓管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="<?php echo $this->config->default_main; ?>/allowance/add" target="default">開立折讓單</a>
							</li>
                            <li class="item">
								<a href="<?php echo $this->config->default_main; ?>/allowance/view" target="default">折讓資料維護</a>
							</li>
						</ul>						
					</li>
				</ul>
				
				<ul class="block">
					<li class="title">兌換中心系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">兌換中心商品管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/exchange_mall" target="default">兌換中心</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_store" target="default">兌換店家</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_product_category" target="default">兌換商品分類</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_product" target="default">兌換商品</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_lifepay" target="default">生活繳費管理</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_card" target="default">商品卡管理</a>
							</li>
							<li class="item">
								<a href="/admin/qrcode" target="default">商品二維碼管理</a>
							</li>
						</ul>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">兌換商品活動</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/promote_rule" target="default">活動規則</a>
							</li>
							<li class="item">
								<a href="/admin/promote_rule_item" target="default">活動規則項目</a>
							</li>
						</ul>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">兌換商品序號</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/exchange_ecoupon" target="default">電子卡序號</a>
							</li>
						</ul>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">兌換相關記錄</div>
					</li>
					<li class="items">
						<ul>
						    <li class="item">
								<a href="/admin/exchange_bonus_history" target="default">兌換鯊魚點記錄</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_gift_history" target="default">兌換禮券記錄</a>
							</li>
							<li class="item">
								<a href="/admin/return_history" target="default">退貨記錄</a>
							</li>
						</ul>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">ibon資料管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/ibon_prdate" target="default">ibon檔期管理</a>
							</li>
							<li class="item">
								<a href="/admin/ibon_prlist" target="default">ibon商品管理</a>
							</li>
							<li class="item">
								<a href="/admin/ibon_record" target="default">ibon交易清單</a>
							</li>
							<li class="item">
								<a href="/admin/ibon_log" target="default">ibon操作管理</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">廣告管理</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">各類廣告管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/ad_category" target="default">廣告版位主題</a>
							</li>
							<li class="item">
								<a href="/admin/ad_list" target="default">廣告管理</a>
							</li>
							<li class="item">
								<a href="/admin/ad_popups_list" target="default">首頁彈窗管理</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">報表</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">資料統計</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/report_account_manage" target="default">會員帳戶管理</a>
							</li>
							<li class="item">
								<a href="/admin/report_member_statistics" target="default">會員統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_deposit_statistics" target="default">儲值統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_product_statistics" target="default">下標商品統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_exchange_statistics" target="default">兌換商品統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_ranking" target="default">排行榜</a>
							</li>
							<li class="item">
								<a href="/admin/report_account_device" target="default">會員使用裝置統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_activity_statistics" target="default">活動來源統計</a>
							</li>
							<?php   if($this->io->input['session']['admin_user']['userid']=='10') { ?>
									<li class="item">
										<a href="/admin/report_interval_analysis_statistics" target="default">區間資料統計</a>
									</li>
							<?php  }  else {  ?>
									<li class="item">
										<a href="/admin/report_interval_analysis_statistics2" target="default">(行銷用)結標資料統計</a>
									</li>
							<?php  }  ?>
							<li class="item">
								<a href="/admin/report_accounting_statistics" target="default">財務分析統計報表</a>
							</li>
						</ul>
					</li>
				</ul>
				
				<ul class="block">
					<li class="title">推播</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">推播功能管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/push_msg" target="default">一般推播管理</a>
							</li>
							<li class="item">
								<a href="/admin/push_msg_manual" target="default">推播排程管理</a>
							</li>
							<li class="item">
								<a href="/admin/push_msg_hand" target="default">即時推播管理</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">系統管理</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">系統功能</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/country" target="default">國別管理</a>
							</li>
							<li class="item">
								<a href="/admin/language" target="default">語系管理</a>
							</li>
							<li class="item">
								<a href="/admin/bank" target="default">銀行管理</a>
							</li>
							<li class="item">
								<a href="/admin/loan" target="default">貸款管理</a>
							</li>
							<li class="item">
								<a href="/admin/telecompany" target="default">電信業者管理</a>
							</li>
							<li class="item">
								<a href="/admin/telpaytype" target="default">電信業務管理</a>
							</li>
							<li class="item">
								<a href="/admin/app_version" target="default">app版本管理</a>
							</li>
							<li class="item">
								<a href="/admin/size" target="default">尺寸使用管理</a>
							</li>
							<li class="item">
								<a href="/admin/color" target="default">色彩使用管理</a>
							</li>
							<li class="item">
								<a href="/admin/saja_sys" target="default">系統訊息管理</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<script type="text/javascript">
			jQuery(function($){
                //展開項目
				$('.block .folder', $('#left-body')).click(function() {
					$(this)
					.children('.icon').toggleClass('collapsed').end()
					.next('.items').find('ul').slideToggle();
				});
                //全部縮小
				$('.button.left-collapse', $('#left-header')).click(function() {
					$('.folder:has(.icon:not(.collapsed))').click()
				});
                //全部展開
				$('.button.left-expand', $('#left-header')).click(function() {
					$('.folder:has(.icon.collapsed)').click()
				});
			});
		</script>
	</body>
</html>
