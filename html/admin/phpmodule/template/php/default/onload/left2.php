<!DOCTYPE html>
<html>
	<head>
		<title>Left Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/image.css" type="text/css">
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/left.css" type="text/css">
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div class="canvas">
			<div class="header" id="header">
				<ul class="button collapse">
					<li class="text">全部縮小</li>
					<li class="icon fold-all"></li>
				</ul>
				<div class="splitter"></div>
				<ul class="button expand">
					<li class="text">全部展開</li>
					<li class="icon fold-all"></li>
				</ul>
			</div>
			<div class="body" id="body">
				<ul class="block">
					<li class="title">使用者系統</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">
						<?php if (empty($this->io->input['session']['user'])) :?>
							<a href="<?php echo $this->config->default_main; ?>/admin_user/login" target="default">登入系統</a>
						<?php else : ?>
							<a href="<?php echo $this->config->default_main; ?>/admin_user/logout" target="default">登出系統</a>
						<?php endif; ?>
						</div>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">使用者管理</div>
					</li>
					<li class="items">
						<ul>
						    <li class="item">
								<a href="<?php echo $this->config->default_main; ?>/member_bonus" target="default">會員鯊魚點點數</a>
							</li>
			
						</ul>
					</li>
				</ul>		
				
			</div>
		</div>
		<script type="text/javascript">
			jQuery(function($){
				$('.block .folder', $('#body')).click(function() {
					$(this)
					.children('.icon').toggleClass('expanded collapsed').end()
					.next('.items').find('ul').slideToggle();
				});
				$('.button.collapse', $('#header')).click(function() {
					$('.folder:has(.icon.expanded)').click()
				});
				$('.button.expand', $('#header')).click(function() {
					$('.folder:has(.icon.collapsed)').click()
				});
			});
		</script>
	</body>
</html>
