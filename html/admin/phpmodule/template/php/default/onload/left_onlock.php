<!DOCTYPE html>
<html>
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Left Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/image.css?t=20190222172156" type="text/css">
<!--	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/left.css" type="text/css">-->
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css">
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-left.css">
	</head>
	<body>
		<div class="canvas">
			<div class="left-header" id="left-header">
				<ul class="button left-collapse">
					<li class="text">全部縮小</li>
					<li class="icon fold-all"></li>
				</ul>
				<div class="splitter"></div>
				<ul class="button left-expand">
					<li class="text">全部展開</li>
					<li class="icon fold-all"></li>
				</ul>
			</div>
			<div class="left-body" id="left-body">
				<ul class="block">
					<li class="title">使用者系統</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">
						<a href="/admin/admin_user/logout" target="default">登出系統</a>
					</div>
					</li>
				</ul>
			</div>
		</div>
		<script type="text/javascript">
			jQuery(function($){
                //展開項目
				$('.block .folder', $('#left-body')).click(function() {
					$(this)
					.children('.icon').toggleClass('collapsed').end()
					.next('.items').find('ul').slideToggle();
				});
                //全部縮小
				$('.button.left-collapse', $('#left-header')).click(function() {
					$('.folder:has(.icon:not(.collapsed))').click()
				});
                //全部展開
				$('.button.left-expand', $('#left-header')).click(function() {
					$('.folder:has(.icon.collapsed)').click()
				});
			});
		</script>
	</body>
</html>
