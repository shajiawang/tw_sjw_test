<!DOCTYPE html>
<html>
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Left Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/image.css?t=20190222172156" type="text/css">
<!--	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/left.css" type="text/css">-->
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css">
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-left.css">
	</head>
	<body>
		<div class="canvas">
			<div class="left-header" id="left-header">
				<ul class="button left-collapse">
					<li class="text">全部縮小</li>
					<li class="icon fold-all"></li>
				</ul>
				<div class="splitter"></div>
				<ul class="button left-expand">
					<li class="text">全部展開</li>
					<li class="icon fold-all"></li>
				</ul>
			</div>
			<div class="left-body" id="left-body">
				<ul class="block">
					<li class="title">使用者系統</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">
						<a href="/admin/admin_user/logout" target="default">登出系統</a>
					</div>
					</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">使用者管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/enterprise" target="default">商家帳號</a>
							</li>
							<li class="item">
								<a href="/admin/enterprise_bonus" target="default">商家鯊魚點點數</a>
							</li>
						</ul>
					</li>
				</ul>
				
				<ul class="block">
					<li class="title">訂單系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">訂單相關管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/order/batch_input" target="default">訂單批次處理</a>
							</li>
							<li class="item">
								<a href="/admin/order_store_list" target="default">商家訂單查詢</a>
							</li>
						</ul>
					</li>
				</ul>				
				
				<ul class="block">
					<li class="title">兌換中心系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">兌換中心商品管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/exchange_store" target="default">兌換店家</a>
							</li>
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">廣告管理</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">各類廣告管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/ad_category" target="default">廣告版位主題</a>
							</li>						
							<li class="item">
								<a href="/admin/ad_list" target="default">廣告管理</a>
							</li>
							<li class="item">
								<a href="/admin/ad_popups_list" target="default">首頁彈窗管理</a>
							</li>								
						</ul>
					</li>
				</ul>
				
				<ul class="block">
					<li class="title">客服管理</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">客服功能</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/issues" target="default">客戶留言管理</a>
							</li>
							<li class="item">
								<a href="/admin/issues_category" target="default">客服分類管理</a>
							</li>
						</ul>
					</li>
				</ul>						
				<ul class="block">
					<li class="title">報表</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">資料統計</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/report_ranking" target="default">排行榜</a>
							</li>
							<li class="item">
								<a href="/admin/report_account_device" target="default">會員使用裝置統計</a>
							</li>
							<li class="item">
								<a href="/admin/report_activity_statistics" target="default">活動來源統計</a>
							</li>							
						</ul>
					</li>
				</ul>
				<ul class="block">
					<li class="title">微信</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">自動回覆管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/response" target="default">自動回覆</a>
							</li>
							<li class="item">
								<a href="/admin/response_category" target="default">自動回覆分類</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<script type="text/javascript">
			jQuery(function($){
                //展開項目
				$('.block .folder', $('#left-body')).click(function() {
					$(this)
					.children('.icon').toggleClass('collapsed').end()
					.next('.items').find('ul').slideToggle();
				});
                //全部縮小
				$('.button.left-collapse', $('#left-header')).click(function() {
					$('.folder:has(.icon:not(.collapsed))').click()
				});
                //全部展開
				$('.button.left-expand', $('#left-header')).click(function() {
					$('.folder:has(.icon.collapsed)').click()
				});
			});
		</script>
	</body>
</html>
