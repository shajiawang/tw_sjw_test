<!DOCTYPE html>
<html>
	<head>
	    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Left Frame</title>
	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/image.css?t=20190222172156" type="text/css">
<!--	    <link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/left.css" type="text/css">-->
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
		
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-default.css">
		<link rel="stylesheet" href="<?php echo $this->config->default_main; ?>/css/esStyle/es-left.css">
	</head>
	<body>
		<div class="canvas">
			<div class="left-header" id="left-header">
				<ul class="button left-collapse">
					<li class="text">全部縮小</li>
					<li class="icon fold-all"></li>
				</ul>
				<div class="splitter"></div>
				<ul class="button left-expand">
					<li class="text">全部展開</li>
					<li class="icon fold-all"></li>
				</ul>
			</div>
			<div class="left-body" id="left-body">
				<ul class="block">
					<li class="title">使用者系統</li>
					
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">
						<a href="/admin/admin_user/logout" target="default">登出系統</a>
					</div>
					</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">使用者管理</div>
					</li>
					<li class="items">					
						<ul>
						    <!-- li class="item">
								<a href="/admin/user" target="default">會員帳號</a>
						    </li -->
							<li class="item">
								<a href="/admin/user_extrainfo" target="default">會員卡包資訊</a>
							</li>
						</ul>
					</li>
				</ul>

				<ul class="block">
					<li class="title">競標系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">競標商品管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/product_category" target="default">商品分類管理</a>
							</li>
							<li class="item">
								<a href="/admin/product" target="default">商品資料管理</a>
							</li>
							<li class="item">
								<a href="/admin/saja_rule" target="default">下標條件管理</a>
							</li>
							<li class="item">
								<a href="/admin/product_today" target="default">主題商品</a>
							</li>
							<li class="item">
								<a href="/admin/product_limited" target="default">限定商品類別管理</a>
							</li>							
						</ul>
					</li>
				</ul>
				
				<ul class="block">
					<li class="title">兌換中心系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">兌換中心商品管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/exchange_mall" target="default">兌換中心</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_store" target="default">兌換店家</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_product_category" target="default">兌換商品分類</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_product" target="default">兌換商品</a>
							</li>
							<li class="item">
								<a href="/admin/exchange_lifepay" target="default">生活繳費管理</a>
							</li>						
							<li class="item">
								<a href="/admin/exchange_card" target="default">商品卡管理</a>
							</li>
						</ul>
					</li>					
				</ul>	
				<ul class="block">
					<li class="title">訂單系統</li>
					<li class="folder">
						<div class="icon fold-vertical collapsed"></div>
						<div class="label">訂單相關管理</div>
					</li>
					<li class="items">
						<ul>
							<li class="item">
								<a href="/admin/order" target="default">訂單管理</a>
							</li>
						</ul>
					</li>
				</ul>				

			</div>
		</div>
		<script type="text/javascript">
			jQuery(function($){
                //展開項目
				$('.block .folder', $('#left-body')).click(function() {
					$(this)
					.children('.icon').toggleClass('collapsed').end()
					.next('.items').find('ul').slideToggle();
				});
                //全部縮小
				$('.button.left-collapse', $('#left-header')).click(function() {
					$('.folder:has(.icon:not(.collapsed))').click()
				});
                //全部展開
				$('.button.left-expand', $('#left-header')).click(function() {
					$('.folder:has(.icon.collapsed)').click()
				});
			});
		</script>
	</body>
</html>
