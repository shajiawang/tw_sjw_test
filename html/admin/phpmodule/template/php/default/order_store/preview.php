<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<style type="text/css">
		.message {
			width: 300px;background-color: #FFFFFF;border: 1px solid #AAAAAA;border-radius: 4px;display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px;background-color: #CCCCCC;border-radius: 4px;position: relative;
		}
		.message header h2 {
			font-weight: bold;font-size: 16px;line-height: 20px; margin: 0 12px;
		}
		.message header .button-close {
			text-decoration: none;color: black;float: right;margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px;padding: 10px 0;
		}
		.message.center {
		  position: fixed;left: 50%;top: 50%;transform: translate(-50%, -50%);-webkit-transform: translate(-50%, -50%);-moz-transform: translate(-50%, -50%);
		}
		</style>
		
		<script type="text/javascript">
			
			
			
			
		//證認碼確認
		function to_sms(){
			$('#msg-dialog').hide();
			
			if($('#s_sms').val() == "")	{
			    alert('證認碼不能空白');
			    $('#msg-dialog').show();
			    //return false;
			}
			else {
				$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/sms',
				{
					type: "check",
					userid: $('input[name="userid"]').val(),
					smscode: $('input[name="s_sms"]').val()
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						
					    if (json.status == 1) {
					    	alert('請先登入管理者帳號!!');
					    	location.href = "/admin/admin_user/login";
					    }
					    else if (json.status < 1) {
					    	alert('手機證認碼發送失敗');
					    	$('#msg-dialog').hide();
					    	//return false;
					    }
					    else {
					    	var msg = '';
					    	if (json.status == 200){ 
					    		//alert('手機證認碼發送成功');
					    		preview_update();
					    		//location.href = "/admin/order_store/previewupdate/";
					    	} 
					    	else if(json.status == 2){ alert('手機號碼不能空白'); $('#msg-dialog').hide(); } 
					    	else if(json.status == 3){ alert('手機號碼不正確'); $('#msg-dialog').hide(); } 
					    	else if(json.status == 4){ 
					    		alert('證認碼輸入錯誤'); 
					    		$('#count').text(json.SMSCount);
					    		$('#msg-dialog').show(); 
					    		
					    	}
					    	else if(json.status == 5){ alert('目前只提供台灣及大陸簡訊證認'); $('#msg-dialog').hide(); }
					    	else if(json.status == 6){ alert('證認碼輸入錯誤3次，請重新建立訂單'); location.href='<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add'; }
					    }
					}
				});
			}
		}
		
		//交易成功傳簡訊給會員
		function to_success(){
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/sms',
			{
				type: "success",
				evrid: $('input[name="evrid"]').val(),
				userid: $('input[name="userid"]').val()
			},
			function(str_json){
				if (str_json) {
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					
				    if (json.status == 1) {
				    	alert('請先登入管理者帳號!!');
				    	location.href = "/admin/admin_user/login";
				    }
				    else if (json.status < 1) {
				    	alert('交易成功訊息發送失敗');
				    	$('#msg-dialog').hide();
				    	//return false;
				    }
				    else {
				    	var msg = '';
				    	if (json.status == 200){ 
				    		alert('交易成功訊息發送成功');
				    		//location.href = "/admin/order_store/previewupdate/";
				    	} 
				    	else if(json.status == 2){ alert('手機號碼不能空白'); } 
				    	else if(json.status == 3){ alert('手機號碼不正確'); } 
				    	else if(json.status == 5){ alert('目前只提供台灣及大陸簡訊證認'); }
				    }
				}
			});
		}
		
		//更新訂單 exchange_vendor_record
		function preview_update()
		{
			$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/previewupdate',
			{
				type: "update",
				evrid: $('input[name="evrid"]').val(),
				userid: $('input[name="userid"]').val()
			},
			function(str_json){
				if (str_json) {
					var json = $.parseJSON(str_json);
					console && console.log($.parseJSON(str_json));
					
				    if (json.status == 1) {
				    	alert('請先登入管理者帳號!!');
				    	location.href = "/admin/admin_user/login";
				    }
				    else {
				    	var msg = '';
				    	if (json.status == 200){ 
				    		//alert('訂單建立成功');
				    		to_success();
				    		location.href = "/admin/order_store/";
				    	} 
				    	else if(json.status == 2){ alert('會員id錯誤'); } 
				    	else if(json.status == 3){ alert('訂單id錯誤'); } 
				    	else if(json.status == 4){ alert('會員鯊魚點點數不足'); } 
				    }
				}
			});
		}
		
		$(function(){
			//訊息窗
			$('#msg-close').on('click', function(){
				$('#msg-dialog').hide();
			});
			
			//輸入證認碼
			$('#sms').on('click', function(){
				//傳送證認碼給會員
				$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/sms',
				{
					type: "sms",
					userid: $('input[name="userid"]').val()
				},
				function(str_json){
					if (str_json) {
						var json = $.parseJSON(str_json);
						console && console.log($.parseJSON(str_json));
						
					    if (json.status == 1) {
					    	alert('請先登入管理者帳號!!');
					    	location.href = "/admin/admin_user/login";
					    }
					    else if (json.status < 1) {
					    	alert('手機證認碼發送失敗');
					    	return false;
					    }
					    else {
					    	var msg = '';
					    	if (json.status == 200){ 
					    		alert('手機證認碼發送成功');
					    		
					    		$('#msg-dialog').show();
					    	} 
					    	else if(json.status == 2){ alert('手機號碼不能空白'); $('#msg-dialog').hide(); } 
					    	else if(json.status == 3){ alert('手機號碼不正確'); $('#msg-dialog').hide(); } 
					    	else if(json.status == 5){ alert('目前只提供台灣及大陸簡訊證認'); $('#msg-dialog').hide(); }
					    }
					}
				});
			});
		});
		</script>
		
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
			<form class="form" id="form-edit" method="post" action="">
			<div class="form-label">預覽資料</div>
			<table class="form-table">
				<tr>
					<td><label for="nickname">會員暱稱</label></td>
					<td><?php echo $this->tplVar['table']['user']['nickname']; ?></td>
				</tr>
				<tr>
					<td><label for="vendor_prodid">*商品名稱</label></td>
					<td><?php echo $this->tplVar['table']['exchange_product']['name']; ?></td>
				</tr>
				<tr>
					<td><label for="vendor_name">*訂單名稱</label></td>
					<td><?php echo $this->tplVar['table']['exchange_vendor_record']['vendor_name']; ?></td>
				</tr>
				<tr>
					<td><label for="unit_price">*商品單價：</label></td>
					<td><?php echo $this->tplVar['table']['exchange_vendor_record']['unit_price']; ?></td>
				</tr>
				<tr>
					<td><label for="tx_quantity">*商品數量：</label></td>
					<td><?php echo $this->tplVar['table']['exchange_vendor_record']['tx_quantity']; ?></td>
				</tr>
				<tr>
					<td><label for="memo">備註：</label></td>
					<td><?php echo nl2br($this->tplVar['table']['exchange_vendor_record']['memo']); ?></td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="evrid" value="<?php echo $this->tplVar["status"]["get"]["evrid"] ;?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['user']["userid"] ;?>">
				<div class="button cancel"><a href="<?php echo $this->config->default_main.'/'.$this->io->input['get']['fun'].'/edit/evrid='.$this->tplVar["status"]["get"]["evrid"].'&location_url='.$this->tplVar["status"]["get"]["location_url"]; ?>">重新編輯</a></div>
				<div class="button submit"><input type="button" value="確認送出" id="sms" class="submit"></div>
				<div class="clear"></div>
			</div>
			</form>
			
			<article class="message center" id="msg-dialog">
				<header>
					<h2>
						<span class="msg" id="msg-title">輸入證認碼</span>
						<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
					</h2>
				</header>
				<div class="entry">
					<p id="msg">
						<table>
							<tr>
								<td>證認碼：<input name="s_sms" type="text" id="s_sms" ></td>
							</tr>
							<tr>
								<td><input type="button" value="確認送出" onclick="to_sms()"></td>
							</tr>
							<tr>
								<td>您還有 <span id="count"><?php echo $_SESSION['SMSCount']; ?></span> 次輸入證認碼機會。</td>
							</tr>
						</table>
					</p>
				</div>
			</article>
	</body>
</html>