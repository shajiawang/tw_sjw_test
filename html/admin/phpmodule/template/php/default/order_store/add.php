<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">會員帳號：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
		<?php if (!empty($this->tplVar['table']['user']['userid'])) { ?>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增訂單</div>
			<table class="form-table">
				<tr>
					<td><label for="nickname">會員暱稱</label></td>
					<td><?php echo $this->tplVar['table']['user']['nickname']; ?></td>
				</tr>
				<tr>
					<td><label for="nickname">會員鯊魚點點數</label></td>
					<td><?php echo $this->tplVar['table']['bonus']['total_bonus']; ?></td>
				</tr>
				<tr>
					<td><label for="vendor_prodid">*商品名稱</label></td>
					<td>
						<select name="vendor_prodid">
							<option value="">請選擇</option>
							<?php 
								if (is_array($this->tplVar['table']['exchange_product'])) {
									foreach ($this->tplVar['table']['exchange_product'] as $key => $value) {
							?>
							<option value="<?php echo $value['epid']; ?>"><?php echo $value['name']; ?></option>
							<?php } } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="vendor_name">*訂單名稱</label></td>
					<td><input name="vendor_name" type="text" size="20" maxlength="20" />(限填寫20個字)</td>
				</tr>
				<tr>
					<td><label for="unit_price">*商品單價：</label></td>
					<td><input name="unit_price" type="text" size="5" /></td>
				</tr>
				<tr>
					<td><label for="tx_quantity">*商品數量：</label></td>
					<td><input name="tx_quantity" type="text" size="5" maxlength="3" /></td>
				</tr>
				<tr>
					<td><label for="memo">備註：</label></td>
					<td><textarea name="memo" rows="3" cols="50"></textarea></td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])); ?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']["user"]["userid"] ;?>">
				<input type="hidden" name="countryid" value="<?php echo $this->tplVar['table']["user"]["countryid"] ;?>">
				<input type="hidden" name="enterpriseid" value="<?php echo $this->tplVar['table']['enterprise']['enterpriseid']; ?>">
				<input type="hidden" name="esid" value="<?php echo $this->tplVar['table']['enterprise']['esid']; ?>">
				<div class="button submit"><input type="submit" class="submit" value="預覽"></div>
				<div class="button cancel"><input type="reset" class="submit" value="重置"></div>
				<div class="clear"></div>
			</div>
		</form>
		<?php } ?>
		</div>
	</body>
</html>