<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>會員統計</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_phone">手機號：</span>
					<input type="text" name="search_phone" size="20" value="<?php echo $this->io->input['get']['search_phone']; ?>"/>
				</li>			
				<li class="search-field">
					<span class="label" for="search_joindate">新增時間：</span>
					<input name="joindatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindatefrom']; ?>" /> ~ 
					<input name="joindateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindateto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_source">來源方式：</span>
					<select name="search_source" id="search_source"/>
						<option value="">全部</option>
						<option value="1" <?php echo ($this->io->input['get']['search_source'] == 1) ? "selected" : "";?> >banner</option>
						<option value="2" <?php echo ($this->io->input['get']['search_source'] == 2) ? "selected" : "";?> >sms</option>
						<option value="3" <?php echo ($this->io->input['get']['search_source'] == 3) ? "selected" : "";?> >line</option>
						<option value="4" <?php echo ($this->io->input['get']['search_source'] == 4) ? "selected" : "";?> >fb</option>
					
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>流水編號</th>
								<th>來源方式</th>
								<th>手機號</th>
								<th>使用狀態</th>
								<th>新增時間</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="asid"><?php echo $rv['asid']; ?></td>
								<td class="column" name="source"><?php echo $this->tplVar['status']['source'][$rv['source']]; ?></td>
								<td class="column" name="phone"><?php echo $rv['phone']; ?></td>
								<td class="column" name="switch"><?php echo $this->tplVar['status']['switch'][$rv['switch']]; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>