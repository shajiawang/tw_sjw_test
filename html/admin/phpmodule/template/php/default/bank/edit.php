<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="langid">銀行ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['bkid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">銀行名稱：</label></td>
					<td><input name="bankname" type="text" value="<?php echo $this->tplVar['table']['record'][0]['bankname']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="bankid">銀行代號：</label></td>
					<td><input name="bankid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['bankid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="content1">銷帳格式說明 - 信用卡：</label></td>
					<td><textarea name="content1" id="ckeditor_content1" class="description"><?php echo $this->tplVar['table']['record'][0]['content1']; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="bank_switch1">是否提供服務 - 信用卡：</label></td>
					<td>
						<select name="bank_switch1">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['bank_switch1']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['bank_switch1']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>				
				</tr>				
				<tr>
					<td><label for="content2">銷帳格式說明 - 房貸：</label></td>
					<td><textarea name="content2" id="ckeditor_content2" class="description"><?php echo $this->tplVar['table']['record'][0]['content2']; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="bank_switch2">是否提供服務 - 房貸：</label></td>
					<td>
						<select name="bank_switch2">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['bank_switch2']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['bank_switch2']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>				
				</tr>
				<tr>
					<td><label for="content3">銷帳格式說明 - 車貸：</label></td>
					<td><textarea name="content3" id="ckeditor_content3" class="description"><?php echo $this->tplVar['table']['record'][0]['content3']; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="bank_switch3">是否提供服務 - 車貸：</label></td>
					<td>
						<select name="bank_switch3">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['bank_switch3']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['bank_switch3']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>				
				</tr>
				<tr>
					<td><label for="content4">銷帳格式說明 - 信貸：</label></td>
					<td><textarea name="content4" id="ckeditor_content4" class="description"><?php echo $this->tplVar['table']['record'][0]['content4']; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="bank_switch4">是否提供服務 - 信貸：</label></td>
					<td>
						<select name="bank_switch4">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['bank_switch4']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['bank_switch4']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>				
				</tr>
				<tr>
					<td><label for="content5">銷帳格式說明 - 學貸：</label></td>
					<td><textarea name="content5" id="ckeditor_content5" class="description"><?php echo $this->tplVar['table']['record'][0]['content5']; ?></textarea></td>
				</tr>
				<tr>
					<td><label for="bank_switch5">是否提供服務 - 學貸：</label></td>
					<td>
						<select name="bank_switch5">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['bank_switch5']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['bank_switch5']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>				
				</tr>
				<tr>
					<td><label for="switch">使用狀態：</label></td>
					<td>
						<select name="switch">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>關閉</option>
						</select>						
					</td>
				</tr>				
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="bkid" value="<?php echo $this->tplVar["status"]["get"]["bkid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>