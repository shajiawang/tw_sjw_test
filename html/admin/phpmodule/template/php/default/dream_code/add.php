<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="dinum">來源編碼：</label></td>
					<td>
						<select name="dinum">
						<?php foreach($this->tplVar['table']['rt']['dream_income'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['dinum']; ?>" <?php if($this->tplVar['table']['record'][0]['dinum']==$cv['dinum']){ echo 'selected'; } ?> ><?php echo $cv['dinum']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>	
				<tr>
					<td><label for="denum">檔期編碼：</label></td>
					<td>
						<select name="denum">
						<?php foreach($this->tplVar['table']['rt']['dream_event'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['denum']; ?>" <?php if($this->tplVar['table']['record'][0]['denum']==$cv['denum']){ echo 'selected'; } ?> ><?php echo $cv['year']; ?> - <?php echo $cv['denum']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>					
				<tr>
					<td><label for="isdefault">是否預設：</label></td>
					<td>
						<select name="isdefault">
							<option value="Y" >啓用</option>
							<option value="N" selected>關閉</option>
						</select>						
					</td>
				</tr>	
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>