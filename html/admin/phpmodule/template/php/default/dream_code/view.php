<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_dinum">來源編碼：</span>
					<select name="search_dinum">
						<option value="">All</option>
						<?php foreach ($this->tplVar['table']['rt']['dream_income'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['dinum']; ?>" <?php echo ($this->io->input['get']['search_dinum']==$cv['dinum']) ? "selected" : "";?> ><?php echo $cv['dinum']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_denum">檔期編碼：</span>
					<select name="search_denum">
						<option value="">All</option>
						<?php foreach ($this->tplVar['table']['rt']['dream_event'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['denum']; ?>" <?php echo ($this->io->input['get']['search_denum']==$cv['denum']) ? "selected" : "";?> ><?php echo $cv['year']; ?>-<?php echo $cv['denum']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>				
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
						<div class="button">	
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>&search_denum=<?php echo $this->io->input['get']['search_denum']; ?>&search_dinum=<?php echo $this->io->input['get']['search_dinum']; ?>">匯出</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<th>圓夢編號</th>
								<th>來源編碼</th>
								<th>檔期編碼</th>
								<th>是否預設</th>
								<th>觸發計數</th>
								<th>顯示</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/dcid=<?php echo $rv["dcid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/dcid=<?php echo $rv["dcid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="dcid"><?php echo $rv['dcid']; ?></td>
								<td class="column" name="dinum"><?php echo $rv['dinum']; ?></td>
								<td class="column" name="denum"><?php echo $rv['denum']; ?></td>
								<td class="column" name="isdefault"><?php echo $rv['isdefault']; ?></td>
								<td class="column" name="count"><?php echo $rv['count']; ?></td>
								<td class="column" name="switch"><?php echo $rv['switch']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>