<?php
$action_memo = $this->tplVar['table']['record'][0]['memo'];
$action_memo = json_decode($action_memo,true);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-edit" method="post" action="#">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="adid">紀錄編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['lid']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">管理者編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">管理者帳號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">功能類型</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['kind']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">動作</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['active']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">路徑</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['root']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">使用者IP</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['ipaddress']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">動作時間</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['atime']; ?></td>
				</tr>
				<tr>
					<td><label for="adid">備註</label></td>
					<td><textarea name="memo" id="memo" cols="50" rows="5" >"<?php echo $this->tplVar['table']['record'][0]['memo']; ?>"</textarea></td>
				</tr>
	
			</table>
			
			<div class="functions">
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">回列表頁</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>