<!DOCTYPE html>
<?php include_once "def.php" ; ?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">殺幣資料</a>>>
			<a>殺幣資料</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>				
				<li class="search-field">
					<span class="label" for="search_btime">起始日期：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">迄止日期：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_switch">remark：</span>
					<select name="search_remark">
						<option value="">不限</option>
						<option value="1" <?php echo ($this->io->input['get']['search_remark']=='1')?'selected':''; ?> >老殺友回娘家</option>
						<option value="2" <?php echo ($this->io->input['get']['search_remark']=='2')?'selected':''; ?> >記者會活動</option>
						<option value="3" <?php echo ($this->io->input['get']['search_remark']=='3')?'selected':''; ?> >信義區地推活動</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_behav">殺幣來源/用途：</span>
					<select name="search_behav">
						<option value="">不限</option>
						<option value="user_deposit" <?php echo ($this->io->input['get']['search_behav']=='P')?'selected':''; ?> >儲值</option>
						<option value="gift" <?php echo ($this->io->input['get']['search_behav']=='gift')?'selected':''; ?> >贈送</option>
						<option value="system" <?php echo ($this->io->input['get']['search_behav']=='system')?'selected':''; ?> >舊系統轉入</option>
						<option value="user_saja" <?php echo ($this->io->input['get']['search_behav']=='user_saja')?'selected':''; ?> >下標</option>
						<option value="bid_refund" <?php echo ($this->io->input['get']['search_behav']=='bid_refund')?'selected':''; ?> >流標退還</option>
						<option value="process_fee" <?php echo ($this->io->input['get']['search_behav']=='process_fee')?'selected':''; ?> >得標處理費</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_switch">資料狀態：</span>
					<select name="search_switch">
						<option value="">不限</option>
						<option value="N" <?php echo ($this->io->input['get']['search_switch']=='N')?'selected':''; ?> >無效</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_switch']=='Y')?'selected':''; ?> >有效</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
						<div class="functions">
							<!--div class="button">
								<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
							</div -->	
                            <div class="button">
							    <a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						    </div>							
						</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>殺幣資料編號</th>
								<th>會員編號</th>
                                <th>會員暱稱</th>
								<th>殺幣額度</th>
								<th>殺幣來源/用途</th>
								<th>資料狀態</th>
								<th>remark</th>
								<th>備註</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) { ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?spointid=<?php echo $rv["spointid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<!--td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/?spointid=<?php echo $rv["spointid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td-->							
								<td class="column" name="spointid"><?php echo $rv['spointid']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
                                <td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="amount"><?php echo number_format($rv['amount'], 2, '.', ''); ?></td>
								<td class="column" name="behav"><?php echo $arrDesc['behav'][$rv['behav']]; ?></td>
								<td class="column" name="switch"><?php echo $arrDesc['switch'][$rv['switch']]; ?></td>
								<td class="column" name="remark"><?php echo $arrDesc['remark'][$rv['remark']]; ?></td>
								<td class="column" name="memo"><?php echo $rv['memo']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>