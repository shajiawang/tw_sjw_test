<!DOCTYPE html>
<?php include_once "def.php" ; ?>
<?php
       $item = $this->tplVar['table']['record'][0];
?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">殺幣資料管理</a>>><a>修改</a>
		</div>
		<form class="form" id="form-add" method="post" onsubmit="return check();" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">修改殺幣資料</div>
			<table class="form-table">
				<tr>
					<td><label for="spointid">殺幣紀錄編號：</label></td>
					<td>
					   <input name="spointid" id="spointid" type="hidden"  value="<?php  echo $item['spointid']; ?>" /><?php  echo $item['spointid']; ?>
					</td>
					<td><label for="switch">資料狀態：</label></td>
					<td>
					    <select name="switch" id="switch" >
					    <?php
					       foreach($arrDesc['switch'] as $k=>$v) {
							   $selected=($k==$item['switch']?"selected":"");
							   echo "<option value='{$k}' ${selected} >".$v."</option>\n";  
						   }
					    ?>
					    </select>	
					</td>
				</tr>
                <tr>				
					<td><label for="userid">用戶編號：</label></td>
					<td><input name="userid" id="userid" type="text"  value="<?php  echo $item['userid']; ?>" /></td>
				
					<td><label for="amount">殺幣點數：</label></td>
					<td><input name="amount" id="amount" type="number" value="<?php  echo number_format($item['amount']); ?>" /></td>
				</tr>
				<tr>
					<td><label for="behav">來源/用途：</label></td>
					<td>
					     <select name="behav" id="behav" >
					     <option value="">無</option>
					  <?php
					       foreach($arrDesc['behav'] as $k=>$v) {
							   $selected=($k==$item['behav']?"selected":"");
							   echo "<option value='{$k}' ${selected} >".$v."</option>\n";  
						   }
					  ?>
					     </select>					
					</td>
					<td><label for="remark">活動註記：</label></td>
					<td>
					  <select name="remark" id="remark" >
					  <?php
					       for($i=0; $i<count($arrDesc['remark']); ++$i) {
							   $selected=($i==$item['remark']?"selected":"");
							   echo "<option value='{$i}'  ${selected} >".$arrDesc['remark'][$i]."</option>\n";  
						   }
					  ?>
					  </select>
					</td>
				</tr>
				<tr>
                   <td><label for="memo">備註說明：</label></td>
				   <td colspan="3"> 				    
					<textarea name="memo" cols="64" rows="8" value=""><?php echo trim($item['memo']); ?></textarea>
				   </td>
				</tr>
			</table>


			<div class="functions">
				<!-- <?php echo $this->tplVar["status"]['get']['location_url']; ?> -->
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="countryid" value="2" />
				<div class="button submit">
				   <input type="submit" value=" 送出 "  class="submit">
				</div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
	<script type="text/javascript" >
		   	   
		   function safeValue(v) {
			       if((typeof v)=="undefined") {
					   return "";
				   } else if(!v || v==""){
                       return "";
                   } else {
					   return v.trim();   
				   }
		   }
		   
		   function check() {
			      var userid=safeValue($('#userid').val());
				  if(userid.length<1) {
					 alert('請填寫用戶編號 !!'); 
				     return false;
				  }
				  
				  var amount = safeValue($('#amount').val());	
                  if(isNaN(amount) || amount=="0") {
                     alert('請確認殺幣點數 !!'); 
				     return false;
                  }

				  var d=safeValue($('#behav').val());
				  if(d.length<1) {
					 alert('請填寫殺幣來源/用途 !!'); 
				     return false;
				  }
				  				 
				  return true;
				  
		   }
	</script>
</html>