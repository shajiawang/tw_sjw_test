<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">發票資料管理</a>>><a>編輯</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<!-- tr>
					<td><label for="switch">資料狀態：</label></td>
					<td>
						<select name="switch">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>有效</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>無效</option>							
						</select>
					</td>
				</tr>	
				<tr -->
					<td><label for="invoiceno">發票編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['invoiceno']; ?></td>
				</tr>
				<tr>
					<td><label for="invoice_datetime">發票時間：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['invoice_datetime']; ?></td>
				</tr>
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<!--tr>
					<td><label for="userid">會員帳號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['uname']; ?></td>
				</tr-->				
				<tr>
					<td><label for="sales_amt">銷售金額：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['sales_amt']; ?></td>
				</tr>
				<tr>
					<td><label for="tax_rate">稅率：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['tax_rate']; ?></td>
				</tr>	
				<tr>
					<td><label for="tax_amt">營業稅：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['tax_amt']; ?></td>
				</tr>
				<tr>
					<td><label for="total_amt">總金額：</label></td>
					<td>
					  <input type="hidden" name="total_amt" id="total_amt" value="<?php echo $this->tplVar['table']['record'][0]['total_amt']; ?>" />
					  <?php echo $this->tplVar['table']['record'][0]['total_amt']; ?>
					</td>
				</tr>	
				<tr>
					<td><label for="allowance_amt">已折讓金額：</label></td>
					<td>
					  <input type="number" name="allowance_amt" id="allowance_amt" value="<?php echo $this->tplVar['table']['record'][0]['allowance_amt']; ?>" />
					</td>
				</tr>
				<tr>
					<td><label for="random_number">發票防偽隨機號碼：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['random_number']; ?></td>
				</tr>
				<tr>
					<td><label for="upload">申報狀態：</label></td>
					<td>
						<select name="upload">
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['upload']=="N"?" selected":""; ?>>尚未申報</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['upload']=="Y"?" selected":""; ?>>已申報完成</option>
							<option value="P" <?php echo $this->tplVar['table']['record'][0]['upload']=="P"?" selected":""; ?>>申報中</option>
						</select>
					</td>
				</tr>
                <tr>
					<td><label for="is_void">作廢：</label></td>
					<td>
						<select name="is_void">
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['is_void']=="N"?" selected":""; ?>>否</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['is_void']=="Y"?" selected":""; ?>>是</option>
							
						</select>
					</td>
				</tr>			
				<tr>
					<td><label for="description">品名：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['description']; ?></td>
				</tr>
				<tr>
					<td><label for="seq_no">數量：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['seq_no']; ?></td>
				</tr>	
				<tr>
					<td><label for="amount">總金額：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['amount']; ?></td>
				</tr>
			</table>
			<table class="form-table" style="margin-left:10rem;">
				<tr>
					<td><label for="is_winprize">是否中獎：</label></td>
					<td>
						<select name="is_winprize">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['is_winprize']=="Y"?" selected":""; ?>>是</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['is_winprize']=="N"?" selected":""; ?>>否</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="userid">會員暱稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['nickname']; ?></td>
				</tr>
				<tr>
					<td><label for="sales_amt">收件人：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['addressee']; ?></td>
				</tr>
				<tr>
					<td><label for="tax_rate">收件人電話：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['rphone']; ?></td>
				</tr>	
				<tr>
					<td><label for="tax_amt">收件地址：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['address']; ?></td>
				</tr>
				<tr>
					<td><label for="allowance_amt">郵遞區號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['area']; ?></td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="siid" value="<?php echo $this->tplVar["status"]["get"]["siid"] ;?>">

				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="button submit"><input type="button" value="列印" id="print" class="submit" onclick="printScreen(printdata,printdata2);"></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
		
		<div id="printdata" style="display:none;">
		<table width="100%" ><tr><td>
			<div id="invoiceBox">
				<div class="titleBox">
					<div class="invoiceLogo"><img src="https://s3.hicloud.net.tw/img.saja.com.tw/site/static/img/saja-invoice.png" style="width:7rem;"></div>
					<div class="invoiceTitle">電子發票證明聯</div>
					<div class="invoiceMonth"><?php echo $this->tplVar['table']['record'][0]['year']; ?>年<?php echo $this->tplVar['table']['record'][0]['start_month']; ?>-<?php echo $this->tplVar['table']['record'][0]['end_month']; ?>月</div>
					<div class="invoiceId"><?php echo $this->tplVar['table']['record'][0]['invoiceno']; ?></div>
				</div>
				<div class="infoBox">
					<div class="infoDateBox d-flex">
						<div class="infoDate"><?php echo $this->tplVar['table']['record'][0]['date']; ?></div>
						<div class="infoTime"><?php echo $this->tplVar['table']['record'][0]['time']; ?></div>
					</div>
					<div class="infoShopping d-flex">
						<div class="randomNum">隨機碼 <?php echo $this->tplVar['table']['record'][0]['random_code']; ?></div>
						<div class="priceBox d-flex">
							<div class="label">總計</div>
							<div class="price ml-auto"><?php echo $this->tplVar['table']['record'][0]['amount']; ?></div>
						</div>
					</div>
					<div class="infoSeller">賣方<?php echo $this->tplVar['table']['record'][0]['SellerId']; ?></div>
				</div>
				<div class="invoiceCodeBox">
					<div class="barCode">
						<img id="barcode" class="img-fluid" src="https://www.saja.com.tw/site/barcodegen/genbarcode_invoice.php?text=<?php echo $this->tplVar['table']['record'][0]['tx_code_bar']; ?>" alt="">
					</div>
					<div class="qrcodeBox d-flex">
						<div class="codeNum">
							<img id="codeNum" class="img-fluid" src="https://www.saja.com.tw/site/phpqrcode/?data=<?php echo $this->tplVar['table']['record'][0]['tx_code_qrcl']; ?>" alt="">
						</div>
						<div class="shopDetails">
							<img id="shopDetails" class="img-fluid" src="https://www.saja.com.tw/site/phpqrcode/?data=<?php echo $this->tplVar['table']['record'][0]['tx_code_qrcr']; ?>" alt="">
						</div>
					</div>
				</div>
			</div>
			</td></tr>
			<tr><td>　　</td></tr>
			<tr><td>
				<div id="invoiceBox2">
				<div class="infoBox">
					<div class="infoDate">交易明細</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">品名：</div>
						<div class="infoTime"><?php echo $this->tplVar['table']['record'][0]['description']; ?></div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">數量：</div>
						<div class="infoTime"><?php echo $this->tplVar['table']['record'][0]['seq_no']; ?></div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">金額：</div>
						<div class="infoTime"><?php echo $this->tplVar['table']['record'][0]['sales_amt']; ?></div>
					</div>
					<hr>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">課稅別：</div>
						<div class="infoTime">應稅 </div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">稅率：</div>
						<div class="infoTime"><?php echo $this->tplVar['table']['record'][0]['tax_rate']; ?></div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">營業稅：</div>
						<div class="infoTime"><?php echo $this->tplVar['table']['record'][0]['tax_amt']; ?></div>
					</div>
					<hr>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">總計：</div>
						<div class="infoTime"><?php echo $this->tplVar['table']['record'][0]['total_amt']; ?></div>
					</div>
					<br>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">備註：</div>
						<div class="infoTime">　　</div>
					</div>
				</div>
			</div>
			</td></tr></table>
			
			 <P style='page-break-after:always'></P>
			 
			<table width="100%" ><tr><td>
			<div id="invoiceBox3">
				<div class="infoBox">
					<div class="infoDate">領獎收據：</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">一、</div>
						<div class="infoTime">金額(新臺幣)　　　　元整</div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">二、</div>
						<div class="infoTime">中獎人簽名(正楷)或蓋章</div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">　　</div>
						<div class="infoTime">_______________________</div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">三、</div>
						<div class="infoTime">電話</div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">　　</div>
						<div class="infoTime">_______________________</div>
					</div>
					<div class="infoDateBox d-flex" style="margin-top:0.3rem">
						<div class="infoDate2">四、</div>
						<div class="infoTime">
							<div class="invoiceCodeBox">
								<div class="qrcodeBox d-flex">
									<div class="codeNum2" style="width:60%" >兌獎若有疑義，請洽客服專線：4128282；或請至下列網址查詢</div>
									<div class="shopDetails3" style="width:40%">
										<img id="shopDetails" class="img-fluid2" src="https://www.saja.com.tw/site/phpqrcode/?data=http://invoice.etax.nat.gov.tw" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="infoDateBox d-flex">
						<div class="infoDate2">　　</div>
						<div class="infoTime" style="font-size:0.1rem">http://invoice.etax.nat.gov.tw</div>
					</div>
					<div class="infoDate">　</div>
					<div class="infoDate">　</div>
					<div class="infoDate">　</div>
					<div class="infoDate">　</div>
					<div class="infoDate">　</div>
				</div>
			</div>
			</td></tr></table> 
		</div>
		
		<div id="printdata2" style="display:none;">
			<style>
			#invoiceBox {
				width: 13.465rem;
				height: 21.259rem;
				/* margin: 3rem auto 0; */
				padding: 0.861rem 1.176rem;
				box-sizing: border-box;
				background: #FFF;
				box-shadow: 0 0 2px 2px #d3d3d3;
			}
			#invoiceBox2 {
				width: 13.465rem;
				height: 21.259rem;
				/* margin: 3rem auto 0; */
				padding: 1.361rem 1.176rem;
				box-sizing: border-box;
				background: #FFF;
				box-shadow: 0 0 2px 2px #d3d3d3;
			}
			#invoiceBox3 {
				width: 13.465rem;
				height: 21.259rem;
				/* margin: 3rem auto 0; */
				padding: 1.361rem 1.176rem;
				box-sizing: border-box;
				background: #FFF;
				box-shadow: 0 0 2px 2px #d3d3d3;
				margin-left:auto; 
			}
			.titleBox {
				font-size: 1.5rem;
				text-align: center;
			}
			.invoiceLogo {
				font-size: 1.1rem;
				letter-spacing: -.1rem;
				font-weight: 600;
				/* margin: .5rem auto; */
			}
			.infoBox {
				font-size: 0.2rem;
				line-height: 1rem;
				margin: .3rem 0 0;
			}
			.infoDateBox .infoDate {
				margin-right: 1rem;
			}
			.infoDateBox .infoDate2 {
				margin-right: 0rem;
			}
			.infoShopping .randomNum {
				margin-right: 3rem;
			}
			.infoShopping .priceBox {
				flex: 1;
			}
			.qrcodeBox .codeNum {
				flex: 1;
				margin-right: 0.75rem;
			}
			.qrcodeBox .codeNum2 {
				flex: 1;
				margin-right: 0.2rem;
			}
			.qrcodeBox .shopDetails {
				flex: 1;
			}
			.print-btn {
				width: 60%;
				max-width: 500px;
				margin: 3rem auto 0;
				padding: 0.75rem;
				text-align: center;
				box-sizing: border-box;
				background: #f9a823;
				color: #FFF;
				font-size: 1.4rem;
				border-radius: 50vh;
				display:block;
				cursor: pointer;
			}
			.canvasBox {
				padding-top: 1.5rem;
				padding-bottom: 1.5rem;
				text-align: center;
				display: block;
			}
			.canvasBox #myCanvas {
				margin: 0 auto;
				border: 1px solid #333;
			}
			.img-fluid {
				max-width: 100%;
				height: auto;
				vertical-align: middle;
				border-style: none;
			}
			.img-fluid2 {
				max-width: 100%;
				height: auto;
				vertical-align: middle;
				border-style: none;
			}
			.d-flex {
				display: -webkit-box !important;
				display: -ms-flexbox !important;
				display: flex !important;
			}
			</style>
		</div>
	</body>
</html>

<script type="text/javascript">
function printScreen(printlist,printlist2)
  {
     var value = printlist.innerHTML;
	 var cssv = printlist2.innerHTML;
     var printPage = window.open("", "Printing...", "");
     printPage.document.open();
     printPage.document.write("<HTML><head>");
	 printPage.document.write(cssv);
	 printPage.document.write("</head><BODY onload='window.print();window.close()'>");
     // printPage.document.write("<PRE>");
     printPage.document.write(value);
     // printPage.document.write("</PRE>"); 
     printPage.document.close("</BODY></HTML>");
  }
</script>