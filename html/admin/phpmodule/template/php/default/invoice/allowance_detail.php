<!DOCTYPE html>
<?php
    
	$arrAllowance=$this->tplVar["table"]['allowance']['table']['record'];
	$arrInvoice=$this->tplVar["table"]['invoice']['table']['record'];
	
	error_log("[allowance_deail.php] allowance : ".json_encode($arrAllowance));
	error_log("[allowance_deail.php] invoice : ".json_encode($arrInvoice));
?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>	
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>折讓資料編號</th>
								<th>折讓單號</th>
								<th>發票號碼</th>
                                <th>發票日期</th>
								<th>單價</th>
								<th>稅額</th>
								<th>總金額</th>
							</tr>
						</thead>
						<tbody>
							<?php 
							    if (is_array($arrAllowance)) { 
							        $total_amt=0;
							        foreach($arrAllowance as $rk => $rv) { 
							           $total_amount+=$rv['amount'];
							?>
							<tr>
								<td class="column" name="salid"><?php echo $rv['salid']; ?></td>
								<td class="column" name="allowance_no"><?php echo $rv['allowance_no']; ?></td>
								<td class="column" name="ori_invoiceno"><?php echo $rv['ori_invoiceno']; ?></td>
                                <td class="column" name="ori_invoice_date"><?php echo $rv['ori_invoice_date']; ?></td>
								<td class="column" name="unitprice"><?php echo $rv['unitprice']; ?></td>
								<td class="column" name="tax"><?php echo $rv['tax']; ?></td>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
							</tr>
							
							<?php 
							        }
								}; 
							?>
						</tbody>
					</table>
				</li>
				<li><div align="right">折讓歷史明細合計 : <?php echo $total_amount; ?> 元</div></li>
			
				<p/>
				<?php 
				    if($arrInvoice) {   
				       $invoiceData= $arrInvoice[0];
				?>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>發票號碼</th>
								<th>用戶編號</th>
                                <th>發票日期</th>
								<th>銷售額</th>
								<th>稅額</th>
								<th>總金額</th>
								<th>目前折讓金額</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="column" name="invoiceno"><?php echo $invoiceData['invoiceno']; ?></td>
								<td class="column" name="userid"><?php echo $invoiceData['userid']; ?></td>
								<td class="column" name="invoice_datetime"><?php echo $invoiceData['invoice_datetime']; ?></td>
								<td class="column" name="sales_amt"><?php echo $invoiceData['sales_amt']; ?></td>
								<td class="column" name="tax_amt"><?php echo $invoiceData['tax_amt']; ?></td>
								<td class="column" name="total_amt"><?php echo $invoiceData['total_amt']; ?></td>
								<td class="column" name="allowance_amt"><?php echo $invoiceData['allowance_amt']; ?></td>
							</tr>
						</tbody>
					</table>
				</li>
				<?php   }   ?>
			</ul>
		</div>
		<center><input type="button" onClick="window.close();" value="close"/></center>
	</body>
</html>