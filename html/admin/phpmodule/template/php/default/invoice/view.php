<!DOCTYPE html>
<?php
    $arrDesc['switch']['Y']='有效';
	$arrDesc['switch']['N']='<font color="red">無效</font>';
?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">發票管理</a>>>
			<a>發票資料管理</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">發票號碼：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>				
				<li class="search-field">
					<span class="label" for="search_btime">起始日期：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">迄止日期：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_upload">申報狀態：</span>
					<select name="search_upload">
						<option value="">不限</option>
						<option value="N" <?php echo ($this->io->input['get']['search_upload']=='N')?'selected':''; ?> >尚未申報</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_upload']=='Y')?'selected':''; ?> >已申報完成</option>
						<option value="P" <?php echo ($this->io->input['get']['search_upload']=='P')?'selected':''; ?> >申報中</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_is_void">是否作廢：</span>
					<select name="search_is_void">
						<option value="">不限</option>
						<option value="N" <?php echo ($this->io->input['get']['search_is_void']=='N')?'selected':''; ?> >否(正常)</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_is_void']=='Y')?'selected':''; ?> >是(作廢)</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_is_winprize">是否中獎：</span>
					<select name="search_is_winprize">
						<option value="">不限</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_is_winprize']=='Y')?'selected':''; ?> >是</option>
						<option value="N" <?php echo ($this->io->input['get']['search_is_winprize']=='N')?'selected':''; ?> >否</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
						<div class="functions">
							<div class="button">
								<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
							</div>	
                            <div class="button">
							    <a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						    </div>
						</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<!--th></th-->
								<th>發票號碼</th>
								<th>發票時間</th>
								<th>會員編號</th>
                                <th>會員暱稱</th>
								<th>銷售金額</th>
								<!-- th>稅率</th -->
								<th>營業稅</th>
								<th>總金額</th>
								<th>已折讓金額</th>
								<!-- th>發票防偽隨機號碼</th -->
								<th>資料狀態</th>
								<th>申報狀態</th>
								<th>儲值方式</th>
								<th>是否作廢</th>
								<th>是否中獎</th>
								<!-- th>流水檔期編號</th -->
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) { ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<!--td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/?siid=<?php echo $rv["siid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td-->							
								<td class="column" name="invoiceno"><?php echo $rv['invoiceno']; ?></td>
								<td class="column" name="invoice_datetime"><?php echo $rv['invoice_datetime']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
                                <td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="sales_amt"><?php echo $rv['sales_amt']; ?></td>
								<!-- td class="column" name="tax_rate"><?php echo $rv['tax_rate']; ?></td -->
								<td class="column" name="tax_amt"><?php echo $rv['tax_amt']; ?></td>
								<td class="column" name="total_amt"><?php echo $rv['total_amt']; ?></td>
								<td class="column" name="allowance_amt"><?php echo $rv['allowance_amt']; ?></td>
								<!-- td class="column" name="random_code"><?php echo $rv['random_number']; ?></td -->
								<td class="column" name="switch"><?php echo $arrDesc['switch'][$rv['switch']]; ?></td>
								<td class="column" name="invoice_upload">
									<?php if ($rv['upload'] == 'N') : ?>
										尚未申報
									<?php elseif ($rv['upload'] == 'Y') : ?>
										已申報完成
									<?php elseif ($rv['upload'] == 'P') : ?>
										申報中
									<?php else : ?>
										尚未申報										
									<?php endif; ?>
								</td>
								<td class="column" name="dr_name">
								   <?php echo empty($rv['dr_name'])?"其他":$rv['dr_name']; ?>
								</td>
								<!-- td class="column" name="railid"><?php echo $rv['railid']; ?></td -->
								<td class="column" name="is_void">
								  <?php echo $rv['is_void']=="Y"?"作廢":"&nbsp;"; ?>
								</td>
								<td class="column" name="is_winprize">
								  <?php echo $rv['is_winprize']=="Y"?"中獎":"&nbsp;"; ?>
								</td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>