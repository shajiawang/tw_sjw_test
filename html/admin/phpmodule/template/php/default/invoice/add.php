<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">發票管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" onsubmit="return check();" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增發票資料</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">用戶編號：</label></td>
					<td><input name="userid" id="userid" type="text" /></td>
				</tr>
				<tr>
					<td><label for="sales_amt">銷售金額(不含稅)：</label></td>
					<td><input name="sales_amt" id="sales_amt" type="number" /></td>
					<td><label for="tax_rate">稅率：</label></td>
					<td>
					   <input name="tax_rate" id="tax_rate" type="number" value="5" /> %
					</td>
					<td>
					   <input type="button" value="計算稅額 & 金額" onClick="javascript:getTotalAmt();" />
					</td>
				</tr>
				<tr>
					<td><label for="tax_amt">稅額：</label></td>
					<td><input name="tax_amt" id="tax_amt" type="number" /></td>
					<td><label for="total_amt">總金額(含稅)：</label></td>
					<td><input name="total_amt" id="total_amt" type="number" /></td>
				</tr>
				<tr>
				   <td><label for="tax_amt">用途類別：</label></td>
				   <td colspan="3">
				     <select name="utype" >
					    <option value="S" selected >可折讓</option>
						<option value="B">不可折讓</option>
					 </select>
				   </td>
				</tr>
				<tr>
					<td><label for="description1">品項 1：</label></td>
					<td><input name="description1" id="description1" type="text" size="24" value="系統使用費" /></td>
					<td><label for="unitprice1">單價 1：</label></td>
					<td><input name="unitprice1" id="unitprice1"  type="number" size="8" maxlength="8" /></td>
					<td><label for="quantity1">數量 1：</label></td>
					<td><input name="quantity1" id="quantity1" value="1" type="number" size="8" maxlength="8" /></td>
				</tr>
				<!-- tr>
				    <th colspan="6">(以下選填)</th>
				</tr>
				<tr>
					<td><label for="description2">品項 2：</label></td>
					<td><input name="description2" type="text" size="24" /></td>
					<td><label for="unitprice2">單價 2：</label></td>
					<td><input name="unitprice2" type="number" size="8" maxlength="8"/></td>
					<td><label for="quantity2">數量 2：</label></td>
					<td><input name="quantity2" value="1" type="number" size="8" maxlength="8"/></td>
				</tr>
				<tr>
					<td><label for="description3">品項 3：</label></td>
					<td><input name="description3" type="text" size="24" /></td>
					<td><label for="unitprice3">單價 3：</label></td>
					<td><input name="unitprice3" type="number" size="8" maxlength="8"/></td>
					<td><label for="quantity3">數量 3：</label></td>
					<td><input name="quantity3" value="1" type="number" size="8" maxlength="8"/></td>
				</tr>
				<tr>
					<td><label for="description4">品項 4：</label></td>
					<td><input name="description4" type="text" size="24" /></td>
					<td><label for="unitprice4">單價 4：</label></td>
					<td><input name="unitprice4" type="number" size="8" maxlength="8"/></td>
					<td><label for="quantity4">數量 4：</label></td>
					<td><input name="quantity4"  value="1" type="number" size="8" maxlength="8"/></td>
				</tr>
				
				<tr>
					<td><label for="description5">品項 5：</label></td>
					<td><input name="description5" type="text" size="24" /></td>
					<td><label for="unitprice5">單價 5：</label></td>
					<td><input name="unitprice5" type="number" size="8" maxlength="8"/></td>
					<td><label for="quantity5">數量 5：</label></td>
					<td><input name="quantity5" value="1" type="number" size="8" maxlength="8"/></td>
				</tr -->
			</table>


			<div class="functions">
				<!-- <?php echo $this->tplVar["status"]['get']['location_url']; ?> -->
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit">
				   <input type="submit" value=" 送出 "  class="submit">
				</div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
	<script type="text/javascript" >
		   /*
		   $(document).ready(function() {
					
					$('#tax_rate').blur(function(e) {
						 getTotalAmt();
					});   
					$('#sales_amt').blur(function(e) {
						 getTotalAmt();
					});
		   });
		   */
		   function getTotalAmt() {
					var rate = $('#tax_rate').val(); 
					var sales = $('#sales_amt').val();	
					if(!isNaN(rate) && !isNaN(sales)) {
						sales=Math.round(sales);	
						if(rate>=0 && sales>0) {
						   tax=Math.round(sales*rate/100.0);
						   $('#tax_amt').val(tax);
						   $('#total_amt').val(parseInt(sales)+parseInt(tax));		
						}
						return true;
					} else {
					    return false;
					}
		   }
		   
		   function safeValue(v) {
			       if((typeof v)=="undefined") {
					   return "";
				   } else if(!v || v==""){
                       return "";
                   } else {
					   return v.trim();   
				   }
		   }
		   
		   function check() {
			      var userid=safeValue($('#userid').val());
				  if(userid.length<1) {
					 alert('請填寫用戶編號 !!'); 
				     return false;
				  }
				  
				  var sales_amt = safeValue($('#sales_amt').val());	
                  if(sales_amt.length<1 || isNaN(sales_amt)) {
                     alert('請確認銷售金額 !!'); 
				     return false;
                  }

                  var tax_rate = safeValue($('#tax_rate').val());	
                  if(tax_rate.length<1 || isNaN(tax_rate)) {
                     alert('請確認稅率 !!'); 
				     return false;
                  }

                  var tax_amt = safeValue($('#tax_amt').val());	
                  if(tax_amt.length<1 || isNaN(tax_amt)) {
                     alert('請確認稅額 !!'); 
				     return false;
                  }	
				  
				  var total_amt = safeValue($('#total_amt').val());	
                  if(total_amt.length<1 || isNaN(total_amt)) {
                     alert('請確認總金額 !!'); 
				     return false;
                  }	
				  
				  var d=safeValue($('#description1').val());
				  if(d.length<1) {
					 alert('請填寫品項 1 !!'); 
				     return false;
				  }
				  
				  var u=safeValue($('#unitprice1').val());
				  if(u.length<1 || isNaN(u)) {
					 alert('請填寫單價 1 !!'); 
				     return false;
				  }
				  var q=safeValue($('#quantity1').val());
				  if(q.length<1 || isNaN(q)) {
					 alert('請填寫數量 1 !!'); 
				     return false;
				  }
				  /*
			      for(idx=2;idx<=5; ++idx) {
					  d=safeValue($('#description'+idx).val());
				      if(d.length>1) {
                         u=safeValue($('#unitprice'+idx).val());
						 if(u.length<1 || isNaN(u)) {
                             alert('請填寫 單價'+idx+' !!');
							 return false;
                         }

                         q=safeValue($('#quantity'+idx).val());
                         if(q.length<1 || isNaN(q)) {
                             alert('請填寫 數量'+idx+' !!');
							 return false;
                         }						 
                      } 						  
				  } */
				  return true;
				  
		   }
	</script>
</html>