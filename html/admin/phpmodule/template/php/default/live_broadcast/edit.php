<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">直播間</a>>>
			<a>編輯</a>
		</div>
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="adid">直播間ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['lbid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">直播主題：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>" maxlength="100" /></td>
				</tr>
				<tr>
					<td><label for="description">直播內容：</label></td>
					<td><textarea name="description" cols="40" rows="4"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail">直播間主圖：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/broadcast/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="lb_url">連結網址</label></td>
					<td><input name="lb_url" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['lb_url']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="share_link">分享連結網址</label></td>
					<td><input name="share_link" type="text" value="<?php echo $this->tplVar['table']['record'][0]['share_link']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="promote_link">活動連結(ex:領券) : </label></td>
					<td><input name="promote_link" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['promote_link']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="adpage_link">廣告頁連結 : </label></td>
					<td><input name="adpage_link" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['adpage_link']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="keyword">金鑰</label></td>
					<td><input name="keyword" type="text" value="<?php echo $this->tplVar['table']['record'][0]['keyword']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" data-org="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="uptime">刷新時間：</label></td>
					<td><input name="uptime" type="text" value="<?php echo $this->tplVar['table']['record'][0]['uptime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
			</table>
			<table class="form-table">
				<tr>
					<td><label for="lbuserid">直播主：</label></td>
					<td>
						<select name="lbuserid">
						<?php foreach($this->tplVar['table']['rt']['live_broadcast_user'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['lbuserid']; ?>" <?php if($this->tplVar['table']['record'][0]['lbuserid']==$cv['lbuserid']){ echo 'selected'; } ?> ><?php echo $cv['lbname']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>

				<tr>
					<td><label for="pcid">分類：</label></td>
					<td>
						<select name="pcid">
						<?php foreach($this->tplVar['table']['rt']['live_broadcast_category'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['pcid']; ?>" <?php if($this->tplVar['table']['record'][0]['pcid']==$cv['pcid']){ echo 'selected'; } ?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>

			</table>

			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="lbid" value="<?php echo $this->tplVar["status"]["get"]["lbid"] ;?>">
				<input type="hidden" name="oldthumbnail" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail'];?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>
