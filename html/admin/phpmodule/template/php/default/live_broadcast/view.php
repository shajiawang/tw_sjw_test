<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">直播間</a>>>
			<a>列表</a>	
		</div>	
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_lbuserid">直播主：</span>
					<select name="search_lbuserid">
						<option value="" >請選擇</option>
						<?php foreach ($this->tplVar['table']['rt']['live_broadcast_user'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['lbuserid']; ?>" <?php echo ($this->io->input['get']['search_lbuserid']==$cv['lbuserid']) ? "selected" : "";?> ><?php echo $cv['lbname']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_pcid">分類項目：</span>
					<select name="search_pcid">
						<option value="" >請選擇</option>
						<?php foreach ($this->tplVar['table']['rt']['live_broadcast_category'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['pcid']; ?>" <?php echo ($this->io->input['get']['search_pcid']==$cv['pcid']) ? "selected" : "";?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>				
				<li class="search-field">
					<span class="label" for="search_name">直播主題：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>刪除</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_lbid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_lbid=desc">直播ID<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_lbid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_lbid=">直播ID<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_lbid=asc">直播ID<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>直播主題</th>
								<th>直播內容</th>
								<th>直播主名稱</th>
								<th>直播分類</th>
								<th>直播連結</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_ontime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_ontime=desc">上線日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_ontime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=">上線日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=asc">上線日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_offtime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_offtime=desc">下線日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_offtime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=">下線日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=asc">下線日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/lbid=<?php echo $rv["lbid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/lbid=<?php echo $rv["lbid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="lbid"><?php echo $rv['lbid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="description"><?php echo $rv['description']; ?></td>
								<td class="column" name="lbname"><?php echo $rv['lbname']; ?></td>
								<td class="column" name="pcname"><?php echo $rv['pcname']; ?></td>							
								<td class="column" name="lb_url"><?php echo $rv['lb_url']; ?></td>
								<td class="column" name="ontime"><?php echo $rv['ontime']; ?></td>
								<td class="column" name="offtime"><?php echo $rv['offtime']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>