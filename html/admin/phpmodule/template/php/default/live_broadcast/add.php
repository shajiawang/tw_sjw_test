<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">直播間</a>>>
			<a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">直播間主題：</label></td>
					<td><input name="name" type="text" maxlength="100" /></td>
				</tr>
				<tr>
					<td><label for="description">直播間內容：</label></td>
					<td><textarea name="description" cols="40" rows="4"></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail">直播間主圖：</label></td>
					<td><input name="thumbnail" type="file"/></td>
				</tr>
				<tr>
					<td><label for="lb_url">連結網址</label></td>
					<td><input name="lb_url" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="share_link">分享連結網址</label></td>
					<td><input name="share_link" type="text" /></td>
				</tr>
				<tr>
					<td><label for="promote_link">活動連結(ex:領券)</label></td>
					<td><input name="promote_link" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="adpage_link">廣告頁連結</label></td>
					<td><input name="adpage_link" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="keyword">金鑰</label></td>
					<td><input name="keyword" type="text" /></td>
				</tr>
				<tr>
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo date("Y-m-d H:i"); ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo date("Y-m-d H:i", strtotime("+1 year")); ?>" data-org="<?php echo date("Y-m-d H:i", strtotime("+1 year")); ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="0"/></td>
				</tr>
				<tr>
					<td><label for="uptime">刷新時間：</label></td>
					<td><input name="uptime" type="text" value="0"/></td>
				</tr>
			</table>
			<table class="form-table">
				<tr>
					<td><label for="lbuserid">直播主：</label></td>
					<td>
						<select name="lbuserid">
							<option value="">請選擇</option>
						<?php foreach($this->tplVar['table']['rt']['live_broadcast_user'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['lbuserid']; ?>" <?php if($this->tplVar['table']['record'][0]['lbuserid']==$cv['lbuserid']){ echo 'selected'; } ?> ><?php echo $cv['lbname']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>

				<tr>
					<td><label for="pcid">分類：</label></td>
					<td>
						<select name="pcid">
							<option value="">請選擇</option>
						<?php foreach($this->tplVar['table']['rt']['live_broadcast_category'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['pcid']; ?>" <?php if($this->tplVar['table']['record'][0]['pcid']==$cv['pcid']){ echo 'selected'; } ?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>

			</table>

			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>
