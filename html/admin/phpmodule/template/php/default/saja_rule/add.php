<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert" enctype="multipart/form-data">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="srid">下標條件ID：</label></td>
					<td><input name="srid" type="text"/></td>
				</tr>
				<tr>
					<td><label for="name">下標條件名稱：</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="seq">下標條件排序：</label></td>
					<td><input name="seq" type="text"/></td>
				</tr>
				<tr>
					<td><label for="seq">下標條件啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y">啟用</option>
							<option value="N">停用</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>