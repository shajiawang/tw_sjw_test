<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_srid">下標條件ID：</span>
					<input type="text" name="search_srid" size="20" value="<?php echo $this->io->input['get']['search_srid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">下標條件名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<?php /*
						<div class="button">
							<a href="javascript:void(0);" class="delete-all">刪除</a>
						</div>*/?>
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<?php /*
								<th class="list-checkbox">
									<input type="checkbox" class="checkbox-all"/>
								</th>*/?>
								<th></th>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_srid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_srid=desc">下標條件ID<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_srid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_srid=">下標條件ID<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_srid=asc">下標條件ID<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>下標條件名稱</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_seq"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_seq=desc">排序<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_seq"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=">排序<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=asc">排序<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>啟用</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<?php /*<td class="list-checkbox"><input type="checkbox" class="list-item"/></td>*/?>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/srid=<?php echo $rv["srid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/srid=<?php echo $rv["srid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="srid"><?php echo $rv['srid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="seq"><?php echo $rv['seq']; ?></td>
								<td class="column" name="switch"><?php echo $rv['switch']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php require_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>