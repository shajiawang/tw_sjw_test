<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="countryid">國別ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['countryid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">國別名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">國別敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="description">語系：</label></td>
					<td>
						<ul class="relation-list">
						<?php foreach ($this->tplVar['table']['rt']['language'] as $pck => $pcv) : ?>
							<li>
								<?php if (empty($pcv['languageid'])) : ?>
								<input class="relation-item" type="checkbox" name="langid[]" id="langid_<?php echo $pck; ?>" value="<?php echo $pcv['langid']; ?>"/>
								<?php else : ?>
								<input class="relation-item" type="checkbox" name="langid[]" id="langid_<?php echo $pck; ?>" value="<?php echo $pcv['langid']; ?>" checked/>
								<?php endif; ?>
								<span class="relation-item-name" for="langid_<?php echo $pck; ?>"><?php echo $pcv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="countryid" value="<?php echo $this->tplVar["status"]["get"]["countryid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>