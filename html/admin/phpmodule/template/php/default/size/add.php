<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<script src="<?php echo $this->config->default_main; ?>/js/jssize.js"></script>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">尺寸名稱：</label></td>
					<td><input name="name" id="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="description">尺寸描述：</label></td>
					<td><input name="description" id="description" type="text" value=""/></td>
				</tr>
				<tr>
					<td><label for="sizecode">尺寸格式：</label></td>
					<td><input class="jssize" name="sizecode" id="sizecode" type="text" value=""/></td>
				</tr>
				<tr>
					<td><label for="seq">尺寸排序：</label></td>
					<td><input name="seq" id="seq" type="text" value="1"/></td>
				</tr>
				<tr>
					<td><label for="stype">尺寸類型：</label></td>
					<td><select name="stype">
							<option value="1" selected>衣物尺寸</option>
							<option value="2" >重量尺寸</option>
							<option value="3" >週邊尺寸</option>
							<option value="4" >容量尺寸</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="used">使用狀態：</label></td>
					<td><select name="used">
							<option value="N" >關閉</option>
							<option value="Y" selected >啓用</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="kind">使用類型：</label></td>
					<td><select name="kind">
							<option value="ad" >廣告</option>
							<option value="product" selected >殺戮戰場</option>
							<option value="mall" >鯊魚商城</option>
							<option value="deposit" >儲值</option>
							<option value="member" >會員</option>
					</select></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>