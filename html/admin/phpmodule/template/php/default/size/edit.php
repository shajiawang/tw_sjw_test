<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<script src="<?php echo $this->config->default_main; ?>/js/jscolor.js"></script>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="cid">尺寸編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['cid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">尺寸名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">尺寸描述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="colorcode">尺寸格式：</label></td>
					<td><input class="jscolor" name="colorcode" id="colorcode" type="text" value="<?php echo $this->tplVar['table']['record'][0]['colorcode']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">尺寸排序：</label></td>
					<td><input name="seq" id="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="stype">尺寸類型：</label></td>
					<td><select name="stype">
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['stype']=="1"?" selected":""; ?>>衣物尺寸</option>
							<option value="2" <?php echo $this->tplVar['table']['record'][0]['stype']=="2"?" selected":""; ?> >重量尺寸</option>
							<option value="3" <?php echo $this->tplVar['table']['record'][0]['stype']=="3"?" selected":""; ?> >週邊尺寸</option>
							<option value="4" <?php echo $this->tplVar['table']['record'][0]['stype']=="4"?" selected":""; ?> >容量尺寸</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="used">使用狀態：</label></td>
					<td><select name="used">
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['used']=="N"?" selected":""; ?>>關閉</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['used']=="Y"?" selected":""; ?> >啓用</option>
					</select></td>
				</tr>
				<tr>
					<td><label for="kind">使用類型：</label></td>
					<td><select name="kind">
							<option value="ad" <?php echo $this->tplVar['table']['record'][0]['kind']=="ad"?" selected":""; ?>>廣告</option>
							<option value="product" <?php echo $this->tplVar['table']['record'][0]['kind']=="product"?" selected":""; ?> >殺戮戰場</option>
							<option value="mall" <?php echo $this->tplVar['table']['record'][0]['kind']=="mall"?" selected":""; ?>>鯊魚商城</option>
							<option value="deposit" <?php echo $this->tplVar['table']['record'][0]['kind']=="deposit"?" selected":""; ?>>儲值</option>
							<option value="member" <?php echo $this->tplVar['table']['record'][0]['kind']=="member"?" selected":""; ?>>會員</option>
					</select></td>
				</tr>					
				
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="cid" value="<?php echo $this->tplVar["status"]["get"]["cid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>