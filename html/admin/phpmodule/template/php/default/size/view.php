<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">尺寸名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_stype">尺寸類型：</span>
					<select name="search_stype">
						<option value="1" <?php echo ($this->io->input['get']['search_stype']=='1')?'selected':''; ?> >衣物尺寸</option>
						<option value="2" <?php echo ($this->io->input['get']['search_stype']=='2')?'selected':''; ?> >重量尺寸</option>
						<option value="3" <?php echo ($this->io->input['get']['search_stype']=='3')?'selected':''; ?> >週邊尺寸</option>
						<option value="4" <?php echo ($this->io->input['get']['search_stype']=='4')?'selected':''; ?> >容量尺寸</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_sid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_sid=desc">尺寸代號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_sid"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_sid=">尺寸代號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_sid=asc">尺寸代號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_name"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_name=desc">尺寸名稱<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_name"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=">尺寸名稱<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=asc">尺寸名稱<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>尺寸格式</th>
								<th>啟用</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/sid=<?php echo $rv["sid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/sid=<?php echo $rv["sid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="sid"><?php echo $rv['sid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="sizecode"><?php echo $rv['sizecode']; ?></td>
								<td class="column" name="used"><?php echo $rv['used']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>