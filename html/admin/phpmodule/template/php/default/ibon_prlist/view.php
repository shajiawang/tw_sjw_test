<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_no">檔期編號：</span>
					<select name="search_no">
						<option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['prdate'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['no']; ?>" <?php if ($this->tplVar['status']["search"]["search_no"] == $pcv['no']) {echo "selected";} ?> ><?php echo $pcv['date_start']. '<=>' .$pcv['date_end']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">商品品名：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>				
				<li class="search-field">
					<span class="label" for="search_code">活動代號：</span>
					<input type="text" name="search_code" size="20" value="<?php echo $this->io->input['get']['search_code']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_id">商品品號：</span>
					<input type="text" name="search_id" size="20" value="<?php echo $this->io->input['get']['search_id']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
			<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/file/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" enctype="multipart/form-data">
			<ul>
				<li class="search-field">
					<span class="label" for="file_name">選取上傳CSV檔：</span>
					<input type="file" name="file_name" />
				</li>
				<li class="button">
					<button>匯入CSV</button>
				</li>
			</ul>
			</form>			
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th>商品編號</th>
								<th>活動代號</th>
								<th>商品品號</th>
								<th>商品品名</th>
								<th>兌換檔次</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/num=<?php echo $rv["num"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="num"><?php echo $rv['num']; ?></td>
								<td class="column" name="ac_code"><?php echo $rv['ac_code']; ?></td>
								<td class="column" name="id"><?php echo $rv['id']; ?></td>
								<td class="column" name="pname"><?php echo $rv['pname']; ?></td>
								<td class="column" name="prdate_no"><?php echo $rv['prdate_no']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>