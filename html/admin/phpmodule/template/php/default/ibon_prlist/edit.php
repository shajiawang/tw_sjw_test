<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="no">商品編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['num']; ?></td>
				</tr>
				<tr>
					<td><label for="ac_code">活動代號：</label></td>
					<td><input name="ac_code" type="text" value="<?php echo $this->tplVar['table']['record'][0]['ac_code']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="id">商品品號：</label></td>
					<td><input name="id" type="text"  value="<?php echo $this->tplVar['table']['record'][0]['id']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="pname">商品品名：</label></td>
					<td><input name="pname" type="text"  value="<?php echo $this->tplVar['table']['record'][0]['pname']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="price">零售價：</label></td>
					<td><input name="price" type="text"  value="<?php echo $this->tplVar['table']['record'][0]['price']; ?>"/></td>
				</tr>	
				<tr>
					<td><label for="bonus">兌換點數：</label></td>
					<td><input name="bonus" type="text"  value="<?php echo $this->tplVar['table']['record'][0]['bonus']; ?>"/></td>
				</tr>	
				<tr>
					<td><label for="prdate_no">零售價：</label></td>
					<td>
						<select name="prdate_no">
							<option value="">All</option>
						<?php foreach ($this->tplVar['table']['rt']['prdate'] as $pck => $pcv) : ?>
							<option value="<?php echo $pcv['no']; ?>" <?php echo $this->tplVar['table']['record'][0]['prdate_no']==$pcv['no'] ? "selected":""; ?>><?php echo $pcv['date_start']. '<=>' .$pcv['date_end']; ?></option>
						<?php endforeach; ?>
						</select>					
					</td>
				</tr>					
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="num" value="<?php echo $this->tplVar["status"]["get"]["num"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>