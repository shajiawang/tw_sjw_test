<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<!--<li class="search-field">
					<span class="label" for="search_uecid">分類：</span>
					<select name="search_uecid"><option value="" >全部</option>
						<?php foreach ($this->tplVar['table']['rt']['ue_category'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['uecid']; ?>" <?php echo ($this->io->input['get']['search_uecid']==$cv['uecid']) ? "selected" : "";?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>-->
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>						
				<li class="search-field">
					<span class="label" for="search_username">會員帳號：</span>
					<input type="text" name="search_username" size="20" value="<?php echo $this->io->input['get']['search_username']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<!--a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a-->
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>會員id</th>
								<th>會員暱稱</th>
								<th>信用卡末四碼</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/ucid=<?php echo $rv["ucid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="creditcard_num4"><?php echo $rv['creditcard_num4']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>