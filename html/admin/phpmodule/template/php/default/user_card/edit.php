<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="faqid">信用卡ID：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['ucid']; ?></td>
				</tr>
				<tr>
					<td><label for="faqid">會員ID：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="cardbank">發卡銀行名稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['cardbank']; ?></td>
				</tr>
				<tr>
					<td><label for="cardtype">信用卡類型：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['cardtype']; ?></td>
				</tr>
				<tr>
					<td><label for="creditcard_num4">信用卡末四碼：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['creditcard_num4']; ?></td>
				</tr>				
				<tr>
					<td><label for="expiration_month">有效月份：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['expiration_month']; ?></td>
				</tr>				
				<tr>
					<td><label for="expiration_year">有效年份：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['expiration_year']; ?></td>
				</tr>
				<tr>
					<td><label for="tokenData">紅陽回傳編碼：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['tokenData']; ?></td>
				</tr>				
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="ucid" value="<?php echo $this->tplVar["status"]["get"]["ucid"] ;?>">
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">回列表頁</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>