<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_joindate">開始日期：</span>
					<input name="joindatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindatefrom']; ?>" /> ~
				</li>
				<li class="search-field">
					<span class="label" for="search_">結束日期：</span>
					<input name="joindateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindateto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
						<input type="text" name="userid" value="<?php echo $this->io->input['get']['userid']; ?>" />
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>累計送殺價卷張數</th>
								<th>使用殺價卷卷張數</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/view_oscode/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" ><?php echo round($this->tplVar["table"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table2"]['record'][0]['total']); ?></td>
							</tr>
						</tbody>
					</table>
				</li>
			</ul>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>累計送超級殺價卷張數</th>
								<th>使用超級殺價卷卷張數</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/view_scode/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" ><?php echo round($this->tplVar["table3"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table4"]['record'][0]['total']); ?></td>
							</tr>
						</tbody>
					</table>
				</li>
			</ul>
		</div>
		
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>流通殺價幣金額</th>
								<th>使用殺價幣金額</th>
								<th>免費殺價幣金額</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/view_spoint/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" ><?php echo round($this->tplVar["table5"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table6"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table7"]['record'][0]['total']); ?></td>
							</tr>
						</tbody>
					</table>
				</li>
			</ul>
		</div>

		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>流通鯊魚點金額</th>
								<th>使用鯊魚點金額</th>
								<th>免費鯊魚點金額</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/view_bonus/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" ><?php echo round($this->tplVar["table8"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table9"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table10"]['record'][0]['total']); ?></td>
							</tr>
						</tbody>
					</table>
				</li>
			</ul>
		</div>

		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th>流通購物金金額</th>
								<th>使用購物金金額</th>
								<th>免費購物金金額</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/view_rebate/?location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" ><?php echo round($this->tplVar["table11"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table12"]['record'][0]['total']); ?></td>
								<td class="column" ><?php echo round($this->tplVar["table13"]['record'][0]['total']); ?></td>
							</tr>
						</tbody>
					</table>
				</li>
			</ul>
		</div>
	</body>
</html>