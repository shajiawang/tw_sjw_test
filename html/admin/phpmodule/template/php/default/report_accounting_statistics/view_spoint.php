<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_joindate">開始日期：</span>
					<input name="joindatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindatefrom']; ?>" /> ~
				</li>
				<li class="search-field">
					<span class="label" for="search_">結束日期：</span>
					<input name="joindateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindateto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
						<input type="text" name="userid" value="<?php echo $this->io->input['get']['userid']; ?>" />
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>會員編號</th>
								<th>累計取得殺價幣金額</th>
								<th>使用殺價幣金額</th>
								<th>免費殺價幣金額</th>
								<th>儲值殺價幣金額</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" ><?php echo round($rv['userid']); ?></td>
								<td class="column"><?php echo round($rv['all_total']); ?></td>
								<td class="column" ><?php echo round($rv['use_total']); ?></td>
								<td class="column" ><?php echo round($rv['free_total']); ?></td>
								<td class="column" ><?php echo round($rv['deposit_total']); ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>