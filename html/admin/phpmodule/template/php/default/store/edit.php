<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">競標商品分類管理</a>>><a>編輯</a>
		</div>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="pcid">商品分類ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['pcid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">商品分類名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">商品分類敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">商品分類排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">商品分類啟用：</label></td>
					<td>
						<select name="switch">
							<option value="Y"<?php echo $this->tplVar['table']['record'][0]['switch']=="Y"?" selected":""; ?>>啟用</option>
							<option value="N"<?php echo $this->tplVar['table']['record'][0]['switch']=="N"?" selected":""; ?>>停用</option>
						</select>
					</td>
				</tr>
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="pcid" value="<?php echo $this->tplVar["status"]["get"]["pcid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>