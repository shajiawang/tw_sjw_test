<!DOCTYPE html>
<html>

		<head>
				<title>Main Frame</title>
				<?php require_once $this->tplVar["block"]["head"]; ?>
                <script type="text/javascript" src="/admin/js/jquery.canvasjs.min.js"></script>
		</head>

		<body>
				<div id="left-control" class="left-control fold-horizontal expanded"></div>
				<div class="breadcrumb header-style">
						<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
						<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
						<a>區間分析統計報表</a>
				</div>
				<div class="searchbar header-style">
						<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
								<ul>
										<li class="search-field">
												<span class="label" for="search_joindate">查詢區間：</span>
												<input name="sdate" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['sdate']; ?>" /> ~
												<input name="edate" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['edate']; ?>"/>
										</li>
										<li class="button">
												<button>搜尋</button>
										</li>
								</ul>
						</form>
				</div>


				<style>
						h2{
								font-weight:bold;
								color:green;
						}
						h3{
								font-weight:bold;
								color:red;
						}
						h4{
								font-weight:bold;
								color:blue;
						}
				</style>


				<!--統計區間區塊-->
				<div class="table-wrapper">
						<ul>
								<?php
										//取得開始日期結束日期並輸出
										$sdate = $this->tplVar["sdate"];
										$edate = $this->tplVar["edate"];
										echo "<h2>統計區間: ".$sdate." ~ ".$edate."</h2>";
								?>
								<?php ?>
						</ul>
				</div>


				<!--商品下標狀況區塊-->
				<script type="text/javascript">
							  window.onload = function () {
								  var chart = new CanvasJS.Chart("chartContainer", {
									  data: [
									  {
										  type: "column",
										  dataPoints: [
										  <?php 
										     $table = $this->tplVar["table"]['table']['record'];
											 error_log("==>".json_encode($table));
											 $total = count($table);
											 for($idx=0; $idx<24; ++$idx) {
												  $item=$table[$idx];
												  $hour=$idx;
												  if(empty($item))  { 
													 $amt=0;
												  } else {
													 $amt=$item['amt'];  
												  }
												  echo "{ ";
												  echo "x:${hour} , y:${amt}"; 
											      echo "}";
                                                  if($idx<23) echo ",";												  
												  echo "\n";
											 } 
										  ?>
										  ]
									  }
									  ]
								  });
						 		  chart.render();
								  
								  var chart2 = new CanvasJS.Chart("chartContainer2",
									{
										title:{
											text: "Gaming Consoles Sold in 2012"
										},
										legend: {
											maxWidth: 350,
											itemWidth: 120
										},
										data: [
										{
											type: "pie",
											showInLegend: true,
											legendText: "{indexLabel}",
											dataPoints: [
												{ y: 4181563, indexLabel: "PlayStation 3" },
												{ y: 2175498, indexLabel: "Wii" },
												{ y: 3125844, indexLabel: "Xbox 360" },
												{ y: 1176121, indexLabel: "Nintendo DS"},
												{ y: 1727161, indexLabel: "PSP" },
												{ y: 4303364, indexLabel: "Nintendo 3DS"},
												{ y: 1717786, indexLabel: "PS Vita"}
											]
										}
										]
									});
									chart2.render();
							  }
						 </script>
				<div class="table-wrapper" id="chartContainer"  style="height: 500px; width: 80%;" >
						<ul>
								<h3>結標商品清單</h3>
						</ul>
						
						<!-- ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>商品編號</th>
																<th>商品名稱</th>
																<th>結標時間</th>
																<th>下標方式</th>
																<th>筆數</th>
																<th>下標手續費</th>
																<th>總下標手續費</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table1"];

														$paid_total_price = 0;
														$free_total_price = 0;

														foreach ($table['table']['record'] as $tk => $tv) {
																echo "<tr>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['productid']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['product_name']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['offtime']."</td>";
																if ($table['table']['record'][$tk]['pay'] == 'paid') {
																		echo "<td class=\"column\">"."付費"."</td>";
																} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																		echo "<td class=\"column\">"."免費"."</td>";
																}
																echo "<td class=\"column\">".$table['table']['record'][$tk]['subtotal']."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['saja_fee'])."</td>";
																echo "<td class=\"column\">".round($table['table']['record'][$tk]['saja_sum'])."</td>";
																echo "</tr>";

																if ($table['table']['record'][$tk]['pay'] == 'paid') {
																		$paid_total_price = $paid_total_price + round($table['table']['record'][$tk]['saja_sum']);
																} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																		$free_total_price = $free_total_price + round($table['table']['record'][$tk]['saja_sum']);
																}
														}

												?>
												</tbody>
										</table>
										<?php
												echo "</br>";
												echo "<h4>付費 總下標手續費 合計: ".$paid_total_price." 元</h4>";
												echo "<h4>免費 總下標手續費 合計: ".$free_total_price." 元</h4>";
												echo "<hr/>";
										?>
								</li>
						</ul -->
				</div>


				<!--付費人數統計區塊-->
				<div align="center" class="table-wrapper"  id="chartContainer2"  style="height: 300px; width: 80%;"  >
						<ul>
								<h3>殺價幣(依會員帳號統計 排序由下標筆數多到少)</h3>
						</ul>
						<!-- ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>會員編號</th>
																<th>會員名稱</th>
																<th>使用時間</th>
																<th>下標方式</th>
																<th>筆數</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table2"];

														$paid_count = 0;
												    $paid_total = 0;

														foreach ($table['table']['record'] as $tk => $tv) {
																if ($table['table']['record'][$tk]['pay'] == 'paid') {
																		echo "<tr>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['userid']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['nickname']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['insertt']."</td>";
																		if ($table['table']['record'][$tk]['pay'] == 'paid') {
																				echo "<td class=\"column\">"."付費"."</td>";
																		} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																				echo "<td class=\"column\">"."免費"."</td>";
																		}
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['subtotal']."</td>";
																		echo "</tr>";
																		$paid_count = $paid_count + 1;
																		$paid_total = $paid_total + $table['table']['record'][$tk]['subtotal'];
																}
														}

												?>
												</tbody>
										</table>
										<?php
												echo "</br>";
												echo "<h4>付費人數 合計: ".$paid_count." 人</h4>";
												echo "<h4>殺價幣 合計: ".$paid_total." 筆</h4>";
												echo "<hr/>";
										?>
								</li>
						</ul -->
				</div>


				<!--免費人數統計區塊-->
				<div class="table-wrapper">
						<ul>
								<h3>殺價券+免下標手續費(依會員帳號統計 排序由下標筆數多到少)</h3>
						</ul>
						<ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>會員編號</th>
																<th>會員名稱</th>
																<th>使用時間</th>
																<th>下標方式</th>
																<th>筆數</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table2"];

														$free_count = 0;
														$free_total = 0;
														foreach ($table['table']['record'] as $tk => $tv) {
																if ($table['table']['record'][$tk]['pay'] == 'free') {
																		echo "<tr>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['userid']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['nickname']."</td>";
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['insertt']."</td>";
																		if ($table['table']['record'][$tk]['pay'] == 'paid') {
																				echo "<td class=\"column\">"."付費"."</td>";
																		} else if ($table['table']['record'][$tk]['pay'] == 'free') {
																				echo "<td class=\"column\">"."免費"."</td>";
																		}
																		echo "<td class=\"column\">".$table['table']['record'][$tk]['subtotal']."</td>";
																		echo "</tr>";
																		$free_count = $free_count + 1;
																		$free_total = $free_total + $table['table']['record'][$tk]['subtotal'];
																}
														}

												?>
												</tbody>
										</table>
										<?php
												echo "</br>";
												echo "<h4>免費人數 合計: ".$free_count." 人</h4>";
												echo "<h4>殺價券+免下標手續費 合計: ".$free_total." 筆</h4>";
												echo "<hr/>";
										?>
								</li>
						</ul>
				</div>


				<!--下標結果統計區塊-->
				<div class="table-wrapper">
						<ul>
								<h3>下標結果統計</h3>
						</ul>
						<ul>
								<?php

										//取得總下標人數資料
										$people_count = $this->tplVar["people_count"];

										echo "<h4>總下標人數: ".$people_count." 人</h4>";

										$total_count = ($paid_total+$free_total);
										echo "<h4>總下標筆數: ".$total_count." 筆</h4>";

										if($people_count > 0){
											$pay_ratio = round((($paid_count/$people_count)*100),2) ." %";
										}else{
											$pay_ratio = "0 %";
										}
										echo "<h4>下標付費人數比率: ".$pay_ratio."</h4>";
										echo "<hr/>";

								?>
						</ul>
				</div>


				<!--新增會員統計區塊-->
				<div class="table-wrapper">
						<ul>
								<h3>新增會員統計</h3>
						</ul>
						<ul>
								<?php
										$new_member_count = 0;
										foreach($table['table']['record'] as $tk => $tv)
										{
											$new_member_count = $new_member_count + 1;
										}
										echo "<h4>  新增會員人數: ".$new_member_count." 人</h4>";
								 ?>
						</ul>
						<ul>
								<h3>新增會員清單:</h3>
						</ul>
						<ul class="table">
								<li class="header">
								</li>
								<li class="body">
										<table>
												<thead>
														<tr>
																<th>會員編號</th>
																<th>會員名稱</th>
																<th>新增時間</th>
														</tr>
												</thead>

												<tbody>
												<?php

														//取得統計資料
														$table['table'] = $this->tplVar["table3"];

														foreach ($table['table']['record'] as $tk => $tv) {
																echo "<tr>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['userid']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['nickname']."</td>";
																echo "<td class=\"column\">".$table['table']['record'][$tk]['insertt']."</td>";
																echo "</tr>";
														}

												?>
												</tbody>
										</table>
										<?php
												echo "<hr/>";
										?>
								</li>
						</ul>
				</div>


	</body>
</html>
