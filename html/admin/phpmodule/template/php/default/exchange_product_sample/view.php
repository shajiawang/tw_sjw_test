<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<!--<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name">商品名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_description">商品敘述：</span>
					<input type="text" name="search_description" size="20" value="<?php echo $this->io->input['get']['search_description']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_epcid">母類別：</span>
					<select name="search_epcid"><option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['epcid']; ?>" <?php echo ($this->io->input['get']['search_epcid']==$pcv['epcid']) ? 'selected' : ''; ?> ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_esid">店家：</span>
					<select name="search_esid"><option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['store'] as $sk => $sv) : ?>
						<option value="<?php echo $sv['esid']; ?>" <?php echo ($this->io->input['get']['search_esid']==$sv['esid']) ? 'selected' : ''; ?>><?php echo $sv['name']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<?php /*
				<li class="search-field">
					<span class="label" for="search_ontime">上架時間：</span>
					<input type="text" name="search_ontime" size="20" value="<?php echo $this->io->input['get']['search_ontime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_offtime">下架時間：</span>
					<input type="text" name="search_offtime" size="20" value="<?php echo $this->io->input['get']['search_offtime']; ?>" class="datetime time-stop"/>
				</li>
				*/?>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>-->
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/form?mod=add&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_epid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_epid=desc">商品編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_epid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_epid=">商品編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_epid=asc">商品編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>品名</th>
								<th>商品品號</th>
								<th>兌換點數</th>
								<th>貼現率</th>
								<th>活動代號</th>
								<th>交易人次</th>
								<th>售價</th>
								<th>總額</th>
								<th>貼現後總交易額</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/form?mod=edit&epid=<?php echo $rv["epid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<td class="icon"><a class="icon-edit" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/copy/epid=<?php echo $rv["epid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">Copy</a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/epid=<?php echo $rv["epid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td>
								<td class="column" name="num"><?php echo $rv['num']; ?></td>
								<td class="column" name="pname"><?php echo $rv['pname']; ?></td>
								<td class="column" name="id"><?php echo $rv['id']; ?></td>
								<td class="column" name="bonus"><?php echo $rv['bonus']; ?></td>
								<td class="column" name="discount_rate"><?php echo $rv['discount_rate']; ?></td>
								<td class="column" name="ac_code"><?php echo $rv['ac_code']; ?></td>
								<td class="column" name="used"><?php echo $rv['used']; ?></td>
								<td class="column" name="price"><?php echo $rv['price']; ?></td>
								<td class="column" name="total"><?php echo number_format($rv['price'] * (int)str_replace(",", "", $rv['used'])); ?></td>
								<td class="column" name="total_price"><?php echo number_format($rv['price'] * (int)str_replace(",", "", $rv['used']) * $rv['discount_rate']); ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>