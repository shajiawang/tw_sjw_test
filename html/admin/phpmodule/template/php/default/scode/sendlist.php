<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name"><?php echo $this->tplVar["page_name"]; ?>：</span>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>活動名稱</th>
								<th>會員帳號</th>
								<th>加入時間</th>
								<th>贈送張數</th>
							</tr>
						</thead>
						<tbody>
							<?php if(is_array($this->tplVar["table"]['record'])){ ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv){
							//promote_amount, num, memo, insertt, spname, u1name, u2name 
							?>
							<tr>
								<td class="column" name="spname"><?php echo $rv['spname']; ?></td>
								<td class="column" name="u1name"><?php echo $rv['u1name']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="num"><?php echo round($rv['amount']); ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>