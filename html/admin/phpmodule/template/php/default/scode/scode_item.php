<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name"><?php echo $this->tplVar["page_name"]; ?>：</span>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_serial">序號：</span> 
					<input type="text" name="search_serial" size="20" value="<?php echo $this->io->input['get']['search_serial']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_verified">激活狀態：</span> 
					<select name="search_verified">
						<option value="" >ALL</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_verified']=='Y') ? "selected" : "";?> >Yes</option>
						<option value="N" <?php echo ($this->io->input['get']['search_verified']=='N') ? "selected" : "";?> >No</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_batch">批號：</span> 
					<input type="text" name="search_batch" size="20" value="<?php echo $this->io->input['get']['search_batch']; ?>"/>
				</li>
				<li class="button">
					<input type="hidden" name="spid" value="<?php echo $this->io->input['get']['spid']; ?>">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
			
			<!--form id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<ul>
				<li class="search-field">
					<span class="label" for="qty">建立數量：</span> 
					<select name="qty">
						<option value="100" >100筆</option>
						<option value="500" >500筆</option>
						<option value="1000" >1000筆</option>
					</select>
				</li>
				<li class="button">
					<input type="hidden" name="type" value="new_scode_item">
					<input type="hidden" name="spid" value="<?php echo $this->io->input['get']['spid']; ?>">
					<input type="hidden" name="location_url" value="<?php echo $this->io->input['get']['location_url']; ?>">
					<button>建立新序號</button>
				</li>
			</ul>
			</form-->
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/item_add/?spid=<?php echo $this->io->input['get']['spid']; ?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增序號</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>活動名稱</th>
								<th>S碼序號<!--serial--></th>
								<th>序號密碼<!--pwd--></th>
								<th>建立時間<!--insertt--></th>
								<th>批號<!--batch--></th>
								<th>S碼贈送組數<!--amount--></th>
								<th>可激活時間<!--insertt--></th>								
								<th>激活狀態<!--verified--></th>
								<th>激活會員<!--userid--></th>
							</tr>
						</thead>
						<tbody>
							<?php if(is_array($this->tplVar["table"]['record'])){ ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv){
							//ssiid spid userid amount serial pwd verified batch insertt 
							?>
							<tr>
								<td class="column" name="spname"><?php echo $rv['spname']; ?></td>
								<td class="column" name="serial"><?php echo $rv['serial']; ?></td>
								<td class="column" name="pwd"><?php echo $rv['pwd']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="batch"><?php echo $rv['batch']; ?></td>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
								<td class="column" name="verified"><?php echo $rv['offtime']; ?></td>
								<td class="column" name="verified"><?php echo $rv['verified']; ?></td>
								<td class="column" name="u1name"><?php echo $rv['u1name']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>