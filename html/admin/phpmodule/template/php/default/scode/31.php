<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name"><?php echo $this->tplVar["page_name"]; ?>：</span>
				</li>
				<?php /*<li class="search-field">
					<span class="label" for="search_channelid">頻道：</span>
					<select name="search_channelid">
						<?php foreach ($this->tplVar['table']['rt']['channel_rt'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['channelid']; ?>" <?php echo ($this->io->input['get']['search_channelid']==$cv['channelid']) ? "selected" : "";?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>*/?>
				<li class="search-field">
					<span class="label" for="search_name">活動名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
			
			<form method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/file/?t=d&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" enctype="multipart/form-data">
			<ul>
				<li class="search-field">
					<span class="label" for="file_name">選取上傳CSV檔：</span>
					<input type="file" name="file_name" />
				</li>
				<li class="button">
					<button>匯入CSV</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/?t=d&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增活動</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_spid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_spid=desc">活動編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_spid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_spid=">活動編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_spid=asc">活動編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_name"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_name=desc">活動名稱<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_name"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=">活動名稱<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=asc">活動名稱<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_ontime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_ontime=desc">活動開始<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_ontime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=">活動開始<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=asc">活動開始<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_offtime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_offtime=desc">活動結束<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_offtime"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=">活動結束<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=asc">活動結束<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								
								<th>贈送組數加總</th>
								<th>一般發送總數</th>
								<th>狀態</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) { 
							if(strtotime($rv['ontime']) > time() ){
								$stat = '未上線';
							} elseif(time() > strtotime($rv['offtime']) ){
								$stat = '已結束';
							} else {
								$stat = '進行中';
							} ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/?t=d&spid=<?php echo $rv["spid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/del/?t=d&spid=<?php echo $rv["spid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="spid"><?php echo $rv['spid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="ontime"><?php echo substr($rv['ontime'],0,16); ?></td>
								<td class="column" name="offtime"><?php echo substr($rv['offtime'],0,16); ?></td>
								
								<td class="column" name="scode_sum"><?php echo $rv['scode_sum']; ?></td>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
								<td class="column"><?php echo $stat; ?></td>
								<td class="column"><a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/detail/?t=d&spid=<?php echo $rv["spid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">記錄詳情</a></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>