<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/item_insert">
			<div class="form-label"><?php echo $this->tplVar["page_name"]; ?>：新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="offtime">可激活時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop"/></td>
				</tr>
				<tr>
					<td><label for="amount">贈送組數：</label></td>
					<td><input name="amount" type="text"  value="1" /></td>
				</tr>
				<tr>
					<td><label for="qty">筆數：</label></td>
					<td>
						<select name="qty">
							<option value="10" >10筆</option>
							<option value="100" >100筆</option>
							<option value="500" >500筆</option>
							<option value="1000" >1000筆</option>
							<option value="3000" >3000筆</option>
							<option value="5000" >5000筆</option>
						</select>
					</td>
				</tr>				
			</table>
	
			<div class="functions">
				<input type="hidden" name="behav" value="<?php echo $this->tplVar["status"]['get']['t']; ?>">
				<input type="hidden" name="productid" value="0">
				<input type="hidden" name="type" value="new_scode_item">
				<input type="hidden" name="spid" value="<?php echo $this->io->input['get']['spid']; ?>">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		
	</body>
</html>