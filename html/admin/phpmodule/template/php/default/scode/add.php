<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<!-- Select2 -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
		<style type="text/css">.text-danger{color:red;}</style>
		<script type="text/javascript">
		$(function (){
		    $("#productid").select2({
		        language: 'zh-TW',
		        width: '100%',
		        // 最多字元限制
		        maximumInputLength: 10,
		        // 最少字元才觸發尋找, 0 不指定
		        minimumInputLength: 0,
		        // 當找不到可以使用輸入的文字
		        tags: true,
		    });
		    var flag=true;
			$(".datetime").on('change',function(){
				console.log($(".time-start").val()!='');
				console.log($(".time-stop").val()!='');
				if (($(".time-start").val()!='')&&($(".time-stop").val()!='')){
					if ($(".time-stop").val()<=$(".time-start").val()){
						//$( "input[name='"+this.name+"']" ).toggle();
						$('#'+this.id).attr('value', '')
						if (this.name=='ontime'){
							$("input[name='ontime']").css('display','none');
							$('#ontime_msg').css('display','block');
							setTimeout(function(){
								$("input[name='ontime']").css('display','block');
								$('#ontime_msg').css('display','none');
							},3000);					
						}else{
							$("input[name='offtime']").css('display','none');
							$('#offtime_msg').css('display','block');
							setTimeout(function(){
								$("input[name='offtime']").css('display','block');
								$('#offtime_msg').css('display','none');						
							},3000);
						}
					}
				}
		    });    
		})
		</script>		
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label"><?php echo $this->tplVar["page_name"]; ?>：新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">活動名稱：</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="description">活動規則敘述：</label></td>
					<td><input name="description" type="text"/></td>
				</tr>				
				<tr>
					<td><label for="ontime">活動開始：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" /><span id='ontime_msg' class="text-danger" style="display: none">結束時間必須比開始時間晚</span> </td>
				</tr>
				<tr>
					<td><label for="offtime">活動結束：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" required/><span id='offtime_msg' class="text-danger" style="display: none">結束時間必須比開始時間晚</span></td>
				</tr>
				<?php if ($this->tplVar["status"]['get']['t'] == 'b' || $this->tplVar["status"]['get']['t'] == 'lb' ) { ?>
				<tr>
					<td><label for="productid">產品id：</label></td>
					<td><select name="productid" id="productid">
						<?php foreach ($this->tplVar["table"]["rt"]["valid_product"] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['productid']; ?>" ><?php echo $pcv['productid'] .' -- '. $pcv['name'] ; ?></option>
						<?php endforeach; ?>
					</select></td>
				</tr>
				<tr>
					<td><label for="onum">組數：</label></td>
					<td><input name="onum" type="number" value="0" min=0/></td>
				</tr>
				<?php } elseif ($this->tplVar["status"]['get']['t'] == 'ad' || $this->tplVar["status"]['get']['t'] == 'c') { ?>
				<tr>
					<td><label for="productid">產品id：</label></td>
					<td><select name="productid" id="productid">
						<?php foreach ($this->tplVar["table"]["rt"]["valid_product"] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['productid']; ?>" ><?php echo $pcv['productid'] .' -- '. $pcv['name'] ; ?></option>
						<?php endforeach; ?>
					</select></td>
				</tr>
				<tr>
					<td><label for="onum">張數：</label></td>
					<td><input name="onum" type="number" value="0" min=0 /></td>
				</tr>
				<!--tr>
					<td><label for="total">發送總組數：</label></td>
					<td><input name="total" type="text"/></td>
				</tr-->				
				<?php }else{ ?>
				<input name="productid" type="hidden" value="0" />
				<input name="onum" type="hidden" value="0" />
				<?php } ?>	
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="number" value="0" min="0"/></td>
				</tr>				
			</table>

			<?php if ($this->tplVar["status"]['get']['t'] == 'lb') { ?>
			<table class="form-table">
				<tr>
					<td><label for="description">直播主：</label></td>
					<td>
						<ul class="relation-list layer1">
						<?php foreach ($this->tplVar['table']['rt']['live_broadcast_user'] as $pck => $pcv) : ?>
							<li>
								<input class="relation-item" type="checkbox" name="lbu[]" id="lbu_<?php echo $pck; ?>" value="<?php echo $pcv['lbuserid']; ?>"/>
								<span class="relation-item-name" for="lbu_<?php echo $pck; ?>"><?php echo $pcv['lbname']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>			
			</table>				
			<?php } ?>		
			
			<?php if ($this->tplVar["status"]['get']['t'] == 'c') { ?>
			<table class="form-table">
				<tr>
					<td><label for="description">儲值項目：</label></td>
					<td>
					<select name="driid" id="driid">
						<?php foreach ($this->tplVar['table']['rt']['deposit_rule_item'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['driid']; ?>" ><?php echo $pcv['name'] .' -- '. $pcv['dr_name'] ; ?></option>
						<?php endforeach; ?>
					</select>
					</td>
				</tr>			
			</table>				
			<?php } ?>			
			<div class="functions">
				<input type="hidden" name="behav" value="<?php echo $this->tplVar["status"]['get']['t']; ?>">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>