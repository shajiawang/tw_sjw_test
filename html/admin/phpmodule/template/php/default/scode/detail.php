<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name"><?php echo $this->tplVar["page_name"]; ?>：</span>
				</li>
				<?php /*
				<li class="search-field">
					<span class="label" for="search_channelid">頻道：</span>
					<select name="search_channelid">
						<?php foreach ($this->tplVar['table']['rt']['channel_rt'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['channelid']; ?>" <?php echo ($this->io->input['get']['search_channelid']==$cv['channelid']) ? "selected" : "";?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">活動名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>*/?>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<?php if($this->io->input["get"]["t"]=='c'){ ?>
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/adddetail/?t=c&spid=<?php echo $this->io->input["get"]["spid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
					<?php } ?>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>活動名稱</th>
								<th>會員帳號</th>
								
								<?php if($this->io->input["get"]["t"]=='e'){ ?>
								<th>激活時間</th>
								<th>激活數量</th>
								<?php }elseif($this->io->input["get"]["t"]=='d'){ ?>
								<th>取得時間</th>
								<th>取得數量</th>
								<?php }elseif($this->io->input["get"]["t"]=='c'){ ?>
								<th>充值時間</th>
								<th>充值金額</th>
								<?php }else{ ?>
								<th>加入時間</th>
								<th>招募者帳號</th>
								<?php } ?>
								
								<th>S碼贈送組數</th>
							</tr>
						</thead>
						<tbody>
							<?php if(is_array($this->tplVar["table"]['record'])){ ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv){
							//promote_amount, num, memo, insertt, spname, u1name, u2name 
							?>
							<tr>
								<td class="column" name="spname"><?php echo $rv['spname']; ?></td>
								
								<?php if($this->io->input["get"]["t"]=='b'){ ?>
								<td class="column" name="u2name"><?php echo $rv['u2name']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="u1name"><?php echo $rv['u1name']; ?></td>
								<?php }else{ ?>
								<td class="column" name="u1name"><?php echo $rv['u1name']; ?></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="promote_amount"><?php echo $rv['promote_amount']; ?></td>
								<?php } ?>
								
								<td class="column" name="num"><?php echo $rv['num']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>