<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insertdetail">
			<div class="form-label"><?php echo $this->tplVar["page_name"]; ?>：新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">會員id：</label></td>
					<td><input name="userid" type="text"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="spid" value="<?php echo $this->tplVar["status"]['get']['spid']; ?>">
				<input type="hidden" name="behav" value="<?php echo $this->tplVar["status"]['get']['t']; ?>">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>