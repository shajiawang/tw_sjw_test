<!DOCTYPE html>
<?php
echo "<pre>".print_r($this->tplVar['table']['rt']['product_category'],1)."</pre>";
exit;
?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){//活動規則
			$("#promote_rule").change(function(){
				var u='<?php echo $this->config->default_main; ?>/promote_rule_item/view/tpl=widget&search_prid='+ $("#promote_rule").val();
				$('#promote_rule_item').load(u);
			});
		});
		
		$(function(){
			$('.relation-list.layer1 .relation-item')
			.on('change', function(){
				var $this = $(this),
					node = $this.val(),
					checked = $this.prop('checked');

				if (checked) {
					$('.relation-list.layer2 li:has(.relation-item[rel='+node+'])').show();
				} else {
					$('.relation-list.layer2 li:has(.relation-item[rel='+node+'])').hide()
					.find('.relation-item[rel='+node+']').prop('checked', false);
				}				
			}).change();
		});
		
		//新增表單
		$(function(){
			$('#add_item').change(function(){
				if ($(this).val() == '') {
					alert('商品名稱错误!');
					return;
				}
				$('#form-add').submit();
				
				/*$.post(
				'<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert',
				{
					name: '$(this).val()',
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url'];?>'
				});*/
			});
		});
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert" enctype="multipart/form-data" >
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">商品名稱：</label></td>
					<td><input name="name" type="text" id="add_item" value='' /></td>
				</tr>
				<tr>
					<td><label for="description">商品說明：</label></td>
					<td><textarea name="description" id="ckeditor_description"></textarea></td>
				</tr>
				<tr>
					<td><label for="description">外部兌換URL：</label></td>
					<td><input name="tx_url" type="text" ></td>
				</tr>				
				<tr>
					<td><label for="point_price">兌換點數：</label></td>
					<td><input name="point_price" type="text" value='0' data-validate="decimal" class="required" /></td>
				</tr>
				<tr>
					<td><label for="process_fee">處理費：</label></td>
					<td><input name="process_fee" type="text" value='0' data-validate="decimal" class="required" /></td>
				</tr>
				<tr>
					<td><label for="retail_price">商品市價：</label></td>
					<td><input name="retail_price" type="text" value='0' data-validate="decimal" class="required"/></td>
				</tr>
				<tr>
					<td><label for="cost_price">進貨價：</label></td>
					<td><input name="cost_price" type="text" value='0' data-validate="decimal" class="required"/></td>
				</tr>
				<tr>
					<td><label for="seq">商品排序：</label></td>
					<td><input name="seq" type="text" value='0' /></td>
				</tr>
				<tr>
					<td><label for="mob_type">手機限用類型：</label></td>
					<td>
						<select name="mob_type">
							<option value="all" >都顯示</option>
							<option value="n" >都不顯示</option>
							<option value="weixin" >只在微信顯示</option>
							<option value="yixin" >只在易信顯示</option>
							<option value="laiwang" >只在来往顯示</option>
						</select>
					</td>
				</tr>
				<input type="hidden" name="ontime" value="">
				<input type="hidden" name="offtime" value="">
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="thumbnail">商品列表主圖：</label></td>
					<td><input name="thumbnail" type="file"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail_file">商品內頁主圖：</label></td>
					<td><input name="thumbnail_file" type="file"/></td>
				</tr>				
				<tr>
					<td><label for="thumbnail_url">商品主圖(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60"/></td>
				</tr>
				
				<tr>
					<td><label for="epcid">商品母類別：</label></td>
					<td>
						<ul class="relation-list layer1">
						<?php foreach($this->tplVar['table']['rt']['product_category'] as $ck => $cv) : ?>
							<?php if ($cv['layer'] == 1) : ?>
							<li>
								<input class="relation-item" type="checkbox" name="epcid[]" id="epcid_<?php echo $ck; ?>" value="<?php echo $cv['epcid']; ?>"/>
								<span class="relation-item-name" for="epcid_<?php echo $ck; ?>"><?php echo $cv['name']; ?></span>
							</li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				
				<tr>
					<td><label for="epcid">商品子類別：</label></td>
					<td>
						<ul class="relation-list layer2">
						<?php foreach ($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
							<?php if ($pcv['layer'] == 2) : ?>
							<li>
								<input class="relation-item" rel="<?php echo $pcv['node']; ?>" type="checkbox" name="epcid[]" id="epcid_<?php echo $pck; ?>" value="<?php echo $pcv['epcid']; ?>"/>
								<span class="relation-item-name" for="epcid_<?php echo $pck; ?>"><?php echo $pcv['name']; ?></span>
							</li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
			</table>
			
			<table class="form-table">	
				<tr>
					<td><label for="esid">供應商(店家)：</label></td>
					<td><select name="esid">
						<?php foreach($this->tplVar['table']['rt']['exchange_store'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['esid']; ?>" ><?php echo $pv['name']; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				
				<tr>
					<td><label for="prid">活動規則：</label></td>
					<td><select name="prid" id='promote_rule'><option value="" >請選擇</option>
						<?php foreach($this->tplVar['table']['rt']['promote_rule'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['prid']; ?>" ><?php echo $pv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						<div id='promote_rule_item'></div>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="switch" value="Y">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>