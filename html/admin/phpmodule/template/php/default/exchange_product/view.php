<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_epid">商品編號：</span>
					<input type="text" name="search_epid" size="20" value="<?php echo $this->io->input['get']['search_epid']; ?>"/>
				</li>			
				<li class="search-field">
					<span class="label" for="search_name">商品名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_description">商品敘述：</span>
					<input type="text" name="search_description" size="20" value="<?php echo $this->io->input['get']['search_description']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_epcid">母類別：</span>
					<select name="search_epcid"><option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['epcid']; ?>" <?php echo ($this->io->input['get']['search_epcid']==$pcv['epcid']) ? 'selected' : ''; ?> ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_esid">店家：</span>
					<select name="search_esid"><option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['store'] as $sk => $sv) : ?>
						<option value="<?php echo $sv['esid']; ?>" <?php echo ($this->io->input['get']['search_esid']==$sv['esid']) ? 'selected' : ''; ?>><?php echo $sv['name']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_eptype">兌換商品類型：</span>
					<select name="search_eptype">
					<option value="">All</option>
						<option value="0" <?php echo ($this->io->input['get']['search_eptype']=='0') ? 'selected' : ''; ?> >一般商品</option>
						<option value="1" <?php echo ($this->io->input['get']['search_eptype']=='1') ? 'selected' : ''; ?> >生活繳費</option>
						<option value="2" <?php echo ($this->io->input['get']['search_eptype']=='2') ? 'selected' : ''; ?> >咖啡卡</option>
						<option value="3" <?php echo ($this->io->input['get']['search_eptype']=='3') ? 'selected' : ''; ?> >有庫存電子票券</option>
						<option value="4" <?php echo ($this->io->input['get']['search_eptype']=='4') ? 'selected' : ''; ?> >無庫存電子票券</option>
					</select>
				</li>
				<?php /*
				<li class="search-field">
					<span class="label" for="search_ontime">上架時間：</span>
					<input type="text" name="search_ontime" size="20" value="<?php echo $this->io->input['get']['search_ontime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_offtime">下架時間：</span>
					<input type="text" name="search_offtime" size="20" value="<?php echo $this->io->input['get']['search_offtime']; ?>" class="datetime time-stop"/>
				</li>
				*/?>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/form?mod=add&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th></th>
								<th>母類別</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_epid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_epid=desc">商品編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_epid"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_epid=">商品編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_epid=asc">商品編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_name"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_name=desc">品名<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_name"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=">品名<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_name=asc">品名<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>上架時間</th>
								<th>下架時間</th>
								<th>兌換點數</th>
								<th>分潤<br>(商品市價-進貨價)</th>
								<th>狀態</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">更新時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">更新時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">更新時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<?php /*
								<th>店家ID</th>
								<th>活動規則ID</th>
								*/?>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/form?mod=edit&epid=<?php echo $rv["epid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a></td>
								<td class="icon"><a class="icon-edit" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/copy/epid=<?php echo $rv["epid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">Copy</a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/epid=<?php echo $rv["epid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td>
								<td class="column" name="epcinfo"><?php echo $rv['epcinfo']; ?></td>
								<td class="column" name="epid"><?php echo $rv['epid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="ontime"><?php echo $rv['ontime']; ?></td>
								<td class="column" name="offtime"><?php echo $rv['offtime']; ?></td>
								<td class="column" name="point_price"><?php echo $rv['point_price']; ?></td>
								<td class="column" name="profit"><?php echo $rv['profit']; ?></td>
								<td class="column" name="status"><?php echo $rv['status']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
								<?php /*
								<td class="column" name="esid"><?php echo $rv['esid']; ?></td>
								<td class="column" name="prid"><?php echo $rv['prid']; ?></td>
								*/?>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>