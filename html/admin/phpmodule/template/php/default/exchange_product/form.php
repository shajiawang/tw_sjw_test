<?php //print_r($this->tplVar['table']['rt']['product_category_level2']);?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		
		<style type="text/css">
		.message {
			width: 300px;
			background-color: #FFFFFF;
			border: 1px solid #AAAAAA;
			border-radius: 4px;
			display: none;
		}

		.message header {
			padding: 4px 0;
			background-color: #CCCCCC;
			margin: 3px;
			border-radius: 4px;
			position: relative;
		}

		.message header h2 {
			font-weight: bold;
			font-size: 16px;
			margin: 0 12px;
			line-height: 20px;
		}

		.message header .button-close {
			text-decoration: none;
			color: black;
			float: right;
			margin-right: -6px;
		}

		.message .entry {
			padding: 3px 0;
		}

		.message .entry p {
			margin: 0 6px;
			padding: 10px 0;
		}

		.message.center {
		  position: fixed;
		  left: 50%;
		  top: 50%;
		  transform: translate(-50%, -50%);
		  -webkit-transform: translate(-50%, -50%);
		  -moz-transform: translate(-50%, -50%);
		}
		</style>
		
		<script type="text/javascript">
		$(function(){
			$('#msg-close').on('click', function(){
				$('#msg-dialog').hide();
			});
		});
		
		<?php if($this->io->input["get"]['mod']=='add'){ ?>
		//新增表單
		$(function(){
			$('#item_name').change(function() {
				if ($(this).val() == '') {
					$('#msg').text('新增商品错误!');
					$('#msg-dialog').show();
					return;
				} //$('#form-edit').submit();
				
				$.post('<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/process?mod=add',
				{
					name: $(this).val(),
					location_url: '<?php echo $this->tplVar["status"]['get']['location_url'];?>'
				}, function(str_json) {
					if(str_json) {
						var json = $.parseJSON(str_json);
						if (json.status) {
							window.location="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/form?mod=edit&epid="+ json.id +"&location_url=<?php echo $this->tplVar["status"]['get']['location_url'];?>";
						} else {
							$('#msg').text('新增商品错误!');
							$('#msg-dialog').show();
						}
					}
				});
			});
		});
		<?php }//endif; ?>
		
		<?php if($this->io->input["get"]['mod']!='add'){ ?>
		$(function(){
			//異動商品說明
			$('.item_description').change(function() {
				if ($(this).val() == '') {
					$('#msg').text('商品說明不可空白!');
					$('#msg-dialog').show();
					return;
				} alert('1');
				$('#form-edit').submit();
			});
			
			
			//商品促銷項目關聯 
            //var cbxVehicle = new Array();
			//$("#tbl input:checkbox#epcid").each(function(i) { cbxVehicle[i] = this.value;  alert($(this).val()); });
			
			$('#product_promote_result').load('<?php echo $this->config->default_main; ?>/product_promote/ajax_mall/tpl=widget&search_epid=<?php echo $this->tplVar['table']['record'][0]['epid']; ?>&search_epcid=<?php echo $this->tplVar['table']['rt']['product_category_rt'];?>', 
				function(){
					var product_promote = [];
					<?php foreach($this->tplVar['table']['rt']['product_promote'] as $rk => $rv) : ?>
					product_promote.push(<?php echo $rv['ppid']; ?>);
					<?php endforeach; ?>
			});
			
			$("#tbl input:checkbox#epcid").click(function(){                
					//if (this.checked){ alert(this.checked);
					var cbx = new Array();
					$("#tbl input:checkbox#epcid").each(function(i) {
						if(this.checked) { cbx[i] = this.value; }
					});
					//if($("#tbl input:checked#epcid").length == $("#tbl input#epcid").length){ $("#chkAll")[0].checked = true; } //alert($(this).val());
					
					$('#product_promote_result').load('<?php echo $this->config->default_main; ?>/product_promote/ajax_mall/tpl=widget&search_epid=<?php echo $this->tplVar['table']['record'][0]['epid']; ?>&search_epcid='+ cbx, 
					function(){
						var product_promote = [];
						<?php foreach($this->tplVar['table']['rt']['product_promote'] as $rk => $rv) : ?>
						product_promote.push(<?php echo $rv['ppid']; ?>);
						<?php endforeach; ?>
					});
            });
		});
		
		$(function(){
			//活動規則
			$("#promote_rule").change(function(){
				$('#promote_rule_item').load('<?php echo $this->config->default_main; ?>/promote_rule_item/view/tpl=widget&search_prid='+ $("#promote_rule").val(), 
				function(){
					var promote_rule_item_product_rt = [];
					<?php foreach($this->tplVar['table']['rt']['promote_rule_item_product_rt'] as $rk => $rv) : ?>
					promote_rule_item_product_rt.push(<?php echo $rv['priid']; ?>);
					<?php endforeach; ?>

					$('#promote_rule_item .relation-item').each(function(idx, elm){
						if (promote_rule_item_product_rt.indexOf(parseInt($(this).val())) != -1) {
							$(this).prop('checked', true);
						} else {
							$(this).prop('checked', false);
						}
					});					
				});
			}).change();
		});
		<?php }//endif; ?>
		
		$(function(){
			<?php for($i=0; $i<($this->tplVar['table']['rt']['product_category_level']-1); $i++){ ?>
			$('.relation-list.layer<?php echo $i+1;?> .relation-item')
			.on('change', function(){
				var $this = $(this),
					node = $this.val(),
					checked = $this.prop('checked');

				if (checked) {
					$('.relation-list.layer<?php echo $i+2;?> li:has(.relation-item[rel='+node+'])').show();
				} else {
					$('.relation-list.layer<?php echo $i+2;?> li:has(.relation-item[rel='+node+'])').hide()
					.find('.relation-item[rel='+node+']').prop('checked', false);
				}				
			}).change();
			<?php } ?>
		});
			function rebate_p(val) {
				var retail_price = document.getElementById('retail_price').value; 
				console.log(retail_price);
				
				var total = Math.floor((val * retail_price)/100);
				console.log(total);
				$('#rebate').val(total);
			}
		</script>
	</head> 
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/process?mod=edit" enctype="multipart/form-data" >
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<?php if($this->io->input["get"]['mod']!='add'){ ?>
				<tr>
					<td><label for="epid">商品ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['epid']; ?></td>
				</tr>
				<?php }//endif; ?>
				<tr>
					<td><label for="name">商品名稱：</label></td>
					<td><input name="name" type="text" id="item_name" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">商品說明：</label></td>
					<td><textarea name="description" class="ckeditor" id="ckeditor_description"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>

				<tr>
					<td><label for="memo">商品備註：</label></td>
					<td><textarea name="memo" class="description" id="ckeditor_memo"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['memo']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail_file">商品內頁主圖：</label></td>
					<td>
						<input name="thumbnail_file" type="file"/><br>
						<?php if ($this->io->input["get"]['mod']!='add' && !empty($this->tplVar['table']['rt']['product_thumbnail_file'][0]["filename"])){ ?>
						<a href="<?php echo "/site/images/site/product/{$this->tplVar['table']['rt']['product_thumbnail_file'][0]["filename"]}"; ?>" target="_blank">預覽</a> |
						<!--a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_thumbnail_file/?epid=<?php echo $this->tplVar['table']['record'][0]['epid'];?>&epftid=<?php echo $this->tplVar['table']['record'][0]['epftid'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">刪除內頁主圖</a-->
						<?php }//endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail_file2">商品內頁主圖2：</label></td>
					<td>
						<input name="thumbnail_file2" type="file"/><br>
						<?php if ($this->io->input["get"]['mod']!='add' && !empty($this->tplVar['table']['rt']['product_thumbnail_file2'][0]["filename"])){ ?>
						<a href="<?php echo "/site/images/site/product/{$this->tplVar['table']['rt']['product_thumbnail_file2'][0]["filename"]}"; ?>" target="_blank">預覽</a> |
						<a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_thumbnail_file/?epid=<?php echo $this->tplVar['table']['record'][0]['epid'];?>&epftid2=<?php echo $this->tplVar['table']['record'][0]['epftid2'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">刪除內頁主圖2</a>
						<?php }//endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail_file3">商品內頁主圖3：</label></td>
					<td>
						<input name="thumbnail_file3" type="file"/><br>
						<?php if ($this->io->input["get"]['mod']!='add' && !empty($this->tplVar['table']['rt']['product_thumbnail_file3'][0]["filename"])){ ?>
						<a href="<?php echo "/site/images/site/product/{$this->tplVar['table']['rt']['product_thumbnail_file3'][0]["filename"]}"; ?>" target="_blank">預覽</a> |
						<a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_thumbnail_file/?epid=<?php echo $this->tplVar['table']['record'][0]['epid'];?>&epftid3=<?php echo $this->tplVar['table']['record'][0]['epftid3'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">刪除內頁主圖3</a>
						<?php }//endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail_file4">商品內頁主圖4：</label></td>
					<td>
						<input name="thumbnail_file4" type="file"/><br>
						<?php if ($this->io->input["get"]['mod']!='add' && !empty($this->tplVar['table']['rt']['product_thumbnail_file4'][0]["filename"])){ ?>
						<a href="<?php echo "/site/images/site/product/{$this->tplVar['table']['rt']['product_thumbnail_file4'][0]["filename"]}"; ?>" target="_blank">預覽</a> |
						<a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_thumbnail_file/?epid=<?php echo $this->tplVar['table']['record'][0]['epid'];?>&epftid4=<?php echo $this->tplVar['table']['record'][0]['epftid4'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">刪除內頁主圖4</a>
						<?php }//endif; ?>
					</td>
				</tr>				
				<tr>
					<td><label for="seq">商品排序：</label></td>
					<td><input name="seq" type="number" value="<?php if (!empty($this->tplVar['table']['record'][0]['seq'])){ echo $this->tplVar['table']['record'][0]['seq']; }else{ echo 1; } ?>"/></td>
				</tr>
				<tr>
					<td><label for="retail_price">商品市價：</label></td>
					<td><input name="retail_price" id="retail_price" type="number" value='<?php echo $this->tplVar['table']['record'][0]['retail_price']; ?>' data-validate="decimal" class="required"/></td>
				</tr>
				<tr>
					<td><label for="cost_price">進貨成本價：</label></td>
					<td><input name="cost_price" type="number" value='<?php echo $this->tplVar['table']['record'][0]['cost_price']; ?>' data-validate="decimal" class="required"/></td>
				</tr>	
				<tr>
					<td><label for="point_price">兌換所需鯊魚點：</label></td>
					<td><input name="point_price" type="number" value='<?php echo $this->tplVar['table']['record'][0]['point_price']; ?>' data-validate="decimal" class="required" /></td>
				</tr>	
				<tr> 
					<td><label for="rebate">兌換可折抵購物金比例：</label></td>
					<td>
						<select name="rebate_pe" id="rebate_pe" onchange="rebate_p(this.value);">
							<?php
								for ($i=0; $i<=50;$i++){
								$b = "";
								if (floor($this->tplVar['table']['record'][0]['retail_price'] / $this->tplVar['table']['record'][0]['rebate']*100) == $i){ $b="selected"; }
								echo "<option value='{$i}' {$b}>{$i}</option>";
								}
							?>
						</select> %
						<input name="rebate" id="rebate" type="number" value='<?php echo $this->tplVar['table']['record'][0]['rebate']; ?>' data-validate="decimal" class="required" />
					</td>
				</tr>
				<tr>
					<td><label for="eptype">兌換商品類型：</label></td>
					<td>
						<select name="eptype">
							<option value="0" <?php echo $this->tplVar['table']['record'][0]['eptype']=="0" ? "selected" : ""; ?> >一般商品</option>
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['eptype']=="1" ? "selected" : ""; ?> >生活繳費</option>
							<option value="2" <?php echo $this->tplVar['table']['record'][0]['eptype']=="2" ? "selected" : ""; ?> >咖啡卡</option>
							<option value="3" <?php echo $this->tplVar['table']['record'][0]['eptype']=="3" ? "selected" : ""; ?> >有庫存電子票券</option>
							<option value="4" <?php echo $this->tplVar['table']['record'][0]['eptype']=="4" ? "selected" : ""; ?> >無庫存電子票券</option>
						</select>
					</td>
				</tr>	
				<?php if($this->io->input["get"]['mod']!='add'){ ?>
				<tr>
					<td><label for="stock">庫存數量：</label></td>
					<td><input name="stock" type="text" value="<?php echo $this->tplVar['table']['rt']['stock']; ?>" />
						<a href="<?php echo $this->config->default_main; ?>/stock/add/epid=<?php echo $this->tplVar['table']['record'][0]['epid'];?>" target="_blank">新增庫存</a>
					</td>
				</tr>
				<?php }//endif; ?>	
				<tr>
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" type="text" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>" class="datetime time-start" /></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" class="datetime time-stop" /></td>
				</tr>
				<tr>
					<td><label for="use_type">前台使用類型：</label></td>
					<td>
						<select name="use_type">
							<option value="0" <?php echo $this->tplVar['table']['record'][0]['use_type']=="0" ? "selected" : ""; ?> selected>全都使用</option>
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['use_type']=="1" ? "selected" : ""; ?> >只WEB端使用</option>
							<option value="2" <?php echo $this->tplVar['table']['record'][0]['use_type']=="2" ? "selected" : ""; ?> >只APP端使用</option>
							<option value="3" <?php echo $this->tplVar['table']['record'][0]['use_type']=="3" ? "selected" : ""; ?> >全都不使用</option>
						</select>
					</td>
				</tr>				
				<tr>
					<td><label for="mob_type">手機限用類型：</label></td>
					<td>
						<select name="mob_type">
							<option value="all" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="all" ? "selected" : ""; ?> selected>都顯示</option>
							<option value="n" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="n" ? "selected" : ""; ?> >都不顯示</option>
							<option value="weixin" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="weixin" ? "selected" : ""; ?> >只在微信顯示</option>
							<option value="yixin" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="yixin" ? "selected" : ""; ?> >只在易信顯示</option>
							<option value="laiwang" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="laiwang" ? "selected" : ""; ?> >只在来往顯示</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="display">是否顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['display']=="Y" ? "selected" : ""; ?> selected>顯示</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['display']=="N" ? "selected" : ""; ?> >不顯示</option>
						</select>
					</td>
				</tr>				
				
				<?php for($i=0; $i<$this->tplVar['table']['rt']['product_category_level']; $i++){ ?>
				<?php if($i == 0) { ?>
				<tr>
					<td><label for="epcid">商品母類別：</label></td>
					<td>
						<ul id="tbl" class="relation-list layer1">
						<?php foreach($this->tplVar['table']['rt']['product_category'] as $ck => $cv){ ?>
							<?php if($cv['layer'] == 1){ ?>
							<li>
								<?php if(empty($cv['product_cid'])){ ?>
								<input class="relation-item" type="checkbox" name="epcid[]" id="epcid" value="<?php echo $cv['epcid']; ?>"/>
								<?php } else { ?>
								<input class="relation-item" type="checkbox" name="epcid[]" id="epcid" value="<?php echo $cv['epcid']; ?>" checked/>
								<?php }//endif; ?>
								<span class="relation-item-name" for="epcid_<?php echo $ck; ?>"><?php echo $cv['name']; ?></span>
							</li>
							<?php }//endif; ?>
						<?php }//endforeach; ?>
						</ul>
					</td>
				</tr>	

				<tr>
					<td><label for="ppid">商品分類促銷：</label></td>
					<td>
						<div id='product_promote_result'></div>
					</td>
				</tr>				
				<?php }else{ ?>				
				<tr>
					<td><label for="description">商品第<?php echo $i+1; ?>層類別：</label></td>
					<td>
						<ul class="relation-list layer<?php echo $i+1; ?>">
						<?php foreach($this->tplVar['table']['rt']['product_category'] as $pck => $pcv){ ?>
							<?php if($pcv['layer'] == ($i+1)){ ?>
							<li>
								<?php if(empty($pcv['product_cid'])){ ?>
								<input class="relation-item" rel="<?php echo $pcv['node']; ?>" type="checkbox" name="epcid[]" id="epcid_<?php echo $pck; ?>" value="<?php echo $pcv['epcid']; ?>"/>
								<?php } else { ?>
								<input class="relation-item" rel="<?php echo $pcv['node']; ?>" type="checkbox" name="epcid[]" id="epcid_<?php echo $pck; ?>" value="<?php echo $pcv['epcid']; ?>" checked/>
								<?php }//endif; ?>
								<span class="relation-item-name" for="epcid_<?php echo $pck; ?>"><?php echo $pcv['name']; ?></span>
							</li>
							<?php }//endif; ?>
						<?php }//endforeach; ?>
						</ul>
					</td>
				</tr>
				<?php } ?>
				<?php } ?>
				
			</table>
			<table name="div1" id="div1" class="form-table" style="display:none;">

				<tr>
					<td><label for="thumbnail">商品列表主圖：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if ($this->io->input["get"]['mod']!='add' && !empty($this->tplVar['table']['rt']['product_thumbnail'][0]["filename"])){ ?>
						<a href="<?php echo "/site/images/site/product/{$this->tplVar['table']['rt']['product_thumbnail'][0]["filename"]}"; ?>" target="_blank">預覽</a> |
						<a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_thumbnail/?epid=<?php echo $this->tplVar['table']['record'][0]['epid'];?>&eptid=<?php echo $this->tplVar['table']['record'][0]['eptid'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">刪除列表主圖</a>
						<?php }//endif; ?>
					</td>
				</tr>
			
				<tr>
					<td><label for="thumbnail_url">商品主圖(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">外部兌換URL：</label></td>
					<td><input name="tx_url" type="text" value='<?php echo $this->tplVar['table']['record'][0]['tx_url']; ?>' data-validate="decimal" class="required" /></td>
				</tr>

				<tr>
					<td><label for="process_fee">處理費：</label></td>
					<td><input name="process_fee" type="number" value='<?php echo $this->tplVar['table']['record'][0]['process_fee']; ?>' data-validate="decimal" class="required" /></td>
				</tr>

				<tr>
					<td><label for="seq">限制購買量：</label></td>
					<td><input name="limit_qty" type="number" value="<?php echo $this->tplVar['table']['record'][0]['limit_qty']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">每組數量：</label></td>
					<td><input name="set_qty" type="number" value="<?php echo $this->tplVar['table']['record'][0]['set_qty']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="reserved_stock">預留庫存：</label></td>
					<td><input name="reserved_stock" type="text" value="<?php echo $this->tplVar['table']['record'][0]['reserved_stock']; ?>"/></td>
				</tr>				

				<tr>
					<td><label for="feedback_price">分潤金額：</label></td>
					<td><input name="feedback_price" type="number" value="<?php echo $this->tplVar['table']['record'][0]['feedback_price']; ?>" /></td>
				</tr>
				
				<tr>
					<td><label for="esid">供應商(店家)：</label></td>
					<td><select name="esid">
						<?php foreach ($this->tplVar['table']['rt']['exchange_store'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['esid']; ?>" <?php if($this->tplVar['table']['record'][0]['esid']==$pv['esid']){ echo 'selected'; } ?> ><?php echo $pv['name']; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="prid">活動規則：</label></td>
					<td><select name="prid" id='promote_rule'>
						<?php foreach($this->tplVar['table']['rt']['promote_rule'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['prid']; ?>" <?php if($this->tplVar['table']['record'][0]['prid']==$pv['prid']){ echo 'selected'; } ?> ><?php echo $pv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						<div id='promote_rule_item'></div>
					</td>
				</tr>
			</table>
			


			
			<div class="functions">
				<input type="hidden" name="switch" value="Y">
				<input type="hidden" name="location_url" value="<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">
				<input type="hidden" name="epid" value="<?php echo $this->tplVar["status"]["get"]["epid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="button submit"><input type="button" value="打開" class="submit" onclick="if(this.value=='打開') {this.value='關閉';document.getElementById('div1').style.display='block';} else {this.value='打開';document.getElementById('div1').style.display='none';}"></div>

				<div class="clear"></div>
			</div>
		</form>
		
		
		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg" id="msg-title">讯息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="entry">
				<p id="msg">Message content</p>
			</div>
		</article>
	</body>
</html>