<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="acid">選擇主題：</label></td>
					<td>
						<select name="acid">
						<?php foreach($this->tplVar['table']['rt']['ad_category'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['acid']; ?>" <?php if($this->tplVar['table']['record'][0]['acid']==$cv['acid']){ echo 'selected'; } ?> ><?php echo $this->tplVar['status']['ad_status'][$cv['place']]; ?> - <?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>
				<tr>
					<td><label for="name">主題：</label></td>
					<td><input name="name" type="text" maxlength="100" /></td>
				</tr>
				<tr>
					<td><label for="description">內容：</label></td>
					<td><textarea name="description" cols="40" rows="4"></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail">廣告圖：</label></td>
					<td>
						<table width="100%">
						　<tr>
							<td><input name="thumbnail" type="file"/></td>
							<td rowspan="2">*圖片尺寸</br>首頁上方輪播-一般=>2.26:1</br>首頁上方輪播-特殊=>1.79:1 </br>首頁下方/鯊魚商城廣告輪播 =>2.73:1
							<!-- 首頁上方輪播-一般：640px X 202px</br>首頁上方輪播-特殊：750px X 420px</br>首頁下方輪播：328px X 120p  -->
							<!-- 首頁上方輪播-一般：720px X 318px</br>首頁上方輪播-特殊：750px X 420px</br>首頁下方輪播：328px X 120p  -->
							</td>
						　</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail2">APP特殊專用圖：</label></td>
					<td><input name="thumbnail2" type="file"/></td>
				</tr>	
				<!-- <tr>
					<td><label for="thumbnail_popup_a">彈出圖片(android)：</label></td>
					<td><input name="thumbnail_popup_a" type="file"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail_popup_i">彈出圖片(ios)：</label></td>
					<td><input name="thumbnail_popup_i" type="file"/></td>
				</tr>		 -->	
				<tr>
					<td><label for="thumbnail_url">廣告圖(外部圖片)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail_url2">APP特殊專用(外部圖片)：</label></td>
					<td><input name="thumbnail_url2" type="text" size="60"/></td>
				</tr>

			<!-- 	<tr>
					<td><label for="url">連結網址</label></td>
					<td><input id="url" name="url" type="text" /></td>
				</tr>
				<tr>
					<td><label for="url_a">連結網址(Android專用)</label></td>
					<td><input id="url_a" name="url_a" type="text" /></td>
				</tr>
				<tr>
					<td><label for="url_i">連結網址(IOS專用)</label></td>
					<td><input id="url_i" name="url_i" type="text" /></td>
				</tr> -->
				
				<tr>
					<td><label for="action_memo_type">點擊事件：</label></td>
					<td>
						<select name="action_memo_type" id="select_action_memo_type">
							<option value="0">不做事</option>
							<option value="1">開啟web(內開)</option>
							<option value="2">開啟web(外連)</option>
							<option value="3">開大圖</option>
							<option value="4">跳頁面</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="action_memo_url_title">連結標題(新架構)</label></td>
					<td><input id="action_memo_url_title" name="action_memo_url_title" type="text" /></td>
				</tr>
				<tr>
					<td><label for="action_memo_url">連結網址(新架構)</label></td>
					<td><input id="action_memo_url" name="action_memo_url" type="text" /></td>
				</tr>
				<tr>
					<td><label for="action_memo_imgurl">彈出圖片(新架構)</label></td>
					<td><input id="action_memo_imgurl" name="action_memo_imgurl" type="text" /></td>
				</tr>
				<tr>
					<td><label for="action_memo_page">跳轉頁面(新架構)</label></td>
					<td><select name="action_memo_page" id="action_memo_page">
							<option value="1">商品頁</option>
							<option value="2">購買殺價幣</option>
							<option value="3">直播頁</option>
							<option value="4">首頁</option>
							<option value="5">殺價紀錄</option>
							<option value="6">商城兌換清單</option>
							<option value="7">邀請好友</option>
							<option value="8">圓夢商品頁</option>
							<option value="9">殺友專區</option>
							<option value="10">我的帳號</option>
							<option value="11">殺戮戰場首頁</option>
							<option value="12">圓夢商品首頁</option>
							<option value="13">鯊魚商城首頁</option>
							<option value="24">鯊魚商城分類頁</option>
							<option value="25">鯊魚商城商品內頁</option>
							<option value="14">殺價幣</option>
							<option value="15">殺價劵</option>
							<option value="16">票劵/卡片</option>
							<option value="17">新手教學</option>
							<option value="18">最新得標</option>
							<option value="19">王者秘笈</option>
							<option value="20">掃瞄支付頁(使用者掃店家)</option>
							<option value="21">鯊魚點使用明細</option>
							<option value="22">超級殺價劵</option>
							<option value="23">關於殺價王</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="action_memo_productid">跳轉商品編號(新架構)</label></td>
					<td><input id="action_memo_productid" name="action_memo_productid" type="number" value="0"/></td>
				</tr>
				<tr>
					<td><label for="action_memo_epcid">跳轉商城分類編號</label></td>
					<td><input id="action_memo_epcid" name="action_memo_epcid" type="number" value="0"/></td>
				</tr>
				<tr>
					<td><label for="action_memo_layer">跳轉商城分類層級</label></td>
					<td><input id="action_memo_layer" name="action_memo_layer" type="number" value="0"/></td>
				</tr>
				<tr>
					<td><label for="action_memo_epid">跳轉商城商品編號</label></td>
					<td><input id="action_memo_epid" name="action_memo_epid" type="number" value="0"/></td>
				</tr>
				<!--tr>
					<td><label for="promotetype">廣告播放方式：</label></td>
					<td>
						<select name="promotetype">
							<option value="P" selected >圖片</option>
							<option value="M" >跑馬燈</option>
						</select>
					</td>
				</tr-->
				<tr>
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo date("Y-m-d H:i"); ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo date("Y-m-d H:i", strtotime("+1 year")); ?>" data-org="<?php echo date("Y-m-d H:i", strtotime("+1 year")); ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="1"/></td>
				</tr>
				<tr>
					<td><label for="used">廣告顯示：</label></td>
					<td>
						<select name="used">
							<option value="Y" selected>啓用</option>
							<option value="N" >關閉</option>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>

<script type="text/javascript">

</script>