<?php
$action_memo = $this->tplVar['table']['record'][0]['action_memo'];
$action_memo = json_decode($action_memo,true);
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="adid">廣告編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['adid']; ?></td>
				</tr>
				<tr>
					<td><label for="acid">選擇主題：</label></td>
					<td>
						<select name="acid">
						<?php foreach($this->tplVar['table']['rt']['ad_category'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['acid']; ?>" <?php if($this->tplVar['table']['record'][0]['acid']==$cv['acid']){ echo 'selected'; } ?> ><?php echo $this->tplVar['status']['ad_status'][$cv['place']]; ?> - <?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>				
				<tr>
					<td><label for="name">主題：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>" maxlength="100" /></td>
				</tr>
				<tr>
					<td><label for="description">內容：</label></td>
					<td><textarea name="description" cols="40" rows="4"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="thumbnail">廣告圖：</label></td>
					<td>
						<table width="100%">
						　<tr>
						　	<td><input name="thumbnail" type="file"/><br>
								<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
								<!-- a href="<?php echo $this->config->img_domain_name .'/site/images/site/ad/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank" -->
								<a href="<?php echo 'https://s3.hicloud.net.tw/img.saja.com.tw/site/images/site/ad/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">
								預覽</a>
								<?php endif; ?>
							</td>
						　	<td rowspan="2">*圖片尺寸</br>首頁上方輪播-一般=>2.26:1</br>首頁上方輪播-特殊=>1.8:1 </br>首頁下方/鯊魚商城廣告輪播 =>2.73:1
						   <!-- </br>首頁上方輪播-一般：640px X 202px</br>首頁上方輪播-特殊：750px X 420px</br>首頁下方輪播：328px X 120p</td> -->
						　</tr>
						</table>					
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail2">APP特殊專用圖：</label></td>
					<td>
						<input name="thumbnail2" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail2'])) : ?>
						<!-- a href="<?php echo $this->config->img_domain_name .'/site/images/site/ad/'. $this->tplVar['table']['record'][0]['thumbnail2']; ?>" target="_blank">預覽</a -->
						<a href="<?php echo 'https://s3.hicloud.net.tw/img.saja.com.tw/site/images/site/ad/'. $this->tplVar['table']['record'][0]['thumbnail2']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				<!-- </tr>
					<td><label for="thumbnail_popup_a">彈出圖片(andorid)：</label></td>
					<td>
						<input name="thumbnail_popup_a" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail_popup_a'])) : ?>
						<a href="<?php echo 'https://s3.hicloud.net.tw/img.saja.com.tw/site/images/site/ad/'.$this->tplVar['table']['record'][0]['thumbnail_popup_a']; ?>" target="_blank">預覽</a> 
						<?php endif; ?>
					</td>
				</tr>
				</tr>
					<td><label for="thumbnail_popup_i">彈出圖片(ios)：</label></td>
					<td>
						<input name="thumbnail_popup_i" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail_popup_i'])) : ?>
						<a href="<?php echo $this->config->img_domain_name .'/site/images/site/ad/'. $this->tplVar['table']['record'][0]['thumbnail_popup_i']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>	 -->			
				<tr>
					<td><label for="thumbnail_url">廣告圖(外部圖片)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail_url2">APP特殊專用(外部圖片)：</label></td>
					<td><input name="thumbnail_url2" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url2']; ?>"/></td>
				</tr>				
			<!-- 	<tr>
					<td><label for="url">連結網址</label></td>
					<td><input name="url" type="text" value="<?php echo $this->tplVar['table']['record'][0]['qrcode_url']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="url_a">連結網址(Android專用)</label></td>
					<td><input name="url_a" type="text" value="<?php echo $this->tplVar['table']['record'][0]['action_a']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="url_i">連結網址(IOS專用)</label></td>
					<td><input name="url_i" type="text"  value="<?php echo $this->tplVar['table']['record'][0]['action_i']; ?>"/></td>
				</tr>		
 -->
				<tr>
					<td><label for="action_memo_type">點擊事件：</label></td>
					<td>
						<select name="action_memo_type" id="select_action_memo_type">
							<option value="0" <?php echo $action_memo['type']=="0"?" selected":""; ?>>不做事</option>
							<option value="1" <?php echo $action_memo['type']=="1"?" selected":""; ?>>開啟web(內開)</option>
							<option value="2" <?php echo $action_memo['type']=="2"?" selected":""; ?>>開啟web(外連)</option>
							<option value="3" <?php echo $action_memo['type']=="3"?" selected":""; ?>>開大圖</option>
							<option value="4" <?php echo $action_memo['type']=="4"?" selected":""; ?>>跳頁面</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="action_memo_url_title">連結標題(新架構)</label></td>
					<td><input id="action_memo_url_title" name="action_memo_url_title" type="text" value="<?php echo urldecode($action_memo['url_title']);?>" /></td>
				</tr>
				<tr>
					<td><label for="action_memo_url">連結網址(新架構)</label></td>
					<td><input id="action_memo_url" name="action_memo_url" type="text" value="<?php echo $action_memo['url']?>" /></td>
				</tr>
				<tr>
					<td><label for="action_memo_imgurl">彈出圖片(新架構)</label></td>
					<td><input id="action_memo_imgurl" name="action_memo_imgurl" type="text" value="<?php echo $action_memo['imgurl']?>" /></td>
				</tr>
				<tr>
					<td><label for="action_memo_page">跳轉頁面(新架構)</label></td>
					<td><select name="action_memo_page" id="action_memo_page">
							<option value="1" <?php echo $action_memo['page']=="1"?" selected":""; ?> >商品頁</option>
							<option value="2" <?php echo $action_memo['page']=="2"?" selected":""; ?> >購買殺價幣</option>
							<option value="3" <?php echo $action_memo['page']=="3"?" selected":""; ?> >直播頁</option>
							<option value="4" <?php echo $action_memo['page']=="4"?" selected":""; ?> >首頁</option>
							<option value="5" <?php echo $action_memo['page']=="5"?" selected":""; ?> >殺價紀錄</option>
							<option value="6" <?php echo $action_memo['page']=="6"?" selected":""; ?> >商城兌換清單</option>
							<option value="7" <?php echo $action_memo['page']=="7"?" selected":""; ?> >邀請好友</option>
							<option value="8" <?php echo $action_memo['page']=="8"?" selected":""; ?> >圓夢商品頁</option>
							<option value="9" <?php echo $action_memo['page']=="9"?" selected":""; ?> >殺友專區</option>
							<option value="10" <?php echo $action_memo['page']=="10"?" selected":""; ?> >我的帳號</option>
							<option value="11" <?php echo $action_memo['page']=="11"?" selected":""; ?> >殺戮戰場首頁</option>
							<option value="12" <?php echo $action_memo['page']=="12"?" selected":""; ?> >圓夢商品首頁</option>
							<option value="13" <?php echo $action_memo['page']=="13"?" selected":""; ?> >鯊魚商城首頁</option>
							<option value="24" <?php echo $action_memo['page']=="24"?" selected":""; ?> >鯊魚商城分類頁</option>
							<option value="25" <?php echo $action_memo['page']=="25"?" selected":""; ?> >鯊魚商城商品內頁</option>
							<option value="14" <?php echo $action_memo['page']=="14"?" selected":""; ?> >殺價幣</option>
							<option value="15" <?php echo $action_memo['page']=="15"?" selected":""; ?> >殺價劵</option>
							<option value="16" <?php echo $action_memo['page']=="16"?" selected":""; ?> >票劵/卡片</option>	
							<option value="17" <?php echo $action_memo['page']=="17"?" selected":""; ?> >新手教學</option>
							<option value="18" <?php echo $action_memo['page']=="18"?" selected":""; ?> >最新得標</option>
							<option value="19" <?php echo $action_memo['page']=="19"?" selected":""; ?> >王者秘笈</option>
							<option value="20" <?php echo $action_memo['page']=="20"?" selected":""; ?> >掃瞄支付頁(使用者掃店家)</option>
							<option value="21" <?php echo $action_memo['page']=="21"?" selected":""; ?> >鯊魚點使用明細</option>
							<option value="22" <?php echo $action_memo['page']=="22"?" selected":""; ?> >超級殺價劵</option>
							<option value="23" <?php echo $action_memo['page']=="23"?" selected":""; ?> >關於殺價王</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="action_memo_productid">跳轉商品編號(新架構)</label></td>
					<td><input id="action_memo_productid" name="action_memo_productid" type="number" value="<?php echo $action_memo['productid']?>" /></td>
				</tr>
				<tr>
					<td><label for="action_memo_epcid">商城分類編號</label></td>
					<td><input id="action_memo_epcid" name="action_memo_epcid" type="number" value="<?php echo $action_memo['epcid']?>"/></td>
				</tr>
				<tr>
					<td><label for="action_memo_layer">商城分類層級</label></td>
					<td><input id="action_memo_layer" name="action_memo_layer" type="number" value="<?php echo $action_memo['layer']?>"/></td>
				</tr>
				<tr>
					<td><label for="action_memo_epid">商城商品編號</label></td>
					<td><input id="action_memo_epid" name="action_memo_epid" type="number" value="<?php echo $action_memo['epid']?>"/></td>
				</tr>
				<tr>
					<td><label for="action_memo_acttype">開大圖 Action Type</label></td>
					<td><input id="action_memo_acttype" name="action_memo_acttype" type="text" value="<?php echo $action_memo['action_type']['act_type']?>"/></td>
				</tr>
				<!--tr>
					<td><label for="promotetype">廣告播放方式：</label></td>
					<td>
						<select name="promotetype">
							<option value="P" <?php echo $this->tplVar['table']['record'][0]['promotetype']=="P"?" selected":""; ?> >圖片</option>
							<option value="M" <?php echo $this->tplVar['table']['record'][0]['promotetype']=="M"?" selected":""; ?> >跑馬燈</option>
						</select>
					</td>
				</tr-->
				<tr>
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" data-org="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="used">顯示：</label></td>
					<td>
						<select name="used">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['used']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['used']=="N"?" selected":""; ?>>關閉</option>
						</select>
					</td>
				</tr>	
			</table>
			<!--table class="form-table">
				<tr>
					<td><label for="acid">分類：</label></td>
					<td>
						<select name="acid">
						<?php foreach($this->tplVar['table']['rt']['ad_category'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['acid']; ?>" <?php if($this->tplVar['table']['record'][0]['acid']==$cv['acid']){ echo 'selected'; } ?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>
			</table-->
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="adid" value="<?php echo $this->tplVar["status"]["get"]["adid"] ;?>">
				<input type="hidden" name="oacid" value="<?php echo $this->tplVar['table']['record'][0]["acid"] ;?>">
				<input type="hidden" name="oldthumbnail" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail'];?>">
				<input type="hidden" name="oldthumbnail2" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail2'];?>">
				<input type="hidden" name="oldthumbnail_popup_a" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_popup_a'];?>">
				<input type="hidden" name="oldthumbnail_popup_i" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_popup_i'];?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>