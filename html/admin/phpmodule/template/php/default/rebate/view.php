<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_behav">購物金行為：</span>
					<select name="search_behav">
						<option value="" <?php echo ($this->io->input['get']['search_behav']=='') ? 'selected' : ''; ?>>ALL</option>
						<option value="order_refund" <?php echo ($this->io->input['get']['search_behav']=='order_refund') ? 'selected' : ''; ?>>訂單退費</option>
						<option value="bid_close" <?php echo ($this->io->input['get']['search_behav']=='bid_close') ? 'selected' : ''; ?>>結標轉入</option>
						<option value="user_exchange" <?php echo ($this->io->input['get']['search_behav']=='user_exchange') ? 'selected' : ''; ?>>兌換折抵</option>
						<option value="user_deposit" <?php echo ($this->io->input['get']['search_behav']=='user_deposit') ? 'selected' : ''; ?>>使用者儲值</option>
						<option value="gift" <?php echo ($this->io->input['get']['search_behav']=='gift') ? 'selected' : ''; ?>>轉贈別人</option>
						<option value="others" <?php echo ($this->io->input['get']['search_behav']=='others') ? 'selected' : ''; ?>>其它</option>
						<option value="system" <?php echo ($this->io->input['get']['search_behav']=='system') ? 'selected' : ''; ?>>系統轉入</option>
						<option value="system_test" <?php echo ($this->io->input['get']['search_behav']=='system_test') ? 'selected' : ''; ?>>系統測試</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_orderid">訂單編號：</span>
					<input type="text" name="search_orderid" size="20" value="<?php echo $this->io->input['get']['search_orderid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_productid">商品編號：</span>
					<input type="text" name="search_uproductid" size="20" value="<?php echo $this->io->input['get']['search_productid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">新增日期：</span>
					<input name="search_btime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>"/> ~ 
					<input name="search_etime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" />
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th></th>
								<th></th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_reid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_reid=desc">購物金帳目編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_reid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_reid=">購物金帳目編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_reid=asc">購物金帳目編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>會員編號</th>
								<th>購物金數量</th>
								<th>購物金行為</th>
								<th>轉贈會員編號</th>
								<th>訂單編號</th>
								<th>商品編號</th>
								<th>活動備註</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/reid=<?php echo $rv["reid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete/reid=<?php echo $rv["reid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="reid"><?php echo $rv['reid']; ?></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="amount" style="text-align:right;"><?php echo number_format(round($rv['amount'])); ?></td>
								<td class="column" name="behav"><?php echo $this->tplVar['status']['rebate_status'][$rv['behav']]; ?></td>
								<td class="column" name="touid"><?php echo $rv['touid']; ?></td>
								<td class="column" name="orderid" ><?php if($rv['orderid'] != 0){ ?><a href="<?php echo $this->config->default_main; ?>/order/form/edit/orderid=<?php echo $rv['orderid']; ?>" target="_blank"><?php echo $rv['orderid']; ?></a><?php }else{ ?>無關聯訂單<?php } ?></td>
								<td class="column" name="productid"><?php if($rv['productid'] != 0){ ?><a href="<?php echo $this->config->default_main; ?>/exchange_product/form/?mod=edit&epid=<?php echo $rv['productid']; ?>" target="_blank"><?php echo $rv['productid']; ?></a><?php }else{ ?>無關聯商品<?php } ?></td>
								<td class="column" name="remark" >殺房活動</td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>