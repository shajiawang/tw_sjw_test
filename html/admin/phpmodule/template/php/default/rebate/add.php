<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>

		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><input name="userid" type="text" value='0' /></td>
				</tr>
				<tr>
					<td><label for="amount">購物金數量：</label></td>
					<td><input name="amount" type="text" value="0"/></td>
				</tr>
				<tr>
					<td><label for="behav">購物金行為：</label></td>
					<td>
						<select name="behav">
							<option value="order_refund" >訂單退費</option>
							<option value="bid_close" >結標轉入</option>
							<option value="user_exchange">兌換折抵</option>
							<option value="user_deposit">使用者儲值</option>
							<option value="gift">轉贈別人</option>
							<option value="others">其它</option>
							<option value="system">系統轉入</option>
							<option value="system_test">系統測試</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="orderid">訂單編號：</label></td>
					<td><input name="orderid" type="text" value="0"/></td>
				</tr>
				<tr>
					<td><label for="productid">商品編號：</label></td>
					<td><input name="productid" type="text" value="0"/></td>
				</tr>
				<tr>
					<td><label for="remark">活動備註：</label></td>
					<td>
						<select name="remark">
							<option value="1" >殺房活動</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="touid">轉贈會員編號：</label></td>
					<td><input name="touid" type="text" value="0"/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="memo">補充說明：</label></td>
					<td><textarea name="memo" id="memo" style="width:20rem;"></textarea></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>