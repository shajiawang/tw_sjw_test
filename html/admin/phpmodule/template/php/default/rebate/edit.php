<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="reid">購物金帳目編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['reid']; ?></td>
				</tr>
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['userid']; ?></td>
				</tr>
				<tr>
					<td><label for="amount">購物金數量：</label></td>
					<td><input name="amount" type="text" value="<?php echo $this->tplVar['table']['record'][0]['amount']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="behav">購物金行為：</label></td>
					<td><input name="behav" type="text" value="<?php echo $this->tplVar['table']['record'][0]['behav']; ?>"/>
						<select name="behav">
							<option value="order_refund" <?php echo ($this->tplVar['table']['record'][0]['behav']=='order_refund') ? 'selected' : ''; ?>>訂單退費</option>
							<option value="bid_close" <?php echo ($this->tplVar['table']['record'][0]['behav']=='bid_close') ? 'selected' : ''; ?>>結標轉入</option>
							<option value="user_exchange" <?php echo ($this->tplVar['table']['record'][0]['behav']=='user_exchange') ? 'selected' : ''; ?>>兌換折抵</option>
							<option value="user_deposit" <?php echo ($this->tplVar['table']['record'][0]['behav']=='user_deposit') ? 'selected' : ''; ?>>使用者儲值</option>
							<option value="gift" <?php echo ($this->tplVar['table']['record'][0]['behav']=='gift') ? 'selected' : ''; ?>>轉贈別人</option>
							<option value="others" <?php echo ($this->tplVar['table']['record'][0]['behav']=='others') ? 'selected' : ''; ?>>其它</option>
							<option value="system" <?php echo ($this->tplVar['table']['record'][0]['behav']=='system') ? 'selected' : ''; ?>>系統轉入</option>
							<option value="system_test" <?php echo ($this->tplVar['table']['record'][0]['behav']=='system_test') ? 'selected' : ''; ?>>系統測試</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="orderid">訂單編號：</label></td>
					<td><input name="orderid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['orderid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="productid">商品編號：</label></td>
					<td><input name="productid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['productid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="remark">活動備註：</label></td>
					<td>
						<select name="remark">
							<option value="1" <?php echo ($this->tplVar['table']['record'][0]['remark']=='1') ? 'selected' : ''; ?>>殺房活動</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="touid">轉贈會員編號：</label></td>
					<td><input name="touid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['touid']; ?>"/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="memo">補充說明：</label></td>
					<td><textarea name="memo" id="memo" style="width:20rem;"><?php echo $this->tplVar['table']['record'][0]['memo']; ?></textarea></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="reid" value="<?php echo $this->tplVar["status"]["get"]["reid"]; ?>">
				<input type="hidden" name="userid" value="<?php echo $this->tplVar['table']['record'][0]['userid']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>