<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>會員統計</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>	
				<li class="search-field">
					<span class="label">暱稱：</span>
					<input type="text" name="search_nickname" size="20" value="<?php echo $this->io->input['get']['search_nickname']; ?>"/>
				</li>				
				<li class="search-field">
					<span class="label" for="search_joindate">會員加入日期：</span>
					<input name="joindatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindatefrom']; ?>" /> ~ 
					<input name="joindateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindateto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_registerfrom">加入方式：</span>
					<select name="registerfrom" id="registerfrom"/>
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['registerfrom'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['registerfrom'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_gender">性別：</span>
					<select name="gender" id="gender"/>
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['gender'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['gender'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_deposit">是否儲值：</span>
					<select name="deposit" id="deposit"/>
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['deposit'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['deposit'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_bid">是否下標：</span>
					<select name="bid" id="bid"/>
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['bid'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['bid'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_exchange">是否兌換：</span>
					<select name="exchange" id="exchange"/>
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['exchange'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['exchange'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>會員帳號</th>
								<th>暱稱</th>
								<th>加入日期</th>
								<th>最近儲值日</th>
								<th>最近下標日</th>
								<th>下標次數</th>
								<th>儲值次數</th>
								<th>兌換次數</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="nickname"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/userview/?userid=<?php echo $rv['userid']; ?>" ><?php echo urldecode($rv['nickname']); ?></a></td>
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="deposit_date"><?php echo $rv['deposit_date']; ?></td>
								<td class="column" name="history_date"><?php echo $rv['history_date']; ?></td>
								<td class="column" name="history_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/historylist/?userid=<?php echo $rv['userid']; ?>" ><?php echo $rv['history_total']; ?></a></td>
								<td class="column" name="deposit_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/depositlist/?userid=<?php echo $rv['userid']; ?>" ><?php echo $rv['deposit_total']; ?></a></td>
								<td class="column" name="exchange_total"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/exchangelist/?userid=<?php echo $rv['userid']; ?>" ><?php echo $rv['exchange_total']; ?></a></td>							
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>