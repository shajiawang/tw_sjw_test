<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_uid">會員編號：</span>
					<input type="text" name="search_uid" size="20" value="<?php echo $this->io->input['get']['search_uid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">暱稱：</span>
					<input type="text" name="search_nickname" size="20" value="<?php echo $this->io->input['get']['search_nickname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label">資料狀態：</span>
					<select name="search_switch">
						<option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_switch']=='N') ? 'selected' : ''; ?> >停權中</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_switch']=='Y') ? 'selected' : ''; ?> >使用中</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>內容</th>
								<th>會員編號</th>
								<th>登入帳號</th>
								<th>暱稱</th>
								<th>身份證字號</th>
								<th>資料狀態</th>
								<th>總兌換點數</th>
								<th>總兌換筆數</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="/admin/ibon_record/?search_uid=<?php echo $rv['userid']; ?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="userid"><?php echo $rv['userid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="nickname"><?php echo $rv['nickname']; ?></td>
								<td class="column" name="idnum"><?php echo $rv['idnum']; ?></td>
								<td class="column" name="switch"><?php if ($rv['switch'] == 'Y') { echo '使用中'; }else{ echo '停權中'; } ?></td>
								<td class="column" name="TotalAmount"><?php if (!empty($rv['TotalAmount'])) { echo $rv['TotalAmount']; }else{ echo '0'; } ?></td>
								<td class="column" name="TotalCount"><?php echo $rv['TotalCount']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>