<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">會員編號：</label></td>
					<td><input name="userid" id="userid" type="text" maxlength="100" /></td>
				</tr>			
				<tr>
					<td><label for="productid">商品資料：</label></td>
					<td>
						<ul id="ul_pcid" class="relation-list layer1">
						<?php foreach ($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<li ptype="<?php echo $rv['ptype']; ?>">
								<input type="radio" name="productid" id="productid<?php echo $rk; ?>" value="<?php echo $rv['productid']; ?>"/>
								<span class="relation-item-name" for="productid_<?php echo $rv; ?>"><?php echo $rv['name']; ?> - (<?php echo $rv['productid']; ?>)</span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				<tr>
					<td><label for="name">殺價券組數：</label></td>
					<td>
						<select id="snum" name="snum">
							<option value="100">100</option>
							<option value="50">50</option>
							<option value="20">20</option>
							<option value="10">10</option>
							<option value="9">9</option>
							<option value="8">8</option>
							<option value="7">7</option>
							<option value="6">6</option>
							<option value="5">5</option>
							<option value="4">4</option>
							<option value="3">3</option>
							<option value="2">2</option>
							<option value="1" selected >1</option>
						</select>					
					</td>
				</tr>
				<!--tr>
					<td><label for="description">驗證碼：</label></td>
					<td><input type="password" name="passwd" id="passwd" value=""></td>
				</tr-->
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>