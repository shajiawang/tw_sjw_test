<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<style type="text/css">
		.message {
			color: #29736c; width:300px; background-color: #ecf8f7; border: 1px solid #005d54; border-radius: 4px; display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px; background-color: #8dc8c3; border-radius: 4px; position: relative;
		}
		.message header h2 {
			font-weight: bold; font-size: 16px; line-height: 20px; margin: 0 12px;			
		}
		.message header .button-close {
			text-decoration: none; color: #ecf8f7; float: right; margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px; padding: 10px 0;
		}
		.message.center {
			position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		.msg{
			color: #29736c;
		}
		</style>
	</head>
	
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a>推薦送幣查詢</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				
				<li class="search-field">
					<span class="label" for="search_detail">推薦明細：</span>
					<select name="search_detail">
						<option value="N" <?php echo ($this->io->input['get']['search_detail']=='N')?'selected':''; ?> >否</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_detail']=='Y')?'selected':''; ?> >是</option>
					</select>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_introby">推薦人會員ID：</span>
					<input type="text" name="search_introby" size="20" value="<?php echo $this->io->input['get']['search_introby']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_act">停權推薦人：</span>
					<select name="search_act">
						<option value="N" <?php echo ($this->io->input['get']['search_act']=='N')?'selected':''; ?> >--</option>
						<option value="A" <?php echo ($this->io->input['get']['search_act']=='A')?'selected':''; ?> >不限</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_act']=='Y')?'selected':''; ?> >是</option>
					</select>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_btime">開始時間：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">截止時間：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				
				<li class="search-field">
					<span class="label" for="search_userid">新人會員ID：</span>
					<input type="text" name="search_userid" size="20" value="<?php echo $this->io->input['get']['search_userid']; ?>"/>
				</li>
				
				<li class="button">
					<button>搜尋</button>
					<button class="clear">清除</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<?php /*<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add">新增推薦</a>*/?>
						</div>					
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								
								<?php /*
								$base_href = $this->tplVar["status"]["path"].$this->tplVar["status"]["search_path"].$this->tplVar["status"]["sort_path"].$this->tplVar["status"]["p"]; 
								*/?>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_intro_by"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_intro_by=desc">推薦人<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_intro_by"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_intro_by=">推薦人<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_intro_by=asc">推薦人<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>推薦人手機</th>
								
								<?php if($this->io->input["get"]["search_detail"] == 'Y'){ ?>
								<th>推薦人贈點</th>
								<th>新會員</th>
								<th>新人贈點</th>
								<th>新人手機(驗證)</th>
								
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $base_href; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>									
									<a href="<?php echo $base_href ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $base_href ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<?php } else { ?>
								<th>推薦人數</th>
								<?php } ?>
								
							</tr>
						</thead>
						
						<tbody>
							<?php if (is_array($this->tplVar['table']['record'])) : ?>
							<?php foreach($this->tplVar['table']['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="intro_by"><?php echo $rv['intro_by'];?> 
									<?php if($rv['switch']=='N'){ ?> (<span style="color:#ff0000">停權</span>) <?php } ?>
									<br><?php echo $rv['nickname']; ?>
								</td>
								
								<td class="column" name="phone"><?php echo $rv['phone']; ?></td>
								
								<?php if($this->io->input['get']["search_detail"] == 'Y'){ ?>
								
								<td class="column" name="intro_spoint">
									<?php if($rv['intro_spoint']){ ?>
									<span style="color:#ff0000"><?php echo ceil($rv['intro_spoint']); ?></span>
									( <?php echo $rv['intro_insertt']; ?> )
									<?php } else { ?>
									
									<?php /*if($rv['switch']=='Y' && ($rv['insertt']>'2019-11-22 10:00:00') && (!empty($rv['verified']) && $rv['verified']=='Y') ){ ?>
									<input class="table_td_button" type="button" value="補送" onclick="to_spoint('<?php echo $rv['userid']; ?>','<?php echo $rv['intro_by'];?>')">
									<?php }*/ ?>
									
									<?php } ?>
								</td>
								
								<td class="column" name="userid">
									<?php echo $rv['userid']; ?><br><?php echo $rv['username']; ?>
								</td>
								
								<td class="column" name="user_spoint">
									<?php if($rv['user_spoint']){ ?>
									<span style="color:#ff0000"><?php echo ceil($rv['user_spoint']); ?></span>
									( <?php echo $rv['user_insertt']; ?> )
									<?php } ?>
								</td>
								
								<td class="column" name="verified">
								<?php echo !empty($rv['phone2'])?$rv['phone2']:$rv['phone3']; ?> (
								<?php if(!empty($rv['verified']) && $rv['verified']=='Y'){ ?>已驗證
								<?php }else{ ?>No<?php } ?> )
								</td>
								
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<?php } else { ?>
								<td class="column" name="cnt"><?php echo $rv['cnt']; ?></td>
								<?php } ?>
								
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		
		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg" id="msg-title">訊息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="" >
				<p id="msg">Message content</p>
			</div>
		</article>
		
	</body>
</html>

<script type="text/javascript">

//補推薦送幣
function to_spoint(userid, intro_by){
	$.post('/admin/user_referral/insert',
		{
			type: 'to_spoint', 
			userid: userid, 
			intro_by: intro_by
		},
		function(str_json){
			if (str_json) {
				var json = $.parseJSON(str_json);
				//console && console.log($.parseJSON(str_json));
				
				if(json.retCode==1){
					//alert(json.retMsg);
					location.reload();
				}else{
					alert(json.retMsg);
				}
			}
		}
	);
};

$(function(){
	//訊息窗
	$('#msg-close').on('click', function(){
		$('#msg-dialog').hide();
	});
});		

</script>