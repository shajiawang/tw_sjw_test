<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="ppid">規則ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['ppid']; ?></td>
				</tr>
				<tr>
					<td><label for="name">項目名稱：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="description">項目敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
			</table>
			
			<?php /*<table class="form-table">
				<tr>
					<td><label for="prid">活動規則ID：</label></td>
					<td><select name="prid">
						<?php foreach ($this->tplVar['table']['rt']['promote_rule'] as $pk => $pv) : ?>
							<option value="<?php echo $pv['prid'];?>" <?php if($this->tplVar['table']['record'][0]['prid']==$pv['prid']){ echo "selected"; }?> ><?php echo $pv['name'];?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
			</table>*/?>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="ppid" value="<?php echo $this->tplVar["status"]["get"]["ppid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>