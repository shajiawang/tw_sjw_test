<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_name"><?php echo $this->tplVar["page_name"]; ?>：</span>
				</li>
				<?php /*
				<li class="search-field">
					<span class="label" for="search_channelid">頻道：</span>
					<select name="search_channelid">
						<?php foreach ($this->tplVar['table']['rt']['channel_rt'] as $ck => $cv) : ?>
						<option value="<?php echo $cv['channelid']; ?>" <?php echo ($this->io->input['get']['search_channelid']==$cv['channelid']) ? "selected" : "";?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">活動名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>*/?>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<?php /*<th>活動名稱</th>*/?>
								<th>競標商品</th>
								<th>會員帳號</th>
								<th>取得時間</th>
								<th>贈送組數</th>
								<th>使用狀態</th>
							</tr>
						</thead>
						<tbody>
							<?php if(is_array($this->tplVar["table"]['record'])){ ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv){
							if($this->io->input["get"]["t"]=='h'){
								$t = $rv['insertt'];
							} else {
								$t = $rv['modifyt'];
							}
							?>
							<tr>
								<?php /*<td class="column" name="spname"><?php echo $rv['spname']; ?></td>*/?>
								<td class="column" name="pname"><?php echo $rv['productid'] .' - '. $rv['pname']; ?></td>
								<td class="column" name="u1name"><?php echo $rv['u1name']; ?></td>
								<td class="column" name="insertt"><?php echo $t; ?></td>
								<?php /*<td class="column" name="promote_amount"><?php echo $rv['promote_amount']; ?></td>*/?>
								<td class="column" name="amount"><?php echo $rv['amount']; ?></td>
								<td class="column" name="used">
								<?php if($this->io->input["get"]["t"]=='g'){ ?>
									<?php if($rv['verified']=='N'){ ?>
									未激活, 
									<?php }else{ ?>
									已激活, 
									<?php } ?>
								<?php } ?>
									
									<?php if($rv['used']=='N'){ ?>
									未使用
									<?php }else{ ?>
									已使用
									<?php } ?>
								</td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>
					
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>