<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label"><?php echo $this->tplVar["page_name"]; ?>：新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">活動名稱：</label></td>
					<td><input name="name" type="text" required/></td>
				</tr>
				<tr>
					<td><label for="productid">競標商品編號：</label></td>
					<td><select name="productid" id="productid">
						<?php foreach ($this->tplVar["table"]["rt"]["valid_product"] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['productid']; ?>" ><?php echo $pcv['productid'] .' -- '. $pcv['name'] ; ?></option>
						<?php endforeach; ?>
					</select></td>
				</tr>
				
				<?php if($this->tplVar["status"]['get']['t']=='h'){ ?>
				<tr>
					<td><label for="onum">限定S碼發送組數：</label></td>
					<td><input name="onum" type="number" value="1" min=0/></td>
				</tr>
				<?php } ?>
				
				<tr>
					<td><label for="description">活動敘述：</label></td>
					<td><input name="description" type="text"/></td>
				</tr>
				
				<?php /*
				<tr>
					<td><label for="ontime">活動開始：</label></td>
					<td><input name="ontime" type="text" class="datetime time-start"/></td>
				</tr>
				<tr>
					<td><label for="offtime">活動結束：</label></td>
					<td><input name="offtime" type="text" class="datetime time-stop"/></td>
				</tr>*/?>
				
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="0" min="0"/></td>
				</tr>
				
			</table>
			
			<div class="functions">
				<input type="hidden" name="behav" value="<?php echo $this->tplVar["status"]['get']['t']; ?>">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>