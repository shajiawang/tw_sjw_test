<!DOCTYPE html>
<?php
    $arrDesc=array();
	$arrDesc['limit']=array();
	$arrDesc['limit']["7"]="新手限定";
	$arrDesc['limit']["13"]="宅公益";
?>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<style type="text/css">
		.message {
			color: #29736c; width:300px; background-color: #ecf8f7; border: 1px solid #005d54; border-radius: 4px; display: none;
		}
		.message header {
			padding: 4px 0; margin: 3px; background-color: #8dc8c3; border-radius: 4px; position: relative;
		}
		.message header h2 {
			font-weight: bold; font-size: 16px; line-height: 20px; margin: 0 12px;
		}
		.message header .button-close {
			text-decoration: none; color: #ecf8f7; float: right; margin-right: -6px;
		}
		.message .entry {
			padding: 3px 0;
		}
		.message .entry p {
			margin: 0 6px; padding: 10px 0;
		}
		.message.center {
			position: fixed; left: 50%; top: 50%; transform: translate(-50%, -50%); -webkit-transform: translate(-50%, -50%);		  -moz-transform: translate(-50%, -50%);
		}
		.msg{
			color: #29736c;
		}
		</style>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">競標商品管理</a>>>
			<a>循環商品資料管理</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_productid">商品編號：</span>
					<input type="text" name="search_productid" size="20" value="<?php echo $this->io->input['get']['search_productid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_name">商品名稱：</span>
					<input type="text" name="search_name" size="20" value="<?php echo $this->io->input['get']['search_name']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_ontime">商品上架日期：</span>
					<input name="search_ontime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['search_ontime']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_offtime">商品結標日期：</span>
					<input name="search_offtime" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['search_offtime']; ?>" />
				</li>
				<li class="search-field">
					<span class="label" for="search_lb">直播商品：</span>
					<select name="search_lb">
						<option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_lb']=='N')?'selected':''; ?> >否</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_lb']=='Y')?'selected':''; ?> >是</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_flash">活動類別：</span>
					<select name="search_flash">
						<option value="">All</option>
						<option value="N" <?php echo ($this->io->input['get']['search_flash']=='N')?'selected':''; ?> >NO</option>
						<option value="Y" <?php echo ($this->io->input['get']['search_flash']=='Y')?'selected':''; ?> >閃殺</option>
						<option value="S" <?php echo ($this->io->input['get']['search_flash']=='S')?'selected':''; ?> >S碼</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_pcid">商品分類：</span>
					<select name="search_pcid">
						<option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['pcid']; ?>" <?php if ($this->tplVar['status']["search"]["search_pcid"] == $pcv['pcid']) {echo "selected";} ?> ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_storeid">商店：</span>
					<select name="search_storeid">
						<option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['store'] as $sk => $sv) : ?>
						<option value="<?php echo $sv['storeid']; ?>"><?php echo $sv['name']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_status">狀態：</span>
					<select name="search_status">
						<option value="">All</option>
						<option value="no" <?php if ($this->tplVar['status']["search"]["search_status"] == 'no') {echo "selected";} ?>>未上架</option>
						<option value="ontime" <?php if ($this->tplVar['status']["search"]["search_status"] == 'ontime') {echo "selected";} ?>>競標中</option>
						<option value="offtime" <?php if ($this->tplVar['status']["search"]["search_status"] == 'offtime') {echo "selected";} ?>>已結束</option>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_status">競標狀態：</span>
					<select name="search_closed">
						<option value="">All</option>
						<option value="Y" <?php if ($this->tplVar['status']["search"]["search_closed"] == 'Y') {echo "selected";} ?>>已結標</option>
						<option value="N" <?php if ($this->tplVar['status']["search"]["search_closed"] == 'N') {echo "selected";} ?>>競標中</option>
						<option value="NB" <?php if ($this->tplVar['status']["search"]["search_closed"] == 'NB') {echo "selected";} ?>>範本(NB)&流標</option>
					</select>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>


		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<!--<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a>
						</div>-->
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<?php /*<th class="list-checkbox">
									<input type="checkbox" name="chkbox"/>
								</th>*/?>
								<th></th>
								<th></th>
								<th></th>
								<th>商品編號</th>
								<th>商品名稱</th>
								<th>活動類別</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_ontime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_ontime=desc">循環上架時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_ontime"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=">循環上架時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_ontime=asc">循環上架時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_offtime"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_offtime=desc">循環下架時間<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_offtime"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=">循環下架時間<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_offtime=asc">循環下架時間<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>間隔時間[分]</th>
								<th>手續費</th>
								<th>拆分比</th>
								<!-- th>保底金額</th -->
								<th>下標限制</th>
								<!--<th>狀態</th>
								 th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_seq"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_seq=desc">排序<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_seq"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=">排序<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_seq=asc">排序<span class="sort init"></span></a>
								<?php endif; ?>
								</th -->
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_insertt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_insertt=desc">新增日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_insertt"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=">新增日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_insertt=asc">新增日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_modifyt"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_modifyt=desc">修改日期<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_modifyt"] == 'desc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=">修改日期<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_modifyt=asc">修改日期<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) { ?>
							<?php
							foreach($this->tplVar["table"]['record'] as $rk => $rv) {
							if($rv['is_flash']=='S'){
								$mod = 'S碼';
							}elseif($rv['is_flash']=='Y'){
								$mod = '閃殺';
							}else{
								if($rv['is_big']=='Y'){
									$mod = '一般(大檔)';
								}else{
									$mod = '一般';
								}
							}
							?>
							<tr>
								<?php /*<td class="list-checkbox"><input type="checkbox" name="chkbox"/></td>*/?>
								<td class="icon" id="editcronprod<?php echo $rv['ori_productid'];?>" ><?php if ($rv['used']=='N'){?><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit_cron/productid=<?php echo $rv["productid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Edit'></a><?php }?></td>
								<td class="icon" id="cronprod<?php echo $rv['ori_productid'];?>" ><?php if ($rv['used']=='N'){?><input class="table_td_button" type="button" value="產生循環商品" onclick="gen_cronprod('<?php echo $rv['productid'];?>')"><?php }else{ echo "已生成";}?></td>
								<td class="icon"><a class="icon-delete icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_cron/productid=<?php echo $rv["productid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>" title='Delete'></a></td>
								<td class="column" name="productid"><?php echo $rv['productid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name'];
									if (!(empty($rv['img1']))) echo "<BR>{$rv['img1']} ";
									if (!(empty($rv['img2']))) echo "{$rv['img2']} ";
									if (!(empty($rv['img3']))) echo "{$rv['img3']} ";
									if (!(empty($rv['img4']))) echo "{$rv['img4']} ";?></td>
								<td class="column" name="is_flash"><?php echo $mod; ?></td>
								<td class="column" name="cronStart"><?php echo substr($rv['cronStart'],0,16); ?></td>
								<td class="column" name="cronEnd"><?php echo substr($rv['cronEnd'],0,16); ?></td>
								<td class="column" name="ini_intval"><?php echo $rv['ini_intval']; ?></td>								
								<td class="column" name="saja_fee"><?php echo $rv['saja_fee']; ?></td>
								<td class="column" name="split_rate"><?php echo $rv['split_rate']; ?>％</td>
								<!-- td class="column" name="base_value"><?php echo $rv['base_value']; ?></td -->
								<td class="column" name="limitid">
								   <?php echo $arrDesc['limit'][$rv['limitid']]; ?>
								</td>
								<!--<td class="column" name="status">
								<?php if (time() < strtotime($rv['ontime'])) : ?>
									未上架
								<?php elseif (time() < strtotime($rv['offtime']) && $rv['closed'] == 'N') : ?>
									競標中
								<?php else : ?>
									已結束(<?php echo number_format($this->tplVar["table"]["historycount"][$rv['productid']][0]['count']); ?>)
								<?php endif; ?>
								</td>
								td class="column" name="seq"><?php echo $rv['seq']; ?></td -->
								<td class="column" name="insertt"><?php echo $rv['insertt']; ?></td>
								<td class="column" name="modifyt"><?php echo $rv['modifyt']; ?></td>
							</tr>
							<?php }//endforeach; ?>
							<?php }//endif; ?>
						</tbody>
					</table>

				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
		<article class="message center" id="msg-dialog">
			<header>
				<h2>
					<span class="msg" id="msg-title">訊息</span>
					<a class="button-close" href="javascript:void(0);" id="msg-close">X</a>
				</h2>
			</header>
			<div class="" >
				<p id="msg">Message content</p>
			</div>
		</article>
	</body>
</html>
<script type="text/javascript">
//修改身份證字號
function gen_cronprod(productid){
	$('#msg-dialog').hide();
	var url="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/crontab/productid="+productid+"&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ; ?>";
	$.post(url,
		{
			productid:productid
		},
		function(str_json){
            alert('資料建立中,按新增筆數多少時間不定可稍後在產品資料中確認');
			$('#cronprod'+productid).html("資料建立中");
			$('#editcronprod'+productid).html("&nbsp;");
		}
	);
};
</script>