<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
			//商品種類
			$('.relation-list.layer1 .relation-item').on('change', function(){
				var $this = $(this),
					node = $this.val(),
					checked = $this.prop('checked');

				if (checked) {
					$('.relation-list.layer2 li:has(.relation-item[rel='+node+'])').show();
				} else {
					$('.relation-list.layer2 li:has(.relation-item[rel='+node+'])').hide()
					.find('.relation-item[rel='+node+']').prop('checked', false);
				}
			}).change();

			//結標方式
			$('#bid-type').on('change', function(){
				var d = new Date(new Date().getTime()+86400000),
					new_offtime = d.getUTCFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate(),
					old_offtime = $('[name=offtime]').val(),
					offtime = $('[name=offtime]'),
					saja_limit = $('[name=saja_limit]');

				switch($(this).val()) {
					//time
					case 'T' :
						offtime.prop('readonly', false).val(offtime.data('org').match(/^0000-00-00/)?new_offtime:old_offtime);
						saja_limit.prop('readonly', true).val('0'); //$('[name=saja_limit]').prop('readonly', true).val('0');
						break;
					case 'count' :
						offtime.prop('readonly', true).val('0000-00-00'); //$('[name=offtime]').prop('readonly', true).val('0000-00-00');
						saja_limit.prop('readonly', false).val(parseInt(saja_limit.data('org')) || 1);
						break;
					//complex
					case 'A' :
						offtime.prop('readonly', false).val(offtime.data('org').match(/^0000-00-00/)?new_offtime:old_offtime);
						saja_limit.prop('readonly', false).val(parseInt(saja_limit.data('org')) || 1);
						break;
				}
			}).change();

			//紅利積分,店點
			$('#bonus_type, #gift_type').on('change', function(){
				var unit = '';
				switch($(this).val()) {
					case 'ratio':
						unit = '％';
						break;
					case 'value':
						unit = '點';
						break;
				}
				$(this).siblings('.unit').text(unit);
			}).change();

			//$('select[name="storeid"] option').attr('style', 'display:none');
			//分館
			/*var channelid = "<?php echo $this->tplVar['table']['rt']['channel_store_rt'][0]['channelid']; ?>";
			var storeid = "<?php echo $this->tplVar['table']['rt']['store_product_rt'][0]['storeid']; ?>";
			$('select[name="channelid"] option[value=' + channelid + ']').prop('selected', true);
			$('select[name="storeid"] option').attr('style', 'display:none');
			$('select[name="storeid"] option[rel="empty"]').removeAttr('style');
			$('select[name="storeid"] option[rel='+channelid+']').removeAttr('style');
			$('select[name="storeid"] option[value=' + storeid + ']').prop('selected', true);

			$('select[name="channelid"]').change(function() {
				var $this = $(this),
					node = $this.val(),
					checked = $this.prop('selected', true);

				$('select[name="storeid"] option').attr('style', 'display:none');

				if (checked) {
					$('select[name="storeid"] option[rel="empty"]').removeAttr('style');
					$('select[name="storeid"] option[rel='+node+']').removeAttr('style');
					$('select[name="storeid"] option[value=""]').prop('selected', true);
				}
			});*/

			//商品種類
			$('.relation-list.channel .relation-item').on('change', function(){
				var $this = $(this),
					node = $this.val(),
					checked = $this.prop('checked');

				if (checked) {
					$('.relation-list.store li:has(.relation-item[rel='+node+'])').show();
				} else {
					$('.relation-list.store li:has(.relation-item[rel='+node+'])').hide()
					.find('.relation-item[rel='+node+']').prop('checked', false);
				}
			}).change();

			$('#edit_item').on('click', function(e){
				if(($("#totalfee_type").val() == 'O') && ($("#saja_fee").val() != 0)){
					alert('下標計費方式為免費，下標金額必須為0!');
					return false;
				}

                /*
				if (CKEDITOR.instances.ckeditor_rule.getData()=='') {
				    // if (document.getElementById("ckeditor_rule").value == ''){
					       alert('商品下標規則未填寫!');
				}
				*/
				codenum = $("#codenum").val();
				if(codenum>=100){
					var answer = confirm('殺價券出貨數量超過100張,是否確認寫入?');
					if(answer){
						alert('寫入殺價券出貨數量:'+codenum+'張');
					}else{
					   return false;
					}
				}
				$('#bid_oscode').attr("disabled", false);
				$('#pay_type').attr("disabled", false);
				$('#totalfee_type').attr("disabled", false);
				$('#form-add').submit();
			});
		});
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">競標商品資料管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update_cron">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
                <tr>
					<td><label for="cronStart">循環上架開始時間 ：</label></td>
					<td><input name="cronStart" id="cronStart" type="text" value="<?php echo $this->tplVar['table']['record'][0]['cronStart']; ?>"/><font style="color:#FF0000">*輸入格式如2021-01-01 11:22:33</font></td>
				</tr>
				<tr>
					<td><label for="cronEnd">循環上架結束時間</label></td>
					<td><input name="cronEnd" id="cronEnd" type="text" value="<?php echo $this->tplVar['table']['record'][0]['cronEnd']; ?>"/><font style="color:#FF0000">*輸入格式如2021-01-01 16:22:33</font></td>
				</tr>
				<tr>
					<td><label for="ini_intval">間隔時間(單位:分鐘)</label></td>
					<td><input name="ini_intval" id="ini_intval" type="number" value="<?php echo $this->tplVar['table']['record'][0]['ini_intval']; ?>"/><font style="color:#FF0000">*僅接受大於0整數</font></td>
				</tr>               
				<tr>
					<td><label for="name">商品名稱：</label></td>
					<td><input name="name" type="text" data-validate="required" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>"/> 商品名稱 請勿輸入 單引號' 雙引號" 右斜線\ (系統儲存時將會自動過濾移除)</td>
				</tr>
				<tr>
					<td><label for="description">商品敘述：</label></td>
					<td><textarea name="description" id="ckeditor_description" class="description"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="rule">商品下標規則：</label></td>
					<td><textarea name="rule" id="ckeditor_rule" class="description"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['rule']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="retail_price">商品市價：</label></td>
					<td><input name="retail_price" type="text" data-validate="" value="<?php echo $this->tplVar['table']['record'][0]['retail_price']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="cost_price">商品進貨成本價：</label></td>
					<td><input name="cost_price" type="text" data-validate="" value="<?php echo $this->tplVar['table']['record'][0]['cost_price']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail">商品主圖 (首頁專用 1:1)：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/'. $this->tplVar['table']['record'][0]['thumbnail']; ?>" target="_blank">預覽</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail2">商品主圖二 (內頁專用4:3)：</label></td>
					<td>
						<input name="thumbnail2" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail2'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/'. $this->tplVar['table']['record'][0]['thumbnail2']; ?>" target="_blank">預覽</a> <?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail3">商品主圖三 (內頁專用4:3)：</label></td>
					<td>
						<input name="thumbnail3" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail3'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/'. $this->tplVar['table']['record'][0]['thumbnail3']; ?>" target="_blank">預覽</a> /
						<a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_thumbnail/?productid=<?php echo $this->tplVar['table']['record'][0]['productid'];?>&ptid3=<?php echo $this->tplVar['table']['record'][0]['ptid3'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">刪除主圖</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail4">商品主圖四 (內頁專用4:3)：</label></td>
					<td>
						<input name="thumbnail4" type="file"/><br>
						<?php if (!empty($this->tplVar['table']['record'][0]['thumbnail4'])) : ?>
						<a href="//<?php echo $this->config->domain_name .'/site/images/site/product/'. $this->tplVar['table']['record'][0]['thumbnail4']; ?>" target="_blank">預覽</a> /
						<a href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/delete_thumbnail/?productid=<?php echo $this->tplVar['table']['record'][0]['productid'];?>&ptid4=<?php echo $this->tplVar['table']['record'][0]['ptid4'];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">刪除主圖</a>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td><label for="thumbnail_url">商品主圖(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail_url']; ?>"/></td>
				</tr>

				<tr>
					<td><label for="pay_type">下標支付方式：</label></td>
					<td>
						<select name="pay_type" id="pay_type" <?php echo ($this->tplVar['table']['already_bid'] == 'true') ? "disabled='disabled'" :'';?>>
							<option value="a" <?php echo $this->tplVar['table']['record'][0]['pay_type']=="a"?" selected":""; ?> >殺幣</option>
							<option value="b" <?php echo $this->tplVar['table']['record'][0]['pay_type']=="b"?" selected":""; ?> >殺幣 + 超殺券 + 殺價券</option>
							<option value="c" <?php echo $this->tplVar['table']['record'][0]['pay_type']=="c"?" selected":""; ?> >殺幣 + 殺價券</option>
							<option value="d" <?php echo $this->tplVar['table']['record'][0]['pay_type']=="d"?" selected":""; ?> >超殺券 + 殺價券</option>
							<option value="e" <?php echo $this->tplVar['table']['record'][0]['pay_type']=="e"?" selected":""; ?> >殺價券</option>
							<option value="f" <?php echo $this->tplVar['table']['record'][0]['pay_type']=="f"?" selected":""; ?> >圓夢券(有返饋+無返饋)</option>
						</select>
						<font color='red'>*注意 : 宅公益商品必須限制只能用殺幣下標</font>
					</td>
				</tr>
				<tr>
					<td><label for="totalfee_type">下標總金額計算方式：</label></td>
					<td>
						<select name="totalfee_type" id="totalfee_type" <?php echo ($this->tplVar['table']['already_bid'] == 'true') ? "disabled='disabled'" :'';?>>
							<!--option value="A" <?php echo $this->tplVar['table']['record'][0]['totalfee_type']=="A"?" selected":""; ?> >手續費 + 出價金額</option>
							<option value="B" <?php echo $this->tplVar['table']['record'][0]['totalfee_type']=="B"?" selected":""; ?> >出價金額</option>
							<option value="F" <?php echo $this->tplVar['table']['record'][0]['totalfee_type']=="F"?" selected":""; ?> >手續費</option>
							<option value="O" <?php echo $this->tplVar['table']['record'][0]['totalfee_type']=="O"?" selected":""; ?> >免費</option-->
							<option value="F" <?php echo $this->tplVar['table']['record'][0]['totalfee_type']=="F"?" selected":""; ?> >手續費</option>
						</select>
					</td>
				</tr>
				<tr>
					<!-- <td><label for="bid_spoint">中標得殺幣(點數)：</label></td> -->
					<td><input name="bid_spoint" type="hidden" value="<?php echo $this->tplVar['table']['record'][0]['bid_spoint']; ?>" readonly/></td>
				</tr>
				<tr>
					<!-- <td><label for="bid_scode">中標送超殺券(一般發送活動編號)：</label></td> -->
					<td><input name="bid_scode" type="hidden" value="<?php echo $this->tplVar['table']['record'][0]['bid_scode']; ?>" readonly/></td>
				</tr>
				<tr>
					<!-- <td><label for="bid_oscode">中標送殺價券(一般發送活動編號)：</label></td> -->
					<!-- <td>
					<select name="bid_oscode" id="bid_oscode" disabled='disabled'>
						<option value="0" <?php if ($this->tplVar['table']['record'][0]['bid_oscode'] == $pcv['spid']) {echo "selected";} ?>>請選擇</option>
					<?php foreach ($this->tplVar['table']['rule_oscode'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['spid']; ?>" <?php if ($this->tplVar['table']['record'][0]['bid_oscode'] == $pcv['spid']) {echo "selected";} ?> ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
					</td> -->
					<td><input name="bid_oscode" type="hidden" value="0" readonly/></td>
				</tr>
				<tr style="display:none">
					<td><label for="is_flash">閃殺活動：</label></td>
					<td>
						<select name="is_flash">
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['is_flash']=="N"?" selected":""; ?> >否</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['is_flash']=="Y"?" selected":""; ?> >是</option>
						</select>
					</td>
				</tr>
				<tr style="display:none">
					<td><label for="flash_loc">閃殺地點：</label></td>
					<td><input name="flash_loc" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['flash_loc']; ?>"/></td>
				</tr>
				<tr style="display:none">
					<td><label for="flash_loc_map">閃殺地點地圖嵌入網址：</label></td>
					<td><input name="flash_loc_map" type="text" size="60" value="<?php echo $this->tplVar['table']['record'][0]['flash_loc_map']; ?>"/></td>
				</tr>
				<tr style="display:none">
					<td><label for="vendorid">(閃殺)場次代碼：</label></td>
					<td><input name="vendorid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['vendorid']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="saja_fee">下標手續費：</label></td>
					<td><input name="saja_fee" id="saja_fee" type="text" value="<?php echo $this->tplVar['table']['record'][0]['saja_fee']; ?>" <?php echo ($this->tplVar['table']['already_bid'] == 'true') ? "readonly" :'';?>/></td>
				</tr>
				<tr>
					<td><label for="process_fee">得標處理費：</label></td>
					<td>
						<select name="process_fee">
							<!-- option value="0">0%</option -->
						<?php for($i=20 ; $i <= 30 ; $i+=1) : ?>
							<?php if ($i == (int)$this->tplVar['table']['record'][0]['process_fee']) : ?>
							<option value="<?php echo $i; ?>" selected><?php echo $i."%"; ?></option>
							<?php else : ?>
							<option value="<?php echo $i; ?>"><?php echo $i."%"; ?></option>
							<?php endif; ?>
						<?php endfor; ?>
						<?php for($i=35 ; $i <= 50 ; $i+=5) : ?>
							<?php if ($i == (int)$this->tplVar['table']['record'][0]['process_fee']) : ?>
							<option value="<?php echo $i; ?>" selected><?php echo $i."%"; ?></option>
							<?php else : ?>
							<option value="<?php echo $i; ?>"><?php echo $i."%"; ?></option>
							<?php endif; ?>
						<?php endfor; ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="checkout_type">得標結帳方式：</label></td>
					<td>
						<select name="checkout_type">
							<option value="all" <?php echo $this->tplVar['table']['record'][0]['checkout_type']=="all"?" selected":""; ?> >得標金額 + 得標處理費</option>
							<option value="pf" <?php echo $this->tplVar['table']['record'][0]['checkout_type']=="pf"?" selected":""; ?> >得標處理費</option>
							<option value="bid" <?php echo $this->tplVar['table']['record'][0]['checkout_type']=="bid"?" selected":""; ?> >得標金額</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="checkout_money">得標處理金：</label></td>
					<td><input name="checkout_money" type="number" value="<?php echo $this->tplVar['table']['record'][0]['checkout_money']; ?>"/></td>
				</tr>
				<tr>
					<!-- <td><label for="price_limit">下標底價：</label></td> -->
					<td><input name="price_limit" type="hidden"" value="<?php echo $this->tplVar['table']['record'][0]['price_limit']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="is_discount">是否折抵：</label></td>
					<td>
						<select name="is_discount">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['is_discount']=="Y"?" selected":""; ?>>可以折抵</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['is_discount']=="N"?" selected":""; ?>>不能折抵</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="bonus">紅利積分：</label></td>
					<td>
						<select name="bonus_type" id="bonus_type">
							<option value="none"<?php echo $this->tplVar['table']['record'][0]['bonus_type']=="none"?" selected":""; ?>>不回饋</option>
							<option value="ratio"<?php echo $this->tplVar['table']['record'][0]['bonus_type']=="ratio"?" selected":""; ?>>回饋比例</option>
							<option value="value"<?php echo $this->tplVar['table']['record'][0]['bonus_type']=="value"?" selected":""; ?>>回饋定額</option>
						</select><br>
						<input name="bonus" type="text" class="" value="<?php echo $this->tplVar['table']['record'][0]['bonus']; ?>"/>
						<span id="bonus_unit" class="unit"></span>
					</td>
				</tr>
				<tr style="display:none">
					<td><label for="bid_type" >結標方式</label></td>
					<td>
						<select id="bid-type" name="bid_type">
						<option value="T" <?php echo ($this->tplVar['table']['record'][0]['bid_type']=='T') ? 'selected':'';?> >時間標</option>
						<option value="A" <?php echo ($this->tplVar['table']['record'][0]['bid_type']=='A') ? 'selected':'';?> >綜合標</option>
						<?php /*if ($this->tplVar['table']['record'][0]['saja_limit']==0) : ?>
							<option value="time" selected>時間標</option>
							<option value="count">次數標</option>
							<option value="complex">綜合標</option>
						<?php elseif ($this->tplVar['table']['record'][0]['offtime']==0) : ?>
							<option value="time">時間標</option>
							<option value="count" selected>次數標</option>
							<option value="complex">綜合標</option>
						<?php else : ?>
							<option value="time">時間標</option>
							<option value="count">次數標</option>
							<option value="complex" selected>綜合標</option>
						<?php endif; */?>
						</select>
					</td>
				</tr>
				<tr style="display:none">
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" id="ontime" type="text" class="datetime time-start" value="<?php echo $this->tplVar['table']['record'][0]['ontime']; ?>"/></td>
				</tr>
				<tr style="display:none">
					<td><label for="offtime">結標時間：</label></td>
					<td><input name="offtime" id="offtime" type="text" class="datetime time-stop" value="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>" data-org="<?php echo $this->tplVar['table']['record'][0]['offtime']; ?>"/></td>
				</tr>
				<tr style="display:none">
					<td><label for="closed">競標狀態：</label></td>
					<td>
						<select name="closed" id="closed">
							<option value="Y"<?php echo $this->tplVar['table']['record'][0]['closed']=="Y"?" selected":""; ?>>已結標</option>
							<option value="N"<?php echo $this->tplVar['table']['record'][0]['closed']=="N"?" selected":""; ?>>競標中</option>
							<option value="NB"<?php echo $this->tplVar['table']['record'][0]['closed']=="NB"?" selected":""; ?>>範本(NB)</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="saja_limit">商品結標次数限制：</label></td>
					<td><input name="saja_limit" type="text" value="<?php echo $this->tplVar['table']['record'][0]['saja_limit']; ?>" data-org="<?php echo $this->tplVar['table']['record'][0]['saja_limit']; ?>"/> *只算殺價幣下標</td>
				</tr>
				<tr>
					<td><label for="user_limit">會員下標次數限制：</label></td>
					<td><input name="user_limit" type="text" value="<?php echo $this->tplVar['table']['record'][0]['user_limit']; ?>"/>(>=999999 視為不限制下標次數)</td>
				</tr>
				<tr>
					<td><label for="usereach_limit">會員每次連續下標次數限制：</label></td>
					<td><input name="usereach_limit" type="text" value="<?php echo $this->tplVar['table']['record'][0]['usereach_limit']; ?>" /></td>
				</tr>
				<tr>
					<!-- <td><label for="split_rate">拆分比：</label>％（0.00％~100.00％）</td> -->
					<td><input name="split_rate" type="hidden"" value="<?php echo $this->tplVar['table']['record'][0]['split_rate']; ?>"/>
				</tr>
				<tr>
					<!-- <td><label for="base_value">保底金額：</label>0.00~(商品市價*殺幣比率)</td> -->
					<input name="base_value" type="hidden" value="<?php echo $this->tplVar['table']['record'][0]['base_value']; ?>"/>
				</tr>

				<?php /*
				//店點暫停 ~ 20140308 peter
				<tr>
					<td><label for="gift">店點：</label></td>
					<td>
						<select name="gift_type" id="gift_type">
							<option value="none"<?php echo $this->tplVar['table']['record'][0]['gift_type']=="none"?" selected":""; ?>>不回饋</option>
							<option value="ratio"<?php echo $this->tplVar['table']['record'][0]['gift_type']=="ratio"?" selected":""; ?>>回饋比例</option>
							<option value="value"<?php echo $this->tplVar['table']['record'][0]['gift_type']=="value"?" selected":""; ?>>回饋定額</option>
						</select><br>
						<input name="gift" type="text" class="" value="<?php echo $this->tplVar['table']['record'][0]['gift']; ?>"/>
						<span id="gift_unit" class="unit"></span>
					</td>
				</tr>*/?>
				<input type="hidden" name="gift_type" value="none">
				<input type="hidden" name="gift" value="0">

				<tr>
					<td><label for="seq">商品排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				<tr style="display:none">
					<td><label for="seq">商品顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['display']=="Y"?" selected":""; ?>>顯示</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['display']=="N"?" selected":""; ?>>不顯示</option>
						</select>
					</td>
				</tr>
				<tr style="display:none">
					<td><label for="display_all">是否在全部商品中顯示：</label></td>
					<td>
						<select name="display_all">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['display_all']=="Y"?" selected":""; ?>>顯示</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['display_all']=="N"?" selected":""; ?>>不顯示</option>
						</select>
					</td>
				</tr>

			</table>

			<table class="form-table">
				<tr  style="display:none">
					<td><label for="is_big">大檔商品：</label></td>
					<td>
						<select name="is_big">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['is_big']=="Y"?" selected":""; ?>>是</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['is_big']=="N"?" selected":""; ?>>否</option>
						</select>
					</td>
				</tr>
				<tr  style="display:none">
					<td><label for="hot_used">熱門商品：</label></td>
					<td>
						<select name="hot_used">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['hot_used']=="Y"?" selected":""; ?>>是</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['hot_used']=="N"?" selected":""; ?>>否</option>
						</select>
					</td>
				</tr>
				<tr  style="display:none">
					<td><label for="lb_used">直播商品：</label></td>
					<td>
						<select name="lb_used">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['lb_used']=="Y"?" selected":""; ?>>是</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['lb_used']=="N"?" selected":""; ?>>否</option>
						</select>
					</td>
				</tr>
				<!--tr>
					<td><label for="loc_type">限用區域：</label></td>
					<td>
						<select name="loc_type">
							<option value="L" <?php echo $this->tplVar['table']['record'][0]['loc_type']=="L" ? "selected" : ""; ?> >地區</option>
							<option value="G" <?php echo $this->tplVar['table']['record'][0]['loc_type']=="G" ? "selected" : ""; ?> >全國</option>
						</select>
					</td>
				</tr-->
				<input type="hidden" name="loc_type" value="G">
				<!--tr>
					<td><label for="mob_type">手機限用類型：</label></td>
					<td>
						<select name="mob_type">
							<option value="all" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="all" ? "selected" : ""; ?> >都顯示</option>
							<option value="n" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="n" ? "selected" : ""; ?> >都不顯示</option>
							<option value="weixin" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="weixin" ? "selected" : ""; ?> >只在微信顯示</option>
							<option value="yixin" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="yixin" ? "selected" : ""; ?> >只在易信顯示</option>
							<option value="laiwang" <?php echo $this->tplVar['table']['record'][0]['mob_type']=="laiwang" ? "selected" : ""; ?> >只在来往顯示</option>
						</select>
					</td>
				</tr-->
				<input type="hidden" name="mob_type" value="all">
				<tr  style="display:none">
					<td><label for="is_test">是否為測試商品：</label></td>
					<td>
						<select name="is_test">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['is_test']=="Y"?" selected":""; ?>>是</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['is_test']=="N"?" selected":""; ?>>否</option>
						</select>
					</td>
				</tr>
				<?php if ($this->tplVar['table']['today'] > 0){ ?>
				<tr  style="display:none">
					<td><label for="is_today">主題商品同步修改：</label></td>
					<td>
						<select name="is_today">
							<option value="N" selected >否</option>
							<option value="Y" >是</option>
						</select>
					</td>
				</tr>
				<?php } ?>
				<tr  style="display:none">
					<td><label for="ptype">商品類型：</label></td>
					<td>
						<select name="ptype" id="ptype">
							<option value="0" <?php echo $this->tplVar['table']['record'][0]['ptype']=="0"?" selected":""; ?>>殺戮商品</option>
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['ptype']=="1"?" selected":""; ?>>圓夢商品</option>
						</select>
					</td>
				</tr>
				<tr style="display:none">
					<td><label for="description">商品種類：</label></td>
					<td>
						<ul id="ul_pcid" class="relation-list layer1">
						<?php foreach ($this->tplVar['table']['rt']['product_category_rt'] as $pck => $pcv) : ?>
							<?php if ($pcv['layer'] == 1) : ?>
							<li ptype="<?php echo $pcv['ptype']; ?>">
								<?php if (empty($pcv['productid'])) : ?>
								<input class="relation-item" type="checkbox" name="pcid[]" id="pcid_<?php echo $pck; ?>" value="<?php echo $pcv['pcid']; ?>"/>
								<?php else : ?>
								<input class="relation-item" type="checkbox" name="pcid[]" id="pcid_<?php echo $pck; ?>" value="<?php echo $pcv['pcid']; ?>" checked/>
								<?php endif; ?>
								<span class="relation-item-name" for="pcid_<?php echo $pck; ?>"><?php echo $pcv['name']; ?></span>
							</li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				<tr style="display:none">
					<td><label for="description">商品子類別：</label></td>
					<td>
						<ul class="relation-list layer2">
						<?php foreach ($this->tplVar['table']['rt']['product_category_rt'] as $pck => $pcv) : ?>
							<?php if ($pcv['layer'] == 2) : ?>
							<li>
								<?php if (empty($pcv['productid'])) : ?>
								<input class="relation-item" rel="<?php echo $pcv['node']; ?>" type="checkbox" name="pcid[]" id="pcid_<?php echo $pck; ?>" value="<?php echo $pcv['pcid']; ?>"/>
								<?php else : ?>
								<input class="relation-item" rel="<?php echo $pcv['node']; ?>" type="checkbox" name="pcid[]" id="pcid_<?php echo $pck; ?>" value="<?php echo $pcv['pcid']; ?>" checked/>
								<?php endif; ?>
								<span class="relation-item-name" for="pcid_<?php echo $pck; ?>"><?php echo $pcv['name']; ?></span>
							</li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				<!--tr>
					<td><label for="seq">下标资格：</label></td>
					<td>
						<ul class="relation-list layer2">
						<?php foreach($this->tplVar['table']['rt']['saja_rule'] as $srk => $srv) : ?>
							<li>
								<?php if(empty($srv['productid'])){ ?>
									<?php if($srv['srid']=='any_saja'){ ?>
									<input class="relation-item" type="radio" name="srid" id="srid_<?php echo $srk; ?>" value="any_saja" checked />
									<?php } else { ?>
									<input class="relation-item" type="radio" name="srid" id="srid_<?php echo $srk; ?>" value="<?php echo $srv['srid']; ?>"/>
									<?php }//endif;?>
								<?php } else { ?>
								<input class="relation-item" type="radio" name="srid" id="srid_<?php echo $srk; ?>" value="<?php echo $srv['srid']; ?>" checked />
								<?php }//endif; ?>
								<?php
								switch ($srv['srid']) {
									case 'le_win_saja':
										echo '得標次數<input class="short-text" type="text" name="'.$srv['srid'].'_value" value="'.$srv['value'].'" size=3/>次以內';
										break;
									case 'ge_win_saja':
										echo '得標次數<input class="short-text" type="text" name="'.$srv['srid'].'_value" value="'.$srv['value'].'" size=3/>次以上';
										break;
									default:
										echo $srv['name'];
										break;
								}
								?>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr-->
				<tr>
					<td><label for="limitid">限定商品：</label></td>
					<td><select name="limitid">
						<option value="0">請選擇</option>
					<?php foreach ($this->tplVar['table']['rt']['product_limited'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['plid']; ?>" <?php echo $this->tplVar['table']['record'][0]['limitid']==$pcv['plid']?" selected":""; ?>><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
					</td>
				</tr>
				<tr>
					<td><label for="is_exchange">是否為兌換商品：</label></td>
					<td>
						<select name="is_exchange">
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['is_exchange']=="1"?" selected":""; ?>>是</option>
							<option value="0" <?php echo $this->tplVar['table']['record'][0]['is_exchange']=="0"?" selected":""; ?>>否</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="epid">兌換商品ID：</label></td>
					<td><input name="epid" type="text" value="<?php echo $this->tplVar['table']['record'][0]['epid']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="ordertype">出貨方式：</label></td>
					<td>
						<select name="ordertype">
							<option value=1 <?php echo $this->tplVar['table']['record'][0]['ordertype']==1 ?" selected":""; ?>>商品出貨</option>
							<option value=2 <?php echo $this->tplVar['table']['record'][0]['ordertype']==2 ?" selected":""; ?>>鯊魚點出貨</option>
							<option value=3 <?php echo $this->tplVar['table']['record'][0]['ordertype']==3 ?" selected":""; ?>>殺價卷出貨</option>
						</select>
					</td>
				</tr>
				<tr style="display:none">
					<td><label for="chainstatus">是否上區塊鋉：</label></td>
					<td>
						<select name="chainstatus">
							<option value="" >請選擇</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['chainstatus']=="N"?" selected":""; ?>>不上區塊鋉</option>
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['chainstatus']=="Y"?" selected":""; ?>>上區塊鋉</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="orderbonus">出貨鯊魚點數量：</label></td>
					<td><input name="orderbonus" type="number" value="<?php echo $this->tplVar['table']['record'][0]['orderbonus']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="everydaybid">每天付費可下標數：</label></td>
					<td><input name="everydaybid" id="everydaybid" type="number" value="<?php echo $this->tplVar['table']['record'][0]['everydaybid']; ?>"/><font style="color:#FF0000">*預設零則不限制下標次數</font></td>
				</tr>
				<tr>
					<td><label for="freepaybid">每天免費可下標數：</label></td>
					<td><input name="freepaybid" id="freepaybid" type="number" value="<?php echo $this->tplVar['table']['record'][0]['freepaybid']; ?>"/><font style="color:#FF0000">*預設零則不限制下標次數</font></td>
				</tr>
				<tr>
					<td><label for="codepid">殺價券關聯商品編號：</label></td>
					<td><input name="codepid" id="codepid" type="number" value="<?php echo $this->tplVar['table']['record'][0]['codepid']; ?>"/><font style="color:#FF0000">*僅限殺價券商品使用</font></td>
				</tr>
				<tr>
					<td><label for="codenum">殺價券出貨數量：</label></td>
					<td><input name="codenum" id="codenum" type="number" value="<?php echo $this->tplVar['table']['record'][0]['codenum']; ?>"/><font style="color:#FF0000">*僅限殺價券商品使用</font></td>
				</tr>
             
				<!--<tr>
					<td><label for="channelid">經銷商：</label></td>
					<td>
						<select name="channelid">
							<option value="">請選擇</option>
							<?php
								if (!empty($this->tplVar['table']['channel'])) {
									foreach ($this->tplVar['table']['channel'] as $key => $value) {
							?>
							<option value="<?php echo $value['channelid']; ?>" ><?php echo $value['name']; ?></option>
							<?php } } ?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="storeid">分館：</label></td>
					<td>
						<select name="storeid">
							<option rel="empty" value="">請選擇</option>
							<?php
								if (!empty($this->tplVar['table']['store'])) {
									foreach ($this->tplVar['table']['store'] as $key => $value) {
							?>
							<option rel="<?php echo $value['channelid']; ?>" value="<?php echo $value['storeid']; ?>" ><?php echo $value['name']; ?></option>
							<?php } } ?>
						</select>
					</td>
				</tr>-->
				<!--tr>
					<td><label for="description">經銷商：</label></td>
					<td>
						<ul class="relation-list channel">
						<?php foreach ($this->tplVar['table']['rt']['channel_store_rt'] as $ck => $cv) : ?>
							<li>
								<?php if (is_null($cv['storeid'])) : ?>
								<input class="relation-item" type="checkbox" name="channelid[]" id="channelid_<?php echo $ck; ?>" value="<?php echo $cv['channelid']; ?>"/>
								<?php else : ?>
								<input class="relation-item" type="checkbox" name="channelid[]" id="channelid_<?php echo $ck; ?>" value="<?php echo $cv['channelid']; ?>" checked/>
								<?php endif; ?>
								<span class="relation-item-name" for="channelid_<?php echo $ck; ?>"><?php echo $cv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr-->
				<input class="relation-item" type="hidden" name="channelid[]" id="channelid_0" value="0" />
				<input class="relation-item" type="hidden" name="storeid[]" id="storeid_0" value="0" />
				<!--tr>
					<td><label for="description">分館：</label></td>
					<td>
						<ul class="relation-list store">
						<?php foreach ($this->tplVar['table']['rt']['store_product_rt'] as $sk => $sv) : ?>
							<li>
								<?php if (is_null($sv['productid'])) : ?>
								<input class="relation-item" rel="<?php echo $sv['channelid']; ?>" type="checkbox" name="storeid[]" id="storeid_<?php echo $sk; ?>" value="<?php echo $sv['storeid']; ?>"/>
								<?php else : ?>
								<input class="relation-item" rel="<?php echo $sv['channelid']; ?>" type="checkbox" name="storeid[]" id="storeid_<?php echo $sk; ?>" value="<?php echo $sv['storeid']; ?>" checked/>
								<?php endif; ?>
								<span class="relation-item-name" for="storeid_<?php echo $sk; ?>"><?php echo $sv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr-->
			</table>

			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="productid" value="<?php echo $this->tplVar["status"]["get"]["productid"] ;?>">
				<input type="hidden" name="oldthumbnail" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail'];?>">
				<input type="hidden" name="oldptid" value="<?php echo $this->tplVar['table']['record'][0]['ptid'];?>">
				<input type="hidden" name="oldthumbnail2" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail2'];?>">
				<input type="hidden" name="oldptid2" value="<?php echo $this->tplVar['table']['record'][0]['ptid2'];?>">
				<input type="hidden" name="oldthumbnail3" value="<?php echo $this->tplVar['table']['record'][0]['thumbnail3'];?>">
				<input type="hidden" name="oldptid3" value="<?php echo $this->tplVar['table']['record'][0]['ptid3'];?>">
				<div class="button submit"><input name="edit_item" id="edit_item" type="button" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>

		</form>
		<div class="dialogs"></div>
	</body>
</html>

<script type="text/javascript">
$(document).ready(function() {
	var ptype = $("#ptype").val();
	console.log("ptype:"+ptype);
	$("#ul_pcid").find("li[ptype]").hide();
	$("#ul_pcid").find("li[ptype='"+ptype+"']").show();
});
$("#ptype").change(function(event) {
	var ptype = $(this).val();
	$("#ul_pcid").find("li[ptype]").hide();
	$("#ul_pcid").find("li[ptype='"+ptype+"']").show();
});
</script>