<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
			//商品種類
			$('.relation-list.layer1 .relation-item').on('change', function(){
				var $this = $(this),
					node = $this.val(),
					checked = $this.prop('checked');

				if (checked) {
					$('.relation-list.layer2 li:has(.relation-item[rel='+node+'])').show();
				} else {
					$('.relation-list.layer2 li:has(.relation-item[rel='+node+'])').hide()
					.find('.relation-item[rel='+node+']').prop('checked', false);
				}				
			}).change();

			//結標方式
			$('#bid-type').on('change', function(){
				var d = new Date(new Date().getTime()+86400000),
					new_offtime = d.getUTCFullYear()+'-'+(d.getMonth()+1)+'-'+d.getDate(),
					offtime = $('[name=offtime]'),
					saja_limit = $('[name=saja_limit]');

				switch($(this).val()) {
					//time
					case 'T' :
						offtime.prop('readonly', false).val(new_offtime);
						saja_limit.prop('readonly', true).val('0'); //$('[name=saja_limit]').prop('readonly', true).val('0');
						break;
					case 'count' :
						offtime.prop('readonly', true).val('0000-00-00'); //$('[name=offtime]').prop('readonly', true).val('0000-00-00');
						saja_limit.prop('readonly', false).val(1);
						break;
					//complex
					case 'A' :
						offtime.prop('readonly', false).val(new_offtime);
						saja_limit.prop('readonly', false).val(1);
						break;
				}
			}).change();

			//紅利積分,店點
			$('#bonus_type, #gift_type').on('change', function(){
				var unit = '';
				switch($(this).val()) {
					case 'ratio':
						$('#bonus').val(100);
						unit = '％';
						break;
					case 'value':
						$('#bonus').val(0);
						unit = '點';
						break;
				}
				$(this).siblings('.unit').text(unit);
			});
			
			//商品種類
			$('.relation-list.channel .relation-item').on('change', function(){
				var $this = $(this),
					node = $this.val(),
					checked = $this.prop('checked');

				if (checked) {
					$('.relation-list.store li:has(.relation-item[rel='+node+'])').show();
				} else {
					$('.relation-list.store li:has(.relation-item[rel='+node+'])').hide()
					.find('.relation-item[rel='+node+']').prop('checked', false);
				}				
			}).change();
		
			$('#add_item').on('click', function(e){
				prod_name=$("#name").val();
				if(!prod_name || prod_name=='') {
				   	alert('請填寫商品名稱 !!');
					return false;
				}
				
				// 用ckeditor 編輯 textarea的值必須以如下方式存取 :
				// CKEDITOR.instances.元素id名稱.getData();
				// CKEDITOR.instances.元素id名稱.setData("value");
				/*
				prod_desc = CKEDITOR.instances.ckeditor_description.getData();
				if (!prod_desc || prod_desc == ''){
					alert('請填寫商品敘述 !');
					return false;
				}
				
				bid_rule=CKEDITOR.instances.ckeditor_rule.getData();
				if (!bid_rule || bid_rule == ''){
					alert('請填寫商品下標規則!');
					return false;
				}
				*/
				retail_price = $("#retail_price").val();
				if (!retail_price || retail_price==''){
					alert('請填寫商品市價 !');
					return false;
				}
				if(retail_price<=0){
				   alert('商品市價錯誤 !');
				   return false; 	
				}
				
				if(($("#totalfee_type").val() == 'O') && ($("#saja_fee").val() != 0)){
					alert('下標計費方式為免費，下標金額必須為0!');
					return false;
				}
                ontime = Date.parse(new Date($('#ontime').val().replace(/-/g, '/')));
				if(isNaN(ontime)) {
					alert('請填寫上架時間!');
					return false;
				}
				offtime = Date.parse(new Date($('#offtime').val().replace(/-/g, '/')));
				if(isNaN(offtime)) {
					alert('請填寫結標時間!');
					return false;
				}
				if(ontime>=offtime) {
				   alert('上架時間不可大於結標時間!');
				   return false;
				}
				
                price_limit=$('#price_limit').val();
                if(!price_limit || price_limit<=0) {
                    alert('商品底價錯誤 !');
				    return false;
                }				
                if(price_limit>=retail_price) {
                    alert('底價不可高於商品市價 ('+retail_price+'元) !');
				    return false;
                }					
				/*
				if (document.getElementById("ckeditor_rule").value == ''){
					alert('商品下標規則未填寫!');
				}
                */
				codenum = $("#codenum").val();
				if(codenum>=100){
					var answer = confirm('殺價券出貨數量超過100張,是否確認寫入?');
					if(answer){
						alert('寫入殺價券出貨數量:'+codenum+'張');
					}else{
					   return false;
					}
				}
				$('#bid_oscode').attr("disabled", false);
				$('#form-add').submit();
				
			});

			$("#ul_pcid").find("li[ptype='0']").hide();
		
		});
		
		
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a>首頁</a>>><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">競標商品資料管理</a>>><a>新增</a>
		</div>
		<form class="form" id="form-add" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">商品名稱：</label></td>
					<td><input name="name" id="name" type="text" class="required"/>請勿輸入 單引號' 雙引號" 右斜線\ (系統會自動過濾移除)</td>
				</tr>
				<tr>
					<td><label for="description">商品敘述：</label></td>
					<td><textarea name="description" id="ckeditor_description" class="description"></textarea></td>
				</tr>
				<tr>
					<td><label for="rule">商品下標規則：</label></td>
					<td><textarea name="rule" id="ckeditor_rule" class="description"></textarea></td>
				</tr>				
				<tr>
					<td><label for="retail_price">商品市價：</label></td>
					<td><input name="retail_price" id="retail_price" type="number" data-validate="decimal" class="required"/></td>
				</tr>
				<tr>
					<td><label for="cost_price">商品進貨成本價：</label></td>
					<td><input name="cost_price" id="cost_price" type="number" data-validate="decimal" class="required"/></td>
				</tr>				
				<tr>
					<td><label for="thumbnail">商品主圖 (首頁專用 1:1)：</label></td>
					<td><input name="thumbnail" type="file"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail2">商品主圖二 (內頁專用4:3)：</label></td>
					<td><input name="thumbnail2" type="file"/></td>
				</tr>
				<tr>
					<td><label for="thumbnail3">商品主圖三 (內頁專用4:3)：</label></td>
					<td><input name="thumbnail3" type="file"/></td>
				</tr>	
				<tr>
					<td><label for="thumbnail4">商品主圖四 (內頁專用4:3)：</label></td>
					<td><input name="thumbnail4" type="file"/></td>
				</tr>		
				<!-- tr>
					<td><label for="thumbnail_url">商品主圖(外部圖片網址)：</label></td>
					<td><input name="thumbnail_url" type="text" size="60"/></td>
				</tr -->
				<tr>
					<td><label for="pay_type">下標支付方式：</label></td>
					<!-- td>
						<select name="pay_type">
							<option value="a" >殺幣</option>
							<option value="b" selected >殺幣 + S碼 + 殺價卷(限定S碼)</option>
							<option value="c" >殺幣 + 殺價卷(限定S碼)</option>
							<option value="d" >S碼 + 殺價卷(限定S碼)</option>
							<option value="e" >殺價卷(限定S碼)</option>
						</select>
					</td -->
					<td>
						<select name="pay_type">
							<option value="a" >殺價幣</option>
							<option value="b" selected >殺幣 + 超殺券 + 殺價券</option>
							<option value="c" >殺價幣 + 殺價券</option>
							<option value="d" >超殺券 + 殺價券</option>
							<option value="e" >殺價券</option>
							<option value="f" >圓夢券(有返饋+無返饋)</option>
						</select>
						<font color='red'>*注意 : 宅公益商品必須限制只能用殺價幣下標</font>
					</td>
				</tr>
                
				<input type="hidden" name="totalfee_type" id="totalfee_type" value="F" />				
				<!-- tr>
					<td><label for="totalfee_type">下標總金額計算方式：</label></td>
					<td>
						<select name="totalfee_type" id="totalfee_type">
							<option value="A" >手續費加出價金額</option>
							<option value="B" >出價金額</option>
							<option value="F" selected>手續費</option>							
							<option value="O" >免費</option>
						</select>
					</td>
				</tr -->
				<input name="bid_spoint" type="hidden" value="0" />
				<!-- tr>
					<td><label for="bid_spoint">中標得殺幣(點數)：</label></td>
					<td><input name="bid_spoint" type="hidden" value="0" readonly/></td>
				</tr -->
				<input name="bid_scode" type="hidden" value="0" />
				<!-- tr>
					<td><label for="bid_scode">中標S碼(一般發送活動編號)：</label></td>
					<td><input name="bid_scode" type="hidden" value="0" readonly/></td>
				</tr-->
				<input name="bid_oscode" type="hidden" value="0" />
				<!-- tr>
					<td><label for="bid_oscode">中標限定S碼(一般發送活動編號)：</label></td>
					<td><select name="bid_oscode" id="bid_oscode" disabled='disabled'>
						<option value="0">請選擇</option>
					<?php foreach ($this->tplVar['table']['rt']['rule_oscode'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['spid']; ?>" ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
					</td>
					<td><input name="bid_oscode" type="hidden" value="0" readonly/></td>
				</tr -->
				<tr>
					<td><label for="is_flash">閃殺活動：</label></td>
					<td>
						<select name="is_flash">
							<option value="N" selected >否</option>
							<option value="Y" >是</option>
						</select>
					</td>
				</tr>
				<!-- tr>
					<td><label for="flash_loc">閃殺地點：</label></td>
					<td><input name="flash_loc" type="text" size="60" /></td>
				</tr>
				<tr>
					<td><label for="flash_loc_map">閃殺地點地圖嵌入網址：</label></td>
					<td><input name="flash_loc_map" type="text" size="60" /></td>
				</tr -->
				<tr>
					<td><label for="vendorid">(閃殺)場次代碼：</label></td>
					<td><input name="vendorid" type="text" value="saja" /></td>
				</tr>
				<tr>
					<td><label for="saja_fee">下標手續費：</label></td>
					<td><input name="saja_fee" id="saja_fee" type="number" value="0"/></td>
				</tr>
				<tr>
					<td><label for="process_fee">得標處理費：</label></td>
					<td>
						<select name="process_fee">
							<!-- option value="0">0%</option -->
						<?php for($i=20 ; $i <= 30 ; $i+=1) : ?>
							<?php if ($i == (int)$this->tplVar['table']['record'][0]['process_fee']) : ?>
							<option value="<?php echo $i; ?>" selected><?php echo $i."%"; ?></option>
							<?php else : ?>
							<option value="<?php echo $i; ?>"><?php echo $i."%"; ?></option>
							<?php endif; ?>
						<?php endfor; ?>						
						<?php for($i=35 ; $i <= 50 ; $i+=5) : ?>
							<?php if ($i == (int)$this->tplVar['table']['record'][0]['process_fee']) : ?>
							<option value="<?php echo $i; ?>" selected><?php echo $i."%"; ?></option>
							<?php else : ?>
							<option value="<?php echo $i; ?>"><?php echo $i."%"; ?></option>
							<?php endif; ?>
						<?php endfor; ?>
						</select>
					</td>
				</tr>
				<input type="hidden" name="checkout_type" value="all" />
				<!-- tr>
					<td><label for="checkout_type">得標結帳方式：</label></td>
					<td>
						<select name="checkout_type">
							<option value="all" selected>得標金額 + 得標處理費</option>
							<option value="pf" >得標處理費</option>
							<option value="bid"  >得標金額</option>
						</select>
					</td>
				</tr -->
				<input name="checkout_money" type="hidden" value="0"/>
				<!-- tr>
					<td><label for="checkout_money">得標處理金：</label></td>
					<td><input name="checkout_money" type="number" value="0"/></td>
				</tr -->				
				<input  type="hidden" name="price_limit" id="price_limit" value="1" />
				<!-- tr>
					<td><label for="price_limit">底價：</label></td>
					<td><input  type="hidden" name="price_limit" id="price_limit" value="1" /></td>
				</tr -->
				<input type="hidden" name="is_discount" value="Y" />
				<!-- tr>
					<td><label for="is_discount">是否折抵：</label></td>
					<td>
						<select name="is_discount">
							<option value="N" >不能折抵</option>
							<option value="Y" selected>可以折抵</option>
						</select>
					</td>
				</tr -->
				<input type="hidden" name="bonus_type" id="bonus_type" value="ratio" />
				<input type="hidden" name="bonus" id="bonus"  value="100" />
				<!-- tr>
					<td><label for="bonus">紅利積分：</label></td>
					<td>
						<select name="bonus_type" id="bonus_type">
							<option value="none" >不回饋</option>
							<option value="ratio" selected>回饋比例</option>
							<option value="value">回饋定額</option>
						</select><br>
						<input name="bonus" id="bonus" type="text" class="" value="100"/>
						<span id="bonus_unit" class="unit"></span>
					</td>
				</tr -->
				<input type="hidden" id="bid-type" name="bid_type" value="T" />
				<!-- tr>
					<td><label for="bid_type">結標方式</label></td>
					<td>
						<select id="bid-type" name="bid_type">
							<option value="T" selected>時間標</option>
							<option value="count">次數標</option>
							<option value="A">綜合標</option>
						</select>
					</td>
				</tr -->
				<tr>
					<td><label for="ontime">上架時間：</label></td>
					<td><input name="ontime" id="ontime" type="text" class="datetime time-start"/></td>
				</tr>
				<tr>
					<td><label for="offtime">下架時間：</label></td>
					<td><input name="offtime" id="offtime" type="text" class="datetime time-stop"/></td>
				</tr>	
				<input type="hidden" name="saja_limit" value="0" />
				<!-- tr>
					<td><label for="saja_limit">商品結標總次数限制：</label></td>
					<td><input name="saja_limit" type="number" value="0"/> *只算殺價幣下標</td>
				</tr -->
				<input type="hidden" name="user_limit" value="20" />
				<!-- tr>
					<td><label for="user_limit">會員下標總次數限制：</label></td>
					<td><input name="user_limit" type="number" value="20"/>( >=999999 視為不限制下標次數)</td>
				</tr -->
				<input type="hidden" name="usereach_limit" value="20" />
				<!-- tr>
					<td><label for="usereach_limit">會員每次連續下標次數限制：</label></td>
					<td><input name="usereach_limit" type="number" value="100" /></td>
				</tr -->
				<input name="split_rate" type="hidden" value="0"/>
				<!-- tr>
					<td><label for="split_rate">拆分比：</label></td>
					<td><input name="split_rate" type="hidden" value="0"/>％（0.00％~100.00％）</td>
				</tr -->
				<input name="base_value" type="hidden" value="0"/>
				<!-- tr>
					<td><label for="base_value">保底金額：</label></td>
					<td><input name="base_value" type="hidden" value="0"/>0.00~(商品市價*殺幣比率)</td>
				</tr -->
				
				<?php /* 
				//店點暫停 ~ 20140308 peter
				<tr>
					<td><label for="gift">店點：</label></td>
					<td>
						<select name="gift_type" id="gift_type">
							<option value="none" selected>不回饋</option>
							<option value="ratio">回饋比例</option>
							<option value="value">回饋定額</option>
						</select><br>
						<input name="gift" type="text" class="" value="0"/>
						<span id="gift_unit" class="unit"></span>
					</td>
				</tr>*/?>
				<input type="hidden" name="gift_type" value="none">
				<input type="hidden" name="gift" value="0">
				<input type="hidden" name="seq" value="20" >
				<!-- tr>
					<td><label for="seq">商品排序：</label></td>
					<td><input name="seq" type="number" value="1"/></td>
				</tr -->
				<tr>
					<td><label for="seq">商品顯示：</label></td>
					<td>
						<select name="display">
							<option value="Y">顯示</option>
							<option value="N" checked>不顯示</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="display_all">是否在全部商品中顯示：</label></td>
					<td>
						<select name="display_all">
							<option value="Y">顯示</option>
							<option value="N" checked>不顯示</option>
						</select>
					</td>
				</tr>
				
				
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="is_big">大檔商品：</label></td>
					<td>
						<select name="is_big">
							<option value="N" selected >否</option>
							<option value="Y" >是</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="hot_used">熱門商品：</label></td>
					<td>
						<select name="hot_used">
							<option value="N" >否</option>
							<option value="Y"  selected>是</option>
						</select>
					</td>
				</tr>
				<input type="hidden" name="lb_used" value="N" >
				<!-- tr>
					<td><label for="lb_used">直播商品：</label></td>
					<td>
						<select name="lb_used">
							<option value="N" selected >否</option>
							<option value="Y" >是</option>
						</select>
					</td>
				</tr -->
                <input type="hidden" name="loc_type" value="G" >				
				<!-- tr>
					<td><label for="loc_type">限用區域：</label></td>
					<td>
						<select name="loc_type">
							<option value="L" >地區</option>
							<option value="G" selected >全國</option>
						</select>
					</td>
				</tr -->
				<input type="hidden" name="mob_type" value="all" >
				<!-- tr>
					<td><label for="mob_type">手機限用類型：</label></td>
					<td>
						<select name="mob_type">
						    <option value="all" selected >都顯示</option>
							<option value="n">都不顯示</option>
							<option value="weixin">只在微信顯示</option>
							<option value="yixin">只在易信顯示</option>
							<option value="laiwang">只在来往顯示</option>
						</select>
					</td>
				</tr -->
				<tr>
					<td><label for="is_test">是否為測試商品：</label></td>
					<td>
						<select name="is_test">
							<option value="Y" >是</option>
							<option value="N" selected>否</option>
						</select>
					</td>
				</tr>					
				<input type="hidden" name="is_today" value="N" >
				<!-- tr>
					<td><label for="is_today">今日必殺同步新增：</label></td>
					<td>
						<select name="is_today">
							<option value="N" selected >否</option>
							<option value="Y" >是</option>
						</select>
					</td>
				</tr -->
				<tr>
					<td><label for="ptype">商品類型</label></td>
					<td>
						<select name="ptype" id="ptype">
							<option value="0" selected>殺戮商品</option>
							<option value="1" >圓夢商品</option>
						</select>
					</td>
				</tr>			
				<tr>
					<td><label for="description">商品種類：</label></td>
					<td>
						<ul id="ul_pcid" class="relation-list layer1">
						<?php foreach ($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
							<?php if ($pcv['layer'] == 1) : ?>
							<li ptype="<?php echo $pcv['ptype']; ?>">
								<input class="relation-item" type="checkbox" name="pcid[]" id="pcid_<?php echo $pck; ?>" <?php if ($pcv['pcid'] == 0 ){ ?>checked<?php }?> value="<?php echo $pcv['pcid']; ?>"/>
								<span class="relation-item-name" for="pcid_<?php echo $pck; ?>"><?php echo $pcv['name']; ?></span>
							</li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				<tr>
					<td><label for="description">商品子類別：</label></td>
					<td>
						<ul class="relation-list layer2">
						<?php foreach ($this->tplVar['table']['rt']['product_category'] as $pck => $pcv) : ?>
							<?php if ($pcv['layer'] == 2) : ?>
							<li>
								<input class="relation-item" rel="<?php echo $pcv['node']; ?>" type="checkbox" name="pcid[]" id="pcid_<?php echo $pck; ?>" value="<?php echo $pcv['pcid']; ?>"/>
								<span class="relation-item-name" for="pcid_<?php echo $pck; ?>"><?php echo $pcv['name']; ?></span>
							</li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr>
				<tr>
					<td><label for="limitid">限定商品：</label></td>
					<td><select name="limitid">
						<option value="0">請選擇</option>
					<?php foreach ($this->tplVar['table']['rt']['product_limited'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['plid']; ?>" ><?php echo $pcv['name']; ?></option>
					<?php endforeach; ?>
					</select>
					</td>
				</tr>
				<tr>
					<td><label for="is_exchange">是否為兌換商品：</label></td>
					<td>
						<select name="is_exchange">
							<option value="0" selected>否</option>
							<option value="1" >是</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="epid">兌換商品ID：</label></td>
					<td><input name="epid" type="text" value="0" /></td>
				</tr>
				<tr>
					<td><label for="ordertype">出貨方式：</label></td>
					<td>
						<select name="ordertype">
							<option value="1" selected>商品出貨</option>
							<option value="2">鯊魚點出貨</option>
							<option value="3">殺價卷出貨</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="orderbonus">出貨鯊魚點數量：</label></td>
					<td><input name="orderbonus" type="number" value="0" /></td>
				</tr>
				<tr>
					<td><label for="everydaybid">每天付費可下標數：</label></td>
					<td><input name="everydaybid" id="everydaybid" type="number" value="0"/><font style="color:#FF0000">*預設零則不限制下標次數</font></td> 
				</tr>
				<tr>
					<td><label for="freepaybid">每天免費可下標數：</label></td>
					<td><input name="freepaybid" id="freepaybid" type="number" value="0"/><font style="color:#FF0000">*預設零則不限制下標次數</font></td> 
				</tr>
				<tr>
					<td><label for="codepid">殺價券關聯商品編號：</label></td>
					<td><input name="codepid" id="codepid" type="number" value="0"/><font style="color:#FF0000">*僅限殺價券商品使用</font></td>
				</tr>
				<tr>
					<td><label for="codenum">殺價券出貨數量：</label></td>
					<td><input name="codenum" id="codenum" type="number" value="0"/><font style="color:#FF0000">*僅限殺價券商品使用</font></td>
				</tr>
				<!--tr>
					<td><label for="description">經銷商：</label></td>
					<td>
						<ul class="relation-list channel">
						<?php foreach ($this->tplVar['table']['rt']['channel_store_rt'] as $ck => $cv) : ?>
							<li>
								<input class="relation-item" type="checkbox" name="channelid[]" id="channelid_<?php echo $ck; ?>" value="<?php echo $cv['channelid']; ?>" <?php if ($cv['channelid'] == 0 ){ ?>checked<?php }?> />
								<span class="relation-item-name" for="channelid_<?php echo $ck; ?>"><?php echo $cv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr-->
				<input class="relation-item" type="hidden" name="channelid[]" id="channelid_0" value="0"  />
				<input class="relation-item" type="hidden" name="storeid[]" id="storeid_0" value="0" />
				<!--tr>
					<td><label for="description">分館：</label></td>
					<td>
						<ul class="relation-list store">
						<?php foreach ($this->tplVar['table']['rt']['store_product_rt'] as $sk => $sv) : ?>
							<li>
								<input class="relation-item" rel="<?php echo $sv['channelid']; ?>" type="checkbox" name="storeid[]" id="storeid_<?php echo $sk; ?>" value="<?php echo $sv['storeid']; ?>" <?php if ($sv['storeid'] == 0 ){ ?>checked<?php }?> />
								<span class="relation-item-name" for="storeid_<?php echo $sk; ?>"><?php echo $sv['name']; ?></span>
							</li>
						<?php endforeach; ?>
						</ul>
					</td>
				</tr-->
			</table>
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input name="add_item" id="add_item" type="button" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>

<script type="text/javascript">
$(function() {
	var ptype = 0;
	$("#ul_pcid").find("li[ptype]").hide();
	$("#ul_pcid").find("li[ptype='"+ptype+"']").show();
});
$("#ptype").change(function(event) {
	var ptype = $(this).val();
	$("#ul_pcid").find("li[ptype]").hide();
	$("#ul_pcid").find("li[ptype='"+ptype+"']").show();
});


</script>
