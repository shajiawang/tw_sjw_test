<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_no">檔期編號：</span>
					<input type="text" name="search_no" size="20" value="<?php echo $this->io->input['get']['search_no']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_start">開始日期：</span>
					<input type="text" name="search_start" size="20" value="<?php echo $this->io->input['get']['search_start']; ?>"/>
				</li>				
				<li class="search-field">
					<span class="label" for="search_end">結束日期：</span>
					<input type="text" name="search_end" size="20" value="<?php echo $this->io->input['get']['search_end']; ?>"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/add/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">新增</a>
						</div>						
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>修改</th>
								<th class="sortable">
								<?php if ($this->tplVar["status"]["get"]["sort_adid"] == 'asc') : ?>
									<a href="<?php echo $this->tplVar['status']['base_href']; ?>&sort_adid=desc">檔期編號<span class="sort asc"></span></a>
								<?php elseif ($this->tplVar["status"]["get"]["sort_adid"] == 'desc') : ?>									
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_adid=">檔期編號<span class="sort desc"></span></a>
								<?php else : ?>
									<a href="<?php echo $this->tplVar['status']['base_href'] ;?>&sort_adid=asc">檔期編號<span class="sort init"></span></a>
								<?php endif; ?>
								</th>
								<th>開始日期</th>
								<th>結束日期</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/no=<?php echo $rv["no"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="no"><?php echo $rv['no']; ?></td>
								<td class="column" name="date_start"><?php echo $rv['date_start']; ?></td>
								<td class="column" name="date_end"><?php echo $rv['date_end']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>