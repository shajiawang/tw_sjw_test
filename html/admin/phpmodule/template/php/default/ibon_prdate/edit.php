<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="no">檔期編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['no']; ?></td>
				</tr>
				<tr>
					<td><label for="date_start">開始日期：</label></td>
					<td><input name="date_start" type="text" value="<?php echo $this->tplVar['table']['record'][0]['date_start']; ?>"/> * 格式為西元年 月 日 如: 20190515</td>
				</tr>
				<tr>
					<td><label for="date_end">結束日期：</label></td>
					<td><input name="date_end" type="text"  value="<?php echo $this->tplVar['table']['record'][0]['date_end']; ?>" data-org="<?php echo $this->tplVar['table']['record'][0]['date_end']; ?>"/> * 格式為西元年 月 日 如: 20190515 </td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="no" value="<?php echo $this->tplVar["status"]["get"]["no"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>