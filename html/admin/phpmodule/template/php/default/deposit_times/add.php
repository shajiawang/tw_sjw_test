<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" enctype="multipart/form-data" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="times">倍數：</label></td>
					<td>
						<select name="times">
							<option value="1">1倍</option>
							<option value="2">2倍</option>
							<option value="3">3倍</option>
							<option value="4">4倍</option>
							<option value="5">5倍</option>
						</select>
					</td>					
				</tr>
				<tr>
					<td><label for="thumbnail">主圖：</label></td>
					<td>
						<input name="thumbnail" type="file"/><br>
					</td>
				</tr>				
				<tr>
					<td><label for="btime">開始時間：</label></td>
					<td><input name="btime" type="text" class="datetime time-start" value=""/></td>
				</tr>
				<tr>
					<td><label for="etime">結束時間：</label></td>
					<td><input name="etime" type="text" class="datetime time-stop" value="" data-org=""/></td>
				</tr>				
			</table>
			

			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>