<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="stockid">庫存ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['stockid']; ?>
						<input type="hidden" name="stockid" value="<?php echo $this->tplVar["status"]["get"]["stockid"] ;?>">
					</td>
				</tr>
				<tr>
					<td><label for="epid">商品ID：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['epid']; ?></td>
				</tr>
				<tr>
					<td><label for="num">庫存數量：</label></td>
					<td><input name="num" type="text" value="<?php echo $this->tplVar['table']['record'][0]['num']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="behav">庫存行為：</label></td>
					<td><input name="behav" type="text" value="<?php echo $this->tplVar['table']['record'][0]['behav']; ?>"/></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>