<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="acid">主題編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['acid']; ?></td>
				</tr>
				<tr>
					<td><label for="place">選擇主題：</label></td>
					<td>
						<select name="place">
							<option value="1" <?php echo $this->tplVar['table']['record'][0]['place']=="1"?" selected":""; ?>>首頁上方輪播</option>
							<option value="2" <?php echo $this->tplVar['table']['record'][0]['place']=="2"?" selected":""; ?>>首頁下方輪播</option>
							<option value="3" <?php echo $this->tplVar['table']['record'][0]['place']=="3"?" selected":""; ?>>殺戮戰場</option>							
							<option value="4" <?php echo $this->tplVar['table']['record'][0]['place']=="4"?" selected":""; ?>>鯊魚商城</option>
						</select>					
					</td>
				</tr>				
				<tr>
					<td><label for="name">主題：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="description">主題敘述：</label></td>
					<td><input name="description" type="text" value="<?php echo $this->tplVar['table']['record'][0]['description']; ?>" /></td>
				</tr>
				<tr>
					<td><label for="used">主題啓用：</label></td>
					<td>
						<select name="used">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['used']=="Y"?" selected":""; ?>>啓用</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['used']=="N"?" selected":""; ?> >關閉</option>
						</select>						
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="acid" value="<?php echo $this->tplVar["status"]["get"]["acid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>