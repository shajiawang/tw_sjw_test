<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="place">選擇廣告版位：</label></td>
					<td>
						<select name="place">
							<option value="1" selected>首頁上方輪播</option>
							<option value="2" >首頁下方輪播</option>
							<option value="3" >殺戮戰場</option>							
							<option value="4" >鯊魚商城</option>
						</select>					
					</td>
				</tr>
				<tr>
					<td><label for="name">主題：</label></td>
					<td><input name="name" type="text"/></td>
				</tr>
				<tr>
					<td><label for="description">主題敘述：</label></td>
					<td><input name="description" type="text"/></td>
				</tr>
				<tr>
					<td><label for="used">主題啓用：</label></td>
					<td>
						<select name="used">
							<option value="Y" selected>啓用</option>
							<option value="N" >關閉</option>
						</select>						
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="seq" id="seq" value="1">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>