<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_no">檔期編號：</span>
					<select name="search_no">
						<option value="">All</option>
					<?php foreach ($this->tplVar['table']['rt']['prdate'] as $pck => $pcv) : ?>
						<option value="<?php echo $pcv['no']; ?>" <?php if ($this->tplVar['status']["search"]["search_no"] == $pcv['no']) {echo "selected";} ?> ><?php echo $pcv['date_start']. '<=>' .$pcv['date_end']; ?></option>
					<?php endforeach; ?>
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_code">活動代號：</span>
					<input type="text" name="search_code" size="20" value="<?php echo $this->io->input['get']['search_code']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_pname">商品名稱：</span>
					<input type="text" name="search_pname" size="20" value="<?php echo $this->io->input['get']['search_pname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_uid">會員編號：</span>
					<input type="text" name="search_uid" size="20" value="<?php echo $this->io->input['get']['search_uid']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_btime">開始時間：</span>
					<input type="text" name="search_btime" size="20" value="<?php echo $this->io->input['get']['search_btime']; ?>" class="datetime time-start"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_etime">截止時間：</span>
					<input type="text" name="search_etime" size="20" value="<?php echo $this->io->input['get']['search_etime']; ?>" class="datetime time-stop"/>
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header"><div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>內容</th>
								<th>交易編號</th>
								<th>商品編號</th>
								<th>活動代號</th>
								<th>商品名稱</th>
								<th>會員編號</th>
								<th>總點數</th>
								<th>兌換檔次</th>
								<th>兌換日期</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="icon"><a class="icon-edit icons" href="<?php echo $this->config->default_main.'/'.$this->tplVar["status"]["get"]["fun"]; ?>/edit/ibonexcid=<?php echo $rv["ibonexcid"];?>&location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>"></a></td>
								<td class="column" name="ibonexcid"><?php echo $rv['ibonexcid']; ?></td>
								<td class="column" name="id"><?php echo $rv['id']; ?></td>
								<td class="column" name="code"><?php echo $rv['code']; ?></td>
								<td class="column" name="prname"><?php echo $rv['prname']; ?></td>
								<td class="column" name="tr_ibons"><?php echo $rv['u_id']; ?></td>
								<td class="column" name="tr_ibons"><?php echo $rv['tr_ibons']; ?></td>
								<td class="column" name="prdate_no"><?php echo $rv['prdate_no']; ?></td>
								<td class="column" name="u_date"><?php echo $rv['u_date']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>