<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="id">商品編號</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['id']; ?></td>
				</tr>
				<tr>
					<td><label for="code">活動代號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['code']; ?></td>
				</tr>
				<tr>
					<td><label for="prname">商品名稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['prname']; ?></td>
				</tr>
				<tr>
					<td><label for="detail_num">電文單號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['detail_num']; ?></td>
				</tr>
				<tr>
					<td><label for="u_id">會員名稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['nickname']; ?> (<?php echo $this->tplVar['table']['record'][0]['u_id']; ?>)</td>
				</tr>
				<tr>
					<td><label for="u_date">日期：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['u_date']; ?></td>
				</tr>
				<tr>
					<td><label for="tr_num">兌換數量：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['tr_num']; ?></td>
				</tr>
				<tr>
					<td><label for="tr_ibons">總ibon點數：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['tr_ibons']; ?></td>
				</tr>
				<tr>
					<td><label for="tr_price">總金額：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['tr_price']; ?></td>
				</tr>
				<tr>
					<td><label for="bonusid">鯊魚點紀錄編號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['bonusid']; ?></td>
				</tr>
				<tr>
					<td><label for="prdate_no">兌換檔次：</label></td>
					<td>
						<select name="prdate_no">
							<option value="">All</option>
						<?php foreach ($this->tplVar['table']['rt']['prdate'] as $pck => $pcv) : ?>
							<option value="<?php echo $pcv['no']; ?>" <?php echo $this->tplVar['table']['record'][0]['prdate_no']==$pcv['no'] ? "selected":""; ?>><?php echo $pcv['date_start']. '<=>' .$pcv['date_end']; ?></option>
						<?php endforeach; ?>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="num" value="<?php echo $this->tplVar["status"]["get"]["num"] ;?>">
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">回上一頁</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>