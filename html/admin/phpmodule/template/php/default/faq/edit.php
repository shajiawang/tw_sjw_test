<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="faqid">問答ID</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['faqid']; ?></td>
				</tr>
				<tr>
					<td><label for="menu">顯示目錄名稱：</label></td>
					<td><input name="menu" type="text" value="<?php echo $this->tplVar['table']['record'][0]['menu']; ?>" maxlength="12" /> (限12個字符!)</td>
				</tr>
				<tr>
					<td><label for="name">問答標題：</label></td>
					<td><input name="name" type="text" value="<?php echo $this->tplVar['table']['record'][0]['name']; ?>" maxlength="100" /></td>
				</tr>
				<tr>
					<td><label for="description">問答內容：</label></td>
					<td><textarea name="description" id="ckeditor_description"><?php echo htmlspecialchars_decode($this->tplVar['table']['record'][0]['description']); ?></textarea></td>
				</tr>
				<tr>
					<td><label for="desc_only">只顯示內容：</label></td>
					<td>
						<select name="desc_only">
							<option value="Y" <?php echo $this->tplVar['table']['record'][0]['desc_only']=="Y" ? "selected" : "";?> >是</option>
							<option value="N" <?php echo $this->tplVar['table']['record'][0]['desc_only']=="N" ? "selected" : "";?> >否</option>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="<?php echo $this->tplVar['table']['record'][0]['seq']; ?>"/></td>
				</tr>
				
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="fcid">分類：</label></td>
					<td>
						<select name="fcid">
						<?php foreach($this->tplVar['table']['rt']['faq_category'] as $ck => $cv) : ?>
							<option value="<?php echo $cv['fcid']; ?>" <?php if($this->tplVar['table']['record'][0]['fcid']==$cv['fcid']){ echo 'selected'; } ?> ><?php echo $cv['name']; ?></option>
						<?php endforeach; ?>
						</select>
						</ul>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="faqid" value="<?php echo $this->tplVar["status"]["get"]["faqid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>