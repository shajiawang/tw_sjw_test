<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	    <script type="text/javascript">
	    	//區域
			function country(id) {
				$('select[name="regionid"] option').hide();
				$('select[name="regionid"]').val("");
				$('select[name="regionid"] option[rel=""]').show();
				$('select[name="regionid"] option[rel='+id+']').show();
				
				region($('select[name="countryid"] option').val());
				province($('select[name="regionid"] option').val());
			}
	    	
	    	//省(直轄市)
			function region(id) {
				$('select[name="provinceid"] option').hide();
				$('select[name="provinceid"]').val("");
				$('select[name="provinceid"] option[rel=""]').show();
				$('select[name="provinceid"] option[rel='+id+']').show();
				
				province($('select[name="regionid"] option').val());
			}
			
			//經銷商(分區)
			function province(id) {
				$('select[name="channelid"] option').hide();
				$('select[name="channelid"]').val("");
				$('select[name="channelid"] option[rel=""]').show();
				$('select[name="channelid"] option[rel='+id+']').show();
			}
	    	
			$(function(){
				$('select[name="countryid"] option[value="<?php echo $this->tplVar['table']['record'][0]['countryid']; ?>"]').prop('selected', true);
				$('select[name="regionid"] option').hide();
				$('select[name="regionid"] option[rel=""]').show();
				$('select[name="regionid"] option[rel="<?php echo $this->tplVar['table']['record'][0]['countryid']; ?>"]').show();
				$('select[name="regionid"] option[value="<?php echo $this->tplVar['table']['record'][0]['regionid']; ?>"]').prop('selected', true);
				$('select[name="provinceid"] option').hide();
				$('select[name="provinceid"] option[rel=""]').show();
				$('select[name="provinceid"] option[rel="<?php echo $this->tplVar['table']['record'][0]['regionid']; ?>"]').show();
				$('select[name="provinceid"] option[value="<?php echo $this->tplVar['table']['record'][0]['provinceid']; ?>"]').prop('selected', true);
				$('select[name="channelid"] option').hide();
				$('select[name="channelid"] option[rel=""]').show();
				$('select[name="channelid"] option[rel="<?php echo $this->tplVar['table']['record'][0]['provinceid']; ?>"]').show();
				$('select[name="channelid"] option[value="<?php echo $this->tplVar['table']['record'][0]['channelid']; ?>"]').prop('selected', true);
			});
		</script>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-edit" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/update">
			<div class="form-label">編輯資料</div>
			<table class="form-table">
				<tr>
					<td><label for="loginname">使用者帳號</label></td>
					<td><input name="loginname" type="text" value="<?php echo $this->tplVar['table']['record'][0]['loginname']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="passwd">密碼：</label></td>
					<td><input name="passwd" type="password"/></td>
				</tr>
				<tr>
					<td><label for="passwd_confirm">密碼確認：</label></td>
					<td><input name="passwd_confirm" type="password"/></td>
				</tr>
				<tr>
					<td><label for="fullname">姓名：</label></td>
					<td><input name="fullname" type="text" value="<?php echo $this->tplVar['table']['record'][0]['fullname']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="phone">電話：</label></td>
					<td><input name="phone" type="text" value="<?php echo $this->tplVar['table']['record'][0]['phone']; ?>"/></td>
				</tr>
				<tr>
					<td><label for="email">電子郵件：</label></td>
					<td><input name="email" type="text" value="<?php echo $this->tplVar['table']['record'][0]['email']; ?>"/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="countryid">國家：</label></td>
					<td>
						<select name="countryid" onchange="country(this.value);">
							<option value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['country'])) {
									foreach ($this->tplVar['table']['rt']['country'] as $key => $value) {
										echo '<option value="'.$value['countryid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="regionid">區域：</label></td>
					<td>
						<select name="regionid" onchange="region(this.value);">
							<option rel="" value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['region'])) {
									foreach ($this->tplVar['table']['rt']['region'] as $key => $value) {
										echo '<option rel="'.$value['countryid'].'" value="'.$value['regionid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="provinceid">省(直轄市)：</label></td>
					<td>
						<select name="provinceid" onchange="province(this.value);">
							<option rel="" value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['province'])) {
									foreach ($this->tplVar['table']['rt']['province'] as $key => $value) {
										echo '<option rel="'.$value['regionid'].'" value="'.$value['provinceid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
				<tr>
					<td><label for="channelid">經銷商(分區)：</label></td>
					<td>
						<select name="channelid">
							<option rel="" value="">請選擇</option>
							<?php
								if (is_array($this->tplVar['table']['rt']['channel'])) {
									foreach ($this->tplVar['table']['rt']['channel'] as $key => $value) {
										echo '<option rel="'.$value['provinceid'].'" value="'.$value['channelid'].'">'.$value['name'].'</option>';
									}
								}
							?>
						</select>
					</td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="faid" value="<?php echo $this->tplVar["status"]["get"]["faid"] ;?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>