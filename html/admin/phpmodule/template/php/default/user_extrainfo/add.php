<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
	</head>
	<body>
<!--		<div id="left-control" class="left-control fold-horizontal expanded"></div>-->
		<?php require_once $this->tplVar["block"]["breadcrumb"]; ?>
		
		<form class="form" id="form-add" method="post" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/insert">
			<div class="form-label">新增資料</div>
			<table class="form-table">
				<tr>
					<td><label for="userid">會員id：</label></td>
					<td><?php echo $this->io->input['get']['userid']; ?></td>
				</tr>
				<?php 
					if ($this->tplVar['table']['rt']['ue_category']) {
						$flagArr = str_split($this->tplVar['table']['rt']['ue_category']['flag']);
						foreach ($flagArr as $fk => $fv) {
							if ($fv) {
								$fieldname = 'field'.($fk + 1).'name';
				?>
				<tr>
					<td><label for="<?php echo $fieldname; ?>"><?php echo $this->tplVar['table']['rt']['ue_category'][$fieldname]; ?>：</label></td>
					<td><input name="<?php echo $fieldname; ?>" type="text" /></td>
				</tr>
				<?php } } } ?>
				<tr>
					<td><label for="seq">排序：</label></td>
					<td><input name="seq" type="text" value="0"/></td>
				</tr>
			</table>
			
			<table class="form-table">
				<tr>
					<td><label for="uecid">分類：</label></td>
					<td><?php echo $this->tplVar['table']['rt']['ue_category']['name']; ?></td>
				</tr>
			</table>
			
			<div class="functions">
				<input type="hidden" name="location_url" value="<?php echo $this->tplVar["status"]['get']['location_url']; ?>">
				<input type="hidden" name="userid" value="<?php echo $this->io->input['get']['userid']; ?>">
				<input type="hidden" name="uecid" value="<?php echo $this->io->input['get']['uecid']; ?>">
				<div class="button submit"><input type="submit" value="送出" class="submit"></div>
				<div class="button cancel"><a href="<?php echo urldecode(base64_decode($this->tplVar["status"]['get']['location_url'])); ?>">取消</a></div>
				<div class="clear"></div>
			</div>
		</form>
	</body>
</html>