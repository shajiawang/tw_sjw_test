<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>儲值統計</a>
		</div>
		<form class="form">
			<div class="form-label">會員資料</div>
			<table class="form-table">
				<tr>
					<td><label for="name">會員帳號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['name']; ?></td>
				</tr>
				<tr>
					<td><label for="nickname">會員暱稱：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['nickname']; ?></td>
				</tr>
				<tr>
					<td><label for="description">性别：</label></td>
					<td><?php if ($this->tplVar['table']['record'][0]['gender'] == 'male') { echo '男'; } else { echo '女'; } ?></td>
				</tr>
				<tr>
					<td><label for="phone">聯絡電話：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['phone']; ?>(<?php if ($this->tplVar['table']['record'][0]['sms_verified'] == 'Y') { echo '已驗證'; } else { echo '未驗證'; } ?>)</td>
				</tr>
				<tr>
					<td><label for="phone">收件人：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['addressee']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">收件人郵遞區號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['area']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">收件地址：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['address']; ?></td>
				</tr>
				<tr>
					<td><label for="phone">註冊來源：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['sso_name']; ?></td>
				</tr>
			</table>
			
			<div class="functions">
				<div class="button submit"><input type="button" value="回上一頁" class="button" onclick="location.href='<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>';"></div>
				<!--<div class="button submit"><input type="button" value="回上一頁" class="button" onclick="history.back();"></div>-->
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>