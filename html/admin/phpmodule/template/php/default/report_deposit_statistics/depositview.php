<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<script type="text/javascript">
		$(function(){
		})
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>儲值統計</a>
		</div>
		<form class="form">
			<div class="form-label">儲值資料</div>
			<table class="form-table">
				<tr>
					<td><label for="depositid">儲值單號：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['depositid']; ?></td>
				</tr>
				<tr>
					<td><label for="depositid">儲值記錄單號(紅陽orderid)：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['dhid']; ?></td>
				</tr>
				<tr>
					<td><label for="drname">匯款來源：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['drname']; ?></td>
				</tr>
				<tr>
					<td><label for="amount">匯款金額：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['amount']; ?></td>
				</tr>
				<tr>
					<td><label for="currency">幣別：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['currency']; ?></td>
				</tr>
				<tr>
					<td><label for="countryid">國家：</label></td>
					<td><?php if ($this->tplVar['table']['record'][0]['countryid'] == 1) { echo '臺灣'; } elseif ($this->tplVar['table']['record'][0]['countryid'] == 2) { echo '中國'; } ?></td>
				</tr>
				<tr>
					<td><label for="phone">入帳時間：</label></td>
					<td><?php echo $this->tplVar['table']['record'][0]['modifyt']; ?></td>
				</tr>
			</table>
			
			<div class="functions">
				<div class="button submit"><input type="button" value="回上一頁" class="button" onclick="location.href='<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>';"></div>
				<!--<div class="button submit"><input type="button" value="回上一頁" class="button" onclick="history.back();"></div>-->
				<div class="clear"></div>
			</div>
		</form>
		<div class="dialogs"></div>
	</body>
</html>