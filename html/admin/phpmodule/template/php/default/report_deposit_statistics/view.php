<!DOCTYPE html>
<html>
	<head>
		<title>Main Frame</title>
		<?php require_once $this->tplVar["block"]["head"]; ?>
		<style>
			.black_list{
				background-color: red !important;
			}
			.white_list{
				background-color: green !important;
			}
		</style>
		<script>
			function list_change(Rtype,ip){
				var aj = $.ajax( {
					url:'/admin/block_list/set/',// 跳轉到 action
					data:{
						Rtype : Rtype,
						ipaddr : ip,
					},
					type:'post',
					cache:false,
					dataType:'json',
					success:function(data) {
						if(data.retCode =="1" ){
							alert("修改成功！");
							$(".ipcol").each(function(){

								if (this.innerText.replace('B','').replace('W','')==ip){
									if (Rtype=='black_list'){
										if (data.retStatus=='show'){
											$(this).addClass('black_list');
										}else{
											$(this).removeClass('black_list');
										}
									}else{
										if (data.retStatus=='show'){
											$(this).addClass('white_list');
										}else{
											$(this).removeClass('white_list');
										}
									}

								}
							})
						}else{
							alert(data.retMsg);
						}
					},
					error : function() {
						alert("異常！");
					}
				});
			}
		</script>
	</head>
	<body>
		<div id="left-control" class="left-control fold-horizontal expanded"></div>
		<div class="breadcrumb header-style">
			<a href="<?php echo $this->config->default_main; ?>">首頁</a>>>
			<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>">資料統計</a>>>
			<a>儲值統計</a>
		</div>
		<div class="searchbar header-style">
			<form method="get" action="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/<?php echo $this->io->input['get']['act']; ?>">
			<ul>
				<li class="search-field">
					<span class="label" for="search_joindate">會員加入日期：</span>
					<input name="joindatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindatefrom']; ?>" /> ~
					<input name="joindateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['joindateto']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_depositdate">會員儲值日期：</span>
					<input name="depositdatefrom" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['depositdatefrom']; ?>" /> ~
					<input name="depositdateto" type="text" class="datetime" size="20" value="<?php echo $this->io->input['get']['depositdateto']; ?>"/>
				</li>

				<li class="search-field">
					<span class="label" for="search_userid">會員編號：</span>
						<input type="text" name="userid" value="<?php echo $this->io->input['get']['userid']; ?>" />
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_src_ip">IP：</span>
						<input type="text" name="src_ip" value="<?php echo $this->io->input['get']['src_ip']; ?>" />
					</select>
				</li>
				<!-- li class="search-field">
					<span class="label" for="search_name">會員帳號：</span>
						<input type="text" name="name" value="<?php echo $this->io->input['get']['name']; ?>" />
					</select>
				</li-->
				<li class="search-field">
					<span class="label">暱稱：</span>
					<input type="text" name="nickname" size="20" value="<?php echo $this->io->input['get']['nickname']; ?>"/>
				</li>
				<li class="search-field">
					<span class="label" for="search_depositfrom">儲值方式：</span>
					<select name="depositfrom" id="depositfrom"/>
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['depositfrom'] as $key => $value) : ?>
						<option value="<?php echo $value['act']; ?>" <?php echo ($this->io->input['get']['depositfrom'] == $value['act']) ? "selected" : "";?> ><?php echo $value['name']; ?></option>
						<?php endforeach; ?>
					</select>
				</li>
				<!-- <li class="search-field">
					<span class="label" for="search_gender">性別：</span>
					<select name="gender" id="gender"/>
						<option value="">全部</option>
						<?php foreach ($this->tplVar['status']['gender'] as $key => $value) : ?>
						<option value="<?php echo $key; ?>" <?php echo ($this->io->input['get']['gender'] == $key) ? "selected" : "";?> ><?php echo $value; ?></option>
						<?php endforeach; ?>
					</select>
				</li> -->
				<li class="search-field">
					<span class="label" for="search_dhid">儲值記錄編號：</span>
						<input type="text" name="dhid" value="<?php echo $this->io->input['get']['dhid']; ?>" />
					</select>
				</li>
				<li class="search-field">
					<span class="label" for="search_usergroup">會員帳號群組：</span>
					<input type="checkbox" name="usergroup" id="usergroup" value="1" <?php echo ($this->io->input['get']['usergroup']) ? "checked" : "";?> />
				</li>
				<li class="button">
					<button>搜尋</button>
				</li>
			</ul>
			</form>
		</div>
		<div class="table-wrapper">
			<ul class="table">
				<li class="header">
					<div class="functions">
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/export/location_url=<?php echo base64_encode(urlencode($this->tplVar['status']['base_href'])) ;?>">匯出</a>
						</div>
						<div class="button">
							<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/depositpic/" >當年度報表</a>
						</div>
					</div>
				</li>
				<li class="body">
					<table>
						<thead>
							<tr>
								<th>編號</th>
								<th>會員帳號</th>
								<th>暱稱</th>
								<!--th>加入日期</th>
								<th>充值單號</th-->
								<th>儲值時間</th>
								<th>儲值方式</th>
								<th>台幣儲值金額</th>
								<th>稅額</th>
								<th>IP</th>
								<th>外幣幣別</th>
								<th>外幣金額</th>
								<th>獲得殺價幣</th>
								<th>修改者</th>
							</tr>
						</thead>
						<tbody>
							<?php if (is_array($this->tplVar["table"]['record'])) : ?>
							<?php foreach($this->tplVar["table"]['record'] as $rk => $rv) : ?>
							<tr>
								<td class="column" name="name"><?php echo $rv['userid']; ?></td>
								<td class="column" name="name"><?php echo $rv['name']; ?></td>
								<td class="column" name="nickname"><a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/userview/?userid=<?php echo $rv['userid']; ?>" ><?php echo $rv['nickname']; ?></a></td>
								<!--td class="column" name="joindate"><?php echo $rv['joindate']; ?></td>
								<td class="column" name="depositid"><?php echo $rv['depositid']; ?></td-->
								<td class="column" name="deposit_date">
									<?php if ($this->io->input['get']['usergroup']) { ?>
									<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/depositlist/?userid=<?php echo $rv['userid']; ?>" >
									<?php } else { ?>
									<a href="<?php echo $this->config->default_main; ?>/<?php echo $this->io->input['get']['fun']; ?>/depositview/?depositid=<?php echo $rv['depositid']; ?>">
									<?php } ?>
									<?php echo $rv['deposit_date']; ?></a></td>
								<td class="column" name="behav"><?php echo $rv['behav']; ?></td>
								<td class="column" name="total_amount"><?php echo round($rv['total_amount']); ?></td>
								<td class="column" name="tax"><?php echo (round($rv['total_amount'])-round($rv['total_amount']/1.05)); ?></td>
								<td class="column ipcol <?php if ($rv['inblack']) echo "black_list";?><?php if ($rv['inwhite']) echo "white_list";?>" name="src_ip" id='ip<?php echo $rv['src_ip']; ?>'><?php echo $rv['src_ip']; ?><button onclick="list_change('black_list','<?php echo $rv['src_ip']; ?>')" title='黑名單設定'>B</button><button onclick="list_change('white_list','<?php echo $rv['src_ip']; ?>')" title='白名單設定'>W</button></td>
								<td class="column" name="ori_currency"><?php echo $rv['ori_currency']; ?></td>
								<td class="column" name="ori_amount"><?php echo intval($rv['ori_amount'] * 100) / 100; ?></td>
								<td class="column" name="spoints_by_deposit"><?php echo round($rv['spoints_by_deposit']); ?></td>
								<td class="column" name="modifiername"><?php echo $rv['modifiername']; ?></td>
							</tr>
							<?php endforeach; ?>
							<?php endif; ?>
						</tbody>
					</table>
				</li>
				<li class="footer">
					<div style="float:left;color: #4b6d23;">總累計金額: <?php echo $this->tplVar['status']['alltotal'];?> 元 </div>
				<!-- Page Start -->
					<?php include_once $this->tplVar["block"]["page"]; ?>
				<!-- Page End -->
				</li>
			</ul>
		</div>
	</body>
</html>